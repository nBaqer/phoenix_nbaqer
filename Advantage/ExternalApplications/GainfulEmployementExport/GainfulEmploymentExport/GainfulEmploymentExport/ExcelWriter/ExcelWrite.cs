﻿using System;
using System.Collections.Generic;

namespace GainfulEmploymentExport.ExcelWriter
{
    public class ExcelWrite : ExcelFileWriter<int>
    {
        public object[,] myExcelData;
        private int myRowCnt;
        public override object[] Headers
        {
            get
            {
                object[] headerName = { "Award Year","Student Social Security Number","Student First Name","Student Middle Name", "Student Last Name","Student Date of Birth","Institution Code (OPEID)","Institution Name",  
                                         "Program Name","CIP Code","Credential Level","Medical or Dental Internship or Residency","Program Attendance Begin Date","Program Attendance Begin Date for This Award Year", 
                                        "Program Attendance Status During Award Year","Program Attendance Status Date","Private Loans Amount","Institutional Debt","Tuition and Fees Amount",
                                        "Allowance For Books, Supplies, and Equipment","Length of GE Program","Length of GE Program Measurement","Student’s Enrollment Status as of the 1st Day of Enrollment in Program"};
                return headerName;
            }
        }

        public override void FillRowData(List<string> list)
        {



            myRowCnt = list.Count ;
            
            myExcelData = new object[RowCount + 1, 23];
            for (int row = 1; row <= myRowCnt; row++)
            {
                string[] myRow = list[row - 1].Split(',');
                Console.WriteLine("Data for student: " + myRow[2] + " " + myRow[4]);

                for (int col = 0; col < 23; col++)
                {
                    myExcelData[row, col] = myRow[col];
                }
            }
        }

        public override object[,] ExcelData
        {
            get
            {
                return myExcelData;
            }
        }

        public override int ColumnCount
        {
            get
            {
                return 23;
            }
        }

        public override int RowCount
        {
            get
            {
                return myRowCnt;
            }
        }
    }
}
