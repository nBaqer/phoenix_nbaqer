﻿
using System;
using System.Configuration;
using System.IO;
using GainfulEmploymentExport.Data;
using GainfulEmploymentExport.ExcelWriter;


namespace GainfulEmploymentExport
{
    
    class Program
    {
        static void Main(string[] args)
        {
            var d = new ExportData();
            var fileDataList = d.CreateDataAllYearsCampuses();

            Console.WriteLine("Files To Create: " + fileDataList.Count.ToString());

            var folderPath = ConfigurationManager.AppSettings["OUTPUT_PATH"];

            ExcelFileWriter<int> myExcel = new ExcelWrite();
            foreach (var myList in fileDataList)
            {
                var filePath = Path.Combine(folderPath, myList.FileName);
                Console.WriteLine("Writing file: " + filePath);
                myExcel.WriteDateToExcel(filePath, myList.ExcelData, "A1", "W1");
            }
        }
    }
}
