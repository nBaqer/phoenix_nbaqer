﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace GainfulEmploymentExport.Data
{
    public class ExportData
    {
        #region Private Class Members
        private readonly nsldslinqDataContext context = new nsldslinqDataContext();

        private List<Campuses> _campuses;
        private List<AwardYears> _awardYears;
        private List<FileData> _fileDataList;
        #endregion


        #region Public Class Methods

        /// <summary>
        /// defeault constructor
        /// </summary>
        public ExportData()
        {
            
            GetCampuses();
            GetAwardYears();
            _fileDataList = new List<FileData>();

        }


        /// <summary>
        /// Get data for all campuses and award years
        /// </summary>
        /// <returns></returns>
        public List<FileData> CreateDataAllYearsCampuses()
        {
            //List<string> xDataList = new List<string>();
            foreach (var year in _awardYears)
            {
                foreach (var campus in _campuses)
                {
                    var exData = GetData(year.Year, campus.CampusId.ToString());

                    var listOfData = Split(exData);
                    int index = 0;
                    foreach (var l in listOfData)
                    {
                        index += 1;
                        var fData = new FileData
                        {
                            FileName = year.Year + "_" + campus.CampusCode + "_("  + index + ")_" + Guid.NewGuid().ToString() + ".xls",
                            ExcelData = l.ToList()
                        };

                        _fileDataList.Add(fData);

                    }

                    
                }
            }
            return _fileDataList;
        }

        #endregion


        #region Private Class Methods
        /// <summary>
        /// Split the list of string in to 50 row chunks
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static IEnumerable<List<string>> Split(IEnumerable<string> source)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / 300)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }


        /// <summary>
        /// get active campuses
        /// </summary>
        private void GetCampuses()
        {

            var statusId = context.syStatuses.FirstOrDefault(r => r.StatusCode == "A").StatusId;

            _campuses = (from r in context.syCampuses
                         where r.StatusId == statusId
                         select new Campuses()
                         {
                             CampusId = r.CampusId,
                             CampusCode = r.CampCode
                         }).ToList();

        }


        /// <summary>
        /// get award years from config file
        /// </summary>
        private void GetAwardYears()
        {
            _awardYears = new List<AwardYears>();
            

            var years = ConfigurationManager.AppSettings["AWARD_YEARS"].Split(',');
            foreach (var yr in years)
            {
                var awYear = new AwardYears();
                awYear.Year = yr;
                _awardYears.Add(awYear);
            }
        }

        /// <summary>
        /// Get nslds data from the database
        /// </summary>
        /// <param name="awardYear"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        private IEnumerable<string> GetData( string awardYear, string campusId)
        {

            var result = new List<string>();

            var data = from r in context.GetNSLDSExportData(awardYear, campusId, null)
                select r;

            var val = new StringBuilder();

            foreach (var r in data)
            {
                val = new StringBuilder();
                val.Append(r.Award_Year != null ? r.Award_Year.ToString() : "" );
                val.Append(",");
                val.Append(r.Student_Social_Security_Number != null ? r.Student_Social_Security_Number.ToString() : "" );
                val.Append(",");
                val.Append(r.Student_First_Name != null ? r.Student_First_Name.ToString() : "" );
                val.Append(",");
                val.Append(r.Student_Middle_Name != null ? r.Student_Middle_Name.ToString() : "");
                val.Append(",");
                val.Append(r.Student_Last_Name != null ? r.Student_Last_Name.ToString() : "" );
                val.Append(",");
                val.Append(r.Student_Date_of_Birth != null ? r.Student_Date_of_Birth.ToShortDateString() : "" );
                val.Append(",");
                val.Append(r.Institution_Code__OPEID_ != null ? r.Institution_Code__OPEID_.ToString() : "" );
                val.Append(",");
                val.Append(r.Institution_Name != null ? r.Institution_Name.ToString() : ""  );
                val.Append(",");
                val.Append(r.Program_Name != null ? r.Program_Name.ToString() : ""  );
                val.Append(",");
                val.Append(r.CIP_Code != null ? r.CIP_Code.ToString() : ""  );
                val.Append(",");
                val.Append(r.Credential_Level != null ? r.Credential_Level.ToString() : "" );
                val.Append(",");
                val.Append(r.Medical_or_Dental_or_Internship_or_Residency != null ? r.Medical_or_Dental_or_Internship_or_Residency.ToString() : "");
                val.Append(",");
                val.Append(r.Program_Attendance_Begin_Date != null ? r.Program_Attendance_Begin_Date.Value.ToShortDateString() : "");
                val.Append(",");
                val.Append(r.Program_Attendance_Begin_Date_for_This_Award_Year != null ? r.Program_Attendance_Begin_Date_for_This_Award_Year.Value.ToShortDateString() : "");
                val.Append(",");
                val.Append(r.Program_Attendance_Status_During_Award_Year != null ? r.Program_Attendance_Status_During_Award_Year.ToString() : ""  );
                val.Append(",");
                val.Append(r.Program_Attendance_Status_Date != null ? r.Program_Attendance_Status_Date.Value.ToShortDateString() : "");
                val.Append(",");
                val.Append(r.Private_Loans_Amount != null ? r.Private_Loans_Amount.ToString() : ""  );
                val.Append(",");
                val.Append( r.Institutional_Debt != null ? r.Institutional_Debt.ToString() : ""   );
                val.Append(",");
                val.Append(r.Tuition_And_Fees_Amount != null ? r.Tuition_And_Fees_Amount.ToString() : ""  );
                val.Append(",");
                val.Append(r.Allowance_FOR_Books__Supplies__and_Equipment != null ? r.Allowance_FOR_Books__Supplies__and_Equipment.ToString() : "");
                val.Append(",");
                val.Append(r.Length_of_GE_Program.ToString() );
                val.Append(",");
                val.Append(r.Length_Of_GE_Program_Measurement != null ? r.Length_Of_GE_Program_Measurement.ToString() : "" );
                val.Append(",");
                val.Append(r.Student_s_Enrollment_Status_as_of_the_1st_Day_of_Enrollment_in_Program != null ? r.Student_s_Enrollment_Status_as_of_the_1st_Day_of_Enrollment_in_Program.ToString() : "");
                
                result.Add( val.ToString() );


            }

            return result;
        }
        #endregion

    }



}
