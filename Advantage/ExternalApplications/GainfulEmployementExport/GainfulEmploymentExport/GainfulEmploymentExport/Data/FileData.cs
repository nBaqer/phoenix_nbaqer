﻿using System.Collections.Generic;

namespace GainfulEmploymentExport.Data
{
    public class FileData
    {
        public string FileName { get; set; }
        public List<string> ExcelData  { get; set; }
    }
}
