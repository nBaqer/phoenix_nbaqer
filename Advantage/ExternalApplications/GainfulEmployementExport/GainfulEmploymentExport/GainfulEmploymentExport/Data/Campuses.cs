﻿using System;

namespace GainfulEmploymentExport.Data
{
    public class Campuses
    {
        public Guid CampusId { get; set; }
        public string CampusCode { get; set; }
    }
}
