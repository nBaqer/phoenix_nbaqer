SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GetSettingsList] ()
RETURNS @RezTable TABLE
    (
        Entity VARCHAR(40)
       ,Value VARCHAR(100)
    )
AS
    BEGIN
        DECLARE @SchoolID INT
               ,@CampusID UNIQUEIDENTIFIER
               ,@CampDescrip VARCHAR(50)
               ,@IsClockHour BIT
               ,@CampusGroupdID UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER;

        SELECT     @SchoolID = sSchool.SchoolID
                  ,@CampusID = dCamp.CampusId
                  ,@CampDescrip = dCamp.CampDescrip
                  ,@IsClockHour = CAST(SDB.IsClockHour AS INT)
                  ,@CampusGroupdID = (
                                     SELECT    TOP 1 cgc.CampGrpId
                                     FROM      FreedomAdvantage..syCmpGrpCmps cgc
                                     LEFT JOIN FreedomAdvantage..syCampGrps cg ON cg.CampGrpId = cgc.CampGrpId
                                     WHERE     cgc.CampusId = dCamp.CampusId
                                               AND cg.IsAllCampusGrp = 0
                                     )
                  ,@ActiveStatusId = (
                                     SELECT TOP 1 StatusId
                                     FROM   FreedomAdvantage..syStatuses
                                     WHERE  StatusCode = 'A'
                                     )
                  ,@CampusGroupAllId = (
                                       SELECT TOP 1 cga.CampGrpId
                                       FROM   FreedomAdvantage..syCampGrps cga
                                       WHERE  cga.IsAllCampusGrp = 1
                                       )
        FROM       Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syCampuses dCamp ON dCamp.CampCode = CAST(sSchool.SchoolID AS VARCHAR(50))
        INNER JOIN dbo.SourceDB SDB ON SDB.SchoolID = sSchool.SchoolID
                                       AND SDB.IsDisabled = 0;

        INSERT INTO @RezTable
                    SELECT 'CampusID' AS Entity
                          ,CAST(@CampusID AS VARCHAR(100)) AS Value
                    UNION ALL
                    SELECT 'SchoolID'
                          ,CAST(@SchoolID AS VARCHAR(100))
                    UNION ALL
                    SELECT 'CampDescrip'
                          ,CAST(@CampDescrip AS VARCHAR(100))
                    UNION
                    SELECT 'IsClockHour'
                          ,CAST(@IsClockHour AS VARCHAR(100))
                    UNION
                    SELECT 'CampusGroupID' AS Entity
                          ,CAST(@CampusGroupdID AS VARCHAR(100)) AS Value
                    UNION
                    SELECT 'ActiveStatusId' AS Entity
                          ,CAST(@ActiveStatusId AS VARCHAR(100)) AS value
                    UNION
                    SELECT 'CampusGroupAllId' AS Entity
                          ,CAST(@CampusGroupAllId AS VARCHAR(100)) AS value
  UNION
                    SELECT 'ConversionUser' AS Entity
                          ,CAST('Conversion SA (' + RTRIM(LTRIM(CAST(@SchoolID AS VARCHAR(10)))) + ')' AS VARCHAR(100)) AS value;


        RETURN;
    END;



GO
