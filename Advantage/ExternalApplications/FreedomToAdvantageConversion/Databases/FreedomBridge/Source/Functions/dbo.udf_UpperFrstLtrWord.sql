SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================

CREATE FUNCTION [dbo].[udf_UpperFrstLtrWord]
    (
        @str NVARCHAR(4000)
    )
RETURNS NVARCHAR(2000)
AS
    BEGIN
        DECLARE @retval NVARCHAR(2000);

        SET @str = RTRIM(LTRIM(@str));
        SET @retval = UPPER(LEFT(@str, 1)) + LOWER(SUBSTRING(@str, 2, LEN(@str) - LEN(SUBSTRING(@str, CHARINDEX(' ', @str + ' ', 1), LEN(@str)))));

        WHILE CHARINDEX(' ', @str, 1) > 0
            BEGIN
                SET @str = LTRIM(RIGHT(@str, LEN(@str) - CHARINDEX(' ', @str, 1)));
                SET @retval += UPPER(LEFT(@str, 1)) + LOWER(SUBSTRING(@str, 2, LEN(@str) - LEN(SUBSTRING(@str, CHARINDEX(' ', @str + ' ', 1), LEN(@str)))));
            END;

        RETURN @retval;
    END;
GO
