SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GetHourMinutesOnly]
    (
        @TimeIntervalDescrip DATETIME2
    )
RETURNS VARCHAR(256)
AS
    BEGIN
        DECLARE @temp VARCHAR(256) = REPLACE(LEFT(CONVERT(VARCHAR(8), SUBSTRING(CAST(@TimeIntervalDescrip AS VARCHAR(50)), 12, 6), 108), 5), ':', '');
	

        declare @retString VARCHAR(256) = CASE WHEN SUBSTRING(@temp, 0, 4) = '000' THEN RIGHT(@temp, 1)
                              WHEN SUBSTRING(@temp, 0, 3) = '00' THEN RIGHT(@temp, 2)
                              WHEN SUBSTRING(@temp, 0, 2) = '0' THEN RIGHT(@temp,3)
							  ELSE @temp
                         END;

        RETURN @retString;
    END;


GO
