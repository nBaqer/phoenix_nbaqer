SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GetFreedomTranTree]
    (
        @LastRecordID INT
    )
RETURNS TABLE
AS
RETURN (
           WITH CTE ( RecordKey, NextRec, TransDate, Amount )
           AS ( SELECT RecordKey
                     , NextRec
                     , TransDate
                     , Amount
                FROM   Freedom..STUTRANS_FIL sTrans
                WHERE  sTrans.NextRec = @LastRecordID
                UNION ALL
                SELECT sTrans.RecordKey
                     , sTrans.NextRec
                     , sTrans.TransDate
                     , sTrans.Amount
                FROM   Freedom..STUTRANS_FIL sTrans
                INNER JOIN CTE sTrans1 ON sTrans1.RecordKey = sTrans.NextRec
              )
           SELECT C1.RecordKey
                --,NextRec
                , C1.TransDate
                , C1.Amount / 100.00 AS Amount
           FROM   CTE C1
           INNER JOIN Freedom..STUTRANS_FIL sTrans ON sTrans.RecordKey = C1.RecordKey
                                                      AND sTrans.AwardLogKey = 0
       );
GO
