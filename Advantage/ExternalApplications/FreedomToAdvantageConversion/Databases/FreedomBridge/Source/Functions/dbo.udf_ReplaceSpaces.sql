SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================


CREATE FUNCTION [dbo].[udf_ReplaceSpaces]
    (
        @String VARCHAR(256)
    )
RETURNS VARCHAR(256)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(256) = NULL;

        WHILE @String <> ' '
        SET @String = REPLACE(@String, ' ', '');

        SET @ReturnValue = NULLIF(@String, '');
        RETURN @ReturnValue;

    END;
GO
