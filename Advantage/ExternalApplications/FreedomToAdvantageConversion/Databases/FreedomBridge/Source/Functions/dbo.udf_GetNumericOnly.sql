SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-24-14>
-- =============================================


CREATE FUNCTION [dbo].[udf_GetNumericOnly]
    (
        @String VARCHAR(256)
    )
RETURNS VARCHAR(256)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(256) = NULL;

        WHILE PATINDEX('%[^0-9]%', @String) <> 0
        SET @String = STUFF(@String, PATINDEX('%[^0-9]%', @String), 1, '');

        SET @ReturnValue = NULLIF(@String, '');
        RETURN @ReturnValue;

    END;

GO
