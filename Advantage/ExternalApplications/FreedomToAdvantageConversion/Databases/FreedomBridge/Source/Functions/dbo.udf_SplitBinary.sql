SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_SplitBinary]
    (
        @RecordKey INT
      , @BinaryStr VARCHAR(31)
    )
RETURNS @List TABLE
    (
        RecordKey INT
      , Col_1 INT
      , Col_2 INT
      , Col_3 INT
      , Col_4 INT
      , Col_5 INT
      , Col_6 INT
      , Col_7 INT
      , Col_8 INT
      , Col_9 INT
      , Col_10 INT
      , Col_11 INT
      , Col_12 INT
      , Col_13 INT
      , Col_14 INT
      , Col_15 INT
      , Col_16 INT
      , Col_17 INT
      , Col_18 INT
      , Col_19 INT
      , Col_20 INT
      , Col_21 INT
      , Col_22 INT
      , Col_23 INT
      , Col_24 INT
      , Col_25 INT
      , Col_26 INT
      , Col_27 INT
      , Col_28 INT
      , Col_29 INT
      , Col_30 INT
      , Col_31 INT
    )
AS
    BEGIN
        DECLARE @pos INT
              , @colName VARCHAR(6)
              , @sqlQuery VARCHAR(MAX);

        SET @BinaryStr = RTRIM(LTRIM(@BinaryStr));
        SET @colName = 'Col_';

        WHILE @pos > 32
            BEGIN
                SELECT @pos = LEN(@BinaryStr);
                SELECT @BinaryStr = SUBSTRING(@BinaryStr, @pos, 1);
                SELECT @colName = @colName + @pos;

                SET @sqlQuery = 'INSERT INTO @List (' + @colName + ')' + 'VALUES  (' + @BinaryStr + ')';
                EXEC @sqlQuery;

                SET @pos = @pos + 1;
            END;

        RETURN;
    END;
GO
