SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GetPlacementNoteComment]
    (
        @CommentKey INT
    )
RETURNS VARCHAR(MAX)
AS
    BEGIN

        DECLARE @Comment AS VARCHAR(MAX) = '';
        WITH CTE
        AS ( SELECT RecordKey
                  , NextRec
                  , sComment.Text
             FROM   Freedom..GRADPLAC_FIL_NOT_CMT sComment
             WHERE  sComment.RecordKey = @CommentKey
             UNION ALL
             SELECT sComment.RecordKey
                  , sComment.NextRec
                  , sComment.Text
             FROM   Freedom..GRADPLAC_FIL_NOT_CMT sComment
             INNER JOIN CTE ON CTE.NextRec = sComment.RecordKey
           )
        SELECT @Comment = @Comment + Text
        FROM   CTE;

        RETURN @Comment;

    END;
GO
