SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[udf_GenerateDatePeriod]
    (
        @StartDate DATE
      , @Iterations INT
      , @DateType INT
      , @TotalAmount REAL
    )
RETURNS @RezTable TABLE
    (
        DateValue DATE
      , DateToValue DATE
      , i INT
      , Amount DECIMAL(10, 2)
    )
AS
    BEGIN
        --@DateType
        --1	Day(s)
        --2	Week(s)
        --3	Month(s)
        --4	Quarter(s)
        --5	Year(s)
        SET @Iterations = ISNULL(NULLIF(@Iterations, 0), 1) --make sure there is no 0 iteration, divide from zero protection
        ;
        WITH DATE_CTE ( DateValue, DateToValue, i, Amount )
        AS ( SELECT @StartDate AS DateValue
                  , CASE @DateType
                         WHEN 1 THEN DATEADD(DAY, 1, @StartDate)
                         WHEN 2 THEN DATEADD(WEEK, 1, @StartDate)
                         WHEN 3 THEN DATEADD(MONTH, 1, @StartDate)
                         WHEN 4 THEN DATEADD(QUARTER, 1, @StartDate)
                         WHEN 5 THEN DATEADD(YEAR, 1, @StartDate)
                    END AS DateToValue
                  , 1 DateValue
                  , CAST(@TotalAmount / @Iterations AS DECIMAL(10, 2)) AS Amount
             UNION ALL
             SELECT CASE @DateType
                         WHEN 1 THEN DATEADD(DAY, i, @StartDate)
                         WHEN 2 THEN DATEADD(WEEK, i, @StartDate)
                         WHEN 3 THEN DATEADD(MONTH, i, @StartDate)
                         WHEN 4 THEN DATEADD(QUARTER, i, @StartDate)
                         WHEN 5 THEN DATEADD(YEAR, i, @StartDate)
                    END
                  , CASE @DateType
                         WHEN 1 THEN DATEADD(DAY, i + 1, @StartDate)
                         WHEN 2 THEN DATEADD(WEEK, i + 1, @StartDate)
                         WHEN 3 THEN DATEADD(MONTH, i + 1, @StartDate)
                         WHEN 4 THEN DATEADD(QUARTER, i + 1, @StartDate)
                         WHEN 5 THEN DATEADD(YEAR, i + 1, @StartDate)
                    END AS DateToValue
                  , i + 1
                  , CAST(@TotalAmount / @Iterations AS DECIMAL(10, 2)) AS Amount
             FROM   DATE_CTE
             WHERE  i < @Iterations
           )
        INSERT INTO @RezTable
                    SELECT DateValue
                         , DateToValue
                         , i
                         , CASE WHEN i < @Iterations THEN Amount
                                ELSE CAST(Amount + @TotalAmount - Amount * @Iterations AS DECIMAL(10, 2))
                           END AS Amount
                    FROM   DATE_CTE
        OPTION ( MAXRECURSION 1000 );
        RETURN;
    END;
GO
