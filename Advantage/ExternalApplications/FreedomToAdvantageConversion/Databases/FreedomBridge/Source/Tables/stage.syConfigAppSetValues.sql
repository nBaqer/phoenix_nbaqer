CREATE TABLE [stage].[syConfigAppSetValues]
(
[ValueId] [uniqueidentifier] NOT NULL,
[SettingId] [int] NULL,
[CampusId] [uniqueidentifier] NULL,
[Value] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [stage].[syConfigAppSetValues] ADD CONSTRAINT [PK_syConfigAppSetValues] PRIMARY KEY CLUSTERED  ([ValueId]) ON [PRIMARY]
GO
