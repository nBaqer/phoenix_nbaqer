CREATE TABLE [bridge].[arGradeScales]
(
[trg_GradeScaleId] [uniqueidentifier] NULL,
[trg_GrdSystemId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGradeScales] ADD CONSTRAINT [PK__arGradeS__078107900FA4394E] PRIMARY KEY CLUSTERED  ([src_Description], [trg_CampusId]) ON [PRIMARY]
GO
