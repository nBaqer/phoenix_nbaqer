CREATE TABLE [bridge].[adSourceType]
(
[trg_SourceTypeId] [uniqueidentifier] NULL,
[src_AdSourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecNum] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adSourceType] ADD CONSTRAINT [PK__adSource__9374A682DBDA75E9] PRIMARY KEY CLUSTERED  ([src_AdSourceName], [src_RecNum], [trg_CampusId]) ON [PRIMARY]
GO
