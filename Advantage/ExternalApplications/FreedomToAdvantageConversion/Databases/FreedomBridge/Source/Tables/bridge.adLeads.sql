CREATE TABLE [bridge].[adLeads]
(
[trg_LeadId] [uniqueidentifier] NULL,
[src_FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_PrivLastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_StuRecordKey] [int] NULL,
[src_StuSSN] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_ProRecordKey] [int] NULL,
[TotalRank] [int] NOT NULL,
[LocalRank] [int] NOT NULL,
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
