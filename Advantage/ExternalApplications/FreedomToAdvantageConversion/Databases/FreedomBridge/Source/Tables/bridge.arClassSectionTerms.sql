CREATE TABLE [bridge].[arClassSectionTerms]
(
[trg_ClsSectTermId] [uniqueidentifier] NULL,
[trg_ClsSectionId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arClassSectionTerms] ADD CONSTRAINT [PK__arClassS__0F155AAFB186A077] PRIMARY KEY CLUSTERED  ([trg_ClsSectionId], [trg_CampusId]) ON [PRIMARY]
GO
