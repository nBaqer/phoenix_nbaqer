CREATE TABLE [dbo].[ScriptStage]
(
[ScriptStageID] [int] NOT NULL,
[ScriptStage] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScriptStage] ADD CONSTRAINT [PK_ScriptType] PRIMARY KEY CLUSTERED  ([ScriptStageID]) ON [PRIMARY]
GO
