CREATE TABLE [bridge].[syPeriods]
(
[trg_PeriodId] [uniqueidentifier] NULL,
[trg_PeriodCode] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ScheduleId] [int] NOT NULL,
[src_Hours] [int] NOT NULL,
[src_SunView] [int] NULL,
[src_MonView] [int] NULL,
[src_TueView] [int] NULL,
[src_WedView] [int] NULL,
[src_ThrView] [int] NULL,
[src_FriView] [int] NULL,
[src_SatView] [int] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syPeriods] ADD CONSTRAINT [PK__syPeriod__274D8C29EEE489C2] PRIMARY KEY CLUSTERED  ([trg_PeriodCode], [src_Hours], [src_ScheduleId], [trg_CampusId]) ON [PRIMARY]
GO
