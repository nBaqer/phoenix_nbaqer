CREATE TABLE [bridge].[arGradeScaleDetails]
(
[trg_GrdScaleDetailId] [uniqueidentifier] NULL,
[src_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_MinGradeValue] [int] NOT NULL,
[src_MaxGradeValue] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGradeScaleDetails] ADD CONSTRAINT [PK__arGradeS__9B87590D16E36136] PRIMARY KEY CLUSTERED  ([src_Description], [src_MinGradeValue], [src_MaxGradeValue], [trg_CampusId]) ON [PRIMARY]
GO
