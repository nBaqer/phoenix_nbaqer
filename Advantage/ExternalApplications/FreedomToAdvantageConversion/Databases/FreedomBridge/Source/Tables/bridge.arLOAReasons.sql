CREATE TABLE [bridge].[arLOAReasons]
(
[trg_LOAReasonId] [uniqueidentifier] NULL,
[src_ID] [int] NOT NULL,
[src_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampGrpId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arLOAReasons] ADD CONSTRAINT [PK_arLOAReasons] PRIMARY KEY CLUSTERED  ([src_ID], [src_Description], [trg_CampGrpId], [trg_CampusId]) ON [PRIMARY]
GO
