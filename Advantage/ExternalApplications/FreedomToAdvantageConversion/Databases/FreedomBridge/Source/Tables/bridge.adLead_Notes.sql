CREATE TABLE [bridge].[adLead_Notes]
(
[trg_id] [int] NOT NULL,
[src_TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adLead_Notes] ADD CONSTRAINT [PK_adLead_Notes] PRIMARY KEY CLUSTERED  ([src_TableName], [src_RecordKey], [trg_CampusID]) ON [PRIMARY]
GO
