CREATE TABLE [bridge].[arStudAddressesOLD]
(
[trg_StdAddressId] [uniqueidentifier] NULL,
[src_Address1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_StudentId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arStudAddressesOLD] ADD CONSTRAINT [PK__arStudAd__5251DA3F0E5D49D2] PRIMARY KEY CLUSTERED  ([src_Address1], [trg_StudentId], [trg_CampusId]) ON [PRIMARY]
GO
