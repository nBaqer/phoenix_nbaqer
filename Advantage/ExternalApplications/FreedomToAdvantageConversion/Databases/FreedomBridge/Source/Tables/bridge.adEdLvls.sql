CREATE TABLE [bridge].[adEdLvls]
(
[trg_EdLvlId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adEdLvls] ADD CONSTRAINT [PK__adEdLvls__EF2815CD2FDA5850] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
