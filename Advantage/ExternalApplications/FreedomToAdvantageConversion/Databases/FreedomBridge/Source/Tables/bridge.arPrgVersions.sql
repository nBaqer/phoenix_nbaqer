CREATE TABLE [bridge].[arPrgVersions]
(
[trg_PrgVerId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arPrgVersions] ADD CONSTRAINT [PK__arPrgVer__96AE1582188B0363] PRIMARY KEY CLUSTERED  ([src_Name], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
