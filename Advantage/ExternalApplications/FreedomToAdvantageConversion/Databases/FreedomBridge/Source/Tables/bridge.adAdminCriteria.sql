CREATE TABLE [bridge].[adAdminCriteria]
(
[trg_AdminCriteriaId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adAdminCriteria] ADD CONSTRAINT [PK__adAdminC__EF2815CD59209AAE] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
