CREATE TABLE [bridge].[adEthCodes]
(
[trg_EthCodeId] [uniqueidentifier] NULL,
[trg_IPEDSValue] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adEthCodes] ADD CONSTRAINT [PK__adEthCod__EF2815CD05625238] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
