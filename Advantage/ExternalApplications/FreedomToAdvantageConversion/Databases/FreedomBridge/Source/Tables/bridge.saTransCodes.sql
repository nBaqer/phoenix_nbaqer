CREATE TABLE [bridge].[saTransCodes]
(
[trg_TransCodeId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_AwardTypeKey] [int] NOT NULL CONSTRAINT [DF_saTransCodes_src_AwardTypeKey] DEFAULT ((0)),
[src_FullDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL,
[UnSubLoanFlag] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
