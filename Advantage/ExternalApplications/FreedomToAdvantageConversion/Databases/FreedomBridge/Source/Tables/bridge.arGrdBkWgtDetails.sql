CREATE TABLE [bridge].[arGrdBkWgtDetails]
(
[trg_InstrGrdBkWgtDetailId] [uniqueidentifier] NULL,
[trg_InstrGrdBkWgtId] [uniqueidentifier] NOT NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[InactiveDateRange] [bit] NOT NULL CONSTRAINT [DF__arGrdBkWg__Inact__4C371A65] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGrdBkWgtDetails] ADD CONSTRAINT [PK__arGrdBkW__C2296817B0C9E337] PRIMARY KEY CLUSTERED  ([trg_InstrGrdBkWgtId], [src_Name], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
