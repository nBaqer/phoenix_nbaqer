CREATE TABLE [bridge].[AllNotes]
(
[trg_NotesId] [int] NOT NULL,
[trg_ModuleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_TableName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[AllNotes] ADD CONSTRAINT [PK_bridgeAllNotes] PRIMARY KEY CLUSTERED  ([src_TableName], [src_RecordKey], [trg_CampusID]) ON [PRIMARY]
GO
