CREATE TABLE [bridge].[arDistanceEdStatusOLD]
(
[trg_DistanceEdStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_LookUpID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arDistanceEdStatusOLD] ADD CONSTRAINT [PK_arDistanceEdStatus] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
