CREATE TABLE [bridge].[arProgSchedules]
(
[trg_ScheduleId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampGrpId] [uniqueidentifier] NOT NULL,
[trg_ProgramVersionId] [uniqueidentifier] NOT NULL,
[FreedomScheduleRecordKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arProgSchedules] ADD CONSTRAINT [PK__arProgSc__2C302015E89BC0C5] PRIMARY KEY CLUSTERED  ([src_Name], [trg_CampGrpId], [trg_ProgramVersionId], [FreedomScheduleRecordKey]) ON [PRIMARY]
GO
