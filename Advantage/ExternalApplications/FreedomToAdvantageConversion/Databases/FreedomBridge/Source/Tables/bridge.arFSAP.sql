CREATE TABLE [bridge].[arFSAP]
(
[trg_SAPId] [uniqueidentifier] NOT NULL,
[src_Descrip] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_ProgramVersionId] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arFSAP] ADD CONSTRAINT [PK__arFSAP__E6E7CB2E8604169F] PRIMARY KEY CLUSTERED  ([trg_SAPId], [src_ProgramVersionId]) ON [PRIMARY]
GO
