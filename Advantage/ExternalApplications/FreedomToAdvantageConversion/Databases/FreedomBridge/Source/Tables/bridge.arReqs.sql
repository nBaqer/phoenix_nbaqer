CREATE TABLE [bridge].[arReqs]
(
[trg_ReqId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_CourseDefKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arReqs] ADD CONSTRAINT [PK__arReqs__4AFD260152ED6832] PRIMARY KEY CLUSTERED  ([src_Name], [src_RecordKey], [src_CourseDefKey], [trg_CampusId]) ON [PRIMARY]
GO
