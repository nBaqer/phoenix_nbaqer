CREATE TABLE [bridge].[adLeadGroups]
(
[trg_LeadGrpId] [uniqueidentifier] NULL,
[src_RecordKey] [int] NOT NULL,
[src_Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[trg_CampusGroupId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adEdLvls_CampusGroupId] DEFAULT ('2AC97FC1-BB9B-450A-AA84-7C503C90A1EB')
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adLeadGroups] ADD CONSTRAINT [PK__adLeadGr__DA708B449DB12AA7] PRIMARY KEY CLUSTERED  ([src_RecordKey], [src_Descrip], [trg_CampusGroupId]) ON [PRIMARY]
GO
