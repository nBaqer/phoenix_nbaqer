CREATE TABLE [bridge].[arSAPChkResults]
(
[src_Period] [int] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_StudrecKey] [int] NOT NULL,
[src_StdRecKey] [uniqueidentifier] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arSAPChkResults] ADD CONSTRAINT [PK_arSAPChkResults] PRIMARY KEY CLUSTERED  ([src_Period], [src_RecordKey], [src_StudrecKey], [src_StdRecKey], [trg_StuEnrollId], [trg_CampusId]) ON [PRIMARY]
GO
