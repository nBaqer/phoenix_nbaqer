CREATE TABLE [bridge].[plEmployers]
(
[trg_EmployerId] [uniqueidentifier] NOT NULL,
[src_ParentRecordKey] [int] NOT NULL,
[src_ChildRecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[plEmployers] ADD CONSTRAINT [PK_plEmployers] PRIMARY KEY CLUSTERED  ([src_ParentRecordKey], [src_ChildRecordKey], [trg_CampusId]) ON [PRIMARY]
GO
