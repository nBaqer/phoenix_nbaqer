CREATE TABLE [stage].[arGradeSystemDetails]
(
[GradeSystemDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Grade] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPass] [bit] NULL,
[GPA] [decimal] (3, 2) NOT NULL,
[IsCreditsEarned] [bit] NULL,
[IsCreditsAttempted] [bit] NULL,
[IsInGPA] [bit] NULL,
[IsInSAP] [bit] NULL,
[IsTransferGrade] [bit] NULL,
[IsIncomplete] [bit] NULL,
[IsDefault] [bit] NULL,
[IsDrop] [bit] NULL,
[MinGradeValue] [int] NULL,
[MaxGradeValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [stage].[arGradeSystemDetails] ADD CONSTRAINT [PK_arGradeSystemDetails] PRIMARY KEY CLUSTERED  ([Grade], [GPA]) ON [PRIMARY]
GO
