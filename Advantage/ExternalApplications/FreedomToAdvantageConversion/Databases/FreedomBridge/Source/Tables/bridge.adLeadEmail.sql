CREATE TABLE [bridge].[adLeadEmail]
(
[trg_adLeadEmailId] [uniqueidentifier] NULL,
[src_Email] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_LeadId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adLeadEmail] ADD CONSTRAINT [PK__adLeadEmail__DA708B449DB12AA7] PRIMARY KEY CLUSTERED  ([src_Email], [trg_LeadId], [trg_CampusId]) ON [PRIMARY]
GO
