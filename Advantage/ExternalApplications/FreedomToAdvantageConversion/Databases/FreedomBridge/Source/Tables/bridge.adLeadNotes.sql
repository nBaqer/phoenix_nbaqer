CREATE TABLE [bridge].[adLeadNotes]
(
[trg_LeadNoteId] [uniqueidentifier] NULL,
[src_RecNum] [int] NOT NULL,
[src_Text] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adLeadNotes] ADD CONSTRAINT [PK__adLeadNo__C64A7AA47E2D42B2] PRIMARY KEY CLUSTERED  ([src_RecNum], [trg_CampusId]) ON [PRIMARY]
GO
