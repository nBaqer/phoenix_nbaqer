CREATE TABLE [bridge].[syPhoneType]
(
[trg_PhoneTypeId] [uniqueidentifier] NULL,
[src_ID] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syPhoneType] ADD CONSTRAINT [PK__syPhoneT__EF2815CD72D691EE] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
