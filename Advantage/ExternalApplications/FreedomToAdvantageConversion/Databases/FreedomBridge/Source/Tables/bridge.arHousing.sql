CREATE TABLE [bridge].[arHousing]
(
[trg_HousingId] [uniqueidentifier] NOT NULL,
[src_ID] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_LookUpID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arHousing] ADD CONSTRAINT [PK__arHousin__EF2815CDFFB84591] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
