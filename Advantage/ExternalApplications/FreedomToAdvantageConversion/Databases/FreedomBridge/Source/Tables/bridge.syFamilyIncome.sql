CREATE TABLE [bridge].[syFamilyIncome]
(
[trg_FamilyIncomeID] [uniqueidentifier] NULL,
[trg_FamilyIncomeDescrip] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_AmountFrom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_AmountTo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syFamilyIncome] ADD CONSTRAINT [PK__syFamilyIncome__EF2815CD8B1A09F8] PRIMARY KEY CLUSTERED  ([trg_FamilyIncomeDescrip]) ON [PRIMARY]
GO
