CREATE TABLE [bridge].[syStudentNotesOLD]
(
[trg_StudentNoteId] [uniqueidentifier] NOT NULL,
[src_NotesKey] [int] NOT NULL,
[src_CommentKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syStudentNotesOLD] ADD CONSTRAINT [PK_syStudentNotes_1] PRIMARY KEY CLUSTERED  ([src_CommentKey], [trg_CampusId], [TableName]) ON [PRIMARY]
GO
