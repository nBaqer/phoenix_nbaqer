CREATE TABLE [stage].[PreConversionResults]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Reason] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Solution] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExecutionTime] [datetime2] NULL
) ON [PRIMARY]
GO
ALTER TABLE [stage].[PreConversionResults] ADD CONSTRAINT [PK__PreConve__3214EC0749F5461B] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
