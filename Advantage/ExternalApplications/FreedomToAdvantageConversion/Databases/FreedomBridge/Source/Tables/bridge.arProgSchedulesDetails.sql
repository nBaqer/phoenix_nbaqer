CREATE TABLE [bridge].[arProgSchedulesDetails]
(
[ScheduleDetailId] [uniqueidentifier] NULL,
[ScheduleId] [uniqueidentifier] NOT NULL,
[FreedomScheduleRecordKey] [int] NOT NULL,
[FreedomScheduleDetailRecordKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arProgSchedulesDetails] ADD CONSTRAINT [PK__arProgSc__F10EF53525D7F771] PRIMARY KEY CLUSTERED  ([ScheduleId], [FreedomScheduleRecordKey], [FreedomScheduleDetailRecordKey]) ON [PRIMARY]
GO
