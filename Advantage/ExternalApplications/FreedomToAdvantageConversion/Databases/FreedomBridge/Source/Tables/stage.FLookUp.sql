CREATE TABLE [stage].[FLookUp]
(
[LookUpID] [int] NOT NULL IDENTITY(1, 1),
[LookupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [stage].[FLookUp] ADD CONSTRAINT [PK_FLookUp] PRIMARY KEY CLUSTERED  ([LookupName], [ID], [Description]) ON [PRIMARY]
GO
