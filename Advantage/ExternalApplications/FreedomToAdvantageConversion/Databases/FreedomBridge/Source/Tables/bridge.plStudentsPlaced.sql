CREATE TABLE [bridge].[plStudentsPlaced]
(
[trg_PlacementId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[plStudentsPlaced] ADD CONSTRAINT [PK_plStudentsPlaced] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
