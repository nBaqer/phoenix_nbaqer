CREATE TABLE [bridge].[arGrdBkResults]
(
[trg_GrdBkResultId] [uniqueidentifier] NULL,
[trg_ClsSectionId] [uniqueidentifier] NOT NULL,
[trg_InstrGrdBkDetailId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGrdBkResults] ADD CONSTRAINT [PK__arGrdBkR__9A3BB85011A6EF19] PRIMARY KEY CLUSTERED  ([trg_ClsSectionId], [trg_StuEnrollId], [trg_InstrGrdBkDetailId], [src_RecordKey]) ON [PRIMARY]
GO
