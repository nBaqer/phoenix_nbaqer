CREATE TABLE [bridge].[saPaymentTypes]
(
[trg_PaymentTypeId] [int] NOT NULL,
[src_ID] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_LookUpID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[saPaymentTypes] ADD CONSTRAINT [PK_saPaymentTypes] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
