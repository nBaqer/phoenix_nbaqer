CREATE TABLE [bridge].[faLenders]
(
[trg_LenderId] [uniqueidentifier] NOT NULL,
[trg_LenderDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[TotalRank] [int] NOT NULL,
[LocalRank] [int] NOT NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[faLenders] ADD CONSTRAINT [PK__faLenders__EF2815CD5D4C123C] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusID]) ON [PRIMARY]
GO
