CREATE TABLE [bridge].[arBuildings]
(
[trg_BldgId] [uniqueidentifier] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_SchoolId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arBuildings] ADD CONSTRAINT [PK__arBuildi__D85B8D8ABA5462BC] PRIMARY KEY CLUSTERED  ([trg_CampusId], [src_SchoolId]) ON [PRIMARY]
GO
