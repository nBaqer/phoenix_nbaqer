CREATE TABLE [bridge].[adCitizenships]
(
[trg_CitizenshipId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL,
[trg_CampusGroupId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_bridgeAdCitizenship_CampusGroupId] DEFAULT ('2AC97FC1-BB9B-450A-AA84-7C503C90A1EB')
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adCitizenships] ADD CONSTRAINT [PK__adCitize__srcDescription_trgCampusGroupsId] PRIMARY KEY CLUSTERED  ([src_Description], [trg_CampusGroupId]) ON [PRIMARY]
GO
