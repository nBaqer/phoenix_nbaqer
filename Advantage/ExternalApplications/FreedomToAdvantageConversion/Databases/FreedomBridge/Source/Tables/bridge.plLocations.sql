CREATE TABLE [bridge].[plLocations]
(
[trg_LocationId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_SchoolID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[plLocations] ADD CONSTRAINT [PK__plLocations__280BEFDEB6A18538] PRIMARY KEY CLUSTERED  ([trg_CampusId], [src_SchoolID]) ON [PRIMARY]
GO
