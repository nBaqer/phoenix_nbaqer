CREATE TABLE [bridge].[arTerm]
(
[trg_TermId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[trg_ProgramVersionId] [uniqueidentifier] NOT NULL,
[FreedomCourseRecordKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arTerm] ADD CONSTRAINT [PK__arTerm__B5F4E56722E1578E] PRIMARY KEY CLUSTERED  ([src_Name], [src_RecordKey], [trg_CampusId], [trg_ProgramVersionId], [FreedomCourseRecordKey]) ON [PRIMARY]
GO
