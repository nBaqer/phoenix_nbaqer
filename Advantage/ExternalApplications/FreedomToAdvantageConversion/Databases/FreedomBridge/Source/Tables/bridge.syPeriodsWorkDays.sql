CREATE TABLE [bridge].[syPeriodsWorkDays]
(
[trg_PeriodIdWorkDayId] [uniqueidentifier] NOT NULL,
[trg_PeriodId] [uniqueidentifier] NOT NULL,
[trg_WorkDayId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syPeriodsWorkDays] ADD CONSTRAINT [PK__syPeriod__B3BE9A7E49E06D77] PRIMARY KEY CLUSTERED  ([trg_PeriodIdWorkDayId], [trg_PeriodId], [trg_WorkDayId], [trg_CampusId]) ON [PRIMARY]
GO
