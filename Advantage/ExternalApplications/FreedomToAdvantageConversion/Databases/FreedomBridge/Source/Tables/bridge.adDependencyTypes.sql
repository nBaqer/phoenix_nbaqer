CREATE TABLE [bridge].[adDependencyTypes]
(
[trg_DependencyTypeId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adDependencyTypes] ADD CONSTRAINT [PK__adDepend__EF2815CDBAC844DB] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
