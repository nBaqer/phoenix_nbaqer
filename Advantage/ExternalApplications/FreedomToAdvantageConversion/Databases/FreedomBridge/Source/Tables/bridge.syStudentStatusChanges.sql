CREATE TABLE [bridge].[syStudentStatusChanges]
(
[trg_StudentStatusChangeId] [uniqueidentifier] NULL,
[src_RecordKey] [int] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_OrigStatusId] [uniqueidentifier] NULL,
[trg_NewStatusId] [uniqueidentifier] NULL,
[trg_Campus] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syStudentStatusChanges] ADD CONSTRAINT [PK__syStuden__77014EA41A56162D] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_StuEnrollId], [trg_Campus]) ON [PRIMARY]
GO
