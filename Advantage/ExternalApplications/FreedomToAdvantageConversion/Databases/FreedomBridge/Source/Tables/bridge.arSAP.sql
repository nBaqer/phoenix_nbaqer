CREATE TABLE [bridge].[arSAP]
(
[trg_SAPId] [uniqueidentifier] NULL,
[src_Descrip] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_Code] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
