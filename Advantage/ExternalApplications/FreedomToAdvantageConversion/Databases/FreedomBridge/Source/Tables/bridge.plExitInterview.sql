CREATE TABLE [bridge].[plExitInterview]
(
[trg_StuEnrollmentId] [uniqueidentifier] NULL,
[trg_PlExitInterviewId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_EmployeeId_RecourdKey] [int] NOT NULL
) ON [PRIMARY]
GO
