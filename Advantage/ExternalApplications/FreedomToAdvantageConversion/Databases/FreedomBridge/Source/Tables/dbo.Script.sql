CREATE TABLE [dbo].[Script]
(
[ScriptID] [int] NOT NULL IDENTITY(1, 1),
[Script] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DBID] [int] NOT NULL,
[ScriptStageID] [int] NOT NULL,
[OrderID] [int] NULL,
[IsDisabled] [bit] NOT NULL CONSTRAINT [DF_Script_IsDisabled] DEFAULT ((0)),
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Script] ADD CONSTRAINT [PK_Script] PRIMARY KEY CLUSTERED  ([ScriptID]) ON [PRIMARY]
GO
