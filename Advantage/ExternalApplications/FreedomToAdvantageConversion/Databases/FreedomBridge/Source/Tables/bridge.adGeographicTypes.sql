CREATE TABLE [bridge].[adGeographicTypes]
(
[trg_GeographicTypeId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adGeographicTypes] ADD CONSTRAINT [PK__adGeogra__EF2815CD353A20C9] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
