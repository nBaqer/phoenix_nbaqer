CREATE TABLE [bridge].[syCampGrps]
(
[trg_syCampGrpId] [uniqueidentifier] NULL,
[src_SchoolId] [int] NOT NULL,
[src_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syCampGrps] ADD CONSTRAINT [PK_CampGrp] PRIMARY KEY CLUSTERED  ([src_SchoolId], [trg_CampusId]) ON [PRIMARY]
GO
