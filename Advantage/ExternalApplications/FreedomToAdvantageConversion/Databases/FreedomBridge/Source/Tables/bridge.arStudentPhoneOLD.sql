CREATE TABLE [bridge].[arStudentPhoneOLD]
(
[trg_StudentPhoneId] [uniqueidentifier] NULL,
[src_Phone] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_PhoneType] [int] NULL,
[trg_PhoneTypeId] [uniqueidentifier] NULL,
[trg_StudentId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arStudentPhoneOLD] ADD CONSTRAINT [PK__arStuden__37D78A0BF89C587B] PRIMARY KEY CLUSTERED  ([src_Phone], [trg_StudentId], [trg_CampusId]) ON [PRIMARY]
GO
