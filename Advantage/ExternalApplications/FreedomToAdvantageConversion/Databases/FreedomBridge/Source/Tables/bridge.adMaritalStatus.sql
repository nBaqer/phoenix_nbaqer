CREATE TABLE [bridge].[adMaritalStatus]
(
[trg_MaritalStatId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adMaritalStatus] ADD CONSTRAINT [PK__adMarita__EF2815CDD6CDC6D3] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
