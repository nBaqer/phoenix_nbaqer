CREATE TABLE [bridge].[arSAPDetails]
(
[trg_SAPDetailId] [uniqueidentifier] NULL,
[src_TrigValue] [int] NULL,
[src_RecordKey] [int] NOT NULL,
[CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
