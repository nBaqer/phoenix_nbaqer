CREATE TABLE [bridge].[arStuEnrollments]
(
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arStuEnrollments] ADD CONSTRAINT [PK_arStuEnrollments] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
