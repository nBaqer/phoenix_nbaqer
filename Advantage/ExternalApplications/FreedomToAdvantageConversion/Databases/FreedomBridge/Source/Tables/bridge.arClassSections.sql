CREATE TABLE [bridge].[arClassSections]
(
[trg_ClsSectionId] [uniqueidentifier] NOT NULL,
[src_CourseNumEnrolledIn] [int] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_ClsSection] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_TermId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_ReqId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arClassSections] ADD CONSTRAINT [PK_arClassSections] PRIMARY KEY CLUSTERED  ([trg_ClsSectionId], [src_CourseNumEnrolledIn], [src_RecordKey], [trg_ClsSection], [trg_TermId], [trg_ReqId], [trg_CampusId]) ON [PRIMARY]
GO
