CREATE TABLE [bridge].[adSourceAdvertisement]
(
[trg_SourceAdvId] [uniqueidentifier] NULL,
[src_AdSourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adSourceAdvertisement] ADD CONSTRAINT [PK__adSource__079BAAC66F3BFE22] PRIMARY KEY CLUSTERED  ([src_AdSourceName], [src_RecordKey]) ON [PRIMARY]
GO
