CREATE TABLE [bridge].[arTriggerOffsetTyps]
(
[trg_TrigOffsetTypId] [tinyint] NULL,
[src_TrigOffDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_TrigOffID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arTriggerOffsetTyps] ADD CONSTRAINT [PK__arTrigge__8F4F5D5C978EAD51] PRIMARY KEY CLUSTERED  ([src_TrigOffDescription]) ON [PRIMARY]
GO
