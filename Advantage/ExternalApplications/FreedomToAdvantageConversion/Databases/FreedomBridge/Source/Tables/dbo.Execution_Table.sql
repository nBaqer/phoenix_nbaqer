CREATE TABLE [dbo].[Execution_Table]
(
[TableID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsDisabled] [bit] NOT NULL CONSTRAINT [DF_Execution_Table_IsDisabled] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Execution_Table] ADD CONSTRAINT [PK_Execution_Table] PRIMARY KEY CLUSTERED  ([TableID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Execution_Table] ADD CONSTRAINT [UIX_Execution_Table] UNIQUE NONCLUSTERED  ([TableName]) ON [PRIMARY]
GO
