CREATE TABLE [bridge].[plEmployerJobs]
(
[trg_EmployerJobId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[plEmployerJobs] ADD CONSTRAINT [PK_plEmployerJobs] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
