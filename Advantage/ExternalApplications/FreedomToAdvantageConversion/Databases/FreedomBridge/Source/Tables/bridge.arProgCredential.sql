CREATE TABLE [bridge].[arProgCredential]
(
[trg_CredentialId] [uniqueidentifier] NULL,
[src_CredentialDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_CredentialID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arProgCredential] ADD CONSTRAINT [PK__arProgCr__8FC4E5103F1EF084] PRIMARY KEY CLUSTERED  ([src_CredentialDescription]) ON [PRIMARY]
GO
