CREATE TABLE [bridge].[faStuPaymentPlanSchedule]
(
[trg_PayPlanScheduleId] [uniqueidentifier] NOT NULL,
[trg_ExpectedDate] [date] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[faStuPaymentPlanSchedule] ADD CONSTRAINT [PK_faStuPaymentPlanSchedule] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_ExpectedDate], [trg_CampusId]) ON [PRIMARY]
GO
