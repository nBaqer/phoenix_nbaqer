CREATE TABLE [bridge].[arGradeSystemDetails]
(
[trg_GrdSystemDetailId] [uniqueidentifier] NULL,
[trg_GrdSystemId] [uniqueidentifier] NOT NULL,
[src_ParentKey] [int] NULL,
[src_RecordKey] [int] NULL,
[src_Grade] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_GPA] [decimal] (8, 2) NOT NULL,
[src_MinGradeValue] [int] NOT NULL,
[src_MaxGradeValue] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGradeSystemDetails] ADD CONSTRAINT [PK__arGradeS__B5343605A5E00FEF] PRIMARY KEY CLUSTERED  ([trg_GrdSystemId], [src_Grade], [src_GPA], [src_MinGradeValue], [trg_CampusId]) ON [PRIMARY]
GO
