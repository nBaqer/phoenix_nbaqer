CREATE TABLE [map].[saFundSources]
(
[trg_FundSourceDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_FullDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnSubLoanFlag] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_saFundSources_UnSubLoanFlag] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [map].[saFundSources] ADD CONSTRAINT [PK_saFundSources] PRIMARY KEY CLUSTERED  ([trg_FundSourceDescrip], [src_FullDescrip], [UnSubLoanFlag]) ON [PRIMARY]
GO
