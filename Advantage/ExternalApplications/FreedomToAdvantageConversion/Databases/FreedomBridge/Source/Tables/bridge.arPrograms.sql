CREATE TABLE [bridge].[arPrograms]
(
[trg_ProgId] [uniqueidentifier] NULL,
[src_ProgDescrip] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arPrograms] ADD CONSTRAINT [PK__arProgra__F0DBA511987FE9B7] PRIMARY KEY CLUSTERED  ([src_ProgDescrip], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
