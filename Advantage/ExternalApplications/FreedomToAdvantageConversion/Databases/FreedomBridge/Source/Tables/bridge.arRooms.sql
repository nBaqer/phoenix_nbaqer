CREATE TABLE [bridge].[arRooms]
(
[trg_RoomId] [uniqueidentifier] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_Descrip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arRooms] ADD CONSTRAINT [PK__arRooms__D85B107627D93F55] PRIMARY KEY CLUSTERED  ([trg_CampusId], [src_Descrip]) ON [PRIMARY]
GO
