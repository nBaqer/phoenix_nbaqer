CREATE TABLE [bridge].[faStudentAwards]
(
[trg_StudentAwardId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[faStudentAwards] ADD CONSTRAINT [PK_faStudentAwards] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
