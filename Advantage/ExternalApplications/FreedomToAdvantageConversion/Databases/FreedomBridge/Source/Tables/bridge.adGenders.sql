CREATE TABLE [bridge].[adGenders]
(
[trg_GenderId] [uniqueidentifier] NULL,
[trg_IPEDSValue] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adGenders] ADD CONSTRAINT [PK__adGender__EF2815CD8B1A09F8] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
