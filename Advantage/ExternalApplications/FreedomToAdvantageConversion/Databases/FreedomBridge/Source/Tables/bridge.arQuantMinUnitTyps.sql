CREATE TABLE [bridge].[arQuantMinUnitTyps]
(
[trg_QuantMinUnitTypId] [tinyint] NULL,
[src_SAPQuantMinUnitTypeDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_SAPQuantMinUnitTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arQuantMinUnitTyps] ADD CONSTRAINT [PK__arQuantM__048C7228B4EFBA11] PRIMARY KEY CLUSTERED  ([src_SAPQuantMinUnitTypeDescription]) ON [PRIMARY]
GO
