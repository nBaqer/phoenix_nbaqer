CREATE TABLE [bridge].[syStatusCodes]
(
[trg_StatusCodeId] [uniqueidentifier] NOT NULL,
[trg_StatusLevelId] [int] NOT NULL,
[src_ID] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_LookUpID] [int] NULL,
[src_RecNum] [int] NULL
) ON [PRIMARY]
GO
