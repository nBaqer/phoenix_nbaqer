CREATE TABLE [bridge].[faStudentAwardSchedule]
(
[trg_AwardScheduleId] [uniqueidentifier] NOT NULL,
[trg_StudentAwardId] [uniqueidentifier] NOT NULL,
[src_AwardRecordKey] [int] NOT NULL,
[trg_SequenceNumber] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_TranRecKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[faStudentAwardSchedule] ADD CONSTRAINT [PK_faStudentAwardSchedule] PRIMARY KEY CLUSTERED  ([src_AwardRecordKey], [trg_SequenceNumber], [trg_CampusId]) ON [PRIMARY]
GO
