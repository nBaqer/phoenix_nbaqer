CREATE TABLE [bridge].[syHolidays]
(
[trg_HolidayId] [uniqueidentifier] NULL,
[src_Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syHolidays] ADD CONSTRAINT [PK__syHolida__88BABEED12AC0BE1] PRIMARY KEY CLUSTERED  ([src_Descrip], [src_RecordKey]) ON [PRIMARY]
GO
