CREATE TABLE [dbo].[SourceDB]
(
[DBID] [int] NOT NULL IDENTITY(1, 1),
[DBName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampusName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RestoreScript] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DataFolder] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDisabled] [bit] NOT NULL,
[SchoolID] [int] NULL,
[IsClockHour] [bit] NOT NULL CONSTRAINT [DF_SourceDB_IsClockHour] DEFAULT ((0)),
[ForceACleanDB] [bit] NOT NULL CONSTRAINT [DF__SourceDB__ForceA__6CD8F421] DEFAULT ((0)),
[ExecutionStart] [datetime2] NULL,
[EmailDomain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchoolUsesAcademicProbationFromFreedom] [bit] NULL CONSTRAINT [DF_SourceDB_SchoolUsesAcademicProbationFromFreedom] DEFAULT ((0)),
[AdvantageDb] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvantageRestoreScript] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SourceDB] ADD CONSTRAINT [PK_DB] PRIMARY KEY CLUSTERED  ([DBID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SourceDB] ADD CONSTRAINT [UIX_SourceDB] UNIQUE NONCLUSTERED  ([DBName]) ON [PRIMARY]
GO
