CREATE TABLE [bridge].[saFundSources]
(
[trg_FundSourceId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NULL,
[src_FullDescrip] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UnSubLoanFlag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_saFundSources_UnSubLoanFlag] DEFAULT (''),
[CampGrpId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[saFundSources] ADD CONSTRAINT [MyPrimaryKey] PRIMARY KEY CLUSTERED  ([src_FullDescrip], [UnSubLoanFlag], [CampGrpId]) ON [PRIMARY]
GO
