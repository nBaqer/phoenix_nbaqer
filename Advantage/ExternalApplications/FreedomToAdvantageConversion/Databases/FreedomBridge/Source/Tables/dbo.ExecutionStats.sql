CREATE TABLE [dbo].[ExecutionStats]
(
[ExecutionStatsID] [int] NOT NULL IDENTITY(1, 1),
[ExecutionID] [int] NOT NULL,
[ExecutionStage] [smallint] NOT NULL,
[StageDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BridgeDataCount] [int] NULL,
[AdvantageDataCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ExecutionStats] ADD CONSTRAINT [PK_ExecutionStats] PRIMARY KEY CLUSTERED  ([ExecutionStatsID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [_dta_index_ExecutionStats_41_1044198770__K2_K3_K5_6_7] ON [dbo].[ExecutionStats] ([ExecutionID], [ExecutionStage], [TableName]) INCLUDE ([AdvantageDataCount], [BridgeDataCount]) ON [PRIMARY]
GO
CREATE STATISTICS [_dta_stat_1044198770_2_5] ON [dbo].[ExecutionStats] ([ExecutionID], [TableName])
GO
CREATE STATISTICS [_dta_stat_1044198770_3_5] ON [dbo].[ExecutionStats] ([ExecutionStage], [TableName])
GO
ALTER TABLE [dbo].[ExecutionStats] ADD CONSTRAINT [FK_ExecutionStats_Execution] FOREIGN KEY ([ExecutionID]) REFERENCES [dbo].[Execution] ([ExecutionID])
GO
