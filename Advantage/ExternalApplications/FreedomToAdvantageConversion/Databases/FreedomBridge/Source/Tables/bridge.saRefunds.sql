CREATE TABLE [bridge].[saRefunds]
(
[trg_TransactionId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_RefundedTransKey] [int] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[saRefunds] ADD CONSTRAINT [PK_saRefunds] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
