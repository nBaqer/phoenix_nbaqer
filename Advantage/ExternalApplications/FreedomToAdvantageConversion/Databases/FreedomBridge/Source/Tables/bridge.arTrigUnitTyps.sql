CREATE TABLE [bridge].[arTrigUnitTyps]
(
[trg_TrigUnitTypId] [tinyint] NULL,
[src_TrigUnitDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_TrigUnitID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arTrigUnitTyps] ADD CONSTRAINT [PK__arTrigUn__66EB8DD22790A507] PRIMARY KEY CLUSTERED  ([src_TrigUnitDescription]) ON [PRIMARY]
GO
