CREATE TABLE [bridge].[adLeadPhone]
(
[trg_LeadPhoneId] [uniqueidentifier] NOT NULL,
[src_Phone] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_LeadId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adLeadPhone] ADD CONSTRAINT [PK__adLeadPhone__DA708B449DB12AA7] PRIMARY KEY CLUSTERED  ([trg_LeadPhoneId], [src_Phone], [trg_LeadId], [trg_CampusId]) ON [PRIMARY]
GO
