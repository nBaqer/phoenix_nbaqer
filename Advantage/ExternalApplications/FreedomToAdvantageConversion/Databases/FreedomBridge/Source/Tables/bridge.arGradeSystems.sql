CREATE TABLE [bridge].[arGradeSystems]
(
[trg_GradeSystemId] [uniqueidentifier] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGradeSystems] ADD CONSTRAINT [PK__arGradeS__F9292F06BD26169F] PRIMARY KEY CLUSTERED  ([src_Description], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
