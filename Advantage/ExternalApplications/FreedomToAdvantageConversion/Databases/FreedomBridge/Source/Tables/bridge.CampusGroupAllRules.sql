CREATE TABLE [bridge].[CampusGroupAllRules]
(
[TableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCampusAll] [bit] NULL,
[IsStarterDbField] [bit] NULL
) ON [PRIMARY]
GO
