CREATE TABLE [bridge].[faStudentPaymentPlans]
(
[trg_PaymentPlanId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[faStudentPaymentPlans] ADD CONSTRAINT [PK_faStudentPaymentPlans] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
