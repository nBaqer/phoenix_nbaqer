CREATE TABLE [bridge].[syCampuses]
(
[trg_syCampusId] [uniqueidentifier] NULL,
[src_SchoolId] [int] NOT NULL,
[src_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syCampuses] ADD CONSTRAINT [PK_syCampuses] PRIMARY KEY CLUSTERED  ([src_SchoolId], [src_Name]) ON [PRIMARY]
GO
