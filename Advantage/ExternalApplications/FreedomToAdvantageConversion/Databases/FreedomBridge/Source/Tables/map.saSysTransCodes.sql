CREATE TABLE [map].[saSysTransCodes]
(
[trg_SysTransCodeId] [int] NOT NULL,
[src_ChargeType] [int] NOT NULL,
[src_AwardPosting] [int] NOT NULL,
[Description] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [map].[saSysTransCodes] ADD CONSTRAINT [PK_saSysTransCodes] PRIMARY KEY CLUSTERED  ([trg_SysTransCodeId], [src_ChargeType], [src_AwardPosting]) ON [PRIMARY]
GO
