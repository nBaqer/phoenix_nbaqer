CREATE TABLE [bridge].[arStudentLOAs]
(
[trg_StudentLOAId] [uniqueidentifier] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_StudentStatusChangeId] [uniqueidentifier] NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arStudentLOAs] ADD CONSTRAINT [PK_arStudentLOAs] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusID]) ON [PRIMARY]
GO
