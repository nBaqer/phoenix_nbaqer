CREATE TABLE [stage].[syUsers]
(
[src_RecordKey] [int] NOT NULL,
[src_FullName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_SchoolID] [int] NULL
) ON [PRIMARY]
GO
