CREATE TABLE [bridge].[syUsers]
(
[trg_UserId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[src_UserId] [int] NOT NULL,
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalRank] [int] NULL,
[LocalRank] [int] NULL
) ON [PRIMARY]
GO
