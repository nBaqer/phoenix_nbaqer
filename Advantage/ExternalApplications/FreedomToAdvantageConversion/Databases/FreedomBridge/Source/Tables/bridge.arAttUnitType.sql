CREATE TABLE [bridge].[arAttUnitType]
(
[trg_UnitTypeId] [uniqueidentifier] NULL,
[src_AttendanceTypeDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_AttendanceTypeID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arAttUnitType] ADD CONSTRAINT [PK__arAttUni__F1B5D968149176DF] PRIMARY KEY CLUSTERED  ([src_AttendanceTypeDescription]) ON [PRIMARY]
GO
