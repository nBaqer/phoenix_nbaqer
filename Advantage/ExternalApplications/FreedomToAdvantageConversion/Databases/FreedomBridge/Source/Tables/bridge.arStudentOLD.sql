CREATE TABLE [bridge].[arStudentOLD]
(
[trg_StudentId] [uniqueidentifier] NOT NULL,
[StuSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[TotalRank] [int] NOT NULL,
[LocalRank] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arStudentOLD] ADD CONSTRAINT [PK_arStudent] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
