CREATE TABLE [bridge].[adLeadAddresses]
(
[trg_adLeadAddressId] [uniqueidentifier] NULL,
[src_Address1] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[trg_LeadId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
