CREATE TABLE [bridge].[arResults]
(
[trg_ResultId] [uniqueidentifier] NULL,
[trg_TestId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_StudrecKey] [int] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arResults] ADD CONSTRAINT [PK__arResult__847A361AA4067CD7] PRIMARY KEY CLUSTERED  ([trg_TestId], [trg_StuEnrollId], [src_RecordKey], [src_StudrecKey]) ON [PRIMARY]
GO
