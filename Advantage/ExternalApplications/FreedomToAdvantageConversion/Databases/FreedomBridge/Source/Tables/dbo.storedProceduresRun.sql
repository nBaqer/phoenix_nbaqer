CREATE TABLE [dbo].[storedProceduresRun]
(
[spId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__storedProc__spId__3C00B29C] DEFAULT (newid()),
[spName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[runTime] [datetime] NULL,
[tablesUpdated] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[orderRun] [int] NULL CONSTRAINT [df_orderRun] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[storedProceduresRun] ADD CONSTRAINT [PK__storedPr__2DD456E4F06C21C5] PRIMARY KEY CLUSTERED  ([spId]) ON [PRIMARY]
GO
