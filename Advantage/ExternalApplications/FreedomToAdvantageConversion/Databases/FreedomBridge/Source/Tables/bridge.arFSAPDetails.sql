CREATE TABLE [bridge].[arFSAPDetails]
(
[trg_SAPId] [uniqueidentifier] NOT NULL,
[trg_SAPDetailId] [uniqueidentifier] NOT NULL,
[src_ProgramVersionId] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arFSAPDetails] ADD CONSTRAINT [PK__arFSAPDe__F3EAE95BF8080205] PRIMARY KEY CLUSTERED  ([trg_SAPId], [src_ProgramVersionId], [trg_SAPDetailId]) ON [PRIMARY]
GO
