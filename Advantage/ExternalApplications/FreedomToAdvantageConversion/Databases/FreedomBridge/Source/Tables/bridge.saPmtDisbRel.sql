CREATE TABLE [bridge].[saPmtDisbRel]
(
[trg_PmtDisbRelId] [uniqueidentifier] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[trg_PayPlanScheduleId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[saPmtDisbRel] ADD CONSTRAINT [PK_saPmtDisbRel] PRIMARY KEY CLUSTERED  ([src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
