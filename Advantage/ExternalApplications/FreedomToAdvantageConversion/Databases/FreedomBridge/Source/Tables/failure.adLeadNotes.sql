CREATE TABLE [failure].[adLeadNotes]
(
[src_ParentKey] [int] NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_FirstName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_PrivLastName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_MidInitial] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_StuSSN] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[src_BirthDate] [date] NULL,
[src_PhoneNum] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [failure].[adLeadNotes] ADD CONSTRAINT [PK__adLeadNo__A0771496C6A5FA70] PRIMARY KEY CLUSTERED  ([src_ParentKey], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
