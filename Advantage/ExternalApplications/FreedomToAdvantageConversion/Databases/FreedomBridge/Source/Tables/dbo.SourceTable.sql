CREATE TABLE [dbo].[SourceTable]
(
[TableID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Script] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comment] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDisabled] [bit] NOT NULL CONSTRAINT [DF_FreedomTable_IsDisabled] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SourceTable] ADD CONSTRAINT [PK_SourceTable] PRIMARY KEY CLUSTERED  ([TableName]) ON [PRIMARY]
GO
