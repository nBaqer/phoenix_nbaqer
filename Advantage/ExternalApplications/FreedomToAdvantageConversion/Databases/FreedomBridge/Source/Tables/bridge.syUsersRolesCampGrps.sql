CREATE TABLE [bridge].[syUsersRolesCampGrps]
(
[trg_UserRoleCmpGrpId] [uniqueidentifier] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[trg_RoleId] [uniqueidentifier] NOT NULL,
[brg_UserId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
