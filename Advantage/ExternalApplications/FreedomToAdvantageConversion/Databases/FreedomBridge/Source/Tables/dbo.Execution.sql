CREATE TABLE [dbo].[Execution]
(
[ExecutionID] [int] NOT NULL IDENTITY(1, 1),
[DBID] [int] NOT NULL,
[CreatedOn] [smalldatetime] NOT NULL CONSTRAINT [DF_Execution_CreatedOn] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Execution] ADD CONSTRAINT [PK_Execution] PRIMARY KEY CLUSTERED  ([ExecutionID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Execution] ADD CONSTRAINT [FK_Execution_SourceDB] FOREIGN KEY ([DBID]) REFERENCES [dbo].[SourceDB] ([DBID])
GO
