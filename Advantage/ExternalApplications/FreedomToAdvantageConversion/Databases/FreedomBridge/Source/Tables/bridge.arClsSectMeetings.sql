CREATE TABLE [bridge].[arClsSectMeetings]
(
[trg_ClsSectMeetingId] [uniqueidentifier] NOT NULL,
[trg_ClsSectionId] [uniqueidentifier] NOT NULL,
[trg_PeriodId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arClsSectMeetings] ADD CONSTRAINT [PK__arClsSec__6950F3FD4F6B8B50] PRIMARY KEY CLUSTERED  ([trg_ClsSectMeetingId], [trg_ClsSectionId], [trg_PeriodId], [trg_CampusId]) ON [PRIMARY]
GO
