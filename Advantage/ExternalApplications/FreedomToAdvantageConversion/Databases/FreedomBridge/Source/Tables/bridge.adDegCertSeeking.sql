CREATE TABLE [bridge].[adDegCertSeeking]
(
[trg_DegCertSeekingId] [uniqueidentifier] NULL,
[trg_IPEDSValue] [int] NULL,
[src_Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_ID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[adDegCertSeeking] ADD CONSTRAINT [PK__adDegCer__EF2815CDAB4AC240] PRIMARY KEY CLUSTERED  ([src_Description]) ON [PRIMARY]
GO
