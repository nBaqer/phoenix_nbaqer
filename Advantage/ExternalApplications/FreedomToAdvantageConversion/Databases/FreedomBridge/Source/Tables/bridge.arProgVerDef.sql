CREATE TABLE [bridge].[arProgVerDef]
(
[trg_ProgVerDefId] [uniqueidentifier] NOT NULL,
[trg_PrgVerId] [uniqueidentifier] NOT NULL,
[trg_ReqId] [uniqueidentifier] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL,
[FreedomSubjectDefRecordKey] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arProgVerDef] ADD CONSTRAINT [PK__arProgVe__91CDAEC1874C48BD] PRIMARY KEY CLUSTERED  ([trg_ProgVerDefId], [trg_PrgVerId], [trg_ReqId], [trg_CampusId]) ON [PRIMARY]
GO
