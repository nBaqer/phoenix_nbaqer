CREATE TABLE [bridge].[arConsequenceTyps]
(
[trg_ConsequenceTypId] [tinyint] NULL,
[src_SAPConsequenceDescription] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_SAPConsequenceID] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arConsequenceTyps] ADD CONSTRAINT [PK__arConseq__16145F9A491522B3] PRIMARY KEY CLUSTERED  ([src_SAPConsequenceDescription]) ON [PRIMARY]
GO
