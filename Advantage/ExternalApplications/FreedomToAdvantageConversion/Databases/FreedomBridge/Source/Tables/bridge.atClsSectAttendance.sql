CREATE TABLE [bridge].[atClsSectAttendance]
(
[trg_ClsSectAttId] [uniqueidentifier] NOT NULL,
[trg_ClsSectionId] [uniqueidentifier] NOT NULL,
[trg_StuEnrollId] [uniqueidentifier] NOT NULL,
[trg_ClsSectMeetingId] [uniqueidentifier] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[atClsSectAttendance] ADD CONSTRAINT [PK__atClsSec__A5374B04C847E360] PRIMARY KEY CLUSTERED  ([trg_ClsSectAttId], [trg_ClsSectionId], [trg_StuEnrollId], [trg_CampusId]) ON [PRIMARY]
GO
