CREATE TABLE [bridge].[arGrdComponentTypes]
(
[trg_GrdComponentTypeId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[src_SubjectDefKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGrdComponentTypes] ADD CONSTRAINT [PK__arGrdCom__99F10D095A6F3A0E] PRIMARY KEY CLUSTERED  ([src_Name], [src_RecordKey], [src_SubjectDefKey], [trg_CampusId]) ON [PRIMARY]
GO
