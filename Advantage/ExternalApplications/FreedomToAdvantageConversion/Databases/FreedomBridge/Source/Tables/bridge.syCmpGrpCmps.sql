CREATE TABLE [bridge].[syCmpGrpCmps]
(
[trg_syCmpGrpCmpId] [uniqueidentifier] NOT NULL,
[trg_CampGrpId] [uniqueidentifier] NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[syCmpGrpCmps] ADD CONSTRAINT [PK__syCmpGrp__BE487365A5ED4FAA] PRIMARY KEY CLUSTERED  ([trg_syCmpGrpCmpId], [trg_CampusId]) ON [PRIMARY]
GO
