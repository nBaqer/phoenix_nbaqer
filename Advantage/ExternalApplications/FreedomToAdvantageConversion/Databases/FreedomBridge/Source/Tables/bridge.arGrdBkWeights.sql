CREATE TABLE [bridge].[arGrdBkWeights]
(
[trg_InstrGrdBkWgtId] [uniqueidentifier] NULL,
[src_Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[src_RecordKey] [int] NOT NULL,
[trg_CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [bridge].[arGrdBkWeights] ADD CONSTRAINT [PK__arGrdBkW__482484A789DAA9FB] PRIMARY KEY CLUSTERED  ([src_Name], [src_RecordKey], [trg_CampusId]) ON [PRIMARY]
GO
