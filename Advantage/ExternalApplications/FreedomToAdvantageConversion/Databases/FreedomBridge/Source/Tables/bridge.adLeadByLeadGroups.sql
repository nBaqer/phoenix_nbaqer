CREATE TABLE [bridge].[adLeadByLeadGroups]
(
[trg_LeadGrpLeadId] [uniqueidentifier] NULL,
[trg_LeadGrpId] [uniqueidentifier] NULL,
[trg_LeadId] [uniqueidentifier] NOT NULL,
[trg_StudentId] [uniqueidentifier] NULL,
[trg_StuEnrollId] [uniqueidentifier] NULL,
[trg_CampusID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
