CREATE TABLE [stage].[saTransCodesPriority]
(
[src_FullDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Priority] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [stage].[saTransCodesPriority] ADD CONSTRAINT [PK_saTransCodesPriority] PRIMARY KEY CLUSTERED  ([src_FullDescrip]) ON [PRIMARY]
GO
