SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-05-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arReqs]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /****** Set up scheduled attendance courses that will need to be registered by tying them to an existing course in Freedom ******/

        SELECT     LTRIM(RTRIM(pd.Name)) AS Descrip
                  ,sCourse.Name AS ProgVerDescrip
                  ,sCourse.PrivLength
                  ,CASE WHEN MIN(sSub.RecordKey) = MIN(sr.SubjectDefKey) THEN MIN(sr.SubjectDefKey)
                        ELSE 0
                   END AS src_RecordKey
                  ,sCourse.RecordKey AS CourseDefKey
                  ,sCourse.PrivAttendType
        INTO       #AttCourse
        FROM       Freedom.dbo.SUBJECT_DEF sSub
        INNER JOIN Freedom.dbo.COURSE_DEF sCourse ON sCourse.RecordKey = sSub.CourseDefKey
        INNER JOIN Freedom.dbo.PROGSTUDY_DEF pd ON pd.RecordKey = sCourse.ParentKey
        INNER JOIN Freedom.dbo.SUBJECT_RES sr ON sr.SubjectDefKey = sSub.RecordKey
        WHERE      sSub.CourseDefKey > 0
        GROUP BY   pd.Name
                  ,sCourse.RecordKey
                  ,sCourse.Name
                  ,sCourse.PrivLength
                  ,sCourse.PrivAttendType;

        /******** Collect course information and rank it by courses with the same name *********/
        SELECT CASE WHEN DupCourse.LocalRank = 1 THEN NEWID()
                    ELSE CAST(NULL AS UNIQUEIDENTIFIER)
               END AS ReqId -- uniqueidentifier
              ,*
        INTO   #DupCourse
        FROM   (
               SELECT     NULL AS CourseID                                   -- int
                         ,sSub.SubjectCode AS Code                           -- varchar(12)
                         ,LTRIM(RTRIM(sSub.Name)) AS Descrip                 -- varchar(100)
                         ,dStatus.StatusId AS StatusId                       -- uniqueidentifier
                         ,1 AS ReqTypeId                                     -- smallint
                         ,sSub.Credits AS Credits                            -- decimal
                         ,sSub.FinAidCredits AS FinAidCredits                -- decimal
                         ,0 AS Hours                                         -- decimal
                         ,NULL AS Cnt                                        -- smallint
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS GrdLvlId         -- uniqueidentifier
                         ,bAttType.trg_UnitTypeId AS UnitTypeId              -- uniqueidentifier
                         ,dCampGrp.CampGrpId AS CampGrpId                    -- uniqueidentifier
                         ,'' AS CourseCatalog                                -- varchar(12)
                         ,'' AS CourseComments                               -- varchar(300)
                         ,GETDATE() AS ModDate                               -- datetime
                         ,'sa' AS ModUser                                    -- varchar(50)
                         ,0 AS SU                                            -- bit
                         ,0 AS PF                                            -- bit
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS CourseCategoryId -- uniqueidentifier
                         ,0 AS TrackTardies                                  -- bit
                         ,NULL AS TardiesMakingAbsence                       -- int
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS DeptId           -- uniqueidentifier
                         ,NULL AS MinDate                                    -- smalldatetime
                         ,0 AS IsComCourse                                   -- bit
                         ,0 AS IsOnLine                                      -- bit
                         ,NULL AS CompletedDate                              -- datetime
                         ,0 AS IsExternship                                  -- bit
                         ,sCourse.UsesTimeClock AS UseTimeClock              -- bit
                         ,0 AS IsAttendanceOnly                              -- bit
                         ,0 AS AllowCompletedCourseRetake                    -- bit
                         ,sSub.CourseDefKey
                         ,sSub.RecordKey AS src_RecordKey
                         --,DENSE_RANK() OVER ( ORDER BY sProg.Name
                         --                             ,sSub.Name
                         --                             ,sSub.SubjectCode
                         --                             ,sCourse.PrivLength
                         --                   ) AS TotalRank
                         ,DENSE_RANK() OVER ( PARTITION BY sProg.CourseDefVerKey
                                                          --,sSub.Name
                                                          ,sCourse.PrivLength
                                                          ,sSub.SubjectCode
                                              ORDER BY sProg.CourseDefVerKey
                                                      ,sSub.SubjectCode
											   ,sCourse.PrivLength
                                            ) AS LocalRank
                         ,sCourse.Name AS ProgramName
                         ,sSub.Name AS CourseName
                         ,sSub.SubjectCode AS SubjectCode
                         ,sCourse.PrivLength AS PrivLength
               FROM       Freedom.dbo.SUBJECT_DEF sSub
               INNER JOIN Freedom.dbo.COURSE_DEF sCourse ON sCourse.RecordKey = sSub.CourseDefKey
               INNER JOIN Freedom.dbo.PROGSTUDY_DEF sProg ON sProg.RecordKey = sCourse.ParentKey
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.Status = 'Active'
               INNER JOIN FreedomAdvantage.dbo.syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(@SchoolID AS VARCHAR(50))
               INNER JOIN FreedomBridge.bridge.arAttUnitType bAttType ON bAttType.src_AttendanceTypeID = sCourse.PrivAttendType
               WHERE      sSub.CourseDefKey > 0
               ) DupCourse;

        /******** Populate the same course names with the same ReqId *********/
        SELECT ISNULL(c1.ReqId, NEWID()) AS ReqId
              ,c1.CourseID
              ,c1.Code
              ,c1.Descrip
              ,c1.StatusId
              ,c1.ReqTypeId
              ,c1.Credits
              ,c1.FinAidCredits
              ,c1.Hours
              ,c1.Cnt
              ,c1.GrdLvlId
              ,c1.UnitTypeId
              ,c1.CampGrpId
              ,c1.CourseCatalog
              ,c1.CourseComments
              ,c1.ModDate
              ,c1.ModUser
              ,c1.SU
              ,c1.PF
              ,c1.CourseCategoryId
              ,c1.TrackTardies
              ,c1.TardiesMakingAbsence
              ,c1.DeptId
              ,c1.MinDate
              ,c1.IsComCourse
              ,c1.IsOnLine
              ,c1.CompletedDate
              ,c1.IsExternship
              ,c1.UseTimeClock
              ,c1.IsAttendanceOnly
              ,c1.AllowCompletedCourseRetake
              ,c1.CourseDefKey
              ,c1.LocalRank
              ,c1.src_RecordKey
              ,c1.ProgramName
              ,c1.CourseName
              ,c1.SubjectCode
              ,c1.PrivLength
        INTO   #MergedCourse
        FROM   #DupCourse c1
        WHERE  c1.LocalRank = 1
               AND c1.ReqId IS NOT NULL;

					SELECT coursename AS CN, ProgramName AS PN, * FROM #DupCourse ORDER BY ProgramName, CourseName


        ALTER TABLE FreedomAdvantage..arReqs DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arReqs
                    SELECT    tCourse.ReqId
                             ,tCourse.CourseID
                             ,tCourse.Code
                             ,tCourse.Descrip
                             ,tCourse.StatusId
                             ,tCourse.ReqTypeId
                             ,tCourse.Credits
                             ,tCourse.FinAidCredits
                             ,tCourse.Hours
                             ,tCourse.Cnt
                             ,tCourse.GrdLvlId
                             ,tCourse.UnitTypeId
                             ,tCourse.CampGrpId
                             ,tCourse.CourseCatalog
                             ,tCourse.CourseComments
                             ,tCourse.ModDate
                             ,tCourse.ModUser
                             ,tCourse.SU
                             ,tCourse.PF
                             ,tCourse.CourseCategoryId
                             ,tCourse.TrackTardies
                             ,tCourse.TardiesMakingAbsence
                             ,tCourse.DeptId
                             ,tCourse.MinDate
                             ,tCourse.IsComCourse
                             ,tCourse.IsOnLine
                             ,tCourse.CompletedDate
                             ,tCourse.IsExternship
                             ,tCourse.UseTimeClock
                             ,tCourse.IsAttendanceOnly
                             ,tCourse.AllowCompletedCourseRetake
                    FROM      #MergedCourse tCourse
                    LEFT JOIN FreedomAdvantage..arReqs dReqs ON dReqs.CampGrpId = tCourse.CampGrpId
                                                                AND dReqs.Descrip = tCourse.Descrip
                                                                AND dReqs.Code = tCourse.Code
                    WHERE     dReqs.ReqId IS NULL;

        UPDATE D
        SET    D.ReqId = (
                         SELECT TOP 1 ReqId
                         FROM   #MergedCourse M
                         WHERE  M.SubjectCode = D.SubjectCode
                                AND M.ProgramName = D.ProgramName
                                --AND M.PrivLength = D.PrivLength
                                AND M.CourseName = D.CourseName
                         )
        FROM   #DupCourse D
        WHERE  D.ReqId IS NULL;



        ALTER TABLE FreedomAdvantage..arReqs ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.arReqs
                    SELECT    tCourse.ReqId
                             ,tCourse.Descrip
                             ,tCourse.src_RecordKey
                             ,tCourse.CourseDefKey
                             ,@CampusId AS CampusId
                    FROM      #DupCourse tCourse
                    LEFT JOIN FreedomBridge.bridge.arReqs bReqs ON bReqs.src_Name = tCourse.Descrip
                                                                   AND bReqs.src_RecordKey = tCourse.src_RecordKey
                                                                   AND bReqs.trg_CampusId = @CampusId
                    WHERE     bReqs.trg_ReqId IS NULL;

        DROP TABLE #Settings;
        DROP TABLE #AttCourse;
        DROP TABLE #DupCourse;
        --DROP TABLE #DupAttCourse;
        DROP TABLE #MergedCourse;
        --DROP TABLE #MergedAttCourse;
        --DROP TABLE #Course;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arReqs]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
