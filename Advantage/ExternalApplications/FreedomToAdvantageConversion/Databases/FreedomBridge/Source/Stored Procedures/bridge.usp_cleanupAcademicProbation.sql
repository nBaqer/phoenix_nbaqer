SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_cleanupAcademicProbation]
AS
    ALTER TABLE [FreedomAdvantage]..syStudentStatusChanges DISABLE TRIGGER ALL;
    BEGIN TRANSACTION ScriptTransaction;
    BEGIN TRY

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';


        DECLARE @StatusCodeId UNIQUEIDENTIFIER = (
                                                 SELECT StatusCodeId
                                                 FROM   [FreedomAdvantage]..syStatusCodes
                                                 WHERE  StatusCodeDescrip = 'Academic Probation'
                                                 );

        DECLARE @EnrollmentsWithIssues TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );

        DECLARE @History TABLE
            (
                RowNumber BIGINT
               ,StudentStatusChangeId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,OrigStatusId UNIQUEIDENTIFIER
               ,FromStatus VARCHAR(100)
               ,NewStatusId UNIQUEIDENTIFIER
               ,ToStatus VARCHAR(100)
               ,CampusId UNIQUEIDENTIFIER
               ,ModDate DATETIME
               ,ModUser VARCHAR(50)
               ,IsReversal BIT
               ,DropReasonId UNIQUEIDENTIFIER
               ,DateOfChange DATETIME
               ,Lda DATETIME2
               ,CaseNumber NVARCHAR(100)
               ,RequestedBy NVARCHAR(200)
               ,HaveBackup BIT
               ,HaveClientConfirmation BIT
            );


        /*
GET THE LIST OF ENROLLMENTS that have an academic probation
*/


        INSERT INTO @EnrollmentsWithIssues
                    SELECT     statuses.StuEnrollId AS StuEnrollId
                    FROM       [FreedomAdvantage]..syStudentStatusChanges statuses
                    INNER JOIN [FreedomAdvantage]..syStatusCodes codes ON codes.StatusCodeId = statuses.NewStatusId
                    INNER JOIN [FreedomAdvantage]..syStatusCodes codes2 ON codes2.StatusCodeId = statuses.OrigStatusId
					INNER JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = statuses.StuEnrollId
                    WHERE      (
                               codes.StatusCodeId = @StatusCodeId
                               OR codes2.StatusCodeId = @StatusCodeId
                               )
							   AND E.CampusId = @CampusId
                    GROUP BY   statuses.StuEnrollId;


        /*
GET THE HISTORY IN ORDER FOR EACH ENROLLMENT
*/

        INSERT INTO @History
                    SELECT     ( ROW_NUMBER() OVER ( PARTITION BY history.StuEnrollId
                                                                 ,history.CampusId
                                                     ORDER BY history.StuEnrollId
                                                             ,history.ModDate
                                                             ,toStatus.SysStatusId ASC
                                                   )
                               ) AS RowNumber
                              ,history.StudentStatusChangeId
                              ,history.StuEnrollId
                              ,history.OrigStatusId
                              ,fromStatus.StatusCodeDescrip AS FromStatus
                              ,history.NewStatusId
                              ,toStatus.StatusCodeDescrip AS ToStatus
                              ,history.CampusId
                              ,history.ModDate
                              ,history.ModUser
                              ,history.IsReversal
                              ,history.DropReasonId
                              ,history.DateOfChange
                              ,history.Lda
                              ,history.CaseNumber
                              ,history.RequestedBy
                              ,history.HaveBackup
                              ,history.HaveClientConfirmation
                    FROM       [FreedomAdvantage]..syStudentStatusChanges history
                    LEFT JOIN  [FreedomAdvantage]..syStatusCodes fromStatus ON fromStatus.StatusCodeId = history.OrigStatusId
                    INNER JOIN [FreedomAdvantage]..syStatusCodes toStatus ON toStatus.StatusCodeId = history.NewStatusId
                    WHERE      StuEnrollId IN (
                                              SELECT StuEnrollId
                                              FROM   @EnrollmentsWithIssues
                                              )
                    ORDER BY   StuEnrollId
                              ,history.ModDate ASC
                              ,toStatus.SysStatusId ASC;


        DECLARE @dateOfChangeSwap TABLE
            (
                DateOfChange DATETIME
               ,ModDate DATETIME
               ,OriginalNumber INT
               ,newNumber INT
               ,StudentStatusChangeId UNIQUEIDENTIFIER
            );

        INSERT INTO @dateOfChangeSwap
                    SELECT     historyDateOfChange.DateOfChange
                              ,historyDateOfChange.ModDate
                              ,historyWithProbations.RowNumber OriginalNumber
                              ,historyNextStatus.RowNumber newNumber
                              ,historyNextStatus.StudentStatusChangeId
                    FROM       [FreedomAdvantage]..syStudentStatusChanges historyDateOfChange
                    INNER JOIN @History historyWithProbations ON historyWithProbations.StudentStatusChangeId = historyDateOfChange.StudentStatusChangeId
                    LEFT JOIN  @History historyNextStatus ON historyWithProbations.StuEnrollId = historyNextStatus.StuEnrollId
                                                             AND historyNextStatus.RowNumber = historyWithProbations.RowNumber + 1
                    WHERE      historyDateOfChange.NewStatusId = @StatusCodeId;

        UPDATE     history
        SET        history.DateOfChange = dateOfChangeSwap.DateOfChange
                  ,history.ModDate = dateOfChangeSwap.ModDate
        FROM       [FreedomAdvantage]..syStudentStatusChanges history
        INNER JOIN @dateOfChangeSwap dateOfChangeSwap ON dateOfChangeSwap.StudentStatusChangeId = history.StudentStatusChangeId;


        DELETE FROM [FreedomAdvantage]..syStudentStatusChanges
        WHERE StudentStatusChangeId IN (
                                       SELECT     HistoryPrev.StudentStatusChangeId
                                       FROM       [FreedomAdvantage]..syStudentStatusChanges changesOnNewStatus
                                       INNER JOIN @History AS HistoryPrev ON changesOnNewStatus.StudentStatusChangeId = HistoryPrev.StudentStatusChangeId
                                       --LEFT JOIN @History AS HistoryNext
                                       --  ON HistoryPrev.StuEnrollId = HistoryNext.StuEnrollId
                                       --   AND HistoryNext.RowNumber = HistoryPrev.RowNumber + 1
                                       WHERE      HistoryPrev.NewStatusId = @StatusCodeId
                                       );



        DECLARE @SwapStatuesForNextAvailable AS TABLE
            (
                RowNumber BIGINT
               ,OrigStatusId UNIQUEIDENTIFIER
               ,StudentStatusChangeId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
            );

        INSERT INTO @SwapStatuesForNextAvailable
                    SELECT    prevHistory.RowNumber
                             ,prevHistory.NewStatusId AS OrigStatusId
                             ,currentData.StudentStatusChangeId
                             ,prevHistory.StuEnrollId
                    FROM      @History currentData
                    LEFT JOIN @History prevHistory ON prevHistory.StuEnrollId = currentData.StuEnrollId
                                                      AND prevHistory.RowNumber = currentData.RowNumber - 2
                    WHERE     currentData.OrigStatusId = @StatusCodeId;


        UPDATE     history
        SET        history.OrigStatusId = swappedStatus.OrigStatusId
        FROM       [FreedomAdvantage]..syStudentStatusChanges history
        INNER JOIN @SwapStatuesForNextAvailable swappedStatus ON swappedStatus.StudentStatusChangeId = history.StudentStatusChangeId;

        --SELECT * FROM @SwapStatuesForNextAvailable
        --SELECT * FROM @History

        /*
	UDPATE STUDENT WITH ACADEMIC PROBATIONS THAT ARE STILL REMAINING WITH THE PREVIOUS STATUS
	*/

        DELETE @History;
        DELETE @EnrollmentsWithIssues;

        INSERT INTO @EnrollmentsWithIssues
                    SELECT     statuses.StuEnrollId AS StuEnrollId
                    FROM       [FreedomAdvantage]..syStudentStatusChanges statuses
                    INNER JOIN [FreedomAdvantage]..syStatusCodes codes ON codes.StatusCodeId = statuses.NewStatusId
                    INNER JOIN [FreedomAdvantage]..syStatusCodes codes2 ON codes2.StatusCodeId = statuses.OrigStatusId
                    WHERE      (
                               codes.StatusCodeId = @StatusCodeId
                               OR codes2.StatusCodeId = @StatusCodeId
                               )
                    GROUP BY   statuses.StuEnrollId;

        INSERT INTO @History
                    SELECT     ( ROW_NUMBER() OVER ( PARTITION BY history.StuEnrollId
                                                                 ,history.CampusId
                                                     ORDER BY history.StuEnrollId
                                                             ,history.ModDate
                                                             ,toStatus.SysStatusId ASC
                                                   )
                               ) AS RowNumber
                              ,history.StudentStatusChangeId
                              ,history.StuEnrollId
                              ,history.OrigStatusId
                              ,fromStatus.StatusCodeDescrip AS FromStatus
                              ,history.NewStatusId
                              ,toStatus.StatusCodeDescrip AS ToStatus
                              ,history.CampusId
                              ,history.ModDate
                              ,history.ModUser
                              ,history.IsReversal
                              ,history.DropReasonId
                              ,history.DateOfChange
                              ,history.Lda
                              ,history.CaseNumber
                              ,history.RequestedBy
                              ,history.HaveBackup
                              ,history.HaveClientConfirmation
                    FROM       [FreedomAdvantage]..syStudentStatusChanges history
                    LEFT JOIN  [FreedomAdvantage]..syStatusCodes fromStatus ON fromStatus.StatusCodeId = history.OrigStatusId
                    INNER JOIN [FreedomAdvantage]..syStatusCodes toStatus ON toStatus.StatusCodeId = history.NewStatusId
                    WHERE      StuEnrollId IN (
                                              SELECT StuEnrollId
                                              FROM   @EnrollmentsWithIssues
                                              )
                    ORDER BY   StuEnrollId
                              ,history.ModDate ASC
                              ,toStatus.SysStatusId ASC;

        DELETE @SwapStatuesForNextAvailable;

        INSERT INTO @SwapStatuesForNextAvailable (
                                                 RowNumber
                                                ,OrigStatusId
                                                ,StudentStatusChangeId
                                                 )
                    SELECT    history.RowNumber
                             ,prevRecords.NewStatusId
                             ,history.StudentStatusChangeId
                    FROM      @History history
                    LEFT JOIN @History prevRecords ON prevRecords.StuEnrollId = history.StuEnrollId
                                                      AND prevRecords.RowNumber = history.RowNumber - 1
                    WHERE     history.OrigStatusId = @StatusCodeId;

        UPDATE     studentHistory
        SET        studentHistory.OrigStatusId = swapStatus.OrigStatusId
        FROM       [FreedomAdvantage]..syStudentStatusChanges studentHistory
        INNER JOIN @SwapStatuesForNextAvailable swapStatus ON swapStatus.StudentStatusChangeId = studentHistory.StudentStatusChangeId;

        DELETE [FreedomAdvantage]..syStudentStatusChanges
        WHERE OrigStatusId = NewStatusId;

        /*
	Clean UP Enrollments that are probation with new status
	*/

        DECLARE @EnrollmentsThatAreProbation TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @EnrollmentsThatAreProbation
                    SELECT StuEnrollId
                    FROM   [FreedomAdvantage]..arStuEnrollments
                    WHERE  StatusCodeId = @StatusCodeId;

        DECLARE @NewHistoryOrdered TABLE
            (
                RowNumber INT
               ,StudentStatusChangeId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,OrigStatusId UNIQUEIDENTIFIER
               ,NewStatusId UNIQUEIDENTIFIER
            );

        INSERT INTO @NewHistoryOrdered
                    SELECT     ( ROW_NUMBER() OVER ( PARTITION BY history.StuEnrollId
                                                                 ,history.CampusId
                                                     ORDER BY history.StuEnrollId
                                                             ,history.ModDate
                                                             ,toStatus.SysStatusId ASC
                                                   )
                               ) AS RowNumber
                              ,history.StudentStatusChangeId
                              ,history.StuEnrollId
                              ,history.OrigStatusId
                              ,history.NewStatusId
                    FROM       [FreedomAdvantage]..syStudentStatusChanges history
                    LEFT JOIN  [FreedomAdvantage]..syStatusCodes fromStatus ON fromStatus.StatusCodeId = history.OrigStatusId
                    INNER JOIN [FreedomAdvantage]..syStatusCodes toStatus ON toStatus.StatusCodeId = history.NewStatusId
                    WHERE      StuEnrollId IN (
                                              SELECT StuEnrollId
                                              FROM   @EnrollmentsThatAreProbation
                                              )
                    ORDER BY   StuEnrollId
                              ,history.ModDate ASC
                              ,toStatus.SysStatusId ASC;

        UPDATE     Enrollments
        SET        Enrollments.StatusCodeId = history.NewStatusId
        FROM       [FreedomAdvantage]..arStuEnrollments AS Enrollments
        INNER JOIN (
                   SELECT   MAX(RowNumber) AS RowNumber
                           ,StuEnrollId
                           ,NewStatusId
                   FROM     @NewHistoryOrdered
                   GROUP BY StuEnrollId
                           ,NewStatusId
                   ) AS history ON history.StuEnrollId = Enrollments.StuEnrollId;


        DROP TABLE #Settings;
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber
              ,ERROR_SEVERITY() AS ErrorSeverity
              ,ERROR_STATE() AS ErrorState
              ,ERROR_PROCEDURE() AS ErrorProcedure
              ,ERROR_LINE() AS ErrorLine
              ,ERROR_MESSAGE() AS ErrorMessage;

        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION ScriptTransaction;
            END;
    END CATCH;

    IF @@TRANCOUNT > 0
        BEGIN
            COMMIT TRANSACTION ScriptTransaction;
        END;

GO
