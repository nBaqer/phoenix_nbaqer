SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arHousing]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
     IF NOT EXISTS (   SELECT TOP 1 *
                      FROM   FreedomAdvantage.dbo.arHousing
                      WHERE Descrip LIKE 'Incarcerated')
        BEGIN
        INSERT INTO FreedomAdvantage..arHousing
                    SELECT NEWID() AS HousingId            -- uniqueidentifier
                         , 'INC' AS Code                   -- varchar(50)
                         , 'Incarcerated' AS Descrip       -- varchar(50)
                         , dStatus.StatusId AS StatusId    -- uniqueidentifier
                         , dCampGrp.CampGrpId AS CampGrpId -- uniqueidentifier
                         , GETDATE() AS ModDate            -- datetime
                         , 'sa' AS ModUser                 -- varchar(50)
                         , NULL AS IPEDSValue
                    FROM   FreedomAdvantage..syStatuses dStatus
                    INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                    LEFT JOIN bridge.arHousing bIgnore ON bIgnore.src_Description = 'Incarcerated'
                    WHERE  dStatus.Status = 'Active'
                           AND bIgnore.src_Description IS NULL;
         END;


        SELECT dHouse.HousingId --uniqueidentifier
             , FL.ID
             , FL.Description
             , FL.LookUpID
        INTO   #arHousing
        FROM   stage.FLookUp FL
        LEFT JOIN FreedomAdvantage..arHousing dHouse ON dHouse.Descrip = CASE WHEN EXISTS ( SELECT TOP 1 1 FROM FreedomAdvantage..arHousing E WHERE E.Descrip = FL.Description) THEN FL.Description  WHEN FL.Description = 'On campus' THEN 'On-Campus'
                                                                              WHEN FL.Description = 'Off campus' THEN 'Off-Campus Not With Family'
                                                                              WHEN FL.Description = 'With parents' THEN 'Off-Campus With Family'
                                                                              WHEN FL.Description = 'Incarcerated' THEN 'Incarcerated'
                                                                         END
        LEFT JOIN bridge.arHousing bIgnore ON bIgnore.src_Description = FL.Description
        WHERE  LookupName = 'Housing'
               AND bIgnore.src_ID IS NULL
			    --AND  NOT EXISTS ( SELECT TOP 1 *
       --       FROM   FreedomAdvantage..arHousing 
       --       WHERE Descrip = FL.Description) ;




        INSERT INTO FreedomBridge.bridge.arHousing
                    SELECT HousingId
                         , ID
                         , Description
                         , LookUpID
                    FROM   #arHousing;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arHousing]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]
					
        COMMIT TRANSACTION;

    END;
GO
