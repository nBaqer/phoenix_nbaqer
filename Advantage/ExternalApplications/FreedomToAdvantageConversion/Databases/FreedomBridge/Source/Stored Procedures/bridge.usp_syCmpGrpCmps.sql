SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-29-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syCmpGrpCmps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT NEWID() AS CmpGrpCmpId          -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId -- uniqueidentifier
             , dCampus.CampusId AS CampusId    -- uniqueidentifier
             , dCampGrp.CampGrpCode
             , 'sa' AS ModUser                 -- varchar(50)
             , GETDATE() AS ModDate            -- datetime
        INTO   #CampGrpCmp
        FROM   Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syCampuses dCampus ON dCampus.CampCode = CAST(sSchool.SchoolID AS VARCHAR(12))
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(sSchool.SchoolID AS VARCHAR(12))
                                                            OR dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..syCmpGrpCmps dCmpGrp ON dCmpGrp.CampGrpId = dCampGrp.CampGrpId
                                                            AND dCmpGrp.CampusId = dCampus.CampusId
        WHERE  dCmpGrp.CampGrpId IS NULL;

        ALTER TABLE FreedomAdvantage..syCmpGrpCmps DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syCmpGrpCmps
                    SELECT tCamp.CmpGrpCmpId
                         , tCamp.CampGrpId
                         , tCamp.CampusId
                         , tCamp.ModUser
                         , tCamp.ModDate
                    FROM   #CampGrpCmp tCamp
                    LEFT JOIN FreedomAdvantage..syCmpGrpCmps dCampGrp ON dCampGrp.CampGrpId = tCamp.CampGrpId
                    WHERE  tCamp.CampGrpCode != 'ALL'
                           AND dCampGrp.CampGrpId IS NULL;

        INSERT INTO FreedomAdvantage..syCmpGrpCmps
                    SELECT tCamp.CmpGrpCmpId
                         , tCamp.CampGrpId
                         , tCamp.CampusId
                         , tCamp.ModUser
                         , tCamp.ModDate
                    FROM   #CampGrpCmp tCamp
                    LEFT JOIN FreedomAdvantage..syCmpGrpCmps dCmpGrp ON dCmpGrp.CampGrpId = tCamp.CampGrpId
                                                                        AND dCmpGrp.CmpGrpCmpId = tCamp.CmpGrpCmpId
                    WHERE  tCamp.CampGrpCode = 'ALL'
                           AND dCmpGrp.CampGrpId IS NULL;

        ALTER TABLE FreedomAdvantage..syCmpGrpCmps ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syCmpGrpCmps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syCmpGrpCmps
                    (
                        trg_syCmpGrpCmpId UNIQUEIDENTIFIER
                      , trg_CampGrpId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_syCmpGrpCmpId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.syCmpGrpCmps
                    SELECT CmpGrpCmpId
                         , CampGrpId
                         , CampusId
                    FROM   #CampGrpCmp;

        DROP TABLE #CampGrpCmp;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syCmpGrpCmps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
