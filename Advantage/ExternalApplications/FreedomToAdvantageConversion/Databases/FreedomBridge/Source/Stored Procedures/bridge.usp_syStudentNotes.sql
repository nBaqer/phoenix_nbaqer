SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_syStudentNotes]
AS
    BEGIN
        BEGIN TRAN;

        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT
              , @MaxNotesId INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @MaxNotesId = ISNULL(MAX(AllNotes.NotesId), 0)
        FROM   FreedomAdvantage..AllNotes AllNotes;

        --Student academic record notes section
        SELECT IDENTITY(INT, 1, 1) AS NotesId                                                        -- INT IDENTITY(1, NULL AS 1) NOT NULL
             , 'AR' AS ModuleCode                                                                    -- CHAR(2) NOT NULL
             , 'Note' AS NoteType                                                                    -- VARCHAR(50) NOT NULL
             , bUser.trg_UserId AS UserId                                                            -- UNIQUEIDENTIFIER NOT NULL
             , dbo.udf_GetStudentNoteComment(sNote1.CommentKey) AS NoteText                          -- VARCHAR(2000) NOT NULL
             , 'SA' AS ModUser                                                                       -- VARCHAR(50) NOT NULL
             , CAST(CAST(CAST(sNote1.PrivAddDate AS DATE) AS VARCHAR(20)) + ' ' + LEFT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), LEN(sNote1.PrivAddTime) - 2)
                    + ':' + RIGHT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), 2) AS DATETIME) AS ModDate -- DATETIME NOT NULL
             , 1 AS PageFieldId                                                                      -- INT NOT NULL (INFO)
             , bLead.trg_LeadId AS LeadId
             , sNote1.RecordKey
        INTO   #AllNotes_AR
        FROM   bridge.arStudentOLD bStud
        INNER JOIN bridge.adLeads bLead ON bLead.src_StuRecordKey = bStud.src_RecordKey
                                           AND bLead.trg_CampusId = bStud.trg_CampusId
        INNER JOIN FreedomAdvantage..arStudent dStud ON dStud.StudentId = bStud.trg_StudentId
        INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = bStud.src_RecordKey
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN Freedom..STUDREC_NOT sNote ON sNote.RecordKey = sStud.NotesKey
        INNER JOIN Freedom..STUDREC_NOT sNote1 ON sNote1.ParentKey = sNote.ParentKey
        LEFT JOIN Freedom..SHIBBOLETH_FIL sUser ON sUser.UserId = sNote1.PrivAddUserId
        LEFT JOIN bridge.syUsers bUser ON bUser.src_UserId = sUser.UserId
                                          AND bUser.trg_CampusId = @CampusId
                                          AND bUser.TableName = 'SHIBBOLETH_FIL'
        WHERE  bStud.trg_CampusId = @CampusId;

        ALTER TABLE FreedomAdvantage..AllNotes DISABLE TRIGGER ALL;
        ALTER TABLE FreedomAdvantage..adLead_Notes DISABLE TRIGGER ALL;
        SET IDENTITY_INSERT FreedomAdvantage..AllNotes ON;

        INSERT INTO FreedomAdvantage..AllNotes (
                                                   NotesId
                                                 , ModuleCode
                                                 , NoteType
                                                 , UserId
                                                 , NoteText
                                                 , ModUser
                                                 , ModDate
                                                 , PageFieldId
                                               )
                    SELECT   NotesId + @MaxNotesId AS NotesId
                           , ModuleCode
                           , NoteType
                           , UserId
                           , NoteText
                           , ModUser
                           , ModDate
                           , PageFieldId
                    FROM     #AllNotes_AR
                    ORDER BY 1;

        INSERT INTO bridge.AllNotes (
                                        trg_NotesId
                                      , trg_ModuleName
                                      , src_TableName
                                      , src_RecordKey
                                      , trg_CampusID
                                    )
                    SELECT NotesId + @MaxNotesId AS trg_NotesId
                         , ModuleCode
                         , 'STUDREC_NOT' AS src_TableName
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                    FROM   #AllNotes_AR;

        DECLARE @BridgeAdLead_Notes TABLE
            (
                id INT
              , LeadID UNIQUEIDENTIFIER
              , NoteID INT
            );

        INSERT INTO FreedomAdvantage..adLead_Notes (
                                                       LeadId
                                                     , NotesId
                                                   )
        OUTPUT Inserted.id
             , Inserted.LeadId
             , Inserted.NotesId
        INTO @BridgeAdLead_Notes
                    SELECT LeadId
                         , NotesId + @MaxNotesId AS NotesId
                    FROM   #AllNotes_AR;

        INSERT INTO bridge.adLead_Notes (
                                            trg_id
                                          , src_TableName
                                          , src_RecordKey
                                          , trg_CampusID
                                        )
                    SELECT tLeadNote.id AS trg_id
                         , 'STUDREC_NOT' AS src_TableName
                         , tNote.RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusID
                    FROM   @BridgeAdLead_Notes tLeadNote
                    INNER JOIN #AllNotes_AR tNote ON tNote.NotesId = tLeadNote.NoteID;

        --SELECT @MaxNotesId = ISNULL(MAX(AllNotes.NotesId), 0)
        --FROM   FreedomAdvantage..AllNotes AllNotes;

        --SELECT IDENTITY(INT, 1, 1) AS NotesId                                                  -- INT IDENTITY(1, NULL AS 1) NOT NULL
        --     , 'PL' AS ModuleCode                                                              -- CHAR(2) NOT NULL
        --     , 'Note' AS NoteType                                                              -- VARCHAR(50) NOT NULL
        --     , bUser.trg_UserId AS UserId                                                      -- UNIQUEIDENTIFIER NOT NULL
        --     , dbo.udf_GetStudentNoteComment(sNote1.CommentKey) AS NoteText                    -- VARCHAR(2000) NOT NULL
        --     , 'SA' AS ModUser                                                                 -- VARCHAR(50) NOT NULL
        --     , CAST(CAST(CAST(sNote1.PrivAddDate AS DATE) AS VARCHAR(20)) + ' ' + LEFT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), LEN(sNote1.PrivAddTime) - 2) + ':'
        --            + RIGHT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), 2) AS DATETIME) AS ModDate -- DATETIME NOT NULL
        --     , 1 AS PageFieldId                                                                -- INT NOT NULL (INFO)
        --     , sNote1.RecordKey
        --INTO   #AllNotes_PL
        --FROM   Freedom..GRADPLAC_FIL_NOT sNote
        --INNER JOIN Freedom..GRADPLAC_FIL sGradPlac ON sGradPlac.NotesKey = sNote.RecordKey
        --INNER JOIN Freedom..GRADPLAC_FIL_NOT sNote1 ON sNote1.ParentKey = sNote.ParentKey
        --INNER JOIN bridge.arStudentOLD bStud ON bStud.src_RecordKey = sGradPlac.StudrecKey
        --INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        --LEFT JOIN Freedom..SHIBBOLETH_FIL sUser ON sUser.UserId = sNote1.PrivAddUserId
        --LEFT JOIN bridge.syUsers bUser ON bUser.src_UserId = sUser.UserId
        --                                  AND bUser.trg_CampusId = @CampusId
        --                                  AND bUser.TableName = 'SHIBBOLETH_FIL'
        --WHERE  bStud.trg_CampusId = @CampusId;

        --INSERT INTO FreedomAdvantage..AllNotes (
        --                                           NotesId
        --                                         , ModuleCode
        --                                         , NoteType
        --                                         , UserId
        --                                         , NoteText
        --                                         , ModUser
        --                                         , ModDate
        --                                         , PageFieldId
        --                                       )
        --            SELECT   NotesId + @MaxNotesId AS NotesId
        --                   , ModuleCode
        --                   , NoteType
        --                   , UserId
        --                   , NoteText
        --                   , ModUser
        --                   , ModDate
        --                   , PageFieldId
        --            FROM     #AllNotes_PL
        --            ORDER BY 1;

        SET IDENTITY_INSERT FreedomAdvantage..AllNotes OFF;
        ALTER TABLE FreedomAdvantage..AllNotes ENABLE TRIGGER ALL;
        ALTER TABLE FreedomAdvantage..adLead_Notes ENABLE TRIGGER ALL;

        --INSERT INTO bridge.AllNotes (
        --                                trg_NotesId
        --                              , trg_ModuleName
        --                              , src_TableName
        --                              , src_RecordKey
        --                              , trg_CampusID
        --                            )
        --            SELECT NotesId + @MaxNotesId AS trg_NotesId
        --                 , ModuleCode
        --                 , 'GRADPLAC_FIL_NOT' AS src_TableName
        --                 , RecordKey AS src_RecordKey
        --                 , @CampusId AS trg_CampusId
        --            FROM   #AllNotes_PL;

	    INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syStudentNotes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;
    END;
GO
