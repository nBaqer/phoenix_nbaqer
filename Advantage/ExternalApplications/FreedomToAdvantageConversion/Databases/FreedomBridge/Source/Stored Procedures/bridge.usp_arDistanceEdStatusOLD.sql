SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arDistanceEdStatusOLD]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT
            -- Manually add values
              CASE WHEN FL.Description = 'Not offered with any distance education courses' THEN 'None'
                   WHEN FL.Description = 'Offered with some but not all distance education courses  ' THEN 'Some'
                   WHEN FL.Description = 'Offered exclusively with distance education courses' THEN 'Only'
              END AS trg_DistanceEdStatus
            , FL.ID
            , FL.Description
            , FL.LookUpID
        INTO  #arDistanceEdStatus
        FROM  stage.FLookUp FL
        LEFT JOIN bridge.arDistanceEdStatus bIgnore ON bIgnore.src_Description = FL.Description
        WHERE LookupName = 'DistanceEducationProgramStatus'
              AND bIgnore.src_ID IS NULL;

        INSERT INTO FreedomBridge.bridge.arDistanceEdStatus
                    SELECT trg_DistanceEdStatus
                         , ID
                         , Description
                         , LookUpID
                    FROM   #arDistanceEdStatus;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arDistanceEdStatusOLD]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
