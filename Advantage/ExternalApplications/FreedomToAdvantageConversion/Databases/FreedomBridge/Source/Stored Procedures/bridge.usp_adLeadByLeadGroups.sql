SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-19-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adLeadByLeadGroups]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT
			        ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@CampGrpId UNIQUEIDENTIFIER;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

		    SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';


        SELECT     ISNULL(bLeadByLeadGroups.trg_LeadGrpLeadId, NEWID()) AS LeadGrpLeadId -- uniqueidentifier
                  ,dLead.LeadId AS LeadId                                                -- uniqueidentifier
                  ,A.LeadGrpId AS LeadGrpId                                              -- uniqueidentifier
                  ,'sa' AS ModUser                                                       -- varchar(50)
                  ,GETDATE() AS ModDate                                                  -- datetime
                  ,dStuEnroll.StudentId AS StudentId                                     -- uniqueidentifier
                  ,dStuEnroll.StuEnrollId AS StuEnrollId                                 -- uniqueidentifier
                  ,@CampusId AS CampusID
                  ,0 AS RecordKey
        INTO       #LeadGrpLead
        FROM       FreedomAdvantage..adLeads dLead
        LEFT JOIN  FreedomAdvantage.dbo.arStuEnrollments dStuEnroll ON dStuEnroll.LeadId = dLead.LeadId
        CROSS JOIN (
                   SELECT dLeadGroups.LeadGrpId
                         ,dLeadGroups.Descrip
                         ,dLeadGroups.StatusId
                         ,dLeadGroups.CampGrpId
                         ,dLeadGroups.ModDate
                         ,dLeadGroups.ModUser
                         ,dLeadGroups.code
                         ,dLeadGroups.UseForScheduling
                         ,dLeadGroups.UseForStudentGroupTracking
                   FROM   FreedomAdvantage..adLeadGroups dLeadGroups
                   WHERE  dLeadGroups.code = 'C'
                   ) A
        LEFT JOIN  bridge.adLeadByLeadGroups bLeadByLeadGroups ON bLeadByLeadGroups.trg_LeadId = dLead.LeadId
                                                                  AND bLeadByLeadGroups.trg_LeadGrpId = A.LeadGrpId
        WHERE      dLead.CampusId = @CampusId;

        INSERT INTO #LeadGrpLead
                    SELECT     ISNULL(bLeadByLeadGroups.trg_LeadGrpLeadId, NEWID()) AS LeadGrpLeadId -- uniqueidentifier
                              ,dLead.LeadId AS LeadId                                                -- uniqueidentifier
                              ,bLeadGroups.trg_LeadGrpId AS LeadGrpId                                -- uniqueidentifier
                              ,'sa' AS ModUser                                                       -- varchar(50)
                              ,GETDATE() AS ModDate                                                  -- datetime
                              ,dStuEnroll.StudentId AS StudentId                                     -- uniqueidentifier
                              ,dStuEnroll.StuEnrollId AS StuEnrollId                                 -- uniqueidentifier
                              ,@CampusId AS CampusID
                              ,srcStudGroup.RecordKey
                    FROM       FreedomAdvantage..adLeads dLead
                    LEFT JOIN  FreedomAdvantage.dbo.arStuEnrollments dStuEnroll ON dStuEnroll.LeadId = dLead.LeadId
                     LEFT JOIN  bridge.arStuEnrollments bStud ON bStud.trg_StuEnrollId = dStuEnroll.StuEnrollId
                                                            AND bStud.trg_CampusId = @CampusId
                    LEFT JOIN  Freedom..STUGROUP_FIL srcStudGroup ON srcStudGroup.StudrecKey = bStud.src_RecordKey
                    INNER JOIN bridge.adLeadGroups bLeadGroups ON bLeadGroups.src_RecordKey = srcStudGroup.GroupDefKey
                                                                  AND bLeadGroups.trg_CampusId = @CampusId
                    LEFT JOIN  bridge.adLeadByLeadGroups bLeadByLeadGroups ON bLeadByLeadGroups.trg_LeadId = dLead.LeadId
                                                                              AND bLeadByLeadGroups.trg_LeadGrpId = bLeadGroups.trg_LeadGrpId;

        ALTER TABLE FreedomAdvantage..adLeadByLeadGroups DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adLeadByLeadGroups
                    SELECT    tLeadGrpLead.LeadGrpLeadId
                             ,tLeadGrpLead.LeadId
                             ,tLeadGrpLead.LeadGrpId
                             ,tLeadGrpLead.ModUser
                             ,tLeadGrpLead.ModDate
                             ,tLeadGrpLead.StudentId
                             ,tLeadGrpLead.StuEnrollId
                    FROM      #LeadGrpLead tLeadGrpLead
                    LEFT JOIN FreedomAdvantage..adLeadByLeadGroups dLeadGrpLead ON dLeadGrpLead.LeadGrpLeadId = tLeadGrpLead.LeadGrpLeadId
                    WHERE     dLeadGrpLead.LeadGrpLeadId IS NULL;

        ALTER TABLE FreedomAdvantage..adLeadByLeadGroups ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.adLeadByLeadGroups
                    SELECT tLeadGrpLead.LeadGrpLeadId
                          ,tLeadGrpLead.LeadGrpId
                          ,tLeadGrpLead.LeadId
                          ,tLeadGrpLead.StudentId
                          ,tLeadGrpLead.StuEnrollId
                          ,tLeadGrpLead.CampusID
                    FROM   #LeadGrpLead tLeadGrpLead;
        --LEFT JOIN FreedomBridge.bridge.adLeadByLeadGroups bLeadGrpLead ON bLeadGrpLead.trg_StuEnrollId = tLeadGrpLead.StuEnrollId
        --                                                                  AND bLeadGrpLead.trg_LeadId = tLeadGrpLead.LeadId
        --WHERE  bLeadGrpLead.trg_LeadGrpLeadId IS NULL;

        ---------------------------------------------------------------------------------------
        ----------------LEAD GROUPS CLEAN UP
        ------------------------------------------------------------------------------------------
        DECLARE @FreedomImportedId UNIQUEIDENTIFIER;
        SET @FreedomImportedId = (
                                 SELECT LeadGrpId
                                 FROM   FreedomAdvantage..adLeadGroups
                                 WHERE  Descrip LIKE 'Freedom Imported' AND CampGrpId = @CampusGroupAllId
                                 );


      --  DELETE FROM FreedomAdvantage..adLeadByLeadGroups
      --  WHERE LeadId IN (
      --                  SELECT     LLG.LeadId
      --                  FROM       FreedomAdvantage..adLeadByLeadGroups LLG
      --                  INNER JOIN FreedomAdvantage..adLeadGroups LG ON LG.LeadGrpId = LLG.LeadGrpId
						--LEFT JOIN FreedomAdvantage..adLeads L ON L.LeadId = LLG.LeadId
      --                  WHERE      LLG.LeadGrpId = @FreedomImportedId AND L.CampusId = @CampusId
      --                  GROUP BY   LLG.LeadId
      --                  HAVING     COUNT(*) > 1
      --                  )
      --        AND LeadGrpId = @FreedomImportedId

        DROP TABLE #LeadGrpLead;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeadByLeadGroups]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
