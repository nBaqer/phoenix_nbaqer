SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-17-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arProgCredential]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dProgCred.CredentialDescription IS NULL THEN NEWID()
                    ELSE dProgCred.CredentialId
               END AS CredentialId                        -- uniqueidentifier
             , FLook.ID AS CredentialCode                 -- varchar(12)
             , dStatus.StatusId AS StatusId               -- uniqueidentifier
             , FLook.Description AS CredentialDescription -- varchar(50)
             , dCampGrp.CampGrpId AS CampGrpId            -- uniqueidentifier
             , FLook.ID AS IPEDSSequence                  -- int
             , CASE WHEN FLook.Description = 'Undergraduate certificate' THEN 154
                    WHEN FLook.Description = 'Associate''s degree' THEN 156
                    WHEN FLook.Description = 'Bachelor''s degree' THEN 157
                    WHEN FLook.Description = 'Post baccalaureate certificate' THEN 155
                    WHEN FLook.Description = 'Master''s degree' THEN 158
                    WHEN FLook.Description = 'Doctoral degree' THEN 159
                    WHEN FLook.Description = 'First-professional degree' THEN 160
               END AS IPEDSValue                          -- int
             , 'sa' AS ModUser                            -- varchar(50)
             , GETDATE() AS ModDate                       --datetime
             , NULL AS GESequence                         -- INT NULL
             , NULL AS GERptAgencyFldValId                -- INT NULL
        INTO   #ProgCred
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..arProgCredential dProgCred ON dProgCred.CampGrpId = dCampGrp.CampGrpId
                                                                  AND dProgCred.CredentialDescription = REPLACE(FLook.Description, '-', ' ')
        WHERE  FLook.LookupName = 'CredentialLevel';

        ALTER TABLE FreedomAdvantage..arProgCredential DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arProgCredential
                    SELECT tProgCred.CredentialId
                         , tProgCred.CredentialCode
                         , tProgCred.StatusId
                         , tProgCred.CredentialDescription
                         , tProgCred.CampGrpId
                         , tProgCred.IPEDSSequence
                         , tProgCred.IPEDSValue
                         , tProgCred.ModUser
                         , tProgCred.ModDate
                         , tProgCred.GESequence
                         , tProgCred.GERptAgencyFldValId
                    FROM   #ProgCred tProgCred
                    LEFT JOIN FreedomAdvantage..arProgCredential dProgCred ON dProgCred.CampGrpId = tProgCred.CampGrpId
                                                                              AND dProgCred.CredentialDescription = REPLACE(
                                                                                                                               tProgCred.CredentialDescription
                                                                                                                             , '-'
                                                                                                                             , ' '
                                                                                                                           )
                    WHERE  dProgCred.CredentialDescription IS NULL;

        ALTER TABLE FreedomAdvantage..arProgCredential ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arProgCredential')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arProgCredential
                    (
                        trg_CredentialId UNIQUEIDENTIFIER
                      , src_CredentialDescription VARCHAR(255)
                      , src_CredentialID INT
                      ,
                      PRIMARY KEY ( src_CredentialDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arProgCredential
                    SELECT tProgCred.CredentialId
                         , tProgCred.CredentialDescription
                         , tProgCred.CredentialCode
                    FROM   #ProgCred tProgCred
                    LEFT JOIN FreedomBridge.bridge.arProgCredential bProgCred ON bProgCred.src_CredentialDescription = tProgCred.CredentialDescription
                                                                                 AND bProgCred.src_CredentialID = tProgCred.CredentialCode
                    WHERE  bProgCred.trg_CredentialId IS NULL;

        DROP TABLE #ProgCred;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arProgCredential]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
