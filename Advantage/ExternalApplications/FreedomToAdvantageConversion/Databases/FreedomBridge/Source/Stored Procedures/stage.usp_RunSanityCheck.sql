SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [stage].[usp_RunSanityCheck]
AS
    BEGIN

        DELETE FROM stage.PreConversionResults;

        ------------------------------------------------------------------------------------
        -- Students with no schedules
        ------------------------------------------------------------------------------------
        SELECT   CourseNumEnrolledIn AS CourseNumber
                ,R.RecordKey + 1000 AS Students
				
        INTO     #StudentWithMissingSchedules
        FROM     Freedom..STUDREC R
        WHERE    PrivTClockSchdRecKey = 0
                 AND NOT EXISTS (
                                SELECT TOP 1 1
                                FROM   Freedom..STUDREC C
                                WHERE  C.PrivTClockSchdRecKey > 0
                                       AND C.CourseNumEnrolledIn = R.CourseNumEnrolledIn
                                )
        --GROUP BY R.CourseNumEnrolledIn;

        INSERT INTO stage.PreConversionResults (
                                               Reason
                                              ,Solution
                                              ,ExecutionTime
                                               )
                    SELECT 'No student enrolled in Course number ' + CAST(CourseNumber AS NVARCHAR(5)) + ' has an schedule assigned to it.'
                          ,'Please provide an schedule to student ' + CAST(students AS NVARCHAR(5))
                          ,GETDATE()
                    FROM   #StudentWithMissingSchedules;

        ------------------------------------------------------------------------------------
        -- program version with no program
        ------------------------------------------------------------------------------------
        SELECT    P.Name PName, C.CourseCode PVName, C.RecordKey CourseRecKey
		INTO #FlaggedProgramVersion
        FROM      Freedom..PROGSTUDY_DEF P
        LEFT JOIN Freedom..COURSE_DEF C ON C.RecordKey = P.CourseDefVerKey
        WHERE     C.TermDefKey = 0;

		INSERT INTO stage.PreConversionResults (
                                               Reason
                                              ,Solution
                                              ,ExecutionTime
                                               )
                    SELECT 'Program Version: '  + PVName + ' from Program: ' + PName ' has not term definition key.',
					'Please update the termdefkey in Course_DEF for '
                          ,GETDATE()
                    FROM   #FlaggedProgramVersion;


		DROP TABLE #FlaggedProgramVersion
        DROP TABLE #StudentWithMissingSchedules;

    END;

GO
