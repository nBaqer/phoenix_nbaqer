SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 02-02-15>
-- =============================================
CREATE PROCEDURE [dbo].[usp_stage_Email_syUsers]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DECLARE @SchoolId INT;

    SET @SchoolId =
    (
        SELECT SchoolID FROM Freedom..SCHOOL_DEF
    );
    DECLARE @EmailDomain VARCHAR(50) =
            (
                SELECT EmailDomain FROM dbo.SourceDB WHERE SchoolID = @SchoolId
            );
    IF (@EmailDomain IS NULL)
    BEGIN
        SET @EmailDomain = 'noemail.com';
    END;
    -- Create bridge table if not already created in the bridge
    IF NOT EXISTS
    (
        SELECT *
        FROM sys.objects
        WHERE object_id = OBJECT_ID(N'FreedomBridge.stage.syUsers')
              AND type IN ( N'U' )
    )
    BEGIN
        CREATE TABLE FreedomBridge.stage.syUsers
        (
            src_RecordKey INT NOT NULL,
            src_FullName VARCHAR(50) NOT NULL,
            src_Email VARCHAR(50) NOT NULL,
            src_UserName VARCHAR(50) NOT NULL,
            TableName VARCHAR(50) NOT NULL,
            src_SchoolID INT
        --,
        --PRIMARY KEY CLUSTERED
        --    (
        --        src_RecordKey
        --      , src_Email
        --      , TableName
        --      , src_SchoolID
        --    )
        );
    END;

    INSERT INTO FreedomBridge.stage.syUsers
    SELECT Users.RecordKey,
           Users.FullName,
           Users.Email,
           Users.UserName,
           Users.TableName,
           Users.SchoolId
    FROM
    (
        SELECT sUser.UserId AS RecordKey,                                                                                        -- int
               FreedomBridge.dbo.udf_UpperFrstLtrWord(LTRIM(RTRIM(sUser.UserFirstName))) + ' ' + sUser.UserLastName AS FullName, -- varchar(50)
               SUBSTRING(LOWER(RTRIM(LTRIM(sUser.UserFirstName))), 0, 1) + LOWER(RTRIM(LTRIM(sUser.UserLastName)))
               + '@' + @EmailDomain AS Email,                                                                                    -- varchar(50)
               LOWER(sUser.UserName) AS UserName,                                                                                -- varchar(50)
               'SHIBBOLETH_FIL' AS TableName,                                                                                    -- varchar(50)
               @SchoolId AS SchoolId
        FROM Freedom..SHIBBOLETH_FIL sUser
        UNION ALL
        SELECT sRecruit.RecordKey AS RecordKey,                                                                -- int
               FreedomBridge.dbo.udf_UpperFrstLtrWord(ISNULL(
                                                                LTRIM(RTRIM(sRecruit.FirstName)),
                                                                LTRIM(RTRIM(sRecruit.FirstName))
                                                            )
                                                     ) + ' ' + sRecruit.LastName AS FullName,                  -- varchar(50)
               SUBSTRING(LOWER(RTRIM(LTRIM(sRecruit.FirstName))), 0, 1) + LOWER(RTRIM(LTRIM(sRecruit.LastName))) + '@'
               + @EmailDomain AS Email,                                                                        -- varchar(50)
               LOWER(LEFT(LTRIM(RTRIM(sRecruit.FirstName)), 1) + LTRIM(RTRIM(sRecruit.LastName))) AS UserName, -- varchar(50)
               'RECRUITR_FIL' AS TableName,                                                                    -- varchar(50)
               @SchoolId AS SchoolId
        FROM Freedom..RECRUITR_FIL sRecruit
    ) Users
        LEFT JOIN FreedomBridge.stage.syUsers stUsers
            ON stUsers.src_RecordKey = Users.RecordKey
               AND stUsers.src_SchoolID = @SchoolId
               AND stUsers.TableName = Users.TableName
    WHERE Users.UserName NOT IN ( 'Admin', 'Support' )
          AND stUsers.src_RecordKey IS NULL;

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_stage_Email_syUsers]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];

    COMMIT TRANSACTION;

END;
GO
