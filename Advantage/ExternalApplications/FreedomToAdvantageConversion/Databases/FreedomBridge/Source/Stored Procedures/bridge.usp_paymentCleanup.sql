SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [bridge].[usp_paymentCleanup]
AS
    DECLARE @LastName VARCHAR(100);
    DECLARE @FirstName VARCHAR(100);
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PmtTransactionId UNIQUEIDENTIFIER;
    DECLARE @ChargeTransactionId UNIQUEIDENTIFIER;
    DECLARE @UnappliedPmtAmt DECIMAL(18, 2);
    DECLARE @UnappliedChargeAmt DECIMAL(18, 2);
    DECLARE @UnappliedPmtAmtBalance DECIMAL(18, 2);
    DECLARE @UnappliedPmtAmtToApply DECIMAL(18, 2);
    DECLARE @ConversionUser VARCHAR(100);

    SELECT Entity
          ,Value
    INTO   #Settings
    FROM   dbo.udf_GetSettingsList();

    SELECT @ConversionUser = Value
    FROM   #Settings
    WHERE  Entity = 'ConversionUser';



    DECLARE studentsCursor CURSOR FORWARD_ONLY FOR
        SELECT     DISTINCT st.LastName
                           ,st.FirstName
                           ,se.StuEnrollId
        FROM       [FreedomAdvantage]..saTransactions tr
        INNER JOIN [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = tr.StuEnrollId
        INNER JOIN [FreedomAdvantage]..arStudent st ON st.StudentId = se.StudentId
        --WHERE st.FirstName='Danielle'
        --AND st.LastName='Woodhall'
        ORDER BY   st.LastName
                  ,st.FirstName
                  ,se.StuEnrollId;

    OPEN studentsCursor;
    FETCH NEXT FROM studentsCursor
    INTO @LastName
        ,@FirstName
        ,@StuEnrollId;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            --Get unapplied payments/credits for the current StuEnrollId
            DECLARE unappliedPmtsCursor CURSOR FAST_FORWARD FOR
                SELECT   tr.TransactionId
                        ,(( tr.TransAmount * -1 ) - ISNULL((
                                                           SELECT SUM(ap.Amount)
                                                           FROM   [FreedomAdvantage]..saAppliedPayments ap
                                                           WHERE  ap.TransactionId = tr.TransactionId
                                                           )
                                                          ,0
                                                          )
                         ) AS UnappliedAmt
                FROM     [FreedomAdvantage]..saTransactions tr
                WHERE    tr.StuEnrollId = @StuEnrollId
                         --AND tr.TransTypeId=2
                         AND tr.TransAmount < 0.00
                         AND tr.Voided = 0
                ORDER BY tr.TransDate;

            OPEN unappliedPmtsCursor;
            FETCH NEXT FROM unappliedPmtsCursor
            INTO @PmtTransactionId
                ,@UnappliedPmtAmt;

            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @UnappliedPmtAmtBalance = @UnappliedPmtAmt;
                    SET @UnappliedPmtAmtToApply = 0;
                    --Get unapplied charges/debits for the current StuEnrollId
                    DECLARE unappliedChargesCursor CURSOR FAST_FORWARD FOR
                        SELECT   tr.TransactionId
                                ,(( tr.TransAmount ) - ISNULL((
                                                              SELECT SUM(ap.Amount)
                                                              FROM   [FreedomAdvantage]..saAppliedPayments ap
                                                              WHERE  ap.ApplyToTransId = tr.TransactionId
                                                              )
                                                             ,0
                                                             )
                                 ) AS UnappliedAmt
                        FROM     [FreedomAdvantage]..saTransactions tr
                        WHERE    tr.StuEnrollId = @StuEnrollId
                                 --AND tr.TransTypeId=0
                                 AND tr.TransAmount > 0.00
                                 AND tr.Voided = 0
                        ORDER BY tr.TransDate;

                    OPEN unappliedChargesCursor;
                    FETCH NEXT FROM unappliedChargesCursor
                    INTO @ChargeTransactionId
                        ,@UnappliedChargeAmt;

                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            --PRINT @UnappliedChargeAmt
                            IF ( @UnappliedPmtAmtBalance < @UnappliedChargeAmt )
                                BEGIN
                                    SET @UnappliedPmtAmtToApply = @UnappliedPmtAmtBalance;
                                    SET @UnappliedPmtAmtBalance = 0;
                                END;
                            IF @UnappliedPmtAmtBalance = @UnappliedChargeAmt
                                BEGIN
                                    SET @UnappliedPmtAmtToApply = @UnappliedChargeAmt;
                                    SET @UnappliedPmtAmtBalance = 0;
                                END;
                            IF @UnappliedPmtAmtBalance > @UnappliedChargeAmt
                                BEGIN
                                    SET @UnappliedPmtAmtToApply = @UnappliedChargeAmt;
                                    SET @UnappliedPmtAmtBalance = @UnappliedPmtAmtBalance - @UnappliedChargeAmt;
                                END;

                            IF @UnappliedPmtAmtToApply <> 0
                                BEGIN
                                    INSERT INTO [FreedomAdvantage]..saAppliedPayments (
                                                                                      AppliedPmtId
                                                                                     ,TransactionId
                                                                                     ,ApplyToTransId
                                                                                     ,Amount
                                                                                     ,ModDate
                                                                                     ,ModUser
                                                                                      )
                                    VALUES ( NEWID()                 -- AppliedPmtId - uniqueidentifier
                                            ,@PmtTransactionId       -- TransactionId - uniqueidentifier
                                            ,@ChargeTransactionId    -- ApplyToTransId - uniqueidentifier
                                            ,@UnappliedPmtAmtToApply -- Amount - decimal(18, 2)
                                            ,GETDATE()               -- ModDate - datetime
                                            ,@ConversionUser              -- ModUser - varchar(50)
                                        );

                                END;


                            FETCH NEXT FROM unappliedChargesCursor
                            INTO @ChargeTransactionId
                                ,@UnappliedChargeAmt;
                        END;

                    CLOSE unappliedChargesCursor;
                    DEALLOCATE unappliedChargesCursor;


                    FETCH NEXT FROM unappliedPmtsCursor
                    INTO @PmtTransactionId
                        ,@UnappliedPmtAmt;
                END;

            CLOSE unappliedPmtsCursor;
            DEALLOCATE unappliedPmtsCursor;

            FETCH NEXT FROM studentsCursor
            INTO @LastName
                ,@FirstName
                ,@StuEnrollId;
        END;


    CLOSE studentsCursor;
    DEALLOCATE studentsCursor;

    DROP TABLE #Settings;
GO
