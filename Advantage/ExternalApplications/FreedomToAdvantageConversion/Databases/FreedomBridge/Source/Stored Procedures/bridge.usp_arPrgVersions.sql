SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arPrgVersions]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@CampGrpId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';


        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..arDepartments
                      WHERE  DeptCode = 'DEF'
                             AND CampGrpId = @CampusGroupAllId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..arDepartments (
                                                            DeptId
                                                           ,DeptCode
                                                           ,DeptDescrip
                                                           ,CollegeDivId
                                                           ,StatusId
                                                           ,ModUser
                                                           ,ModDate
                                                           ,CampGrpId
                                                            )
                VALUES ( NEWID(), 'DEF', 'Default', 'BCAD9923-1FBC-4AA7-9049-FEAB72BCB267', 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965', 'Support', GETDATE()
                        ,@CampusGroupAllId );
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..arPrgGrp
                      WHERE  PrgGrpCode = 'DEF'
                             AND CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..arPrgGrp (
                                                       PrgGrpId
                                                      ,PrgGrpCode
                                                      ,StatusId
                                                      ,PrgGrpDescrip
                                                      ,ModUser
                                                      ,ModDate
                                                      ,CampGrpId
                                                       )
                VALUES ( NEWID()                                -- PrgGrpId - uniqueidentifier
                        ,'DEF'                                  -- PrgGrpCode - varchar(12)
                        ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                        ,'1500 HR'                              -- PrgGrpDescrip - varchar(50)
                        ,'Support'                              -- ModUser - varchar(50)
                        ,GETDATE()                              -- ModDate - datetime
                        ,@CampGrpId                             -- CampGrpId - uniqueidentifier
                    );
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..arProgTypes
                      WHERE  Code = 'DEF'
                             AND CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..arProgTypes (
                                                          ProgTypId
                                                         ,Code
                                                         ,Description
                                                         ,StatusId
                                                         ,CampGrpId
                                                         ,ModUser
                                                         ,ModDate
                                                         ,IPEDSSequence
                                                         ,IPEDSValue
                                                          )
                VALUES ( NEWID()                                -- ProgTypId - uniqueidentifier
                        ,'DEF'                                  -- Code - varchar(12)
                        ,'Undergraduate'                        -- Description - varchar(50)
                        ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                        ,@CampGrpId                             -- CampGrpId - uniqueidentifier
                        ,'Support'                              -- ModUser - varchar(50)
                        ,GETDATE()                              -- ModDate - datetime
                        ,1                                      -- IPEDSSequence - int
                        ,58                                     -- IPEDSValue - int
                    );
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..arDegrees
                      WHERE  DegreeCode = 'DEF'
                             AND CampGrpId = @CampusGroupAllId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..arDegrees (
                                                        DegreeId
                                                       ,DegreeCode
                                                       ,StatusId
                                                       ,DegreeDescrip
                                                       ,CampGrpId
                                                       ,DegreeTypeId
                                                       ,ModUser
                                                       ,ModDate
                                                       ,IPEDSValue
                                                        )
                VALUES ( NEWID()                                -- DegreeId - uniqueidentifier
                        ,'DEF'                                  -- DegreeCode - varchar(12)
                        ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                        ,'Certificate'                          -- DegreeDescrip - varchar(50)
                        ,@CampusGroupAllId                      -- CampGrpId - uniqueidentifier
                        ,NULL                                   -- DegreeTypeId - uniqueidentifier
                        ,'Support'                              -- ModUser - varchar(50)
                        ,GETDATE()                              -- ModDate - datetime
                        ,144                                    -- IPEDSValue - int
                    );

            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..saTuitionEarnings
                      WHERE  TuitionEarningsCode = 'DEF'
                             AND CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..saTuitionEarnings (
                                                                TuitionEarningId
                                                               ,TuitionEarningsCode
                                                               ,StatusId
                                                               ,TuitionEarningsDescrip
                                                               ,CampGrpId
                                                               ,RevenueBasisIdx
                                                               ,PercentageRangeBasisIdx
                                                               ,PercentToEarnIdx
                                                               ,FullMonthBeforeDay
                                                               ,HalfMonthBeforeDay
                                                               ,IsAttendanceRequired
                                                               ,RequiredAttendancePercent
                                                               ,RequiredCumulativeHoursAttended
                                                               ,ModUser
                                                               ,ModDate
                                                               ,TakeHolidays
                                                               ,RemainingMethod
                                                                )
                VALUES ( NEWID()                                -- TuitionEarningId - uniqueidentifier
                        ,'DEF'                                  -- TuitionEarningsCode - varchar(12)
                        ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                        ,'Freedom Method'                       -- TuitionEarningsDescrip - varchar(50)
                        ,@CampGrpId                             -- CampGrpId - uniqueidentifier
                        ,0                                      -- RevenueBasisIdx - tinyint
                        ,5                                      -- PercentageRangeBasisIdx - tinyint
                        ,0                                      -- PercentToEarnIdx - tinyint
                        ,0                                      -- FullMonthBeforeDay - tinyint
                        ,0                                      -- HalfMonthBeforeDay - tinyint
                        ,0                                      -- IsAttendanceRequired - bit
                        ,0                                      -- RequiredAttendancePercent - tinyint
                        ,0                                      -- RequiredCumulativeHoursAttended - smallint
                        ,'Support'                              -- ModUser - varchar(50)
                        ,GETDATE()                              -- ModDate - datetime
                        ,0                                      -- TakeHolidays - bit
                        ,0                                      -- RemainingMethod - bit
                    );


            END;

        SELECT NEWID() AS PrgVerId
              ,P.*
        INTO   #PrgVersion
        FROM   (
               SELECT      DISTINCT bProg.trg_ProgId AS ProgId                     -- uniqueidentifier
                                   ,sCourse.RecordKey AS PrgVerCode                -- varchar(12)
                                   ,dStatus.StatusId AS StatusId                   -- uniqueidentifier
                                   ,dCampGrp.CampGrpId AS CampGrpId                -- uniqueidentifier
                                   ,dPrgGrp.PrgGrpId AS PrgGrpId                   -- uniqueidentifier
                                   ,LTRIM(RTRIM(sCourse.Name)) AS PrgVerDescrip    -- varchar(50)
                                   ,dDeg.DegreeId AS DegreeId                      -- uniqueidentifier
                                   ,bSAP.trg_SAPId AS SAPId                        -- uniqueidentifier
                                   ,CAST(NULL AS UNIQUEIDENTIFIER) AS ThGrdScaleId -- uniqueidentifier
                                   ,2 AS TestingModelId                            -- tinyint
                                   ,sCourse.PrivProgramWeeks AS Weeks              -- smallint
                                   ,sCourse.NumAcYrPayPrds AS Terms                -- smallint
                                   ,CASE WHEN sCourse.PrivAcCalType = 5 THEN sCourse.PrivLength
                                         ELSE 0
                                    END AS Hours                                   -- decimal
                                   ,CASE WHEN sCourse.PrivAcCalType != 5 THEN sCourse.PrivLength
                                         ELSE 0
                                    END AS Credits                                 -- decimal
                                   ,0 AS LTHalfTime                                -- smallint
                                   ,0 AS HalfTime                                  -- smallint
                                   ,0 AS ThreeQuartTime                            -- smallint
                                   ,0 AS FullTime                                  -- bit
                                   ,dDept.DeptId AS DeptId                         -- uniqueidentifier
                                   ,bGrdSystem.trg_GradeSystemId AS GrdSystemId    -- uniqueidentifier
                                   ,NULL AS [Weighted GPA]                         -- bit
                                   ,dBillMethod.BillingMethodId AS BillingMethodId -- uniqueidentifier
                                   ,dTuitEarn.TuitionEarningId AS TuitionEarningId -- uniqueidentifier
                                   ,'sa' AS ModUser                                -- varchar(50)
                                   ,GETDATE() AS ModDate                           -- datetime
                                   ,NULL AS AttendanceLevel                        -- bit
                                   ,NULL AS CustomAttendance                       -- bit
                                   ,dProgType.ProgTypId AS ProgTypId               -- uniqueidentifier
                                   ,0 AS IsContinuingEd                            -- bit
                                   ,bAttUnit.trg_UnitTypeId AS UnitTypeId          -- uniqueidentifier
                                   ,sCourse.TrackTardies AS TrackTardies           -- bit
                                   ,sCourse.NumTardies AS TardiesMakingAbsence     -- int
                                   ,4 AS SchedMethodId                             -- int
                                   ,sCourse.UsesTimeClock AS UseTimeClock          -- bit
                                   ,0 AS TotalCost                                 -- decimal
                                   ,sCourse.PrivAcYear AS AcademicYearLength       -- decimal
                                   ,sCourse.PrivAcYrWeeks AS AcademicYearWeeks     -- smallint
                                   ,sCourse.NumAcYrPayPrds AS PayPeriodPerAcYear   -- smallint
                                   ,CAST(NULL AS UNIQUEIDENTIFIER) AS FASAPId      -- uniqueidentifier
                                   ,0 AS IsBillingMethodCharged                    -- bit
                                   ,CASE WHEN EXISTS (
                                                     SELECT 1
                                                     FROM   Freedom..SUBJECT_DEF S
                                                     WHERE  S.CourseDefKey = sCourse.RecordKey
                                                            AND S.Weight > 1
                                                     )
                                              OR EXISTS (
                                                        SELECT    1
                                                        FROM      Freedom..WORKUNIT_DEF WD
                                                        LEFT JOIN Freedom..SUBJECT_DEF S ON WD.SubjectDefKey = S.RecordKey
                                                        WHERE     S.CourseDefKey = sCourse.RecordKey
                                                                  AND WD.Weight > 1
                                                        ) THEN 1
                                         ELSE 0
                                    END AS DoCourseWeightOverallGPA
               FROM        Freedom..COURSE_DEF sCourse
               LEFT JOIN   FreedomBridge.bridge.arPrograms bProg ON bProg.src_RecordKey = sCourse.ParentKey
                                                                    AND bProg.trg_CampusId = @CampusId
               LEFT JOIN   FreedomBridge.bridge.arSAP bSAP ON bSAP.src_RecordKey = sCourse.SapPolicyKey
                                                              AND bSAP.trg_CampusId = @CampusId
               INNER JOIN  FreedomBridge.bridge.arAttUnitType bAttUnit ON bAttUnit.src_AttendanceTypeID = sCourse.PrivAttendType
               LEFT JOIN   FreedomBridge.bridge.arGradeSystems bGrdSystem ON bGrdSystem.src_Description = CASE WHEN sCourse.GradeScaleKey = 0 THEN 'Default'
                                                                                                               WHEN bGrdSystem.src_RecordKey = sCourse.GradeScaleKey THEN
                                                                                                                   bGrdSystem.src_Description
                                                                                                          END
                                                                             AND bGrdSystem.trg_CampusId = @CampusId
               LEFT JOIN   FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpId = @CampusGroupAllId
               INNER JOIN  FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
               INNER JOIN  FreedomAdvantage..arProgTypes dProgType ON dProgType.Code = 'DEF'
                                                                      AND dProgType.CampGrpId = @CampGrpId
               INNER JOIN  FreedomAdvantage..arDegrees dDeg ON dDeg.DegreeCode = 'DEF'
               INNER JOIN  FreedomAdvantage..saTuitionEarnings dTuitEarn ON dTuitEarn.TuitionEarningsCode = 'DEF'
                                                                            AND dTuitEarn.CampGrpId = @CampGrpId
               INNER JOIN  FreedomAdvantage..saBillingMethods dBillMethod ON dBillMethod.BillingMethodCode = 'CP'
               LEFT JOIN   FreedomAdvantage..arDepartments dDept ON dDept.DeptCode = 'DEF'
                                                                    AND dDept.CampGrpId = @CampusGroupAllId
               LEFT JOIN   FreedomAdvantage..arPrgGrp dPrgGrp ON dPrgGrp.PrgGrpCode = 'DEF'
                                                                 AND dPrgGrp.CampGrpId = @CampGrpId
               OUTER APPLY (
                           SELECT TOP 1 bPrgVer1.trg_PrgVerId
                           FROM   bridge.arPrgVersions bPrgVer1
                           WHERE  bPrgVer1.src_Name = sCourse.Name
                                  AND bPrgVer1.src_RecordKey = sCourse.RecordKey
                                  AND bPrgVer1.trg_CampusId = @CampusId
                           ) bPrgVer
               WHERE       sCourse.Approved = 1
                           OR EXISTS (
                                     SELECT TOP 1 1
                                     FROM   Freedom..STUDREC PVE
                                     WHERE  PVE.CourseNumEnrolledIn = sCourse.RecordKey
                                     )
               ) AS P;


        INSERT INTO FreedomBridge.bridge.arPrgVersions
                    SELECT PrgVerId
                          ,tPrgVer.PrgVerDescrip
                          ,tPrgVer.PrgVerCode
                          ,@CampusId AS CampusId
                    FROM   #PrgVersion tPrgVer;



        ALTER TABLE FreedomAdvantage..arPrgVersions DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arPrgVersions
                    SELECT    tPrgVer.PrgVerId
                             ,tPrgVer.ProgId
                             ,tPrgVer.PrgVerCode
                             ,tPrgVer.StatusId
                             ,tPrgVer.CampGrpId
                             ,tPrgVer.PrgGrpId
                             ,tPrgVer.PrgVerDescrip
                             ,tPrgVer.DegreeId
                             ,tPrgVer.SAPId
                             ,tPrgVer.ThGrdScaleId
                             ,tPrgVer.TestingModelId
                             ,tPrgVer.Weeks
                             ,tPrgVer.Terms
                             ,tPrgVer.Hours
                             ,tPrgVer.Credits
                             ,tPrgVer.LTHalfTime
                             ,tPrgVer.HalfTime
                             ,tPrgVer.ThreeQuartTime
                             ,tPrgVer.FullTime
                             ,tPrgVer.DeptId
                             ,tPrgVer.GrdSystemId
                             ,tPrgVer.[Weighted GPA]
                             ,tPrgVer.BillingMethodId
                             ,tPrgVer.TuitionEarningId
                             ,tPrgVer.ModUser
                             ,tPrgVer.ModDate
                             ,tPrgVer.AttendanceLevel
                             ,tPrgVer.CustomAttendance
                             ,tPrgVer.ProgTypId
                             ,tPrgVer.IsContinuingEd
                             ,tPrgVer.UnitTypeId
                             ,tPrgVer.TrackTardies
                             ,tPrgVer.TardiesMakingAbsence
                             ,tPrgVer.SchedMethodId
                             ,tPrgVer.UseTimeClock
                             ,tPrgVer.TotalCost
                             ,tPrgVer.AcademicYearLength
                             ,tPrgVer.AcademicYearWeeks
                             ,tPrgVer.PayPeriodPerAcYear
                             ,tPrgVer.FASAPId
                             ,tPrgVer.IsBillingMethodCharged
                             ,tPrgVer.DoCourseWeightOverallGPA --course weight gpa
                             ,1                                -- program registration type
                    FROM      #PrgVersion tPrgVer
                    LEFT JOIN FreedomAdvantage..arPrgVersions dPrgVer ON dPrgVer.PrgVerId = tPrgVer.PrgVerId
                    WHERE     dPrgVer.PrgVerDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..arPrgVersions ENABLE TRIGGER ALL;

        DROP TABLE #PrgVersion;
        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arPrgVersions]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
