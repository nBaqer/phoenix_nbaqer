SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-21-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arQuantMinUnitTyps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN FLook.LookUpID = 702 THEN 3
                    WHEN FLook.LookUpID = 703 THEN 1
                    WHEN FLook.LookUpID = 704 THEN 0
                    ELSE NULL
               END AS QuantMinUnitTypId             -- tinyint
             , FLook.Description AS QuantMinTypDesc -- varchar(50)
             , CASE WHEN FLook.LookUpID = 702 THEN 1
                    WHEN FLook.LookUpID = 703 THEN 2
                    WHEN FLook.LookUpID = 704 THEN 0
                    ELSE NULL
               END AS ID
        INTO   #QtyMinUnitTyp
        FROM   FreedomBridge.stage.FLookUp FLook
        LEFT JOIN FreedomAdvantage.dbo.arQuantMinUnitTyps dQtyMinUnitTyp ON dQtyMinUnitTyp.QuantMinUnitTypId = CASE WHEN FLook.LookUpID = 702 THEN 3
                                                                                                                    WHEN FLook.LookUpID = 703 THEN 1
                                                                                                                    WHEN FLook.LookUpID = 704 THEN 0
                                                                                                                    ELSE 0
                                                                                                               END
        WHERE  FLook.LookupName = 'SAPQuantMinUnitType';

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arQuantMinUnitTyps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arQuantMinUnitTyps
                    (
                        trg_QuantMinUnitTypId TINYINT
                      , src_SAPQuantMinUnitTypeDescription VARCHAR(255)
                      , src_SAPQuantMinUnitTypeID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_SAPQuantMinUnitTypeDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arQuantMinUnitTyps
                    SELECT tQtyMinUnitTyp.QuantMinUnitTypId
                         , tQtyMinUnitTyp.QuantMinTypDesc
                         , tQtyMinUnitTyp.ID
                    FROM   #QtyMinUnitTyp tQtyMinUnitTyp
                    LEFT JOIN FreedomBridge.bridge.arQuantMinUnitTyps bTrigOff ON bTrigOff.src_SAPQuantMinUnitTypeDescription = tQtyMinUnitTyp.QuantMinTypDesc
                                                                                  AND bTrigOff.src_SAPQuantMinUnitTypeID = tQtyMinUnitTyp.ID
                    WHERE  bTrigOff.trg_QuantMinUnitTypId IS NULL;

        DROP TABLE #QtyMinUnitTyp;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arQuantMinUnitTyps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
