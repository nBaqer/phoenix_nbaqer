SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-07-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adAdminCriteria]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dAdminCriteria.Descrip IS NULL THEN NEWID()
                    ELSE dAdminCriteria.admincriteriaid
               END AS admincriteriaid                                              -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS Code -- varchar(50)
             , FLook.Description AS Descrip                                        -- varchar(50)
             , dStatus.StatusId AS StatusId                                        -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId                                     -- uniqueidentifier
             , GETDATE() AS ModDate                                                -- datetime
             , 'sa' AS ModUser                                                     -- varchar(50)
             , FLook.ID
        INTO   #AdminCriteria
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adAdminCriteria dAdminCriteria ON dAdminCriteria.CampGrpId = dCampGrp.CampGrpId
                                                                      AND dAdminCriteria.Descrip = FLook.Description
        WHERE  FLook.LookupName = 'AdmissionCriteria';

        ALTER TABLE FreedomAdvantage..adAdminCriteria DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adAdminCriteria
                    SELECT tAdminCriteria.admincriteriaid
                         , tAdminCriteria.Code
                         , tAdminCriteria.Descrip
                         , tAdminCriteria.StatusId
                         , tAdminCriteria.CampGrpId
                         , tAdminCriteria.ModDate
                         , tAdminCriteria.ModUser
                    FROM   #AdminCriteria tAdminCriteria
                    LEFT JOIN FreedomAdvantage..adAdminCriteria dAdminCriteria ON dAdminCriteria.CampGrpId = tAdminCriteria.CampGrpId
                                                                                  AND dAdminCriteria.Descrip = tAdminCriteria.Descrip
                    WHERE  dAdminCriteria.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..adAdminCriteria ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adAdminCriteria')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adAdminCriteria
                    (
                        trg_AdminCriteriaId UNIQUEIDENTIFIER
                      , src_AdmissionsCriteriaDescription VARCHAR(255)
                      , src_AdmissionsCriteriaID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_AdmissionsCriteriaDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adAdminCriteria
                    SELECT tAdminCriteria.admincriteriaid
                         , tAdminCriteria.Descrip
                         , tAdminCriteria.ID
                    FROM   #AdminCriteria tAdminCriteria
                    LEFT JOIN FreedomBridge.bridge.adAdminCriteria bAdminCriteria ON bAdminCriteria.src_Description = tAdminCriteria.Descrip
                                                                                     AND bAdminCriteria.src_ID = tAdminCriteria.ID
                    WHERE  bAdminCriteria.trg_AdminCriteriaId IS NULL;

        DROP TABLE #AdminCriteria;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adAdminCriteria]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun] 
        COMMIT TRANSACTION;

    END;
GO
