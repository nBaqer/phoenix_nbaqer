SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arStudentTimeClockPunches]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        ALTER TABLE [FreedomAdvantage]..arStudentTimeClockPunches DISABLE TRIGGER ALL;

        INSERT INTO [FreedomAdvantage]..arStudentTimeClockPunches (
                                                                  BadgeId
                                                                 ,PunchTime
                                                                 ,PunchType
                                                                 ,ClockId
                                                                 ,Status
                                                                 ,StuEnrollId
                                                                 ,FromSystem
                                                                 ,ClsSectMeetingID
                                                                 ,SpecialCode
                                                                  )
                    SELECT *
                    FROM   (
                           SELECT    L.StudentNumber AS BadgeId
                                    ,DATEADD(
                                                HOUR
                                               ,( CAST(LEFT(CASE WHEN LEN(CAST(TC.PunchIn * 100 AS VARCHAR(4))) = 3 THEN '0' + CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                                WHEN LEN(CAST(TC.PunchIn * 100 AS VARCHAR(4))) = 2 THEN '00' + CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                                ELSE CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                           END, 2) AS INT)
                                               )
                                               ,DATEADD(
                                                           MINUTE
                                                          ,( CAST(RIGHT(CASE WHEN LEN(CAST(TC.PunchIn * 100 AS VARCHAR(4))) = 3 THEN
                                                                                '0' + CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                                            WHEN LEN(CAST(TC.PunchIn * 100 AS VARCHAR(4))) = 2 THEN
                                                                                '00' + CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                                            ELSE CAST(TC.PunchIn * 100 AS VARCHAR(4))
                                                                       END, 2) AS INT)
                                                          )
                                                          ,TC.Date
                                                       )
                                            ) AS PunchTime
                                    ,1 AS PunchType
                                    ,'FC00' AS ClockId
                                    ,1 AS Status
                                    ,E.StuEnrollId AS StuEnrollId
                                    ,CAST(NULL AS BIT) AS FromSystem
                                    ,CAST(NULL AS UNIQUEIDENTIFIER) AS ClsSectMeetingID
                                    ,NULL AS SepcialCode
                           FROM      [Freedom]..TIMECLOCK TC
                           LEFT JOIN bridge.arStuEnrollments BE ON BE.src_RecordKey = TC.StudRecKey
                           LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = BE.trg_StuEnrollId
                           LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = E.LeadId
                           WHERE     Date IS NOT NULL
                           UNION
                           SELECT    L.StudentNumber AS BadgeId
                                    ,DATEADD(
                                                HOUR
                                               ,( CAST(LEFT(CASE WHEN LEN(CAST(TC.PunchOut * 100 AS VARCHAR(4))) = 3 THEN '0' + CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                                WHEN LEN(CAST(TC.PunchOut * 100 AS VARCHAR(4))) = 2 THEN
                                                                    '00' + CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                                ELSE CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                           END, 2) AS INT)
                                               )
                                               ,DATEADD(
                                                           MINUTE
                                                          ,( CAST(RIGHT(CASE WHEN LEN(CAST(TC.PunchOut * 100 AS VARCHAR(4))) = 3 THEN
                                                                                '0' + CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                                            WHEN LEN(CAST(TC.PunchOut * 100 AS VARCHAR(4))) = 2 THEN
                                                                                '00' + CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                                            ELSE CAST(TC.PunchOut * 100 AS VARCHAR(4))
                                                                       END, 2) AS INT)
                                                          )
                                                          ,TC.Date
                                                       )
                                            ) AS PunchTime
                                    ,2 AS PunchType
                                    ,'FC00' AS ClockId
                                    ,1 AS Status
                                    ,E.StuEnrollId AS StuEnrollId
                                    ,CAST(NULL AS BIT) AS FromSystem
                                    ,CAST(NULL AS UNIQUEIDENTIFIER) AS ClsSectMeetingID
                                    ,NULL AS SepcialCode
                           FROM      [Freedom]..TIMECLOCK TC
                           LEFT JOIN bridge.arStuEnrollments BE ON BE.src_RecordKey = TC.StudRecKey
                           LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = BE.trg_StuEnrollId
                           LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = E.LeadId
                           WHERE     Date IS NOT NULL
                           ) AS Punches
                    WHERE  Punches.StuEnrollId IS NOT NULL;


        ALTER TABLE [FreedomAdvantage]..arStudentTimeClockPunches ENABLE TRIGGER ALL;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,OBJECT_NAME(@@PROCID)
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;
    END;


GO
