SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-16-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syPeriods]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS PeriodId
              ,T.*
        INTO   #Period
        FROM   (
               SELECT     DISTINCT '9-5 M-U' AS PeriodCode                   -- varchar(12)
                                  ,dStat.StatusId AS StatusId                -- uniqueidentifier
                                  ,'(9:00 - 17:00) MTWRFSU' AS PeriodDescrip -- varchar(50)
                                  ,dCampGrp.CampGrpId AS CampGrpId           -- uniqueidentifier
                                  ,dStartTime.TimeIntervalId AS StartTimeId  -- uniqueidentifier
                                  ,dEndTime.TimeIntervalId AS EndTimeId      -- uniqueidentifier
                                  ,'sa' AS ModUser                           -- varchar(50)
                                  ,GETDATE() AS ModDate                      -- datetime
                                  ,0 AS StartTimeAndEndTimeAreFixed          -- bit
                                  ,1 AS MonView
                                  ,2 AS TueView
                                  ,3 AS WedView
                                  ,4 AS ThrView
                                  ,5 AS FriView
                                  ,6 AS SatView
                                  ,0 AS SunView
                                  ,1 AS ScheduleId
                                  ,56 AS Hours
               FROM       FreedomAdvantage.dbo.cmTimeInterval dStartTime
               INNER JOIN FreedomAdvantage.dbo.cmTimeInterval dEndTime ON dEndTime.TimeIntervalDescrip = '1899-12-30 17:00:00.000'
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStat ON dStat.Status = 'Active'
               INNER JOIN FreedomAdvantage.dbo.syCampGrps dCampGrp ON dCampGrp.CampGrpDescrip = 'ALL'
               LEFT JOIN  FreedomAdvantage.dbo.syPeriods dPeriod ON dPeriod.PeriodDescrip = '(9:00 - 17:00) MTWRFSU'
                                                                    AND dPeriod.PeriodCode = '9-5 M-U'
               WHERE      dStartTime.TimeIntervalDescrip = '1899-12-30 09:00:00.000'
               ) T;
        --AND dPeriod.PeriodId IS NULL

        INSERT INTO FreedomAdvantage.dbo.syPeriods
                    SELECT    tPeriod.PeriodId AS PeriodId                                       -- uniqueidentifier
                             ,tPeriod.PeriodCode AS PeriodCode                                   -- varchar(12)
                             ,tPeriod.StatusId AS StatusId                                       -- uniqueidentifier
                             ,tPeriod.PeriodDescrip AS PeriodDescrip                             -- varchar(50)
                             ,tPeriod.CampGrpId AS CampGrpId                                     -- uniqueidentifier
                             ,tPeriod.StartTimeId AS StartTimeId                                 -- uniqueidentifier
                             ,tPeriod.EndTimeId AS EndTimeId                                     -- uniqueidentifier
                             ,tPeriod.ModUser AS ModUser                                         -- varchar(50)
                             ,tPeriod.ModDate AS ModDate                                         -- datetime
                             ,tPeriod.StartTimeAndEndTimeAreFixed AS StartTimeAndEndTimeAreFixed -- bit
                    FROM      #Period tPeriod
                    LEFT JOIN FreedomAdvantage.dbo.syPeriods dPeriod ON dPeriod.PeriodId = tPeriod.PeriodId
                    WHERE     dPeriod.PeriodId IS NULL;

        INSERT INTO FreedomBridge.bridge.syPeriods
                    SELECT tPeriod.PeriodId
                          ,tPeriod.PeriodCode
                          ,tPeriod.ScheduleId
                          ,tPeriod.Hours
                          ,tPeriod.SunView
                          ,tPeriod.MonView
                          ,tPeriod.TueView
                          ,tPeriod.WedView
                          ,tPeriod.ThrView
                          ,tPeriod.FriView
                          ,tPeriod.SatView
                          ,@CampusId AS CampusId
                    FROM   #Period tPeriod;

        DROP TABLE #Settings;
        DROP TABLE #Period;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_syPeriods]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
