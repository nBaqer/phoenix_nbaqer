SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saAcademicYears]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        INSERT INTO FreedomAdvantage..saAcademicYears
                    SELECT NEWID() AS AcademicYearId                                                                    -- uniqueidentifier
                         , YearCode AS AcademicYearCode                                                                 -- varchar(12)
                         , dStatus.StatusId AS StatusId                                                                 -- uniqueidentifier
                         , CAST(YearCode AS VARCHAR(4)) + '/' + CAST(YearCode + 1 AS VARCHAR(4)) AS AcademicYearDescrip -- varchar(50)
                         , dCampGrp.CampGrpId AS CampGrpId                                                              -- uniqueidentifierINSERT INTO [dbo].[saAcademicYears]
                         , 'sa' AS ModUser                                                                              -- varchar(50)
                         , GETDATE() AS ModDate                                                                         -- datetime
                    FROM   (
                               VALUES ( 2001 )
                                    , ( 2002 )
                                    , ( 2003 )
                                    , ( 2004 )
                                    , ( 2005 )
                                    , ( 2006 )
                                    , ( 2007 )
                                    , ( 2008 )
                                    , ( 2009 )
                                    , ( 2010 )
                                    , ( 2011 )
                                    , ( 2012 )
                                    , ( 2013 )
                                    , ( 2014 )
                                    , ( 2015 )
                                    , ( 2016 )
                                    , ( 2017 )
                                    , ( 2018 )
                                    , ( 2019 )
                                    , ( 2020 )
                                    , ( 2021 )
                           ) AS VAL ( YearCode )
                    LEFT JOIN FreedomAdvantage..saAcademicYears dYear ON dYear.AcademicYearCode = YearCode
                    INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
                    INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                    WHERE  dYear.AcademicYearId IS NULL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saAcademicYears]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
