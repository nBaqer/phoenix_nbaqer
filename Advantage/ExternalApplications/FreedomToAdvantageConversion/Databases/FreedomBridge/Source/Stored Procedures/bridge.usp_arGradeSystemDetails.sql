SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-04-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGradeSystemDetails]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';
        WITH CTE_GrdSclDtl
        AS ( SELECT gsd.RecordKey
                  , gs.RecordKey AS ParentKey
                  , gsd.NextRec
                  , gsd.RangeStart AS MinGradeValue
                  , gsd.PrivPoints
                  , gsd.Grade
                  , gs.MaxResult
             FROM   Freedom..GRADESCALE_DTL gsd
             INNER JOIN Freedom..GRADESCALE_FIL gs ON gs.DetailKey = gsd.RecordKey
             UNION ALL
             SELECT gsd_Child.RecordKey
                  , gs_Parent.ParentKey
                  , gsd_Child.NextRec
                  , gsd_Child.RangeStart AS MinGradeValue
                  , gsd_Child.PrivPoints
                  , gsd_Child.Grade
                  , gs_Parent.MaxResult
             FROM   Freedom..GRADESCALE_DTL gsd_Child
             INNER JOIN CTE_GrdSclDtl gs_Parent ON gs_Parent.NextRec = gsd_Child.RecordKey
           )
        SELECT   *
        INTO     #GrdSclDtl
        FROM     CTE_GrdSclDtl
        ORDER BY CTE_GrdSclDtl.ParentKey;

        SELECT   gsd1.RecordKey
               , gsd1.ParentKey
               , gsd1.NextRec
               , gsd1.MinGradeValue
               , CASE WHEN gsd2.MinGradeValue IS NULL THEN gsd1.MaxResult
                      ELSE gsd2.MinGradeValue - 1
                 END AS MaxGradeValue
               , CAST(gsd1.PrivPoints / 100.00 AS DECIMAL(8, 2)) AS PrivPoints
               , LTRIM(RTRIM(gsd1.Grade)) AS Grade
        INTO     #GrdSclDtlTmp
        FROM     #GrdSclDtl gsd1
        LEFT JOIN #GrdSclDtl gsd2 ON gsd2.RecordKey = gsd1.NextRec
        ORDER BY gsd1.ParentKey
               , gsd1.RecordKey;

        SELECT COALESCE(dGrdSysDtl.GrdSysDetailId, NEWID()) AS GrdSysDetailId -- uniqueidentifier)
             , Detail.GrdSystemId
             , Detail.Grade
             , Detail.IsPass
             , Detail.GPA
             , Detail.IsCreditsEarned
             , Detail.IsCreditsAttempted
             , Detail.IsInGPA
             , Detail.IsInSAP
             , Detail.IsTransferGrade
             , Detail.IsIncomplete
             , Detail.IsDefault
             , Detail.IsDrop
             , Detail.ViewOrder
             , Detail.ShowInstructor
             , Detail.IsSatisfactory
             , Detail.IsUnSatisfactory
             , Detail.IsInPass
             , Detail.IsInFail
             , Detail.ModUser
             , Detail.ModDate
             , Detail.GradeDescription
             , Detail.quality
             , Detail.IsCreditsAwarded
             , Detail.MinGradeValue
             , Detail.MaxGradeValue
             , Detail.RecordKey
             , Detail.ParentKey
        INTO   #GrdSclDetail
        /****** Use staging table provided by client to gather grade system detail information to insert into arGradeSystemDetails table ******/
        FROM   (
                   SELECT bGrdSys.trg_GradeSystemId AS GrdSystemId                   -- uniqueidentifier
                        , stGrdDtl.Grade AS Grade                                    -- char(10)
                        , stGrdDtl.IsPass AS IsPass                                  -- bit
                        , stGrdDtl.GPA AS GPA                                        -- decimal
                        , stGrdDtl.IsCreditsEarned AS IsCreditsEarned                -- bit
                        , stGrdDtl.IsCreditsAttempted AS IsCreditsAttempted          -- bit
                        , stGrdDtl.IsInGPA AS IsInGPA                                -- bit
                        , stGrdDtl.IsInSAP AS IsInSAP                                -- bit
                        , stGrdDtl.IsTransferGrade AS IsTransferGrade                -- bit
                        , stGrdDtl.IsIncomplete AS IsIncomplete                      -- bit
                        , 0 AS IsDefault                                             -- bit
                        , stGrdDtl.IsDrop AS IsDrop                                  -- bit
                        , ROW_NUMBER() OVER ( ORDER BY stGrdDtl.Grade ) AS ViewOrder -- int
                        , NULL AS ShowInstructor                                     -- bit
                        , NULL AS IsSatisfactory                                     -- bit
                        , NULL AS IsUnSatisfactory                                   -- bit
                        , NULL AS IsInPass                                           -- bit
                        , NULL AS IsInFail                                           -- bit
                        , 'sa' AS ModUser                                            -- varchar(50)
                        , GETDATE() AS ModDate                                       -- datetime
                        , NULL AS GradeDescription                                   -- varchar(50)
                        , NULL AS quality                                            -- varchar(50)
                        , 0 AS IsCreditsAwarded                                      -- bit
                        , stGrdDtl.MinGradeValue
                        , stGrdDtl.MaxGradeValue
                        , @SchoolID AS RecordKey
                        , NULL AS ParentKey
                   FROM   FreedomBridge.stage.arGradeSystemDetails stGrdDtl
                   LEFT JOIN FreedomBridge.bridge.arGradeSystems bGrdSys ON bGrdSys.src_Description = stGrdDtl.GradeSystemDescription
                                                                            AND bGrdSys.trg_CampusId = @CampusId
                   LEFT JOIN FreedomAdvantage..arGradeSystemDetails dGrdSysDtl ON dGrdSysDtl.Grade = stGrdDtl.Grade
                                                                                  AND dGrdSysDtl.GPA = stGrdDtl.GPA
                                                                                  AND dGrdSysDtl.GrdSystemId = bGrdSys.trg_GradeSystemId
                   UNION ALL
                   /****** Use Freedom grade scales if they exist to populate grade system details *******/
                   SELECT   bGrdSys.trg_GradeSystemId AS GrdSystemId -- uniqueidentifier
                          , tGrdSclDtl.Grade AS Grade                -- char(10)
                          , CASE WHEN tGrdSclDtl.Grade = 'F' THEN 0
                                 ELSE 1
                            END AS IsPass                            -- bit
                          , tGrdSclDtl.PrivPoints AS GPA             -- decimal
                          , CASE WHEN tGrdSclDtl.MinGradeValue >= MAX(sSub.MinResult) THEN 1
                                 ELSE 0
                            END AS IsCreditsEarned                   -- bit
                          , 1 AS IsCreditsAttempted                  -- bit
                          , 1 AS IsInGPA                           -- bit
                          , 1 AS IsInSAP                           -- bit
                          , CASE WHEN tGrdSclDtl.MinGradeValue >= MAX(sSub.MinResult) THEN 1
                                 ELSE 0
                            END AS IsTransferGrade                   -- bit
                          , 0 AS IsIncomplete                        -- bit
                          , 0 IsDefault                              -- bit
                          , 0 AS IsDrop                              -- bit
                          , ROW_NUMBER() OVER ( PARTITION BY tGrdSclDtl.ParentKey
                                                ORDER BY tGrdSclDtl.Grade
                                              ) AS ViewOrder         -- int
                          , NULL AS ShowInstructor                   -- bit
                          , NULL AS IsSatisfactory                   -- bit
                          , NULL AS IsUnSatisfactory                 -- bit
                          , NULL AS IsInPass                         -- bit
                          , NULL AS IsInFail                         -- bit
                          , 'sa' AS ModUser                          -- varchar(50)
                          , GETDATE() AS ModDate                     -- datetime
                          , NULL AS GradeDescription                 -- varchar(50)
                          , NULL AS quality                          -- varchar(50)
                          , 0 AS IsCreditsAwarded                    -- bit
                          , tGrdSclDtl.MinGradeValue AS MinGradeValue
                          , tGrdSclDtl.MaxGradeValue AS MaxGradeValue
                          , tGrdSclDtl.RecordKey
                          , tGrdSclDtl.ParentKey
                   FROM     #GrdSclDtlTmp tGrdSclDtl
                   INNER JOIN FreedomBridge.bridge.arGradeSystems bGrdSys ON bGrdSys.src_RecordKey = tGrdSclDtl.ParentKey
                                                                             AND bGrdSys.trg_CampusId = @CampusId
                   LEFT JOIN Freedom..COURSE_DEF sCourse ON sCourse.GradeScaleKey = tGrdSclDtl.ParentKey
                   LEFT JOIN Freedom..SUBJECT_DEF sSub ON sSub.CourseDefKey = sCourse.RecordKey
                   GROUP BY bGrdSys.trg_GradeSystemId
                          , tGrdSclDtl.Grade
                          , tGrdSclDtl.PrivPoints
                          , tGrdSclDtl.MinGradeValue
                          , tGrdSclDtl.MaxGradeValue
                          , tGrdSclDtl.RecordKey
                          , tGrdSclDtl.ParentKey
               ) Detail
        LEFT JOIN FreedomAdvantage..arGradeSystemDetails dGrdSysDtl ON dGrdSysDtl.Grade = Detail.Grade
                                                                       AND dGrdSysDtl.GPA = Detail.GPA
                                                                       AND dGrdSysDtl.ViewOrder = Detail.ViewOrder
                                                                       AND dGrdSysDtl.GrdSystemId = Detail.GrdSystemId;

        ALTER TABLE FreedomAdvantage..arGradeSystemDetails DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGradeSystemDetails
                    SELECT tGrdSclDtl.GrdSysDetailId AS GrdSysDetailId         -- uniqueidentifier
                         , tGrdSclDtl.GrdSystemId AS GrdSystemId               -- uniqueidentifier
                         , tGrdSclDtl.Grade AS Grade                           -- char(10)
                         , tGrdSclDtl.IsPass AS IsPass                         -- bit
                         , tGrdSclDtl.GPA AS GPA                               -- decimal
                         , tGrdSclDtl.IsCreditsEarned AS IsCreditsEarned       -- bit
                         , tGrdSclDtl.IsCreditsAttempted AS IsCreditsAttempted -- bit
                         , tGrdSclDtl.IsInGPA AS IsInGPA                       -- bit
                         , tGrdSclDtl.IsInSAP AS IsInSAP                       -- bit
                         , tGrdSclDtl.IsTransferGrade AS IsTransferGrade       -- bit
                         , tGrdSclDtl.IsIncomplete AS IsIncomplete             -- bit
                         , tGrdSclDtl.IsDefault AS IsDefault                   -- bit
                         , tGrdSclDtl.IsDrop AS IsDrop                         -- bit
                         , tGrdSclDtl.ViewOrder AS ViewOrder                   -- int
                         , tGrdSclDtl.ShowInstructor AS ShowInstructor         -- bit
                         , tGrdSclDtl.IsSatisfactory AS IsSatisfactory         -- bit
                         , tGrdSclDtl.IsUnSatisfactory AS IsUnSatisfactory     -- bit
                         , tGrdSclDtl.IsInPass AS IsInPass                     -- bit
                         , tGrdSclDtl.IsInFail AS IsInFail                     -- bit
                         , tGrdSclDtl.ModUser AS ModUser                       -- varchar(50)
                         , tGrdSclDtl.ModDate AS ModDate                       -- datetime
                         , tGrdSclDtl.GradeDescription AS GradeDescription     -- varchar(50)
                         , tGrdSclDtl.quality AS quality                       -- varchar(50)
                         , tGrdSclDtl.IsCreditsAwarded AS IsCreditsAwarded     -- bit
                    FROM   #GrdSclDetail tGrdSclDtl
                    LEFT JOIN FreedomAdvantage..arGradeSystemDetails dGrdSysDtl ON dGrdSysDtl.Grade = tGrdSclDtl.Grade
                                                                                   AND dGrdSysDtl.GPA = tGrdSclDtl.GPA
                                                                                   AND dGrdSysDtl.ViewOrder = tGrdSclDtl.ViewOrder
                                                                                   AND dGrdSysDtl.GrdSystemId = tGrdSclDtl.GrdSystemId
                    WHERE  dGrdSysDtl.GrdSysDetailId IS NULL;

        ALTER TABLE FreedomAdvantage..arGradeSystemDetails ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge

        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGradeSystemDetails')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGradeSystemDetails
                    (
                        trg_GrdSystemDetailId UNIQUEIDENTIFIER NULL
                      , trg_GrdSystemId UNIQUEIDENTIFIER NOT NULL
                      , src_ParentKey INT NULL
                      , src_RecordKey INT NULL
                      , src_Grade CHAR(10) NOT NULL
                      , src_GPA DECIMAL(8, 2) NOT NULL
                      , src_MinGradeValue INT NOT NULL
                      , src_MaxGradeValue INT NOT NULL
                      , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_GrdSystemId
                            , src_Grade
                            , src_GPA
                            , src_MinGradeValue
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGradeSystemDetails
                    SELECT tGrdSclDtl.GrdSysDetailId
                         , tGrdSclDtl.GrdSystemId
                         , tGrdSclDtl.ParentKey
                         , tGrdSclDtl.RecordKey
                         , tGrdSclDtl.Grade
                         , tGrdSclDtl.GPA
                         , tGrdSclDtl.MinGradeValue
                         , tGrdSclDtl.MaxGradeValue
                         , @CampusId AS CampusId
                    FROM   #GrdSclDetail tGrdSclDtl
                    LEFT JOIN FreedomBridge.bridge.arGradeSystemDetails bGrdSysDtl ON bGrdSysDtl.src_Grade = tGrdSclDtl.Grade
                                                                                      AND bGrdSysDtl.src_GPA = tGrdSclDtl.GPA
                                                                                      AND (
                                                                                              bGrdSysDtl.src_RecordKey = tGrdSclDtl.RecordKey
                                                                                              OR bGrdSysDtl.src_RecordKey = @SchoolID
                                                                                          )
                                                                                      AND bGrdSysDtl.trg_GrdSystemDetailId = tGrdSclDtl.GrdSysDetailId
                    WHERE  bGrdSysDtl.trg_GrdSystemDetailId IS NULL;

        DROP TABLE #GrdSclDtl;
        DROP TABLE #GrdSclDtlTmp;
        DROP TABLE #GrdSclDetail;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGradeSystemDetails]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
