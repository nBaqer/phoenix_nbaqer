SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arDropReasons]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT   ISNULL(dr.DropReasonId,NEWID()) AS DropReasonId                               -- uniqueidentifier
               , UPPER(dbo.udf_FrstLtrWord_V2(FL.Description)) AS Code -- varchar(50)
               , CAST(FL.Description AS VARCHAR(50)) AS Descrip        -- varchar(50)
               , dStatus.StatusId AS StatusId                          -- uniqueidentifier
               , dCampGrp.CampGrpId AS CampGrpId                       -- uniqueidentifier
               , 'SA' AS ModUser                                       -- varchar(50)
               , GETDATE() AS ModDate                                  -- datetime
               , FL.IPEDSValue AS IPEDSValue                           -- int
               , FL.ID
               , FL.LookUpID
        INTO     #arDropReasons
        FROM     stage.FLookUp FL
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN bridge.arDropReasons bIgnore ON bIgnore.src_Description = CAST(FL.Description AS VARCHAR(50))
		LEFT JOIN  FreedomAdvantage..arDropReasons dr
              on dr.Code = UPPER(dbo.udf_FrstLtrWord_V2(FL.Description)) AND Descrip = CAST(FL.Description AS VARCHAR(50))
			  AND dr.StatusId = dStatus.StatusId AND dr.CampGrpId = dCampGrp.CampGrpId
        WHERE    LookupName = 'DropReasons'
                 AND bIgnore.src_ID IS NULL
				 
        ORDER BY CAST(FL.ID AS INT);
		
        INSERT INTO FreedomAdvantage..arDropReasons
                    SELECT DropReasonId
                         , Code
                         , Descrip
                         , StatusId
                         , CampGrpId
                         , ModUser
                         , ModDate
                         , IPEDSValue
                    FROM   #arDropReasons
					WHERE DropReasonId NOT IN (SELECT DropReasonId FROM FreedomAdvantage.dbo.arDropReasons)

        INSERT INTO FreedomBridge.bridge.arDropReasons
                    SELECT DropReasonId
                         , ID
                         , Descrip
                         , LookUpID
                    FROM   #arDropReasons;

        DROP TABLE #arDropReasons;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arDropReasons]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;
    END;
GO
