SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-12-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arStudentPhoneOLD]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN dStuPh.Phone IS NULL THEN NEWID()
                    ELSE dStuPh.LeadPhoneId
               END AS StudentPhoneId                                                                      -- uniqueidentifier
             , bStud.trg_StudentId AS StudentId                                                           -- uniqueidentifier
             , CASE WHEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1 = '' THEN NULL
                    ELSE ISNULL(bPhType.trg_PhoneTypeId
                              , (
                                    SELECT trg_PhoneTypeId
                                    FROM   FreedomBridge.bridge.syPhoneType
                                    WHERE  src_Description = 'Mobile'
                                )
                               )
               END AS PhoneTypeId                                                                         -- uniqueidentifier
             , NULLIF(sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1, '') AS Phone -- varchar(50)
             , NULL AS BestTime                                                                           -- varchar(50)
             , dStat.StatusId AS StatusId                                                                 -- uniqueidentifier
             , NULL AS Ext                                                                                -- varchar(50)
             , 'sa' AS ModUser                                                                            -- varchar(50)
             , GETDATE() AS ModDate                                                                       -- datetime
             , CASE WHEN bStud.LocalRank = 1 THEN 1
                    ELSE 0
               END AS default1                                                                            -- bit
             , 0 AS ForeignPhone                                                                          -- bit
             , bStud.StuSSN
             , bPhType.src_ID
             , bStud.TotalRank
             , bStud.LocalRank
        INTO   #StuPh1
        FROM   Freedom..STUDREC sStud
        INNER JOIN FreedomBridge.bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                              AND bStud.trg_CampusId = @CampusId
        LEFT JOIN FreedomBridge.bridge.syPhoneType bPhType ON bPhType.src_ID = sStud.PhoneType1
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        LEFT JOIN FreedomAdvantage..adLeadPhone dStuPh ON dStuPh.Phone = sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1;

        SELECT CASE WHEN dStuPh.Phone IS NULL THEN NEWID()
                    ELSE dStuPh.LeadPhoneId
               END AS StudentPhoneId                                                                      -- uniqueidentifier
             , bStud.trg_StudentId AS StudentId                                                           -- uniqueidentifier
             , CASE WHEN sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2 = '' THEN NULL
                    ELSE ISNULL(bPhType.trg_PhoneTypeId
                              , (
                                    SELECT trg_PhoneTypeId
                                    FROM   FreedomBridge.bridge.syPhoneType
                                    WHERE  src_Description = 'Work'
                                )
                               )
               END AS PhoneTypeId                                                                         -- uniqueidentifier
             , NULLIF(sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2, '') AS Phone -- varchar(50)
             , NULL AS BestTime                                                                           -- varchar(50)
             , dStat.StatusId AS StatusId                                                                 -- uniqueidentifier
             , NULL AS Ext                                                                                -- varchar(50)
             , 'sa' AS ModUser                                                                            -- varchar(50)
             , GETDATE() AS ModDate                                                                       -- datetime
             , 0 AS default1                                                                              -- bit
             , 0 AS ForeignPhone                                                                          -- bit
             , bStud.StuSSN
             , bPhType.src_ID
             , bStud.TotalRank
             , bStud.LocalRank
        INTO   #StuPh2
        FROM   Freedom..STUDREC sStud
        INNER JOIN FreedomBridge.bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                              AND bStud.trg_CampusId = @CampusId
        LEFT JOIN FreedomBridge.bridge.syPhoneType bPhType ON bPhType.src_ID = sStud.PhoneType2
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        LEFT JOIN FreedomAdvantage..adLeadPhone dStuPh ON dStuPh.Phone = sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1;

        SELECT *
        INTO   #StuPh
        FROM   (
                   SELECT *
                        , ROW_NUMBER() OVER ( PARTITION BY DupStuPh.StuSSN
                                                         , DupStuPh.Phone
                                              ORDER BY DupStuPh.StuSSN
                                            ) AS rownum
                   FROM   (
                              SELECT tStuPh1.StudentPhoneId
                                   , tStuPh1.StudentId
                                   , tStuPh1.PhoneTypeId
                                   , tStuPh1.Phone
                                   , tStuPh1.BestTime
                                   , tStuPh1.StatusId
                                   , tStuPh1.Ext
                                   , tStuPh1.ModUser
                                   , tStuPh1.ModDate
                                   , tStuPh1.default1
                                   , tStuPh1.ForeignPhone
                                   , tStuPh1.StuSSN
                                   , tStuPh1.TotalRank
                                   , tStuPh1.LocalRank
                                   , tStuPh1.src_ID
                              FROM   #StuPh1 tStuPh1
                              UNION ALL
                              SELECT tStuPh2.StudentPhoneId
                                   , tStuPh2.StudentId
                                   , tStuPh2.PhoneTypeId
                                   , tStuPh2.Phone
                                   , tStuPh2.BestTime
                                   , tStuPh2.StatusId
                                   , tStuPh2.Ext
                                   , tStuPh2.ModUser
                                   , tStuPh2.ModDate
                                   , tStuPh2.default1
                                   , tStuPh2.ForeignPhone
                                   , tStuPh2.StuSSN
                                   , tStuPh2.TotalRank
                                   , tStuPh2.LocalRank
                                   , tStuPh2.src_ID
                              FROM   #StuPh2 tStuPh2
                          ) DupStuPh
               ) StuPh
        WHERE  StuPh.rownum = 1;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arStudentPhoneOLD')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arStudentPhoneOLD
                    (
                        trg_StudentPhoneId UNIQUEIDENTIFIER
                      , src_Phone VARCHAR(255)
                      , src_PhoneType INT
                      , trg_PhoneTypeId UNIQUEIDENTIFIER
                      , trg_StudentId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Phone
                            , trg_StudentId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arStudentPhoneOLD
                    SELECT tStuPh.StudentPhoneId
                         , tStuPh.Phone
                         , tStuPh.src_ID
                         , tStuPh.PhoneTypeId
                         , tStuPh.StudentId
                         , @CampusId AS CampusId
                    FROM   #StuPh tStuPh
                    LEFT JOIN FreedomBridge.bridge.arStudentPhoneOLD bStuPh ON bStuPh.src_Phone = tStuPh.Phone
                                                                               AND bStuPh.trg_StudentId = tStuPh.StudentId
                    WHERE  bStuPh.trg_StudentPhoneId IS NULL
                           AND tStuPh.Phone IS NOT NULL;

      --  ALTER TABLE FreedomAdvantage..arStudentPhoneOld DISABLE TRIGGER ALL;

        --INSERT INTO FreedomAdvantage..arStudentPhoneOld
        --            SELECT tStuPh.StudentPhoneId
        --                 , tStuPh.StudentId
        --                 , tStuPh.PhoneTypeId
        --                 , tStuPh.Phone
        --                 , tStuPh.BestTime
        --                 , tStuPh.StatusId
        --                 , tStuPh.Ext
        --                 , tStuPh.ModUser
        --                 , tStuPh.ModDate
        --                 , tStuPh.default1
        --                 , tStuPh.ForeignPhone
        --            FROM   #StuPh tStuPh
        --            LEFT JOIN FreedomAdvantage..arStudentPhoneOld dStuPh ON dStuPh.StudentPhoneId = tStuPh.StudentPhoneId
        --            WHERE  dStuPh.Phone IS NULL
        --                   AND tStuPh.Phone IS NOT NULL;

    --    ALTER TABLE FreedomAdvantage..arStudentPhoneOld ENABLE TRIGGER ALL;

        DROP TABLE #StuPh1;
        DROP TABLE #StuPh2;
        DROP TABLE #StuPh;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arStudentPhoneOLD]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
