SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_FSAPPolicyDetails]
AS
  BEGIN

        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @TrigOffsetTypId INT;
        SET @TrigOffsetTypId = (
                               SELECT TrigOffsetTypId
                               FROM   FreedomAdvantage..arTrigOffsetTyps
                               WHERE  TrigOffTypDescrip = 'start date'
                               );

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        DECLARE @CampusGroupId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER;
        DECLARE @GradeFormat VARCHAR(50);

        SET @CampusId = (
                        SELECT TOP ( 1 ) Value
                        FROM   #Settings
                        WHERE  Entity = 'CampusID'
                        );

        SET @ActiveStatusId = (
                              SELECT   TOP ( 1 ) StatusId
                              FROM     FreedomAdvantage..syStatuses
                              WHERE    StatusCode = 'A'
                              ORDER BY StatusCode
                              );
        SET @CampusGroupId = (
                             SELECT CampGrpId
                             FROM   FreedomAdvantage..syCampGrps
                             WHERE  CampusId = @CampusId
                                    OR CampGrpCode = (
                                                     SELECT TOP 1 Value
                                                     FROM   #Settings
                                                     WHERE  Entity = 'SchoolId'
                                                     )
                             );

        SET @GradeFormat = (
                           SELECT     TOP ( 1 ) Value
                           FROM       FreedomAdvantage.dbo.syConfigAppSetValues settingValue
                           INNER JOIN FreedomAdvantage.dbo.syConfigAppSettings configSettings ON configSettings.SettingId = settingValue.SettingId
                           WHERE      configSettings.KeyName = 'GradesFormat'
                           );


        SELECT     NEWID() AS SAPDetailId                                        -- SAPDetailId - uniqueidentifier
                  ,freedomProgramVersion.PrivFASAPMinResult AS QualMinValue      -- QualMinValue - decimal(19, 2)
                  ,freedomProgramVersion.PrivFASAPQuantMinValue AS QuantMinValue -- QuantMinValue - decimal(18, 2)
                  ,CASE @GradeFormat
                        WHEN 'Numeric' THEN 1
                        ELSE 2
                   END AS QualMinTypId                                           -- QualMinTypId - tinyint
                  ,CASE freedomProgramVersion.PrivFASAPQuantMinUnit
                        WHEN 1 THEN 3
                        WHEN 2 THEN 3
                        ELSE CASE PrivFASAPQuantMinType
                                  WHEN 1 THEN 1
                                  WHEN 2 THEN 1
                             END
                   END AS QuantMinUnitTypId                                      -- QuantMinUnitTypId - tinyint
                  ,0 AS TrigValue                                                -- TrigValue - int
                  ,CASE freedomProgramVersion.FASAPRunBy
                        WHEN 1 THEN 4
                        WHEN 2 THEN 5
                        ELSE 4
                   END AS TrigUnitTypId                                          -- TrigUnitTypId - tinyint
                  ,3 AS TrigOffsetTypId                                          -- TrigOffsetTypId - tinyint
                  ,NULL AS TrigOffsetSeq                                         -- TrigOffsetSeq - tinyint
                  ,0 AS ConsequenceTypId                                         -- ConsequenceTypId - tinyint
                  ,0 AS MinCredsCompltd                                          -- MinCredsCompltd - decimal(18, 2)
                  ,fsap.trg_SAPId AS SAPId                                       -- SAPId - uniqueidentifier
                  ,0 AS MinAttendanceValue                                       -- MinAttendanceValue - decimal(19, 2)
                  ,NULL AS accuracy                                              -- accuracy - decimal(18, 2)
                  ,freedomProgramVersion.RecordKey AS RecordKey
                  ,freedomProgramVersion.NumAcYrPayPrds AS NumAcYrPayPrds
                  ,freedomProgramVersion.PrivLength AS PrivLength
                  ,freedomProgramVersion.PrivAcYear AS PrivAcYear
        INTO       #FreedomToAdvSAPDetails
        FROM       Freedom..COURSE_DEF freedomProgramVersion
        INNER JOIN bridge.arFSAP fsap ON fsap.src_ProgramVersionId = freedomProgramVersion.RecordKey
                                         AND fsap.trg_CampusId = @CampusId;

     

        IF EXISTS (
                  SELECT COUNT(*)
                  FROM   #FreedomToAdvSAPDetails
                  )
            BEGIN
                UPDATE     configValues
                SET        configValues.Value = 'True'
                FROM       FreedomAdvantage..syConfigAppSettings configSetting
                INNER JOIN FreedomAdvantage..syConfigAppSetValues configValues ON configValues.SettingId = configSetting.SettingId
                WHERE      KeyName = 'TrackFASAP';
            END;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arFSAPDetails')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arFSAPDetails
                    (
                        trg_SAPId UNIQUEIDENTIFIER
                       ,trg_SAPDetailId UNIQUEIDENTIFIER
                       ,src_ProgramVersionId INT
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       trg_SAPId
                      ,src_ProgramVersionId
                      ,trg_SAPDetailId
                       )
                    );
            END;
        ELSE
            BEGIN
                TRUNCATE TABLE FreedomBridge.bridge.arFSAPDetails;
            END;

        DECLARE @SAPDetailId UNIQUEIDENTIFIER
               ,@SAPId UNIQUEIDENTIFIER;
        DECLARE @QualMinValue DECIMAL(19, 2);
        DECLARE @QuantMinValue DECIMAL(18, 2);
        DECLARE @QualMinTypId TINYINT
               ,@QuantMinUnitTypId TINYINT
               ,@TrigUnitTypId TINYINT
               ,@TrigOffsetSeq TINYINT
               ,@ConsequenceTypId TINYINT;
        DECLARE @TrigValue INT;
        DECLARE @MinAttendanceValue DECIMAL(19, 2);
        DECLARE @accuracy DECIMAL(18, 2)
               ,@MinCredsCompltd DECIMAL(18, 2);
        DECLARE @RecordKey INT;
        DECLARE @NumAcYrPayPrds INT;
        DECLARE @PrivLength DECIMAL(18, 2);
        DECLARE @PrivAcYear DECIMAL(18, 2);


        DECLARE sapDetailsCursor CURSOR FOR
            SELECT *
            FROM   #FreedomToAdvSAPDetails;
        OPEN sapDetailsCursor;
        FETCH NEXT FROM sapDetailsCursor
        INTO @SAPDetailId
            ,@QualMinValue
            ,@QuantMinValue
            ,@QualMinTypId
            ,@QuantMinUnitTypId
            ,@TrigValue
            ,@TrigUnitTypId
            ,@TrigOffsetTypId
            ,@TrigOffsetSeq
            ,@ConsequenceTypId
            ,@MinCredsCompltd
            ,@SAPId
            ,@MinAttendanceValue
            ,@accuracy
            ,@RecordKey
            ,@NumAcYrPayPrds
            ,@PrivLength
            ,@PrivAcYear;

        WHILE @@FETCH_STATUS = 0
            BEGIN
               -- PRINT @SAPDetailId;

                DECLARE @incrementTrigerValue DECIMAL(19, 2) = 0;
				DECLARE @incrementTrigerValueLastYear DECIMAL(19, 2) = 0;
				DECLARE @ACYearLengthLastYear DECIMAL(19, 2) = 0;
				DECLARE @curentTotalHour DECIMAL(19, 2) = 0;
			    DECLARE @lastYearcounter INT = 1;
                DECLARE @counter INT = 1;

                IF ( @PrivLength <= @PrivAcYear )
                    BEGIN
                        SET @incrementTrigerValue = @PrivLength / @NumAcYrPayPrds;

                        WHILE @counter <= @NumAcYrPayPrds
                            BEGIN
                                SET @SAPDetailId = NEWID();

                                INSERT INTO FreedomAdvantage..arSAPDetails (
                                                                           SAPDetailId
                                                                          ,QualMinValue
                                                                          ,QuantMinValue
                                                                          ,QualMinTypId
                                                                          ,QuantMinUnitTypId
                                                                          ,TrigValue
                                                                          ,TrigUnitTypId
                                                                          ,TrigOffsetTypId
                                                                          ,TrigOffsetSeq
                                                                          ,ConsequenceTypId
                                                                          ,MinCredsCompltd
                                                                          ,SAPId
                                                                          ,MinAttendanceValue
                                                                          ,accuracy
                                                                           )
                                VALUES ( @SAPDetailId                -- SAPDetailId - uniqueidentifier
                                        ,@QualMinValue               -- QualMinValue - decimal(19, 2)
                                        ,@QuantMinValue              -- QuantMinValue - decimal(18, 2)
                                        ,@QualMinTypId               -- QualMinTypId - tinyint
                                        ,@QuantMinUnitTypId          -- QuantMinUnitTypId - tinyint
                                        ,@incrementTrigerValue * 100 -- TrigValue - int
                                        ,@TrigUnitTypId              -- TrigUnitTypId - tinyint
                                        ,@TrigOffsetTypId            -- TrigOffsetTypId - tinyint
                                        ,@TrigOffsetSeq              -- TrigOffsetSeq - tinyint
                                        ,@ConsequenceTypId           -- ConsequenceTypId - tinyint
                                        ,@MinCredsCompltd            -- MinCredsCompltd - decimal(18, 2)
                                        ,@SAPId                      -- SAPId - uniqueidentifier
                                        ,@MinAttendanceValue         -- MinAttendanceValue - decimal(19, 2)
                                        ,@accuracy                   -- accuracy - decimal(18, 2)
                                    );

                                INSERT INTO bridge.arFSAPDetails (
                                                                 trg_SAPId
                                                                ,trg_SAPDetailId
                                                                ,src_ProgramVersionId
                                                                ,trg_CampusId
                                                                 )
                                VALUES ( @SAPId, @SAPDetailId, @RecordKey, @CampusId );
                                SET @counter += 1;
								SET @incrementTrigerValue += @incrementTrigerValue;
                            END;
                    END;
                IF ( @PrivLength > @PrivAcYear )
                    BEGIN
					   
                        SET @incrementTrigerValue = @PrivAcYear / @NumAcYrPayPrds;
						SET @ACYearLengthLastYear = @PrivLength % @PrivAcYear
						SET @incrementTrigerValueLastYear = @ACYearLengthLastYear / @NumAcYrPayPrds
						
                        WHILE ( @counter * @incrementTrigerValue <= @PrivLength - @ACYearLengthLastYear ) OR (@curentTotalHour + @lastYearcounter * @incrementTrigerValueLastYear <= @PrivLength)
                            BEGIN
							
                                DECLARE @IncrementToInsert DECIMAL(18, 2);
								
                                IF @counter * @incrementTrigerValue <= @PrivLength - @ACYearLengthLastYear
                                    BEGIN
									   
                                        SET @IncrementToInsert = @incrementTrigerValue * @counter;
										SET @curentTotalHour = @IncrementToInsert
                                    END
								ELSE IF (@curentTotalHour + @lastYearcounter * @incrementTrigerValueLastYear <= @PrivLength)
								BEGIN
							    	SET @IncrementToInsert =@curentTotalHour + @incrementTrigerValueLastYear;
								    SET @curentTotalHour = @IncrementToInsert
								    SET @lastYearcounter += 1;
								END;
                             




                                SET @SAPDetailId = NEWID();

                                INSERT INTO FreedomAdvantage..arSAPDetails (
                                                                           SAPDetailId
                                                                          ,QualMinValue
                                                                          ,QuantMinValue
                                                                          ,QualMinTypId
                                                                          ,QuantMinUnitTypId
                                                                          ,TrigValue
                                                                          ,TrigUnitTypId
                                                                          ,TrigOffsetTypId
                                                                          ,TrigOffsetSeq
                                                                          ,ConsequenceTypId
                                                                          ,MinCredsCompltd
                                                                          ,SAPId
                                                                          ,MinAttendanceValue
                                                                          ,accuracy
                                                                           )
                                VALUES ( @SAPDetailId             -- SAPDetailId - uniqueidentifier
                                        ,@QualMinValue            -- QualMinValue - decimal(19, 2)
                                        ,@QuantMinValue           -- QuantMinValue - decimal(18, 2)
                                        ,@QualMinTypId            -- QualMinTypId - tinyint
                                        ,@QuantMinUnitTypId       -- QuantMinUnitTypId - tinyint
                                        ,@IncrementToInsert * 100 -- TrigValue - int
                                        ,@TrigUnitTypId           -- TrigUnitTypId - tinyint
                                        ,@TrigOffsetTypId         -- TrigOffsetTypId - tinyint
                                        ,@TrigOffsetSeq           -- TrigOffsetSeq - tinyint
                                        ,@ConsequenceTypId        -- ConsequenceTypId - tinyint
                                        ,@MinCredsCompltd         -- MinCredsCompltd - decimal(18, 2)
                                        ,@SAPId                   -- SAPId - uniqueidentifier
                                        ,@MinAttendanceValue      -- MinAttendanceValue - decimal(19, 2)
                                        ,@accuracy                -- accuracy - decimal(18, 2)
                                    );
									
                                INSERT INTO bridge.arFSAPDetails (
                                                                 trg_SAPId
                                                                ,trg_SAPDetailId
                                                                ,src_ProgramVersionId
                                                                ,trg_CampusId
                                                                )
                                VALUES ( @SAPId, @SAPDetailId, @RecordKey, @CampusId );
                                SET @counter += 1;
                            END;
							--
                    END;
                


                DECLARE @Check DECIMAL(18, 2) = 0;
                SET @Check = @incrementTrigerValue;






                FETCH NEXT FROM sapDetailsCursor
                INTO @SAPDetailId
                    ,@QualMinValue
                    ,@QuantMinValue
                    ,@QualMinTypId
                    ,@QuantMinUnitTypId
                    ,@TrigValue
                    ,@TrigUnitTypId
                    ,@TrigOffsetTypId
                    ,@TrigOffsetSeq
                    ,@ConsequenceTypId
                    ,@MinCredsCompltd
                    ,@SAPId
                    ,@MinAttendanceValue
                    ,@accuracy
                    ,@RecordKey
                    ,@NumAcYrPayPrds
                    ,@PrivLength
                    ,@PrivAcYear;
            END;

        CLOSE sapDetailsCursor;
        DEALLOCATE sapDetailsCursor;

	
       DROP TABLE #FreedomToAdvSAPDetails;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_FSAPPolicyDetails]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
