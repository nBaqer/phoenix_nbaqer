SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-04-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arClassSections]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        -- Collect each course to make a class section

        SELECT *
        INTO   #ClsSections
        FROM   (
               SELECT     bTerm.trg_TermId AS TermId                                      -- uniqueidentifier
                         ,bReqs.trg_ReqId AS ReqId                                        -- uniqueidentifier
                         ,FreedomBridge.dbo.udf_FrstLtrWord(bReqs.src_Name) AS ClsSection -- varchar(12)
                         ,bReqs.src_CourseDefKey
                         ,sSub.Name
                         ,bReqs.src_Name
                         ,sTerm.RecordKey AS TermKey
                         ,sSub.RecordKey AS SubKey
                         ,ROW_NUMBER() OVER ( PARTITION BY bTerm.trg_TermId
                                                          ,bReqs.trg_ReqId
                                              ORDER BY bTerm.trg_TermId
                                            ) AS rownum
                         ,dUser.UserId AS InstructorId                                    -- uniqueidentifier

                         ,'1/1/2000' AS StartDate                                         -- datetime
                         ,'12/31/2050' AS EndDate                                         -- datetime
                         ,25 AS MaxStud                                                   -- int
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS InstrGrdBkWgtId               -- uniqueidentifier
                         ,bGrdScl.trg_GradeScaleId AS GrdScaleId                          -- uniqueidentifier
                         ,@CampusId AS CampusId                                           -- uniqueidentifier
                         ,NULL AS IsGraded                                                -- bit
                         ,'sa' AS ModUser                                                 -- varchar(50)
                         ,GETDATE() AS ModDate                                            -- datetime
                         ,NULL AS CourseId                                                -- varchar(50)
                         ,NULL AS TermGUID                                                -- varchar(50)
                         ,NULL AS Session                                                 -- varchar(50)
                         ,NULL AS ClassRoom                                               -- varchar(50)
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftId                       -- uniqueidentifier
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS PeriodId                      -- uniqueidentifier
                         ,NULL AS StudentStartDate                                        -- datetime
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS LeadGrpId                     -- uniqueidentifier
                         ,NULL AS CohortStartDate                                         -- datetime
                         ,sSub.RecordKey
                         ,bTerm.src_RecordKey TermRecKey
                         ,sSubRes.StudrecKey
               FROM       Freedom.dbo.SUBJECT_DEF sSub
               INNER JOIN Freedom.dbo.SUBJECT_RES sSubRes ON sSubRes.SubjectDefKey = sSub.RecordKey
               INNER JOIN Freedom.dbo.TERM_RES sTermRes ON sTermRes.RecordKey = sSubRes.TermResKey
               INNER JOIN Freedom.dbo.TERM_DEF sTerm ON sTerm.RecordKey = sTermRes.TermDefKey
               INNER JOIN Freedom.dbo.STUDREC sStu ON sStu.RecordKey = sTermRes.StudrecKey
               INNER JOIN FreedomBridge.bridge.arReqs bReqs ON bReqs.src_RecordKey = sSub.RecordKey
                                                               AND bReqs.trg_CampusId = @CampusId
               INNER JOIN FreedomBridge.bridge.arTerm bTerm ON bTerm.src_RecordKey = sTermRes.TermDefKey
                                                               AND bTerm.trg_CampusId = @CampusId
               INNER JOIN FreedomBridge.bridge.arGradeScales bGrdScl ON bGrdScl.src_Description = 'Default'
                                                                        AND bGrdScl.trg_CampusId = @CampusId
               INNER JOIN FreedomAdvantage.dbo.syUsers dUser ON dUser.FullName = 'sa'
               ) Course;



        -- Collect each different schedule possibility for each course
        SELECT   ROW_NUMBER() OVER ( PARTITION BY CourseNumEnrolledIn
                                     ORDER BY CourseNumEnrolledIn
                                   ) AS SchedRank
                ,AttndSchdHrsMon
                ,AttndSchdHrsTue
                ,AttndSchdHrsWed
                ,AttndSchdHrsThr
                ,AttndSchdHrsFri
                ,AttndSchdHrsSat
                ,AttndSchdHrsSun
                ,CourseNumEnrolledIn
                ,@CampusId AS CampusId
                ,MIN(StartDate) AS StartDate
                ,MAX(RevisedGradDate) AS EndDate
        INTO     #Schedule
        FROM     Freedom..STUDREC
        GROUP BY AttndSchdHrsMon
                ,AttndSchdHrsTue
                ,AttndSchdHrsWed
                ,AttndSchdHrsThr
                ,AttndSchdHrsFri
                ,AttndSchdHrsSat
                ,AttndSchdHrsSun
                ,CourseNumEnrolledIn;

        -- Merge the class sections with each schedule for unique class sections
        --SELECT CASE WHEN dClsSect.ClsSection IS NULL THEN NEWID()
        --            ELSE dClsSect.ClsSectionId
        --       END AS ClsSectionId
        SELECT NEWID() AS ClsSectionId
              ,U.*
        INTO   #ClassSect
        FROM   (
               SELECT     DISTINCT cs.TermId
                                  ,cs.ReqId
                                  ,LEFT(CAST(s.AttndSchdHrsSun AS VARCHAR(10)) + CAST(s.AttndSchdHrsMon AS VARCHAR(10)) + CAST(s.AttndSchdHrsTue AS VARCHAR(10))
                                        + CAST(s.AttndSchdHrsWed AS VARCHAR(10)) + CAST(s.AttndSchdHrsThr AS VARCHAR(10)) + CAST(s.AttndSchdHrsFri AS VARCHAR(10))
                                        + CAST(s.AttndSchdHrsSat AS VARCHAR(10)) + cs.ClsSection, 12) AS ClsSection
                                  ,cs.Name
                                  ,cs.src_Name
                                  ,cs.src_CourseDefKey
                                  ,cs.InstructorId
                                  ,s.StartDate
                                  ,s.EndDate
                                  ,cs.MaxStud
                                  ,cs.InstrGrdBkWgtId
                                  ,cs.GrdScaleId
                                  ,cs.CampusId
                                  ,cs.IsGraded
                                  ,cs.ModUser
                                  ,cs.ModDate
                                  ,cs.CourseId
                                  ,cs.TermGUID
                                  ,cs.Session
                                  ,cs.ClassRoom
                                  ,cs.ShiftId
                                  ,cs.PeriodId
                                  ,cs.StudentStartDate
                                  ,cs.LeadGrpId
                                  ,cs.CohortStartDate
                                  ,s.CourseNumEnrolledIn
                                  ,cs.RecordKey
               FROM       #ClsSections cs
               INNER JOIN #Schedule s ON s.CourseNumEnrolledIn = cs.src_CourseDefKey
                                         AND s.CampusId = @CampusId
               LEFT JOIN  FreedomAdvantage..arClassSections dClsSect ON dClsSect.ClsSection = CAST(s.AttndSchdHrsSun AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsMon AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsTue AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsWed AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsThr AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsFri AS VARCHAR(10))
                                                                                              + CAST(s.AttndSchdHrsSat AS VARCHAR(10)) + cs.ClsSection
                                                                        AND dClsSect.CampusId = cs.CampusId
                                                                        AND dClsSect.ReqId = cs.ReqId
                                                                        AND dClsSect.TermId = cs.TermId
               ) U;

   


        ALTER TABLE FreedomAdvantage..arClassSections DISABLE TRIGGER ALL;

       -- Insert data into arClassSections table
        INSERT INTO FreedomAdvantage..arClassSections
                    SELECT    tClsSect.ClsSectionId
                             ,tClsSect.TermId
                             ,tClsSect.ReqId
                             ,tClsSect.ClsSection
                             ,tClsSect.InstructorId
                             ,tClsSect.StartDate
                             ,tClsSect.EndDate
                             ,tClsSect.MaxStud
                             ,tClsSect.InstrGrdBkWgtId
                             ,tClsSect.GrdScaleId
                             ,tClsSect.CampusId
                             ,tClsSect.IsGraded
                             ,tClsSect.ModUser
                             ,tClsSect.ModDate
                             ,tClsSect.CourseId
                             ,tClsSect.TermGUID
                             ,tClsSect.Session
                             ,tClsSect.ClassRoom
                             ,tClsSect.ShiftId
                             ,tClsSect.PeriodId
                             ,tClsSect.StudentStartDate
                             ,tClsSect.LeadGrpId
                             ,tClsSect.CohortStartDate
                             ,NULL
                    FROM      #ClassSect tClsSect
                    LEFT JOIN FreedomAdvantage..arClassSections dClsSect ON dClsSect.ClsSection = tClsSect.ClsSection
                                                                            AND dClsSect.CampusId = tClsSect.CampusId
                                                                            AND dClsSect.TermId = tClsSect.TermId
                                                                            AND dClsSect.ReqId = tClsSect.ReqId
                    WHERE      dClsSect.ClsSectionId IS NULL;


        ALTER TABLE FreedomAdvantage..arClassSections ENABLE TRIGGER ALL;
		SELECT * FROM #ClassSect WHERE RecordKey = 179

         INSERT INTO FreedomBridge.bridge.arClassSections
        SELECT    DISTINCT tClsSect.ClsSectionId
                          ,tClsSect.CourseNumEnrolledIn
                          ,tClsSect.RecordKey
                          ,tClsSect.ClsSection
                          ,tClsSect.TermId
                          ,tClsSect.ReqId
                          ,tClsSect.CampusId
        FROM      #ClassSect tClsSect
        LEFT JOIN FreedomBridge.bridge.arClassSections bClsSect ON bClsSect.trg_ClsSection = tClsSect.ClsSection
                                                                   AND bClsSect.trg_CampusId = tClsSect.CampusId
                                                                   AND bClsSect.src_RecordKey = tClsSect.RecordKey
        WHERE     bClsSect.trg_ClsSectionId IS NULL
        ORDER BY  tClsSect.ClsSection;

        DROP TABLE #ClsSections;
        DROP TABLE #Schedule;
        DROP TABLE #ClassSect;
        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arClassSections]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
