SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=============================================
--Author:		<Author, Spencer Garrett>
--Create date: <Create Date, 11-11-14>
--=============================================
CREATE PROCEDURE [bridge].[usp_adLeadPhone]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #LeadPhone
        FROM   (
               SELECT     CASE WHEN dLeadPhone.LeadPhoneId IS NULL THEN NEWID()
                               ELSE dLeadPhone.LeadPhoneId
                          END AS LeadPhoneId                                                             -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                                                     -- uniqueidentifier
                         ,bPhoneType.trg_PhoneTypeId AS PhoneTypeId                                      -- uniqueidentifier
                         ,sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1 AS Phone -- varchar(50)
                         ,GETDATE() AS ModDate                                                           -- datetime
                         ,'sa' AS ModUser                                                                -- varchar(50)
                         ,1 AS Position                                                                  -- int
                         ,'' AS Extension                                                                -- varchar(10)
                         ,0 AS IsForeignPhone                                                            -- bit
                         ,1 AS IsBest                                                                    -- bit
                         ,1 AS IsShowOnLeadPage                                                          -- bit
                         ,dStatus.StatusId AS StatusId                                                   -- uniqueidentifier
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.StartDate DESC
                                            ) AS LocalRank
                         ,sStud.StartDate
               FROM       Freedom.dbo.STUDREC sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_StuRecordKey = sStud.RecordKey
               INNER JOIN FreedomBridge.bridge.syPhoneType bPhoneType ON bPhoneType.src_ID = CASE sStud.PhoneType1
                                                                                                  WHEN '' THEN 1
                                                                                                  ELSE CAST(sStud.PhoneType1 AS INT)
                                                                                             END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadPhone dLeadPhone ON dLeadPhone.Phone = sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
               WHERE      LTRIM(RTRIM(sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1)) <> ''
               GROUP BY   sStud.PhoneNumAreaCode1
                         ,sStud.PhoneNumPrefix1
                         ,sStud.PhoneNumBody1
                         ,dLeadPhone.LeadPhoneId
                         ,bLead.trg_LeadId
                         ,bPhoneType.trg_PhoneTypeId
                         ,dStatus.StatusId
                         ,sStud.StartDate
               UNION
               SELECT     CASE WHEN dLeadPhone.LeadPhoneId IS NULL THEN NEWID()
                               ELSE dLeadPhone.LeadPhoneId
                          END AS LeadPhoneId                        -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                -- uniqueidentifier
                         ,bPhoneType.trg_PhoneTypeId AS PhoneTypeId -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.PhoneNum1)) AS Phone    -- varchar(50)
                         ,GETDATE() AS ModDate                      -- datetime
                         ,'sa' AS ModUser                           -- varchar(50)
                         ,2 AS Position                             -- int
                         ,'' AS Extension                           -- varchar(10)
                         ,0 AS IsForeignPhone                       -- bit
                         ,0 AS IsBest                               -- bit
                         ,0 AS IsShowOnLeadPage                     -- bit
                         ,dStatus.StatusId AS StatusId              -- uniqueidentifier
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.PhoneNum1
                                            ) AS LocalRank
                         ,NULL AS StartDate
               FROM       Freedom.dbo.PROSPECT_FIL sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_ProRecordKey = sStud.RecordKey
               INNER JOIN FreedomBridge.bridge.syPhoneType bPhoneType ON bPhoneType.src_ID = CASE sStud.PhoneType1
                                                                                                  WHEN '' THEN 1
                                                                                                  ELSE CAST(sStud.PhoneType1 AS INT)
                                                                                             END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadPhone dLeadPhone ON dLeadPhone.Phone = sStud.PhoneNum1
               WHERE      LTRIM(RTRIM(sStud.PhoneNum1)) <> ''
               GROUP BY   sStud.PhoneNum1
                         ,dLeadPhone.LeadPhoneId
                         ,bLead.trg_LeadId
                         ,bPhoneType.trg_PhoneTypeId
                         ,dStatus.StatusId
               UNION
               SELECT     CASE WHEN dLeadPhone.LeadPhoneId IS NULL THEN NEWID()
                               ELSE dLeadPhone.LeadPhoneId
                          END AS LeadPhoneId                        -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                -- uniqueidentifier
                         ,bPhoneType.trg_PhoneTypeId AS PhoneTypeId -- uniqueidentifier
                         ,sStud.PhoneNum2 AS Phone                  -- varchar(50)
                         ,GETDATE() AS ModDate                      -- datetime
                         ,'sa' AS ModUser                           -- varchar(50)
                         ,3 AS Position                             -- int
                         ,'' AS Extension                           -- varchar(10)
                         ,0 AS IsForeignPhone                       -- bit
                         ,0 AS IsBest                               -- bit
                         ,0 AS IsShowOnLeadPage                     -- bit
                         ,dStatus.StatusId AS StatusId              -- uniqueidentifier
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.PhoneNum2
                                            ) AS LocalRank
                         ,NULL AS StartDate
               FROM       Freedom.dbo.PROSPECT_FIL sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_ProRecordKey = sStud.RecordKey
               INNER JOIN FreedomBridge.bridge.syPhoneType bPhoneType ON bPhoneType.src_ID = CASE sStud.PhoneType2
                                                                                                  WHEN '' THEN 1
                                                                                                  ELSE CAST(sStud.PhoneType2 AS INT)
                                                                                             END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadPhone dLeadPhone ON dLeadPhone.Phone = sStud.PhoneNum2
               WHERE      LTRIM(RTRIM(sStud.PhoneNum2)) <> ''
               GROUP BY   sStud.PhoneNum2
                         ,dLeadPhone.LeadPhoneId
                         ,bLead.trg_LeadId
                         ,bPhoneType.trg_PhoneTypeId
                         ,dStatus.StatusId
               UNION
               SELECT     CASE WHEN dLeadPhone.LeadPhoneId IS NULL THEN NEWID()
                               ELSE dLeadPhone.LeadPhoneId
                          END AS LeadPhoneId                                                             -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                                                     -- uniqueidentifier
                         ,bPhoneType.trg_PhoneTypeId AS PhoneTypeId                                      -- uniqueidentifier
                         ,sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2 AS Phone -- varchar(50)
                         ,GETDATE() AS ModDate                                                           -- datetime
                         ,'sa' AS ModUser                                                                -- varchar(50)
                         ,3 AS Position                                                                  -- int
                         ,'' AS Extension                                                                -- varchar(10)
                         ,0 AS IsForeignPhone                                                            -- bit
                         ,0 AS IsBest                                                                    -- bit
                         ,0 AS IsShowOnLeadPage                                                          -- bit
                         ,dStatus.StatusId AS StatusId                                                   -- uniqueidentifier
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2
                                            ) AS LocalRank
                         ,NULL AS StartDate
               FROM       Freedom.dbo.STUDREC sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_StuRecordKey = sStud.RecordKey
               INNER JOIN FreedomBridge.bridge.syPhoneType bPhoneType ON bPhoneType.src_ID = CASE sStud.PhoneType2
                                                                                                  WHEN '' THEN 1
                                                                                                  ELSE CAST(sStud.PhoneType2 AS INT)
                                                                                             END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadPhone dLeadPhone ON dLeadPhone.Phone = sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2
               WHERE      LTRIM(RTRIM(sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2)) <> ''
               GROUP BY   sStud.PhoneNumAreaCode2
                         ,sStud.PhoneNumPrefix2
                         ,sStud.PhoneNumBody2
                         ,dLeadPhone.LeadPhoneId
                         ,bLead.trg_LeadId
                         ,bPhoneType.trg_PhoneTypeId
                         ,dStatus.StatusId
               ) AS leadPhone;

        ALTER TABLE FreedomAdvantage.dbo.adLeadPhone DISABLE TRIGGER ALL;

        SELECT   *
                ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                     ORDER BY Position ASC
                                             ,IsShowOnLeadPage DESC
											 ,StartDate desc
                                   ) AS finalPosition
        INTO     #LeadPhoneMerged
        FROM     #LeadPhone
        ORDER BY LeadId;

		

        SELECT NEWID() AS LeadPhoneId
              ,F.*
        INTO   #FinalLeadPhones
        FROM   (
               SELECT DISTINCT LeadId
                              ,PhoneTypeId
                              ,Phone
                              ,ModDate
                              ,ModUser
                              ,finalPosition AS Position
                              ,Extension
                              ,IsForeignPhone
                              ,CASE WHEN finalPosition = 1
                                         AND IsBest = 1 THEN 1
                                    ELSE 0
                               END AS IsBest
                              ,IsShowOnLeadPage
                              ,StatusId
                              ,CampusId
               FROM   #LeadPhoneMerged
               ) F;


        SELECT   LeadId
                ,Phone
        INTO     #DupePhones
        FROM     #FinalLeadPhones
        GROUP BY LeadId
                ,Phone
        HAVING   COUNT(*) > 1;

        SELECT   E.LeadPhoneId
        INTO     #DupesToDelete
        FROM     #FinalLeadPhones E
        WHERE    E.Phone IN (
                            SELECT D.Phone
                            FROM   #DupePhones D
                            )
                 AND EXISTS (
                            SELECT 1
                            FROM   #FinalLeadPhones C
                            WHERE  C.Phone = E.Phone
                                   AND C.LeadId = E.LeadId
                                   AND C.IsBest = 1
                            )
                 AND E.IsBest = 0
        ORDER BY E.Phone;

        DELETE FROM #FinalLeadPhones
        WHERE LeadPhoneId IN (
                             SELECT *
                             FROM   #DupesToDelete
                             );


        INSERT INTO FreedomAdvantage.dbo.adLeadPhone
                    SELECT    tLeadPhone.LeadPhoneId
                             ,tLeadPhone.LeadId
                             ,tLeadPhone.PhoneTypeId
                             ,tLeadPhone.Phone
                             ,tLeadPhone.ModDate
                             ,tLeadPhone.ModUser
                             ,tLeadPhone.Position
                             ,tLeadPhone.Extension
                             ,tLeadPhone.IsForeignPhone
                             ,tLeadPhone.IsBest
                             ,tLeadPhone.IsShowOnLeadPage
                             ,tLeadPhone.StatusId
                    FROM      #FinalLeadPhones tLeadPhone
                    LEFT JOIN FreedomAdvantage.dbo.adLeadPhone dLeadPhone ON dLeadPhone.PhoneTypeId = tLeadPhone.PhoneTypeId
                                                                             AND dLeadPhone.Phone = tLeadPhone.Phone
                    WHERE     NOT EXISTS (
                                         SELECT TOP 1 *
                                         FROM   FreedomAdvantage..adLeadPhone T
                                         WHERE  T.LeadPhoneId = tLeadPhone.LeadPhoneId
                                         );

        ALTER TABLE FreedomAdvantage.dbo.adLeadPhone ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.adLeadPhone
                    SELECT    tLeadPhone.LeadPhoneId
                             ,tLeadPhone.Phone
                             ,tLeadPhone.LeadId
                             ,@CampusId AS CampusId
                    FROM      #FinalLeadPhones tLeadPhone
                    LEFT JOIN FreedomBridge.bridge.adLeadPhone bLeadPhone ON bLeadPhone.trg_LeadPhoneId = tLeadPhone.LeadPhoneId
                                                                             AND bLeadPhone.src_Phone = tLeadPhone.Phone;
        DROP TABLE #FinalLeadPhones;
        DROP TABLE #LeadPhone;
        DROP TABLE #Settings;
        DROP TABLE #DupePhones;
        DROP TABLE #DupesToDelete;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeadPhone]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
