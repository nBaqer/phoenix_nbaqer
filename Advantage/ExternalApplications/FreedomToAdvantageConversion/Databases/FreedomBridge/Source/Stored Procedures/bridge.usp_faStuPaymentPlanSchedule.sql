SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_faStuPaymentPlanSchedule]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS PayPlanScheduleId                    -- uniqueidentifier
             , bPayPlan.trg_PaymentPlanId AS PaymentPlanId     -- uniqueidentifier
             , tmp_Plan.DateValue AS ExpectedDate              -- datetime 2012-11-20 00:00:00.000
             , tmp_Plan.Amount AS Amount                       -- decimal(19,4)
             , NULL AS Reference                               -- varchar(50)
             , 'SA' AS ModUser                                 -- varchar(50)
             , GETDATE() AS ModDate                            -- varchar(50)
             , CAST(NULL AS UNIQUEIDENTIFIER) AS TransactionId -- uniqueidentifier
             , sPayPlan.RecordKey
             , sPayPlan.LastPmtKey
             , CASE --In case if it is Last payment schedule - we count all later transactions to it
                    WHEN sPayPlan.NumPmts = tmp_Plan.i THEN GETDATE()
                    ELSE tmp_Plan.DateToValue
               END AS DateToValue
        INTO   #faStuPaymentPlanSchedule
        FROM   bridge.faStudentPaymentPlans bPayPlan
        INNER JOIN Freedom..STUPMT_PLN sPayPlan ON sPayPlan.RecordKey = bPayPlan.src_RecordKey
        CROSS APPLY dbo.udf_GenerateDatePeriod(sPayPlan.FirstPmtDueDate
                                             , sPayPlan.NumPmts
                                             , sPayPlan.PrivSchedType
                                             , sPayPlan.PlanAmt
                                              ) tmp_Plan
        WHERE  sPayPlan.FirstPmtDueDate IS NOT NULL
               AND bPayPlan.trg_CampusId = @CampusId;

        ALTER TABLE FreedomAdvantage..faStuPaymentPlanSchedule DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..faStuPaymentPlanSchedule
                    SELECT PayPlanScheduleId
                         , PaymentPlanId
                         , ExpectedDate
                         , Amount
                         , Reference
                         , ModUser
                         , ModDate
                         , TransactionId
                    FROM   #faStuPaymentPlanSchedule;

        ALTER TABLE FreedomAdvantage..faStuPaymentPlanSchedule ENABLE TRIGGER ALL;

        INSERT INTO bridge.faStuPaymentPlanSchedule
                    SELECT PayPlanScheduleId AS trg_PayPlanScheduleId
                         , ExpectedDate AS trg_ExpectedDate
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                    FROM   #faStuPaymentPlanSchedule;

        --Adding Payment Plan disbursements
        SELECT NEWID() AS PmtDisbRelId                           -- uniqueidentifier
             , bTran.trg_TransactionId AS TransactionId          -- uniqueidentifier
             , dTran.TransAmount AS Amount                       -- decimal(18,4)
             , CAST(NULL AS UNIQUEIDENTIFIER) AS AwardScheduleId -- uniqueidentifier
             , tSched.PayPlanScheduleId AS PayPlanScheduleId     -- uniqueidentifier
             , 'SA' AS ModUser                                   -- varchar(50)
             , GETDATE() AS ModDate                              -- datetime
             , CAST(NULL AS UNIQUEIDENTIFIER) AS StuAwardId      -- uniqueidentifier
             , tTree.RecordKey
        INTO   #saPmtDisbRel
        FROM   #faStuPaymentPlanSchedule tSched
        CROSS APPLY (
                        SELECT tTree1.RecordKey
                        FROM   dbo.udf_GetFreedomTranTree(LastPmtKey) tTree1
                        WHERE  tTree1.TransDate < tSched.DateToValue
                               AND tTree1.TransDate > tSched.ExpectedDate
                    ) tTree
        INNER JOIN bridge.saTransactions bTran ON bTran.src_RecordKey = tTree.RecordKey
                                                  AND bTran.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..saTransactions dTran ON dTran.TransactionId = bTran.trg_TransactionId
        WHERE  tSched.LastPmtKey <> 0;

        ALTER TABLE FreedomAdvantage..saPmtDisbRel DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..saPmtDisbRel
                    SELECT PmtDisbRelId
                         , TransactionId
                         , Amount
                         , AwardScheduleId
                         , PayPlanScheduleId
                         , ModUser
                         , ModDate
                         , StuAwardId
                    FROM   #saPmtDisbRel;
        ALTER TABLE FreedomAdvantage..saPmtDisbRel ENABLE TRIGGER ALL;

        INSERT INTO bridge.saPmtDisbRel
                    SELECT PmtDisbRelId AS trg_PmtDisbRelId
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                         , PayPlanScheduleId AS trg_PayPlanScheduleId
                    FROM   #saPmtDisbRel;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_faStuPaymentPlanSchedule]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;
    END;
GO
