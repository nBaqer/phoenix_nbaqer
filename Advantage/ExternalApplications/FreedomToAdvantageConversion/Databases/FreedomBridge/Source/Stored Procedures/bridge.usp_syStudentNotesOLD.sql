SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_syStudentNotesOLD]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';


        --Student academic record notes section

        SELECT NEWID() AS StudentNoteId                                                                  -- uniqueidentifier
             , dStatus.StatusId AS StatusId                                                              -- uniqueidentifier
             , dStud.StudentId AS StudentId                                                              -- uniqueidentifier
             , dbo.udf_GetStudentNoteComment(sNote1.CommentKey) AS StudentNoteDescrip                    -- varchar(2000)
             , 'AR' AS ModuleCode                                                                        -- char(2)pabandy
             , bUser.trg_UserId AS UserId                                                                -- uniqueidentifier
             , CAST(CAST(CAST(sNote1.PrivAddDate AS DATE) AS VARCHAR(20)) + ' ' + LEFT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), LEN(sNote1.PrivAddTime) - 2)
                    + ':' + RIGHT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), 2) AS DATETIME) AS CreatedDate -- datetime
             , 'SA' AS ModUser                                                                           -- varchar(50)
             , GETDATE() AS ModDate                                                                      -- datetime
             , sNote1.RecordKey AS Noteskey
        INTO   #AR_syStudentNotes
        FROM   bridge.arStudentOLD bStud
        INNER JOIN FreedomAdvantage..arStudent dStud ON dStud.StudentId = bStud.trg_StudentId
        INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = bStud.src_RecordKey
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN Freedom..STUDREC_NOT sNote ON sNote.RecordKey = sStud.NotesKey
        INNER JOIN Freedom..STUDREC_NOT sNote1 ON sNote1.ParentKey = sNote.ParentKey
        LEFT JOIN Freedom..SHIBBOLETH_FIL sUser ON sUser.UserId = sNote1.PrivAddUserId
        LEFT JOIN bridge.syUsers bUser ON bUser.src_UserId = sUser.UserId
                                          AND bUser.trg_CampusId = @CampusId
                                          AND bUser.TableName = 'SHIBBOLETH_FIL'
        WHERE  bStud.trg_CampusId = @CampusId;

        ALTER TABLE FreedomAdvantage..syStudentNotes DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syStudentNotes
                    SELECT StudentNoteId
                         , StatusId
                         , StudentId
                         , StudentNoteDescrip
                         , ModuleCode
                         , UserId
                         , CreatedDate
                         , ModUser
                         , ModDate
                    FROM   #AR_syStudentNotes;

        ALTER TABLE FreedomAdvantage..syStudentNotes ENABLE TRIGGER ALL;

        INSERT INTO bridge.syStudentNotes
                    SELECT tNote.StudentNoteId AS trg_StudentNoteId
                         , tNote.Noteskey AS src_Noteskey
                         , sComment1.RecordKey AS src_CommentKey
                         , @CampusId AS trg_CampusID
                         , 'STUDREC_NOT' AS TableName
                    FROM   #AR_syStudentNotes tNote
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT sNote ON tNote.Noteskey = sNote.RecordKey
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT_CMT sComment ON sComment.RecordKey = sNote.CommentKey
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT_CMT sComment1 ON sComment1.ParentKey = sComment.ParentKey;


        DROP TABLE #AR_syStudentNotes;

        --Student placement notes

        SELECT NEWID() AS StudentNoteId                                                                  -- uniqueidentifier
             , dStatus.StatusId AS StatusId                                                              -- uniqueidentifier
             , dStuEnr.StudentId AS StudentId                                                            -- uniqueidentifier
             , dbo.udf_GetPlacementNoteComment(sNote1.CommentKey) AS StudentNoteDescrip                  -- varchar(2000)
             , 'PL' AS ModuleCode                                                                        -- char(2)
             , bUser.trg_UserId AS UserId                                                                -- uniqueidentifier
             , CAST(CAST(CAST(sNote1.PrivAddDate AS DATE) AS VARCHAR(20)) + ' ' + LEFT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), LEN(sNote1.PrivAddTime) - 2)
                    + ':' + RIGHT(CAST(sNote1.PrivAddTime AS VARCHAR(4)), 2) AS DATETIME) AS CreatedDate -- datetime
             , 'SA' AS ModUser                                                                           -- varchar(50)
             , GETDATE() AS ModDate                                                                      -- datetime
             , sNote1.RecordKey AS Noteskey
        INTO   #PL_syStudentNotes
        FROM   bridge.plStudentsPlaced bStudPl
        INNER JOIN FreedomAdvantage..PlStudentsPlaced dStudPl ON dStudPl.PlacementId = bStudPl.trg_PlacementId
        INNER JOIN Freedom..GRADPLAC_FIL sGradPlac ON sGradPlac.RecordKey = bStudPl.src_RecordKey
        INNER JOIN FreedomAdvantage..arStuEnrollments dStuEnr ON dStuEnr.StuEnrollId = dStudPl.StuEnrollId
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN Freedom..GRADPLAC_FIL_NOT sNote ON sNote.RecordKey = sGradPlac.NotesKey
        INNER JOIN Freedom..GRADPLAC_FIL_NOT sNote1 ON sNote1.ParentKey = sNote.ParentKey
        LEFT JOIN Freedom..SHIBBOLETH_FIL sUser ON sUser.UserId = sNote1.PrivAddUserId
        LEFT JOIN bridge.syUsers bUser ON bUser.src_UserId = sUser.UserId
                                          AND bUser.trg_CampusId = @CampusId
                                          AND bUser.TableName = 'SHIBBOLETH_FIL'
        WHERE  bStudPl.trg_CampusId = @CampusId;

        INSERT INTO FreedomAdvantage..syStudentNotes
                    SELECT StudentNoteId
                         , StatusId
                         , StudentId
                         , StudentNoteDescrip
                         , ModuleCode
                         , UserId
                         , CreatedDate
                         , ModUser
                         , ModDate
                    FROM   #PL_syStudentNotes;

        ALTER TABLE FreedomAdvantage..syStudentNotes ENABLE TRIGGER ALL;

        INSERT INTO bridge.syStudentNotes
                    SELECT tNote.StudentNoteId AS trg_StudentNoteId
                         , tNote.Noteskey AS src_Noteskey
                         , sComment1.RecordKey AS src_CommentKey
                         , @CampusId AS trg_CampusID
                         , 'GRADPLAC_FIL_NOT' AS TableName
                    FROM   #PL_syStudentNotes tNote
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT sNote ON tNote.Noteskey = sNote.RecordKey
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT_CMT sComment ON sComment.RecordKey = sNote.CommentKey
                    INNER JOIN Freedom..GRADPLAC_FIL_NOT_CMT sComment1 ON sComment1.ParentKey = sComment.ParentKey;

        DROP TABLE #PL_syStudentNotes;

	    INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syStudentNotesOLD]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
