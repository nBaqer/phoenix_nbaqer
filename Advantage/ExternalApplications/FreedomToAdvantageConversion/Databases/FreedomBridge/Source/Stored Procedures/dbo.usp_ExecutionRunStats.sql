SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_ExecutionRunStats]
    @DBID AS INT
  , @ExecutionStage AS SMALLINT
  , @ExecutionDescription AS VARCHAR(100)
AS
    BEGIN

        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION T1;

        DECLARE @ExecutionID AS INT;



        INSERT INTO FreedomBridge.dbo.Execution_Table
                    SELECT bSysTable.TABLE_NAME AS TableName
                         , 0 AS IsDisabled
                    FROM   INFORMATION_SCHEMA.TABLES bSysTable
                    LEFT JOIN FreedomBridge.dbo.Execution_Table bETable ON bETable.TableName = bSysTable.TABLE_NAME
                    WHERE  TABLE_SCHEMA = 'bridge'
                           AND bETable.TableID IS NULL
						   AND bSysTable.TABLE_NAME NOT IN ('arFSAP', 'arFSAPDetails', 'arProgSchedulesDetails', 'AttendanceExceptionList')
						   AND  bSysTable.TABLE_NAME NOT LIKE '%OLD'
						   
        IF @ExecutionStage = 1
            BEGIN
			
                INSERT INTO FreedomBridge.dbo.Execution (
                                                            DBID
                                                        )
                            SELECT @DBID;
                SELECT @ExecutionID = @@IDENTITY;
            END;
        ELSE
            SELECT @ExecutionID = MAX(ExecutionID)
            FROM   FreedomBridge.dbo.Execution;

        --EXEC FreedomBridge.dbo.usp_ExecutionLogStats @ExecutionID
        --                                           , @ExecutionStage
        --                                           , @ExecutionDescription;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_ExecutionRunStats]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION T1;

    END;


GO
