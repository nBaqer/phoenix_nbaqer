SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_syFamilyIncome]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        INSERT INTO bridge.syFamilyIncome (
                                              trg_FamilyIncomeID
                                            , trg_FamilyIncomeDescrip
                                            , src_AmountFrom
                                            , src_AmountTo
                                          )
                    SELECT   FamilyIncomeID AS trg_FamilyIncomeID
                           , FamilyIncomeDescrip AS trg_FamilyIncomeDescrip
                           , CASE WHEN dIncome.FamilyIncomeDescrip = 'Under $30,000' THEN 1
                                  WHEN dIncome.FamilyIncomeDescrip = '$30,001 - $48,000' THEN 30001
                                  WHEN dIncome.FamilyIncomeDescrip = '$48,001 - $75,000' THEN 48001
                                  WHEN dIncome.FamilyIncomeDescrip = '$75,001 - $110,000' THEN 75001
                                  WHEN dIncome.FamilyIncomeDescrip = '$110,001 - Above' THEN 110001

								  WHEN dIncome.FamilyIncomeDescrip = 'Over $110,000' THEN 110001
								  WHEN dIncome.FamilyIncomeDescrip = '$10,000 - $19,999' THEN 10000
								  WHEN dIncome.FamilyIncomeDescrip = '$20,000 - $30,000' THEN 20000
								  WHEN dIncome.FamilyIncomeDescrip = 'Under $10,000' THEN 1
                                  ELSE -1
                             END AS trg_IncomeFrom
                           , CASE WHEN dIncome.FamilyIncomeDescrip = 'Under $30,000' THEN 30000
                                  WHEN dIncome.FamilyIncomeDescrip = '$30,001 - $48,000' THEN 48000
                                  WHEN dIncome.FamilyIncomeDescrip = '$48,001 - $75,000' THEN 75000
                                  WHEN dIncome.FamilyIncomeDescrip = '$75,001 - $110,000' THEN 110000
                                  WHEN dIncome.FamilyIncomeDescrip = '$110,001 - Above' THEN 999999

								  WHEN dIncome.FamilyIncomeDescrip = 'Over $110,000' THEN 999999
								  WHEN dIncome.FamilyIncomeDescrip = '$10,000 - $19,999' THEN 19999
								  WHEN dIncome.FamilyIncomeDescrip = '$20,000 - $30,000' THEN 30000
								  WHEN dIncome.FamilyIncomeDescrip = 'Under $10,000' THEN 9999
                                  ELSE -1
                             END AS trg_IncomeTo
                    FROM     FreedomAdvantage.dbo.syFamilyIncome dIncome
                    ORDER BY ViewOrder;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syFamilyIncome]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
