SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-28-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adLeadGroups]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@CampGrpId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@CampusGroupToUseId UNIQUEIDENTIFIER;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SET @CampusGroupToUseId = CASE WHEN EXISTS (
                                                   SELECT 1
                                                   FROM   bridge.CampusGroupAllRules
                                                   WHERE  TableName = 'adLeadGroups'
                                                          AND IsCampusAll = 0
                                                   ) THEN @CampGrpId
                                       ELSE @CampusGroupAllId
                                  END;


        DECLARE @LeadGroups TABLE
            (
                LeadGrpID UNIQUEIDENTIFIER NOT NULL
               ,Descrip VARCHAR(50)       -- varchar(50)
               ,StatusId UNIQUEIDENTIFIER -- uniqueidentifier
               ,CampGrpId UNIQUEIDENTIFIER
               ,ModDate DATETIME
               ,ModUser VARCHAR(50)
               ,code VARCHAR(50)
               ,UseForScheduling BIT
               ,UseForStudentGroupTracking BIT
               ,CampusId UNIQUEIDENTIFIER
               ,RecordKey INT
               ,IsNewRecord BIT
            );

        --insert not existing groups
        INSERT INTO @LeadGroups
                    SELECT     NEWID() AS LeadGrpId             -- uniqueidentifier
                              ,RTRIM(sGrp.Name) AS Descrip      -- varchar(50)
                              ,dStatus.StatusId AS StatusId     -- uniqueidentifier
                              ,@CampusGroupToUseId AS CampGrpId -- uniqueidentifier
                              ,GETDATE() AS ModDate             -- datetime
                              ,'sa' AS ModUser                  -- varchar(50)
                              ,sGrp.Descrip AS code             -- varchar(50)
                              ,0 AS UseForScheduling            -- bit
                              ,1 AS UseForStudentGroupTracking  --bit
                              ,@CampusId AS CampusId
                              ,sGrp.RecordKey AS RecordKey
                              ,1 AS IsNewRecord
                    FROM       Freedom..GROUP_DEF sGrp
                    INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN sGrp.Active = 1 THEN 'Active'
                                                                                             ELSE 'Inactive'
                                                                                        END
                    INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                    WHERE      NOT EXISTS (
                                          SELECT TOP 1 ELG.LeadGrpId
                                          FROM   FreedomAdvantage..adLeadGroups ELG
                                          WHERE  ELG.Descrip = sGrp.Name
                                                 AND sGrp.Descrip = ELG.code
                                                 AND ELG.CampGrpId = @CampGrpId
                                          );



        --records for existing
        INSERT INTO @LeadGroups
                    SELECT     aLeadGroup.LeadGrpId AS LeadGrpId                                   -- uniqueidentifier
                              ,aLeadGroup.Descrip AS Descrip                                       -- varchar(50)
                              ,aLeadGroup.StatusId AS StatusId                                     -- uniqueidentifier
                              ,@CampusGroupToUseId AS CampGrpId                                    -- uniqueidentifier
                              ,GETDATE() AS ModDate                                                -- datetime
                              ,'sa' AS ModUser                                                     -- varchar(50)
                              ,aLeadGroup.code AS code                                             -- varchar(50)
                              ,aLeadGroup.UseForScheduling AS UseForScheduling                     -- bit
                              ,aLeadGroup.UseForStudentGroupTracking AS UseForStudentGroupTracking --bit
                              ,@CampusId AS CampusId
                              ,sGrp.RecordKey AS RecordKey
                              ,0 AS IsNewRecord
                    FROM       Freedom..GROUP_DEF sGrp
                    LEFT JOIN  FreedomAdvantage..adLeadGroups aLeadGroup ON aLeadGroup.Descrip = sGrp.Name
                                                                            AND sGrp.Descrip = aLeadGroup.code
                    INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN sGrp.Active = 1 THEN 'Active'
                                                                                             ELSE 'Inactive'
                                                                                        END
                    WHERE      aLeadGroup.LeadGrpId IS NOT NULL;

        IF NOT EXISTS (
                      SELECT TOP 1 LeadGrpId
                      FROM   FreedomAdvantage..adLeadGroups
                      WHERE  Descrip = 'Freedom Imported' AND CampGrpId = @CampusGroupAllId
                      )
            BEGIN
                INSERT INTO @LeadGroups
                            SELECT    ISNULL(bLeadGrps.trg_LeadGrpId, A.LeadGrpId) AS LeadGrpId
                                     ,A.Descrip
                                     ,A.StatusId
                                     ,A.CampGrpId
                                     ,A.ModDate
                                     ,A.ModUser
                                     ,A.code
                                     ,A.UseForScheduling
                                     ,A.UseForStudentGroupTracking
                                     ,A.CampusId
                                     ,A.recordkey
                                     ,A.NewRecord
                            FROM      (
                                      SELECT NEWID() AS LeadGrpId            -- LeadGrpId -- uniqueidentifier
                                            ,'Freedom Imported' AS Descrip   -- Descrip -- varchar(50)
                                            ,(
                                             SELECT StatusId
                                             FROM   FreedomAdvantage..syStatuses
                                             WHERE  Status = 'Active'
                                             ) AS StatusId                   -- StatusId -- uniqueidentifier
                                            ,(
                                             SELECT CampGrpId
                                             FROM   FreedomAdvantage..syCampGrps
                                             WHERE  IsAllCampusGrp = 1
                                             ) AS CampGrpId                  -- CampGrpId -- uniqueidentifier
                                            ,GETDATE() AS ModDate            -- ModDate -- datetime
                                            ,'sa' AS ModUser                 --  ModUser -- varchar(50)
                                            ,'C' AS code                     --  code -- varchar(50)
                                            ,0 AS UseForScheduling           -- UseForScheduling -- bit
                                            ,0 AS UseForStudentGroupTracking -- UseForStudentGroupTracking bit NULL CONSTRAINT DF_adLeadGroups_UseForStudentGroupTracking DEFAULT ((0))
                                            ,@CampusId AS CampusId
                                            ,0 AS recordkey                  --recordkey
                                            ,1 AS NewRecord
                                      ) A
                            LEFT JOIN bridge.adLeadGroups bLeadGrps ON bLeadGrps.src_RecordKey = 0;
            END;

        ELSE
            BEGIN

                INSERT INTO @LeadGroups
                            SELECT LG.LeadGrpId AS LeadGrpId           -- LeadGrpId -- uniqueidentifier
                                  ,LTRIM(RTRIM(LG.Descrip)) AS Descrip -- Descrip -- varchar(50)
                                  ,LG.StatusId AS StatusId             -- StatusId -- uniqueidentifier
                                  ,@CampusGroupToUseId AS CampGrpId    -- CampGrpId -- uniqueidentifier
                                  ,LG.ModDate AS ModDate               -- ModDate -- datetime
                                  ,LG.ModUser AS ModUser               --  ModUser -- varchar(50)
                                  ,LTRIM(RTRIM(LG.code)) AS code       --  code -- varchar(50)
                                  ,0 AS UseForScheduling               -- UseForScheduling -- bit
                                  ,0 AS UseForStudentGroupTracking     -- UseForStudentGroupTracking bit NULL CONSTRAINT DF_adLeadGroups_UseForStudentGroupTracking DEFAULT ((0))
                                  ,@CampusId AS CampusId
                                  ,0 AS recordkey                      --recordkey
                                  ,0 AS IsNewRecord
                            FROM   FreedomAdvantage..adLeadGroups LG
                            WHERE  LG.code = 'C' AND LG.CampGrpId =@CampusGroupToUseId 

            END;



        ALTER TABLE FreedomAdvantage..adLeadGroups DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adLeadGroups
                    SELECT    tLeadGrp.LeadGrpID AS LeadGrpId               -- uniqueidentifier
                             ,CASE WHEN LEN(LTRIM(RTRIM(tLeadGrp.Descrip))) >= 50 THEN SUBSTRING(LTRIM(RTRIM(tLeadGrp.Descrip)), 0, 49)
                                   ELSE LTRIM(RTRIM(tLeadGrp.Descrip))
                              END AS Descrip                                -- varchar(50)
                             ,tLeadGrp.StatusId AS StatusId                 -- uniqueidentifier
                             ,tLeadGrp.CampGrpId AS CampGrpId               -- uniqueidentifier
                             ,tLeadGrp.ModDate AS ModDate                   -- datetime
                             ,tLeadGrp.ModUser AS ModUser                   -- varchar(50)
                             ,CASE WHEN LEN(LTRIM(RTRIM(tLeadGrp.code))) >= 50 THEN SUBSTRING(LTRIM(RTRIM(tLeadGrp.code)), 0, 49)
                                   ELSE LTRIM(RTRIM(tLeadGrp.code))
                              END AS code                                   -- varchar(50)
                             ,tLeadGrp.UseForScheduling AS UseForScheduling -- bit
                             ,tLeadGrp.UseForStudentGroupTracking
                    FROM      @LeadGroups tLeadGrp
                    LEFT JOIN FreedomAdvantage..adLeadGroups dLeadGrp ON dLeadGrp.LeadGrpId = tLeadGrp.LeadGrpID
                    WHERE     tLeadGrp.IsNewRecord = 1
                              AND NOT EXISTS (
                                             SELECT 1
                                             FROM   FreedomAdvantage..adLeadGroups C
                                             WHERE  C.CampGrpId = @CampusGroupToUseId
                                                    AND C.Descrip = tLeadGrp.Descrip
                                             );

        ALTER TABLE FreedomAdvantage..adLeadGroups ENABLE TRIGGER ALL;


        --delete groups for current campus if conversion was run before for same campus
        DELETE FROM FreedomBridge.bridge.adLeadGroups
        WHERE trg_CampusId = @CampusId;


        INSERT INTO FreedomBridge.bridge.adLeadGroups
                    SELECT    tLeadGrp.LeadGrpID
                             ,tLeadGrp.RecordKey
                             ,tLeadGrp.Descrip
                             ,tLeadGrp.CampusId
                             ,tLeadGrp.CampGrpId
                    FROM      @LeadGroups tLeadGrp
                    LEFT JOIN FreedomBridge.bridge.adLeadGroups bLeadGrp ON bLeadGrp.trg_LeadGrpId = tLeadGrp.LeadGrpID
                                                                            AND bLeadGrp.src_RecordKey = tLeadGrp.RecordKey
                    WHERE     bLeadGrp.trg_LeadGrpId IS NULL
                              AND NOT EXISTS (
                                             SELECT *
                                             FROM   bridge.adLeadGroups E
                                             WHERE  E.trg_CampusId = tLeadGrp.CampusId
                                                    AND E.src_RecordKey = tLeadGrp.RecordKey
                                                    AND E.src_Descrip = tLeadGrp.Descrip
                                                    AND E.trg_CampusGroupId = tLeadGrp.CampGrpId
                                             );


        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeadGroups]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
