SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adEdLvls]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dEd.EdLvlDescrip IS NULL THEN NEWID()
                    ELSE dEd.EdLvlId
               END AS EdLvlId                                                           -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS EdLvlCode -- varchar(50)
             , dStatus.StatusId AS StatusId                                             -- uniqueidentifier
             , FLook.Description AS Descrip                                             -- varchar(50)
             , dCampGrp.CampGrpId AS CampGrpId                                          -- uniqueidentifier
             , 'sa' AS ModUser                                                          -- varchar(50)
             , GETDATE() AS ModDate                                                     -- datetime
             , FLook.ID
        INTO   #EdLvls
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adEdLvls dEd ON dEd.CampGrpId = dCampGrp.CampGrpId
                                                    AND dEd.EdLvlDescrip = FLook.Description
        WHERE  FLook.LookupName = 'EducationLevel';

        ALTER TABLE FreedomAdvantage..adEdLvls DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adEdLvls
                    SELECT tEd.EdLvlId
                         , tEd.EdLvlCode
                         , tEd.StatusId
                         , tEd.Descrip
                         , tEd.CampGrpId
                         , tEd.ModUser
                         , tEd.ModDate
                    FROM   #EdLvls tEd
                    LEFT JOIN FreedomAdvantage..adEdLvls dEd ON dEd.CampGrpId = tEd.CampGrpId
                                                                AND dEd.EdLvlId = tEd.EdLvlId
                    WHERE  dEd.EdLvlDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..adEdLvls ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adEdLvls')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adEdLvls
                    (
                        trg_EdLvlId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adEdLvls
                    SELECT tEd.EdLvlId
                         , tEd.Descrip
                         , tEd.ID
                    FROM   #EdLvls tEd
                    LEFT JOIN FreedomBridge.bridge.adEdLvls bEd ON bEd.trg_EdLvlId = tEd.EdLvlId
                    WHERE  bEd.trg_EdLvlId IS NULL;

        DROP TABLE #EdLvls;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adEdLvls]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]
        COMMIT TRANSACTION;

    END;
GO
