SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-05-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGradeScales]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /****** Use staging table provided by client to gather grade scale information to insert into arGradeScales table
		******* and remove duplicates ******/
        SELECT CASE WHEN dGrdScale.Descrip IS NULL THEN NEWID()
                    ELSE dGrdScale.GrdScaleId
               END AS GrdScaleId -- uniqueidentifier
             , GrdScale.InstructorId
             , GrdScale.StatusId
             , GrdScale.Descrip
             , GrdScale.CampGrpId
             , GrdScale.GrdSystemId
             , GrdScale.ModUser
             , GrdScale.ModDate
        INTO   #GrdScale
        FROM   (
                   SELECT DISTINCT CAST(NULL AS UNIQUEIDENTIFIER) AS InstructorId -- uniqueidentifier
                        , dStatus.StatusId AS StatusId                            -- uniqueidentifier
                        , bGrdSys.src_Description AS Descrip                      -- varchar(80)
                        , dCampGrp.CampGrpId AS CampGrpId                         -- uniqueidentifier
                        , bGrdSys.trg_GradeSystemId AS GrdSystemId                -- uniqueidentifier
                        , 'sa' AS ModUser                                         -- varchar(50)
                        , GETDATE() AS ModDate                                    -- datetime
                   FROM   FreedomBridge.bridge.arGradeSystems bGrdSys
                   INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
                   INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
               ) GrdScale
        LEFT JOIN FreedomAdvantage..arGradeScales dGrdScale ON dGrdScale.CampGrpId = GrdScale.CampGrpId
                                                               AND dGrdScale.Descrip = GrdScale.Descrip;


        ALTER TABLE FreedomAdvantage..arGradeScales DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGradeScales
                    SELECT tGrdScale.GrdScaleId
                         , tGrdScale.InstructorId
                         , tGrdScale.StatusId
                         , tGrdScale.Descrip
                         , tGrdScale.CampGrpId
                         , tGrdScale.GrdSystemId
                         , tGrdScale.ModUser
                         , tGrdScale.ModDate
                    FROM   #GrdScale tGrdScale
                    LEFT JOIN FreedomAdvantage..arGradeScales dGrdScale ON dGrdScale.CampGrpId = tGrdScale.CampGrpId
                                                                           AND dGrdScale.Descrip = tGrdScale.Descrip
                    WHERE  dGrdScale.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..arGradeScales ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGradeScales')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGradeScales
                    (
                        trg_GradeScaleId UNIQUEIDENTIFIER
                      , trg_GrdSystemId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Description
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGradeScales
                    SELECT tGrdScale.GrdScaleId
                         , tGrdScale.GrdSystemId
                         , tGrdScale.Descrip
                         , @CampusId
                    FROM   #GrdScale tGrdScale
                    LEFT JOIN FreedomBridge.bridge.arGradeScales bGrdScale ON bGrdScale.src_Description = tGrdScale.Descrip
                                                                              AND bGrdScale.trg_CampusId = @CampusId
                    WHERE  bGrdScale.trg_GradeScaleId IS NULL;

        DROP TABLE #GrdScale;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGradeScales]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
