SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-15-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_atClsSectAttendance]
AS
BEGIN
    --	 SET NOCOUNT ON added to prevent extra result sets from
    --	 interfering with SELECT statements.
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DECLARE @CampusId UNIQUEIDENTIFIER,
            @SchoolID INT;

    DECLARE @CreateExceptionList INT = 0;

    SELECT Entity,
           Value
    INTO #Settings
    FROM dbo.udf_GetSettingsList();

    SELECT @CampusId = Value
    FROM #Settings
    WHERE Entity = 'CampusID';

    SELECT @SchoolID = Value
    FROM #Settings
    WHERE Entity = 'SchoolID';

    SELECT ROW_NUMBER() OVER (ORDER BY compareFrom.RecordKey ASC) AS RowNumber,
           compareFrom.RecordKey recordKeyFrom,
           compareFrom.StudrecKey,
           compareTo.RecordKey recordKeyTo
    INTO #ComparedParentChildAtendanceRecords
    FROM Freedom..ATTENDMO_FIL compareFrom
        LEFT JOIN Freedom..ATTENDMO_FIL compareTo
            ON compareFrom.RecordKey = compareTo.NextRec
               AND compareTo.StudrecKey = compareFrom.StudrecKey
    ORDER BY compareFrom.RecordKey ASC;

    SELECT MAX(RowNumber) RowNumber,
           StudrecKey
    INTO #LastRecordKeyforCompare
    FROM #ComparedParentChildAtendanceRecords
    GROUP BY StudrecKey;

    SELECT parent.recordKeyFrom AS recordKey
    INTO #ATTENDMO_FIL
    FROM #ComparedParentChildAtendanceRecords parent
    WHERE parent.recordKeyTo IS NOT NULL
          OR EXISTS
    (
        SELECT recordKeyFrom
        FROM #LastRecordKeyforCompare child
        WHERE parent.RowNumber = child.RowNumber
              AND parent.StudrecKey = child.StudrecKey
    );

    CREATE NONCLUSTERED INDEX IX_TMP0 ON #ATTENDMO_FIL (recordKey ASC);

    SELECT DISTINCT
           actual.*
    INTO #FilteredFreedomAttendance
    FROM Freedom.dbo.ATTENDMO_FIL actual
        INNER JOIN #ATTENDMO_FIL child
            ON child.recordKey = actual.RecordKey
    WHERE actual.Year > 0
          AND actual.Month > 0;


    CREATE NONCLUSTERED INDEX IX_TMP01
    ON #FilteredFreedomAttendance (RecordKey ASC);


    SELECT unpiv.RecordKey,
           unpiv.StudrecKey,
           unpiv.NextRec,
           unpiv.Year,
           unpiv.Month,
           SUBSTRING(unpiv.actualColumName, CHARINDEX('_', unpiv.actualColumName) + 1, LEN(unpiv.actualColumName)) AS day,
           CASE
               WHEN unpiv.actualHours = 32763 THEN
                   unpiv.PrivMonthTot + unpiv.PrivMonthMakeup
               ELSE
                   unpiv.actualHours
           END AS actualHours,
           --,unpiv.actualHours / 100.00 AS Actual
           DAY(EOMONTH(CONVERT(
                                  DATETIME,
                                  CONVERT(VARCHAR(20), unpiv.Year) + '-' + CONVERT(VARCHAR(20), unpiv.Month) + '-1'
                              )
                      )
              ) lastDayOfTheMonth,
           CASE
               WHEN unpiv.actualHours = 32763
                    AND unpiv.actualColumName = 'PrivDayHours_'
                                                + CONVERT(
                                                             VARCHAR(2),
                                                             DAY(EOMONTH(CONVERT(
                                                                                    DATETIME,
                                                                                    CONVERT(VARCHAR(20), unpiv.Year)
                                                                                    + '-'
                                                                                    + CONVERT(VARCHAR(20), unpiv.Month)
                                                                                    + '-1'
                                                                                )
                                                                        )
                                                                )
                                                         ) THEN
           (unpiv.PrivMonthTot)
               WHEN unpiv.actualHours = 32763
                    AND unpiv.actualColumName <> 'PrivDayHours_'
                                                 + CONVERT(
                                                              VARCHAR(2),
                                                              DAY(EOMONTH(CONVERT(
                                                                                     DATETIME,
                                                                                     CONVERT(VARCHAR(20), unpiv.Year)
                                                                                     + '-'
                                                                                     + CONVERT(VARCHAR(20), unpiv.Month)
                                                                                     + '-1'
                                                                                 )
                                                                         )
                                                                 )
                                                          ) THEN
           (0)
               WHEN unpiv.actualHours = 32760 THEN
                   0
               WHEN unpiv.actualHours = 32761 THEN
                   0
               WHEN unpiv.actualHours = 32762 THEN
                   0
               WHEN unpiv.actualHours = 32765 THEN
                   0
               WHEN unpiv.actualHours = 32766 THEN
                   0
               WHEN unpiv.actualHours = 32767 THEN
                   0
               ELSE
                   unpiv.actualHours / 100.00
           END AS Actual,
           unpiv.actualColumName,
           CASE
               WHEN unpiv.actualHours = 32763
                    AND unpiv.actualColumName = 'PrivDayHours_'
                                                + CONVERT(
                                                             VARCHAR(2),
                                                             DAY(EOMONTH(CONVERT(
                                                                                    DATETIME,
                                                                                    CONVERT(VARCHAR(20), unpiv.Year)
                                                                                    + '-'
                                                                                    + CONVERT(VARCHAR(20), unpiv.Month)
                                                                                    + '-1'
                                                                                )
                                                                        )
                                                                )
                                                         ) THEN
                   1
               ELSE
                   0
           END AS ShouldPostOnLastDay
    INTO #ActualRecords
    FROM #FilteredFreedomAttendance actual
        UNPIVOT
        (
            actualHours
            FOR actualColumName IN (actual.PrivDayHours_1, actual.PrivDayHours_2, actual.PrivDayHours_3,
                                    actual.PrivDayHours_4, actual.PrivDayHours_5, actual.PrivDayHours_6,
                                    actual.PrivDayHours_7, actual.PrivDayHours_8, actual.PrivDayHours_9,
                                    actual.PrivDayHours_10, actual.PrivDayHours_11, actual.PrivDayHours_12,
                                    actual.PrivDayHours_13, actual.PrivDayHours_14, actual.PrivDayHours_15,
                                    actual.PrivDayHours_16, actual.PrivDayHours_17, actual.PrivDayHours_18,
                                    actual.PrivDayHours_19, actual.PrivDayHours_20, actual.PrivDayHours_21,
                                    actual.PrivDayHours_22, actual.PrivDayHours_23, actual.PrivDayHours_24,
                                    actual.PrivDayHours_25, actual.PrivDayHours_26, actual.PrivDayHours_27,
                                    actual.PrivDayHours_28, actual.PrivDayHours_29, actual.PrivDayHours_30,
                                    actual.PrivDayHours_31
                                   )
        ) unpiv
    --
    WHERE unpiv.actualHours >= 0
          AND unpiv.actualHours NOT IN ( 32760, 32761, 32762, 32764, 32765, 32767 );





    CREATE NONCLUSTERED INDEX IX_TMP1
    ON #ActualRecords (
                          RecordKey ASC,
                          StudrecKey ASC,
                          NextRec ASC,
                          Year ASC,
                          Month ASC,
                          day ASC
                      );


    SELECT 'Actual Records created';
    SELECT COUNT(*)
    FROM #ActualRecords;

    SELECT bridgeEnrollments.trg_StuEnrollId enrollmentId,
           attendance.RecordKey,
           attendance.StudrecKey,
           attendance.NextRec,
           attendance.Year,
           attendance.Month,
           attendance.day,
           attendance.actualHours,
           attendance.Actual,
           attendance.ShouldPostOnLastDay,
           attendance.lastDayOfTheMonth,
           NEWID() AS ID
    INTO #enrollmentActualAttendance
    FROM #ActualRecords attendance
        INNER JOIN Freedom..STUDREC freedomEnrollment
            ON freedomEnrollment.RecordKey = attendance.StudrecKey
        INNER JOIN bridge.arStuEnrollments bridgeEnrollments
            ON bridgeEnrollments.src_RecordKey = freedomEnrollment.RecordKey
               AND bridgeEnrollments.trg_CampusId = @CampusId
    WHERE attendance.day <= attendance.lastDayOfTheMonth;

    CREATE NONCLUSTERED INDEX IX_TMP2
    ON #enrollmentActualAttendance (
                                       RecordKey ASC,
                                       StudrecKey ASC,
                                       NextRec ASC,
                                       Year ASC,
                                       Month ASC,
                                       day ASC
                                   );

    SELECT 'Enrolment Actual Attendance created';
    SELECT COUNT(*)
    FROM #enrollmentActualAttendance;

    SELECT unpiv.RecordKey,
           StudrecKey,
           NextRec,
           Year,
           Month,
           SUBSTRING(scheduledColumnName, CHARINDEX('_', scheduledColumnName) + 1, LEN(scheduledColumnName)) AS day,
           CASE
               WHEN scheduledHours = 32763 THEN
                   unpiv.PrivMonthSched
               ELSE
                   scheduledHours
           END AS scheduledHours,
           CASE
               WHEN unpiv.scheduledHours = 32763
                    AND unpiv.scheduledColumnName = 'PrivDaySchedHours_'
                                                    + CONVERT(
                                                                 VARCHAR(2),
                                                                 DAY(EOMONTH(CONVERT(
                                                                                        DATETIME,
                                                                                        CONVERT(VARCHAR(20), unpiv.Year)
                                                                                        + '-'
                                                                                        + CONVERT(
                                                                                                     VARCHAR(20),
                                                                                                     unpiv.Month
                                                                                                 ) + '-1'
                                                                                    )
                                                                            )
                                                                    )
                                                             ) THEN
           (unpiv.PrivMonthSched)
               WHEN unpiv.scheduledHours = 32763
                    AND unpiv.scheduledColumnName <> 'PrivDaySchedHours_'
                                                     + CONVERT(
                                                                  VARCHAR(2),
                                                                  DAY(EOMONTH(CONVERT(
                                                                                         DATETIME,
                                                                                         CONVERT(
                                                                                                    VARCHAR(20),
                                                                                                    unpiv.Year
                                                                                                ) + '-'
                                                                                         + CONVERT(
                                                                                                      VARCHAR(20),
                                                                                                      unpiv.Month
                                                                                                  ) + '-1'
                                                                                     )
                                                                             )
                                                                     )
                                                              ) THEN
           (0)
               WHEN unpiv.scheduledHours = 32760 THEN
                   0
               WHEN unpiv.scheduledHours = 32761 THEN
                   0
               WHEN unpiv.scheduledHours = 32762 THEN
                   0
               WHEN unpiv.scheduledHours = 32765 THEN
                   0
               WHEN unpiv.scheduledHours = 32766 THEN
                   0
               WHEN unpiv.scheduledHours = 32767 THEN
                   0
               ELSE
                   scheduledHours / 100.00
           END AS Scheduled
    INTO #ScheduledRecords
    FROM #FilteredFreedomAttendance freedomAttendance
        UNPIVOT
        (
            scheduledHours
            FOR scheduledColumnName IN (PrivDaySchedHours_1, PrivDaySchedHours_2, PrivDaySchedHours_3,
                                        PrivDaySchedHours_4, PrivDaySchedHours_5, PrivDaySchedHours_6,
                                        PrivDaySchedHours_7, PrivDaySchedHours_8, PrivDaySchedHours_9,
                                        PrivDaySchedHours_10, PrivDaySchedHours_11, PrivDaySchedHours_12,
                                        PrivDaySchedHours_13, PrivDaySchedHours_14, PrivDaySchedHours_15,
                                        PrivDaySchedHours_16, PrivDaySchedHours_17, PrivDaySchedHours_18,
                                        PrivDaySchedHours_19, PrivDaySchedHours_20, PrivDaySchedHours_21,
                                        PrivDaySchedHours_22, PrivDaySchedHours_23, PrivDaySchedHours_24,
                                        PrivDaySchedHours_25, PrivDaySchedHours_26, PrivDaySchedHours_27,
                                        PrivDaySchedHours_28, PrivDaySchedHours_29, PrivDaySchedHours_30,
                                        PrivDaySchedHours_31
                                       )
        ) unpiv
    WHERE unpiv.scheduledHours > 0;


    CREATE NONCLUSTERED INDEX IX_TMP2
    ON #ScheduledRecords (
                             RecordKey ASC,
                             StudrecKey ASC,
                             NextRec ASC,
                             Year ASC,
                             Month ASC,
                             day ASC
                         );

    SELECT 'Scheduled Records Created';
    SELECT COUNT(*)
    FROM #ScheduledRecords;

    DECLARE @AttendanceExceptionList TABLE
    (
        StudrecKey INT,
        ErrorMessage VARCHAR(MAX)
    );
    IF NOT EXISTS
    (
        SELECT *
        FROM sys.objects
        WHERE object_id = OBJECT_ID(N'FreedomBridge.bridge.AttendanceExceptionList')
              AND type IN ( N'U' )
    )
    BEGIN
        CREATE TABLE bridge.AttendanceExceptionList
        (
            StudrecKey INT,
            ErrorMessage VARCHAR(MAX)
        );

    END;

    DECLARE @firstScheduleID AS UNIQUEIDENTIFIER;


    SELECT bridgeEnrollment.trg_StuEnrollId AS StuEnrollId,
           CASE
               WHEN bridgeSchedules.trg_ScheduleId IS NOT NULL THEN
                   bridgeSchedules.trg_ScheduleId
               ELSE
           (
               SELECT TOP 1
                      trg_ScheduleId
               FROM bridge.arProgSchedules subBridgeSchedules
                   INNER JOIN Freedom..TCSCHED_FIL freedomSchedule
                       ON freedomSchedule.RecordKey = subBridgeSchedules.FreedomScheduleRecordKey
               WHERE subBridgeSchedules.trg_ProgramVersionId = programVersions.PrgVerId
               ORDER BY freedomSchedule.RecordKey
           )
           END AS ScheduleId,
           enrollmentAttendance.StudrecKey,
           enrollmentAttendance.RecordKey,
           CAST(CONVERT(VARCHAR(50), enrollmentAttendance.Year) + '-'
                + CASE
                      WHEN enrollmentAttendance.Month >= 10 THEN
                          CONVERT(VARCHAR(50), enrollmentAttendance.Month)
                      ELSE
                          '0' + CONVERT(VARCHAR(50), enrollmentAttendance.Month)
                  END + '-' + +CASE
                                   WHEN enrollmentAttendance.day >= 10 THEN
                                       CONVERT(VARCHAR(50), enrollmentAttendance.day)
                                   ELSE
                                       '0' + CONVERT(VARCHAR(50), enrollmentAttendance.day)
                               END + ' 12:00:00 AM' AS VARCHAR(50)) AS RecordDate,
           CASE
               WHEN scheduleRecords.Scheduled IS NULL THEN
                   0
               ELSE
                   scheduleRecords.Scheduled
           END AS Scheduled,
           enrollmentAttendance.Actual AS Actual,
           CAST(CONVERT(VARCHAR(50), enrollmentAttendance.Year) + '-'
                + CASE
                      WHEN enrollmentAttendance.Month >= 10 THEN
                          CONVERT(VARCHAR(50), enrollmentAttendance.Month)
                      ELSE
                          '0' + CONVERT(VARCHAR(50), enrollmentAttendance.Month)
                  END + '-' + +CASE
                                   WHEN enrollmentAttendance.day >= 10 THEN
                                       CONVERT(VARCHAR(50), enrollmentAttendance.day)
                                   ELSE
                                       '0' + CONVERT(VARCHAR(50), enrollmentAttendance.day)
                               END + ' 12:00:00 AM' AS VARCHAR(50)) AS ModDate,
           'sa' AS ModUser,
           0 AS isTardy,
           NULL AS PostByException,
           NULL AS Comments,
           NULL AS TardyProcessed,
           0 AS Converted,
           NEWID() AS ID
    INTO #StudentClockAttendanceTemp
    FROM #enrollmentActualAttendance enrollmentAttendance
        LEFT JOIN #ScheduledRecords scheduleRecords
            ON scheduleRecords.RecordKey = enrollmentAttendance.RecordKey
               AND scheduleRecords.Month = enrollmentAttendance.Month
               AND scheduleRecords.NextRec = enrollmentAttendance.NextRec
               AND scheduleRecords.day = enrollmentAttendance.day
               AND scheduleRecords.Year = enrollmentAttendance.Year
        INNER JOIN bridge.arStuEnrollments bridgeEnrollment
            ON bridgeEnrollment.src_RecordKey = enrollmentAttendance.StudrecKey
               AND bridgeEnrollment.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..arStuEnrollments enrollments
            ON enrollments.StuEnrollId = bridgeEnrollment.trg_StuEnrollId
        INNER JOIN FreedomAdvantage..arPrgVersions programVersions
            ON programVersions.PrgVerId = enrollments.PrgVerId
        INNER JOIN Freedom..STUDREC freedomEnrollment
            ON freedomEnrollment.RecordKey = bridgeEnrollment.src_RecordKey
        LEFT JOIN bridge.arProgSchedules bridgeSchedules
            ON freedomEnrollment.PrivTClockSchdRecKey = bridgeSchedules.FreedomScheduleRecordKey
               AND bridgeSchedules.trg_ProgramVersionId = programVersions.PrgVerId;


    SELECT *,
           ROW_NUMBER() OVER (PARTITION BY StuEnrollId,
                                           ScheduleId,
                                           RecordDate
                              ORDER BY StuEnrollId,
                                       ScheduleId,
                                       RecordDate,
                                       ModDate
                             ) AS RowNumber
    INTO #StudentClockAttendance
    FROM #StudentClockAttendanceTemp;


    CREATE NONCLUSTERED INDEX IX_TMP4
    ON #StudentClockAttendance (StudrecKey ASC);


    SELECT 'Student Clock Attendance Created';
    SELECT COUNT(*)
    FROM #StudentClockAttendance;

    IF (@CreateExceptionList = 0)
    BEGIN

        --SELECT RecordDate, CONVERT(DATETIME,  FROM #StudentClockAttendance ORDER BY RecordDate
        PRINT 'dont create exception list';

        ALTER TABLE FreedomAdvantage..arStudentClockAttendance DISABLE TRIGGER ALL;
        SELECT DISTINCT
               I.StuEnrollId,
               I.ScheduleId,
               CAST(I.RecordDate AS DATETIME) AS RecordDate,
               I.Scheduled,
               I.Actual,
               CAST(I.ModDate AS DATETIME) AS ModDate,
               I.ModUser,
               I.isTardy,
               I.PostByException,
               I.Comments,
               I.TardyProcessed,
               I.Converted
        INTO #StudentClockAttendanceCleanTemp
        FROM #StudentClockAttendance I
        WHERE I.RowNumber IN ( 1 )
              AND
              (
                  I.RecordDate IS NOT NULL
                  AND I.ModDate IS NOT NULL
              )
			  AND I.ScheduleId IS NOT NULL;

        INSERT INTO FreedomAdvantage..arStudentClockAttendance
        (
            StuEnrollId,
            ScheduleId,
            RecordDate,
            SchedHours,
            ActualHours,
            ModDate,
            ModUser,
            isTardy,
            PostByException,
            comments,
            TardyProcessed,
            Converted
        )
        SELECT  StuEnrollId,
            ScheduleId,
            RecordDate,
            Scheduled,
            Actual,
            ModDate,
            ModUser,
            isTardy,
            PostByException,
            comments,
            TardyProcessed,
            Converted
        FROM #StudentClockAttendanceCleanTemp;

        ALTER TABLE FreedomAdvantage..arStudentClockAttendance ENABLE TRIGGER ALL;
    --WHERE  NOT EXISTS (
    --                  SELECT TOP 1 *
    --                  FROM   FreedomAdvantage..arStudentClockAttendance E
    --                  WHERE  E.StuEnrollId = I.StuEnrollId
    --                         AND E.ScheduleId = I.ScheduleId
    --                         AND E.RecordDate = I.RecordDate
    --                  );
    END;
    ELSE IF (@CreateExceptionList = 1)
    BEGIN
        SELECT 'inserting student clock attendance';

        SET NOCOUNT ON;
        DECLARE @StudrecKey INT;
        DECLARE enrollmentsCursor CURSOR FOR
        SELECT StudrecKey
        FROM #enrollmentActualAttendance
        GROUP BY StudrecKey;


        OPEN enrollmentsCursor;

        SELECT 'inserting student clock attendance';
        FETCH NEXT FROM enrollmentsCursor
        INTO @StudrecKey;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            BEGIN TRY
                --BEGIN TRANSACTION enrollmentTransaction
                INSERT INTO FreedomAdvantage..arStudentClockAttendance
                (
                    StuEnrollId,
                    ScheduleId,
                    RecordDate,
                    SchedHours,
                    ActualHours,
                    ModDate,
                    ModUser,
                    isTardy,
                    PostByException,
                    comments,
                    TardyProcessed,
                    Converted
                )
                SELECT StuEnrollId,
                       ScheduleId,
                       CONVERT(DATETIME, RecordDate, 103),
                       Scheduled,
                       Actual,
                       CONVERT(DATETIME, ModDate, 103),
                       ModUser,
                       isTardy,
                       PostByException,
                       Comments,
                       TardyProcessed,
                       Converted
                FROM #StudentClockAttendance
				WHERE ScheduleId IS NOT NULL
            --WHERE  enroll

            END TRY
            BEGIN CATCH

                DECLARE @error INT,
                        @message VARCHAR(MAX),
                        @xstate INT;
                SELECT @error = ERROR_NUMBER(),
                       @message = ERROR_MESSAGE(),
                       @xstate = XACT_STATE();

                PRINT 'error found ' + @message;
                INSERT INTO @AttendanceExceptionList
                VALUES
                (@StudrecKey, @message);

            END CATCH;
            FETCH NEXT FROM enrollmentsCursor
            INTO @StudrecKey;
        END;
        CLOSE enrollmentsCursor;
        DEALLOCATE enrollmentsCursor;

    END;

    INSERT INTO bridge.AttendanceExceptionList
    (
        StudrecKey,
        ErrorMessage
    )
    SELECT *
    FROM @AttendanceExceptionList;

    DROP TABLE #ActualRecords;
    DROP TABLE #enrollmentActualAttendance;
    DROP TABLE #ScheduledRecords;
    DROP TABLE #Settings;
    DROP TABLE #StudentClockAttendance;
    DROP TABLE #ComparedParentChildAtendanceRecords;
    DROP TABLE #LastRecordKeyforCompare;
    DROP TABLE #ATTENDMO_FIL;
    DROP TABLE #FilteredFreedomAttendance;
    DROP TABLE #StudentClockAttendanceTemp;
    DROP TABLE #StudentClockAttendanceCleanTemp;

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_atClsSectAttendance]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];

    COMMIT TRAN;
END;
GO
