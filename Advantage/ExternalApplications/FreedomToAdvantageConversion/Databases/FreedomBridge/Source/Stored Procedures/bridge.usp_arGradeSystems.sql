SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGradeSystems]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /****** Use staging table provided by client to gather grade system information to insert into arGradeSystems table
		******* and remove duplicates ******/
        SELECT CASE WHEN dGrdSytem.Descrip IS NULL THEN NEWID()
                    ELSE dGrdSytem.GrdSystemId
               END AS GrdSystemId -- uniqueidentifier
             , GrdSys.Descrip
             , GrdSys.StatusId
             , GrdSys.CampGrpId
             , GrdSys.ModUser
             , GrdSys.ModDate
             , GrdSys.RecordKey
             , @CampusId AS CampusId
        INTO   #GrdScale
        FROM   (
                   SELECT   stGrdSys.GradeSystemDescription AS Descrip -- varchar(80)
                          , dStatus.StatusId AS StatusId               -- uniqueidentifier
                          , dCampGrp.CampGrpId AS CampGrpId            -- uniqueidentifier
                          , 'sa' AS ModUser                            -- varchar(50)
                          , GETDATE() AS ModDate                       -- datetime
                          , @SchoolID AS RecordKey
                   FROM     FreedomBridge.stage.arGradeSystemDetails stGrdSys
                   INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
                   INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                   GROUP BY stGrdSys.GradeSystemDescription
                          , dCampGrp.CampGrpId
                          , dStatus.StatusId
                   UNION ALL
                   SELECT sGrdScale.Description AS Descrip -- varchar(80)
                        , dStatus.StatusId AS StatusId     -- uniqueidentifier
                        , dCampGrp.CampGrpId AS CampGrpId  -- uniqueidentifier
                        , 'sa' AS ModUser                  -- varchar(50)
                        , GETDATE() AS ModDate             -- datetime
                        , sGrdScale.RecordKey
                   FROM   Freedom..GRADESCALE_FIL sGrdScale
                   INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
                   INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
               ) GrdSys
        LEFT JOIN FreedomAdvantage..arGradeSystems dGrdSytem ON dGrdSytem.CampGrpId = GrdSys.CampGrpId
                                                                AND dGrdSytem.Descrip = GrdSys.Descrip;

        ALTER TABLE FreedomAdvantage..arGradeSystems DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGradeSystems
                    SELECT tGrdScale.GrdSystemId
                         , tGrdScale.Descrip
                         , tGrdScale.StatusId
                         , tGrdScale.CampGrpId
                         , tGrdScale.ModUser
                         , tGrdScale.ModDate
                    FROM   #GrdScale tGrdScale
                    LEFT JOIN FreedomAdvantage..arGradeSystems dGrdSystem ON dGrdSystem.CampGrpId = tGrdScale.CampGrpId
                                                                             AND dGrdSystem.Descrip = tGrdScale.Descrip
                    WHERE  dGrdSystem.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..arGradeSystems ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGradeSystems')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGradeSystems
                    (
                        trg_GradeSystemId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , src_RecordKey INT
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Description
                            , src_RecordKey
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGradeSystems
                    SELECT tGrdScale.GrdSystemId
                         , tGrdScale.Descrip
                         , tGrdScale.RecordKey
                         , @CampusId AS CampusId
                    FROM   #GrdScale tGrdScale
                    LEFT JOIN FreedomBridge.bridge.arGradeSystems bGrdSystem ON bGrdSystem.src_Description = tGrdScale.Descrip
                                                                                AND (
                                                                                        bGrdSystem.src_RecordKey = @SchoolID
                                                                                        OR bGrdSystem.src_RecordKey = tGrdScale.RecordKey
                                                                                    )
                                                                                AND bGrdSystem.trg_CampusId = tGrdScale.CampusId
                    WHERE  bGrdSystem.trg_GradeSystemId IS NULL;

        DROP TABLE #GrdScale;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGradeSystems]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
