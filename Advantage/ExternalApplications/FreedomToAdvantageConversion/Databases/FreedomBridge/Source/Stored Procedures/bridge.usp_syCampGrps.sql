SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-24-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syCampGrps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CampGrpId = @CampGrpId
        FROM   FreedomAdvantage..syCampGrps
        WHERE  CampGrpDescrip = 'ALL';

		SELECT NEWID() AS CampGrpId,*
		   INTO   #CampGrps
		FROM ( 
		
        /**** Create both All and individual campus groups for each campus ****/
        SELECT DISTINCT  sSchool.SchoolID AS CampGrpCode            -- varchar(12)
                                                          --,sSchool.PhysAddrCity AS CampGrpDescrip -- varchar(50)
             , REPLACE(REPLACE(sSchool.Name,'''',''),'`','') AS CampGrpDescrip             -- varchar(50)
             , dStatus.StatusId AS StatusId               -- uniqueidentifier
             , 'sa' AS ModUser                            -- varchar(50)
             , GETDATE() AS ModDate                       -- datetime
             , 0 AS IsAllCampusGrp                        -- bit
             , CAST(NULL AS UNIQUEIDENTIFIER) AS CampusId -- uniqueidentifier
        
        FROM   Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        WHERE NOT EXISTS ( SELECT 1 FROM  FreedomAdvantage..syCampGrps dCampGrp WHERE dCampGrp.CampGrpDescrip = sSchool.Name  
                                                           AND dCampGrp.CampGrpCode = sSchool.SchoolID)) Camp
														
        --WHERE  dCampGrp.CampGrpId IS NULL;

        INSERT INTO FreedomAdvantage..syCampGrps
                    SELECT tCampGrp.CampGrpId AS CampGrpId           -- uniqueidentifier
                         , tCampGrp.CampGrpCode AS CampGrpCode       -- varchar(12)
                         , tCampGrp.CampGrpDescrip AS CampGrpDescrip -- varchar(50)
                         , tCampGrp.StatusId AS StatusId             -- uniqueidentifier
                         , tCampGrp.ModUser AS ModUser               -- varchar(50)
                         , tCampGrp.ModDate AS ModDate               -- datetime
                         , tCampGrp.IsAllCampusGrp AS IsAllCampusGrp -- bit
                         , tCampGrp.CampusId AS CampusId             -- uniqueidentifier
                    FROM   #CampGrps tCampGrp;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syCampGrps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syCampGrps
                    (
                        trg_syCampGrpId UNIQUEIDENTIFIER
                      , src_SchoolId INT NOT NULL
                      , src_Name VARCHAR(50)
                      , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                      , CONSTRAINT pk_SchoolId_CampusId
                            PRIMARY KEY CLUSTERED
                                (
                                    src_SchoolId ASC
                                  , trg_CampusId ASC
                                )
                            WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                    ) ON [PRIMARY];
            END;

        INSERT INTO FreedomBridge.bridge.syCampGrps
                    SELECT tCampGrp.CampGrpId
                         , tCampGrp.CampGrpCode
                         , tCampGrp.CampGrpDescrip
                         , @CampusId AS CampusId
                    FROM   #CampGrps tCampGrp;

        DROP TABLE #CampGrps;
        DROP TABLE #Settings;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syCampGrps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;

GO
