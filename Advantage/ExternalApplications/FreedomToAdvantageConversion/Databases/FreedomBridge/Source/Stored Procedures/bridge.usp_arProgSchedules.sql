SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arProgSchedules]
AS
    BEGIN

        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@CampusGroupId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER
               ,@InactiveStatusId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@ConversionUser VARCHAR(50);


        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SELECT @CampusGroupId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @ConversionUser = Value
        FROM   #Settings
        WHERE  Entity = 'ConversionUser';


        SET @ActiveStatusId = (
                              SELECT   TOP ( 1 ) StatusId
                              FROM     FreedomAdvantage..syStatuses
                              WHERE    StatusCode = 'A'
                              ORDER BY StatusCode
                              );
        SET @InactiveStatusId = (
                                SELECT   TOP ( 1 ) StatusId
                                FROM     FreedomAdvantage..syStatuses
                                WHERE    StatusCode = 'I'
                                ORDER BY StatusCode
                                );


        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ------------------- BEGIN OF SCHEDULES CREATION FOR PROGRAM THAT USES CLOCK ATTENDANCE ------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------

        SELECT     NEWID() AS ScheduleId                                                                     -- ScheduleId - uniqueidentifier
                  ,SUBSTRING(UPPER(LTRIM(RTRIM(REPLACE(freedomSchedules.Descrip, ' ', '')))), 0, 12) AS Code -- Code - varchar(12)
                  ,LTRIM(RTRIM(freedomSchedules.Descrip)) AS Descrip                                         -- Descrip - varchar(50)
                  ,freedomSchedules.Active AS Active                                                         -- Active - bit       
                  ,freedomStudentEnrollment.CourseNumEnrolledIn FreedomProgramVersionId                      -- PrgVerId - int
                  ,NULL AS UseFlexTime                                                                       -- UseFlexTime - bit
                  ,@ConversionUser AS ModUser                                                                -- ModUser - varchar(50)
                  ,GETDATE() AS ModDate                                                                      -- ModDate - smalldatetime
                  ,freedomSchedules.RecordKey FreedomScheduleRecordKey
                  ,bridgeProgramVersion.trg_PrgVerId AS PrgVerId                                             --PrgVerId UNIQUEIDENTIFIER
                  ,advantageProgramVersion.CampGrpId
        --,advantageCampusGroup.CampusId
        INTO       #Schedules
        FROM       Freedom..TCSCHED_FIL AS freedomSchedules
        INNER JOIN Freedom..TCSCHED_DAY AS freedomScheduleDetail ON freedomScheduleDetail.SchedNum = freedomSchedules.RecordKey
        INNER JOIN Freedom..STUDREC AS freedomStudentEnrollment ON freedomStudentEnrollment.PrivTClockSchdRecKey = freedomSchedules.RecordKey
        INNER JOIN FreedomBridge.bridge.arPrgVersions bridgeProgramVersion ON bridgeProgramVersion.src_RecordKey = freedomStudentEnrollment.CourseNumEnrolledIn
        INNER JOIN FreedomAdvantage..arPrgVersions advantageProgramVersion ON advantageProgramVersion.PrgVerId = bridgeProgramVersion.trg_PrgVerId
        GROUP BY   freedomSchedules.Descrip
                  ,freedomSchedules.Active
                  ,freedomStudentEnrollment.CourseNumEnrolledIn
                  ,freedomSchedules.RecordKey
                  ,bridgeProgramVersion.trg_PrgVerId
                  ,advantageProgramVersion.CampGrpId;

        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arProgSchedules')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE bridge.arProgSchedules
                    (
                        trg_ScheduleId UNIQUEIDENTIFIER
                       ,src_Name VARCHAR(255)
                       ,trg_CampGrpId UNIQUEIDENTIFIER
                       ,trg_ProgramVersionId UNIQUEIDENTIFIER
                       ,FreedomScheduleRecordKey INT
                            PRIMARY KEY CLUSTERED
                            (
                            src_Name
                           ,trg_CampGrpId
                           ,trg_ProgramVersionId
                           ,FreedomScheduleRecordKey
                            )
                    );
            END;


        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arProgSchedulesDetails')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE bridge.arProgSchedulesDetails
                    (
                        ScheduleDetailId UNIQUEIDENTIFIER
                       ,ScheduleId UNIQUEIDENTIFIER
                       ,FreedomScheduleRecordKey INT
                       ,FreedomScheduleDetailRecordKey INT
                            PRIMARY KEY CLUSTERED
                            (
                            ScheduleId
                           ,FreedomScheduleRecordKey
                           ,FreedomScheduleDetailRecordKey
                            )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arProgSchedules
                    SELECT    ScheduleId
                             ,Descrip
                             ,CampGrpId
                             ,PrgVerId
                             ,schedules.FreedomScheduleRecordKey
                    FROM      #Schedules schedules
                    LEFT JOIN FreedomBridge.bridge.arProgSchedules bridgeSchedule ON bridgeSchedule.FreedomScheduleRecordKey = schedules.FreedomScheduleRecordKey
                                                                                     AND schedules.CampGrpId = bridgeSchedule.trg_CampGrpId
                                                                                     AND schedules.PrgVerId = bridgeSchedule.trg_ProgramVersionId
                    WHERE     bridgeSchedule.trg_ScheduleId IS NULL;

        ALTER TABLE FreedomAdvantage..arProgSchedules DISABLE TRIGGER ALL;



        INSERT INTO FreedomAdvantage..arProgSchedules (
                                                      ScheduleId
                                                     ,Code
                                                     ,Descrip
                                                     ,PrgVerId
                                                     ,Active
                                                     ,UseFlexTime
                                                     ,ModUser
                                                     ,ModDate
                                                      )
                    SELECT    schedules.ScheduleId
                             ,schedules.Code
                             ,schedules.Descrip
                             ,schedules.PrgVerId
                             ,schedules.Active
                             ,schedules.UseFlexTime
                             ,schedules.ModUser
                             ,schedules.ModDate
                    FROM      #Schedules schedules
                    LEFT JOIN FreedomAdvantage..arProgSchedules advantageProgramScehdule ON advantageProgramScehdule.PrgVerId = schedules.PrgVerId
                                                                                            AND advantageProgramScehdule.Descrip = schedules.Descrip
                    WHERE     advantageProgramScehdule.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..arProgSchedules ENABLE TRIGGER ALL;

        SELECT     NEWID() AS ScheduleDetailId
                  ,schedules.ScheduleId AS ScheduleId
                  ,freedomScheduleDetail.DayOfWeek AS DayOfWeek
                  ,CASE WHEN LEN(freedomScheduleDetail.TimeIn) <= 3 THEN CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.TimeIn, 2, 0, ':'))
                        ELSE CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.TimeIn, 3, 0, ':'))
                   END AS timein
                  ,CASE WHEN LEN(freedomScheduleDetail.LunchIn) <= 3 THEN CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.LunchOut, 2, 0, ':'))
                        ELSE CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.LunchIn, 3, 0, ':'))
                   END AS lunchin
                  ,CASE WHEN LEN(freedomScheduleDetail.LunchOut) <= 3 THEN CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.LunchOut, 2, 0, ':'))
                        ELSE CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.LunchOut, 3, 0, ':'))
                   END AS lunchout
                  ,CASE WHEN LEN(freedomScheduleDetail.TimeOut) <= 3 THEN CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.TimeOut, 2, 0, ':'))
                        ELSE CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.TimeOut, 3, 0, ':'))
                   END AS timeout
                  ,NULL AS maxnolunch   --CHECK WITH PO
                  ,freedomScheduleDetail.AllowEarlyPunchIn AS AllowEarlyPunchIn
                  ,freedomScheduleDetail.AllowLatePunchOut AS AllowLatePunchOut
                  ,freedomScheduleDetail.AllowExtraHours AS AllowExtraHours
                  ,freedomScheduleDetail.LimitPunchInTime AS check_tardyin
                  ,CASE WHEN LEN(freedomScheduleDetail.MaxPunchIn) <= 3 THEN CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.MaxPunchIn, 2, 0, ':'))
                        ELSE CONVERT(DATETIME, '1/1/1990 ' + STUFF(freedomScheduleDetail.MaxPunchIn, 3, 0, ':'))
                   END AS max_beforetardy
                  ,NULL AS tardy_intime --CHECK WITH PO
                  ,freedomScheduleDetail.RecordKey FreedomScheduleDetailId
                  ,freedomSchedules.RecordKey FreedomScheduleId
                  ,freedomScheduleDetail.PrivSchedHours * 1.00 AS TotalScheduledHours
                  ,freedomScheduleDetail.MinHours AS MinimumHoursToBePresent
        INTO       #SchedulesDetails
        FROM       #Schedules schedules
        INNER JOIN Freedom..TCSCHED_FIL AS freedomSchedules ON freedomSchedules.RecordKey = schedules.FreedomScheduleRecordKey
        INNER JOIN Freedom..TCSCHED_DAY AS freedomScheduleDetail ON freedomScheduleDetail.SchedNum = schedules.FreedomScheduleRecordKey
        INNER JOIN bridge.arProgSchedules AS bridgeSchedule ON bridgeSchedule.FreedomScheduleRecordKey = freedomSchedules.RecordKey
                                                               AND schedules.CampGrpId = bridgeSchedule.trg_CampGrpId
                                                               AND schedules.PrgVerId = bridgeSchedule.trg_ProgramVersionId
        LEFT JOIN  bridge.arProgSchedulesDetails bridgeDetails ON bridgeDetails.FreedomScheduleRecordKey = freedomScheduleDetail.RecordKey
                                                                  AND bridgeDetails.FreedomScheduleDetailRecordKey = freedomScheduleDetail.RecordKey
        WHERE      bridgeDetails.ScheduleId IS NULL;


        ALTER TABLE FreedomAdvantage..arProgScheduleDetails DISABLE TRIGGER ALL;


        INSERT INTO FreedomAdvantage..arProgScheduleDetails (
                                                            ScheduleDetailId
                                                           ,ScheduleId
                                                           ,dw
                                                           ,total
                                                           ,timein
                                                           ,lunchin
                                                           ,lunchout
                                                           ,timeout
                                                           ,maxnolunch
                                                           ,allow_earlyin
                                                           ,allow_lateout
                                                           ,allow_extrahours
                                                           ,check_tardyin
                                                           ,max_beforetardy
                                                           ,tardy_intime
                                                           ,MinimumHoursToBePresent
                                                            )
                    SELECT    details.ScheduleDetailId
                             ,details.ScheduleId
                             --,details.DayOfWeek
                             ,CASE WHEN details.DayOfWeek = 1 THEN 7
                                   ELSE ( details.DayOfWeek - 1 )
                              END AS DayOfWeek
                             ,details.TotalScheduledHours Total
                             ,details.timein
                             ,details.lunchin
                             ,details.lunchout
                             ,details.timeout
                             ,details.maxnolunch
                             ,details.AllowEarlyPunchIn
                             ,details.AllowLatePunchOut
                             ,details.AllowExtraHours
                             ,details.check_tardyin
                             ,details.max_beforetardy
                             ,details.tardy_intime
                             ,details.MinimumHoursToBePresent
                    FROM      #SchedulesDetails details
                    LEFT JOIN bridge.arProgSchedulesDetails bridgeDetails ON bridgeDetails.FreedomScheduleRecordKey = details.FreedomScheduleId
                                                                             AND bridgeDetails.FreedomScheduleDetailRecordKey = details.FreedomScheduleDetailId
                    WHERE     bridgeDetails.ScheduleId IS NULL;


        UPDATE FreedomAdvantage..arProgScheduleDetails
        SET    total = CASE WHEN total IS NULL THEN DATEPART(HOUR, ( timeout ) - ( timein ))
                            ELSE total
                       END;

        ALTER TABLE FreedomAdvantage..arProgScheduleDetails ENABLE TRIGGER ALL;

   


        INSERT INTO bridge.arProgSchedulesDetails
                    SELECT    details.ScheduleDetailId
                             ,details.ScheduleId
                             ,details.FreedomScheduleId
                             ,details.FreedomScheduleDetailId
                    FROM      #SchedulesDetails details
                    LEFT JOIN bridge.arProgSchedulesDetails bridgeDetails ON bridgeDetails.FreedomScheduleRecordKey = details.FreedomScheduleId
                                                                             AND bridgeDetails.FreedomScheduleDetailRecordKey = details.FreedomScheduleDetailId
                    WHERE     bridgeDetails.ScheduleId IS NULL;

        ----------------------------------------------------------------------------------------
        ---FIX SCHEDULE MAX HOURS
        ----------------------------------------------------------------------------------------

        UPDATE     scheduleDetails
        SET        scheduleDetails.maxnolunch = freedomSchedule.CheckForLunchMark
        FROM       FreedomAdvantage..arProgSchedules schedules
        INNER JOIN FreedomAdvantage..arProgScheduleDetails scheduleDetails ON scheduleDetails.ScheduleId = schedules.ScheduleId
        INNER JOIN bridge.arProgSchedulesDetails bridgeScheduleDetails ON bridgeScheduleDetails.ScheduleDetailId = scheduleDetails.ScheduleDetailId
        INNER JOIN Freedom..TCSCHED_DAY freedomSchedule ON freedomSchedule.RecordKey = bridgeScheduleDetails.FreedomScheduleDetailRecordKey;
        DROP TABLE #SchedulesDetails;


        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ------------------- END OF SCHEDULES CREATION FOR PROGRAM THAT USES CLOCK ATTENDANCE --------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------




        --THROW 1, 'test', 1;
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ------------------- BEGIN OF SCHEDULES CREATION FOR PROGRAM THAT NOT USES CLOCK ATTENDANCE --------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------

        SELECT    DISTINCT AttndSchdHrsMon
                          ,AttndSchdHrsTue
                          ,AttndSchdHrsWed
                          ,AttndSchdHrsThr
                          ,AttndSchdHrsFri
                          ,AttndSchdHrsSat
                          ,AttndSchdHrsSun
                          ,B_PV.trg_PrgVerId AS ProgramVersionId
                          ,B_PV.src_RecordKey AS FreedomProgramVersion
                          ,C.Name AS ProgramVersionName
                          ,C.CourseCode AS ProgramVersionCode
                          ,( CASE WHEN R.AttndSchdHrsMon > 0 THEN 'M'
                                  ELSE ''
                             END
                           ) + ( CASE WHEN R.AttndSchdHrsTue > 0 THEN 'T'
                                      ELSE ''
                                 END
                               ) + ( CASE WHEN R.AttndSchdHrsWed > 0 THEN 'W'
                                          ELSE ''
                                     END
                                   ) + ( CASE WHEN R.AttndSchdHrsThr > 0 THEN 'R'
                                              ELSE ''
                                         END
                                       ) + ( CASE WHEN R.AttndSchdHrsFri > 0 THEN 'F'
                                                  ELSE ''
                                             END
                                           ) + ( CASE WHEN R.AttndSchdHrsSat > 0 THEN 'S'
                                                      ELSE ''
                                                 END
                                               ) + ( CASE WHEN R.AttndSchdHrsSun > 0 THEN 'Su'
                                                          ELSE ''
                                                     END
                                                   ) AS ScheduleDays
        INTO      #SchedulesNotUsingTimeClock
        FROM      [Freedom]..STUDREC R
        LEFT JOIN [Freedom]..COURSE_DEF C ON R.CourseNumEnrolledIn = C.RecordKey
        LEFT JOIN bridge.arPrgVersions B_PV ON R.CourseNumEnrolledIn = B_PV.src_RecordKey
        WHERE     C.UsesTimeClock = 0
        ORDER BY  C.CourseCode;

        SELECT NEWID() AS ScheduleId
              ,S.*
              ,S.ProgramVersionName + ' [' + S.ScheduleDays + ']' AS ScheduleName
              ,CAST(S.FreedomProgramVersion AS VARCHAR(10)) + S.ScheduleDays AS ScheduleCode
              ,ROW_NUMBER() OVER ( ORDER BY S.FreedomProgramVersion ) AS rn
        INTO   #SchedulesNotUsingTimeClockWithId
        FROM   #SchedulesNotUsingTimeClock S;

        SELECT *
        FROM   #SchedulesNotUsingTimeClockWithId;

        --insert schedules into advantage (no time clock)
        INSERT INTO [FreedomAdvantage]..arProgSchedules (
                                                        ScheduleId
                                                       ,Code
                                                       ,Descrip
                                                       ,PrgVerId
                                                       ,Active
                                                       ,UseFlexTime
                                                       ,ModUser
                                                       ,ModDate
                                                        )
                    SELECT S.ScheduleId
                          ,S.ScheduleCode
                          ,S.ScheduleName
                          ,S.ProgramVersionId
                          ,1
                          ,CAST(NULL AS BIT)
                          ,@ConversionUser
                          ,GETDATE()
                    FROM   #SchedulesNotUsingTimeClockWithId S;


        --insert schedules into advantage (no time clock)
        INSERT INTO bridge.arProgSchedules (
                                           trg_ScheduleId
                                          ,src_Name
                                          ,trg_CampGrpId
                                          ,trg_ProgramVersionId
                                          ,FreedomScheduleRecordKey
                                           )
                    SELECT S.ScheduleId
                          ,S.ScheduleName
                          ,@CampusGroupId
                          ,S.ProgramVersionId
                          ,S.rn
                    FROM   #SchedulesNotUsingTimeClockWithId S;

        SELECT NEWID() AS ScheduleDetailId
              ,*
              ,ROW_NUMBER() OVER ( ORDER BY NTCD.ScheduleId ) AS Rn
        INTO   #NonTimeClockScheduleDetails
        FROM   (
               SELECT ScheduleId
                     ,AttndSchdHrsMon AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,1 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsTue AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,2 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsWed AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,3 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsThr AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,4 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsFri AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,5 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsSat AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,6 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               UNION
               SELECT ScheduleId
                     ,AttndSchdHrsSun AS ScheduledHours
                     ,rn AS FreedomScheduleKey
                     ,7 AS dw
               FROM   #SchedulesNotUsingTimeClockWithId
               ) AS NTCD;

        INSERT INTO [FreedomAdvantage]..arProgScheduleDetails (
                                                              ScheduleDetailId
                                                             ,ScheduleId
                                                             ,dw
                                                             ,total
                                                             ,timein
                                                             ,lunchin
                                                             ,lunchout
                                                             ,timeout
                                                             ,maxnolunch
                                                             ,allow_earlyin
                                                             ,allow_lateout
                                                             ,allow_extrahours
                                                             ,check_tardyin
                                                             ,max_beforetardy
                                                             ,tardy_intime
                                                             ,MinimumHoursToBePresent
                                                              )
                    SELECT ScheduleDetailId
                          ,ScheduleId
                          ,dw
                          ,ScheduledHours
                          ,GETDATE() -- timein - datetime
                          ,GETDATE() -- lunchin - datetime
                          ,GETDATE() -- lunchout - datetime
                          ,GETDATE() -- timeout - datetime
                          ,NULL      -- maxnolunch - decimal(18, 2)
                          ,NULL      -- allow_earlyin - bit
                          ,NULL      -- allow_lateout - bit
                          ,NULL      -- allow_extrahours - bit
                          ,NULL      -- check_tardyin - bit
                          ,GETDATE() -- max_beforetardy - datetime
                          ,GETDATE() -- tardy_intime - datetime
                          ,NULL      -- MinimumHoursToBePresent - decimal(18, 2) 
                    FROM   #NonTimeClockScheduleDetails;

        INSERT INTO bridge.arProgSchedulesDetails (
                                                  ScheduleDetailId
                                                 ,ScheduleId
                                                 ,FreedomScheduleRecordKey
                                                 ,FreedomScheduleDetailRecordKey
                                                  )
                    SELECT ScheduleDetailId
                          ,ScheduleId
                          ,FreedomScheduleKey
                          ,Rn
                    FROM   #NonTimeClockScheduleDetails;

        --finally update freedom data to give an schedule created in bridge
        UPDATE    F
        SET       F.PrivTClockSchdRecKey = S.rn
        FROM      [Freedom]..STUDREC F
        LEFT JOIN #SchedulesNotUsingTimeClockWithId S ON F.CourseNumEnrolledIn = S.FreedomProgramVersion
                                                         AND F.AttndSchdHrsMon = S.AttndSchdHrsMon
                                                         AND F.AttndSchdHrsTue = S.AttndSchdHrsTue
                                                         AND F.AttndSchdHrsWed = S.AttndSchdHrsWed
                                                         AND F.AttndSchdHrsThr = S.AttndSchdHrsThr
                                                         AND F.AttndSchdHrsFri = S.AttndSchdHrsFri
                                                         AND F.AttndSchdHrsSat = S.AttndSchdHrsSat
                                                         AND F.AttndSchdHrsSun = S.AttndSchdHrsSun
        LEFT JOIN [Freedom]..COURSE_DEF C ON C.RecordKey = F.CourseNumEnrolledIn
        WHERE     C.UsesTimeClock = 0;

     ALTER TABLE FreedomAdvantage..arStudentSchedules DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arStudentSchedules (
                                                         StuScheduleId
                                                        ,StuEnrollId
                                                        ,ScheduleId
                                                        ,StartDate
                                                        ,EndDate
                                                        ,Active
                                                        ,ModUser
                                                        ,ModDate
                                                        ,Source
                                                         )
                    SELECT     NEWID()                        -- StuScheduleId - uniqueidentifier
                              ,enrollments.StuEnrollId        -- StuEnrollId - uniqueidentifier
                              ,bridgeSchedules.trg_ScheduleId -- ScheduleId - uniqueidentifier
                              ,NULL                           -- StartDate - smalldatetime
                              ,NULL                           -- EndDate - smalldatetime
                              ,1                              -- Active - bit
                              ,@ConversionUser                -- ModUser - varchar(50)
                              ,GETDATE()                      -- ModDate - smalldatetime
                              ,NULL                           -- Source - varchar(50)
                    FROM       Freedom..STUDREC freedomEnrollment
                    INNER JOIN bridge.arProgSchedules bridgeSchedules ON freedomEnrollment.PrivTClockSchdRecKey = bridgeSchedules.FreedomScheduleRecordKey
                    INNER JOIN bridge.arStuEnrollments bridgeEnrollments ON bridgeEnrollments.src_RecordKey = freedomEnrollment.RecordKey
                    INNER JOIN FreedomAdvantage..arStuEnrollments enrollments ON enrollments.StuEnrollId = bridgeEnrollments.trg_StuEnrollId
                    INNER JOIN FreedomAdvantage..arPrgVersions programVersions ON programVersions.PrgVerId = enrollments.PrgVerId
                    LEFT JOIN  FreedomAdvantage..arStudentSchedules schedules ON schedules.StuEnrollId = enrollments.StuEnrollId
                                                                                 AND schedules.ScheduleId = bridgeSchedules.trg_ScheduleId
                    WHERE      schedules.StuScheduleId IS NULL
                               AND enrollments.PrgVerId = bridgeSchedules.trg_ProgramVersionId;


        ALTER TABLE FreedomAdvantage..arStudentSchedules ENABLE TRIGGER ALL;


        DROP TABLE #SchedulesNotUsingTimeClock;
        DROP TABLE #SchedulesNotUsingTimeClockWithId;
        DROP TABLE #NonTimeClockScheduleDetails;





        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ------------------- END OF SCHEDULES CREATION FOR PROGRAM THAT NOT USES CLOCK ATTENDANCE ----------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------
        ---------------------------------------------------------------------------------------------------------------------------

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arProgSchedules]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT;
    END;
GO
