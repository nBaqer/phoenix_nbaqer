SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syUsersRolesCampGrps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS UserRoleCampGrpId    -- uniqueidentifier
             , bUser.trg_UserId AS UserId      -- uniqueidentifier
             , dRole.RoleId AS RoleId          -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId -- uniqueidentifier
             , GETDATE() AS ModDate            -- datetime
             , 'sa' AS ModUser                 -- varchar(50)
        INTO   #UserRole
        FROM   FreedomBridge.bridge.syUsers bUser
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        INNER JOIN FreedomAdvantage..syRoles dRole ON dRole.Role IN ( 'Instructor', 'Admissions Rep', 'Academic Advisor' )
        LEFT JOIN FreedomAdvantage..syUsersRolesCampGrps dUserRole ON dUserRole.UserId = bUser.trg_UserId
                                                                      AND dUserRole.RoleId = dRole.RoleId
        WHERE  dUserRole.UserId IS NULL
               AND bUser.LocalRank = 1;

        ALTER TABLE FreedomAdvantage..syUsersRolesCampGrps DISABLE TRIGGER ALL;

		/*
			DISABILING THE INSERT OF ROLES FOR USERS SINCE THIS IS A POST CONVERSION TASK DONE MANULLY TO REMOVE THE USERS FROM THIS ROLES.
		*/

        --INSERT INTO FreedomAdvantage..syUsersRolesCampGrps
        --            SELECT UserRoleCampGrpId
        --                 , UserId
        --                 , RoleId
        --                 , CampGrpId
        --                 , ModDate
        --                 , ModUser
        --            FROM   #UserRole;

        ALTER TABLE FreedomAdvantage..syUsersRolesCampGrps ENABLE TRIGGER ALL;
	
        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syUsersRolesCampGrps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syUsersRolesCampGrps
                    (
                        trg_UserRoleCmpGrpId UNIQUEIDENTIFIER NULL
                      , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                      , trg_RoleId UNIQUEIDENTIFIER NOT NULL
                      , brg_UserId UNIQUEIDENTIFIER NOT NULL
                      --,
                      --PRIMARY KEY CLUSTERED
                      --    (
                      --        trg_CampusId
                      --      , trg_RoleId
                      --      , brg_UserId
                      --    )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.syUsersRolesCampGrps
                    SELECT UserRoleCampGrpId
                         , @CampusId AS CampusId
                         , RoleId
                         , UserId
                    FROM   #UserRole;

        DROP TABLE #UserRole;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syUsersRolesCampGrps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
