SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syUsers]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN Users.dFullName IS NULL
                         AND LocalRank = 1 THEN NEWID()
                    ELSE Users.dUserId
               END AS UserId -- uniqueidentifier
              ,*
        INTO   #MergedUsers
        FROM   (
               SELECT    DupUsers.FullName
                        ,DupUsers.Email
                        ,DupUsers.UserName
                        ,CASE DupUsers.TableName
                              WHEN 'RECRUITR_FIL' THEN DupUsers.Password + '1'
                              ELSE DupUsers.Password
                         END AS Password
                        ,DupUsers.ConfirmPassword
                        ,DupUsers.Salt
                        ,DupUsers.AccountActive
                        ,DupUsers.ModDate
                        ,DupUsers.ModUser
                        ,DupUsers.CampusId
                        ,DupUsers.ShowDefaultCampus
                        ,DupUsers.ModuleId
                        ,DupUsers.IsAdvantageSuperUser
                        ,DupUsers.IsDefaultAdminRep
                        ,DupUsers.IsLoggedIn
                        ,DupUsers.SrcUserId
                        ,DupUsers.TableName
                        ,dUser.UserId AS dUserId
                        ,dUser.FullName AS dFullName
                        ,DENSE_RANK() OVER ( ORDER BY DupUsers.Email ) AS TotalRank
                        ,ROW_NUMBER() OVER ( PARTITION BY DupUsers.Email
                                                         ,DupUsers.CampusId
                                             ORDER BY DupUsers.SrcUserId
                                                     ,DupUsers.TableName
                                           ) AS LocalRank
               FROM      (
                         SELECT     stUser.src_FullName AS FullName -- varchar(100)
                                   ,stUser.src_Email AS Email       -- varchar(100)
                                   ,stUser.src_UserName AS UserName -- varchar(50)
                                   ,'test' AS Password              -- varchar(40)
                                   ,'test' AS ConfirmPassword       -- varchar(40)
                                   ,NULL AS Salt                    -- varchar(10)
                                   ,CASE sUser.Disabled
                                         WHEN 'True' THEN 0
                                         ELSE 1
                                    END AS AccountActive            -- bit
                                   ,GETDATE() AS ModDate            -- datetime
                                   ,'sa' AS ModUser                 -- varchar(50)
                                   ,@CampusId AS CampusId           -- uniqueidentifier
                                   ,0 AS ShowDefaultCampus          -- bit
                                   ,26 AS ModuleId                  -- smallint
                                   ,0 AS IsAdvantageSuperUser       -- bit
                                   ,0 AS IsDefaultAdminRep          -- bit
                                   ,0 AS IsLoggedIn                 -- bit
                                   ,sUser.UserId AS SrcUserId
                                   ,stUser.TableName AS TableName
                         FROM       Freedom..SHIBBOLETH_FIL sUser
                         INNER JOIN FreedomBridge.stage.syUsers stUser ON stUser.src_RecordKey = sUser.UserId
                                                                          AND stUser.src_SchoolID = @SchoolID
                                                                          AND stUser.TableName = 'SHIBBOLETH_FIL'
                         UNION ALL
                         SELECT     stUser.src_FullName AS FullName -- varchar(100)
                                   ,stUser.src_Email AS Email       -- varchar(100)
                                   ,stUser.src_UserName AS UserName -- varchar(50)
                                   ,'test' AS Password              -- varchar(40)
                                   ,'test' AS ConfirmPassword       -- varchar(40)
                                   ,NULL AS Salt                    -- varchar(10)
                                   ,CASE sUser.Disabled
                                         WHEN 'True' THEN 0
                                         ELSE 1
                                    END AS AccountActive            -- bit
                                   ,GETDATE() AS ModDate            -- datetime
                                   ,'sa' AS ModUser                 -- varchar(50)
                                   ,@CampusId AS CampusId           -- uniqueidentifier
                                   ,0 AS ShowDefaultCampus          -- bit
                                   ,26 AS ModuleId                  -- smallint
                                   ,0 AS IsAdvantageSuperUser       -- bit
                                   ,0 AS IsDefaultAdminRep          -- bit
                                   ,0 AS IsLoggedIn                 -- bit
                                   ,sRecruit.RecordKey AS SrcUserId
                                   ,stUser.TableName AS TableName
                         FROM       Freedom..RECRUITR_FIL sRecruit
                         LEFT JOIN  Freedom..SHIBBOLETH_FIL sUser ON LTRIM(RTRIM(sUser.UserFirstName)) = LTRIM(RTRIM(sRecruit.FirstName))
                                                                     AND LTRIM(RTRIM(sUser.UserLastName)) = LTRIM(RTRIM(sRecruit.LastName))
                         INNER JOIN FreedomBridge.stage.syUsers stUser ON stUser.src_RecordKey = sRecruit.RecordKey
                                                                          AND stUser.src_SchoolID = @SchoolID
                                                                          AND stUser.TableName = 'RECRUITR_FIL'
                         ) DupUsers
               LEFT JOIN FreedomAdvantage..syUsers dUser ON dUser.UserName = DupUsers.UserName
               ) Users;




        SELECT    ISNULL(u1.UserId, u2.UserId) AS UserId
                 ,u1.FullName
                 ,u1.Email
                 ,u1.UserName
                 ,u1.Password
                 ,u1.ConfirmPassword
                 ,u1.Salt
                 ,u1.AccountActive
                 ,u1.ModDate
                 ,u1.ModUser
                 ,u1.CampusId
                 ,u1.ShowDefaultCampus
                 ,u1.ModuleId
                 ,u1.IsAdvantageSuperUser
                 ,u1.IsDefaultAdminRep
                 ,u1.IsLoggedIn
                 ,u1.SrcUserId
                 ,u1.TableName
                 ,u1.TotalRank
                 ,u1.LocalRank
        INTO      #Users
        FROM      #MergedUsers u1
        LEFT JOIN #MergedUsers u2 ON u1.TotalRank = u2.TotalRank
                                     AND u2.UserId IS NOT NULL
                                     AND u1.UserId IS NULL
                                     AND u2.LocalRank = 1;


        ALTER TABLE FreedomAdvantage..syUsers DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syUsers
                    SELECT    tUser.UserId
                             ,LTRIM(RTRIM(tUser.FullName)) AS FullName
                             ,LTRIM(RTRIM(tUser.Email)) AS Email
                             ,LTRIM(RTRIM(tUser.UserName)) AS UserName
                             ,tUser.Password
                             ,tUser.ConfirmPassword
                             ,tUser.Salt
                             ,tUser.AccountActive
                             ,tUser.ModDate
                             ,tUser.ModUser
                             ,@CampusId AS CampusId
                             ,tUser.ShowDefaultCampus
                             ,tUser.ModuleId
                             ,tUser.IsAdvantageSuperUser
                             ,tUser.IsDefaultAdminRep
                             ,tUser.IsLoggedIn
							 ,1 AS UserTypeId
                    FROM      #Users tUser
                    LEFT JOIN FreedomAdvantage..syUsers dUser ON dUser.UserName = tUser.UserName
                    WHERE     dUser.UserId IS NULL AND NOT EXISTS ( SELECT T.UserId FROM FreedomAdvantage..syusers T WHERE t.Email = tUser.Email AND T.CampusId = @CampusId )
                              AND tUser.LocalRank = 1; --AND (tUser.Password NOT IN ('test') AND tUser.CampusId IS NULL);


        SELECT UserName
              ,UserId
              ,Email
              ,Password
              ,ROW_NUMBER() OVER ( ORDER BY FullName ) AS RowNumber
        INTO   #TempTableForMembership
        FROM   FreedomAdvantage..syUsers
        WHERE  IsAdvantageSuperUser = 0;


        DECLARE @CurrentRow INT = 0;
        DECLARE @TempUserName VARCHAR(256);
        DECLARE @TempUserId UNIQUEIDENTIFIER;
        DECLARE @TempEmail VARCHAR(256);
        DECLARE @tempPassword VARCHAR(256);
        DECLARE @tempExecResult INT;

        -- Iterate over all customers
        WHILE ( 1 = 1 )
            BEGIN

                -- Get next customerId
                SELECT   TOP 1 @TempUserName = UserName
                              ,@TempEmail = Email
                              ,@tempPassword = Password
                              ,@TempUserId = UserId
                              ,@CurrentRow = RowNumber
                FROM     #TempTableForMembership
                WHERE    RowNumber > @CurrentRow
                ORDER BY RowNumber;

                -- Exit loop if no more customers
                IF @@ROWCOUNT = 0
                    BREAK;

                -- call your sproc
                EXEC @tempExecResult = bridge.usp_TenantDbMembership @TempUserName
                                                                    ,@TempUserId
                                                                    ,@tempPassword
                                                                    ,@TempEmail;

            END;


        ALTER TABLE FreedomAdvantage..syUsers ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syUsers')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syUsers
                    (
                        trg_UserId UNIQUEIDENTIFIER NOT NULL
                       ,trg_CampusId UNIQUEIDENTIFIER NOT NULL
                       ,src_UserId INT NOT NULL
                       ,TableName VARCHAR(50)
                       ,TotalRank INT
                       ,LocalRank INT
                       --,
                       --PRIMARY KEY CLUSTERED (
                       --                      trg_CampusId
                       --                     ,src_UserId
                       --                     ,TableName
                       --                      )

                    );
            END;

        INSERT INTO FreedomBridge.bridge.syUsers
                    SELECT tUser.UserId
                          ,@CampusId AS CampusId
                          ,tUser.SrcUserId
                          ,tUser.TableName
                          ,tUser.TotalRank
                          ,tUser.LocalRank
                    --,*
                    FROM   #Users tUser;



        --WHERE	tUser.TotalRank = 19
        --ORDER BY tUser.SrcUserId

        --UPDATE  FreedomAdvantage..syUsers
        --SET     Email = 'sa@antonelli.edu'
        --       ,CampusId = @CampusId
        --WHERE   FullName = 'sa'
		DROP TABLE #TempTableForMembership
        DROP TABLE #MergedUsers;
        DROP TABLE #Users;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syUsers]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;

GO
