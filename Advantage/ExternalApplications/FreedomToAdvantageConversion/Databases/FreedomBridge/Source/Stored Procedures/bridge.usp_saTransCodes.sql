SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saTransCodes]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #ReMapAwardType
        FROM   (
               SELECT 'Federal Stafford Loan' AS AwardTypeDescription
                     ,'Direct Subsidized Loan' AS NewAwardTypeDescription
                     ,'' AS UnSubLoanFlag
               UNION ALL
               SELECT 'Federal Stafford Loan' AS AwardTypeDescription
                     ,'Direct Unsubsidized Loan' AS NewAwardTypeDescription
                     ,'U' AS UnSubLoanFlag
               ) A;


        -- we combine trantype  with award type
        SELECT    sTranType.RecordKey AS TranTypeKey
                 ,sTranType.MiniDescrip
                 ,sTranType.FullDescrip AS TrasnCodeDecription
                 ,sAwardType.RecordKey AS AwardTypeKey
                 ,sAwardType.KisMiniDescrip
                 ,LTRIM(RTRIM(sAwardType.FullDescrip)) AS AwardTypeDescription
                 ,CASE WHEN CHARINDEX('Refund', sTranType.MiniDescrip) > 0 THEN 'Refund '
                       ELSE ''
                  END AS Refund
        INTO      #AwardTranType
        FROM      Freedom..STUTRANS_FIL sTran
        LEFT JOIN Freedom..TRANTYPE_FIL sTranType ON sTran.TransTypeKey = sTranType.RecordKey
        LEFT JOIN Freedom..AWDTYPES_FIL sAwardType ON sAwardType.RecordKey = sTran.AwardTypeKey
        WHERE     sTranType.RecordKey IS NOT NULL
                  AND sAwardType.FullDescrip IS NOT NULL
        GROUP BY  sTranType.RecordKey
                 ,sTranType.MiniDescrip
                 ,sTranType.FullDescrip
                 ,sAwardType.RecordKey
                 ,sAwardType.KisMiniDescrip
                 ,sAwardType.FullDescrip;




        --adding UnSubLoanFlag column to #AwardTranType
        SELECT    TranTypeKey
                 ,MiniDescrip
                 ,TrasnCodeDecription
                 ,AwardTypeKey
                 ,KisMiniDescrip
                 ,ISNULL(CAST(tReMap.NewAwardTypeDescription AS VARCHAR(150)), tAwardType.AwardTypeDescription) AS AwardTypeDescription
                 ,Refund
                 ,UnSubLoanFlag
        INTO      #AwardTranTypeStage2
        FROM      #AwardTranType tAwardType
        LEFT JOIN #ReMapAwardType tReMap ON tReMap.AwardTypeDescription = tAwardType.AwardTypeDescription;


        SELECT     NEWID() AS TransCodeId                                                                                                                      -- uniqueidentifier
                  ,ISNULL(CAST(( tAwardType.Refund + tAwardType.KisMiniDescrip ) AS VARCHAR(12)), LTRIM(RTRIM(LEFT(sTrans.MiniDescrip, 12)))) AS TransCodeCode -- varchar(12)
                  ,dStatus.StatusId AS StatusId                                                                                                                -- uniqueidentifier
                  ,ISNULL(tAwardType.Refund + tAwardType.AwardTypeDescription, LTRIM(RTRIM(sTrans.FullDescrip))) + ( CASE WHEN sTrans.Active = 0 THEN '(I)'
                                                                                                                          ELSE ''
                                                                                                                     END
                                                                                                                   ) AS TransCodeDescrip                       -- varchar(50)
                  ,dCampGrp.CampGrpId AS CampGrpId                                                                                                             -- uniqueidentifier
                  ,dBillType.BillTypeId AS BillTypeId                                                                                                          -- uniqueidentifier
                  ,0 AS DefEarnings                                                                                                                            -- bit
                  ,'SA' AS ModUser                                                                                                                             -- varchar(50)
                  ,GETDATE() AS ModDate                                                                                                                        -- datetime
                  ,0 AS IsInstCharge                                                                                                                           -- bit               
                  ,CASE WHEN sTrans.RecordKey = 3
                             OR sTrans.RecordKey = 4
                             OR sTrans.RecordKey = 25 THEN 16
                        ELSE mTCode.trg_SysTransCodeId
                   END AS SysTransCodeId                                                                                                                       -- int
                  ,0 AS Is1098T                                                                                                                                -- bit
                  ,sTrans.RecordKey
                  ,ISNULL(tAwardType.Refund + tAwardType.AwardTypeDescription, sTrans.FullDescrip) AS FullDescrip
                  ,ISNULL(tAwardType.AwardTypeKey, 0) AS AwardTypeKey
                  ,UnSubLoanFlag
        INTO       #saTransCodes
        FROM       Freedom..TRANTYPE_FIL sTrans
        INNER JOIN map.saSysTransCodes mTCode ON mTCode.src_ChargeType = sTrans.ChargeType
                                                 AND mTCode.src_AwardPosting = sTrans.AwardPosting
        INNER JOIN FreedomAdvantage..saBillTypes dBillType ON dBillType.BillTypeDescrip = CASE WHEN sTrans.ChargeType = 2 THEN 'Applicant' --Registration Fee
                                                                                               ELSE 'Student'
                                                                                          END
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN  #AwardTranTypeStage2 tAwardType ON tAwardType.TranTypeKey = sTrans.RecordKey
        ORDER BY   mTCode.Description;

		

        --if saTransCodes is already inserted with another campus - reuse existing TransCodeId
        INSERT INTO bridge.saTransCodes
                    SELECT     DISTINCT bTCode.trg_TransCodeId
                                       ,tTransCode.RecordKey AS src_RecordKey
                                       ,tTransCode.AwardTypeKey AS src_AwartTypeKey
                                       ,tTransCode.FullDescrip AS src_FullDescrip
                                       ,@CampusId AS trg_CampusID
                                       ,tTransCode.UnSubLoanFlag AS src_UnSubLoanFlag
                    FROM       #saTransCodes tTransCode
                    INNER JOIN bridge.saTransCodes bTCode ON bTCode.src_FullDescrip = tTransCode.FullDescrip
                    LEFT JOIN  bridge.saTransCodes bIgnore ON bIgnore.src_FullDescrip = tTransCode.FullDescrip
                                                              AND bIgnore.trg_CampusID = @CampusId
                    WHERE      bIgnore.src_FullDescrip IS NULL;

        SELECT    ISNULL(tTransCodeMap.TransCodeId, tTransCode.TransCodeId) AS TransCodeId
                 ,tTransCode.TransCodeCode
                 ,tTransCode.StatusId
                 ,tTransCode.TransCodeDescrip
                 ,tTransCode.CampGrpId
                 ,tTransCode.BillTypeId
                 ,tTransCode.DefEarnings
                 ,tTransCode.ModUser
                 ,tTransCode.ModDate
                 ,tTransCode.IsInstCharge
                 ,tTransCode.SysTransCodeId
                 ,tTransCode.Is1098T
                 ,tTransCode.RecordKey
                 ,LTRIM(RTRIM(tTransCode.FullDescrip)) AS FullDescrip
                 ,tTransCode.AwardTypeKey
                 ,CASE WHEN ISNULL(tTransCodeMap.TransCodeId, bIgnore.trg_TransCodeId) IS NOT NULL THEN 1
                       ELSE 0
                  END AS SkipRow
                 ,tTransCode.UnSubLoanFlag
        INTO      #saTransCodesStage2
        FROM      #saTransCodes tTransCode
        LEFT JOIN #saTransCodes tTransCodeMap ON UPPER(tTransCodeMap.FullDescrip) = UPPER(tTransCode.FullDescrip) --mapping dublicates that were inserted into TRANTYPE_FIL and already exist in awardtypes
                                                 AND tTransCodeMap.AwardTypeKey > 0
                                                 AND tTransCodeMap.RecordKey <> tTransCode.RecordKey
        LEFT JOIN bridge.saTransCodes bIgnore ON bIgnore.src_FullDescrip = tTransCode.FullDescrip
                                                 AND bIgnore.trg_CampusID = @CampusId
        WHERE     bIgnore.src_FullDescrip IS NULL
                  AND NOT EXISTS (
                                 SELECT *
                                 FROM   FreedomAdvantage..saTransCodes TRC
                                 WHERE  TRC.TransCodeCode LIKE tTransCode.TransCodeCode
                                        AND TRC.TransCodeDescrip = tTransCode.TransCodeDescrip
                                        AND TRC.CampGrpId = tTransCode.CampGrpId
                                 );


        INSERT INTO bridge.saTransCodes
                    SELECT   tTransCode.TransCodeId AS trg_TransCodeId
                            ,tTransCode.RecordKey AS src_RecordKey
                            ,tTransCode.AwardTypeKey AS src_AawardTypeKey
                            ,tTransCode.TransCodeDescrip AS src_FullDescrip
                            ,@CampusId AS trg_CampusID
                            ,UnSubLoanFlag AS src_UnSubLoanFlag
                    FROM     #saTransCodesStage2 tTransCode
                    ORDER BY tTransCode.TransCodeDescrip;


        INSERT INTO FreedomAdvantage..saTransCodes
                    SELECT tTransCode.TransCodeId
                          ,tTransCode.TransCodeCode
                          ,tTransCode.StatusId
                          ,CASE WHEN tTransCode.TransCodeDescrip = 'Student Stipend-Living expense'
                                     AND tTransCode.RecordKey = 58 THEN tTransCode.TransCodeDescrip + '.'
                                ELSE tTransCode.TransCodeDescrip
                           END AS TrasnCodeDecription
                          ,tTransCode.CampGrpId
                          ,tTransCode.BillTypeId
                          ,tTransCode.DefEarnings
                          ,tTransCode.ModUser
                          ,tTransCode.ModDate
                          ,tTransCode.IsInstCharge
                          ,tTransCode.SysTransCodeId
                          ,tTransCode.Is1098T
                    FROM   #saTransCodesStage2 tTransCode
                    WHERE  tTransCode.SkipRow = 0;


        UPDATE FreedomAdvantage..saTransCodes
        SET    DefEarnings = 1
        WHERE  TransCodeDescrip = 'Tuition Charge';

        DROP TABLE #saTransCodesStage2;
        DROP TABLE #saTransCodes;
        DROP TABLE #AwardTranTypeStage2;
        DROP TABLE #AwardTranType;
        DROP TABLE #ReMapAwardType;
        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_saTransCodes]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;
    END;

GO
