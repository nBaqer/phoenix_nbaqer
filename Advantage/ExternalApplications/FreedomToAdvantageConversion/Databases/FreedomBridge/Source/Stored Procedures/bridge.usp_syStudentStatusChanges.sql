SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-18-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syStudentStatusChanges]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        DECLARE @numberOfTotalChangesInFreedom INT = (
                                                     SELECT COUNT(*)
                                                     FROM   Freedom..STATHIST_FIL
                                                     ) + 1;

        SELECT     NEWID() AS StudentStatusChangeId              -- uniqueidentifier
                  ,bStuEnroll.trg_StuEnrollId AS StuEnrollId     -- uniqueidentifier MANDATORY
                  ,bStatCodePrv.trg_StatusCodeId AS OrigStatusId -- uniqueidentifier
                                                             --, bStatCodeCur.trg_StatusCodeId AS NewStatusId  -- uniqueidentifier
                  ,CASE WHEN sStu.PrivCurrentEnrollStatus = 5 --when case is dropped out but there is not attendance then it is a no start
                             AND (
                                 NOT EXISTS (
                                            SELECT TOP 1 RecordKey
                                            FROM   Freedom..ATTENDMO_FIL Atten
                                            WHERE  Atten.StudrecKey = sStu.RecordKey
                                            )
                                 OR ((
                                     SELECT SUM(Atten.PrivMonthTot)
                                     FROM   Freedom..ATTENDMO_FIL Atten
                                     WHERE  Atten.StudrecKey = sStu.RecordKey
                                     ) = 0
                                    )
                                 OR ((
                                     SELECT LDACheck.LastDateAttended
                                     FROM   Freedom..STUDREC LDACheck
                                     WHERE  LDACheck.RecordKey = sStu.RecordKey
                                     ) IS NULL
                                    )
                                 ) THEN (
                                        SELECT TOP 1 scTemp.StatusCodeId
                                        FROM   FreedomAdvantage..syStatusCodes scTemp
                                        WHERE  scTemp.SysStatusId = 8
                                        )
                        WHEN sStu.PrivCurrentEnrollStatus = 1
                             AND sStu.StartDate > GETDATE() THEN (
                                                                 SELECT TOP 1 scTemp.StatusCodeId
                                                                 FROM   FreedomAdvantage..syStatusCodes scTemp
                                                                 WHERE  scTemp.SysStatusId = 7
                                                                 )
                        ELSE bStatCodeCur.trg_StatusCodeId
                   END AS NewStatusId
                  ,@CampusId AS CampusId                         -- uniqueidentifier
                  ,DATEADD(   MILLISECOND
                             ,CASE WHEN sStat.NextRec = 0 THEN @numberOfTotalChangesInFreedom
                                   ELSE sStat.NextRec
                              END
                             ,sStat.PostingDate
                          ) AS ModDate                           -- datetime
                  ,'sa' AS ModUser                               -- varchar(50)
                  ,0 AS IsReversal                               -- bit
                  ,CASE WHEN sStat.CurStatusCode IN ( 5,6) THEN bDrop.trg_DropReasonId
                        ELSE NULL
                   END AS DropReasonId                           -- uniqueidentifier
                  ,DATEADD(   MILLISECOND
                             ,CASE WHEN sStat.NextRec = 0 THEN @numberOfTotalChangesInFreedom
                                   ELSE sStat.NextRec
                              END
                             ,sStat.EffectiveDate
                          ) AS DateOfChange                      -- datetime
                  ,sStat.RecordKey
        --,NULL AS Lda
        --,NULL AS CaseNumber
        --,NULL AS RequestedBy
        --,NULL AS HaveBackup
        --,NULL AS HaveClientConfirmation
        INTO       #StuStatChg
        FROM       Freedom..STATHIST_FIL sStat
        INNER JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = sStat.StudrecKey
                                                                       AND bStuEnroll.trg_CampusId = @CampusId
        LEFT JOIN  FreedomBridge.bridge.syStatusCodes bStatCodePrv ON bStatCodePrv.src_ID = sStat.PrvStatusCode
        INNER JOIN FreedomBridge.bridge.syStatusCodes bStatCodeCur ON bStatCodeCur.src_ID = sStat.CurStatusCode
        INNER JOIN Freedom..STUDREC sStu ON sStu.RecordKey = sStat.StudrecKey
        LEFT JOIN  FreedomBridge.bridge.arDropReasons bDrop ON bDrop.src_ID = sStu.DropReasonCode
        INNER JOIN FreedomBridge.bridge.arLOAReasons bLOA ON bLOA.src_ID = 3
                                                             AND bLOA.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(@SchoolID AS VARCHAR(10))
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active';
        --LEFT JOIN FreedomAdvantage..syStudentStatusChanges dStuStatChg ON dStuStatChg.StuEnrollId = bStuEnroll.trg_StuEnrollId
        --AND dStuLOA.LOAReasonId = bStatCode.trg_StatusCodeId
        --ORDER BY sStat.StudrecKey

        ALTER TABLE FreedomAdvantage..syStudentStatusChanges DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syStudentStatusChanges (
                                                             StudentStatusChangeId
                                                            ,StuEnrollId
                                                            ,OrigStatusId
                                                            ,NewStatusId
                                                            ,CampusId
                                                            ,ModDate
                                                            ,ModUser
                                                            ,IsReversal
                                                            ,DropReasonId
                                                            ,DateOfChange
                                                             --,Lda
                                                             --,CaseNumber
                                                             --,RequestedBy
                                                             --,HaveBackup
                                                             --,HaveClientConfirmation
                                                             )
                    SELECT    tStuStatChg.StudentStatusChangeId
                             ,tStuStatChg.StuEnrollId
                             ,tStuStatChg.OrigStatusId
                             ,tStuStatChg.NewStatusId
                             ,tStuStatChg.CampusId
                             ,tStuStatChg.ModDate
                             ,tStuStatChg.ModUser
                             ,tStuStatChg.IsReversal
                             ,tStuStatChg.DropReasonId
                             ,tStuStatChg.DateOfChange
                    --,tStuStatChg.Lda
                    --,tStuStatChg.CaseNumber
                    --,tStuStatChg.RequestedBy
                    --,tStuStatChg.HaveBackup
                    --,tStuStatChg.HaveClientConfirmation
                    FROM      #StuStatChg tStuStatChg
                    LEFT JOIN FreedomAdvantage..syStudentStatusChanges dStuStatChg ON dStuStatChg.CampusId = tStuStatChg.CampusId
                                                                                      AND dStuStatChg.StuEnrollId = tStuStatChg.StuEnrollId
                                                                                      AND dStuStatChg.NewStatusId = tStuStatChg.NewStatusId
                                                                                      AND dStuStatChg.OrigStatusId = tStuStatChg.OrigStatusId
                    WHERE     dStuStatChg.StudentStatusChangeId IS NULL;

        DECLARE @FutureStartStatusCode UNIQUEIDENTIFIER = (
                                                          SELECT TOP 1 scTemp.StatusCodeId
                                                          FROM   FreedomAdvantage..syStatusCodes scTemp
                                                          WHERE  scTemp.SysStatusId = 7
                                                          );

        DECLARE @CurrentlyAttendingStatusCode UNIQUEIDENTIFIER = (
                                                                 SELECT TOP 1 scTemp.StatusCodeId
                                                                 FROM   FreedomAdvantage..syStatusCodes scTemp
                                                                 WHERE  scTemp.SysStatusId = 9
                                                                        AND scTemp.IsDefaultLeadStatus = 1
                                                                 );

        SELECT @FutureStartStatusCode;

        --inserting future start for all records 
        INSERT INTO FreedomAdvantage..syStudentStatusChanges (
                                                             StudentStatusChangeId
                                                            ,StuEnrollId
                                                            ,OrigStatusId
                                                            ,NewStatusId
                                                            ,CampusId
                                                            ,ModDate
                                                            ,ModUser
                                                            ,IsReversal
                                                            ,DropReasonId
                                                            ,DateOfChange
                                                             )
                    SELECT NEWID() AS StudentStatusChangeId
                          ,E.StuEnrollId AS StuEnrollId
                          ,NULL AS OrigStatusId
                          ,@FutureStartStatusCode AS NewStatusId
                          ,@CampusId AS CampusId   -- uniqueidentifier
                          ,E.EnrollDate AS ModDate -- datetime
                          ,'sa' AS ModUser         -- varchar(50)
                          ,0 AS IsReversal         -- bit
                          ,NULL AS DropReasonId    -- uniqueidentifier
                          ,E.EnrollDate AS DateOfChange
                    FROM   FreedomAdvantage..arStuEnrollments E;


        --update current records with no origin status and new status is currently attending or no show
        UPDATE FreedomAdvantage..syStudentStatusChanges
        SET    OrigStatusId = @FutureStartStatusCode
        WHERE  OrigStatusId IS NULL
               AND NewStatusId IN (
                                  SELECT StatusCodeId
                                  FROM   FreedomAdvantage..syStatusCodes
                                  WHERE  SysStatusId IN ( 8, 9 )
                                  );


        --inserting currently attending for changes straing on dropped out
        INSERT INTO FreedomAdvantage..syStudentStatusChanges (
                                                             StudentStatusChangeId
                                                            ,StuEnrollId
                                                            ,OrigStatusId
                                                            ,NewStatusId
                                                            ,CampusId
                                                            ,ModDate
                                                            ,ModUser
                                                            ,IsReversal
                                                            ,DropReasonId
                                                            ,DateOfChange
                                                             )
                    SELECT NEWID() AS StudentStatusChangeId
                          ,SSC.StuEnrollId AS StuEnrollId
                          ,@FutureStartStatusCode AS OrigStatusId
                          ,@CurrentlyAttendingStatusCode AS NewStatusId
                          ,@CampusId AS CampusId  -- uniqueidentifier
                          ,E.StartDate AS ModDate -- datetime
                          ,'sa' AS ModUser        -- varchar(50)
                          ,0 AS IsReversal        -- bit
                          ,NULL AS DropReasonId   -- uniqueidentifier
                          ,E.StartDate AS DateOfChange
                    FROM   FreedomAdvantage..syStudentStatusChanges SSC
                    JOIN   FreedomAdvantage..arStuEnrollments E ON E.StuEnrollId = SSC.StuEnrollId
                    WHERE  SSC.OrigStatusId IS NULL
                           AND SSC.NewStatusId IN (
                                                  SELECT StatusCodeId
                                                  FROM   FreedomAdvantage..syStatusCodes
                                                  WHERE  SysStatusId IN ( 12 )
                                                  );


        --update current records with no origin status and new status dropped out
        UPDATE FreedomAdvantage..syStudentStatusChanges
        SET    OrigStatusId = @CurrentlyAttendingStatusCode
        WHERE  OrigStatusId IS NULL
               AND NewStatusId IN (
                                  SELECT StatusCodeId
                                  FROM   FreedomAdvantage..syStatusCodes
                                  WHERE  SysStatusId IN ( 12 )
                                  );

        ALTER TABLE FreedomAdvantage..syStudentStatusChanges ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syStudentStatusChanges')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syStudentStatusChanges
                    (
                        trg_StudentStatusChangeId UNIQUEIDENTIFIER
                       ,src_RecordKey INT
                       ,trg_StuEnrollId UNIQUEIDENTIFIER
                       ,trg_OrigStatusId UNIQUEIDENTIFIER
                       ,trg_NewStatusId UNIQUEIDENTIFIER
                       ,trg_Campus UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       src_RecordKey
                      ,trg_StuEnrollId
                      ,trg_Campus
                       )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.syStudentStatusChanges
                    SELECT    tStuStatChg.StudentStatusChangeId
                             ,tStuStatChg.RecordKey
                             ,tStuStatChg.StuEnrollId
                             ,tStuStatChg.OrigStatusId
                             ,tStuStatChg.NewStatusId
                             ,tStuStatChg.CampusId
                    FROM      #StuStatChg tStuStatChg
                    LEFT JOIN FreedomBridge.bridge.syStudentStatusChanges bLOA ON bLOA.src_RecordKey = tStuStatChg.RecordKey
                                                                                  AND bLOA.trg_Campus = tStuStatChg.CampusId
                                                                                  AND bLOA.trg_StuEnrollId = tStuStatChg.StuEnrollId
                    WHERE     bLOA.trg_StudentStatusChangeId IS NULL;

        DROP TABLE #StuStatChg;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_syStudentStatusChanges]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;



GO
