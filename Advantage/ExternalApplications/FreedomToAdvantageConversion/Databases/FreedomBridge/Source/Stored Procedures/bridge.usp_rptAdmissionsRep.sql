SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_rptAdmissionsRep]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /******** Add users to rptAdmissionsRep table so user is available in options list for reports ****************/

        INSERT INTO FreedomAdvantage..rptAdmissionsRep
                    SELECT sUser.UserId AS AdmissionsRepID        -- uniqueidentifier
                         , sUser.FullName AS AdmissionsRepDescrip -- varchar(150)
                         , sStatus.StatusId AS StatusId           -- uniqueidentifier
                         , @CampusId AS CampusId                  -- uniqueidentifier
                         , NEWID() AS rptAdmissionsRepId          -- uniqueidentifier
                    FROM   FreedomBridge.bridge.syUsers bUser
                    INNER JOIN FreedomAdvantage..syUsers sUser ON sUser.UserId = bUser.trg_UserId
                    INNER JOIN FreedomAdvantage..syStatuses sStatus ON sStatus.Status = 'Active'
                    LEFT JOIN FreedomAdvantage..rptAdmissionsRep dAdminRep ON dAdminRep.AdmissionsRepID = sUser.UserId
                    WHERE  dAdminRep.AdmissionsRepID IS NULL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_rptAdmissionsRep]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
