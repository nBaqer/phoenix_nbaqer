SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adGenders]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dGen.GenderDescrip IS NULL THEN NEWID()
                    ELSE dGen.GenderId
               END AS GenderId                                                           -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS GenderCode -- varchar(50)
             , dStatus.StatusId AS StatusId                                              -- uniqueidentifier
             , FLook.Description AS GenderDescrip                                        -- varchar(50)
             , dCampGrp.CampGrpId AS CampGrpId                                           -- uniqueidentifier
             , 'sa' AS ModUser                                                           -- varchar(50)
             , GETDATE() AS ModDate                                                      -- datetime
             , ISNULL(dGen.IPEDSSequence, 0) AS IPEDSSequence                            -- int
             , ISNULL(dGen.IPEDSValue, 0) AS IPEDSValue                                  -- int
             , FLook.ID
        INTO   #Gender
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adGenders dGen ON dGen.CampGrpId = dCampGrp.CampGrpId
                                                      AND dGen.GenderDescrip = FLook.Description
        WHERE  FLook.LookupName = 'SexTypes';

        ALTER TABLE FreedomAdvantage..adGenders DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adGenders
                    SELECT tGen.GenderId
                         , tGen.GenderCode
                         , tGen.StatusId
                         , tGen.GenderDescrip
                         , tGen.CampGrpId
                         , tGen.ModUser
                         , tGen.ModDate
                         , tGen.IPEDSSequence
                         , tGen.IPEDSValue
						 , NULL
                    FROM   #Gender tGen
                    LEFT JOIN FreedomAdvantage..adGenders dGen ON dGen.CampGrpId = tGen.CampGrpId
                                                                  AND dGen.GenderId = tGen.GenderId
                    WHERE  dGen.GenderId IS NULL;

        ALTER TABLE FreedomAdvantage..adGenders ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adGenders')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adGenders
                    (
                        trg_GenderId UNIQUEIDENTIFIER
                      , trg_IPEDSValue INT
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adGenders
                    SELECT tGen.GenderId
                         , tGen.IPEDSValue
                         , tGen.GenderDescrip
                         , tGen.ID
                    FROM   #Gender tGen
                    LEFT JOIN FreedomBridge.bridge.adGenders bGen ON bGen.trg_GenderId = tGen.GenderId
                    WHERE  bGen.trg_GenderId IS NULL;

        DROP TABLE #Gender;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adGenders]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
