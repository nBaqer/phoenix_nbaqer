SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [stage].[usp_UpdateConfigurationSetting]
AS
    IF (
       SELECT COUNT(*)
       FROM   FreedomAdvantage..syCampuses
       ) < 2
        BEGIN
            DECLARE @ServerName VARCHAR(100)
                   ,@ServiceURI VARCHAR(200)
                   ,@SiteURI VARCHAR(200)
                   ,@ServiceKey UNIQUEIDENTIFIER
                   ,@DatabaseName VARCHAR(100)
                   ,@NewSupportUser UNIQUEIDENTIFIER
                   ,@OldSupportUser UNIQUEIDENTIFIER
                   ,@CampCode VARCHAR(5);

            SET @DatabaseName = DB_NAME();

            SELECT @ServerName = 'MATUSEVICIUS' --CAST(SERVERPROPERTY('MachineName') AS VARCHAR(100))
                  ,@ServiceURI = 'http://' + @ServerName + '/FAME/Advantage/Current/Services/'
                  ,@SiteURI = 'http://' + @ServerName + '/FAME/Advantage/Current/Site/';

            -- Verify that the API Authentication key exists in the Tenant for the correct database and store key value
            IF NOT EXISTS (
                          SELECT     aak.[Key]
                          FROM       TenantAuthDB..Tenant t
                          INNER JOIN TenantAuthDB..ApiAuthenticationKey aak ON aak.TenantId = t.TenantId
                          WHERE      t.DatabaseName = @DatabaseName
                          )
                BEGIN

                    PRINT 'Create API Authentication key through ''Advantage Manage Hosting'' tool.';

                END;

            ELSE
                BEGIN

                    SELECT     @ServiceKey = aak.[Key]
                    FROM       TenantAuthDB..Tenant t
                    INNER JOIN TenantAuthDB..ApiAuthenticationKey aak ON aak.TenantId = t.TenantId
                    WHERE      t.DatabaseName = @DatabaseName;

                END;

            -- Delete invalid lookup keys from syConfigAppSet_Lookup table for URI keys
            DELETE FreedomAdvantage..syConfigAppSet_Lookup
            WHERE  SettingId IN (
                                SELECT SettingId
                                FROM   FreedomAdvantage..syConfigAppSettings
                                WHERE  KeyName IN ( 'AdvantageServiceUri', 'AdvantageSiteUri', 'AdvantageServiceAuthenticationKey' )
                                )
                   AND LookUpId IS NOT NULL;

            -- Update the Advantage Service URI in the Advantage database
            UPDATE     cav
            SET        Value = @ServiceURI
            FROM       FreedomAdvantage..syConfigAppSetValues cav
            INNER JOIN FreedomAdvantage..syConfigAppSettings ON syConfigAppSettings.SettingId = cav.SettingId
            WHERE      KeyName = 'AdvantageServiceUri';

            -- Update the Advantage Site URI in the Advantage database
            UPDATE     cav
            SET        Value = @SiteURI
            FROM       FreedomAdvantage..syConfigAppSetValues cav
            INNER JOIN FreedomAdvantage..syConfigAppSettings ON syConfigAppSettings.SettingId = cav.SettingId
            WHERE      KeyName = 'AdvantageSiteUri';

            -- Update the Advantage Service Key in the Advantage database
            UPDATE     cav
            SET        Value = LOWER(@ServiceKey)
            FROM       FreedomAdvantage..syConfigAppSetValues cav
            INNER JOIN FreedomAdvantage..syConfigAppSettings ON syConfigAppSettings.SettingId = cav.SettingId
            WHERE      KeyName = 'AdvantageServiceAuthenticationKey';

            SET @CampCode = (
                            SELECT   TOP 1 CampCode
                            FROM     FreedomAdvantage..syCampuses
                            ORDER BY ModDate DESC
                            );

            --UPDATE    U
            --SET       UserName = 'support_' + C.CampCode
            --         ,U.FullName = 'support_' + C.CampCode
            --         ,Email = 'support_' + C.CampCode + '@yahoo.com'
            --FROM      FreedomAdvantage..syUsers U
            --LEFT JOIN FreedomAdvantage..syCampuses C ON C.CampusId = U.CampusId
            --WHERE     U.CampusId IS NOT NULL
            --          AND FullName = 'support';

            IF NOT EXISTS (
                          SELECT *
                          FROM   FreedomAdvantage..syUsers
                          WHERE  FullName = 'support'
                                 AND CampusId IS NULL
                          )
                BEGIN
                    PRINT 'here';
                    EXEC TenantAuthDB..UpdateAdvantageSupportUser FreedomAdvantage;
                END;



            IF NOT EXISTS (
                          SELECT *
                          FROM   FreedomAdvantage..syUsers
                          WHERE  FullName = 'support_' + @CampCode
                          )
                BEGIN
                    PRINT @CampCode;
                    EXEC TenantAuthDB..UpdateAdvantageSupportUser @DatabaseName = FreedomAdvantage
                                                                 ,@CampusCode = @CampCode;
                END;


        END;
GO
