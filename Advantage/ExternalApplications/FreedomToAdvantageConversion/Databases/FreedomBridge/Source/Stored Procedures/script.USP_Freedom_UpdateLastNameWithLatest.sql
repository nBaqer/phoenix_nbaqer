SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [script].[USP_Freedom_UpdateLastNameWithLatest]
AS
    SET XACT_ABORT ON;
    SET NOCOUNT ON;
    BEGIN
        BEGIN TRAN;

        SELECT   StuSSN
               , PrivLastName
        INTO     #TmpStud
        FROM     Freedom..STUDREC
        GROUP BY StuSSN
               , PrivLastName;

        SELECT   StuSSN
        INTO     #TmpStud1
        FROM     #TmpStud
        GROUP BY StuSSN
        HAVING   COUNT(1) > 1;

        SELECT   sStud.RecordKey
               , sStud.StuSSN
               , sStud.FirstName
               , sStud.PrivLastName
               , DENSE_RANK() OVER ( PARTITION BY sStud.StuSSN
                                     ORDER BY sStud.RecordKey DESC
                                   ) AS LocalRank
        INTO     #TmpStud2
        FROM     Freedom..STUDREC sStud
        INNER JOIN #TmpStud1 tStud ON tStud.StuSSN = sStud.StuSSN
        ORDER BY sStud.StuSSN
               , sStud.RecordKey DESC;

        UPDATE sStud
        SET    PrivLastName = A.PrivLastName
        FROM   Freedom..STUDREC sStud
        INNER JOIN (
                       SELECT tStud.StuSSN
                            , tStud.PrivLastName
                       FROM   #TmpStud2 tStud
                       WHERE  tStud.LocalRank = 1
                   ) A ON A.StuSSN = sStud.StuSSN
        WHERE  sStud.PrivLastName <> A.PrivLastName;

        INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[USP_Freedom_UpdateLastNameWithLatest]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;
    END;
GO
