SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_FSAPPolicy]
AS
    BEGIN

        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @TrigOffsetTypId INT;
        SET @TrigOffsetTypId = (
                               SELECT TrigOffsetTypId
                               FROM   FreedomAdvantage..arTrigOffsetTyps
                               WHERE  TrigOffTypDescrip = 'start date'
                               );


        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        DECLARE @CampusGroupId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER;

        SET @CampusId = (
                        SELECT TOP ( 1 ) Value
                        FROM   #Settings
                        WHERE  Entity = 'CampusID'
                        );

        SET @ActiveStatusId = (
                              SELECT   TOP ( 1 ) StatusId
                              FROM     FreedomAdvantage..syStatuses
                              WHERE    StatusCode = 'A'
                              ORDER BY StatusCode
                              );
        SET @CampusGroupId = (
                             SELECT CampGrpId
                             FROM   FreedomAdvantage..syCampGrps
                             WHERE  CampusId = @CampusId
                                    OR CampGrpCode = (
                                                     SELECT TOP 1 Value
                                                     FROM   #Settings
                                                     WHERE  Entity = 'SchoolId'
                                                     )
                             );

        SELECT     NEWID() AS SAPId                                                                                -- SAPId - uniqueidentifier
                  ,SUBSTRING(UPPER(LTRIM(RTRIM(REPLACE(freedomProgramVersions.Name, ' ', '')))), 0, 12) AS SAPCode -- SAPCode - varchar(12)
                  ,'[' + LTRIM(RTRIM(freedomProgramVersions.Name)) + '] SAP Policy' AS SAPDescrip                  -- SAPDescrip - varchar(50)
                  ,NULL AS SAPCriteriaId                                                                           -- SAPCriteriaId - uniqueidentifier
                  ,NULL AS MinTermGPA                                                                              -- MinTermGPA - decimal(6, 2)
                  ,NULL AS MinTermAv                                                                               -- MinTermAv - decimal(18, 0)
                  ,NULL AS TermGPAOver                                                                             -- TermGPAOver - decimal(6, 2)
                  ,NULL AS TermAvOver                                                                              -- TermAvOver - decimal(18, 0)
                  ,@ActiveStatusId AS StatusId                                                                     -- StatusId - uniqueidentifier
                  ,'sa' AS ModUser                                                                                 -- ModUser - varchar(50)
                  ,GETDATE() AS ModDate                                                                            -- ModDate - datetime
                  ,CASE freedomProgramVersions.FASAPRunBy
                        WHEN 1 THEN 4
                        WHEN 2 THEN 5
                   END AS TrigUnitTypId                                                                            -- TrigUnitTypId - tinyint
                  ,@TrigOffsetTypId AS TrigOffsetTypId                                                             -- TrigOffsetTypId - tinyint
                  ,@CampusGroupId AS CampGrpId                                                                     -- CampGrpId - uniqueidentifier
                  ,0 AS TerminationProbationCnt                                                                    -- TerminationProbationCnt - int
                  ,NULL AS TrackExternAttendance                                                                   -- TrackExternAttendance - bit
                  ,NULL AS IncludeTransferHours                                                                    -- IncludeTransferHours - bit
                  ,1 AS FaSapPolicy                                                                                -- FaSapPolicy - bit
                  ,freedomProgramVersions.FASAPPayOnProbatation AS PayOnProbation                                  -- PayOnProbation - bit
                  ,freedomProgramVersions.RecordKey AS FreedomProgramVerionId
        INTO       #FSAP
        FROM       Freedom..COURSE_DEF freedomProgramVersions
        INNER JOIN bridge.arPrgVersions bridgeProgramVersion ON freedomProgramVersions.RecordKey = bridgeProgramVersion.src_RecordKey
        WHERE      freedomProgramVersions.FASAPEnabled = 1
                   AND LTRIM(RTRIM(freedomProgramVersions.Name)) <> 'DO NOT USE';

        ALTER TABLE FreedomAdvantage..arSAP DISABLE TRIGGER ALL;


		
        INSERT INTO FreedomAdvantage..arSAP (
                                            SAPId
                                           ,SAPCode
                                           ,SAPDescrip
                                           ,StatusId
                                           ,ModUser
                                           ,ModDate
                                           ,TrigUnitTypId
                                           ,TrigOffsetTypId
                                           ,CampGrpId
                                           ,TerminationProbationCnt
                                           ,FaSapPolicy
                                           ,PayOnProbation
                                            )
                    SELECT    tSAP.SAPId
                             ,tSAP.SAPCode
                             ,tSAP.SAPDescrip
                             ,tSAP.StatusId
                             ,tSAP.ModUser
                             ,tSAP.ModDate
                             ,tSAP.TrigUnitTypId
                             ,tSAP.TrigOffsetTypId
                             ,tSAP.CampGrpId
                             ,tSAP.TerminationProbationCnt
                             ,tSAP.FaSapPolicy
                             ,tSAP.PayOnProbation
                    FROM      #FSAP tSAP
                    LEFT JOIN FreedomAdvantage..arSAP dSAP ON dSAP.CampGrpId = tSAP.CampGrpId
                                                              AND dSAP.SAPDescrip = tSAP.SAPDescrip
                                                              AND dSAP.SAPCode = tSAP.SAPCode
                    WHERE     dSAP.SAPDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..arSAP ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arFSAP')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arFSAP
                    (
                        trg_SAPId UNIQUEIDENTIFIER
                       ,src_Descrip VARCHAR(255)
                       ,src_ProgramVersionId INT
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED (
                                             trg_SAPId
                                            ,src_ProgramVersionId
                                             )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arFSAP
                    SELECT    tSAP.SAPId
                             ,tSAP.SAPDescrip
                             ,tSAP.FreedomProgramVerionId
                             ,@CampusId
                    FROM      #FSAP tSAP
                    LEFT JOIN FreedomBridge.bridge.arSAP bSAP ON bSAP.trg_CampusId = @CampusId
                                                                 AND bSAP.src_Descrip = tSAP.SAPDescrip
                                                               --  AND bSAP.src_RecordKey = tSAP.SAPCode  
                    WHERE     bSAP.trg_SAPId IS NULL;


        UPDATE     advantageProgramVersion
        SET        advantageProgramVersion.FASAPId = fsap.SAPId
        FROM       FreedomAdvantage..arPrgVersions advantageProgramVersion
        INNER JOIN bridge.arPrgVersions bridgeProgramVersion ON advantageProgramVersion.PrgVerId = bridgeProgramVersion.trg_PrgVerId
        INNER JOIN #FSAP fsap ON fsap.FreedomProgramVerionId = bridgeProgramVersion.src_RecordKey
                                 AND bridgeProgramVersion.trg_CampusId = @CampusId;


        INSERT INTO FreedomAdvantage.dbo.arCampusPrgVersions (
                                                             CampusPrgVerId
                                                            ,CampusId
                                                            ,PrgVerId
                                                            ,IsTitleIV
                                                            ,IsFAMEApproved
                                                            ,IsSelfPaced
                                                            ,CalculationPeriodTypeId
                                                            ,AllowExcusAbsPerPayPrd
                                                            ,TermSubEqualInLen
                                                            ,ModUser
                                                            ,ModDate
                                                             )
                    SELECT     NEWID()                 -- CampusPrgVerId - uniqueidentifier
                              ,campusGroup.CampusId    -- CampusId - uniqueidentifier
                              ,programVersion.PrgVerId -- PrgVerId - uniqueidentifier
                              ,freedomProgram.TitleIVEligible                   -- IsTitleIV - bit
                              ,freedomProgram.Approved                       -- IsFAMEApproved - bit
                              ,freedomProgram.SelfPaced                    -- IsSelfPaced - bit
                              ,NULL                    -- CalculationPeriodTypeId - uniqueidentifier (Where to get the Calculculation period type if its a payment period or a Perid of enrollment)
                              ,NULL                    -- AllowExcusAbsPerPayPrd - decimal(18, 1)
                              ,null                    -- TermSubEqualInLen - bit
                              ,'sa'                    -- ModUser - varchar(50)
                              ,GETDATE()               -- ModDate - datetime
                    FROM       FreedomAdvantage.dbo.arPrgVersions programVersion
                    INNER JOIN FreedomAdvantage.dbo.syCampGrps campusGroup ON campusGroup.CampGrpId = programVersion.CampGrpId
                    INNER JOIN FreedomBridge.bridge.arPrgVersions freedomBridgeProgramVersion ON freedomBridgeProgramVersion.trg_PrgVerId = programVersion.PrgVerId
                                                                                                 AND freedomBridgeProgramVersion.trg_CampusId = @CampusId
					INNER JOIN Freedom.dbo.COURSE_DEF freedomProgram ON freedomProgram.RecordKey = freedomBridgeProgramVersion.src_RecordKey
                    LEFT JOIN  FreedomAdvantage.dbo.arCampusPrgVersions campusProgramVersion ON campusProgramVersion.PrgVerId = programVersion.PrgVerId
                    WHERE      campusProgramVersion.PrgVerId IS NULL;


							  


        INSERT INTO FreedomAdvantage..syTitleIVSapCustomVerbiage (
                                                                 SapId
                                                                ,TitleIVSapStatusId
                                                                ,Message
                                                                ,StatusId
                                                                ,CreatedById
                                                                ,CreatedDate
                                                                 )
                    SELECT    SAPId                         -- SapId - uniqueidentifier
                             ,systemVerbiage.Id             -- TitleIVSapStatusId - int
                             ,systemVerbiage.DefaultMessage -- Message - nvarchar(2000)
                             ,@ActiveStatusId               -- StatusId - uniqueidentifier
                             ,systemVerbiage.CreatedById    -- CreatedById - uniqueidentifier
                             ,GETDATE()                     -- CreatedDate - datetime
                    FROM      #FSAP fsap
                    LEFT JOIN FreedomAdvantage..syTitleIVSapStatus systemVerbiage ON systemVerbiage.StatusId = @ActiveStatusId;



        DROP TABLE #FSAP;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_FSAPPolicy]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
