SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adGeographicTypes]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dGeo.Descrip IS NULL THEN NEWID()
                    ELSE dGeo.GeographicTypeId
               END AS GeographicTypeId                                             -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS Code -- varchar(50)
             , FLook.Description AS Descrip                                        -- varchar(50)
             , dStatus.StatusId AS StatusId                                        -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId                                     -- uniqueidentifier
             , GETDATE() AS ModDate                                                -- datetime
             , 'sa' AS ModUser                                                     -- varchar(50)
             , FLook.ID
        INTO   #GeoType
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adGeographicTypes dGeo ON dGeo.CampGrpId = dCampGrp.CampGrpId
                                                              AND dGeo.Descrip = FLook.Description
        WHERE  FLook.LookupName = 'GeographicTypes';

        ALTER TABLE FreedomAdvantage..adGeographicTypes DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adGeographicTypes
                    SELECT tGeo.GeographicTypeId
                         , tGeo.Code
                         , tGeo.Descrip
                         , tGeo.StatusId
                         , tGeo.CampGrpId
                         , tGeo.ModDate
                         , tGeo.ModUser
                    FROM   #GeoType tGeo
                    LEFT JOIN FreedomAdvantage..adGeographicTypes dGeo ON dGeo.CampGrpId = tGeo.CampGrpId
                                                                          AND dGeo.GeographicTypeId = tGeo.GeographicTypeId
                    WHERE  dGeo.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..adGeographicTypes ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adGeographicTypes')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adGeographicTypes
                    (
                        trg_GeographicTypeId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adGeographicTypes
                    SELECT tGeo.GeographicTypeId
                         , tGeo.Descrip
                         , tGeo.ID
                    FROM   #GeoType tGeo
                    LEFT JOIN FreedomBridge.bridge.adGeographicTypes bGeo ON bGeo.trg_GeographicTypeId = tGeo.GeographicTypeId
                    WHERE  bGeo.trg_GeographicTypeId IS NULL;

        DROP TABLE #GeoType;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adGeographicTypes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]
        COMMIT TRANSACTION;

    END;
GO
