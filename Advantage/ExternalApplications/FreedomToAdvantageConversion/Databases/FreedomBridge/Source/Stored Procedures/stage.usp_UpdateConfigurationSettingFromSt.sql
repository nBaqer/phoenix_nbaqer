SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [stage].[usp_UpdateConfigurationSettingFromStage]
AS
    IF (
       SELECT COUNT(*)
       FROM   FreedomAdvantage..syCampuses
       ) < 2
        BEGIN
            DELETE FROM FreedomAdvantage..syConfigAppSetValues
            WHERE SettingId IN (
                               SELECT SettingId
                               FROM   FreedomBridge.stage.syConfigAppSetValues
                               );

            INSERT INTO FreedomAdvantage..syConfigAppSetValues
                        SELECT ValueId
                              ,SettingId
                              ,CampusId
                              ,Value
                              ,ModUser
                              ,ModDate
                              ,Active
                        FROM   FreedomBridge.stage.syConfigAppSetValues;
        END;
GO
