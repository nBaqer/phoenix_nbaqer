SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-07-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGrdBkWeights]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT   sd.RecordKey
               , MIN(s.StartDate) AS EffectiveDate
        INTO     #EffDates
        FROM     Freedom..SUBJECT_DEF sd
        LEFT JOIN Freedom..SUBJECT_RES sr ON sr.SubjectDefKey = sd.RecordKey
        INNER JOIN Freedom..COURSE_DEF cd ON cd.RecordKey = sd.CourseDefKey
        INNER JOIN Freedom..STUDREC s ON s.CourseNumEnrolledIn = cd.RecordKey
        WHERE    sd.CourseDefKey > 0
        GROUP BY sd.RecordKey
        ORDER BY sd.RecordKey;

        SELECT CASE WHEN dGrdBkWgts.Descrip IS NULL THEN NEWID()
                    ELSE dGrdBkWgts.InstrGrdBkWgtId
               END AS InstrGrdBkWgtId                                       -- uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS InstructorId               -- uniqueidentifier
             , LEFT(bReqs.src_Name, 50) AS Descrip                          -- varchar(50)
             , dStatus.StatusId AS StatusId                                 -- uniqueidentifier
             , 'sa' AS ModUser                                              -- varchar(50)
             , GETDATE() AS ModDate                                         -- datetime
             , bReqs.trg_ReqId AS ReqId                                     -- uniqueidentifier
             , ISNULL(sSub.BegDate, EffDate.EffectiveDate) AS EffectiveDate -- datetime
             , bReqs.src_RecordKey
        INTO   #GrdBkWgts
        FROM   FreedomBridge.bridge.arReqs bReqs
        INNER JOIN Freedom..SUBJECT_DEF sSub ON sSub.RecordKey = bReqs.src_RecordKey
        INNER JOIN #EffDates EffDate ON EffDate.RecordKey = bReqs.src_RecordKey
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        LEFT JOIN FreedomAdvantage..arGrdBkWeights dGrdBkWgts ON dGrdBkWgts.ReqId = bReqs.trg_ReqId
                                                                 AND dGrdBkWgts.Descrip = bReqs.src_Name
                                                                 AND dGrdBkWgts.EffectiveDate = EffDate.EffectiveDate
        WHERE  bReqs.trg_CampusId = @CampusId;

        ALTER TABLE FreedomAdvantage..arGrdBkWeights DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGrdBkWeights
                    SELECT tGrdBkWgts.InstrGrdBkWgtId
                         , tGrdBkWgts.InstructorId
                         , tGrdBkWgts.Descrip
                         , tGrdBkWgts.StatusId
                         , tGrdBkWgts.ModUser
                         , tGrdBkWgts.ModDate
                         , tGrdBkWgts.ReqId
                         , tGrdBkWgts.EffectiveDate
                    FROM   #GrdBkWgts tGrdBkWgts
                    LEFT JOIN FreedomAdvantage..arGrdBkWeights dGrdBkWgts ON dGrdBkWgts.Descrip = tGrdBkWgts.Descrip
                                                                             AND dGrdBkWgts.ReqId = tGrdBkWgts.ReqId
                                                                             AND dGrdBkWgts.EffectiveDate = tGrdBkWgts.EffectiveDate
                    WHERE  dGrdBkWgts.InstrGrdBkWgtId IS NULL;

        ALTER TABLE FreedomAdvantage..arGrdBkWeights ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGrdBkWeights')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGrdBkWeights
                    (
                        trg_InstrGrdBkWgtId UNIQUEIDENTIFIER
                      , src_Name VARCHAR(255)
                      , src_RecordKey INT
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Name
                            , src_RecordKey
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGrdBkWeights
                    SELECT tGrdBkWgts.InstrGrdBkWgtId
                         , tGrdBkWgts.Descrip
                         , tGrdBkWgts.src_RecordKey
                         , @CampusId AS CampusId
                    FROM   #GrdBkWgts tGrdBkWgts
                    LEFT JOIN FreedomBridge.bridge.arGrdBkWeights bGrdBkWgts ON bGrdBkWgts.src_Name = tGrdBkWgts.Descrip
                                                                                AND bGrdBkWgts.src_RecordKey = tGrdBkWgts.src_RecordKey
                                                                                AND bGrdBkWgts.trg_CampusId = @CampusId
                    WHERE  bGrdBkWgts.trg_InstrGrdBkWgtId IS NULL;

        DROP TABLE #EffDates;
        DROP TABLE #GrdBkWgts;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGrdBkWeights]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
