SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_faLenders]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';


        SELECT CASE WHEN A.LocalRank = 1
                         AND A.ExistingLenderID IS NULL THEN NEWID()
                    ELSE A.ExistingLenderID
               END AS LenderId -- uniqueidentifier
              ,*
        INTO   #faLenders
        FROM   (
               SELECT     dLender.LenderId AS ExistingLenderID           -- uniqueidentifier
                         ,sBank.BankName AS Code                         -- varchar(50)
                         ,dStatus.StatusId AS StatusId                   -- uniqueidentifier
                         ,sBank.BankName AS LenderDescrip                -- varchar(50)
                         ,'' AS ForeignAddress                           -- bit
                         ,REPLACE(sBank.BankAddressLine1, '.', '') AS Address1             -- varchar(50)
                         ,REPLACE(sBank.BankAddressLine1, '.', '') AS Address2             -- varchar(50)
                         ,sBank.BankAddressCity AS City                  -- varchar(50)
                         ,dState.StateId AS StateId                      -- uniqueidentifier
                         ,sBank.BankAddressState AS OtherState           -- varchar(50)
                         ,sBank.privBankAddressZip AS Zip                -- varchar(50)
                         ,dCountry.CountryId AS CountryId                -- uniqueidentifier
                         ,NULL AS Email                                  -- varchar(50)
                         ,sBank.BankPhoneNum AS PrimaryContact           -- varchar(50)
                         ,NULL AS OtherContact                           -- varchar(50)
                         ,0 AS IsLender                                  -- bit
                         ,0 AS IsServicer                                -- bit
                         ,0 AS IsGuarantor                               -- bit
                         ,0 AS ForeignPayAddress                         -- bit
                         ,NULL AS PayAddress1                            -- varchar(50)
                         ,NULL AS PayAddress2                            -- varchar(50)
                         ,NULL AS PayCity                                -- varchar(50)
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS PayStateId   -- uniqueidentifier
                         ,NULL AS OtherPayState                          -- varchar(50)
                         ,NULL AS PayZip                                 -- varchar(50)
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS PayCountryId -- uniqueidentifier
                         ,NULL AS CustService                            -- varchar(50)
                         ,0 AS ForeignCustService                        -- bit
                         ,NULL AS Fax                                    -- varchar(50)
                         ,0 AS ForeignFax                                -- bit
                         ,NULL AS PreClaim                               -- varchar(50)
                         ,0 AS ForeignPreClaim                           -- bit
                         ,NULL AS PostClaim                              -- varchar(50)
                         ,0 AS ForeignPostClaim                          -- bit
                         ,NULL AS Comments                               -- varchar(2000)
                         ,'SA' AS ModUser                                -- varchar(50)
                         ,GETDATE() AS ModDate                           -- datetime
                         ,dCampGrp.CampGrpId AS CampGrpId                -- uniqueidentifier
                         ,sBank.RecordKey
                         ,sBank.BankType
                         ,DENSE_RANK() OVER ( ORDER BY BankName
                                                      ,BankPhoneNum
                                            ) AS TotalRank
                         ,ROW_NUMBER() OVER ( PARTITION BY BankName
                                                          ,BankPhoneNum
                                              ORDER BY BankName
                                            ) AS LocalRank
               FROM       Freedom..FABANKS_FIL sBank
               INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN sBank.IsBankActive = 1 THEN 'Active'
                                                                                        ELSE 'Inactive'
                                                                                   END
               INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
               INNER JOIN FreedomAdvantage..adCountries dCountry ON dCountry.CountryCode = 'USA'
               LEFT JOIN  FreedomAdvantage..syStates dState ON dState.StateCode = sBank.BankAddressState
               LEFT JOIN  FreedomAdvantage..faLenders dLender ON dLender.LenderDescrip = sBank.BankName
                                                                 AND dLender.Address1 = sBank.BankAddressLine1
                                                                 AND dLender.Address2 = sBank.BankAddressLine2
                                                                 AND dLender.City = sBank.BankAddressCity
                                                                 AND dLender.OtherState = sBank.BankAddressState
                                                                 AND dLender.Zip = sBank.privBankAddressZip
               ) A;

        UPDATE tLender
        SET    IsLender = ISNULL((
                                 SELECT TOP 1 1
                                 FROM   #faLenders tLender1
                                 WHERE  tLender1.TotalRank = tLender.TotalRank
                                        AND tLender1.BankType = '1'
                                 )
                                ,0
                                )
              ,IsServicer = ISNULL((
                                   SELECT TOP 1 1
                                   FROM   #faLenders tLender1
                                   WHERE  tLender1.TotalRank = tLender.TotalRank
                                          AND tLender1.BankType = '2'
                                   )
                                  ,0
                                  )
              ,IsGuarantor = ISNULL((
                                    SELECT TOP 1 1
                                    FROM   #faLenders tLender1
                                    WHERE  tLender1.TotalRank = tLender.TotalRank
                                           AND tLender1.BankType = '3'
                                    )
                                   ,0
                                   )
        FROM   #faLenders tLender;



        UPDATE    Adv
        SET       IsLender = CASE WHEN NewLender.IsLender IS NOT NULL
                                       AND NewLender.IsLender = 1 THEN 1
                                  ELSE Adv.IsLender
                             END
                 ,IsServicer = CASE WHEN NewLender.IsServicer IS NOT NULL
                                         AND NewLender.IsServicer = 1 THEN 1
                                    ELSE Adv.IsServicer
                               END
                 ,IsGuarantor = CASE WHEN NewLender.IsGuarantor IS NOT NULL
                                          AND NewLender.IsGuarantor = 1 THEN 1
                                     ELSE Adv.IsGuarantor
                                END
        FROM      FreedomAdvantage..faLenders Adv
        LEFT JOIN #faLenders NewLender ON RTRIM(LTRIM(Adv.Code)) = RTRIM(LTRIM(NewLender.Code))
                                          AND RTRIM(LTRIM(Adv.PrimaryContact)) = RTRIM(LTRIM(NewLender.PrimaryContact))
                                          AND RTRIM(LTRIM(Adv.LenderDescrip)) = RTRIM(LTRIM(NewLender.LenderDescrip))
										  AND newlender.LocalRank =1

      

        INSERT INTO FreedomAdvantage..faLenders
                    SELECT    tLender.LenderId
                             ,tLender.Code
                             ,tLender.StatusId
                             ,tLender.LenderDescrip
                             ,tLender.ForeignAddress
                             ,tLender.Address1
                             ,tLender.Address2
                             ,tLender.City
                             ,tLender.StateId
                             ,tLender.OtherState
                             ,tLender.Zip
                             ,tLender.CountryId
                             ,tLender.Email
                             ,tLender.PrimaryContact
                             ,tLender.OtherContact
                             ,tLender.IsLender
                             ,tLender.IsServicer
                             ,tLender.IsGuarantor
                             ,tLender.ForeignPayAddress
                             ,tLender.PayAddress1
                             ,tLender.PayAddress2
                             ,tLender.PayCity
                             ,tLender.PayStateId
                             ,tLender.OtherPayState
                             ,tLender.PayZip
                             ,tLender.PayCountryId
                             ,tLender.CustService
                             ,tLender.ForeignCustService
                             ,tLender.Fax
                             ,tLender.ForeignFax
                             ,tLender.PreClaim
                             ,tLender.ForeignPreClaim
                             ,tLender.PostClaim
                             ,tLender.ForeignPostClaim
                             ,tLender.Comments
                             ,tLender.ModUser
                             ,tLender.ModDate
                             ,tLender.CampGrpId
                    FROM      #faLenders tLender
                    LEFT JOIN FreedomAdvantage..faLenders dLender ON dLender.LenderId = tLender.LenderId
                    WHERE     LocalRank = 1
                              AND dLender.LenderId IS NULL
                              AND NOT EXISTS (
                                             SELECT *
                                             FROM   FreedomAdvantage..faLenders lend
                                             WHERE  lend.Code = tLender.Code
                                                    AND lend.LenderDescrip = tLender.LenderDescrip
                                                    AND lend.PrimaryContact = tLender.PrimaryContact
                                             );



        INSERT INTO bridge.faLenders
                    SELECT    ISNULL(tLender.LenderId, tLender1.LenderId)
                             ,tLender.LenderDescrip
                             ,tLender.RecordKey
                             ,tLender.TotalRank
                             ,tLender.LocalRank
                             ,@CampusId
                    FROM      #faLenders tLender
                    LEFT JOIN #faLenders tLender1 ON tLender1.TotalRank = tLender.TotalRank
                                                     AND tLender1.LocalRank = 1
                    WHERE     NOT EXISTS (
                                         SELECT *
                                         FROM   bridge.faLenders bl
                                         WHERE  bl.src_RecordKey = tLender.RecordKey
                                                AND bl.trg_CampusID = @CampusId
                                         );

        DROP TABLE #faLenders;
        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_faLenders]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;
GO
