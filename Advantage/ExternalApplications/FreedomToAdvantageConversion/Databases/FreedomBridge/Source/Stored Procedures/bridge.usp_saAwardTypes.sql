SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saAwardTypes]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT dAWType.AwardTypeId --uniqueidentifier
             , FL.ID
             , FL.Description
             , FL.LookUpID
        INTO   #saAwardTypes
        FROM   stage.FLookUp FL
        LEFT JOIN FreedomAdvantage..saAwardTypes dAWType ON dAWType.Descrip = FL.Description
        LEFT JOIN bridge.saAwardTypes bIgnore ON bIgnore.src_Description = FL.Description
        WHERE  LookupName = 'AwardType'
               AND bIgnore.src_ID IS NULL;

        INSERT INTO FreedomBridge.bridge.saAwardTypes
                    SELECT AwardTypeId
                         , ID
                         , Description
                         , LookUpID
                    FROM   #saAwardTypes;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saAwardTypes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
