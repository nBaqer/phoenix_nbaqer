SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-05-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGradeScaleDetails]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /****** Use staging table provided by client to gather grade system detail information to insert into arGradeSystemDetails table ******/
        SELECT     CASE WHEN dGrdScaleDtl.MinVal IS NULL THEN NEWID()
                        ELSE dGrdScaleDtl.GrdScaleDetailId
                   END AS GrdScaleDetailId                            -- uniqueidentifier
                  ,bGrdScl.trg_GradeScaleId AS GrdScaleId             -- uniqueidentifier
                  ,bGrdSysDtl.src_MinGradeValue AS MinVal             -- smallint
                  ,bGrdSysDtl.src_MaxGradeValue AS MaxVal             -- smallint
                  ,bGrdSysDtl.trg_GrdSystemDetailId AS GrdSysDetailId -- uniqueidentifier
                  ,ROW_NUMBER() OVER ( PARTITION BY bGrdSysDtl.trg_GrdSystemId
                                       ORDER BY bGrdSysDtl.src_MaxGradeValue DESC
                                     ) AS ViewOrder                   -- int
                  ,'sa' AS ModUser                                    -- varchar(50)
                  ,GETDATE() AS ModDate                               -- datetime
                  ,bGrdScl.src_Description
        INTO       #GrdScaleDetail
        FROM       FreedomBridge.bridge.arGradeSystemDetails bGrdSysDtl
        INNER JOIN FreedomBridge.bridge.arGradeScales bGrdScl ON bGrdScl.trg_GrdSystemId = bGrdSysDtl.trg_GrdSystemId
                                                                 AND bGrdScl.trg_CampusId = @CampusId
                                                                 AND bGrdSysDtl.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage..arGradeScaleDetails dGrdScaleDtl ON dGrdScaleDtl.GrdSysDetailId = bGrdSysDtl.trg_GrdSystemDetailId;

        ALTER TABLE FreedomAdvantage..arGradeScaleDetails DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGradeScaleDetails
                    SELECT    tGrdScaleDtl.GrdScaleDetailId AS GrdScaleDetailId -- uniqueidentifier
                             ,tGrdScaleDtl.GrdScaleId AS GrdScaleId             -- uniqueidentifier
                             ,tGrdScaleDtl.MinVal AS MinVal                     -- smallint
                             ,tGrdScaleDtl.MaxVal AS MaxVal                     -- smallint
                             ,tGrdScaleDtl.GrdSysDetailId AS GrdSysDetailId     -- uniqueidentifier
                             ,tGrdScaleDtl.ViewOrder AS ViewOrder               -- int
                             ,tGrdScaleDtl.ModUser AS ModUser                   -- varchar(50)
                             ,tGrdScaleDtl.ModDate AS ModDate                   -- datetime
                    FROM      #GrdScaleDetail tGrdScaleDtl
                    LEFT JOIN FreedomAdvantage..arGradeScaleDetails dGrdScaleDtl ON dGrdScaleDtl.GrdScaleDetailId = tGrdScaleDtl.GrdScaleDetailId
                    WHERE     dGrdScaleDtl.GrdScaleDetailId IS NULL;

        ALTER TABLE FreedomAdvantage..arGradeScaleDetails ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge

        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGradeScaleDetails')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGradeScaleDetails
                    (
                        trg_GrdScaleDetailId UNIQUEIDENTIFIER NULL
                       ,src_Description VARCHAR(50)
                       ,src_MinGradeValue INT NOT NULL
                       ,src_MaxGradeValue INT NOT NULL
                       ,trg_CampusId UNIQUEIDENTIFIER NOT NULL
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       src_Description
                      ,src_MinGradeValue
                      ,src_MaxGradeValue
                      ,trg_CampusId
                       )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGradeScaleDetails
                    SELECT    tGrdScaleDtl.GrdScaleDetailId
                             ,tGrdScaleDtl.src_Description
                             ,tGrdScaleDtl.MinVal
                             ,tGrdScaleDtl.MaxVal
                             ,@CampusId AS CampusId
                    FROM      #GrdScaleDetail tGrdScaleDtl
                    LEFT JOIN FreedomBridge.bridge.arGradeScaleDetails bGrdScaleDtl ON bGrdScaleDtl.src_Description = tGrdScaleDtl.src_Description
                                                                                       AND bGrdScaleDtl.src_MinGradeValue = tGrdScaleDtl.MinVal
                                                                                       AND bGrdScaleDtl.src_MaxGradeValue = tGrdScaleDtl.MaxVal
                                                                                       AND bGrdScaleDtl.trg_CampusId = @CampusId
                    WHERE     bGrdScaleDtl.trg_GrdScaleDetailId IS NULL;

        DECLARE @gradeSysTemIdForGradeScale UNIQUEIDENTIFIER;
        SET @gradeSysTemIdForGradeScale = (
                                          SELECT TOP 1 GrdSystemId
                                          FROM   FreedomAdvantage..arGradeScales
                                          WHERE  Descrip = 'Grade Scale'
                                          );

        SELECT FreedomGradeScaleDetail.RangeStart AS MinValue
              ,FreedomGradeScaleDetail.Grade AS Grade
              ,ROW_NUMBER() OVER ( ORDER BY Grade ASC ) AS RowNumber
        INTO   #InitialGradeScaleDetails
        FROM   Freedom..GRADESCALE_DTL FreedomGradeScaleDetail;

        UPDATE     FreedomAdvantage..arGradeScaleDetails
        SET        MinVal = IGSD.MinValue
                  ,MaxVal = CASE WHEN MaXGSD.MinValue IS NULL THEN 100
                                 ELSE MaXGSD.MinValue - 1
                            END
        FROM       FreedomAdvantage..arGradeScaleDetails SD
        INNER JOIN FreedomAdvantage..arGradeSystemDetails SystemDet ON SystemDet.GrdSysDetailId = SD.GrdSysDetailId
                                                                       AND SystemDet.GrdSystemId = @gradeSysTemIdForGradeScale
        INNER JOIN #InitialGradeScaleDetails IGSD ON IGSD.Grade = SystemDet.Grade
        LEFT JOIN  #InitialGradeScaleDetails MaXGSD ON IGSD.RowNumber = MaXGSD.RowNumber + 1;

        DROP TABLE #InitialGradeScaleDetails;

        DROP TABLE #GrdScaleDetail;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGradeScaleDetails]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
