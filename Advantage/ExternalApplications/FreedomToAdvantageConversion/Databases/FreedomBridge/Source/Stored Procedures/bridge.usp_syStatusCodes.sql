SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_syStatusCodes]
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    DECLARE @syStatusCodes TABLE
    (
        trg_StatusCodeId UNIQUEIDENTIFIER NOT NULL,
        trg_StatusLevelId INT NOT NULL,
        src_ID INT NULL,
        src_Description VARCHAR(255) NULL,
        trg_Description VARCHAR(255) NULL,
        src_LookUpID INT NULL,
        src_RecNum INT NULL
    );

    IF NOT EXISTS
    (
        SELECT *
        FROM FreedomAdvantage..syStatusCodes
        WHERE StatusCodeDescrip = 'Academic Probation'
    )
    BEGIN

        INSERT INTO FreedomAdvantage..syStatusCodes
        (
            StatusCodeId,
            StatusCode,
            StatusCodeDescrip,
            StatusId,
            CampGrpId,
            SysStatusId,
            ModDate,
            ModUser,
            AcadProbation,
            DiscProbation,
            IsDefaultLeadStatus
        )
        VALUES
        (   NEWID(),                                -- StatusCodeId - uniqueidentifier
            'PRO',                                  -- StatusCode - varchar(20)
            'Academic Probation',                   -- StatusCodeDescrip - varchar(80)
            'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965', -- StatusId - uniqueidentifier
            '2AC97FC1-BB9B-450A-AA84-7C503C90A1EB', -- CampGrpId - uniqueidentifier
            9,                                      -- SysStatusId - int
            GETDATE(),                              -- ModDate - datetime
            'Support',                              -- ModUser - varchar(50)
            NULL,                                   -- AcadProbation - bit
            NULL,                                   -- DiscProbation - bit
            0                                       -- IsDefaultLeadStatus - bit
            );
    END;

	 IF NOT EXISTS
    (
        SELECT *
        FROM FreedomAdvantage..syStatusCodes
        WHERE StatusCodeDescrip = 'Suspended'
    )
    BEGIN

        INSERT INTO FreedomAdvantage..syStatusCodes
        (
            StatusCodeId,
            StatusCode,
            StatusCodeDescrip,
            StatusId,
            CampGrpId,
            SysStatusId,
            ModDate,
            ModUser,
            AcadProbation,
            DiscProbation,
            IsDefaultLeadStatus
        )
        VALUES
        (   NEWID(),                                -- StatusCodeId - uniqueidentifier
            'SUSP',                                  -- StatusCode - varchar(20)
            'Suspended',                   -- StatusCodeDescrip - varchar(80)
            'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965', -- StatusId - uniqueidentifier
            '2AC97FC1-BB9B-450A-AA84-7C503C90A1EB', -- CampGrpId - uniqueidentifier
            11,                                      -- SysStatusId - int
            GETDATE(),                              -- ModDate - datetime
            'Support',                              -- ModUser - varchar(50)
            NULL,                                   -- AcadProbation - bit
            NULL,                                   -- DiscProbation - bit
            0                                       -- IsDefaultLeadStatus - bit
            );
    END;


	 IF NOT EXISTS
    (
        SELECT *
        FROM FreedomAdvantage..syStatusCodes
        WHERE StatusCodeDescrip = 'Incomplete'
    )
    BEGIN

        INSERT INTO FreedomAdvantage..syStatusCodes
        (
            StatusCodeId,
            StatusCode,
            StatusCodeDescrip,
            StatusId,
            CampGrpId,
            SysStatusId,
            ModDate,
            ModUser,
            AcadProbation,
            DiscProbation,
            IsDefaultLeadStatus
        )
        VALUES
        (   NEWID(),                                -- StatusCodeId - uniqueidentifier
            'I',                                  -- StatusCode - varchar(20)
            'Incomplete',                   -- StatusCodeDescrip - varchar(80)
            'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965', -- StatusId - uniqueidentifier
            '2AC97FC1-BB9B-450A-AA84-7C503C90A1EB', -- CampGrpId - uniqueidentifier
            9,                                      -- SysStatusId - int
            GETDATE(),                              -- ModDate - datetime
            'Support',                              -- ModUser - varchar(50)
            NULL,                                   -- AcadProbation - bit
            NULL,                                   -- DiscProbation - bit
            0                                       -- IsDefaultLeadStatus - bit
            );
    END;

	
	 IF NOT EXISTS
    (
        SELECT *
        FROM FreedomAdvantage..syStatusCodes
        WHERE StatusCodeDescrip = 'Graduate (Delinquent)'
    )
    BEGIN

    INSERT INTO FreedomAdvantage..syStatusCodes
    SELECT NEWID() AS StatusCodeId,                      -- uniqueidentifier
           'DG' AS StatusCode,                           -- varchar(20)
           'Graduate (Delinquent)' AS StatusCodeDescrip, -- varchar(80)
           dStatus.StatusId,                             -- uniqueidentifier
           dCampGrp.CampGrpId,                           -- uniqueidentifier
           14,                         -- SysStatusId -- int
           GETDATE() AS ModDate,                         -- datetime
           'sa' AS ModUser,                              -- varchar(50)
           NULL AS AcadProbation,                        -- bit
           NULL AS DiscProbation,                        -- bit
           0 AS IsDefaultLeadStatus                      -- bit
    FROM FreedomAdvantage..syStatuses dStatus
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp
            ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..syStatusCodes dIgnoreCode
            ON dIgnoreCode.StatusCodeDescrip = 'Graduate (Delinquent)'
    WHERE dStatus.Status = 'Active'
          AND dIgnoreCode.StatusCodeId IS NULL;
		  end


    INSERT INTO @syStatusCodes
    SELECT dSCode.StatusCodeId, --uniqueidentifier
           dSStatus.StatusLevelId,
           FL.ID,
           FL.Description,
           dSCode.StatusCodeDescrip,
           FL.LookUpID,
           NULL AS src_RecNum   --this is for Lead Status
    FROM stage.FLookUp FL
        INNER JOIN FreedomAdvantage.dbo.syStatusCodes dSCode
            ON dSCode.StatusCodeDescrip = CASE
                                              WHEN FL.Description = 'Currently Attending' THEN
                                                  'Active'
                                              WHEN FL.Description = 'Probation' THEN
                                                  'Academic Probation'
                                              WHEN FL.Description = 'Leave of Absence' THEN
                                                  'Leave of Absence'
                                              WHEN FL.Description = 'Suspended' THEN
                                                  'Suspended'
                                              WHEN FL.Description = 'Dropped Out' THEN
                                                  'Dropped'
                                              WHEN FL.Description = 'Transferred' THEN
                                                  'Transfer Out'
                                              WHEN FL.Description = 'Expelled' THEN
                                                  'Dropped'
                                              WHEN FL.Description = 'Graduated' THEN
                                                  'Graduated'
                                              WHEN FL.Description = 'Incomplete Grad' THEN
                                                  'Incomplete'
                                              WHEN FL.Description = 'Delinquent Grad' THEN
                                                  'Graduated'
										      WHEN FL.Description = 'No Start' THEN
                                                  'No Start'
                                          END
        INNER JOIN FreedomAdvantage..sySysStatus dSStatus
            ON dSStatus.SysStatusId = dSCode.SysStatusId
        LEFT JOIN bridge.syStatusCodes bIgnore
            ON bIgnore.src_Description = FL.Description
    WHERE LookupName = 'AttndStatus'
          AND bIgnore.src_ID IS NULL;


    INSERT INTO @syStatusCodes
    SELECT dSCode.StatusCodeId, --uniqueidentifier
           dSStatus.StatusLevelId,
           FL.ID,
           FL.Description,
           dSCode.StatusCodeDescrip,
           FL.LookUpID,
           NULL AS src_RecNum   --this is for Lead Status
    FROM stage.FLookUp FL
        INNER JOIN FreedomAdvantage.dbo.syStatusCodes dSCode
            ON dSCode.StatusCodeDescrip = CASE
                                              WHEN FL.Description = 'Incomplete Grad' THEN
                                                  'Completed - Pending Academics'
                                              ELSE
                                                  FL.Description
                                          END
        INNER JOIN FreedomAdvantage..sySysStatus dSStatus
            ON dSStatus.SysStatusId = dSCode.SysStatusId
        LEFT JOIN bridge.syStatusCodes bIgnore
            ON bIgnore.src_Description = FL.Description
    WHERE LookupName = 'AttndStatus'
          AND NOT EXISTS
    (
        SELECT TOP 1 src_ID FROM @syStatusCodes R WHERE R.src_ID = FL.ID
    );
    --AND bIgnore.src_ID IS NULL;

    INSERT INTO FreedomAdvantage..syStatusCodes
    SELECT NEWID() AS StatusCodeId,            -- uniqueidentifier
           'AL' AS StatusCode,                 -- varchar(20)
           'Active lead' AS StatusCodeDescrip, -- varchar(80)
           dStatus.StatusId,                   -- uniqueidentifier
           dCampGrp.CampGrpId,                 -- uniqueidentifier
           dSStatus.SysStatusId,               -- SysStatusId -- int
           GETDATE() AS ModDate,               -- datetime
           'sa' AS ModUser,                    -- varchar(50)
           NULL AS AcadProbation,              -- bit
           NULL AS DiscProbation,              -- bit
           0 AS IsDefaultLeadStatus            -- bit
    FROM FreedomAdvantage..syStatuses dStatus
        LEFT JOIN FreedomAdvantage..syCampGrps dCampGrp
            ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..sySysStatus dSStatus
            ON dSStatus.SysStatusDescrip = 'Enrolled'
        LEFT JOIN FreedomAdvantage..syStatusCodes dIgnoreCode
            ON dIgnoreCode.StatusCodeDescrip = 'Active lead'
    WHERE dStatus.Status = 'Active'
          AND dIgnoreCode.StatusCodeId IS NULL
          AND dSStatus.StatusLevelId = 1;

    INSERT INTO FreedomAdvantage..syStatusCodes
    SELECT NEWID() AS StatusCodeId,      -- uniqueidentifier
           'ERR' AS StatusCode,          -- varchar(20)
           'Error' AS StatusCodeDescrip, -- varchar(80)
           dStatus.StatusId,             -- uniqueidentifier
           dCampGrp.CampGrpId,           -- uniqueidentifier
           dSStatus.SysStatusId,         -- SysStatusId -- int
           GETDATE() AS ModDate,         -- datetime
           'sa' AS ModUser,              -- varchar(50)
           NULL AS AcadProbation,        -- bit
           NULL AS DiscProbation,        -- bit
           0 AS IsDefaultLeadStatus      -- bit
    FROM FreedomAdvantage..syStatuses dStatus
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp
            ON dCampGrp.CampGrpCode = 'ALL'
        INNER JOIN FreedomAdvantage..sySysStatus dSStatus
            ON dSStatus.SysStatusDescrip = 'Dead Lead'
        LEFT JOIN FreedomAdvantage..syStatusCodes dIgnoreCode
            ON dIgnoreCode.StatusCodeDescrip = 'Error'
    WHERE dStatus.Status = 'Active'
          AND dIgnoreCode.StatusCodeId IS NULL
          AND dSStatus.StatusLevelId = 1;

    INSERT INTO @syStatusCodes
    SELECT dSCode.StatusCodeId,
           dSStatus.StatusLevelId,
           NULL AS ID,
           sUCode.Description AS src_Description,
           dSCode.StatusCodeDescrip + ' ' + CAST(sUCode.RecNum AS VARCHAR(10)) AS trg_Description,
           NULL AS LookUpID,
           sUCode.RecNum AS src_RecNum
    FROM Freedom..USERCODE_FIL sUCode
        INNER JOIN FreedomAdvantage..syStatusCodes dSCode
            ON dSCode.StatusCodeDescrip = CASE
                                              WHEN sUCode.Description = 'Interview' THEN
                                                  'Interviewed'
                                              WHEN sUCode.Description = 'Not Interested' THEN
                                                  'Not Interested'
                                              WHEN sUCode.Description = 'Enrolled' THEN
                                                  'Enrolled'
                                              WHEN sUCode.Description = 'Start' THEN
                                                  'New Lead'
                                              WHEN sUCode.Description = 'Active' THEN
                                                  'Active Lead'
                                              WHEN sUCode.Description = 'Error' THEN
                                                  'Error'
                                              WHEN sUCode.Description = 'DNC' THEN
                                                  'Do Not Contact'
                                              WHEN sUCode.Description = 'Chose Another School' THEN
                                                  'Not Interested'
                                              WHEN sUCode.Description = 'Future High Schoo' THEN
                                                  'Rescheduled'
                                              WHEN sUCode.Description = 'Does not Speak Engli' THEN
                                                  'Do Not Contact'
                                              WHEN sUCode.Description = 'NO HS Diploma/GED' THEN
                                                  'Rescheduled'
                                              WHEN sUCode.Description = 'Incorrect Contact In' THEN
                                                  'Do Not Contact'
                                              WHEN sUCode.Description = 'Duplicate Lead' THEN
                                                  'Error'
                                          END
        LEFT JOIN FreedomAdvantage..sySysStatus dSStatus
            ON dSStatus.SysStatusId = dSCode.SysStatusId
               AND dSStatus.StatusLevelId = 1
    WHERE sUCode.CodeKey = 'LeadStatus'
          AND sUCode.Active = 'Y';

    INSERT INTO FreedomBridge.bridge.syStatusCodes
    SELECT trg_StatusCodeId,
           trg_StatusLevelId,
           src_ID,
           src_Description,
           trg_Description,
           src_LookUpID,
           src_RecNum
    FROM @syStatusCodes N
    WHERE NOT EXISTS
    (
        SELECT TOP 1
               E.src_ID
        FROM bridge.syStatusCodes E
        WHERE E.src_ID = N.src_ID
              OR E.src_RecNum = N.src_RecNum
    );


    UPDATE statusCodes
    SET statusCodes.StatusCodeDescrip = 'Currently Attending'
    FROM FreedomAdvantage.dbo.syStatusCodes statusCodes
    WHERE statusCodes.SysStatusId = 9
          AND statusCodes.StatusCodeDescrip = 'Active';

    UPDATE statusCodes
    SET statusCodes.StatusCodeDescrip = 'Dropped Out'
    FROM FreedomAdvantage.dbo.syStatusCodes statusCodes
    WHERE statusCodes.SysStatusId = 12
          AND statusCodes.StatusCodeDescrip = 'Dropped';

    UPDATE statusCodes
    SET statusCodes.StatusCodeDescrip = 'Transferred'
    FROM FreedomAdvantage.dbo.syStatusCodes statusCodes
    WHERE statusCodes.SysStatusId = 19
          AND statusCodes.StatusCodeDescrip = 'Transferred Program';

    INSERT INTO FreedomAdvantage.dbo.syStatusCodes
    (
        StatusCodeId,
        StatusCode,
        StatusCodeDescrip,
        StatusId,
        CampGrpId,
        SysStatusId,
        ModDate,
        ModUser,
        AcadProbation,
        DiscProbation,
        IsDefaultLeadStatus
    )
    SELECT NEWID(),               -- StatusCodeId - uniqueidentifier
           'Probation',           -- StatusCode - varchar(20)
           'Probation',           -- StatusCodeDescrip - varchar(80)
           statuses.StatusId,     -- StatusId - uniqueidentifier
           campusGroup.CampGrpId, -- CampGrpId - uniqueidentifier
           9,                     -- SysStatusId - int
           GETDATE(),             -- ModDate - datetime
           'sa',                  -- ModUser - varchar(50)
           NULL,                  -- AcadProbation - bit
           NULL,                  -- DiscProbation - bit
           NULL                   -- IsDefaultLeadStatus - bit
    FROM FreedomAdvantage.dbo.syStatuses statuses
        INNER JOIN FreedomAdvantage.dbo.syCampGrps campusGroup
            ON campusGroup.StatusId = statuses.StatusId
    WHERE statuses.StatusCode = 'A'
          AND NOT EXISTS
    (
        SELECT TOP 1
               *
        FROM FreedomAdvantage..syStatusCodes T
        WHERE T.StatusCode = 'Probation'
              AND T.StatusCodeDescrip = 'Probation'
              AND T.CampGrpId = campusGroup.CampGrpId
    )
          AND campusGroup.IsAllCampusGrp = 1;

    DELETE FROM FreedomAdvantage.dbo.syStatusCodes
    WHERE SysStatusId = 19
          AND StatusCodeDescrip = 'Transferred Campus';

    --insert expelled status
    INSERT INTO FreedomAdvantage..syStatusCodes
    (
        StatusCodeId,
        StatusCode,
        StatusCodeDescrip,
        StatusId,
        CampGrpId,
        SysStatusId,
        ModDate,
        ModUser,
        AcadProbation,
        DiscProbation,
        IsDefaultLeadStatus
    )
    SELECT NEWID(),
           UPPER(src_Description),
           src_Description,
           (
               SELECT StatusId FROM FreedomAdvantage..syStatuses WHERE StatusCode = 'A'
           ),
           (
               SELECT CampGrpId
               FROM FreedomAdvantage..syCampGrps
               WHERE IsAllCampusGrp = 1
           ),
           12,
           GETDATE(),
           'sa',
           NULL,
           NULL,
           NULL
    FROM FreedomBridge.bridge.syStatusCodes
    WHERE src_Description = 'Expelled'
          AND NOT EXISTS
    (
        SELECT TOP 1
               *
        FROM FreedomAdvantage..syStatusCodes T
        WHERE T.StatusCode = 'Expelled'
              AND T.StatusCodeDescrip = 'Expelled'
              AND T.CampGrpId IN
                  (
                      SELECT CampGrpId
                      FROM FreedomAdvantage..syCampGrps
                      WHERE IsAllCampusGrp = 1
                  )
    );

    --update bridge table for expelled status
    UPDATE FreedomBridge.bridge.syStatusCodes
    SET trg_StatusCodeId =
        (
            SELECT TOP 1
                   StatusCodeId
            FROM FreedomAdvantage..syStatusCodes
            WHERE StatusCode = 'EXPELLED'
        )
    WHERE src_Description = 'Expelled';

	    UPDATE FreedomBridge.bridge.syStatusCodes
    SET trg_StatusCodeId =
        (
            SELECT TOP 1
                   StatusCodeId
            FROM FreedomAdvantage..syStatusCodes
            WHERE StatusCode = 'DG'
        )
    WHERE src_Description = 'Delinquent Grad';

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_syStatusCodes]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];

    COMMIT TRANSACTION;

END;




GO
