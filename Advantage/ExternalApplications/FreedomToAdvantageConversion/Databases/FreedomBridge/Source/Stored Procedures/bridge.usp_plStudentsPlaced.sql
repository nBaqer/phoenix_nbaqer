SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_plStudentsPlaced]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @ActiveStatusId = Value
        FROM   #Settings
        WHERE  Entity = 'ActiveStatusId';


        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        DECLARE @Conversion VARCHAR(10) = 'Conversion';

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..plJobStatus
                      WHERE  JobStatusDescrip = 'Active'
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..plJobStatus
                VALUES ( NEWID()           -- JobStatusId - uniqueidentifier
                        ,'Active'          -- JobStatusDescrip - varchar(50)
                        ,@ActiveStatusId   -- StatusId - uniqueidentifier
                        ,'A'               -- JobCode - varchar(12)
                        ,@Conversion       -- ModUser - varchar(50)
                        ,GETDATE()         -- ModDate - datetime
                        ,@CampusGroupAllId -- CampGrpId - uniqueidentifier
                    );
            END;

        SELECT     NEWID() AS PlacementId                                                 -- uniqueidentifier
                  ,NULL AS Supervisor                                                     -- varchar(80)
                  ,dJobStatus.JobStatusId AS JobStatusId                                  -- uniqueidentifier
                  ,dbo.udf_GetNumericOnly(sStud.StuSSN) AS SSN                            -- varchar(50)
                  ,sGrad.HiredDate AS StartDate                                           -- datetime
                  ,sGrad.ApxSalary AS Salary                                              -- decimal(18,2) NO DATA
                  ,NULL AS TerminationReason                                              -- varchar(100) NO DATA
                  ,NULL AS TerminationDate                                                -- datetime NO DATA
                  ,NULL AS Notes                                                          -- varchar(500) -- Notes are going to syStudentNotes
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS WorkDaysId                           -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS BenefitsId                           -- uniqueidentifier NO DATA
                  ,sGrad.EmpName AS JobDescrip                                            -- varchar(200)
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS PlacementRep                         -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ScheduleId                           -- uniqueidentifier NO DATA
                  ,NULL AS Reason                                                         -- varchar(300) NO DATA
                  ,GETDATE() AS ModDate                                                   -- datetime
                  ,'SA' AS ModUser                                                        -- varchar(50)
                  ,dSalaryType.SalaryTypeId AS SalaryTypeId                               -- uniqueidentifier NO DATA
                  ,sGrad.HiredDate AS PlacedDate                                          -- datetime
                  ,dInterview.InterviewId AS InterviewId                                  -- uniqueidentifier NO DATA
                  ,dStudy.FldStudyId AS FldStudyId                                        -- uniqueidentifier
                  ,bEnr.trg_StuEnrollId AS StuEnrollId                                    -- uniqueidentifier
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS Fee                                  -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS HowPlacedId                          -- uniqueidentifier NO DAtA
                  ,ISNULL(sStud.RevisedGradDate, sStud.ContractedGradDt) AS GraduatedDate -- datetime
                  ,bJob.trg_EmployerJobId AS EmployerJobId                                -- uniqueidentifier
                  ,sGrad.RecordKey
        INTO       #plStudentsPlaced
        FROM       bridge.plEmployerJobs bJob
        INNER JOIN Freedom..GRADPLAC_FIL sGrad ON sGrad.RecordKey = bJob.src_RecordKey
        INNER JOIN FreedomAdvantage..plJobStatus dJobStatus ON dJobStatus.JobStatusDescrip = 'Active'
        INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = sGrad.StudrecKey
        INNER JOIN bridge.arStuEnrollments bEnr ON bEnr.src_RecordKey = sStud.RecordKey
                                                   AND bEnr.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..plInterview dInterview ON dInterview.InterViewCode = 'No'
        LEFT JOIN  FreedomAdvantage..plFldStudy dStudy ON dStudy.FldStudyCode = CASE WHEN sGrad.InIndustryFlag = 'N' THEN 'No'
                                                                                     WHEN sGrad.InIndustryFlag = 'S' THEN 'No'
                                                                                     WHEN sGrad.InIndustryFlag = 'X' THEN 'No'
                                                                                     WHEN sGrad.InIndustryFlag = 'Y' THEN 'Yes'
                                                                                END
        INNER JOIN FreedomAdvantage..plSalaryType dSalaryType ON dSalaryType.SalaryTypeCode = 'Per Year'
        WHERE      bJob.trg_CampusId = @CampusId;

        --InIndustryFlag
        --ID	Description
        --N	Not industry related No
        --S	Self employed in industry No
        --X	Requested not to be placed No
        --Y	Industry related Yes

        ALTER TABLE FreedomAdvantage..PlStudentsPlaced DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage.dbo.PlStudentsPlaced
                    SELECT PlacementId
                          ,Supervisor
                          ,JobStatusId
                          ,SSN
                          ,StartDate
                          ,Salary
                          ,TerminationReason
                          ,TerminationDate
                          ,Notes
                          ,WorkDaysId
                          ,BenefitsId
                          ,JobDescrip
                          ,PlacementRep
                          ,ScheduleId
                          ,Reason
                          ,ModDate
                          ,ModUser
                          ,SalaryTypeId
                          ,PlacedDate
                          ,InterviewId
                          ,FldStudyId
                          ,StuEnrollId
                          ,Fee
                          ,HowPlacedId
                          ,GraduatedDate
                          ,EmployerJobId
                    FROM   #plStudentsPlaced;
        ALTER TABLE FreedomAdvantage..PlStudentsPlaced ENABLE TRIGGER ALL;

        INSERT INTO bridge.plStudentsPlaced
                    SELECT PlacementId AS trg_EmployerJobId
                          ,RecordKey AS src_RecordKey
                          ,@CampusId AS trg_CampusId
                    FROM   #plStudentsPlaced;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_plStudentsPlaced]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
