SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_InsertSetupDataCampusBased]
AS
    BEGIN

        BEGIN TRANSACTION ScriptTransaction;
        BEGIN TRY


            DECLARE @CampGrpId UNIQUEIDENTIFIER
                   ,@CampusGroupAllId UNIQUEIDENTIFIER
                   ,@CampusGroupToUseId UNIQUEIDENTIFIER;

            DECLARE @ConversionSA VARCHAR(50) = 'Conversion SA';
            DECLARE @ExecutionTime DATETIME2 = GETDATE();

            SELECT Entity
                  ,Value
            INTO   #Settings
            FROM   dbo.udf_GetSettingsList();

            SELECT @CampGrpId = Value
            FROM   #Settings
            WHERE  Entity = 'CampusGroupID';

            SELECT @CampusGroupAllId = Value
            FROM   #Settings
            WHERE  Entity = 'CampusGroupAllId';

            DECLARE @NewCampusGroupId UNIQUEIDENTIFIER;
            DECLARE @AllCampusGroupId UNIQUEIDENTIFIER;

            SET @AllCampusGroupId = @CampusGroupAllId;

            SET @NewCampusGroupId = @CampGrpId;

            UPDATE FreedomAdvantage..adAgencySponsors
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adColleges
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adCounties
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adExtraCurr
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adExtraCurrGrp
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adHighSchools
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adImportLeadsMappings
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adLeadGroups
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adReqGroups
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adReqs
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adSourceAdvertisement
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adSourceCatagory
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adSourceType
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..adTitles
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arBkCategories
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arBooks
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arBuildings
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arCourseCategories
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arFERPACategory
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arFERPAEntity
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arGradeScales
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arGradeSystems
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arGrdBkEvalTyps
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arGrdComponentTypes
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arPrgGrp
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arPrgVersionFees
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arPrgVersions
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arProgCredential
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arPrograms
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arProgTypes
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arReqs
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arSAP
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arSDateSetup
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..arTerm
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..cmDocuments
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..faLenders
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..msgGroups
            SET    CampGroupId = @NewCampusGroupId
            WHERE  CampGroupId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..msgRules
            SET    CampGroupId = @NewCampusGroupId
            WHERE  CampGroupId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..msgTemplates
            SET    CampGroupId = @NewCampusGroupId
            WHERE  CampGroupId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plEmployerJobs
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plEmployers
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plIndustries
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plJobCats
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plLocations
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plSkillGroups
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..plSkills
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saBankAccounts
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saBankCodes
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saGLAccounts
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saPaymentDescriptions
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saRateSchedules
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saTuitionCategories
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..saTuitionEarnings
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syAnnouncements
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syDepartments
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syEvents
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syHolidays
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syHomePageNotes
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syHRDepartments
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syMessageSchemas
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syMessageTemplates
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syPeriods
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syPositions
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syRDFSiteSums
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syReasonNotEnrolled
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syRelationshipStatus
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syStuRestrictionTypes
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;
            UPDATE FreedomAdvantage..syUserResources
            SET    CampGrpId = @NewCampusGroupId
            WHERE  CampGrpId = @AllCampusGroupId;

        --SELECT ' UPDATE FreedomAdvantage..'+TableName+'
        --    SET '+ColumnName+' = @NewCampusGroupId
        --    WHERE '+ColumnName+' = @AllCampusGroupId;' FROM [bridge].[CampusGroupAllRules]
        --WHERE IsCampusAll = 0


        END TRY
        BEGIN CATCH
            SELECT ERROR_NUMBER() AS ErrorNumber
                  ,ERROR_SEVERITY() AS ErrorSeverity
                  ,ERROR_STATE() AS ErrorState
                  ,ERROR_PROCEDURE() AS ErrorProcedure
                  ,ERROR_LINE() AS ErrorLine
                  ,ERROR_MESSAGE() AS ErrorMessage;

            IF @@TRANCOUNT > 0
                BEGIN
                    ROLLBACK TRANSACTION ScriptTransaction;
                END;
        END CATCH;

        IF @@TRANCOUNT > 0
            BEGIN
                COMMIT TRANSACTION ScriptTransaction;
            END;




    END;

GO
