SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-02-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_rptInstructor]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /******** Add users to rptInstructor table so user is available in options list for reports ****************/

        INSERT INTO FreedomAdvantage..rptInstructor
                    SELECT NEWID() AS InstructorID             -- uniqueidentifier
                         , sUser.FullName AS InstructorDescrip -- varchar(150)
                    FROM   FreedomBridge.bridge.syUsers bUser
                    INNER JOIN FreedomAdvantage..syUsers sUser ON sUser.UserId = bUser.trg_UserId
                    LEFT JOIN FreedomAdvantage..rptInstructor dInstruct ON dInstruct.InstructorDescrip = sUser.FullName
                    WHERE  dInstruct.InstructorDescrip IS NULL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_rptInstructor]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
