SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_faStudentAwardSchedule]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        --THROW 1, 'exe', 1;
        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@CurrentAward INT
               ,@CurrentAwardAmount DECIMAL(18, 2)
               ,@CurrentDisbursement INT;

        DECLARE @DisbursmentMerged TABLE
            (
                AwardRecordKey INT
               ,Amount DECIMAL(18, 2)
               ,SEQ INT
               ,TransDate DATETIME2 NULL
               ,CkRcptNum INT
               ,TranRecKey INT NULL
               ,Studreckey INT
            );

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        DECLARE @maxRecords INT = ((
                                   SELECT COUNT(*)
                                   FROM   [Freedom]..STUTRANS_FIL
                                   ) + 1
                                  );

        SELECT   RecordKey
                ,Amount
                ,UPVT.StudrecKey
                ,ROW_NUMBER() OVER ( PARTITION BY RecordKey
                                     ORDER BY RecordKey
                                             ,Disbursement
                                   ) AS SEQ
        INTO     #DisbursementLog
        FROM     (
                 SELECT Disb1Amt
                       ,Disb2Amt
                       ,Disb3Amt
                       ,Disb4Amt
                       ,RecordKey
                       ,sFALog1.StudrecKey
                 FROM   Freedom..FALOGFIL sFALog1
                 WHERE  (
                        SELECT COUNT(*)
                        FROM   Freedom..STUTRANS_FIL T
                        WHERE  T.AwardLogKey = sFALog1.RecordKey
                               AND T.DebitCredit = 1
                        ) <= 4
                 --WHERE sFALog1.StudrecKey != 0
                 ) AS PVT UNPIVOT(Amount FOR Disbursement IN(Disb1Amt, Disb2Amt, Disb3Amt, Disb4Amt)) AS UPVT
        ORDER BY RecordKey
                ,Disbursement;

        SELECT AwardLogKey
              ,Amount / 100.00 AS Amount
              ,ROW_NUMBER() OVER ( PARTITION BY AwardLogKey
                                   ORDER BY DATEADD(MILLISECOND, CASE WHEN M.NextRec = 0 THEN @maxRecords ELSE M.NextRec end, TransDate)
                                 ) AS SEQ
              ,DATEADD(MILLISECOND, CASE WHEN M.NextRec = 0 THEN @maxRecords ELSE M.NextRec end, TransDate) AS transdate
              ,CkRcptNum
              ,RecordKey AS TranRecKey
              ,M.StudrecKey
        INTO   #AwTransaction
        FROM   Freedom..STUTRANS_FIL M
        WHERE  M.AwardLogKey > 0
               AND (
                   SELECT COUNT(*)
                   FROM   Freedom..STUTRANS_FIL T
                   WHERE  T.AwardLogKey = M.AwardLogKey
                          AND T.DebitCredit = 1
                   ) <= 4
               AND M.ReversedTransKey = 0
               AND M.DebitCredit = 1;


        ---PART FOR SPECIAL CASES
        SELECT   RecordKey
                ,Amount
                ,UPVT.StudrecKey
                ,ROW_NUMBER() OVER ( PARTITION BY RecordKey
                                     ORDER BY RecordKey
                                             ,Disbursement
                                   ) AS SEQ
        INTO     #DisbursementLogSpecialCase
        FROM     (
                 SELECT Disb1Amt
                       ,Disb2Amt
                       ,Disb3Amt
                       ,Disb4Amt
                       ,RecordKey
                       ,sFALog1.StudrecKey
                 FROM   Freedom..FALOGFIL sFALog1
                 WHERE  (
                        SELECT COUNT(*)
                        FROM   Freedom..STUTRANS_FIL T
                        WHERE  T.AwardLogKey = sFALog1.RecordKey
                               AND T.DebitCredit = 1
                        ) > 4
                 --WHERE sFALog1.StudrecKey != 0
                 ) AS PVT UNPIVOT(Amount FOR Disbursement IN(Disb1Amt, Disb2Amt, Disb3Amt, Disb4Amt)) AS UPVT
        ORDER BY RecordKey
                ,Disbursement;

        SELECT AwardLogKey
              ,Amount / 100.00 AS Amount
              ,ROW_NUMBER() OVER ( PARTITION BY AwardLogKey
                                   ORDER BY DATEADD(MILLISECOND, CASE WHEN M.NextRec = 0 THEN @maxRecords ELSE M.NextRec end, TransDate)
                                 ) AS SEQ
              ,DATEADD(MILLISECOND, CASE WHEN M.NextRec = 0 THEN @maxRecords ELSE M.NextRec end, TransDate) AS transdate
              ,CkRcptNum
              ,RecordKey AS TranRecKey
              ,M.DebitCredit
              ,0 AS proccessed
              ,M.StudrecKey
        INTO   #AwTransactionSpecialCase
        FROM   Freedom..STUTRANS_FIL M
        WHERE  M.AwardLogKey > 0
               AND (
                   SELECT COUNT(*)
                   FROM   Freedom..STUTRANS_FIL T
                   WHERE  T.AwardLogKey = M.AwardLogKey
                          AND T.DebitCredit = 1
                   ) > 4
               AND M.ReversedTransKey = 0;
        --AND m.DebitCredit = 1

        DECLARE award_Cursor CURSOR FOR
            SELECT DISTINCT RecordKey
                           ,SEQ
                           ,Amount
            FROM   #DisbursementLogSpecialCase;

        OPEN award_Cursor;
        FETCH NEXT FROM award_Cursor
        INTO @CurrentAward
            ,@CurrentDisbursement
            ,@CurrentAwardAmount;


        --iterating thru schedules disbursements
        WHILE @@FETCH_STATUS = 0
            BEGIN
                PRINT @CurrentAward;
                DECLARE @currentTotal DECIMAL(18, 2) = 0;
                DECLARE @curTranAmount DECIMAL(18, 2) = 0;
                DECLARE @curTranDate DATETIME2;
                DECLARE @curCheckReceipt INT;
                DECLARE @curAwardKey INT;
                DECLARE @curTranRecKey INT;
                DECLARE @curDebitCredit BIT;
                DECLARE @curStudRecKey INT;
                DECLARE @hasMatchedDisbursment BIT = 0;


                DECLARE schedule_Cursor CURSOR FOR
                    SELECT AwardLogKey
                          ,Amount
                          ,TransDate
                          ,CkRcptNum
                          ,TranRecKey
                          ,DebitCredit
                          ,StudrecKey
                    FROM   #AwTransactionSpecialCase
                    WHERE  AwardLogKey = @CurrentAward
                           AND proccessed = 0;

                OPEN schedule_Cursor;
                FETCH NEXT FROM schedule_Cursor
                INTO @curAwardKey
                    ,@curTranAmount
                    ,@curTranDate
                    ,@curCheckReceipt
                    ,@curTranRecKey
                    ,@curDebitCredit
                    ,@curStudRecKey;

                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF ( @hasMatchedDisbursment = 0 )
                            BEGIN

                                IF @curDebitCredit = 1
                                    BEGIN
                                        SET @currentTotal = @currentTotal + @curTranAmount;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @currentTotal = @currentTotal - @curTranAmount;
                                    END;


                                IF @currentTotal = @CurrentAwardAmount
                                    BEGIN
                                        SET @hasMatchedDisbursment = 1;
                                    END;

                                IF @curDebitCredit = 1
                                    BEGIN
                                        INSERT INTO @DisbursmentMerged (
                                                                       AwardRecordKey
                                                                      ,Amount
                                                                      ,SEQ
                                                                      ,TransDate
                                                                      ,CkRcptNum
                                                                      ,TranRecKey
                                                                      ,Studreckey
                                                                       )
                                        VALUES ( @curAwardKey         -- AwardRecordKey - int
                                                ,@curTranAmount       -- Amount - decimal(18, 2)
                                                ,@CurrentDisbursement -- SEQ - int
                                                ,@curTranDate         -- TransDate - datetime2
                                                ,@curCheckReceipt     -- CkRcptNum - int
                                                ,@curTranRecKey       -- TranRecKey - int
                                                ,@curStudRecKey );
                                    END;

                                UPDATE #AwTransactionSpecialCase
                                SET    proccessed = 1
                                WHERE  AwardLogKey = @curAwardKey
                                       AND Amount = @curTranAmount
                                       AND TransDate = @curTranDate
                                       AND TranRecKey = @curTranRecKey
                                       AND CkRcptNum = @curCheckReceipt;


                            END;



                        FETCH NEXT FROM schedule_Cursor
                        INTO @curAwardKey
                            ,@curTranAmount
                            ,@curTranDate
                            ,@curCheckReceipt
                            ,@curTranRecKey
                            ,@curDebitCredit
                            ,@curStudRecKey;





                    END;


                CLOSE schedule_Cursor;
                DEALLOCATE schedule_Cursor;


                FETCH NEXT FROM award_Cursor
                INTO @CurrentAward
                    ,@CurrentDisbursement
                    ,@CurrentAwardAmount;
            END;

        CLOSE award_Cursor;
        DEALLOCATE award_Cursor;
        --END SPECIAL CASES


        --Merging FALOGFIL disbursments with STUTRANS_FIL, as sometimes LOG doesnt have amount
        SELECT    DLog.RecordKey AS AwardRecordKey
                 ,COALESCE(NULLIF(DLog.Amount, 0), NULLIF(AwTran.Amount, 0), 0) AS Amount
                 ,DLog.SEQ
                 ,AwTran.TransDate
                 ,AwTran.CkRcptNum
                 ,AwTran.TranRecKey
                 ,DLog.StudrecKey
        INTO #DisbursementMerged
        FROM      #DisbursementLog DLog
        LEFT JOIN #AwTransaction AwTran ON AwTran.AwardLogKey = DLog.RecordKey
                                           AND AwTran.SEQ = DLog.SEQ
        UNION
        SELECT AwardRecordKey
              ,Amount
              ,SEQ
              ,TransDate
              ,CkRcptNum
              ,TranRecKey AS TranRecKey
              ,Studreckey
        FROM   @DisbursmentMerged;


        SELECT      DISTINCT bAward.trg_StudentAwardId AS StudentAwardId                -- uniqueidentifier
                            ,CAST(sPayPeriod.[PP Beg Date] AS DATETIME) AS ExpectedDate -- datetime
                            ,tDisb.Amount AS Amount                                     -- decimal(19,4) dispersment amount
                            ,tDisb.CkRcptNum AS Reference                               -- varchar(50) [CkRcptNum] 
                            ,'SA' AS ModUser                                            -- varchar(50)
                            ,GETDATE() AS ModDate                                       -- datetime
                            ,bTran.trg_TransactionId AS TransactionId                   -- uniqueidentifier
                            ,NULL AS TermNumber                                         -- char(2)
                            ,tDisb.CkRcptNum AS recid                                   -- varchar(50) [CkRcptNum]
                            ,CASE WHEN tDisb.TransDate IS NULL THEN 0
                                  ELSE 1
                             END AS isProcessed                                         -- bit if [STUTRANS_FIL] shows the tran
                            ,dAward.Disbursements AS DisbursementNumber                 -- tinyint
                            ,tDisb.SEQ AS SequenceNumber                                -- tinyint
                            ,tDisb.AwardRecordKey
                            ,tDisb.TranRecKey
                            ,tDisb.TransDate
                            ,sFALog.StudrecKey
        INTO        #faStudentAwardSchedule
        FROM        bridge.faStudentAwards bAward
        INNER JOIN  FreedomAdvantage..faStudentAwards dAward ON dAward.StudentAwardId = bAward.trg_StudentAwardId
                                                                AND bAward.trg_CampusId = @CampusId
        INNER JOIN  Freedom..FALOGFIL sFALog ON sFALog.RecordKey = bAward.src_RecordKey
        LEFT JOIN   #DisbursementMerged tDisb ON tDisb.AwardRecordKey = sFALog.RecordKey
        LEFT JOIN   bridge.saTransactions bTran ON bTran.src_RecordKey = tDisb.TranRecKey
                                                   AND bTran.trg_CampusId = @CampusId
        OUTER APPLY ( -- tries to find first award payment period
                    SELECT   TOP 1 sPayPeriod1.[PayPeriod No]
                    FROM     Freedom..StuPayPeriod sPayPeriod1
                    WHERE    sPayPeriod1.[Student ID] = sFALog.StudrecKey
                             AND sPayPeriod1.[PP Beg Date] = sFALog.AwardStartDate
                    ORDER BY sPayPeriod1.[PayPeriod No]
                    ) SchedDateFirstPeriod
        LEFT JOIN   Freedom..StuPayPeriod sPayPeriod ON sPayPeriod.[Student ID] = sFALog.StudrecKey
                                                        AND sPayPeriod.[PayPeriod No] = tDisb.SEQ
        WHERE       tDisb.Amount > 0;
        --ORDER BY    sFALog.StudrecKey
        --           ,tDisb.TranRecKey
        --           ,tDisb.SEQ DESC;



        SELECT   NEWID() AS AwardScheduleId
                ,*
        INTO     #faStudentAwardScheduleFinal
        FROM     #faStudentAwardSchedule
        ORDER BY TranRecKey
                ,SequenceNumber;

        --SELECT * FROM #faStudentAwardScheduleFinal WHERE StudrecKey = 882 ORDER BY StudentAwardId


        ALTER TABLE FreedomAdvantage..faStudentAwardSchedule DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..faStudentAwardSchedule
                    SELECT AwardScheduleId
                          ,StudentAwardId
                          ,ExpectedDate
                          ,Amount
                          ,Reference
                          ,ModUser
                          ,ModDate
                          ,TransactionId
                          ,TermNumber
                          ,recid
                          ,isProcessed
                          ,DisbursementNumber
                          ,ROW_NUMBER() OVER ( PARTITION BY StudentAwardId
                                               ORDER BY SequenceNumber ASC
                                                       ,TransDate ASC
                                             ) AS SEQ
                          ,NULL
                          ,NULL
                          ,NULL
                    FROM   #faStudentAwardScheduleFinal;


        ALTER TABLE FreedomAdvantage..faStudentAwardSchedule ENABLE TRIGGER ALL;

        INSERT INTO bridge.faStudentAwardSchedule
                    SELECT AwardScheduleId
                          ,StudentAwardId
                          ,AwardRecordKey
                          ,ROW_NUMBER() OVER ( PARTITION BY StudentAwardId
                                               ORDER BY C.SequenceNumber ASC
                                                       ,TransDate ASC
                                             ) AS SEQ
                          ,@CampusId AS CampusId
                          ,TranRecKey
                    FROM   #faStudentAwardScheduleFinal C;


        DROP TABLE #faStudentAwardSchedule;
        DROP TABLE #DisbursementMerged;
        DROP TABLE #DisbursementLog;
        DROP TABLE #AwTransaction;
        DROP TABLE #AwTransactionSpecialCase;
        DROP TABLE #DisbursementLogSpecialCase;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_faStudentAwardSchedule]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;



GO
