SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_cleanConfigAppSettings]
AS
    BEGIN

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@ConversionUser VARCHAR(100);

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @ConversionUser = Value
        FROM   #Settings
        WHERE  Entity = 'ConversionUser';

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @ActiveStatusId = Value
        FROM   #Settings
        WHERE  Entity = 'ActiveStatusId';


        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = '2'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'MaxNoOfGradeBookWeightings' )
                            );


        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = 'False'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'ShowRescheduleButtonOnEnrollmentTab' )
                            );


        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = (
                       SELECT TOP 1 ClientName
                       FROM   dbo.SourceDB
                       WHERE  IsDisabled = 0
                       )
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'SchoolName' )
                            );

        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = 'No'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'TimeClockClassSection' )
                            );

        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = 'byDay'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'TrackSapAttendance' )
                            );


        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = 'Yes'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'AllowCourseWeighting' )
                            );

        UPDATE [FreedomAdvantage]..syConfigAppSetValues
        SET    Value = 'No'
        WHERE  SettingId IN (
                            SELECT SettingId
                            FROM   [FreedomAdvantage]..syConfigAppSettings
                            WHERE  KeyName IN ( 'MajorsMinorsConcentrations' )
                            );





        UPDATE     tr
        SET        tr.FeeLevelId = 5
        FROM       [FreedomAdvantage]..arStudent st
        INNER JOIN [FreedomAdvantage]..arStuEnrollments se ON se.StudentId = st.StudentId
        INNER JOIN [FreedomAdvantage]..saTransactions tr ON tr.StuEnrollId = se.StuEnrollId
        LEFT JOIN  [FreedomAdvantage]..saFeeLevels fl ON fl.FeeLevelId = tr.FeeLevelId
        INNER JOIN [FreedomAdvantage]..saTransTypes tt ON tt.TransTypeId = tr.TransTypeId
        INNER JOIN [FreedomAdvantage]..saTransCodes tc ON tc.TransCodeId = tr.TransCodeId
        WHERE      tt.Description = 'Reversal'
                   AND tc.TransCodeDescrip IN ( 'Tuition Charge', 'Books and Kits Charge' )
                   AND tr.FeeLevelId IS NULL
                   AND se.CampusId = @CampusId;


        -- if the school being executed  ( disabled = 0 ) is fresh conversion ( first campus for client ) force clean db  = 1
        IF EXISTS (
                  SELECT 1
                  FROM   dbo.SourceDB
                  WHERE  IsDisabled = 0
                         AND ForceACleanDB = 1
                  )
            BEGIN

                UPDATE    V
                SET       V.Value = ''
                FROM      [FreedomAdvantage]..syConfigAppSettings K
                LEFT JOIN [FreedomAdvantage]..syConfigAppSetValues V ON V.SettingId = K.SettingId
                WHERE     K.KeyName LIKE '%SMTP%';

            END;

        DECLARE @FrontDeskId UNIQUEIDENTIFIER = (
                                                SELECT   TOP 1 RoleId
                                                FROM     [FreedomAdvantage]..syRoles
                                                WHERE    Role IN ( 'Front Desk', 'Guest Services' )
                                                         OR SysRoleId = 2
                                                ORDER BY Role
                                                );
        DECLARE @resourceId INT = (
                                  SELECT TOP 1 ResourceID
                                  FROM   [FreedomAdvantage]..syResources
                                  WHERE  Resource LIKE 'Edit TimeClock'
                                  );

        IF NOT EXISTS (
                      SELECT *
                      FROM   [FreedomAdvantage]..syRlsResLvls
                      WHERE  RoleId = @FrontDeskId
                             AND ResourceID = @resourceId
                      )
            BEGIN
                INSERT INTO [FreedomAdvantage]..syRlsResLvls (
                                                             RRLId
                                                            ,RoleId
                                                            ,ResourceID
                                                            ,AccessLevel
                                                            ,ModDate
                                                            ,ModUser
                                                            ,ParentId
                                                             )
                VALUES ( NEWID()         -- RRLId - uniqueidentifier
                        ,@FrontDeskId    -- RoleId - uniqueidentifier
                        ,@resourceId     -- ResourceID - smallint
                        ,15              -- AccessLevel - smallint
                        ,GETDATE()       -- ModDate - datetime
                        ,@ConversionUser -- ModUser - varchar(50)
                        ,NULL            -- ParentId - int
                    );
            END;

        ------------------------------------------------------------------------------------------------------
        ---------------DELETE ATTENDANCE WHEN LOA
        ------------------------------------------------------------------------------------------------------


        DELETE A
        FROM   [FreedomAdvantage]..arStudentClockAttendance A
        WHERE  EXISTS (
                      SELECT *
                      FROM   [FreedomAdvantage]..arStudentLOAs LOA
                      WHERE  A.RecordDate >= LOA.StartDate
                             AND A.RecordDate <= LOA.EndDate
                             AND A.StuEnrollId = LOA.StuEnrollId
                             AND A.ActualHours IN ( 0, 999, 9999 )
                             AND A.SchedHours IN ( 0, 999, 9999 )
                      );


        ---------------------------------------------------------------------------------------
        ----------------SET PROGRAM VERSION TYPE 0
        ------------------------------------------------------------------------------------------
        UPDATE [FreedomAdvantage]..arStuEnrollments
        SET    PrgVersionTypeId = 0
        WHERE  PrgVersionTypeId > 0
               AND CampusId = @CampusId;



        ---------------------------------------------------------------------------------------
        ----------------Clean UP Future Start
        ------------------------------------------------------------------------------------------

        DELETE FROM [FreedomAdvantage]..arGrdBkResults
        WHERE GrdBkResultId IN (
                               SELECT     gradeBooksResults.GrdBkResultId
                               FROM       [FreedomAdvantage]..arStuEnrollments enrollment
                               INNER JOIN [FreedomAdvantage]..syStatusCodes codes ON codes.StatusCodeId = enrollment.StatusCodeId
                               INNER JOIN [FreedomAdvantage]..arGrdBkResults gradeBooksResults ON gradeBooksResults.StuEnrollId = enrollment.StuEnrollId
                               WHERE      codes.SysStatusId = 7
                                          AND enrollment.CampusId = @CampusId
                               );

        DELETE FROM [FreedomAdvantage]..arResults
        WHERE ResultId IN (
                          SELECT     results.ResultId
                          FROM       [FreedomAdvantage]..arStuEnrollments enrollment
                          INNER JOIN [FreedomAdvantage]..syStatusCodes codes ON codes.StatusCodeId = enrollment.StatusCodeId
                          INNER JOIN [FreedomAdvantage]..arResults results ON results.StuEnrollId = enrollment.StuEnrollId
                          WHERE      codes.SysStatusId = 7
                                     AND enrollment.CampusId = @CampusId
                          );

        ------------------------------------------------------------------------------------------------------
        ---------------UPDATE TRANSACTION PERIODS
        ------------------------------------------------------------------------------------------------------

        UPDATE     Adv
        SET        Adv.PaymentPeriodNumber = freedomToAdv.PeriodNumber
        FROM       [FreedomAdvantage]..saTransactions Adv
        INNER JOIN (
                   SELECT     TransactionId
                             ,PeriodNumber
                   FROM       [FreedomAdvantage]..saTransactions
                   INNER JOIN [FreedomAdvantage]..saPmtPeriods ON saPmtPeriods.PmtPeriodId = saTransactions.PmtPeriodId
                   ) freedomToAdv ON Adv.TransactionId = freedomToAdv.TransactionId
        WHERE      Adv.CampusId = @CampusId;


        UPDATE     trans
        SET        trans.PaymentPeriodNumber = freedomToAdv.ChargePrdSeq
        FROM       [FreedomAdvantage]..saTransactions trans
        INNER JOIN (
                   SELECT *
                   FROM   (
                          SELECT     sfil.ChargePrdSeq
                                    ,advTrans.TransactionId
                          FROM       bridge.saTransactions st
                          INNER JOIN Freedom..[STUTRANS_FIL] sfil ON st.src_RecordKey = sfil.RecordKey
                          INNER JOIN [FreedomAdvantage]..saTransactions advTrans ON advTrans.TransactionId = st.trg_TransactionId
                          WHERE      sfil.ChargePrdType = 2
                          ) temp
                   ) freedomToAdv ON trans.TransactionId = freedomToAdv.TransactionId
        WHERE      trans.CampusId = @CampusId;



        ----------------------------------------------------------------------------------
        --CLEAN UP STUDENT GROUPS
        -----------------------------------------------------------
        ----INSERT MISSING LEAD LEAD GROUP RECORDS
        INSERT INTO [FreedomAdvantage]..adLeadByLeadGroups (
                                                           LeadGrpLeadId
                                                          ,LeadId
                                                          ,LeadGrpId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,StudentId
                                                          ,StuEnrollId
                                                           )
                    SELECT NEWID()
                          ,StudentGroup.LeadId
                          ,StudentGroup.LeadGrpId
                          ,@ConversionUser
                          ,'2018-10-13 19:36:32.547'
                          ,StudentGroup.StudentId
                          ,StudentGroup.StuEnrollId
                    FROM   (
                           SELECT    DISTINCT F_SG.StudrecKey
                                             ,F_SG.GroupDefKey
                                             ,L.LastName
                                             ,L.FirstName
                                             ,PV.PrgVerDescrip
                                             ,LG.Descrip
                                             ,L.StudentId
                                             ,L.LeadId
                                             ,LG.LeadGrpId
                                             ,E.StuEnrollId
                           FROM      Freedom..STUGROUP_FIL F_SG
                           LEFT JOIN bridge.adLeads B_L ON B_L.src_StuRecordKey = F_SG.StudrecKey
                           LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = B_L.trg_LeadId
                           LEFT JOIN bridge.adLeadGroups B_LG ON B_LG.src_RecordKey = F_SG.GroupDefKey
                           LEFT JOIN [FreedomAdvantage]..adLeadGroups LG ON LG.LeadGrpId = B_LG.trg_LeadGrpId
                           LEFT JOIN bridge.arStuEnrollments B_E ON B_E.src_RecordKey = F_SG.StudrecKey
                           LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = B_E.trg_StuEnrollId
                           LEFT JOIN [FreedomAdvantage]..arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                           WHERE     F_SG.StudrecKey <> 0
                                     AND L.LeadId IS NOT NULL
                                     AND NOT EXISTS (
                                                    SELECT *
                                                    FROM   [FreedomAdvantage]..adLeadByLeadGroups T
                                                    WHERE  T.LeadGrpId = LG.LeadGrpId
                                                           AND T.LeadId = L.LeadId
                                                    )
                           ) AS StudentGroup;


        -- GET ALL CORRECT LEAD LEAD GROUPS RELATIONS FROM FREEDOM
        SELECT    DISTINCT F_SG.StudrecKey
                          ,F_SG.GroupDefKey
                          ,L.LastName
                          ,L.FirstName
                          ,PV.PrgVerDescrip
                          ,LG.Descrip
                          ,L.StudentId
                          ,L.LeadId
                          ,LG.LeadGrpId
                          ,E.StuEnrollId
        INTO      #CorrectGroups
        FROM      Freedom..STUGROUP_FIL F_SG
        LEFT JOIN bridge.adLeads B_L ON B_L.src_StuRecordKey = F_SG.StudrecKey
        LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = B_L.trg_LeadId
        LEFT JOIN bridge.adLeadGroups B_LG ON B_LG.src_RecordKey = F_SG.GroupDefKey
        LEFT JOIN [FreedomAdvantage]..adLeadGroups LG ON LG.LeadGrpId = B_LG.trg_LeadGrpId
        LEFT JOIN bridge.arStuEnrollments B_E ON B_E.src_RecordKey = F_SG.StudrecKey
        LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = B_E.trg_StuEnrollId
        LEFT JOIN [FreedomAdvantage]..arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
        WHERE     F_SG.StudrecKey <> 0
        ORDER BY  L.LastName
                 ,L.FirstName;


        -- DELETE WRONG LEAD LEAD GROUPS
        DELETE FROM [FreedomAdvantage]..adLeadByLeadGroups
        WHERE LeadGrpLeadId IN (
                               SELECT    ELG.LeadGrpLeadId
                               FROM      [FreedomAdvantage]..adLeadByLeadGroups ELG
                               LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = ELG.LeadId
                               LEFT JOIN [FreedomAdvantage]..adLeadGroups LG ON LG.LeadGrpId = ELG.LeadGrpId
                               LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = ELG.StuEnrollId
                               LEFT JOIN [FreedomAdvantage]..arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE     NOT EXISTS (
                                                    SELECT *
                                                    FROM   #CorrectGroups C
                                                    WHERE  C.LeadGrpId = ELG.LeadGrpId
                                                           AND C.LeadId = ELG.LeadId
                                                           AND C.StudentId = ELG.StudentId
                                                           AND C.StuEnrollId = ELG.StuEnrollId
                                                    )
                                         AND LTRIM(RTRIM(LG.Descrip)) <> 'Freedom Imported'
                                         AND L.CampusId = @CampusId
                               );

        ----------------------------------------------------------------------------------
        --UPDATE WEIGHT FOR COURSES WHEN PV IS NOT USING WEIGHTED GPA
        ----------------------------------------------------------------------------------
        UPDATE    PVD
        SET       PVD.CourseWeight = 100 / ( CASE WHEN (
                                                       SELECT COUNT(*)
                                                       FROM   [FreedomAdvantage]..arProgVerDef T
                                                       WHERE  T.PrgVerId = PVD.PrgVerId
                                                              AND T.CourseWeight > 0
                                                       ) > 0 THEN (
                                                                  SELECT COUNT(*)
                                                                  FROM   [FreedomAdvantage]..arProgVerDef T
                                                                  WHERE  T.PrgVerId = PVD.PrgVerId
                                                                         AND T.CourseWeight > 0
                                                                  )
                                                  ELSE (
                                                       SELECT COUNT(*)
                                                       FROM   [FreedomAdvantage]..arProgVerDef T
                                                       WHERE  T.PrgVerId = PVD.PrgVerId
                                                       )
                                             END
                                           )
        FROM      [FreedomAdvantage]..arProgVerDef PVD
        LEFT JOIN [FreedomAdvantage]..arPrgVersions PV ON PV.PrgVerId = PVD.PrgVerId
        WHERE     PV.DoCourseWeightOverallGPA = 0
                  AND PVD.CourseWeight > 0;

        ---------------------------------------------------------------------------------
        --UPDATE SCHEDULES DISBURSEMENTE FOR SECOND HALF
        ----------------------------------------------------------------------------------
        DECLARE @ExecutionTime DATETIME2 = (
                                           SELECT TOP 1 ExecutionStart
                                           FROM   dbo.SourceDB
                                           WHERE  IsDisabled = 0
                                           );

        DECLARE @ExecutionYear INT = YEAR(GETDATE());
        DECLARE @SecondHalfStart DATETIME2 = '7-1-' + CAST(@ExecutionYear AS VARCHAR(4));
        SELECT @SecondHalfStart;
        --if we are only in the first half of the year, not execute on second year
        IF @ExecutionTime < @SecondHalfStart
            BEGIN


                SELECT    S.AwardScheduleId
                INTO      #AwardsScheduleToDelete
                FROM      [FreedomAdvantage]..faStudentAwardSchedule S
                LEFT JOIN [FreedomAdvantage]..faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
                LEFT JOIN [FreedomAdvantage]..saFundSources FS ON FS.FundSourceId = A.AwardTypeId
                WHERE     FS.FundSourceCode = 'PELL'
                          AND S.ExpectedDate >= @SecondHalfStart;

                SELECT   S.StudentAwardId
                        ,SUM(S.Amount) AS AmountToReduce
                INTO     #AmountToReduce
                FROM     [FreedomAdvantage]..faStudentAwardSchedule S
                WHERE    S.AwardScheduleId IN (
                                              SELECT *
                                              FROM   #AwardsScheduleToDelete
                                              )
                GROUP BY S.StudentAwardId;

                UPDATE     A
                SET        A.GrossAmount = A.GrossAmount - AR.AmountToReduce
                FROM       [FreedomAdvantage]..faStudentAwards A
                INNER JOIN #AmountToReduce AR ON AR.StudentAwardId = A.StudentAwardId;


                DELETE FROM [FreedomAdvantage]..faStudentAwardSchedule
                WHERE AwardScheduleId IN (
                                         SELECT *
                                         FROM   #AwardsScheduleToDelete
                                         );

                DROP TABLE #AmountToReduce;
                DROP TABLE #AwardsScheduleToDelete;
            END;


        ----------------------------------------------------------------------------------
        -- Add a defult for saTuitionCategories
        ----------------------------------------------------------------------------------

        IF NOT EXISTS (
                      SELECT TOP 1 1
                      FROM   [FreedomAdvantage]..saTuitionCategories E
                      WHERE  E.TuitionCategoryCode = 'DEF'
                             AND E.TuitionCategoryDescrip = 'Default'
                             AND E.CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO [FreedomAdvantage]..saTuitionCategories
                VALUES ( NEWID(), 'DEF', @ActiveStatusId, 'Default', @CampGrpId, 'Support', GETDATE(), NULL );
            END;


        ----------------------------------------------------------------------------------
        -- Assign sa as default admission rep
        ----------------------------------------------------------------------------------
        DECLARE @roleId UNIQUEIDENTIFIER;
        DECLARE @userId UNIQUEIDENTIFIER;

        IF NOT EXISTS (
                      SELECT TOP 1 UserId
                      FROM   [FreedomAdvantage]..syUsers
                      WHERE  UserName = 'sa'
                             AND CampusId = @CampusId
                      )
            BEGIN
                INSERT INTO [FreedomAdvantage]..syUsers (
                                                        UserId
                                                       ,FullName
                                                       ,Email
                                                       ,UserName
                                                       ,Password
                                                       ,ConfirmPassword
                                                       ,Salt
                                                       ,AccountActive
                                                       ,ModDate
                                                       ,ModUser
                                                       ,CampusId
                                                       ,ShowDefaultCampus
                                                       ,ModuleId
                                                       ,IsAdvantageSuperUser
                                                       ,IsDefaultAdminRep
                                                       ,IsLoggedIn
                                                       ,UserTypeId
                                                        )
                VALUES ( NEWID()          -- UserId - uniqueidentifier
                        ,'sa'             -- FullName - varchar(100)
                        ,'sa@fameinc.com' -- Email - varchar(100)
                        ,'sa'             -- UserName - varchar(50)
                        ,'test'           -- Password - varchar(40)
                        ,'test'           -- ConfirmPassword - varchar(40)
                        ,''               -- Salt - varchar(10)
                        ,1                -- AccountActive - bit
                        ,GETDATE()        -- ModDate - datetime
                        ,@ConversionUser  -- ModUser - varchar(50)
                        ,@CampusId        -- CampusId - uniqueidentifier
                        ,0                -- ShowDefaultCampus - bit
                        ,26               -- ModuleId - smallint
                        ,1                -- IsAdvantageSuperUser - bit
                        ,0                -- IsDefaultAdminRep - bit
                        ,0                -- IsLoggedIn - bit
                        ,1                -- UserTypeId - int
                    );
            END;

        SET @userId = (
                      SELECT TOP 1 UserId
                      FROM   [FreedomAdvantage]..syUsers
                      WHERE  UserName = 'sa'
                             AND CampusId = @CampusId
                      );
        SET @roleId = (
                      SELECT TOP 1 RoleId
                      FROM   [FreedomAdvantage]..syRoles
                      WHERE  SysRoleId = 3
                             AND StatusId = @ActiveStatusId
                      );

        IF NOT EXISTS (
                      SELECT TOP 1 1
                      FROM   [FreedomAdvantage]..syUsersRolesCampGrps userRoles
                      WHERE  userRoles.RoleId = @roleId
                             AND userRoles.UserId = @userId
                             AND userRoles.CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO [FreedomAdvantage]..syUsersRolesCampGrps (
                                                                     UserRoleCampGrpId
                                                                    ,UserId
                                                                    ,RoleId
                                                                    ,CampGrpId
                                                                    ,ModDate
                                                                    ,ModUser
                                                                     )
                VALUES ( NEWID()         -- UserRoleCampGrpId - uniqueidentifier
                        ,@userId         -- UserId - uniqueidentifier
                        ,@roleId         -- RoleId - uniqueidentifier
                        ,@CampGrpId      -- CampGrpId - uniqueidentifier
                        ,GETDATE()       -- ModDate - datetime
                        ,@ConversionUser -- ModUser - varchar(50)
                    );

            END;


        ----------------------------------------------------------------------------------
        --AD-14043: Projected amount must be the same as received amount for disbursed awards
        ----------------------------------------------------------------------------------
        UPDATE    S
        SET       S.Amount = ( T.TransAmount * -1 )
        FROM      [FreedomAdvantage]..faStudentAwardSchedule S
        LEFT JOIN [FreedomAdvantage]..saPmtDisbRel D ON D.AwardScheduleId = S.AwardScheduleId
        LEFT JOIN [FreedomAdvantage]..saTransactions T ON T.TransactionId = D.TransactionId
        LEFT JOIN [FreedomAdvantage]..faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
        --LEFT JOIN [FreedomAdvantage]..saFundSources FS ON FS.FundSourceId = A.AwardTypeId
        WHERE     T.TransAmount IS NOT NULL
                  AND ( T.TransAmount * -1 ) <> S.Amount;
        --AND FS.AdvFundSourceId NOT IN ( 19 ,14);

        ----------------------------------------------------------------------------------
        --AD-14752 CLEAN UP arGradeScales set campus group id 
        ----------------------------------------------------------------------------------

        UPDATE g
        SET    g.GrdSystemId = gs.GrdSystemId
        FROM   [FreedomAdvantage]..arGradeScales g
        JOIN   [FreedomAdvantage]..arGradeSystems gs ON gs.CampGrpId = g.CampGrpId
        WHERE  g.Descrip = 'Default';


        ----------------------------------------------------------------------------------
        --CLEAN UP GRADE BOOK WEIGHT DETAILS
        ----------------------------------------------------------------------------------
        EXEC bridge.usp_Cleanup_InsertMissingGradeBookWeightDetails;


        ----------------------------------------------------------------------------------
        --UPDATE SEQUENCE
        ----------------------------------------------------------------------------------
        EXEC bridge.usp_Cleanup_UpdateGradeBookWeightDetailsSequence;

        EXEC bridge.usp_InsertSetupDataCampusBased;

        ----------------------------------------------------------------------------------
        --RESET HOURS TO 0 IF ACTUAL HOURS ARE NOT GREATER THAN MINIMUM HOURS FOR DAY
        ----------------------------------------------------------------------------------

        ALTER TABLE [FreedomAdvantage]..arStudentClockAttendance DISABLE TRIGGER ALL;
        UPDATE    A
        SET       A.ActualHours = 0
        FROM      [FreedomAdvantage]..arStudentClockAttendance A
        LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = A.StuEnrollId
        LEFT JOIN [FreedomAdvantage]..arProgScheduleDetails D ON D.ScheduleId = A.ScheduleId
                                                                 AND ( DATEPART(WEEKDAY, A.RecordDate) - 1 ) = D.dw
        WHERE     E.CampusId = @CampusId
                  AND A.ActualHours < D.MinimumHoursToBePresent
                  AND D.MinimumHoursToBePresent > 0
                  AND A.ActualHours > 0;
        ALTER TABLE [FreedomAdvantage]..arStudentClockAttendance ENABLE TRIGGER ALL;


        ----------------------------------------------------------------------------------
        --CLEAN UP ACADEMIC PROBATION STUDENT CAHNGES HISTORY
        ----------------------------------------------------------------------------------

        IF EXISTS (
                  SELECT 1
                  FROM   dbo.SourceDB
                  WHERE  IsDisabled = 0
                         AND SchoolUsesAcademicProbationFromFreedom = 0
                  )
            BEGIN
                EXEC bridge.usp_cleanupAcademicProbation;
            END;
        ----------------------------------------------------------------------------------
        --CLEAN UP STUDENT STATUS CHANGES
        ----------------------------------------------------------------------------------
        --select status changes with issues
        SELECT    DISTINCT SSC.StuEnrollId
        INTO      #cleanUpNoStartEnrollmentIds
        FROM      [FreedomAdvantage]..syStudentStatusChanges SSC
        LEFT JOIN [FreedomAdvantage]..syStatusCodes SC ON SC.StatusCodeId = SSC.NewStatusId
        LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = SSC.StuEnrollId
        WHERE     SC.SysStatusId = 8
                  AND SSC.DropReasonId IS NOT NULL
                  AND E.CampusId = @CampusId;

        --delete status changes that are from future start to no start for those enrollments that need clean up
        DELETE    SSC
        FROM      [FreedomAdvantage]..syStudentStatusChanges SSC
        LEFT JOIN [FreedomAdvantage]..syStatusCodes NSC ON NSC.StatusCodeId = SSC.NewStatusId
        LEFT JOIN [FreedomAdvantage]..syStatusCodes OSC ON OSC.StatusCodeId = SSC.OrigStatusId
        WHERE     StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM   #cleanUpNoStartEnrollmentIds
                                 )
                  AND NSC.SysStatusId = 8
                  AND OSC.SysStatusId = 7
                  AND SSC.DropReasonId IS NULL;

        --update the original status id to be FS if going to NS
        UPDATE    SSC
        SET       SSC.OrigStatusId = (
                                     SELECT TOP 1 FS.StatusCodeId
                                     FROM   [FreedomAdvantage]..syStatusCodes FS
                                     WHERE  SysStatusId = 7
                                     )
        FROM      [FreedomAdvantage]..syStudentStatusChanges SSC
        LEFT JOIN [FreedomAdvantage]..syStatusCodes SC ON SC.StatusCodeId = SSC.NewStatusId
        WHERE     SC.SysStatusId = 8
                  AND SSC.DropReasonId IS NOT NULL
                  AND StuEnrollId IN (
                                     SELECT StuEnrollId
                                     FROM   #cleanUpNoStartEnrollmentIds
                                     );

        --get the futures start status code id
        DECLARE @fsStatusCodeId UNIQUEIDENTIFIER = (
                                                   SELECT TOP 1 StatusCodeId
                                                   FROM   [FreedomAdvantage]..syStatusCodes
                                                   WHERE  SysStatusId = 7
                                                   );

        --select statuses that will need to be cleaned up
        SELECT    DISTINCT SSC.StudentStatusChangeId
        INTO      #cleanUpCAToNSId
        FROM      [FreedomAdvantage]..syStudentStatusChanges SSC
        LEFT JOIN [FreedomAdvantage]..syStatusCodes NSC ON NSC.StatusCodeId = SSC.NewStatusId
        LEFT JOIN [FreedomAdvantage]..syStatusCodes OSC ON OSC.StatusCodeId = SSC.OrigStatusId
        LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = SSC.StuEnrollId
        WHERE     NSC.SysStatusId = 8
                  AND OSC.SysStatusId = 9
                  AND (
                      SELECT   TOP 1 C.OrigStatusId
                      FROM     [FreedomAdvantage]..syStudentStatusChanges C
                      WHERE    C.ModDate > SSC.ModDate
                               AND C.StuEnrollId = SSC.StuEnrollId
                      ORDER BY C.ModDate
                      ) = @fsStatusCodeId
                  AND E.CampusId = @CampusId;

        DELETE FROM [FreedomAdvantage]..syStudentStatusChanges
        WHERE StudentStatusChangeId IN (
                                       SELECT *
                                       FROM   #cleanUpCAToNSId
                                       );

        DROP TABLE #cleanUpNoStartEnrollmentIds;
        DROP TABLE #cleanUpCAToNSId;

        ----------------------------------------------------------------------------------
        --CREATE RECORDS FOR STUDENT STATUS CHANGES WHERE NOT MATCHE WITH STUDENT ENROLLMENT STATUS CODE
        ----------------------------------------------------------------------------------

        --get latest status on the student status changes table
        SELECT StudentStatusChangeId
              ,OrigStatusId
              ,NewStatusId
              ,StuEnrollId
              ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                   ORDER BY ModDate DESC
                                 ) AS rn
              ,DateOfChange
        INTO   #lastStudentStatuses
        FROM   [FreedomAdvantage]..syStudentStatusChanges;


        --check which enrollments statuses mismatch
        SELECT    E.StuEnrollId
                 ,E.StatusCodeId AS CurrentStatusId
                 ,E.CampusId
                 ,E.DropReasonId
                 ,E.ExpGradDate
                 ,E.ModDate
                 ,E.StartDate
        INTO      #EnrollmentsToFix
        FROM      [FreedomAdvantage]..arStuEnrollments E
        LEFT JOIN #lastStudentStatuses S ON S.StuEnrollId = E.StuEnrollId
                                            AND S.NewStatusId = E.StatusCodeId
        WHERE     S.StudentStatusChangeId IS NULL;

        --insert changes
        INSERT INTO [FreedomAdvantage]..syStudentStatusChanges (
                                                               StudentStatusChangeId
                                                              ,StuEnrollId
                                                              ,OrigStatusId
                                                              ,NewStatusId
                                                              ,CampusId
                                                              ,ModDate
                                                              ,ModUser
                                                              ,IsReversal
                                                              ,DropReasonId
                                                              ,DateOfChange
                                                              ,Lda
                                                              ,CaseNumber
                                                              ,RequestedBy
                                                              ,HaveBackup
                                                              ,HaveClientConfirmation
                                                               )
                    SELECT    NEWID()
                             ,F.StuEnrollId
                             ,LS.NewStatusId AS OrigStatusId
                             ,F.CurrentStatusId AS NewStatusId
                             ,F.CampusId
                             ,GETDATE()
                             ,@ConversionUser
                             ,0
                             ,CASE WHEN F.DropReasonId IS NOT NULL THEN F.DropReasonId
                                   ELSE NULL
                              END
                             ,CASE WHEN SCN.SysStatusId = 12
                                        AND F.DropReasonId IS NOT NULL
                                        AND EXISTS (
                                                   SELECT 1
                                                   FROM   [FreedomAdvantage]..arStudentClockAttendance
                                                   WHERE  StuEnrollId = F.StuEnrollId
                                                   ) THEN (
                                                          SELECT DATEADD(DAY, 1, ( MAX(RecordDate)))
                                                          FROM   [FreedomAdvantage]..arStudentClockAttendance
                                                          WHERE  StuEnrollId = F.StuEnrollId
                                                          )
                                   WHEN SCN.SysStatusId = 12
                                        AND F.DropReasonId IS NOT NULL
                                        AND NOT EXISTS (
                                                       SELECT 1
                                                       FROM   [FreedomAdvantage]..arStudentClockAttendance
                                                       WHERE  StuEnrollId = F.StuEnrollId
                                                       ) THEN F.ModDate
                                   WHEN SCN.SysStatusId = 9
                                        AND SCO.SysStatusId = 7 THEN F.StartDate
                                   WHEN SCN.SysStatusId = 14 THEN F.ExpGradDate
                                   ELSE GETDATE()
                              END
                             ,NULL
                             ,NULL
                             ,NULL
                             ,0
                             ,0
                    FROM      #EnrollmentsToFix F
                    LEFT JOIN #lastStudentStatuses LS ON LS.StuEnrollId = F.StuEnrollId
                                                         AND LS.rn = 1
                    LEFT JOIN [FreedomAdvantage]..syStatusCodes SCN ON SCN.StatusCodeId = F.CurrentStatusId
                    LEFT JOIN [FreedomAdvantage]..syStatusCodes SCO ON SCO.StatusCodeId = LS.NewStatusId
                    WHERE     SCN.SysStatusId <> 7;

        DROP TABLE #lastStudentStatuses;
        DROP TABLE #EnrollmentsToFix;

        ----------------------------------------------------------------------------------
        -- UPDATE DESCRIPTION FOR TITLE IV POLICIES
        ----------------------------------------------------------------------------------
        UPDATE P
        SET    P.SAPDescrip = CASE WHEN CHARINDEX('(Title IV)', P.SAPDescrip) > 0 THEN P.SAPDescrip
                                   ELSE P.SAPDescrip + ' (Title IV)'
                              END
        FROM   [FreedomAdvantage]..arSAP P
        WHERE  FaSapPolicy = 1;

        ----------------------------------------------------------------------------------
        --CLEAN UP PROGRAM VERSIONS WITHOUT PROGRAM
        ----------------------------------------------------------------------------------

        SELECT PV.PrgVerId
        INTO   #ProgramVersionToDelete
        FROM   [FreedomAdvantage]..arPrgVersions PV
        WHERE  PV.ProgId IS NULL
               AND PV.CampGrpId = @CampGrpId;

        SELECT StuEnrollId
        INTO   #EnrollmentsToDelete
        FROM   [FreedomAdvantage]..arStuEnrollments
        WHERE  PrgVerId IN (
                           SELECT *
                           FROM   #ProgramVersionToDelete
                           );

        DELETE FROM [FreedomAdvantage]..plExitInterview
        WHERE EnrollmentId IN (
                              SELECT *
                              FROM   #EnrollmentsToDelete
                              );



        DELETE FROM [FreedomAdvantage]..syStudentStatusChanges
        WHERE StuEnrollId IN (
                             SELECT *
                             FROM   #EnrollmentsToDelete
                             );

        DELETE FROM [FreedomAdvantage]..PlStudentsPlaced
        WHERE StuEnrollId IN (
                             SELECT *
                             FROM   #EnrollmentsToDelete
                             );

        DELETE FROM [FreedomAdvantage]..arStuEnrollments
        WHERE StuEnrollId IN (
                             SELECT *
                             FROM   #EnrollmentsToDelete
                             );

        DELETE FROM [FreedomAdvantage]..syApprovedNACCASProgramVersion
        WHERE ProgramVersionId IN (
                                  SELECT *
                                  FROM   #ProgramVersionToDelete
                                  );

        DELETE FROM [FreedomAdvantage]..arCampusPrgVersions
        WHERE PrgVerId IN (
                          SELECT *
                          FROM   #ProgramVersionToDelete
                          );


        UPDATE [FreedomAdvantage]..adLeads
        SET    PrgVerId = NULL
        WHERE  PrgVerId IN (
                           SELECT *
                           FROM   #ProgramVersionToDelete
                           );

        DELETE FROM [FreedomAdvantage]..arPrgVersions
        WHERE PrgVerId IN (
                          SELECT *
                          FROM   #ProgramVersionToDelete
                          );

        DROP TABLE #EnrollmentsToDelete;
        DROP TABLE #ProgramVersionToDelete;
        DROP TABLE #CorrectGroups;

        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_cleanConfigAppSettings]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT;

        ----------------------------------------------------------------------------------
        --AD-16011
        ----------------------------------------------------------------------------------
        EXEC bridge.usp_paymentCleanup;

        EXEC bridge.usp_UpdateConversionDataSetup;

        ----------------------------------------------------------------------------------
        --AD-16314
        ----------------------------------------------------------------------------------
        DELETE    SAS
        FROM      [FreedomAdvantage]..faStudentAwardSchedule SAS
        LEFT JOIN [FreedomAdvantage]..saPmtDisbRel D ON D.TransactionId = SAS.TransactionId
        LEFT JOIN [FreedomAdvantage]..faStudentAwards SA ON SA.StudentAwardId = SAS.StudentAwardId
        LEFT JOIN [FreedomAdvantage]..arStuEnrollments E ON E.StuEnrollId = SA.StuEnrollId
        LEFT JOIN [FreedomAdvantage]..arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
        LEFT JOIN [FreedomAdvantage]..syStatusCodes SC ON SC.StatusCodeId = E.StatusCodeId
        LEFT JOIN [FreedomAdvantage]..adLeads L ON L.LeadId = E.LeadId
        LEFT JOIN [FreedomAdvantage]..saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
        WHERE     SA.GrossAmount = 0
                  AND D.PmtDisbRelId IS NULL;

        ----------------------------------------------------------------------------------
        --AD-16260
        ----------------------------------------------------------------------------------
        EXEC bridge.usp_arStudentTimeClockPunches;




        ----------------------------------------------------------------------------------
        --FIX CLASS SECTIONS AND GRADES
        ----------------------------------------------------------------------------------

        EXEC FixClassSections;

        ----------------------------------------------------------------------------------
        --FIX FOR Completed courses
        ----------------------------------------------------------------------------------
        ALTER TABLE [FreedomAdvantage]..arResults DISABLE TRIGGER ALL;
        UPDATE FR
        SET    FR.IsCourseCompleted = CASE WHEN NOT EXISTS (
                                                           SELECT 1
                                                           FROM   [FreedomAdvantage]..arGrdBkResults C
                                                           WHERE  C.ClsSectionId = FR.TestId
                                                                  AND C.StuEnrollId = FR.StuEnrollId
                                                                  AND (
                                                                      C.Score IS NULL
                                                                      OR C.PostDate IS NULL
                                                                      OR C.DateCompleted IS NULL
                                                                      )
                                                           ) THEN 1
                                           ELSE 0
                                      END
        FROM   [FreedomAdvantage]..arResults FR;

        ALTER TABLE [FreedomAdvantage]..arResults ENABLE TRIGGER ALL;

        ----------------------------------------------------------------------------------
        --Place Holder inserts for grades
        ----------------------------------------------------------------------------------
        EXEC bridge.usp_InsertComponentPlaceHolders;
    END;












GO
