SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [stage].[usp_UpdateStoreProcedureDatabasesNames]
    @NewAdvantageName VARCHAR(100)
   ,@NewFreedomName VARCHAR(100)
   ,@OldAdvantageName VARCHAR(100) = '[FreedomAdvantage]'
   ,@OldFreedomName VARCHAR(100) = '[Freedom]'
AS
    DECLARE @Sprocs TABLE
        (
            Name VARCHAR(100)
           ,Defnition NVARCHAR(MAX)
           ,AlterSproc NVARCHAR(MAX)
        );

    DECLARE @SprocName VARCHAR(100);
    DECLARE @AlterSproc NVARCHAR(MAX);


    INSERT INTO @Sprocs (
                        Name
                       ,Defnition
                       ,AlterSproc
                        )
                SELECT    P.name
                         ,M.definition
                         ,REPLACE(
                                     REPLACE(REPLACE(M.definition, @OldFreedomName, @NewFreedomName), @OldAdvantageName, @NewAdvantageName)
                                    ,'CREATE PROCEDURE'
                                    ,'ALTER PROCEDURE'
                                 )
                FROM      sys.procedures P
                LEFT JOIN sys.sql_modules M ON M.object_id = P.object_id


    DECLARE SprocCursor CURSOR FOR
        SELECT Name
              ,AlterSproc
        FROM   @Sprocs;

    OPEN SprocCursor;
    FETCH NEXT FROM SprocCursor
    INTO @SprocName
        ,@AlterSproc;

    WHILE @@FETCH_STATUS = 0
        BEGIN


            EXEC ( @AlterSproc );
            PRINT @SprocName + ' modified';

            FETCH NEXT FROM SprocCursor
            INTO @SprocName
                ,@AlterSproc;
        END;



    CLOSE SprocCursor;
    DEALLOCATE SprocCursor;

GO
