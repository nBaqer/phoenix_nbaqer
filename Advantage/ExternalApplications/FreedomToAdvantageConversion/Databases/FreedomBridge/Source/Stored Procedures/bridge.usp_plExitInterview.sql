SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_plExitInterview]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT    NEWID() AS ExitInvterviewId
                 ,B_Enrollments.trg_StuEnrollId AS EnrollmentId
                 ,CAST(NULL AS UNIQUEIDENTIFIER) AS ScStatusId
                 ,NULL AS WantAssistance
                 ,NULL AS WaiverSigned
                 ,CASE WHEN ( F_Employee.IneligibleToWork ^ 1 ) = 1 THEN 'Yes'
                       ELSE 'No'
                  END AS Eligible
                 ,L.Description AS Reason
                 ,CAST(NULL AS UNIQUEIDENTIFIER) AS AreaId
                 ,NULL AS AvailableDate
                 ,NULL AS AvailableDays
                 ,NULL AS AvailableHours
                 ,NULL AS LowSalary
                 ,NULL AS HighSalary
                 ,CAST(NULL AS UNIQUEIDENTIFIER) AS TransportationId
                 ,'Conversion' AS ModUser
                 ,GETDATE() AS ModDate
                 ,CAST(NULL AS UNIQUEIDENTIFIER) AS ExpertiseLevelId
                 ,CAST(NULL AS UNIQUEIDENTIFIER) AS FullTimeId
                 ,ISNULL(F_Employee.ExitInterviewDate, AE.ExpGradDate) AS ExitInterviewDate
                 ,F_Employee.RecordKey AS EmployerRecordKey
                 ,IR.InelReasonId AS InelReasonId
        INTO      #ExitInterview
        FROM      Freedom..GRADPLAC_FIL F_Employee
        LEFT JOIN stage.FLookUp L ON L.LookupName = 'ReasonsUnavailableToWork'
                                     AND F_Employee.IneligibleReason = L.ID
        LEFT JOIN bridge.arStuEnrollments B_Enrollments ON B_Enrollments.src_RecordKey = F_Employee.StudrecKey
        LEFT JOIN FreedomAdvantage..arStuEnrollments AE ON AE.StuEnrollId = B_Enrollments.trg_StuEnrollId
        LEFT JOIN bridge.plEmployers B_Employee ON F_Employee.RecordKey = B_Employee.src_ChildRecordKey
        LEFT JOIN FreedomAdvantage..syIneligibilityReasons IR ON IR.InelReasonCode = CASE WHEN F_Employee.IneligibleReason = 1 THEN 'DEC'
                                                                                          WHEN F_Employee.IneligibleReason = 2 THEN 'PD'
                                                                                          WHEN F_Employee.IneligibleReason = 3 THEN 'DM'
                                                                                          WHEN F_Employee.IneligibleReason = 4 THEN 'SUV'
                                                                                          WHEN F_Employee.IneligibleReason = 5 THEN 'CE'
                                                                                     END;



        INSERT INTO FreedomAdvantage..plExitInterview (
                                                      ExtInterviewId
                                                     ,EnrollmentId
                                                     ,ScStatusId
                                                     ,WantAssistance
                                                     ,WaiverSigned
                                                     ,Eligible
                                                     ,Reason
                                                     ,AreaId
                                                     ,AvailableDate
                                                     ,AvailableDays
                                                     ,AvailableHours
                                                     ,LowSalary
                                                     ,HighSalary
                                                     ,TransportationId
                                                     ,ModUser
                                                     ,ModDate
                                                     ,ExpertiseLevelId
                                                     ,FullTimeId
                                                     ,ExitInterviewDate
													 ,inelreasonid
                                                      )
                    SELECT ExitInvterviewId
                          ,EnrollmentId
                          ,ScStatusId
                          ,WantAssistance
                          ,WaiverSigned
                          ,Eligible
                          ,Reason
                          ,AreaId
                          ,AvailableDate
                          ,AvailableDays
                          ,AvailableHours
                          ,LowSalary
                          ,HighSalary
                          ,TransportationId
                          ,ModUser
                          ,ModDate
                          ,ExpertiseLevelId
                          ,FullTimeId
                          ,ExitInterviewDate
						  ,InelReasonId
                    FROM   #ExitInterview;

        INSERT INTO bridge.plExitInterview
                    SELECT T.EnrollmentId
                          ,T.ExitInvterviewId
                          ,@CampusId
                          ,T.EmployerRecordKey
                    FROM   #ExitInterview T;


        SELECT StudrecKey
              ,up.ColumnName
              ,up.ColumnValue
        INTO   #PivotLicenseValues
        FROM   (
               SELECT StudrecKey
                     ,CAST(LicExamDate1 AS VARCHAR(10)) AS LicExamDate1
                     ,CAST(LicExamPassed1 AS VARCHAR(10)) AS LicExamPassed1
                     ,CAST(LicExamDate2 AS VARCHAR(10)) AS LicExamDate2
                     ,CASE WHEN LicExamDate2 IS NULL THEN NULL
                           ELSE CAST(LicExamPassed2 AS VARCHAR(10))
                      END AS LicExamPassed2
                     ,CAST(LicExamDate3 AS VARCHAR(10)) AS LicExamDate3
                     ,CASE WHEN LicExamDate3 IS NULL THEN NULL
                           ELSE CAST(LicExamPassed3 AS VARCHAR(10))
                      END AS LicExamPassed3
               FROM   Freedom..GRADPLAC_FIL
               ) P UNPIVOT(ColumnValue FOR ColumnName IN(LicExamDate1, LicExamPassed1, LicExamDate2, LicExamPassed2, LicExamDate3, LicExamPassed3)) AS up;

        DECLARE @firstCertificationExamSchedule VARCHAR(50) = 'First Certification Exam Scheduled';
        DECLARE @secondCertificationExamSchedule VARCHAR(50) = 'Second Certification Exam Scheduled';
        DECLARE @thirdCertificationExamSchedule VARCHAR(50) = 'Third Certification Exam Scheduled';
        DECLARE @firstCertificationExamResult VARCHAR(50) = 'First Certification Result';
        DECLARE @secondCertificationExamResult VARCHAR(50) = 'Second Certification Result';
        DECLARE @thirdCertificationExamResult VARCHAR(50) = 'Third Certification Result';


        DECLARE @firstCertificationExamScheduleId UNIQUEIDENTIFIER = NEWID();
        DECLARE @secondCertificationExamScheduleId UNIQUEIDENTIFIER = NEWID();
        DECLARE @thirdCertificationExamScheduleId UNIQUEIDENTIFIER = NEWID();
        DECLARE @firstCertificationExamResultId UNIQUEIDENTIFIER = NEWID();
        DECLARE @secondCertificationExamResultId UNIQUEIDENTIFIER = NEWID();
        DECLARE @thirdCertificationExamResultId UNIQUEIDENTIFIER = NEWID();

        DECLARE @pos INT;
        SET @pos = (
                   SELECT ISNULL(MAX(Position), 0)
                   FROM   FreedomAdvantage..syResourceSdf
                   WHERE  ResourceId = 97
                   );

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @firstCertificationExamSchedule
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @firstCertificationExamScheduleId, @firstCertificationExamSchedule, 3, 8, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @firstCertificationExamScheduleId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @secondCertificationExamSchedule
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @secondCertificationExamScheduleId, @secondCertificationExamSchedule, 3, 8, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @secondCertificationExamScheduleId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @thirdCertificationExamSchedule
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @thirdCertificationExamScheduleId, @thirdCertificationExamSchedule, 3, 8, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @thirdCertificationExamScheduleId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @firstCertificationExamResult
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @firstCertificationExamResultId, @firstCertificationExamResult, 1, 50, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @firstCertificationExamResultId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @secondCertificationExamResult
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @secondCertificationExamResultId, @secondCertificationExamResult, 1, 50, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @secondCertificationExamResultId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        IF NOT EXISTS (
                      SELECT 1
                      FROM   FreedomAdvantage..sySDF
                      WHERE  SDFDescrip = @thirdCertificationExamResult
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..sySDF
                VALUES ( @thirdCertificationExamResultId, @thirdCertificationExamResult, 1, 50, 0, '', NULL, 0, 'Conversion SA', GETDATE()
                        ,'{f23de1e2-d90a-4720-b4c7-0f6fb09c9965}', 0 );

                INSERT INTO FreedomAdvantage..syResourceSdf
                VALUES ( NEWID(), 97, @thirdCertificationExamResultId, 'Edit', 'Support', GETDATE(), 394, @pos );

                SET @pos = @pos + 1;
            END;

        INSERT INTO FreedomAdvantage..sySDFModuleValue (
                                                       SDFPKID
                                                      ,PgPKID
                                                      ,SDFID
                                                      ,SDFValue
                                                      ,ModUser
                                                      ,ModDate
                                                       )
                    SELECT    NEWID()
                             ,B.trg_PlExitInterviewId
                             ,SDF.SDFId
                             ,CASE WHEN LV.ColumnValue = 'Y' THEN 'Passed'
                                   WHEN LV.ColumnValue = 'N' THEN 'Not Passed'
                                   ELSE LV.ColumnValue
                              END
                             ,'Conversion SA'
                             ,GETDATE()
                    FROM      #PivotLicenseValues LV
                    LEFT JOIN Freedom..GRADPLAC_FIL F_Placement ON F_Placement.StudrecKey = LV.StudrecKey
                    LEFT JOIN FreedomBridge.bridge.plExitInterview B ON F_Placement.RecordKey = B.src_EmployeeId_RecourdKey
                    LEFT JOIN FreedomAdvantage..sySDF SDF ON SDF.SDFDescrip = CASE WHEN LV.ColumnName = 'LicExamDate1' THEN @firstCertificationExamSchedule
                                                                                   WHEN LV.ColumnName = 'LicExamPassed1' THEN @firstCertificationExamResult
                                                                                   WHEN LV.ColumnName = 'LicExamDate2' THEN @secondCertificationExamSchedule
                                                                                   WHEN LV.ColumnName = 'LicExamPassed2' THEN @secondCertificationExamResult
                                                                                   WHEN LV.ColumnName = 'LicExamDate3' THEN @thirdCertificationExamSchedule
                                                                                   WHEN LV.ColumnName = 'LicExamPassed3' THEN @thirdCertificationExamResult
                                                                              END;

        INSERT INTO FreedomAdvantage..plExitInterview (
                                                      ExtInterviewId
                                                     ,EnrollmentId
                                                     ,ScStatusId
                                                     ,WantAssistance
                                                     ,WaiverSigned
                                                     ,Eligible
                                                     ,Reason
                                                     ,AreaId
                                                     ,AvailableDate
                                                     ,AvailableDays
                                                     ,AvailableHours
                                                     ,LowSalary
                                                     ,HighSalary
                                                     ,TransportationId
                                                     ,ModUser
                                                     ,ModDate
                                                     ,ExpertiseLevelId
                                                     ,FullTimeId
                                                     --,InelReasonId
                                                     ,ExitInterviewDate
                                                      )
                    SELECT    NEWID()       -- ExtInterviewId - uniqueidentifier
                             ,E.StuEnrollId -- EnrollmentId - uniqueidentifier
                             ,NULL          -- ScStatusId - uniqueidentifier
                             ,NULL AS WantAssistance
                             ,NULL AS WaiverSigned
                             ,'Yes' AS Eligible
                             ,NULL AS Reason
                             ,CAST(NULL AS UNIQUEIDENTIFIER) AS AreaId
                             ,NULL AS AvailableDate
                             ,NULL AS AvailableDays
                             ,NULL AS AvailableHours
                             ,NULL AS LowSalary
                             ,NULL AS HighSalary
                             ,CAST(NULL AS UNIQUEIDENTIFIER) AS TransportationId
                             ,'Conversion' AS ModUser
                             ,GETDATE() AS ModDate
                             ,CAST(NULL AS UNIQUEIDENTIFIER) AS ExpertiseLevelId
                             ,CAST(NULL AS UNIQUEIDENTIFIER) AS FullTimeId
                                            --,NUll
                             ,E.ExpGradDate -- ExitInterviewDate - datetime2(7)
                    FROM      FreedomAdvantage..arStuEnrollments E
                    LEFT JOIN FreedomAdvantage..syStatusCodes SC ON SC.StatusCodeId = E.StatusCodeId
                    LEFT JOIN FreedomAdvantage..sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                    WHERE     SS.SysStatusId = 14
                              AND NOT EXISTS (
                                             SELECT 1
                                             FROM   FreedomAdvantage..plExitInterview P
                                             WHERE  P.EnrollmentId = E.StuEnrollId
                                             );

        DROP TABLE #PivotLicenseValues;
        DROP TABLE #Settings;
        DROP TABLE #ExitInterview;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_plExitInterview]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
