SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-11-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arTerm]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT     ISNULL(bTerm.trg_TermId, NEWID()) AS TermId     -- uniqueidentifier
                  ,sTerm.RecordKey AS TermCode                     -- varchar(12)
                  ,dStat.StatusId AS StatusId                      -- uniqueidentifier
                  ,LTRIM(RTRIM(sTerm.Name)) AS TermDescrip         -- varchar(50)
                  ,dCmpGrp.CampGrpId AS CampGrpId                  -- uniqueidentifier
                  ,ISNULL(sTerm.BegDate, '1-1-1980') AS StartDate  -- datetime
                  ,ISNULL(sTerm.EndDate, '12-31-2050') AS EndDate  -- datetime
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftId       -- uniqueidentifier
                  ,'sa' AS ModUser                                 -- varchar(50)
                  ,GETDATE() AS ModDate                            -- datetime
                  ,0 AS IsModule                                   -- bit
                  ,bAttUnitType.src_AttendanceTypeID AS TermTypeId -- int
                  --,bProgVer.trg_PrgVerId AS PrgVerId                       -- uniqueidentifier
                  ,bPrg.trg_ProgId AS ProgId
				  ,NULL AS MidPtDate                               -- datetime
                  ,NULL AS MaxGradDate                             -- datetime
				  ,sCourse.RecordKey AS FreedomCourseRecordKey     -- INT
				  --,bPrg.trg_ProgId AS ProgId
				   ,bProgVer.trg_PrgVerId AS PrgVerId 
        INTO       #Term
        FROM       Freedom..TERM_DEF sTerm
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCmpGrp ON dCmpGrp.CampGrpCode = 'ALL'
        INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sTerm.CourseDefKey
        INNER JOIN FreedomBridge.bridge.arAttUnitType bAttUnitType ON bAttUnitType.src_AttendanceTypeID = sCourse.PrivAttendType
		INNER JOIN FreedomBridge.bridge.arPrgVersions bProgVer ON bProgVer.src_RecordKey = sCourse.RecordKey 
                                                                         AND bProgVer.trg_CampusId = @CampusId
        INNER JOIN FreedomBridge.bridge.arPrograms bPrg ON bPrg.src_RecordKey = sCourse.ParentKey
                                                           --AND bPrg.trg_CampusId = @CampusId

        LEFT JOIN  bridge.arTerm bTerm ON bTerm.src_Name = CASE WHEN LEN(LTRIM(RTRIM(sTerm.Name)) + ' - ' + LTRIM(RTRIM(sCourse.Name))) <= 50 THEN
                                                                    LTRIM(RTRIM(sTerm.Name)) + ' - ' + LTRIM(RTRIM(sCourse.Name))
                                                                ELSE LTRIM(RTRIM(sTerm.Name))
                                                           END
                                          AND bTerm.trg_CampusId = @CampusId
                                          AND sTerm.RecordKey = bTerm.src_RecordKey;

        ALTER TABLE FreedomAdvantage..arTerm DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arTerm
                    SELECT tTerm.TermId
                          ,tTerm.TermCode
                          ,tTerm.StatusId
                          ,tTerm.TermDescrip
                          ,tTerm.CampGrpId
                          ,tTerm.StartDate
                          ,tTerm.EndDate
                          ,tTerm.ShiftId
                          ,tTerm.ModUser
                          ,tTerm.ModDate
                          ,tTerm.IsModule
                          ,tTerm.TermTypeId
                          ,tTerm.ProgId
                          ,tTerm.MidPtDate
                          ,tTerm.MaxGradDate
						  ,tTerm.PrgVerId

                    FROM   #Term tTerm;


        ALTER TABLE FreedomAdvantage..arTerm ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
		

        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arTerm')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arTerm
                    (
                        trg_TermId UNIQUEIDENTIFIER
                       ,src_Name VARCHAR(255)
                       ,src_RecordKey INT
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,trg_ProgramVersionId UNIQUEIDENTIFIER
					   ,FreedomCourseRecordKey INT
                            PRIMARY KEY CLUSTERED (
                                                  src_Name
                                                 ,src_RecordKey
                                                 ,trg_CampusId
                                                 ,trg_ProgramVersionId
												 ,FreedomCourseRecordKey
                                                  )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arTerm
                    SELECT tTerm.TermId
                          ,tTerm.TermDescrip
                          ,tTerm.TermCode
                          ,@CampusId AS CampusId
                          ,tTerm.PrgVerId
						 ,tTerm.FreedomCourseRecordKey
                    FROM   #Term tTerm;


        DROP TABLE #Term;
        DROP TABLE #Settings;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arTerm]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;

GO
