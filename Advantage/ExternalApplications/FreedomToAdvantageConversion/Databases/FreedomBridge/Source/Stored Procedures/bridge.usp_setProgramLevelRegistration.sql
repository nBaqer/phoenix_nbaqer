SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_setProgramLevelRegistration]
AS
    BEGIN

        BEGIN TRANSACTION;

        UPDATE FreedomAdvantage..arPrgVersions
        SET    ProgramRegistrationType = 1
        WHERE  ProgramRegistrationType = 0 OR ProgramRegistrationType IS NULL;

        UPDATE     terms
        SET        terms.ProgramVersionId = bridgeTerm.trg_ProgramVersionId
        FROM       FreedomAdvantage..arTerm terms
        INNER JOIN FreedomBridge.bridge.arTerm bridgeTerm ON bridgeTerm.trg_TermId = terms.TermId
		INNER JOIN FreedomAdvantage..arPrgVersions programVersions ON programVersions.PrgVerId = bridgeTerm.trg_ProgramVersionId

        UPDATE     classSections
        SET        classSections.ProgramVersionDefinitionId = bridgeProgramVersionDef.trg_ProgVerDefId
        FROM       FreedomAdvantage..arClassSections AS classSections
        INNER JOIN FreedomBridge.bridge.arClassSections bridgeClassSection ON classSections.ClsSectionId = bridgeClassSection.trg_ClsSectionId
        INNER JOIN FreedomBridge.bridge.arProgVerDef bridgeProgramVersionDef ON bridgeClassSection.src_RecordKey = bridgeProgramVersionDef.FreedomSubjectDefRecordKey;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_setProgramLevelRegistration]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;

    END;
GO
