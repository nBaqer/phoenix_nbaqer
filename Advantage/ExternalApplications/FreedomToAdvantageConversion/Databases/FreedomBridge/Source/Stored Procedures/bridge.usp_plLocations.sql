SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_plLocations]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS LocationId                   -- uniqueidentifier
             , sSchool.PhysAddrState AS LocationCode   -- varchar(12)
             , dStatus.StatusId AS StatusId            -- uniqueidentifier
             , sSchool.PhysAddrCity AS LocationDescrip -- varchar(50)
             , dCampGrp.CampGrpId AS CampGrpId         -- uniqueidentifier
             , 'SA' AS ModUser                         -- varchar(50)
             , GETDATE() AS ModDate                    -- datetime
             , sSchool.SchoolID
        INTO   #plLocations
        FROM   Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN bridge.plLocations bIgnore ON bIgnore.src_SchoolID = sSchool.SchoolID
                                                AND bIgnore.trg_CampusId = @CampusId
        WHERE  bIgnore.src_SchoolID IS NULL;

        INSERT INTO FreedomBridge.bridge.plLocations
                    SELECT LocationId AS trg_LocationId
                         , @CampusId AS trg_CampusId
                         , SchoolID AS src_SchoolID
                    FROM   #plLocations;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_plLocations]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
