SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adDegCertSeeking]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dDegCertSeek.Descrip IS NULL THEN NEWID()
                    ELSE dDegCertSeek.DegCertSeekingId
               END AS DegCertSeekingId                                             -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS Code -- varchar(50)
             , FLook.Description AS Descrip                                        -- varchar(50)
             , dStatus.StatusId AS StatusId                                        -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId                                     -- uniqueidentifier
             , GETDATE() AS ModDate                                                -- datetime
             , 'sa' AS ModUser                                                     -- varchar(50)
             , ISNULL(dDegCertSeek.IPEDSSequence, 0) AS IPEDSSequence              -- int
             , ISNULL(dDegCertSeek.IPEDSValue, 0) AS IPEDSValue                    -- int
             , FLook.ID
        INTO   #DegCertSeeking
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adDegCertSeeking dDegCertSeek ON dDegCertSeek.CampGrpId = dCampGrp.CampGrpId
		                                                             AND dDegCertSeek.Descrip = FLook.Description
        WHERE  FLook.LookupName = 'DegreeCertSeeking';


        ALTER TABLE FreedomAdvantage..adDegCertSeeking DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adDegCertSeeking
                    SELECT tDegCertSeek.DegCertSeekingId
                         , tDegCertSeek.Code
                         , tDegCertSeek.Descrip
                         , tDegCertSeek.StatusId
                         , tDegCertSeek.CampGrpId
                         , tDegCertSeek.ModDate
                         , tDegCertSeek.ModUser
                         , tDegCertSeek.IPEDSSequence
                         , tDegCertSeek.IPEDSValue
                    FROM   #DegCertSeeking tDegCertSeek
                  WHERE NOT EXISTS( SELECT 1 FROM FreedomAdvantage..adDegCertSeeking E WHERE e.Code = tDegCertSeek.Code AND e.CampGrpId = tDegCertSeek.CampGrpId)
        
        ALTER TABLE FreedomAdvantage..adDegCertSeeking ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adDegCertSeeking')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adDegCertSeeking
                    (
                        trg_DegCertSeekingId UNIQUEIDENTIFIER
                      , trg_IPEDSValue INT
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;


        INSERT INTO FreedomBridge.bridge.adDegCertSeeking
                    SELECT tAdminCriteria.DegCertSeekingId
                         , tAdminCriteria.IPEDSValue
                         , tAdminCriteria.Descrip
                         , tAdminCriteria.ID
                    FROM   #DegCertSeeking tAdminCriteria
                    LEFT JOIN FreedomBridge.bridge.adDegCertSeeking bDegCertSeek ON bDegCertSeek.trg_DegCertSeekingId = tAdminCriteria.DegCertSeekingId
                    WHERE  bDegCertSeek.trg_DegCertSeekingId IS NULL;

        DROP TABLE #DegCertSeeking;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adDegCertSeeking]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun] 
        COMMIT TRANSACTION;
		
    END;
GO
