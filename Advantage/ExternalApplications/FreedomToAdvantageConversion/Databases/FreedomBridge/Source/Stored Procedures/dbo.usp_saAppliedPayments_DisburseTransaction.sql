SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_saAppliedPayments_DisburseTransaction]
    @PayTransactionID AS UNIQUEIDENTIFIER
AS
    BEGIN
        --The procedure takes payment transaction and applies it to charge transaction
        --that belongs to the same student enrollment
        --The procedure is performed by arranging transactions by Transaction type priority
        --Which is set in saTransCodesPriority table 
        SET XACT_ABORT ON;
        BEGIN TRANSACTION Apply_Payment_Tran;

        DECLARE @Balance MONEY
              , @EnrollmentID UNIQUEIDENTIFIER;

        SELECT   @Balance = dTran.TransAmount + ISNULL(SUM(dPay.Amount), 0)
               , @EnrollmentID = dTran.StuEnrollId
        FROM     FreedomAdvantage..saTransactions dTran
        LEFT JOIN FreedomAdvantage..saAppliedPayments dPay ON dPay.TransactionId = dTran.TransactionId
        WHERE    dTran.TransactionId = @PayTransactionID
        GROUP BY dTran.TransAmount
               , dTran.StuEnrollId;

        SELECT   CAST(dTran.TransAmount AS MONEY) - ISNULL(Payed.Amount, 0) AS TransAmount
               , dTranCode.TransCodeDescrip
               , dTran.TransDate
               , ROW_NUMBER() OVER ( ORDER BY bPriority.Priority DESC
                                            , dTran.TransDate
                                   ) AS RowNum
               , dTran.TransactionId
        INTO     #Charge
        FROM     FreedomAdvantage..saTransactions dTran
        INNER JOIN FreedomAdvantage..saTransCodes dTranCode ON dTranCode.TransCodeId = dTran.TransCodeId
        INNER JOIN FreedomBridge.bridge.arStuEnrollments bEnr ON bEnr.trg_StuEnrollId = dTran.StuEnrollId
        INNER JOIN FreedomBridge.bridge.syCampuses bCampus ON bCampus.trg_syCampusId = bEnr.trg_CampusId
        INNER JOIN FreedomBridge.stage.saTransCodesPriority bPriority ON bPriority.src_FullDescrip = dTranCode.TransCodeDescrip
        OUTER APPLY  (
                         SELECT SUM(dPay1.Amount) AS Amount
                         FROM   FreedomAdvantage..saAppliedPayments dPay1
                         WHERE  dPay1.ApplyToTransId = dTran.TransactionId
                     ) Payed
        WHERE    bEnr.trg_StuEnrollId = @EnrollmentID
                 AND dTran.Voided = 0
                 AND dTran.TransAmount > 0
        ORDER BY dTran.TransDate;

        WITH CTE_APPLY
        AS ( SELECT TransAmount
                  , CASE WHEN TransAmount + @Balance > 0 THEN 0
                         ELSE TransAmount + @Balance
                    END AS MoneyLeft
                  , CASE WHEN TransAmount + @Balance <= 0 THEN TransAmount * ( -1 )
                         ELSE @Balance
                    END AS Applied
                  , TransCodeDescrip
                  , TransDate
                  , RowNum
                  , TransactionId AS ChargeTransactionID
             FROM   #Charge
             WHERE  RowNum = 1
             UNION ALL
             SELECT Charge.TransAmount
                  , CASE WHEN Charge.TransAmount + CTE.MoneyLeft > 0 THEN 0
                         ELSE Charge.TransAmount + CTE.MoneyLeft
                    END AS MoneyLeft
                  , CASE WHEN CTE.MoneyLeft + Charge.TransAmount <= 0 THEN Charge.TransAmount * ( -1 )
                         ELSE CTE.MoneyLeft
                    END AS Applied
                  , Charge.TransCodeDescrip
                  , Charge.TransDate
                  , Charge.RowNum
                  , Charge.TransactionId
             FROM   #Charge Charge
             INNER JOIN CTE_APPLY CTE ON CTE.RowNum + 1 = Charge.RowNum
           )
        SELECT CTE.TransAmount
             , CTE.MoneyLeft
             , CASE WHEN CTE1.MoneyLeft = 0 THEN 0
                    ELSE CTE.Applied
               END AS Applied
             , CTE.TransCodeDescrip
             , CTE.TransDate
             , CTE.RowNum
             , CTE.ChargeTransactionID
        INTO   #Applied
        FROM   CTE_APPLY CTE
        LEFT JOIN CTE_APPLY CTE1 ON CTE1.RowNum = CTE.RowNum - 1;

        INSERT INTO FreedomAdvantage..saAppliedPayments (
                                                            AppliedPmtId
                                                          , TransactionId
                                                          , ApplyToTransId
                                                          , Amount
                                                          , ModDate
                                                          , ModUser
                                                        )
                    SELECT NEWID() AppliedPmtId                         -- uniqueidentifier
                         , @PayTransactionID AS TransactionId           -- uniqueidentifier
                         , tApply.ChargeTransactionID AS ApplyToTransId -- uniqueidentifier
                         , tApply.Applied * ( -1 ) AS Amount            -- decimal(18,2)
                         , GETDATE() AS ModDate                         -- datetime
                         , 'SA' AS ModUser                              -- varchar(50)
                    FROM   #Applied tApply
                    WHERE  tApply.Applied <> 0;

        INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saAppliedPayments_DisburseTransaction]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION Apply_Payment_Tran;
    END;
GO
