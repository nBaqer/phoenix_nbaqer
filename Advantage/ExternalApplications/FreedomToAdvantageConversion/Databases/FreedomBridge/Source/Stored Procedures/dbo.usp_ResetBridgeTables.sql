SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_ResetBridgeTables]
AS
BEGIN

    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION T1;

    DECLARE @TableName VARCHAR(128),
            @ScriptPattern AS VARCHAR(MAX) = 'TRUNCATE TABLE [bridge].[TruncateMe];',
            @Script AS VARCHAR(MAX);


    DECLARE @SchoolId INT;

    SET @SchoolId =
    (
        SELECT TOP 1 SchoolID FROM Freedom..SCHOOL_DEF
    );
    DECLARE @EmailDomain VARCHAR(50) =
	(
		SELECT EmailDomain FROM dbo.SourceDB WHERE SchoolID = @SchoolId
	);

    DECLARE dbCursor CURSOR FOR
    SELECT TABLE_NAME
    FROM INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = 'bridge'
          AND TABLE_NAME NOT IN ( 'AttendanceExceptionList' );

    OPEN dbCursor;
    FETCH NEXT FROM dbCursor
    INTO @TableName;

    WHILE (@@fetch_status <> -1)
    BEGIN
        SELECT @Script = REPLACE(@ScriptPattern, 'TruncateMe', @TableName);
        EXEC (@Script);
        --PRINT @Script
        FETCH NEXT FROM dbCursor
        INTO @TableName;
    END;

    CLOSE dbCursor;
    DEALLOCATE dbCursor;

	IF (@EmailDomain IS NOT NULL)
	BEGIN
		TRUNCATE TABLE stage.syUsers;
	END

    COMMIT TRANSACTION T1;

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_ResetBridgeTables]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];
END;

GO
