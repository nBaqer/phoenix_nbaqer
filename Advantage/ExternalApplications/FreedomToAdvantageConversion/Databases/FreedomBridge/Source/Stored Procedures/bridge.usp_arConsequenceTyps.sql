SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-21-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arConsequenceTyps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dConTyp.ConseqTypDesc IS NULL THEN FLook.ID
                    ELSE dConTyp.ConsequenceTypId
               END AS ConsequenceTypId            -- tinyint
             , FLook.Description AS ConseqTypDesc -- varchar(50)
             , FLook.ID
        INTO   #ConTyp
        FROM   FreedomBridge.stage.FLookUp FLook
        LEFT JOIN FreedomAdvantage..arConsequenceTyps dConTyp ON dConTyp.ConsequenceTypId = FLook.ID
        WHERE  FLook.LookupName = 'SAPConsequence';

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arConsequenceTyps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arConsequenceTyps
                    (
                        trg_ConsequenceTypId TINYINT
                      , src_SAPConsequenceDescription VARCHAR(255)
                      , src_SAPConsequenceID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_SAPConsequenceDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arConsequenceTyps
                    SELECT tConTyp.ConsequenceTypId
                         , tConTyp.ConseqTypDesc
                         , tConTyp.ID
                    FROM   #ConTyp tConTyp
                    LEFT JOIN FreedomBridge.bridge.arConsequenceTyps bConTyp ON bConTyp.src_SAPConsequenceDescription = tConTyp.ConseqTypDesc
                                                                                AND bConTyp.src_SAPConsequenceID = tConTyp.ID
                    WHERE  bConTyp.trg_ConsequenceTypId IS NULL;

        DROP TABLE #ConTyp;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arConsequenceTyps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
