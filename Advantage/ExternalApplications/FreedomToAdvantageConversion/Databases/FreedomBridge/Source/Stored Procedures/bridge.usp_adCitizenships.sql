SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-08-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adCitizenships]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@CampusGroupToUseId UNIQUEIDENTIFIER;

        DECLARE @ConversionSA VARCHAR(50) = 'Conversion SA';
        DECLARE @ExecutionTime DATETIME2 = GETDATE();

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SET @CampusGroupToUseId = CASE WHEN EXISTS (
                                                   SELECT 1
                                                   FROM   bridge.CampusGroupAllRules
                                                   WHERE  TableName = 'adCitizenships'
                                                          AND IsCampusAll = 0
                                                   ) THEN @CampGrpId
												    WHEN  @CampusGroupAllId IS NULL THEN '2AC97FC1-BB9B-450A-AA84-7C503C90A1EB' 
                                       ELSE @CampusGroupAllId
                                  END;


        SELECT     CASE WHEN dCit.CitizenshipDescrip IS NULL THEN NEWID()
                        ELSE dCit.CitizenshipId
                   END AS CitizenshipId                                                           -- uniqueidentifier
                  ,UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS CitizenshipCode -- varchar(12)
                  ,dStatus.StatusId AS StatusId                                                   -- uniqueidentifier
                  ,FLook.Description AS CitizenshipDescrip                                        -- varchar(50)
                  ,@CampusGroupToUseId AS CampGrpId                                                -- uniqueidentifier
                  ,'sa' AS ModUser                                                                -- varchar(50)
                  ,GETDATE() AS ModDate                                                           -- datetime
                  ,0 AS IPEDSSequence                                                             -- int
                  ,0 AS IPEDSValue                                                                -- int
                  ,FLook.ID
        INTO       #Citizenship
        FROM       FreedomBridge.stage.FLookUp FLook
        INNER JOIN [FreedomAdvantage]..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN [FreedomAdvantage]..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN  [FreedomAdvantage]..adCitizenships dCit ON dCit.CampGrpId = @CampusGroupToUseId
                                                            AND REPLACE(dCit.CitizenshipDescrip, '-', ' ') = FLook.Description
        WHERE      FLook.LookupName = 'Citizenship';

        ALTER TABLE [FreedomAdvantage]..adCitizenships DISABLE TRIGGER ALL;

        INSERT INTO [FreedomAdvantage]..adCitizenships
                    SELECT    tCit.CitizenshipId
                             ,tCit.CitizenshipCode
                             ,tCit.StatusId
                             ,tCit.CitizenshipDescrip
                             ,@CampGrpId
                             ,tCit.ModUser
                             ,tCit.ModDate
                             ,tCit.IPEDSSequence
                             ,tCit.IPEDSValue
                             ,NULL
                    FROM      #Citizenship tCit
                    LEFT JOIN [FreedomAdvantage]..adCitizenships dCit ON dCit.CampGrpId = tCit.CampGrpId
                                                                       AND REPLACE(dCit.CitizenshipDescrip, '-', ' ') = tCit.CitizenshipDescrip
                    WHERE     dCit.CitizenshipDescrip IS NULL ;

        ALTER TABLE [FreedomAdvantage]..adCitizenships ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adCitizenships')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE bridge.adCitizenships
                    (
                        trg_CitizenshipId UNIQUEIDENTIFIER
                       ,src_Description VARCHAR(255)
                       ,src_ID INT
                       ,
                       PRIMARY KEY ( src_Description )
                    );
            END;

        INSERT INTO bridge.adCitizenships
                    SELECT    tCit.CitizenshipId
                             ,tCit.CitizenshipDescrip
                             ,tCit.ID
							 ,tcit.CampGrpId
                    FROM      #Citizenship tCit
                    LEFT JOIN FreedomBridge.bridge.adCitizenships bCit ON bCit.trg_CitizenshipId = tCit.CitizenshipId
                    WHERE     bCit.trg_CitizenshipId IS NULL AND NOT EXISTS ( SELECT 1 FROM bridge.adCitizenships C WHERE C.src_Description = tCit.CitizenshipDescrip AND C.trg_CampusGroupId = tcit.CampGrpId);

        DROP TABLE #Citizenship;
		DROP TABLE #Settings
        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adCitizenships]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];
        COMMIT TRANSACTION;

    END;


GO
