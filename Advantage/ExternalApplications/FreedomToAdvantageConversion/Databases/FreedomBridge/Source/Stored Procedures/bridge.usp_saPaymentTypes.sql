SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saPaymentTypes]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT dPayType.PaymentTypeId --int
             , FL.ID
             , FL.Description
             , FL.LookUpID
        INTO   #saPaymentTypes
        FROM   stage.FLookUp FL
        LEFT JOIN FreedomAdvantage..saPaymentTypes dPayType ON dPayType.Description = CASE WHEN FL.Description = 'Electronic Transfer' THEN 'EFT'
                                                                                           ELSE FL.Description
                                                                                      END
        LEFT JOIN bridge.saPaymentTypes bIgnore ON bIgnore.src_Description = FL.Description
        WHERE  LookupName = 'PaymentType'
               AND bIgnore.src_ID IS NULL;

        INSERT INTO FreedomBridge.bridge.saPaymentTypes
                    SELECT PaymentTypeId
                         , ID
                         , Description
                         , LookUpID
                    FROM   #saPaymentTypes;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saPaymentTypes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
