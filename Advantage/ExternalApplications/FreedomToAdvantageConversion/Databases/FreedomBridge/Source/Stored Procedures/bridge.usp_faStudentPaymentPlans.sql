SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_faStudentPaymentPlans]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        --Additional info
        --FROM FreedomBridge.stage.FLookUp
        --WHERE LookupName = 'StuPmtPlanSchedType'
        --ID	Description
        --1	Day(s)
        --2	Week(s)
        --3	Month(s)
        --4	Quarter(s)
        --5	Year(s)

        SELECT NEWID() AS PaymentPlanId                     -- uniqueidentifier
             , bEnr.trg_StuEnrollId AS StuEnrollId          -- uniqueidentifier
             , sPayPlan.FirstPmtDueDate AS PayPlanStartDate -- datetime
             , CASE WHEN sPayPlan.PrivSchedType = 1 THEN DATEADD(DAY, sPayPlan.NumPmts, sPayPlan.FirstPmtDueDate)
                    WHEN sPayPlan.PrivSchedType = 2 THEN DATEADD(WEEK, sPayPlan.NumPmts, sPayPlan.FirstPmtDueDate)
                    WHEN sPayPlan.PrivSchedType = 3 THEN DATEADD(MONTH, sPayPlan.NumPmts, sPayPlan.FirstPmtDueDate)
                    WHEN sPayPlan.PrivSchedType = 4 THEN DATEADD(QUARTER, sPayPlan.NumPmts, sPayPlan.FirstPmtDueDate)
                    WHEN sPayPlan.PrivSchedType = 5 THEN DATEADD(YEAR, sPayPlan.NumPmts, sPayPlan.FirstPmtDueDate)
               END AS PayPlanEndDate                        -- datetime
             , dYear.AcademicYearId AS AcademicYearId       -- uniqueidentifier
             , sPayPlan.PlanAmt AS TotalAmountDue           -- decimal(19,4)
             , CASE WHEN sPayPlan.PrivSchedType = 1 THEN
                        'Amount: ' + CAST(sPayPlan.PlanAmt AS VARCHAR(10)) + ', Period: Day, Dirbursements: ' + CAST(sPayPlan.NumPmts AS VARCHAR(10))
                    WHEN sPayPlan.PrivSchedType = 2 THEN
                        'Amount: ' + CAST(sPayPlan.PlanAmt AS VARCHAR(10)) + ', Period: Week, Dirbursements: ' + CAST(sPayPlan.NumPmts AS VARCHAR(10))
                    WHEN sPayPlan.PrivSchedType = 3 THEN
                        'Amount: ' + CAST(sPayPlan.PlanAmt AS VARCHAR(10)) + ', Period: Month, Dirbursements: ' + CAST(sPayPlan.NumPmts AS VARCHAR(10))
                    WHEN sPayPlan.PrivSchedType = 4 THEN
                        'Amount: ' + CAST(sPayPlan.PlanAmt AS VARCHAR(10)) + ', Period: Quarter, Dirbursements: ' + CAST(sPayPlan.NumPmts AS VARCHAR(10))
                    WHEN sPayPlan.PrivSchedType = 5 THEN
                        'Amount: ' + CAST(sPayPlan.PlanAmt AS VARCHAR(10)) + ', Period: Year, Dirbursements: ' + CAST(sPayPlan.NumPmts AS VARCHAR(10))
               END AS PayPlanDescrip                        -- varchar(50)
             , 'SA' AS ModUser                              -- varchar(50)
             , GETDATE() AS ModDate                         -- datetime
             , sPayPlan.NumPmts AS Disbursements            -- int
             , NULL AS AwardId                              -- int
             , sPayPlan.RecordKey
        INTO   #faStudentPaymentPlans
        FROM   Freedom..STUPMT_PLN sPayPlan --INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = sPayPlan.RecordKey
        INNER JOIN bridge.arStuEnrollments bEnr ON bEnr.src_RecordKey = sPayPlan.RecordKey
                                                   AND bEnr.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..saAcademicYears dYear ON CAST(dYear.AcademicYearCode AS INT) - 1 = YEAR(sPayPlan.FirstPmtDueDate)
        WHERE  sPayPlan.PlanAmt > 0
		AND dYear.AcademicYearDescrip NOT IN ('2015/20151','2016/20161','2017/20171');;

        ALTER TABLE FreedomAdvantage..faStudentPaymentPlans DISABLE TRIGGER ALL;




        INSERT INTO FreedomAdvantage..faStudentPaymentPlans
                    SELECT PaymentPlanId
                         , StuEnrollId
                         , PayPlanStartDate
                         , PayPlanEndDate
                         , AcademicYearId
                         , TotalAmountDue
                         , PayPlanDescrip
                         , ModUser
                         , ModDate
                         , Disbursements
                         , AwardId
                    FROM   #faStudentPaymentPlans;

        ALTER TABLE FreedomAdvantage..faStudentPaymentPlans ENABLE TRIGGER ALL;

        INSERT INTO bridge.faStudentPaymentPlans
                    SELECT PaymentPlanId AS trg_TransactionId
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                    FROM   #faStudentPaymentPlans;

        INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_faStudentPaymentPlans]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
