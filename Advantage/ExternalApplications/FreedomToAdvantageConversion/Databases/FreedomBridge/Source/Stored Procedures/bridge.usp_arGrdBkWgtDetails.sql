SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-07-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGrdBkWgtDetails]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';


        SELECT     CASE WHEN dGrdBkWgtDtl.Descrip IS NULL THEN NEWID()
                        ELSE dGrdBkWgtDtl.InstrGrdBkWgtDetailId
                   END AS InstrGrdBkWgtDetailId                             -- uniqueidentifier
                  ,bGrdBkWgts.trg_InstrGrdBkWgtId AS InstrGrdBkWgtId        -- uniqueidentifier
                  ,bGrdBkWgts.src_Name
                  ,bGrdCmpType.src_RecordKey AS Code                        -- varchar(12)
                  ,bGrdCmpType.src_Name AS Descrip                          -- varchar(50)
                  ,CASE WHEN sWork.Weight = 0 THEN 100.00 / CAST(GrdCmp.WgtCount AS DECIMAL)
                        ELSE ( CAST(sWork.Weight AS DECIMAL) / CAST(GrdCmp.Total AS DECIMAL)) * 100.00
                   END AS Weight                                            -- decimal
                  ,sWork.Seq AS Seq                                         -- int
                  ,'sa' AS ModUser                                          -- varchar(50)
                  ,GETDATE() AS ModDate                                     -- datetime
                  ,bGrdCmpType.trg_GrdComponentTypeId AS GrdComponentTypeId -- uniqueidentifier
                  ,0 AS Parameter                                           -- int
                  ,CASE WHEN sWork.Type = 4 THEN sWork.MinResult
                        ELSE CASE WHEN sWork.MinLabCnt = 0 THEN 1
                                  ELSE sWork.MinLabCnt
                             END
                   END AS Number                                            -- decimal
                  ,NULL AS GrdPolicyId                                      -- int
                  ,0 AS Required                                            -- bit
                  ,0 AS MustPass                                            -- bit
                  ,NULL AS CreditsPerService                                -- decimal
                  ,bGrdCmpType.src_SubjectDefKey
                  ,GrdCmp.Total
                  ,GrdCmp.WgtCount
                  ,sWork.CourseDefKey
                  ,sWork.SubjectDefKey
                  ,sWork.Seq AS oldSeq
                  ,sWork.Name
                  ,sWork.BegDate
                  ,sWork.EndDate
                  ,0 AS InactiveDateRange
        INTO       #GrdBkWgtDtl

        --Group the weights from Freedom to calculate weighted averages into percentages used by Advantage
        FROM       (
                   SELECT     SUM(sWork.Weight) AS Total
                             ,COUNT(sWork.Weight) AS WgtCount
                             ,bGrdCmpType.src_SubjectDefKey
                   FROM       FreedomBridge.bridge.arGrdComponentTypes bGrdCmpType
                   INNER JOIN Freedom..WORKUNIT_DEF sWork ON sWork.RecordKey = bGrdCmpType.src_RecordKey
                                                             AND sWork.Name = bGrdCmpType.src_Name
                                                             AND bGrdCmpType.trg_CampusId = @CampusId
                   GROUP BY   bGrdCmpType.src_SubjectDefKey
                   ) GrdCmp
        INNER JOIN FreedomBridge.bridge.arGrdComponentTypes bGrdCmpType ON bGrdCmpType.src_SubjectDefKey = GrdCmp.src_SubjectDefKey
                                                                           AND bGrdCmpType.trg_CampusId = @CampusId
        INNER JOIN Freedom..WORKUNIT_DEF sWork ON sWork.RecordKey = bGrdCmpType.src_RecordKey
        INNER JOIN FreedomBridge.bridge.arGrdBkWeights bGrdBkWgts ON bGrdBkWgts.src_RecordKey = bGrdCmpType.src_SubjectDefKey
                                                                     AND bGrdBkWgts.src_Name NOT LIKE '%Attendance%'
                                                                     AND bGrdBkWgts.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        LEFT JOIN  FreedomAdvantage..arGrdBkWgtDetails dGrdBkWgtDtl ON dGrdBkWgtDtl.GrdComponentTypeId = bGrdCmpType.trg_GrdComponentTypeId
                                                                       AND dGrdBkWgtDtl.Descrip = bGrdCmpType.src_Name
                                                                       AND dGrdBkWgtDtl.Code = bGrdCmpType.src_RecordKey
                                                                       AND dGrdBkWgtDtl.InstrGrdBkWgtId = bGrdBkWgts.trg_InstrGrdBkWgtId;



        UPDATE #GrdBkWgtDtl
        SET    InactiveDateRange = 1
        WHERE  InstrGrdBkWgtDetailId IN (
                                        SELECT   c.InstrGrdBkWgtDetailId
                                        FROM     (
                                                 SELECT   cd.Name
                                                         ,cd.CourseDefKey
                                                         ,cd.SubjectDefKey
                                                         ,cd.oldSeq
                                                         ,COUNT(*) AS count
                                                 FROM     #GrdBkWgtDtl cd
                                                 GROUP BY cd.Name
                                                         ,cd.CourseDefKey
                                                         ,cd.SubjectDefKey
                                                         ,cd.oldSeq
                                                 HAVING   COUNT(*) > 1
                                                 ) x
                                        JOIN     #GrdBkWgtDtl c ON c.Name = x.Name
                                                                   AND c.CourseDefKey = x.CourseDefKey
                                                                   AND c.SubjectDefKey = x.SubjectDefKey
                                                                   AND c.oldSeq = x.oldSeq
                                        WHERE    EndDate IS NOT NULL
                                        GROUP BY c.CourseDefKey
                                                ,c.EndDate
                                                ,c.InstrGrdBkWgtDetailId
                                        );


        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGrdBkWgtDetails')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGrdBkWgtDetails
                    (
                        trg_InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
                       ,trg_InstrGrdBkWgtId UNIQUEIDENTIFIER
                       ,src_Name VARCHAR(255)
                       ,src_RecordKey INT
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,InactiveDateRange BIT NOT NULL
                            DEFAULT 0
                       ,
                       PRIMARY KEY CLUSTERED (
                                             trg_InstrGrdBkWgtId
                                            ,src_Name
                                            ,src_RecordKey
                                            ,trg_CampusId
                                             )
                    );
            END;
        SELECT *
        FROM   FreedomBridge.bridge.arGrdBkWgtDetails;
        INSERT INTO FreedomBridge.bridge.arGrdBkWgtDetails
                    SELECT    tGrdBkWgtDtl.InstrGrdBkWgtDetailId
                             ,tGrdBkWgtDtl.InstrGrdBkWgtId
                             ,tGrdBkWgtDtl.Descrip
                             ,tGrdBkWgtDtl.Code
                             ,@CampusId AS CampusId
                             ,tGrdBkWgtDtl.InactiveDateRange
                    FROM      #GrdBkWgtDtl tGrdBkWgtDtl
                    LEFT JOIN FreedomBridge.bridge.arGrdBkWgtDetails bGrdBkWgtDtl ON bGrdBkWgtDtl.src_Name = tGrdBkWgtDtl.Descrip
                                                                                     AND bGrdBkWgtDtl.src_RecordKey = tGrdBkWgtDtl.Code
                                                                                     AND bGrdBkWgtDtl.trg_CampusId = @CampusId
                                                                                     AND bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId = tGrdBkWgtDtl.InstrGrdBkWgtDetailId
                    WHERE     bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId IS NULL;





        ALTER TABLE FreedomAdvantage..arGrdBkWgtDetails DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..arGrdBkWgtDetails
                    SELECT    tGrdBkWgtDtl.InstrGrdBkWgtDetailId
                             ,tGrdBkWgtDtl.InstrGrdBkWgtId
                             ,tGrdBkWgtDtl.Code
                             ,tGrdBkWgtDtl.Descrip
                             ,tGrdBkWgtDtl.Weight
                             ,tGrdBkWgtDtl.Seq
                             ,tGrdBkWgtDtl.ModUser
                             ,tGrdBkWgtDtl.ModDate
                             ,tGrdBkWgtDtl.GrdComponentTypeId
                             ,tGrdBkWgtDtl.Parameter
                             ,tGrdBkWgtDtl.Number
                             ,tGrdBkWgtDtl.GrdPolicyId
                             ,tGrdBkWgtDtl.Required
                             ,tGrdBkWgtDtl.MustPass
                             ,tGrdBkWgtDtl.CreditsPerService
                    FROM      #GrdBkWgtDtl tGrdBkWgtDtl
                    LEFT JOIN FreedomAdvantage..arGrdBkWgtDetails dGrdBkWgtDtl ON dGrdBkWgtDtl.Descrip = tGrdBkWgtDtl.Descrip
                                                                                  AND dGrdBkWgtDtl.GrdComponentTypeId = tGrdBkWgtDtl.GrdComponentTypeId
                                                                                  AND dGrdBkWgtDtl.InstrGrdBkWgtId = tGrdBkWgtDtl.InstrGrdBkWgtId
                    WHERE     dGrdBkWgtDtl.InstrGrdBkWgtDetailId IS NULL
                              AND tGrdBkWgtDtl.InactiveDateRange = 0;

        ALTER TABLE FreedomAdvantage..arGrdBkWgtDetails ENABLE TRIGGER ALL;



        DROP TABLE #GrdBkWgtDtl;
        DROP TABLE #Settings;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arGrdBkWgtDetails]'
                          ,GETDATE()
                          ,'arGrdBkWgtDetails,FreedomBridge.bridge.arGrdBkWgtDetails'
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;
GO
