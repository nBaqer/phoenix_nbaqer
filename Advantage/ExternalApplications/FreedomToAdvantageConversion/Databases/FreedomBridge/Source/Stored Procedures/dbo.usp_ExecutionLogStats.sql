SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_ExecutionLogStats]
    @ExecutionID AS INT
   ,@ExecutionStage AS SMALLINT
   ,@ExecutionDescription AS VARCHAR(100)
AS
    BEGIN
        --DO NOT EXECUTE THIS DIRECTLY
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION T1;

        DECLARE @TableName VARCHAR(128)
               ,@BridgeScriptPattern AS NVARCHAR(MAX) = 'SELECT @BridgeRowcount = COUNT(1) FROM FreedomBridge.bridge.[CountMe];'
               ,@AdvantageScriptPattern AS NVARCHAR(MAX) = 'IF EXISTS (SELECT 1 FROM FreedomAdvantage.INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND TABLE_NAME = ''[CountMe]'') Begin SELECT @AdvantageRowcount = COUNT(1) FROM FreedomAdvantage.dbo.[CountMe] END;'
               ,@Script AS NVARCHAR(MAX)
               ,@BridgeRowcount AS INT
               ,@AdvantageRowcount AS INT;

        DECLARE dbCursor CURSOR FOR
            SELECT TableName
            FROM   FreedomBridge.dbo.Execution_Table
            WHERE  IsDisabled = 0;

        OPEN dbCursor;
        FETCH NEXT FROM dbCursor
        INTO @TableName;

        WHILE ( @@fetch_status <> -1 )
            BEGIN
                SELECT @Script = REPLACE(@BridgeScriptPattern, '[CountMe]', @TableName);
                EXEC sp_executesql @Script
                                  ,N'@BridgeRowcount int output'
                                  ,@BridgeRowcount OUTPUT;

                SELECT @Script = REPLACE(@AdvantageScriptPattern, '[CountMe]', @TableName);
                EXEC sp_executesql @Script
                                  ,N'@AdvantageRowcount int output'
                                  ,@AdvantageRowcount OUTPUT;

                INSERT INTO FreedomBridge.dbo.ExecutionStats
                            SELECT @ExecutionID AS ExecutionID                   -- int
                                  ,@ExecutionStage AS ExecutionStage             -- smallint
                                  ,@ExecutionDescription AS ExecutionDescription -- VARCHAR(100)
                                  ,@TableName AS TableName                       -- varchar(50)
                                  ,@BridgeRowcount AS BridgeDataCount            -- int
                                  ,@AdvantageRowcount AS AdvantageDataCount;     -- int


                FETCH NEXT FROM dbCursor
                INTO @TableName;
            END;

        CLOSE dbCursor;
        DEALLOCATE dbCursor;
        COMMIT TRANSACTION T1;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_ExecutionLogStats]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

    END;

GO
