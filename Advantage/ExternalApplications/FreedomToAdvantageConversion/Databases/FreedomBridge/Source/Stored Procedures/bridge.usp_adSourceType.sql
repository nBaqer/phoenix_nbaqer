SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-08-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adSourceType]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @CampusId UNIQUEIDENTIFIER,
            @SchoolID INT;

    SELECT Entity,
           Value
    INTO #Settings
    FROM dbo.udf_GetSettingsList();

    SELECT @CampusId = Value
    FROM #Settings
    WHERE Entity = 'CampusID';

    SELECT @SchoolID = Value
    FROM #Settings
    WHERE Entity = 'SchoolID';

    BEGIN TRANSACTION;

    SELECT CASE
               WHEN dSrcType.SourceTypeDescrip IS NULL THEN
                   NEWID()
               ELSE
                   dSrcType.SourceTypeId
           END AS SourceTypeId,                          -- uniqueidentifier
           sAdType.Description AS SourceTypeDescrip,     -- varchar(50)
           dSrcCat.SourceCatagoryId AS SourceCatagoryId, -- uniqueidentifier
           dStatus.StatusId AS StatusId,                 -- uniqueidentifier
           'sa' AS ModUser,                              -- varchar(50)
           GETDATE() AS ModDate,                         -- datetime
           sAdType.RecNum AS SourceTypeCode,             -- varchar(50)
           dCampGrp.CampGrpId AS CampGrpId               -- uniqueidentifier
    INTO #SourceType
    FROM Freedom..USERCODE_FIL sAdType
        INNER JOIN FreedomAdvantage..syStatuses dStatus
            ON dStatus.Status = CASE
                                    WHEN sAdType.Active = 'Y' THEN
                                        'Active'
                                    ELSE
                                        'Inactive'
                                END
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp
            ON dCampGrp.CampGrpCode = 'ALL'
        INNER JOIN FreedomAdvantage..adSourceCatagory dSrcCat
            ON dSrcCat.SourceCatagoryDescrip = 'Ad Category'  AND dSrcCat.CampGrpId = dCampGrp.CampGrpId
        LEFT JOIN FreedomAdvantage..adSourceType dSrcType
            ON dSrcType.CampGrpId = dCampGrp.CampGrpId
               AND dSrcType.SourceCatagoryId = dSrcCat.SourceCatagoryId
               AND dSrcType.SourceTypeDescrip = sAdType.Description
    WHERE sAdType.CodeKey = 'LeadSourceType';

    ALTER TABLE FreedomAdvantage..adSourceType DISABLE TRIGGER ALL;

    SELECT tSrcType.SourceTypeId,
           tSrcType.SourceTypeDescrip,
           tSrcType.SourceCatagoryId,
           tSrcType.StatusId,
           tSrcType.ModUser,
           tSrcType.ModDate,
           tSrcType.SourceTypeCode,
           tSrcType.CampGrpId
    FROM #SourceType tSrcType
        LEFT JOIN FreedomAdvantage..adSourceType dSrcType
            ON dSrcType.CampGrpId = tSrcType.CampGrpId
               AND dSrcType.SourceTypeDescrip = tSrcType.SourceTypeDescrip
    WHERE dSrcType.SourceTypeId IS NULL;

    SELECT *
    FROM FreedomAdvantage..adSourceType;

    INSERT INTO FreedomAdvantage..adSourceType
    SELECT tSrcType.SourceTypeId,
           tSrcType.SourceTypeDescrip,
           tSrcType.SourceCatagoryId,
           tSrcType.StatusId,
           tSrcType.ModUser,
           tSrcType.ModDate,
           tSrcType.SourceTypeCode,
           tSrcType.CampGrpId
    FROM #SourceType tSrcType
        LEFT JOIN FreedomAdvantage..adSourceType dSrcType
            ON dSrcType.CampGrpId = tSrcType.CampGrpId
               AND dSrcType.SourceTypeDescrip = tSrcType.SourceTypeDescrip
    WHERE dSrcType.SourceTypeId IS NULL;

    ALTER TABLE FreedomAdvantage..adSourceType ENABLE TRIGGER ALL;

    -- Create bridge table if not already created in the bridge
    IF NOT EXISTS
    (
        SELECT *
        FROM sys.objects
        WHERE object_id = OBJECT_ID(N'FreedomBridge.bridge.adSourceType')
              AND type IN ( N'U' )
    )
    BEGIN
        CREATE TABLE FreedomBridge.bridge.adSourceType
        (
            trg_SourceTypeId UNIQUEIDENTIFIER,
            src_AdSourceName VARCHAR(50),
            src_RecNum INT,
            trg_CampusId UNIQUEIDENTIFIER,
            PRIMARY KEY (
                            src_AdSourceName,
                            src_RecNum
                        )
        );
    END;

    INSERT INTO FreedomBridge.bridge.adSourceType
    SELECT SourceTypeId,
           SourceTypeDescrip,
           tSrcType.SourceTypeCode,
           @CampusId
    FROM #SourceType tSrcType
        LEFT JOIN FreedomBridge.bridge.adSourceType bSrcType
            ON bSrcType.trg_SourceTypeId = tSrcType.SourceTypeId
    WHERE bSrcType.trg_SourceTypeId IS NULL;

    DROP TABLE #SourceType;

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_adSourceType]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];

    COMMIT TRANSACTION;

END;

GO
