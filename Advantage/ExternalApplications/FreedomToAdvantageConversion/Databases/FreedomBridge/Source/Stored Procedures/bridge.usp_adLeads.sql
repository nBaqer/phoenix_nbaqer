SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-13-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adLeads]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusID UNIQUEIDENTIFIER
               ,@SchoolID INT;

        DECLARE @ActiveStatus UNIQUEIDENTIFIER;

        SET @ActiveStatus = (
                            SELECT   TOP ( 1 ) StatusId
                            FROM     FreedomAdvantage..syStatuses
                            WHERE    StatusCode = 'A'
                            ORDER BY StatusCode
                            );

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusID = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';



        DECLARE @LeadStudentMapping AS TABLE
            (
                FirstName VARCHAR(300) NULL
               ,PrivLastName VARCHAR(300) NULL
               ,StuSSN VARCHAR(300) NULL
               ,LeadSSN VARCHAR(300) NULL
               ,LeadId INT NULL
               ,StudentId INT NULL
               ,ProspectKey INT NULL
               ,LeadAddressLine VARCHAR(300) NULL
               ,LeadAddressState VARCHAR(300) NULL
               ,StudentAddressLine VARCHAR(300) NULL
               ,StudentAddressState VARCHAR(300) NULL
               ,NumberOfRecords INT NULL
            );
        /*MAPPING LEADS AND STUDENTS BASED ON FIRST NAME, LAST NAME, STATE, ADDRESS, SSN*/
        INSERT INTO @LeadStudentMapping
                    SELECT     students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN AS LeadSSN
                              ,leads.RecordKey AS LeadId
                              ,students.RecordKey AS StudentId
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.AddressLine1
                              ,students.AddressState
                              ,COUNT(*) AS NumberOfRecords
                    FROM       Freedom..STUDREC students
                    INNER JOIN Freedom..PROSPECT_FIL leads ON students.FirstName = leads.FirstName
                                                              AND students.PrivLastName = leads.LastName
                                                              AND leads.StuSSN = students.StuSSN
                                                              AND leads.AddressState = students.AddressState
                                                              AND leads.AddressLine1 = students.AddressLine1
                    WHERE      students.ProspectKey = 0
                               AND students.RecordKey NOT IN (
                                                             SELECT StudentId
                                                             FROM   @LeadStudentMapping
                                                             )
                    GROUP BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN
                              ,leads.RecordKey
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.RecordKey
                              ,students.AddressState
                              ,students.AddressLine1
                    ORDER BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN;

        /*MAPPING LEADS AND STUDENTS BASED ON FIRST NAME, LAST NAME, STATE, ADDRESS*/
        INSERT INTO @LeadStudentMapping
                    SELECT     students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN AS LeadSSN
                              ,leads.RecordKey AS LeadId
                              ,students.RecordKey AS StudentId
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.AddressLine1
                              ,students.AddressState
                              ,COUNT(*) AS NumberOfRecords
                    FROM       Freedom..STUDREC students
                    INNER JOIN Freedom..PROSPECT_FIL leads ON students.FirstName = leads.FirstName
                                                              AND students.PrivLastName = leads.LastName
                                                              AND leads.AddressState = students.AddressState
                                                              AND leads.AddressLine1 = students.AddressLine1
                    WHERE      students.ProspectKey = 0
                               AND students.RecordKey NOT IN (
                                                             SELECT StudentId
                                                             FROM   @LeadStudentMapping
                                                             )
                    GROUP BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN
                              ,leads.RecordKey
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.RecordKey
                              ,students.AddressState
                              ,students.AddressLine1
                    ORDER BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN;

        /*MAPPING LEADS AND STUDENTS BASED ON FIRST NAME, LAST NAME, STATE*/
        INSERT INTO @LeadStudentMapping
                    SELECT     students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN AS LeadSSN
                              ,leads.RecordKey AS LeadId
                              ,students.RecordKey AS StudentId
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.AddressLine1
                              ,students.AddressState
                              ,COUNT(*) AS NumberOfRecords
                    FROM       Freedom..STUDREC students
                    INNER JOIN Freedom..PROSPECT_FIL leads ON students.FirstName = leads.FirstName
                                                              AND students.PrivLastName = leads.LastName
                                                              AND leads.AddressState = students.AddressState
                    WHERE      students.ProspectKey = 0
                               AND students.RecordKey NOT IN (
                                                             SELECT StudentId
                                                             FROM   @LeadStudentMapping
                                                             )
                               AND leads.AddressLine1 IS NOT NULL
                               AND LEN(leads.AddressLine1) > 0
                    GROUP BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN
                              ,leads.RecordKey
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.RecordKey
                              ,students.AddressState
                              ,students.AddressLine1
                    ORDER BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN;

        /*MAPPING LEADS AND STUDENTS BASED ON FIRST NAME, LAST NAME*/
        INSERT INTO @LeadStudentMapping
                    SELECT     students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN AS LeadSSN
                              ,leads.RecordKey AS LeadId
                              ,students.RecordKey AS StudentId
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.OldAddressLine1
                              ,students.AddressState
                              ,COUNT(*) AS NumberOfRecords
                    FROM       Freedom..STUDREC students
                    INNER JOIN Freedom..PROSPECT_FIL leads ON students.FirstName = leads.FirstName
                                                              AND students.PrivLastName = leads.LastName
                    WHERE      students.ProspectKey = 0
                               AND students.RecordKey NOT IN (
                                                             SELECT StudentId
                                                             FROM   @LeadStudentMapping
                                                             )
                    --AND leads.AddressLine1Old IS NOT NULL
                    --AND LEN(leads.AddressLine1Old) > 0
                    --AND leads.AddressState IS NOT NULL
                    --AND LEN(leads.AddressState) > 0
                    GROUP BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN
                              ,leads.StuSSN
                              ,leads.RecordKey
                              ,students.ProspectKey
                              ,leads.AddressLine1
                              ,leads.AddressState
                              ,students.RecordKey
                              ,students.AddressState
                              ,students.OldAddressLine1
                    ORDER BY   students.FirstName
                              ,students.PrivLastName
                              ,students.StuSSN;

        UPDATE     lead
        SET        lead.StuSSN = studentLeadMapping.StuSSN
        FROM       Freedom..PROSPECT_FIL lead
        INNER JOIN @LeadStudentMapping studentLeadMapping ON lead.RecordKey = studentLeadMapping.LeadId;

        UPDATE     student
        SET        student.ProspectKey = studentLeadMapping.LeadId
        FROM       Freedom..STUDREC student
        INNER JOIN @LeadStudentMapping studentLeadMapping ON student.RecordKey = studentLeadMapping.StudentId;


        SELECT CASE WHEN A.LocalRank = 1 THEN NEWID()
                    ELSE CAST(NULL AS UNIQUEIDENTIFIER)
               END AS LeadId
              ,*
        INTO   #StuLeads
        FROM   (
               SELECT DENSE_RANK() OVER ( ORDER BY FirstName
                                                  ,LastName
                                                  ,StuSSN
                                        ) AS TotalRank
                     ,DENSE_RANK() OVER ( PARTITION BY FirstName
                                                      ,LastName
                                                      ,StuSSN
                                          ORDER BY StuRecordKey DESC
                                                  ,dupPros.ProRecordKey DESC
                                        ) AS LocalRank
                     ,*
               FROM   (
                      SELECT          sStu.RecordKey AS StuRecordKey
                                     ,sStu.StuSSN
                                     ,sStu.ProspectKey
                                     ,ISNULL(sStu.FirstName, sPro.FirstName) AS FirstName
                                     ,ISNULL(sStu.PrivLastName, sPro.LastName) AS LastName
                                     ,LTRIM(RTRIM(sStu.FirstName)) AS StuFirstName
                                     ,LTRIM(RTRIM(sPro.FirstName)) AS ProFirstName
                                     ,LTRIM(RTRIM(sStu.PrivLastName)) AS StuLastName
                                     ,LTRIM(RTRIM(sPro.LastName)) AS ProLastName
                                     ,ISNULL(sPro.RecordKey, 0) AS ProRecordKey
                                     ,FreedomBridge.dbo.udf_GetNumericOnly(sPro.PhoneNum1) AS ProPhone
                                     ,CASE WHEN sStu.RecordKey IS NOT NULL THEN 'STUDREC_FIL'
                                           ELSE 'PROSPECT_FIL'
                                      END AS TableName
                                     ,sStu.PrivCurrentEnrollStatus
                                     ,sPro.InquiryDate AS InquiryDate
                                     ,sPro.InquiryTime AS InquiryTime
                                     ,sStu.StartDate AS StartDate
                                     ,sStu.TClockBadgeNum AS BadgeNumber
									 ,sstu.DropReasonCode
                      FROM            Freedom..STUDREC sStu
                      FULL OUTER JOIN Freedom..PROSPECT_FIL sPro ON sStu.ProspectKey = sPro.RecordKey
                      ) dupPros
               WHERE  ISNULL(dupPros.PrivCurrentEnrollStatus, 1) != 0
               ) A;

        CREATE NONCLUSTERED INDEX IX_TMP1
            ON #StuLeads
        (
        StuRecordKey ASC
       ,TotalRank ASC
       ,LocalRank ASC
       ,LeadId ASC
        )   ;

        SELECT     ISNULL(l1.LeadId, l2.LeadId) AS LeadId
                  ,l1.StuRecordKey
                  ,l1.StuSSN
                  ,l1.ProspectKey
                  ,l1.ProRecordKey
                  ,l1.TableName
                  ,l1.TotalRank
                  ,l1.LocalRank
                  ,l1.InquiryDate
                  ,l1.InquiryTime
                  ,l1.PrivCurrentEnrollStatus
                  ,l1.StartDate
                  ,l1.BadgeNumber
				  ,l1.DropReasonCode
        INTO       #MergeProspects
        FROM       #StuLeads l1
        INNER JOIN #StuLeads l2 ON l1.TotalRank = l2.TotalRank
                                   AND l2.LeadId IS NOT NULL
        ORDER BY   l1.StuSSN;

        CREATE NONCLUSTERED INDEX IX_TMP2
            ON #MergeProspects
        (
        StuSSN ASC
       ,StuRecordKey ASC
       ,TableName ASC
       ,ProRecordKey ASC
        )   ;



        SELECT   *
        INTO     #Leads
        FROM     (
                 SELECT      ISNULL(bLead.trg_LeadId, tStus.LeadId) AS LeadId                  -- uniqueidentifier
                            ,ISNULL(tStus.ProspectKey, tStus.ProRecordKey) AS ProspectID       -- int
                            ,LTRIM(RTRIM(sStud.FirstName)) AS FirstName                        -- varchar(50)
                            ,LTRIM(RTRIM(sStud.PrivLastName)) AS LastName                      -- varchar(50)
                            ,LTRIM(RTRIM(NULLIF(sStud.MidInitial, ''))) AS MiddleName          -- varchar(50)
                            ,FreedomBridge.dbo.udf_GetNumericOnly(sStud.StuSSN) AS SSN         -- varchar(50)
                            ,'sa' AS ModUser                                                   -- varchar(50)
                            ,GETDATE() AS ModDate                                              -- datetime
                            ,CASE WHEN bPhType1.src_Description = 'Home' THEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
                                  WHEN bPhType1.src_Description = 'Other' THEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
                                  WHEN bPhType1.src_Description = 'Mobile' THEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
                                  WHEN bPhType1.src_Description = 'Fax' THEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
                                  WHEN bPhType1.src_Description = 'Emergency' THEN sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1
                             END AS Phone                                                      -- varchar(50)
                            ,NULLIF(sStud.Email, '') AS HomeEmail                              -- varchar(50)
                            ,RTRIM(   ( CASE WHEN sStud.AddressLine1 NOT IN ( '' ) THEN ( sStud.AddressLine1 )
                                             ELSE (
                                                  SELECT TOP ( 1 ) AddressLine1
                                                  FROM   Freedom..STUDREC
                                                  WHERE  AddressLine1 NOT IN ( '' )
                                                  )
                                        END
                                      )
                                  ) AS Address1                                                -- varchar(50)
                            ,NULL AS Address2                                                  -- varchar(50)
                            ,RTRIM(sStud.AddressCity) AS City                                  -- varchar(50)
                            ,dState.StateId AS StateId                                         -- uniqueidentifier
                            ,FreedomBridge.dbo.udf_GetNumericOnly(sStud.PrivAddressZip) AS Zip -- varchar(50)
                            ,CASE WHEN tStus.PrivCurrentEnrollStatus = 5 --when case is dropped out but there is not attendance then it is a no start
                                       AND (
                                           NOT EXISTS (
                                                      SELECT TOP 1 RecordKey
                                                      FROM   Freedom..ATTENDMO_FIL Atten
                                                      WHERE  Atten.StudrecKey = tStus.StuRecordKey
                                                      )
                                           OR ((
                                               SELECT SUM(Atten.PrivMonthTot)
                                               FROM   Freedom..ATTENDMO_FIL Atten
                                               WHERE  Atten.StudrecKey = tStus.StuRecordKey
                                               ) = 0
                                              )
                                           OR ((
                                               SELECT LDACheck.LastDateAttended
                                               FROM   Freedom..STUDREC LDACheck
                                               WHERE  LDACheck.RecordKey = tStus.StuRecordKey
                                               ) IS NULL
                                              )
                                           ) THEN (
                                                  SELECT TOP 1 scTemp.StatusCodeId
                                                  FROM   FreedomAdvantage..syStatusCodes scTemp
                                                  WHERE  scTemp.SysStatusId = 8
                                                  )
												  
                                  WHEN tStus.PrivCurrentEnrollStatus = 1
                                       AND tStus.StartDate > GETDATE() THEN (
                                                                            SELECT TOP 1 scTemp.StatusCodeId
                                                                            FROM   FreedomAdvantage..syStatusCodes scTemp
                                                                            WHERE  scTemp.SysStatusId = 7
                                                                            )
                                  ELSE ISNULL(bStatCode.trg_StatusCodeId
                                             ,(
                                              SELECT StatusCodeId
                                              FROM   FreedomAdvantage..syStatusCodes
                                              WHERE  StatusCodeDescrip = 'New Lead'
                                              )
                                             )
                             END AS LeadStatus                                                 -- uniqueidentifier
                            ,NULL AS WorkEmail                                                 -- varchar(50)
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS AddressType                     -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS Prefix                          -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS Suffix                          -- uniqueidentifier
                            ,sStud.BirthDate AS BirthDate                                      -- datetime
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS Sponsor                         -- uniqueidentifier
                            ,ISNULL(bUser.trg_UserId, dUser.UserId) AS AdmissionsRep           -- uniqueidentifier
                            ,GETDATE() AS AssignedDate                                         -- datetime
                            ,bGen.trg_GenderId AS Gender                                       -- uniqueidentifier
                            ,bEth.trg_EthCodeId AS Race                                        -- uniqueidentifier
                            ,bMar.trg_MaritalStatId AS MaritalStatus                           -- uniqueidentifier
                            ,bFamInc.trg_FamilyIncomeID AS FamilyIncome                        -- uniqueidentifier
                            ,sStud2.NumOfDeps AS Children                                      -- varchar(50)
                            ,bPhType1.trg_PhoneTypeId AS PhoneType                             -- uniqueidentifier
                            ,CASE WHEN bPhType1.trg_PhoneTypeId IS NULL THEN NULL
                                  ELSE dStat.StatusId
                             END AS PhoneStatus                                                -- uniqueidentifier
                            ,dSrcCat.SourceCatagoryId AS SourceCategoryID                      -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS SourceTypeID                    -- uniqueidentifier
                            ,tStus.InquiryDate AS SourceDate                                   -- datetime
                            ,dPrgGrp.PrgGrpId AS AreaID                                        -- uniqueidentifier
                            ,bProg.trg_ProgId AS ProgramID                                     -- uniqueidentifier
                            ,sStud.EnrolledDate AS ExpectedStart                               -- datetime
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftID                         -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS Nationality                     -- uniqueidentifier
                            ,bCit.trg_CitizenshipId AS Citizen                                 -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS DrivLicStateID                  -- uniqueidentifier
                            ,NULL AS DrivLicNumber                                             -- varchar(50)
                            ,NULL AS AlienNumber                                               -- varchar(50)
                            ,NULL AS Comments                                                  -- varchar(240)
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS SourceAdvertisement             -- uniqueidentifier
                            ,@CampusID AS CampusId                                             -- uniqueidentifier
                            ,bPrgVer.trg_PrgVerId AS PrgVerId                                  -- uniqueidentifier
                            ,dCountry.CountryId AS Country                                     -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS County                          -- uniqueidentifier
                            ,DATEDIFF(YEAR, sStud.BirthDate, GETDATE()) - 1 AS Age             -- varchar(50)
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS PreviousEducation               -- uniqueidentifier
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS AddressStatus                   -- uniqueidentifier
                            ,sStud.EnrolledDate AS CreatedDate                                 -- datetime
                            ,NULL AS RecruitmentOffice                                         -- varchar(50)
                            ,NULL AS OtherState                                                -- varchar(50)
                            ,0 AS ForeignPhone                                                 -- bit
                            ,0 AS ForeignZip                                                   -- bit
                            ,dLeadGrps.LeadGrpId AS LeadgrpId                                  -- uniqueidentifier
                            ,bDepType.trg_DependencyTypeId AS DependencyTypeId                 -- uniqueidentifier
                            ,bDegCert.trg_DegCertSeekingId AS DegCertSeekingId                 -- uniqueidentifier
                            ,bGeoType.trg_GeographicTypeId AS GeographicTypeId                 -- uniqueidentifier
                            ,aHousing.trg_HousingId AS HousingId                               -- uniqueidentifier
                            ,bAdmin.trg_AdminCriteriaId AS admincriteriaid                     -- uniqueidentifier
                            ,NULL AS DateApplied                                               -- datetime
                            ,tStus.InquiryTime AS InquiryTime                                  -- varchar(50)
                            ,NULL AS AdvertisementNote                                         -- varchar(50)
                            ,CASE WHEN bPhType1.src_Description = 'Work' THEN sStud.PhoneNumAreaCode2 + sStud.PhoneNumPrefix2 + sStud.PhoneNumBody2
                             END AS Phone2                                                     -- varchar(50)
                            ,bPhType2.trg_PhoneTypeId AS PhoneType2                            -- uniqueidentifier
                            ,CASE WHEN bPhType2.trg_PhoneTypeId IS NULL THEN NULL
                                  ELSE dStat.StatusId
                             END AS PhoneStatus2                                               -- uniqueidentifier
                            ,0 AS ForeignPhone2                                                -- bit
                            ,0 AS DefaultPhone                                                 -- tinyint
                            ,0 AS IsDisabled                                                   -- BIT NULL
                            ,1 AS IsFirstTimeInSchool                                          -- BIT NOT NULL
                            ,1 AS IsFirstTimePostSecSchool                                     -- BIT NOT NULL
                            ,NULL AS EntranceInterviewDate                                     -- datetime NULL
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS ProgramScheduleId               -- uniqueidentifier NULL
                            ,NULL AS BestTime                                                  -- varchar -- (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                            ,NULL AS DistanceToSchool                                          -- int NULL
                            ,NULL AS CampaignId                                                -- int NULL
                            ,NULL AS ProgramOfInterest                                         -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                            ,NULL AS CampusOfInterest                                          -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS TransportationId                -- uniqueidentifier NULL
                            ,'' AS NickName                                                    -- varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_NickName DEFAULT ('')
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS AttendTypeId                    -- uniqueidentifier NULL
                            ,1 AS PreferredContactId                                           -- int NOT NULL CONSTRAINT DF_adLeads_PreferredContactId DEFAULT ((1))
                            ,'' AS AddressApt                                                  -- varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_AddressApt DEFAULT ('')
                            ,0 AS NoneEmail                                                    -- bit NOT NULL CONSTRAINT DF_adLeads_NoneEmail DEFAULT ((0))
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS HighSchoolId                    -- uniqueidentifier NULL
                            ,NULL AS HighSchoolGradDate                                        -- datetime NULL
                            ,NULL AS AttendingHs                                               -- bit NULL
                            ,'00000000-0000-0000-0000-000000000000' AS StudentId               -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentId DEFAULT ('00000000-0000-0000-0000-000000000000')
                            ,CASE WHEN tStus.StuRecordKey IS NULL THEN '0'
                                  ELSE CAST(tStus.StuRecordKey AS INT) + 1000
                             END AS StudentNumber                                              -- nvarchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_StudentNumber DEFAULT ('')
                            ,@ActiveStatus AS StudentStatusId                                  -- uniqueidentifier NOT NULL
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS ReasonNotEnrolledId             -- uniqueidentifier NULL
                            ,CAST(NULL AS UNIQUEIDENTIFIER) AS EnrollStateId                   -- uniqueidentifier NULL
                            ,tStus.StuRecordKey
                            ,tStus.ProRecordKey
                            ,tStus.TotalRank
                            ,tStus.LocalRank
                            ,tStus.TableName
                 FROM        #MergeProspects tStus
                 INNER JOIN  Freedom..STUDREC sStud ON sStud.StuSSN = tStus.StuSSN
                                                       AND sStud.RecordKey = tStus.StuRecordKey
                                                       AND tStus.TableName = 'STUDREC_FIL'
                 INNER JOIN  Freedom..STUDREC2 sStud2 ON sStud2.RecordKey = sStud.RecordKey
                 LEFT JOIN   Freedom..PROSPECT_FIL sPro ON sPro.RecordKey = sStud.ProspectKey
                 LEFT JOIN   Freedom..RECRUITR_FIL sRecruit ON sRecruit.RecordKey = sPro.RecruiterRecKey
                 INNER JOIN  FreedomBridge.bridge.syStatusCodes bStatCode ON bStatCode.src_ID = sStud.PrivCurrentEnrollStatus
                                                                             AND bStatCode.trg_StatusLevelId = 2
                 LEFT JOIN   FreedomBridge.bridge.syPhoneType bPhType1 ON bPhType1.src_ID = sStud.PhoneType1
                 LEFT JOIN   FreedomBridge.bridge.syPhoneType bPhType2 ON bPhType2.src_ID = sStud.PhoneType2
                 LEFT JOIN   FreedomAdvantage..syStates dState ON dState.StateCode = sStud.AddressState
                 INNER JOIN  FreedomAdvantage..adCountries dCountry ON dCountry.CountryCode = 'USA'
                 INNER JOIN  FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
                 LEFT JOIN   FreedomBridge.stage.syUsers stUser ON stUser.src_FullName = CASE WHEN sStud.ProspectKey = sPro.RecordKey
                                                                                                   AND sPro.RecruiterRecKey != 0 THEN
                                                                                                  LTRIM(RTRIM(sRecruit.FirstName)) + ' '
                                                                                                  + LTRIM(RTRIM(sRecruit.LastName))
                                                                                         END
                                                                   AND stUser.TableName = 'RECRUITR_FIL'
                                                                   AND stUser.src_SchoolID = @SchoolID
                 INNER JOIN  FreedomAdvantage..syUsers dUser ON dUser.FullName = 'sa'
                 LEFT JOIN   FreedomBridge.bridge.syUsers bUser ON bUser.src_UserId = stUser.src_RecordKey
                                                                   AND bUser.TableName = stUser.TableName
                                                                   AND bUser.trg_CampusId = @CampusID
                 LEFT JOIN   FreedomBridge.bridge.syFamilyIncome bFamInc ON bFamInc.trg_FamilyIncomeDescrip = CASE WHEN sStud2.YearlyIncome
                                                                                                                        BETWEEN bFamInc.src_AmountFrom AND bFamInc.src_AmountTo THEN
                                                                                                                       bFamInc.trg_FamilyIncomeDescrip
                                                                                                                   WHEN sStud2.YearlyIncome = 0 THEN 'Unknown'
                                                                                                              END
                 LEFT JOIN   FreedomBridge.bridge.adGenders bGen ON bGen.src_ID = sStud2.Sex
                 LEFT JOIN   FreedomBridge.bridge.adEthCodes bEth ON bEth.src_ID = sStud2.Race
                 LEFT JOIN   FreedomBridge.bridge.adMaritalStatus bMar ON bMar.src_ID = sStud2.MaritalStatus
                 LEFT JOIN   FreedomAdvantage..adSourceCatagory dSrcCat ON dSrcCat.SourceCatagoryDescrip = 'Ad Category'
                 LEFT JOIN   FreedomBridge.bridge.adCitizenships bCit ON bCit.src_ID = CASE WHEN sStud2.CitizenType = 0 THEN
                                                                                            (
                                                                                            SELECT TOP ( 1 ) st2.CitizenType
                                                                                            FROM   Freedom..STUDREC AS st
                                                                                            JOIN   Freedom..STUDREC2 AS st2 ON st2.RecordKey = st.RecordKey
                                                                                            WHERE  st2.CitizenType > 0
                                                                                            )
                                                                                            ELSE sStud2.CitizenType
                                                                                       END
                 LEFT JOIN   FreedomBridge.bridge.arHousing aHousing ON aHousing.src_ID = sStud2.HousingType
                 LEFT JOIN   FreedomBridge.bridge.adDependencyTypes bDepType ON bDepType.src_ID = sStud2.Dependency
                 LEFT JOIN   FreedomBridge.bridge.adDegCertSeeking bDegCert ON bDegCert.src_ID = sStud.DegreeSeekingType
                 LEFT JOIN   FreedomBridge.bridge.adGeographicTypes bGeoType ON bGeoType.src_ID = sStud2.UrbanType
                 LEFT JOIN   FreedomAdvantage..arPrgGrp dPrgGrp ON dPrgGrp.PrgGrpCode = 'DEF'
                 LEFT JOIN   FreedomAdvantage..adLeadGroups dLeadGrps ON dLeadGrps.code = 'C'
                 LEFT JOIN   FreedomBridge.bridge.arPrgVersions bPrgVer ON bPrgVer.src_RecordKey = sStud.CourseNumEnrolledIn
                                                                           AND bPrgVer.trg_CampusId = @CampusID
                 LEFT JOIN   Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sStud.RecordKey
                 LEFT JOIN   FreedomBridge.bridge.arPrograms bProg ON bProg.src_RecordKey = sCourse.ParentKey
                                                                      AND bProg.trg_CampusId = @CampusID
                 LEFT JOIN   FreedomBridge.bridge.adAdminCriteria bAdmin ON bAdmin.src_ID = sStud.AbilityToBenefit
                 OUTER APPLY (
                             SELECT TOP 1 bLead1.trg_LeadId
                             FROM   bridge.adLeads bLead1
                             WHERE  bLead1.src_StuSSN = dbo.udf_GetNumericOnly(tStus.StuSSN)
                                    AND bLead1.LocalRank = 1
                             ) bLead
                 UNION
                 SELECT     tPro.LeadId                                                          -- uniqueidentifier
                           ,ISNULL(tPro.ProspectKey, tPro.ProRecordKey) AS ProspectID            -- int
                           ,LTRIM(RTRIM(sPros.FirstName)) AS FirstName                           -- varchar(50)
                           ,LTRIM(RTRIM(sPros.LastName)) AS LastName                             -- varchar(50)
                           ,LTRIM(RTRIM(NULLIF(sPros.MidInitial, ''))) AS MiddleName             -- varchar(50)
                           ,ISNULL(FreedomBridge.dbo.udf_GetNumericOnly(tPro.StuSSN), '') AS SSN -- varchar(50)
                           ,'sa' AS ModUser                                                      -- varchar(50)
                           ,GETDATE() AS ModDate                                                 -- datetime
                           ,sPros.PhoneNum1 AS Phone                                             -- varchar(50)
                           ,NULLIF(sPros.EMail, '') AS HomeEmail                                 -- varchar(50)
                           ,sPros.addressline1 AS Address1                                    -- varchar(50)
                           ,NULL AS Address2                                                     -- varchar(50)
                           ,sPros.AddressCity AS City                                            -- varchar(50)
                           ,dState.StateId AS StateId                                            -- uniqueidentifier
                           ,FreedomBridge.dbo.udf_GetNumericOnly(sPros.PrivAddressZip) AS Zip    -- varchar(50)
                           ,CASE WHEN tPro.PrivCurrentEnrollStatus = 5 --when case is dropped out but there is not attendance then it is a no start
                                      AND (
                                          NOT EXISTS (
                                                     SELECT TOP 1 RecordKey
                                                     FROM   Freedom..ATTENDMO_FIL Atten
                                                     WHERE  Atten.StudrecKey = tPro.StuRecordKey
                                                     )
                                          OR ((
                                              SELECT SUM(Atten.PrivMonthTot)
                                              FROM   Freedom..ATTENDMO_FIL Atten
                                              WHERE  Atten.StudrecKey = tPro.StuRecordKey
                                              ) = 0
                                             )
                                          OR ((
                                              SELECT LDACheck.LastDateAttended
                                              FROM   Freedom..STUDREC LDACheck
                                              WHERE  LDACheck.RecordKey = tPro.StuRecordKey
                                              ) IS NULL
                                             )
                                          ) THEN (
                                                 SELECT TOP 1 scTemp.StatusCodeId
                                                 FROM   FreedomAdvantage..syStatusCodes scTemp
                                                 WHERE  scTemp.SysStatusId = 8
                                                 )
										
                                 WHEN tPro.PrivCurrentEnrollStatus = 1
                                      AND tPro.StartDate > GETDATE() THEN (
                                                                          SELECT TOP 1 scTemp.StatusCodeId
                                                                          FROM   FreedomAdvantage..syStatusCodes scTemp
                                                                          WHERE  scTemp.SysStatusId = 7
                                                                          )
                                 ELSE ISNULL(bStatCode.trg_StatusCodeId
                                            ,(
                                             SELECT StatusCodeId
                                             FROM   FreedomAdvantage..syStatusCodes
                                             WHERE  StatusCodeDescrip = 'New Lead'
                                             )
                                            )
                            END AS LeadStatus                                                    -- uniqueidentifier
                           ,NULL AS WorkEmail                                                    -- varchar(50)
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS AddressType                        -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Prefix                             -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Suffix                             -- uniqueidentifier
                           ,sPros.BirthDate AS BirthDate                                         -- datetime
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Sponsor                            -- uniqueidentifier
                           ,ISNULL(bUser.trg_UserId, dUser.UserId) AS AdmissionsRep              -- uniqueidentifier
                           ,GETDATE() AS AssignedDate                                            -- datetime
                           ,bGen.trg_GenderId AS Gender                                          -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Race                               -- uniqueidentifier
                           ,bMar.trg_MaritalStatId AS MaritalStatus                              -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS FamilyIncome                       -- uniqueidentifier
                           ,NULL AS Children                                                     -- varchar(50)
                           ,bPhType1.trg_PhoneTypeId AS PhoneType                                -- uniqueidentifier
                           ,CASE WHEN bPhType1.trg_PhoneTypeId IS NULL THEN NULL
                                 ELSE dStat.StatusId
                            END AS PhoneStatus                                                   -- uniqueidentifier
                           ,dSrcCat.SourceCatagoryId AS SourceCategoryID                         -- uniqueidentifier
                           ,bSrcType.trg_SourceTypeId AS SourceTypeID                            -- uniqueidentifier
                           ,sPros.InquiryDate AS SourceDate                                      -- datetime
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS AreaID                             -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS ProgramID                          -- uniqueidentifier
                           ,sPros.EnrolledDate AS ExpectedStart                                  -- datetime
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftID                            -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Nationality                        -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS Citizen                            -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS DrivLicStateID                     -- uniqueidentifier
                           ,NULL AS DrivLicNumber                                                -- varchar(50)
                           ,NULL AS AlienNumber                                                  -- varchar(50)
                           ,NULL AS Comments                                                     -- varchar(240)
                           ,bSrcAd.trg_SourceAdvId AS SourceAdvertisement                        -- uniqueidentifier
                           ,dCamp.CampusId AS CampusId                                           -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS PrgVerId                           -- uniqueidentifier
                           ,dCountry.CountryId AS Country                                        -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS County                             -- uniqueidentifier
                           ,DATEDIFF(YEAR, sPros.BirthDate, GETDATE()) - 1 AS Age                -- varchar(50)
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS PreviousEducation                  -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS AddressStatus                      -- uniqueidentifier
                           ,sPros.InquiryDate AS CreatedDate                                     -- datetime
                           ,NULL AS RecruitmentOffice                                            -- varchar(50)
                           ,NULL AS OtherState                                                   -- varchar(50)
                           ,0 AS ForeignPhone                                                    -- bit
                           ,0 AS ForeignZip                                                      -- bit
                           ,dLeadGrps.LeadGrpId AS LeadgrpId                                     -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS DependencyTypeId                   -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS DegCertSeekingId                   -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS GeographicTypeId                   -- uniqueidentifier
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS HousingId                          -- uniqueidentifier
                           ,bAdmin.trg_AdminCriteriaId AS admincriteriaid                        -- uniqueidentifier
                           ,sPros.InquiryDate AS DateApplied                                     -- datetime
                           ,NULLIF(sPros.InquiryTime, 0) AS InquiryTime                          -- varchar(50)
                           ,NULL AS AdvertisementNote                                            -- varchar(50)
                           ,NULLIF(sPros.PhoneNum2, '') AS Phone2                                -- varchar(50)
                           ,bPhType2.trg_PhoneTypeId AS PhoneType2                               -- uniqueidentifier
                           ,CASE WHEN bPhType2.trg_PhoneTypeId IS NULL THEN NULL
                                 ELSE dStat.StatusId
                            END AS PhoneStatus2                                                  -- uniqueidentifier
                           ,0 AS ForeignPhone2                                                   -- bit
                           ,0 AS DefaultPhone                                                    -- tinyin
                           ,0 AS IsDisabled                                                      -- BIT NULL
                           ,1 AS IsFirstTimeInSchool                                             -- BIT NOT NULL
                           ,1 AS IsFirstTimePostSecSchool                                        -- BIT NOT NULL
                           ,NULL AS EntranceInterviewDate                                        -- datetime NULL
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS ProgramScheduleId                  -- uniqueidentifier NULL
                           ,NULL AS BestTime                                                     -- varchar -- (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                           ,NULL AS DistanceToSchool                                             -- int NULL
                           ,NULL AS CampaignId                                                   -- int NULL
                           ,NULL AS ProgramOfInterest                                            -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                           ,NULL AS CampusOfInterest                                             -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS TransportationId                   -- uniqueidentifier NULL
                           ,'' AS NickName                                                       -- varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_NickName DEFAULT ('')
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS AttendTypeId                       -- uniqueidentifier NULL
                           ,1 AS PreferredContactId                                              -- int NOT NULL CONSTRAINT DF_adLeads_PreferredContactId DEFAULT ((1))
                           ,'' AS AddressApt                                                     -- varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_AddressApt DEFAULT ('')
                           ,0 AS NoneEmail                                                       -- bit NOT NULL CONSTRAINT DF_adLeads_NoneEmail DEFAULT ((0))
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS HighSchoolId                       -- uniqueidentifier NULL
                           ,NULL AS HighSchoolGradDate                                           -- datetime NULL
                           ,NULL AS AttendingHs                                                  -- bit NULL
                           ,'00000000-0000-0000-0000-000000000000' AS StudentId                  -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentId DEFAULT ('00000000-0000-0000-0000-000000000000')
                           ,CASE WHEN tPro.BadgeNumber IS NULL THEN '0'
                                 ELSE tPro.BadgeNumber
                            END AS StudentNumber                                                 -- nvarchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_StudentNumber DEFAULT ('')
                           ,@ActiveStatus AS StudentStatusId                                     -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentStatusId DEFAULT ('1AF592A6-8790-48EC-9916-5412C25EF49F')
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS ReasonNotEnrolledId                -- uniqueidentifier NULL
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS EnrollStateId                      -- uniqueidentifier NULL
                           ,tPro.StuRecordKey
                           ,tPro.ProRecordKey
                           ,tPro.TotalRank
                           ,tPro.LocalRank
                           ,tPro.TableName
                 FROM       #MergeProspects tPro
                 INNER JOIN Freedom..PROSPECT_FIL sPros ON sPros.RecordKey = tPro.ProRecordKey
                                                           AND tPro.TableName = 'PROSPECT_FIL'
                 LEFT JOIN  Freedom..USERCODE_FIL sUserCode ON sUserCode.RecNum = sPros.RecordKey
                 LEFT JOIN  Freedom..GRADPLAC_FIL F_Placement ON F_Placement.StudrecKey = sPros.StudRecKey
                 LEFT JOIN  FreedomBridge.bridge.syStatusCodes bStatCode ON bStatCode.src_RecNum = sPros.Status
                                                                            AND bStatCode.trg_StatusLevelId = 1
                                                                            AND bStatCode.src_Description = sUserCode.Description
                 LEFT JOIN  FreedomBridge.bridge.syPhoneType bPhType1 ON bPhType1.src_ID = sPros.PhoneType1
                 LEFT JOIN  FreedomBridge.bridge.syPhoneType bPhType2 ON bPhType2.src_ID = sPros.PhoneType2
                 LEFT JOIN  FreedomAdvantage..syStates dState ON dState.StateCode = sPros.AddressState
                 INNER JOIN FreedomAdvantage..adCountries dCountry ON dCountry.CountryCode = 'USA'
                 INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
                 LEFT JOIN  FreedomBridge.stage.syUsers stUser ON stUser.src_RecordKey = sPros.RecruiterRecKey
                                                                  AND stUser.TableName = 'RECRUITR_FIL'
                                                                  AND stUser.src_SchoolID = @SchoolID
                 LEFT JOIN  FreedomBridge.bridge.syUsers bUser ON bUser.src_UserId = stUser.src_RecordKey
                                                                  AND bUser.TableName = stUser.TableName
                                                                  AND bUser.trg_CampusId = @CampusID
                 INNER JOIN FreedomAdvantage..syUsers dUser ON dUser.FullName = 'sa'
                 LEFT JOIN  FreedomBridge.bridge.adGenders bGen ON bGen.src_ID = sPros.Sex
                 LEFT JOIN  FreedomBridge.bridge.adMaritalStatus bMar ON bMar.src_ID = sPros.MaritalStatus
                 INNER JOIN FreedomAdvantage..adSourceCatagory dSrcCat ON dSrcCat.SourceCatagoryDescrip = 'Ad Category'
                 LEFT JOIN  FreedomBridge.bridge.adSourceType bSrcType ON bSrcType.src_RecNum = sPros.AdSourceRecKey
                                                                          AND bSrcType.trg_CampusId = @CampusID
                 LEFT JOIN  Freedom..ADSOURCE_FIL sAd ON sAd.RecordKey = sPros.AdSourceRecKey
                 LEFT JOIN  FreedomBridge.bridge.adSourceAdvertisement bSrcAd ON bSrcAd.src_RecordKey = sAd.AdSourceType
                                                                                 AND bSrcAd.trg_CampusId = @CampusID
                 LEFT JOIN  FreedomAdvantage..adLeadGroups dLeadGrps ON dLeadGrps.code = 'C'
                 INNER JOIN FreedomAdvantage..syCampuses dCamp ON dCamp.CampusId = @CampusID
                 LEFT JOIN  FreedomBridge.bridge.adAdminCriteria bAdmin ON bAdmin.src_ID = sPros.AdmisCrit
                 ) MergeLead
        ORDER BY MergeLead.ProspectID;

        ALTER TABLE FreedomAdvantage..adLeads DISABLE TRIGGER ALL;


        SELECT    DISTINCT tLead.LeadId AS LeadId                           -- uniqueidentifier
                          ,tLead.ProspectID AS ProspectID                   -- int
                          ,tLead.FirstName AS FirstName                     -- varchar(50)
                          ,tLead.LastName AS LastName                       -- varchar(50)
                          ,tLead.MiddleName AS MiddleName                   -- varchar(50)
                          ,tLead.SSN AS SSN                                 -- varchar(50)
                          ,tLead.ModUser AS ModUser                         -- varchar(50)
                          ,tLead.ModDate AS ModDate                         -- datetime
                          ,tLead.Phone AS Phone                             -- varchar(50)
                          ,tLead.HomeEmail AS HomeEmail                     -- varchar(50)
                          ,tLead.Address1 AS Address1                       -- varchar(50)
                          ,tLead.Address2 AS Address2                       -- varchar(50)
                          ,tLead.City AS City                               -- varchar(50)
                          ,tLead.StateId AS StateId                         -- uniqueidentifier
                          ,tLead.Zip AS Zip                                 -- varchar(50)
                          ,tLead.LeadStatus AS LeadStatus                   -- uniqueidentifier
                          ,tLead.WorkEmail AS WorkEmail                     -- varchar(50)
                          ,tLead.AddressType AS AddressType                 -- uniqueidentifier
                          ,tLead.Prefix AS Prefix                           -- uniqueidentifier
                          ,tLead.Suffix AS Suffix                           -- uniqueidentifier
                          ,tLead.BirthDate AS BirthDate                     -- datetime
                          ,tLead.Sponsor AS Sponsor                         -- uniqueidentifier
                          ,tLead.AdmissionsRep AS AdmissionsRep             -- uniqueidentifier
                          ,tLead.AssignedDate AS AssignedDate               -- datetime
                          ,tLead.Gender AS Gender                           -- uniqueidentifier
                          ,tLead.Race AS Race                               -- uniqueidentifier
                          ,tLead.MaritalStatus AS MaritalStatus             -- uniqueidentifier
                          ,tLead.FamilyIncome AS FamilyIncome               -- uniqueidentifier
                          ,tLead.Children AS Children                       -- varchar(50)
                          ,tLead.PhoneType AS PhoneType                     -- uniqueidentifier
                          ,tLead.PhoneStatus AS PhoneStatus                 -- uniqueidentifier
                          ,tLead.SourceCategoryID AS SourceCategoryID       -- uniqueidentifier
                          ,tLead.SourceTypeID AS SourceTypeID               -- uniqueidentifier
                          ,tLead.SourceDate AS SourceDate                   -- datetime
                          ,tLead.AreaID AS AreaID                           -- uniqueidentifier
                          ,tLead.ProgramID AS ProgramID                     -- uniqueidentifier
                          ,tLead.ExpectedStart AS ExpectedStart             -- datetime
                          ,tLead.ShiftID AS ShiftID                         -- uniqueidentifier
                          ,tLead.Nationality AS Nationality                 -- uniqueidentifier
                          ,tLead.Citizen AS Citizen                         -- uniqueidentifier
                          ,tLead.DrivLicStateID AS DrivLicStateID           -- uniqueidentifier
                          ,tLead.DrivLicNumber AS DrivLicNumber             -- varchar(50)
                          ,tLead.AlienNumber AS AlienNumber                 -- varchar(50)
                          ,tLead.Comments AS Comments                       -- varchar(240)
                          ,tLead.SourceAdvertisement AS SourceAdvertisement -- uniqueidentifier
                          ,tLead.CampusId AS CampusId                       -- uniqueidentifier
                          ,tLead.PrgVerId AS PrgVerId                       -- uniqueidentifier
                          ,tLead.Country AS Country                         -- uniqueidentifier
                          ,tLead.County AS County                           -- uniqueidentifier
                          ,tLead.Age AS Age                                 -- varchar(50)
                          ,tLead.PreviousEducation AS PreviousEducation     -- uniqueidentifier
                          ,tLead.AddressStatus AS AddressStatus             -- uniqueidentifier
                          ,tLead.CreatedDate AS CreatedDate                 -- datetime
                          ,tLead.RecruitmentOffice AS RecruitmentOffice     -- varchar(50)
                          ,tLead.OtherState AS OtherState                   -- varchar(50)
                          ,tLead.ForeignPhone AS ForeignPhone               -- bit
                          ,tLead.ForeignZip AS ForeignZip                   -- bit
                          ,tLead.LeadgrpId AS LeadgrpId                     -- uniqueidentifier
                          ,tLead.DependencyTypeId AS DependencyTypeId       -- uniqueidentifier
                          ,tLead.DegCertSeekingId AS DegCertSeekingId       -- uniqueidentifier
                          ,tLead.GeographicTypeId AS GeographicTypeId       -- uniqueidentifier
                          ,tLead.HousingId AS HousingId                     -- uniqueidentifier
                          ,tLead.admincriteriaid AS admincriteriaid         -- uniqueidentifier
                          ,tLead.DateApplied AS DateApplied                 -- datetime
                          ,CASE WHEN tLead.InquiryTime IS NULL THEN NULL
                                WHEN CONVERT(BIGINT, tLead.InquiryTime) <= 1159 THEN CONVERT(VARCHAR(50), STUFF(tLead.InquiryTime, 3, 0, ':')) + ':00AM'
                                ELSE CONVERT(VARCHAR(50), STUFF(tLead.InquiryTime, 3, 0, ':')) + ':00PM'
                           END AS InquiryTime                               -- varchar(50)
                          ,tLead.AdvertisementNote AS AdvertisementNote     -- varchar(50)
                          ,tLead.Phone2 AS Phone2                           -- varchar(50)
                          ,tLead.PhoneType2 AS PhoneType2                   -- uniqueidentifier
                          ,tLead.PhoneStatus2 AS PhoneStatus2               -- uniqueidentifier
                          ,tLead.ForeignPhone2 AS ForeignPhone2             -- bit
                          ,tLead.DefaultPhone AS DefaultPhone               -- tinyint
                          ,tLead.IsDisabled                                 -- BIT NULL
                          ,tLead.IsFirstTimeInSchool                        -- BIT NOT NULL
                          ,tLead.IsFirstTimePostSecSchool                   -- BIT NOT NULL
                          ,tLead.EntranceInterviewDate                      -- datetime NULL
                          ,tLead.ProgramScheduleId                          -- uniqueidentifier NULL
                          ,tLead.BestTime                                   -- varchar -- (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                          ,tLead.DistanceToSchool                           -- int NULL
                          ,tLead.CampaignId                                 -- int NULL
                          ,tLead.ProgramOfInterest                          -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                          ,tLead.CampusOfInterest                           -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                          ,tLead.TransportationId                           -- uniqueidentifier NULL
                          ,tLead.NickName                                   -- varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_NickName DEFAULT ('')
                          ,tLead.AttendTypeId                               -- uniqueidentifier NULL
                          ,tLead.PreferredContactId                         -- int NOT NULL CONSTRAINT DF_adLeads_PreferredContactId DEFAULT ((1))
                          ,tLead.AddressApt                                 -- varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_AddressApt DEFAULT ('')
                          ,tLead.NoneEmail                                  -- bit NOT NULL CONSTRAINT DF_adLeads_NoneEmail DEFAULT ((0))
                          ,tLead.HighSchoolId                               -- uniqueidentifier NULL
                          ,tLead.HighSchoolGradDate                         -- datetime NULL
                          ,tLead.AttendingHs                                -- bit NULL
                          ,tLead.StudentId                                  -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentId DEFAULT ('00000000-0000-0000-0000-000000000000')
                          ,tLead.StudentNumber                              -- nvarchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_StudentNumber DEFAULT ('')
                          ,tLead.StudentStatusId                            -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentStatusId DEFAULT ('1AF592A6-8790-48EC-9916-5412C25EF49F')
                          ,tLead.ReasonNotEnrolledId                        -- uniqueidentifier NULL
                          ,tLead.EnrollStateId
                          ,NULL AS VendorId
                          ,NULL AS AfaStudentId
                          ,ROW_NUMBER() OVER ( PARTITION BY tLead.FirstName
                                                           ,tLead.LastName
                                                           ,tLead.SSN
                                               ORDER BY tLead.LastName
                                             ) AS TotalRank
                          ,tLead.LocalRank
                          ,tLead.TableName
                          ,tLead.StuRecordKey
                          ,tLead.ProRecordKey
        INTO      #testTable1 -- uniqueidentifier NULL
        FROM      #Leads tLead
        LEFT JOIN FreedomAdvantage..adLeads dLead ON tLead.LeadId = dLead.LeadId
        WHERE     tLead.LocalRank = 1
                  AND dLead.LeadId IS NULL;


        INSERT INTO FreedomAdvantage..adLeads
                    SELECT DISTINCT tLead.LeadId AS LeadId                           -- uniqueidentifier
                                   ,tLead.ProspectID AS ProspectID                   -- int
                                   ,tLead.FirstName AS FirstName                     -- varchar(50)
                                   ,tLead.LastName AS LastName                       -- varchar(50)
                                   ,tLead.MiddleName AS MiddleName                   -- varchar(50)
                                   ,tLead.SSN AS SSN                                 -- varchar(50)
                                   ,tLead.ModUser AS ModUser                         -- varchar(50)
                                   ,tLead.ModDate AS ModDate                         -- datetime
                                   ,tLead.Phone AS Phone                             -- varchar(50)
                                   ,tLead.HomeEmail AS HomeEmail                     -- varchar(50)
                                   ,tLead.Address1 AS Address1                       -- varchar(50)
                                   ,tLead.Address2 AS Address2                       -- varchar(50)
                                   ,tLead.City AS City                               -- varchar(50)
                                   ,tLead.StateId AS StateId                         -- uniqueidentifier
                                   ,tLead.Zip AS Zip                                 -- varchar(50)
                                   ,tLead.LeadStatus AS LeadStatus                   -- uniqueidentifier
                                   ,tLead.WorkEmail AS WorkEmail                     -- varchar(50)
                                   ,tLead.AddressType AS AddressType                 -- uniqueidentifier
                                   ,tLead.Prefix AS Prefix                           -- uniqueidentifier
                                   ,tLead.Suffix AS Suffix                           -- uniqueidentifier
                                   ,tLead.BirthDate AS BirthDate                     -- datetime
                                   ,tLead.Sponsor AS Sponsor                         -- uniqueidentifier
                                   ,tLead.AdmissionsRep AS AdmissionsRep             -- uniqueidentifier
                                   ,tLead.AssignedDate AS AssignedDate               -- datetime
                                   ,tLead.Gender AS Gender                           -- uniqueidentifier
                                   ,tLead.Race AS Race                               -- uniqueidentifier
                                   ,tLead.MaritalStatus AS MaritalStatus             -- uniqueidentifier
                                   ,tLead.FamilyIncome AS FamilyIncome               -- uniqueidentifier
                                   ,tLead.Children AS Children                       -- varchar(50)
                                   ,tLead.PhoneType AS PhoneType                     -- uniqueidentifier
                                   ,tLead.PhoneStatus AS PhoneStatus                 -- uniqueidentifier
                                   ,tLead.SourceCategoryID AS SourceCategoryID       -- uniqueidentifier
                                   ,tLead.SourceTypeID AS SourceTypeID               -- uniqueidentifier
                                   ,tLead.SourceDate AS SourceDate                   -- datetime
                                   ,tLead.AreaID AS AreaID                           -- uniqueidentifier
                                   ,tLead.ProgramID AS ProgramID                     -- uniqueidentifier
                                   ,tLead.ExpectedStart AS ExpectedStart             -- datetime
                                   ,tLead.ShiftID AS ShiftID                         -- uniqueidentifier
                                   ,tLead.Nationality AS Nationality                 -- uniqueidentifier
                                   ,tLead.Citizen AS Citizen                         -- uniqueidentifier
                                   ,tLead.DrivLicStateID AS DrivLicStateID           -- uniqueidentifier
                                   ,tLead.DrivLicNumber AS DrivLicNumber             -- varchar(50)
                                   ,tLead.AlienNumber AS AlienNumber                 -- varchar(50)
                                   ,tLead.Comments AS Comments                       -- varchar(240)
                                   ,tLead.SourceAdvertisement AS SourceAdvertisement -- uniqueidentifier
                                   ,tLead.CampusId AS CampusId                       -- uniqueidentifier
                                   ,tLead.PrgVerId AS PrgVerId                       -- uniqueidentifier
                                   ,tLead.Country AS Country                         -- uniqueidentifier
                                   ,tLead.County AS County                           -- uniqueidentifier
                                   ,tLead.Age AS Age                                 -- varchar(50)
                                   ,tLead.PreviousEducation AS PreviousEducation     -- uniqueidentifier
                                   ,tLead.AddressStatus AS AddressStatus             -- uniqueidentifier
                                   ,tLead.CreatedDate AS CreatedDate                 -- datetime
                                   ,tLead.RecruitmentOffice AS RecruitmentOffice     -- varchar(50)
                                   ,tLead.OtherState AS OtherState                   -- varchar(50)
                                   ,tLead.ForeignPhone AS ForeignPhone               -- bit
                                   ,tLead.ForeignZip AS ForeignZip                   -- bit
                                   ,tLead.LeadgrpId AS LeadgrpId                     -- uniqueidentifier
                                   ,tLead.DependencyTypeId AS DependencyTypeId       -- uniqueidentifier
                                   ,tLead.DegCertSeekingId AS DegCertSeekingId       -- uniqueidentifier
                                   ,tLead.GeographicTypeId AS GeographicTypeId       -- uniqueidentifier
                                   ,tLead.HousingId AS HousingId                     -- uniqueidentifier
                                   ,tLead.admincriteriaid AS admincriteriaid         -- uniqueidentifier
                                   ,tLead.DateApplied AS DateApplied                 -- datetime
                                   ,tLead.InquiryTime                                -- varchar(50)
                                   ,tLead.AdvertisementNote AS AdvertisementNote     -- varchar(50)
                                   ,tLead.Phone2 AS Phone2                           -- varchar(50)
                                   ,tLead.PhoneType2 AS PhoneType2                   -- uniqueidentifier
                                   ,tLead.PhoneStatus2 AS PhoneStatus2               -- uniqueidentifier
                                   ,tLead.ForeignPhone2 AS ForeignPhone2             -- bit
                                   ,tLead.DefaultPhone AS DefaultPhone               -- tinyint
                                   ,tLead.IsDisabled                                 -- BIT NULL
                                   ,tLead.IsFirstTimeInSchool                        -- BIT NOT NULL
                                   ,tLead.IsFirstTimePostSecSchool                   -- BIT NOT NULL
                                   ,tLead.EntranceInterviewDate                      -- datetime NULL
                                   ,tLead.ProgramScheduleId                          -- uniqueidentifier NULL
                                   ,tLead.BestTime                                   -- varchar -- (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                                   ,tLead.DistanceToSchool                           -- int NULL
                                   ,tLead.CampaignId                                 -- int NULL
                                   ,tLead.ProgramOfInterest                          -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                                   ,tLead.CampusOfInterest                           -- varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
                                   ,tLead.TransportationId                           -- uniqueidentifier NULL
                                   ,tLead.NickName                                   -- varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_NickName DEFAULT ('')
                                   ,tLead.AttendTypeId                               -- uniqueidentifier NULL
                                   ,tLead.PreferredContactId                         -- int NOT NULL CONSTRAINT DF_adLeads_PreferredContactId DEFAULT ((1))
                                   ,tLead.AddressApt                                 -- varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_AddressApt DEFAULT ('')
                                   ,tLead.NoneEmail                                  -- bit NOT NULL CONSTRAINT DF_adLeads_NoneEmail DEFAULT ((0))
                                   ,tLead.HighSchoolId                               -- uniqueidentifier NULL
                                   ,tLead.HighSchoolGradDate                         -- datetime NULL
                                   ,tLead.AttendingHs                                -- bit NULL
                                   ,tLead.StudentId                                  -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentId DEFAULT ('00000000-0000-0000-0000-000000000000')
                                   ,tLead.StudentNumber                              -- nvarchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_adLeads_StudentNumber DEFAULT ('')
                                   ,tLead.StudentStatusId                            -- uniqueidentifier NOT NULL CONSTRAINT DF_adLeads_StudentStatusId DEFAULT ('1AF592A6-8790-48EC-9916-5412C25EF49F')
                                   ,tLead.ReasonNotEnrolledId                        -- uniqueidentifier NULL
                                   ,tLead.EnrollStateId
                                   ,NULL AS VendorId
                                   ,NULL AS AfaStudentId
                    FROM   #testTable1 tLead
                    WHERE  tLead.TotalRank = 1;


        ALTER TABLE FreedomAdvantage..adLeads ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.adLeads
                    SELECT DISTINCT tLead.LeadId
                                   ,tLead.FirstName
                                   ,tLead.LastName
                                   ,tLead.StuRecordKey
                                   ,tLead.SSN
                                   ,tLead.ProRecordKey
                                   ,tLead.TotalRank
                                   ,tLead.LocalRank
                                   ,tLead.TableName
                                   ,@CampusID AS CampusId
                    FROM   #Leads tLead
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   FreedomBridge.bridge.adLeads E
                                      WHERE  E.trg_LeadId = tLead.LeadId
                                             AND E.src_StuRecordKey = tLead.StuRecordKey
                                      );

        DROP TABLE #StuLeads;
        DROP TABLE #MergeProspects;
        DROP TABLE #Leads;
        DROP TABLE #Settings;
        DROP TABLE #testTable1;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeads]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;





GO
