SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-15-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arPrograms]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
		, @CampusGroupId UNIQUEIDENTIFIER
               ,@SchoolID INT
               ,@IsClockHour BIT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

		        SELECT @CampusGroupId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @IsClockHour = Value
        FROM   #Settings
        WHERE  Entity = 'IsClockHour';

        SELECT     CASE WHEN NOT EXISTS ( SELECT TOP 1 E.progId FROM FreedomAdvantage..arPrograms E WHERE RTRIM(LTRIM(E.ProgDescrip)) = RTRIM(LTRIM(sProg.Name)) AND E.CampGrpId = dCampGrp.CampGrpId) THEN NEWID()
                        ELSE ( SELECT TOP 1 E.progId FROM FreedomAdvantage..arPrograms E WHERE RTRIM(LTRIM(E.ProgDescrip)) = RTRIM(LTRIM(sProg.Name)) AND E.CampGrpId = dCampGrp.CampGrpId)
                   END AS ProgId                                 -- uniqueidentifier
                  ,sProg.RecordKey AS ProgCode                   -- varchar(12)
                  ,dStatus.StatusId AS StatusId                  -- uniqueidentifier
                  ,dCampGrp.CampGrpCode
                  ,LTRIM(RTRIM(sProg.Name)) AS ProgDescrip       -- varchar(100)
                  ,'sa' AS ModUser                               -- varchar(50)
                  ,GETDATE() AS ModDate                          -- datetime
                  ,NULL AS NumPmtPeriods                         -- tinyint
                  ,dCampGrp.CampGrpId AS CampGrpId               -- uniqueidentifier
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS DegreeId    -- uniqueidentifier
                  ,NULL AS CalendarTypeId                        -- int
                  ,sCourse.PrivAcCalType AS ACId                 -- int
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS shiftid     -- uniqueidentifier
                  ,NULLIF(sProg.CIPCode, '') AS CIPCode          -- varchar(6)
                  ,NULL AS CredentialLevel                       -- varchar(2)
                  ,0 AS IsGEProgram                              -- bit
                  ,bProgCred.trg_CredentialId AS CredentialLvlId -- uniqueidentifier
                  ,sCourse.Report1098T AS Is1098T                --NULL AS Is1098T                               -- BIT NULL
        INTO       #Program
        FROM       Freedom..PROGSTUDY_DEF sProg
        INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sProg.CourseDefVerKey
                                                  AND sCourse.TermDefKey > 0
                                                  AND sProg.Name != 'DNU'
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'All'
        LEFT JOIN  FreedomBridge.bridge.arProgCredential bProgCred ON bProgCred.src_CredentialID = sProg.CredentialLevel

        ALTER TABLE FreedomAdvantage..arPrograms DISABLE TRIGGER ALL;




        INSERT INTO FreedomAdvantage..arPrograms
                    SELECT    tProg.ProgId
                             ,tProg.ProgCode
                             ,tProg.StatusId
                             ,tProg.ProgDescrip
                             ,tProg.ModUser
                             ,tProg.ModDate
                             ,tProg.NumPmtPeriods
                             ,tProg.CampGrpId
                             ,tProg.DegreeId
                             ,tProg.CalendarTypeId
                             ,tProg.ACId
                             ,tProg.shiftid
                             ,tProg.CIPCode
                             ,tProg.CredentialLevel
                             ,tProg.IsGEProgram
                             ,tProg.CredentialLvlId
                             ,tProg.Is1098T
                    FROM      #Program tProg
                    LEFT JOIN FreedomAdvantage..arPrograms dProg ON dProg.ProgDescrip = tProg.ProgDescrip
					 AND dProg.CampGrpId = tProg.CampGrpId
                    WHERE     dProg.ProgDescrip IS NULL
                              AND NOT EXISTS (
                                             SELECT TOP 1 ProgId
                                             FROM   FreedomAdvantage..arPrograms
                                             WHERE  ProgId = dProg.ProgId
                                             );

        ALTER TABLE FreedomAdvantage..arPrograms ENABLE TRIGGER ALL;


        INSERT INTO FreedomBridge.bridge.arPrograms
                    SELECT    tProg.ProgId
                             ,tProg.ProgDescrip
                             ,tProg.ProgCode
                             ,@CampusId
                    FROM      #Program tProg
                    LEFT JOIN FreedomBridge.bridge.arPrograms bProg ON bProg.src_ProgDescrip = tProg.ProgDescrip
                                                                       AND bProg.trg_CampusId = @CampusId
                    WHERE     bProg.trg_ProgId IS NULL;

        DROP TABLE #Program;
		DROP TABLE #Settings

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arPrograms]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;


GO
