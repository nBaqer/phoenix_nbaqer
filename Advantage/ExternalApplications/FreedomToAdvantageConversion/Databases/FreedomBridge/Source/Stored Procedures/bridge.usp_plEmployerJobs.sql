SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_plEmployerJobs]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @ActiveStatusId = Value
        FROM   #Settings
        WHERE  Entity = 'ActiveStatusId';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        --InIndustryFlag
        --ID	Description
        --N	Not industry related
        --S	Self employed in industry
        --X	Requested not to be placed
        --Y	Industry related

        DECLARE @ConversionDescription VARCHAR(10) = 'Conversion';
        DECLARE @ConversionCatId UNIQUEIDENTIFIER = NEWID();

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..plJobCats
                      WHERE  JobCatDescrip = @ConversionDescription
                             AND CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..plJobCats
                VALUES ( @ConversionCatId       -- JobCatId - uniqueidentifier
                        ,'C'                    -- JobCatCode - varchar(12)
                        ,@ActiveStatusId        -- StatusId - uniqueidentifier
                        ,@ConversionDescription -- JobCatDescrip - varchar(50)
                        ,@CampGrpId             -- CampGrpId - uniqueidentifier
                        ,''                     -- ModUser - varchar(50)
                        ,GETDATE()              -- ModDate - datetime
                    );

            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..adTitles
                      WHERE  TitleDescrip = @ConversionDescription
                             AND CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..adTitles
                VALUES ( NEWID()                        -- TitleId - uniqueidentifier
                        ,'C'                            -- TitleCode - varchar(12)
                        ,@ActiveStatusId                -- StatusId - uniqueidentifier
                        ,@ConversionDescription         -- TitleDescrip - varchar(50)
                        ,@ConversionCatId               -- JobCatId - uniqueidentifier
                        ,@CampGrpId                     -- CampGrpId - uniqueidentifier
                        ,@ConversionDescription + ' SA' -- ModUser - varchar(50)
                        ,GETDATE()                      -- ModDate - datetime
                    );
            END;
		
        SELECT     NEWID() AS EmployerJobId                       -- uniqueidentifier
                  ,dbo.udf_FrstLtrWord_V2(sGrad.EmpName) AS Code  -- varchar(50)
                  ,dStatus.StatusId AS StatusId                   -- uniqueidentifier
                  ,sGrad.EmpName AS EmployerJobTitle              -- varchar(50)
                  ,dTitle.TitleId AS JobTitleId                   -- uniqueidentifier
                  ,sGrad.EmpName AS JobDescription                -- varchar(300)
                  ,dJobCat.JobCatId AS JobGroupId                 -- uniqueidentifier
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ContactId    -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS TypeId       -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS AreaId       -- uniqueidentifier FK to [dbo].[adCounties]
                  ,@CampGrpId AS CampGrpId                -- uniqueidentifier
                  ,NULL AS WorkDays                               -- varchar(50) NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS FeeId        -- uniqueidentifier [dbo].[plFee] NO DATA
                  ,CAST(1 AS INT) AS NumberOpen                                -- int
                  ,CAST(1 AS INT) AS NumberFilled                              -- int
                  ,NULL AS OpenedFrom                             -- datetime NO DATA
                  ,NULL AS OpenedTo                               -- datetime NO DATA
                  ,sGrad.HiredDate AS Start                       -- datetime
                  ,NULL AS SalaryFrom                             -- varchar(50) NO DATA
                  ,NULL AS SalaryTo                               -- varchar(50) NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS BenefitsId   -- uniqueidentifier NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ScheduleId   -- uniqueidentifier NO DATA
                  ,NULL AS HoursFrom                              -- varchar(50) NO DATA
                  ,NULL AS HoursTo                                -- varchar(50) NO DATA
                  ,NULL AS Notes                                  -- varchar(300) NO DATA
                  ,'SA' AS ModUser                                -- varchar(50)
                  ,GETDATE() AS ModDate                           -- datetime
                  ,bEmp.trg_EmployerId AS EmployerId              -- uniqueidentifier
                  ,NULL AS JobRequirements                        -- varchar(300) NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS SalaryTypeID -- uniqueidentifier NO DATA
                  ,NULL AS JobPostedDate                          -- datetime NO DATA
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ExpertiseId  -- uniqueidentifier NO DATA
                  ,sGrad.RecordKey AS RecordKey
        INTO       #plEmployerJobs
        FROM       bridge.plEmployers bEmp
        INNER JOIN Freedom..GRADPLAC_FIL sGrad ON sGrad.RecordKey = bEmp.src_ChildRecordKey
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN sGrad.HiredDate IS NULL THEN 'Active'
                                                                                 ELSE 'Inactive'
                                                                            END
        --INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpId = @CampGrpId
        INNER JOIN FreedomAdvantage..adTitles dTitle ON dTitle.TitleDescrip = 'Conversion'
                                                        AND dTitle.CampGrpId = @CampGrpId
        INNER JOIN FreedomAdvantage..plJobCats dJobCat ON dJobCat.JobCatDescrip = 'Conversion'
                                                          AND dJobCat.CampGrpId = @CampGrpId
        WHERE      bEmp.trg_CampusId = @CampusId;

        SELECT *
              ,ROW_NUMBER() OVER ( PARTITION BY Code
                                   ORDER BY Code
                                 ) AS Rep
        INTO   #RepeatedEmployerJobs
        FROM   #plEmployerJobs;

        SELECT EmployerJobId
              ,( Code + ( CASE WHEN Rep = 1 THEN ''
                               ELSE CAST(( Rep - 1 ) AS VARCHAR)
                          END
                        )
               ) AS Code
              ,StatusId
              ,EmployerJobTitle
              ,JobTitleId
              ,JobDescription
              ,JobGroupId
              ,ContactId
              ,TypeId
              ,AreaId
              ,CampGrpId
              ,WorkDays
              ,FeeId
              ,NumberOpen
              ,NumberFilled
              ,OpenedFrom
              ,OpenedTo
              ,Start
              ,SalaryFrom
              ,SalaryTo
              ,BenefitsId
              ,ScheduleId
              ,HoursFrom
              ,HoursTo
              ,Notes
              ,ModUser
              ,ModDate
              ,EmployerId
              ,JobRequirements
              ,SalaryTypeID
              ,JobPostedDate
              ,ExpertiseId
              ,RecordKey
        INTO   #plEmployerJobsFinal
        FROM   #RepeatedEmployerJobs;


        ALTER TABLE FreedomAdvantage..plEmployerJobs DISABLE TRIGGER ALL;


        INSERT INTO FreedomAdvantage.dbo.plEmployerJobs
                    SELECT EmployerJobId
                          ,Code
                          ,StatusId
                          ,EmployerJobTitle
                          ,JobTitleId
                          ,JobDescription
                          ,JobGroupId
                          ,ContactId
                          ,TypeId
                          ,AreaId
                          ,CampGrpId
                          ,WorkDays
                          ,FeeId
                          ,NumberOpen
                          ,NumberFilled
                          ,OpenedFrom
                          ,OpenedTo
                          ,Start
                          ,SalaryFrom
                          ,SalaryTo
                          ,BenefitsId
                          ,ScheduleId
                          ,HoursFrom
                          ,HoursTo
                          ,Notes
                          ,ModUser
                          ,ModDate
                          ,EmployerId
                          ,JobRequirements
                          ,SalaryTypeID
                          ,JobPostedDate
                          ,ExpertiseId
                    FROM   #plEmployerJobsFinal;

        ALTER TABLE FreedomAdvantage..plEmployerJobs ENABLE TRIGGER ALL;

        INSERT INTO bridge.plEmployerJobs
                    SELECT EmployerJobId AS trg_EmployerJobId
                          ,RecordKey AS src_RecordKey
                          ,@CampusId AS trg_CampusId
                    FROM   #plEmployerJobsFinal;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_plEmployerJobs]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

       EXEC bridge.usp_plExitInterview;

        DROP TABLE #plEmployerJobs;
        DROP TABLE #plEmployerJobsFinal;
        DROP TABLE #RepeatedEmployerJobs;
        DROP TABLE #Settings;

    END;





GO
