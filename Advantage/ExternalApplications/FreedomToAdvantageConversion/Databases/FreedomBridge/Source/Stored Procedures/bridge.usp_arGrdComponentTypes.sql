SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-07-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGrdComponentTypes]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
		--THROW 1,'test',1
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN DupGrdCmp.LocalRank = 1
                         AND dGrdCmpType.Descrip IS NULL THEN NEWID()
                    WHEN DupGrdCmp.LocalRank = 1
                         AND dGrdCmpType.Descrip IS NOT NULL THEN dGrdCmpType.GrdComponentTypeId
                    ELSE CAST(NULL AS UNIQUEIDENTIFIER)
               END AS GrdComponentTypeId -- uniqueidentifier
             , DupGrdCmp.Code
             , DupGrdCmp.Descrip
             , DupGrdCmp.StatusId
             , DupGrdCmp.CampGrpId
             , DupGrdCmp.ModDate
             , DupGrdCmp.ModUser
             , DupGrdCmp.SysComponentTypeId
             , DupGrdCmp.SubjectDefKey
             , DupGrdCmp.RecordKey
             , DupGrdCmp.LocalRank
        INTO   #DupGrdCmp
        FROM   (
                   SELECT sWork.Name AS Code                    -- varchar(50)
                        , sWork.Name AS Descrip                 -- varchar(50)
                        , dStatus.StatusId AS StatusId          -- uniqueidentifier
                        , dCampGrp.CampGrpId AS CampGrpId       -- uniqueidentifier
                        , GETDATE() AS ModDate                  -- datetime
                        , 'sa' AS ModUser                       -- varchar(50)
                        , dRes.ResourceID AS SysComponentTypeId -- smallint
                        , sWork.SubjectDefKey AS SubjectDefKey
                        , sWork.RecordKey
                        , ROW_NUMBER() OVER ( PARTITION BY sWork.Name, sWork.Type, sWork.RecordKey
                                              ORDER BY sWork.Name, sWork.Type, sWork.RecordKey
                                            ) AS LocalRank
                   FROM   Freedom..WORKUNIT_DEF sWork
                   INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
                   INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                   LEFT JOIN FreedomAdvantage..syResources dRes ON dRes.ResourceID = CASE WHEN sWork.Type = 1 THEN 501
                                                                                          WHEN sWork.Type = 2 THEN 533
                                                                                          WHEN sWork.Type = 3 THEN 500
                                                                                          WHEN sWork.Type = 4 THEN 503
                                                                                     END
                   WHERE  sWork.SubjectDefKey > 0
                          AND sWork.Name != ''
               ) DupGrdCmp
        LEFT JOIN FreedomAdvantage..arGrdComponentTypes dGrdCmpType ON dGrdCmpType.Descrip = DupGrdCmp.Descrip
                                                                       AND dGrdCmpType.Code = CAST(DupGrdCmp.Code AS VARCHAR(50))  AND dGrdCmpType.CampGrpId = DupGrdCmp.CampGrpId;


																

        SELECT   ISNULL(g1.GrdComponentTypeId, NEWID()) AS GrdComponentTypeId
               , g1.Code
               , g1.Descrip
               , g1.StatusId
               , g1.CampGrpId
               , g1.ModDate
               , g1.ModUser
               , g1.SysComponentTypeId
               , g1.SubjectDefKey
               , g1.RecordKey
               , g1.LocalRank
        INTO     #MergedGrdCmp
        FROM     #DupGrdCmp g1
        WHERE g1.LocalRank = 1 AND g1.GrdComponentTypeId IS NOT NULL
        ORDER BY g1.Descrip;
			  

        SELECT GrdCmp.GrdComponentTypeId
             , GrdCmp.Code
             , GrdCmp.Descrip
             , GrdCmp.StatusId
             , GrdCmp.CampGrpId
             , GrdCmp.ModDate
             , GrdCmp.ModUser
             , GrdCmp.SysComponentTypeId
             , GrdCmp.LocalRank
             , GrdCmp.SubjectDefKey
             , GrdCmp.RecordKey
        INTO   #GrdCmp
        FROM   (
                   SELECT tMrgGrdCmp.GrdComponentTypeId
                        , tMrgGrdCmp.Code
                        , tMrgGrdCmp.Descrip
                        , tMrgGrdCmp.StatusId
                        , tMrgGrdCmp.CampGrpId
                        , tMrgGrdCmp.ModDate
                        , tMrgGrdCmp.ModUser
                        , tMrgGrdCmp.SysComponentTypeId
                        , tMrgGrdCmp.LocalRank
                        , tMrgGrdCmp.SubjectDefKey
                        , tMrgGrdCmp.RecordKey
                   FROM   #MergedGrdCmp tMrgGrdCmp
                   LEFT JOIN FreedomAdvantage..arGrdComponentTypes dGrdCmpType ON dGrdCmpType.Descrip = tMrgGrdCmp.Descrip
                                                                                  AND dGrdCmpType.Code = CAST(tMrgGrdCmp.Code AS VARCHAR(10))
                                                                                  AND dGrdCmpType.CampGrpId = tMrgGrdCmp.CampGrpId
																				  AND dGrdCmpType.SysComponentTypeId = tMrgGrdCmp.SysComponentTypeId
               ) GrdCmp;
			
        ALTER TABLE FreedomAdvantage..arGrdComponentTypes DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGrdComponentTypes
                    SELECT DISTINCT tGrdCmp.GrdComponentTypeId
                         , tGrdCmp.Code
                         , tGrdCmp.Descrip
                         , tGrdCmp.StatusId
                         , tGrdCmp.CampGrpId
                         , tGrdCmp.ModDate
                         , tGrdCmp.ModUser
                         , tGrdCmp.SysComponentTypeId
                    FROM   #GrdCmp tGrdCmp
                    LEFT JOIN FreedomAdvantage..arGrdComponentTypes dGrdCmpType ON dGrdCmpType.Descrip = tGrdCmp.Descrip
                                                                                   AND dGrdCmpType.Code = CAST(tGrdCmp.Code AS VARCHAR(50))
                                                                                   AND dGrdCmpType.CampGrpId = tGrdCmp.CampGrpId
																				   	  AND dGrdCmpType.SysComponentTypeId = tGrdCmp.SysComponentTypeId
                    WHERE  dGrdCmpType.GrdComponentTypeId IS NULL
                           AND tGrdCmp.LocalRank = 1 

        ALTER TABLE FreedomAdvantage..arGrdComponentTypes ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGrdComponentTypes')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGrdComponentTypes
                    (
                        trg_GrdComponentTypeId UNIQUEIDENTIFIER
                      , src_Name VARCHAR(255)
                      , src_RecordKey INT
                      , src_SubjectDefKey INT
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Name
                            , src_RecordKey
                            , src_SubjectDefKey
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGrdComponentTypes
                    SELECT DISTINCT tGrdCmp.GrdComponentTypeId
                         , tGrdCmp.Descrip
                         , tGrdCmp.RecordKey
                         , tGrdCmp.SubjectDefKey
                         , @CampusId AS CampusId
                    FROM   #GrdCmp tGrdCmp
                    LEFT JOIN FreedomBridge.bridge.arGrdComponentTypes bGrdCmpType ON bGrdCmpType.src_Name = tGrdCmp.Descrip
                                                                                      AND bGrdCmpType.src_RecordKey = tGrdCmp.RecordKey
                    WHERE  bGrdCmpType.trg_GrdComponentTypeId IS NULL ORDER BY tGrdCmp.Descrip

        DROP TABLE #DupGrdCmp;
        DROP TABLE #MergedGrdCmp;
        DROP TABLE #GrdCmp;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arGrdComponentTypes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;


GO
