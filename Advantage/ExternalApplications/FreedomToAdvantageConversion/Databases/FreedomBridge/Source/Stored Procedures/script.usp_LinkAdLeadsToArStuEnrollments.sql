SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [script].[usp_LinkAdLeadsToArStuEnrollments]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

		/*
		-------------------------------------------------------------------------------------------
		STARTS			UPDATE Enrollments where there are dup lead id with the original lead id
		--------------------------------------------------------------------------------------------
		*/
		SELECT dStudEnr.StudentId, COUNT(dStudEnr.StudentId) NumberOfErollments
		INTO #NumberOfErollments
		FROM FreedomAdvantage..arStuEnrollments dStudEnr
		GROUP BY dStudEnr.StudentId

		SELECT leadId, enrollments.studentId, enrollments.StuEnrollId, ROW_NUMBER() OVER ( PARTITION BY enrollments.StudentId
                     ORDER BY enrollments.StudentId asc
                   ) AS RowNumber 
		INTO #DupLeads 
		FROM FreedomAdvantage..arStuEnrollments enrollments 
		INNER JOIN #NumberOfErollments groupOfEnrollments ON groupOfEnrollments.StudentId = enrollments.StudentId
		WHERE groupOfEnrollments.NumberOfErollments > 1 
		ORDER BY enrollments.StudentId

		UPDATE enrollments
		SET enrollments.LeadId = (SELECT TOP 1 dupLeadsTo.leadId FROM #DupLeads dupLeadsTo WHERE dupLeadsTo.StudentId = dupLeadsFrom.StudentId  AND dupLeadsTo.RowNumber = 1)
		FROM FreedomAdvantage..arStuEnrollments enrollments 
		INNER JOIN #DupLeads dupLeadsFrom ON dupLeadsFrom.StuEnrollId = enrollments.StuEnrollId
		WHERE dupLeadsFrom.RowNumber > 0

		DROP TABLE #DupLeads
		DROP TABLE #NumberOfErollments

		/*
		-------------------------------------------------------------------------------------------
		ENDS			UPDATE Enrollments where there are dup lead id with the original lead id
		--------------------------------------------------------------------------------------------
		*/

        UPDATE dLead
        SET    StudentId = dStudEnr.StudentId
             , dLead.LeadStatus = dStatusCode.StatusCodeId
        FROM   FreedomAdvantage.dbo.arStuEnrollments dStudEnr
        INNER JOIN FreedomAdvantage.dbo.adLeads dLead ON dLead.LeadId = dStudEnr.LeadId
        INNER JOIN FreedomAdvantage.dbo.syStatusCodes dStatusCode ON dStatusCode.StatusCodeDescrip = 'Enrolled'
        INNER JOIN FreedomAdvantage.dbo.sySysStatus dSysStatus ON dSysStatus.SysStatusId = dStatusCode.SysStatusId
                                                                  AND dSysStatus.SysStatusDescrip = 'Enrolled'
        INNER JOIN FreedomAdvantage.dbo.syStatusLevels dStatusLevel ON dStatusLevel.StatusLevelId = dSysStatus.StatusLevelId
                                                                       AND dStatusLevel.StatusLevelDescrip = 'Lead'
        WHERE  dLead.StudentId = '00000000-0000-0000-0000-000000000000';

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_LinkAdLeadsToArStuEnrollments]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;
    END;
GO
