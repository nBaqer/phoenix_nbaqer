SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [bridge].[usp_Cleanup_InsertMissingGradeBookWeightDetails]
AS
begin
    
        DECLARE @CampusId UNIQUEIDENTIFIER;

        SELECT Entity,
               Value
        INTO #Settings
        FROM dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM #Settings
        WHERE Entity = 'CampusID';

        SELECT sd.RecordKey,
               MIN(s.StartDate) AS EffectiveDate
        INTO #EffDates
        FROM Freedom..SUBJECT_DEF sd
            LEFT JOIN Freedom..SUBJECT_RES sr
                ON sr.SubjectDefKey = sd.RecordKey
            INNER JOIN Freedom..COURSE_DEF cd
                ON cd.RecordKey = sd.CourseDefKey
            INNER JOIN Freedom..STUDREC s
                ON s.CourseNumEnrolledIn = cd.RecordKey
        WHERE sd.CourseDefKey > 0
        GROUP BY sd.RecordKey
        ORDER BY sd.RecordKey;

        SELECT CASE
                   WHEN dGrdBkWgts.Descrip IS NULL THEN
                       NEWID()
                   ELSE
                       dGrdBkWgts.InstrGrdBkWgtId
               END AS InstrGrdBkWgtId,                                                                          -- uniqueidentifier
               CAST(NULL AS UNIQUEIDENTIFIER) AS InstructorId,                                                  -- uniqueidentifier
               LEFT(bReqs.src_Name, 50) AS Descrip,                                                             -- varchar(50)
               dStatus.StatusId AS StatusId,                                                                    -- uniqueidentifier
               'sa' AS ModUser,                                                                                 -- varchar(50)
               GETDATE() AS ModDate,                                                                            -- datetime
               bReqs.trg_ReqId AS ReqId,                                                                        -- uniqueidentifier
               ISNULL(ISNULL(sSub.BegDate, EffDate.EffectiveDate), '2018-10-12 00:00:00.000') AS EffectiveDate, -- datetime
               bReqs.src_RecordKey
        INTO #CourseWeights
        FROM FreedomBridge.bridge.arReqs bReqs
            INNER JOIN Freedom..SUBJECT_DEF sSub
                ON sSub.RecordKey = bReqs.src_RecordKey
            INNER JOIN FreedomAdvantage..syStatuses dStatus
                ON dStatus.Status = 'Active'
            LEFT JOIN #EffDates EffDate
                ON EffDate.RecordKey = bReqs.src_RecordKey
            LEFT JOIN FreedomAdvantage..arGrdBkWeights dGrdBkWgts
                ON dGrdBkWgts.ReqId = bReqs.trg_ReqId
                   AND dGrdBkWgts.Descrip = bReqs.src_Name
                   AND dGrdBkWgts.EffectiveDate = ISNULL(
                                                            ISNULL(sSub.BegDate, EffDate.EffectiveDate),
                                                            '2018-10-12 00:00:00.000'
                                                        );



        SELECT *
        INTO #MissingCourseWeights
        FROM #CourseWeights
        WHERE InstrGrdBkWgtId NOT IN
              (
                  SELECT trg_InstrGrdBkWgtId FROM bridge.arGrdBkWeights
              );

        ALTER TABLE FreedomAdvantage..arGrdBkWeights DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGrdBkWeights
        SELECT tGrdBkWgts.InstrGrdBkWgtId,
               tGrdBkWgts.InstructorId,
               tGrdBkWgts.Descrip,
               tGrdBkWgts.StatusId,
               tGrdBkWgts.ModUser,
               tGrdBkWgts.ModDate,
               tGrdBkWgts.ReqId,
               tGrdBkWgts.EffectiveDate
        FROM #MissingCourseWeights tGrdBkWgts
		WHERE NOT EXISTS(SELECT * FROM FreedomAdvantage..arGrdBkWeights gbw
		 WHERE gbw.InstrGrdBkWgtId =  tGrdBkWgts.InstrGrdBkWgtId);

	

        ALTER TABLE FreedomAdvantage..arGrdBkWeights ENABLE TRIGGER ALL;

		
        INSERT INTO FreedomBridge.bridge.arGrdBkWeights
        SELECT tGrdBkWgts.InstrGrdBkWgtId,
               tGrdBkWgts.Descrip,
               tGrdBkWgts.src_RecordKey,
               @CampusId AS CampusId
        FROM #MissingCourseWeights tGrdBkWgts
		WHERE NOT EXISTS(SELECT * FROM FreedomBridge.bridge.arGrdBkWeights bri 
		WHERE bri.src_Name =  tGrdBkWgts.Descrip AND bri.src_RecordKey = tGrdBkWgts.src_RecordKey AND bri.trg_CampusId = @CampusId );



        SELECT CASE
                   WHEN dGrdBkWgtDtl.Descrip IS NULL THEN
                       NEWID()
                   ELSE
                       dGrdBkWgtDtl.InstrGrdBkWgtDetailId
               END AS InstrGrdBkWgtDetailId,                             -- uniqueidentifier
               bGrdBkWgts.InstrGrdBkWgtId AS InstrGrdBkWgtId,        -- uniqueidentifier
               bGrdBkWgts.Descrip AS src_Name,
               bGrdCmpType.src_RecordKey AS Code,                        -- varchar(12)
               bGrdCmpType.src_Name AS Descrip,                          -- varchar(50)
               CASE
                   WHEN sWork.Weight = 0 THEN
                       100.00 / CAST(GrdCmp.WgtCount AS DECIMAL)
                   ELSE
               (CAST(sWork.Weight AS DECIMAL) / CAST(GrdCmp.Total AS DECIMAL)) * 100.00
               END AS Weight,                                            -- decimal
               ROW_NUMBER() OVER (PARTITION BY bGrdCmpType.src_SubjectDefKey
                                  ORDER BY bGrdCmpType.src_Name
                                 ) AS Seq,                               -- int
			  
               'sa' AS ModUser,                                          -- varchar(50)
               GETDATE() AS ModDate,                                     -- datetime
               bGrdCmpType.trg_GrdComponentTypeId AS GrdComponentTypeId, -- uniqueidentifier
               0 AS Parameter,                                           -- int
               CASE
                   WHEN sWork.MinLabCnt = 0 THEN
                       1
                   ELSE
                       sWork.MinLabCnt
               END AS Number,                                            -- decimal
               NULL AS GrdPolicyId,                                      -- int
               0 AS Required,                                            -- bit
               0 AS MustPass,                                            -- bit
               NULL AS CreditsPerService,                                -- decimal
               bGrdCmpType.src_SubjectDefKey,
               GrdCmp.Total,
               GrdCmp.WgtCount
        INTO #GrdBkWgtDtl

        --Group the weights from Freedom to calculate weighted averages into percentages used by Advantage
        FROM
        (
            SELECT SUM(sWork.Weight) AS Total,
                   COUNT(sWork.Weight) AS WgtCount,
                   bGrdCmpType.src_SubjectDefKey
            FROM FreedomBridge.bridge.arGrdComponentTypes bGrdCmpType
                INNER JOIN Freedom..WORKUNIT_DEF sWork
                    ON sWork.RecordKey = bGrdCmpType.src_RecordKey
                       AND sWork.Name = bGrdCmpType.src_Name
                       AND bGrdCmpType.trg_CampusId = @CampusId
            GROUP BY bGrdCmpType.src_SubjectDefKey
        ) GrdCmp
            INNER JOIN FreedomBridge.bridge.arGrdComponentTypes bGrdCmpType
                ON bGrdCmpType.src_SubjectDefKey = GrdCmp.src_SubjectDefKey
                   AND bGrdCmpType.trg_CampusId = @CampusId
            INNER JOIN Freedom..WORKUNIT_DEF sWork
                ON sWork.RecordKey = bGrdCmpType.src_RecordKey
            INNER JOIN #MissingCourseWeights bGrdBkWgts
                ON bGrdBkWgts.src_RecordKey = bGrdCmpType.src_SubjectDefKey
                   AND bGrdBkWgts.Descrip NOT LIKE '%Attendance%'
            INNER JOIN FreedomAdvantage..syStatuses dStatus
                ON dStatus.Status = 'Active'
            LEFT JOIN FreedomAdvantage..arGrdBkWgtDetails dGrdBkWgtDtl
                ON dGrdBkWgtDtl.GrdComponentTypeId = bGrdCmpType.trg_GrdComponentTypeId
                   AND dGrdBkWgtDtl.Descrip = bGrdCmpType.src_Name
                   AND dGrdBkWgtDtl.Code = bGrdCmpType.src_RecordKey
                   AND dGrdBkWgtDtl.InstrGrdBkWgtId = bGrdBkWgts.InstrGrdBkWgtId;

				   

        ALTER TABLE FreedomAdvantage..arGrdBkWgtDetails DISABLE TRIGGER ALL;


        INSERT INTO FreedomAdvantage..arGrdBkWgtDetails
        SELECT tGrdBkWgtDtl.InstrGrdBkWgtDetailId,
               tGrdBkWgtDtl.InstrGrdBkWgtId,
               tGrdBkWgtDtl.Code,
               tGrdBkWgtDtl.Descrip,
               tGrdBkWgtDtl.Weight,
               tGrdBkWgtDtl.Seq,
               tGrdBkWgtDtl.ModUser,
               tGrdBkWgtDtl.ModDate,
               tGrdBkWgtDtl.GrdComponentTypeId,
               tGrdBkWgtDtl.Parameter,
               tGrdBkWgtDtl.Number,
               tGrdBkWgtDtl.GrdPolicyId,
               tGrdBkWgtDtl.Required,
               tGrdBkWgtDtl.MustPass,
               tGrdBkWgtDtl.CreditsPerService
        FROM #GrdBkWgtDtl tGrdBkWgtDtl
            LEFT JOIN FreedomAdvantage..arGrdBkWgtDetails dGrdBkWgtDtl
                ON dGrdBkWgtDtl.Descrip = tGrdBkWgtDtl.Descrip
                   AND dGrdBkWgtDtl.GrdComponentTypeId = tGrdBkWgtDtl.GrdComponentTypeId
                   AND dGrdBkWgtDtl.InstrGrdBkWgtId = tGrdBkWgtDtl.InstrGrdBkWgtId
        WHERE dGrdBkWgtDtl.InstrGrdBkWgtDetailId IS NULL;

        ALTER TABLE FreedomAdvantage..arGrdBkWgtDetails ENABLE TRIGGER ALL;

		INSERT INTO FreedomBridge.bridge.arGrdBkWgtDetails
            SELECT tGrdBkWgtDtl.InstrGrdBkWgtDetailId
                 , tGrdBkWgtDtl.InstrGrdBkWgtId
                 , tGrdBkWgtDtl.Descrip
                 , tGrdBkWgtDtl.Code
                 , @CampusId AS CampusId
				 , 0 AS InactiveDateRange
            FROM   #GrdBkWgtDtl tGrdBkWgtDtl
            LEFT JOIN FreedomBridge.bridge.arGrdBkWgtDetails bGrdBkWgtDtl ON bGrdBkWgtDtl.src_Name = tGrdBkWgtDtl.Descrip
                                                                             AND bGrdBkWgtDtl.src_RecordKey = tGrdBkWgtDtl.Code
                                                                             AND bGrdBkWgtDtl.trg_CampusId = @CampusId
                                                                             AND bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId = tGrdBkWgtDtl.InstrGrdBkWgtDetailId
            WHERE  bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId IS NULL;
			

        DROP TABLE #EffDates;
        DROP TABLE #CourseWeights;
        DROP TABLE #MissingCourseWeights;
        DROP TABLE #Settings;
		DROP TABLE #GrdBkWgtDtl;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_Cleanup_InsertMissingGradeBookWeightDetails]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

end;
GO
