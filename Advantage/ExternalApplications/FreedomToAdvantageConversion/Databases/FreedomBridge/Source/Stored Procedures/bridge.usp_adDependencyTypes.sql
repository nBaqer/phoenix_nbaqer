SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adDependencyTypes]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dDep.Descrip IS NULL THEN NEWID()
                    ELSE dDep.DependencyTypeId
               END AS DependencyTypeId                                             -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS Code -- varchar(50)
             , FLook.Description AS Descrip                                        -- varchar(50)
             , dStatus.StatusId AS StatusId                                        -- uniqueidentifier
             , dCampGrp.CampGrpId AS CampGrpId                                     -- uniqueidentifier
             , GETDATE() AS ModDate                                                -- datetime
             , 'sa' AS ModUser                                                     -- varchar(50)
             , FLook.ID
        INTO   #DepType
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adDependencyTypes dDep ON dDep.CampGrpId = dCampGrp.CampGrpId
                                                              AND dDep.Descrip = FLook.Description
        WHERE  FLook.LookupName = 'DependencyTypes';

        ALTER TABLE FreedomAdvantage..adDependencyTypes DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adDependencyTypes
                    SELECT tDep.DependencyTypeId
                         , tDep.Code
                         , tDep.Descrip
                         , tDep.StatusId
                         , tDep.CampGrpId
                         , tDep.ModDate
                         , tDep.ModUser
                    FROM   #DepType tDep
                    LEFT JOIN FreedomAdvantage..adDependencyTypes dDep ON dDep.CampGrpId = tDep.CampGrpId
                                                                          AND dDep.DependencyTypeId = tDep.DependencyTypeId
                    WHERE  dDep.Descrip IS NULL;

        ALTER TABLE FreedomAdvantage..adDependencyTypes ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adDependencyTypes')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adDependencyTypes
                    (
                        trg_DependencyTypeId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adDependencyTypes
                    SELECT tDep.DependencyTypeId
                         , tDep.Descrip
                         , tDep.ID
                    FROM   #DepType tDep
                    LEFT JOIN FreedomBridge.bridge.adDependencyTypes bDep ON bDep.trg_DependencyTypeId = tDep.DependencyTypeId
                    WHERE  bDep.trg_DependencyTypeId IS NULL;

        DROP TABLE #DepType;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adDependencyTypes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]
        COMMIT TRANSACTION;

    END;
GO
