SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-29-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syHolidays]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        DECLARE @ExecutionDate DATETIME2 = (
                                           SELECT TOP 1 ExecutionStart
                                           FROM   dbo.SourceDB
                                           WHERE  IsDisabled = 0
                                           );

        SELECT NEWID() AS HolidayId
              ,*
        INTO   #Holidays
        FROM   ( -- uniqueidentifier
               SELECT     DISTINCT sClose.RecordKey AS HolidayCode                                               -- varchar(12)
                                  ,dStatus.StatusId AS StatusId                                                  -- uniqueidentifier
                                  ,RTRIM(sClose.Descrip) + ' ' + CAST(@SchoolID AS VARCHAR(4)) AS HolidayDescrip -- varchar(50)
                                  ,dCampGrp.CampGrpId AS CampGrpId                                               -- uniqueidentifier
                                  ,sClose.Date AS HolidayStartDate                                               -- datetime
                                  ,CASE sClose.BegTime
                                        WHEN 0 THEN 1
                                        ELSE 0
                                   END AS AllDay                                                                 -- bit
                                  ,CASE sClose.BegTime
                                        WHEN 0 THEN NULL
                                        ELSE (
                                             SELECT TOP 1 TimeIntervalId
                                             FROM   FreedomAdvantage..cmTimeInterval
                                             WHERE  FreedomBridge.dbo.udf_GetHourMinutesOnly(TimeIntervalDescrip) = sClose.BegTime
                                             )
                                   END AS StartTimeId                                                            -- uniqueidentifier
                                  ,CASE sClose.EndTime
                                        WHEN 2359 THEN NULL
                                        ELSE (
                                             SELECT TOP 1 TimeIntervalId
                                             FROM   FreedomAdvantage..cmTimeInterval
                                             WHERE  FreedomBridge.dbo.udf_GetHourMinutesOnly(TimeIntervalDescrip) = sClose.EndTime
                                             )
                                   END AS EndTimeId                                                              -- uniqueidentifier
                                  ,'sa' AS ModUser                                                               -- varchar(50)
                                  ,GETDATE() AS ModDate                                                          -- datetime
                                  ,DATEADD(SECOND, -1,DATEADD(DAY,1,sClose.Date)) AS HolidayEndDate                                                 -- datetime

               FROM       Freedom..CLOSURES_FIL sClose
               INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN @ExecutionDate >= sClose.Date THEN 'Inactive'
                                                                                        ELSE 'Active'
                                                                                   END
               INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
               LEFT JOIN  FreedomAdvantage..syHolidays dHoliday ON dHoliday.CampGrpId = dCampGrp.CampGrpId
                                                                   AND dHoliday.HolidayStartDate = sClose.Date
               WHERE      sClose.Date IS NOT NULL
               ) Holiday;


        WITH resultGroups
        AS ( SELECT tHol.HolidayId
                   ,tHol.HolidayCode
                   ,tHol.StatusId
                   ,tHol.HolidayDescrip
                   ,tHol.CampGrpId
                   ,tHol.HolidayStartDate
                   ,tHol.AllDay
                   ,tHol.StartTimeId
                   ,tHol.EndTimeId
                   ,tHol.ModUser
                   ,tHol.ModDate
                   ,tHol.HolidayEndDate
                   ,ROW_NUMBER() OVER ( PARTITION BY tHol.StatusId
                                                    ,tHol.CampGrpId
                                                    ,tHol.HolidayStartDate
                                        ORDER BY tHol.HolidayStartDate
                                      ) AS [ROW NUMBER]
             FROM   #Holidays tHol )
        SELECT *
        INTO   #holidayFinalResult
        FROM   resultGroups
        WHERE  [ROW NUMBER] = 1;

        ALTER TABLE FreedomAdvantage..syHolidays DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syHolidays
                    SELECT tHoliday.HolidayId
                          ,tHoliday.HolidayCode
                          ,tHoliday.StatusId
                          ,tHoliday.HolidayDescrip
                          ,tHoliday.CampGrpId
                          ,tHoliday.HolidayStartDate
                          ,tHoliday.AllDay
                          ,tHoliday.StartTimeId
                          ,tHoliday.EndTimeId
                          ,tHoliday.ModUser
                          ,tHoliday.ModDate
                          ,tHoliday.HolidayEndDate
                    FROM   #holidayFinalResult tHoliday
                    WHERE  NOT EXISTS (
                                      SELECT 1
                                      FROM   FreedomAdvantage..syHolidays h
                                      WHERE  h.StatusId = tHoliday.StatusId
                                             AND h.CampGrpId = tHoliday.CampGrpId
                                             AND h.HolidayStartDate = tHoliday.HolidayStartDate
                                      );

        ALTER TABLE FreedomAdvantage..syHolidays ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syHolidays')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syHolidays
                    (
                        trg_HolidayId UNIQUEIDENTIFIER NULL
                       ,src_Descrip VARCHAR(50) NOT NULL
                       ,src_RecordKey INT NOT NULL
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       src_Descrip
                      ,src_RecordKey
                       )
                    );
            END;


        INSERT INTO FreedomBridge.bridge.syHolidays
                    SELECT DISTINCT HolidayId
                                   ,HolidayDescrip
                                   ,HolidayCode
                    FROM   #Holidays;

        DROP TABLE #Holidays;
        DROP TABLE #holidayFinalResult;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_syHolidays]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
