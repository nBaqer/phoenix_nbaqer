SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saRefunds]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT TOP 100 PERCENT ROW_NUMBER() OVER ( PARTITION BY AwardLogKey
                                                   ORDER BY sTran.RecordKey
                                                 ) AS SEQ
                              ,AwardLogKey
                              ,sTran.RecordKey
                              ,StudrecKey
                              ,DebitCredit
                              ,CAST(Amount / 100.00 AS MONEY) AS Amount
                              ,TransTypeKey
        INTO   #AwardTran
        FROM   Freedom..STUTRANS_FIL sTran
        WHERE  AwardLogKey > 0;

        SELECT      tAw.AwardLogKey
                   ,tAw.RecordKey
                   ,ReversedTran.RecordKey AS RefundedTransKey
        INTO        #AwardReverseTran
        FROM        #AwardTran tAw
        CROSS APPLY (
                    SELECT TOP 1 tAw1.RecordKey
                    FROM   #AwardTran tAw1
                    WHERE  tAw1.AwardLogKey = tAw.AwardLogKey
                           AND tAw1.SEQ < tAw.SEQ
                           AND tAw1.DebitCredit = 1
                    ) ReversedTran
        INNER JOIN  Freedom..TRANTYPE_FIL sTranType ON tAw.TransTypeKey = sTranType.RecordKey
        WHERE       FullDescrip LIKE '%refund%'
                    AND DebitCredit = 0;

        SELECT     bTran.trg_TransactionId AS TransactionId             -- uniqueidentifier
                  ,CASE WHEN sTran.TransTypeKey = 4 THEN 0
                        ELSE 1
                   END AS RefundTypeId                                  -- int
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS BankAcctId         -- uniqueidentifier
                  ,'SA' AS ModUser                                      -- varchar(50)
                  ,GETDATE() AS ModDate                                 -- datetime
                  ,dTran.FundSourceId AS FundSourceId                   -- uniqueidentifier
                  ,bAward.trg_StudentAwardId AS StudentAwardId          -- uniqueidentifier
                  ,0 AS IsInstCharge                                    -- bit
                  ,bAWScheduleRe.trg_AwardScheduleId AS AwardScheduleId -- uniqueidentifier
                  ,sTran.Amount / 100.00 AS RefundAmount                -- money
                  ,sTran.RecordKey
                  ,tRefund.RefundedTransKey AS RefundedTransKey
        INTO       #saRefunds
        FROM       bridge.saTransactions bTran
        INNER JOIN Freedom..STUTRANS_FIL sTran ON sTran.RecordKey = bTran.src_RecordKey
        INNER JOIN Freedom..TRANTYPE_FIL sTranType ON sTran.TransTypeKey = sTranType.RecordKey
        INNER JOIN Freedom..FALOGFIL sFALog ON sFALog.RecordKey = sTran.AwardLogKey
        LEFT JOIN  #AwardReverseTran tRefund ON tRefund.RecordKey = bTran.src_RecordKey
                                                AND bTran.trg_CampusId = @CampusId
        LEFT JOIN  bridge.saTransactions bTranReverse ON bTranReverse.src_RecordKey = tRefund.RefundedTransKey
                                                         AND bTranReverse.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage..saTransactions dTran ON dTran.TransactionId = bTran.trg_TransactionId
        --AND dTran.TransTypeId = 3 -- reversal type
        LEFT JOIN  bridge.faStudentAwards bAward ON bAward.src_RecordKey = sTran.AwardLogKey
                                                    AND bAward.trg_CampusId = @CampusId
        LEFT JOIN  bridge.faStudentAwardSchedule bAWScheduleRe ON bAWScheduleRe.src_TranRecKey = tRefund.RefundedTransKey
                                                                  AND bAWScheduleRe.trg_CampusId = @CampusId
        WHERE      bTran.trg_CampusId = @CampusId
                   AND FullDescrip LIKE '%refund%'
                   AND DebitCredit = 0;

        ALTER TABLE FreedomAdvantage..saRefunds DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..saRefunds
                    SELECT TransactionId
                          ,RefundTypeId
                          ,BankAcctId
                          ,ModUser
                          ,ModDate
                          ,FundSourceId
                          ,StudentAwardId
                          ,IsInstCharge
                          ,AwardScheduleId
                          ,RefundAmount
                    FROM   #saRefunds;
        ALTER TABLE FreedomAdvantage..saRefunds ENABLE TRIGGER ALL;

        INSERT INTO bridge.saRefunds
                    SELECT TransactionId AS trg_TransactionId
                          ,RecordKey AS src_RecordKey
                          ,RefundedTransKey AS src_RefundedTransKey
                          ,@CampusId AS trg_CampusId
                    FROM   #saRefunds;

        DROP TABLE #saRefunds;
        DROP TABLE #AwardReverseTran;
        DROP TABLE #AwardTran;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_saRefunds]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
