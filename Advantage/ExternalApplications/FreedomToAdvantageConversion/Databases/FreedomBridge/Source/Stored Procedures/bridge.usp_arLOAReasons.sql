SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-13-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arLOAReasons]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN Descrip IS NULL THEN NEWID()
                    ELSE LOAReasonId
               END AS LOAReasonId                   -- uniqueidentifier
             , bStatCode.src_ID AS Code             -- varchar(50)
             , bStatCode.src_Description AS Descrip -- varchar(50)
             , dCampGrp.CampGrpId AS CampGrpId      -- uniqueidentifier
             , GETDATE() AS ModDate                 -- datetime
             , 'sa' AS ModUser                      -- varchar(50)
             , dStat.StatusId AS StatusId           -- uniqueidentifier
        INTO   #LOA
        FROM   FreedomBridge.bridge.syStatusCodes bStatCode
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(@SchoolID AS VARCHAR(10))
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        LEFT JOIN FreedomAdvantage..arLOAReasons dLOA ON dLOA.Descrip = bStatCode.src_Description
                                                         AND dLOA.Code = CAST(bStatCode.src_ID AS VARCHAR(10))
        WHERE  bStatCode.src_ID = 3;

        ALTER TABLE FreedomAdvantage..arLOAReasons DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arLOAReasons
                    SELECT tLOA.LOAReasonId
                         , tLOA.Code
                         , tLOA.Descrip
                         , tLOA.CampGrpId
                         , tLOA.ModDate
                         , tLOA.ModUser
                         , tLOA.StatusId
                    FROM   #LOA tLOA
                    LEFT JOIN FreedomAdvantage..arLOAReasons dLOA ON dLOA.LOAReasonId = tLOA.LOAReasonId
                    WHERE  dLOA.LOAReasonId IS NULL;


        ALTER TABLE FreedomAdvantage..arLOAReasons ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arLOAReasons')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arLOAReasons
                    (
                        trg_LOAReasonId UNIQUEIDENTIFIER
                      , src_ID INT
                      , src_Description VARCHAR(50)
                      , trg_CampGrpId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_ID
                            , src_Description
                            , trg_CampGrpId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arLOAReasons
                    SELECT tLOA.LOAReasonId
                         , tLOA.Code
                         , tLOA.Descrip
                         , tLOA.CampGrpId AS CampGrpId
                         , @CampusId AS trg_CampusID
                    FROM   #LOA tLOA;

        DROP TABLE #LOA;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arLOAReasons]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
