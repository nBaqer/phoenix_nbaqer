SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-06-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arProgVerDef]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #ProgVerDef
        FROM   (
               SELECT     CASE WHEN dProgVerDef.ReqId IS NULL THEN NEWID()
                               ELSE dProgVerDef.ProgVerDefId
                          END AS ProgVerDefId                              -- uniqueidentifier
                         ,bProgVer.trg_PrgVerId AS PrgVerId                -- uniqueidentifier
                         ,bReqs.trg_ReqId AS ReqId                         -- uniqueidentifier
                         ,bReqs.src_Name
                         ,ROW_NUMBER() OVER ( PARTITION BY sSub.CourseDefKey
                                              ORDER BY sSub.CourseDefKey
                                            ) AS ReqSeq                    -- int
                         ,1 AS IsRequired                                  -- bit
                         ,0 AS Cnt                                         -- smallint
                         ,NULL AS Hours                                    -- decimal
                         ,0 AS TrkForCompletion                            -- bit
                         ,sSub.Credits AS Credits                          -- decimal
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS GrdSysDetailId -- uniqueidentifier
                         ,'sa' AS ModUser                                  -- varchar(50)
                         ,GETDATE() AS ModDate                             -- datetime
                         ,NULL AS TermNo                                   -- int
                         ,sSub.RecordKey AS FreedomSubjectDefRecordKey     --INT
						 ,sSub.Weight AS CourseWeight
               FROM       Freedom..SUBJECT_DEF sSub
               INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sSub.CourseDefKey
               INNER JOIN Freedom..PROGSTUDY_DEF sProg ON sProg.RecordKey = sCourse.ParentKey
               INNER JOIN FreedomBridge.bridge.arReqs bReqs ON bReqs.src_RecordKey = sSub.RecordKey
                                                               --AND bReqs.src_Name = sSub.Name
                                                               AND bReqs.trg_CampusId = @CampusId
               INNER JOIN FreedomBridge.bridge.arPrgVersions bProgVer ON bProgVer.src_RecordKey = sSub.CourseDefKey
                                                                         AND bProgVer.trg_CampusId = @CampusId
               LEFT JOIN  FreedomAdvantage..arProgVerDef dProgVerDef ON dProgVerDef.PrgVerId = bProgVer.trg_PrgVerId
                                                                        AND dProgVerDef.ReqId = bReqs.trg_ReqId
               UNION ALL
               SELECT     CASE WHEN dProgVerDef.ReqId IS NULL THEN NEWID()
                               ELSE dProgVerDef.ProgVerDefId
                          END AS ProgVerDefId                              -- uniqueidentifier
                         ,bProgVer.trg_PrgVerId AS PrgVerId                -- uniqueidentifier
                         ,bReqs.trg_ReqId AS ReqId                         -- uniqueidentifier
                         ,bReqs.src_Name
                         ,ROW_NUMBER() OVER ( PARTITION BY sCourse.RecordKey
                                              ORDER BY sCourse.RecordKey
                                            ) AS ReqSeq                    -- int
                         ,1 AS IsRequired                                  -- bit
                         ,0 AS Cnt                                         -- smallint
                         ,NULL AS Hours                                    -- decimal
                         ,0 AS TrkForCompletion                            -- bit
                         ,0 AS Credits                                     -- decimal
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS GrdSysDetailId -- uniqueidentifier
                         ,'sa' AS ModUser                                  -- varchar(50)
                         ,GETDATE() AS ModDate                             -- datetime
                         ,NULL AS TermNo                                   -- int
                         ,sSub.RecordKey AS FreedomSubjectDefRecordKey     --INT
						 ,sSub.Weight AS CourseWeight
               FROM       Freedom..SUBJECT_DEF sSub
               INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sSub.CourseDefKey
               INNER JOIN Freedom..PROGSTUDY_DEF sProg ON sProg.RecordKey = sCourse.ParentKey
               INNER JOIN FreedomBridge.bridge.arReqs bReqs ON bReqs.src_CourseDefKey = sCourse.RecordKey
                                                               AND bReqs.src_Name = sSub.Name + ' Attendance'
                                                               AND bReqs.trg_CampusId = @CampusId
               INNER JOIN FreedomBridge.bridge.arPrgVersions bProgVer ON bProgVer.src_RecordKey = sCourse.RecordKey
                                                                         AND bProgVer.trg_CampusId = @CampusId
               LEFT JOIN  FreedomAdvantage..arProgVerDef dProgVerDef ON dProgVerDef.PrgVerId = bProgVer.trg_PrgVerId
                                                                        AND dProgVerDef.ReqId = bReqs.trg_ReqId
               ) ProgVerDef;

        ALTER TABLE FreedomAdvantage..arProgVerDef DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arProgVerDef (
                                                   ProgVerDefId
                                                  ,PrgVerId
                                                  ,ReqId
                                                  ,ReqSeq
                                                  ,IsRequired
                                                  ,Cnt
                                                  ,Hours
                                                  ,TrkForCompletion
                                                  ,Credits
                                                  ,GrdSysDetailId
                                                  ,ModUser
                                                  ,ModDate
                                                  ,TermNo
                                                  ,CourseWeight
                                                   )
                    SELECT    tProgVerDef.ProgVerDefId
                             ,tProgVerDef.PrgVerId
                             ,tProgVerDef.ReqId
                             ,tProgVerDef.ReqSeq
                             ,tProgVerDef.IsRequired
                             ,tProgVerDef.Cnt
                             ,tProgVerDef.Hours
                             ,tProgVerDef.TrkForCompletion
                             ,tProgVerDef.Credits
                             ,tProgVerDef.GrdSysDetailId
                             ,tProgVerDef.ModUser
                             ,tProgVerDef.ModDate
                             ,tProgVerDef.TermNo
                             ,tProgVerDef.CourseWeight
                    FROM      #ProgVerDef tProgVerDef
                    LEFT JOIN FreedomAdvantage..arProgVerDef dProgVerDef ON dProgVerDef.ReqId = tProgVerDef.ReqId
                                                                            AND dProgVerDef.PrgVerId = tProgVerDef.PrgVerId
                    WHERE     dProgVerDef.ProgVerDefId IS NULL;

        ALTER TABLE FreedomAdvantage..arProgVerDef ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.arProgVerDef
                    SELECT    tProgVerDef.ProgVerDefId
                             ,tProgVerDef.PrgVerId
                             ,tProgVerDef.ReqId
                             ,@CampusId AS CampusId
                             ,tProgVerDef.FreedomSubjectDefRecordKey
                    FROM      #ProgVerDef tProgVerDef
                    LEFT JOIN FreedomBridge.bridge.arProgVerDef bProgVerDef ON bProgVerDef.trg_PrgVerId = tProgVerDef.PrgVerId
                                                                               AND bProgVerDef.trg_ReqId = tProgVerDef.ReqId
                                                                               AND bProgVerDef.trg_CampusId = @CampusId
                    WHERE     bProgVerDef.trg_ProgVerDefId IS NULL;

        DROP TABLE #ProgVerDef;


        UPDATE     courses
        SET        courses.Descrip = RTRIM(LTRIM(courses.Descrip)) + ' [' + CoursesByProgramVersion.PrgVerDescrip + ']'
        FROM       FreedomAdvantage.dbo.arReqs courses
        INNER JOIN (
                   SELECT     programVersion.PrgVerId
                             ,programVersion.PrgVerDescrip
                             ,courses.Descrip
                             ,courses.ReqId
                   FROM       FreedomAdvantage.dbo.arProgVerDef programVersionDefinition
                   INNER JOIN FreedomAdvantage.dbo.arPrgVersions programVersion ON programVersion.PrgVerId = programVersionDefinition.PrgVerId
                   INNER JOIN FreedomAdvantage.dbo.arReqs courses ON courses.ReqId = programVersionDefinition.ReqId
                   ) AS CoursesByProgramVersion ON CoursesByProgramVersion.ReqId = courses.ReqId;


		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arProgVerDef]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
