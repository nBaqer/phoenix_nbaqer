SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_UpdateConversionDataSetup]
AS
    BEGIN

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@ActiveStatudId UNIQUEIDENTIFIER
               ,@InactiveStatudId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';


        SELECT @ActiveStatudId = Value
        FROM   #Settings
        WHERE  Entity = 'ActiveStatusId';


        SET @InactiveStatudId = (
                                SELECT TOP 1 StatusId Value
                                FROM   FreedomAdvantage..syStatuses
                                WHERE  StatusCode = 'I'
                                );

        UPDATE AC
        SET    AC.Code = 'O'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Code = 'HM';

        UPDATE AC
        SET    AC.Code = 'E'
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Code = 'HSD';

        UPDATE AC
        SET    AC.Code = 'H'
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Code = 'HSG';


		-------------------------------------------------------------------------------------------------------------
		----AD-15974 Admission criteria mapping for AFA
		-------------------------------------------------------------------------------------------------------------
		  UPDATE AC
        SET    AC.Code = 'H'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'High School Diploma';

			  UPDATE AC
        SET    AC.Code = 'H'
              ,AC.StatusId = @InactiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'Counsel/Remedial';

				  UPDATE AC
        SET    AC.Code = 'H'
              ,AC.StatusId = @InactiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'Degree';

				  UPDATE AC
        SET    AC.Code = 'H'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'High school grad';

		UPDATE AC
        SET    AC.Code = 'A'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'ATB';

				UPDATE AC
        SET    AC.Code = 'A'
              ,AC.StatusId = @InactiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'SEC (formerly ATB)';

		
				UPDATE AC
        SET    AC.Code = 'E'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'GED';

		
		
				UPDATE AC
        SET    AC.Code = 'O'
              ,AC.StatusId = @ActiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'Home School';

			UPDATE AC
        SET    AC.Code = 'E'
              ,AC.StatusId = @InactiveStatudId
        FROM   FreedomAdvantage..adAdminCriteria AC
        WHERE  AC.Descrip = 'GED preparation';
    END;

GO
