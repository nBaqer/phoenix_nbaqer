SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_plEmployers]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
        DECLARE @CampusGroupAllId UNIQUEIDENTIFIER;
        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;
        DECLARE @ModUser VARCHAR(50) = 'SA';
        DECLARE @ModDate DATETIME = GETDATE();
        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';


        SET @ActiveStatusId = (
                              SELECT   TOP ( 1 ) StatusId
                              FROM     FreedomAdvantage..syStatuses
                              WHERE    StatusCode = 'A'
                              ORDER BY StatusCode
                              );
        SET @CampusGroupAllId = (
                             SELECT   TOP 1 CampGrpId
                             FROM     FreedomAdvantage..syCampGrps
                             WHERE    CampGrpCode = 'All'
                             ORDER BY CampGrpCode
                             );

        IF NOT (
               SELECT COUNT(*)
               FROM   FreedomAdvantage..plIndustries WHERE CampGrpId = @CampusGroupAllId
               ) >= 1
            BEGIN
                INSERT INTO FreedomAdvantage..[plIndustries] (
                                                             [IndustryCode]
                                                            ,[StatusId]
                                                            ,[IndustryDescrip]
                                                            ,[CampGrpId]
                                                            ,[ModUser]
                                                            ,[ModDate]
                                                             )
                VALUES ( 'N', @ActiveStatusId, 'Not industry related', @CampusGroupAllId, @ModUser, @ModDate )
                      ,( 'S', @ActiveStatusId, 'Self employed in industry', @CampusGroupAllId, @ModUser, @ModDate )
                      ,( 'X', @ActiveStatusId, 'Requested not to be placed', @CampusGroupAllId, @ModUser, @ModDate )
                      ,( 'Y', @ActiveStatusId, 'Industry related', @CampusGroupAllId, @ModUser, @ModDate );
            END;





        SELECT     NEWID() AS EmployerId                                                                   -- uniqueidentifier
                  ,CAST(@SchoolID AS VARCHAR(4)) + '_' + CAST(sEmp.ParentRecordKey AS VARCHAR(10)) AS Code -- varchar(50)
                  ,dStatus.StatusId AS StatusId                                                            -- uniqueidentifier
                  ,CASE WHEN ROW_NUMBER() OVER ( PARTITION BY sEmp.EmpName
                                                 ORDER BY sEmp.EmpName
                                               ) > 1 THEN LTRIM(RTRIM(sEmp.EmpName)) + ' ' + CAST(ROW_NUMBER() OVER ( PARTITION BY sEmp.EmpName
                                                                                                                      ORDER BY sEmp.EmpName
                                                                                                                    ) AS VARCHAR(4))
                        ELSE sEmp.EmpName
                   END AS EmployerDescrip                                                                  -- varchar(50)
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ParentId                                              -- uniqueidentifier
                  ,dCampGrp.CampGrpId AS CampGrpId                                                         -- uniqueidentifier
                  ,sEmp.EmpAdrsLine1 AS Address1                                                           -- varchar(80)
                  ,NULL AS Address2                                                                        -- varchar(50)
                  ,sEmp.EmpAdrsCity AS City                                                                -- varchar(80)
                  ,dState.StateId AS StateId                                                               -- uniqueidentifier
                  ,sEmp.PrivEmpAdrsZip AS Zip                                                              -- varchar(50)
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS CountyId                                              -- uniqueidentifier
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS LocationId                                            -- uniqueidentifier
                  ,sGrad.EmpPhNum AS Phone                                                                 -- varchar(50)
                  ,NULL AS Fax                                                                             -- varchar(50)
                  ,NULL AS Email                                                                           -- varchar(50)
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS FeeId                                                 -- uniqueidentifier
                  ,ind.IndustryId AS IndustryId                                                            -- uniqueidentifier --not sure what is InIndustryFlag with values N,S,X,Y
                  ,'SA' AS ModUser                                                                         -- varchar(50)
                  ,GETDATE() AS ModDate                                                                    -- datetime
                  ,0 AS GroupName                                                                          -- bit
                  ,dCountry.CountryId AS CountryId                                                         -- uniqueidentifier
                  ,0 AS ForeignPhone                                                                       -- bit
                  ,0 AS ForeignFax                                                                         -- bit
                  ,0 AS ForeignZip                                                                         -- bit
                  ,NULL AS OtherState                                                                      -- varchar(50)
                  ,sEmp.ParentRecordKey
                  ,sChildGrad.RecordKey AS ChildRecordKey
        INTO       #plEmployers
        FROM       (
                   SELECT   EmpName
                           ,EmpAdrsLine1
                           ,EmpAdrsCity
                           ,EmpAdrsState
                           ,PrivEmpAdrsZip
                           ,MIN(RecordKey) AS ParentRecordKey
                   FROM     Freedom.dbo.GRADPLAC_FIL sGrad1
                   WHERE    EmpName NOT IN ( '' )
                   GROUP BY EmpName
                           ,EmpAdrsLine1
                           ,EmpAdrsCity
                           ,EmpAdrsState
                           ,PrivEmpAdrsZip
                   ) sEmp
        INNER JOIN Freedom.dbo.GRADPLAC_FIL sGrad ON sGrad.RecordKey = sEmp.ParentRecordKey
        INNER JOIN Freedom.dbo.GRADPLAC_FIL sChildGrad ON sChildGrad.EmpName = sEmp.EmpName
                                                          AND sChildGrad.EmpAdrsLine1 = sEmp.EmpAdrsLine1
                                                          AND sChildGrad.EmpAdrsCity = sEmp.EmpAdrsCity
                                                          AND sChildGrad.EmpAdrsState = sEmp.EmpAdrsState
                                                          AND sChildGrad.PrivEmpAdrsZip = sEmp.PrivEmpAdrsZip
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN  FreedomAdvantage..syStates dState ON dState.StateCode = sEmp.EmpAdrsState
        INNER JOIN FreedomAdvantage..adCountries dCountry ON dCountry.CountryCode = 'USA'
        LEFT JOIN  FreedomAdvantage..plIndustries ind ON ind.IndustryCode = sGrad.InIndustryFlag AND ind.CampGrpId = @CampusGroupAllId
        LEFT JOIN  bridge.plEmployers bIgnore ON bIgnore.src_ParentRecordKey = sEmp.ParentRecordKey
                                                 AND bIgnore.src_ChildRecordKey = sChildGrad.RecordKey
                                                 AND bIgnore.trg_CampusId = @CampusId
        WHERE      bIgnore.src_ParentRecordKey IS NULL;



        ALTER TABLE FreedomAdvantage..plEmployers DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..plEmployers
                    SELECT EmployerId
                          ,Code
                          ,StatusId
                          ,EmployerDescrip
                          ,ParentId
                          ,CampGrpId
                          ,Address1
                          ,Address2
                          ,City
                          ,StateId
                          ,Zip
                          ,CountyId
                          ,LocationId
                          ,Phone
                          ,Fax
                          ,Email
                          ,FeeId
                          ,IndustryId
                          ,ModUser
                          ,ModDate
                          ,GroupName
                          ,CountryId
                          ,ForeignPhone
                          ,ForeignFax
                          ,ForeignZip
                          ,OtherState
                    FROM   #plEmployers;
        ALTER TABLE FreedomAdvantage..plEmployers ENABLE TRIGGER ALL;

        INSERT INTO bridge.plEmployers
                    SELECT EmployerId AS trg_EmployerId
                          ,ParentRecordKey AS src_ParentRecordKey
                          ,ChildRecordKey AS scr_ChildRecordKey
                          ,@CampusId AS trg_CampusId
                    FROM   #plEmployers;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_plEmployers]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
