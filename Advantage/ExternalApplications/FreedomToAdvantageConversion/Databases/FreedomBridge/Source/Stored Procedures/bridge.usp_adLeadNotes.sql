SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-28-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adLeadNotes]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        /****** Connect Notes together using the Freedom NextRec Key based on the Comment Key from Prospect.fil *******/

        IF EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'Freedom.dbo.PROSPECT_FIL_Cmt')
                             AND type IN ( N'U' )
                  )
            BEGIN;

                WITH CTE_Sap
                AS ( SELECT     C.RecNum
                              , P.RecordKey AS ParentKey
                              , C.NextRec
                              , C.Text
                              , C.Time
                              , C.UserId
                              , C.Date
                     FROM       Freedom..PROSPECT_FIL_Cmt C
                     INNER JOIN Freedom..PROSPECT_FIL P ON P.CommentRecKey = C.RecNum
                     UNION ALL
                     SELECT     P_Child.RecNum
                              , P_Parent.ParentKey
                              , P_Child.NextRec
                              , P_Child.Text
                              , P_Child.Time
                              , P_Child.UserId
                              , P_Child.Date
                     FROM       Freedom..PROSPECT_FIL_Cmt P_Child
                     INNER JOIN CTE_Sap P_Parent ON P_Parent.NextRec = P_Child.RecNum )
                SELECT   *
                INTO     #Comment
                FROM     CTE_Sap
                ORDER BY CTE_Sap.ParentKey
                       , CTE_Sap.RecNum;

                /****** Merge comments together based on date of comment since Freedom could only hold a certain amount of characters ******/

                SELECT     sProCmt.RecNum
                         , sProCmt.ParentKey
                         , STUFF((
                                     SELECT   CAST(Text AS VARCHAR(MAX))
                                     FROM     #Comment sProCmt1
                                     WHERE    (
                                                  sProCmt1.ParentKey = sProCmt.ParentKey
                                                  AND sProCmt1.Time = sProCmt.Time
                                                  AND sProCmt1.Date = sProCmt.Date
                                              )
                                     ORDER BY sProCmt1.ParentKey DESC
                                     FOR XML PATH(''), TYPE
                                 ).value('.', 'VARCHAR(MAX)')
                               , 1
                               , 0
                               , ''
                                ) AS ProspectNoteDescrip
                         , sProCmt.Time
                         , sProCmt.UserId
                         , sProCmt.Date
                         , CAST(CAST(CAST(sProCmt.Date AS DATE) AS VARCHAR(10)) + ' ' + CASE WHEN LEN(sProCmt.Time) = 3 THEN LEFT(sProCmt.Time, 1)
                                                                                             ELSE LEFT(sProCmt.Time, 2)
                                                                                        END + ':' + RIGHT(sProCmt.Time, 2) AS DATETIME) AS CreatedDate
                INTO       #NoteStage1
                FROM       Freedom..PROSPECT_FIL sPro
                INNER JOIN #Comment sProCmt ON sProCmt.ParentKey = sPro.RecordKey
                WHERE      sProCmt.Text IS NOT NULL
                           OR sProCmt.Text != ''
                GROUP BY   sProCmt.RecNum
                         , sProCmt.ParentKey
                         , sProCmt.Time
                         , sProCmt.UserId
                         , sProCmt.Date
                ORDER BY   ParentKey;

                /****** Remove duplicate comments after merge of comments are complete ******/

                SELECT CASE WHEN a.LeadNoteDescrip IS NULL THEN NEWID()
                            ELSE a.LeadNoteId
                       END AS LeadNoteId                        -- uniqueidentifier
                     , a.syStatusId AS StatusId                 -- uniqueidentifier
                     , a.trg_LeadId AS LeadId                   -- uniqueidentifier
                     , a.ProspectNoteDescrip AS LeadNoteDescrip -- varchar(2000)
                     , 'AD' AS ModuleCode                       -- char(2)
                     , a.trg_UserId AS UserId                   -- uniqueidentifier
                     , a.CreatedDate AS CreatedDate             -- datetime
                     , 'sa' AS ModUser                          -- varchar(50)
                     , GETDATE() AS ModDate                     -- datetime
                     , a.RecNum AS RecNum
                INTO   #LeadNotes
                FROM   (
                           SELECT     sNote.ProspectNoteDescrip
                                    , sNote.Time
                                    , sNote.UserId
                                    , sNote.Date
                                    , sNote.CreatedDate
                                    , sNote.RecNum
                                    , dStat.StatusId AS syStatusId
                                    , dStat.Status
                                    , dStat.StatusCode
                                    , bLead.trg_LeadId
                                    , bLead.src_StuRecordKey
                                    , bLead.src_StuSSN
                                    , bLead.src_ProRecordKey
                                    , bLead.TotalRank
                                    , bLead.LocalRank
                                    , bLead.TableName
                                    , bLead.trg_CampusId
                                    , bUser.trg_UserId
                                    , bUser.trg_CampusId AS UserCampusId
                                    , bUser.src_UserId
                                    , bUser.TableName AS UserTableName
                                    , dLeadNotes.LeadNoteId
                                    , dLeadNotes.StatusId
                                    , dLeadNotes.LeadId
                                    , dLeadNotes.LeadNoteDescrip
                                    , dLeadNotes.ModuleCode
                                    , dLeadNotes.UserId AS LeadUserId
                                    , dLeadNotes.CreatedDate AS LeadCreatedDate
                                    , dLeadNotes.ModUser
                                    , dLeadNotes.ModDate
                                    , ROW_NUMBER() OVER ( PARTITION BY sNote.ParentKey
                                                                     , sNote.Time
                                                                     , sNote.UserId
                                                                     , sNote.Date
                                                          ORDER BY sNote.ParentKey
                                                        ) AS rownum
                           FROM       #NoteStage1 sNote
                           INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
                           INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_ProRecordKey = sNote.ParentKey
                                                                            AND bLead.trg_CampusId = @CampusId
                           INNER JOIN FreedomBridge.bridge.syUsers bUser ON bUser.src_UserId = sNote.UserId
                                                                            AND bUser.TableName = 'SHIBBOLETH_FIL'
                                                                            AND bUser.trg_CampusId = @CampusId
                           LEFT JOIN  FreedomAdvantage..adLeadNotes dLeadNotes ON dLeadNotes.LeadNoteDescrip = sNote.ProspectNoteDescrip
                       ) a
                WHERE  a.rownum = 1
                       AND (
                               a.ProspectNoteDescrip IS NOT NULL
                               OR a.ProspectNoteDescrip != ''
                           );

                /****** Look for duplicate comments for students that do have the same Prospect Key as a completely different student due to no validation in Freedom ******/

                SELECT     a.ParentKey AS ParentKey
                         , sStud.RecordKey AS RecordKey
                         , LTRIM(RTRIM(sStud.FirstName)) AS FirstName
                         , LTRIM(RTRIM(sStud.PrivLastName)) AS LastName
                         , sStud.MidInitial AS MidInitial
                         , sStud.StuSSN AS StuSSN
                         , sStud.BirthDate AS BirthDate
                         , sStud.PhoneNumAreaCode1 + sStud.PhoneNumPrefix1 + sStud.PhoneNumBody1 AS PhoneNum
                INTO       #DupLeads
                FROM       (
                               SELECT     dLead.trg_LeadId
                                        , dLead.trg_CampusId
                                        , dLead.src_StuSSN
                                        , tNote.ParentKey
                                        , ROW_NUMBER() OVER ( PARTITION BY tNote.ParentKey
                                                              ORDER BY tNote.ParentKey
                                                            ) rownum
                               FROM       #NoteStage1 tNote
                               INNER JOIN Freedom..PROSPECT_FIL sPro ON sPro.RecordKey = tNote.ParentKey
                               INNER JOIN FreedomBridge.bridge.adLeads dLead ON dLead.src_ProRecordKey = sPro.RecordKey
                               GROUP BY   dLead.trg_LeadId
                                        , dLead.trg_CampusId
                                        , dLead.src_StuSSN
                                        , tNote.ParentKey
                           ) a
                INNER JOIN Freedom..STUDREC sStud ON sStud.ProspectKey = a.ParentKey
                WHERE      a.rownum > 1
                ORDER BY   a.ParentKey;

                ALTER TABLE FreedomAdvantage..adLeadNotes DISABLE TRIGGER ALL;

                INSERT INTO FreedomAdvantage..adLeadNotes
                            SELECT    tLeadNotes.LeadNoteId AS LeadNoteId           -- uniqueidentifier
                                    , tLeadNotes.StatusId AS StatusId               -- uniqueidentifier
                                    , tLeadNotes.LeadId AS LeadId                   -- uniqueidentifier
                                    , tLeadNotes.LeadNoteDescrip AS LeadNoteDescrip -- varchar(2000)
                                    , tLeadNotes.ModuleCode AS ModuleCode           -- char(2)
                                    , tLeadNotes.UserId AS UserId                   -- uniqueidentifier
                                    , tLeadNotes.CreatedDate AS CreatedDate         -- datetime
                                    , tLeadNotes.ModUser AS ModUser                 -- varchar(50)
                                    , tLeadNotes.ModDate AS ModDate                 -- datetime
                            FROM      #LeadNotes tLeadNotes
                            LEFT JOIN FreedomAdvantage..adLeadNotes dLeadNotes ON dLeadNotes.LeadNoteDescrip = tLeadNotes.LeadNoteDescrip
                            WHERE     dLeadNotes.LeadNoteId IS NULL;

                ALTER TABLE FreedomAdvantage..adLeadNotes ENABLE TRIGGER ALL;

                -- Create bridge table if not already created in the bridge

                IF NOT EXISTS (
                                  SELECT *
                                  FROM   sys.objects
                                  WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adLeadNotes')
                                         AND type IN ( N'U' )
                              )
                    BEGIN
                        CREATE TABLE FreedomBridge.bridge.adLeadNotes
                            (
                                trg_LeadNoteId UNIQUEIDENTIFIER NULL
                              , src_RecNum INT
                              , src_Text VARCHAR(MAX) NOT NULL
                              , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                              ,
                              PRIMARY KEY CLUSTERED
                              (
                                  src_RecNum
                                , trg_CampusId
                              )
                            );
                    END;

                INSERT INTO FreedomBridge.bridge.adLeadNotes
                            SELECT    tLeadNotes.LeadNoteId
                                    , tLeadNotes.RecNum
                                    , tLeadNotes.LeadNoteDescrip
                                    , @CampusId
                            FROM      #LeadNotes tLeadNotes
                            LEFT JOIN FreedomBridge.bridge.adLeadNotes bLeadNotes ON bLeadNotes.src_Text = tLeadNotes.LeadNoteDescrip
                            WHERE     bLeadNotes.trg_LeadNoteId IS NULL;

                -- Create duplicate table if not already created in the bridge

                IF NOT EXISTS (
                                  SELECT *
                                  FROM   sys.objects
                                  WHERE  object_id = OBJECT_ID(N'FreedomBridge.failure.adLeadNotes')
                                         AND type IN ( N'U' )
                              )
                    BEGIN
                        CREATE TABLE FreedomBridge.failure.adLeadNotes
                            (
                                src_ParentKey INT
                              , src_RecordKey INT
                              , src_FirstName VARCHAR(25)
                              , src_PrivLastName VARCHAR(25)
                              , src_MidInitial VARCHAR(1)
                              , src_StuSSN VARCHAR(15)
                              , src_BirthDate DATE
                              , src_PhoneNum VARCHAR(15)
                              , trg_CampusId UNIQUEIDENTIFIER
                              ,
                              PRIMARY KEY CLUSTERED
                              (
                                  src_ParentKey
                                , src_RecordKey
                                , trg_CampusId
                              )
                            );
                    END;

                INSERT INTO FreedomBridge.failure.adLeadNotes
                            SELECT    tDupLead.ParentKey
                                    , tDupLead.RecordKey
                                    , tDupLead.FirstName
                                    , tDupLead.LastName
                                    , tDupLead.MidInitial
                                    , tDupLead.StuSSN
                                    , tDupLead.BirthDate
                                    , tDupLead.PhoneNum
                                    , @CampusId
                            FROM      #DupLeads tDupLead
                            LEFT JOIN FreedomBridge.failure.adLeadNotes bLeadNotes ON bLeadNotes.src_RecordKey = tDupLead.RecordKey
                                                                                      AND bLeadNotes.src_ParentKey = tDupLead.ParentKey
                                                                                      AND bLeadNotes.trg_CampusId = @CampusId
                            WHERE     bLeadNotes.src_RecordKey IS NULL;

                DROP TABLE #Comment;
                DROP TABLE #NoteStage1;
                DROP TABLE #LeadNotes;

            END;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adLeadNotes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
