SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arAttendTypes]
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    BEGIN TRANSACTION;

    IF NOT EXISTS
    (
        SELECT TOP 1
               *
        FROM FreedomAdvantage.dbo.arAttendTypes
        WHERE Descrip = 'Three quarter time'
    )
    BEGIN
        INSERT INTO FreedomAdvantage..arAttendTypes
        SELECT NEWID() AS AttendTypeId,         -- uniqueidentifier
               '3/4' AS Code,                   -- varchar(50)
               'Three quarter time' AS Descrip, -- varchar(50)
               dStatus.StatusId AS StatusId,    -- uniqueidentifier
               dCampGrp.CampGrpId AS CampGrpId, -- uniqueidentifier
               GETDATE() AS ModDate,            -- datetime
               'SA' AS ModUser,                 -- varchar(50)
               NULL AS IPEDSSequence,           -- int
               NULL AS IPEDSValue,              -- int
               NULL AS GESequence,              -- INT NULL
               NULL AS GERptAgencyFldValId      -- INT NULL
        FROM FreedomAdvantage..syStatuses dStatus
            INNER JOIN FreedomAdvantage..syCampGrps dCampGrp
                ON dCampGrp.CampGrpCode = 'ALL'
            LEFT JOIN bridge.arAttendTypes bIgnore
                ON bIgnore.src_Description = 'Three quarter time'
        WHERE dStatus.Status = 'Active'
              AND bIgnore.src_ID IS NULL;
    END;


    SELECT dAType.AttendTypeId, --uniqueidentifier
           FL.ID,
           FL.Description,
           FL.LookUpID
    INTO #arAttendTypes
    FROM stage.FLookUp FL
        LEFT JOIN FreedomAdvantage..arAttendTypes dAType
            ON FL.Description = CASE
                                    WHEN dAType.Descrip = 'Full time' THEN
                                        'Full Time'
                                    WHEN dAType.Descrip = 'Full-time' THEN
                                        'Full Time'
                                    WHEN dAType.Descrip = 'Part time' THEN
                                        'Part Time'
                                    WHEN dAType.Descrip = 'Part-time' THEN
                                        'Part Time'
                                    WHEN dAType.Descrip = 'Three quarter time' THEN
                                        'Three quarter time'
                                END
        LEFT JOIN bridge.arAttendTypes bIgnore
            ON bIgnore.src_Description = FL.Description
    WHERE LookupName = 'AttendTypes'
          AND bIgnore.src_ID IS NULL;


    INSERT INTO FreedomBridge.bridge.arAttendTypes
    SELECT AttendTypeId,
           ID,
           Description,
           LookUpID
    FROM #arAttendTypes ara
    WHERE NOT EXISTS
    (
        SELECT *
        FROM FreedomBridge.bridge.arAttendTypes
        WHERE Description = ara.Description
    );

	
    DROP TABLE #arAttendTypes;

    INSERT INTO [dbo].[storedProceduresRun]
    SELECT NEWID(),
           '[usp_arAttendTypes]',
           GETDATE(),
           NULL,
           ISNULL(MAX(orderRun), 0) + 1
    FROM [dbo].[storedProceduresRun];

    COMMIT TRANSACTION;

END;
GO
