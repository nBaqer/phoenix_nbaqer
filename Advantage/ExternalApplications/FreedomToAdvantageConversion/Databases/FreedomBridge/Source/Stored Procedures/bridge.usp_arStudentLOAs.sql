SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-13-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arStudentLOAs]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT   *
        INTO     #Stat
        FROM     Freedom..STATHIST_FIL sStat
        WHERE    sStat.CurStatusCode = 3
        ORDER BY sStat.StudrecKey;

        SELECT   s1.StudrecKey
               , s1.RecordKey AS StartRecord
               , s1.PrvStatusCode
               , s1.CurStatusCode
               , s1.EffectiveDate AS StartDate
               , ISNULL(s2.EffectiveDate, sStud.EndLeaveDate) AS EndDate
               , s1.PostingDate AS LOARequestDate
               , s1.PostingDate AS ModDate
               , CASE WHEN s2.RecordKey IS NULL THEN s1.RecordKey
                      ELSE NULL
                 END AS EndRecord
               , s2.EffectiveDate AS LOAReturnDate
        INTO     #StatHist
        FROM     #Stat s1
        INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = s1.StudrecKey
        INNER JOIN bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                AND bStud.trg_CampusId = @CampusId
        LEFT JOIN Freedom..STATHIST_FIL s2 ON s2.RecordKey = s1.NextRec
        ORDER BY s1.StudrecKey;

        SELECT   CASE WHEN dStuLOA.StuEnrollId IS NULL THEN NEWID()
                      ELSE dStuLOA.StudentLOAId
                 END AS StudentLOAId                                            -- uniqueidentifier
              , bStuEnroll.trg_StuEnrollId AS StuEnrollId  
			   --,dStuLOA.StuEnrollId AS StuEnrollId                -- uniqueidentifier
               , tStatHist.StartDate AS StartDate                               -- datetime
               , tStatHist.EndDate AS EndDate                                   -- datetime
               , bLOA.trg_LOAReasonId AS LOAReasonId                            -- uniqueidentifier
               , tStatHist.ModDate AS ModDate                                   -- datetime
               , 'sa' AS ModUser                                                -- varchar(50)
               , tStatHist.LOAReturnDate AS LOAReturnDate                       -- datetime
               , bStuStatChg.trg_StudentStatusChangeId AS StudentStatusChangeId -- uniqueidentifier
               , bStatCode.trg_StatusCodeId AS StatusCodeId                     -- uniqueidentifier
               , tStatHist.LOARequestDate AS LOARequestDate                     -- datetime
               , tStatHist.StartRecord AS RecordKey
        INTO     #StuLOA
        FROM     #StatHist tStatHist
        INNER JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = tStatHist.StudrecKey
                                                                      AND bStuEnroll.trg_CampusId = @CampusId
        INNER JOIN FreedomBridge.bridge.syStatusCodes bStatCode ON bStatCode.src_ID = tStatHist.CurStatusCode
        LEFT JOIN FreedomBridge.bridge.arLOAReasons bLOA ON bLOA.src_ID = bStatCode.src_ID
                                                            AND bLOA.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(@SchoolID AS VARCHAR(10))
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        LEFT JOIN FreedomBridge.bridge.syStudentStatusChanges bStuStatChg ON bStuStatChg.trg_StuEnrollId = bStuEnroll.trg_StuEnrollId
                                                                             AND bStuStatChg.src_RecordKey = tStatHist.EndRecord
                                                                             AND bStuStatChg.trg_Campus = @CampusId
        LEFT JOIN FreedomAdvantage..arStudentLOAs dStuLOA ON dStuLOA.StuEnrollId = bStuEnroll.trg_StuEnrollId
                                                             AND dStuLOA.LOAReasonId = bStatCode.trg_StatusCodeId

															 
        ORDER BY tStatHist.StudrecKey
               , tStatHist.EndDate;

        ALTER TABLE FreedomAdvantage..arStudentLOAs DISABLE TRIGGER ALL;
		

        INSERT INTO FreedomAdvantage..arStudentLOAs
                    SELECT tStuLOA.StudentLOAId
                         , tStuLOA.StuEnrollId
                         , tStuLOA.StartDate
                         , tStuLOA.EndDate
                         , tStuLOA.LOAReasonId
                         , tStuLOA.ModDate
                         , tStuLOA.ModUser
                         , tStuLOA.LOAReturnDate
                         , tStuLOA.StudentStatusChangeId
                         , tStuLOA.StatusCodeId
                         , tStuLOA.LOARequestDate
                    FROM   #StuLOA tStuLOA
                    LEFT JOIN FreedomAdvantage..arStudentLOAs dStuLOA ON dStuLOA.StuEnrollId = tStuLOA.StuEnrollId
                                                                         AND dStuLOA.StudentStatusChangeId = tStuLOA.StudentStatusChangeId
                    WHERE  dStuLOA.StudentLOAId IS NULL;

        ALTER TABLE FreedomAdvantage..arStudentLOAs ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.arStudentLOAs
                    SELECT tStuLOA.StudentLOAId
                         , tStuLOA.StuEnrollId
                         , tStuLOA.StudentStatusChangeId
                         , tStuLOA.RecordKey
                         , @CampusId AS trg_CampusID
                    FROM   #StuLOA tStuLOA
                    LEFT JOIN FreedomBridge.bridge.arStudentLOAs bStuLOA ON bStuLOA.trg_StuEnrollId = tStuLOA.StuEnrollId
                                                                            AND bStuLOA.trg_StudentStatusChangeId = tStuLOA.StudentStatusChangeId
                    WHERE  bStuLOA.trg_StudentLOAId IS NULL;

        DROP TABLE #Stat;
        DROP TABLE #StatHist;
        DROP TABLE #StuLOA;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arStudentLOAs]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
