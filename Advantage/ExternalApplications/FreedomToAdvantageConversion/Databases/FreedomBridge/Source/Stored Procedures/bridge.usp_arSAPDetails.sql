SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arSAPDetails]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';
        WITH CTE_Sap
        AS ( SELECT     D.RecordKey
                       ,M.RecordKey AS ParentKey
                       ,NextRec
                       ,D.PrivMinResult
                       ,D.PrivConsequence
                       ,D.PrivQuantMinValue
                       ,D.PrivQuantMinType
                       ,D.PrivQuantMinUnit
                       ,D.Sequence
                       ,D.TrigValue
                       ,D.PrivTrigUnit
                       ,D.PrivTrigOffsetType
                       ,D.TrigOffsetSeq
                       ,D.ScriptKey
             FROM       Freedom.dbo.SAPDETAIL_DEF D
             INNER JOIN Freedom.dbo.SAPMASTER_DEF M ON M.SAPDetailKey = D.RecordKey
             UNION ALL
             SELECT     SD_Child.RecordKey
                       ,SD_Parent.ParentKey
                       ,SD_Child.NextRec
                       ,SD_Child.PrivMinResult
                       ,SD_Child.PrivConsequence
                       ,SD_Child.PrivQuantMinValue
                       ,SD_Child.PrivQuantMinType
                       ,SD_Child.PrivQuantMinUnit
                       ,SD_Child.Sequence
                       ,SD_Child.TrigValue
                       ,SD_Child.PrivTrigUnit
                       ,SD_Child.PrivTrigOffsetType
                       ,SD_Child.TrigOffsetSeq
                       ,SD_Child.ScriptKey
             FROM       Freedom.dbo.SAPDETAIL_DEF SD_Child
             INNER JOIN CTE_Sap SD_Parent ON SD_Parent.NextRec = SD_Child.RecordKey )
        SELECT   *
        INTO     #SAPMD
        FROM     CTE_Sap
        ORDER BY CTE_Sap.ParentKey
                ,CTE_Sap.NextRec;

        SELECT     CASE WHEN dSAPDetail.SAPDetailId IS NULL THEN NEWID()
                        ELSE dSAPDetail.SAPDetailId
                   END AS SAPDetailId                                        -- uniqueidentifier
                  ,bSAP.src_Descrip
                  ,tSAPMD.PrivMinResult AS QualMinValue                      -- decimal
                  ,CASE WHEN tSAPMD.PrivQuantMinValue < 1 THEN tSAPMD.PrivQuantMinValue * 100
                        ELSE tSAPMD.PrivQuantMinValue
                   END AS QuantMinValue                                      -- decimal
                  ,2 AS QualMinTypId                                         -- tinyint
                  ,bQtyMinUnitTyp.trg_QuantMinUnitTypId AS QuantMinUnitTypId -- tinyint
                  ,tSAPMD.TrigValue * 100 AS TrigValue                       -- int
                  ,bTrigUnit.trg_TrigUnitTypId AS TrigUnitTypId              -- tinyint
                  ,bTrigOff.trg_TrigOffsetTypId AS TrigOffsetTypId           -- tinyint
                  ,NULL AS TrigOffsetSeq                                     -- tinyint
                  ,bConTyp.trg_ConsequenceTypId AS ConsequenceTypId          -- tinyint
                  ,CASE WHEN tSAPMD.PrivQuantMinUnit = 1 THEN 0
                        WHEN tSAPMD.PrivQuantMinValue < 1 THEN tSAPMD.PrivQuantMinValue * 100
                        ELSE tSAPMD.PrivQuantMinValue
                   END AS MinCredsCompltd                                    -- decimal
                  ,bSAP.trg_SAPId AS SAPId                                   -- uniqueidentifier
                  ,CASE WHEN tSAPMD.PrivQuantMinValue < 1 THEN tSAPMD.PrivQuantMinValue * 100
                        WHEN tSAPMD.PrivQuantMinUnit = 2 THEN 0
                        ELSE tSAPMD.PrivQuantMinValue
                   END AS MinAttendanceValue                                 -- decimal
                  ,0 AS accuracy                                             -- decimal
                  ,tSAPMD.RecordKey
                  ,tSAPMD.ParentKey
                  ,@CampusId AS CampusId
        INTO       #SAP
        FROM       #SAPMD tSAPMD
        INNER JOIN FreedomBridge.bridge.arTrigUnitTyps bTrigUnit ON bTrigUnit.src_TrigUnitID = tSAPMD.PrivTrigUnit
        INNER JOIN FreedomBridge.bridge.arTriggerOffsetTyps bTrigOff ON bTrigOff.src_TrigOffID = tSAPMD.PrivTrigOffsetType
        INNER JOIN FreedomBridge.bridge.arQuantMinUnitTyps bQtyMinUnitTyp ON bQtyMinUnitTyp.src_SAPQuantMinUnitTypeID = tSAPMD.PrivQuantMinType
        INNER JOIN FreedomBridge.bridge.arConsequenceTyps bConTyp ON bConTyp.src_SAPConsequenceID = CASE WHEN tSAPMD.PrivConsequence = 0 THEN 3
                                                                                                         ELSE tSAPMD.PrivConsequence
                                                                                                    END
        INNER JOIN FreedomBridge.bridge.arSAP bSAP ON bSAP.src_RecordKey = tSAPMD.ParentKey
                                                      AND bSAP.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomBridge.bridge.syCampGrps dCampGrp ON dCampGrp.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage.dbo.arSAPDetails dSAPDetail ON dSAPDetail.TrigValue = tSAPMD.TrigValue
                                                                   AND dSAPDetail.SAPId = bSAP.trg_SAPId;

        ALTER TABLE FreedomAdvantage..arSAPDetails DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage.dbo.arSAPDetails
                    SELECT    tSAP.SAPDetailId
                             ,tSAP.QualMinValue
                             ,tSAP.QuantMinValue
                             ,tSAP.QualMinTypId
                             ,tSAP.QuantMinUnitTypId
                             ,tSAP.TrigValue
                             ,tSAP.TrigUnitTypId
                             ,tSAP.TrigOffsetTypId
                             ,tSAP.TrigOffsetSeq
                             ,tSAP.ConsequenceTypId
                             ,tSAP.MinCredsCompltd
                             ,tSAP.SAPId
                             ,tSAP.MinAttendanceValue
                             ,tSAP.accuracy
                    FROM      #SAP tSAP
                    LEFT JOIN FreedomAdvantage.dbo.arSAPDetails dSAPDetail ON dSAPDetail.SAPId = tSAP.SAPId
                                                                              AND dSAPDetail.TrigValue = tSAP.TrigValue
                                                                              AND dSAPDetail.QuantMinValue = tSAP.QuantMinValue
                    --WHERE    dSAPDetail.SAPDetailId IS NULL
                    ORDER BY  tSAP.src_Descrip;

        ALTER TABLE FreedomAdvantage..arSAPDetails ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arSAPDetails')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arSAPDetails
                    (
                        trg_SAPDetailId UNIQUEIDENTIFIER
                       ,src_TrigValue INT
                       ,src_RecordKey INT
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED ( src_RecordKey )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arSAPDetails
                    SELECT    tSAP.SAPDetailId
                             ,tSAP.TrigValue
                             ,tSAP.RecordKey
                             ,@CampusId AS CampusId
                    FROM      #SAP tSAP
                    LEFT JOIN FreedomBridge.bridge.arSAPDetails bSAP ON bSAP.trg_SAPDetailId = tSAP.SAPDetailId
                                                                        AND bSAP.src_TrigValue = tSAP.TrigValue
                                                                        AND bSAP.src_RecordKey = tSAP.RecordKey
                    WHERE     bSAP.trg_SAPDetailId IS NULL;

        UPDATE FreedomAdvantage..arSAPDetails
        SET    ConsequenceTypId = 3
        WHERE  ConsequenceTypId = 0;

        DROP TABLE #SAPMD;
        DROP TABLE #SAP;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arSAPDetails]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
