SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arRooms]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS RoomId              -- uniqueidentifier
             , 'R1' AS Code                   -- varchar(12)
             , 'Room 1' AS Descrip            -- varchar(50)
             , bBuilding.trg_BldgId AS BldgId -- uniqueidentifier
             , dStatus.StatusId AS StatusId   -- uniqueidentifier
             , 'sa' AS ModUser                -- varchar(50)
             , GETDATE() AS ModDate           -- datetime
             , 50 AS Capacity                 -- int
        INTO   #Rooms
        FROM   FreedomBridge.bridge.arBuildings bBuilding
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        LEFT JOIN FreedomAdvantage..arRooms dRooms ON dRooms.Code = 'R1'
                                                      AND dRooms.BldgId = bBuilding.trg_BldgId
        WHERE  bBuilding.trg_CampusId = @CampusId
               AND dRooms.BldgId IS NULL;

        ALTER TABLE FreedomAdvantage..arRooms DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arRooms
                    SELECT tRoom.RoomId
                         , tRoom.Code
                         , tRoom.Descrip
                         , tRoom.BldgId
                         , tRoom.StatusId
                         , tRoom.ModUser
                         , tRoom.ModDate
                         , tRoom.Capacity
                    FROM   #Rooms tRoom;

        ALTER TABLE FreedomAdvantage..arRooms ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arRooms')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arRooms
                    (
                        trg_RoomId UNIQUEIDENTIFIER NULL
                      , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                      , src_Descrip VARCHAR(10) NOT NULL
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_CampusId
                            , src_Descrip
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arRooms
                    SELECT RoomId
                         , @CampusId
                         , Code
                    FROM   #Rooms;

        DROP TABLE #Rooms;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arRooms]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
