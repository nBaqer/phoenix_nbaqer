SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--SET QUOTED_IDENTIFIER ON|OFF
--SET ANSI_NULLS ON|OFF
--GO
CREATE PROCEDURE [bridge].[usp_Cleanup_UpdateGradeBookWeightDetailsSequence]
AS
begin


        DECLARE @CampusId UNIQUEIDENTIFIER;

        SELECT Entity,
               Value
        INTO #Settings
        FROM dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM #Settings
        WHERE Entity = 'CampusID';



UPDATE GBKD SET GBKD.Seq = fd.Seq
FROM FreedomAdvantage..arGrdBkWgtDetails GBKD
LEFT JOIN FreedomBridge.bridge.arGrdBkWgtDetails B ON b.trg_InstrGrdBkWgtDetailId = GBKD.InstrGrdBkWgtDetailId
LEFT JOIN Freedom..WORKUNIT_DEF FD ON FD.RecordKey = b.src_RecordKey
JOIN FreedomAdvantage..arGrdBkWeights w ON w.InstrGrdBkWgtId = GBKD.InstrGrdBkWgtId
JOIN FreedomAdvantage..arReqs r ON w.ReqId = r.ReqId
JOIN FreedomBridge.bridge.syCampGrps c ON c.trg_syCampGrpId = r.CampGrpId
WHERE fd.seq IS NOT NULL  AND  c.trg_CampusId = @CampusId

        INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_Cleanup_UpdateGradeBookWeightDetailsSequence]', GETDATE() , 'arGrdBkWgtDetails updateing Seq' ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]
end;


GO
