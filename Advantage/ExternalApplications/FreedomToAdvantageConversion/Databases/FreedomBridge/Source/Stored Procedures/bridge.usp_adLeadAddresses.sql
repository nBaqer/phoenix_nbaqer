SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=============================================
--Author:		<Author, Spencer Garrett>
--Create date: <Create Date, 11-11-14>
--=============================================
CREATE PROCEDURE [bridge].[usp_adLeadAddresses]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #LeadAdd
        FROM   (
               SELECT     CASE WHEN dLeadAdd.adLeadAddressId IS NULL THEN NEWID()
                               ELSE dLeadAdd.adLeadAddressId
                          END AS adLeadAddressId                        -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                    -- uniqueidentifier
                         ,dAdd.AddressTypeId AS AddressTypeId           -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.AddressLine1)) AS Address1  -- varchar(250)
                         ,NULL AS Address2                              -- varchar(250)
                         ,LTRIM(RTRIM(sStud.AddressCity)) AS City       -- varchar(250)
                         ,dState.StateId AS StateId                     -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.PrivAddressZip)) AS ZipCode -- varchar(10)
                         ,dCountry.CountryId AS CountryId               -- uniqueidentifier
                         ,dStatus.StatusId AS StatusId                  -- uniqueidentifier
                         ,1 AS IsMailingAddress                         -- bit
                         ,1 AS IsShowOnLeadPage                         -- bit
                         ,GETDATE() AS ModDate                          -- datetime
                         ,'sa' AS ModUser                               -- varchar(50)
                         ,LTRIM(RTRIM(sStud.AddressState)) AS State     -- varchar(100)
                         ,CASE WHEN dState.StateId IS NULL THEN 1
                               ELSE 0
                          END AS IsInternational                        -- bit
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS CountyId    -- uniqueidentifier
                         ,'' AS ForeignCountyStr                        -- varchar(100)
                         ,'' AS ForeignCountryStr                       -- varchar(100)
                         ,'' AS AddressApto                             -- varchar(20)
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.StartDate DESC
                                            ) AS LocalRank
               --INTO       #LeadAdd
               FROM       Freedom.dbo.STUDREC sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_StuRecordKey = sStud.RecordKey
               INNER JOIN FreedomAdvantage.dbo.plAddressTypes dAdd ON dAdd.AddressDescrip = 'Home'
               LEFT JOIN  FreedomAdvantage.dbo.syStates dState ON dState.StateCode = sStud.AddressState
               left JOIN FreedomAdvantage.dbo.adCountries dCountry ON dCountry.CountryCode = CASE WHEN dState.StateId IS NOT NULL THEN 'USA'
                                                                                                      ELSE ( CASE WHEN sStud.AddressState = 'MX' THEN 'MEX'
                                                                                                                  WHEN sStud.AddressState = 'CN' THEN 'CAN'
                                                                                                                  ELSE sStud.AddressState
                                                                                                             END
                                                                                                           )
                                                                                                 END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadAddresses dLeadAdd ON dLeadAdd.Address1 = sStud.AddressLine1
                                                                           AND dLeadAdd.City = sStud.AddressCity
                                                                           AND dLeadAdd.StateId = dState.StateId
               GROUP BY   dLeadAdd.adLeadAddressId
                         ,bLead.trg_LeadId
                         ,dAdd.AddressTypeId
                         ,sStud.AddressLine1
                         ,sStud.AddressCity
                         ,dState.StateId
                         ,sStud.PrivAddressZip
                         ,dCountry.CountryId
                         ,dStatus.StatusId
                         ,sStud.AddressState
                         ,sStud.StartDate
               UNION
               SELECT     CASE WHEN dLeadAdd.adLeadAddressId IS NULL THEN NEWID()
                               ELSE dLeadAdd.adLeadAddressId
                          END AS adLeadAddressId                          -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId                      -- uniqueidentifier
                         ,dAdd.AddressTypeId AS AddressTypeId             -- uniqueidentifier
                         ,RTRIM(LTRIM(sStud.AddressLine1)) AS Address1 -- varchar(250)
                         ,NULL AS Address2                                -- varchar(250)
                         ,LTRIM(RTRIM(sStud.AddressCity)) AS City         -- varchar(250)
                         ,dState.StateId AS StateId                       -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.PrivAddressZip)) AS ZipCode   -- varchar(10)
                         ,dCountry.CountryId AS CountryId                 -- uniqueidentifier
                         ,dStatus.StatusId AS StatusId                    -- uniqueidentifier
                         ,1 AS IsMailingAddress                           -- bit
                         ,0 AS IsShowOnLeadPage                           -- bit
                         ,GETDATE() AS ModDate                            -- datetime
                         ,'sa' AS ModUser                                 -- varchar(50)
                         ,LTRIM(RTRIM(sStud.AddressState)) AS State       -- varchar(100)
                         ,CASE WHEN dState.StateId IS NULL THEN 1
                               ELSE 0
                          END AS IsInternational                          -- bit
                         ,CAST(NULL AS UNIQUEIDENTIFIER) AS CountyId      -- uniqueidentifier
                         ,'' AS ForeignCountyStr                          -- varchar(100)
                         ,'' AS ForeignCountryStr                         -- varchar(100)
                         ,'' AS AddressApto                               -- varchar(20)
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.AddressLine1
                                            ) AS LocalRank
               --INTO       #LeadAdd
               FROM       Freedom.dbo.PROSPECT_FIL sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_ProRecordKey = sStud.RecordKey
               INNER JOIN FreedomAdvantage.dbo.plAddressTypes dAdd ON dAdd.AddressDescrip = 'Home'
               LEFT JOIN  FreedomAdvantage.dbo.syStates dState ON dState.StateCode = sStud.AddressState
               left JOIN FreedomAdvantage.dbo.adCountries dCountry ON dCountry.CountryCode = CASE WHEN dState.StateId IS NOT NULL THEN 'USA'
                                                                                                      ELSE ( CASE WHEN sStud.AddressState = 'MX' THEN 'MX'
                                                                                                                  WHEN sStud.AddressState = 'CN' THEN 'CAN'
                                                                                                                  ELSE sStud.AddressState
                                                                                                             END
                                                                                                           )
                                                                                                 END
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.adLeadAddresses dLeadAdd ON dLeadAdd.Address1 = sStud.AddressLine1
                                                                           AND dLeadAdd.City = sStud.AddressCity
                                                                           AND dLeadAdd.StateId = dState.StateId
               GROUP BY   dLeadAdd.adLeadAddressId
                         ,bLead.trg_LeadId
                         ,dAdd.AddressTypeId
                         ,sStud.AddressLine1
                         ,sStud.AddressCity
                         ,dState.StateId
                         ,sStud.PrivAddressZip
                         ,dCountry.CountryId
                         ,dStatus.StatusId
                         ,sStud.AddressState
               HAVING     RTRIM(LTRIM(AddressLine1)) <> ''
               ) AS leadAddress;


        SELECT NEWID() AS adLeadAddressId
              ,f.*
        INTO   #FinalLeadAddress
        FROM   (
               SELECT DISTINCT LeadId
                              ,AddressTypeId
                              ,Address1
                              ,Address2
                              ,City
                              ,StateId
                              ,ZipCode
                              ,CountryId
                              ,StatusId
                              ,CASE WHEN LocalRank = 1
                                         AND IsShowOnLeadPage = 1 THEN 1
                                    ELSE 0
                               END AS IsMailingAddress
                              ,IsShowOnLeadPage
                              ,ModDate
                              ,ModUser
                              ,State
                              ,IsInternational
                              ,CountyId
                              ,ForeignCountyStr
                              ,ForeignCountryStr
                              ,AddressApto
                              ,CampusId
               FROM   #LeadAdd
               ) f;

        SELECT   LeadId
                ,Address1
                ,Address2
                ,City
                ,ZipCode
                ,State
                ,CountryId
                ,StateId
        INTO     #DupeAddresses
        FROM     #FinalLeadAddress
        GROUP BY LeadId
                ,Address1
                ,Address2
                ,City
                ,StateId
                ,ZipCode
                ,CountryId
                ,State
        HAVING   COUNT(*) > 1;



        SELECT   E.adLeadAddressId
        INTO     #DupesToDelete
        FROM     #FinalLeadAddress E
        WHERE    EXISTS (
                        SELECT 1
                        FROM   #DupeAddresses D
                        WHERE  D.Address1 = E.Address1
                               AND (
                                   ( D.Address2 = E.Address2 )
                                   OR ( ISNULL(D.Address2, E.Address2) IS NULL )
                                   )
                               AND D.City = E.City
                               AND D.ZipCode = E.ZipCode
                               AND D.CountryId = E.CountryId
                               AND D.StateId = E.StateId
                               AND D.State = E.State
                               AND D.LeadId = E.LeadId
                        )
                 AND EXISTS (
                            SELECT 1
                            FROM   #FinalLeadAddress C
                            WHERE  C.Address1 = E.Address1
                                   AND (
                                       ( C.Address2 = E.Address2 )
                                       OR ( ISNULL(C.Address2, E.Address2) IS NULL )
                                       )
                                   AND C.City = E.City
                                   AND C.ZipCode = E.ZipCode
                                   AND C.State = E.State
                                   AND C.CountryId = E.CountryId
                                   AND C.StateId = E.StateId
                                   AND C.LeadId = E.LeadId
                                   AND C.IsMailingAddress = 1
                            )
                 AND E.IsMailingAddress = 0
        ORDER BY E.Address1;

        DELETE FROM #FinalLeadAddress
        WHERE adLeadAddressId IN (
                                 SELECT *
                                 FROM   #DupesToDelete
                                 );




        INSERT INTO FreedomAdvantage.dbo.adLeadAddresses
                    SELECT    tLeadAdd.adLeadAddressId
                             ,tLeadAdd.LeadId
                             ,tLeadAdd.AddressTypeId
                             ,tLeadAdd.Address1
                             ,tLeadAdd.Address2
                             ,tLeadAdd.City
                             ,tLeadAdd.StateId
                             ,tLeadAdd.ZipCode
                             ,tLeadAdd.CountryId
                             ,tLeadAdd.StatusId
                             ,tLeadAdd.IsMailingAddress
                             ,tLeadAdd.IsShowOnLeadPage
                             ,tLeadAdd.ModDate
                             ,tLeadAdd.ModUser
                             ,tLeadAdd.State
                             ,tLeadAdd.IsInternational
                             ,tLeadAdd.CountyId
                             ,tLeadAdd.ForeignCountyStr
                             ,tLeadAdd.ForeignCountryStr
                             ,tLeadAdd.AddressApto
                    FROM      #FinalLeadAddress tLeadAdd
                    LEFT JOIN FreedomAdvantage.dbo.adLeadAddresses dLeadAdd ON dLeadAdd.Address1 = tLeadAdd.Address1
                                                                               AND dLeadAdd.City = tLeadAdd.City
                                                                               AND dLeadAdd.StateId = tLeadAdd.StateId
                    WHERE     dLeadAdd.Address1 IS NULL;

        ALTER TABLE FreedomAdvantage.dbo.adLeadAddresses ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.adLeadAddresses
                    SELECT    tLeadAdd.adLeadAddressId
                             ,tLeadAdd.Address1
                             ,tLeadAdd.LeadId
                             ,@CampusId AS CampusId
                    FROM      #FinalLeadAddress tLeadAdd
                    LEFT JOIN FreedomBridge.bridge.adLeadAddresses bLeadAdd ON bLeadAdd.src_Address1 = tLeadAdd.Address1
                                                                               AND bLeadAdd.trg_LeadId = tLeadAdd.LeadId
                                                                               AND bLeadAdd.trg_CampusId = tLeadAdd.CampusId
                    WHERE     bLeadAdd.src_Address1 IS NULL;

        DROP TABLE #FinalLeadAddress;
        DROP TABLE #LeadAdd;
        DROP TABLE #Settings;
        DROP TABLE #DupeAddresses;
        DROP TABLE #DupesToDelete;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeadAddresses]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];
        COMMIT TRANSACTION;

    END;





GO
