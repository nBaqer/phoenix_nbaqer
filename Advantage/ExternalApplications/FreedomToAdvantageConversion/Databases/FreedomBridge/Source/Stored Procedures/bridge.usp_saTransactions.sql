SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saTransactions]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        --Fixing data issue - the same transaction has been reversed twice
        --that is producing dublicate - so we just select distinct values
        SELECT DISTINCT ReversedTransKey
        INTO   #ReversedTran
        FROM   Freedom..STUTRANS_FIL sTranReverse
        WHERE  ReversedTransKey > 0;

        DECLARE @activeStatusId UNIQUEIDENTIFIER = (
                                                   SELECT StatusId
                                                   FROM   FreedomAdvantage..syStatuses
                                                   WHERE  StatusCode = 'A'
                                                   );

        --this is getting null 
        SELECT     DISTINCT sTran.ChargePrdType
                           -- uniqueidentifier
                           ,bEnr.trg_StuEnrollId AS StuEnrollId                                                                                    -- uniqueidentifier
                           ,bTerm.trg_TermId AS TermId                                                                                             -- uniqueidentifier
                           ,@CampusId AS CampusId                                                                                                  -- uniqueidentifier
                           ,sTran.TransDate AS TransDate                                                                                           -- datetime
                           ,BridgeTransCodes.trg_TransCodeId AS TransCodeId                                                                        -- uniqueidentifier
                           ,ISNULL(LEFT(LTRIM(RTRIM(sUser.UserFirstName)), 1) + LEFT(LTRIM(RTRIM(sUser.UserLastName)), 1), 'SA') AS TransReference -- varchar(50) --code persons initials
                           ,dYear.AcademicYearId AS AcademicYearId                                                                                 -- uniqueidentifier -- falog
                           ,CASE WHEN sTran.ReversedTransKey > 0 THEN 'Reversed '
                                 ELSE ''
                            END + sTranType.FullDescrip + CASE WHEN sTran.ReversedTransKey > 0
                                                                    AND R.RecordKey IS NOT NULL THEN ' - ' + R.ReasonText
                                                               ELSE ''
                                                          END AS TransDescrip                                                                      -- varchar(100) --we populate from transtype full desc
                           ,sTran.Amount / 100.00 AS TransAmount                                                                                   -- decimal(19,4)
                           ,CASE WHEN sTranType.CreditDebit = 1
                                      AND sTranType.CreditDebit = sTran.DebitCredit THEN 2 --Payment 
                                 WHEN (
                                      sTranType.CreditDebit = 0
                                      AND sTranType.CreditDebit = sTran.DebitCredit
                                      ) THEN CASE WHEN sTran.TransTypeKey = 3 THEN 1 -- Adjustment 
                                                  ELSE 0                             -- Charge                        
                                             END
                                 WHEN sTranType.CreditDebit <> sTran.DebitCredit THEN 3    --Reversal
                            END AS TransTypeId                                                                                                     -- int -- need saTransTypes bridge TransTypeKey + DebitCredit mapping to TRANTYPE_FIL
                           ,1 AS IsPosted                                                                                                          -- bit
                           ,sTran.PostingDate AS CreateDate                                                                                        -- datetime
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS BatchPaymentId                                                                       -- uniqueidentifier --none
                           ,NULL AS ViewOrder                                                                                                      -- int -- No Data
                           ,0 AS IsAutomatic                                                                                                       -- bit
                           ,'SA' AS ModUser                                                                                                        -- varchar(50)
                           ,GETDATE() AS ModDate                                                                                                   -- datetime
                           --,CASE WHEN sTran.ReversedTransKey > 0
                           --           OR EXISTS (
                           --                     SELECT 1
                           --                     FROM   #ReversedTran C
                           --                     WHERE  C.ReversedTransKey = sTran.RecordKey
                           --                     ) THEN 1
                           --      ELSE 0
                           -- END AS Voided   
                           ,0 AS voided                                                                                                            -- bit
                           ,bFeeLevel.trg_FeeLevelId AS FeeLevelId                                                                                 -- tinyint need bridge chargeprdtype flookup
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS FeeId                                                                                -- uniqueidentifier -- No data
                           ,CASE WHEN sTranType.CreditDebit = 1
                                      AND sTranType.CreditDebit = sTran.DebitCredit --Payment
                           THEN ( BridgeTransCodes.trg_TransCodeId )
                                 ELSE NULL
                            END AS PaymentCodeId                                                                                                   -- uniqueidentifier AS PaymentCodeId -- uniqueidentifier saPayments, anything that got awardlogkey > 0 goes to fundsources
                           ,bFund.trg_FundSourceId AS FundSourceId                                                                                 -- uniqueidentifier ---
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS StuEnrollPayPeriodId                                                                 -- uniqueidentifier No data
                           ,99999 AS DisplaySequence                                                                                               -- int
                           ,99999 AS SecondDisplaySequence                                                                                         -- int
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS PmtPeriodId                                                                          -- UNIQUEIDENTIFIER NULL
                           ,CAST(NULL AS UNIQUEIDENTIFIER) AS PrgChrPeriodSeqId                                                                    -- uniqueidentifier No data
                           ,sTran.RecordKey
                           ,sTran.ChargePrdSeq AS PaymentPeriodNumber
                           ,sTran.ReversedTransKey
                           ,sTran.DebitCredit
        INTO       #saTransactions
        FROM       Freedom..STUTRANS_FIL sTran
        LEFT JOIN  Freedom..FALOGFIL sFALog ON sFALog.RecordKey = sTran.AwardLogKey
        --LEFT JOIN  bridge.saTransCodes bTCode ON bTCode.src_RecordKey = sTran.TransTypeKey
        --                                         AND bTCode.src_AwardTypeKey = sTran.AwardTypeKey
        --                                         AND bTCode.trg_CampusID = @CampusId
        --                                         AND CHECKSUM(ISNULL(bTCode.UnSubLoanFlag, '')) = CHECKSUM(COALESCE(NULLIF(sFALog.UnSubLoanFlag, 'S'), ''))

        INNER JOIN bridge.arStuEnrollments bEnr ON bEnr.src_RecordKey = sTran.StudrecKey
                                                   AND bEnr.trg_CampusId = @CampusId
        INNER JOIN Freedom..STUDREC sStud ON sStud.RecordKey = bEnr.src_RecordKey
        INNER JOIN Freedom..TERM_RES sTermRes ON sTermRes.RecordKey = sStud.TermResKey
        --INNER JOIN Freedom..TERM_DEF sTerm ON sTerm.CourseDefKey = sStud.CourseNumEnrolledIn
        LEFT JOIN  Freedom..RVREASON_FIL R ON sTran.ReasonCode = R.RecordKey
        LEFT JOIN  bridge.arTerm bTerm ON bTerm.src_RecordKey = sTermRes.TermDefKey
        LEFT JOIN  FreedomAdvantage..saAcademicYears dYear ON CAST(dYear.AcademicYearCode AS INT) = ISNULL(
                                                                                                              NULLIF(sFALog.PrivAwardYear, 0) - 1
                                                                                                             ,CASE WHEN CAST(sFALog.AwardStartDate AS DATE)
                                                                                                                        BETWEEN CAST('7-1-'
                                                                                                                                     + CAST(YEAR(sFALog.AwardStartDate) AS VARCHAR(4)) AS DATE) AND CAST('6-30-'
                                                                                                                                                                                                         + CAST(YEAR(sFALog.AwardStartDate) AS VARCHAR(4)) AS DATE) THEN
                                                                                                                       YEAR(sFALog.AwardStartDate) - 1
                                                                                                                   ELSE YEAR(sFALog.AwardStartDate)
                                                                                                              END
                                                                                                          )
        INNER JOIN Freedom..TRANTYPE_FIL sTranType ON sTranType.RecordKey = sTran.TransTypeKey
        LEFT JOIN  Freedom..AWDTYPES_FIL aTypes ON aTypes.RecordKey = sTran.AwardTypeKey
        LEFT JOIN  FreedomAdvantage..saTransCodes TransCodesAdv ON TransCodesAdv.TransCodeDescrip = CASE WHEN sTranType.FullDescrip LIKE '%Financial Aid Refund%'
                                                                                                              AND sTran.AwardTypeKey = 2
                                                                                                              AND sFALog.UnSubLoanFlag = 'U' THEN
                                                                                                             'Refund Direct Unsubsidized Loan'
                                                                                                         WHEN sTranType.FullDescrip LIKE '%Financial Aid Refund%'
                                                                                                              AND sTran.AwardTypeKey = 2
                                                                                                              AND sFALog.UnSubLoanFlag IN ( '', 'S' ) THEN
                                                                                                             'Refund Direct Subsidized Loan'
                                                                                                         WHEN sTranType.FullDescrip LIKE '%Financial Aid Disbursement%'
                                                                                                              AND sTran.AwardTypeKey = 2
                                                                                                              AND sFALog.UnSubLoanFlag = 'U' THEN
                                                                                                             'Direct Unsubsidized Loan'
                                                                                                         WHEN sTranType.FullDescrip LIKE '%Financial Aid Disbursement%'
                                                                                                              AND sTran.AwardTypeKey = 2
                                                                                                              AND sFALog.UnSubLoanFlag IN ( '', 'S' ) THEN
                                                                                                             'Direct Subsidized Loan'
                                                                                                         WHEN sTranType.FullDescrip LIKE '%Financial Aid Refund%'
                                                                                                              AND sTran.AwardTypeKey <> 2 THEN
                                                                                                             'Refund ' + aTypes.FullDescrip
                                                                                                         WHEN sTran.AwardTypeKey <> 0
                                                                                                              AND sTran.TransTypeKey = 2 THEN aTypes.FullDescrip
                                                                                                         WHEN EXISTS (
                                                                                                                     SELECT TOP 1 E.StatusId
                                                                                                                     FROM   FreedomAdvantage..saTransCodes E
                                                                                                                     WHERE  LTRIM(RTRIM(E.TransCodeDescrip)) = LTRIM(RTRIM(sTranType.FullDescrip))
                                                                                                                     ) THEN LTRIM(RTRIM(sTranType.FullDescrip))
                                                                                                         ELSE LTRIM(RTRIM(sTranType.FullDescrip)) + '(I)'
                                                                                                    END
        LEFT JOIN  bridge.saTransCodes BridgeTransCodes ON BridgeTransCodes.src_RecordKey = sTran.TransTypeKey
                                                           AND BridgeTransCodes.trg_TransCodeId = TransCodesAdv.TransCodeId
        --LEFT JOIN  #ReversedTran sTranReverse ON sTranReverse.ReversedTransKey = sTran.RecordKey
        LEFT JOIN  bridge.saFeeLevels bFeeLevel ON bFeeLevel.src_ID = sTran.ChargePrdType --No mapping for 0
        LEFT JOIN  bridge.saFundSources bFund ON bFund.src_RecordKey = sFALog.FinAidTypeId
                                                 AND bFund.CampGrpId = @CampGrpId
                                                 AND bFund.UnSubLoanFlag = CASE WHEN bFund.src_FullDescrip = 'Federal Stafford Loan'
                                                                                     AND sFALog.UnSubLoanFlag = 'U' THEN 1
                                                                                ELSE 0
                                                                           END
        --AND bFund.UnSubLoanFlag = CASE WHEN bFund.src_FullDescrip = 'Federal Stafford Loan' --If is Stafford loan and has empty Flag it is S - subsidized
        --                                    AND sFALog.UnSubLoanFlag = '' THEN 'S'
        --                               ELSE sFALog.UnSubLoanFlag
        --                          END
        LEFT JOIN  Freedom..SHIBBOLETH_FIL sUser ON sUser.UserId = sTran.UserId
        --WHERE      sTran.ReversedTransKey = 0
        --           AND 
        WHERE      (
                   dYear.AcademicYearDescrip NOT IN ( '2015/20151', '2016/20161', '2017/20171' )
                   OR dYear.AcademicYearDescrip IS NULL
                   )
                   AND (
                       TransCodesAdv.TransCodeId IS NULL
                       OR TransCodesAdv.TransCodeCode <> 'AUTOPOSTTUIT'
                       )
                   AND BridgeTransCodes.trg_TransCodeId IS NOT NULL
        ORDER BY   TransDate;


        ALTER TABLE FreedomAdvantage..saTransactions DISABLE TRIGGER ALL;

        SELECT NEWID() AS TransactionId
              ,*
        INTO   #saTransactionsFinal
        FROM   #saTransactions;


        INSERT INTO FreedomAdvantage..saTransactions
                    SELECT TransactionId
                          ,StuEnrollId
                          ,TermId
                          ,CampusId
                          ,TransDate
                          ,TransCodeId
                          ,TransReference
                          ,AcademicYearId
                          ,TransDescrip
                          ,CASE WHEN DebitCredit = 1 THEN TransAmount * -1
                                ELSE TransAmount
                           END AS TransAmount
                          ,TransTypeId
                          ,IsPosted
                          ,CreateDate
                          ,BatchPaymentId
                          ,ViewOrder
                          ,IsAutomatic
                          ,ModUser
                          ,ModDate
                          ,voided
                          ,FeeLevelId
                          ,FeeId
                          ,PaymentCodeId
                          ,FundSourceId
                          ,StuEnrollPayPeriodId
                          ,DisplaySequence
                          ,SecondDisplaySequence
                          ,PmtPeriodId
                          ,PrgChrPeriodSeqId
                          ,CASE WHEN PaymentPeriodNumber > 0 THEN PaymentPeriodNumber
                                ELSE NULL
                           END
                    FROM   #saTransactionsFinal;
        ALTER TABLE FreedomAdvantage..saTransactions ENABLE TRIGGER ALL;

        SELECT   *
        FROM     #saTransactionsFinal
        ORDER BY RecordKey;

        INSERT INTO bridge.saTransactions
                    SELECT DISTINCT TransactionId AS trg_TransactionId
                                   ,RecordKey AS src_RecordKey
                                   ,@CampusId AS trg_CampusId
                    FROM   #saTransactionsFinal;



        --------------------------------------------------------------------------------------------------------
        -----------------voids voided transactions
        --------------------------------------------------------------------------------------------------------
        --UPDATE    Adv
        --SET       Adv.Voided = CASE WHEN F.ReversedTransKey > 0 THEN 1
        --                            ELSE 0
        --                       END
        --FROM      FreedomAdvantage..saTransactions Adv
        --LEFT JOIN bridge.saTransactions B ON Adv.TransactionId = B.trg_TransactionId
        --LEFT JOIN Freedom..STUTRANS_FIL F ON B.src_RecordKey = F.RecordKey
        --WHERE     B.trg_TransactionId IS NOT NULL
        --          AND F.RecordKey IS NOT NULL
        --          AND Adv.Voided <> F.ReversedTransKey;


        --------------------------------------------------------------------------------------------------------
        -----------------Voids original transactions
        --------------------------------------------------------------------------------------------------------
        --UPDATE    Adv
        --SET       Adv.Voided = 1
        --FROM      Freedom..STUTRANS_FIL F
        --LEFT JOIN bridge.saTransactions BT ON BT.src_RecordKey = F.ReversedTransKey
        --LEFT JOIN FreedomAdvantage..saTransactions Adv ON Adv.TransactionId = BT.trg_TransactionId
        --WHERE     F.ReversedTransKey > 0;


        DROP TABLE #Settings;
        DROP TABLE #ReversedTran;
        DROP TABLE #saTransactions;
        DROP TABLE #saTransactionsFinal;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_saTransactions]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;
    END;



GO
