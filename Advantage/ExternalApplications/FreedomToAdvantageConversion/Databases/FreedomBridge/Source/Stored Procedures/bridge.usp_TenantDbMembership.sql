SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jose Astudillo
-- Create date: 2018-09-1
-- Description:	This sprocs insert the user to the aspnet membership table
-- =============================================
--  [bridge].[usp_TenantDbMembership]  'yesenia','047F9996-7383-4FE8-B9ED-74F072B73BD8','test','@beautyacademy.edu'
CREATE PROCEDURE [bridge].[usp_TenantDbMembership]
    -- Add the parameters for the stored procedure here
    @username VARCHAR(256)
   ,@userId UNIQUEIDENTIFIER
   ,@password VARCHAR(256)
   ,@email VARCHAR(256)
AS
    BEGIN


        -- declare variables
        DECLARE @applicationID UNIQUEIDENTIFIER;
        DECLARE @salt UNIQUEIDENTIFIER;
        DECLARE @encodedHashedPassword NVARCHAR(128);
        DECLARE @encodedSalt NVARCHAR(128);
        DECLARE @createDate DATETIME;
        DECLARE @PasswordQuestion NVARCHAR(256);
        DECLARE @PasswordAnswer NVARCHAR(128);
        DECLARE @RC INT;

        --Set variables
        SET @applicationID = (
                             SELECT TOP 1 ApplicationId
                             FROM   TenantAuthDB..aspnet_Applications
                             WHERE  LoweredApplicationName = 'advantage'
                             );

        SET @salt = NEWID();
        SET @encodedSalt = dbo.base64_encode(@salt);
        SET @createDate = GETDATE();
        SET @PasswordQuestion = N'Default';
        SET @PasswordAnswer = N'Default';

        SET @encodedHashedPassword = dbo.base64_encode(HASHBYTES('SHA1', CAST(@salt AS VARBINARY(MAX)) + CAST(@password AS VARBINARY(MAX))));

        --will hold the existing user id to update
        DECLARE @checkExistUserId UNIQUEIDENTIFIER;
        SET @checkExistUserId = (
                                SELECT TOP 1 UserId
                                FROM   TenantAuthDB..aspnet_Membership
                                WHERE  LoweredEmail = '@beautyacademy.edu'
                                );


        --if user does not exist at all
        IF @checkExistUserId IS NULL
            BEGIN
                EXECUTE @RC = TenantAuthDB..aspnet_Membership_CreateUser @applicationID
                                                                        ,@username              --username
                                                                        ,@encodedHashedPassword --password encoded here
                                                                        ,@encodedSalt           --salt enconded
                                                                        ,@email                 --email
                                                                        ,@PasswordQuestion      --password question
                                                                        ,@PasswordAnswer        --password question answer
                                                                        ,1                      --is approved
                                                                        ,@createDate            --currentUtc
                                                                        ,@createDate            --create Date
                                                                        ,0                      --unique email
                                                                        ,1                      --passwordFormat
                                                                        ,@userId --user id
                                                                        OUTPUT;
            END;
        ELSE IF NOT EXISTS (
                           SELECT UserId
                           FROM   TenantAuthDB..aspnet_Users
                           WHERE  UserId = @userId
                           )
                 BEGIN
                     INSERT INTO TenantAuthDB..aspnet_Users (
                                                            ApplicationId
                                                           ,UserId
                                                           ,UserName
                                                           ,LoweredUserName
                                                           ,MobileAlias
                                                           ,IsAnonymous
                                                           ,LastActivityDate
                                                            )
                                 SELECT ApplicationId          -- ApplicationId - uniqueidentifier
                                       ,@userId                -- UserId - uniqueidentifier
                                       ,'dummyUserName'        -- UserName - nvarchar(256)
                                       ,'dummyLoweredUserName' -- LoweredUserName - nvarchar(256)
                                       ,MobileAlias            -- MobileAlias - nvarchar(16)
                                       ,IsAnonymous            -- IsAnonymous - bit
                                       ,LastActivityDate       -- LastActivityDate - datetime
                                 FROM   TenantAuthDB..aspnet_Users
                                 WHERE  UserId = @checkExistUserId;

                     UPDATE TenantAuthDB..aspnet_Membership
                     SET    UserId = @userId
                     WHERE  UserId = @checkExistUserId;

                     DECLARE @LoweredUserName VARCHAR(100);
                     SET @LoweredUserName = (
                                            SELECT   TOP 1 LoweredUserName
                                            FROM     TenantAuthDB..aspnet_Users
                                            WHERE    UserId = @checkExistUserId
                                            ORDER BY LoweredUserName DESC
                                            );

                     DELETE FROM TenantAuthDB..aspnet_Users
                     WHERE UserId = @checkExistUserId;

                     UPDATE TenantAuthDB..aspnet_Users
                     SET    UserName = @username
                           ,LoweredUserName = @LoweredUserName
                     WHERE  UserId = @userId;

                 --UPDATE TenantAuthDB..aspnet_Users
                 --            SET    UserId = @userId
                 --            WHERE  UserId = @checkExistUserId;
                 END;

		--INSERT INTO [dbo].[storedProceduresRun] 
  --      SELECT NEWID(),'[usp_TenantDbMembership]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
  --      FROM [dbo].[storedProceduresRun]

    END;
GO
