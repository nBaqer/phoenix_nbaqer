SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-30-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arBuildings]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS BldgId                                                   -- uniqueidentifier
             , sSchool.SchoolID AS BldgCode                                        -- varchar(12)
             , dStatus.StatusId AS StatusId                                        -- uniqueidentifier
             , 10 AS BldgRooms                                                     -- tinyint
             , sSchool.Name AS BldgDescrip                                         -- varchar(50)
             , bCampGrp.trg_syCampGrpId AS CampGrpId                               -- uniqueidentifier
             , sSchool.PhysAddrLine1 AS Address1                                   -- varchar(80)
             , NULL AS Address2                                                    -- varchar(50)
             , sSchool.PhysAddrCity AS City                                        -- varchar(80)
             , dState.StateId AS StateId                                           -- uniqueidentifier
             , FreedomBridge.dbo.udf_GetNumericOnly(sSchool.DfltSchPhone) AS Phone -- char(50)
             , NULL AS Fax                                                         -- char(50)
             , '' AS BldgOpen                                                      -- varchar(12)
             , '' AS BldgClose                                                     -- varchar(12)
             , sSchool.WeekdaysOpen_1 AS Sun                                       -- bit
             , sSchool.WeekdaysOpen_2 AS Mon                                       -- bit
             , sSchool.WeekdaysOpen_3 AS Tue                                       -- bit
             , sSchool.WeekdaysOpen_4 AS Wed                                       -- bit
             , sSchool.WeekdaysOpen_5 AS Thu                                       -- bit
             , sSchool.WeekdaysOpen_6 AS Fri                                       -- bit
             , sSchool.WeekdaysOpen_7 AS Sat                                       -- bit
             , '' AS BldgName                                                      -- varchar(50)
             , '' AS BldgTitle                                                     -- varchar(50)
             , '' AS BldgEmail                                                     -- varchar(50)
             , '' AS BldgComments                                                  -- varchar(300)
             , FreedomBridge.dbo.udf_GetNumericOnly(sSchool.PhysAddrZip) AS Zip    -- char(20)
             , 'sa' AS ModUser                                                     -- varchar(50)
             , GETDATE() AS ModDate                                                -- datetime
             , NULL AS ForeignPhone                                                -- bit
             , NULL AS ForeignFax                                                  -- bit
             , NULL AS ForeignZip                                                  -- bit
             , '' AS OtherState                                                    -- varchar(50)
             , @CampusId AS CampusId                                               -- uniqueidentifier
        INTO   #Buildings
        FROM   Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomBridge.bridge.syCampGrps bCampGrp ON
        --bCampGrp.src_Name = sSchool.PhysAddrCity
        --                                                          AND 
        bCampGrp.src_SchoolId = sSchool.SchoolID
        INNER JOIN FreedomAdvantage..syStates dState ON dState.StateCode = sSchool.PhysAddrState
        LEFT JOIN FreedomAdvantage..arBuildings dBuilding ON dBuilding.BldgDescrip = sSchool.Name
                                                             AND dBuilding.BldgCode = sSchool.SchoolID
        WHERE  dBuilding.BldgId IS NULL;

        ALTER TABLE FreedomAdvantage..arBuildings DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arBuildings
                    SELECT tBuilding.BldgId AS BldgId             -- uniqueidentifier
                         , tBuilding.BldgCode AS BldgCode         -- varchar(12)
                         , tBuilding.StatusId AS StatusId         -- uniqueidentifier
                         , tBuilding.BldgRooms AS BldgRooms       -- tinyint
                         , tBuilding.BldgDescrip AS BldgDescrip   -- varchar(50)
                         , tBuilding.CampGrpId AS CampGrpId       -- uniqueidentifier
                         , tBuilding.Address1 AS Address1         -- varchar(80)
                         , tBuilding.Address2 AS Address2         -- varchar(50)
                         , tBuilding.City AS City                 -- varchar(80)
                         , tBuilding.StateId AS StateId           -- uniqueidentifier
                         , tBuilding.Phone AS Phone               -- char(50)
                         , tBuilding.Fax AS Fax                   -- char(50)
                         , tBuilding.BldgOpen AS BldgOpen         -- varchar(12)
                         , tBuilding.BldgClose AS BldgClose       -- varchar(12)
                         , tBuilding.Sun AS Sun                   -- bit
                         , tBuilding.Mon AS Mon                   -- bit
                         , tBuilding.Tue AS Tue                   -- bit
                         , tBuilding.Wed AS Wed                   -- bit
                         , tBuilding.Thu AS Thu                   -- bit
                         , tBuilding.Fri AS Fri                   -- bit
                         , tBuilding.Sat AS Sat                   -- bit
                         , tBuilding.BldgName AS BldgName         -- varchar(50)
                         , tBuilding.BldgTitle AS BldgTitle       -- varchar(50)
                         , tBuilding.BldgEmail AS BldgEmail       -- varchar(50)
                         , tBuilding.BldgComments AS BldgComments -- varchar(300)
                         , tBuilding.Zip AS Zip                   -- char(20)
                         , tBuilding.ModUser AS ModUser           -- varchar(50)
                         , tBuilding.ModDate AS ModDate           -- datetime
                         , tBuilding.ForeignPhone AS ForeignPhone -- bit
                         , tBuilding.ForeignFax AS ForeignFax     -- bit
                         , tBuilding.ForeignZip AS ForeignZip     -- bit
                         , tBuilding.OtherState AS OtherState     -- varchar(50)
                         , tBuilding.CampusId AS CampusId         -- uniqueidentifier
                    FROM   #Buildings tBuilding;

        ALTER TABLE FreedomAdvantage..arBuildings ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arBuildings')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arBuildings
                    (
                        trg_BldgId UNIQUEIDENTIFIER NULL
                      , trg_CampusId UNIQUEIDENTIFIER NOT NULL
                      , src_SchoolId INT NOT NULL
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_CampusId
                            , src_SchoolId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arBuildings
                    SELECT BldgId
                         , CampusId
                         , BldgCode
                    FROM   #Buildings;

        DROP TABLE #Buildings;
        DROP TABLE #Settings;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arBuildings]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
