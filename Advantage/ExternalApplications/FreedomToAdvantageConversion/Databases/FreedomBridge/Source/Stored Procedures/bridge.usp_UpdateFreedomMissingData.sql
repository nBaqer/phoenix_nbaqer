SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_UpdateFreedomMissingData]
AS
    BEGIN

        UPDATE E
        SET    PrivTClockSchdRecKey = ISNULL((
                                             SELECT TOP 1 PrivTClockSchdRecKey
                                             FROM   Freedom..STUDREC C
                                             WHERE  C.CourseNumEnrolledIn = E.CourseNumEnrolledIn
                                                    AND C.PrivTClockSchdRecKey > 0
                                             )
                                            ,0
                                            )
        FROM   Freedom..STUDREC E
        WHERE  E.PrivTClockSchdRecKey = 0
               OR E.PrivTClockSchdRecKey IS NULL;

        UPDATE W
        SET    W.PrivResult = -1
        FROM   Freedom..WORKUNIT_RES W
        WHERE  W.PrivSpecialResult > 0
               AND W.PrivResult >= 0;
    END;
GO
