SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arStuEnrollments]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@ActiveStatusId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';


        SELECT @ActiveStatusId = Value
        FROM   #Settings
        WHERE  Entity = 'ActiveStatusId';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';

        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        IF NOT EXISTS (
                      SELECT TOP 1 1
                      FROM   FreedomAdvantage..saTuitionCategories E
                      WHERE  E.TuitionCategoryCode = 'DEF'
                             AND E.CampGrpId = @CampGrpId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..saTuitionCategories
                VALUES ( NEWID(), 'DEF', @ActiveStatusId, 'Default', @CampGrpId, 'Support', GETDATE(), NULL );
            END;


        SELECT     NEWID() AS StuEnrollId                                                               -- uniqueidentifier
                  ,bStud.trg_StudentId AS StudentId                                                     -- uniqueidentifier
                  ,sStud.EnrolledDate AS EnrollDate                                                     -- datetime
                  ,bProgV.trg_PrgVerId AS PrgVerId                                                      -- uniqueidentifier
                  ,sStud.StartDate AS StartDate                                                         -- datetime
                  ,sStud.StartDate AS ExpStartDate                                                      -- datetime
                  ,NULL AS MidPtDate                                                                    -- datetime --calculation
                  ,sStud.RevisedGradDate AS ExpGradDate                                                 -- datetime
                  ,NULL AS TransferDate                                                                 -- datetime -- No data
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftId                                            -- uniqueidentifier -- No data
                  ,dBillM.BillingMethodId AS BillingMethodId                                            -- uniqueidentifier -- "Charge By Program" as Default
                  ,@CampusId AS CampusId                                                                -- uniqueidentifier                                     -- uniqueidentifier
                  ,CASE WHEN sStud.PrivCurrentEnrollStatus = 5 --when case is dropped out but there is not attendance then it is a no start
                             AND (
                                 NOT EXISTS (
                                            SELECT TOP 1 RecordKey
                                            FROM   Freedom..ATTENDMO_FIL Atten
                                            WHERE  Atten.StudrecKey = sStud.RecordKey
                                            )
                                 OR ((
                                     SELECT SUM(Atten.PrivMonthTot)
                                     FROM   Freedom..ATTENDMO_FIL Atten
                                     WHERE  Atten.StudrecKey = sStud.RecordKey
                                     ) = 0
                                    )
                                 OR ((
                                     SELECT LDACheck.LastDateAttended
                                     FROM   Freedom..STUDREC LDACheck
                                     WHERE  LDACheck.RecordKey = bStud.src_RecordKey
                                     ) IS NULL
                                    )
                                 ) THEN (
                                        SELECT TOP 1 scTemp.StatusCodeId
                                        FROM   FreedomAdvantage..syStatusCodes scTemp
                                        WHERE  scTemp.SysStatusId = 8
                                        )
                        WHEN sStud.PrivCurrentEnrollStatus = 1
                             AND sStud.StartDate > GETDATE() THEN (
                                                                  SELECT TOP 1 scTemp.StatusCodeId
                                                                  FROM   FreedomAdvantage..syStatusCodes scTemp
                                                                  WHERE  scTemp.SysStatusId = 7
                                                                  )
                        ELSE bStatCode.trg_StatusCodeId
                   END AS StatusCodeId
                  ,sStud.LastDateAttended AS LDA                                                        -- datetime
                  ,GETDATE() AS ModDate                                                                 -- datetime
                  ,'SA' AS ModUser                                                                      -- varchar(50)
                  ,CAST(@SchoolID AS VARCHAR(4)) + CAST(sStud.RecordKey AS VARCHAR(10)) AS EnrollmentId -- varchar(50)
                  ,dLead.AdmissionsRep AS AdmissionsRep                                                 -- uniqueidentifier --Comes from Leads
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS AcademicAdvisor                                    -- uniqueidentifier -- No data
                  ,bLead.trg_LeadId AS LeadId                                                           -- uniqueidentifier
                  ,dTuitCat.TuitionCategoryId AS TuitionCategoryId                                      -- uniqueidentifier -- No data
                  ,bDrop.trg_DropReasonId AS DropReasonId                                               -- uniqueidentifier
                  ,sStud.EndLeaveDate AS DateDetermined                                                 -- datetime -- Only When Status is "Dropped"
                  ,bEdu.trg_EdLvlId AS EdLvlId                                                          -- uniqueidentifier
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS FAAdvisorId                                        -- uniqueidentifier -- No data
                  ,bAttendType.trg_AttendTypeId AS attendtypeid                                         -- uniqueidentifier -- Full Time / Part time
                  ,bDegCert.trg_DegCertSeekingId AS degcertseekingid                                    -- uniqueidentifier
                  ,sStud.ReEnrolledDate AS ReEnrollmentDate                                             -- datetime
                  ,sStud.TClockBadgeNum AS BadgeNumber                                                  -- varchar(50)
                  ,NULL AS CohortStartDate                                                              -- datetime -- No data
                  ,bSAP.trg_SAPId AS SAPId                                                              -- uniqueidentifier
                  ,sStud.ContractedGradDt AS ContractedGradDate                                         -- datetime
                  ,CAST(sStud.PrivTransferedInHrs AS DECIMAL(18, 2)) AS TransferHours                   -- decimal(18,2)
                  ,sStud2.HighSchGEDDate AS graduatedorreceiveddate                                     -- datetime
                                                                                                    -- bDistance.trg_DistanceEdStatus
                  ,NULL AS DistanceEdStatus                                                             -- varchar(10) -- At course/program level
                  ,0 AS PrgVersionTypeId                                                                -- int
                  ,0 AS IsDisabled                                                                      -- bit NULL
                  ,sStud.FirstTimeAtThisSchool AS IsFirstTimeInSchool                                   -- bit NOT NULL
                  ,sStud.FirstTimeAtAnySchool AS IsFirstTimePostSecSchool                               -- bit NOT NULL
                  ,sStud.EntranceInterviewDate AS EntranceInterviewDate
                  ,sStud.RecordKey                                                                      -- datetime NULL
                  ,CASE WHEN F_Placement.LicExamDate1 IS NOT NULL
                             OR F_Placement.LicExamDate2 IS NOT NULL
                             OR F_Placement.LicExamDate3 IS NOT NULL THEN 1
                        ELSE 0
                   END AS LicensureWrittenAllParts
                  ,CASE WHEN F_Placement.LicExamDate3 IS NOT NULL THEN F_Placement.LicExamDate3
                        WHEN F_Placement.LicExamDate2 IS NOT NULL
                             AND F_Placement.LicExamDate3 IS NULL THEN F_Placement.LicExamDate2
                        WHEN F_Placement.LicExamDate1 IS NOT NULL
                             AND F_Placement.LicExamDate3 IS NULL
                             AND F_Placement.LicExamDate2 IS NULL THEN F_Placement.LicExamDate1
                        ELSE NULL
                   END AS LicensureLastPartWrittenOn
                  ,CASE WHEN (
                             F_Placement.LicExamDate3 IS NOT NULL
                             AND RTRIM(LTRIM(F_Placement.LicExamPassed3)) = 'Y'
                             )
                             OR (
                                F_Placement.LicExamDate2 IS NOT NULL
                                AND F_Placement.LicExamDate3 IS NULL
                                AND RTRIM(LTRIM(F_Placement.LicExamPassed2)) = 'Y'
                                )
                             OR (
                                F_Placement.LicExamDate1 IS NOT NULL
                                AND F_Placement.LicExamDate3 IS NULL
                                AND F_Placement.LicExamDate2 IS NULL
                                AND RTRIM(LTRIM(F_Placement.LicExamPassed1)) = 'Y'
                                ) THEN 1
                        ELSE 0
                   END AS LicensurePassedAllParts
        INTO       #arStuEnrollments
        FROM       Freedom..STUDREC sStud
        INNER JOIN Freedom..STUDREC2 sStud2 ON sStud2.RecordKey = sStud.RecordKey
        LEFT JOIN  Freedom..GRADPLAC_FIL F_Placement ON F_Placement.StudrecKey = sStud.RecordKey
        INNER JOIN bridge.arPrgVersions bProgV ON bProgV.src_RecordKey = sStud.CourseNumEnrolledIn
                                                  AND bProgV.trg_CampusId = @CampusId
        INNER JOIN bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                AND bStud.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage.dbo.saBillingMethods dBillM ON dBillM.BillingMethodDescrip = 'Charge by Program'
        LEFT JOIN  FreedomAdvantage.dbo.saTuitionCategories dTuitCat ON dTuitCat.TuitionCategoryCode = 'DEF'
                                                                        AND dTuitCat.CampGrpId = @CampGrpId
        LEFT JOIN  FreedomBridge.bridge.syStatusCodes bStatCode ON bStatCode.src_ID = sStud.PrivCurrentEnrollStatus
        INNER JOIN bridge.adLeads bLead ON (
                                           bLead.src_StuRecordKey = sStud.RecordKey
                                           OR (
                                              bLead.src_StuSSN = sStud.StuSSN
                                              AND bLead.src_ProRecordKey IS NULL
                                              )
                                           )
                                           AND bLead.trg_CampusId = @CampusId
        LEFT JOIN  bridge.arDropReasons bDrop ON bDrop.src_ID = sStud.DropReasonCode
        INNER JOIN FreedomAdvantage..adLeads dLead ON (
                                                      dLead.LeadId = bLead.trg_LeadId
                                                      OR (
                                                         dLead.SSN = bLead.src_StuSSN
                                                         AND bLead.src_ProRecordKey IS NULL
                                                         )
                                                      ) --losing 100 records
                                                      AND bLead.trg_CampusId = @CampusId
        LEFT JOIN  bridge.adEdLvls bEdu ON bEdu.src_ID = sStud2.EducationLevel
        INNER JOIN bridge.arAttendTypes bAttendType ON bAttendType.src_ID = CASE WHEN LTRIM(RTRIM(sStud2.FullTimePartTime)) = '' THEN 1
                                                                                 ELSE sStud2.FullTimePartTime
                                                                            END
        INNER JOIN bridge.adDegCertSeeking bDegCert ON bDegCert.src_ID = sStud.DegreeSeekingType
        INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sStud.CourseNumEnrolledIn
        LEFT JOIN  bridge.arSAP bSAP ON bSAP.src_RecordKey = sCourse.SapPolicyKey
                                        AND bSAP.trg_CampusId = @CampusId
        WHERE      sStud.PrivCurrentEnrollStatus <> 0
                   AND sStud.EnrolledDate IS NOT NULL;
        --AND dStuEnroll.StuEnrollId IS NULL;

        ALTER TABLE FreedomAdvantage..arStuEnrollments DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arStuEnrollments (
                                                       StuEnrollId
                                                      ,StudentId
                                                      ,EnrollDate
                                                      ,PrgVerId
                                                      ,StartDate
                                                      ,ExpStartDate
                                                      ,MidPtDate
                                                      ,ExpGradDate
                                                      ,TransferDate
                                                      ,ShiftId
                                                      ,BillingMethodId
                                                      ,CampusId
                                                      ,StatusCodeId
                                                      ,LDA
                                                      ,ModDate
                                                      ,ModUser
                                                      ,EnrollmentId
                                                      ,AdmissionsRep
                                                      ,AcademicAdvisor
                                                      ,LeadId
                                                      ,TuitionCategoryId
                                                      ,DropReasonId
                                                      ,DateDetermined
                                                      ,EdLvlId
                                                      ,FAAdvisorId
                                                      ,attendtypeid
                                                      ,degcertseekingid
                                                      ,ReEnrollmentDate
                                                      ,BadgeNumber
                                                      ,CohortStartDate
                                                      ,SAPId
                                                      ,ContractedGradDate
                                                      ,TransferHours
                                                      ,graduatedorreceiveddate
                                                      ,DistanceEdStatus
                                                      ,PrgVersionTypeId
                                                      ,IsDisabled
                                                      ,IsFirstTimeInSchool
                                                      ,IsFirstTimePostSecSchool
                                                      ,EntranceInterviewDate
                                                      ,DisableAutoCharge
                                                      ,LicensureLastPartWrittenOn
                                                      ,LicensureWrittenAllParts
                                                      ,LicensurePassedAllParts
                                                       )
                    SELECT tStuEnroll.StuEnrollId
                          ,tStuEnroll.StudentId
                          ,tStuEnroll.EnrollDate
                          ,tStuEnroll.PrgVerId
                          ,tStuEnroll.StartDate
                          ,tStuEnroll.ExpStartDate
                          ,tStuEnroll.MidPtDate
                          ,tStuEnroll.ExpGradDate
                          ,tStuEnroll.TransferDate
                          ,tStuEnroll.ShiftId
                          ,tStuEnroll.BillingMethodId
                          ,tStuEnroll.CampusId
                          ,tStuEnroll.StatusCodeId
                          ,tStuEnroll.LDA
                          ,tStuEnroll.ModDate
                          ,tStuEnroll.ModUser
                          ,CAST(tStuEnroll.EnrollmentId AS INT) + 1000
                          ,tStuEnroll.AdmissionsRep
                          ,tStuEnroll.AcademicAdvisor
                          ,tStuEnroll.LeadId
                          ,tStuEnroll.TuitionCategoryId
                          ,tStuEnroll.DropReasonId
                          ,tStuEnroll.DateDetermined
                          ,tStuEnroll.EdLvlId
                          ,tStuEnroll.FAAdvisorId
                          ,tStuEnroll.attendtypeid
                          ,tStuEnroll.degcertseekingid
                          ,tStuEnroll.ReEnrollmentDate
                          ,tStuEnroll.BadgeNumber
                          ,tStuEnroll.CohortStartDate
                          ,tStuEnroll.SAPId
                          ,tStuEnroll.ContractedGradDate
                          ,tStuEnroll.TransferHours
                          ,tStuEnroll.graduatedorreceiveddate
                          ,tStuEnroll.DistanceEdStatus
                          ,tStuEnroll.PrgVersionTypeId
                          ,tStuEnroll.IsDisabled               -- bit NULL
                          ,tStuEnroll.IsFirstTimeInSchool      -- bit NOT NULL
                          ,tStuEnroll.IsFirstTimePostSecSchool -- bit NOT NULL
                          ,tStuEnroll.EntranceInterviewDate    -- datetime NULL
                          ,0                                   -- bit NOT NULL
                          ,tStuEnroll.LicensureLastPartWrittenOn
                          ,tStuEnroll.LicensureWrittenAllParts
                          ,tStuEnroll.LicensurePassedAllParts
                    FROM   #arStuEnrollments tStuEnroll;

        --SELECT * FROM #arStuEnrollments

        INSERT INTO bridge.arStuEnrollments
                    SELECT StuEnrollId AS trg_StuEnrollId
                          ,tStuEnroll.RecordKey AS src_RecordKey
                          ,@CampusId AS trg_CampusId
                    FROM   #arStuEnrollments tStuEnroll;
        --SELECT * FROM bridge.arStuEnrollments
        --LEFT JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = RIGHT(tStuEnroll.EnrollmentId,
        --                                                                                        LEN(tStuEnroll.EnrollmentId)
        --                                                                                        - 4)
        --                                                              AND bStuEnroll.trg_CampusId = @CampusId
        --WHERE   bStuEnroll.trg_StuEnrollId IS NULL
        --ORDER BY tStuEnroll.EnrollmentId

        DROP TABLE #arStuEnrollments;
        DROP TABLE #Settings;
        ALTER TABLE FreedomAdvantage..arStuEnrollments ENABLE TRIGGER ALL;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arStuEnrollments]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;

GO
