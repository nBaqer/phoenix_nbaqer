SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-08-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arGrdBkResults]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
        DECLARE @SchoolID INT
               ,@CampusID UNIQUEIDENTIFIER;

        SELECT     @SchoolID = SchoolID
                  ,@CampusID = dCamp.CampusId
        FROM       Freedom.dbo.SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage.dbo.syCampuses dCamp ON dCamp.CampCode = CAST(sSchool.SchoolID AS VARCHAR(50));

        SELECT   MAX(RecordKey) AS RecordKey
                ,StudrecKey
        INTO     #MaxRecKeys
        FROM     Freedom.dbo.WORKUNIT_RES sWorkRes
        GROUP BY StudrecKey;

        WITH CTE_Filter
        AS ( SELECT     sWorkRes.RecordKey
                       ,sWorkRes.NextRec
                       ,sWorkRes.StudrecKey
             FROM       Freedom.dbo.WORKUNIT_RES sWorkRes
             INNER JOIN #MaxRecKeys tMax ON tMax.RecordKey = sWorkRes.RecordKey
             UNION ALL
             SELECT     sWorkRes.RecordKey
                       ,sWorkRes.NextRec
                       ,sWorkRes.StudrecKey
             FROM       Freedom.dbo.WORKUNIT_RES sWorkRes
             INNER JOIN CTE_Filter Filter ON Filter.NextRec = sWorkRes.RecordKey )
        SELECT Filter.RecordKey
              ,Filter.StudrecKey
        INTO   #Filter
        FROM   CTE_Filter Filter
        OPTION ( MAXRECURSION 32767 );


        SELECT *
        INTO   #WorkRes
        FROM   (
               SELECT     CASE WHEN dGrdBkRes.ClsSectionId IS NULL THEN NEWID()
                               ELSE dGrdBkRes.GrdBkResultId
                          END AS GrdBkResultId                                            -- uniqueidentifier
                         ,bClsSect.trg_ClsSectionId AS ClsSectionId                       -- uniqueidentifier
                         ,bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId AS InstrGrdBkWgtDetailId -- uniqueidentifier
                         ,CASE WHEN sWorkRes.PrivResult = -1
                                    AND sWorkRes.PrivSpecialResult = 0
                                    AND sWorkDef.MinLabCnt > 0
                                    AND sWorkRes.PrivLabCnt > 0 AND sWorkDef.Type = 3 THEN sWorkRes.PrivLabCnt
                               WHEN sWorkRes.PrivResult = -1
                                    AND sWorkRes.PrivSpecialResult = 1 THEN -1 -- transfered
                               WHEN sWorkRes.PrivResult >= 0 THEN sWorkRes.PrivResult
                               ELSE CAST(NULL AS INT)
                          END AS Score                                                    -- decimal
                         ,'' AS Comments                                                  -- varchar(50)
                         ,bStuEnroll.trg_StuEnrollId AS StuEnrollId                       -- uniqueidentifier
                         ,sWorkRes.StudrecKey
                         ,'sa' AS ModUser                                                 -- varchar(50)
                         ,GETDATE() AS ModDate                                            -- datetime
                         ,ROW_NUMBER() OVER ( PARTITION BY bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId
                                                          ,bClsSect.trg_ClsSectionId
                                                          ,sWorkRes.StudrecKey
                                              ORDER BY sWorkRes.StudrecKey
                                                      ,sWorkRes.Date DESC
                                            ) AS ResNum                                   -- int
                         ,CASE WHEN sWorkRes.Date = '4200-02-12'
                                    OR sWorkRes.Date > '2079-06-06' THEN '2079-06-06' --Central State Beauty and Wellness College fix
                               WHEN sWorkRes.Date IS NOT NULL THEN sWorkRes.Date
                               WHEN sWorkRes.Date IS NULL
                                    AND sWorkRes.PrivResult < 0
                                    AND sSubRes.PrivSpecialResult = 0
                                    AND EXISTS (
                                               SELECT 1
                                               FROM   Freedom..WORKUNIT_RES C
                                               WHERE  C.Date IS NOT NULL
                                                      AND C.RecordKey = sWorkRes.NextRec
                                               ) THEN (
                                                      SELECT C.Date
                                                      FROM   Freedom..WORKUNIT_RES C
                                                      WHERE  C.Date IS NOT NULL
                                                             AND C.RecordKey = sWorkRes.NextRec
                                                      )
                               WHEN sWorkRes.Date IS NULL
                                    AND sWorkRes.PrivResult < 0
                                    AND sSubRes.PrivSpecialResult = 0
                                    AND EXISTS (
                                               SELECT 1
                                               FROM   Freedom..WORKUNIT_RES C
                                               WHERE  C.Date IS NOT NULL
                                                      AND C.NextRec = sWorkRes.RecordKey
                                               ) THEN (
                                                      SELECT C.Date
                                                      FROM   Freedom..WORKUNIT_RES C
                                                      WHERE  C.Date IS NOT NULL
                                                             AND C.NextRec = sWorkRes.RecordKey
                                                      )
                               WHEN sStu.LastDateAttended IS NOT NULL THEN sStu.LastDateAttended
                               ELSE sStu.StartDate
                          END AS PostDate                                                 -- smalldatetime
                         ,CASE WHEN sWorkRes.PrivSpecialResult = 1 THEN 1
                               WHEN sWorkRes.PrivResult < 0
                                    AND sWorkRes.PrivLabCnt >= sWorkDef.MinLabCnt THEN 1
                               WHEN sWorkRes.PrivResult >= sWorkDef.MinResult THEN 1
                               ELSE NULL
                          END AS IsCompGraded                                             -- bit
                         ,CASE WHEN sWorkRes.PrivSpecialResult = 1 THEN 1
                               WHEN sWorkRes.PrivResult < 0
                                    AND sWorkRes.PrivLabCnt >= sWorkDef.MinLabCnt THEN 1
                               WHEN sWorkRes.PrivResult >= sWorkDef.MinResult THEN 1
                               ELSE NULL
                          END AS isCourseCredited                                         -- bit
                         ,sWorkRes.RecordKey
                         ,sWorkRes.PrivResult
                         ,sWorkRes.WorkUnitDefKey
                         ,bReq.src_Name
               FROM       Freedom.dbo.WORKUNIT_RES sWorkRes
               INNER JOIN #Filter Filter ON Filter.RecordKey = sWorkRes.RecordKey
               INNER JOIN Freedom.dbo.WORKUNIT_DEF sWorkDef ON sWorkDef.RecordKey = sWorkRes.WorkUnitDefKey
               INNER JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = sWorkRes.StudrecKey
                                                                              AND bStuEnroll.trg_CampusId = @CampusID
               INNER JOIN Freedom.dbo.SUBJECT_RES sSubRes ON sSubRes.RecordKey = sWorkRes.SubjectResKey
               LEFT JOIN  FreedomBridge.bridge.arReqs bReq ON bReq.src_RecordKey = sSubRes.SubjectDefKey
                                                              AND bReq.trg_CampusId = @CampusID
                                                              AND bReq.src_Name NOT LIKE '%Attendance'
               INNER JOIN Freedom.dbo.STUDREC sStu ON sStu.RecordKey = sWorkRes.StudrecKey
               INNER JOIN FreedomBridge.bridge.arClassSections bClsSect ON bClsSect.trg_ReqId = bReq.trg_ReqId
                                                                           AND bClsSect.trg_CampusId = @CampusID
                                                                           AND bClsSect.src_RecordKey = sSubRes.SubjectDefKey
                                                                           AND LEFT(bClsSect.trg_ClsSection, 7) = LEFT(CAST(sStu.AttndSchdHrsSun AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsMon AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsTue AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsWed AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsThr AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsFri AS VARCHAR(10))
                                                                                                                       + CAST(sStu.AttndSchdHrsSat AS VARCHAR(10)), 7)
               INNER JOIN FreedomBridge.bridge.arGrdBkWgtDetails bGrdBkWgtDtl ON bGrdBkWgtDtl.src_RecordKey = sWorkRes.WorkUnitDefKey
                                                                                 AND bGrdBkWgtDtl.trg_CampusId = @CampusID
                                                                                 AND bGrdBkWgtDtl.InactiveDateRange = 0
               LEFT JOIN  FreedomAdvantage.dbo.arGrdBkResults dGrdBkRes ON dGrdBkRes.InstrGrdBkWgtDetailId = bGrdBkWgtDtl.trg_InstrGrdBkWgtDetailId
                                                                           AND dGrdBkRes.ClsSectionId = bClsSect.trg_ClsSectionId
                                                                           AND dGrdBkRes.StuEnrollId = bStuEnroll.trg_StuEnrollId
               ) a
        WHERE  a.ResNum = 1;

        UPDATE #WorkRes
        SET    PostDate = NULL
        WHERE  Score IS NULL;


        ALTER TABLE FreedomAdvantage..arGrdBkResults DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arGrdBkResults
                    SELECT    tWorkRes.GrdBkResultId
                             ,tWorkRes.ClsSectionId
                             ,tWorkRes.InstrGrdBkWgtDetailId
                             ,tWorkRes.Score
                             ,tWorkRes.Comments
                             ,tWorkRes.StuEnrollId
                             ,tWorkRes.ModUser
                             ,tWorkRes.ModDate
                             ,tWorkRes.ResNum
                             ,CAST(tWorkRes.PostDate AS SMALLDATETIME)
                             ,tWorkRes.IsCompGraded
                             ,tWorkRes.isCourseCredited
                             ,CAST(tWorkRes.PostDate AS DATE)
                    FROM      #WorkRes tWorkRes
                    LEFT JOIN FreedomAdvantage..arGrdBkResults dGrdBkRes ON dGrdBkRes.InstrGrdBkWgtDetailId = tWorkRes.InstrGrdBkWgtDetailId
                                                                            AND dGrdBkRes.ClsSectionId = tWorkRes.ClsSectionId
                                                                            AND dGrdBkRes.StuEnrollId = tWorkRes.StuEnrollId
                    WHERE     dGrdBkRes.GrdBkResultId IS NULL
                    ORDER BY  tWorkRes.StuEnrollId
                             ,tWorkRes.ClsSectionId
                             ,tWorkRes.InstrGrdBkWgtDetailId;

        ALTER TABLE FreedomAdvantage..arGrdBkResults ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arGrdBkResults')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arGrdBkResults
                    (
                        trg_GrdBkResultId UNIQUEIDENTIFIER
                       ,trg_ClsSectionId UNIQUEIDENTIFIER
                       ,trg_InstrGrdBkDetailId UNIQUEIDENTIFIER
                       ,src_RecordKey INT
                       ,trg_StuEnrollId UNIQUEIDENTIFIER
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       trg_ClsSectionId
                      ,trg_StuEnrollId
                      ,trg_InstrGrdBkDetailId
                      ,src_RecordKey
                       )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arGrdBkResults
                    SELECT    tWorkRes.GrdBkResultId
                             ,tWorkRes.ClsSectionId
                             ,tWorkRes.InstrGrdBkWgtDetailId
                             ,tWorkRes.RecordKey
                             ,tWorkRes.StuEnrollId
                             ,@CampusID AS CampusId
                    FROM      #WorkRes tWorkRes
                    LEFT JOIN FreedomBridge.bridge.arGrdBkResults bGrdBkRes ON bGrdBkRes.trg_StuEnrollId = tWorkRes.StuEnrollId
                                                                               AND bGrdBkRes.trg_ClsSectionId = tWorkRes.ClsSectionId
                                                                               AND bGrdBkRes.trg_InstrGrdBkDetailId = tWorkRes.InstrGrdBkWgtDetailId
                                                                               AND bGrdBkRes.trg_CampusId = @CampusID
                    WHERE     bGrdBkRes.trg_GrdBkResultId IS NULL;

        DROP TABLE #WorkRes;
        DROP TABLE #MaxRecKeys;
        DROP TABLE #Filter;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arGrdBkResults]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;





GO
