SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saPayments]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT bTran.trg_TransactionId AS TransactionId     -- uniqueidentifier
             , bPayType.trg_PaymentTypeId AS PaymentTypeId  -- int
             , NULLIF(sTran.CkRcptNum, 0) AS CheckNumber    -- varchar(50)
             , 0 AS ScheduledPayment                        -- bit
             , CAST(NULL AS UNIQUEIDENTIFIER) AS BankAcctId -- uniqueidentifier
             , CASE WHEN PendingDeposit = 1 THEN 0
                    ELSE 1
               END AS IsDeposited                           -- bit
             , 'SA' AS ModUser                              -- varchar(50)
             , GETDATE() AS ModDate                         -- smalldatetime
             , sTran.RecordKey
        INTO   #saPayments
        FROM   Freedom..STUTRANS_FIL sTran
        INNER JOIN bridge.saTransactions bTran ON bTran.src_RecordKey = sTran.RecordKey
                                                  AND bTran.trg_CampusId = @CampusId
        INNER JOIN bridge.saPaymentTypes bPayType ON bPayType.src_ID = sTran.PaymentType
        LEFT JOIN bridge.saPayments bIgnore ON bIgnore.trg_TransactionId = bTran.trg_TransactionId
                                               AND bIgnore.trg_CampusId = @CampusId;

        ALTER TABLE FreedomAdvantage..saPayments DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..saPayments
                    SELECT TransactionId
                         , PaymentTypeId
                         , CheckNumber
                         , ScheduledPayment
                         , BankAcctId
                         , IsDeposited
                         , ModUser
                         , ModDate
                    FROM   #saPayments;
        ALTER TABLE FreedomAdvantage..saPayments ENABLE TRIGGER ALL;

        INSERT INTO bridge.saPayments
                    SELECT TransactionId AS trg_TransactionId
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                    FROM   #saPayments;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saPayments]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
