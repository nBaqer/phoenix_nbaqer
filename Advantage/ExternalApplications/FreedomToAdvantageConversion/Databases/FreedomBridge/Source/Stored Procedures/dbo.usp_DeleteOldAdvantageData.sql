SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteOldAdvantageData]
AS
  DECLARE @ForceACleanDB BIT
  SET @ForceACleanDB = 0
 
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;
        BEGIN TRAN;

		SET @ForceACleanDB = (
		SELECT ForceACleanDB FROM SourceDB WHERE IsDisabled = 0
		)

		UPDATE  SourceDB
		SET ExecutionStart = GETDATE()
		WHERE IsDisabled = 0

	 IF ( @ForceACleanDB = 1 )
        BEGIN

        DELETE FROM FreedomAdvantage..saPmtDisbRel;
        DELETE FROM FreedomAdvantage..faStuPaymentPlanSchedule;
        DELETE FROM FreedomAdvantage..faStudentPaymentPlans;
        DELETE FROM FreedomAdvantage..arSAPChkResults;
        DELETE FROM FreedomAdvantage..arOverridenPreReqs;
        DELETE FROM FreedomAdvantage..arGrdBkResults;
        DELETE FROM FreedomAdvantage..arGrdBkConversionResults;
        DELETE FROM FreedomAdvantage..arStudentLOAs;
        DELETE FROM FreedomAdvantage..arStdSuspensions;
        DELETE FROM FreedomAdvantage..arTransferGrades;
        DELETE FROM FreedomAdvantage..arStuProbWarnings;
        DELETE FROM FreedomAdvantage..arStudentSchedules;
        DELETE FROM FreedomAdvantage..arStudentTimeClockPunches;
        DELETE FROM FreedomAdvantage..arResults;
        DELETE FROM FreedomAdvantage..atClsSectAttendance;
        DELETE FROM FreedomAdvantage..saRefunds;
        DELETE FROM FreedomAdvantage..saPayments;
        DELETE FROM FreedomAdvantage..saTransactions;
        DELETE FROM FreedomAdvantage..syStudentStatusChanges;
        DELETE FROM FreedomAdvantage..faStudentAwardSchedule;
        DELETE FROM FreedomAdvantage..faStudentAwards;
        DELETE FROM FreedomAdvantage..arExternshipAttendance;
        DELETE FROM FreedomAdvantage..PlStudentsPlaced;
        DELETE FROM FreedomAdvantage..arStuEnrollments;
        DELETE FROM FreedomAdvantage..arDropReasons;
        DELETE FROM FreedomAdvantage..faLenders;
        DELETE FROM FreedomAdvantage..plJobWorkDays;
        DELETE FROM FreedomAdvantage..plEmployerJobs;
        DELETE FROM FreedomAdvantage..plEmployerJobCats;
        DELETE FROM FreedomAdvantage..plEmployerContact;
        DELETE FROM FreedomAdvantage..plEmployers;
        DELETE FROM FreedomAdvantage..plLocations;
        --DELETE  FROM FreedomAdvantage..syStudentNotes
        DELETE FROM FreedomAdvantage..plEmployerJobs;
        DELETE FROM FreedomAdvantage..plStudentDocs;
        --DELETE  FROM FreedomAdvantage..arStudentPhoneOLD
        DELETE FROM FreedomAdvantage..syDocumentHistory;
        --DELETE  FROM FreedomAdvantage..plStudentEducation
        DELETE FROM FreedomAdvantage..arStudent;
        DELETE FROM FreedomAdvantage..adLeadByLeadGroups;
        DELETE FROM FreedomAdvantage..adEntrTestOverRide;
        DELETE FROM FreedomAdvantage..adLeadDocsReceived;
        DELETE FROM FreedomAdvantage..adLeads;
        DELETE FROM FreedomAdvantage..adCounties;
        DELETE FROM FreedomAdvantage..saLateFees;
        DELETE FROM FreedomAdvantage..saTransCodeGLAccounts;
        DELETE FROM FreedomAdvantage..saProgramVersionFees;
        DELETE FROM FreedomAdvantage..saTransCodes;
        DELETE FROM FreedomAdvantage..saAppliedPayments;
		END;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_DeleteOldAdvantageData]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRAN;
		
    END;
GO
