SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[FixClassSections]
AS
    BEGIN
        DECLARE @Error AS INTEGER;
        SET @Error = 0;

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampusGroupAllId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT @CampGrpId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupID';


        SELECT @CampusGroupAllId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusGroupAllId';

        BEGIN TRANSACTION FixesClassSections;
        BEGIN TRY



            SELECT --TOP 100 
                       courses.ReqId
                      ,courses.Descrip AS CourseDescription
                      ,classSections.ClsSectionId
                      ,classSections.ClsSection
                      ,gradeBookResults.GrdBkResultId
                      ,gradeBookResults.Score
                      ,gradeBookResults.Comments
                      ,gradeBookResults.StuEnrollId
                      ,gradeBookResults.ModUser
                      ,gradeBookResults.ModDate
                      ,gradeBookResults.ResNum
                      ,gradeBookResults.PostDate
                      ,gradeBookResults.IsCompGraded
                      ,gradeBookResults.isCourseCredited
                      ,gradeBookResults.DateCompleted
                      ,gradebookWeightDetails.InstrGrdBkWgtDetailId
                      ,gradebookWeightDetails.Descrip AS GradeBookWeightDetailDescription
                      ,gradebookWeightDetails.InstrGrdBkWgtId
                      ,gradebookWeight.Descrip AS GradeBookWeightDescription
                      ,classSections.TermId
                      ,classSections.ProgramVersionDefinitionId
            INTO       #gradeBookResults
            FROM       [FreedomAdvantage]..arGrdBkResults gradeBookResults
            INNER JOIN [FreedomAdvantage]..arClassSections classSections ON classSections.ClsSectionId = gradeBookResults.ClsSectionId
            INNER JOIN [FreedomAdvantage]..arReqs courses ON courses.ReqId = classSections.ReqId
                                                             AND courses.CampGrpId = @CampGrpId
            INNER JOIN [FreedomAdvantage]..arGrdBkWgtDetails gradebookWeightDetails ON gradebookWeightDetails.InstrGrdBkWgtDetailId = gradeBookResults.InstrGrdBkWgtDetailId
            INNER JOIN [FreedomAdvantage]..arGrdBkWeights gradebookWeight ON gradebookWeight.InstrGrdBkWgtId = gradebookWeightDetails.InstrGrdBkWgtId;


            PRINT 'GradeBookResults Select Finish';

            SELECT     courses.ReqId
                      ,courses.Descrip AS CourseDescription
                      ,classSections.ClsSectionId
                      ,classSections.ClsSection
                      ,results.ResultId
                      ,results.TestId
                      ,results.Score
                      ,( CASE WHEN F_PV.GradeScaleKey > 0 THEN (
                                                               SELECT    TOP 1 GSD.GrdSysDetailId
                                                               FROM      [FreedomAdvantage]..arGradeScaleDetails GSD
                                                               LEFT JOIN [FreedomAdvantage]..arGradeScales GS ON GS.GrdScaleId = GSD.GrdScaleId
                                                               LEFT JOIN bridge.arGradeScales B_GS ON GS.GrdScaleId = B_GS.trg_GradeScaleId
                                                               WHERE     LTRIM(RTRIM(GS.Descrip)) = 'Grade Scale'
                                                                         AND B_GS.src_Description = f_gs.description
                                                                         AND ROUND(results.Score, 2)
                                                                         BETWEEN ( GSD.MinVal - 0.5 ) AND ( GSD.MaxVal + 0.49 )
                                                               )
                              WHEN EXISTS (
                                          SELECT 1
                                          FROM   [Freedom]..GRADESCALE_FIL
                                          ) THEN (
                                                 SELECT    TOP 1 GSD.GrdSysDetailId
                                                 FROM      [FreedomAdvantage]..arGradeScaleDetails GSD
                                                 LEFT JOIN [FreedomAdvantage]..arGradeScales GS ON GS.GrdScaleId = GSD.GrdScaleId
                                                 WHERE     LTRIM(RTRIM(GS.Descrip)) = (
                                                                                      SELECT TOP 1 Description
                                                                                      FROM   [Freedom]..GRADESCALE_FIL
                                                                                      )
                                                           AND ROUND(results.Score, 2)
                                                           BETWEEN ( GSD.MinVal - 0.5 ) AND ( GSD.MaxVal + 0.49 )
                                                 )
                              ELSE (
                                   SELECT    TOP 1 GSD.GrdSysDetailId
                                   FROM      [FreedomAdvantage]..arGradeScaleDetails GSD
                                   LEFT JOIN [FreedomAdvantage]..arGradeScales GS ON GS.GrdScaleId = GSD.GrdScaleId
                                   WHERE     LTRIM(RTRIM(GS.Descrip)) = 'Default'
                                             AND ROUND(results.Score, 2)
                                             BETWEEN ( GSD.MinVal - 0.5 ) AND ( GSD.MaxVal + 0.49 )
                                   )
                         END
                       ) AS GrdSysDetailId
                      ,results.Cnt
                      ,results.Hours
                      ,results.StuEnrollId
                      ,results.IsInComplete
                      ,results.DroppedInAddDrop
                      ,results.ModUser
                      ,results.ModDate
                      ,results.IsTransfered
                      ,results.isClinicsSatisfied
                      ,results.DateDetermined
                      ,results.IsCourseCompleted
                      ,results.IsGradeOverridden
                      ,results.GradeOverriddenBy
                      ,results.GradeOverriddenDate
                      ,results.DateCompleted
                      ,classSections.TermId
                      ,classSections.ProgramVersionDefinitionId
            INTO       #arResults
            FROM       [FreedomAdvantage]..arResults results
            LEFT JOIN  [FreedomAdvantage].dbo.arStuEnrollments E ON E.StuEnrollId = results.StuEnrollId
            LEFT JOIN  bridge.arPrgVersions B_PB ON B_PB.trg_PrgVerId = E.PrgVerId
            LEFT JOIN  [Freedom]..COURSE_DEF F_PV ON F_PV.RecordKey = B_PB.src_RecordKey
            LEFT JOIN  [Freedom]..GRADESCALE_FIL F_GS ON F_GS.RecordKey = F_PV.GradeScaleKey
            INNER JOIN [FreedomAdvantage]..arClassSections classSections ON classSections.ClsSectionId = results.TestId
            INNER JOIN [FreedomAdvantage]..arReqs courses ON courses.ReqId = classSections.ReqId
                                                             AND courses.CampGrpId = @CampGrpId;
            PRINT 'ArResults Select Finish';



            PRINT 'Starting deleting all records';
            DELETE FROM [FreedomAdvantage]..arGrdBkResults
            WHERE StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM   [FreedomAdvantage]..arStuEnrollments
                                 WHERE  CampusId = @CampusId
                                 );

            DELETE FROM [FreedomAdvantage]..arResults
            WHERE StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM   [FreedomAdvantage]..arStuEnrollments
                                 WHERE  CampusId = @CampusId
                                 );

            DELETE FROM [FreedomAdvantage]..arClassSectionTerms
            WHERE ClsSectionId IN (
                                  SELECT ClsSectionId
                                  FROM   [FreedomAdvantage]..arClassSections
                                  WHERE  CampusId = @CampusId
                                  );

            DELETE FROM [FreedomAdvantage]..arClassSections
            WHERE CampusId = @CampusId;

            PRINT 'Done Delete records finished';

            SELECT    CAST(MIN(enrollments.StartDate) AS DATETIME) AS StartDate
                     ,CAST(MAX(enrollments.ContractedGradDate) AS DATETIME) EndDate
                     ,programVersions.PrgVerId
            INTO      #ProgramVersionLength
            FROM      [FreedomAdvantage]..arPrgVersions programVersions
            LEFT JOIN [FreedomAdvantage]..arStuEnrollments enrollments ON enrollments.PrgVerId = programVersions.PrgVerId
            GROUP BY  programVersions.PrgVerId;

            SELECT     NEWID() AS ClsSectionId
                      ,terms.TermId
                      ,courses.ReqId
                      ,courses.Code
                      ,NULL AS InstructorId
                      ,CASE WHEN programVersionLength.StartDate IS NULL THEN CAST('1/1/2000' AS DATETIME)
                            ELSE programVersionLength.StartDate
                       END AS StartDate
                      ,CASE WHEN programVersionLength.EndDate IS NULL THEN CAST('12/31/2050' AS DATETIME)
                            ELSE programVersionLength.EndDate
                       END AS EndDate
                      ,2147483647 AS MaxStud                             -- int
                      ,CAST(NULL AS UNIQUEIDENTIFIER) AS InstrGrdBkWgtId -- uniqueidentifier
                      ,bGrdScl.GrdScaleId AS GrdScaleId                  -- uniqueidentifier
                      ,@CampusId AS CampusId                             -- uniqueidentifier
                      ,CAST(NULL AS BIT) AS IsGraded                     -- bit
                      ,'sa' AS ModUser                                   -- varchar(50)
                      ,GETDATE() AS ModDate                              -- datetime
                      ,NULL AS CourseId                                  -- varchar(50)
                      ,NULL AS TermGUID                                  -- varchar(50)
                      ,NULL AS Session                                   -- varchar(50)
                      ,NULL AS ClassRoom                                 -- varchar(50)
                      ,CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftId         -- uniqueidentifier
                      ,CAST(NULL AS UNIQUEIDENTIFIER) AS PeriodId        -- uniqueidentifier
                      ,CAST(NULL AS DATETIME) AS StudentStartDate        -- datetime
                      ,CAST(NULL AS UNIQUEIDENTIFIER) AS LeadGrpId       -- uniqueidentifier
                      ,CAST(NULL AS DATETIME) AS CohortStartDate
                      ,programVersionDefinition.ProgVerDefId AS ProgramVersionDefinitionId
            INTO       #ClassSection
            FROM       [FreedomAdvantage]..arReqs courses
            INNER JOIN [FreedomAdvantage]..arProgVerDef programVersionDefinition ON programVersionDefinition.ReqId = courses.ReqId
            INNER JOIN #ProgramVersionLength programVersionLength ON programVersionLength.PrgVerId = programVersionDefinition.PrgVerId
            INNER JOIN [FreedomAdvantage]..arTerm terms ON terms.ProgramVersionId = programVersionDefinition.PrgVerId
                                                           AND terms.CampGrpId = @CampGrpId
            INNER JOIN [FreedomAdvantage]..arGradeScales bGrdScl ON bGrdScl.Descrip = 'Default'
                                                                    AND bGrdScl.CampGrpId = @CampGrpId
            WHERE      courses.CampGrpId = @CampGrpId;


            PRINT 'Inserting Class Sections';
            ALTER TABLE [FreedomAdvantage]..arClassSections DISABLE TRIGGER ALL;
            INSERT INTO [FreedomAdvantage]..arClassSections (
                                                            ClsSectionId
                                                           ,TermId
                                                           ,ReqId
                                                           ,ClsSection
                                                           ,StartDate
                                                           ,EndDate
                                                           ,MaxStud
                                                           ,GrdScaleId
                                                           ,CampusId
                                                           ,ModUser
                                                           ,ModDate
                                                           ,ProgramVersionDefinitionId
                                                            )
                        SELECT ClsSectionId
                              ,TermId
                              ,ReqId
                              ,Code
                              ,StartDate
                              ,EndDate
                              ,MaxStud
                              ,GrdScaleId
                              ,CampusId
                              ,ModUser
                              ,ModDate
                              ,ProgramVersionDefinitionId
                        FROM   #ClassSection;
            ALTER TABLE [FreedomAdvantage]..arClassSections ENABLE TRIGGER ALL;


            PRINT 'Inserting class section terms';
            ALTER TABLE [FreedomAdvantage]..arClassSectionTerms DISABLE TRIGGER ALL;
            INSERT INTO [FreedomAdvantage]..arClassSectionTerms (
                                                                ClsSectTermId
                                                               ,ClsSectionId
                                                               ,TermId
                                                               ,ModDate
                                                               ,ModUser
                                                                )
                        SELECT NEWID()      -- ClsSectTermId - uniqueidentifier
                              ,ClsSectionId -- ClsSectionId - uniqueidentifier
                              ,TermId       -- TermId - uniqueidentifier
                              ,GETDATE()    -- ModDate - datetime
                              ,'sa'         -- ModUser - varchar(50)
                        FROM   #ClassSection;
            ALTER TABLE [FreedomAdvantage]..arClassSectionTerms ENABLE TRIGGER ALL;

            SELECT   ROW_NUMBER() OVER ( PARTITION BY results.StuEnrollId
                                                     ,results.InstrGrdBkWgtDetailId
                                                     ,results.GrdBkResultId
                                         ORDER BY results.StuEnrollId
                                                 ,results.PostDate DESC
                                       ) AS RowNumber
                    ,*
            INTO     #SortedGradeBookResults
            FROM     #gradeBookResults results
            --WHERE    results.Score IS NOT NULL
            ORDER BY results.StuEnrollId;

            PRINT 'Inserting ArGrdBkResults';
            ALTER TABLE [FreedomAdvantage]..arGrdBkResults DISABLE TRIGGER ALL;
            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Score
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,ResNum
                                                          ,PostDate
                                                          ,IsCompGraded
                                                          ,isCourseCredited
                                                          ,DateCompleted
                                                           )
                        SELECT     DISTINCT results.GrdBkResultId         -- GrdBkResultId - uniqueidentifier
                                           ,classSection.ClsSectionId     -- ClsSectionId - uniqueidentifier
                                           ,results.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                           ,results.Score                 -- Score - decimal(6, 2)
                                           ,results.Comments              -- Comments - varchar(50)
                                           ,results.StuEnrollId           -- StuEnrollId - uniqueidentifier
                                           ,results.ModUser               -- ModUser - varchar(50)
                                           ,results.ModDate               -- ModDate - datetime
                                           ,results.ResNum                -- ResNum - int
                                           ,results.PostDate              -- PostDate - smalldatetime
                                           ,results.IsCompGraded          -- IsCompGraded - bit
                                           ,results.isCourseCredited      -- isCourseCredited - bit
                                           ,results.DateCompleted         -- DateCompleted - date
                        FROM       #SortedGradeBookResults results
                        INNER JOIN #ClassSection classSection ON classSection.ReqId = results.ReqId
                                                                 AND classSection.TermId = results.TermId
                                                                 AND classSection.ProgramVersionDefinitionId = results.ProgramVersionDefinitionId
                        WHERE      results.RowNumber = 1
                                   AND NOT EXISTS (
                                                  SELECT *
                                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                                  WHERE  GrdBkResultId = results.GrdBkResultId
                                                         AND classSection.CampusId = @CampusId
                                                  )
                        ORDER BY   results.GrdBkResultId;
            ALTER TABLE [FreedomAdvantage]..arGrdBkResults ENABLE TRIGGER ALL;

            PRINT 'removing dups for arresults';
            SELECT     results.ResultId
                      ,classSection.ClsSectionId AS ClsSectionId
                      ,results.Score
                      ,results.GrdSysDetailId
                      ,results.Cnt
                      ,results.Hours
                      ,results.StuEnrollId
                      ,results.IsInComplete
                      ,results.DroppedInAddDrop
                      ,results.ModUser
                      ,results.ModDate
                      ,results.IsTransfered
                      ,results.isClinicsSatisfied
                      ,results.DateDetermined
                      ,results.IsCourseCompleted
                      ,results.IsGradeOverridden
                      ,results.GradeOverriddenBy
                      ,results.GradeOverriddenDate
                      ,results.DateCompleted
                      ,ROW_NUMBER() OVER ( PARTITION BY results.StuEnrollId
                                                       ,classSection.ClsSectionId
                                                       ,results.ResultId
                                           ORDER BY results.StuEnrollId
                                                   ,results.DateCompleted DESC
                                         ) AS RowNumber
            INTO       #SortedArResults
            FROM       #arResults results
            INNER JOIN #ClassSection classSection ON classSection.ReqId = results.ReqId
                                                     AND classSection.TermId = results.TermId
                                                     AND classSection.ProgramVersionDefinitionId = results.ProgramVersionDefinitionId;
            --WHERE      results.Score IS NOT NULL;


            ALTER TABLE [FreedomAdvantage]..arResults DISABLE TRIGGER ALL;
            PRINT 'Inserting ArResults';
            INSERT INTO [FreedomAdvantage]..arResults (
                                                      ResultId
                                                     ,TestId
                                                     ,Score
                                                     ,GrdSysDetailId
                                                     ,Cnt
                                                     ,Hours
                                                     ,StuEnrollId
                                                     ,IsInComplete
                                                     ,DroppedInAddDrop
                                                     ,ModUser
                                                     ,ModDate
                                                     ,IsTransfered
                                                     ,isClinicsSatisfied
                                                     ,DateDetermined
                                                     ,IsCourseCompleted
                                                     ,IsGradeOverridden
                                                     ,GradeOverriddenBy
                                                     ,GradeOverriddenDate
                                                     ,DateCompleted
                                                      )
                        SELECT results.ResultId
                              ,results.ClsSectionId
                              ,results.Score
                              ,results.GrdSysDetailId
                              ,results.Cnt
                              ,results.Hours
                              ,results.StuEnrollId
                              ,results.IsInComplete
                              ,results.DroppedInAddDrop
                              ,results.ModUser
                              ,results.ModDate
                              ,results.IsTransfered
                              ,results.isClinicsSatisfied
                              ,results.DateDetermined
                              ,results.IsCourseCompleted
                              ,results.IsGradeOverridden
                              ,results.GradeOverriddenBy
                              ,results.GradeOverriddenDate
                              ,results.DateCompleted
                        FROM   #SortedArResults results
                        WHERE  results.RowNumber = 1
                               AND NOT EXISTS (
                                              SELECT *
                                              FROM   [FreedomAdvantage]..arResults
                                              WHERE  ResultId = results.ResultId
                                              );
            ALTER TABLE [FreedomAdvantage]..arResults ENABLE TRIGGER ALL;

            PRINT 'inserting missing grades';
            ALTER TABLE [FreedomAdvantage]..arResults DISABLE TRIGGER ALL;


            SELECT CS.ClsSectionId
                  ,TermId
                  ,ReqId
                  ,ProgramVersionDefinitionId
                  ,ROW_NUMBER() OVER ( PARTITION BY CS.TermId
                                                   ,CS.ReqId
                                       ORDER BY CS.TermId
                                     ) AS rn
            INTO   #NotRepeatClassSection
            FROM   [FreedomAdvantage]..arClassSections CS;

            SELECT NEWID() AS resultid
                  ,M.*
            INTO   #missingResults
            FROM   (
                   SELECT     DISTINCT classSections.ClsSectionId AS ClsSectionId
                                      ,CAST(NULL AS DECIMAL(18, 2)) AS Score            -- Score - decimal(18, 2)
                                      ,CAST(NULL AS UNIQUEIDENTIFIER) AS GrdSysDetailId -- GrdSysDetailId - uniqueidentifier
                                      ,0 AS cnt                                         -- Cnt - int
                                      ,0 AS Hours                                       -- Hours - int
                                      ,enrollments.StuEnrollId AS StuEnrollId
                                      ,CAST(NULL AS BIT) AS IsInComplete                -- IsInComplete - bit
                                      ,CAST(NULL AS BIT) AS DroppedInAddDrop            -- DroppedInAddDrop - bit
                                      ,CAST('sa' AS VARCHAR(50)) AS ModUser             -- ModUser - varchar(50)
                                      ,GETDATE() AS moddate                             -- ModDate - datetime
                                      ,CAST(NULL AS BIT) AS istransfered                -- IsTransfered - bit
                                      ,CAST(NULL AS BIT) AS isClinicsSatisfied          -- isClinicsSatisfied - bit
                                      ,GETDATE() AS datedetermined                      -- DateDetermined - datetime
                                      ,CAST(0 AS BIT) AS IsCourseCompleted              -- IsCourseCompleted - bit
                                      ,CAST(0 AS BIT) AS IsGradeOverridden              -- IsGradeOverridden - bit
                                      ,CAST(NULL AS VARCHAR(50)) AS GradeOverriddenBy   -- GradeOverriddenBy - varchar(50)
                                      ,CAST(NULL AS DATETIME) AS GradeOverriddenDate    -- GradeOverriddenDate - datetime
                                      ,CAST(NULL AS DATETIME) AS DateCompleted          -- DateCompleted - datetime
                   FROM       [FreedomAdvantage]..arStuEnrollments enrollments
                   LEFT JOIN  bridge.arStuEnrollments BE ON BE.trg_StuEnrollId = enrollments.StuEnrollId
                   INNER JOIN [FreedomAdvantage]..arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                   INNER JOIN [FreedomAdvantage]..arProgVerDef programVersionDefinition ON programVersionDefinition.PrgVerId = programVersion.PrgVerId
                   INNER JOIN [FreedomAdvantage]..arReqs courses ON courses.ReqId = programVersionDefinition.ReqId
                   INNER JOIN #NotRepeatClassSection classSections ON classSections.ReqId = courses.ReqId
                                                                      AND classSections.ProgramVersionDefinitionId = programVersionDefinition.ProgVerDefId
                                                                      AND rn = 1
                   LEFT JOIN  [FreedomAdvantage]..arResults missingResulstByClassSection ON missingResulstByClassSection.StuEnrollId = enrollments.StuEnrollId
                                                                                            AND missingResulstByClassSection.TestId = classSections.ClsSectionId
                   WHERE      missingResulstByClassSection.ResultId IS NULL
                              AND programVersion.CampGrpId = @CampGrpId
                              AND NOT EXISTS (
                                             SELECT 1
                                             FROM   [FreedomAdvantage]..arResults C
                                             WHERE  C.TestId = classSections.ClsSectionId
                                                    AND C.StuEnrollId = enrollments.StuEnrollId
                                             )
                   ) M;




            INSERT INTO [FreedomAdvantage]..arResults (
                                                      ResultId
                                                     ,TestId
                                                     ,Score
                                                     ,GrdSysDetailId
                                                     ,Cnt
                                                     ,Hours
                                                     ,StuEnrollId
                                                     ,IsInComplete
                                                     ,DroppedInAddDrop
                                                     ,ModUser
                                                     ,ModDate
                                                     ,IsTransfered
                                                     ,isClinicsSatisfied
                                                     ,DateDetermined
                                                     ,IsCourseCompleted
                                                     ,IsGradeOverridden
                                                     ,GradeOverriddenBy
                                                     ,GradeOverriddenDate
                                                     ,DateCompleted
                                                      )
                        SELECT *
                        FROM   #missingResults;
            ALTER TABLE [FreedomAdvantage]..arResults ENABLE TRIGGER ALL;

            DROP TABLE #gradeBookResults;
            DROP TABLE #arResults;
            DROP TABLE #ProgramVersionLength;
            DROP TABLE #ClassSection;
            DROP TABLE #SortedArResults;
            DROP TABLE #SortedGradeBookResults;
            DROP TABLE #Settings;
            --ROLLBACK TRANSACTION;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;




        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixesClassSections;
                PRINT 'Transaction was committed ';
            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixesClassSections;
                PRINT 'Transaction was rolled back';
            END;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[FixClassSections]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];
    END;





GO
