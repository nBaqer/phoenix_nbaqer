SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-03-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adSourceCatagory]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT 'Ad Category' AS SourceCatagoryDescrip -- varchar(50)
             , dStatus.StatusId AS StatusId           -- uniqueidentifier
             , 'AC' AS SourceCatagoryCode             -- varchar(50)
             , NEWID() AS SourceCatagoryId            -- uniqueidentifier
             , 'sa' AS ModUser                        -- varchar(50)
             , GETDATE() AS ModDate                   -- datetime
             , dCampGrp.CampGrpId AS CampGrpId        -- datetime
        INTO   #SourceCategory
        FROM   FreedomAdvantage..syStatuses dStatus
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adSourceCatagory dSourceCat ON dSourceCat.CampGrpId = dCampGrp.CampGrpId
                                                                   AND dSourceCat.SourceCatagoryDescrip = 'Ad Category'
        WHERE  dStatus.Status = 'Active'
               AND dSourceCat.SourceCatagoryId IS NULL;

        ALTER TABLE FreedomAdvantage..adSourceCatagory DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adSourceCatagory
                    SELECT tSourceCat.SourceCatagoryDescrip
                         , tSourceCat.StatusId
                         , tSourceCat.SourceCatagoryCode
                         , tSourceCat.SourceCatagoryId
                         , tSourceCat.ModUser
                         , tSourceCat.ModDate
                         , tSourceCat.CampGrpId
                    FROM   #SourceCategory tSourceCat;

        ALTER TABLE FreedomAdvantage..adSourceCatagory ENABLE TRIGGER ALL;

        DROP TABLE #SourceCategory;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adSourceCatagory]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
