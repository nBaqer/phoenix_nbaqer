SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [bridge].[usp_UpsertSchoolInformation]
    @InitialCampus BIT --is initial Campus
   ,@FreedomDbName VARCHAR(50) --freedom db name
   ,@BasePath VARCHAR(1000) --freedom data location
   ,@Backupname VARCHAR(1000) --freedom back up name
   ,@SchoolName VARCHAR(50) --client name
   ,@CampusName VARCHAR(50) --campus name
   ,@SchoolCode INT, --school code
   @AdvantageDbName VARCHAR(50)
   ,@UseAcademicProbationFromFreedom BIT = 0 
   ,@AdvantageBackupLocation VARCHAR(1000)
   ,@ConversionStructureFolder VARCHAR(1000) = 'C:\SSIS\DB\FreedomBridge'
AS
    DECLARE @Path VARCHAR(1000) = @BasePath + '\' + @Backupname;
	DECLARE @ConversionStructureFolderPath VARCHAR(1000) = @ConversionStructureFolder + '\';
    DECLARE @Table TABLE
        (
            LogicalName VARCHAR(128)
           ,[PhysicalName] VARCHAR(128)
           ,[Type] VARCHAR
           ,[FileGroupName] VARCHAR(128)
           ,[Size] VARCHAR(128)
           ,[MaxSize] VARCHAR(128)
           ,[FileId] VARCHAR(128)
           ,[CreateLSN] VARCHAR(128)
           ,[DropLSN] VARCHAR(128)
           ,[UniqueId] VARCHAR(128)
           ,[ReadOnlyLSN] VARCHAR(128)
           ,[ReadWriteLSN] VARCHAR(128)
           ,[BackupSizeInBytes] VARCHAR(128)
           ,[SourceBlockSize] VARCHAR(128)
           ,[FileGroupId] VARCHAR(128)
           ,[LogGroupGUID] VARCHAR(128)
           ,[DifferentialBaseLSN] VARCHAR(128)
           ,[DifferentialBaseGUID] VARCHAR(128)
           ,[IsReadOnly] VARCHAR(128)
           ,[IsPresent] VARCHAR(128)
           ,[TDEThumbprint] VARCHAR(128)
           ,[SnapshotUrl] VARCHAR(128)
        );
    DECLARE @LogicalNameData VARCHAR(128)
           ,@LogicalNameLog VARCHAR(128)
		   , @AdvLogicalNameData VARCHAR(128)
           ,@AdvLogicalNameLog VARCHAR(128),
		   @AdvLogicalNameDocument VARCHAR(128);
    INSERT INTO @Table
    EXEC ( '
RESTORE FILELISTONLY 
   FROM DISK=''' + @Path + '''
   '     );

    SET @LogicalNameData = (
                           SELECT TOP 1 LogicalName
                           FROM   @Table
                           WHERE  Type = 'D'
                           );
    SET @LogicalNameLog = (
                          SELECT TOP 1 LogicalName
                          FROM   @Table
                          WHERE  Type = 'L'
                          );

						  DELETE FROM @Table

						      INSERT INTO @Table
    EXEC ( '
RESTORE FILELISTONLY 
   FROM DISK=''' + @AdvantageBackupLocation + '''
   '     );

    SET @AdvLogicalNameData = (
                           SELECT TOP 1 LogicalName
                           FROM   @Table
                           WHERE  Type = 'D'
                           );
    SET @AdvLogicalNameLog = (
                          SELECT TOP 1 LogicalName
                          FROM   @Table
                          WHERE  Type = 'L'
                          );

						   SET @AdvLogicalNameDocument = (
                          SELECT TOP 1 LogicalName
                          FROM   @Table
                          WHERE  Type = 'D' AND FileGroupName = 'Document'
                          );



    --disables all dbs
    UPDATE dbo.SourceDB
    SET    IsDisabled = 1;
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.SourceDB
                  WHERE  DBName = @FreedomDbName
                  )
        BEGIN


            INSERT INTO dbo.SourceDB (
                                     DBName
                                    ,CampusName
                                    ,ClientName
                                    ,RestoreScript
                                    ,DataFolder
                                    ,IsDisabled
                                    ,SchoolID
                                    ,IsClockHour
                                    ,ForceACleanDB
                                    ,ExecutionStart
                                    ,EmailDomain
									,SchoolUsesAcademicProbationFromFreedom
									,AdvantageDb
									,AdvantageRestoreScript
                                     )
            VALUES ( @FreedomDbName, @CampusName, @SchoolName
                    ,'
USE [master]
DECLARE @kill VARCHAR(8000) = '''';
SELECT  @kill = @kill + ''kill '' + CONVERT(VARCHAR(5),spid) + '';''
FROM    master..sysprocesses
WHERE   dbid = DB_ID(''Freedom'');
EXEC (@kill);

GO

RESTORE DATABASE [Freedom] 
FROM  DISK = N''' + @Path + ''' WITH REPLACE, FILE = 1,
MOVE N''' + @LogicalNameData + ''' TO N'''+@ConversionStructureFolderPath+'Freedom.mdf'',
MOVE N''' + @LogicalNameLog + ''' TO N'''++@ConversionStructureFolderPath+'Freedom.ldf'',
NOUNLOAD,  STATS = 5


'  , @BasePath, 0, @SchoolCode, 1, @InitialCampus, NULL, NULL, @UseAcademicProbationFromFreedom,@AdvantageDbName,  '
USE [master]
DECLARE @kill VARCHAR(8000) = '''';
SELECT  @kill = @kill + ''kill '' + CONVERT(VARCHAR(5),spid) + '';''
FROM    master..sysprocesses
WHERE   dbid = DB_ID('''+@AdvantageDbName+''');
EXEC (@kill);

GO

RESTORE DATABASE ['+@AdvantageDbName+'] 
FROM  DISK = N''' + @AdvantageBackupLocation + ''' WITH REPLACE, FILE = 1,
MOVE N''' + @AdvLogicalNameData + ''' TO N''' + @ConversionStructureFolderPath+@AdvantageDbName+'.mdf'',
MOVE N''' + @AdvLogicalNameLog + ''' TO N'''+ @ConversionStructureFolderPath+@AdvantageDbName+'.ldf'',
MOVE N''' + @AdvLogicalNameDocument + ''' TO N'''+ @ConversionStructureFolderPath+@AdvantageDbName+'.ndf'',
NOUNLOAD,  STATS = 5


');
        END;
    ELSE
        BEGIN

            UPDATE dbo.SourceDB
            SET    ForceACleanDB = @InitialCampus
                  ,IsDisabled = 0
				  ,SchoolUsesAcademicProbationFromFreedom = @UseAcademicProbationFromFreedom
				  ,SchoolID  = @SchoolCode
				  ,AdvantageDb = @AdvantageDbName
				  ,AdvantageRestoreScript =  '
USE [master]
DECLARE @kill VARCHAR(8000) = '''';
SELECT  @kill = @kill + ''kill '' + CONVERT(VARCHAR(5),spid) + '';''
FROM    master..sysprocesses
WHERE   dbid = DB_ID('''+@AdvantageDbName+''');
EXEC (@kill);

GO

RESTORE DATABASE ['+@AdvantageDbName+'] 
FROM  DISK = N''' + @AdvantageBackupLocation + ''' WITH REPLACE, FILE = 1,
MOVE N''' + @AdvLogicalNameData + ''' TO N'''+@ConversionStructureFolderPath+@AdvantageDbName+'.mdf'',
MOVE N''' + @AdvLogicalNameLog + ''' TO N'''+@ConversionStructureFolderPath+@AdvantageDbName+'.ldf'',
MOVE N''' + @AdvLogicalNameDocument + ''' TO N'''+ @ConversionStructureFolderPath+@AdvantageDbName+'.ndf'',
NOUNLOAD,  STATS = 5


'
                  ,RestoreScript = '
USE [master]
DECLARE @kill VARCHAR(8000) = '''';
SELECT  @kill = @kill + ''kill '' + CONVERT(VARCHAR(5),spid) + '';''
FROM    master..sysprocesses
WHERE   dbid = DB_ID(''Freedom'');
EXEC (@kill);

GO

RESTORE DATABASE [Freedom] 
FROM  DISK = N''' + @Path + ''' WITH REPLACE, FILE = 1,
MOVE N''' +       @LogicalNameData + ''' TO N'''+@ConversionStructureFolderPath+'Freedom.mdf'',
MOVE N''' +       @LogicalNameLog  + ''' TO N'''+@ConversionStructureFolderPath+'Freedom.ldf'',
NOUNLOAD,  STATS = 5


'
            WHERE  DBName = @FreedomDbName;
        END;


GO
