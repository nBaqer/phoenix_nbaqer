SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arSAP]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT     CASE WHEN dSAP.SAPDescrip IS NULL THEN NEWID()
                        ELSE dSAP.SAPId
                   END AS SAPId                                                                                     -- uniqueidentifier
                 , CAST(bCampGrp.src_SchoolId AS VARCHAR(4)) + CAST(sSAPMaster.RecordKey AS VARCHAR(50)) AS SAPCode -- varchar(12)
                 , sSAPMaster.Descrip AS SAPDescrip                                                                 -- varchar(50)
                 , CAST(NULL AS UNIQUEIDENTIFIER) AS SAPCriteriaId                                                  -- uniqueidentifier
                 , NULL AS MinTermGPA                                                                               -- decimal
                 , NULL AS MinTermAv                                                                                -- decimal
                 , NULL AS TermGPAOver                                                                              -- decimal
                 , NULL AS TermAvOver                                                                               -- decimal
                 , dStatus.StatusId AS StatusId                                                                     -- uniqueidentifier
                 , 'sa' AS ModUser                                                                                  -- varchar(50)
                 , GETDATE() AS ModDate                                                                             -- datetime
                 , bTrigUnit.trg_TrigUnitTypId AS TrigUnitTypId                                                     -- tinyint
                 , bTrigOff.trg_TrigOffsetTypId AS TrigOffsetTypId                                                  -- tinyint
                 , bCampGrp.trg_syCampGrpId AS CampGrpId                                                            -- uniqueidentifier
                 , 0 AS TerminationProbationCnt                                                                     -- int
                 , 0 AS TrackExternAttendance                                                                       -- bit
                 , 0 AS IncludeTransferHours                                                                        -- bit
                 , 0 AS FaSapPolicy                                                                                 -- bit
                 , sSAPMaster.SAPDetailKey
                 , @CampusId AS CampusId
                 , sSAPMaster.RecordKey
        INTO       #SAP
        FROM       Freedom.dbo.SAPMASTER_DEF sSAPMaster
        INNER JOIN Freedom.dbo.SAPDETAIL_DEF sSAPDetail ON sSAPDetail.RecordKey = sSAPMaster.SAPDetailKey
        INNER JOIN FreedomBridge.bridge.arTrigUnitTyps bTrigUnit ON bTrigUnit.src_TrigUnitID = sSAPDetail.PrivTrigUnit
        INNER JOIN FreedomBridge.bridge.arTriggerOffsetTyps bTrigOff ON bTrigOff.src_TrigOffID = sSAPDetail.PrivTrigOffsetType
        INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomBridge.bridge.syCampGrps bCampGrp ON bCampGrp.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage.dbo.arSAP dSAP ON dSAP.CampGrpId = bCampGrp.trg_syCampGrpId
                                                      AND dSAP.SAPDescrip = sSAPMaster.Descrip;

        ALTER TABLE FreedomAdvantage..arSAP DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arSAP
                    SELECT    tSAP.SAPId
                            , tSAP.SAPCode
                            , tSAP.SAPDescrip
                            , tSAP.SAPCriteriaId
                            , tSAP.MinTermGPA
                            , tSAP.MinTermAv
                            , tSAP.TermGPAOver
                            , tSAP.TermAvOver
                            , tSAP.StatusId
                            , tSAP.ModUser
                            , tSAP.ModDate
                            , tSAP.TrigUnitTypId
                            , tSAP.TrigOffsetTypId
                            , tSAP.CampGrpId
                            , tSAP.TerminationProbationCnt
                            , tSAP.TrackExternAttendance
                            , tSAP.IncludeTransferHours
                            , tSAP.FaSapPolicy
							, NULL
                    FROM      #SAP tSAP
                    LEFT JOIN FreedomAdvantage..arSAP dSAP ON dSAP.CampGrpId = tSAP.CampGrpId
                                                              AND dSAP.SAPDescrip = tSAP.SAPDescrip
                                                              AND dSAP.SAPCode = tSAP.SAPCode
                    WHERE     dSAP.SAPDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..arSAP ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arSAP')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arSAP
                    (
                        trg_SAPId UNIQUEIDENTIFIER
                      , src_Descrip VARCHAR(255)
                      , src_RecordKey INT
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                      (
                          src_Descrip
                        , src_RecordKey
                      )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arSAP
                    SELECT    tSAP.SAPId
                            , tSAP.SAPDescrip
                            , tSAP.RecordKey
                            , tSAP.SAPCode
                            , @CampusId
                    FROM      #SAP tSAP
                    LEFT JOIN FreedomBridge.bridge.arSAP bSAP ON bSAP.trg_CampusId = tSAP.CampusId
                                                                 AND bSAP.src_Descrip = tSAP.SAPDescrip
                                                                 AND bSAP.src_RecordKey = tSAP.SAPCode
                    WHERE     bSAP.trg_SAPId IS NULL;

        DROP TABLE #SAP;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arSAP]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
