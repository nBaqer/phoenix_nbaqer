SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 01-06-15>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arClsSectMeetings]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN dClsSectMtg.ClsSectionId IS NULL THEN NEWID()
                    ELSE dClsSectMtg.ClsSectMeetingId
               END AS ClsSectMeetingId                          -- uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS WorkDaysId     -- uniqueidentifier
             , dRoom.RoomId AS RoomId                           -- uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS TimeIntervalId -- uniqueidentifier
             , bClsSect.trg_ClsSection
             , bClsSect.trg_ClsSectionId AS ClsSectionId        -- uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS EndIntervalId  -- uniqueidentifier
             , 'sa' AS ModUser                                  -- varchar(50)
             , GETDATE() AS ModDate                             -- datetime
             , bPeriod.trg_PeriodId AS PeriodId                 -- uniqueidentifier
             , bPeriod.trg_PeriodCode
             , bPeriod.src_Hours
             , CAST(NULL AS UNIQUEIDENTIFIER) AS AltPeriodId    -- uniqueidentifier
             , dClsSect.StartDate AS StartDate                  -- datetime
             , dClsSect.EndDate AS EndDate                      -- datetime
             , dInstType.InstructionTypeId AS InstructionTypeID -- uniqueidentifier
             , 0 AS IsMeetingRescheduled                        -- bit
             , 0 AS BreakDuration                               -- int
        INTO   #ClsSectMtg
        FROM   FreedomBridge.bridge.arClassSections bClsSect --FreedomBridge.bridge.syPeriods bPeriod
        INNER JOIN FreedomAdvantage..arRooms dRoom ON dRoom.Descrip = 'Room 1'
        INNER JOIN FreedomBridge.bridge.arRooms bRoom ON bRoom.trg_RoomId = dRoom.RoomId
                                                         AND bRoom.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..arBuildings dBuilding ON dBuilding.BldgId = dRoom.BldgId
                                                              AND dBuilding.CampusId = @CampusId
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = CAST(@SchoolID AS VARCHAR(50))
        INNER JOIN FreedomBridge.bridge.syPeriods bPeriod ON bPeriod.trg_PeriodCode = '9-5 M-U'
                                                             AND bPeriod.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..arClassSections dClsSect ON dClsSect.ClsSectionId = bClsSect.trg_ClsSectionId
        INNER JOIN FreedomAdvantage..arReqs dReq ON dReq.ReqId = dClsSect.ReqId
                                                    AND dReq.Descrip LIKE '%Attendance%'
        INNER JOIN FreedomAdvantage..arInstructionType dInstType ON dInstType.InstructionTypeDescrip = 'Theory'
        LEFT JOIN FreedomAdvantage..arClsSectMeetings dClsSectMtg ON dClsSectMtg.ClsSectionId = dClsSect.ClsSectionId
                                                                     AND dClsSectMtg.PeriodId = bPeriod.trg_PeriodId
                                                                     AND dClsSectMtg.RoomId = bRoom.trg_RoomId;

        ALTER TABLE FreedomAdvantage..arClsSectMeetings ENABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arClsSectMeetings
                    SELECT tClsSectMtg.ClsSectMeetingId
                         , tClsSectMtg.WorkDaysId
                         , tClsSectMtg.RoomId
                         , tClsSectMtg.TimeIntervalId
                         , tClsSectMtg.ClsSectionId
                         , tClsSectMtg.EndIntervalId
                         , tClsSectMtg.ModUser
                         , tClsSectMtg.ModDate
                         , tClsSectMtg.PeriodId
                         , tClsSectMtg.AltPeriodId
                         , tClsSectMtg.StartDate
                         , tClsSectMtg.EndDate
                         , tClsSectMtg.InstructionTypeID
                         , tClsSectMtg.IsMeetingRescheduled
                         , tClsSectMtg.BreakDuration
                    FROM   #ClsSectMtg tClsSectMtg
                    LEFT JOIN FreedomAdvantage..arClsSectMeetings dClsSectMtg ON dClsSectMtg.ClsSectionId = tClsSectMtg.ClsSectionId
                                                                                 AND dClsSectMtg.PeriodId = tClsSectMtg.PeriodId
                    WHERE  dClsSectMtg.ClsSectMeetingId IS NULL;

        ALTER TABLE FreedomAdvantage..arClsSectMeetings ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arClsSectMeetings')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arClsSectMeetings
                    (
                        trg_ClsSectMeetingId UNIQUEIDENTIFIER
                      , trg_ClsSectionId UNIQUEIDENTIFIER
                      , trg_PeriodId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_ClsSectMeetingId
                            , trg_ClsSectionId
                            , trg_PeriodId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arClsSectMeetings
                    SELECT tClsSectMtg.ClsSectMeetingId
                         , tClsSectMtg.ClsSectionId
                         , tClsSectMtg.PeriodId
                         , @CampusId AS CampusId
                    FROM   #ClsSectMtg tClsSectMtg
                    LEFT JOIN FreedomBridge.bridge.arClsSectMeetings bClsSectMtg ON bClsSectMtg.trg_ClsSectionId = tClsSectMtg.ClsSectionId
                                                                                    AND bClsSectMtg.trg_PeriodId = tClsSectMtg.PeriodId
                                                                                    AND bClsSectMtg.trg_CampusId = @CampusId
                    WHERE  bClsSectMtg.trg_ClsSectMeetingId IS NULL;

        DROP TABLE #Settings;
        DROP TABLE #ClsSectMtg;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arClsSectMeetings]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
