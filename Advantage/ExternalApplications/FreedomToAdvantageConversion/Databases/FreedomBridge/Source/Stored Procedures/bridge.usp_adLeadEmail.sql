SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=============================================
--Author:		<Author, Spencer Garrett>
--Create date: <Create Date, 11-11-14>
--=============================================
CREATE PROCEDURE [bridge].[usp_adLeadEmail]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #LeadEmail
        FROM   (
               SELECT     CASE WHEN dLeadEmail.LeadEMailId IS NULL THEN NEWID()
                               ELSE dLeadEmail.LeadEMailId
                          END AS LeadEmailId                 -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId         -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.Email)) AS Email -- varchar(100)
                         ,dEmail.EMailTypeId AS EmailTypeId  -- uniqueidentifier
                         ,1 AS IsPreferred                   -- bit
                         ,0 AS IsPortalUserName              -- bit
                         ,'sa' AS ModUser                    -- varchar(50)
                         ,GETDATE() AS ModDate               -- datetime
                         ,dStatus.StatusId AS StatusId       -- uniqueidentifier
                         ,1 AS IsShowOnLeadPage              -- bit
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.StartDate DESC
                                            ) AS LocalRank
               FROM       Freedom.dbo.STUDREC sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_StuRecordKey = sStud.RecordKey
               INNER JOIN FreedomAdvantage.dbo.syEmailType dEmail ON dEmail.EMailTypeCode = 'Home'
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.AdLeadEmail dLeadEmail ON dLeadEmail.EMail = sStud.Email
               WHERE      sStud.Email != ''
               GROUP BY   dLeadEmail.EMailTypeId
                         ,dLeadEmail.LeadEMailId
                         ,sStud.Email
                         ,dEmail.EMailTypeId
                         ,bLead.trg_LeadId
                         ,sStud.Email
                         ,dStatus.StatusId
                         ,sStud.StartDate
               UNION
               SELECT     CASE WHEN dLeadEmail.LeadEMailId IS NULL THEN NEWID()
                               ELSE dLeadEmail.LeadEMailId
                          END AS LeadEmailId                 -- uniqueidentifier
                         ,bLead.trg_LeadId AS LeadId         -- uniqueidentifier
                         ,LTRIM(RTRIM(sStud.EMail)) AS Email -- varchar(100)
                         ,dEmail.EMailTypeId AS EmailTypeId  -- uniqueidentifier
                         ,0 AS IsPreferred                   -- bit
                         ,0 AS IsPortalUserName              -- bit
                         ,'sa' AS ModUser                    -- varchar(50)
                         ,GETDATE() AS ModDate               -- datetime
                         ,dStatus.StatusId AS StatusId       -- uniqueidentifier
                         ,0 AS IsShowOnLeadPage              -- bit
                         ,@CampusId AS CampusId
                         ,DENSE_RANK() OVER ( PARTITION BY bLead.trg_LeadId
                                              ORDER BY sStud.EMail
                                            ) AS LocalRank
               FROM       Freedom.dbo.PROSPECT_FIL sStud
               INNER JOIN FreedomBridge.bridge.adLeads bLead ON bLead.src_ProRecordKey = sStud.RecordKey
               INNER JOIN FreedomAdvantage.dbo.syEmailType dEmail ON dEmail.EMailTypeCode = 'Home'
               INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.StatusCode = 'A'
               LEFT JOIN  FreedomAdvantage.dbo.AdLeadEmail dLeadEmail ON dLeadEmail.EMail = sStud.EMail
               WHERE      sStud.EMail != ''
                          AND sStud.EMail NOT IN (
                                                 SELECT Email
                                                 FROM   Freedom.dbo.STUDREC
                                                 )
               GROUP BY   dLeadEmail.EMailTypeId
                         ,dLeadEmail.LeadEMailId
                         ,sStud.EMail
                         ,dEmail.EMailTypeId
                         ,bLead.trg_LeadId
                         ,sStud.EMail
                         ,dStatus.StatusId
               ) AS leadEmail;

			   

        ALTER TABLE FreedomAdvantage..AdLeadEmail DISABLE TRIGGER ALL;

        SELECT NEWID() AS LeadEmailId
              ,F.*
        INTO   #FinalLeadEmails
        FROM   (
               SELECT DISTINCT LeadId
                              ,Email
                              ,EmailTypeId
                              ,CASE WHEN LocalRank = 1
                                         AND IsPreferred = 1 THEN 1
                                    ELSE 0
                               END AS IsPreferred
                              ,IsPortalUserName
                              ,ModUser
                              ,ModDate
                              ,StatusId
                              ,IsShowOnLeadPage
                              ,CampusId
               FROM   #LeadEmail
               ) F;

        SELECT   LeadId
                ,Email
        INTO     #DupeEmails
        FROM     #FinalLeadEmails
        GROUP BY LeadId
                ,Email
        HAVING   COUNT(*) > 1;


        SELECT   E.LeadEmailId
        INTO     #DupesToDelete
        FROM     #FinalLeadEmails E
        WHERE    Email IN (
                          SELECT D.Email
                          FROM   #DupeEmails D
                          )
                 AND EXISTS (
                            SELECT 1
                            FROM   #FinalLeadEmails C
                            WHERE  C.Email = E.Email
                                   AND C.LeadId = E.LeadId
                                   AND C.IsPreferred = 1
                            )
                 AND E.IsPreferred = 0
        ORDER BY E.Email;

        DELETE FROM #FinalLeadEmails
        WHERE LeadEmailId IN (
                             SELECT *
                             FROM   #DupesToDelete
                             );



        INSERT INTO FreedomAdvantage.dbo.AdLeadEmail
                    SELECT    tLeadEmail.LeadEmailId
                             ,tLeadEmail.LeadId
                             ,tLeadEmail.Email
                             ,tLeadEmail.EmailTypeId
                             ,tLeadEmail.IsPreferred
                             ,tLeadEmail.IsPortalUserName
                             ,tLeadEmail.ModUser
                             ,tLeadEmail.ModDate
                             ,tLeadEmail.StatusId
                             ,tLeadEmail.IsShowOnLeadPage
                    FROM      #FinalLeadEmails tLeadEmail
                    LEFT JOIN FreedomAdvantage.dbo.AdLeadEmail dLeadEmail ON dLeadEmail.LeadEMailId = tLeadEmail.LeadEmailId
                                                                             AND dLeadEmail.EMail = tLeadEmail.Email
                    WHERE     NOT EXISTS (
                                         SELECT TOP 1 *
                                         FROM   FreedomAdvantage..AdLeadEmail T
                                         WHERE  T.LeadEMailId = tLeadEmail.LeadEmailId
                                         );

        ALTER TABLE FreedomAdvantage.dbo.AdLeadEmail ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.adLeadEmail
                    SELECT    tLeadEmail.LeadEmailId
                             ,tLeadEmail.Email
                             ,tLeadEmail.LeadId
                             ,@CampusId AS CampusId
                    FROM      #FinalLeadEmails tLeadEmail
                    LEFT JOIN FreedomBridge.bridge.adLeadEmail bLeadEmail ON bLeadEmail.trg_adLeadEmailId = tLeadEmail.LeadEmailId
                                                                             AND bLeadEmail.src_Email = tLeadEmail.Email;

        DROP TABLE #FinalLeadEmails;
        DROP TABLE #LeadEmail;
        DROP TABLE #Settings;
        DROP TABLE #DupeEmails;
        DROP TABLE #DupesToDelete;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_adLeadEmail]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
