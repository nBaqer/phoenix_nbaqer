SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jose Astudillo
-- Create date: 9/28/18
-- Description:	Update IsRequired and weight fields
-- =============================================
CREATE PROCEDURE [bridge].[usp_arProgVerDefUpdate]
AS
    BEGIN
        BEGIN TRANSACTION;
        UPDATE PVD
        SET    PVD.CourseWeight = CASE WHEN R.Code = 'attendance' OR S.Weight IN (0) THEN 0
                                        ELSE S.Weight
                                  END
              ,PVD.IsRequired = CASE WHEN R.Code = 'attendance' THEN 0
                                     ELSE 1
                                END
        FROM   FreedomAdvantage..arProgVerDef PVD
        JOIN   bridge.arProgVerDef B ON PVD.PrgVerId = B.trg_PrgVerId
                                        AND PVD.ProgVerDefId = B.trg_ProgVerDefId
                                        AND PVD.ReqId = B.trg_ReqId
        JOIN   Freedom..SUBJECT_DEF S ON B.FreedomSubjectDefRecordKey = S.RecordKey
        JOIN   FreedomAdvantage..arReqs R ON PVD.ReqId = R.ReqId;

		--not needd anymore prog version sproc set it up already
        --UPDATE FreedomAdvantage..arPrgVersions
        --SET    DoCourseWeightOverallGPA = 1;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arProgVerDefUpdate]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;
    END;
GO
