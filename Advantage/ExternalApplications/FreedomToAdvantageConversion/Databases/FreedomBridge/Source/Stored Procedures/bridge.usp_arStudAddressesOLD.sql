SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 11-11-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arStudAddressesOLD]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN bStuAdd.trg_StdAddressId IS NULL THEN NEWID()
                    ELSE bStuAdd.trg_StdAddressId
               END AS StdAddressId                                               -- uniqueidentifier
             , bStud.trg_StudentId AS StudentId                                  -- uniqueidentifier
             , sStud.AddressLine1 AS Address1                                    -- varchar(50)
             , NULL AS Address2                                                  -- varchar(50)
             , sStud.AddressCity AS City                                         -- varchar(50)
             , dState.StateId AS StateId                                         -- uniqueidentifier
             , FreedomBridge.dbo.udf_GetNumericOnly(sStud.PrivAddressZip) AS Zip -- varchar(50)
             , dCountry.CountryId AS CountryId                                   -- uniqueidentifier
             , dAdd.AddressTypeId AS AddressTypeId                               -- uniqueidentifier
             , 'sa' AS ModUser                                                   -- varchar(50)
             , GETDATE() AS ModDate                                              -- datetime
             , dStat.StatusId AS StatusId                                        -- uniqueidentifier
             , 1 AS default1                                                     -- bit
             , 0 AS ForeignZip                                                   -- bit
             , NULL AS OtherState                                                -- varchar(50)
        INTO   #StuAdd
        FROM   Freedom..STUDREC sStud
        INNER JOIN FreedomBridge.bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                              AND bStud.trg_CampusId = @CampusId
        INNER JOIN FreedomAdvantage..syStates dState ON dState.StateCode = sStud.AddressState
        INNER JOIN FreedomAdvantage..adCountries dCountry ON dCountry.CountryDescrip = 'USA'
        INNER JOIN FreedomAdvantage..plAddressTypes dAdd ON dAdd.AddressDescrip = 'Residence'
        INNER JOIN FreedomAdvantage..syStatuses dStat ON dStat.Status = 'Active'
        --LEFT JOIN FreedomAdvantage..arStudAddressesOLD dStuAdd ON dStuAdd.Address1 = sStud.AddressLine1
        --                                                       AND sStud.AddressLine1 NOT IN ( '','N/A' )
        LEFT JOIN bridge.arStudAddressesOLD bStuAdd ON bStuAdd.src_Address1 = sStud.AddressLine1
                                                       AND sStud.AddressLine1 NOT IN ( '', 'N/A' )
                                                       AND bStuAdd.trg_StudentId = bStud.trg_StudentId
        WHERE  bStud.LocalRank = 1;

       -- ALTER TABLE FreedomAdvantage..arStudAddressesOld DISABLE TRIGGER ALL;

        --INSERT INTO FreedomAdvantage..arStudAddressesOld
        --            SELECT tStuAdd.StdAddressId
        --                 , tStuAdd.StudentId
        --                 , tStuAdd.Address1
        --                 , tStuAdd.Address2
        --                 , tStuAdd.City
        --                 , tStuAdd.StateId
        --                 , tStuAdd.Zip
        --                 , tStuAdd.CountryId
        --                 , tStuAdd.AddressTypeId
        --                 , tStuAdd.ModUser
        --                 , tStuAdd.ModDate
        --                 , tStuAdd.StatusId
        --                 , tStuAdd.default1
        --                 , tStuAdd.ForeignZip
        --                 , tStuAdd.OtherState
        --            FROM   #StuAdd tStuAdd
        --            LEFT JOIN FreedomAdvantage..arStudAddressesOld dStuAdd ON dStuAdd.Address1 = tStuAdd.Address1
        --                                                                      AND dStuAdd.StudentId = tStuAdd.StudentId
        --            WHERE  dStuAdd.Address1 IS NULL;

      --  ALTER TABLE FreedomAdvantage..arStudAddressesOld ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arStudAddressesOLD')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arStudAddressesOLD
                    (
                        trg_StdAddressId UNIQUEIDENTIFIER
                      , src_Address1 VARCHAR(255)
                      , trg_StudentId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_Address1
                            , trg_StudentId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arStudAddressesOLD
                    SELECT tStuAdd.StdAddressId
                         , tStuAdd.Address1
                         , tStuAdd.StudentId
                         , @CampusId
                    FROM   #StuAdd tStuAdd
                    LEFT JOIN FreedomBridge.bridge.arStudAddressesOLD bStuAdd ON bStuAdd.src_Address1 = tStuAdd.Address1
                                                                                 AND bStuAdd.trg_StudentId = tStuAdd.StudentId
                    WHERE  bStuAdd.trg_StdAddressId IS NULL;

        DROP TABLE #StuAdd;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arStudAddressesOLD]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
