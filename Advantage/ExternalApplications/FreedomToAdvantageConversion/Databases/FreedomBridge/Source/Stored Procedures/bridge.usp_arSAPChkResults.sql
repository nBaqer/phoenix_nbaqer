SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 04-16-18>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arSAPChkResults]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        DECLARE @Comments TABLE
            (
                ParentKey INT NOT NULL
               ,Text NVARCHAR(MAX)
               ,NextRec INT NOT NULL
            );

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        INSERT INTO @Comments (
                              ParentKey
                             ,Text
                             ,NextRec
                              )
                    SELECT   ParentKey
					 ,Text
                            ,CASE NextRec
                                  WHEN 0 THEN 999999
                                  ELSE NextRec
                             END
                           
                    FROM     Freedom..ACADCHK_LOG_CMT
                    ORDER BY ParentKey, CASE NextRec
                                  WHEN 0 THEN 999999
                                  ELSE NextRec
                             END;

            SELECT   DISTINCT ParentKey
                         ,( STUFF((
                                  SELECT CAST(', ' + Text AS VARCHAR(MAX))
                                  FROM   @Comments C
                                  WHERE  ( C.ParentKey = E.ParentKey )
                                  FOR XML PATH('')
                                  )
                                 ,1
                                 ,2
                                 ,''
                                 )
                          ) AS Text
        INTO     #Comments
        FROM     @Comments E
        GROUP BY ParentKey
                ,NextRec;

        SELECT     CASE WHEN dSAPChkRes.StdRecKey IS NULL THEN NEWID()
                        ELSE dSAPChkRes.StdRecKey
                   END AS StdRecKey                          -- uniqueidentifier
                  ,bStuEnroll.trg_StuEnrollId AS StuEnrollId -- uniqueidentifier
                  ,bStuEnroll.src_RecordKey AS StudRecKey
                  ,sSAPChk.RecordKey
                  ,sSAPChk.Sequence AS Period                -- int
                  ,CASE WHEN sSAPChk.PrivResult IN ( 1, 0 ) THEN 1
                        ELSE 0
                   END AS IsMakingSAP                        -- bit
                  ,CASE WHEN sSAPChk.PrivTrigUnit < 4 THEN sSAPChk.HrsAttempted
                        ELSE 0
                   END AS HrsAttended                        -- int
                  ,CASE WHEN sSAPChk.PrivTrigUnit < 4 THEN sSAPChk.HrsEarned
                        ELSE 0
                   END AS HrsEarned                          -- int
                  ,CASE WHEN sSAPChk.PrivTrigUnit > 3 THEN sSAPChk.HrsAttempted
                        ELSE 0
                   END AS CredsAttempted                     -- decimal(18, 2)
                  ,CASE WHEN sSAPChk.PrivTrigUnit > 3 THEN sSAPChk.HrsEarned
                        ELSE 0
                   END AS CredsEarned                        -- decimal(18, 2)
                  ,sSAPChk.GPA AS GPA                        -- decimal(18, 2)
                  ,sSAPChk.AddDate AS DatePerformed          -- datetime
                  ,sSAPChkCmt.Text AS Comments               -- varchar(max)
                  ,'sa' AS ModUser                           -- varchar(50)
                  ,GETDATE() AS ModDate                      -- datetime
                  ,bSAPDetail.trg_SAPDetailId AS SAPDetailId -- uniqueidentifier
                  ,NULL AS Average                           -- decimal(18, 2)
                  ,NULL AS Attendance                        -- decimal(19, 2)
                  ,NULL AS CumFinAidCredits                  -- decimal(19, 2)
                  ,CASE WHEN sSAPChk.HrsAttempted < 1 THEN 0
                        ELSE sSAPChk.HrsEarned / sSAPChk.HrsAttempted * 100
                   END AS PercentCompleted                   -- decimal(19, 2)
                  ,sSAPChk.EffDate AS CheckPointDate         -- datetime
                  ,NULL AS TermStartDate                     -- datetime
                  ,0 AS PreviewSapCheck                      -- bit
        INTO       #SapChkResults
        FROM       Freedom.dbo.ACADCHK_LOG sSAPChk
        LEFT JOIN  #Comments sSAPChkCmt ON sSAPChkCmt.ParentKey = sSAPChk.RecordKey
        INNER JOIN Freedom.dbo.SAPDETAIL_DEF sSAPDetail ON sSAPDetail.ParentKey = sSAPChk.SapParentkey
                                                           AND sSAPDetail.Sequence = sSAPChk.Sequence
                                                           AND sSAPChk.PrivType = 1
                                                           AND sSAPChk.PrivResult IN ( 1, 2, 3, 0 )
                                                           -- AND sSAPChk.PrintId NOT IN ( 0, 30000 )
                                                           AND sSAPChk.StudRecKey NOT IN ( 0 )
        INNER JOIN FreedomBridge.bridge.arSAPDetails bSAPDetail ON bSAPDetail.src_RecordKey = sSAPDetail.RecordKey
                                                                   AND bSAPDetail.src_RecordKey = sSAPChk.Period
                                                                   AND bSAPDetail.CampusID = @CampusId
        INNER JOIN Freedom.dbo.STUDREC sStu ON sStu.RecordKey = sSAPChk.StudRecKey
        INNER JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = sStu.RecordKey
                                                                       AND bStuEnroll.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage.dbo.arSAPChkResults dSAPChkRes ON dSAPChkRes.StuEnrollId = bStuEnroll.trg_StuEnrollId
                                                                      AND dSAPChkRes.Period = sSAPChk.Period
                                                                      AND dSAPChkRes.GPA = sSAPChk.GPA;
        --WHERE freedomPrograms.TitleIVEligible = 0; /*This condition will exclude all the results that are for title iv sap check results, those results will be inserted into a different table*/

        SELECT *
              ,DENSE_RANK() OVER ( PARTITION BY StuEnrollId
                                               ,Period
                                   ORDER BY DatePerformed DESC
                                 ) AS LatestRecord
        INTO   #SapChkResults_Final
        FROM   #SapChkResults;


        ALTER TABLE FreedomAdvantage.dbo.arSAPChkResults DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage.dbo.arSAPChkResults
                    SELECT    tSAPChkRes.StdRecKey
                             ,tSAPChkRes.StuEnrollId
                             ,tSAPChkRes.Period
                             ,tSAPChkRes.IsMakingSAP
                             ,tSAPChkRes.HrsAttended
                             ,tSAPChkRes.HrsEarned
                             ,tSAPChkRes.CredsAttempted
                             ,tSAPChkRes.CredsEarned
                             ,tSAPChkRes.GPA
                             ,tSAPChkRes.DatePerformed
                             ,tSAPChkRes.Comments
                             ,tSAPChkRes.ModUser
                             ,tSAPChkRes.ModDate
                             ,tSAPChkRes.SAPDetailId
                             ,tSAPChkRes.Average
                             ,tSAPChkRes.Attendance
                             ,tSAPChkRes.CumFinAidCredits
                             ,tSAPChkRes.PercentCompleted
                             ,tSAPChkRes.CheckPointDate
                             ,tSAPChkRes.TermStartDate
                             ,tSAPChkRes.PreviewSapCheck
                    FROM      #SapChkResults_Final tSAPChkRes
                    LEFT JOIN FreedomAdvantage.dbo.arSAPChkResults dSAPChkRes ON dSAPChkRes.StuEnrollId = tSAPChkRes.StuEnrollId
                                                                                 AND dSAPChkRes.Period = tSAPChkRes.Period
                    WHERE     tSAPChkRes.LatestRecord = 1;

        ALTER TABLE FreedomAdvantage.dbo.arSAPChkResults ENABLE TRIGGER ALL;

        INSERT INTO FreedomBridge.bridge.arSAPChkResults
                    SELECT    tSAPChkRes.Period
                             ,tSAPChkRes.RecordKey
                             ,tSAPChkRes.StudRecKey
                             ,tSAPChkRes.StdRecKey
                             ,tSAPChkRes.StuEnrollId
                             ,@CampusId AS CampusId
                    FROM      #SapChkResults_Final tSAPChkRes
                    LEFT JOIN FreedomBridge.bridge.arSAPChkResults bSAPChkRes ON bSAPChkRes.trg_StuEnrollId = tSAPChkRes.StuEnrollId
                                                                                 AND bSAPChkRes.src_Period = tSAPChkRes.Period
                                                                                 AND bSAPChkRes.src_RecordKey = tSAPChkRes.RecordKey
                                                                                 AND bSAPChkRes.trg_CampusId = @CampusId
                    WHERE     tSAPChkRes.LatestRecord = 1;

        DROP TABLE #Settings;
        DROP TABLE #SapChkResults;
        DROP TABLE #Comments;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arSAPChkResults]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;


GO
