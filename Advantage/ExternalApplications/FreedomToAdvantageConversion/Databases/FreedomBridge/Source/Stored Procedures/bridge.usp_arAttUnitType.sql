SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-21-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arAttUnitType]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT dAttUnitType.UnitTypeId AS UnitTypeId -- uniqueidentifier
             , FLook.Description AS UnitTypeDescrip  -- varchar(50)
             , FLook.ID
             , dAttUnitType.UnitTypeDescrip AS dUnitTypeDescrip
        INTO   #AttUnitType
        FROM   FreedomBridge.stage.FLookUp FLook
        LEFT JOIN FreedomAdvantage..arAttUnitType dAttUnitType ON dAttUnitType.UnitTypeDescrip = CASE WHEN FLook.ID = 1 THEN 'Clock Hours'
                                                                                                      WHEN FLook.ID = 2 THEN 'Present Absent'
                                                                                                      WHEN FLook.ID = 3 THEN 'None'
                                                                                                 END
        WHERE  FLook.LookupName = 'AttendanceType';

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   FreedomBridge.sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arAttUnitType')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arAttUnitType
                    (
                        trg_UnitTypeId UNIQUEIDENTIFIER
                      , src_AttendanceTypeDescription VARCHAR(255)
                      , trg_Description VARCHAR(50)
                      , src_AttendanceTypeID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_AttendanceTypeDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arAttUnitType
                    SELECT tAttUnitType.UnitTypeId
                         , tAttUnitType.UnitTypeDescrip
                         , tAttUnitType.dUnitTypeDescrip
                         , tAttUnitType.ID
                    FROM   #AttUnitType tAttUnitType
                    LEFT JOIN FreedomBridge.bridge.arAttUnitType bAttUnitType ON bAttUnitType.src_AttendanceTypeDescription = tAttUnitType.UnitTypeDescrip
                                                                                 AND bAttUnitType.src_AttendanceTypeID = tAttUnitType.ID
                    WHERE  bAttUnitType.trg_UnitTypeId IS NULL;

        DROP TABLE #AttUnitType;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arAttUnitType]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
