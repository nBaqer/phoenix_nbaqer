SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_arStudentOLD]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        DECLARE @IsClockHour BIT;

        SELECT @IsClockHour = IsClockHour
        FROM   dbo.SourceDB D
        WHERE  D.SchoolID = @SchoolID
               AND D.IsDisabled = 0;

        DECLARE @BridgeStage TABLE
            (
                StudentID UNIQUEIDENTIFIER
              , RecordKey INT PRIMARY KEY
              , StuSSN VARCHAR(50)
              , TotalRank INT
              , LocalRank INT
            );

        INSERT INTO @BridgeStage
                    SELECT CASE WHEN LocalRank = 1 THEN NEWID()
                                ELSE CAST(NULL AS UNIQUEIDENTIFIER)
                           END AS StudentID
                         , RecordKey
                         , StuSSN
                         , TotalRank
                         , LocalRank
                    FROM   (
                               SELECT sStud1.RecordKey
                                    , dbo.udf_GetNumericOnly(StuSSN) AS StuSSN
                                    , DENSE_RANK() OVER ( --PARTITION BY StuSSN 
                                    ORDER BY StuSSN
                                                        ) AS TotalRank
                                    , DENSE_RANK() OVER ( PARTITION BY StuSSN
                                                          ORDER BY StuSSN
                                                                 , sStud1.RecordKey
                                                        ) LocalRank -- 1 - identifies record that is first with the same SSN
                               --RecordKey, NextStudRecKey, PrevStudRecKey, StuSSN, privCurrentEnrollStatus
                               FROM   Freedom..STUDREC sStud1
                               INNER JOIN Freedom..COURSE_DEF sCourse1 ON sCourse1.RecordKey = sStud1.CourseNumEnrolledIn
                               WHERE  PrivCurrentEnrollStatus <> 0
                                      --AND sStud1.FirstName = 'Deliliah'
                                      --AND sStud1.PrivLastName = 'Hensley'
                                      AND (
                                              (
                                                  @IsClockHour = 1
                                                  AND sCourse1.PrivAcCalType = 5
                                              ) -- Clock hour only
                                              OR (
                                                     @IsClockHour = 0
                                                     AND sCourse1.PrivAcCalType <> 5
                                                 )
                                          )
                           ) A;

        INSERT INTO FreedomBridge.bridge.arStudentOLD
                    SELECT COALESCE(bStudExist.trg_StudentId, B1.StudentID, B12.StudentID) AS StudentID
                         , B1.StuSSN AS StuSSN
                         , B1.RecordKey
                         , @CampusId AS CampusID
                         , B1.TotalRank
                         , B1.LocalRank
                    FROM   @BridgeStage B1
                    INNER JOIN @BridgeStage B12 ON B1.TotalRank = B12.TotalRank -- Total rank is identifies student with the same SSN
                                                   AND B12.StudentID IS NOT NULL
                    LEFT JOIN bridge.arStudentOLD bStudExist ON bStudExist.StuSSN = B1.StuSSN
                                                                AND bStudExist.LocalRank = 1; --

        ALTER TABLE FreedomAdvantage..arStudentOld DISABLE TRIGGER ALL;

        SELECT bStud.trg_StudentId StudentId                                                                          --uniqueidentifier
             , LTRIM(RTRIM(sStud.FirstName)) AS FirstName                                                             --varchar(50)
             , LTRIM(RTRIM(sStud.MidInitial)) AS MiddleName                                                           --varchar(50)
             , LTRIM(RTRIM(PrivLastName)) AS LastName                                                                 --varchar(50)
             , dbo.udf_GetNumericOnly(LTRIM(RTRIM(sStud.StuSSN))) AS StudSSN                                          --varchar(50)
             , dStatus.StatusId AS StudentStatus                                                                      --uniqueidentifier
             , NULL AS WorkEmail                                                                                      --varchar(50)
             , NULLIF(LTRIM(RTRIM(sStud.Email)), '') AS HomeEmail                                                     --varchar(50)
             , sStud.BirthDate AS DOB                                                                                 --datetime
             , 'SA' AS ModUser                                                                                        --varchar(50)
             , GETDATE() AS ModDate                                                                                   --datetime
             , CAST(NULL AS UNIQUEIDENTIFIER) AS Prefix                                                               --uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS Suffix                                                               --uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS Sponsor                                                              --uniqueidentifier
             , CAST(NULL AS DATETIME) AS AssignedDate                                                                 --datetime
             , bGender.trg_GenderId AS Gender                                                                         --uniqueidentifier
             , bRace.trg_EthCodeId AS Race                                                                            --uniqueidentifier
             , bMStatus.trg_MaritalStatId AS MaritalStatus                                                            --uniqueidentifier
             , bIncome.trg_FamilyIncomeID AS FamilyIncome                                                             --uniqueidentifier
             , NumOfDeps AS Children                                                                                  --int
             , sStud.EnrolledDate AS ExpectedStart                                                                    --datetime
             , CAST(NULL AS UNIQUEIDENTIFIER) AS ShiftId                                                              --uniqueidentifier
             , dNation.NationalityId AS Nationality                                                                   --uniqueidentifier
             , bCitizen.trg_CitizenshipId AS Citizen                                                                  --uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS DrivLicStateId                                                       --uniqueidentifier
             , NULL AS DrivLicNumber                                                                                  --varchar(50)
             , NULL AS AlienNumber                                                                                    --varchar(50)
             , NULL AS Comments                                                                                       --varchar(300) --Goes from STUDREC_NOT_CMT to dbo.syStudentNotes
             , CAST(NULL AS UNIQUEIDENTIFIER) AS County                                                               --uniqueidentifier
             , CAST(NULL AS DATETIME) AS SourceDate                                                                   --datetime
             , bEdu.trg_EdLvlId AS EdLvlId                                                                            --uniqueidentifier
             , CAST(bCampus.src_SchoolId AS VARCHAR(4)) + '-' + CAST(sStud.RecordKey AS VARCHAR(10)) AS StudentNumber --varchar(50)
             , NULL AS Objective                                                                                      --varchar(300)
             , bDepend.trg_DependencyTypeId AS DependencyTypeId                                                       --uniqueidentifier
             , bDegree.trg_DegCertSeekingId AS DegCertSeekingId                                                       --uniqueidentifier
             , bGeo.trg_GeographicTypeId AS GeographicTypeId                                                          --uniqueidentifier
             , bHouse.trg_HousingId AS HousingId                                                                      --uniqueidentifier
             , bAdmin.trg_AdminCriteriaId AS admincriteriaid                                                          --uniqueidentifier
             , bAttend.trg_AttendTypeId AS AttendTypeId                                                               --uniqueidentifier
             , NULL AS ApportionedCreditBalanceAY1                                                                    --decimal(18,2)
             , NULL AS ApportionedCreditBalanceAY2                                                                    --decimal(18,2))
        INTO   #arStudent
        FROM   Freedom..STUDREC sStud
        INNER JOIN bridge.arStudentOLD bStud ON bStud.src_RecordKey = sStud.RecordKey
                                                AND bStud.LocalRank = 1
                                                AND bStud.trg_CampusId = @CampusId
        INNER JOIN Freedom..STUDREC2 sStud2 ON sStud2.RecordKey = sStud.RecordKey
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN bridge.adGenders bGender ON bGender.src_ID = sStud2.Sex
        INNER JOIN bridge.adEthCodes bRace ON bRace.src_ID = sStud2.Race
        LEFT JOIN bridge.adMaritalStatus bMStatus ON bMStatus.src_ID = sStud2.MaritalStatus
        INNER JOIN bridge.syCampuses bCampus ON bCampus.trg_syCampusId = @CampusId
        INNER JOIN bridge.syFamilyIncome bIncome ON bIncome.trg_FamilyIncomeDescrip = CASE WHEN sStud2.YearlyIncome
                                                                                                BETWEEN bIncome.src_AmountFrom AND bIncome.src_AmountTo THEN
                                                                                               bIncome.trg_FamilyIncomeDescrip
                                                                                           WHEN sStud2.YearlyIncome = 0 THEN 'Unknown'
                                                                                      END
        INNER JOIN Freedom..COURSE_DEF sCourse ON sCourse.RecordKey = sStud.CourseNumEnrolledIn
        LEFT JOIN bridge.adEdLvls bEdu ON bEdu.src_ID = sStud2.EducationLevel
        LEFT JOIN bridge.adCitizenships bCitizen ON bCitizen.src_ID = sStud2.CitizenType
        INNER JOIN FreedomAdvantage..adNationalities dNation ON dNation.NationalityDescrip = CASE WHEN sStud2.CitizenType = 1 THEN 'American'
                                                                                                  ELSE 'Non-American'
                                                                                             END
        LEFT JOIN bridge.adDependencyTypes bDepend ON bDepend.src_ID = sStud2.Dependency
        LEFT JOIN bridge.adDegCertSeeking bDegree ON bDegree.src_ID = sStud.DegreeSeekingType
        LEFT JOIN bridge.adGeographicTypes bGeo ON bGeo.src_ID = sStud2.UrbanType
        LEFT JOIN bridge.arHousing bHouse ON bHouse.src_ID = sStud2.HousingType
        LEFT JOIN bridge.adAdminCriteria bAdmin ON bAdmin.src_ID = sStud.AbilityToBenefit
        LEFT JOIN bridge.arAttendTypes bAttend ON bAttend.src_ID = sStud2.FullTimePartTime
        LEFT JOIN FreedomAdvantage..arStudentOld dStudExist ON dStudExist.StudentId = bStud.trg_StudentId
                                                               AND bStud.LocalRank = 1 --
        WHERE  sStud.PrivCurrentEnrollStatus <> 0 --Tech support masking of student that does not exist
               AND dStudExist.StudentId IS NULL
               AND (
                       (
                           @IsClockHour = 1
                           AND sCourse.PrivAcCalType = 5
                       ) -- Clock hour only
                       OR (
                              @IsClockHour = 0
                              AND sCourse.PrivAcCalType <> 5
                          )
                   );

        INSERT INTO FreedomAdvantage..arStudentOld
                    SELECT *
                    FROM   #arStudent;

        DROP TABLE #arStudent;
        DROP TABLE #Settings;

        ALTER TABLE FreedomAdvantage..arStudentOld ENABLE TRIGGER ALL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arStudentOLD]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
