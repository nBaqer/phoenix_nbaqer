SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-10-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adSourceAdvertisement]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        BEGIN TRAN;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT *
        INTO   #SourceAd
        FROM   (
                   SELECT CASE WHEN dSrcAd.SourceAdvDescrip IS NULL THEN NEWID()
                               ELSE dSrcAd.SourceAdvId
                          END AS SourceAdvId                                                         -- uniqueidentifier
                        , sAdType.AdSourceName AS SourceAdvDescrip                                   -- varchar(50)
                        , CASE WHEN sAdCost.BeginDate IS NULL THEN '1/1/2000'
                               ELSE sAdCost.BeginDate
                          END AS startdate                                                           -- datetime
                        , CASE WHEN sAdCost.EndDate IS NULL THEN DATEADD(YEAR, 1, GETDATE())
                               ELSE sAdCost.EndDate
                          END AS enddate                                                             -- datetime
                        , ISNULL(sAdCost.Amount, 0) AS cost                                          -- decimal
                        , bSrcType.trg_SourceTypeId AS SourceTypeId                                  -- uniqueidentifier
                        , ISNULL(dAdInt.AdvIntervalId, dAdIntDefault.AdvIntervalId) AS AdvIntervalId -- uniqueidentifier
                        , FreedomBridge.dbo.udf_FrstLtrWord(sAdType.AdSourceName) AS SourceAdvCode   -- varchar(50)
                        , dStatus.StatusId AS StatusId                                               -- uniqueidentifier
                        , 'sa' AS ModUser                                                            -- varchar(50)
                        , GETDATE() AS ModDate                                                       -- datetime
                        , dCampGrp.CampGrpId AS CampGrpId                                            -- uniqueidentifier
                        , sAdType.RecordKey
                        , ROW_NUMBER() OVER ( PARTITION BY sAdType.AdSourceName
                                                         , sAdType.RecordKey
                                              ORDER BY sAdCost.Amount DESC
                                            ) AS dupAdType
                   FROM   Freedom..ADSOURCE_FIL sAdType
                   LEFT JOIN Freedom..ADCOST_FIL sAdCost ON sAdCost.AdSourceRecKey = sAdType.RecordKey
                   LEFT JOIN FreedomBridge.stage.FLookUp FLook ON FLook.ID = sAdCost.CostInterval
                                                                  AND FLook.LookupName = 'CostInterval'
                   LEFT JOIN FreedomAdvantage..adAdvInterval dAdInt ON dAdInt.AdvIntervalDescrip = FLook.Description
                   LEFT JOIN FreedomAdvantage..adAdvInterval dAdIntDefault ON dAdIntDefault.AdvIntervalDescrip = 'Yearly'
                   INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = CASE WHEN sAdType.Active = 'Y' THEN 'Active'
                                                                                            ELSE 'Inactive'
                                                                                       END
                   INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
                   INNER JOIN FreedomBridge.bridge.adSourceType bSrcType ON bSrcType.src_RecNum = sAdType.AdSourceType
                                                                            AND bSrcType.trg_CampusId = @CampusId
                   LEFT JOIN FreedomAdvantage..adSourceAdvertisement dSrcAd ON dSrcAd.CampGrpId = dCampGrp.CampGrpId
                                                                               AND dSrcAd.SourceAdvDescrip = sAdType.AdSourceName
               ) SourceAd
        WHERE  SourceAd.dupAdType = 1;

        ALTER TABLE FreedomAdvantage..adSourceAdvertisement DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adSourceAdvertisement
                    SELECT tSrcAd.SourceAdvId
                         , tSrcAd.SourceAdvDescrip
                         , tSrcAd.startdate
                         , tSrcAd.enddate
                         , tSrcAd.cost
                         , tSrcAd.SourceTypeId
                         , tSrcAd.AdvIntervalId
                         , tSrcAd.SourceAdvCode
                         , tSrcAd.StatusId
                         , tSrcAd.ModUser
                         , tSrcAd.ModDate
                         , tSrcAd.CampGrpId
                    FROM   #SourceAd tSrcAd
                    LEFT JOIN FreedomAdvantage..adSourceAdvertisement dSrcType ON dSrcType.CampGrpId = tSrcAd.CampGrpId
                                                                                  AND dSrcType.SourceAdvDescrip = tSrcAd.SourceAdvDescrip
                    WHERE  dSrcType.SourceTypeId IS NULL;

        ALTER TABLE FreedomAdvantage..adSourceAdvertisement ENABLE TRIGGER ALL;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adSourceAdvertisement')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adSourceAdvertisement
                    (
                        trg_SourceAdvId UNIQUEIDENTIFIER
                      , src_AdSourceName VARCHAR(50)
                      , src_RecordKey INT
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              src_AdSourceName
                            , src_RecordKey
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adSourceAdvertisement
                    SELECT tSrcAd.SourceAdvId
                         , tSrcAd.SourceAdvDescrip
                         , tSrcAd.RecordKey
                         , @CampusId
                    FROM   #SourceAd tSrcAd
                    LEFT JOIN FreedomBridge.bridge.adSourceAdvertisement bSrcType ON bSrcType.src_RecordKey = tSrcAd.RecordKey
                                                                                     AND bSrcType.src_AdSourceName = tSrcAd.SourceAdvDescrip
                    WHERE  bSrcType.trg_SourceAdvId IS NULL;

        DROP TABLE #SourceAd;
        DROP TABLE #Settings;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adSourceAdvertisement]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRAN;

    END;
GO
