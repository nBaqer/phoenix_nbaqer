SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adMaritalStatus]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dMar.MaritalStatDescrip IS NULL THEN NEWID()
                    ELSE dMar.MaritalStatId
               END AS MaritalStatId                                                           -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS MaritalStatCode -- varchar(50)
             , dStatus.StatusId AS StatusId                                                   -- uniqueidentifier
             , FLook.Description AS MaritalStatDescrip                                        -- varchar(50)               
             , dCampGrp.CampGrpId AS CampGrpId                                                -- uniqueidentifier
             , 'sa' AS ModUser                                                                -- varchar(50)
             , GETDATE() AS ModDate                                                           -- datetime               
             , FLook.ID
        INTO   #MaritalStat
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adMaritalStatus dMar ON dMar.CampGrpId = dCampGrp.CampGrpId
                                                            AND dMar.MaritalStatDescrip = FLook.Description
        WHERE  FLook.LookupName = 'MaritalStatus';

        ALTER TABLE FreedomAdvantage..adMaritalStatus DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adMaritalStatus
                    SELECT tMar.MaritalStatId
                         , tMar.MaritalStatCode
                         , tMar.StatusId
                         , tMar.MaritalStatDescrip
                         , tMar.CampGrpId
                         , tMar.ModUser
                         , tMar.ModDate
						 , Null
                    FROM   #MaritalStat tMar
                    LEFT JOIN FreedomAdvantage..adMaritalStatus dMar ON dMar.CampGrpId = tMar.CampGrpId
                                                                        AND dMar.MaritalStatId = tMar.MaritalStatId
                    WHERE  dMar.MaritalStatDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..adMaritalStatus ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adMaritalStatus')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adMaritalStatus
                    (
                        trg_MaritalStatId UNIQUEIDENTIFIER
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adMaritalStatus
                    SELECT tMar.MaritalStatId
                         , tMar.MaritalStatDescrip
                         , tMar.ID
                    FROM   #MaritalStat tMar
                    LEFT JOIN FreedomBridge.bridge.adMaritalStatus bMar ON bMar.trg_MaritalStatId = tMar.MaritalStatId
                    WHERE  bMar.trg_MaritalStatId IS NULL;

        DROP TABLE #MaritalStat;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adMaritalStatus]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
