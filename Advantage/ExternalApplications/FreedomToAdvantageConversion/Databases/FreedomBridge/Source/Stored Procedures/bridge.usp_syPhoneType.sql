SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_syPhoneType]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT     CASE WHEN dPType.PhoneTypeDescrip IS NULL THEN NEWID()
                        ELSE dPType.PhoneTypeId
                   END AS PhoneTypeId                                     --uniqueidentifier
                 , ISNULL(FL.ID, 1) AS PhoneTypeCode                      -- varchar(12)
                 , ISNULL(FL.Description, 'Home') AS PhoneTypeDescription -- varchar(50)
                 , dStatus.StatusId                                       -- uniqueidentifier
                 , dCampGrp.CampGrpId                                     -- uniqueidentifier
                 , NULL AS Sequence
                 , 'sa' AS ModUser
                 , GETDATE() AS ModDate
                 , ROW_NUMBER() OVER ( PARTITION BY FL.Description
                                       ORDER BY FL.ID
                                     ) AS rownum
        INTO       #syPhoneType
        FROM       stage.FLookUp FL
        INNER JOIN FreedomAdvantage.dbo.syStatuses dStatus ON dStatus.Status = 'Active'
        --INNER JOIN FreedomBridge.bridge.syCampuses bCampus ON bCampus.trg_syCampusId = @CampusId
        INNER JOIN FreedomAdvantage.dbo.syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'All'--CAST(bCampus.src_SchoolId AS VARCHAR(4))
        LEFT JOIN  FreedomAdvantage.dbo.syPhoneType dPType ON dPType.PhoneTypeDescrip = FL.Description
        WHERE      LookupName = 'PhoneType';

        INSERT INTO FreedomAdvantage.dbo.syPhoneType
                    SELECT    tPhoneType.PhoneTypeId
                            , tPhoneType.PhoneTypeCode
                            , tPhoneType.PhoneTypeDescription
                            , tPhoneType.StatusId
                            , tPhoneType.CampGrpId
                            , tPhoneType.Sequence
                            , tPhoneType.ModUser
                            , tPhoneType.ModDate
                    FROM      #syPhoneType tPhoneType
                    LEFT JOIN FreedomAdvantage.dbo.syPhoneType dPhoneType ON dPhoneType.PhoneTypeId = tPhoneType.PhoneTypeId
                                                                             AND dPhoneType.PhoneTypeDescrip = tPhoneType.PhoneTypeDescription
                    WHERE     rownum = 1
                              AND dPhoneType.PhoneTypeDescrip IS NULL;

        INSERT INTO FreedomBridge.bridge.syPhoneType
                    SELECT PhoneTypeId
                         , PhoneTypeCode
                         , PhoneTypeDescription
                    FROM   #syPhoneType
                    WHERE  rownum = 1;

        DROP TABLE #syPhoneType;

	    INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syPhoneType]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
