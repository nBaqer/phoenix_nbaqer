SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-08-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arResults]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        -- Convert Freedom results into Advantage complete status
        SELECT    wd.Weight
                 ,wd.MinLabCnt
                 ,wd.SubjectDefKey
                 ,ws.WorkUnitDefKey
                 ,ws.PrivResult
                 ,CASE WHEN wd.Type <> 3
                            AND ws.PrivResult >= wd.MinResult THEN 1
                       WHEN wd.Type = 3
                            AND ws.PrivLabCnt >= wd.MinLabCnt
                            AND ws.PrivSpecialResult = 0 THEN 1
                       WHEN ws.PrivSpecialResult = 1 THEN 2
                       ELSE 0
                  END AS Complete
                 ,ws.StudrecKey
                 ,ws.PrivLabCnt
                 ,ws.PrivSpecialResult
                 ,ws.Date
        INTO      #Work
        FROM      Freedom.dbo.WORKUNIT_DEF wd
        LEFT JOIN Freedom.dbo.WORKUNIT_RES ws ON ws.WorkUnitDefKey = wd.RecordKey;


        -- Compile grades for students from previous tables
        SELECT     CASE WHEN dRes.TestId IS NULL THEN NEWID()
                        ELSE dRes.ResultId
                   END AS ResultId                           -- uniqueidentifier
                  ,bReqs.src_Name AS ReqDescrip
                  ,bClsSect.trg_ClsSectionId AS TestId       -- uniqueidentifier

                  ,sSubRes.RecordKey AS src_RecordKey
                  ,ROW_NUMBER() OVER ( PARTITION BY bStuEnroll.trg_StuEnrollId
                                                   ,bClsSect.trg_ClsSectionId
                                                   ,bClsSect.src_RecordKey
                                       ORDER BY bStuEnroll.trg_StuEnrollId
                                     ) AS rownum
                  ,bClsSect.trg_ClsSection
                  ,CASE WHEN sSubRes.PrivResultCnt = 0
                             OR bReqs.src_Name LIKE '%Attendance' THEN NULL
                        ELSE ROUND(sSubRes.PrivResultTot / sSubRes.PrivResultCnt, 2)
                   END AS Score                              -- decimal
                  ,CASE WHEN bReqs.src_Name LIKE '%Attendance' THEN NULL
                        WHEN sWork.Complete = 2 THEN (
                                                     SELECT GrdSysDetailId
                                                     FROM   FreedomAdvantage.dbo.arGradeSystemDetails
                                                     WHERE  Grade = 'TR'
                                                     )
                        ELSE (
                             SELECT    TOP 1 GSD.GrdSysDetailId
                             FROM      FreedomAdvantage..arGradeScaleDetails GSD
                             LEFT JOIN FreedomAdvantage..arGradeScales GS ON GS.GrdScaleId = GSD.GrdScaleId
                             WHERE     LTRIM(RTRIM(GS.Descrip)) = 'Default'
                                       AND ROUND(   CASE WHEN sSubRes.PrivResultCnt = 0 THEN NULL
                                                         ELSE sSubRes.PrivResultTot / sSubRes.PrivResultCnt
                                                    END
                                                   ,2
                                                )
                                       BETWEEN ( GSD.MinVal - 0.5 ) AND ( GSD.MaxVal + 0.49 )
                             )
                   END AS GrdSysDetailId                     -- uniqueidentifier
                  ,sSubRes.PrivLabCnt AS Cnt                 -- int
                  ,0 AS Hours                                -- int
                  ,bStuEnroll.trg_StuEnrollId AS StuEnrollId -- uniqueidentifier
                  ,0 AS IsInComplete                         -- bit
                  ,0 AS DroppedInAddDrop                     -- bit
                  ,'sa' AS ModUser                           -- varchar(50)
                  ,GETDATE() AS ModDate                      -- datetime
                  ,sSub.BegDate
                  ,sStu.StartDate
                  ,CASE WHEN sWork.Complete = 2 THEN 1
                        ELSE 0
                   END AS IsTransfered                       -- bit
                  ,CASE WHEN sCourse.PrivLength = sStu.PrivTotalHrsAttended
                             AND bReqs.src_Name LIKE '%Attendance' THEN 1
                        WHEN sWork.SubjectDefKey = sSubRes.SubjectDefKey THEN sWork.Complete
                        ELSE 0
                   END AS isClinicsSatisfied                 -- bit
                  ,GETDATE() AS DateDetermined               -- datetime
                  ,CASE WHEN sCourse.PrivLength = sStu.PrivTotalHrsAttended
                             AND bReqs.src_Name LIKE '%Attendance' THEN 1
                        WHEN sWork.SubjectDefKey = sSubRes.SubjectDefKey
                             AND sWork.Complete > 0 THEN 1
                        ELSE 0
                   END AS IsCourseCompleted                  -- bit
                  ,sCourse.PrivLength
                  ,CASE WHEN sWork.Complete = 2 THEN 1
                        ELSE 0
                   END AS IsGradeOverridden                  -- bit
                  ,NULL AS GradeOverriddenBy                 -- varchar(50)
                  ,NULL AS GradeOverriddenDate               -- datetime
                  ,sStu.RecordKey
                  ,bStuEnroll.src_RecordKey StudrecKey
                  ,bTerm.src_RecordKey AS TermRecKey
                  ,bTerm.src_Name AS TermDescrip
                  ,sSubRes.SubjectDefKey AS SubjectDefKey
        INTO       #Results
        FROM       FreedomBridge.bridge.arReqs bReqs
        INNER JOIN Freedom.dbo.SUBJECT_DEF sSub ON sSub.RecordKey = bReqs.src_RecordKey
        INNER JOIN Freedom.dbo.SUBJECT_RES sSubRes ON sSubRes.SubjectDefKey = bReqs.src_RecordKey
        INNER JOIN FreedomBridge.bridge.arStuEnrollments bStuEnroll ON bStuEnroll.src_RecordKey = sSubRes.StudrecKey
                                                                       AND bStuEnroll.trg_CampusId = @CampusId
        INNER JOIN Freedom.dbo.STUDREC sStu ON sStu.RecordKey = bStuEnroll.src_RecordKey
        INNER JOIN Freedom.dbo.COURSE_DEF sCourse ON sCourse.RecordKey = bReqs.src_CourseDefKey
        INNER JOIN Freedom.dbo.TERM_RES sTermRes ON sTermRes.RecordKey = sSubRes.TermResKey
        INNER JOIN FreedomBridge.bridge.arTerm bTerm ON bTerm.src_RecordKey = sTermRes.TermDefKey
                                                        AND bTerm.trg_CampusId = @CampusId
        INNER JOIN FreedomBridge.bridge.arClassSections bClsSect ON bClsSect.trg_ReqId = bReqs.trg_ReqId
                                                                    AND LEFT(bClsSect.trg_ClsSection, 7) = LEFT(CAST(sStu.AttndSchdHrsSun AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsMon AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsTue AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsWed AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsThr AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsFri AS VARCHAR(10))
                                                                                                                + CAST(sStu.AttndSchdHrsSat AS VARCHAR(10)), 7)
                                                                    AND bClsSect.trg_TermId = bTerm.trg_TermId
                                                                    AND bClsSect.trg_CampusId = @CampusId
        INNER JOIN #Work sWork ON sWork.SubjectDefKey = sSubRes.SubjectDefKey
                                  AND sWork.StudrecKey = sSubRes.StudrecKey
        INNER JOIN FreedomBridge.bridge.arGradeSystems bGrdSys ON bGrdSys.src_Description = CASE WHEN sCourse.GradeScaleKey = 0 THEN 'Default'
                                                                                                 WHEN bGrdSys.src_RecordKey = sCourse.GradeScaleKey THEN
                                                                                                     bGrdSys.src_Description
                                                                                            END
                                                                  AND bGrdSys.trg_CampusId = @CampusId
        LEFT JOIN  FreedomBridge.bridge.arGradeSystemDetails bGrdSysDtl ON bGrdSysDtl.trg_GrdSystemId = bGrdSys.trg_GradeSystemId
                                                                           AND ROUND(   CASE WHEN sSubRes.PrivResultCnt = 0 THEN NULL
                                                                                             ELSE sSubRes.PrivResultTot / sSubRes.PrivResultCnt
                                                                                        END
                                                                                       ,2
                                                                                    )
                                                                           BETWEEN bGrdSysDtl.src_MinGradeValue AND bGrdSysDtl.src_MaxGradeValue
                                                                           AND bGrdSysDtl.trg_CampusId = @CampusId
        LEFT JOIN  FreedomAdvantage.dbo.arResults dRes ON dRes.TestId = bClsSect.trg_ClsSectionId
                                                          AND dRes.StuEnrollId = bStuEnroll.trg_StuEnrollId
                                                          AND dRes.Score = CASE WHEN sSubRes.PrivResultCnt = 0
                                                                                     OR bReqs.src_Name LIKE '%Attendance' THEN NULL
                                                                                ELSE ROUND(sSubRes.PrivResultTot / sSubRes.PrivResultCnt, 2)
                                                                           END
        WHERE      bReqs.trg_CampusId = @CampusId
                   

        SELECT *
        FROM   #Results;


        WITH resultGroups
        AS ( SELECT tRes.ResultId
                   ,tRes.TestId
                   ,tRes.Score
                   ,tRes.GrdSysDetailId
                   ,tRes.Cnt
                   ,tRes.Hours
                   ,tRes.StuEnrollId
                   ,tRes.IsInComplete
                   ,tRes.DroppedInAddDrop
                   ,tRes.ModUser
                   ,tRes.ModDate
                   ,tRes.IsTransfered
                   ,tRes.isClinicsSatisfied
                   ,tRes.DateDetermined
                   ,tRes.IsCourseCompleted
                   ,tRes.IsGradeOverridden
                   ,tRes.GradeOverriddenBy
                   ,tRes.GradeOverriddenDate
                   ,tRes.src_RecordKey
                   ,tRes.RecordKey
                   ,ROW_NUMBER() OVER ( PARTITION BY tRes.src_RecordKey
                                                    ,tRes.StuEnrollId
                                        ORDER BY tRes.StuEnrollId
                                      ) AS [ROW NUMBER]
             FROM   #Results tRes
             WHERE  tRes.rownum = 1 )
        SELECT *
        INTO   #finalResult
        FROM   resultGroups
        WHERE  [ROW NUMBER] = 1;

        UPDATE FR
        SET    FR.IsCourseCompleted = CASE WHEN NOT EXISTS (
                                                           SELECT 1
                                                           FROM   #Results C
                                                           WHERE  C.TestId = FR.TestId
                                                                  AND C.StuEnrollId = FR.StuEnrollId
                                                                  AND C.IsCourseCompleted = 0
                                                           ) THEN 1
                                           ELSE 0
                                      END
        FROM   #finalResult FR;


        ALTER TABLE FreedomAdvantage..arResults DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage.dbo.arResults
                    SELECT    tRes.ResultId
                             ,tRes.TestId
                             ,tRes.Score
                             ,tRes.GrdSysDetailId
                             ,tRes.Cnt
                             ,tRes.Hours
                             ,tRes.StuEnrollId
                             ,tRes.IsInComplete
                             ,tRes.DroppedInAddDrop
                             ,tRes.ModUser
                             ,tRes.ModDate
                             ,tRes.IsTransfered
                             ,tRes.isClinicsSatisfied
                             ,tRes.DateDetermined
                             ,tRes.IsCourseCompleted
                             ,tRes.IsGradeOverridden
                             ,tRes.GradeOverriddenBy
                             ,tRes.GradeOverriddenDate
                             ,NULL
                    FROM      #finalResult tRes
                    LEFT JOIN FreedomAdvantage.dbo.arResults dRes ON dRes.StuEnrollId = tRes.StuEnrollId
                                                                     AND dRes.TestId = tRes.TestId;




        ALTER TABLE FreedomAdvantage..arResults ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arResults')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arResults
                    (
                        trg_ResultId UNIQUEIDENTIFIER
                       ,trg_TestId UNIQUEIDENTIFIER
                       ,src_RecordKey INT
                       ,src_StudrecKey INT
                       ,trg_StuEnrollId UNIQUEIDENTIFIER
                       ,trg_CampusId UNIQUEIDENTIFIER
                       ,
                       PRIMARY KEY CLUSTERED
                       (
                       trg_TestId
                      ,trg_StuEnrollId
                      ,src_RecordKey
                      ,src_StudrecKey
                       )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arResults
                    SELECT    tRes.ResultId
                             ,tRes.TestId
                             ,tRes.src_RecordKey
                             ,tRes.RecordKey
                             ,tRes.StuEnrollId
                             ,@CampusId AS CampusId
                    FROM      #finalResult tRes
                    LEFT JOIN FreedomBridge.bridge.arResults bRes ON bRes.trg_StuEnrollId = tRes.StuEnrollId
                                                                     AND bRes.trg_TestId = tRes.TestId
                                                                     AND bRes.src_RecordKey = tRes.src_RecordKey
                                                                     AND bRes.trg_CampusId = @CampusId
                    WHERE     bRes.trg_ResultId IS NULL;

        DROP TABLE #Work;
        DROP TABLE #Results;
        DROP TABLE #Settings;
        DROP TABLE #finalResult;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arResults]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;




GO
