SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saFeeLevels]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT dFeeLevel.FeeLevelId --tinyint
             , FL.ID
             , FL.Description
             , FL.LookUpID
        INTO   #saFeeLevels
        FROM   stage.FLookUp FL
        LEFT JOIN FreedomAdvantage..saFeeLevels dFeeLevel ON dFeeLevel.Description = CASE WHEN FL.Description = 'Academic year' THEN
                                                                                              'Program Version - Academic Year'
                                                                                          WHEN FL.Description = 'Payment period' THEN
                                                                                              'Program Version - Payment Period'
                                                                                          WHEN FL.Description = 'Program' THEN 'Program Version'
                                                                                     END
        LEFT JOIN bridge.saFeeLevels bIgnore ON bIgnore.src_Description = FL.Description
        WHERE  FL.LookupName = 'ChargePeriodType'
               AND bIgnore.src_ID IS NULL;

        INSERT INTO FreedomBridge.bridge.saFeeLevels
                    SELECT FeeLevelId
                         , ID
                         , Description
                         , LookUpID
                    FROM   #saFeeLevels;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saFeeLevels]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
