SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saFundSources]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        DECLARE @ForceACleanDB BIT;
        SET @ForceACleanDB = 0;

        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        DECLARE @CampusGroupId UNIQUEIDENTIFIER;

        SET @ActiveStatusId = (
                              SELECT   TOP ( 1 ) StatusId
                              FROM     FreedomAdvantage..syStatuses
                              WHERE    StatusCode = 'A'
                              ORDER BY StatusCode
                              );
        SET @CampusGroupId = (
                             SELECT CampGrpId
                             FROM   FreedomAdvantage..syCampGrps
                             WHERE  CampusId = (
                                               SELECT TOP ( 1 ) Value
                                               FROM   #Settings
                                               WHERE  Entity = 'CampusID'
                                               )
                                    OR CampGrpCode = (
                                                     SELECT TOP ( 1 ) Value
                                                     FROM   #Settings
                                                     WHERE  Entity = 'SchoolId'
                                                     )
                             );

        SET @ForceACleanDB = (
                             SELECT ForceACleanDB
                             FROM   SourceDB
                             WHERE  IsDisabled = 0
                             );

        IF ( @ForceACleanDB = 1 )
            BEGIN
                DELETE FROM FreedomAdvantage.dbo.saFundSources;
            END;

        SELECT NEWID() AS FundSourceId
              ,RTRIM(LTRIM(KisDescrip)) AS FundSourceCode
              ,@ActiveStatusId AS StatusId
              ,RTRIM(LTRIM(FullDescrip)) AS FundSourceDescrip
              ,@CampusGroupId AS CampGrpId
              ,'Conversion' AS ModUser
              ,GETDATE() AS ModDate
              ,IsTitleIV AS TitleIV
              ,CASE AidType
                    WHEN 1 THEN 1
                    WHEN 2 THEN 2
                    WHEN 3 THEN 3
                    WHEN 4 THEN 4
                    WHEN 5 THEN 6
                    WHEN 6 THEN 5
                    ELSE NULL
               END AS AwardTypeId
              ,NULL AS AwardYear
              ,CASE RTRIM(LTRIM(FullDescrip))
                    WHEN 'Federal Pell Grant' THEN 2
                    WHEN 'Federal Stafford Loan' THEN 7
                    WHEN 'Federal SEOG' THEN 3
                    WHEN 'Federal Work Study' THEN 5
                    WHEN 'Federal Perkins Loan' THEN 4
                    WHEN 'State Student Incentive Grant' THEN 19
                    WHEN 'Federal PLUS' THEN 6
                    WHEN 'Supplemental Loans for Students' THEN 19
                    WHEN 'Job Training Grant' THEN 15
                    WHEN 'Veterans Benefits' THEN 16
                    WHEN 'Vocational Rehabilitation' THEN 17
                    WHEN 'Regional Occupational Program' THEN 18
                    WHEN 'Other Miscellaneous Aid' THEN 19
                    WHEN 'School Scholarship' THEN 14
                    WHEN 'SLM Smart Option' THEN 13
                    WHEN 'TUBC Grant' THEN 14
                    WHEN 'TUBC Salon Corps Scholarship' THEN 14
                    WHEN 'TUBC Alumni Scholarship' THEN 14
                    WHEN 'TUBC Associate Scholarship' THEN 14
                    WHEN 'WIA Scholarship' THEN 14
                    WHEN 'TUBC Teacher Training Scholarship' THEN 14
                    WHEN 'TUBC Transfer Grant' THEN 14
                    WHEN 'TUBC Special Grant' THEN 14
                    WHEN 'VA Grant' THEN 16
                    WHEN 'ACE Grant' THEN 12
                    WHEN 'MyCAA Program' THEN 14
                    WHEN 'DCFS Office of Ed/Transition' THEN 19
                    WHEN 'Post 9/11 GI Bill Benefits' THEN 24
                    WHEN 'Dept of Defense Tuition Assistance' THEN 25
                    WHEN 'Outside Scholarship' THEN 14
                    WHEN 'TuitionFlex' THEN 19
                    WHEN 'FSEOG Match' THEN 3
                    ELSE 19
               END AS AdvFundSourceId
              ,NULL AS CutoffDate
              ,NULL AS IPEDSValue
        INTO   #saFundSourcesTempTable
        FROM   Freedom.dbo.AWDTYPES_FIL;

        INSERT INTO FreedomAdvantage.dbo.saFundSources
                    SELECT DISTINCT sftt.FundSourceId
                                   ,sftt.FundSourceCode
                                   ,sftt.StatusId
                                   ,sftt.FundSourceDescrip
                                   ,sftt.CampGrpId
                                   ,sftt.ModUser
                                   ,sftt.ModDate
                                   ,sftt.TitleIV
                                   ,sftt.AwardTypeId
                                   ,sftt.AwardYear
                                   ,sftt.AdvFundSourceId
                                   ,sftt.CutoffDate
                                   ,sftt.IPEDSValue
                    FROM   #saFundSourcesTempTable AS sftt
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   FreedomAdvantage.dbo.saFundSources
                                      WHERE  FundSourceCode LIKE sftt.FundSourceCode
                                             AND FundSourceDescrip LIKE sftt.FundSourceDescrip
                                             AND CampGrpId LIKE sftt.CampGrpId
                                      );


        --- adding just one if not exists to FreedomAdvantage..saFundSources
        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..saFundSources
                      WHERE  AdvFundSourceId IN ( 8 )
                             AND CampGrpId = @CampusGroupId
                      )
            BEGIN
                INSERT INTO FreedomAdvantage..saFundSources (
                                                            FundSourceId
                                                           ,FundSourceCode
                                                           ,StatusId
                                                           ,FundSourceDescrip
                                                           ,CampGrpId
                                                           ,ModUser
                                                           ,ModDate
                                                           ,TitleIV
                                                           ,AwardTypeId
                                                           ,AwardYear
                                                           ,AdvFundSourceId
                                                           ,CutoffDate
                                                           ,IPEDSValue
                                                            )
                VALUES ( NEWID()                 -- FundSourceId - uniqueidentifier
                        ,'STAFU'                 -- FundSourceCode - varchar(12)
                        ,@ActiveStatusId         -- StatusId - uniqueidentifier
                        ,'Federal Stafford Loan' -- FundSourceDescrip - varchar(100)
                        ,@CampusGroupId          -- CampGrpId - uniqueidentifier
                        ,'Conversion'            -- ModUser - varchar(50)
                        ,GETDATE()               -- ModDate - datetime
                        ,1                       -- TitleIV - bit
                        ,2                       -- AwardTypeId - int
                        ,NULL                    -- AwardYear - char(4)
                        ,8                       -- AdvFundSourceId - tinyint
                        ,NULL                    -- CutoffDate - datetime
                        ,NULL                    -- IPEDSValue - int
                    );
            END;

        --For FreedomBridge.bridge.saFundSources
        SELECT     targetDb.FundSourceId AS trg_FundSourceId
                  ,targetDb.FundSourceDescrip AS FundSourceDescrip
                  ,sourceDb.RecordKey AS src_RecordKey
                  ,RTRIM(LTRIM(sourceDb.FullDescrip)) AS src_FullDescrip
                  ,CASE WHEN targetDb.AdvFundSourceId = 8 THEN 1
                        ELSE 0
                   END AS src_UnSubLoanFlag
                  ,targetDb.CampGrpId AS CampGrpId
        INTO       #saFundSources
        FROM       FreedomAdvantage.dbo.saFundSources AS targetDb
        INNER JOIN Freedom.dbo.AWDTYPES_FIL AS sourceDb ON targetDb.FundSourceDescrip = RTRIM(LTRIM(sourceDb.FullDescrip))
        WHERE      targetDb.CampGrpId = @CampusGroupId
                   AND NOT EXISTS (
                                  SELECT *
                                  FROM   FreedomBridge.bridge.saFundSources
                                  WHERE  src_FullDescrip = targetDb.FundSourceDescrip
                                         AND targetDb.CampGrpId = @CampusGroupId
                                         AND targetDb.FundSourceId = trg_FundSourceId
                                  );



        INSERT INTO FreedomBridge.bridge.saFundSources
                    SELECT trg_FundSourceId
                          ,src_RecordKey
                          ,src_FullDescrip
                          ,ISNULL(src_UnSubLoanFlag, '')
                          ,CampGrpId
                    FROM   #saFundSources
                    WHERE  trg_FundSourceId IS NOT NULL;
        --AND NOT EXISTS (
        --               SELECT T.trg_FundSourceId
        --               FROM   bridge.saFundSources T
        --               WHERE  T.src_FullDescrip = src_FullDescrip
        --                      AND T.CampGrpId = CampGrpId
        --               );


        DROP TABLE #saFundSources;
        DROP TABLE #Settings;
        DROP TABLE #saFundSourcesTempTable;


        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..saFundSources
                      WHERE  FundSourceDescrip = 'Federal Unsubsidized Loan'
                             AND FundSourceCode = 'DL - Unsub'
                             AND AdvFundSourceId = 8
                             AND CampGrpId = @CampusGroupId
                      )
            BEGIN
                UPDATE FreedomAdvantage..saFundSources
                SET    FundSourceDescrip = 'Federal Unsubsidized Loan'
                      ,FundSourceCode = 'DL - Unsub'
                WHERE  (
                       (
                       AdvFundSourceId = 8
                       AND FundSourceDescrip = 'Federal Stafford Loan'
                       )
                       OR FundSourceCode = 'STAFU'
                       )
                       AND CampGrpId = @CampusGroupId;
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   FreedomAdvantage..saFundSources
                      WHERE  FundSourceDescrip = 'Federal Subsidized Loan'
                             AND FundSourceCode = 'DL - Sub'
                             AND AdvFundSourceId = 7
                             AND CampGrpId = @CampusGroupId
                      )
            BEGIN
                UPDATE FreedomAdvantage..saFundSources
                SET    FundSourceDescrip = 'Federal Subsidized Loan'
                      ,FundSourceCode = 'DL - Sub'
                WHERE  (
                       (
                       AdvFundSourceId = 7
                       AND FundSourceDescrip = 'Federal Stafford Loan'
                       )
                       OR FundSourceCode = 'STAF'
                       )
                       AND CampGrpId = @CampusGroupId;
            END;
        --SELECT    dFSource.FundSourceId AS trg_FundSourceId      --uniqueidentifier
        --         ,dFSource.FundSourceDescrip
        --         ,sAType.RecordKey AS src_RecordKey              --uniqueidentifier
        --         ,sAType.FullDescrip AS src_FullDescrip          --varchar(35)
        --         ,mFundSource.UnSubLoanFlag AS src_UnSubLoanFlag --varchar(1)
        ---- INTO   #saFundSources
        --FROM      Freedom.dbo.AWDTYPES_FIL sAType
        --LEFT JOIN map.saFundSources mFundSource ON mFundSource.src_FullDescrip = LTRIM(RTRIM(sAType.FullDescrip))
        --LEFT JOIN FreedomAdvantage..saFundSources dFSource ON dFSource.FundSourceDescrip = mFundSource.trg_FundSourceDescrip
        --OR (
        --       dFSource.FundSourceDescrip = LTRIM(RTRIM(sAType.FullDescrip))
        --       AND dFSource.FundSourceDescrip <> 'Federal Stafford Loan'
        --   )
        --LEFT JOIN bridge.saFundSources bIgnore ON bIgnore.src_FullDescrip = sAType.FullDescrip
        --WHERE     bIgnore.src_RecordKey IS NULL;

        --INSERT INTO FreedomBridge.bridge.saFundSources
        --            SELECT trg_FundSourceId
        --                  ,src_RecordKey
        --                  ,src_FullDescrip
        --                  ,ISNULL(src_UnSubLoanFlag, '')
        --            FROM   #saFundSources
        --            WHERE  trg_FundSourceId IS NOT NULL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saFundSources]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun] 

        COMMIT TRANSACTION;

    END;


GO
