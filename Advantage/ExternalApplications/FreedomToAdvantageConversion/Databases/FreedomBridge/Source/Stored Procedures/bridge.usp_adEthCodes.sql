SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-09-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_adEthCodes]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dEth.EthCodeDescrip IS NULL THEN NEWID()
                    ELSE dEth.EthCodeId
               END AS EthCodeId                                                       -- uniqueidentifier
             , UPPER(FreedomBridge.dbo.udf_FrstLtrWord(FLook.Description)) AS EthCode -- varchar(50)
             , dStatus.StatusId AS StatusId                                           -- uniqueidentifier
             , FLook.Description AS EthCodeDescrip                                    -- varchar(50)		        
             , dCampGrp.CampGrpId AS CampGrpId                                        -- uniqueidentifier
             , 'sa' AS ModUser                                                        -- varchar(50)
             , GETDATE() AS ModDate                                                   -- datetime				
             , ISNULL(dEth.IPEDSSequence, 0) AS IPEDSSequence                         -- int
             , ISNULL(dEth.IPEDSValue, 0) AS IPEDSValue                               -- int
             , FLook.ID
        INTO   #EthCodes
        FROM   FreedomBridge.stage.FLookUp FLook
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        INNER JOIN FreedomAdvantage..syCampGrps dCampGrp ON dCampGrp.CampGrpCode = 'ALL'
        LEFT JOIN FreedomAdvantage..adEthCodes dEth ON dEth.CampGrpId = dCampGrp.CampGrpId
                                                       AND dEth.IPEDSValue = CASE WHEN FLook.ID = 1 THEN 23
                                                                                  WHEN FLook.ID = 2 THEN 24
                                                                                  WHEN FLook.ID = 3 THEN 25
                                                                                  WHEN FLook.ID = 4 THEN 26
                                                                                  WHEN FLook.ID = 5 THEN 27
                                                                                  WHEN FLook.ID = 6 THEN 28
                                                                                  WHEN FLook.ID = 7 THEN 82
                                                                                  WHEN FLook.ID = 8 THEN 83
                                                                             END
        WHERE  FLook.LookupName = 'RaceTypes';

        ALTER TABLE FreedomAdvantage..adEthCodes DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..adEthCodes
                    SELECT tEth.EthCodeId
                         , tEth.EthCode
                         , tEth.StatusId
                         , tEth.EthCodeDescrip
                         , tEth.CampGrpId
                         , tEth.ModUser
                         , tEth.ModDate
                         , tEth.IPEDSSequence
                         , tEth.IPEDSValue
                    FROM   #EthCodes tEth
                    LEFT JOIN FreedomAdvantage..adEthCodes dEth ON dEth.CampGrpId = tEth.CampGrpId
                                                                   AND dEth.EthCodeId = tEth.EthCodeId
                    WHERE  dEth.EthCodeDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..adDegCertSeeking ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.adEthCodes')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.adEthCodes
                    (
                        trg_EthCodeId UNIQUEIDENTIFIER
                      , trg_IPEDSValue INT
                      , src_Description VARCHAR(255)
                      , src_ID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_Description )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.adEthCodes
                    SELECT tEth.EthCodeId
                         , tEth.IPEDSValue
                         , tEth.EthCodeDescrip
                         , tEth.ID
                    FROM   #EthCodes tEth
                    LEFT JOIN FreedomBridge.bridge.adEthCodes bEth ON bEth.trg_EthCodeId = tEth.EthCodeId
                    WHERE  bEth.trg_EthCodeId IS NULL;

        DROP TABLE #EthCodes;
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_adEthCodes]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun] 
        COMMIT TRANSACTION;

    END;
GO
