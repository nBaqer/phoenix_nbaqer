SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arTriggerOffsetTyps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dTrigOff.TrigOffTypDescrip IS NULL THEN FLook.ID
                    ELSE dTrigOff.TrigOffsetTypeId
               END AS TrigOffsetTypeId                -- tinyint
             , FLook.Description AS TrigOffTypDescrip -- varchar(50)
             , FLook.ID
        INTO   #TrigOff
        FROM   FreedomBridge.stage.FLookUp FLook
        LEFT JOIN FreedomAdvantage..arTriggerOffsetTyps dTrigOff ON dTrigOff.TrigOffTypDescrip = FLook.Description
        WHERE  FLook.LookupName = 'SAPTrigOffsetType';

        ALTER TABLE FreedomAdvantage..arTriggerOffsetTyps DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arTriggerOffsetTyps
                    SELECT tTrigOff.TrigOffsetTypeId
                         , tTrigOff.TrigOffTypDescrip
                    FROM   #TrigOff tTrigOff
                    LEFT JOIN FreedomAdvantage..arTriggerOffsetTyps dTrigOff ON dTrigOff.TrigOffTypDescrip = tTrigOff.TrigOffTypDescrip
                    WHERE  dTrigOff.TrigOffTypDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..arTriggerOffsetTyps ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arTriggerOffsetTyps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arTriggerOffsetTyps
                    (
                        trg_TrigOffsetTypId TINYINT
                      , src_TrigOffDescription VARCHAR(255)
                      , src_TrigOffID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_TrigOffDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arTriggerOffsetTyps
                    SELECT tTrigOff.TrigOffsetTypeId
                         , tTrigOff.TrigOffTypDescrip
                         , tTrigOff.ID
                    FROM   #TrigOff tTrigOff
                    LEFT JOIN FreedomBridge.bridge.arTriggerOffsetTyps bTrigOff ON bTrigOff.src_TrigOffDescription = tTrigOff.TrigOffTypDescrip
                                                                                   AND bTrigOff.src_TrigOffID = tTrigOff.ID
                    WHERE  bTrigOff.trg_TrigOffsetTypId IS NULL;

        DROP TABLE #TrigOff;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arTriggerOffsetTyps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
