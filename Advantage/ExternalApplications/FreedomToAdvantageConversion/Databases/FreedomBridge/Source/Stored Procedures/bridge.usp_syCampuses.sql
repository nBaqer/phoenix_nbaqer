SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 9-24-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syCampuses]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@CampDescrip VARCHAR(50);

        SELECT @CampusId = CampusId
              ,@CampDescrip = CampDescrip
        FROM   FreedomAdvantage..syCampuses;

        SELECT     CASE WHEN dCamp.CampCode IS NULL
                             AND @CampDescrip != 'Sample' THEN NEWID()
                        WHEN @CampDescrip = 'Sample' THEN @CampusId
                        ELSE dCamp.CampusId
                   END AS CampusId                                                         -- uniqueidentifier
                  ,sSchool.SchoolID AS CampCode                                            -- varchar(10)
                  ,dCamp.CampusId AS dCampusId
                  ,REPLACE(REPLACE(sSchool.Name,'''',''),'`','') AS CampDescrip                                             -- varchar(100)
                  ,sSchool.PhysAddrLine1 AS Address1                                       -- varchar(80)
                  ,NULL AS Address2                                                        -- varchar(50)
                  ,sSchool.PhysAddrCity AS City                                            -- varchar(80)
                  ,dState.StateId AS StateId                                               -- uniqueidentifier
                  ,FreedomBridge.dbo.udf_GetNumericOnly(sSchool.PhysAddrZip) AS Zip        -- varchar(10)
                  ,dCountry.CountryId AS CountryId                                         -- uniqueidentifier
                  ,dStatus.StatusId AS StatusId                                            -- uniqueidentifier
                  ,GETDATE() AS ModDate                                                    -- datetime
                  ,'sa' AS ModUser                                                         -- varchar(50)
                  ,FreedomBridge.dbo.udf_GetNumericOnly(sSchool.DfltSchPhone) AS Phone1    -- char(30)
                  ,NULL AS Phone2                                                          -- char(30)
                  ,NULL AS Phone3                                                          -- char(30)
                  ,NULL AS Fax                                                             -- char(30)
                  ,NULL AS Email                                                           -- varchar(50)
                  ,NULL AS Website                                                         -- varchar(50)
                  ,NULL AS IsCorporate                                                     -- bit
                  ,NULL AS TranscriptAuthZnTitle                                           -- varchar(100)
                  ,NULL AS TranscriptAuthZnName                                            -- varchar(150)
                  ,NULL AS TCSourcePath                                                    -- varchar(100)
                  ,NULL AS TCTargetPath                                                    -- varchar(100)
                  ,NULL AS IsRemoteServer                                                  -- bit
                  ,NULL AS PortalContactEmail                                              -- varchar(100)
                  ,NULL AS RemoteServerUsrNm                                               -- varchar(100)
                  ,NULL AS RemoteServerPwd                                                 -- varchar(100)
                  ,NULL AS SourceFolderLoc                                                 -- varchar(100)
                  ,NULL AS TargetFolderLoc                                                 -- varchar(100)
                  ,1 AS UseCampusAddress                                                   -- bit
                  ,sSchool.PhysAddrLine1 AS InvAddress1                                    -- varchar(80)
                  ,NULL AS InvAddress2                                                     -- varchar(50)
                  ,sSchool.PhysAddrCity AS InvCity                                         -- varchar(80)
                  ,dState.StateId AS InvStateID                                            -- uniqueidentifier
                  ,FreedomBridge.dbo.udf_GetNumericOnly(sSchool.PhysAddrZip) AS InvZip     -- varchar(10)
                  ,dCountry.CountryId AS InvCountryID                                      -- uniqueidentifier
                  ,FreedomBridge.dbo.udf_GetNumericOnly(sSchool.DfltSchPhone) AS InvPhone1 -- char(30)
                  ,NULL AS InvFax                                                          -- char(30)
                  ,NULL AS FLSourcePath                                                    -- varchar(100)
                  ,NULL AS FLTargetPath                                                    -- varchar(100)
                  ,NULL AS FLExceptionPath                                                 -- varchar(100)
                  ,0 AS IsRemoteServerFL                                                   -- bit
                  ,NULL AS RemoteServerUsrNmFL                                             -- varchar(100)
                  ,NULL AS RemoteServerPwdFL                                               -- varchar(100)
                  ,NULL AS SourceFolderLocFL                                               -- varchar(100)
                  ,NULL AS TargetFolderLocFL                                               -- varchar(100)
                  ,NULL AS ILSourcePath                                                    -- varchar(100)
                  ,NULL AS ILArchivePath                                                   -- varchar(100)
                  ,NULL AS ILExceptionPath                                                 -- varchar(100)
                  ,0 AS IsRemoteServerIL                                                   -- bit
                  ,NULL AS RemoteServerUserNameIL                                          -- varchar(100)
                  ,NULL AS RemoteServerPasswordIL                                          -- varchar(100)
                  ,NULL AS SourceCategoryAll                                               -- bit
                  ,NULL AS SourceCategoryId                                                -- varchar(50)
                  ,NULL AS SourceTypeAll                                                   -- bit
                  ,NULL AS SourceTypeId                                                    -- varchar(50)
                  ,NULL AS AdmRepAll                                                       -- bit
                  ,NULL AS AdmRepId                                                        -- varchar(50)
                  ,NULL AS LeadStatusAll                                                   -- bit
                  ,NULL AS LeadStatusId                                                    -- varchar(50)
                  ,NULL AS PhoneType1All                                                   -- bit
                  ,NULL AS PhoneType1Id                                                    -- varchar(50)
                  ,NULL AS PhoneType2All                                                   -- bit
                  ,NULL AS PhoneType2Id                                                    -- varchar(50)
                  ,NULL AS AddTypeAll                                                      -- bit
                  ,NULL AS AddTypeId                                                       -- varchar(50)
                  ,NULL AS GenderAll                                                       -- bit
                  ,NULL AS GenderId                                                        -- varchar(50)
                  ,NULL AS PortalCountryAll                                                -- bit
                  ,NULL AS PortalCountryId                                                 -- varchar(50)
                  ,NULL AS PortalContactEmailAll                                           -- bit
                  ,NULL AS EmailSubjectAll                                                 -- bit
                  ,NULL AS EmailSubject                                                    -- varchar(250)
                  ,NULL AS EmailBodyAll                                                    -- bit
                  ,CAST(NULL AS TEXT) AS EmailBody                                         -- text
                  ,sSchool.OPEID AS OPEID                                                  -- VARCHAR(8) NULL
                  ,REPLACE(REPLACE(sSchool.Name,'''',''),'`','') AS SchoolName                                              -- VARCHAR(100) NULL
                  ,NULL AS Token1098TService                                               -- VARCHAR(38) NULL
                  ,NULL AS SchoolCodeKissSchoolId                                          -- VARCHAR(20) NULL
                  ,NULL AS CmsId                                                           -- VARCHAR(7) NULL
                  ,NULL AS FSEOGMatchType                                                  -- VARCHAR(1) NULL
                  ,CAST(0 AS BIT) AS IsBranch
                  ,CAST(NULL AS UNIQUEIDENTIFIER) AS ParentCampusId
                  ,0 AS AllowGraduateAndStillOweMoney
                  ,0 AS AllowGraduateWithoutFullCompletion
        INTO       #Campuses
        FROM       Freedom..SCHOOL_DEF sSchool
        INNER JOIN FreedomAdvantage..syStates dState ON dState.StateCode = sSchool.PhysAddrState
        INNER JOIN FreedomAdvantage..adCountries dCountry ON dCountry.CountryCode = 'USA'
        INNER JOIN FreedomAdvantage..syStatuses dStatus ON dStatus.Status = 'Active'
        LEFT JOIN  FreedomAdvantage..syCampuses dCamp ON dCamp.CampDescrip = REPLACE(REPLACE(sSchool.Name,'''',''),'`','')
                                                         AND dCamp.CampCode = sSchool.SchoolID
        WHERE      dCamp.CampCode IS NULL;

        -- Insert campus if the sample campus does not already exist
        IF NOT EXISTS (
                      SELECT CampCode
                      FROM   FreedomAdvantage..syCampuses
                      WHERE  CampCode = 'SAMPLE'
                      )
            BEGIN

                ALTER TABLE FreedomAdvantage..syCampuses DISABLE TRIGGER ALL;

                INSERT INTO FreedomAdvantage..syCampuses (
                                                         CampusId
                                                        ,CampCode
                                                        ,CampDescrip
                                                        ,Address1
                                                        ,Address2
                                                        ,City
                                                        ,StateId
                                                        ,Zip
                                                        ,CountryId
                                                        ,StatusId
                                                        ,ModDate
                                                        ,ModUser
                                                        ,Phone1
                                                        ,Phone2
                                                        ,Phone3
                                                        ,Fax
                                                        ,Email
                                                        ,Website
                                                        ,IsCorporate
                                                        ,TranscriptAuthZnTitle
                                                        ,TranscriptAuthZnName
                                                        ,TCSourcePath
                                                        ,TCTargetPath
                                                        ,IsRemoteServer
                                                        ,PortalContactEmail
                                                        ,RemoteServerUsrNm
                                                        ,RemoteServerPwd
                                                        ,SourceFolderLoc
                                                        ,TargetFolderLoc
                                                        ,UseCampusAddress
                                                        ,InvAddress1
                                                        ,InvAddress2
                                                        ,InvCity
                                                        ,InvStateID
                                                        ,InvZip
                                                        ,InvCountryID
                                                        ,InvPhone1
                                                        ,InvFax
                                                        ,FLSourcePath
                                                        ,FLTargetPath
                                                        ,FLExceptionPath
                                                        ,IsRemoteServerFL
                                                        ,RemoteServerUsrNmFL
                                                        ,RemoteServerPwdFL
                                                        ,SourceFolderLocFL
                                                        ,TargetFolderLocFL
                                                        ,ILSourcePath
                                                        ,ILArchivePath
                                                        ,ILExceptionPath
                                                        ,IsRemoteServerIL
                                                        ,RemoteServerUserNameIL
                                                        ,RemoteServerPasswordIL
                                                        ,SourceCategoryAll
                                                        ,SourceCategoryId
                                                        ,SourceTypeAll
                                                        ,SourceTypeId
                                                        ,AdmRepAll
                                                        ,AdmRepId
                                                        ,LeadStatusAll
                                                        ,LeadStatusId
                                                        ,PhoneType1All
                                                        ,PhoneType1Id
                                                        ,PhoneType2All
                                                        ,PhoneType2Id
                                                        ,AddTypeAll
                                                        ,AddTypeId
                                                        ,GenderAll
                                                        ,GenderId
                                                        ,PortalCountryAll
                                                        ,PortalCountryId
                                                        ,PortalContactEmailAll
                                                        ,EmailSubjectAll
                                                        ,EmailSubject
                                                        ,EmailBodyAll
                                                        ,EmailBody
                                                        ,OPEID
                                                        ,SchoolName
                                                        ,Token1098TService
                                                        ,SchoolCodeKissSchoolId
                                                        ,CmsId
                                                        ,FSEOGMatchType
                                                        ,Isbranch
                                                        ,ParentCampusId
                                                        ,AllowGraduateAndStillOweMoney
                                                        ,AllowGraduateWithoutFullCompletion
                                                         )
                            SELECT CampusId                                         -- uniqueidentifier
                                  ,CampCode AS CampCode                             -- varchar(10)
                                  ,CampDescrip AS CampDescrip                       -- varchar(100)
                                  ,Address1 AS Address1                             -- varchar(80)
                                  ,Address2 AS Address2                             -- varchar(50)
                                  ,City AS City                                     -- varchar(80)
                                  ,StateId AS StateId                               -- uniqueidentifier
                                  ,Zip AS Zip                                       -- varchar(10)
                                  ,CountryId AS CountryId                           -- uniqueidentifier
                                  ,StatusId AS StatusId                             -- uniqueidentifier
                                  ,ModDate AS ModDate                               -- datetime
                                  ,ModUser AS ModUser                               -- varchar(50)
                                  ,Phone1 AS Phone1                                 -- char(30)
                                  ,Phone2 AS Phone2                                 -- char(30)
                                  ,Phone3 AS Phone3                                 -- char(30)
                                  ,Fax AS Fax                                       -- char(30)
                                  ,Email AS Email                                   -- varchar(50)
                                  ,Website AS Website                               -- varchar(50)
                                  ,IsCorporate AS IsCorporate                       -- bit
                                  ,TranscriptAuthZnTitle AS TranscriptAuthZnTitle   -- varchar(100)
                                  ,TranscriptAuthZnName AS TranscriptAuthZnName     -- varchar(150)
                                  ,TCSourcePath AS TCSourcePath                     -- varchar(100)
                                  ,TCTargetPath AS TCTargetPath                     -- varchar(100)
                                  ,IsRemoteServer AS IsRemoteServer                 -- bit
                                  ,PortalContactEmail AS PortalContactEmail         -- varchar(100)
                                  ,RemoteServerUsrNm AS RemoteServerUsrNm           -- varchar(100)
                                  ,RemoteServerPwd AS RemoteServerPwd               -- varchar(100)
                                  ,SourceFolderLoc AS SourceFolderLoc               -- varchar(100)
                                  ,TargetFolderLoc AS TargetFolderLoc               -- varchar(100)
                                  ,UseCampusAddress AS UseCampusAddress             -- bit
                                  ,InvAddress1 AS InvAddress1                       -- varchar(80)
                                  ,InvAddress2 AS InvAddress2                       -- varchar(50)
                                  ,InvCity AS InvCity                               -- varchar(80)
                                  ,InvStateID AS InvStateID                         -- uniqueidentifier
                                  ,InvZip AS InvZip                                 -- varchar(10)
                                  ,InvCountryID AS InvCountryID                     -- uniqueidentifier
                                  ,InvPhone1 AS InvPhone1                           -- char(30)
                                  ,InvFax AS InvFax                                 -- char(30)
                                  ,FLSourcePath AS FLSourcePath                     -- varchar(100)
                                  ,FLTargetPath AS FLTargetPath                     -- varchar(100)
                                  ,FLExceptionPath AS FLExceptionPath               -- varchar(100)
                                  ,IsRemoteServerFL AS IsRemoteServerFL             -- bit
                                  ,RemoteServerUsrNmFL AS RemoteServerUsrNmFL       -- varchar(100)
                                  ,RemoteServerPwdFL AS RemoteServerPwdFL           -- varchar(100)
                                  ,SourceFolderLocFL AS SourceFolderLocFL           -- varchar(100)
                                  ,TargetFolderLocFL AS TargetFolderLocFL           -- varchar(100)
                                  ,ILSourcePath AS ILSourcePath                     -- varchar(100)
                                  ,ILArchivePath AS ILArchivePath                   -- varchar(100)
                                  ,ILExceptionPath AS ILExceptionPath               -- varchar(100)
                                  ,IsRemoteServerIL AS IsRemoteServerIL             -- bit
                                  ,RemoteServerUserNameIL AS RemoteServerUserNameIL -- varchar(100)
                                  ,RemoteServerPasswordIL AS RemoteServerPasswordIL -- varchar(100)
                                  ,SourceCategoryAll AS SourceCategoryAll           -- bit
                                  ,SourceCategoryId AS SourceCategoryId             -- varchar(50)
                                  ,SourceTypeAll AS SourceTypeAll                   -- bit
                                  ,SourceTypeId AS SourceTypeId                     -- varchar(50)
                                  ,AdmRepAll AS AdmRepAll                           -- bit
                                  ,AdmRepId AS AdmRepId                             -- varchar(50)
                                  ,LeadStatusAll AS LeadStatusAll                   -- bit
                                  ,LeadStatusId AS LeadStatusId                     -- varchar(50)
                                  ,PhoneType1All AS PhoneType1All                   -- bit
                                  ,PhoneType1Id AS PhoneType1Id                     -- varchar(50)
                                  ,PhoneType2All AS PhoneType2All                   -- bit
                                  ,PhoneType2Id AS PhoneType2Id                     -- varchar(50)
                                  ,AddTypeAll AS AddTypeAll                         -- bit
                                  ,AddTypeId AS AddTypeId                           -- varchar(50)
                                  ,GenderAll AS GenderAll                           -- bit
                                  ,GenderId AS GenderId                             -- varchar(50)
                                  ,PortalCountryAll AS PortalCountryAll             -- bit
                                  ,PortalCountryId AS PortalCountryId               -- varchar(50)
                                  ,PortalContactEmailAll AS PortalContactEmailAll   -- bit
                                  ,EmailSubjectAll AS EmailSubjectAll               -- bit
                                  ,EmailSubject AS EmailSubject                     -- varchar(250)
                                  ,EmailBodyAll AS EmailBodyAll                     -- bit
                                  ,EmailBody AS EmailBody                           -- text
                                  ,RTRIM(LTRIM(OPEID))                              -- VARCHAR(8) NULL
                                  ,SchoolName                                       -- VARCHAR(100) NULL
                                  ,Token1098TService                                -- VARCHAR(38) NULL
                                  ,SchoolCodeKissSchoolId                           -- VARCHAR(20) NULL
                                  ,CmsId                                            -- VARCHAR(7) NULL
                                  ,FSEOGMatchType                                   -- VARCHAR(7) NULL
                                  ,IsBranch
                                  ,ParentCampusId
                                  ,AllowGraduateAndStillOweMoney
                                  ,AllowGraduateWithoutFullCompletion
                            FROM   #Campuses;

            END;

        ELSE
            -- Update sample campus with school campus information
            BEGIN

                UPDATE     dCamp
                SET        CampCode = tCamp.CampCode
                          ,CampDescrip = tCamp.CampDescrip
                          --,CampDescrip = tCamp.City
                          ,Address1 = tCamp.Address1
                          ,City = tCamp.City
                          ,StateId = tCamp.StateId
                          ,Zip = tCamp.Zip
                          ,CountryId = tCamp.CountryId
                          ,StatusId = tCamp.StatusId
                          ,ModDate = tCamp.ModDate
                          ,ModUser = tCamp.ModUser
                          ,Phone1 = tCamp.Phone1
                          ,Email = tCamp.Email
                          ,Website = tCamp.Website
                          ,InvAddress1 = tCamp.InvAddress1
                          ,InvCity = tCamp.InvCity
                          ,InvStateID = tCamp.InvStateID
                          ,InvZip = tCamp.InvZip
                          ,InvCountryID = tCamp.InvCountryID
                          ,InvPhone1 = tCamp.InvPhone1
                          ,SchoolName = tCamp.SchoolName
                          ,dCamp.OPEID = tCamp.OPEID
                FROM       FreedomAdvantage..syCampuses dCamp
                INNER JOIN #Campuses tCamp ON tCamp.CampusId = dCamp.CampusId;

                ALTER TABLE FreedomAdvantage..syCampuses ENABLE TRIGGER ALL;

            END;

        -- Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                      SELECT *
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syCampuses')
                             AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syCampuses
                    (
                        trg_syCampusId UNIQUEIDENTIFIER
                       ,src_SchoolId INT NOT NULL
                       ,src_Name VARCHAR(50)
                       ,
                       PRIMARY KEY ( src_SchoolId )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.syCampuses
                    SELECT DISTINCT CampusId
                          ,CampCode
                          ,CampDescrip
                    FROM   #Campuses;

        --ALTER TABLE FreedomAdvantage..syConfigAppSetValues DISABLE TRIGGER ALL

        --UPDATE  FreedomAdvantage..syConfigAppSetValues
        --SET     Value = CampDescrip
        --FROM    #Campuses
        --WHERE   SettingId IN ( 15,66 )

        --ALTER TABLE FreedomAdvantage..syConfigAppSetValues ENABLE TRIGGER ALL

        DROP TABLE #Campuses;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_syCampuses]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;



GO
