SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 04-16-18>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arFASAPChkResults]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;
        DECLARE @CampusId UNIQUEIDENTIFIER
               ,@SchoolID INT;

        DECLARE @Comments TABLE
            (
                ParentKey INT NOT NULL
               ,Text NVARCHAR(MAX)
               ,NextRec INT NOT NULL
            );

        SELECT Entity
              ,Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        INSERT INTO @Comments (
                              ParentKey
                             ,Text
                             ,NextRec
                              )
                    SELECT   ParentKey
                            ,Text
                            ,CASE NextRec
                                  WHEN 0 THEN 999999
                                  ELSE NextRec
                             END
                    FROM     Freedom..ACADCHK_LOG_CMT
                    ORDER BY ParentKey
                            ,CASE NextRec
                                  WHEN 0 THEN 999999
                                  ELSE NextRec
                             END;

        SELECT   DISTINCT ParentKey
                         ,( STUFF((
                                  SELECT CAST(', ' + Text AS VARCHAR(MAX))
                                  FROM   @Comments C
                                  WHERE  ( C.ParentKey = E.ParentKey )
                                  FOR XML PATH('')
                                  )
                                 ,1
                                 ,2
                                 ,''
                                 )
                          ) AS Text
        INTO     #Comments
        FROM     @Comments E
        GROUP BY ParentKey
                ,NextRec;


        SELECT   ROW_NUMBER() OVER ( PARTITION BY advantageSapDetails.SAPId
                                     ORDER BY advantageSapDetails.SAPId
                                             ,advantageSapDetails.TrigOffsetSeq
                                             ,advantageSapDetails.TrigValue
                                   ) + 1 AS Increment
                ,*
        INTO     #FSAPDetailsWithIncrement
        FROM     FreedomAdvantage..arSAPDetails advantageSapDetails
        ORDER BY advantageSapDetails.SAPId;

        SELECT     NEWID() AS StdRecKey                                    -- StdRecKey - uniqueidentifier
                  ,bridgeStudentEnrollments.trg_StuEnrollId AS StuEnrollId -- StuEnrollId - uniqueidentifier
                  ,freedomFSapResults.Period - 1 AS Period                 -- Period - int
                  ,CASE WHEN freedomFSapResults.PrivResult = 1 THEN 1
                        ELSE 0
                   END AS IsMakingSAP                                      -- IsMakingSAP - bit
                  ,CASE WHEN freedomFSapResults.PrivTrigUnit < 4 THEN freedomFSapResults.HrsAttempted
                        ELSE 0
                   END AS HrsAttended                                      -- HrsAttended - int
                  ,CASE WHEN freedomFSapResults.PrivTrigUnit < 4 THEN freedomFSapResults.HrsEarned
                        ELSE 0
                   END AS HrsEarned                                        -- HrsEarned - int
                  ,CASE WHEN freedomFSapResults.PrivTrigUnit > 3 THEN freedomFSapResults.HrsAttempted
                        ELSE 0
                   END AS CredsAttempted                                   -- CredsAttempted - decimal(18, 2)
                  ,CASE WHEN freedomFSapResults.PrivTrigUnit > 3 THEN freedomFSapResults.HrsEarned
                        ELSE 0
                   END AS CredsEarned                                      -- CredsEarned - decimal(18, 2)
                  ,freedomFSapResults.GPA                                  -- GPA - decimal(18, 2)
                  ,freedomFSapResults.AddDate AS DatePerformed             -- DatePerformed - datetime
                  ,CASE WHEN freedomSapCheckComments.Text LIKE '%Grad%' THEN
                            SUBSTRING(
                                         freedomSapCheckComments.Text
                                        ,CHARINDEX('Grad', freedomSapCheckComments.Text)
                                        ,LEN(freedomSapCheckComments.Text) - ( CHARINDEX('Grad', freedomSapCheckComments.Text) - 1 )
                                     )
                        WHEN freedomSapCheckComments.Text LIKE '%Balance is%' THEN ''
                        ELSE freedomSapCheckComments.Text
                   END AS Comments                                         -- Comments - varchar(max)
                  ,'sa' AS ModUser                                         -- ModUser - varchar(50)
                  ,GETDATE() AS ModDate                                    -- ModDate - datetime
                  ,advantageSapDetails.SAPDetailId AS SAPDetailId          -- SAPDetailId - uniqueidentifier
                  ,freedomFSapResults.GPA AS Average                       -- Average - decimal(18, 2)
                  ,NULL AS Attendance                                      -- Attendance - decimal(19, 2)
                  ,NULL AS CumFinAidCredits                                -- CumFinAidCredits - decimal(19, 2)
                  ,CASE WHEN freedomFSapResults.HrsAttempted < 1 THEN 0
                        ELSE freedomFSapResults.HrsEarned / freedomFSapResults.HrsAttempted * 100
                   END AS PercentCompleted                                 -- PercentCompleted - decimal(19, 2)
                  ,freedomFSapResults.EffDate AS CheckPointDate            -- CheckPointDate - datetime
                  ,NULL AS TermStartDate                                   -- TermStartDate - datetime
                  ,0 AS PreviewSapCheck                                    -- PreviewSapCheck - bit
                  ,AdvantageTitleIVSAPStatus.Id AS TitleIVStatusId         -- TitleIVStatusId - int
                  ,CASE WHEN freedomFSapResults.PrintDate IS NULL THEN 0
                        ELSE 1
                   END AS Printed                                          -- Printed - bit
                  ,freedomFSapResults.RecordKey AS RecordKey
                  ,freedomFSapResults.StudRecKey AS StudRecKey
        INTO       #FASapChkResults
        FROM       Freedom..ACADCHK_LOG freedomFSapResults
        INNER JOIN Freedom..STUDREC freedomStudentEnrollments ON freedomStudentEnrollments.RecordKey = freedomFSapResults.StudRecKey
        INNER JOIN bridge.arStuEnrollments bridgeStudentEnrollments ON bridgeStudentEnrollments.src_RecordKey = freedomFSapResults.StudRecKey
                                                                       AND bridgeStudentEnrollments.trg_CampusId = @CampusId
        INNER JOIN Freedom..COURSE_DEF freedomProgramVersions ON freedomProgramVersions.RecordKey = freedomStudentEnrollments.CourseNumEnrolledIn
        INNER JOIN bridge.arFSAPDetails bridgeSApDetails ON bridgeSApDetails.src_ProgramVersionId = freedomProgramVersions.RecordKey
                                                            AND bridgeSApDetails.trg_CampusId = @CampusId
        INNER JOIN #FSAPDetailsWithIncrement AS advantageSapDetails ON advantageSapDetails.SAPDetailId = bridgeSApDetails.trg_SAPDetailId
                                                                       AND freedomFSapResults.Period = advantageSapDetails.Increment
        INNER JOIN FreedomAdvantage..syTitleIVSapStatus AdvantageTitleIVSAPStatus ON AdvantageTitleIVSAPStatus.Code = (
                                                                                                                      SELECT CASE freedomFSapResults.PrivResult
                                                                                                                                  WHEN 21 THEN 'Passed'
                                                                                                                                  WHEN 22 THEN 'Warning'
                                                                                                                                  WHEN 23 THEN 'Probation'
                                                                                                                                  WHEN 24 THEN 'Ineligible'
                                                                                                                                  ELSE 'freedomFSapResults'
                                                                                                                             END AS StatusCode
                                                                                                                      )
        LEFT JOIN  #Comments freedomSapCheckComments ON freedomSapCheckComments.ParentKey = freedomFSapResults.RecordKey
        WHERE      freedomFSapResults.PrivType = 7
        --AND freedomSapCheckComments.Text NOT LIKE '%$%'
        ORDER BY   freedomFSapResults.StudRecKey
                  ,freedomFSapResults.Period;



        SELECT *
              ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                               ,Period
                                   --,Comments
                                   ORDER BY DatePerformed DESC
                                 ) AS LatestRecord
        INTO   #FASapChkResults_Final
        FROM   #FASapChkResults;


        ALTER TABLE FreedomAdvantage.dbo.arFASAPChkResults DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage.dbo.arFASAPChkResults (
                                                           StdRecKey
                                                          ,StuEnrollId
                                                          ,Period
                                                          ,IsMakingSAP
                                                          ,HrsAttended
                                                          ,HrsEarned
                                                          ,CredsAttempted
                                                          ,CredsEarned
                                                          ,GPA
                                                          ,DatePerformed
                                                          ,Comments
                                                          ,ModUser
                                                          ,ModDate
                                                          ,SAPDetailId
                                                          ,Average
                                                          ,Attendance
                                                          ,CumFinAidCredits
                                                          ,PercentCompleted
                                                          ,CheckPointDate
                                                          ,TermStartDate
                                                          ,PreviewSapCheck
                                                          ,TitleIVStatusId
                                                          ,Printed
                                                           )
                    SELECT    tSAPChkRes.StdRecKey
                             ,tSAPChkRes.StuEnrollId
                             ,tSAPChkRes.Period
                             ,tSAPChkRes.IsMakingSAP
                             ,tSAPChkRes.HrsAttended
                             ,tSAPChkRes.HrsEarned
                             ,tSAPChkRes.CredsAttempted
                             ,tSAPChkRes.CredsEarned
                             ,tSAPChkRes.GPA
                             ,tSAPChkRes.DatePerformed
                             ,tSAPChkRes.Comments
                             ,tSAPChkRes.ModUser
                             ,tSAPChkRes.ModDate
                             ,tSAPChkRes.SAPDetailId
                             ,tSAPChkRes.Average
                             ,tSAPChkRes.Attendance
                             ,tSAPChkRes.CumFinAidCredits
                             ,tSAPChkRes.PercentCompleted
                             ,tSAPChkRes.CheckPointDate
                             ,tSAPChkRes.TermStartDate
                             ,tSAPChkRes.PreviewSapCheck
                             ,tSAPChkRes.TitleIVStatusId
                             ,tSAPChkRes.Printed
                    FROM      #FASapChkResults_Final tSAPChkRes
                    LEFT JOIN FreedomAdvantage.dbo.arFASAPChkResults dSAPChkRes ON dSAPChkRes.StuEnrollId = tSAPChkRes.StuEnrollId
                                                                                   AND dSAPChkRes.Period = tSAPChkRes.Period
                    WHERE     tSAPChkRes.LatestRecord = 1;

        INSERT INTO FreedomBridge.bridge.arFASAPChkResults
                    SELECT    tSAPChkRes.Period
                             ,tSAPChkRes.RecordKey
                             ,tSAPChkRes.StudRecKey
                             ,tSAPChkRes.StdRecKey
                             ,tSAPChkRes.StuEnrollId
                             ,@CampusId AS CampusId
                    FROM      #FASapChkResults_Final tSAPChkRes
                    LEFT JOIN FreedomBridge.bridge.arFASAPChkResults bSAPChkRes ON bSAPChkRes.trg_StuEnrollId = tSAPChkRes.StuEnrollId
                                                                                   AND bSAPChkRes.src_Period = tSAPChkRes.Period
                                                                                   AND bSAPChkRes.src_RecordKey = tSAPChkRes.RecordKey
                                                                                   AND bSAPChkRes.trg_CampusId = @CampusId
                    WHERE     tSAPChkRes.LatestRecord = 1;


        ALTER TABLE FreedomAdvantage.dbo.arFASAPChkResults ENABLE TRIGGER ALL;


        DROP TABLE #FASapChkResults;
        DROP TABLE #FSAPDetailsWithIncrement;
        DROP TABLE #Comments;

        INSERT INTO [dbo].[storedProceduresRun]
                    SELECT NEWID()
                          ,'[usp_arFASAPChkResults]'
                          ,GETDATE()
                          ,NULL
                          ,ISNULL(MAX(orderRun), 0) + 1
                    FROM   [dbo].[storedProceduresRun];

        COMMIT TRANSACTION;

    END;
GO
