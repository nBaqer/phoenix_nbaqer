SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_InsertComponentPlaceHolders]
AS
    BEGIN

        DECLARE @error INT = 0;
        --SELECT * FROM [FreedomAdvantage]..syResources WHERE ResourceID IN (499,500,501,502,503,533,544)
        BEGIN TRANSACTION addGradePlaceHolders;

        BEGIN TRY
            ALTER TABLE [FreedomAdvantage]..arGrdBkResults DISABLE TRIGGER ALL;
            --Insert Homework placeholders
            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 499
                                  AND gradedComponents.GradedComponent IS NULL;

            --Insert Lab work placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 500
                                  AND gradedComponents.GradedComponent IS NULL;

            --Insert Exam placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 501
                                  AND gradedComponents.GradedComponent IS NULL;
            ----Insert Final placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 502
                                  AND gradedComponents.GradedComponent IS NULL;
            --Insert Lab Hours placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 503
                                  AND gradedComponents.GradedComponent IS NULL;
            --Insert Practical Exams placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 533
                                  AND gradedComponents.GradedComponent IS NULL;

            --Insert Externship placeholders

            INSERT INTO [FreedomAdvantage]..arGrdBkResults (
                                                           GrdBkResultId
                                                          ,ClsSectionId
                                                          ,InstrGrdBkWgtDetailId
                                                          ,Comments
                                                          ,StuEnrollId
                                                          ,ModUser
                                                          ,ModDate
                                                           )
                        SELECT    NEWID()                    -- GrdBkResultId - uniqueidentifier
                                 ,cs.ClsSectionId            -- ClsSectionId - uniqueidentifier
                                 ,gbwd.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                 ,gct.Descrip                -- Comments - varchar(50)
                                 ,se.StuEnrollId             -- StuEnrollId - uniqueidentifier
                                 ,'PH'                       -- ModUser - varchar(50)
                                 ,GETDATE()                  -- ModDate - datetime

                        FROM      [FreedomAdvantage]..arResults ar
                        JOIN      [FreedomAdvantage]..arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
                        JOIN      [FreedomAdvantage]..arClassSections cs ON ar.TestId = cs.ClsSectionId
                        JOIN      [FreedomAdvantage]..arProgVerDef pv ON pv.ProgVerDefId = cs.ProgramVersionDefinitionId
                        JOIN      [FreedomAdvantage]..arReqs reqs ON pv.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arGrdBkWeights gbw ON gbw.ReqId = reqs.ReqId
                        JOIN      [FreedomAdvantage]..arTerm term ON term.TermId = cs.TermId
                        JOIN      [FreedomAdvantage]..arGrdBkWgtDetails gbwd ON gbw.InstrGrdBkWgtId = gbwd.InstrGrdBkWgtId
                        JOIN      [FreedomAdvantage]..arGrdComponentTypes gct ON gbwd.GrdComponentTypeId = gct.GrdComponentTypeId
                        LEFT JOIN (
                                  SELECT ClsSectionId AS GradedClassSection
                                        ,InstrGrdBkWgtDetailId AS GradedComponent
                                        ,StuEnrollId
                                        ,Score
                                  FROM   [FreedomAdvantage]..arGrdBkResults
                                  ) gradedComponents ON gradedComponents.StuEnrollId = ar.StuEnrollId
                                                        AND gradedComponents.GradedClassSection = cs.ClsSectionId
                                                        AND gradedComponents.GradedComponent = gbwd.InstrGrdBkWgtDetailId
                        WHERE     gct.SysComponentTypeId = 544
                                  AND gradedComponents.GradedComponent IS NULL;

            ALTER TABLE [FreedomAdvantage]..arGrdBkResults ENABLE TRIGGER ALL;
        END TRY
        BEGIN CATCH
            SELECT ERROR_NUMBER() AS ErrorNumber;
            SELECT ERROR_SEVERITY() AS ErrorSeverity;
            SELECT ERROR_STATE() AS ErrorState;
            SELECT ERROR_PROCEDURE() AS ErrorProcedure;
            SELECT ERROR_LINE() AS ErrorLine;
            SELECT ERROR_MESSAGE() AS ErrorMessage;

            SET @error = 1;
        END CATCH;

        IF @error > 0
            BEGIN
                ALTER TABLE [FreedomAdvantage]..arGrdBkResults ENABLE TRIGGER ALL;
                ROLLBACK TRANSACTION addGradePlaceHolders;
                PRINT 'Failed to add grade place holders.';

            END;
        ELSE
            BEGIN
                COMMIT TRANSACTION addGradePlaceHolders;
                PRINT 'Successfully added grade place holders.';
            END;





    END;
GO
