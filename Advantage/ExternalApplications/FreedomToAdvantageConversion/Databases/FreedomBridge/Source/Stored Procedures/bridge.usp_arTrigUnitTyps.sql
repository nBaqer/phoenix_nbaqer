SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 10-20-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arTrigUnitTyps]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        SELECT CASE WHEN dTrigUnit.TrigUnitTypDescrip IS NULL THEN FLook.ID
                    ELSE dTrigUnit.TrigUnitTypId
               END AS TrigUnitTypId                    -- tinyint
             , FLook.Description AS TrigUnitTypDescrip -- varchar(50)
             , FLook.ID
        INTO   #TrigUnit
        FROM   FreedomBridge.stage.FLookUp FLook
        LEFT JOIN FreedomAdvantage..arTrigUnitTyps dTrigUnit ON dTrigUnit.TrigUnitTypDescrip LIKE LEFT(FLook.Description, 6) + '%'
        WHERE  FLook.LookupName = 'SAPTrigUnit';

        ALTER TABLE FreedomAdvantage..arTrigUnitTyps DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..arTrigUnitTyps
                    SELECT tTrigUnit.TrigUnitTypId
                         , tTrigUnit.TrigUnitTypDescrip
                    FROM   #TrigUnit tTrigUnit
                    LEFT JOIN FreedomAdvantage..arTrigUnitTyps dTrigUnit ON dTrigUnit.TrigUnitTypDescrip LIKE LEFT(tTrigUnit.TrigUnitTypDescrip, 6) + '%'
                    WHERE  dTrigUnit.TrigUnitTypDescrip IS NULL;

        ALTER TABLE FreedomAdvantage..arTrigUnitTyps ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arTrigUnitTyps')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arTrigUnitTyps
                    (
                        trg_TrigUnitTypId TINYINT
                      , src_TrigUnitDescription VARCHAR(255)
                      , src_TrigUnitID INT
                      ,
                      PRIMARY KEY CLUSTERED ( src_TrigUnitDescription )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arTrigUnitTyps
                    SELECT tTrigUnit.TrigUnitTypId
                         , tTrigUnit.TrigUnitTypDescrip
                         , tTrigUnit.ID
                    FROM   #TrigUnit tTrigUnit
                    LEFT JOIN FreedomBridge.bridge.arTrigUnitTyps bTrigUnit ON bTrigUnit.src_TrigUnitDescription = tTrigUnit.TrigUnitTypDescrip
                                                                               AND bTrigUnit.src_TrigUnitID = tTrigUnit.ID
                    WHERE  bTrigUnit.trg_TrigUnitTypId IS NULL;

        DROP TABLE #TrigUnit;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arTrigUnitTyps]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
