SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 12-04-14>
-- =============================================
CREATE PROCEDURE [bridge].[usp_arClassSectionTerms]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN dClsSectTerm.ClsSectionId IS NULL THEN NEWID()
                    ELSE dClsSectTerm.ClsSectTermId
               END AS ClsSectTermId                  -- uniqueidentifier
             , dClsSect.ClsSectionId AS ClsSectionId -- uniqueidentifier
             , dClsSect.TermId AS TermId             -- uniqueidentifier
             , dClsSect.ModDate AS ModDate           -- datetime
             , dClsSect.ModUser AS ModUser           -- varchar(50)
        INTO   #ClsSectTerm
        FROM   FreedomAdvantage..arClassSections dClsSect
        LEFT JOIN FreedomAdvantage..arClassSectionTerms dClsSectTerm ON dClsSectTerm.ClsSectionId = dClsSect.ClsSectionId
                                                                        AND dClsSectTerm.TermId = dClsSect.TermId;

        ALTER TABLE FreedomAdvantage..arClassSectionTerms DISABLE TRIGGER ALL;

        -- Insert data into arClassSections table
        INSERT INTO FreedomAdvantage..arClassSectionTerms
                    SELECT tClsSectTerm.ClsSectTermId
                         , tClsSectTerm.ClsSectionId
                         , tClsSectTerm.TermId
                         , tClsSectTerm.ModDate
                         , tClsSectTerm.ModUser
                    FROM   #ClsSectTerm tClsSectTerm
                    LEFT JOIN FreedomAdvantage..arClassSectionTerms dClsSectTerm ON dClsSectTerm.ClsSectionId = tClsSectTerm.ClsSectionId
                                                                                    AND dClsSectTerm.TermId = tClsSectTerm.TermId
                    WHERE  dClsSectTerm.ClsSectTermId IS NULL;

        ALTER TABLE FreedomAdvantage..arClassSectionTerms ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.arClassSectionTerms')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.arClassSectionTerms
                    (
                        trg_ClsSectTermId UNIQUEIDENTIFIER
                      , trg_ClsSectionId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_ClsSectionId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.arClassSectionTerms
                    SELECT tClsSectTerm.ClsSectTermId
                         , tClsSectTerm.ClsSectionId
                         , @CampusId
                    FROM   #ClsSectTerm tClsSectTerm
                    LEFT JOIN FreedomBridge.bridge.arClassSectionTerms bClsSect ON bClsSect.trg_ClsSectionId = tClsSectTerm.ClsSectionId
                                                                                   AND bClsSect.trg_CampusId = @CampusId
                    WHERE  bClsSect.trg_ClsSectionId IS NULL;

        DROP TABLE #ClsSectTerm;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_arClassSectionTerms]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
