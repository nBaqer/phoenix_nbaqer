SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author, Spencer Garrett>
-- Create date: <Create Date, 01-06-15>
-- =============================================
CREATE PROCEDURE [bridge].[usp_syPeriodsWorkDays]
AS
    BEGIN
        --	 SET NOCOUNT ON added to prevent extra result sets from
        --	 interfering with SELECT statements.
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT CASE WHEN dPeriodWorkDay.PeriodId IS NULL THEN NEWID()
                    ELSE dPeriodWorkDay.PeriodIdWorkDayId
               END AS PeriodIdWorkDayId         -- uniqueidentifier
             , bPeriod.trg_PeriodId AS PeriodId -- uniqueidentifier
             , dWorkDay.WorkDaysId AS WorkDayId -- uniqueidentifier
        INTO   #PeriodWorkDay
        FROM   FreedomBridge.bridge.syPeriods bPeriod
        INNER JOIN FreedomAdvantage..plWorkDays dWorkDay ON dWorkDay.ViewOrder = CASE WHEN bPeriod.src_MonView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_TueView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_WedView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_ThrView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_FriView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_SatView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                      WHEN bPeriod.src_SunView = dWorkDay.ViewOrder THEN dWorkDay.ViewOrder
                                                                                 END
        LEFT JOIN FreedomAdvantage..syPeriodsWorkDays dPeriodWorkDay ON dPeriodWorkDay.PeriodId = bPeriod.trg_PeriodId
                                                                        AND dPeriodWorkDay.WorkDayId = dWorkDay.WorkDaysId;

        ALTER TABLE FreedomAdvantage..syPeriodsWorkDays ENABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..syPeriodsWorkDays
                    SELECT tPeriodWorkDay.PeriodIdWorkDayId
                         , tPeriodWorkDay.PeriodId
                         , tPeriodWorkDay.WorkDayId
                    FROM   #PeriodWorkDay tPeriodWorkDay
                    LEFT JOIN FreedomAdvantage..syPeriodsWorkDays dPeriodWorkDay ON dPeriodWorkDay.PeriodId = tPeriodWorkDay.PeriodId
                                                                                    AND dPeriodWorkDay.WorkDayId = tPeriodWorkDay.WorkDayId
                    WHERE  dPeriodWorkDay.PeriodIdWorkDayId IS NULL;

        ALTER TABLE FreedomAdvantage..syPeriodsWorkDays ENABLE TRIGGER ALL;

        --Create bridge table if not already created in the bridge
        IF NOT EXISTS (
                          SELECT *
                          FROM   sys.objects
                          WHERE  object_id = OBJECT_ID(N'FreedomBridge.bridge.syPeriodsWorkDays')
                                 AND type IN ( N'U' )
                      )
            BEGIN
                CREATE TABLE FreedomBridge.bridge.syPeriodsWorkDays
                    (
                        trg_PeriodIdWorkDayId UNIQUEIDENTIFIER
                      , trg_PeriodId UNIQUEIDENTIFIER
                      , trg_WorkDayId UNIQUEIDENTIFIER
                      , trg_CampusId UNIQUEIDENTIFIER
                      ,
                      PRIMARY KEY CLUSTERED
                          (
                              trg_PeriodIdWorkDayId
                            , trg_PeriodId
                            , trg_WorkDayId
                            , trg_CampusId
                          )
                    );
            END;

        INSERT INTO FreedomBridge.bridge.syPeriodsWorkDays
                    SELECT tPeriodWorkDay.PeriodIdWorkDayId
                         , tPeriodWorkDay.PeriodId
                         , tPeriodWorkDay.WorkDayId
                         , @CampusId AS CampusId
                    FROM   #PeriodWorkDay tPeriodWorkDay
                    LEFT JOIN FreedomBridge.bridge.syPeriodsWorkDays bPeriodWorkDay ON bPeriodWorkDay.trg_PeriodId = tPeriodWorkDay.PeriodId
                                                                                       AND bPeriodWorkDay.trg_WorkDayId = tPeriodWorkDay.WorkDayId
                    WHERE  bPeriodWorkDay.trg_PeriodIdWorkDayId IS NULL;

        DROP TABLE #PeriodWorkDay;
		
		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_syPeriodsWorkDays]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
