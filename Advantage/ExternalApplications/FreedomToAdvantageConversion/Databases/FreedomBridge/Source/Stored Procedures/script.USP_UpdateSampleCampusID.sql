SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [script].[USP_UpdateSampleCampusID]
AS
    BEGIN
        BEGIN TRAN;
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        DECLARE @OldCampusID UNIQUEIDENTIFIER = 'BF6CE61C-38C2-4612-AEF3-2CE715848776'
              , @NewCampusID UNIQUEIDENTIFIER = NEWID();

        ALTER TABLE FreedomAdvantage.dbo.syUsers NOCHECK CONSTRAINT ALL;
        ALTER TABLE FreedomAdvantage.dbo.syCmpGrpCmps NOCHECK CONSTRAINT ALL;

        UPDATE dUser
        SET    CampusId = @NewCampusID
        FROM   FreedomAdvantage.dbo.syUsers dUser
        WHERE  dUser.CampusId = @OldCampusID;

        UPDATE dCampGroup
        SET    CampusId = @NewCampusID
        FROM   FreedomAdvantage.dbo.syCmpGrpCmps dCampGroup
        WHERE  dCampGroup.CampusId = @OldCampusID;

        UPDATE dCampus
        SET    CampusId = @NewCampusID
        FROM   FreedomAdvantage.dbo.syCampuses dCampus
        WHERE  dCampus.CampusId = @OldCampusID;

        ALTER TABLE FreedomAdvantage.dbo.syUsers WITH CHECK CHECK CONSTRAINT ALL;
        ALTER TABLE FreedomAdvantage.dbo.syCmpGrpCmps WITH CHECK CHECK CONSTRAINT ALL;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[USP_UpdateSampleCampusID]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT;
    END;
GO
