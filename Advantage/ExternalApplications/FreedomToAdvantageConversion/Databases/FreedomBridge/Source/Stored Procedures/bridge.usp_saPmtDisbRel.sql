SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_saPmtDisbRel]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

        --This only awards disbursements records, other group of records comes from usp_faStuPaymentPlanSchedule
        --As payment plan installments

        DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

        SELECT NEWID() AS PmtDisbRelId                             -- uniqueidentifier
             , bTran.trg_TransactionId AS TransactionId            -- uniqueidentifier
             , sTran.Amount / 100.00 AS Amount                     -- decimal(18,4)
             , bAWSchedule.trg_AwardScheduleId AS AwardScheduleId  -- uniqueidentifier
             , CAST(NULL AS UNIQUEIDENTIFIER) AS PayPlanScheduleId -- uniqueidentifier
             , 'SA' AS ModUser                                     -- varchar(50)
             , GETDATE() AS ModDate                                -- datetime
             , bAward.trg_StudentAwardId AS StuAwardId             -- uniqueidentifier
             , sTran.RecordKey
        INTO   #saPmtDisbRel
        FROM   Freedom..STUTRANS_FIL sTran
        INNER JOIN bridge.saTransactions bTran ON bTran.src_RecordKey = sTran.RecordKey
                                                  AND bTran.trg_CampusId = @CampusId
        INNER JOIN bridge.faStudentAwardSchedule bAWSchedule ON bAWSchedule.src_TranRecKey = bTran.src_RecordKey
                                                                AND bAWSchedule.trg_CampusId = @CampusId
        INNER JOIN bridge.faStudentAwards bAward ON bAward.src_RecordKey = bAWSchedule.src_AwardRecordKey
                                                    AND bAward.trg_CampusId = @CampusId

        ALTER TABLE FreedomAdvantage..saPmtDisbRel DISABLE TRIGGER ALL;
        INSERT INTO FreedomAdvantage..saPmtDisbRel
                    SELECT PmtDisbRelId
                         , TransactionId
                         , Amount
                         , AwardScheduleId
                         , PayPlanScheduleId
                         , ModUser
                         , ModDate
                         , StuAwardId
                    FROM   #saPmtDisbRel;
        ALTER TABLE FreedomAdvantage..saPmtDisbRel ENABLE TRIGGER ALL;

        INSERT INTO bridge.saPmtDisbRel
                    SELECT PmtDisbRelId AS trg_PmtDisbRelId
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                         , PayPlanScheduleId AS trg_PayPlanScheduleId
                    FROM   #saPmtDisbRel;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_saPmtDisbRel]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;
GO
