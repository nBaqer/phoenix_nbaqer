SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [bridge].[usp_faStudentAwards]
AS
    BEGIN
        SET NOCOUNT ON;
        SET XACT_ABORT ON;

        BEGIN TRANSACTION;

		DECLARE @CampGrpId UNIQUEIDENTIFIER;
        DECLARE @CampusId UNIQUEIDENTIFIER
              , @SchoolID INT;

        SELECT Entity
             , Value
        INTO   #Settings
        FROM   dbo.udf_GetSettingsList();

        SELECT @CampusId = Value
        FROM   #Settings
        WHERE  Entity = 'CampusID';

        SELECT @SchoolID = Value
        FROM   #Settings
        WHERE  Entity = 'SchoolID';

		SET @CampGrpId = (SELECT cpg.CampGrpId FROM FreedomAdvantage..syCampGrps cpg WHERE cpg.CampGrpCode = (
		SELECT cmp.CampCode FROM FreedomAdvantage..syCampuses cmp WHERE cmp.CampusId = @CampusId ))

	

        SELECT NEWID() AS StudentAwardId               -- uniqueidentifier
             , NULL AS AwardId                         -- int
             , bEnr.trg_StuEnrollId AS StuEnrollId     -- uniqueidentifier
             , bFund.trg_FundSourceId AS AwardTypeId   -- uniqueidentifier --FundSourceId AwardTypeId
             , bFund.src_FullDescrip
             , dSFund.Descrip
             , CASE WHEN bFund.src_FullDescrip = 'Federal Stafford Loan' AND sFALog.UnSubLoanFlag = '' THEN 'S'
                                                                                ELSE sFALog.UnSubLoanFlag
                                                                           END AS UnSubLoanFlag
             , dYear.AcademicYearId AS AcademicYearId  -- uniqueidentifier
             , dYear.AcademicYearCode
             , dLender.LenderId AS LenderId            -- uniqueidentifier  - [IsLender] flag = 1 on [faLenders] Department of Education
             , dLender.LenderId AS ServicerId          -- uniqueidentifier - [IsServicer] flag = 1 on [faLenders]
             , dLender.LenderId AS GuarantorId         -- uniqueidentifier - [IsGuarantor] flag = 1 on [faLenders]
             , sFALog.TotalAwardAmt AS GrossAmount     -- decimal(19,4)
             , sFALog.LoanFees AS LoanFees             -- decimal(19,4)
             , 'SA' AS ModUser                         -- varchar(50)
             , GETDATE() AS ModDate                    -- datetime
             , sFALog.AwardStartDate AS AwardStartDate -- datetime
             , sFALog.AwardEndDate AS AwardEndDate     -- datetime
             , CASE WHEN Disbursements.Cnt > TranCount.Cnt THEN Disbursements.Cnt
                    ELSE TranCount.Cnt
               END AS Disbursements                    -- int
             , 'NA' AS LoanId                          -- varchar(50)
             , NULL AS EMASFundCode                    -- varchar(3)
             , NULL AS FA_Id                           -- varchar(50)
             , NULL AS AwardCode                       -- varchar(50)
             , NULL AS AwardSubCode                    -- varchar(50)
             , NULL AS AwardStatus                     -- varchar(50)
             , sFALog.RecordKey                        --int
			 ,sFALog.TotalAmountRcvd AS TotalAmountReceived
        INTO   #faStudentAwards
        FROM   bridge.arStuEnrollments bEnr
        INNER JOIN Freedom.dbo.FALOGFIL sFALog ON sFALog.StudrecKey = bEnr.src_RecordKey
        INNER JOIN bridge.saFundSources bFund ON bFund.src_RecordKey = sFALog.FinAidTypeId AND bFund.UnSubLoanFlag = CASE WHEN bFund.src_FullDescrip = 'Federal Stafford Loan'
                                                                                     AND sFALog.UnSubLoanFlag = 'U' THEN 1
                                                                                ELSE 0
                                                                           END
        INNER JOIN FreedomAdvantage..saFundSources dFund ON dFund.FundSourceId = bFund.trg_FundSourceId
        INNER JOIN FreedomAdvantage..syAdvFundSources dSFund ON dSFund.AdvFundSourceId = dFund.AdvFundSourceId
        LEFT JOIN FreedomAdvantage..saAcademicYears dYear ON CAST(dYear.AcademicYearCode AS INT) = ISNULL(
                                                                                                             NULLIF(sFALog.PrivAwardYear, 0) - 1
                                                                                                           , CASE WHEN CAST(sFALog.AwardStartDate AS DATE)
                                                                                                                       BETWEEN CAST('7-1-'
                                                                                                                                    + CAST(YEAR(sFALog.AwardStartDate) AS VARCHAR(4)) AS DATE) AND CAST('6-30-'
                                                                                                                                                                                                        + CAST(YEAR(sFALog.AwardStartDate) AS VARCHAR(4)) AS DATE) THEN
                                                                                                                      YEAR(sFALog.AwardStartDate) - 1
                                                                                                                  ELSE YEAR(sFALog.AwardStartDate)
                                                                                                             END
                                                                                                         )
        LEFT JOIN bridge.faLenders bLender ON bLender.src_RecordKey = sFALog.BankRecKey
                                               AND bLender.trg_CampusID = @CampusId
        LEFT JOIN FreedomAdvantage..faLenders dLender ON dLender.LenderId = bLender.trg_LenderId
        OUTER APPLY (
                        SELECT COUNT(1) AS Cnt
                        FROM   Freedom.dbo.STUTRANS_FIL sTran1
                        WHERE  sTran1.AwardLogKey = sFALog.RecordKey
                               AND DebitCredit = 1
                    ) TranCount
        OUTER APPLY (
                        SELECT CASE WHEN sFALog.Disb4Amt > 0 THEN 4
                                    WHEN sFALog.Disb3Amt > 0 THEN 3
                                    WHEN sFALog.Disb2Amt > 0 THEN 2
                                    WHEN sFALog.Disb1Amt > 0 THEN 1
                                    ELSE 0
                               END AS Cnt
                    ) Disbursements -- int
        WHERE  bEnr.trg_CampusId = @CampusId
		AND bfund.CampGrpId = @CampGrpId
		AND dYear.AcademicYearDescrip NOT IN ('2015/20151','2016/20161','2017/20171');

        ALTER TABLE FreedomAdvantage..faStudentAwards DISABLE TRIGGER ALL;

        INSERT INTO FreedomAdvantage..faStudentAwards
                    SELECT StudentAwardId
                         , AwardId
                         , StuEnrollId
                         , AwardTypeId
                         , AcademicYearId
                         , LenderId
                         , ServicerId
                         , GuarantorId
                         , GrossAmount
                         , LoanFees
                         , ModUser
                         , ModDate
                         , AwardStartDate
                         , AwardEndDate
                         , Disbursements
                         , LoanId
                         , EMASFundCode
                         , FA_Id
                         , AwardCode
                         , AwardSubCode
                         , AwardStatus
                    FROM   #faStudentAwards
					WHERE GrossAmount <> 0 OR TotalAmountReceived > 0;
        ALTER TABLE FreedomAdvantage..faStudentAwards ENABLE TRIGGER ALL;


	
        INSERT INTO bridge.faStudentAwards
                    SELECT StudentAwardId AS trg_StudentAwardId
                         , RecordKey AS src_RecordKey
                         , @CampusId AS trg_CampusId
                    FROM   #faStudentAwards;

					DROP TABLE #faStudentAwards;
					DROP TABLE #Settings;

		INSERT INTO [dbo].[storedProceduresRun] 
        SELECT NEWID(),'[usp_faStudentAwards]', GETDATE() , NULL ,ISNULL(MAX(orderRun),0)+1
        FROM [dbo].[storedProceduresRun]

        COMMIT TRANSACTION;

    END;

GO
