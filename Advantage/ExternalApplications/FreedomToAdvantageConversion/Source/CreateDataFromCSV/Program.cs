﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDataFromCSV
{
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Runtime.InteropServices;

    class Program
    {
        static int Main(string[] args)
        {
            
            if (args.Length < 3)
            {
                System.Console.WriteLine("Please enter the path and the file name.");
                System.Console.WriteLine("Usage: [Path] [FileNameNotExtension] [SchoolCode] [ConnectionString]");
                return 1;
            }
            var path = Convert.ToString(args[0].ToString());
            var file = Convert.ToString(args[1].ToString());
            var schoolCode = Convert.ToString(args[2].ToString());
            var connectionString = Convert.ToString(args[3].ToString());

            //var path = @"C:\SSIS\DataToConvert\FR_Aveda_5841";
            //var file = "StuPayPeriod";
            //var schoolCode = "5841";
            //var connectionString = "Data Source=.\\SQLDEV2016;Initial Catalog=Freedom;User ID=sa;Password=Fame.Fame4321"

            ReadCsvFile(path, file, schoolCode, connectionString);
            return 0;
        }

        /// <summary>
        /// The read csv file.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        public static void ReadCsvFile(string path, string fileName, string schoolCode, string connectionstring)
        {
            string fullPath = string.Empty;

            using (SqlConnection sqlCon = new SqlConnection(connectionstring))
            {
                sqlCon.Open();
                int lineCounter = 0;
                switch (fileName)
                {
                    case "StuPayPeriod":
                        fullPath = path + "\\" + fileName + "_" + schoolCode + ".csv";
                        using (StreamReader reader = new StreamReader(File.OpenRead(fullPath)))
                        {
                            while (!reader.EndOfStream)
                            {
                                string line = reader.ReadLine();
                                if (!string.IsNullOrEmpty(line))
                                {
                                    using (SqlCommand sqlCmd = new SqlCommand
                                    {
                                        CommandText = "INSERT INTO StuPayPeriod ( [Student ID],[PayPeriod No], [PP Beg Date],[PP End Date],[PP Beg Unit],[PP End Unit]) VALUES (@studentId, @ppNo, @ppBegDate, @ppEndDate, @ppBegUnit, @ppEndUnit)",
                                        Connection = sqlCon
                                    })
                                    {
                                        System.Console.WriteLine(line);
                                        if (lineCounter > 0)
                                        {
                                            string[] values = line.Split(',');

                                            if (values.Length == 6)
                                            {

                                            sqlCmd.Parameters.AddWithValue("@studentId", values[0]);
                                            sqlCmd.Parameters.AddWithValue("@ppNo", values[1]);
                                            sqlCmd.Parameters.AddWithValue("@ppBegDate", values[2]);
                                            sqlCmd.Parameters.AddWithValue("@ppEndDate", values[3]);
                                            sqlCmd.Parameters.AddWithValue("@ppBegUnit", values[4]);
                                            sqlCmd.Parameters.AddWithValue("@ppEndUnit", values[5]);
                                            sqlCmd.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }

                                lineCounter++;

                            }
                        }

                        break;
                    case "USERCODE.FIL":
                        fullPath = path + "\\" + fileName + ".csv";
                        using (StreamReader reader = new StreamReader(File.OpenRead(fullPath)))
                        {
                            while (!reader.EndOfStream)
                            {
                                string line = reader.ReadLine();

                                if (!string.IsNullOrEmpty(line))
                                {
                                    using (SqlCommand sqlCmd = new SqlCommand
                                    {
                                        CommandText = "INSERT INTO USERCODE_FIL (RecNum,Code,CodeKey,Description,Sequence,Active) VALUES (@RecNum, @Code, @CodeKey, @Description, @Sequence, @Active)",
                                        Connection = sqlCon
                                    })
                                    {


                                        System.Console.WriteLine(line);
                                        if (lineCounter > 1)
                                        {
                                            line = line.Replace("\"", "");
                                            string[] values = line.Split(',');

                                            if (values.Length == 6)
                                            {
                                                sqlCmd.Parameters.AddWithValue("@RecNum", Convert.ToDecimal(values[0]));
                                            sqlCmd.Parameters.AddWithValue("@Code", Convert.ToDecimal(values[1]));
                                            sqlCmd.Parameters.AddWithValue("@CodeKey", values[2]);
                                            sqlCmd.Parameters.AddWithValue("@Description", values[3].Replace("\"", string.Empty));
                                            sqlCmd.Parameters.AddWithValue("@Sequence", Convert.ToDecimal(values[4]));
                                            sqlCmd.Parameters.AddWithValue("@Active", values[5]);
                                            sqlCmd.ExecuteNonQuery();
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }

                                    }
                                }
                                lineCounter++;
                            }
                        }

                        break;

                }
                sqlCon.Close();
            }
            //StreamReader reader = new StreamReader(File.OpenRead(path + fileName + "csv"));
            //List<string> listA = new List<String>();
            //List<string> listB = new List<String>();
            //List<string> listC = new List<String>();
            //List<string> listD = new List<String>();
            ////string vara1, vara2, vara3, vara4;
            //while (!reader.EndOfStream)
            //{
            //    string line = reader.ReadLine();
            //    if (!String.IsNullOrWhiteSpace(line))
            //    {
            //        string[] values = line.Split(',');
            //        if (values.Length >= 4)
            //        {
            //            listA.Add(values[0]);
            //            listB.Add(values[1]);
            //            listC.Add(values[2]);
            //            listD.Add(values[3]);
            //        }
            //    }
            //}
            //string[] firstlistA = listA.ToArray();
            //string[] firstlistB = listB.ToArray();
            //string[] firstlistC = listC.ToArray();
            //string[] firstlistD = listD.ToArray();
        }

    }

}
