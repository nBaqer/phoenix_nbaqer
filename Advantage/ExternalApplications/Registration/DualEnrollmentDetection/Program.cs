﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DualEnrollment.LoggerSupport;
using DualEnrollment.SupportClasses;
using DualEnrollmentDetection.Business;

namespace DualEnrollmentDetection
{
    class Program
    {
        #region Fields

        public static string LogFileName { get; set; }

        #endregion

        static void Main(string[] args)
        {
            try
            {
                // Advise what database you are try to access before operate!!
                var conn = Properties.Settings.Default.AdvantageDb.Split(';');
  
                //Init DLL
                DualEnrollment.InitDualEnrollment.ConnectionString = Properties.Settings.Default.AdvantageDb;
                
                // Analysis parameters for logger file
                LogFileName = args.Length > 0 ? args[0] : "DualEnrollmentDetection.log";
                
                // Get the Version
                Version version = Assembly.GetEntryAssembly().GetName().Version;
                Console.WriteLine("Dual Enrollments Detections Version {0}", version);
                Console.WriteLine("This is configured to run over{2} {0} - {1}", conn[0], conn[1], Environment.NewLine);
                Console.WriteLine("{0} Processing begin --------------------------------", DateTime.Now);
                Logger.InitializeLogger(LogFileName);
                LoggerDb.InitializeLogger();

                // Get list for output need registration
                IList<StudentInfo> needRegistrationList0 = new    List<StudentInfo>();
                IList<StudentInfo> needRegistrationList1 = new    List<StudentInfo>();
                IList<StudentInfo> needRegistrationList2 = new    List<StudentInfo>();
                IList<StudentInfo> notNeedRegistrationList3 = new List<StudentInfo>();
                IList<StudentInfo> notNeedRegistrationList4 = new List<StudentInfo>();
                IList<StudentInfo> notNeedRegistrationList6 = new List<StudentInfo>();

                // screen process signal
                int stu = 0;

                // Get the list of all students with more than one enrollments possible to share classes
                var studentIdWithMoreThanOneEnrollmentList = DetectionBusiness.GetAllStudentWithTwoEnrollmentsCapableToShareClass();

                // Get the list of all School Requirements
                IList<string> requirementInSchoolList = DetectionBusiness.GetAllRequirementInSchool();


                // logging information
                Logger.WriteLine(string.Format("Student to be Processed {0}", studentIdWithMoreThanOneEnrollmentList.Count));
                Logger.WriteLine(string.Format("Requirements to be processed {0}", requirementInSchoolList.Count));
                Console.WriteLine("Total of Student to be processed {0}", studentIdWithMoreThanOneEnrollmentList.Count);
                Console.WriteLine("Total of Requirements to be processed {0}", requirementInSchoolList.Count);

                Console.WriteLine("Processing Student...");
                
                foreach (string student in studentIdWithMoreThanOneEnrollmentList)
                {
                    stu++;
                    Console.WriteLine("Student {0}", stu);
                    Console.SetCursorPosition(0, Console.CursorTop - 1); 
                   
                    foreach (string req in requirementInSchoolList)
                    {
                        var registration = new DetectionBusiness(student, req);
                        int registrationStatus = registration.CheckIfItIsRegisterOrTransferred();
                        var info = new StudentInfo
                        {
                            ClassId = string.Empty,
                            StudentId = student,
                            ReqId = req
                        };
                        switch (registrationStatus)
                        {
                            case 0:
                                {
                                    //string reg = registration.GetInformationString(student, classreq.ClassId);
                                    needRegistrationList0.Add(info);
                                    break;
                                }
                            case 1:
                                {
                                   
                                    needRegistrationList1.Add(info);
                                    break;
                                }
                            case 2:
                                {
                                 
                                    needRegistrationList2.Add(info);
                                    break;
                                }
                            case 5:
                                {
                                    // these are not scheduled in any enrollment
                                    break;
                                }
                            case 3:
                                {
                                    // Correctly register in all student enrollment
                                    notNeedRegistrationList3.Add(info);
                                    break;
                                }
                            case 4:
                                {
                                    // Correctly register in all student enrollment
                                    notNeedRegistrationList4.Add(info);
                                    break;
                                }
                            case 6:
                                {
                                    // Correctly register in all student enrollment
                                    notNeedRegistrationList6.Add(info);
                                    break;
                                }
                        }
                    }
                }




                Console.WriteLine();
                Console.WriteLine("Processing resulting statistic...");
                DetectionBusiness.ProcessingStatistic(needRegistrationList0, needRegistrationList1,
                    needRegistrationList2,
                    notNeedRegistrationList3, notNeedRegistrationList4, notNeedRegistrationList6);
                Console.WriteLine("{0} Process Ended -----------------------------------", DateTime.Now);
                Console.WriteLine("Press Enter (return) key to exit - Result in logger File: {0} ", LogFileName);
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }

            // Analysis of 

           // Console.ReadLine();
        }
    }
}
