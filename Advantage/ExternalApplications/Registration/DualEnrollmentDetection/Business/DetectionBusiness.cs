﻿using System.Collections.Generic;
using System.Linq;
using DualEnrollment.LoggerSupport;
using DualEnrollment.SupportClasses;

namespace DualEnrollmentDetection.Business
{
    public class DetectionBusiness
    {

        /// <summary>
        /// Constructor. Initialize class and get the necessary enrollments
        /// </summary>
        /// <param name="studentId">This enroll is used to get the studentdId</param>
        /// <param name="reqid">The class that wants to be enrolled the student</param>
        /// <remarks>Use always this constructor</remarks>

        public DetectionBusiness(string studentId, string reqid)
        {
            // ClassId = classreq.ClassId;
            ReqId = reqid;
            StudentId = studentId;

            //Initialize the list of co-requirements
            RegisterCoRequirementList = new List<RegisterCoRequirement>();

            //Initialize the list of warning and error messages
            InformationMessagesList = new List<string>();

            // Create the list of enrollment active that the student has, that contain the reqId 
            EnrollmentIdToModify =
                DualEnrollment.Facade.DbFacade.GetActiveEnrollmentsWithRegisteredRequerimentId(studentId,
                    reqid);

            //Initialize the Validate Enrollment class registration list
            ValidatedEnrollmentClassRegistrationList = new List<RegistrationClassInfo>();
        }


        #region "Properties"

        /// <summary>
        /// This is TestID in arResults and
        /// ClsSectionId in arGrdBkResults AND
        /// ClsSectionId in atClsSectAttendance
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ClassId { get; set; }

        /// <summary>
        /// The requirement associated with the class in question
        /// </summary>
        public string ReqId { get; set; }

        /// <summary>
        /// Student ID
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string StudentId { get; set; }

        /// <summary>
        /// Application user
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string UserName { get; set; }

        public string CampusId { get; set; }

        /// <summary>
        /// The list of enrollment where the operation is applied.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public IList<string> EnrollmentIdToModify { get; set; }

        public int Registered;

        public int Transferred;

        /// <summary>
        /// List of Enrollment validated to modify
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<RegistrationClassInfo> ValidatedEnrollmentClassRegistrationList { get; set; }

        /// <summary>
        /// List of messages with the warning or error 
        /// of the last Validate Operation
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<string> InformationMessagesList { get; set; }




        /// <summary>
        /// Store the co-requirement if any
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<RegisterCoRequirement> RegisterCoRequirementList { get; set; }


        #endregion

        #region "Methods Public"


        public void RegisterClassWithEnrollments()
        {
            // Try To register the class in all enrollments 
            DualEnrollment.Facade.DbFacade.RegisterClassInEnrollmentsOfStudents(ValidatedEnrollmentClassRegistrationList);

        }


        /// <summary>
        /// Validate if all enrollment in the list are registered o none was register
        /// This method decide if we need to repair the enrollment because some of then was enrolled
        /// and not the other
        /// </summary>
        /// <returns>
        /// 0: BAD Registered. Appear in not all enrollments
        /// 1: BAD: The class has being transferred not for all enrollments
        /// 2: BAD: The mix of transferred ans Registered class is not for all enrollments
        /// 3: OK: The mix of Registered and Transferred class is for all enrollments
        /// 4: OK Class requirement was transferred to all enrollments
        /// 5: OK No register in any enrollment
        /// 6: OK Correctly registered in all enrollment
        /// </returns>
        public int ValidateRegistrationAdd()
        {

            // Clear 
            Registered = 0;
            Transferred = 0;
            ValidatedEnrollmentClassRegistrationList = new List<RegistrationClassInfo>();

            // Check if the enrollment has register the operation
            foreach (string stuEnrollId in EnrollmentIdToModify)
            {
                bool isRegister = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollId, ClassId);
                if (isRegister)
                {
                    Registered++;
                    continue;
                }

                bool isTransfer = DualEnrollment.Facade.DbFacade.CheckIfCourseIsTransferred(stuEnrollId, ReqId);
                if (isTransfer)
                {
                    Transferred++;
                }
            }

            // Validation output
            int numberEnrollment = EnrollmentIdToModify.Count;

            if (Registered == 0)
            {
                if (Transferred == 0)
                {
                    return 5; //OK  Class is not used or is not schedule in none of the enrollment
                }

                if (Transferred < numberEnrollment)
                {
                    return 1; // BAD: The class has being transferred not for all enrollments
                }

                return 4; //OK Class requirement was transferred to all enrollments
            }

            if (Transferred == 0)
            {
                if (Registered < numberEnrollment)
                {
                    return 0; // BAD: The class was not register in all enrollments
                }

                return 6; // OK Class was register in all enrollments
            }

            if (Transferred + Registered < numberEnrollment)
            {
                return 2; // BAD: The mix of transferred ans Registered class is not for all enrollments
            }

            return 3; //OK: The mix of Registered and Transferred class is for all enrollments
        }

        public int CheckIfItIsRegisterOrTransferred()
        {
            // Clear 
            Registered = 0;
            Transferred = 0;
            ValidatedEnrollmentClassRegistrationList = new List<RegistrationClassInfo>();

            // Check if the enrollment has register the operation
            foreach (string stuEnrollId in EnrollmentIdToModify)
            {
                bool isRegister = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegisterReq(stuEnrollId, ReqId);
                if (isRegister)
                {
                    Registered++;
                    continue;
                }

                bool isTransfer = DualEnrollment.Facade.DbFacade.CheckIfCourseIsTransferred(stuEnrollId, ReqId);
                if (isTransfer)
                {
                    Transferred++;
                }
            }

            // Validation output
            int numberEnrollment = EnrollmentIdToModify.Count;

            if (Registered == 0)
            {
                if (Transferred == 0)
                {
                    return 5; //OK  Class is not used or is not schedule in none of the enrollment
                }

                if (Transferred < numberEnrollment)
                {
                    return 1; // BAD: The class has being transferred not for all enrollments
                }

                return 4; //OK Class requirement was transferred to all enrollments
            }

            if (Transferred == 0)
            {
                if (Registered < numberEnrollment)
                {
                    return 0; // BAD: The class was not register in all enrollments
                }

                return 6; // OK Class was register in all enrollments
            }

            if (Transferred + Registered < numberEnrollment)
            {
                return 2; // BAD: The mix of transferred ans Registered class is not for all enrollments
            }

            return 3; //OK: The mix of Registered and Transferred class is for all enrollments
        }

        /// <summary>
        /// Validate if one enrollment is register
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ValidateRegistrationAddSingleEnrollment(string stuEnrollmentId)
        {
            // Check if the enrollment has register the operation
            bool response = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollmentId, ClassId);
            return (response);
        }


        #endregion

        ///// <summary>
        ///// Given a student id and a class-id return a string with additional information about 
        ///// the student and the class.
        ///// </summary>
        ///// <param name="student">student id</param>
        ///// <param name="classid">class id</param>
        ///// <returns>a string with information about student and class</returns>
        //public string GetInformationString(string student, string classid)
        //{
        //    var result = DualEnrollment.Facade.DbFacade.GetInformationString(student, classid);
        //    return result;
        //}

        #region Static Auxiliary Method


        public static IList<string> GetAllStudentWithTwoEnrollmentsCapableToShareClass()
        {
            var list = DualEnrollment.Facade.DbFacade.GetAllStudentWithTwoEnrollmentsCapableToShareClass();
            return list;
        }

        public static IList<ClassSection> GetAllClassesInSchool()
        {
            var list = DualEnrollment.Facade.DbFacade.GetAllClassesWithRequirementInSchool();
            return list;
        }

        public static IList<string> GetAllRequirementInSchool()
        {
            var list = DualEnrollment.Facade.DbFacade.GetAllRequirementInSchool();
            return list;
        }

        public static void ProcessingStatistic(IList<StudentInfo> needRegistrationList0,
               IList<StudentInfo> needRegistrationList1, IList<StudentInfo> needRegistrationList2,
               IList<StudentInfo> notNeedRegistrationList3, IList<StudentInfo> notNeedRegistrationList4,
               IList<StudentInfo> notNeedRegistrationList6)
        {
            // Total student with problems
            var list = new List<StudentInfo>();
            list.AddRange(needRegistrationList0);
            list.AddRange(needRegistrationList1);
            list.AddRange(needRegistrationList2);
            var total = list.Distinct(new DistinctStudentIdComparer()).Count();
            Logger.WriteLine(string.Format("Total of Students with any Problem: {0}", total));


            var list1 = new List<StudentInfo>();
            list1.AddRange(notNeedRegistrationList3);
            list1.AddRange(notNeedRegistrationList4);
            list1.AddRange(notNeedRegistrationList6);

            var result = list1.Where(p => list.All(p2 => p2.StudentId != p.StudentId));

            var totalg = result.Distinct(new DistinctStudentIdComparer()).Count();
            Logger.WriteLine(string.Format("Total of Students without Problem: {0}", totalg));

            // Because we don't considerer students that are not register or transfer any class or requirement.
            Logger.WriteLine("The students that are not registered or transfer in any course are disregarded...");

            // List the students with problems 0
            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" Students with multiple enrollments that have only register the class to one enrollment. Type zero");
            Logger.WriteLine(" ..........................................................................................................");
            var infolist = new List<StudentInfo>();
            for (int i = 0; i < needRegistrationList0.Count; i++)
            {
                var info = DualEnrollment.Facade.DbFacade.GetClassInfoRegistered(needRegistrationList0[i]); //.GetStudentInfoReq(needRegistrationList0[i].StudentId, needRegistrationList0[i].ReqId);
                info.Classification = 0;
                infolist.Add(info);
                Logger.WriteLine(string.Format("{0} - {1} - {2} - class: {3} - Init Date: {4} - End Date: {5} "
                    , info.FullName,
                    info.RequirementCode, info.RequirementDescription, info.ClassSection, info.StartDate, info.EndDate));
            }

            // Write result in db
            LoggerDb.WriteListOfLogInfo(infolist, 0);

            // List the students with problems 1
            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" Students with multiple enrollments that have only transferred the requirement to one enrollment. Type 1");
            Logger.WriteLine(" ..........................................................................................................");

            var cleanlist = needRegistrationList1.Distinct(new DistinctStudentIdReqIdComparer()).ToList();
            for (int i = 0; i < cleanlist.Count; i++)
            {
                cleanlist[i] = DualEnrollment.Facade.DbFacade.GetStudentInfoReq(cleanlist[i].StudentId, cleanlist[i].ReqId);

                Logger.WriteLine(string.Format("{0} - {1} - {2}"
                    , cleanlist[i].FullName,
                    cleanlist[i].RequirementCode, cleanlist[i].RequirementDescription));
            }

            // Write result in db
            LoggerDb.WriteListOfLogInfo(cleanlist, 1);


            // List the students with problems 2
            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" The mix of transferred and Registered class are not for all enrollments. Type 2");
            Logger.WriteLine(" ..........................................................................................................");

            cleanlist = needRegistrationList2.Distinct(new DistinctStudentIdReqIdComparer()).ToList();
            for (int i = 0; i < cleanlist.Count; i++)
            {
                cleanlist[i] = DualEnrollment.Facade.DbFacade.GetStudentInfoReq(cleanlist[i].StudentId, cleanlist[i].ReqId);

                Logger.WriteLine(string.Format("{0} - {1} - {2}"
                    , cleanlist[i].FullName,
                    cleanlist[i].RequirementCode, cleanlist[i].RequirementDescription));
            }

            // Write result in db
            LoggerDb.WriteListOfLogInfo(cleanlist, 2);


            // List the students with problems 2
            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" Students without problems");
            Logger.WriteLine(" ..........................................................................................................");

            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" OK: The mix of Registered and Transferred requirement is for all enrollments (Type 3)");
            Logger.WriteLine(" ..........................................................................................................");

            cleanlist = notNeedRegistrationList3.Distinct(new DistinctStudentIdReqIdComparer()).ToList();
            for (int i = 0; i < cleanlist.Count; i++)
            {
                cleanlist[i] = DualEnrollment.Facade.DbFacade.GetStudentInfoReq(cleanlist[i].StudentId, cleanlist[i].ReqId);

                Logger.WriteLine(string.Format("{0} - {1} - {2}"
                    , cleanlist[i].FullName,
                    cleanlist[i].RequirementCode, cleanlist[i].RequirementDescription));
            }

            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" OK Requirement was transferred to all enrollments (Type 4)");
            Logger.WriteLine(" ..........................................................................................................");

            cleanlist = notNeedRegistrationList4.Distinct(new DistinctStudentIdReqIdComparer()).ToList();
            for (int i = 0; i < cleanlist.Count; i++)
            {
                cleanlist[i] = DualEnrollment.Facade.DbFacade.GetStudentInfoReq(cleanlist[i].StudentId, cleanlist[i].ReqId);

                Logger.WriteLine(string.Format("{0} - {1} - {2}"
                    , cleanlist[i].FullName,
                    cleanlist[i].RequirementCode, cleanlist[i].RequirementDescription));
            }

            Logger.WriteLine(" ..........................................................................................................");
            Logger.WriteLine(" OK Requirement Correctly registered in all enrollment. (type 6)");
            Logger.WriteLine(" ..........................................................................................................");

            cleanlist = notNeedRegistrationList6.Distinct(new DistinctStudentIdReqIdComparer()).ToList();
            for (int i = 0; i < cleanlist.Count; i++)
            {
                cleanlist[i] = DualEnrollment.Facade.DbFacade.GetStudentInfoReq(cleanlist[i].StudentId, cleanlist[i].ReqId);

                Logger.WriteLine(string.Format("{0} - {1} - {2}"
                    , cleanlist[i].FullName,
                    cleanlist[i].RequirementCode, cleanlist[i].RequirementDescription));
            }

        }
    }
        #endregion


}



