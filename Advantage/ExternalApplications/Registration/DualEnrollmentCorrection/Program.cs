﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DualEnrollment.LoggerSupport;
using DualEnrollment.SupportClasses;
using DualEnrollmentCorrection.Business;

namespace DualEnrollmentCorrection
{
    class Program
    {
        #region Fields

        public static string LogFileName { get; set; }

        #endregion

        static void Main(string[] args)
        {
            try
            {
                // Advise what database you are try to access before operate!!
                var conn = Properties.Settings.Default.AdvantageDb.Split(';');
                Console.WriteLine("This is configured to run over{2} {0} - {1}", conn[0], conn[1], Environment.NewLine);
                string answer;
                do
                {
                   Console.WriteLine( "Do you want to continue (yes(y)/no(n))?");
                    answer = Console.ReadLine();
                    if (answer != null && answer.ToLower() == "y")
                    {
                        break;
                    }
                    if (answer != null && answer.ToLower() == "n")
                    {
                        return;
                    }

                } while (answer != null && answer.ToLower() == "y");

                // Init DLL file
                DualEnrollment.InitDualEnrollment.ConnectionString = Properties.Settings.Default.AdvantageDb;
                // Analysis parameters for logger file
                LogFileName = args.Length > 0 ? args[0] : "DualEnrollmentCorrection.log";
               
                // Init presentation in screen
                Version version = Assembly.GetEntryAssembly().GetName().Version;
                Console.WriteLine("Dual Enrollments Correction Version {0}", version);
                Console.WriteLine("{0} Processing begin -------------------------------------------", DateTime.Now);
                
                Logger.InitializeLogger(LogFileName);
                LoggerDb.InitializeLogger();

                // Get list for output need registration
                // Get list for output need registration
                IList<StudentInfo> needRegistrationList0 = new List<StudentInfo>();
                IList<StudentInfo> needTransferList1 = new List<StudentInfo>();


                // screen process signal
                int stu = 0;

                // Get the list of all students with more than one enrollments possible to share classes
                var studentIdWithMoreThanOneEnrollmentList = MiniDetectionBusiness.GetAllStudentWithTwoEnrollmentsCapableToShareClass();

                // Get the list of all School Requirements
                IList<string> requirementInSchoolList = MiniDetectionBusiness.GetAllRequirementInSchool();


                // logger information
                Logger.WriteLine(string.Format("Student to be Processed {0}", studentIdWithMoreThanOneEnrollmentList.Count));
                Logger.WriteLine(string.Format("Requirements to be processed {0}", requirementInSchoolList.Count));
                Console.WriteLine("Total of Student to be processed {0}", studentIdWithMoreThanOneEnrollmentList.Count);
                Console.WriteLine("Total of Requirements to be processed {0}", requirementInSchoolList.Count);

                Console.WriteLine("Processing Student...");

                foreach (string student in studentIdWithMoreThanOneEnrollmentList)
                {
                    stu++;
                    Console.WriteLine("Student {0}", stu);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);

                    foreach (string req in requirementInSchoolList)
                    {
                        var registration = new MiniDetectionBusiness(student, req);
                        int registrationStatus = registration.CheckIfItIsRegisterOrTransferred();
                        var info = new StudentInfo
                        {
                            ClassId = string.Empty,
                            StudentId = student,
                            ReqId = req,
                            ModUser = "SUPPORT"
                        };
                        switch (registrationStatus)
                        {
                            case 0:
                                {
                                    needRegistrationList0.Add(info);
                                    break;
                                }
                            case 1:
                                {
                                    needTransferList1.Add(info);
                                    break;
                                }
                        }
                    }
                }

                int enrollmentsTransferred = 0;
                int enrollmentsRegistered = 0;

                Console.WriteLine("Begin correction Partial Transferred Student...");
                if (needTransferList1.Count > 0)
                {
                    // Transferred requirement to all enrollments selected for the student
                    enrollmentsTransferred = TransferBusiness.InsertTransferInAllEnrollmentForTheRequirements(needTransferList1);
                }

                Console.WriteLine("Begin correction Partial Registered Student...");
                if (needRegistrationList0.Count > 0)
                {
                    //Register requirement to all enrollments selected for the student
                    enrollmentsRegistered = RegisterBusiness.InsertRegisterInAllEnrollmentForTheRequirements(needRegistrationList0);
                }

                Console.WriteLine();

                Console.WriteLine("Reporting result to logger...");
                Console.WriteLine("Resolved the Partial Transfer problem in: {0} Courses", enrollmentsTransferred);
                Console.WriteLine("Resolved the Partial Registration problem in: {0} Courses", enrollmentsRegistered);
                Logger.WriteLine(string.Format("Resolved the Partial Requirement Transfer problem in: {0} Courses", enrollmentsTransferred));
                Logger.WriteLine(string.Format("Resolved the Partial Requirement Registration problem in: {0} Courses", enrollmentsRegistered));
                Console.WriteLine("{0} Process Ended -----------------------------------", DateTime.Now);
                Console.WriteLine("Result in logger File: {0} ", LogFileName);
                Console.WriteLine("Press Enter (return) key to exit ");
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }

          //  Console.ReadLine();
        }
    }
}
