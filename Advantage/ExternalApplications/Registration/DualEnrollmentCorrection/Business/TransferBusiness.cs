﻿using System;
using System.Collections.Generic;
using DualEnrollment.Facade;
using DualEnrollment.SupportClasses;

namespace DualEnrollmentCorrection.Business
{
    public class TransferBusiness
    {
        /// <summary>
        /// Insert the transfer enrollment info in all related enrollments.
        /// </summary>
        /// <param name="needTransferList1"></param>
        /// <returns></returns>
        public static int InsertTransferInAllEnrollmentForTheRequirements(IList<StudentInfo> needTransferList1)
        {
            // Create list of insertion. this will be holding all the information to fill the ArTransferTable
            IList<TransferRecordInfo> insertionTransferList = new List<TransferRecordInfo>();

            // For all StudentsInfo
            foreach (StudentInfo info in needTransferList1)
            {
                // Temp list of all enrollment of the student
                IList<TransferRecordInfo> tempList = new List<TransferRecordInfo>();
                var transferred = new TransferRecordInfo {EnrollmentId = Guid.Empty.ToString()};
                
                //Get per student the list of enrollments with the ReqId
                var enrollmentOfThisStudentList = DbFacade.GetActiveEnrollmentsWithRegisteredRequerimentId(info.StudentId, info.ReqId);
                
                // For each enrollment that the student share the reqid
                foreach (string s in enrollmentOfThisStudentList)
                {
                    var record = DbFacade.GetRequirementTransferredRecord(s, info.ReqId);
                    if (record.EnrollmentId == Guid.Empty.ToString())
                    {
                        // No found record , create a new record to insert
                        var trecord = new TransferRecordInfo {EnrollmentId = s};
                        tempList.Add(trecord);
                    }
                    else
                    {
                        // Found the transferred record
                        transferred = record;
                    }
                }

                // Analysis Conditions
                if (transferred.EnrollmentId == Guid.Empty.ToString())
                {
                    throw new ArgumentException(
                        string.Format(
                            "A reported transfer partially element is not transferred Student {0} Requirement {1}",
                            info.StudentId, info.ReqId));
                }

                // Copy the information from transferred to temporal list, and put the record in insertion list.
                foreach (TransferRecordInfo recordInfo in tempList)
                {
                    //recordInfo.EnrollmentId = transferred.EnrollmentId;
                    recordInfo.GradeOverridenBy = "SUPPORT";
                    recordInfo.GradeOverridenDate = DateTime.Now;
                    recordInfo.GrdSysDetailId = transferred.GrdSysDetailId;
                    recordInfo.IsClinicsSatisfied = transferred.IsClinicsSatisfied;
                    recordInfo.IsCourseCompleted = transferred.IsCourseCompleted;
                    recordInfo.IsGradeOverriden = transferred.IsGradeOverriden;
                    recordInfo.IsTransferred = transferred.IsTransferred;
                    recordInfo.ModDate = DateTime.Now;
                    recordInfo.ModUser = "SUPPORT";
                    recordInfo.ReqId = transferred.ReqId;
                    recordInfo.Score = transferred.Score;
                    recordInfo.TermId = transferred.TermId;
                    insertionTransferList.Add(recordInfo);
                }
            } // End loop
            
            // Insert list of insertion values in database in one transaction.
            ModificationFacade.InsertAllTransferRecordTransactional(insertionTransferList);

            return insertionTransferList.Count;
        }
    }
}
