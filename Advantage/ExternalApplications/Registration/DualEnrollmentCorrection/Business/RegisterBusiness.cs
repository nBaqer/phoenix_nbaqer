﻿using System.Collections.Generic;
using DualEnrollment.Facade;
using DualEnrollment.LoggerSupport;
using DualEnrollment.SupportClasses;

namespace DualEnrollmentCorrection.Business
{
    public class RegisterBusiness
    {
        public static int InsertRegisterInAllEnrollmentForTheRequirements(IList<StudentInfo> needRegistrationList0)
        {
            // Create list of insertion. this will be holding all the information to fill the ArRegister and grade book
            IList<ICorrectionRegInfo> insertionRegisterList = new List<ICorrectionRegInfo>();

            // For all StudentsInfo

            foreach (StudentInfo info in needRegistrationList0)
            {
                // problem flag
                bool problem = false;
                // Create a temporal list of insertions
                IList<ICorrectionRegInfo> temoralInsertionList = new List<ICorrectionRegInfo>();

                // Define the holder of registered information for the student
                ICorrectionRegInfo registeredEnrollment = null;

                //Get per student the enrollments with the ReqId
                var enrollmentOfThisStudentList = DbFacade.GetActiveEnrollmentsWithRegisteredRequerimentId(info.StudentId, info.ReqId);
                // For each enrollments
                foreach (string s in enrollmentOfThisStudentList)
                {
                    // Test if the enrollment is registered in (ArResults)
                    var listResult = DbFacade.GetRegisterInformation(s, info.ReqId);
                    if (listResult.Count > 0) //Exists
                    {
                        // Found the registered record. Test if it is the first found
                        if (registeredEnrollment == null)
                        {
                            // First Time create the registered record
                            registeredEnrollment = CorrectionRegInfo.Factory();
                            registeredEnrollment.ArInfoList = listResult;


                            // Fill the Information for the enrollment
                            registeredEnrollment = DbFacade.GetAllAcademicInformation(registeredEnrollment);
                        }
                        else
                        {
                            var stuinfo = DbFacade.GetStudentInfoReq(info.StudentId, info.ReqId);
                            Logger.StudentHasAcademicInfoInMorethanOneEnrollment(stuinfo);
                            problem = true;
                            break;
                        }
                    }
                    else
                    {
                        // Check if the enrollment has any academic information
                        string tablesWithAcademicInfo = DbFacade.HasTheEnrollmentAnyAcademicRecord(s, info.ReqId);
                        if (!string.IsNullOrWhiteSpace(tablesWithAcademicInfo))
                        {
                            var stuinfo = DbFacade.GetStudentInfoReq(info.StudentId, info.ReqId);
                            Logger.StudentIsNotRegisterButHaveAcademicInfo(stuinfo);
                            // Remove the academic info (this information without student registration in ArResults should be 
                            // spurious
                            DbFacade.DeleteSpuriousAcademicInfo(s, info.ReqId, tablesWithAcademicInfo);
                            problem = false;
                            //break;
                        }
                        // No found record , create a new record to insert in the insertion list
                        var correction = CorrectionRegInfo.Factory();

                        correction.Enrollment = s;
                        //correction.ArInfoList.EnrollmentId = s;

                        temoralInsertionList.Add(correction);

                    }
                }

                // All the enrollments of the student was scanned, now 
                if (problem)
                {
                    continue;
                }

                // Check if was captured at least one enrollment (this should be always happen.)
                if (registeredEnrollment == null)
                {
                    var stuinfo = DbFacade.GetStudentInfoReq(info.StudentId, info.ReqId);
                    Logger.StudentHasNoneRegistered(stuinfo);
                    continue;
                }


                // Fill the temporal list with the captured academic information and insert in the main list
                foreach (ICorrectionRegInfo correct in temoralInsertionList)
                {
                    
                   var recordToinsert = CorrectionRegInfo.Factory();
                     
                    foreach (ArResultsClass ar in registeredEnrollment.ArInfoList)
                    {
                        ar.EnrollmentId = correct.Enrollment;
                        recordToinsert.ArInfoList.Add(ar.Clone());
                    }
                    
                    //recordToinsert.ArInfoList = registeredEnrollment.ArInfoList.Clone();
                    //recordToinsert.ArInfoList.EnrollmentId = correct.ArInfoList.EnrollmentId;

                    foreach (AtClsSectAttendance att in registeredEnrollment.AttendanceList)
                    {
                        att.StuEnrollId = correct.Enrollment;
                        recordToinsert.AttendanceList.Add(att.Clone());
                    }

                    foreach (GradeBookResultInfo re in registeredEnrollment.GradeBookList)
                    {
                        re.StuEnrollId = correct.Enrollment;
                        recordToinsert.GradeBookList.Add(re.Clone());
                    }

                    // Test if exists a duplicate record in table ArResults
                    //if (
                    //    insertionRegisterList.Any(
                    //        x => x.ArInfoList.EnrollmentId == recordToinsert.ArInfoList.EnrollmentId & x.ArInfoList.TestId == recordToinsert.ArInfoList.TestId))
                    //{
                    //    Console.WriteLine("enrollment: {0} - testId:{1}", recordToinsert.ArInfoList.EnrollmentId, recordToinsert.ArInfoList.TestId);
                    //    throw new ConstraintException(
                    //        string.Format("A duplicate record was found when the information were collecting. Program abort The record is: enrollment: {0} TestId: {1}"
                    //        , recordToinsert.ArInfoList.EnrollmentId, recordToinsert.ArInfoList.TestId));
                    //}

                    insertionRegisterList.Add(recordToinsert);
                }
            }

            // Transfer all information to the database
            DbFacade.InsertAllcademicInformationInDataBase(insertionRegisterList);
            return insertionRegisterList.Count;
        }

    }
}
