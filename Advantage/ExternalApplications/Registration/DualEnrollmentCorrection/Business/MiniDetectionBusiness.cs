﻿using System.Collections.Generic;
using DualEnrollment.SupportClasses;

namespace DualEnrollmentCorrection.Business
{
    public class MiniDetectionBusiness
    {
        #region Constructor
        /// <summary>
        /// Constructor. Initialize class and get the necessary enrollments
        /// </summary>
        /// <param name="studentId">This enroll is used to get the studentdId</param>
        /// <param name="reqid">The class that wants to be enrolled the student</param>
        /// <remarks>Use always this constructor</remarks>

        public MiniDetectionBusiness(string studentId, string reqid)
        {
            ReqId = reqid;
            StudentId = studentId;

            //Initialize the list of co-requirements
            RegisterCoRequirementList = new List<RegisterCoRequirement>();

            //Initialize the list of warning and error messages
            InformationMessagesList = new List<string>();

            // Create the list of enrollment active that the student has, that contain the reqId 
            EnrollmentIdToModify =
                DualEnrollment.Facade.DbFacade.GetActiveEnrollmentsWithRegisteredRequerimentId(studentId,
                    reqid);

            //Initialize the Validate Enrollment class registration list
            ValidatedEnrollmentClassRegistrationList = new List<RegistrationClassInfo>();

        }
        #endregion

        #region Properties
        /// <summary>
        /// This is TestID in arResults and
        /// ClsSectionId in arGrdBkResults AND
        /// ClsSectionId in atClsSectAttendance
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string ClassId { get; set; }

        /// <summary>
        /// The requirement associated with the class in question
        /// </summary>
        public string ReqId { get; set; }


        /// <summary>
        /// Student ID
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string StudentId { get; set; }

        /// <summary>
        /// Application user
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string UserName { get; set; }

        public string CampusId { get; set; }

        /// <summary>
        /// The list of enrollment where the operation is applied.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public IList<string> EnrollmentIdToModify { get; set; }

        /// <summary>
        /// List of Enrollment validated to modify
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<RegistrationClassInfo> ValidatedEnrollmentClassRegistrationList { get; set; }

        /// <summary>
        /// List of messages with the warning or error 
        /// of the last Validate Operation
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<string> InformationMessagesList { get; set; }

        /// <summary>
        /// Store the co-requirement if any
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public List<RegisterCoRequirement> RegisterCoRequirementList { get; set; }

        public int Registered;

        public int Transferred;

        #endregion

        #region "Methods Public"

         public void RegisterClassWithEnrollments()
        {
            // Try To register the class in all enrollments 
            DualEnrollment.Facade.DbFacade.RegisterClassInEnrollmentsOfStudents(ValidatedEnrollmentClassRegistrationList);
        }

        /// <summary>
        /// Validate if all enrollment in the list are registered o none was register
        /// This method decide if we need to repair the enrollment because some of then was enrolled
        /// and not the other
        /// </summary>
        /// <returns>
        /// 0: Bad Registered. Appear in only one enrollment
        /// 1: OK No register in any enrollment
        /// 2: OK Correctly registered in all enrollment
        /// </returns>
        /// <remarks>TODO: Not used here in the final version delete it </remarks>
        public int ValidateRegistrationAdd()
        {

            // Clear list of error
            InformationMessagesList.Clear();
            ValidatedEnrollmentClassRegistrationList = new List<RegistrationClassInfo>();

            // Check if the enrollment has register the operation
            foreach (string stuEnrollId in EnrollmentIdToModify)
            {
                bool isRegister = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollId, ClassId);
                if (isRegister == false)
                {
                    var registration = new RegistrationClassInfo(string.Empty, ClassId, stuEnrollId)
                    {
                        CampusId = CampusId,
                        UserName = UserName
                    };
                    ValidatedEnrollmentClassRegistrationList.Add(registration);
                    //Verified that is not registered in the enrollment the class.
                }
            }

            if (ValidatedEnrollmentClassRegistrationList.Count == EnrollmentIdToModify.Count)
            {
                return 1; // No registered in any enrollment you not need to considered it
            }
            if (ValidatedEnrollmentClassRegistrationList.Count == 0)
            {
                return 2; // Correctly registered 
            }

            //Verify Co requirement list.....
            dynamic temp = new List<RegistrationClassInfo>();

            foreach (RegisterCoRequirement co in RegisterCoRequirementList)
            {
                foreach (RegistrationClassInfo info in ValidatedEnrollmentClassRegistrationList)
                {
                    if ((co.StuEnrollmentId == info.StuEnrollmentId))
                    {
                        // Validate the relation
                        bool isRegister = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegister(co.StuEnrollmentId, co.ClassId);
                        if (isRegister == false)
                        {
                            var registration = new RegistrationClassInfo(string.Empty, co.ClassId, co.StuEnrollmentId)
                            {
                                CampusId = CampusId,
                                UserName = UserName
                            };
                            temp.Add(registration);
                            //Verified that is not registered in the enrollment the class.
                        }
                    }
                }
            }

            // Pass the possible co requirement to be added to the list 
            foreach (RegistrationClassInfo info in temp)
            {
                ValidatedEnrollmentClassRegistrationList.Add(info);
            }

            return 0;
        }

        /// <summary>
        /// Validate if one enrollment is register
        /// </summary>
        /// <returns></returns>
        /// <remarks>TODO: Not used here in the final version delete it </remarks>
        public bool ValidateRegistrationAddSingleEnrollment(string stuEnrollmentId)
        {
            // Check if the enrollment has register the operation
            bool response = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegister(stuEnrollmentId, ClassId);
            return (response);
        }

        /// <summary>
        /// for the student enrollment capable of register classes get if they are registered or transferred.
        /// </summary>
        /// <returns></returns>
        public int CheckIfItIsRegisterOrTransferred()
        {
            // Clear 
            Registered = 0;
            Transferred = 0;
  
            // Check if the enrollment has register the operation
            foreach (string stuEnrollId in EnrollmentIdToModify)
            {
                bool isRegister = DualEnrollment.Facade.DbFacade.CheckIfEnrollmentIsAlreadyRegisterReq(stuEnrollId,
                    ReqId);
                if (isRegister)
                {
                    Registered++;
                    continue;
                }

                bool isTransfer = DualEnrollment.Facade.DbFacade.CheckIfCourseIsTransferred(stuEnrollId, ReqId);
                if (isTransfer)
                {
                    Transferred++;
                }
            }

            // Validation output
            int numberEnrollment = EnrollmentIdToModify.Count;

            if (Registered == 0)
            {
                if (Transferred == 0)
                {
                    return 5; //OK  Class is not used or is not schedule in none of the enrollment
                }

                if (Transferred < numberEnrollment)
                {
                    return 1; // BAD: The class has being transferred not for all enrollments
                }

                return 4; //OK Class requirement was transferred to all enrollments
            }

            if (Transferred == 0)
            {
                if (Registered < numberEnrollment)
                {
                    return 0; // BAD: The class was not register in all enrollments
                }

                return 6; // OK Class was register in all enrollments
            }

            if (Transferred + Registered < numberEnrollment)
            {
                return 2; // BAD: The mix of transferred ans Registered class is not for all enrollments
            }

            return 3; //OK: The mix of Registered and Transferred class is for all enrollments
        }
   
        /// <summary>
        /// Given a student id and a class-id return a string with additional information about 
        /// the student and the class.
        /// </summary>
        /// <param name="student">student id</param>
        /// <param name="classid">class id</param>
        /// <returns>a string with information about student and class</returns>
        /// <remarks>TODO: Not used here in the final version delete it </remarks>
        public string GetInformationString(string student, string classid)
        {
            var result = DualEnrollment.Facade.DbFacade.GetInformationString(student, classid);
            return result;
        }

     #endregion

        #region Static Class

        public static IList<string> GetAllStudentWithTwoEnrollmentsCapableToShareClass()
        {
            var list = DualEnrollment.Facade.DbFacade.GetAllStudentWithTwoEnrollmentsCapableToShareClass();
            return list;
        }

        public static IList<string> GetAllRequirementInSchool()
        {
            var list = DualEnrollment.Facade.DbFacade.GetAllRequirementInSchool();
            return list;
        }

        #endregion
    }
}
