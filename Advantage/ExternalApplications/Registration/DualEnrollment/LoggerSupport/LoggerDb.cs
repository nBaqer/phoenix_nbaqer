﻿using System.Collections.Generic;
using DualEnrollment.Facade;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.LoggerSupport
{
    public class LoggerDb
    {
        public static void InitializeLogger()
        {
            LoggerFacade.TruncateLoggerTable();
        }

        public static void WriteInfo(StudentInfo info)
        {
            LoggerFacade.WriteInfo(info);
        }

        public static void WriteListOfLogInfo(IList<StudentInfo> needRegistrationList, int classification)
        {
            LoggerFacade.WriteListOfLogInfo(needRegistrationList, classification);
        }
    }
}
