﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.LoggerSupport
{
    public static class Logger
    {
        public static string LogFileName { get; set; }

        /// <summary>
        /// Use this first to initialize the logger class
        /// </summary>
        /// <param name="loggerFile"></param>
        public static void InitializeLogger(string loggerFile)
        {
            LogFileName = loggerFile;
            if (File.Exists(loggerFile))
            {
                File.Delete(loggerFile);
            }
            File.WriteAllText(loggerFile, string.Format("Login begin at {0}{1}", DateTime.Now, Environment.NewLine));
        }

        /// <summary>
        /// Write a line in logger
        /// </summary>
        /// <param name="content"></param>
        public static void WriteLine(string content)
        {
            if (File.Exists(LogFileName))
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(content);
                }

                return;
            }

            Trace.WriteLine("Log file not Found");
        }

     

        public static void LogException(Exception ex)
        {
            Trace.WriteLine(ex.Message);
            Trace.WriteLine(ex.StackTrace);
            WriteLine(String.Format("{0}------------------------------------------------------------------------------------------{1}", DateTime.Now, Environment.NewLine));
            WriteLine(ex.Message);
            WriteLine(ex.StackTrace);
            WriteLine(String.Format("{0}------------------------------------------------------------------------------------------{1}", DateTime.Now, Environment.NewLine));
            Console.WriteLine("The following Exception happen: {0}{1}{0}{2}", Environment.NewLine, ex.Message, ex.StackTrace);
        }

        public static void LogList(IList<string> needRegistrationList)
        {
            foreach (string s in needRegistrationList)
            {
                WriteLine(s);
            }
        }

 

        #region Registration correction process error logger

        public static void StudentHasAcademicInfoInMorethanOneEnrollment(StudentInfo info)
        {
            WriteLine(string.Format("Student {0} appear with academic information in more that one enrollments, the user should manually decide what enrollment has the correct information. Requirement: {1} - {2}", info.FullName, info.RequirementCode, info.RequirementDescription));
            info.Classification = 7;
            LoggerDb.WriteInfo(info);
        }

 
        public static void StudentIsNotRegisterButHaveAcademicInfo(StudentInfo info)
        {
            WriteLine(string.Format("Student {0} is not register but has spurious info for requirement {1} - {2} {3} " +
                 "The information should be replaced with the register enrollment academic info", 
                info.FullName, info.RequirementCode, info.RequirementDescription, Environment.NewLine));
            info.Classification = 8;
            LoggerDb.WriteInfo(info);
        }

        public static void StudentHasNoneRegistered(StudentInfo info)
        {
            WriteLine(string.Format("Student {0} has not register for any of his enrollment. does not need to be analysis for this reqId:  {1} - {2}", info.FullName, info.RequirementCode, info.RequirementDescription));
        } 

        #endregion

    }
}
