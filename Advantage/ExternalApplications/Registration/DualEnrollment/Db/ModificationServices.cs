﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Db
{
    public class ModificationServices
    {
        /// <summary>
        /// This insert all missing transfer in the database.
        /// </summary>
        /// <param name="insertionTransferList"></param>
        public void InsertAllTransferRecordTransactional(IList<TransferRecordInfo> insertionTransferList)
        {
            //Prepare connexion
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            // Prepare SQL
            var sb = new StringBuilder();
            sb.Append(" INSERT dbo.arTransferGrades ");
            sb.Append("         ( TransferId,StuEnrollId,ReqId,GrdSysDetailId,TermId,ModDate,ModUser,Score,IsTransferred ");
            sb.Append("         ,isClinicsSatisfied,IsCourseCompleted,IsGradeOverridden,GradeOverriddenBy,GradeOverriddenDate ) ");
            sb.Append(" VALUES  ( NEWID(),@StuEnrollId,@ReqId,@GrdSysDetailId,@TermId,GETDATE(),@ModUser,@Score,@IsTransferred  ");
            sb.Append("          ,@isClinicsSatisfied,@IsCourseCompleted,@IsGradeOverridden,@GradeOverriddenBy,GETDATE()) ");

            var sql = sb.ToString();
            // prepare List of command
            var commandList = new List<SqlCommand>();
            foreach (TransferRecordInfo info in insertionTransferList)
            {
                var command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@StuEnrollId", info.EnrollmentId);
                command.Parameters.AddWithValue("@ReqId", info.ReqId);
                command.Parameters.AddWithValue("@GrdSysDetailId", info.GrdSysDetailId);
                command.Parameters.AddWithValue("@TermId", info.TermId);
                command.Parameters.AddWithValue("@ModUser", info.ModUser);
                if (info.Score == -1)
                {
                    command.Parameters.AddWithValue("@Score", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@Score", info.Score);
                }

                command.Parameters.AddWithValue("@IsTransferred", info.IsTransferred);
                command.Parameters.AddWithValue("@isClinicsSatisfied", info.IsClinicsSatisfied);
                command.Parameters.AddWithValue("@IsCourseCompleted", info.IsCourseCompleted);
                command.Parameters.AddWithValue("@IsGradeOverridden", info.IsGradeOverriden);
                command.Parameters.AddWithValue("@GradeOverriddenBy", info.GradeOverridenBy);
                commandList.Add(command);
            }

            // Prepare the transaction
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction("StuTransfer");
            try
            {
                foreach (SqlCommand command in commandList)
                {
                    command.Transaction = transaction;
                }

                // Do the insertions
                foreach (SqlCommand command in commandList)
                {
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback("StuTransfer");
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
