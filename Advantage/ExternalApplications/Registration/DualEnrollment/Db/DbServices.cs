﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Db
{
    public class DbServices
    {
        /// <summary>
        /// Get all student with two enrollments actives with possibility to accept two shared courses
        /// do not consider future start
        /// </summary>
        /// <returns></returns>
        public IList<string> GetAllStudentWithTwoEnrollmentsCapableToShareClass()
        {
            var sql = new StringBuilder();
            sql.Append("        SELECT  StudentId ");
            sql.Append("               ,COUNT(*) AS CNT");
            sql.Append("        FROM    dbo.arStuEnrollments enroll");
            sql.Append("        JOIN    dbo.syStatusCodes codes ON codes.StatusCodeId = enroll.StatusCodeId");
            sql.Append("        JOIN    dbo.sySysStatus status ON status.SysStatusId = codes.SysStatusId");
            sql.Append("        WHERE   status.PostAcademics = 1");
            //sql.Append("                OR status.SysStatusId = 7 ");
            sql.Append(" GROUP BY        StudentId");
            sql.Append("        HAVING  ( COUNT(*) > 1 )");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            conn.Open();
            try
            {
                var list = new List<string>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(reader[0].ToString());
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Get all class section in the schools
        /// </summary>
        /// <returns></returns>
        public IList<string> GetAllClassesInSchool()
        {
            var sql = new StringBuilder();
            sql.Append(" SELECT ClsSectionId FROM dbo.arClassSections ");
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            conn.Open();
            try
            {
                var list = new List<string>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(reader[0].ToString());
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Get all class section in the schools
        /// </summary>
        /// <returns>The list of class id with his ReqId associate</returns>
        public IList<ClassSection> GetAllClassesWithRequirementInSchool()
        {
            var sql = new StringBuilder();
            sql.Append(" SELECT ClsSectionId, ReqId FROM dbo.arClassSections ");
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            conn.Open();
            try
            {
                var list = new List<ClassSection>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    var sec = new ClassSection { ClassId = reader[0].ToString(), ReqId = reader[1].ToString() };
                    list.Add(sec);
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Return the requirement id for all school
        /// </summary>
        /// <returns></returns>
        public IList<string> GetAllRequirementInSchool()
        {
            const string SQL = "SELECT arReqs.ReqId FROM dbo.arReqs WHERE ReqTypeId = 1 ";
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(SQL, conn);
            conn.Open();
            try
            {
                var list = new List<string>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {

                    list.Add(reader[0].ToString());
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Get the enrollments for the student that have the specific ClassId
        /// </summary>
        /// <param name="studentId">Student Id</param>
        /// <param name="classId">ClassId</param>
        /// <returns>A list of enrollment that share the same classid for a specific student</returns>
        public IList<string> GetActiveEnrollmentsWithClassAssociatedForRegisterClass(string studentId, string classId)
        {
            var sql = new StringBuilder();
            sql.Append("       DECLARE @reqId VARCHAR(50) = ( SELECT   ReqId ");
            sql.Append("                                      FROM     arClassSections ");
            sql.Append("                                      WHERE    ClsSectionId = @ClassId ");
            sql.Append("                                    ); ");
            sql.Append("       ");
            sql.Append("       SELECT  enroll.StuEnrollId ");
            sql.Append("       FROM    arStuEnrollments enroll ");
            sql.Append("       JOIN    dbo.arPrgVersions ver ON ver.PrgVerId = enroll.PrgVerId ");
            sql.Append("       JOIN    dbo.arProgVerDef def ON def.PrgVerId = ver.PrgVerId ");
            sql.Append("       WHERE   enroll.StudentId = @StudentId ");
            sql.Append("               AND ( def.ReqId IN ( SELECT GrpId ");
            sql.Append("                                    FROM   dbo.arReqGrpDef ");
            sql.Append("                                    WHERE  ReqId = @reqId ) ");
            sql.Append("                     OR def.ReqId = @reqId ");
            sql.Append("                   ) ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            command.Parameters.AddWithValue("@StudentId", studentId);
            command.Parameters.AddWithValue("@ClassId", classId);
            conn.Open();
            try
            {
                var list = new List<string>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(reader[0].ToString());
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        public IList<string> GetActiveEnrollmentsWithRegisteredRequerimentId(string studentId, string reqid)
        {
            var sql = new StringBuilder();
            sql.Append("       SELECT  enroll.StuEnrollId ");
            sql.Append("       FROM    arStuEnrollments enroll ");
            sql.Append("        JOIN    dbo.syStatusCodes codes ON codes.StatusCodeId = enroll.StatusCodeId");
            sql.Append("        JOIN    dbo.sySysStatus status ON status.SysStatusId = codes.SysStatusId");
            sql.Append("       JOIN    dbo.arPrgVersions ver ON ver.PrgVerId = enroll.PrgVerId ");
            sql.Append("       JOIN    dbo.arProgVerDef def ON def.PrgVerId = ver.PrgVerId ");
            sql.Append("       WHERE   enroll.StudentId = @StudentId ");
            sql.Append("               AND  status.PostAcademics = 1 ");
            sql.Append("               AND ( def.ReqId IN ( SELECT GrpId ");
            sql.Append("                                    FROM   dbo.arReqGrpDef ");
            sql.Append("                                    WHERE  ReqId = @reqId ) ");
            sql.Append("                     OR def.ReqId = @reqId ");
            sql.Append("                   ) ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            command.Parameters.AddWithValue("@StudentId", studentId);
            command.Parameters.AddWithValue("@reqId", reqid);
            conn.Open();
            try
            {
                var list = new List<string>();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    list.Add(reader[0].ToString());
                }

                return list;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Check if the enrollment is register or not
        /// </summary>
        /// <param name="stuEnrollId">The enrollment</param>
        /// <param name="classId">The class</param>
        /// <returns>True if it is register</returns>
        public bool CheckIfEnrollmentIsAlreadyRegister(string stuEnrollId, string classId)
        {
            var sb = new StringBuilder();
            sb.Append(" SELECT count(*) as Count ");
            sb.Append(" FROM arResults a ");
            sb.Append(" WHERE");
            sb.Append("  a.StuEnrollId = @StuEnrollId ");
            sb.Append("  AND a.TestID= @ClassId");
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId);
            command.Parameters.AddWithValue("@ClassId", classId);
            conn.Open();
            try
            {
                var count = (int)command.ExecuteScalar();
                return (count > 0);
            }
            finally
            {
                conn.Close();
            }
        }

        public bool CheckIfEnrollmentIsAlreadyRegisterReq(string stuEnrollId, string reqId)
        {
            var sb = new StringBuilder();
            sb.Append(" SELECT COUNT(*) FROM arResults res ");
            sb.Append(" JOIN dbo.arClassSections sec ON sec.ClsSectionId = res.TestId ");
            sb.Append(" JOIN dbo.arStuEnrollments enroll ON enroll.StuEnrollId = res.StuEnrollId ");
            sb.Append(" JOIN dbo.arReqs req ON req.ReqId = sec.ReqId ");
            sb.Append(" WHERE ");
            sb.Append(" enroll.StuEnrollId = @StuEnrollId ");
            sb.Append(" AND  req.ReqId = @reqID ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId);
            command.Parameters.AddWithValue("@reqID", reqId);
            conn.Open();
            try
            {
                var count = (int)command.ExecuteScalar();
                //if (stuEnrollId.ToUpper() == "D91E2791-2E6F-49FB-B833-C70F1CC85A64" )
                //{
                //    Console.WriteLine("ReqId: {0}", reqId);
                //    Console.WriteLine("Match : {0}{1}", count, Environment.NewLine);
                //}
                return (count > 0);
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Check if a requirement is transferred to the enrollments
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="reqId"></param>
        /// <returns></returns>
        public bool CheckIfCourseIsTransferred(string stuEnrollId, string reqId)
        {
            var sb = new StringBuilder();
            sb.Append(" SELECT  COUNT(*) AS Count ");
            sb.Append(" FROM    dbo.arTransferGrades ");
            sb.Append(" WHERE   StuEnrollId = @StuEnrollId ");
            sb.Append("         AND ReqId = @ReqId ");
            sb.Append("  ");
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId);
            command.Parameters.AddWithValue("@ReqId", reqId);
            conn.Open();
            try
            {
                var count = (int)command.ExecuteScalar();
                return (count > 0);
            }
            finally
            {
                conn.Close();
            }
        }

        public TransferRecordInfo GetRequirementTransferredRecord(string stuEnrollId, string reqId)
        {
            var sb = new StringBuilder();
            sb.Append(" SELECT  StuEnrollId, ReqId, GrdSysDetailId, TermId, Score, IsTransferred, isClinicsSatisfied ");
            sb.Append(" ,IsCourseCompleted, IsGradeOverridden ");
            sb.Append(" FROM    dbo.arTransferGrades ");
            sb.Append(" WHERE   StuEnrollId = @StuEnrollId ");
            sb.Append("         AND ReqId = @ReqId ");
            sb.Append("  ");
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId);
            command.Parameters.AddWithValue("@ReqId", reqId);
            conn.Open();
            try
            {
                var tr = new TransferRecordInfo { EnrollmentId = Guid.Empty.ToString() };
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    tr.EnrollmentId = reader[0].ToString();
                    tr.ReqId = reader[1].ToString();
                    tr.GrdSysDetailId = reader[2].ToString();
                    tr.TermId = reader[3].ToString();
                    tr.Score = (reader[4] is DBNull) ? -1 : (decimal)reader[4];
                    tr.IsTransferred = (!(reader[5] is DBNull) && ((bool)reader[5]));
                    tr.IsClinicsSatisfied = (!(reader[6] is DBNull) && ((bool)reader[6]));
                    tr.IsCourseCompleted = ((bool)reader[6]);
                    tr.IsGradeOverriden = ((bool)reader[7]);
                }

                return tr;
            }
            finally
            {
                conn.Close();
            }
        }

        public string GetInformationString(string student, string classid)
        {
            var sb = new StringBuilder();
            sb.Append("  DECLARE @fullname VARCHAR(100) = ( SELECT   CONCAT(LastName,' ',FirstName) AS FullName ");
            sb.Append("                                     FROM     dbo.arStudent ");
            sb.Append("                                     WHERE    StudentId = @StudentId ");
            sb.Append("                                   )  ");
            sb.Append("  DECLARE @ClassInfo VARCHAR(500) = ( SELECT  CONCAT(ClsSection,' - ',req.Code,' - ',req.Descrip,' StartDate: ', ");
            sb.Append("                                                     CAST(StartDate AS DATE),' End Date: ',CAST(EndDate AS DATE)) ");
            sb.Append("                                      FROM    dbo.arClassSections ");
            sb.Append("                                      JOIN    dbo.arReqs req ON req.ReqId = arClassSections.ReqId ");
            sb.Append("                                      WHERE   ClsSectionId = @ClassId ");
            sb.Append("                                    ) ");
            sb.Append("   ");
            sb.Append("  SELECT CONCAT (@fullname, ' - ', @ClassInfo) ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StudentId", student);
            command.Parameters.AddWithValue("@ClassId", classid);
            conn.Open();
            try
            {
                var info = command.ExecuteScalar().ToString();
                return info;
            }
            finally
            {
                conn.Close();
            }
        }

        public StudentInfo GetStudentInfo(string student, string classid)
        {
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            var sb = new StringBuilder();
            sb.Append("   SELECT   CONCAT(LastName,' ',FirstName) AS FullName  ");
            sb.Append("                                     FROM     dbo.arStudent ");
            sb.Append("                                     WHERE    StudentId = @StudentId ");
            var command1 = new SqlCommand(sb.ToString(), conn);
            command1.Parameters.AddWithValue("@StudentId", student);

            sb.Clear();
            sb.Append("  SELECT  ClsSection, req.Code, req.Descrip, ");
            sb.Append("  StartDate, EndDate ");
            sb.Append("        FROM    dbo.arClassSections ");
            sb.Append("        JOIN    dbo.arReqs req ON req.ReqId = arClassSections.ReqId ");
            sb.Append("        WHERE   ClsSectionId = @ClassId ");

            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StudentId", student);
            command.Parameters.AddWithValue("@ClassId", classid);
            conn.Open();
            try
            {
                var info = new StudentInfo { FullName = command1.ExecuteScalar().ToString(), StudentId = student };

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    info.ClassId = classid;
                    info.ClassSection = reader[0].ToString();
                    info.RequirementCode = reader[1].ToString();
                    info.RequirementDescription = reader[2].ToString();
                    info.StartDate = (DateTime)reader[3];
                    info.EndDate = (DateTime)reader[4];
                }

                return info;
            }
            finally
            {
                conn.Close();
            }
        }

        public StudentInfo GetStudentInfoReq(string studentId, string reqId)
        {
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            var sb = new StringBuilder();
            sb.Append("   SELECT   CONCAT(LastName,' ',FirstName) AS FullName  ");
            sb.Append("                                     FROM     dbo.arStudent ");
            sb.Append("                                     WHERE    StudentId = @StudentId ");
            var command1 = new SqlCommand(sb.ToString(), conn);
            command1.Parameters.AddWithValue("@StudentId", studentId);

            const string SQL = "SELECT  Code, Descrip  FROM dbo.arReqs WHERE ReqId = @ReqId";

            var command = new SqlCommand(SQL, conn);
            command.Parameters.AddWithValue("@ReqId", reqId);
            conn.Open();
            try
            {
                var info = new StudentInfo { FullName = command1.ExecuteScalar().ToString(), StudentId = studentId };

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    info.ReqId = reqId;
                    info.RequirementCode = reader[0].ToString();
                    info.RequirementDescription = reader[1].ToString();
                }

                return info;
            }
            finally
            {
                conn.Close();
            }
        }

        public List<ArResultsClass> GetRegisterInformation(string stuEnrollId, string reqId)
        {
            var sb = new StringBuilder();
            sb.Append(" SELECT Score, GrdSysDetailId, res.Cnt, res.Hours, IsInComplete, DroppedInAddDrop ");
            sb.Append(" 	, IsTransfered, isClinicsSatisfied, res.DateDetermined, isCourseCompleted, res.ModUser ");
            sb.Append(" 	, IsGradeOverridden, GradeOverriddenBy, GradeOverriddenDate, res.StuEnrollId, TestId ");
            sb.Append("  FROM arResults res ");
            sb.Append(" JOIN dbo.arClassSections sec ON sec.ClsSectionId = res.TestId ");
            sb.Append(" JOIN dbo.arStuEnrollments enroll ON enroll.StuEnrollId = res.StuEnrollId ");
            sb.Append(" JOIN dbo.arReqs req ON req.ReqId = sec.ReqId ");
            sb.Append(" WHERE req.ReqId = @reqID ");
            sb.Append(" AND enroll.StuEnrollId = @StuEnrollId ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", stuEnrollId);
            command.Parameters.AddWithValue("@reqID", reqId);
            conn.Open();
            try
            {
                // Read ArResults
                var reader = command.ExecuteReader();
                var arInfoList = new List<ArResultsClass>();
                //var i = 0;
                while (reader.Read())
                {
                    var arInfo = new ArResultsClass();
                    //if (i > 0)
                    //{
                    //    Console.WriteLine(" retake");
                    //}
                    //i++;
                    var cnt = reader["Cnt"];
                    arInfo.Cnt = (cnt == DBNull.Value) ? null : (int?)cnt;
                    var dd = reader["DateDetermined"];
                    arInfo.DateDetermined = (dd == DBNull.Value) ? null : (DateTime?)dd;
                    arInfo.DroppedInAddDrop = (bool)reader["DroppedInAddDrop"];
                    arInfo.GradeOverridenBy = reader["GradeOverriddenBy"].ToString();
                    var dov = reader["GradeOverriddenDate"];
                    arInfo.GradeOverridenDate = (dov == DBNull.Value) ? null : (DateTime?)dov;
                    arInfo.GrdSysDetailId = reader["GrdSysDetailId"].ToString();
                    var h = reader["Hours"];
                    arInfo.Hours = (h == DBNull.Value) ? null : (int?)h;
                    var ic = reader["isClinicsSatisfied"];
                    arInfo.IsClinicsSatisfied = (ic == DBNull.Value) ? null : (bool?)ic;
                    var it = reader["IsTransfered"];
                    arInfo.IsTransferred = (it == DBNull.Value) ? null : (bool?)it;
                    arInfo.ModDate = DateTime.Now;
                    arInfo.ModUser = reader["ModUser"].ToString();
                    var sc = reader["Score"];
                    arInfo.Score = (sc == DBNull.Value) ? null : (decimal?)sc;
                    arInfo.EnrollmentId = reader["StuEnrollId"].ToString();
                    var test = reader["TestId"];
                    arInfo.TestId = (test == DBNull.Value) ? null : test.ToString();
                    var incom = reader["IsInComplete"];
                    arInfo.IsIncomplete = (incom == DBNull.Value) ? null : (bool?)incom;
                    arInfo.IsCourseCompleted = (bool)reader["IsCourseCompleted"];
                    arInfo.IsGradeOverriden = (bool)reader["IsGradeOverridden"];
                    arInfoList.Add(arInfo);
                }

                reader.Close();
                return arInfoList;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Get all academic information for the given enrollment
        /// the information from three tables should be gotten
        /// </summary>
        /// <param name="registeredEnrollment">The in parameter is also the out parameters to fill with the info</param>
        /// <returns></returns>
        public ICorrectionRegInfo GetAllAcademicInformation(ICorrectionRegInfo registeredEnrollment)
        {

            var resultClasslist = registeredEnrollment.ArInfoList;
            foreach (ArResultsClass resultsClass in resultClasslist)
            {
                registeredEnrollment = GetAllAcademicInformationForOneClass(registeredEnrollment, resultsClass);
            }

            return registeredEnrollment;
        }

        private ICorrectionRegInfo GetAllAcademicInformationForOneClass(ICorrectionRegInfo registeredEnrollment, ArResultsClass arInfo)
        {
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            // Prepare get info from arGrdBkResults
            const string SQL = "SELECT InstrGrdBkWgtDetailId, Score, Comments, ResNum, PostDate, IsCompGraded, isCourseCredited " +
                               "FROM dbo.arGrdBkResults WHERE StuEnrollId = @StuEnroll AND ClsSectionId = @TestId";

            var command1 = new SqlCommand(SQL, conn);
            command1.Parameters.AddWithValue("@StuEnroll", arInfo.EnrollmentId);
            command1.Parameters.AddWithValue("@TestId", arInfo.TestId);

            // Prepare to get info from atClsSectAttendance
            const string SQL1 = "SELECT	ClsSectMeetingId, MeetDate, Actual, Tardy, Excused, Comments, Scheduled  " +
                                "FROM atClsSectAttendance WHERE StuEnrollId = @StuEnroll AND ClsSectionId = @TestId ";
            var command2 = new SqlCommand(SQL1, conn);
            command2.Parameters.AddWithValue("@StuEnroll", arInfo.EnrollmentId);
            command2.Parameters.AddWithValue("@TestId", arInfo.TestId);

            conn.Open();
            try
            {
                // Read arGrdBkResults
                var reader1 = command1.ExecuteReader();
                while (reader1.Read())
                {
                    var gr = new GradeBookResultInfo
                    {
                        ClsSectionId = arInfo.TestId,

                        InstrGrdBkWgtDetailId = reader1["InstrGrdBkWgtDetailId"].ToString(),
                        ModDate = DateTime.Now,
                        ModUser = arInfo.ModUser,
                        ResNum = (int)reader1["ResNum"],
                    };

                    var sc = reader1["Score"];
                    gr.Score = (sc == DBNull.Value) ? null : (decimal?)sc;
                    var comm = reader1["Comments"];
                    gr.Comments = (comm == DBNull.Value) ? null : comm.ToString();
                    gr.StuEnrollId = arInfo.EnrollmentId;
                    var post = reader1["PostDate"];
                    gr.PostDate = (post == DBNull.Value) ? null : (DateTime?)post;
                    var isc = reader1["IsCompGraded"];
                    gr.IsCompGraded = (isc == DBNull.Value) ? null : (bool?)isc;
                    var icc = reader1["IsCourseCredited"];
                    gr.IsCourseCredited = (icc == DBNull.Value) ? null : (bool?)icc;

                    registeredEnrollment.GradeBookList.Add(gr);
                }
                reader1.Close();

                // Read atClsSectAttendance
                var reader2 = command2.ExecuteReader();
                while (reader2.Read())
                {

                    var at = new AtClsSectAttendance
                    {
                        Actual = (decimal)reader2["Actual"],
                        MeetDate = (DateTime)reader2["MeetDate"],
                        ClsSectionId = arInfo.TestId,
                        Excused = (bool)reader2["Excused"],
                        StuEnrollId = arInfo.EnrollmentId,
                        Tardy = (bool)reader2["Tardy"]
                    };

                    var sch = reader2["Scheduled"];
                    at.Scheduled = (sch == DBNull.Value) ? null : (decimal?)sch;
                    var co = reader2["Comments"];
                    at.Comments = (co == DBNull.Value) ? null : co.ToString();
                    var clm = reader2["ClsSectMeetingId"];
                    at.ClsSectMeetingId = (clm == DBNull.Value) ? null : clm.ToString();
                    registeredEnrollment.AttendanceList.Add(at);
                }

                // Return all Academics information
                return registeredEnrollment;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Return if the enrollment has not academic information inserted.
        /// it is has returned true if not return false.
        /// </summary>
        /// <param name="enrollId">the enroll id</param>
        /// <param name="reqId">the requirement id</param>
        /// <returns>String empty if the information does not exists. 
        /// if exists return the name of the tables that contain information,
        ///  separate by spaces
        /// </returns>
        public string HasTheEnrollmentAnyAcademicRecord(string enrollId, string reqId)
        {
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);


            // Prepare get info from arGrdBkResults
            const string SQL =
                "SELECT COUNT(*) FROM dbo.arGrdBkResults " +
                "JOIN dbo.arClassSections sec ON sec.ClsSectionId = arGrdBkResults.ClsSectionId JOIN dbo.arReqs req ON req.ReqId = sec.ReqId " +
                "WHERE StuEnrollId = @stuEnrollId AND sec.ReqId = @ReqId ";
            var command1 = new SqlCommand(SQL, conn);
            command1.Parameters.AddWithValue("@stuEnrollId", enrollId);
            command1.Parameters.AddWithValue("@ReqId", reqId);

            // Prepare to get info from atClsSectAttendance
            const string SQL1 =
                "SELECT COUNT(*) FROM atClsSectAttendance att " +
                "JOIN dbo.arClassSections sec ON sec.ClsSectionId = att.ClsSectionId JOIN dbo.arReqs req ON req.ReqId = sec.ReqId " +
                "WHERE StuEnrollId = @stuEnrollId AND sec.ReqId = @ReqId";
            var command2 = new SqlCommand(SQL1, conn);
            command2.Parameters.AddWithValue("@stuEnrollId", enrollId);
            command2.Parameters.AddWithValue("@ReqId", reqId);
            var returnInfo = string.Empty;
            conn.Open();
            try
            {
                // Read arGrdBkResults
                var count = (int)command1.ExecuteScalar();
                if (count > 0) returnInfo = "arGrdBkResults";


                // Read atClsSectAttendance
                var count1 = (int)command2.ExecuteScalar();
                returnInfo += (count1 > 0) ? " atClsSectAttendance" : string.Empty;
                return returnInfo;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// This is a transactional operation to insert all record missing of academic.
        /// </summary>
        /// <param name="insertionRegisterList"></param>
        public void InsertAllcademicInformationInDataBase(IList<ICorrectionRegInfo> insertionRegisterList)
        {
            //begin to create the necessary sql
            // Insert in ArResults
            var sb = new StringBuilder();
            sb.Append(" INSERT dbo.arResults ");
            sb.Append("         ( ResultId ");
            sb.Append("         ,TestId ");
            sb.Append("         ,Score ");
            sb.Append("         ,GrdSysDetailId ");
            sb.Append("         ,Cnt ");
            sb.Append("         ,Hours ");
            sb.Append("         ,StuEnrollId ");
            sb.Append("         ,IsInComplete ");
            sb.Append("         ,DroppedInAddDrop ");
            sb.Append("         ,ModUser ");
            sb.Append("         ,ModDate ");
            sb.Append("         ,IsTransfered ");
            sb.Append("         ,isClinicsSatisfied ");
            sb.Append("         ,DateDetermined ");
            sb.Append("         ,IsCourseCompleted ");
            sb.Append("         ,IsGradeOverridden ");
            sb.Append("         ,GradeOverriddenBy ");
            sb.Append("         ,GradeOverriddenDate ");
            sb.Append("         ) ");
            sb.Append(" VALUES  ( @ResultId ");
            sb.Append("         , @TestId  ");
            sb.Append("         , @Score ");
            sb.Append("         , @GrdSysDetailId ");
            sb.Append("         , @Cnt ");
            sb.Append("         , @Hours ");
            sb.Append("         , @StuEnrollId ");
            sb.Append("         , @IsInComplete ");
            sb.Append("         , @DroppedInAddDrop ");
            sb.Append("         , @ModUser ");
            sb.Append("         ,GETDATE() ");
            sb.Append("         , @IsTransfered ");
            sb.Append("         , @isClinicsSatisfied ");
            sb.Append("         , @DateDetermined ");
            sb.Append("         , @IsCourseCompleted ");
            sb.Append("         , @IsGradeOverridden ");
            sb.Append("         , @GradeOverriddenBy ");
            sb.Append("         , @GradeOverriddenDate ");
            sb.Append("         ) ");

            // Insert in arGrdBkResults
            var sb1 = new StringBuilder();
            sb1.Append(" INSERT  dbo.arGrdBkResults  ");
            sb1.Append("         ( GrdBkResultId  ");
            sb1.Append("         ,ClsSectionId  ");
            sb1.Append("         ,InstrGrdBkWgtDetailId  ");
            sb1.Append("         ,Score  ");
            sb1.Append("         ,Comments  ");
            sb1.Append("         ,StuEnrollId  ");
            sb1.Append("         ,ModUser  ");
            sb1.Append("         ,ModDate  ");
            sb1.Append("         ,ResNum  ");
            sb1.Append("         ,PostDate  ");
            sb1.Append("         ,IsCompGraded  ");
            sb1.Append("         ,isCourseCredited  ");
            sb1.Append("         )  ");
            sb1.Append(" VALUES  ( @GrdBkResultId  ");
            sb1.Append("         , @ClsSectionId  ");
            sb1.Append("         , @InstrGrdBkWgtDetailId  ");
            sb1.Append("         , @Score  ");
            sb1.Append("         , @Comments  ");
            sb1.Append("         , @StuEnrollId  ");
            sb1.Append("         , @ModUser  ");
            sb1.Append("         , GETDATE()  ");
            sb1.Append("         , @ResNum  ");
            sb1.Append("         , @PostDate  ");
            sb1.Append("         , @IsCompGraded  ");
            sb1.Append("         , @IsCourseCredited  ");
            sb1.Append("         )  ");

            // Insert in atClsSectAttendance
            var sb2 = new StringBuilder();
            sb2.Append("  INSERT dbo.atClsSectAttendance ");
            sb2.Append("          ( ClsSectAttId ");
            sb2.Append("          ,StudentId ");
            sb2.Append("          ,ClsSectionId ");
            sb2.Append("          ,ClsSectMeetingId ");
            sb2.Append("          ,MeetDate ");
            sb2.Append("          ,Actual ");
            sb2.Append("          ,Tardy ");
            sb2.Append("          ,Excused ");
            sb2.Append("          ,Comments ");
            sb2.Append("          ,StuEnrollId ");
            sb2.Append("          ,Scheduled ");
            sb2.Append("          ) ");
            sb2.Append("  VALUES  ( @ClsSectAttId ");
            sb2.Append("          , @StudentId ");
            sb2.Append("          , @ClsSectionId  ");
            sb2.Append("          , @ClsSectMeetingId  ");
            sb2.Append("          , @MeetDate ");
            sb2.Append("          , @Actual  ");
            sb2.Append("          , @Tardy  ");
            sb2.Append("          , @Excused  ");
            sb2.Append("          , @Comments  ");
            sb2.Append("          , @StuEnrollId  ");
            sb2.Append("          , @Scheduled  ");
            sb2.Append("          ) ");

            // Create connection
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            //Create a list of commands
            IList<SqlCommand> commandList = new List<SqlCommand>();

            // Create Commands
            foreach (ICorrectionRegInfo res in insertionRegisterList)
            {
                // Debug.WriteLine("{2}   -ArResults - Enroll: {0} - TestID:{1}",res.ArInfo.EnrollmentId, res.ArInfo.TestId, commandList.Count);
                // Create command for ArResults
                foreach (ArResultsClass info in res.ArInfoList)
                {
                    var ac = new SqlCommand(sb.ToString(), conn);
                    ac.Parameters.AddWithValue("@ResultId", Guid.NewGuid());
                    ac.Parameters.AddWithValue("@TestId", Guid.Parse(info.TestId));
                    ac.Parameters.AddWithValue("@Score", (object)info.Score ?? DBNull.Value);
                    if (string.IsNullOrEmpty(info.GrdSysDetailId)) ac.Parameters.AddWithValue("@GrdSysDetailId", DBNull.Value);
                    else ac.Parameters.AddWithValue("@GrdSysDetailId", Guid.Parse(info.GrdSysDetailId));

                    ac.Parameters.AddWithValue("@Cnt", (object)info.Cnt ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@Hours", (object)info.Hours ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@StuEnrollId", Guid.Parse(info.EnrollmentId));
                    ac.Parameters.AddWithValue("@IsInComplete", (object)info.IsIncomplete ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@DroppedInAddDrop", info.DroppedInAddDrop);
                    ac.Parameters.AddWithValue("@ModUser", info.ModUser);
                    ac.Parameters.AddWithValue("@IsTransfered ", (object)info.IsTransferred ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@isClinicsSatisfied", (object)info.IsClinicsSatisfied ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@DateDetermined", (object)info.DateDetermined ?? DBNull.Value);
                    ac.Parameters.AddWithValue("@IsCourseCompleted", info.IsCourseCompleted);
                    ac.Parameters.AddWithValue("@IsGradeOverridden", info.IsGradeOverriden);
                    ac.Parameters.AddWithValue("@GradeOverriddenBy", info.GradeOverridenBy);
                    ac.Parameters.AddWithValue("@GradeOverriddenDate", (object)info.GradeOverridenDate ?? DBNull.Value);
                    commandList.Add(ac);
                }

                //Create Command List for arGrdBkResults
                foreach (GradeBookResultInfo info in res.GradeBookList)
                {
                    //Debug.WriteLine("{3}  -arGrdBkResults enroll: {0} - ClsSectionID: {1} -  DetailId{2}", info.StuEnrollId, info.ClsSectionId, info.InstrGrdBkWgtDetailId, commandList.Count);
                    var gbc = new SqlCommand(sb1.ToString(), conn);
                    gbc.Parameters.AddWithValue("@GrdBkResultId", Guid.NewGuid());
                    gbc.Parameters.AddWithValue("@ClsSectionId", Guid.Parse(info.ClsSectionId));
                    gbc.Parameters.AddWithValue("@InstrGrdBkWgtDetailId", Guid.Parse(info.InstrGrdBkWgtDetailId));
                    gbc.Parameters.AddWithValue("@Score", (object)info.Score ?? DBNull.Value);
                    gbc.Parameters.AddWithValue("@Comments", (object)info.Comments ?? DBNull.Value);
                    gbc.Parameters.AddWithValue("@StuEnrollId", Guid.Parse(info.StuEnrollId));
                    gbc.Parameters.AddWithValue("@ModUser", "SUPPORT");
                    gbc.Parameters.AddWithValue("@ResNum", info.ResNum);
                    gbc.Parameters.AddWithValue("@PostDate", (object)info.PostDate ?? DBNull.Value);
                    gbc.Parameters.AddWithValue("@IsCompGraded", (object)info.IsCompGraded ?? DBNull.Value);
                    gbc.Parameters.AddWithValue("@IsCourseCredited", (object)info.IsCourseCredited ?? DBNull.Value);

                    commandList.Add(gbc);
                }

                // Create command List of atClsSectAttendance 
                foreach (AtClsSectAttendance att in res.AttendanceList)
                {
                    //Debug.WriteLine("{3}  -atClsSectAttendance enroll: {0} - ClsSectionID: {1} -  DetailId{2}", att.StuEnrollId, att.ClsSectionId, att.ClsSectMeetingId, commandList.Count);
                    var atc = new SqlCommand(sb2.ToString(), conn);
                    atc.Parameters.AddWithValue("@ClsSectAttId", Guid.NewGuid());
                    atc.Parameters.AddWithValue("@StudentId", DBNull.Value);
                    atc.Parameters.AddWithValue("@ClsSectionId", Guid.Parse(att.ClsSectionId));
                    if (string.IsNullOrEmpty(att.ClsSectMeetingId)) atc.Parameters.AddWithValue("@ClsSectMeetingId", DBNull.Value);
                    else atc.Parameters.AddWithValue("@ClsSectMeetingId", Guid.Parse(att.ClsSectMeetingId));
                    atc.Parameters.AddWithValue("@MeetDate", att.MeetDate);
                    atc.Parameters.AddWithValue("@Actual", att.Actual);
                    atc.Parameters.AddWithValue("@Tardy", att.Tardy);
                    atc.Parameters.AddWithValue("@Excused", att.Excused);
                    atc.Parameters.AddWithValue("@Comments", (object)att.Comments ?? DBNull.Value);
                    atc.Parameters.AddWithValue("@StuEnrollId", Guid.Parse(att.StuEnrollId));
                    atc.Parameters.AddWithValue("@Scheduled", (object)att.Scheduled ?? DBNull.Value);
                    commandList.Add(atc);
                }
            }

            // Open connection and assign the transactions
            conn.Open();
            SqlTransaction transaction = conn.BeginTransaction();
            try
            {
                // Prepare the transaction
                foreach (SqlCommand command in commandList)
                {
                    command.Transaction = transaction;
                }

                int counter = 0;
                foreach (SqlCommand command in commandList)
                {
                    Console.WriteLine("{0}", counter);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    counter++;
                    command.ExecuteNonQuery();
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }

            finally
            {
                conn.Close();
            }
        }

        public StudentInfo GetClassInfoRegistered(StudentInfo info)
        {
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);

            var sb = new StringBuilder();
            sb.Append("  SELECT CONCAT(LastName,' ',FirstName), req.Code, req.Descrip, res.TestID, sec.ClsSection, sec.StartDate, sec.EndDate    ");
            sb.Append("  FROM ArResults res   ");
            sb.Append("  JOIN dbo.arStuEnrollments enroll ON enroll.StuEnrollId = res.StuEnrollId   ");
            sb.Append("  JOIN dbo.arStudent stu ON stu.StudentId = enroll.StudentId   ");
            sb.Append("  JOIN dbo.arClassSections sec ON sec.ClsSectionId = res.TestId   ");
            sb.Append("  JOIN arReqs req ON req.ReqId = sec.ReqId   ");
            sb.Append("  WHERE   ");
            sb.Append("  enroll.StudentId = @StudentId   ");
            sb.Append("  AND req.ReqId = @ReqId   ");


            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StudentId", info.StudentId);
            command.Parameters.AddWithValue("@ReqId", info.ReqId);
            conn.Open();
            try
            {
                var inf = new StudentInfo();

                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    inf.FullName = reader[0].ToString();
                    inf.RequirementCode = reader[1].ToString();
                    inf.RequirementDescription = reader[2].ToString();
                    inf.ClassId = reader[3].ToString();
                    inf.ClassSection = reader[4].ToString();
                    inf.StartDate = ((DateTime?)reader[5]).GetValueOrDefault();
                    inf.EndDate = ((DateTime?)reader[6]).GetValueOrDefault();
                    inf.StudentId = info.StudentId;
                    inf.ReqId = info.ReqId;
                }

                return inf;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Delete attendance info for specific enrollment and specific class
        /// </summary>
        /// <param name="enrollmentId"></param>
        /// <param name="reqId"></param>
        public void DeleteAttendanceInfo(string enrollmentId, string reqId)
        {
            var sb = new StringBuilder();
            sb.Append("  DELETE FROM dbo.atClsSectAttendance ");
            sb.Append("  WHERE  ClsSectAttId IN ( SELECT    att.ClsSectAttId ");
            sb.Append("                           FROM      atClsSectAttendance att ");
            sb.Append("                           JOIN      dbo.arClassSections sec ON sec.ClsSectionId = att.ClsSectionId ");
            sb.Append("                           JOIN      dbo.arReqs req ON req.ReqId = sec.ReqId ");
            sb.Append("                           WHERE     StuEnrollId = @StuEnrollId ");
            sb.Append("                                     AND sec.ReqId = @reqId ) ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", enrollmentId);
            command.Parameters.AddWithValue("@reqID", reqId);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Delete Academic Info.
        /// </summary>
        /// <param name="enrollmentId"></param>
        /// <param name="reqId"></param>
        public void DeleteAcademicInfo(string enrollmentId, string reqId)
        {
            var sb = new StringBuilder();

            sb.Append(" DELETE FROM dbo.arGrdBkResults ");
            sb.Append(" WHERE  GrdBkResultId IN ( SELECT   GrdBkResultId ");
            sb.Append("                           FROM     dbo.arGrdBkResults ");
            sb.Append("                           JOIN     dbo.arClassSections sec ON sec.ClsSectionId = arGrdBkResults.ClsSectionId ");
            sb.Append("                           JOIN     dbo.arReqs req ON req.ReqId = sec.ReqId ");
            sb.Append("                           WHERE    StuEnrollId = @stuEnrollId ");
            sb.Append("                                    AND sec.ReqId = @reqId ) ");

            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sb.ToString(), conn);
            command.Parameters.AddWithValue("@StuEnrollId", enrollmentId);
            command.Parameters.AddWithValue("@reqID", reqId);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
