﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Db
{
    public class LoggerServices
    {
        public void WriteInfo(StudentInfo info)
        {
            var sql = new StringBuilder();
            sql.Append("  INSERT INTO dbo.SyLoggerDualEnrollment  ");
            sql.Append("          ( StudentId  ");
            sql.Append("          ,FullName  ");
            sql.Append("          ,ClassId  ");
            sql.Append("          ,ClassSection  ");
            sql.Append("          ,RequirementCode  ");
            sql.Append("          ,StartDate  ");
            sql.Append("          ,EndDate  ");
            sql.Append("          ,Classification  ");
            sql.Append("          ,ModUser  ");
            sql.Append("          ,ModDate  ");
            sql.Append("          )  ");
            sql.Append("  VALUES  ( @StudentId         ");
            sql.Append("          , @FullName          ");
            sql.Append("          , @ClassId           ");
            sql.Append("          , @ClassSection      ");
            sql.Append("          , @RequirementCode   ");
            sql.Append("          , @StartDate         ");
            sql.Append("          , @EndDate           ");
            sql.Append("          , @Classification    ");
            sql.Append("          , @ModUser           ");
            sql.Append("          ,GETDATE()           ");
            sql.Append("          )  ");


            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(sql.ToString(), conn);
            var classid = info.ClassId ?? Guid.Empty.ToString();
            var classSection = info.ClassSection ?? string.Empty;
            command.Parameters.AddWithValue("@StudentId", info.StudentId);
            command.Parameters.AddWithValue("@FullName", info.FullName);
            command.Parameters.AddWithValue("@ClassId", classid);
            command.Parameters.AddWithValue("@ClassSection", classSection);
            command.Parameters.AddWithValue("@RequirementCode", info.RequirementCode);
            if (info.StartDate == DateTime.MinValue)
            {
                command.Parameters.AddWithValue("@StartDate", DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue("@StartDate", info.StartDate);
            }
            if (info.EndDate == DateTime.MinValue)
            {
                command.Parameters.AddWithValue("@EndDate", DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue("@EndDate", info.EndDate);
            }

            command.Parameters.AddWithValue("@Classification", info.Classification);
            command.Parameters.AddWithValue("@ModUser", "SUPPORT");

            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void TruncateLoggerTable()
        {
            const string SQL = "TRUNCATE TABLE syLoggerDualEnrollment ";
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            var command = new SqlCommand(SQL, conn);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        public void WriteListOfLogInfo(IList<StudentInfo> needRegistrationList, int classification)
        {
            var sql = new StringBuilder();
            sql.Append("  INSERT INTO dbo.SyLoggerDualEnrollment  ");
            sql.Append("          ( StudentId  ");
            sql.Append("          ,FullName  ");
            sql.Append("          ,ClassId  ");
           sql.Append("          ,ClassSection  ");
            sql.Append("          ,RequirementCode  ");
            sql.Append("          ,StartDate  ");
            sql.Append("          ,EndDate  ");
            sql.Append("          ,Classification  ");
            sql.Append("          ,ModUser  ");
            sql.Append("          ,ModDate  ");
            sql.Append("          )  ");
            sql.Append("  VALUES  ( @StudentId         ");
            sql.Append("          , @FullName          ");
            sql.Append("          , @ClassId           ");
            sql.Append("          , @ClassSection      ");
            sql.Append("          , @RequirementCode   ");
            sql.Append("          , @StartDate         ");
            sql.Append("          , @EndDate           ");
            sql.Append("          , @Classification    ");
            sql.Append("          , @ModUser           ");
            sql.Append("          ,GETDATE()           ");
            sql.Append("          )  ");

            // Create the commands list to execute
            var connString = InitDualEnrollment.ConnectionString;
            var conn = new SqlConnection(connString);
            IList<SqlCommand> commandlist = new List<SqlCommand>();

            foreach (StudentInfo info in needRegistrationList)
            {
                info.Classification = classification;
                var command = new SqlCommand(sql.ToString(), conn);
                var classid = string.IsNullOrEmpty(info.ClassId)? Guid.Empty.ToString(): info.ClassId;
                var classSection = info.ClassSection ?? string.Empty;
                var fulln = info.FullName ?? string.Empty;
                var reqcode = info.RequirementCode ?? string.Empty;
                command.Parameters.AddWithValue("@StudentId", info.StudentId);
                command.Parameters.AddWithValue("@FullName", fulln);
              command.Parameters.AddWithValue("@ClassId", classid);
               command.Parameters.AddWithValue("@ClassSection", classSection);
                command.Parameters.AddWithValue("@RequirementCode", reqcode);
                if (info.StartDate == DateTime.MinValue)
                {
                    command.Parameters.AddWithValue("@StartDate", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@StartDate", info.StartDate);
                }
                if (info.EndDate == DateTime.MinValue)
                {
                    command.Parameters.AddWithValue("@EndDate", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("@EndDate", info.EndDate);
                }

                command.Parameters.AddWithValue("@Classification", info.Classification);
                command.Parameters.AddWithValue("@ModUser", "SUPPORT");
                commandlist.Add(command);
            }

            conn.Open();
            try
            {
                foreach (SqlCommand command in commandlist)
                {
                    command.ExecuteNonQuery();
                }
                
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
