﻿using System.Collections.Generic;

namespace DualEnrollment.SupportClasses
{
    public interface ICorrectionRegInfo
    {
        /// <summary>
        /// The record in ArResults
        /// </summary>
        IList<ArResultsClass> ArInfoList { get; set; }

        /// <summary>
        /// The records in Grade-book.
        /// </summary>
        IList<GradeBookResultInfo> GradeBookList { get; set; }

        /// <summary>
        /// The attendance Records
        /// </summary>
        IList<AtClsSectAttendance> AttendanceList { get; set; }

        /// <summary>
        /// This field is only used to store the enrollments referred to this class
        /// </summary>
        string Enrollment { get; set; }

    }
}
