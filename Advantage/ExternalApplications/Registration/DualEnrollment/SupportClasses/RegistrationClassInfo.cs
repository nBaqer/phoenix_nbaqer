﻿namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Hold the necessary information to register a class in one enrollment
    /// </summary>
    public class RegistrationClassInfo
    {

        public RegistrationClassInfo(string studentid, string classId, string stuEnrollmentId)
        {
            StuEnrollmentId = stuEnrollmentId;
            ClassId = classId;
            StudentId = studentid;
        }

        public string StudentId { get; set; }
        public string ClassId { get; set; }
        public string StuEnrollmentId { get; set; }
        public string ReqId { get; set; }
        public string UserName { get; set; }
        public string CampusId { get; set; }
   }
}
