﻿using System;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    ///  Hold information from ArGrdBkResults Table.
    /// </summary>
    [Serializable]
    public class GradeBookResultInfo
    {
        public string ClsSectionId { get; set; }
        public string InstrGrdBkWgtDetailId { get; set; }
        public decimal? Score { get; set; }
        public string Comments { get; set; }
        public string StuEnrollId { get; set; }
        public string ModUser { get; set; }
        public DateTime ModDate { get; set; }
        public int ResNum { get; set; }
        public DateTime? PostDate { get; set; }
        public bool? IsCompGraded { get; set; }
        public bool? IsCourseCredited { get; set; }

    }
}
