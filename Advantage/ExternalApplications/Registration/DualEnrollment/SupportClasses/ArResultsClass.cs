﻿using System;

namespace DualEnrollment.SupportClasses
{
    [Serializable]
    public class ArResultsClass
    {
        /// <summary>
        /// Record where we take the information
        /// </summary>
        public string TestId { get; set; }
        public decimal? Score { get; set; }
        public string GrdSysDetailId { get; set; }
        public int? Cnt { get; set; }
        public int? Hours { get; set; }
        public string EnrollmentId { get; set; }
        public bool? IsIncomplete { get; set; }
        public bool DroppedInAddDrop { get; set; }
        public string ModUser { get; set; }
        public DateTime ModDate { get; set; }
        public bool? IsTransferred { get; set; }
        public bool? IsClinicsSatisfied { get; set; }
        public DateTime? DateDetermined { get; set; }
        public bool IsCourseCompleted { get; set; }
        public bool IsGradeOverriden { get; set; }
        public string GradeOverridenBy { get; set; }
        public DateTime? GradeOverridenDate { get; set; }

    }
}
