﻿using System;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Hold information from a record in the table
    /// </summary>
    public class TransferRecordInfo
    {
        /// <summary>
        /// Record where we take the information
        /// </summary>
        public string TransferId { get; set; }
        public string EnrollmentId { get; set; }
        public string ReqId { get; set; }
        public string GrdSysDetailId { get; set; }
        public string TermId { get; set; }
        public DateTime  ModDate { get; set; }
        public string ModUser { get; set; }
        public decimal? Score { get; set; }
        public bool IsTransferred { get; set; }
        public bool IsClinicsSatisfied { get; set; }
        public bool IsCourseCompleted { get; set; }
        public bool IsGradeOverriden { get; set; }
        public string GradeOverridenBy { get; set; }
        public DateTime GradeOverridenDate { get; set; }
    }
}
