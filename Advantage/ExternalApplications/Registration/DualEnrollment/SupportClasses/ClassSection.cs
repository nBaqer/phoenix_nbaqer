﻿namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Hold the class Id and the Requirement Id
    /// </summary>
    public class ClassSection
    {
        /// <summary>
        /// The class Section Id
        /// </summary>
        public string ClassId { get; set; }

        /// <summary>
        /// The Requirement associated wit the class
        /// </summary>
        public string ReqId { get; set; }
    }
}
