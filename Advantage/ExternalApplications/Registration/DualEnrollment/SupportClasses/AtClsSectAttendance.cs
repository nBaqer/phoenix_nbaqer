﻿using System;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Store a record of the table AtClsSectAtendance
    /// </summary>
    [Serializable] 
    public class AtClsSectAttendance
    {
        public string ClsSectAttId { get; set; }
        public string ClsSectionId { get; set; }
        public string ClsSectMeetingId { get; set; }
        public DateTime MeetDate { get; set; }
        public decimal Actual { get; set; }
        public bool Tardy { get; set; }
        public bool Excused { get; set; }
        public string Comments { get; set; }
        public string StuEnrollId { get; set; }
        public decimal? Scheduled { get; set; }
    }

}
