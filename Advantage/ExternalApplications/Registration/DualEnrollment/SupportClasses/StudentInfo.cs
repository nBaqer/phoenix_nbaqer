﻿using System;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Object student
    /// </summary>
    public class StudentInfo
    {
        /// <summary>
        /// Student Id
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// Full name student
        /// </summary>
        public string FullName { get; set; }

        public string ClassId { get; set; }

        public string ReqId { get; set; }

        public string ClassSection { get; set; }
   
        public string RequirementCode { get; set; }

        public string RequirementDescription  { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string ModUser { get; set; }

        /// <summary>
        /// Type of problem 1 to 8
        /// </summary>
        public int Classification { get; set; }


    }
}
