﻿namespace DualEnrollment.SupportClasses
{
    public class RegisterCoRequirement
    {
        public RegisterCoRequirement(string studentid, string classId, string stuEnrollmentId)
        {
            StuEnrollmentId = stuEnrollmentId;
            ClassId = classId;
            StudentId = studentid;
        }

        public string StudentId { get; set; }
        public string ClassId { get; set; }
        public string StuEnrollmentId { get; set; }

    }


}
