﻿using System.Collections.Generic;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Comparer for StudentInfo Student id and reqId equals
    /// </summary>
    public class DistinctStudentIdReqIdComparer : IEqualityComparer<StudentInfo>
    {

        public bool Equals(StudentInfo x, StudentInfo y)
        {
            return x.StudentId == y.StudentId & x.ReqId == y.ReqId;
        }

        public int GetHashCode(StudentInfo obj)
        {
            return obj.StudentId.GetHashCode() ^ obj.ReqId.GetHashCode();
        }
    }
}
