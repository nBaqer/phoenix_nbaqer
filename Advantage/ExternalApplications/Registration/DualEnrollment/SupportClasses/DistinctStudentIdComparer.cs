﻿using System.Collections.Generic;

namespace DualEnrollment.SupportClasses
{
    /// <summary>
    /// Comparer for Student Info. Only compare StudentId
    /// </summary>
    public class DistinctStudentIdComparer : IEqualityComparer<StudentInfo>
    {

        public bool Equals(StudentInfo x, StudentInfo y)
        {
            return x.StudentId == y.StudentId;
        }

        public int GetHashCode(StudentInfo obj)
        {
            return obj.StudentId.GetHashCode();
        }
    }
}
