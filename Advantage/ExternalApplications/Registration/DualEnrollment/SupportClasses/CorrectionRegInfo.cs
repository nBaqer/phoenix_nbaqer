﻿using System.Collections.Generic;

namespace DualEnrollment.SupportClasses
{
  
    /// <summary>
    /// Class to register the information of one student
    /// This register, grade components, grade final and Attendance
    /// </summary>
    public class CorrectionRegInfo : ICorrectionRegInfo
    {
        /// <summary>
        /// Create a instance of CorrectionRegInfo
        /// </summary>
        /// <returns></returns>
        public static ICorrectionRegInfo Factory()
        {
            ICorrectionRegInfo reg = new CorrectionRegInfo();
            reg.ArInfoList = new List<ArResultsClass>();
            reg.AttendanceList = new List<AtClsSectAttendance>();
            reg.GradeBookList = new List<GradeBookResultInfo>();
            return reg;
        }
       
        /// <summary>
        /// The record in ArResults
        /// </summary>
        public IList<ArResultsClass> ArInfoList { get; set; }

        /// <summary>
        /// The records in Grade-book.
        /// </summary>
        public IList<GradeBookResultInfo> GradeBookList { get; set; }

        /// <summary>
        /// The attendance Records
        /// </summary>
        public IList<AtClsSectAttendance> AttendanceList { get; set; }

        /// <summary>
        /// This field is only used to store the enrollments referred to this class
        /// </summary>
        public string Enrollment { get; set; }

    } 
   
}
