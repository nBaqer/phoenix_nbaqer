﻿using System.Collections.Generic;
using DualEnrollment.Db;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Facade
{
    public class LoggerFacade
    {
        public static void WriteInfo(StudentInfo info)
        {
            var db = new LoggerServices();
            db.WriteInfo(info);

        }

        public static void TruncateLoggerTable()
        {
            var db = new LoggerServices();
            db.TruncateLoggerTable();
        }

        public static void WriteListOfLogInfo(IList<StudentInfo> needRegistrationList, int classification)
        {
            var db = new LoggerServices();
            db.WriteListOfLogInfo(needRegistrationList, classification);
        }
    }
}
