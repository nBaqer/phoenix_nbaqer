﻿using System.Collections.Generic;
using DualEnrollment.Db;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Facade
{
    public static class ModificationFacade
    {
        #region Insertions 

        /// <summary>
        /// Insert in ArTransferGrades
        /// </summary>
        /// <param name="insertionTransferList"></param>
        public static void InsertAllTransferRecordTransactional(IList<TransferRecordInfo> insertionTransferList)
        {
            var db = new ModificationServices();
            db.InsertAllTransferRecordTransactional(insertionTransferList);
        }

        #endregion
    }
}
