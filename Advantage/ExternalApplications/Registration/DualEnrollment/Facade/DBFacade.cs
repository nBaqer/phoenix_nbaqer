﻿using System.Collections.Generic;
using DualEnrollment.Db;
using DualEnrollment.SupportClasses;

namespace DualEnrollment.Facade
{
    public static class DbFacade
    {
        /// <summary>
        /// Get all student with two enrollments actives with possibility to accept two shared courses
        /// </summary>
        /// <returns>A list with all student Id</returns>
        public static IList<string> GetAllStudentWithTwoEnrollmentsCapableToShareClass()
        {
            var db = new DbServices();
            var list = db.GetAllStudentWithTwoEnrollmentsCapableToShareClass();
            return list;
        }

        /// <summary>
        /// Get all class section in the schools
        /// </summary>
        /// <returns>A list with all class section Id</returns>
        public static IList<string> GetAllClassesInSchool()
        {
            var db = new DbServices();
            var list = db.GetAllClassesInSchool();
            return list;
        }

        public static IList<string> GetAllRequirementInSchool()
        {
            var db = new DbServices();
            var list = db.GetAllRequirementInSchool();
            return list;
        }

        /// <summary>
        /// Get all class section in the schools
        /// </summary>
        /// <returns>The list of class id with his ReqId associate</returns>
        public static IList<ClassSection> GetAllClassesWithRequirementInSchool()
        {
            var db = new DbServices();
            var list = db.GetAllClassesWithRequirementInSchool();
            return list;
        }


        public static IList<string> GetActiveEnrollmentsWithClassAssociatedForRegisterClass(string studentId, string classId)
        {
            var db = new DbServices();
            IList<string> result = db.GetActiveEnrollmentsWithClassAssociatedForRegisterClass(studentId, classId);
            return result;
        }

        public static IList<string> GetActiveEnrollmentsWithRegisteredRequerimentId(string studentId, string reqid)
        {
            var db = new DbServices();
            IList<string> result = db.GetActiveEnrollmentsWithRegisteredRequerimentId(studentId, reqid);
            return result;
        }

        /// <summary>
        /// Check if the enrollment is register or not
        /// </summary>
        /// <param name="stuEnrollId">The enrollment</param>
        /// <param name="classId">The class</param>
        /// <returns>True if it is register</returns>
        public static bool CheckIfEnrollmentIsAlreadyRegister(string stuEnrollId, string classId)
        {
            var db = new DbServices();
            bool result = db.CheckIfEnrollmentIsAlreadyRegister(stuEnrollId, classId);
            return result;
        }

        public static bool CheckIfEnrollmentIsAlreadyRegisterReq(string stuEnrollId, string reqId)
        {
            var db = new DbServices();
            bool result = db.CheckIfEnrollmentIsAlreadyRegisterReq(stuEnrollId, reqId);
            return result;
        }

        public static bool CheckIfCourseIsTransferred(string stuEnrollId, string reqId)
        {
            var db = new DbServices();
            bool result = db.CheckIfCourseIsTransferred(stuEnrollId, reqId);
            return result;
        }

        /// <summary>
        /// Get the record if exists in ArTransferGrade for a determinate ReqId and Enrollment.
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="reqId"></param>
        /// <returns>If not exists return a empty record</returns>
        public static TransferRecordInfo GetRequirementTransferredRecord(string stuEnrollId, string reqId)
        {
            var db = new DbServices();
            var result = db.GetRequirementTransferredRecord(stuEnrollId, reqId);
            return result;
        }

        public static void RegisterClassInEnrollmentsOfStudents(List<RegistrationClassInfo> validatedEnrollmentClassRegistrationList)
        {
            throw new System.NotImplementedException();
        }

        public static string GetInformationString(string student, string classid)
        {
            var db = new DbServices();
            string result = db.GetInformationString(student, classid);
            return result;
        }

        /// <summary>
        /// return a class with the info of the student and the class
        /// </summary>
        /// <param name="student"></param>
        /// <param name="classid"></param>
        /// <returns></returns>
        public static StudentInfo GetStudentInfo(string student, string classid)
        {
            var db = new DbServices();
            var si = db.GetStudentInfo(student, classid);
            return si;
        }

        public static StudentInfo GetStudentInfoReq(string studentId, string reqId)
        {
            var db = new DbServices();
            var si = db.GetStudentInfoReq(studentId, reqId);
            return si;
        }

        #region Process of correction of Academic Information

        public static IList<ArResultsClass> GetRegisterInformation(string stuEnrollId, string reqId)
        {
            var db = new DbServices();
            var result = db.GetRegisterInformation(stuEnrollId, reqId);
            return result;
        }

        public static ICorrectionRegInfo GetAllAcademicInformation(ICorrectionRegInfo registeredEnrollment)
        {
            var db = new DbServices();
            ICorrectionRegInfo info = db.GetAllAcademicInformation(registeredEnrollment);
            return info;
        }

        /// <summary>
        /// Return if the enrollment has not academic information inserted.
        /// it is has returned true if not return true.
        /// </summary>
        /// <param name="enrollId">the enroll id</param>
        /// <param name="reqId">the requirement id</param>
        /// <returns>String empty if the information does not exists. 
        /// if exists return the name of the tables that contain information,
        ///  separate by spaces
        /// </returns>
        public static string HasTheEnrollmentAnyAcademicRecord(string enrollId, string reqId)
        {
            var db = new DbServices();
            string res = db.HasTheEnrollmentAnyAcademicRecord(enrollId, reqId);
            return res;
        }


        public static void InsertAllcademicInformationInDataBase(IList<ICorrectionRegInfo> insertionRegisterList)
        {
            var db = new DbServices();
            db.InsertAllcademicInformationInDataBase(insertionRegisterList);
        }
        #endregion

        public static StudentInfo GetClassInfoRegistered(StudentInfo studentInfo)
        {
            var db = new DbServices();
            var stuin =  db.GetClassInfoRegistered(studentInfo);
            return stuin;
        }

        /// <summary>
        /// Delete spurious information if exists.
        /// </summary>
        /// <param name="enrollmentId"></param>
        /// <param name="reqId"></param>
        /// <param name="tablesWithAcademicInfo"></param>
        public static void DeleteSpuriousAcademicInfo(string enrollmentId, string reqId, string tablesWithAcademicInfo)
        {
            var db = new DbServices();
            if (tablesWithAcademicInfo.Contains("atClsSectAttendance"))
            {
                db.DeleteAttendanceInfo(enrollmentId, reqId);
            }

            if (tablesWithAcademicInfo.Contains("arGrdBkResults"))
            {
                db.DeleteAcademicInfo(enrollmentId, reqId);
            }

        }
    }
}
