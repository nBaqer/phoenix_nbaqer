﻿using System;

namespace DualEnrollment
{
    public static class InitDualEnrollment
    {
        /// <summary>
        /// Init this before begin to work with the DLL
        /// </summary>
        public static string ConnectionString { get; set; }

    }
}
