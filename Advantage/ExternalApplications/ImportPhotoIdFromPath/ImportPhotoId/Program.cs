﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="FAME">
//   2016
// </copyright>
// <summary>
// JAGG  Import Lead in table
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ImportPhotoId
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// The program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The truncate table.
        /// </summary>
        private static bool truncateTable;

        /// <summary>
        /// The photo path.
        /// </summary>
        private static string photoPath;

        /// <summary>
        /// The filename.
        /// </summary>
        private static string filename;

        /// <summary>
        /// The main.
        /// </summary>
        /// <param name="args">
        /// clean = Delete all images in table LeadImage
        /// </param>
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("------------------------------------------------------");
                Console.WriteLine(" Import Photograph from Folder to Table from JAGG :-) ");
                Console.WriteLine("------------------------------------------------------");
                Program.truncateTable = args.Length != 0 && args[0] == "clean";

                // Get the configuration settings
                var db = new AdvantageDataContext();
                var setting = db.syConfigAppSettings.SingleOrDefault(x => x.KeyName == "CreateDocumentPath");
                int? settingId = setting?.SettingId ?? 0;
                if (settingId != 0)
                {
                    Program.photoPath = db.syConfigAppSetValues.Single(x => x.SettingId == settingId).Value;
                    Console.WriteLine($"Photos Located at {Program.photoPath}");
                }

                if (string.IsNullOrWhiteSpace(Program.photoPath))
                {
                    throw new ApplicationException("Configuration Setting for Image Photo not found");
                }

                // Delete the information from LeadImage if clean was selected
                if (Program.truncateTable)
                {
                    db.ExecuteCommand("TRUNCATE TABLE [doc].[LeadImage]");
                    Console.WriteLine("LeadImage table cleaned.. All record deleted");
                }

                // Get all Photos information from Table Historical 
                var photosList = new List<Dto>();
                var infoFromTable = db.syDocumentHistories.Where(x => x.DocumentType == "PhotoId" && x.FileExtension != ".pdf");
                Console.WriteLine("Reading the image table. be Patient...........");
                int studentPhoto = 0;
                int leadPhoto = 0;
                var stu = 0;
                foreach (syDocumentHistory doc in infoFromTable)
                {
                    stu++;
                    Console.WriteLine("Student {0}", stu);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    if (doc.LeadId != null)
                    {
                        // Get lead Photo
                        var dto = new Dto
                        {
                            LeadId = doc.LeadId.GetValueOrDefault(),
                            AlternativeFullFileName = $"{doc.FileId}{doc.FileExtension}",
                            FullFileName = string.Format("{0}.{1}", doc.FileName, doc.FileExtension),
                            FileType = doc.FileExtension,
                            ModDate = doc.ModDate.GetValueOrDefault(DateTime.Now),
                            ModUser = doc.ModUser
                        };
                        photosList.Add(dto);
                        leadPhoto++;
                    }
                    else
                    {
                        // Get student Photo
                        var shadowLead = db.adLeads.SingleOrDefault(x => x.StudentId == doc.StudentId);
                        if (shadowLead == null)
                        {
                            Console.WriteLine();
                            Console.WriteLine(
                                "Student {0} has not lead equivalence. Photo is not migrate",
                                doc.StudentId);
                        }
                        else
                        {
                            var dto = new Dto
                            {
                                LeadId = shadowLead.LeadId,
                                AlternativeFullFileName = $"{doc.FileId}{doc.FileExtension}",
                                FullFileName = $"{doc.FileName}{doc.FileExtension}",
                                FileType = doc.FileExtension,
                                ModDate = doc.ModDate.GetValueOrDefault(DateTime.Now),
                                ModUser = doc.ModUser
                            };
                            photosList.Add(dto);
                            studentPhoto++;
                        }
                    }
                }

                Console.WriteLine();
                
                // Get information from path and store in database...............................................
                // See if path exists
                var completePath = Program.photoPath + "PhotoId";
                if (Directory.Exists(completePath) == false)
                {
                    throw new ApplicationException("The path to photo does not exists. ergo, there are not photo to migrate");
                }

                int numberOfImageReportedButNotFound = 0;
                int numberOfAlreadyExists = 0;
                int numberOfPhotoWithBadFormat = 0;

                // Path Exists.
                stu = 0;
               
                Console.WriteLine("Inserting in LeadImage records. be patient......");
                var tempList = new List<Dto>();
                foreach (Dto dto in photosList)
                {
                    Program.filename = dto.FullFileName;
                    stu++;
                    Console.WriteLine("Student {0}", stu);
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    
                    // Check if exists a image associated with the student in the table
                    var exists = tempList.Any(x => x.LeadId == dto.LeadId);
                    exists |= db.LeadImages.Any(x => x.LeadId == dto.LeadId && x.TypeId == 1);
                    if (exists)
                    {
                        // Image exist log it and continue to next
                        ////Console.WriteLine("Image already exists for Lead Id {0}", dto.LeadId);
                        numberOfAlreadyExists++;
                        continue;
                    }

                    // Image does not exists ask if the file exists
                    var fullAddress = $"{completePath}\\{dto.FullFileName}";
                    var alternativeAddress = $"{completePath}\\{dto.AlternativeFullFileName}";
                    byte[] photo = File.Exists(fullAddress) ? File.ReadAllBytes(fullAddress) : null;
                    ////Console.WriteLine($"Processing photo {fullAddress} - {alternativeAddress}");
                    if (photo == null)
                    {
                       //// Console.WriteLine("Photo is null ");
                        if (File.Exists(alternativeAddress))
                        {
                            FileInfo info = new FileInfo(alternativeAddress);
                            if (info.Length > 10)
                            {
                                photo = File.ReadAllBytes(alternativeAddress);
                            }
                            else
                            {
                                numberOfPhotoWithBadFormat++;
                                continue;
                            }
                        }
                    }
                  
                    if (photo != null)
                    {
                        // File Exists get photo
                        // photo = File.ReadAllBytes(fullAddress);
                        ////Console.WriteLine("Photo is not null, it was null now is alternative ");
                        
                        // Get type from image
                        Image img = null;
                        using (MemoryStream stream = new MemoryStream(photo))
                        {
                            img = Image.FromStream(stream);
                        }

                        var encoderType = ImageFormat.Jpeg.Equals(img.RawFormat) ? "image/jpeg" : "none";
                        encoderType = ImageFormat.Png.Equals(img.RawFormat) ? "image/png" : encoderType;

                        if (encoderType == "none")
                        {
                            numberOfPhotoWithBadFormat++;
                            continue;
                        }

                       // // Check if the Photo is png or jpg. If not does not process it!
                       //// var encoderType = "image/png";
                       // //if (dto.FileType.ToLower() == ".jpg")
                       // //{
                       // //    encoderType = "image/jpeg";
                       // //}

                       // //if (dto.FileType.ToLower() != ".jpg" && dto.FileType.ToLower() != ".png")
                       // //{
                       // //    ////Console.WriteLine("Photo format {0} can not be processed and will be omitted");
                       // //    numberOfPhotoWithBadFormat++;
                       // //    continue;
                       // //}

                        // Test if the image if bigger that 20kb if it thumbnail it.
                        if (photo.Length > 20480)
                        {
                            ////Console.WriteLine($"Create ThumbNail, encoder type {encoderType}");
                            photo = ProcessImage.CreateThumbNail(photo, encoderType);
                            ////Console.WriteLine("ThumbNail created..");

                        }

                        // Limit to 49 characters
                        if (dto.FullFileName.Length > 49)
                        {
                            dto.FullFileName = dto.FullFileName.Substring(0, 48);
                        }

                        // Send photo to table LeadImage
                        var imageLead = new LeadImage
                        {
                            LeadId = dto.LeadId,
                            TypeId = 1,
                            Image = photo,
                            ImgSize = photo.Length,
                            MediaType = encoderType,
                            ModDate = dto.ModDate,
                            ModUser = dto.ModUser,
                            ImgFile = Path.GetFileNameWithoutExtension(dto.FullFileName)
                        };
                        ////Console.WriteLine("Previous to insert");
                        db.LeadImages.InsertOnSubmit(imageLead);
                        tempList.Add(dto);
                    }
                    else
                    {
                        ////Console.WriteLine("Image of Lead {0} not found in directory. Image is not processed");
                        numberOfImageReportedButNotFound++;
                    }
                }

                // Commit changes to Database
                db.SubmitChanges();
                Console.WriteLine();
                var notProcessed = numberOfPhotoWithBadFormat + numberOfImageReportedButNotFound + numberOfAlreadyExists;
                var total = photosList.Count - notProcessed;
                Console.WriteLine("Process Finished!............................");
                Console.WriteLine("Database Scan Report!............................");
                Console.WriteLine(" - {0} records of images from Lead was found", leadPhoto);
                Console.WriteLine(" - {0} records of images from Student was found", studentPhoto);
                Console.WriteLine();
                Console.WriteLine("Copy from Folder to Database Report!............................");
                Console.WriteLine(" - {0} records of images from {1} were migrated", total, photosList.Count);
                Console.WriteLine(" - {0} records was not processed because a bad format", numberOfPhotoWithBadFormat);
                Console.WriteLine(" - {0} records was reported in the database but not found in folder", numberOfImageReportedButNotFound);
                Console.WriteLine(" - {0} records already exists in the database", numberOfAlreadyExists);
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine();
                var message = ex.Message + Environment.NewLine;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    message += ex.Message;
                    message += Environment.NewLine;
                }

                Console.WriteLine("A Exception was detected. Program abort. Exception message: {0} on {1}", message, Program.filename);
            }

            Console.ReadLine();
        }
    }
}
