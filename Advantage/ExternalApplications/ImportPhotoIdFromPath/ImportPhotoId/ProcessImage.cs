﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProcessImage.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//  JAGG - Defines the ProcessImage type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ImportPhotoId
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;

    /// <summary>
    /// The process image.
    /// </summary>
    internal static class ProcessImage
    {
        /// <summary>
        ///  The create thumb nail.
        /// </summary>
        /// <param name="imageArray">
        ///  The image array.
        /// </param>
        /// <param name="encoderType">
        ///  The encoder type.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        internal static byte[] CreateThumbNail(byte[] imageArray, string encoderType)
        {
            Image image;
            using (MemoryStream ms = new MemoryStream(imageArray))
            {
                image = Image.FromStream(ms);
            }

            var output = ProcessImage.CreateThumbnailBase(image, encoderType);
            return output;
        }

        /// <summary>
        ///  The create thumbnail base.
        /// </summary>
        /// <param name="image">
        ///  The image.
        /// </param>
        /// <param name="encoderType">
        ///  The encoder type.
        /// </param>
        /// <returns>
        /// The image in bytes
        /// </returns>
        private static byte[] CreateThumbnailBase(Image image, string encoderType)
        {
            // Create the thumbnail
            Image.GetThumbnailImageAbort myCallback = ProcessImage.ThumbnailCallback;
           //// Console.WriteLine("After GetThumbnailImageAbort");
            Image thumb = ResizeImage(image, new Size(100, 100));

             ////  Image thumb = image.GetThumbnailImage(100, 100, myCallback, IntPtr.Zero);
            ////Console.WriteLine("After GetThumbnailImage");

            var encoder = ImageFormat.Png;

            if (encoderType == "image/jpeg")
            {
                encoder = ImageFormat.Jpeg;
            }

            var output = ProcessImage.ImageToBytesArray(thumb, encoder);
           //// Console.WriteLine("After ImageToBytesArray");

            return output;
        }

        /// <summary>
        /// The resize image.
        /// </summary>
        /// <param name="imgToResize">
        /// The image to resize.
        /// </param>
        /// <param name="size">
        /// The size.
        /// </param>
        /// <returns>
        /// The <see cref="Image"/>.
        /// </returns>
        public static Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        /// <summary>
        /// The thumbnail callback.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool ThumbnailCallback()
        {
            return false;
        }

        /// <summary>
        /// The image to bytes array.
        /// </summary>
        /// <param name="image">
        /// The image.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <returns>
        /// The array
        /// </returns>
        private static byte[] ImageToBytesArray(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();
                return imageBytes;
            }
        }
    }
}
