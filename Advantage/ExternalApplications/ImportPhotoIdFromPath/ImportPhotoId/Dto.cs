﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Dto.cs" company="FAME">
//   2016
// </copyright>
// <summary>
//  JAGG Defines the DTO type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ImportPhotoId
{
    using System;

    /// <summary>
    ///  The DTO.
    /// </summary>
    internal class Dto
    {
        /// <summary>
        /// Gets or sets the Alternative Full File Name.
        /// File name can be also the name of the file stored
        /// </summary>
        public string AlternativeFullFileName { get; set; }
        
        /// <summary>
        /// Gets or sets the full file name.
        /// some time is this and some time is FileId
        /// </summary>
        public string FullFileName { get; set; }

        /// <summary>
        /// Gets or sets the File Type.
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the modification date.
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets the modification user.
        /// </summary>
        public string ModUser { get; set; }
    }
}
