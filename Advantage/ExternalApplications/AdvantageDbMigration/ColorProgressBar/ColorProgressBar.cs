﻿using System;
    2  using System.ComponentModel;
    3  using System.ComponentModel.Design;
    4  using System.Windows.Forms;
    5  using System.Drawing;
    6  using System.Drawing.Drawing2D;
    7  
    8  namespace ColorProgressBar
9  {
10  	[Description("Color Progress Bar")]
11  	[ToolboxBitmap(typeof(ProgressBar))]
12  	[Designer(typeof(ColorProgressBarDesigner))]
13  	public class ColorProgressBar : System.Windows.Forms.Control
    14  	{	
    15  	
    16  		//
    17  		// set default values
    18  		//
    19  		private int _Value = 0;
20  		private int _Minimum = 0;
21  		private int _Maximum = 100;
22  		private int _Step = 10;
23  		
24  		private FillStyles _FillStyle = FillStyles.Solid;
25  
26  		private Color _BarColor = Color.FromArgb(255, 128, 128);
27  		private Color _BorderColor = Color.Black;
28  
29  		public enum FillStyles
30  		{
31  			Solid,
32  			Dashed
33  		}
34  
35  		public ColorProgressBar()
36  		{
37  			base.Size = new Size(150, 15);
38  			SetStyle(
39  				ControlStyles.AllPaintingInWmPaint |
40  				ControlStyles.ResizeRedraw |
41  				ControlStyles.DoubleBuffer,
42  				true
43  				);
44  		}
45  
46  		[Description("ColorProgressBar color")]
47  		[Category("ColorProgressBar")]
48  		public Color BarColor
49  		{
50  			get
51  			{
52  				return _BarColor;
53  			}
54  			set
55  			{
56  				_BarColor = value;
57  				this.Invalidate();
58  			}
59  		}
60  
61  		[Description("ColorProgressBar fill style")]
62  		[Category("ColorProgressBar")]
63  		public FillStyles FillStyle
64  		{
65  			get
66  			{
67  				return _FillStyle;
68  			}
69  			set
70  			{
71  				_FillStyle = value;
72  				this.Invalidate();
73  			}
74  		}
75  
76  		[Description("The current value for the ColorProgressBar, " +
77              "in the range specified by the Minimum and Maximum properties.")]
78  		[Category("ColorProgressBar")]
79  		// the rest of the Properties windows must be updated when this peroperty is changed.
80  		[RefreshProperties(RefreshProperties.All)]
81  		public int Value
82  		{
83  			get
84  			{
85  				return _Value;
86  			}
87  			set
88  			{
89  				if (value<_Minimum)
90  				{
91  					throw new ArgumentException("'"+value+"' is not a valid value for 'Value'.\n"+
92  						"'Value' must be between 'Minimum' and 'Maximum'.");
93  				}
94  
95  				if (value > _Maximum)
96  				{
97  					throw new ArgumentException("'"+value+"' is not a valid value for 'Value'.\n"+
98  						"'Value' must be between 'Minimum' and 'Maximum'.");
99  				}
100  
101  				_Value = value;			
102  				this.Invalidate();
103  			}
104  		}
105  		
106  		[Description("The lower bound of the range this ColorProgressbar is working with.")]
107  		[Category("ColorProgressBar")]
108  		[RefreshProperties(RefreshProperties.All)]
109  		public int Minimum
110  		{
111  			get
112  			{
113  				return _Minimum;
114  			}
115  			set
116  			{
117  				_Minimum = value;
118  
119  				if (_Minimum > _Maximum)
120  					_Maximum = _Minimum;
121  				if (_Minimum > _Value)
122  					_Value = _Minimum;
123  
124  				this.Invalidate();
125  			}
126  		}
127  
128  		[Description("The uppper bound of the range this ColorProgressbar is working with.")]
129  		[Category("ColorProgressBar")]
130  		[RefreshProperties(RefreshProperties.All)]
131  		public int Maximum
132  		{
133  			get
134  			{
135  				return _Maximum;
136  			}
137  			set
138  			{
139  				_Maximum = value;
140  
141  				if (_Maximum<_Value)
142  					_Value = _Maximum;
143  				if (_Maximum<_Minimum)
144  					_Minimum = _Maximum;
145  
146  				this.Invalidate();
147  			}
148  		}
149  
150  		[Description("The amount to jump the current value of the control by when the Step() method is called.")]
151  		[Category("ColorProgressBar")]		
152  		public int Step
153  		{
154  			get
155  			{
156  				return _Step;
157  			}
158  			set
159  			{
160  				_Step = value;
161  				this.Invalidate();
162  			}
163  		}
164  
165  		[Description("The border color of ColorProgressBar")]
166  		[Category("ColorProgressBar")]		
167  		public Color BorderColor
168  		{
169  			get
170  			{
171  				return _BorderColor;
172  			}
173  			set
174  			{
175  				_BorderColor = value;
176  				this.Invalidate();
177  			}
178  		}
179  		
180  		//
181  		// Call the PerformStep() method to increase the value displayed by the amount set in the Step property
182  		//
183  		public void PerformStep()
184  		{
185  			if (_Value<_Maximum)
186  				_Value += _Step;
187  			else
188  				_Value = _Maximum;
189  
190  			this.Invalidate();
191  		}
192  		
193  		//
194  		// Call the PerformStepBack() method to decrease the value displayed by the amount set in the Step property
195  		//
196  		public void PerformStepBack()
197  		{
198  			if (_Value > _Minimum)
199  				_Value -= _Step;
200  			else
201  				_Value = _Minimum;
202  
203  			this.Invalidate();
204  		}
205  
206  		//
207  		// Call the Increment() method to increase the value displayed by an integer you specify
208  		// 
209  		public void Increment(int value)
210  		{
211  			if (_Value<_Maximum)
212  				_Value += value;
213  			else
214  				_Value = _Maximum;
215  
216  			this.Invalidate();
217  		}
218  		
219  		//
220  		// Call the Decrement() method to decrease the value displayed by an integer you specify
221  		// 
222  		public void Decrement(int value)
223  		{
224  			if (_Value > _Minimum)
225  				_Value -= value;
226  			else
227  				_Value = _Minimum;
228  
229  			this.Invalidate();
230  		}
231  		
232  		protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
233  		{
234  			//
235  			// Calculate matching colors
236  			//
237  			Color darkColor = ControlPaint.Dark(_BarColor);
238  			Color bgColor = ControlPaint.Dark(_BarColor);
239  
240  			//
241  			// Fill background
242  			//
243  			SolidBrush bgBrush = new SolidBrush(bgColor);
244  			e.Graphics.FillRectangle(bgBrush, this.ClientRectangle);
245  			bgBrush.Dispose();
246  			
247  			// 
248  			// Check for value
249  			//
250  			if (_Maximum == _Minimum || _Value == 0)
251  			{
252  				// Draw border only and exit;
253                 drawBorder(e.Graphics);
254                 return;
255             }
256  
257  			//
258  			// The following is the width of the bar. This will vary with each value.
259  			//
260  			int fillWidth = (this.Width * _Value) / (_Maximum - _Minimum);
261  			
262  			//
263  			// GDI+ doesn't like rectangles 0px wide or high
264  			//
265  			if (fillWidth == 0)
266  			{
267  				// Draw border only and exti;
268                 drawBorder(e.Graphics);
269                 return;
270             }
271  
272  			//
273  			// Rectangles for upper and lower half of bar
274  			//
275  			Rectangle topRect = new Rectangle(0, 0, fillWidth, this.Height / 2);
276  			Rectangle buttomRect = new Rectangle(0, this.Height / 2, fillWidth, this.Height / 2);
277  
278  			//
279  			// The gradient brush
280  			//
281  			LinearGradientBrush brush;
282  
283  			//
284  			// Paint upper half
285  			//
286  			brush = new LinearGradientBrush(new Point(0, 0),
287  				new Point(0, this.Height / 2), darkColor, _BarColor);
288  			e.Graphics.FillRectangle(brush, topRect);
289  			brush.Dispose();
290  
291  			//
292  			// Paint lower half
293  			// (this.Height/2 - 1 because there would be a dark line in the middle of the bar)
294  			//
295  			brush = new LinearGradientBrush(new Point(0, this.Height / 2 - 1),
296  				new Point(0, this.Height), _BarColor, darkColor);
297  			e.Graphics.FillRectangle(brush, buttomRect);
298  			brush.Dispose();
299  
300  			//
301  			// Calculate separator's setting
302  			//
303  			int sepWidth = (int)(this.Height * .67);
304  			int sepCount = (int)(fillWidth / sepWidth);
305  			Color sepColor = ControlPaint.LightLight(_BarColor);
306  
307  			//
308  			// Paint separators
309  			//
310  			switch (_FillStyle)
311  			{
312  				case FillStyles.Dashed:
313  					// Draw each separator line
314  					for (int i = 1; i <= sepCount; i++)
315  					{
316  						e.Graphics.DrawLine(new Pen(sepColor, 1),
317  							sepWidth* i, 0, sepWidth* i, this.Height);
318  					}
319  					break;
320  
321  				case FillStyles.Solid:
322  					// Draw nothing
323  					break;
324  
325  				default:
326  					break;
327  			}
328  
329  			//
330  			// Draw border and exit
331  			//
332  			drawBorder(e.Graphics);
333  		}
334  
335  		//
336  		// Draw border
337  		//
338  		protected void drawBorder(Graphics g)
339  		{
340  			Rectangle borderRect = new Rectangle(0, 0,
341                 ClientRectangle.Width - 1, ClientRectangle.Height - 1);
342  			g.DrawRectangle(new Pen(_BorderColor, 1), borderRect);
343  		}
344  	}
345  }
