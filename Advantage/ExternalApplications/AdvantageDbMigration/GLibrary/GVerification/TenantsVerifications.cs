﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantsVerifications.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The tenants verifications.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GVerification
{
    using System;
    using System.Data.Common;

    using GLibrary.GTypes;

    /// <summary>
    /// The tenants verifications.
    /// </summary>
    public static class TenantsVerifications
    {
        /// <summary>
        /// The verify tenant started configuration.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string VerifyTenantStartedConfiguration(ITenants tenant)
        {
            // Check all controls with value
            if (string.IsNullOrWhiteSpace(tenant.ConfigurationSettingsObject.SchoolName))
            {
                return @"School name can not be null, empty or white space";
            }

            if (string.IsNullOrWhiteSpace(tenant.ConfigurationSettingsObject.ReportServerVirtualBaseAddress) | string.IsNullOrWhiteSpace(tenant.ConfigurationSettingsObject.ReportServerPort))
            {
                return @"Report Server information can not be null, empty or white space";
            }

            if (string.IsNullOrWhiteSpace(tenant.ConfigurationSettingsObject.AdvantageVirtualBaseAddress) | string.IsNullOrWhiteSpace(tenant.ConfigurationSettingsObject.Protocol))
            {
                return @"Advantage Virtual Path information can not be null, empty or white space";
            }

            try
            {
                // Verify Connection string TenantDb (only the server existence and syntax)
                DbConnectionStringBuilder csb = new DbConnectionStringBuilder { ConnectionString = tenant.TenantDataBaseConnectionString };

                // Verify now Initial Catalog must be empty or set to TenantAuthDB
                if (csb.ContainsKey("Initial Catalog") == false || csb["Initial Catalog"].ToString().ToLower() != "tenantauthdb")
                {
                    return @"Initial Catalog in Tenant Connection string must be present and must point to TenantAuthDB";
                }

                // Verify Connection string Advantage (only the server existence and syntax)
                csb.ConnectionString = tenant.AdvantageDataBaseConnectionString;

                // Verify now Initial Catalog must be empty or set to Advantage
                if (csb.ContainsKey("Initial Catalog") && csb["Initial Catalog"].ToString().ToLower() != "advantage")
                {
                    return @"Initial Catalog in Advantage Connection string must NOT be present or must point to Advantage";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return e.Message;
            }

            return string.Empty;
        }
    }
}
