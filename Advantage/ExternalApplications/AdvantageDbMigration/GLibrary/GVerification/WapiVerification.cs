﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiVerification.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassVerificationDb type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GVerification
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;

    using GLibrary.Facade;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;
    using GLibrary.GTypes.VerificationType;
    using GLibrary.GTypes.WapiVerification;

    /// <summary>
    /// The <code>KlassApp</code> verification DB.
    /// </summary>
    public class WapiVerification
    {
        /// <summary>
        /// The tenant.
        /// </summary>
        private readonly ITenants tenant;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiVerification"/> class.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public WapiVerification(ITenants tenant)
        {
            this.tenant = tenant;
        }

        /// <summary>
        /// The check WAPI path allowed services.
        /// </summary>
        /// <param name="servicePath">
        /// The service path.
        /// without the basic path. The basic path must end with slash
        /// </param>
        /// <param name="checkActive">
        /// The check active. if true if checked if the service is active or not
        /// </param>
        /// <param name="result">
        /// The result.
        /// It is empty count = 0 if not error
        /// Reported error can be MISSING, BADURL, INACTIVE
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if no error happens
        /// </returns>
        public bool CheckWapiPathAllowedServices(string servicePath, bool checkActive, out IList<WapiServicesResult> result)
        {
            // Initialize output parameter
            result = new List<WapiServicesResult>();
            bool output = true;

            // Read from database all basic
            IList<WapiAllowedServiceType> list = WapiServicesFacade.GetAllAllowedOperation(this.tenant);

            // Check values
            foreach (WapiAllowedServiceType st in list)
            {
                var res = new WapiServicesResult();
                if (WapiDictionaries.AllowedServicesDictionary.ContainsKey(st.Code) == false)
                {
                    res.Error = "Missing";
                    res.ErrorCode = "MISSING";
                    res.Property = st.Code;
                    res.IsOk = false;
                    result.Add(res);
                    output = false;
                    continue;
                }

                var allowed = WapiDictionaries.AllowedServicesDictionary[st.Code];

                // Verify URL
                if (st.Url != (this.tenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath + allowed))
                {
                    res.Error = "Bad URL";
                    res.ErrorCode = "BADURL";
                    res.Property = st.Code;
                    res.IsOk = false;
                    result.Add(res);
                    output = false;
                    continue;
                }

                // Verify if it is active if proceeded
                if (checkActive)
                {
                    if (st.IsActive == false)
                    {
                        res.Error = "No Active";
                        res.ErrorCode = "INACTIVE";
                        res.Property = st.Code;
                        res.IsOk = false;
                        result.Add(res);
                        output = false;
                    }
                }
            }

            // Fill result
            return output;
        }

        /// <summary>
        /// The get WAPI catalogs.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <param name="apikey">
        /// The API key.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public Tuple<string, string, string> GetWapiCatalogs(WapiAllowedServiceType service, string apikey, string parameters = "")
        {
            string servicesUrl = $"{service.Url}{parameters}";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(servicesUrl),
                Method = HttpMethod.Get,
            };
            var output = new Tuple<string, string, string>(string.Empty, string.Empty, string.Empty);
            request.Headers.Add("AuthKey", apikey);
            var task = client.SendAsync(request).ContinueWith(taskwithmsg =>
                {
                    var response = taskwithmsg.Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                    output = new Tuple<string, string, string>(response.StatusCode.ToString(), response.ReasonPhrase, content);
                });
            task.Wait();

            return output;
        }

        ////public bool CheckWapiWindowsServiceUserForClassApplication(Tenants39 tenants, out IList<WapiServicesResult> resultList)
        ////{
        ////    throw new System.NotImplementedException();
        ////}
    }
}
