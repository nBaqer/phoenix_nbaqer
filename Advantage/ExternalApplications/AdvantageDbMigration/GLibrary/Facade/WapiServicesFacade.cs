﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServicesFacade.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the WapiServicesFacade type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Facade
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Transactions;

    using GLibrary.DataLayer;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;
    using GLibrary.GTypes.WapiVerification;
    using GLibrary.GVerification;

    /// <summary>
    /// The WAPI services facade.
    /// </summary>
    public static class WapiServicesFacade
    {
        /// <summary>
        /// The get all allowed operation.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;WapiAllowedServiceType&gt;"/>.
        /// </returns>
        public static IList<WapiAllowedServiceType> GetAllAllowedOperation(ITenants tenant)
        {
            var db = new WapiServicesDb(tenant);
            var list = db.GetWapiAllowedServices();
            return list;
        }

        /// <summary>
        /// The get all companies code advantage.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ExternalCompanies&gt;"/>.
        /// </returns>
        public static IList<ExternalCompanies> GetAllCompaniesCodeAdvantage(ITenants tenant)
        {
            var db = new WapiServicesDb(tenant);
            var list = db.GetAllCompaniesCodeAdvantage(tenant);
            return list;
        }

        /// <summary>
        /// The get all campus from tenant.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;Campus&gt;"/>.
        /// </returns>
        public static IList<Campus> GetAllCampusFromTenant(ITenants tenant)
        {
            var db = new WapiServicesDb(tenant);
            var list = db.GetCampusActiveList();
            return list;
        }

        #region Company Codes

        /// <summary>
        /// The get code companies tenants.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ExternalCompanyTenantDb&gt;"/>.
        /// </returns>
        public static IList<ExternalCompanyTenantDb> GetCodeCompaniesTenants(ITenants tenant)
        {
            var db = new WapiServicesDb(tenant);
            var list = db.GetCompanyCodeFromTenantDb();
            return list;
        }

        /// <summary>
        /// The set company code to KLASSAPP services.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public static void SetCompanyCodeToKlassAppServices(ITenants tenant)
        {
            var sql = File.ReadAllText("Scripts//SetCompanyCodeWithKlassAppOperations.sql");
            Dictionary<string, object> parameters = new Dictionary<string, object>();

            // Parameter must be added with @
            parameters.Add("@CompanySecret", tenant.WindowsServiceSettingsObject.CompanyCode);

            var db = new DbServices();
            db.RunScriptWithParameter(sql, tenant.AdvantageDataBaseConnectionString, parameters);
        }

        /// <summary>
        /// The insert company code in advantage.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public static void InsertCompanyCodeInAdvantage(ITenants tenant)
        {
            var sql = File.ReadAllText("Scripts//InsertCompanySecretInAdvantage.sql");
            Dictionary<string, object> parameters =
                new Dictionary<string, object>
                    {
                        {
                            "@CompanySecret",
                            tenant.WindowsServiceSettingsObject.CompanyCode
                        }
                    };

            // Parameter must be added with @
            var db = new DbServices();
            db.RunScriptWithParameter(sql, tenant.AdvantageDataBaseConnectionString, parameters);
        }

        /// <summary>
        /// The insert company code in tenant.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public static void InsertCompanyCodeInTenant(ITenants tenant)
        {
            var db = new DbServices();
            var tenantid = db.GetTenantId(tenant);
            var sql = File.ReadAllText("Scripts//InsertCompanySecretInTenant.sql");

            // Parameter must be added with @
            Dictionary<string, object> parameters =
                 new Dictionary<string, object>
                     {
                        { "@CompanySecret", tenant.WindowsServiceSettingsObject.CompanyCode },
                        { "@TenantId", tenantid }
                     };

            db.RunScriptWithParameter(sql, tenant.TenantDataBaseConnectionString, parameters);
        }

        /// <summary>
        /// The update advantage company code.
        /// </summary>
        /// <param name="renamedCompanyCode">
        /// The renamed company code.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public static void UpdateAdvantageCompanyCode(string renamedCompanyCode, ITenants tenant)
        {
            var dbi = new DbServices();
            var tenantId = dbi.GetTenantId(tenant);
            using (TransactionScope scope = new TransactionScope())
            {
                var db = new WapiServicesDb(tenant);
                db.UpdateAdvantageCompanyCode(renamedCompanyCode, tenant);

                db.UpdateTenantCompanyCode(renamedCompanyCode, tenant, tenantId);
                scope.Complete();
            }
        }
        #endregion

        #region Allowed Services

        /// <summary>
        /// The test allowed operations path.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string TestAllowedOperationsPath(ITenants tenant)
        {
            // Get the list of allowed services
            var db = new WapiServicesDb(tenant);
            var serviceList = db.GetWapiAllowedServices();

            // Check if the path begin with the given string
            var message = string.Empty;
            foreach (WapiAllowedServiceType serviceType in serviceList)
            {
                if (!serviceType.Url.StartsWith(tenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath))
                {
                    // List those haven't the given string
                    message += $"The Service {serviceType.Code} has a wrong URL: {serviceType.Url}";
                }
            }

            if (message != string.Empty)
            {
                return message;
            }

            // Test if the Address of service is OK...
            var service = serviceList.SingleOrDefault(x => x.Code == "ECHOSL");
            if (service == null)
            {
                return "Service ECHOSL was not found";
            }
            var api = DbInstallation.GetApiKey(tenant);
            var wapi = new WapiVerification(tenant);
            var result = wapi.GetWapiCatalogs(service, api, $"/?CatalogoOperation={service.Code}");
            if (result.Item1 != "OK")
            {
                return $"Error Accessing Allowed Service: {result.Item1} - {result.Item2} - {result.Item3}";
            }

            return string.Empty;
        }

        /// <summary>
        /// The configure allowed operations path.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ConfigureAllowedOperationsPath(ITenants tenant)
        {
            // Get the list of allowed services
            var db = new WapiServicesDb(tenant);
            var serviceList = db.GetWapiAllowedServices();

            // Check if the path begin with the given string
            var message = string.Empty;
            foreach (WapiAllowedServiceType serviceType in serviceList)
            {
                var market = "services/api";
                var position = serviceType.Url.ToLower().IndexOf(market, StringComparison.Ordinal);
                if (position >= 0)
                {
                    if (position != 0)
                    {
                         serviceType.Url = serviceType.Url.Remove(0, position);
                    }
                   
                    serviceType.Url = ConcatenateUrlAddress(
                        tenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath,
                        serviceType.Url);
                    db.UpdateWapiAllowedServices(tenant, serviceType);
                    message += $"Service {serviceType.Code} URL: {serviceType.Url}";
                    message += Environment.NewLine;
                }
            }

            message += $"{serviceList.Count} were found in the application";
            message += Environment.NewLine;
            return message;
        }

        #endregion

        /// <summary>
        /// The get external operations.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;WapiOperationSettingOutputModel&gt;"/>.
        /// </returns>
        public static IList<WapiOperationSettingOutputModel> GetExternalOperations(ITenants tenant)
        {
            var db = new WapiServicesDb(tenant);

            // Read File SelectExternalOperations
            var query = File.ReadAllText("Scripts\\SelectExternalOperations.sql");
            var list = db.GetExternalOperations(query);
            return list;
        }

        /// <summary>
        /// The get test response from KLASSAPP.
        /// </summary>
        /// <param name="address">
        /// The address.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="WapiServicesResult"/>.
        /// </returns>
        public static WapiServicesResult GetTestResponsefromKlassApp(string address, ITenants tenant)
        {
            // Sending information to KlassApp ...........................................
            // Create the client
            var client = new HttpClient();

            // Create the headers
            client.DefaultRequestHeaders.Add("auth_id", tenant.WindowsServiceSettingsObject.ConsumerKey);
            client.DefaultRequestHeaders.Add("auth_token", tenant.WindowsServiceSettingsObject.PrivateKey);

            // Compose the URI
            var uri = new Uri(address);

            // Post to Service............
            var response = WapiServicesFacade.GetFromExternalApiAsync(client, uri).Result;
            var result = new WapiServicesResult { IsOk = response.StatusCode == HttpStatusCode.OK };
            result.ErrorCode = result.IsOk ? string.Empty : response.ReasonPhrase;
            return result;
        }

        public static async Task<HttpResponseMessage> GetFromExternalApiAsync(HttpClient client, Uri uri)
        {
            var task = client.GetAsync(uri);
            var msg = await task;
            return msg;
        }

        public static Tuple<bool, string> TestDatabaseConexion(ITenants tenant, string conexionToTest)
        {
            var db = new WapiServicesDb(tenant);
            var tuple = db.TestDatabaseConexion(conexionToTest);
            return tuple;
        }

        /// <summary>
        /// The test URL exists.
        /// </summary>
        /// <param name="url">
        /// The fast report server web URL.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public static Tuple<bool, string> TestUrlExists(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "HEAD";
                var response = (HttpWebResponse)request.GetResponse();
                var success = response.StatusCode == HttpStatusCode.OK;
                if (success)
                {
                    return new Tuple<bool, string>(true, string.Empty);
                }

                return new Tuple<bool, string>(false, $"Address is incorrect or not found. Reason: {response.StatusDescription}");
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    return new Tuple<bool, string>(true, string.Empty);
                }

                return new Tuple<bool, string>(false, $"Address is incorrect or not found. Reason: {e.Message}");
            }
            catch (Exception e)
            {
                return new Tuple<bool, string>(false, $"Address is incorrect or not found. Reason: {e.Message}");
            }
        }

        /// <summary>
        /// The Concatenate URL address.
        /// </summary>
        /// <param name="prefix">
        /// The prefix.
        /// </param>
        /// <param name="sufix">
        /// The suffix.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string ConcatenateUrlAddress(string prefix, string suffix)
        {
            prefix = prefix.EndsWith("/") ? prefix : $"{prefix}/";
            return $"{prefix}{suffix}";
        }
    }
}
