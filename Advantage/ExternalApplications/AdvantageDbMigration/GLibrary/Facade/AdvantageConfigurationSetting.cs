﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageConfigurationSetting.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the AdvantageConfigurationSetting type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Facade
{
    using System;
    using System.Collections.Generic;

    using GLibrary.DataLayer;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The advantage configuration setting facade.
    /// </summary>
    public static class AdvantageConfigurationSetting
    {
        /// <summary>
        /// The get single configuration setting.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// if the setting does not exists
        /// </exception>
        public static string GetSingleConfigurationSetting(string key, ITenants tenant)
        {
            var db = new ConfigurationSettingDb();
            IList<string> setting = db.GetConfigurationSetting(key, tenant);
            if (setting.Count > 0)
            {
                return setting[0];
            }

            throw new ApplicationException($"setting {key} does not exists");
        }

        /// <summary>
        /// The get multiple configuration setting.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The List of string
        /// </returns>
        public static IList<SettingWithCampus> GetMultipleConfigurationSetting(string key, ITenants tenant)
        {
            var db = new ConfigurationSettingDb();
            IList<SettingWithCampus> setting = db.GetConfigurationSettingWithCampus(key, tenant);
            if (setting.Count > 0)
            {
                return setting;
            }

            throw new ApplicationException($"setting {key} does not exists");
        }

        /// <summary>
        /// The set single configuration setting.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="settingName">
        /// The setting name.
        /// </param>
        /// <param name="settingValue">
        /// The setting value.
        /// </param>
        public static void SetSingleConfigurationSetting(ITenants tenant, string settingName, string settingValue)
        {
            var db = new ConfigurationSettingDb();
            db.SetConfigurationSetting(tenant, settingName, settingValue);
        }
    }
}
