﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowDomainAccess.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The window domain access.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Facade
{
    using System.DirectoryServices.AccountManagement;

    /// <summary>
    /// The window domain access.
    /// </summary>
    public static class WindowDomainAccess
    {
        /// <summary>
        /// Check if a domain name is present in the domain.
        /// </summary>
        /// <param name="username">
        /// The user name to be tested.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool CheckUserName(string username)
        {
           // create your domain context
            using (PrincipalContext domain = new PrincipalContext(ContextType.Domain))
            {
                // find the user
                UserPrincipal foundUser = UserPrincipal.FindByIdentity(domain, IdentityType.Name, username);

                return foundUser != null;
            }
        }

        /// <summary>
        /// The check if local user exists.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool CheckIfLocalUserExists(string userName)
        {
            using (var domainContext = new PrincipalContext(ContextType.Machine))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    return foundUser != null;
                }
            }
        }

        public static UserPrincipal GetLocalUser(string userName)
        {
            using (var domainContext = new PrincipalContext(ContextType.Machine))
            {
                using (var foundUser = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, userName))
                {
                    return foundUser;
                }
            }
        }

        /// <summary>
        /// The create new local user.
        /// </summary>
        /// <param name="sUserName">
        /// The s user name.
        /// </param>
        /// <param name="sPassword">
        /// The s password.
        /// </param>
        /// <returns>
        /// The <see cref="UserPrincipal"/>.
        /// </returns>
        public static UserPrincipal CreateNewLocalUser(string sUserName, string sPassword)
        {
            // first check that the user doesn't exist
            if (CheckIfLocalUserExists(sUserName) == false)
            {
                PrincipalContext principalContext = new PrincipalContext(ContextType.Machine);
                UserPrincipal userPrincipal = new UserPrincipal(principalContext)
                                                   {
                                                       Name = sUserName
                                                   };
                userPrincipal.SetPassword(sPassword);
                
                // User Log on Name
                // oUserPrincipal.UserPrincipalName = sUserName;
                userPrincipal.Save();

                return userPrincipal;
            }

            // if it already exists, return the old user
            return GetLocalUser(sUserName);
        }
    }
}
