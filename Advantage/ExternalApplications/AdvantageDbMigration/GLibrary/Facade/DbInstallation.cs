﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbInstallation.cs" company="FAME">
// 2017  
// </copyright>
// <summary>
//   The DB installation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Facade
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using GLibrary.DataLayer;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The DB installation.
    /// </summary>
    public static class DbInstallation
    {
        /// <summary>
        /// The advantage DB Name.
        /// </summary>
        private const string Advantage = "Advantage";

        /////// <summary>
        /////// The sand box DB Name.
        /////// </summary>
        ////private const string SandBox = "SandBox";
        
        /// <summary>
        /// The run script.
        /// </summary>
        /// <param name="scriptfile">
        /// The script-file.
        /// </param>
        /// <param name="selectedTenant">
        /// The selected tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Return a exception if something wrong happen
        /// </exception>
        public static int RunScript(string scriptfile, ITenants selectedTenant)
        {
            try
            {
                // Read File
                var query = File.ReadAllText(scriptfile);

                // Execute query
                var db = new DbServices();
                var ret = db.RunScript(query, selectedTenant);
                return ret;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException($"Script {scriptfile} fail with: {Environment.NewLine} {Common.CollectAllExceptionsMessages(e)}");
            }
        }

        /// <summary>
        /// The execute batch non query.
        /// </summary>
        /// <param name="scriptfile">
        /// The script file.
        /// </param>
        /// <param name="selectedTenant">
        /// The selected tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Is something in the query go wrong
        /// </exception>
        public static int ExecuteBatchNonQuery(string scriptfile, ITenants selectedTenant)
        {
            try
            {
                // Read File
                var query = File.ReadAllText(scriptfile);

                // Execute query
                var db = new DbServices();
                var ret = db.ExecuteSqlSriptsWithGo(query, selectedTenant);
                return ret;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException($"Script {scriptfile} fail with: {Environment.NewLine} {Common.CollectAllExceptionsMessages(e)}");
            }
        }

        /// <summary>
        /// This overload work directly with the selected connexion string.
        /// </summary>
        /// <param name="scriptfile"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static int ExecuteBatchNonQuery(string scriptfile, string connectionString)
        {
            try
            {
                // Read File
                var query = File.ReadAllText(scriptfile);

                // Execute query
                var db = new DbServices();
                var ret = db.ExecuteSqlSriptsWithGo(query, connectionString);
                return ret;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException($"Script {scriptfile} fail with: {Environment.NewLine} {Common.CollectAllExceptionsMessages(e)}");
            }
        }

        /// <summary>
        /// The run script with parameter. Execute a script with parameters
        /// and execute a execute not query. Return column affected
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="parameterList">
        /// The parameter list.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int RunScriptWithParameter(
            string query,
            string connection,
            Dictionary<string, object> parameterList)
        {
            var db = new DbServices();
            var res = db.RunScriptWithParameter(query, connection, parameterList);
            return res;
        }

        /// <summary>
        /// The run script without parameter.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int RunScriptWithoutParameter(string query, string connection)
        {
            {
                var db = new DbServices();
                var res = db.RunScript(query, connection);
                return res;
            }
        }

        /// <summary>
        /// The execute batch non query attendance job.
        /// </summary>
        /// <param name="scriptfile">
        /// The script file.
        /// </param>
        /// <param name="selectedTenant">
        /// The selected tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// If the script fail, return a exception
        /// </exception>
        public static int ExecuteBatchNonQueryAttendanceJob(string scriptfile, ITenants selectedTenant)
        {
            try
            {
                // Read File
                var query = File.ReadAllText(scriptfile);

                // Execute query
                var db = new DbServices();
                db.ExecuteAttendanceJob(query, selectedTenant);
                return 1;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException($"Script {scriptfile} fail with: {Environment.NewLine} {Common.CollectAllExceptionsMessages(e)}");
            }
        }

        /// <summary>
        /// The create table version and initiate
        /// to the given version as first version
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        public static int CreateTableVersion(ITenants tenant, DbVersion version)
        {
            var db = new DbServices();
            var ret = db.CreateDataBaseVersion(tenant, version);
            return ret;
        }

        /// <summary>
        /// The get DB version.
        /// </summary>
        /// <param name="selectedTenant">
        /// The selected tenant.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        public static DbVersion GetDbVersion(ITenants selectedTenant)
        {
            var db = new DbServices();
            var ret = db.GetDbCurrentVersionHistory(selectedTenant);
            return ret;
        }

        /// <summary>
        /// The set current version.
        /// This set the history version of the database
        /// to the current (Advantage DB version)
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Return a SQL exception if something is wrong
        /// </exception>
        public static DbVersion SetCurrentVersion(ITenants tenant, DbVersion version)
        {
            try
            {
                var db = new DbServices();
                var output = db.SetCurrentVersion(tenant, version);
                return output;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new ApplicationException($"Script [Scripts/UpdateDatabaseVersion.sql] fail with: {Environment.NewLine} {Common.CollectAllExceptionsMessages(e)}");
            }
        }

        /// <summary>
        /// The get database level of compatibility.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int GetDatabaseLevelOfCompatibility(ITenants tenant)
        {
            var db = new DbServices();
            var output = db.GetAdvantageDatabaseLevelOfCompatibility(tenant);
            return output;
        }

        /// <summary>
        /// The change compatibility level.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        public static void ChangeCompatibilityLevel(ITenants tenant, int setting)
        {
            var db = new DbServices();
            db.ChangeCompatibilityLevel(tenant, setting);
        }

        /// <summary>
        /// The exists version table.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ExistsVersionTable(ITenants tenant)
        {
            var db = new DbServices();
            bool exists = db.ExistsVersionTable(tenant);
            return exists;
        }

        /// <summary>
        /// The verify view <code>arStudAddress</code>.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool VerifyViewArStudAddress(ITenants tenant)
        {
            var db = new DbServices();
            bool exists = db.VerifyViewArStudAddresses(tenant);
            return exists;
        }

        /// <summary>
        /// The exists database.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="dbname">
        /// The DB name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool ExistsDatabase(ITenants tenant, string dbname)
        {
            var db = new DbServices();
            bool exists = db.ExistsDatabase(tenant, dbname);
            return exists;
        }


        /// <summary>
        /// The set advantage configuration values.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public static void SetAdvantageConfigurationValues(ITenants tenantObject, string databaseName)
        {
            var db = new DbServices();

            // Set the school Name
            db.SetAdvantageConfigurationValue(tenantObject, "SchoolName", tenantObject.ConfigurationSettingsObject.SchoolName, databaseName);

            // Set the authentication key
            var apikey = (databaseName == Advantage)
                             ? tenantObject.ConfigurationSettingsObject.ServiceLayerAdvantageApiKey.ToString().ToUpperInvariant()
                             : tenantObject.ConfigurationSettingsObject.ServiceLayerSandBoxApiKey.ToString().ToUpperInvariant();
            db.SetAdvantageConfigurationValue(tenantObject, "AdvantageServiceAuthenticationKey", apikey, databaseName);

            var siteUri = $"{tenantObject.ConfigurationSettingsObject.AdvantageVirtualBaseAddress}/Advantage/Current/Site/";
            siteUri = $"{tenantObject.ConfigurationSettingsObject.Protocol}://{siteUri}";
            var serviceUri = $"{tenantObject.ConfigurationSettingsObject.AdvantageVirtualBaseAddress}/Advantage/Current/Services/";
            serviceUri = $"{tenantObject.ConfigurationSettingsObject.Protocol}://{serviceUri}";
            db.SetAdvantageConfigurationValue(tenantObject, "AdvantageSiteUri", siteUri, databaseName);
            db.SetAdvantageConfigurationValue(tenantObject, "AdvantageServiceUri", serviceUri, databaseName);
            var reportpath = $"{tenantObject.ConfigurationSettingsObject.ReportServerVirtualBaseAddress}:{tenantObject.ConfigurationSettingsObject.ReportServerPort}";
            reportpath = $"{tenantObject.ConfigurationSettingsObject.Protocol}://{reportpath}";
            var reportService = $"{reportpath}/ReportService2005.asmx";
            var reportExecutionServices = $"{reportpath}/ReportExecution2005.asmx";
            db.SetAdvantageConfigurationValue(tenantObject, "ReportServices", reportService, databaseName);
            db.SetAdvantageConfigurationValue(tenantObject, "ReportExecutionServices", reportExecutionServices, databaseName);
        }

        /// <summary>
        /// The configure tenant advantage.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="enviromentName">
        /// The environment name.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public static void ConfigureTenantAdvantage(ITenants tenantObject, string enviromentName, string databaseName)
        {
            var db = new DbServices();
            
            // Set environment
            var environmentId = db.SetEnvironment(tenantObject, enviromentName);

            // Set Tenant Name
            var tenantId = db.SetTenantName(tenantObject, environmentId, databaseName);

            // Set API Key
            var api = databaseName == Advantage
                          ? tenantObject.ConfigurationSettingsObject.ServiceLayerAdvantageApiKey
                          : tenantObject.ConfigurationSettingsObject.ServiceLayerSandBoxApiKey;
            db.SetApiKey(tenantObject, tenantId, api);

            // Set Default User
            db.SetDefaultUser(tenantObject, tenantId);
        }

        /// <summary>
        /// The set support user in tenant.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        public static void SetSupportUserInTenant(ITenants tenantObject)
        {
            var db = new DbServices();
           
            // Run procedure to adapt the tenant support user to the support stored in Tenant membership
            db.UpdateAdvantageTenant(tenantObject);
        }

        /// <summary>
        /// The get tenant ID.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int GetTenantId(ITenants tenant)
        {
            var db = new DbServices();
            int tenantid = db.GetTenantId(tenant);
            return tenantid;
        }

        /// <summary>
        /// The get API key.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetApiKey(ITenants tenant)
        {
            var db = new DbServices();
           var apikey = db.GetApiKey(tenant);
            return apikey;
        }
    }
}
