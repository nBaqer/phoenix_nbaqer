﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageConfigurationSettings.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The advantage configuration settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using System;

    /// <summary>
    /// The advantage configuration settings.
    /// </summary>
    public class AdvantageConfigurationSettings
    {
        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the service layer API key for DB Advantage.
        /// </summary>
        public Guid ServiceLayerAdvantageApiKey { get; set; }

        /// <summary>
        /// Gets or sets the service layer for DB Sandbox API key.
        /// </summary>
        public Guid ServiceLayerSandBoxApiKey { get; set; }

        /// <summary>
        /// Gets or sets the protocol.
        /// </summary>
        public string Protocol { get; set; }

        /// <summary>
        /// Gets or sets the advantage virtual base address.
        /// </summary>
        public string AdvantageVirtualBaseAddress { get; set; }

        /// <summary>
        /// Gets or sets the report server virtual base address.
        /// </summary>
        public string ReportServerVirtualBaseAddress { get; set; }

        /// <summary>
        /// Gets or sets the report server port.
        /// </summary>
        public string ReportServerPort { get; set; }

        /// <summary>
        /// Gets or sets the connection string to access the table 
        /// Catalog in the report server (use in Fast Report)
        /// </summary>
        public string ReportServerConexion { get; set; }

        /// <summary>
        /// Gets or sets the report server service instance
        /// this is use in Fast Report
        /// </summary>
        public string ReportServerInstance { get; set; }
    }
}