﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageWindowsServiceSettings.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The advantage windows service settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using System;

    /// <summary>
    /// The advantage windows service settings.
    /// </summary>
    public class AdvantageWindowsServiceSettings
    {
        /// <summary>
        /// Gets or sets the code operation.
        /// this is a arbitrary name given to the
        /// Windows Server operation
        /// </summary>
        public string CodeOperation { get; set; }
        
        /// <summary>
        /// Gets or sets the company Code default user.
        /// </summary>
        public string CompanyCode { get; set; }

        /// <summary>
        /// Gets or sets the windows service context user name.
        /// </summary>
        public string WindowsServiceContextUserName { get; set; }

        /// <summary>
        /// Gets or sets the windows service default password.
        /// </summary>
        public string WindowsServiceContextPassword { get; set; }

        /// <summary>
        /// Gets or sets the <code>KlassApp</code> token.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the <code>KlassApp</code> password.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the last execution date of the windows service.
        /// It is also used to set the initial point of data recollection
        /// from windows server operation.
        /// </summary>
        public DateTime LastExecutionDate { get; set; }

        /// <summary>
        /// Gets or sets the schedule time send student seconds.
        /// </summary>
        public string ScheduleTimeSendStudentSeconds { get; set; }

        /// <summary>
        /// Gets or sets the schedule time send student on demand poll.
        /// </summary>
        public string ScheduleTimeSendStudentOnDemandPoll { get; set; }

        /// <summary>
        /// Gets or sets Allowed Services Basic Path. Must terminate in slash
        /// it is the path before Advantage/ (example <code>http://localhost/</code>)
        /// </summary>
        public string WapiAllowedServicesBasicPath { get; set; }

        /// <summary>
        /// Gets or sets the KLASSAPP external URL.
        /// This is the address that appear in WAPI External Operation as External URL
        /// and give the base URL to access KLASSAPP
        /// </summary>
        public string KlassAppExternalUrl { get; set; }

        /////// <summary>
        /////// Gets or sets the windows service path to configuration.
        /////// </summary>
        ////public string WindowsServicePathToConfiguration { get; set; }

        /// <summary>
        /// Gets or sets the advantage proxy URL.
        /// </summary>
        public string AdvantageProxyUrl { get; set; }
    }
}