﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Properties.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Properties type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    /// <summary>
    /// The properties.
    /// </summary>
    public class Properties
    {
        /// <summary>
        /// Gets or sets the caption.
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public object Value { get; set; }
    }
}
