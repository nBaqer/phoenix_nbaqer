﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastReportObject.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the FastReportObject type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    /// <summary>
    /// The fast report object.
    /// </summary>
    public class FastReportObject
    {
        /// <summary>
        /// Gets or sets the report server web URL.
        /// </summary>
        public string ReportServerWebUrl { get; set; }

        /// <summary>
        /// Gets or sets the report server DB connection string.
        /// </summary>
        public string ReportServerDbConnectionString { get; set; }

        /// <summary>
        /// Gets or sets windows user that impersonate Advantage to access the 
        /// report server
        /// </summary>
        public string ReportUserImpersonator { get; set; }
    }
}
