﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExternalCompanyTenantDb.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ExternalCompanyTenantDb type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    /// <summary>
    /// The external company tenant DB.
    /// </summary>
    public class ExternalCompanyTenantDb
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the external company code.
        /// </summary>
        public string ExternalCompanyCode { get; set; }

        /// <summary>
        /// Gets or sets the tenant id.
        /// </summary>
        public int TenantId { get; set; }
    }
}
