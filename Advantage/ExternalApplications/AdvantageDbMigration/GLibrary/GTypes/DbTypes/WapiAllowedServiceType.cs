﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiAllowedServiceType.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the WapiAllowedServiceType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    /// <summary>
    /// The WAPI allowed service type.
    /// </summary>
    public class WapiAllowedServiceType 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WapiAllowedServiceType"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="url">
        /// The URL.
        /// </param>
        /// <param name="isActive">
        /// The is active.
        /// </param>
        public WapiAllowedServiceType(int id, string code, string description, string url, bool isActive)
        {
            this.Id = id;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Code = code;
            this.Description = description;
            this.Url = url;
            this.IsActive = isActive;

            // ReSharper restore DoNotCallOverridableMethodsInConstructor 
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; protected set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; protected set; }
     }
}
