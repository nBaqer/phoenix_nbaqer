﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationSettingOutputModel.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The WAPI operation setting output model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    using System;

    /// <summary>
    /// The WAPI operation setting output model.
    /// </summary>
    public class WapiOperationSettingOutputModel
    {
        /// <summary>
        /// Gets or sets Unique ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the code operation.
        /// this is the name of the operation
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets Code of the company that owner the web service
        /// in the case that the operation access a external
        /// resource.
        /// </summary>
        public string CodeExtCompany { get; set; }

        /// <summary>
        /// Gets or sets Description of the external company. 
        /// usually the complete name of the company
        /// </summary>
        public string DescripExtCompany { get; set; }

        /// <summary>
        /// Gets or sets the code of the external operation mode
        /// can be push or pull.
        /// Reference <code>syWapiExternalOperationMode</code>
        /// </summary>
        public string CodeExtOperationMode { get; set; }

        /// <summary>
        /// Gets or sets the description external operation mode.
        /// Reference <code>syWapiExternalOperationMode</code>
        /// </summary>
        public string DescripExtOperationMode { get; set; }

        /// <summary>
        /// Gets or sets The code of operation in the WAPI
        /// Code of the primary Advantage WAPI server to be accessed through
        /// the proxy.
        /// </summary>
        public string CodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets This is to show values. not to be changes
        /// </summary>
        public string DescripWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        public string SecondCodeWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Some operation require two operation to be completed
        /// In this case this field is used.
        /// </summary>
        public string SecondDescWapiServ { get; set; }

        /// <summary>
        /// Gets or sets the URL WAPI service.
        /// </summary>
        public string UrlWapiServ { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active WAPI service.
        /// </summary>
        public bool IsActiveWapiServ { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service if exists
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets Security key to interact with external service
        /// if exists.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets Time that a windows service should repeat the operation
        /// if it is needed. -1 if inactive.0 one time operation.
        /// </summary>
        public int OperationSecInterval { get; set; }

        /// <summary>
        /// Gets or sets Poll WAPI from windows service to look for a configuration change.
        /// </summary>
        public int PollSecOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets 0: inactive, 1: active
        /// </summary>
        public int FlagOnDemandOperation { get; set; }

        /// <summary>
        /// Gets or sets 0: inactive, 1: active
        /// </summary>
        public int FlagRefreshConfig { get; set; }

        /// <summary>
        /// Gets or sets External URL to looking for, if exists.
        /// </summary>
        public string ExternalUrl { get; set; }

        /// <summary>
        /// Gets or sets Last execution date and time.
        /// When the operation is created is entered the actual date and time.
        /// This should be use to calculate the initial time of the operation
        /// in order to calculate the next execution.
        /// </summary>
        public DateTime DateLastExecution { get; set; }

        /// <summary>
        /// Gets or sets ask if the operation is active o inactive.
        /// </summary>
        public bool IsActiveOperation { get; set; }
    }
}
