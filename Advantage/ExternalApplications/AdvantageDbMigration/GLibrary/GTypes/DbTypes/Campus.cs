﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Campus.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Campus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    using System;

    /// <summary>
    /// The campus.
    /// </summary>
    public class Campus
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
