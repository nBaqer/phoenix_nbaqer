﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExternalCompanies.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The external companies.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    /// <summary>
    /// The external companies.
    /// </summary>
    public class ExternalCompanies
    {
       

        public ExternalCompanies(int id, string code, string description, bool isActive)
        {
            this.Id = id;
            this.Code = code;
            this.Description = description;
            this.IsActive = isActive;
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }  
    }
}
