﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SettingWithCampus.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the SettingWithCampus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    /// <summary>
    /// The setting with campus.
    /// Return the value of the settings and the campus associated
    /// </summary>
    public class SettingWithCampus
    {
        /// <summary>
        /// Gets or sets the setting value.
        /// </summary>
        public string SettingValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether campus specific.
        /// </summary>
        public bool CampusSpecific { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public string CampusId { get; set; }
    }
}
