﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbVersion.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the DbVersion type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.DbTypes
{
    using System;

    /// <summary>
    /// The DB version.
    /// </summary>
    public class DbVersion
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        public int Major { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public int Minor { get; set; }

        /// <summary>
        /// Gets or sets the minor.
        /// </summary>
        public int Build { get; set; }

        /// <summary>
        /// Gets or sets the build.
        /// </summary>
        public int Revision { get; set; }

        /// <summary>
        /// Gets or sets the version begin.
        /// </summary>
        public DateTime VersionBegin { get; set; }

        /// <summary>
        /// Gets or sets the version end.
        /// </summary>
        public DateTime? VersionEnd { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// The get string version.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetStringVersion()
        {
            var ver = $"{this.Major}.{this.Minor}.{this.Build}.{this.Revision}";
            return ver;
        }
    }
}
