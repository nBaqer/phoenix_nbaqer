﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITenants.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ITenants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using System.Collections.Generic;

    /// <summary>
    /// The Tenants interface.
    /// </summary>
    public interface ITenants
    {
        /// <summary>
        /// Gets or sets a value indicating whether is the field new inserted.
        /// </summary>
        bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets the advantage physical base address.
        /// All path before 
        /// </summary>
        string AdvantagePhysicalBaseAddress { get; set; }

        /// <summary>
        /// Gets or sets the advantage data base connection string.
        /// </summary>
        string AdvantageDataBaseConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the advantage service url base address.
        /// </summary>
        string AdvantageServiceUrlBaseAddress { get; set; }

        /// <summary>
        /// Gets or sets the configuration settings object.
        /// </summary>
        AdvantageConfigurationSettings ConfigurationSettingsObject { get; set; }
        
        /// <summary>
        /// Gets or sets the windows service settings object.
        /// </summary>
        AdvantageWindowsServiceSettings WindowsServiceSettingsObject { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is restore DB relocated.
        /// </summary>
        bool IsRestoreDbRelocated { get; set; }

        /// <summary>
        /// Gets or sets the relocation string for DB restore.
        /// </summary>
        string RelocationStringForDbRestore { get; set; }

        /// <summary>
        /// Gets or sets the tenant data base connection string.
        /// </summary>
        string TenantDataBaseConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the tenant display name.
        /// </summary>
        string TenantDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the tenant name.
        /// </summary>
        string TenantName { get; set; }

        /// <summary>
        /// Gets or sets the advantage impersonator windows user.
        /// this is the windows user impersonated by Advantage
        /// it is used to access the report server DB
        /// </summary>
        string ReportUserImpersonator { get; set; }

        /// <summary>
        /// The convert to properties.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        IList<Properties> ConvertToProperties();

        /// <summary>
        /// The convert to settings.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        IList<Properties> ConvertToSettings();

        /// <summary>
        /// The convert to settings 2.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        IList<Properties> ConvertToSettings2();

        /// <summary>
        /// The update tenant properties.
        /// </summary>
        /// <param name="propertiesList">
        /// The properties list.
        /// </param>
        void UpdateTenantProperties(IList<Properties> propertiesList);

        /// <summary>
        /// The update tenant settings.
        /// </summary>
        /// <param name="settingsList">
        /// The settings list.
        /// </param>
        void UpdateTenantSettings(IList<Properties> settingsList);

        /// <summary>
        /// The update tenant settings 2.
        /// </summary>
        /// <param name="settingsList">
        /// The settings list.
        /// </param>
        void UpdateTenantSettings2(IList<Properties> settingsList);
    }
}