﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppConfiguration.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassAppConfiguration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using System.Collections.Generic;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The KLASSAPP configuration.
    /// </summary>
    public class KlassAppConfiguration
    {
        public IList<Campus> CampusList { get; set; }
    }


}
