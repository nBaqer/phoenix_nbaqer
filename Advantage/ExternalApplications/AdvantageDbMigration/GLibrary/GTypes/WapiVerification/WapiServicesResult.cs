﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServicesResult.cs" company="Fame">
//   2017
// </copyright>
// <summary>
//   Defines the WapiServicesResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.WapiVerification
{
    /// <summary>
    /// The WAPI services result.
    /// </summary>
    public class WapiServicesResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether is ok.
        /// </summary>
        public bool IsOk { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the property.
        /// </summary>
        public string Property { get; set; }
    }
}
