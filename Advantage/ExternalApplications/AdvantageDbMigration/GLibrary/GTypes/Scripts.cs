﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Scripts.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Scripts type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The scripts.
    /// </summary>
    public class Scripts
    {
        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the compare minor.
        /// </summary>
        public string CompareMinor { get; set; }

        /// <summary>
        /// Gets or sets the version created.
        /// </summary>
        public DbVersion VersionCreated { get; set; }
    }
}
