﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GLibrary.GTypes.VerificationType
{
    public static class WapiDictionaries
    {
        /// <summary>
        /// Initializes static members of the <see cref="WapiDictionaries"/> class.
        /// </summary>
        static WapiDictionaries()
        {
            AllowedServicesDictionary = new Dictionary<string, string>();
            AllowedServicesDictionary.Add("ECHO_GET_PARAM", "Advantage/Current/Services/api/SystemStuff/Maintenance/EchoTest/GetParam");
            AllowedServicesDictionary.Add("ECHO_POST_PARAM", "Advantage/Current/Services/api/SystemStuff/Maintenance/EchoTest/Post");
            AllowedServicesDictionary.Add("ECHOSL", "Advantage/Current/Services/api/SystemStuff/Maintenance/EchoTest/");
            AllowedServicesDictionary.Add("GET_ON_DEMAND_FLAG", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiOperationSettings/GetOnDemandFlags");
            AllowedServicesDictionary.Add("INITWSERV", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiWindowsService/Post");
            AllowedServicesDictionary.Add("KLASS_GET_CONFIGURATION_STATUS", "Advantage/Current/Services/api/SystemStuff/Mobile/KlassAppUi/Get");
            AllowedServicesDictionary.Add("KLASS_GET_STUDENT_INFORMATION", "Advantage/Current/Services/api/SystemStuff/Mobile/KlassApp/Get");
            AllowedServicesDictionary.Add("POST_DATA_LAST_EXECUTION", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiOperationSettings/PostLastExecutedTime");
            AllowedServicesDictionary.Add("RSETTING", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiOperationSettings");
            AllowedServicesDictionary.Add("SET_ON_DEMAND_FLAG", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiOperationSettings/PostOnDemandFlags");
            AllowedServicesDictionary.Add("TEST_GET_FAKE", "Advantage/Current/Services/api/Wapi/Voyant/Voyant/GetFake");
            AllowedServicesDictionary.Add("VOYANT_DASHBOARD_INFO", "Advantage/Current/Services/api/Wapi/Voyant/Voyant/Post");
            AllowedServicesDictionary.Add("VOYANT_DATA", "Advantage/Current/Services/api/Wapi/Voyant/Voyant/Get");
            AllowedServicesDictionary.Add("WLOG", "Advantage/Current/Services/api/SystemStuff/Maintenance/WapiOperationSettings/PostLoggerRecord");
        }

        /// <summary>
        /// Gets The allowed services dictionary.
        /// </summary>
        public static Dictionary<string, string> AllowedServicesDictionary { get; }

    }
}
