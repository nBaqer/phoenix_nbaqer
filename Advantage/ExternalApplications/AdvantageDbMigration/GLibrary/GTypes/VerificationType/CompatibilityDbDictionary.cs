﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompatibilityDbDictionary.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   The compatibility DB dictionary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes.VerificationType
{
    using System.Collections.Generic;

    /// <summary>
    /// The compatibility DB dictionary.
    /// </summary>
    public static class CompatibilityDbDictionary
    {
        /// <summary>
        /// Initializes static members of the <see cref="CompatibilityDbDictionary"/> class.
        /// </summary>
        static CompatibilityDbDictionary()
        {
            CompatibilityLevelDictionary =
                new Dictionary<int, string>
                    {
                        { 80, "SQL Server 2000" },
                        { 90, "SQL Server 2005" },
                        { 100, "SQL Server 2008 or 2008R2" },
                        { 110, "SQL Server 2012" },
                        { 120, "SQL Server 2014" },
                        { 130, "SQL Server 2016 & Azure" },
                        { 140, "SQL Server vNext" }
                    };
        }

        /// <summary>
        /// Gets The allowed services dictionary.
        /// </summary>
        public static Dictionary<int, string> CompatibilityLevelDictionary { get; }
    }
}
