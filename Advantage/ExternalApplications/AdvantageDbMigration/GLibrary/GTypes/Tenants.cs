﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tenants.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the tenants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.GTypes
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The tenants.
    /// </summary>
    public class Tenants : TenantsAbstract
    {
        /// <summary>
        /// The factory.
        /// </summary>
        /// <returns>
        /// The <see cref="Tenants"/>.
        /// </returns>
        public static ITenants Factory()
        {
            var ten = new Tenants
                          {
                              ConfigurationSettingsObject = new AdvantageConfigurationSettings(),
                              WindowsServiceSettingsObject = new AdvantageWindowsServiceSettings()
                          };
            return ten;
        }

        /// <summary>
        /// Convert some items of the class in a dictionary
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        public override IList<Properties> ConvertToProperties()
        {
            var list = new List<Properties>();
            var dic = new Properties { Caption = "Tenant Display Name", Value = this.TenantDisplayName };
            list.Add(dic);
            dic = new Properties { Caption = "Tenant Name", Value = this.TenantName };
            list.Add(dic);
            dic = new Properties { Caption = "Tenant Connection String", Value = this.TenantDataBaseConnectionString };
            list.Add(dic);
            dic = new Properties { Caption = "Advantage Connection String", Value = this.AdvantageDataBaseConnectionString };
            list.Add(dic);
            return list;
        }

        /// <summary>
        /// The convert to settings.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        public override IList<Properties> ConvertToSettings()
        {
            var list = new List<Properties>();
            var dic = new Properties { Caption = "Report Server Connection", Value = this.ConfigurationSettingsObject == null ? string.Empty : this.ConfigurationSettingsObject.ReportServerConexion };
            list.Add(dic);
            dic = new Properties { Caption = "Report Server Instance", Value = this.ConfigurationSettingsObject == null ? string.Empty : this.ConfigurationSettingsObject.ReportServerInstance };
            list.Add(dic);
            dic = new Properties { Caption = "Report User Impersonator", Value = this.ReportUserImpersonator };
            list.Add(dic);
            return list;
        }

        /// <summary>
        /// The convert to settings 2.
        /// </summary>
        /// <returns>
        /// The <see cref="IList&lt;Properties&gt;"/>.
        /// </returns>
        public override IList<Properties> ConvertToSettings2()
        {
            var list = new List<Properties>();
            var dic = new Properties { Caption = "Basic Path for Allowed WAPI Services", Value = this.WindowsServiceSettingsObject == null ? string.Empty : this.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath };
            list.Add(dic);
            dic = new Properties { Caption = "Consumer Key", Value = this.WindowsServiceSettingsObject == null ? string.Empty : this.WindowsServiceSettingsObject.ConsumerKey };
            list.Add(dic);
            dic = new Properties { Caption = "Private Key", Value = this.WindowsServiceSettingsObject == null ? string.Empty : this.WindowsServiceSettingsObject.PrivateKey };
            list.Add(dic);
            dic = new Properties { Caption = "Is Active", Value = this.WindowsServiceSettingsObject?.IsActive ?? false };
            list.Add(dic);
            dic = new Properties { Caption = "Schedule Time Minutes", Value = this.WindowsServiceSettingsObject == null ? 60 : Convert.ToInt32(this.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds) / 60 };
            list.Add(dic);
            dic = new Properties { Caption = "Windows Service Context User name", Value = this.WindowsServiceSettingsObject?.WindowsServiceContextUserName };
            list.Add(dic);
            dic = new Properties { Caption = "Windows Service Context Password", Value = this.WindowsServiceSettingsObject?.WindowsServiceContextPassword };
            list.Add(dic);
            return list;
        }

        /// <summary>
        /// The update tenant properties.
        /// </summary>
        /// <param name="propertiesList">
        /// The properties list.
        /// </param>
        /// <exception cref="ApplicationException">
        /// Raise if the property is not valid
        /// </exception>
        public override void UpdateTenantProperties(IList<Properties> propertiesList)
        {
            foreach (Properties p in propertiesList)
            {
                switch (p.Caption)
                {
                    case "Tenant Display Name":
                        {
                            this.TenantDisplayName = (string)p.Value;
                            break;
                        }

                    case "Tenant Name":
                        {
                            this.TenantName = (string)p.Value;
                            break;
                        }

                    case "Tenant Connection String":
                        {
                            this.TenantDataBaseConnectionString = (string)p.Value;
                            break;
                        }

                    case "Advantage Connection String":
                        {
                            this.AdvantageDataBaseConnectionString = (string)p.Value;
                            break;
                        }

                    default:
                        {
                            throw new ApplicationException("Invalid property name");
                        }
                }
            }
        }

        /// <summary>
        /// The update tenant settings.
        /// </summary>
        /// <param name="settingsList">
        /// The settings list.
        /// </param>
        /// <exception cref="ApplicationException">
        /// Exception if the settings is not recognize
        /// </exception>
        public override void UpdateTenantSettings(IList<Properties> settingsList)
        {
            foreach (Properties p in settingsList)
            {
                switch (p.Caption)
                {
                    case "Report Server Connection":
                        {
                            this.ConfigurationSettingsObject.ReportServerConexion = (string)p.Value;
                            break;
                        }

                    case "Report Server Instance":
                        {
                            this.ConfigurationSettingsObject.ReportServerInstance = (string)p.Value;
                            break;
                        }

                    case "Report User Impersonator":
                        {
                            this.ReportUserImpersonator = (string)p.Value;
                            break;
                        }

                     default:
                        {
                            throw new ApplicationException("Invalid setting name");
                        }
                }
            }
        }

        /// <summary>
        /// The update tenant settings 2.
        /// </summary>
        /// <param name="settingsList">
        /// The settings list.
        /// </param>
        /// <exception cref="ApplicationException">
        /// If something go wrong...
        /// </exception>
        public override void UpdateTenantSettings2(IList<Properties> settingsList)
        {
            if (this.WindowsServiceSettingsObject == null)
            {
                this.WindowsServiceSettingsObject = new AdvantageWindowsServiceSettings();
            }

            foreach (Properties p in settingsList)
            {
                switch (p.Caption)
                {
                    case "Basic Path for Allowed WAPI Services":
                        {
                            this.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath = (string)p.Value;
                            break;
                        }

                    case "Consumer Key":
                        {
                            this.WindowsServiceSettingsObject.ConsumerKey = (string)p.Value;
                            break;
                        }

                    case "Private Key":
                        {
                            this.WindowsServiceSettingsObject.PrivateKey = (string)p.Value;
                            break;
                        }

                    case "Is Active":
                        {
                            this.WindowsServiceSettingsObject.IsActive = (bool)p.Value;
                            break;
                        }

                    case "Schedule Time Minutes":
                        {
                            this.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds = (Convert.ToInt32(p.Value) * 60).ToString();
                            break;
                        }

                    case "Windows Service Context User name":
                        {
                            this.WindowsServiceSettingsObject.WindowsServiceContextUserName = (string)p.Value;
                            break;
                        }

                    case "Windows Service Context Password":
                        {
                            this.WindowsServiceSettingsObject.WindowsServiceContextPassword = (string)p.Value;
                            break;
                        }

                    default:
                        {
                            throw new ApplicationException("Invalid setting name");
                        }
                }
            }
        }
    }
}
