﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurationSettingDb.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ConfigurationSettingDb type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The configuration setting DB.
    /// </summary>
    public class ConfigurationSettingDb
    {
        /// <summary>
        /// The get configuration setting.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The List of string
        /// </returns>
        public IList<string> GetConfigurationSetting(string key, ITenants tenant)
        {
            var dv = new List<string>();
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(" SELECT val.Value FROM [dbo].[syConfigAppSetValues] val " +
                                                " JOIN[dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId " +
                                                " WHERE KeyName = @KeyValue")
            {
                Connection = conn,
            };
            command.Parameters.AddWithValue("@KeyValue", key);
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    dv.Add(Convert.ToString(rd[0]));
                }

                return dv;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get configuration setting with campus.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;SettingWithCampus&gt;"/>.
        /// </returns>
        public IList<SettingWithCampus> GetConfigurationSettingWithCampus(string key, ITenants tenant)
        {
            var dv = new List<SettingWithCampus>();
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(" SELECT val.Value, app.CampusSpecific, val.CampusId FROM [dbo].[syConfigAppSetValues] val " +
                                                " JOIN[dbo].[syConfigAppSettings] app ON app.SettingId = val.SettingId " +
                                                " WHERE KeyName = @KeyValue")
            {
                Connection = conn,
            };
            command.Parameters.AddWithValue("@KeyValue", key);
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var seti = new SettingWithCampus();
                    seti.SettingValue = Convert.ToString(rd[0]);
                    seti.CampusSpecific = Convert.ToBoolean(rd[1]);
                    seti.CampusId = Convert.ToString(rd[2]);
                    dv.Add(seti);
                }

                return dv;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Update the value of a configuration setting.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="settingName">
        /// The setting name.
        /// </param>
        /// <param name="settingValue">
        /// The setting value.
        /// </param>
        /// <remarks>
        /// Does not use with CAMPUS specific settings!!!
        /// </remarks>
        public void SetConfigurationSetting(ITenants tenant, string settingName, string settingValue)
        {
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(
                " UPDATE  dbo.syConfigAppSetValues " + " SET     Value = @Value " + " WHERE   SettingId = ( "
                + "        SELECT    val.SettingId " + " FROM      dbo.syConfigAppSetValues val "
                + " JOIN      dbo.syConfigAppSettings ON syConfigAppSettings.SettingId = val.SettingId "
                + " WHERE     KeyName = @KeyName );")
            {
                Connection = conn,
            };
            command.Parameters.AddWithValue("@KeyName", settingName);
            command.Parameters.AddWithValue("@Value", settingValue);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
