﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageDbInfo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the AdvantageDbInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.DataLayer
{
    /// <summary>
    /// The advantage DB info.
    /// </summary>
    public class AdvantageDbInfo
    {
        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the API key.
        /// </summary>
        public string ApiKey { get; set; }
    }
}