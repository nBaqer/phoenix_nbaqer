﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServicesDb.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The WAPI services DB.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The WAPI services DB.
    /// </summary>
    public class WapiServicesDb
    {
        /// <summary>
        /// The selected tenant.
        /// </summary>
        private readonly ITenants tenant;

        /// <summary>
        /// The advantage information DB.
        /// </summary>
        private AdvantageDbInfo advantageInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiServicesDb"/> class.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public WapiServicesDb(ITenants tenant)
        {
            this.tenant = tenant;
            var cdb = new CommonDb();
            this.advantageInfo = cdb.GetAdvantageConnectionString(tenant);
        }

        /// <summary>
        /// The get WAPI allowed services.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<WapiAllowedServiceType> GetWapiAllowedServices()
        {
            IList<WapiAllowedServiceType> output = new List<WapiAllowedServiceType>();
            SqlConnection conn = new SqlConnection(this.advantageInfo.ConnectionString);
            SqlCommand command = new SqlCommand("SELECT * FROM dbo.syWapiAllowedServices") { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new WapiAllowedServiceType((int)rd["Id"], (string)rd["Code"], (string)rd["Description"], (string)rd["Url"], (bool)rd["IsActive"]);
                    output.Add(row);
                }

                return output;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get all companies code advantage.
        /// </summary>
        /// <param name="tenants">
        /// The tenants.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ExternalCompanies&gt;"/>.
        /// </returns>
        public IList<ExternalCompanies> GetAllCompaniesCodeAdvantage(ITenants tenants)
        {
            IList<ExternalCompanies> output = new List<ExternalCompanies>();
            SqlConnection conn = new SqlConnection(this.advantageInfo.ConnectionString);
            SqlCommand command = new SqlCommand("SELECT * FROM dbo.syWapiExternalCompanies WHERE IsActive = 1") { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new ExternalCompanies((int)rd["Id"], (string)rd["Code"], (string)rd["Description"], (bool)rd["IsActive"]);
                    output.Add(row);
                }

                return output;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get campus active list.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<Campus> GetCampusActiveList()
        {
            IList<Campus> output = new List<Campus>();
            SqlConnection conn = new SqlConnection(this.advantageInfo.ConnectionString);
            const string Sql = @"SELECT CampusId, CampDescrip FROM dbo.syCampuses JOIN dbo.syStatuses ON syStatuses.StatusId = syCampuses.StatusId WHERE dbo.syStatuses.StatusCode = 'A'";
            SqlCommand command = new SqlCommand(Sql)
            { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new Campus
                    {
                        Id = (Guid)rd["CampusId"],
                        Name = (string)rd["CampDescrip"]
                    };
                    output.Add(row);
                }

                return output;
            }
            finally
            {
                conn.Close();
            }
        }

        public void UpdateAdvantageCompanyCode(string renamedCompanyCode, ITenants tenants)
        {
            SqlConnection conn = new SqlConnection(this.advantageInfo.ConnectionString);
            SqlCommand command = new SqlCommand("UPDATE dbo.syWapiExternalCompanies SET Code = @NewCode WHERE Code = @OldCode") { Connection = conn };
            command.Parameters.AddWithValue("@NewCode", renamedCompanyCode);
            command.Parameters.AddWithValue("@OldCode", tenants.WindowsServiceSettingsObject.CompanyCode);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();

            }
            finally
            {
                conn.Close();
            }

        }


        public void UpdateTenantCompanyCode(string renamedCompanyCode, ITenants tenants, int tenantid)
        {
            SqlConnection conn = new SqlConnection(tenants.TenantDataBaseConnectionString);
            SqlCommand command = new SqlCommand("UPDATE dbo.WAPITenantCompanySecret SET ExternalCompanyCode = @NewCode WHERE ExternalCompanyCode = @OldCode AND TenantId = @TenantId")
            { Connection = conn };
            command.Parameters.AddWithValue("@NewCode", renamedCompanyCode);
            command.Parameters.AddWithValue("@OldCode", tenants.WindowsServiceSettingsObject.CompanyCode);
            command.Parameters.AddWithValue("@TenantId", tenantid);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// The get company code from tenant db.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<ExternalCompanyTenantDb> GetCompanyCodeFromTenantDb()
        {
            IList<ExternalCompanyTenantDb> output = new List<ExternalCompanyTenantDb>();
            SqlConnection conn = new SqlConnection(this.tenant.TenantDataBaseConnectionString);
            const string Sql = @"SELECT * FROM dbo.WAPITenantCompanySecret";
            SqlCommand command = new SqlCommand(Sql)
            { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new ExternalCompanyTenantDb
                    {
                        Id = (int)rd["Id"],
                        ExternalCompanyCode = (string)rd["ExternalCompanyCode"],
                        TenantId = (int)rd["TenantId"]
                    };
                    output.Add(row);
                }

                return output;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The update WAPI allowed services.
        /// </summary>
        /// <param name="tenants">
        /// The tenants.
        /// </param>
        /// <param name="serviceType">
        /// The service type.
        /// </param>
        public void UpdateWapiAllowedServices(ITenants tenants, WapiAllowedServiceType serviceType)
        {
            SqlConnection conn = new SqlConnection(this.tenant.AdvantageDataBaseConnectionString);
            const string Sql = @"UPDATE dbo.syWapiAllowedServices SET Url = @url WHERE Id = @id";
            SqlCommand command = new SqlCommand(Sql) { Connection = conn };
            command.Parameters.AddWithValue("@url", serviceType.Url);
            command.Parameters.AddWithValue("@id", serviceType.Id);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get external operations.
        /// </summary>
        /// <param name="sqlQuery">
        /// The SQL query.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<WapiOperationSettingOutputModel> GetExternalOperations(string sqlQuery)
        {
            IList<WapiOperationSettingOutputModel> output = new List<WapiOperationSettingOutputModel>();
            SqlConnection conn = new SqlConnection(this.tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(sqlQuery)
            { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new WapiOperationSettingOutputModel
                    {
                        ID = (int)rd["Id"],
                        CodeOperation = (string)rd["CodeOperation"],
                        ExternalUrl = (string)rd["ExternalUrl"],
                        CodeExtCompany = (string)rd["CompanyCode"],
                        DateLastExecution = (DateTime)rd["DateLastExecution"],
                        PollSecOnDemandOperation = (int)rd["PollSecondForOnDemandOperation"],
                        CodeExtOperationMode = (string)rd["ExternalOperationMode"],
                        ConsumerKey = (string)rd["ConsumerKey"],
                        OperationSecInterval = (int)rd["OperationSecondTimeInterval"],
                        FlagOnDemandOperation = (bool)rd["FlagOnDemandOperation"] ? 1 : 0,
                        IsActiveOperation = (bool)rd["IsActive"],
                        PrivateKey = (string)rd["PrivateKey"],
                        CodeWapiServ = (string)rd["FirstAllowedService"],
                        SecondCodeWapiServ = (string)rd["SecondAllowedService"],
                    };
                    output.Add(row);
                }

                return output;
            }
            finally
            {
                conn.Close();
            }
        }

        public Tuple<bool, string> TestDatabaseConexion(string sqlConnection)
        {
            SqlConnection conn = new SqlConnection(sqlConnection);
            try
            {
                conn.Open(); // throws if invalid
                return new Tuple<bool, string>(true, string.Empty);
            }
            catch (Exception e)
            {
                var message = Common.CollectAllExceptionsMessages(e);
                return new Tuple<bool, string>(false, message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}