﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CommonDb.cs" company="FAME">
//   2014
// </copyright>
// <summary>
//   Defines the CommonDb type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.DataLayer
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using GLibrary.GTypes;

    /// <summary>
    /// The common DB.
    /// </summary>
    public class CommonDb
    {
        /// <summary>
        /// The get advantage connection string.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="AdvantageDbInfo"/>.
        /// </returns>
        public AdvantageDbInfo GetAdvantageConnectionString(ITenants tenant)
        {
            var info = new AdvantageDbInfo();
            SqlConnection conn = new SqlConnection(tenant.TenantDataBaseConnectionString);
            SqlCommand command =
                new SqlCommand("SELECT * FROM Tenant WHERE TenantName = @TenantName") { Connection = conn };
            command.Parameters.AddWithValue("@TenantName", tenant.TenantName);
            SqlCommand command1 =
                new SqlCommand(
                    "SELECT [Key] FROM ApiAuthenticationKey WHERE TenantId = @TenantId")
                    {
                        Connection = conn
                    };
            conn.Open();
            int tenantId = 0;

            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.SingleRow);
                while (rd.Read())
                {
                    info.ConnectionString =
                        $"server={rd["ServerName"]};Database={rd["DataBaseName"]};Uid={rd["UserName"]};password={rd["Password"]}";
                    tenantId = (int)rd["TenantId"];
                }

                rd.Close();

                command1.Parameters.AddWithValue("@TenantId", tenantId);
               var key = command1.ExecuteScalar();
                if (key == null)
                {
                    throw new ApplicationException("The API key for the tenant is returning null, please configure tenant API key");
                }

                info.ApiKey = key.ToString();

                return info;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
