﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbServices.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The DB services.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.DataLayer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Transactions;

    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The DB services.
    /// </summary>
    public class DbServices
    {
        /// <summary>
        /// The run script.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int RunScript(string query, ITenants tenant)
        {
            SqlConnection conn = new SqlConnection
            {
                ConnectionString = tenant.AdvantageDataBaseConnectionString
            };
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                var rd = command.ExecuteNonQuery();
                return rd;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The run script.
        /// run a script using a arbitrary connection string
        /// entered as parameter.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int RunScript(string query, string connection)
        {
            SqlConnection conn = new SqlConnection
                                     {
                                         ConnectionString = connection
                                     };
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                var rd = command.ExecuteNonQuery();
                return rd;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The run script with parameter.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="connection">
        /// The connection.
        /// </param>
        /// <param name="parameterList">
        /// The parameter list.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        /// <summary>
        /// The run script.
        /// </summary>
        /// <param name="script">
        /// The script.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="shouldStripGoStatement">
        /// The should strip go statement.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        public static void RunScript(string script, string connectionString, bool shouldStripGoStatement = true)
        {
            int blockId = 0;
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                SqlCommand command;

                if (shouldStripGoStatement)
                {
                    script = script.Replace("--GO", "--").Replace("-- GO", "-- ").Replace("/* GO", "/*")
                        .Replace("/*GO", "/*");

                    string[] scripts = script.Split(new string[] { "GO\r\n", "GO ", "GO\t", "GO\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var splitScript in scripts)
                    {
                        command = new SqlCommand(
                            splitScript,
                            connection);
                        command.CommandTimeout = int.MaxValue;
                        command.ExecuteNonQuery();
                        blockId += 1;
                    }
                }
                else
                {
                    command = new SqlCommand(
                        script,
                        connection);
                    command.CommandTimeout = int.MaxValue;
                    command.ExecuteNonQuery();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }
        public int RunScriptWithParameter(string query, string connection, Dictionary<string, object> parameterList)
        {
            SqlConnection conn = new SqlConnection
                                     {
                                         ConnectionString = connection
            };
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            foreach (KeyValuePair<string, object> pair in parameterList)
            {
                command.Parameters.AddWithValue(pair.Key, pair.Value);
            }

            conn.Open();
            try
            {
                var rd = command.ExecuteNonQuery();
                return rd;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The execute SQL scripts with go.
        /// </summary>
        /// <param name="sql">
        /// The SQL.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int ExecuteSqlSriptsWithGo(string sql, ITenants tenant)
        {
            try
            {
                RunScript(sql, tenant.AdvantageDataBaseConnectionString, true);
                return 0;
            }
            catch (Exception e)
            {
                return 1;
            }
        }

        public int ExecuteSqlSriptsWithGo(string sql, string connectionString)
        {
            try
            {
                RunScript(sql, connectionString, true);
                return 0;
            }
            catch (Exception e)
            {
                return 1;
            }
        }

        #region Database Control History Version

        /// <summary>
        /// The create data base version.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        public int CreateDataBaseVersion(ITenants tenant, DbVersion version)
        {
            // Read File
            var query = File.ReadAllText("Scripts/CreateDatabaseVersion.sql");
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);

            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@Major", version.Major);
            command.Parameters.AddWithValue("@Minor", version.Minor);
            command.Parameters.AddWithValue("@Build", version.Build);
            command.Parameters.AddWithValue("@Revision", version.Revision);
            command.Parameters.AddWithValue("@Description", version.Description);
            command.Parameters.AddWithValue("@User", version.ModUser);
            conn.Open();
            try
            {
                var rd = command.ExecuteNonQuery();
                return rd;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get DB current version history.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// if the table return more or none value raise a error
        /// because should be only one current state
        /// </exception>
        public DbVersion GetDbCurrentVersionHistory(ITenants tenant)
        {
            var dv = new List<DbVersion>();
            var dbo = new CommonDb();
            SqlConnection conn = new SqlConnection
            {
                ConnectionString = tenant.AdvantageDataBaseConnectionString // dbo.GetAdvantageConnectionString(tenant).ConnectionString
            };

            SqlCommand command = new SqlCommand("SELECT * FROM dbo.syVersionHistory WHERE VersionEnd IS NULL") { Connection = conn };
            conn.Open();
            try
            {
                SqlDataReader rd = command.ExecuteReader(CommandBehavior.Default);
                while (rd.Read())
                {
                    var row = new DbVersion
                    {
                        Id = (int)rd["Id"],
                        Major = (int)rd["Major"],
                        Minor = (int)rd["Minor"],
                        Build = (int)rd["Build"],
                        Revision = (int)rd["Revision"],
                        VersionBegin = (DateTime)rd["VersionBegin"],
                        VersionEnd = (rd["VersionEnd"] == DBNull.Value ? (ValueType)null : (DateTime)rd["VersionEnd"]) as DateTime?,
                        Description = rd["Description"].ToString(),
                        ModUser = rd["ModUser"].ToString()
                    };
                    dv.Add(row);
                }

                if (dv.Count != 1)
                {
                    throw new ApplicationException(
                          "Version History Table is corrupt, or it is empty or two or more values are declared as current version");
                }

                return dv[0];
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The set current version.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        public DbVersion SetCurrentVersion(ITenants tenant, DbVersion version)
        {
            // Read File
            var query = File.ReadAllText("Scripts/UpdateDatabaseVersion.sql");

            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@Major", version.Major);
            command.Parameters.AddWithValue("@Minor", version.Minor);
            command.Parameters.AddWithValue("@Build", version.Build);
            command.Parameters.AddWithValue("@Revision", version.Revision);
            command.Parameters.AddWithValue("@Description", version.Description);
            command.Parameters.AddWithValue("@User", version.ModUser);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
                return version;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The exists version table.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExistsVersionTable(ITenants tenant)
        {
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            var query = "IF EXISTS ( SELECT *  FROM   INFORMATION_SCHEMA.TABLES  WHERE  TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'syVersionHistory' ) SELECT  1; ELSE  SELECT  0";
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                var rd = command.ExecuteScalar();
                return Convert.ToBoolean(rd);
            }
            finally
            {
                conn.Close();
            }
        }

        #endregion

        /// <summary>
        /// The get advantage database level of compatibility.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetAdvantageDatabaseLevelOfCompatibility(ITenants tenant)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(tenant.AdvantageDataBaseConnectionString);
            string database = builder.InitialCatalog;
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand("SELECT compatibility_level FROM sys.databases WHERE name = @DatabaseName") { Connection = conn };
            command.Parameters.AddWithValue("@DatabaseName", database);
            conn.Open();
            try
            {
                var rd = command.ExecuteScalar();
                return Convert.ToInt32(rd);
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The change compatibility level.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="setting">
        /// The setting.
        /// </param>
        public void ChangeCompatibilityLevel(ITenants tenant, int setting)
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(tenant.AdvantageDataBaseConnectionString);
            string database = builder.InitialCatalog;
            var query = $"ALTER DATABASE [{database}] SET COMPATIBILITY_LEVEL = {setting};";
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The verify view <code>arStudAddresses</code>.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool VerifyViewArStudAddresses(ITenants tenant)
        {
            var query = "IF EXISTS  (SELECT * FROM sys.views where name = 'arStudAddresses') SELECT 1 ELSE SELECT 0";
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                var result = (int)command.ExecuteScalar();
                return result == 1;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// Test if the database exist.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ExistsDatabase(ITenants tenant, string databaseName)
        {
            var query = $"IF EXISTS (SELECT NAME FROM master.sys.databases WHERE name = '{databaseName}') SELECT 1 ELSE SELECT 0";
            SqlConnection conn = new SqlConnection(tenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            conn.Open();
            try
            {
                var result = (int)command.ExecuteScalar();
                return result == 1;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The execute attendance job.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="selectedTenant">
        /// The selected tenant.
        /// </param>
        public void ExecuteAttendanceJob(string query, ITenants selectedTenant)
        {
            // Extract from connection string the database name.
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(selectedTenant.AdvantageDataBaseConnectionString);
            var dbname = builder.InitialCatalog;
            SqlConnection conn = new SqlConnection(selectedTenant.AdvantageDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@DatabaseName", dbname);
            command.Parameters.AddWithValue("@JobStringName", "_AttendanceJob");

            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get API key.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        internal string GetApiKey(ITenants tenant)
        {
            var query = File.ReadAllText("Scripts/GetApiKey.sql");
            
            SqlConnection conn = new SqlConnection(tenant.TenantDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@TenantName", tenant.TenantName);
    
            conn.Open();
            try
            {
               var value = command.ExecuteScalar();
                return value?.ToString() ?? string.Empty;
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// The set advantage configuration value.
        /// </summary>
        /// <param name="tenants">
        /// The tenants.
        /// </param>
        /// <param name="settingKeyName">
        /// The setting key name.
        /// </param>
        /// <param name="settingValue">
        /// The setting value.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        internal void SetAdvantageConfigurationValue(ITenants tenants, string settingKeyName, string settingValue, string databaseName)
        {
            // Extract from connection string the database name.
            SqlConnectionStringBuilder builder =
                new SqlConnectionStringBuilder(tenants.AdvantageDataBaseConnectionString)
                {
                    InitialCatalog = databaseName
                };
            SqlConnection conn = new SqlConnection(builder.ConnectionString);
            var query = "UPDATE dbo.syConfigAppSetValues SET [Value] = @SettingValue  WHERE SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = @SettingKeyName)";
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@SettingValue", settingValue);
            command.Parameters.AddWithValue("@SettingKeyName", settingKeyName);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The set environment.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="environmentName">
        /// The environment name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>. it is the Environment ID to be used in Tenant
        /// </returns>
        internal int SetEnvironment(ITenants tenantObject, string environmentName)
        {
            SqlConnection conn = new SqlConnection(tenantObject.TenantDataBaseConnectionString);
            var query = File.ReadAllText("Scripts/SetEnvironmentTenant.sql");
            var query2 = "SELECT EnvironmentId FROM dbo.Environment WHERE EnvironmentName = @EnviromentName";

            SqlCommand command = new SqlCommand(query) { Connection = conn };
            SqlCommand command2 = new SqlCommand(query2) { Connection = conn };
            command.Parameters.AddWithValue("@EnviromentName", environmentName);
            command2.Parameters.AddWithValue("@EnviromentName", environmentName);
            conn.Open();
            try
            {
                object ret;
                using (TransactionScope scope = new TransactionScope())
                {
                    command.ExecuteNonQuery();
                    ret = command2.ExecuteScalar();
                    scope.Complete();
                }

                return ret != null ? Convert.ToInt32(ret) : 0;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The set tenant name.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="environmentId">
        /// The environment id.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>. The TenantId
        /// </returns>
        internal int SetTenantName(ITenants tenantObject, int environmentId, string databaseName)
        {
            SqlConnection conn = new SqlConnection(tenantObject.TenantDataBaseConnectionString);

            SqlConnectionStringBuilder builder =
                new SqlConnectionStringBuilder(tenantObject.AdvantageDataBaseConnectionString);

            var query = File.ReadAllText("Scripts/SetTenantTable.sql");
            var query2 = "SELECT TenantId FROM dbo.Tenant WHERE TenantName = @TenantName";
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            SqlCommand command2 = new SqlCommand(query2) { Connection = conn };
            command.Parameters.AddWithValue("@DatabaseName", databaseName);
            command.Parameters.AddWithValue("@ServerName", builder.DataSource);
            command.Parameters.AddWithValue("@UserName", builder.UserID);
            command.Parameters.AddWithValue("@Password", builder.Password);
            command.Parameters.AddWithValue("@EnvironmentId", environmentId);

            command2.Parameters.AddWithValue("@TenantName", databaseName);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
                var ret = command2.ExecuteScalar();
                return ret != null ? Convert.ToInt32(ret) : 0;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The set API key.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="tenantId">
        /// The tenant ID.
        /// </param>
        /// <param name="apiKey">
        /// The API Key.
        /// </param>
        internal void SetApiKey(ITenants tenantObject, int tenantId, Guid apiKey)
        {
            SqlConnection conn = new SqlConnection(tenantObject.TenantDataBaseConnectionString);
            var query = File.ReadAllText("Scripts/SetApiAuthenticationKeyTable.sql");
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@TenantId", tenantId);
            command.Parameters.AddWithValue("@ApiKey", apiKey);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The set default user.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        /// <param name="tenantId">
        /// The tenant id.
        /// </param>
        internal void SetDefaultUser(ITenants tenantObject, int tenantId)
        {
            SqlConnection conn = new SqlConnection(tenantObject.TenantDataBaseConnectionString);
            var query = File.ReadAllText("Scripts/CreateTenantUser.sql");
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@TenantId", tenantId);
            command.Parameters.AddWithValue("@IsDefaultTenant", 1);
            command.Parameters.AddWithValue("@IsSupportUser", 1);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The update advantage tenant.
        /// </summary>
        /// <param name="tenantObject">
        /// The tenant object.
        /// </param>
        internal void UpdateAdvantageTenant(ITenants tenantObject)
        {
            SqlConnection conn = new SqlConnection(tenantObject.TenantDataBaseConnectionString);
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(tenantObject.AdvantageDataBaseConnectionString);
            string dbase = $"[{sb.InitialCatalog}]";
            var query = "[UpdateAdvantageSupportUser]";
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@DatabaseName", dbase);
            conn.Open();
            try
            {
                command.ExecuteNonQuery();
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// The get tenant ID.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        internal int GetTenantId(ITenants tenant)
        {
            var query = "SELECT TenantId FROM Tenant WHERE TenantName = @TenantName";
            SqlConnection conn = new SqlConnection(tenant.TenantDataBaseConnectionString);
            SqlCommand command = new SqlCommand(query) { Connection = conn };
            command.Parameters.AddWithValue("@TenantName", tenant.TenantName);
            conn.Open();
            try
            {
                var result = (int)command.ExecuteScalar();
                return result;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
