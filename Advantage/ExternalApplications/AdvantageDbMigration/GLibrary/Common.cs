﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Common.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the Common type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace GLibrary
{
    public class Common
    {

        private static DateTime lastMeasure;

        /// <summary>
        /// The get time span.
        /// </summary>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public static DateTime GetTimeSpan()
        {

            var stamp = DateTime.Now;
            lastMeasure = stamp;
            return stamp;
        }

        /// <summary>
        /// The time difference between the  last measure and now.
        /// </summary>
        /// <returns>
        /// The <see cref="TimeSpan"/>.
        /// </returns>
        public static TimeSpan TimeDifference()
        {
            var stamp = DateTime.Now;
            var diff = stamp - lastMeasure;
            lastMeasure = stamp;
            return diff;
        }

        /// <summary>
        /// The write line. Return the parameter text with a line feed added
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string WriteLine(string text)
        {
            return text + Environment.NewLine;
        }

        /// <summary>
        /// Collect all exceptions messages.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CollectAllExceptionsMessages(Exception ex)
        {
            var output = ex.Message;
            var exce = ex;
            while (exce.InnerException != null)
            {
                exce = exce.InnerException;
                output = $" {Environment.NewLine} {exce.Message}";
            }

            return output;
        }
    }
}
