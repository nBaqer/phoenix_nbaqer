﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WindowsServiceConfigurationBo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the WindowsServiceConfigurationBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Business
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Permissions;
    using System.Xml;

    using GLibrary.GTypes;

    /// <summary>
    /// The windows service configuration business object.
    /// </summary>
    public class WindowsServiceConfigurationBo
    {
        /// <summary>
        /// The refresh configuration.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="oldvalue">
        /// The old value.
        /// </param>
        public static void RefreshConfiguration(ITenants tenant, string key, string oldvalue = "")
        {
            var pathToWindowsService = tenant.AdvantagePhysicalBaseAddress.EndsWith("\\")
                                           ? tenant.AdvantagePhysicalBaseAddress
                                           : $"{tenant.AdvantagePhysicalBaseAddress}\\";
            pathToWindowsService = $"{pathToWindowsService}WapiWindowsService\\Configs\\AppSettings.config";
            if (key.ToUpper() == "SECRETCOMPANYKEYS")
            {
                // Read the configuration file
                XmlDocument doc = new XmlDocument();
                doc.Load(pathToWindowsService);
                XmlNodeList xxxnList = doc.SelectNodes("/configuration/appSettings/add[@key='SecretCompanyKeys']");
                if (xxxnList != null && xxxnList.Count > 0)
                {
                    var secrets = xxxnList.Item(0).Attributes["value"].Value;
                    string[] separator = { "," };
                    string[] codesList = secrets.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    IList<string> newcode = new List<string>();
                    foreach (string s in codesList)
                    {
                        if (s != oldvalue & s != tenant.WindowsServiceSettingsObject.CompanyCode)
                        {
                            newcode.Add(s);
                        }
                    }

                    newcode.Add(tenant.WindowsServiceSettingsObject.CompanyCode);

                    var newsecret = string.Empty;
                    foreach (string s in newcode)
                    {
                        newsecret += $"{s},";
                    }

                    newsecret = newsecret.TrimEnd(',');
                    xxxnList.Item(0).Attributes["value"].Value = newsecret;
                    doc.Save(pathToWindowsService);
                }
                else
                {
                    throw new ApplicationException(" Application Setting node [SecretCompanyKeys] was not found");
                }
            }
        }

        [PrincipalPermission(SecurityAction.Demand, Role = @"BUILTIN\Administrators")]
        public static void SetAppSettingInWindowService(string filename, string key, string value)
        {
            ////var pathToWindowsService = tenant.WindowsServiceSettingsObject.WindowsServicePathToConfiguration;
            if (!File.Exists(filename))
            {
                throw new ApplicationException($"File {filename} does not exists");
            }


            // Read the configuration file
            XmlDocument doc = new XmlDocument();
            doc.Load(filename);
            XmlNodeList xxxnList = doc.SelectNodes($"/appSettings/add[@key='{key}']");
            if (xxxnList != null && xxxnList.Count > 0)
            {
                var secrets = xxxnList.Item(0).Attributes["value"].Value;
                string[] separator = { "," };
                string[] codesList = secrets.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                IList<string> newcode = new List<string>();
                foreach (string s in codesList)
                {
                    if (s != value)
                    {
                        newcode.Add(s);
                    }
                }

                newcode.Add(value);

                var newsecret = string.Empty;
                foreach (string s in newcode)
                {
                    newsecret += $"{s},";
                }

                newsecret = newsecret.TrimEnd(',');
                xxxnList.Item(0).Attributes["value"].Value = newsecret;
                doc.Save(filename);
            }
            else
            {
                throw new ApplicationException($"Application Setting node [{key}] was not found");
            }
        }
    }
}
