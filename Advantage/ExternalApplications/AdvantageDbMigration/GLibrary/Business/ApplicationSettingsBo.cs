﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApplicationSettingsBo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the ApplicationSettingsBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Business
{
    using GLibrary.GTypes;

    /// <summary>
    /// The application settings bo.
    /// </summary>
    public class ApplicationSettingsBo
    {
        /// <summary>
        /// The create report service.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateReportService(ITenants tenant)
        {
            var settings = tenant.ConfigurationSettingsObject;
            return $"http://{settings.ReportServerVirtualBaseAddress}:{settings.ReportServerPort}/ReportServer/ReportService2005.asmx";
        }

        /// <summary>
        /// The create execution service.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateExecutionService(ITenants tenant)
        {
            var settings = tenant.ConfigurationSettingsObject;
            return $"http://{settings.ReportServerVirtualBaseAddress}:{settings.ReportServerPort}/ReportServer/ReportExecution2005.asmx";
        }

        /// <summary>
        /// The create http address service.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateHttpAddressService(ITenants tenant)
        {
            var settings = tenant.ConfigurationSettingsObject;
            return $"{settings.Protocol}://{settings.AdvantageVirtualBaseAddress}/Advantage/Current/Services";
        }

        /// <summary>
        /// The create HTTP address host.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string CreateHttpAddressSite(ITenants tenant)
        {
            var settings = tenant.ConfigurationSettingsObject;
            return $"{settings.Protocol}://{settings.AdvantageVirtualBaseAddress}/Advantage/Current/Site";
        }
    }
}
