﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FastReportBo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the FastReportBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Business
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using GLibrary.Facade;
    using GLibrary.GTypes;

    /// <summary>
    /// The fast report BO.
    /// </summary>
    public class FastReportBo
    {
        /// <summary>
        /// The get fast report configuration.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <returns>
        /// The <see cref="FastReportObject"/>.
        /// </returns>
        public FastReportObject GetFastReportConfiguration(ITenants info)
        {
            var fro = new FastReportObject
                          {
                              ReportServerDbConnectionString =
                                  AdvantageConfigurationSetting.GetSingleConfigurationSetting(
                                      "ReportServerConexion",
                                      info),
                              ReportServerWebUrl =
                                  AdvantageConfigurationSetting.GetSingleConfigurationSetting(
                                      "ReportServerInstance",
                                      info)
                          };

            // Get the two configuration settings
            return fro;
        }

        /// <summary>
        /// The get settings from configuration.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="FastReportObject"/>.
        /// </returns>
        public FastReportObject GetSettingsFromConfiguration(ITenants tenant)
        {
            var fastreport = new FastReportObject
                                 {
                                     ReportServerDbConnectionString =
                                         tenant.ConfigurationSettingsObject.ReportServerConexion,
                                     ReportServerWebUrl =
                                         tenant.ConfigurationSettingsObject.ReportServerInstance,
                                     ReportUserImpersonator = tenant.ReportUserImpersonator
                                 };
            return fastreport;
        }

        /// <summary>
        /// The verification fast report.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="fastreport">
        /// The fast report.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string VerificationApplicationSettingsFastReport(ITenants tenant, FastReportObject fastreport)
        {
            string message = string.Empty;
            
            // Try to connect with the Connection String
            var tuple = WapiServicesFacade.TestDatabaseConexion(tenant, fastreport.ReportServerDbConnectionString);
            var tuple2 = WapiServicesFacade.TestUrlExists(fastreport.ReportServerWebUrl);
            
            if (tuple.Item1 == false)
            {
                message += $"Fast Report Database wrong connection string. System return: {tuple.Item2}";
                message += Environment.NewLine;
            }

            if (tuple2.Item1 == false)
            {
                message += $"Report Server URL error. System return: {tuple2.Item2}";
                message += Environment.NewLine;
            }

            var username = fastreport.ReportUserImpersonator;
            if (string.IsNullOrWhiteSpace(username))
            {
                message += $"Report Server Impersonator setting is not configured (it is blank or null)";
                message += Environment.NewLine;
            }

            return message;
        }

        /// <summary>
        /// The set fast report configuration setting.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="fastreport">
        /// The fast report.
        /// </param>
        public void SetFastReportConfigurationSetting(ITenants tenant, FastReportObject fastreport)
        {
           AdvantageConfigurationSetting.SetSingleConfigurationSetting(
                tenant,
                "ReportServerConexion",
                fastreport.ReportServerDbConnectionString);

            AdvantageConfigurationSetting.SetSingleConfigurationSetting(
                tenant,
                "ReportServerInstance",
                fastreport.ReportServerWebUrl);

            // This is a script to running
            var username = fastreport.ReportUserImpersonator;
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ApplicationException("Please configure Report User Name. Report User name is the domain and name of the user,used to impersonate advantage to access report server");
            }

            // Insert user impersonator in the local database
            string query1 = File.ReadAllText("Scripts/CreateUser.sql");
            query1 = query1.Replace("@UserName", username);
            query1 = query1.Replace("@Role", "db_owner");
            var connection1 = tenant.AdvantageDataBaseConnectionString;
            DbInstallation.RunScriptWithoutParameter(query1, connection1);

            // Try to insert the windows user impersonator in Report database. If exists the procedure do nothing
            string query = File.ReadAllText("Scripts/CreateUser.sql");
            query = query.Replace("@UserName", username);
            query = query.Replace("@Role", "db_owner");
            var connection = tenant.ConfigurationSettingsObject.ReportServerConexion;
            DbInstallation.RunScriptWithoutParameter(query, connection);
        }
    }
}
