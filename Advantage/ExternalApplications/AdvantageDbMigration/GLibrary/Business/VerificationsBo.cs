﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VerificationsBo.cs" company="FAME">
//  2017  
// </copyright>
// <summary>
//   Defines the VerificationsBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GLibrary.Business
{
    using System;

    using GLibrary.Facade;
    using GLibrary.GTypes;

    /// <summary>
    /// The verifications bo.
    /// </summary>
    public static class VerificationsBo
    {
        /// <summary>
        /// The test web api key.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// </returns>
        public static Tuple<string,Boolean> TestWebApiKey(ITenants tenant)
        {
            // API key comparation
            var advantageApiKey = AdvantageConfigurationSetting.GetSingleConfigurationSetting("AdvantageServiceAuthenticationKey", tenant);
            var apikey = DbInstallation.GetApiKey(tenant);
            
            if (apikey == null || apikey.Length != 36)
            {
                var tulpe = Tuple.Create(Common.WriteLine($"TenantDB has a bad declaration of API key for tenant {tenant.TenantDisplayName}, Value = {apikey}"), false);
                return tulpe;
            }

            if (advantageApiKey == null || apikey.Length != 36)
            {
                var tulpe = Tuple.Create(Common.WriteLine($"Advantage {tenant.TenantDisplayName} has a bad WEB API value = {apikey}"), false);
                return tulpe;
            }

            var apikeyPartial = $"{apikey.Substring(0, 15)}...";
            var advantageApiKeyPartial = $"{advantageApiKey.Substring(0, 15)}...";

            var outputText = Common.WriteLine($"ApiKey for tenant (partial) {tenant.TenantDisplayName} in TenantAuthDb = {apikeyPartial}...");
            outputText += Common.WriteLine($"ApiKey for Advantage (partial) {tenant.TenantDisplayName} in Advantage = {advantageApiKeyPartial}...");
            if(apikey.ToUpperInvariant() != advantageApiKey.ToUpperInvariant())
            {
                outputText += Common.WriteLine($"The WEB API key are not equal in TenantAuthDB and Advantage for Tenant = {tenant.TenantDisplayName}!!");
                outputText += Common.WriteLine("You can still upgrade the DB but you must correct this problem before go to production!!");
                var tulpe = Tuple.Create(outputText, false);
                return tulpe;
            }

            return Tuple.Create(outputText, true);
        }
    }
}
