﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StartingClient.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the StartingClient type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDbMigration
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    using GLibrary;
    using GLibrary.Business;
    using GLibrary.Facade;
    using GLibrary.GTypes;
    using GLibrary.GVerification;

    using Microsoft.Data.ConnectionUI;

    using Newtonsoft.Json;

    /// <summary>
    /// The starting client.
    /// </summary>
    public partial class StartingClient : Form
    {
        /// <summary>
        /// The tenant object.
        /// </summary>
        private ITenants tenantObject;

        /// <summary>
        /// The process time.
        /// </summary>
        private DateTime processTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="StartingClient"/> class.
        /// </summary>
        public StartingClient()
        {
            this.InitializeComponent();
            this.tenantObject = Tenants.Factory();
            this.cbProtocol.SelectedIndex = 0;
            this.tbDbPath.Enabled = false;
            this.btnBrowse.Enabled = false;
        }

        #region Configuration

        /// <summary>
        /// The button create tenant click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCreateTenantClick(object sender, EventArgs e)
        {
            // Create the dialog...
            DataConnectionDialog dcd = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dcd);
            dcd.SelectedDataSource = DataSource.SqlDataSource;
            dcd.SelectedDataProvider = DataProvider.SqlDataProvider;
            try
            {
                // See if there a object previously created
                if (!string.IsNullOrWhiteSpace(this.tbConnectionTenant.Text))
                {
                    dcd.ConnectionString = this.tbConnectionTenant.Text;
                }

                // Show dialog
                var result = DataConnectionDialog.Show(dcd);
                if (result == DialogResult.OK)
                {
                    // Save connection to text
                    this.tbConnectionTenant.Text = dcd.ConnectionString;
                }

                dcd.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(
                    this,
                    exception.Message,
                    @"Error in Connection String",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                dcd.Dispose();
            }
        }

        /// <summary>
        /// The button create advantage click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCreateAdvantageClick(object sender, EventArgs e)
        {
            // Create the dialog...
            DataConnectionDialog dcd = new DataConnectionDialog();
            DataSource.AddStandardDataSources(dcd);
            dcd.SelectedDataSource = DataSource.SqlDataSource;
            dcd.SelectedDataProvider = DataProvider.SqlDataProvider;
            try
            {
                // See if there a object previously created
                if (!string.IsNullOrWhiteSpace(this.tbConnectionAdvantage.Text))
                {
                    dcd.ConnectionString = this.tbConnectionAdvantage.Text;
                }

                // Show dialog
                var result = DataConnectionDialog.Show(dcd);
                if (result == DialogResult.OK)
                {
                    // Save connection to text
                    this.tbConnectionAdvantage.Text = dcd.ConnectionString;
                }

                dcd.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(
                    this,
                    exception.Message,
                    @"Error in Connection String",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                dcd.Dispose();
            }
        }

        /// <summary>
        /// The button confirm click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnConfirmClick(object sender, EventArgs e)
        {
            // Configure the tenant object
            this.tenantObject.AdvantageDataBaseConnectionString = this.tbConnectionAdvantage.Text;
            this.tenantObject.TenantDataBaseConnectionString = this.tbConnectionTenant.Text;
            this.tenantObject.ConfigurationSettingsObject.SchoolName = this.tbSchoolName.Text;
            this.tenantObject.ConfigurationSettingsObject.Protocol = this.cbProtocol.Text;
            this.tenantObject.ConfigurationSettingsObject.AdvantageVirtualBaseAddress = this.tbVirtualAddress.Text;
            this.tenantObject.ConfigurationSettingsObject.ReportServerVirtualBaseAddress = this.tbReportServerDomain.Text;
            this.tenantObject.ConfigurationSettingsObject.ReportServerPort = this.tbReportServerPort.Text;
            if (this.tenantObject.ConfigurationSettingsObject.ServiceLayerAdvantageApiKey == Guid.Empty)
            {
               this.tenantObject.ConfigurationSettingsObject.ServiceLayerAdvantageApiKey = Guid.NewGuid(); 
            }

            if (this.tenantObject.ConfigurationSettingsObject.ServiceLayerSandBoxApiKey == Guid.Empty)
            {
                this.tenantObject.ConfigurationSettingsObject.ServiceLayerSandBoxApiKey = Guid.NewGuid();
            }

            this.tenantObject.IsRestoreDbRelocated = this.cbRelocate.Checked;
            this.tenantObject.RelocationStringForDbRestore = this.tbDbPath.Text;

            // Verify Info
            var error = TenantsVerifications.VerifyTenantStartedConfiguration(this.tenantObject);
            if (string.IsNullOrWhiteSpace(error))
            {
                // Verification OK.
                MessageBox.Show(
                    this,
                    @"Information Accepted",
                    @"Information Verification",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                // Show info in text - box
                this.tbConfig.Text =
                    $@"SchoolName: {this.tenantObject.ConfigurationSettingsObject.SchoolName}{Environment.NewLine}";
                this.tbConfig.Text += $@"Tenant Connection: {this.tenantObject.TenantDataBaseConnectionString}{Environment.NewLine}";
                this.tbConfig.Text += $@"Advantage Connection: {this.tenantObject.AdvantageDataBaseConnectionString}{Environment.NewLine}";
                this.tbConfig.Text += $@"Report Service: {ApplicationSettingsBo.CreateReportService(this.tenantObject)}{Environment.NewLine}";
                this.tbConfig.Text += $@"Report Execution Service: {ApplicationSettingsBo.CreateExecutionService(this.tenantObject)}{Environment.NewLine}";
                this.tbConfig.Text += $@"Advantage Service Uri: {ApplicationSettingsBo.CreateHttpAddressService(this.tenantObject)}{Environment.NewLine}";
                this.tbConfig.Text += $@"Advantage Site Uri: {ApplicationSettingsBo.CreateHttpAddressSite(this.tenantObject)}{Environment.NewLine}";
                this.tbConfig.Text += $@"Advantage API Key: {this.tenantObject.ConfigurationSettingsObject.ServiceLayerAdvantageApiKey}{Environment.NewLine}";
                this.tbConfig.Text += $@"SandBox API Key: {this.tenantObject.ConfigurationSettingsObject.ServiceLayerSandBoxApiKey}{Environment.NewLine}";
                this.tbConfig.Text += $@"Is Relocated: {this.tenantObject.IsRestoreDbRelocated}{Environment.NewLine}";
                this.tbConfig.Text += $@"DB Relocation Path: {this.tenantObject.RelocationStringForDbRestore}{Environment.NewLine}";
            }
            else
            {
                // Verification Fail.
                MessageBox.Show(
                    this,
                    error,
                    @"Information Verification",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// The button browse click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnBrowseClick(object sender, EventArgs e)
        {
            var dir = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
                Description = @"Select the path where you want to store the DB"
            };
            if (dir.ShowDialog(this) == DialogResult.OK)
            {
                this.tbDbPath.Text = dir.SelectedPath;
            }
        }

        #endregion

        #region Menu 

        /// <summary>
        /// The export configuration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ExportConfigurationToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var tenants = JsonConvert.SerializeObject(this.tenantObject);

                // Stored in a file
                var dialog = new SaveFileDialog
                {
                    DefaultExt = ".json",
                    Filter = @"txt file|*.txt|json file |*.json",
                    Title = @"Select file to store Started Configuration"
                };
                var res = dialog.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    // Store the info in the file
                    File.WriteAllText(dialog.FileName, tenants);
                    MessageBox.Show(this, $@"The Configuration was exported to file {dialog.FileName}", @"File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
                MessageBox.Show(
                    this,
                    @"Error in Configuration Format. Please re-edit configuration",
                    @"Error en Edition",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The import configuration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ImportConfigurationToolStripMenuItemClick(object sender, EventArgs e)
        {
            // Open Dialog to get configuration
            var open = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                DefaultExt = "txt",
                SupportMultiDottedExtensions = true,
                Filter = @".text files|*.txt| .json files|*.json"
            };
            if (open.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    var text = File.ReadAllText(open.FileName);
                    this.tenantObject = JsonConvert.DeserializeObject<Tenants>(text);

                    // Put the info in the controls
                    this.tbConnectionAdvantage.Text = this.tenantObject.AdvantageDataBaseConnectionString;
                    this.tbConnectionTenant.Text = this.tenantObject.TenantDataBaseConnectionString;
                    this.tbSchoolName.Text = this.tenantObject.ConfigurationSettingsObject.SchoolName;
                    this.tbVirtualAddress.Text = this.tenantObject.ConfigurationSettingsObject.AdvantageVirtualBaseAddress;
                    this.tbReportServerDomain.Text = this.tenantObject.ConfigurationSettingsObject.ReportServerVirtualBaseAddress;
                    this.tbReportServerPort.Text = this.tenantObject.ConfigurationSettingsObject.ReportServerPort;
                    this.cbProtocol.Text = this.tenantObject.ConfigurationSettingsObject.Protocol;
                    this.cbRelocate.Checked = this.tenantObject.IsRestoreDbRelocated;
                    this.tbDbPath.Text = this.tenantObject.RelocationStringForDbRestore;

                    MessageBox.Show(
                        this,
                        @"File Imported. Press Save to validate and finish the import process",
                        @"Importing",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        this,
                        $@"Error open the file: {exception.Message}",
                        @"Error Importing File",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// The return to main tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ReturnToMainToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Verification

        /// <summary>
        /// The button verification click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="ApplicationException">
        /// if something is wrong
        /// </exception>
        private void BtnVerificationClick(object sender, EventArgs e)
        {
            try
            {
                this.tbLogVerification.Clear();
                this.tbLogVerification.BackColor = Color.LightGray;
                this.tbLogVerification.Text = $@"Begin verification process at {DateTime.Now}";
                this.tbLogVerification.Text += Environment.NewLine;
                this.tbLogVerification.Text += $@"--------------------------------------------------------------------";
                this.tbLogVerification.Text += Environment.NewLine;

                // Verify that configuration is created
                if (this.tenantObject == null)
                {
                    throw new ApplicationException("Please, First Configure the school to Install");
                }

                // Verify Configuration
                var error = TenantsVerifications.VerifyTenantStartedConfiguration(this.tenantObject);
                if (!string.IsNullOrWhiteSpace(error))
                {
                    throw new ApplicationException(error);
                }

                // Verify that does not exists a DB named Advantage
                var result = DbInstallation.ExistsDatabase(this.tenantObject, "Advantage");
                if (result)
                {
                    throw new ApplicationException("A database with the name Advantage Exists. You can not install other DB with the same name. Please Delete it but prior verify that your client does not lose information.");
                }

                result = DbInstallation.ExistsDatabase(this.tenantObject, "SandBox");
                if (result)
                {
                    throw new ApplicationException("A database with the name SandBox Exists. You can not install other DB with the same name. Please Delete it but prior verify that your client does not lose information.");
                }

                this.tbLogVerification.Text += $@"Verification Process success. You can begin to install the new database.";
                this.tbLogVerification.Text += Environment.NewLine;
                this.tbLogVerification.BackColor = Color.LightGreen;
            }
            catch (Exception exception)
            {
                this.tbLogVerification.Text += @"Verification Fail. The following Error is reported:";
                this.tbLogVerification.Text += Environment.NewLine;
                this.tbLogVerification.Text += $@"{exception.Message}";
                this.tbLogVerification.Text += Environment.NewLine;
                this.tbLogVerification.BackColor = Color.Tomato;
            }

            this.tbLogVerification.Text += $@"-----------------------------------------------------------------------";
            this.tbLogVerification.Text += Environment.NewLine;
            this.tbLogVerification.Text += $@"Process finished at {DateTime.Now}";
            this.tbLogVerification.Text += Environment.NewLine;
        }

        #endregion

        #region Installations 

        /// <summary>
        /// The button begin starting click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private async void BtnBeginStartingClick(object sender, EventArgs e)
        {
            try
            {
                this.processTime = Common.GetTimeSpan();
                this.tbStarterInstallation.Clear();
                this.tbStarterInstallation.BackColor = Color.LightGray;
                this.tbStarterInstallation.Text = $@"Begin New Client Installation Process at {DateTime.Now}";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"--------------------------------------------------------------------";
                this.tbStarterInstallation.Text += Environment.NewLine;

                this.tbStarterInstallation.Text += $@"Verification Phase >> ";

                // Verify that configuration is created
                if (this.tenantObject == null)
                {
                    throw new ApplicationException("Fail. Please, First Configure the school to Install");
                }

                // Verify Configuration
                var error = TenantsVerifications.VerifyTenantStartedConfiguration(this.tenantObject);
                if (!string.IsNullOrWhiteSpace(error))
                {
                    throw new ApplicationException(error);
                }

                this.tbStarterInstallation.Text += @"Success.....";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin installation Phase..................";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin TenantAuthDb Installation >> ";
                 
                // if not exists Create Tenant
                var result1 = DbInstallation.ExistsDatabase(this.tenantObject, "TenantAuthDb");
                if (result1 == false)
                {
                    // Create tenant....
                    await DbInstallation.RestoreDatabaseSync(this.tenantObject, "TenantAuthDb", "\\StarterDb\\TenantAuthDb_Starter_2008.bak");
                    this.tbStarterInstallation.Text += @"TenantDb Database was successful Created";
               }
                else
                {
                    this.tbStarterInstallation.Text += @"TenantAuthDb creation skipped, because the Db exists previously:";
                }

                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin Advantage DB Installation >> ";

                this.tbStarterInstallation.Refresh();

                var result2 = DbInstallation.ExistsDatabase(this.tenantObject, "Advantage");

                // if not exists Create Advantage
                if (result2 == false)
                {
                    // Create Advantage....
                    await DbInstallation.RestoreDatabaseSync(this.tenantObject, "Advantage", "\\StarterDb\\A1_Starter_2008.bak");
                    this.tbStarterInstallation.Text += $@"Advantage Database was successful Created";
                }
                else
                {
                    this.tbStarterInstallation.Text += @"Advantage creation skipped, because the Db exists";
                }

                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin SandBox Installation >> ";
                this.tbStarterInstallation.Refresh();

                // if not exists create SandBox
                var result3 = DbInstallation.ExistsDatabase(this.tenantObject, "SandBox");
                if (result3 == false)
                {
                    // Create SandBox....
                    await DbInstallation.RestoreDatabaseSync(this.tenantObject, "SandBox", "\\StarterDb\\A1_Starter_2008.bak");
                    this.tbStarterInstallation.Text += $@"SandBox Database was successful Created";
                }
                else
                {
                    this.tbStarterInstallation.Text += @"Sandbox creation skipped, because the Db exists previously:";
                }

                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin Advantage Setting Configuration >> ";
                this.tbStarterInstallation.Refresh();

                // Configure application option (run script with the configuration)
                DbInstallation.SetAdvantageConfigurationValues(this.tenantObject, "Advantage");
                this.tbStarterInstallation.Text += @"Advantage Settings Where Configured";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Text += $@"Begin SandBox Setting Configuration >> ";
                this.tbStarterInstallation.Refresh();

                DbInstallation.SetAdvantageConfigurationValues(this.tenantObject, "SandBox");
                this.tbStarterInstallation.Text += @"SandBox Settings Where Configured";
                this.tbStarterInstallation.Text += Environment.NewLine;
               this.tbStarterInstallation.Text += Environment.NewLine;
 
                this.tbStarterInstallation.Text += @"Begin Tenant Configuration settings >> ";
                this.tbStarterInstallation.Refresh();

                // Configure Tenant with the two databases
                DbInstallation.ConfigureTenantAdvantage(this.tenantObject, "Live", "Advantage");
                this.tbStarterInstallation.Text += @"Advantage Configured in Tenant";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Refresh();
                DbInstallation.ConfigureTenantAdvantage(this.tenantObject, "SandBox", "SandBox");
                this.tbStarterInstallation.Text += @"SandBox Configured in Tenant";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.Refresh();
                this.tbStarterInstallation.BackColor = Color.LightGreen;
            }
            catch (Exception exception)
            {
                this.tbStarterInstallation.Text += @"Verification Fail. The following Error is reported:";
                this.tbStarterInstallation.Text += Environment.NewLine;
                var message = exception.Message;
                while (exception.InnerException != null)
                {
                    exception = exception.InnerException;
                    message += $"{Environment.NewLine}{exception.Message}";
                }
              
                this.tbStarterInstallation.Text += $@"{message}";
                this.tbStarterInstallation.Text += Environment.NewLine;
                this.tbStarterInstallation.BackColor = Color.Tomato;
            }

            this.tbStarterInstallation.Text += Environment.NewLine;
            this.tbStarterInstallation.Text += $@"-----------------------------------------------------------------------";
            this.tbStarterInstallation.Text += Environment.NewLine;
            this.tbStarterInstallation.Text += $@"Process finished at {DateTime.Now}";
            this.tbStarterInstallation.Text += Environment.NewLine;
        }

        /// <summary>
        /// The combo box relocate check state changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CbRelocateCheckStateChanged(object sender, EventArgs e)
        {
            this.tbDbPath.Enabled = this.cbRelocate.Checked;
            this.btnBrowse.Enabled = this.cbRelocate.Checked;
        }

        #endregion
    }
}
