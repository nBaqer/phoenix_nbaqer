﻿namespace AdvantageDbMigration
{
    partial class FormSetDbVersion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSetDbVersion));
            this.label1 = new System.Windows.Forms.Label();
            this.numericMajor = new System.Windows.Forms.NumericUpDown();
            this.numericMinor = new System.Windows.Forms.NumericUpDown();
            this.numericBuild = new System.Windows.Forms.NumericUpDown();
            this.numericRevision = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetValue = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericMajor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuild)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRevision)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LemonChiffon;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(534, 64);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // numericMajor
            // 
            this.numericMajor.Location = new System.Drawing.Point(130, 130);
            this.numericMajor.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericMajor.Name = "numericMajor";
            this.numericMajor.Size = new System.Drawing.Size(54, 20);
            this.numericMajor.TabIndex = 1;
            this.numericMajor.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // numericMinor
            // 
            this.numericMinor.Location = new System.Drawing.Point(201, 130);
            this.numericMinor.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericMinor.Name = "numericMinor";
            this.numericMinor.Size = new System.Drawing.Size(54, 20);
            this.numericMinor.TabIndex = 2;
            this.numericMinor.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // numericBuild
            // 
            this.numericBuild.Location = new System.Drawing.Point(270, 130);
            this.numericBuild.Name = "numericBuild";
            this.numericBuild.Size = new System.Drawing.Size(54, 20);
            this.numericBuild.TabIndex = 3;
            // 
            // numericRevision
            // 
            this.numericRevision.Location = new System.Drawing.Point(343, 130);
            this.numericRevision.Name = "numericRevision";
            this.numericRevision.Size = new System.Drawing.Size(54, 20);
            this.numericRevision.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(130, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(258, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Major              Minor              Build                 Revision";
            // 
            // btnSetValue
            // 
            this.btnSetValue.Location = new System.Drawing.Point(133, 198);
            this.btnSetValue.Name = "btnSetValue";
            this.btnSetValue.Size = new System.Drawing.Size(122, 23);
            this.btnSetValue.TabIndex = 6;
            this.btnSetValue.Text = "Create &Version Table";
            this.btnSetValue.UseVisualStyleBackColor = true;
            this.btnSetValue.Click += new System.EventHandler(this.BtnSetValueClick);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(287, 198);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(110, 23);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "&Close Window";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // FormSetDbVersion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(534, 261);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSetValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericRevision);
            this.Controls.Add(this.numericBuild);
            this.Controls.Add(this.numericMinor);
            this.Controls.Add(this.numericMajor);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSetDbVersion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Set Database Version";
            ((System.ComponentModel.ISupportInitialize)(this.numericMajor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMinor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBuild)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRevision)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericMajor;
        private System.Windows.Forms.NumericUpDown numericMinor;
        private System.Windows.Forms.NumericUpDown numericBuild;
        private System.Windows.Forms.NumericUpDown numericRevision;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSetValue;
        private System.Windows.Forms.Button btnClose;
    }
}