﻿namespace AdvantageDbMigration
{
    partial class StartingClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartingClient));
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.returnToMainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabConfiguration = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.tbDbPath = new System.Windows.Forms.TextBox();
            this.cbRelocate = new System.Windows.Forms.CheckBox();
            this.btnCreateAdvantage = new System.Windows.Forms.Button();
            this.btnCreateTenant = new System.Windows.Forms.Button();
            this.tbConnectionAdvantage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbConnectionTenant = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbProtocol = new System.Windows.Forms.ComboBox();
            this.tbVirtualAddress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbReportServerPort = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbReportServerDomain = new System.Windows.Forms.TextBox();
            this.tbSchoolName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbConfig = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.tabVerification = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbLogVerification = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVerification = new System.Windows.Forms.Button();
            this.tabInstallation = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbStarterInstallation = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnBeginStarting = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabConfiguration.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabVerification.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabInstallation.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(957, 42);
            this.panel1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(957, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportConfigurationToolStripMenuItem,
            this.importConfigurationToolStripMenuItem,
            this.toolStripMenuItem1,
            this.returnToMainToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportConfigurationToolStripMenuItem
            // 
            this.exportConfigurationToolStripMenuItem.Name = "exportConfigurationToolStripMenuItem";
            this.exportConfigurationToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exportConfigurationToolStripMenuItem.Text = "E&xport Configuration";
            this.exportConfigurationToolStripMenuItem.Click += new System.EventHandler(this.ExportConfigurationToolStripMenuItemClick);
            // 
            // importConfigurationToolStripMenuItem
            // 
            this.importConfigurationToolStripMenuItem.Name = "importConfigurationToolStripMenuItem";
            this.importConfigurationToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.importConfigurationToolStripMenuItem.Text = "&Import Configuration";
            this.importConfigurationToolStripMenuItem.Click += new System.EventHandler(this.ImportConfigurationToolStripMenuItemClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(184, 6);
            // 
            // returnToMainToolStripMenuItem
            // 
            this.returnToMainToolStripMenuItem.Name = "returnToMainToolStripMenuItem";
            this.returnToMainToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.returnToMainToolStripMenuItem.Text = "&Return to Main";
            this.returnToMainToolStripMenuItem.Click += new System.EventHandler(this.ReturnToMainToolStripMenuItemClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 574);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(957, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 42);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(957, 532);
            this.panel2.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabConfiguration);
            this.tabControl1.Controls.Add(this.tabVerification);
            this.tabControl1.Controls.Add(this.tabInstallation);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(957, 532);
            this.tabControl1.TabIndex = 0;
            // 
            // tabConfiguration
            // 
            this.tabConfiguration.Controls.Add(this.groupBox6);
            this.tabConfiguration.Controls.Add(this.groupBox5);
            this.tabConfiguration.Controls.Add(this.tbConfig);
            this.tabConfiguration.Controls.Add(this.btnConfirm);
            this.tabConfiguration.Location = new System.Drawing.Point(4, 22);
            this.tabConfiguration.Name = "tabConfiguration";
            this.tabConfiguration.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfiguration.Size = new System.Drawing.Size(949, 506);
            this.tabConfiguration.TabIndex = 0;
            this.tabConfiguration.Text = "Configuration";
            this.tabConfiguration.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnBrowse);
            this.groupBox6.Controls.Add(this.tbDbPath);
            this.groupBox6.Controls.Add(this.cbRelocate);
            this.groupBox6.Controls.Add(this.btnCreateAdvantage);
            this.groupBox6.Controls.Add(this.btnCreateTenant);
            this.groupBox6.Controls.Add(this.tbConnectionAdvantage);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.tbConnectionTenant);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Location = new System.Drawing.Point(8, 152);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(933, 155);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Database Configuration";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(842, 120);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 18;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.BtnBrowseClick);
            // 
            // tbDbPath
            // 
            this.tbDbPath.Location = new System.Drawing.Point(92, 120);
            this.tbDbPath.Name = "tbDbPath";
            this.tbDbPath.Size = new System.Drawing.Size(742, 20);
            this.tbDbPath.TabIndex = 17;
            // 
            // cbRelocate
            // 
            this.cbRelocate.AutoSize = true;
            this.cbRelocate.Location = new System.Drawing.Point(6, 120);
            this.cbRelocate.Name = "cbRelocate";
            this.cbRelocate.Size = new System.Drawing.Size(85, 17);
            this.cbRelocate.TabIndex = 16;
            this.cbRelocate.Text = "Relocate To";
            this.cbRelocate.UseVisualStyleBackColor = true;
            this.cbRelocate.CheckStateChanged += new System.EventHandler(this.CbRelocateCheckStateChanged);
            // 
            // btnCreateAdvantage
            // 
            this.btnCreateAdvantage.Location = new System.Drawing.Point(842, 79);
            this.btnCreateAdvantage.Name = "btnCreateAdvantage";
            this.btnCreateAdvantage.Size = new System.Drawing.Size(75, 23);
            this.btnCreateAdvantage.TabIndex = 15;
            this.btnCreateAdvantage.Text = "Create";
            this.btnCreateAdvantage.UseVisualStyleBackColor = true;
            this.btnCreateAdvantage.Click += new System.EventHandler(this.BtnCreateAdvantageClick);
            // 
            // btnCreateTenant
            // 
            this.btnCreateTenant.Location = new System.Drawing.Point(844, 40);
            this.btnCreateTenant.Name = "btnCreateTenant";
            this.btnCreateTenant.Size = new System.Drawing.Size(75, 23);
            this.btnCreateTenant.TabIndex = 14;
            this.btnCreateTenant.Text = "Create";
            this.btnCreateTenant.UseVisualStyleBackColor = true;
            this.btnCreateTenant.Click += new System.EventHandler(this.BtnCreateTenantClick);
            // 
            // tbConnectionAdvantage
            // 
            this.tbConnectionAdvantage.Location = new System.Drawing.Point(6, 81);
            this.tbConnectionAdvantage.Name = "tbConnectionAdvantage";
            this.tbConnectionAdvantage.Size = new System.Drawing.Size(830, 20);
            this.tbConnectionAdvantage.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Enter Connection string for Advantage DB";
            // 
            // tbConnectionTenant
            // 
            this.tbConnectionTenant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbConnectionTenant.Location = new System.Drawing.Point(6, 42);
            this.tbConnectionTenant.Name = "tbConnectionTenant";
            this.tbConnectionTenant.Size = new System.Drawing.Size(830, 20);
            this.tbConnectionTenant.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Enter Connection string for Tenant DB";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.cbProtocol);
            this.groupBox5.Controls.Add(this.tbVirtualAddress);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.tbReportServerPort);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.tbReportServerDomain);
            this.groupBox5.Controls.Add(this.tbSchoolName);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(8, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(933, 138);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Advantage Configuration Settings";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Protocol";
            // 
            // cbProtocol
            // 
            this.cbProtocol.FormattingEnabled = true;
            this.cbProtocol.Items.AddRange(new object[] {
            "http",
            "https"});
            this.cbProtocol.Location = new System.Drawing.Point(9, 89);
            this.cbProtocol.Name = "cbProtocol";
            this.cbProtocol.Size = new System.Drawing.Size(121, 21);
            this.cbProtocol.TabIndex = 28;
            // 
            // tbVirtualAddress
            // 
            this.tbVirtualAddress.Location = new System.Drawing.Point(136, 89);
            this.tbVirtualAddress.Name = "tbVirtualAddress";
            this.tbVirtualAddress.Size = new System.Drawing.Size(275, 20);
            this.tbVirtualAddress.TabIndex = 27;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(133, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Advantage Virtual base Address";
            // 
            // tbReportServerPort
            // 
            this.tbReportServerPort.Location = new System.Drawing.Point(621, 90);
            this.tbReportServerPort.Name = "tbReportServerPort";
            this.tbReportServerPort.Size = new System.Drawing.Size(50, 20);
            this.tbReportServerPort.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(618, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "Port:";
            // 
            // tbReportServerDomain
            // 
            this.tbReportServerDomain.Location = new System.Drawing.Point(485, 90);
            this.tbReportServerDomain.Name = "tbReportServerDomain";
            this.tbReportServerDomain.Size = new System.Drawing.Size(130, 20);
            this.tbReportServerDomain.TabIndex = 23;
            // 
            // tbSchoolName
            // 
            this.tbSchoolName.Location = new System.Drawing.Point(9, 42);
            this.tbSchoolName.Name = "tbSchoolName";
            this.tbSchoolName.Size = new System.Drawing.Size(832, 20);
            this.tbSchoolName.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(482, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Report Server Domain";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "School Name";
            // 
            // tbConfig
            // 
            this.tbConfig.Location = new System.Drawing.Point(8, 342);
            this.tbConfig.Multiline = true;
            this.tbConfig.Name = "tbConfig";
            this.tbConfig.Size = new System.Drawing.Size(933, 190);
            this.tbConfig.TabIndex = 13;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(8, 313);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(156, 23);
            this.btnConfirm.TabIndex = 12;
            this.btnConfirm.Text = "Confirm Configuration";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.BtnConfirmClick);
            // 
            // tabVerification
            // 
            this.tabVerification.Controls.Add(this.groupBox2);
            this.tabVerification.Controls.Add(this.panel3);
            this.tabVerification.Controls.Add(this.groupBox1);
            this.tabVerification.Location = new System.Drawing.Point(4, 22);
            this.tabVerification.Name = "tabVerification";
            this.tabVerification.Padding = new System.Windows.Forms.Padding(3);
            this.tabVerification.Size = new System.Drawing.Size(949, 506);
            this.tabVerification.TabIndex = 1;
            this.tabVerification.Text = "Verifications";
            this.tabVerification.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbLogVerification);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(226, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(720, 451);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log.";
            // 
            // tbLogVerification
            // 
            this.tbLogVerification.AcceptsReturn = true;
            this.tbLogVerification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLogVerification.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLogVerification.Location = new System.Drawing.Point(3, 16);
            this.tbLogVerification.Multiline = true;
            this.tbLogVerification.Name = "tbLogVerification";
            this.tbLogVerification.ReadOnly = true;
            this.tbLogVerification.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLogVerification.Size = new System.Drawing.Size(714, 432);
            this.tbLogVerification.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(226, 454);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(720, 49);
            this.panel3.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnVerification);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(223, 500);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // btnVerification
            // 
            this.btnVerification.Location = new System.Drawing.Point(23, 29);
            this.btnVerification.Name = "btnVerification";
            this.btnVerification.Size = new System.Drawing.Size(167, 23);
            this.btnVerification.TabIndex = 0;
            this.btnVerification.Text = "Launch Verification";
            this.btnVerification.UseVisualStyleBackColor = true;
            this.btnVerification.Click += new System.EventHandler(this.BtnVerificationClick);
            // 
            // tabInstallation
            // 
            this.tabInstallation.Controls.Add(this.groupBox4);
            this.tabInstallation.Controls.Add(this.panel4);
            this.tabInstallation.Controls.Add(this.groupBox3);
            this.tabInstallation.Location = new System.Drawing.Point(4, 22);
            this.tabInstallation.Name = "tabInstallation";
            this.tabInstallation.Size = new System.Drawing.Size(949, 506);
            this.tabInstallation.TabIndex = 2;
            this.tabInstallation.Text = "Installation";
            this.tabInstallation.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbStarterInstallation);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(200, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(749, 463);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Log";
            // 
            // tbStarterInstallation
            // 
            this.tbStarterInstallation.AcceptsReturn = true;
            this.tbStarterInstallation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbStarterInstallation.Location = new System.Drawing.Point(3, 16);
            this.tbStarterInstallation.Multiline = true;
            this.tbStarterInstallation.Name = "tbStarterInstallation";
            this.tbStarterInstallation.ReadOnly = true;
            this.tbStarterInstallation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbStarterInstallation.Size = new System.Drawing.Size(743, 444);
            this.tbStarterInstallation.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(200, 463);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(749, 43);
            this.panel4.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnBeginStarting);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 506);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Actions";
            // 
            // btnBeginStarting
            // 
            this.btnBeginStarting.Location = new System.Drawing.Point(9, 35);
            this.btnBeginStarting.Name = "btnBeginStarting";
            this.btnBeginStarting.Size = new System.Drawing.Size(175, 23);
            this.btnBeginStarting.TabIndex = 0;
            this.btnBeginStarting.Text = "Installation";
            this.btnBeginStarting.UseVisualStyleBackColor = true;
            this.btnBeginStarting.Click += new System.EventHandler(this.BtnBeginStartingClick);
            // 
            // StartingClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 596);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "StartingClient";
            this.Text = "Starting New Advantage Client";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabConfiguration.ResumeLayout(false);
            this.tabConfiguration.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabVerification.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabInstallation.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabConfiguration;
        private System.Windows.Forms.TabPage tabVerification;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnVerification;
        private System.Windows.Forms.TabPage tabInstallation;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnBeginStarting;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.TextBox tbConfig;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem returnToMainToolStripMenuItem;
        private System.Windows.Forms.TextBox tbLogVerification;
        private System.Windows.Forms.TextBox tbStarterInstallation;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox tbDbPath;
        private System.Windows.Forms.CheckBox cbRelocate;
        private System.Windows.Forms.Button btnCreateAdvantage;
        private System.Windows.Forms.Button btnCreateTenant;
        private System.Windows.Forms.TextBox tbConnectionAdvantage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbConnectionTenant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbProtocol;
        private System.Windows.Forms.TextBox tbVirtualAddress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbReportServerPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbReportServerDomain;
        private System.Windows.Forms.TextBox tbSchoolName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
    }
}