﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdvantageDbMigration.Helpers
{
    public static class Notification
    {
        public static void Error(Form form, string message)
        {
            MessageBox.Show(
                   form,
                   message,
                   @"ERROR NOTIFICATION",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
        }

        public static void Information(Form form, string message)
        {
            MessageBox.Show(
                   form,
                   message,
                   @"INFORMATION NOTIFICATION",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
        }

        public static void Warning(Form form, string message)
        {
            MessageBox.Show(
                   form,
                   message,
                   @"WARNING NOTIFICATION",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Warning);
        }

        public static DialogResult Question(Form form, string message)
        {
           var res = MessageBox.Show(
                   form,
                   message,
                   @"WARNING NOTIFICATION",
                   MessageBoxButtons.OKCancel,
                   MessageBoxIcon.Warning);
            return res;
        }
    }
}
