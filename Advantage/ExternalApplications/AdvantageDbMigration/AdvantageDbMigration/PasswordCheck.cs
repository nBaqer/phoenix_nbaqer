﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PasswordCheck.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the PasswordCheck type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDbMigration
{
    using System;
    using System.Configuration;
    using System.Windows.Forms;

    /// <summary>
    /// The password check.
    /// </summary>
    public partial class PasswordCheck : Form
    {
         /// <summary>
        /// The parent.
        /// </summary>
        private Form parent;  
        
        /// <summary>
        /// Initializes a new instance of the <see cref="PasswordCheck"/> class.
        /// </summary>
        /// <param name="parent">
        /// The parent.
        /// </param>
        public PasswordCheck(Form parent)
        {
            this.parent = parent;
            this.InitializeComponent();
        }
   
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The button OK click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnOkClick(object sender, EventArgs e)
        {
            this.Password = this.tbPassword.Text;
            this.Close();
        }

        /// <summary>
        /// The password check form closed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PasswordCheckFormClosed(object sender, FormClosedEventArgs e)
        {
            var pass = ConfigurationManager.AppSettings["pass"];
            if (pass != this.Password)
            {
                Application.Exit();
            }
            else
            {
                this.parent.Enabled = true;
            }
        }

        /// <summary>
        /// The button cancel click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
