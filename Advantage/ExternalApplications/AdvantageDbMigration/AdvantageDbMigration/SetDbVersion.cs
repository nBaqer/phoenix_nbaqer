﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SetDbVersion.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the FormSetDbVersion type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDbMigration
{
    using System;
    using System.Windows.Forms;

    using GLibrary.Facade;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    /// <summary>
    /// The form set DB version.
    /// </summary>
    public partial class FormSetDbVersion : Form
    {
        /// <summary>
        /// The tenant.
        /// </summary>
        private readonly ITenants tenant;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="FormSetDbVersion"/> class.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public FormSetDbVersion(ITenants tenant)
        {
            this.InitializeComponent();
            this.tenant = tenant;

            // Determine if exists the version table
            var exists = !DbInstallation.ExistsVersionTable(tenant);

            // If not enable the controls if not disable they.
            this.numericMajor.Enabled =
                this.numericMinor.Enabled =
                     this.numericBuild.Enabled =
                          this.numericRevision.Enabled =
                               this.btnSetValue.Enabled = exists;
        }

        /// <summary>
        /// The button set value click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnSetValueClick(object sender, EventArgs e)
        {
            // Get version from controls
            DbVersion version = new DbVersion
                                    {
                                        Major = Convert.ToInt32(this.numericMajor.Value),
                                        Minor = Convert.ToInt32(this.numericMinor.Value),
                                        Build = Convert.ToInt32(this.numericBuild.Value),
                                        Revision = Convert.ToInt32(this.numericRevision.Value),
                                        Description = "Initial Version set by User",
                                        ModUser = "SUPPORT",
                                        VersionBegin = DateTime.Now
                                    };
            
            // Create the table and set the version
            try
            {
                this.Cursor = Cursors.WaitCursor;
                DbInstallation.CreateTableVersion(this.tenant, version);
                var ver = DbInstallation.GetDbVersion(this.tenant);
                MessageBox.Show(
                    this,
                    $@"Table Version was created successfully and set to version: {ver.GetStringVersion()}",
                    @"EXECUTED OPERATION",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(
                    this,
                    $@"Error, The following exception was throw: {exception.Message}",
                    @"ERROR",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
         }
    }
}
