﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("AdvantageDbMigration")]
[assembly: AssemblyDescription("Installer to Update Advantage Database")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("FAME")]
[assembly: AssemblyProduct("AdvantageDbMigration")]
[assembly: AssemblyCopyright("Copyright ©  FAME 2017")]
[assembly: AssemblyTrademark("FAME")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6c6f25df-c68c-472b-b00b-e623077c8e07")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.9.3.*")]
[assembly: AssemblyFileVersion("3.9.3.1")]
