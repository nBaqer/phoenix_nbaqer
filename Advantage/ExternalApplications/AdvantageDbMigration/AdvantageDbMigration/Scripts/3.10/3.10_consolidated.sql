-- ===============================================================================================
-- Consolidated Script Version 3.10
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================

--=================================================================================================
-- US 11000: (JAGG) TECH Initial Consolidated Scrip Changes for Advantage API
--=================================================================================================
--=================================================================================================
-- Find all the system roles where RoleTypeId is null and set them to 1 in sySysRoles table
--=================================================================================================
UPDATE sySysRoles
SET    RoleTypeId = 1
WHERE  RoleTypeId IS NULL;
GO
--=================================================================================================
-- Insert the in sySysRoles table the record for "Admission Api" with RoleTypeId = 2 and SysRoleId = 17
--=================================================================================================
IF NOT EXISTS (
              SELECT 1
              FROM   dbo.sySysRoles
              WHERE  SysRoleId = 17
              )
    BEGIN
        INSERT INTO dbo.sySysRoles (
                                   SysRoleId
                                  ,Descrip
                                  ,StatusId
                                  ,ModUser
                                  ,ModDate
                                  ,RoleTypeId
                                  ,Permission
                                   )
        VALUES ( 17                                     -- SysRoleId - int
                ,'Admission Api'                        -- Descrip - varchar(80)
                ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                ,'SUPPORT'                              -- ModUser - varchar(50)
                ,GETDATE()                              -- ModDate - datetime
                ,2
                ,NULL
               );
    END;
ELSE
	BEGIN
		UPDATE SSR
		   SET SSR.Descrip = 'Admission Api'   
        FROM sySysRoles AS SSR
		WHERE SSR.Descrip <> 'Admission Api'
		      AND SSR.SysRoleId = 17
	END
GO
--=================================================================================================
-- Insert a record to SyConfigSettings for the AdvantageApiURL
--=================================================================================================
DECLARE @KeyName VARCHAR(50) = 'AdvantageApiURL';

IF NOT EXISTS (
              SELECT 1
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN
        BEGIN TRANSACTION TEMP1;
        DECLARE @MaxId INT = (
                             SELECT MAX(SettingId)
                             FROM   dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;

        INSERT INTO dbo.syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
        VALUES ( @MaxId              -- SettingId - int
                ,@KeyName            -- KeyName - varchar(200)
                ,'Advantage Api URL' -- Description - varchar(1000)
                ,'Support'           -- ModUser - varchar(50)
                ,GETDATE()           -- ModDate - datetime
                ,0                   -- CampusSpecific - bit
                ,0                   -- ExtraConfirmation - bit
               );

        INSERT INTO dbo.syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
        VALUES ( NEWID()                             -- ValueId - uniqueidentifier
                ,@MaxId                              -- SettingId - int
                ,NULL                                -- CampusId - uniqueidentifier
                ,'http://current/fame/advantage/api' -- Value - varchar(1000)
                ,'Support'                           -- ModUser - varchar(50)
                ,GETDATE()                           -- ModDate - datetime
                ,1                                   -- Active - bit
               );

        COMMIT TRANSACTION TEMP1;
    END;
GO
--=================================================================================================
-- Insert a record to SyConfigSettings for the TermsOfUseVersionNumber
--=================================================================================================
DECLARE @KeyName VARCHAR(50) = 'TermsOfUseVersionNumber';

IF NOT EXISTS (
              SELECT *
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN
        BEGIN TRANSACTION TEMP2;
        DECLARE @MaxId INT = (
                             SELECT MAX(SettingId)
                             FROM   dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;

        INSERT INTO dbo.syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
        VALUES ( @MaxId                                          -- SettingId - int
                ,@KeyName                                        -- KeyName - varchar(200)
                ,'Terms Of Use Version Number: format vYYYYMMDD' -- Description - varchar(1000)
                ,'Support'                                       -- ModUser - varchar(50)
                ,GETDATE()                                       -- ModDate - datetime
                ,0                                               -- CampusSpecific - bit
                ,0                                               -- ExtraConfirmation - bit
               );

        INSERT INTO dbo.syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
        VALUES ( NEWID()     -- ValueId - uniqueidentifier
                ,@MaxId      -- SettingId - int
                ,NULL        -- CampusId - uniqueidentifier
                ,'v20170830' -- Value - varchar(1000)
                ,'Support'   -- ModUser - varchar(50)
                ,GETDATE()   -- ModDate - datetime
                ,1           -- Active - bit
               );

        COMMIT TRANSACTION TEMP2;
    END;
GO
-- ===============================================================================================
-- END US 11000: --   Consolidated Script Version 3.10
-- ===============================================================================================

--=================================================================================================
-- US11001 Create the update script for the permission for the existing users as per the role
--=================================================================================================
BEGIN TRANSACTION rolePermissions;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @fldId INT;

    BEGIN
        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "delete"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 1
               AND Permission IS NULL; -- System Administrator

        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 2
               AND Permission IS NULL; -- Instructors


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 3
               AND Permission IS NULL; -- Admission Reps


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 4
               AND Permission IS NULL; -- Academic Advisors


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 5
               AND Permission IS NULL; -- Other


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 6
               AND Permission IS NULL; -- Placement Reps


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 7
               AND Permission IS NULL; -- Financial Aid Advisor


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "delete"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 8
               AND Permission IS NULL; -- Director Of Admissions


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "delete"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 9
               AND Permission IS NULL; -- Director of Financial Aid


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "delete"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 10
               AND Permission IS NULL; -- Director of Business Office


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 11
               AND Permission IS NULL; -- Instructors Supervisor


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 12
               AND Permission IS NULL; -- Front Desk


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "delete"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 13
               AND Permission IS NULL; -- Director of Academics


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 14
               AND Permission IS NULL; -- Report Administrator


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "read"}, {"name": "AD","level": "read"}, 
									{"name": "FC","level": "read"}, {"name": "FA","level": "read"}, 
									{"name": "HR","level": "read"}, {"name": "PL","level": "read"}, 
									{"name": "SA","level": "read"}, {"name": "SY","level": "read"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 15
               AND Permission IS NULL; -- Dashboard


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 16
               AND Permission IS NULL; -- View Confidential


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 17
               AND Permission IS NULL; -- Admission API
    END;

    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION rolePermissions;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION rolePermissions;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION rolePermissions;
        PRINT 'Update successful';
    END;
GO
--=================================================================================================
-- END --   US11001 Create the update script for the permission for the existing users as per the role
--=================================================================================================

--=================================================================================================
-- US11053 New System Role - Student Termination
--=================================================================================================
BEGIN TRANSACTION STrole;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @fldId INT;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.sySysRoles
                  WHERE  SysRoleId = 18
                  )
        BEGIN

            DECLARE @STpermission NVARCHAR(MAX) = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}';

            DECLARE @ActiveStatus UNIQUEIDENTIFIER;

            SELECT @ActiveStatus = StatusId
            FROM   syStatuses
            WHERE  StatusCode = 'A';

            INSERT INTO dbo.sySysRoles (
                                       SysRoleId
                                      ,Descrip
                                      ,StatusId
                                      ,ModUser
                                      ,ModDate
                                      ,RoleTypeId
                                      ,Permission
                                       )
            VALUES ( 18, 'Student Termination', @ActiveStatus, 'sa', GETDATE(), 1, @STpermission );
        END;

    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION AArole;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION STrole;
        PRINT 'Insert failed';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION STrole;
        PRINT 'Insert successful';
    END;
GO
--=================================================================================================
-- END --  US11053 New System Role - Student Termination
--=================================================================================================


