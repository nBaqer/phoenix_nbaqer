BEGIN TRANSACTION UpdateVersion
BEGIN TRY

    UPDATE  dbo.syVersionHistory
    SET     VersionEnd = GETDATE()
    WHERE   Id = (
                   SELECT   Id
                   FROM     dbo.syVersionHistory
                   WHERE    VersionEnd IS NULL
                 );
    INSERT  dbo.syVersionHistory
            (
             Major
            ,Minor
            ,Build
            ,Revision
            ,VersionBegin
            ,VersionEnd
            ,Description
            ,ModUser
            )
    VALUES  (
             @Major  -- Major - int
            ,@Minor  -- Minor - int
            ,@Build  -- Build - int
            ,@Revision -- Revision int
            ,GETDATE()  -- VersionBegin - datetime
            ,NULL  -- VersionEnd - datetime
            ,@Description  -- Description - varchar(100)
            ,@User  -- ModUser - varchar(50)
            );
    COMMIT TRANSACTION UpdateVersion;
END TRY

BEGIN CATCH
    ROLLBACK TRANSACTION UpdateVersion;
    RAISERROR('Version Table can not be updated',16,1);
END CATCH;