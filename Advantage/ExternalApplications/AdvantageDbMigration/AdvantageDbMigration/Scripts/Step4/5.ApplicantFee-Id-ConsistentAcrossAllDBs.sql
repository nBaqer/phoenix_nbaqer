--DE13478
BEGIN
    DECLARE @AppFee_TranscodeId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    saSysTransCodes
                    WHERE   SysTransCodeId = 20
                            AND Description = 'Applicant Fee' )
        BEGIN
            DECLARE @AllCampGrpId UNIQUEIDENTIFIER;
            SET @AllCampGrpId = (
                                  SELECT TOP 1
                                            CampGrpId
                                  FROM      syCampGrps
                                  WHERE     CampGrpDescrip = 'All'
                                );
            INSERT  INTO saSysTransCodes
            VALUES  ( 20,'Applicant Fee','F23DE1E2-D90A-4720-B4C7-0F6FB09C9965',@AllCampGrpId );
        END;
    SET @AppFee_TranscodeId = (
                                SELECT TOP 1
                                        SysTransCodeId
                                FROM    saSysTransCodes
                                WHERE   SysTransCodeId = 20
                                        AND Description = 'Applicant Fee'
                              );
    UPDATE  saTransCodes
    SET     SysTransCodeId = @AppFee_TranscodeId
    WHERE   SysTransCodeId IN ( SELECT TOP 1
                                        SysTransCodeId
                                FROM    saSysTransCodes
                                WHERE   Description = 'Applicant Fee'
                                ORDER BY SysTransCodeId );
    
	IF (SELECT COUNT(*) FROM saSysTransCodes
                                WHERE   Description = 'Applicant Fee') > 1
	BEGIN
		DECLARE @OldApplicantFeeId INT 
		SET @OldApplicantFeeId = ( SELECT TOP 1
                                        SysTransCodeId
                                FROM    saSysTransCodes
                                WHERE   Description = 'Applicant Fee'
                                ORDER BY SysTransCodeId )

		IF @OldApplicantFeeId<>20
		BEGIN
          DELETE  FROM saSysTransCodes
			 WHERE   SysTransCodeId = @OldApplicantFeeId
            AND Description = 'Applicant Fee';
		END
	END
END;
GO


