-- ===============================================================================================
-- Consolidated Script Version 3.8GE_Patch
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- ===============================================================================================

-- ===============================================================================================
-- US10324 GE: Graduation Analysis - Report Update
-- ===============================================================================================
 IF EXISTS ( SELECT 1
			FROM syReports AS SRR
				INNER JOIN syResources AS SR ON SR.ResourceID = SRR.ResourceId
				INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
				INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
				INNER JOIN ParamDetail AS PD ON PD.SectionId = PS.SectionId
				INNER JOIN ParamDetailProp AS PDP ON PDP.detailid = PD.DetailId
			WHERE SR.ResourceId IN( 677)
          )
	BEGIN
		UPDATE PDP
			SET PDP.detailid = 0
		FROM syReports AS SRR
			INNER JOIN syResources AS SR ON SR.ResourceID = SRR.ResourceId
			INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
			INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
			INNER JOIN ParamDetail AS PD ON PD.SectionId = PS.SectionId
			INNER JOIN ParamDetailProp AS PDP ON PDP.detailid = PD.DetailId
		WHERE SR.ResourceId IN( 677)  -- Resource --> 'Graduation Analysis Report'
	END
-- ===============================================================================================
-- END  --  US10324 GE: Graduation Analysis - Report Update
-- ===============================================================================================
GO
 
-- ===============================================================================================
-- US10395 GE: Median Loan Debt - Report Update UI
-- ===============================================================================================
-- 1)  -- Change the ReportTabLayOut to use in ParamReport(Class) -- AddReportControlsToTabs(Method) -- OptionsOnly(select)
IF EXISTS ( SELECT  1
            FROM    syReports AS SR
            WHERE   SR.ResourceId = 676
                    AND SR.ReportTabLayout <> 1 )
    BEGIN
        UPDATE  SR
        SET     SR.ReportTabLayout = 1
        FROM    syReports AS SR
        WHERE   SR.ResourceId = 676
    END
GO

-- 2)  -- Delete syReportTab.  -- Tabs are any more needed.
DECLARE @ReportId AS UNIQUEIDENTIFIER
DECLARE @PageViewId AS NVARCHAR(200)

IF EXISTS ( SELECT  1
            FROM    syReportTabs AS SRT
            INNER JOIN syReports AS SR ON SR.ReportId = SRT.ReportId
            WHERE   SR.ResourceId = 676
                    AND SRT.PageViewId = 'RadRptPage_Filtering' )
    BEGIN
        SELECT  @ReportId = SRT.ReportId
               ,@PageViewId = SRT.PageViewId
        FROM    syReportTabs AS SRT
        INNER JOIN syReports AS SR ON SR.ReportId = SRT.ReportId
        WHERE   SR.ResourceId = 676
                AND SRT.PageViewId = 'RadRptPage_Filtering'
		
        DELETE  syReportTabs
        WHERE   ReportId = @ReportId
                AND PageViewId = @PageViewId
    END
GO

-- 3) -- Update syReportTab. The tab Options remain in UI
IF EXISTS ( SELECT  1
            FROM    syReports AS SRR
            INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
            WHERE   SRR.ResourceId = 676
                    AND SRT.PageViewId = 'RadRptPage_Options' )
    BEGIN
        UPDATE  SRT
        SET     SRT.PageViewId = 'RadRptPage_Options'
               ,SRT.DisplayName = 'Custom Options Set for Median Loan Debt Report'
               ,SRT.ParameterSetLookUp = 'MedianLoanReportFilterSet'
        FROM    syReports AS SRR
        INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
        WHERE   SRR.ResourceId = 676
                AND SRT.PageViewId = 'RadRptPage_Options'
    END
GO

-- 4) -- Update ParamSet
IF EXISTS ( SELECT  1
            FROM    syReports AS SRR
            INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
            INNER JOIN ParamSet AS PS2 ON PS2.SetId = SRT.SetId
            WHERE   SRR.ResourceId = 676
                    AND PS2.SetName = 'MedianLoanDebtCustomOptionsSet' )
    BEGIN
        UPDATE  PS2
        SET     PS2.SetName = 'MedianLoanDebtFilterSet'
               ,PS2.SetDescription = 'Custom settings for Median Loan Debt report'
               ,PS2.SetType = 'Custom'
        FROM    syReports AS SRR
        INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
        INNER JOIN ParamSet AS PS2 ON PS2.SetId = SRT.SetId
        WHERE   SRR.ResourceId = 676
                AND PS2.SetName = 'MedianLoanDebtCustomOptionsSet'
    END
GO

-- 5) -- Update ParamSection
IF EXISTS ( SELECT  1
            FROM    syReports AS SRR
            INNER JOIN syResources AS SR ON SR.ResourceID = SRR.ResourceId
            INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
            INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
            WHERE   SR.ResourceID = 676
                    AND PS.SectionName = 'Custom Options for Median Loan Debt Report' )
    BEGIN
        UPDATE  PS
        SET     PS.SectionName = 'Parameters for Median Loan Debt Report '
               ,PS.SectionCaption = 'Median Loan Debt Report Parameters'
               ,PS.SectionDescription = 'Choose Median Loan Debt report parameters'
        FROM    syReports AS SRR
        INNER JOIN syResources AS SR ON SR.ResourceID = SRR.ResourceId
        INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
        INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
        WHERE   SR.ResourceID = 676
                AND PS.SectionName = 'Custom Options for Median Loan Debt Report'
    END
GO

-- 6) -- Add ParamItem 
-- 7) -- Add ParamDetail connected to ParamItem and connect it with ParamSection
DECLARE @ItemId AS BIGINT

IF NOT EXISTS ( SELECT  1
                FROM    ParamItem AS PI
                WHERE   PI.Caption = 'MedianLoanDebtOptions' )
    BEGIN
		-- Insert ParamItem
        INSERT  ParamItem
                (
                 Caption
                ,ControllerClass
                ,valueprop
                ,ReturnValueName
                ,IsItemOverriden
				)
        VALUES  (
                 N'MedianLoanDebtOptions'       -- Caption - nvarchar(50)
                ,N'ParamGradCompletion.ascx'    -- ControllerClass - nvarchar(50)
                ,0                              -- valueprop - int
                ,N'MedianLoanDebtOptions'       -- ReturnValueName - nvarchar(50)
                ,NULL                           -- IsItemOverriden - bit
				)
        SET @ItemId = (
                        SELECT  IDENT_CURRENT('ParamItem')
                      )
        IF NOT EXISTS ( SELECT  *
                        FROM    syReports AS SRR
                        INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
                        INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
                        INNER JOIN ParamDetail AS PD ON PD.SectionId = PS.SectionId
                        WHERE   SRR.ResourceId = 676 )
            BEGIN
				-- INSERT ParamDetail
                INSERT  ParamDetail
                        (
                         SectionId
                        ,ItemId
                        ,CaptionOverride
                        ,ItemSeq
		                )
                VALUES  (
                         (
                           SELECT   PS.SectionId
                           FROM     syReports AS SRR
                           INNER JOIN syReportTabs AS SRT ON SRT.ReportId = SRR.ReportId
                           INNER JOIN ParamSection AS PS ON PS.SetId = SRT.SetId
                           WHERE    SRR.ResourceId = 676
                         )					-- SectionId - bigint
                        ,@ItemId			-- ItemId - bigint
                        ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                        ,1					-- ItemSeq - int
		                )
            END
    END
-- ===============================================================================================
-- END  --  US10324 GE: Median Loan Debt - Report Update UI
-- ===============================================================================================
GO
-- ===============================================================================================
-- DE13770   Remove "Resume" from Placement>>Manage Students>Academics>Resume
-- ===============================================================================================
IF EXISTS ( SELECT  *
            FROM    syMenuItems AS SMI
            WHERE   SMI.MenuName = 'Resume'
                    AND SMI.IsActive = 1 )
    BEGIN
        UPDATE  SMI
        SET     SMI.IsActive = 0
               ,SMI.ParentId = NULL
               ,SMI.ModUser = 'support'
               ,SMI.ModDate = GETDATE()
        FROM    syMenuItems AS SMI
        WHERE   SMI.MenuName = 'Resume';

        IF EXISTS ( SELECT  1
                    FROM    syNavigationNodes AS SNN
                    WHERE   SNN.ResourceId = 114 )
            BEGIN
                DELETE  FROM syNavigationNodes
                WHERE   ResourceId = 114;
            END;	
    END;	
GO	
-- ===============================================================================================
-- END  --   DE13770   Remove "Resume" from Placement>>Manage Students>Academics>Resume
-- ===============================================================================================

-- ===============================================================================================
-- END  --   Consolidated Script Version 3.8SP1
-- ===============================================================================================

