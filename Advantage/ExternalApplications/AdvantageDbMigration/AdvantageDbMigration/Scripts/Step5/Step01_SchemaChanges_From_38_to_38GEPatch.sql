/*
Run this script on:

        TORRES\SQLDEV2016.AMC_3.8    -  This database will be modified

to synchronize it with:

        TORRES\SQLDEV2016.AMC_3.8GE_Patch

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.1.0.3760 from Red Gate Software Ltd at 4/24/2017 4:34:16 PM

*/
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION;
GO


IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_getOnTimeGraduationDetails_RevisedGradDate]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails_RevisedGradDate
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_getOnTimeGraduationDetails_RevisedGradDate]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_getOnTimeGraduationDetails_RevisedGradDate;
    END;
    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_getOnTimeGraduationDetails_RevisedGradDate]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails_RevisedGradDate
--=================================================================================================
CREATE PROCEDURE dbo.USP_getOnTimeGraduationDetails_RevisedGradDate
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusId AS VARCHAR(50)
    ,@ProgramIdList AS VARCHAR(MAX) = NULL  
    )
AS
    BEGIN  
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
    
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';					-- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'


        BEGIN /** Step 1: Declare all temp tables in use **/
            DECLARE @getStudentsbyGEProgramsandDate TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,StatusCodeId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,CampusId UNIQUEIDENTIFIER
                ,StartDate DATETIME
                ,SysStatusDescrip VARCHAR(50)
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(10)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerDescrip VARCHAR(100)
                ,Weeks DECIMAL(18,2)
                ,SSN VARCHAR(50)
                ,EnrollmentId VARCHAR(50)
                ,StudentNumber VARCHAR(50)
                ,LastName VARCHAR(50)
                ,FirstName VARCHAR(50)
                ,MiddleName VARCHAR(50)
                ,DOB DATETIME
                ,ContractedGradDate DATETIME
                ,RevisedGradDate DATETIME
                ,StudentProgramStartDate DATETIME
                ,GraduationDate DATETIME
                ,RevisedGradDateCalculated DATETIME
                ,StudentName VARCHAR(100)
                ,StudentIdentification VARCHAR(50)
                ,HasTitleIV_Aid BIT
                ,OnTimeCompleter VARCHAR(5)
                ,RowNumber INTEGER
                ,PRIMARY KEY ( StuEnrollId )
                );

            DECLARE @getTransactionsSummary TABLE
                (
                 TransactionId UNIQUEIDENTIFIER
                ,TransAmount DECIMAL(18,2)
                ,TitleIV INT
                ,Voided INT
                ,StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,ProgId UNIQUEIDENTIFIER
                );
        END; /** END  Step 1 **/

        BEGIN /** Step 2: Get Students enrolled in GE Program and who started in date range STUDENT PROGRAM START DATE **/
            INSERT  INTO @getStudentsbyGEProgramsandDate
                    SELECT  GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.StatusCodeId
                           ,GS.PrgVerId
                           ,GS.CampusId
                           ,GS.StartDate
                           ,GS.SysStatusDescrip
                           ,GS.CampDescrip
                           ,GS.OPEID
                           ,GS.ProgId
                           ,GS.ProgDescrip
                           ,GS.CIPCode
                           ,GS.CredentialCode
                           ,GS.CredentialDescription
                           ,GS.PrgVerDescrip
                           ,GS.Weeks
                           ,GS.SSN
                           ,GS.EnrollmentId
                           ,GS.StudentNumber
                           ,GS.LastName
                           ,GS.FirstName
                           ,GS.MiddleName
                           ,GS.DOB
                           ,GS.ContractedGradDate
                           ,GS.RevisedGradDate
                           ,GS.StudentProgramStartDate
                           ,GS.GraduationDate
                           ,GS.RevisedGradDateCalculated
                           ,GS.StudentName
                           ,GS.StudentIdentification
                           ,0  -- False so far
                           ,''
                           ,GS.RowNumber
                    FROM    (
                              SELECT    ASE.StudentId
                                       ,ASE.StuEnrollId
                                       ,ASE.StatusCodeId
                                       ,ASE.PrgVerId
                                       ,ASE.CampusId
                                       ,ASE.StartDate
                                       ,SSS.SysStatusDescrip
                                       ,SC.CampDescrip
                                       ,SC.OPEID
                                       ,AP.ProgId
                                       ,AP.ProgDescrip
                                       ,AP.CIPCode
                                       ,APC.CredentialCode
                                       ,APC.CredentialDescription
                                       ,APV.PrgVerDescrip
                                       ,APV.Weeks
                                       ,AST.SSN
                                       ,ASE.EnrollmentId
                                       ,AST.StudentNumber
                                       ,AST.LastName
                                       ,AST.FirstName
                                       ,AST.MiddleName
                                       ,AST.DOB
                                       ,ASE.ContractedGradDate
                                       ,ASE.ExpGradDate AS RevisedGradDate
                                       ,MIN(ISNULL(ASE.StartDate,'2099-12-31')) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS StudentProgramStartDate -- -- used only to filter student with at least one enrollments out of rangedat
                                       ,ASE.ContractedGradDate AS GraduationDate
                                       ,MIN(CASE WHEN SSS.SysStatusDescrip = 'Graduated' THEN ASE.ExpGradDate
                                                 ELSE '2099-12-31'
                                            END) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS RevisedGradDateCalculated
                                       ,RTRIM(AST.LastName + ', ' + AST.FirstName + ' ' + ISNULL(UPPER(SUBSTRING(AST.MiddleName,1,1)),'')) AS StudentName
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(AST.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN ASE.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN AST.StudentNumber
                                             ELSE AST.StudentNumber
                                        END AS StudentIdentification
                                       ,ROW_NUMBER() OVER ( PARTITION BY AP.ProgId,ASE.StudentId ORDER BY ASE.StartDate ASC ) AS RowNumber
                              FROM      arStudent AS AST
                              INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
                              INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                              INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                              INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                              LEFT JOIN arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                              INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                              WHERE     SC.CampusId = @CampusId
                                        AND (
                                              ISNULL(@ProgramIdList,'') = ''
                                              OR AP.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgramIdList,',',1) )
                                            )
                                        AND AP.IsGEProgram = 1
                                        AND SSS.SysStatusId NOT IN ( 7,8 )
                            ) AS GS
                    WHERE   StudentProgramStartDate BETWEEN @StartDate AND @EndDate
                    ORDER BY StudentId;
        END;  /** END  --  Step 2 **/

        BEGIN /** Step 3: Get Total of Transaction for those studnent selected in Step 1 (Join saTransactions, saFundSources and getStudentsbyGEProgramsandDate) **/
            INSERT  INTO @getTransactionsSummary
                    SELECT  ST.TransactionId
                           ,ISNULL(ST.TransAmount,0) AS TransAmount
                           ,ISNULL(SFS.TitleIV,0) AS TitleIV
                           ,ISNULL(ST.Voided,0) AS Voided
                           ,GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.ProgId
                    FROM    saTransactions AS ST
                    INNER JOIN @getStudentsbyGEProgramsandDate AS GS ON GS.StuEnrollId = ST.StuEnrollId
                    LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    WHERE   ISNULL(SFS.TitleIV,0) = 1
                            AND ISNULL(ST.Voided,0) = 0;
        END;  /** END  --  Step 3 **/

        BEGIN /** Step 4: From Step 1 results, get TitleIV Aid for each student  **/

            UPDATE  GS
            SET     GS.HasTitleIV_Aid = (
                                          SELECT    CASE WHEN SUM(ISNULL(ST.TransAmount,0.00) * -1) <> 0 THEN 1
                                                         ELSE 0
                                                    END
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                        )
            FROM    @getStudentsbyGEProgramsandDate GS;

        END;  /** END  --  Step 4:  **/
        
        BEGIN /** Step 5: Return data to the report **/
            SELECT  GS.StudentIdentification
                   ,GS.StudentName
                   ,GS.CampDescrip
                   ,GS.OPEID
                   ,GS.ProgId
                   ,GS.ProgDescrip
                   ,GS.CIPCode
                   ,GS.CredentialCode
                   ,GS.CredentialDescription
                   ,GS.PrgVerId
                   ,GS.PrgVerDescrip
                   ,GS.GraduationDate
                   ,CASE WHEN GS.RevisedGradDateCalculated = '2099-12-31' THEN GS.RevisedGradDate
                         ELSE GS.RevisedGradDateCalculated
                    END AS RevisedGradDate
                   ,( CASE WHEN GS.RevisedGradDateCalculated <= GS.GraduationDate THEN 'Yes'
                           ELSE 'Not'
                      END ) AS OnTimeCompleter
            FROM    @getStudentsbyGEProgramsandDate AS GS
            WHERE   GS.RowNumber = 1
                    AND GS.HasTitleIV_Aid > 0
            ORDER BY GS.ProgDescrip
                   ,GS.PrgVerDescrip
                   ,GS.StudentName;

        END;  /** END  --  Step 5 **/


    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails_RevisedGradDate
--=================================================================================================    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_getOnTimeGraduationDetails_ProgramWeeks]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails_ProgramWeeks
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_getOnTimeGraduationDetails_ProgramWeeks]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_getOnTimeGraduationDetails_ProgramWeeks;
    END;
    
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_getOnTimeGraduationDetails_ProgramWeeks]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails_ProgramWeeks
--=================================================================================================
CREATE  PROCEDURE dbo.USP_getOnTimeGraduationDetails_ProgramWeeks
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusId AS VARCHAR(50)
    ,@ProgramIdList AS VARCHAR(MAX) = NULL  
    )
AS
    BEGIN  
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
    
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';					-- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'

        BEGIN /** Step 1: Declare all temp tables in use **/
            DECLARE @getStudentsbyGEProgramsandDate TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,StatusCodeId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,CampusId UNIQUEIDENTIFIER
                ,StartDate DATETIME
                ,SysStatusDescrip VARCHAR(50)
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(10)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerDescrip VARCHAR(100)
                ,Weeks DECIMAL(18,2)
                ,SSN VARCHAR(50)
                ,EnrollmentId VARCHAR(50)
                ,StudentNumber VARCHAR(50)
                ,LastName VARCHAR(50)
                ,FirstName VARCHAR(50)
                ,MiddleName VARCHAR(50)
                ,DOB DATETIME
                ,ContractedGradDate DATETIME
                ,RevisedGradDate DATETIME
                ,StudentProgramStartDate DATETIME
                ,GraduationDate DATETIME
                ,RevisedGradDateCalculated DATETIME
                ,StudentName VARCHAR(100)
                ,StudentIdentification VARCHAR(50)
                ,HasTitleIV_Aid BIT
                ,OnTimeCompleter VARCHAR(5)
                ,RowNumber INTEGER
                ,PRIMARY KEY ( StuEnrollId )
                );

            DECLARE @getTransactionsSummary TABLE
                (
                 TransactionId UNIQUEIDENTIFIER
                ,TransAmount DECIMAL(18,2)
                ,TitleIV INT
                ,Voided INT
                ,StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,ProgId UNIQUEIDENTIFIER
                );
        END; /** END  Step 1 **/

        BEGIN /** Step 2: Get Students enrolled in GE Program and who started in date range STUDENT PROGRAM START DATE **/
            INSERT  INTO @getStudentsbyGEProgramsandDate
                    SELECT  GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.StatusCodeId
                           ,GS.PrgVerId
                           ,GS.CampusId
                           ,GS.StartDate
                           ,GS.SysStatusDescrip
                           ,GS.CampDescrip
                           ,GS.OPEID
                           ,GS.ProgId
                           ,GS.ProgDescrip
                           ,GS.CIPCode
                           ,GS.CredentialCode
                           ,GS.CredentialDescription
                           ,GS.PrgVerDescrip
                           ,GS.Weeks
                           ,GS.SSN
                           ,GS.EnrollmentId
                           ,GS.StudentNumber
                           ,GS.LastName
                           ,GS.FirstName
                           ,GS.MiddleName
                           ,GS.DOB
                           ,GS.ContractedGradDate
                           ,GS.RevisedGradDate
                           ,GS.StudentProgramStartDate
                           ,GS.GraduationDate
                           ,GS.RevisedGradDateCalculated
                           ,GS.StudentName
                           ,GS.StudentIdentification
                           ,0  -- False so far
                           ,''
                           ,GS.RowNumber
                    FROM    (
                              SELECT    ASE.StudentId
                                       ,ASE.StuEnrollId
                                       ,ASE.StatusCodeId
                                       ,ASE.PrgVerId
                                       ,ASE.CampusId
                                       ,ASE.StartDate
                                       ,SSS.SysStatusDescrip
                                       ,SC.CampDescrip
                                       ,SC.OPEID
                                       ,AP.ProgId
                                       ,AP.ProgDescrip
                                       ,AP.CIPCode
                                       ,APC.CredentialCode
                                       ,APC.CredentialDescription
                                       ,APV.PrgVerDescrip
                                       ,APV.Weeks
                                       ,AST.SSN
                                       ,ASE.EnrollmentId
                                       ,AST.StudentNumber
                                       ,AST.LastName
                                       ,AST.FirstName
                                       ,AST.MiddleName
                                       ,AST.DOB
                                       ,ASE.ContractedGradDate
                                       ,ASE.ExpGradDate AS RevisedGradDate
                                       ,MIN(ISNULL(ASE.StartDate,'2099-12-31')) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS StudentProgramStartDate -- -- used only to filter student with at least one enrollments out of rangedat
                                       ,DATEADD(DAY,
                                                dbo.GetTotalNoofHolidaysbtwGivenDatesandCampus(MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ),
                                                                                               DATEADD(WEEK,APV.Weeks,
                                                                                                       MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,
                                                                                                                                 ASE.StudentId )),ASE.CampusId),
                                                DATEADD(WEEK,APV.Weeks,MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ))) AS GraduationDate
                                       ,MIN(CASE WHEN SSS.SysStatusDescrip = 'Graduated' THEN ASE.ExpGradDate
                                                 ELSE '2099-12-31'
                                            END) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS RevisedGradDateCalculated
                                       ,RTRIM(AST.LastName + ', ' + AST.FirstName + ' ' + ISNULL(UPPER(SUBSTRING(AST.MiddleName,1,1)),'')) AS StudentName
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(AST.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN ASE.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN AST.StudentNumber
                                             ELSE AST.StudentNumber
                                        END AS StudentIdentification
                                       ,ROW_NUMBER() OVER ( PARTITION BY AP.ProgId,ASE.StudentId ORDER BY ASE.StartDate ASC ) AS RowNumber
                              FROM      arStudent AS AST
                              INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
                              INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                              INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                              INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                              LEFT JOIN arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                              INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                              WHERE     SC.CampusId = @CampusId
                                        AND (
                                              ISNULL(@ProgramIdList,'') = ''
                                              OR AP.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgramIdList,',',1) )
                                            )
                                        AND AP.IsGEProgram = 1
                                        AND SSS.SysStatusId NOT IN ( 7,8 )
                            ) AS GS
                    WHERE   StudentProgramStartDate BETWEEN @StartDate AND @EndDate
                    ORDER BY StudentId;
        END;  /** END  --  Step 2 **/

        BEGIN /** Step 3: Get Total of Transaction for those studnent selected in Step 1 (Join saTransactions, saFundSources and getStudentsbyGEProgramsandDate) **/
            INSERT  INTO @getTransactionsSummary
                    SELECT  ST.TransactionId
                           ,ISNULL(ST.TransAmount,0) AS TransAmount
                           ,ISNULL(SFS.TitleIV,0) AS TitleIV
                           ,ISNULL(ST.Voided,0) AS Voided
                           ,GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.ProgId
                    FROM    saTransactions AS ST
                    INNER JOIN @getStudentsbyGEProgramsandDate AS GS ON GS.StuEnrollId = ST.StuEnrollId
                    LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    WHERE   ISNULL(SFS.TitleIV,0) = 1
                            AND ISNULL(ST.Voided,0) = 0;
        END;  /** END  --  Step 3 **/
		 
        BEGIN /** Step 4: From Step 1 results, get TitleIV Aid for each student  **/
            UPDATE  GS
            SET     GS.HasTitleIV_Aid = (
                                          SELECT    CASE WHEN SUM(ISNULL(ST.TransAmount,0.00) * -1) <> 0 THEN 1
                                                         ELSE 0
                                                    END
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                        )
            FROM    @getStudentsbyGEProgramsandDate GS;

        END;  /** END  --  Step 4:  **/
        
        BEGIN /** Step 5: Return data to the report **/
            SELECT  GS.StudentIdentification
                   ,GS.StudentName
                   ,GS.CampDescrip
                   ,GS.OPEID
                   ,GS.ProgId
                   ,GS.ProgDescrip
                   ,GS.CIPCode
                   ,GS.CredentialCode
                   ,GS.CredentialDescription
                   ,GS.PrgVerId
                   ,GS.PrgVerDescrip
                   ,GS.GraduationDate
                   ,CASE WHEN GS.RevisedGradDateCalculated = '2099-12-31' THEN GS.RevisedGradDate
                         ELSE GS.RevisedGradDateCalculated
                    END AS RevisedGradDate
                   ,( CASE WHEN GS.RevisedGradDateCalculated <= GS.GraduationDate THEN 'Yes'
                           ELSE 'Not'
                      END ) AS OnTimeCompleter
            FROM    @getStudentsbyGEProgramsandDate AS GS
            WHERE   GS.RowNumber = 1
                    AND GS.HasTitleIV_Aid > 0
            ORDER BY GS.ProgDescrip
                   ,GS.PrgVerDescrip
                   ,GS.StudentName;

        END;  /** END  --  Step 5 **/

    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails_ProgramWeeks
--=================================================================================================    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_getOnTimeGraduationDetails]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_getOnTimeGraduationDetails]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_getOnTimeGraduationDetails;
    END;
    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_getOnTimeGraduationDetails]';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails
--=================================================================================================
CREATE PROCEDURE dbo.USP_getOnTimeGraduationDetails
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@RevGradDateType AS VARCHAR(50)
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        IF ( @RevGradDateType = 'fromprogram' )
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_ProgramWeeks @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
        ELSE
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_RevisedGradDate @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
    
    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails
--=================================================================================================    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_MedianLoanDebt_RevisedGradDate]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_RevisedGradDate
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_MedianLoanDebt_RevisedGradDate]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_MedianLoanDebt_RevisedGradDate;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_MedianLoanDebt_RevisedGradDate]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_RevisedGradDate
--=================================================================================================
CREATE PROCEDURE dbo.USP_MedianLoanDebt_RevisedGradDate
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN 
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
    
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';					-- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'
 
        BEGIN /** Declare all temp tables in use **/
            DECLARE @getStudentsbyGEProgramsandDate TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,StatusCodeId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,CampusId UNIQUEIDENTIFIER
                ,StudentProgramStartDate DATE
                ,StartDate DATE
                ,SysStatusDescrip VARCHAR(50)
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(10)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerDescrip VARCHAR(100)
                ,Weeks DECIMAL(18,2)
                ,SSN VARCHAR(50)
                ,EnrollmentId VARCHAR(50)
                ,StudentNumber VARCHAR(50)
                ,LastName VARCHAR(50)
                ,FirstName VARCHAR(50)
                ,MiddleName VARCHAR(50)
                ,DOB DATE
                ,ContractedGradDate DATE
                ,RevisedGradDate DATE
                ,PRIMARY KEY ( StuEnrollId )
                );
            DECLARE @getStudentLoanAmount TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,ProgId UNIQUEIDENTIFIER
                ,TitleIVLoanAmt DECIMAL(18,2)
                ,NonTitleIVLoanAmt DECIMAL(18,2)
                ,InstitutionalOwed DECIMAL(18,2)
                ,TotalPellAward DECIMAL(18,2)
                ,TitleIVAid DECIMAL(18,2)
                );
            DECLARE @getFinalStudentsList TABLE
                (
                 RowNumber INT
                ,StudentIdentification VARCHAR(50)
                ,StudentId UNIQUEIDENTIFIER
                ,StudentName VARCHAR(100)
                ,DOB DATE
                ,CampusId UNIQUEIDENTIFIER
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(50)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerId UNIQUEIDENTIFIER
                ,PrgVerDescrip VARCHAR(100)
                ,StuEnrollId UNIQUEIDENTIFIER
                ,SysStatusDescrip VARCHAR(50)
                ,StartDate DATE
                ,TitleIVLoanAmt DECIMAL(18,2)
                ,NonTitleIVLoanAmt DECIMAL(18,2)
                ,InstitutionalOwed DECIMAL(18,2)
                ,TotalLoanAmt DECIMAL(18,2)
                ,TotalPellAward DECIMAL(18,2)
                ,TitleIV_Aid DECIMAL(18,2)
                ,GraduationDate DATE
                ,RevisedGradDate DATE
                ,RevisedGradDateCalculated DATE
                ,OnTimeCompleter VARCHAR(5)
                );
            DECLARE @getTransactionsSummary TABLE
                (
                 TransactionId UNIQUEIDENTIFIER
                ,TransAmount DECIMAL(18,2)
                ,AdvFundSourceId INT
                ,TitleIV INT
                ,Voided INT
                ,StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,AwardTypeId INT
                ,SAT_Descrip VARCHAR(50)
                ,ProgId UNIQUEIDENTIFIER
                ,SAFS_Descrip VARCHAR(80)
                );
            DECLARE @PrgVersionMedian TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,LoanAmount DECIMAL(18,2)
                );
            DECLARE @getMedianLoanAmountbyProgramVersion TABLE
                (
                 PrgVerId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                ,PrgVersionRowNumber INT
                ,ProgramVersionCount INT
                );
            DECLARE @getMedianLoanAmountbyProgram TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                ,ProgramRowNumber INT
                ,ProgramCount INT
                );
            DECLARE @medianAmountbyProgramVersion TABLE
                (
                 PrgVerId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                );
            DECLARE @medianAmountbyProgram TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                );
        END; 

        BEGIN /** Step 1 : Get Students enrolled in GE Program and who started in date range **/
            INSERT  INTO @getStudentsbyGEProgramsandDate
                    SELECT  GS.*
                    FROM    (
                              SELECT    AST.StudentId
                                       ,ASE.StuEnrollId
                                       ,ASE.StatusCodeId
                                       ,ASE.PrgVerId
                                       ,ASE.CampusId
                                       ,MIN(ISNULL(ASE.StartDate,'2099-12-31')) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS StudentProgramStartDate -- -- used only to filter student with at least one enrollments out of rangedat
                                       ,ASE.StartDate
                                       ,SSS.SysStatusDescrip
                                       ,SC.CampDescrip
                                       ,SC.OPEID
                                       ,AP.ProgId
                                       ,AP.ProgDescrip
                                       ,AP.CIPCode
                                       ,APC.CredentialCode
                                       ,APC.CredentialDescription
                                       ,APV.PrgVerDescrip
                                       ,APV.Weeks
                                       ,AST.SSN
                                       ,ASE.EnrollmentId
                                       ,AST.StudentNumber
                                       ,AST.LastName
                                       ,AST.FirstName
                                       ,AST.MiddleName
                                       ,AST.DOB
                                       ,ASE.ContractedGradDate
                                       ,ASE.ExpGradDate
                              FROM      arStudent AS AST
                              INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
                              INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                              INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                              INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                              LEFT JOIN arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                              INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                              WHERE     SC.CampusId = @CampusId
                                        AND (
                                              @ProgramIdList IS NULL
                                              OR AP.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgramIdList,',',1) )
                                            )
                                        AND AP.IsGEProgram = 1
                                        AND SSS.SysStatusId NOT IN ( 7,8 )
                            ) AS GS
                    WHERE   StudentProgramStartDate BETWEEN @StartDate AND @EndDate
                    ORDER BY StudentId;
        END;

        BEGIN /**Join saTransactions, saFundSources and getStudentsbyGEProgramsandDate*/
            INSERT  INTO @getTransactionsSummary
                    SELECT  ST.TransactionId
                           ,ISNULL(ST.TransAmount,0) AS TransAmount
                           ,ISNULL(SFS.AdvFundSourceId,0) AS AdvFundSourceId
                           ,ISNULL(SFS.TitleIV,0) AS TitleIV
                           ,ISNULL(ST.Voided,0) AS Voided
                           ,GS.StudentId
                           ,GS.StuEnrollId
                           ,ISNULL(SFS.AwardTypeId,0) AS AwardTypeId
                           ,ISNULL(SAT.Descrip,'') AS SAT_Descrip
                           ,GS.ProgId
                           ,ISNULL(SAFS.Descrip,'') AS SAFS_Descrip
                    FROM    saTransactions AS ST
                    INNER JOIN @getStudentsbyGEProgramsandDate AS GS ON GS.StuEnrollId = ST.StuEnrollId
                    LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                    LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId; 
        END;

        BEGIN /** Step 2: From Step 1 results, get TitleIV Loan Payment, Refunds, TitleIV Aid, Pell Award for each student  **/
            INSERT  INTO @getStudentLoanAmount
                    SELECT  GSLA.StudentId
                           ,GSLA.ProgId
                           ,( ISNULL(GSLA.TitleIVLoanPayment,0.00) - ISNULL(GSLA.TitleIVLoanRefund,0.00) ) AS TitleIVLoanAmt
                           ,( ISNULL(GSLA.NonTitleIVLoanPayment,0.00) - ISNULL(GSLA.NonTitleIVLoanRefund,0.00) ) AS NonTitleIVLoanAmt
                           ,ISNULL(GSLA.InstitutionalOwed,0.00) AS InstitutionalOwed
                           ,ISNULL(GSLA.TotalPellAward,0.00) AS TotalPellAward
                           ,ISNULL(GSLA.TitleIVAid,0.00) AS TitleIVAid
                    FROM    (
                              SELECT    GS.StudentId
                                       ,GS.ProgId
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip IN ( 'DL - SUB','DL - UNSUB' )  -- ST.AdvFundSourceId IN ( 7,8 ) --AdvFundSourceId from syAdvFundSources for DL - SUB and DL - UNSUB
                                                    AND ST.SAT_Descrip IN ( 'Loan' )                    -- SFS.AwardTypeId IN (2)
                                        ) AS TitleIVLoanPayment
                                       ,(
                                          SELECT    SUM(ISNULL(SR.RefundAmount,0.00)) AS RefundAmount
                                          FROM      saRefunds AS SR
                                          INNER JOIN @getTransactionsSummary AS ST ON ST.TransactionId = SR.TransactionId
                                          LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = SR.FundSourceId
                                          LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                          LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId
                                          WHERE     ST.Voided = 0
                                                    AND ISNULL(SFS.TitleIV,0) = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ISNULL(SAFS.Descrip,'') IN ( 'DL - SUB','DL - UNSUB' )  -- ST.AdvFundSourceId IN ( 7,8 ) --AdvFundSourceId from syAdvFundSources for DL - SUB and DL - UNSUB
                                                    AND ISNULL(SAT.Descrip,'') IN ( 'Loan' )                    -- SFS.AwardTypeId IN ( 2 )
                                        ) AS TitleIVLoanRefund
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 0          --Non-TitleIv
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip NOT IN ( 'PERKINS','DL - PLUS','FFEL - PLUS' ) -- ST.AdvFundSourceId NOT IN (4,6,9)
                                                    AND ST.SAT_Descrip IN ( 'Loan' )                                   -- SFS.AwardTypeId IN (2)
                                        ) AS NonTitleIVLoanPayment
                                       ,(
                                          SELECT    SUM(ISNULL(SR.RefundAmount,0.00))
                                          FROM      saRefunds AS SR
                                          INNER JOIN @getTransactionsSummary AS ST ON ST.TransactionId = SR.TransactionId
                                          LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = SR.FundSourceId
                                          LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                          LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId
                                          WHERE     ST.Voided = 0
                                                    AND ISNULL(SFS.TitleIV,0) = 0		--Non-TitleIv
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ISNULL(SAFS.Descrip,'') NOT IN ( 'PERKINS','DL - PLUS','FFEL - PLUS' )  -- ST.AdvFundSourceId NOT IN (4,6,9)
                                                    AND ISNULL(SAT.Descrip,'') IN ( 'Loan' )                                    -- SFS.AwardTypeId IN ( 2 )
                                        ) AS NonTitleIVLoanRefund
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00))
                                          FROM      saTransactions AS ST
                                          INNER JOIN @getStudentsbyGEProgramsandDate AS GEP ON GEP.StuEnrollId = ST.StuEnrollId
                                          WHERE     ST.Voided = 0
                                                    AND GEP.StudentId = GS.StudentId
                                                    AND GEP.ProgId = GS.ProgId
                                        ) AS InstitutionalOwed
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip IN ( 'PELL','SEOG' )  -- ST.AdvFundSourceId IN ( 2,3 ) 
                                        ) AS TotalPellAward
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                        ) AS TitleIVAid
                              FROM      @getStudentsbyGEProgramsandDate GS
                              GROUP BY  GS.StudentId
                                       ,GS.ProgId
                            ) GSLA;
        END;
		
        BEGIN /** Step 3: Combine Step 1 and Step 2 and get the Final Student Data Output*/
            INSERT  INTO @getFinalStudentsList
                    SELECT  *
                    FROM    (
                              SELECT    ROW_NUMBER() OVER ( PARTITION BY GS.ProgId,GS.StudentId ORDER BY GS.StartDate ASC ) AS RowNumber
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(GS.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN GS.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN GS.StudentNumber
                                             ELSE GS.StudentNumber
                                        END AS StudentIdentification
                                       ,GS.StudentId
                                       ,RTRIM(GS.LastName + ', ' + GS.FirstName + ' ' + ISNULL(UPPER(SUBSTRING(GS.MiddleName,1,1)),'')) AS StudentName
                                       ,GS.DOB
                                       ,GS.CampusId
                                       ,GS.CampDescrip
                                       ,GS.OPEID
                                       ,GS.ProgId
                                       ,GS.ProgDescrip
                                       ,GS.CIPCode
                                       ,GS.CredentialCode
                                       ,GS.CredentialDescription
                                       ,GS.PrgVerId
                                       ,GS.PrgVerDescrip
                                       ,GS.StuEnrollId
                                       ,GS.SysStatusDescrip
                                       ,GS.StartDate
                                       ,ISNULL(GLA.TitleIVLoanAmt,0.00) AS TitleIVLoanAmt
                                       ,ISNULL(GLA.NonTitleIVLoanAmt,0.00) AS NonTitleIVLoanAmt
                                       ,ISNULL(GLA.InstitutionalOwed,0.00) AS InstitutionalOwed
                                       ,( ISNULL(GLA.TitleIVLoanAmt,0.00) + ISNULL(GLA.NonTitleIVLoanAmt,0.00) + ISNULL(GLA.InstitutionalOwed,0.00) ) AS TotalLoanAmt
                                       ,ISNULL(GLA.TotalPellAward,0.00) AS TotalPellAward
                                       ,ISNULL(GLA.TitleIVAid,0.00) AS TitleIVAid
                                       ,MIN(ContractedGradDate) OVER ( PARTITION BY GS.ProgId,GS.StudentId ) AS GraduationDate
                                       ,RevisedGradDate
                                       ,MIN(CASE WHEN GS.SysStatusDescrip = 'Graduated' THEN GS.RevisedGradDate
                                                 ELSE '2099-12-31'
                                            END) OVER ( PARTITION BY GS.ProgId,GS.StudentId ) AS RevisedGradDateCalculated
                                       ,( CASE WHEN (
                                                      ( MIN(CASE WHEN GS.SysStatusDescrip = 'Graduated' THEN GS.RevisedGradDate
                                                                 ELSE '2099-12-31'
                                                            END) OVER ( PARTITION BY GS.ProgId,GS.StudentId ) <= GS.ContractedGradDate )
                                                      AND ( ISNULL(GLA.TitleIVAid,0) > 0 )
                                                    ) THEN 'Yes'
                                               ELSE 'Not'
                                          END ) AS OnTimeCompleter
                              FROM      @getStudentsbyGEProgramsandDate GS
                              INNER JOIN @getStudentLoanAmount GLA ON GLA.StudentId = GS.StudentId
                                                                      AND GLA.ProgId = GS.ProgId
                            ) T1
                    WHERE   T1.RowNumber = 1
                    ORDER BY T1.ProgDescrip
                           ,T1.PrgVerDescrip
                           ,T1.StudentName;
        END;

        BEGIN /**Step 4: Calculate the MedianLoanAmount only for students who completed program on time and who received titleIV**/
            INSERT  INTO @PrgVersionMedian
                    SELECT  GFSL.ProgId
                           ,GFSL.PrgVerId
                           ,GFSL.TotalLoanAmt AS MedianLoanAmount
                    FROM    @getFinalStudentsList AS GFSL
                    WHERE   GFSL.OnTimeCompleter = 'Yes'
                    ORDER BY GFSL.PrgVerId
                           ,MedianLoanAmount;
        END;

        BEGIN /**Step 6: Calculate the Median Amount by Program version*/
            INSERT  INTO @getMedianLoanAmountbyProgramVersion
                    SELECT  X.PrgVerId
                           ,X.LoanAmount
                           ,X.rownumber
                           ,X.PrgVersionCount
                    FROM    (
                              SELECT    PVM.PrgVerId
                                       ,PVM.LoanAmount
                                       ,ROW_NUMBER() OVER ( PARTITION BY PVM.PrgVerId ORDER BY PVM.PrgVerId ) AS rownumber
                                       ,COUNT(*) OVER ( PARTITION BY PVM.PrgVerId ) AS PrgVersionCount
                              FROM      @PrgVersionMedian AS PVM
                            ) AS X
                    WHERE   X.rownumber IN ( ( X.PrgVersionCount + 1 ) / 2,( X.PrgVersionCount + 2 ) / 2 );

            INSERT  INTO @medianAmountbyProgramVersion
                    SELECT  GMLAPV.PrgVerId
                           ,AVG(GMLAPV.MedianAmount) AS MedianAmount
                    FROM    @getMedianLoanAmountbyProgramVersion AS GMLAPV
                    GROUP BY GMLAPV.PrgVerId;

        END; 

        BEGIN /**Step 7: Calculate the Median Amount by Program */
            INSERT  INTO @getMedianLoanAmountbyProgram
                    SELECT  X.ProgId
                           ,X.LoanAmount
                           ,X.rownumber
                           ,X.ProgramCount
                    FROM    (
                              SELECT    PVM.ProgId
                                       ,PVM.LoanAmount
                                       ,ROW_NUMBER() OVER ( PARTITION BY PVM.ProgId ORDER BY PVM.ProgId ) AS rownumber
                                       ,COUNT(*) OVER ( PARTITION BY PVM.ProgId ) AS ProgramCount
                              FROM      @PrgVersionMedian AS PVM
                            ) AS X
                    WHERE   X.rownumber IN ( ( X.ProgramCount + 1 ) / 2,( X.ProgramCount + 2 ) / 2 );

            INSERT  INTO @medianAmountbyProgram
                    SELECT  GMLAP.ProgId
                           ,AVG(GMLAP.MedianAmount) AS MedianAmount
                    FROM    @getMedianLoanAmountbyProgram AS GMLAP
                    GROUP BY GMLAP.ProgId;

        END;

        BEGIN /**Step 8: Return data to the report **/
            SELECT  *
                   ,(
                      SELECT    MLAP.MedianAmount
                      FROM      @medianAmountbyProgramVersion AS MLAP
                      WHERE     MLAP.PrgVerId = GFSL.PrgVerId
                    ) AS prgVersionMedianAmount
                   ,(
                      SELECT    MAP.MedianAmount
                      FROM      @medianAmountbyProgram AS MAP
                      WHERE     MAP.ProgId = GFSL.ProgId
                    ) AS programMedianAmount
            FROM    @getFinalStudentsList AS GFSL;
        END;
    END;     
--=================================================================================================
-- END  --  USP_MedianLoanDebt_RevisedGradDate
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_MedianLoanDebt_ProgramWeeks]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_ProgramWeeks
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_MedianLoanDebt_ProgramWeeks]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_MedianLoanDebt_ProgramWeeks;
    END;
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_MedianLoanDebt_ProgramWeeks]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_ProgramWeeks
--=================================================================================================
CREATE PROCEDURE USP_MedianLoanDebt_ProgramWeeks
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN 
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
    
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';					-- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'
        
        BEGIN /** Declare all temp tables in use **/
            DECLARE @getStudentsbyGEProgramsandDate TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,StatusCodeId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,CampusId UNIQUEIDENTIFIER
                ,StudentProgramStartDate DATE
                ,StartDate DATE
                ,SysStatusDescrip VARCHAR(50)
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(10)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerDescrip VARCHAR(100)
                ,Weeks DECIMAL(18,2)
                ,SSN VARCHAR(50)
                ,EnrollmentId VARCHAR(50)
                ,StudentNumber VARCHAR(50)
                ,LastName VARCHAR(50)
                ,FirstName VARCHAR(50)
                ,MiddleName VARCHAR(50)
                ,DOB DATE
                ,ContractedGradDate DATE
                ,RevisedGradDate DATE
                ,PRIMARY KEY ( StuEnrollId )
                );
            DECLARE @getStudentLoanAmount TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,ProgId UNIQUEIDENTIFIER
                ,TitleIVLoanAmt DECIMAL(18,2)
                ,NonTitleIVLoanAmt DECIMAL(18,2)
                ,InstitutionalOwed DECIMAL(18,2)
                ,TotalPellAward DECIMAL(18,2)
                ,TitleIVAid DECIMAL(18,2)
                );
            DECLARE @getFinalStudentsList TABLE
                (
                 RowNumber INT
                ,StudentIdentification VARCHAR(50)
                ,StudentId UNIQUEIDENTIFIER
                ,StudentName VARCHAR(100)
                ,DOB DATE
                ,CampusId UNIQUEIDENTIFIER
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(50)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerId UNIQUEIDENTIFIER
                ,PrgVerDescrip VARCHAR(100)
                ,StuEnrollId UNIQUEIDENTIFIER
                ,SysStatusDescrip VARCHAR(50)
                ,StartDate DATE
                ,TitleIVLoanAmt DECIMAL(18,2)
                ,NonTitleIVLoanAmt DECIMAL(18,2)
                ,InstitutionalOwed DECIMAL(18,2)
                ,TotalLoanAmt DECIMAL(18,2)
                ,TotalPellAward DECIMAL(18,2)
                ,TitleIV_Aid DECIMAL(18,2)
                ,GraduationDate DATE
                ,RevisedGradDate DATE
                ,RevisedGradDateCalculated DATE
                ,OnTimeCompleter VARCHAR(5)
                );
            DECLARE @getTransactionsSummary TABLE
                (
                 TransactionId UNIQUEIDENTIFIER
                ,TransAmount DECIMAL(18,2)
                ,AdvFundSourceId INT
                ,TitleIV INT
                ,Voided INT
                ,StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,AwardTypeId INT
                ,SAT_Descrip VARCHAR(50)
                ,ProgId UNIQUEIDENTIFIER
                ,SAFS_Descrip VARCHAR(80)
                );
            DECLARE @PrgVersionMedian TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,LoanAmount DECIMAL(18,2)
                );
            DECLARE @getMedianLoanAmountbyProgramVersion TABLE
                (
                 PrgVerId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                ,PrgVersionRowNumber INT
                ,ProgramVersionCount INT
                );
            DECLARE @getMedianLoanAmountbyProgram TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                ,ProgramRowNumber INT
                ,ProgramCount INT
                );
            DECLARE @medianAmountbyProgramVersion TABLE
                (
                 PrgVerId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                );
            DECLARE @medianAmountbyProgram TABLE
                (
                 ProgId UNIQUEIDENTIFIER
                ,MedianAmount DECIMAL(18,2)
                );
        END; 

        BEGIN /** Step 1 : Get Students enrolled in GE Program and who started in date range **/
            INSERT  INTO @getStudentsbyGEProgramsandDate
                    SELECT  GS.*
                    FROM    (
                              SELECT    AST.StudentId
                                       ,ASE.StuEnrollId
                                       ,ASE.StatusCodeId
                                       ,ASE.PrgVerId
                                       ,ASE.CampusId
                                       ,MIN(ISNULL(ASE.StartDate,'2099-12-31')) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS StudentProgramStartDate -- -- used only to filter student with at least one enrollments out of rangedat
                                       ,ASE.StartDate
                                       ,SSS.SysStatusDescrip
                                       ,SC.CampDescrip
                                       ,SC.OPEID
                                       ,AP.ProgId
                                       ,AP.ProgDescrip
                                       ,AP.CIPCode
                                       ,APC.CredentialCode
                                       ,APC.CredentialDescription
                                       ,APV.PrgVerDescrip
                                       ,APV.Weeks
                                       ,AST.SSN
                                       ,ASE.EnrollmentId
                                       ,AST.StudentNumber
                                       ,AST.LastName
                                       ,AST.FirstName
                                       ,AST.MiddleName
                                       ,AST.DOB
                                       ,ASE.ContractedGradDate
                                       ,ASE.ExpGradDate
                              FROM      arStudent AS AST
                              INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
                              INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                              INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                              INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                              LEFT JOIN arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                              INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                              WHERE     SC.CampusId = @CampusId
                                        AND (
                                              @ProgramIdList IS NULL
                                              OR AP.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgramIdList,',',1) )
                                            )
                                        AND AP.IsGEProgram = 1
                                        AND SSS.SysStatusId NOT IN ( 7,8 )
                            ) AS GS
                    WHERE   StudentProgramStartDate BETWEEN @StartDate AND @EndDate
                    ORDER BY StudentId;
        END;

        BEGIN /**Join saTransactions, saFundSources and getStudentsbyGEProgramsandDate*/
            INSERT  INTO @getTransactionsSummary
                    SELECT  ST.TransactionId
                           ,ISNULL(ST.TransAmount,0) AS TransAmount
                           ,ISNULL(SFS.AdvFundSourceId,0) AS AdvFundSourceId
                           ,ISNULL(SFS.TitleIV,0) AS TitleIV
                           ,ISNULL(ST.Voided,0) AS Voided
                           ,GS.StudentId
                           ,GS.StuEnrollId
                           ,ISNULL(SFS.AwardTypeId,0) AS AwardTypeId
                           ,ISNULL(SAT.Descrip,'') AS SAT_Descrip
                           ,GS.ProgId
                           ,ISNULL(SAFS.Descrip,'') AS SAFS_Descrip
                    FROM    saTransactions AS ST
                    INNER JOIN @getStudentsbyGEProgramsandDate AS GS ON GS.StuEnrollId = ST.StuEnrollId
                    LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                    LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId;
        END;

        BEGIN /** Step 2: From Step 1 results, get TitleIV Loan Payment, Refunds, TitleIV Aid, Pell Award for each student  **/
            INSERT  INTO @getStudentLoanAmount
                    SELECT  GSLA.StudentId
                           ,GSLA.ProgId
                           ,( ISNULL(GSLA.TitleIVLoanPayment,0.00) - ISNULL(GSLA.TitleIVLoanRefund,0.00) ) AS TitleIVLoanAmt
                           ,( ISNULL(GSLA.NonTitleIVLoanPayment,0.00) - ISNULL(GSLA.NonTitleIVLoanRefund,0.00) ) AS NonTitleIVLoanAmt
                           ,ISNULL(GSLA.InstitutionalOwed,0.00) AS InstitutionalOwed
                           ,ISNULL(GSLA.TotalPellAward,0.00) AS TotalPellAward
                           ,ISNULL(GSLA.TitleIVAid,0.00) AS TitleIVAid
                    FROM    (
                              SELECT    GS.StudentId
                                       ,GS.ProgId
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip IN ( 'DL - SUB','DL - UNSUB' )  -- ST.AdvFundSourceId IN ( 7,8 ) --AdvFundSourceId from syAdvFundSources for DL - SUB and DL - UNSUB
                                                    AND ST.SAT_Descrip IN ( 'Loan' )                    -- SFS.AwardTypeId IN (2)
                                        ) AS TitleIVLoanPayment
                                       ,(
                                          SELECT    SUM(ISNULL(SR.RefundAmount,0.00)) AS RefundAmount
                                          FROM      saRefunds AS SR
                                          INNER JOIN @getTransactionsSummary AS ST ON ST.TransactionId = SR.TransactionId
                                          LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = SR.FundSourceId
                                          LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                          LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId
                                          WHERE     ST.Voided = 0
                                                    AND ISNULL(SFS.TitleIV,0) = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ISNULL(SAFS.Descrip,'') IN ( 'DL - SUB','DL - UNSUB' )  -- ST.AdvFundSourceId IN ( 7,8 ) --AdvFundSourceId from syAdvFundSources for DL - SUB and DL - UNSUB
                                                    AND ISNULL(SAT.Descrip,'') IN ( 'Loan' )                    -- SFS.AwardTypeId IN ( 2 )
                                        ) AS TitleIVLoanRefund
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 0          --Non-TitleIv
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip NOT IN ( 'PERKINS','DL - PLUS','FFEL - PLUS' ) -- ST.AdvFundSourceId NOT IN (4,6,9)
                                                    AND ST.SAT_Descrip IN ( 'Loan' )                                   -- SFS.AwardTypeId IN (2)
                                        ) AS NonTitleIVLoanPayment
                                       ,(
                                          SELECT    SUM(ISNULL(SR.RefundAmount,0.00))
                                          FROM      saRefunds AS SR
                                          INNER JOIN @getTransactionsSummary AS ST ON ST.TransactionId = SR.TransactionId
                                          LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = SR.FundSourceId
                                          LEFT JOIN syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                          LEFT JOIN saAwardTypes AS SAT ON SAT.AwardTypeId = SFS.AwardTypeId
                                          WHERE     ST.Voided = 0
                                                    AND ISNULL(SFS.TitleIV,0) = 0		--Non-TitleIv
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ISNULL(SAFS.Descrip,'') NOT IN ( 'PERKINS','DL - PLUS','FFEL - PLUS' )  -- ST.AdvFundSourceId NOT IN (4,6,9)
                                                    AND ISNULL(SAT.Descrip,'') IN ( 'Loan' )                                    -- SFS.AwardTypeId IN ( 2 )
                                        ) AS NonTitleIVLoanRefund
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00))
                                          FROM      saTransactions AS ST
                                          INNER JOIN @getStudentsbyGEProgramsandDate AS GEP ON GEP.StuEnrollId = ST.StuEnrollId
                                          WHERE     ST.Voided = 0
                                                    AND GEP.StudentId = GS.StudentId
                                                    AND GEP.ProgId = GS.ProgId
                                        ) AS InstitutionalOwed
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                                    AND ST.SAFS_Descrip IN ( 'PELL','SEOG' )  -- ST.AdvFundSourceId IN ( 2,3 ) 
                                        ) AS TotalPellAward
                                       ,(
                                          SELECT    SUM(ISNULL(ST.TransAmount,0.00) * -1)
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                        ) AS TitleIVAid
                              FROM      @getStudentsbyGEProgramsandDate GS
                              GROUP BY  GS.StudentId
                                       ,GS.ProgId
                            ) GSLA;
        END;

        BEGIN /** Step 3: Combine Step 1 and Step 2 and get the Final Student Data Output*/
            INSERT  INTO @getFinalStudentsList
                    SELECT  *
                    FROM    (
                              SELECT    ROW_NUMBER() OVER ( PARTITION BY GS.ProgId,GS.StudentId ORDER BY GS.StartDate ASC ) AS RowNumber
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(GS.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN GS.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN GS.StudentNumber
                                             ELSE GS.StudentNumber
                                        END AS StudentIdentification
                                       ,GS.StudentId
                                       ,RTRIM(GS.LastName + ', ' + GS.FirstName + ' ' + ISNULL(UPPER(SUBSTRING(GS.MiddleName,1,1)),'')) AS StudentName
                                       ,GS.DOB
                                       ,GS.CampusId
                                       ,GS.CampDescrip
                                       ,GS.OPEID
                                       ,GS.ProgId
                                       ,GS.ProgDescrip
                                       ,GS.CIPCode
                                       ,GS.CredentialCode
                                       ,GS.CredentialDescription
                                       ,GS.PrgVerId
                                       ,GS.PrgVerDescrip
                                       ,GS.StuEnrollId
                                       ,GS.SysStatusDescrip
                                       ,GS.StartDate
                                       ,ISNULL(GLA.TitleIVLoanAmt,0.00) AS TitleIVLoanAmt
                                       ,ISNULL(GLA.NonTitleIVLoanAmt,0.00) AS NonTitleIVLoanAmt
                                       ,ISNULL(GLA.InstitutionalOwed,0.00) AS InstitutionalOwed
                                       ,( ISNULL(GLA.TitleIVLoanAmt,0.00) + ISNULL(GLA.NonTitleIVLoanAmt,0.00) + ISNULL(GLA.InstitutionalOwed,0.00) ) AS TotalLoanAmt
                                       ,ISNULL(GLA.TotalPellAward,0.00) AS TotalPellAward
                                       ,ISNULL(GLA.TitleIVAid,0.00) AS TitleIVAid
                                       ,DATEADD(DAY,
                                                dbo.GetTotalNoofHolidaysbtwGivenDatesandCampus(MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,GS.StudentId ),
                                                                                               DATEADD(WEEK,GS.Weeks,
                                                                                                       MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,
                                                                                                                                GS.StudentId )),GS.CampusId),
                                                DATEADD(WEEK,GS.Weeks,MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,GS.StudentId ))) AS GraduationDate
                                       ,GS.RevisedGradDate
                                       ,MIN(CASE WHEN GS.SysStatusDescrip = 'Graduated' THEN GS.RevisedGradDate
                                                 ELSE '2099-12-31'
                                            END) OVER ( PARTITION BY GS.ProgId,GS.StudentId ) AS RevisedGradDateCalculated
                                       ,( CASE WHEN (
                                                      ( MIN(CASE WHEN GS.SysStatusDescrip = 'Graduated' THEN GS.RevisedGradDate
                                                                 ELSE '2099-12-31'
                                                            END) OVER ( PARTITION BY GS.ProgId,GS.StudentId ) <= DATEADD(DAY,
                                                                                                                         dbo.GetTotalNoofHolidaysbtwGivenDatesandCampus(MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,
                                                                                                                                                GS.StudentId ),
                                                                                                                                                DATEADD(WEEK,
                                                                                                                                                GS.Weeks,
                                                                                                                                                MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,
                                                                                                                                                GS.StudentId )),
                                                                                                                                                GS.CampusId),
                                                                                                                         DATEADD(WEEK,GS.Weeks,
                                                                                                                                 MIN(GS.StartDate) OVER ( PARTITION BY GS.ProgId,
                                                                                                                                                GS.StudentId ))) )
                                                      AND ( ISNULL(GLA.TitleIVAid,0) > 0 )
                                                    ) THEN 'Yes'
                                               ELSE 'Not'
                                          END ) AS OnTimeCompleter
                              FROM      @getStudentsbyGEProgramsandDate AS GS
                              INNER JOIN @getStudentLoanAmount GLA ON GLA.StudentId = GS.StudentId
                                                                      AND GLA.ProgId = GS.ProgId
                            ) T1
                    WHERE   T1.RowNumber = 1
                    ORDER BY T1.ProgDescrip
                           ,T1.PrgVerDescrip
                           ,T1.StudentName;
        END;

        BEGIN /**Step 4: Calculate the MedianLoanAmount only for students who completed program on time and who received titleIV**/
            INSERT  INTO @PrgVersionMedian
                    SELECT  GFSL.ProgId
                           ,GFSL.PrgVerId
                           ,GFSL.TotalLoanAmt AS MedianLoanAmount
                    FROM    @getFinalStudentsList AS GFSL
                    WHERE   GFSL.OnTimeCompleter = 'Yes'
                    ORDER BY GFSL.PrgVerId
                           ,MedianLoanAmount;
        END;

        BEGIN /**Step 6: Calculate the Median Amount by Program version*/
            INSERT  INTO @getMedianLoanAmountbyProgramVersion
                    SELECT  X.PrgVerId
                           ,X.LoanAmount
                           ,X.rownumber
                           ,X.PrgVersionCount
                    FROM    (
                              SELECT    PVM.PrgVerId
                                       ,PVM.LoanAmount
                                       ,ROW_NUMBER() OVER ( PARTITION BY PVM.PrgVerId ORDER BY PVM.PrgVerId ) AS rownumber
                                       ,COUNT(*) OVER ( PARTITION BY PVM.PrgVerId ) AS PrgVersionCount
                              FROM      @PrgVersionMedian AS PVM
                            ) AS X
                    WHERE   X.rownumber IN ( ( X.PrgVersionCount + 1 ) / 2,( X.PrgVersionCount + 2 ) / 2 );

            INSERT  INTO @medianAmountbyProgramVersion
                    SELECT  GMLAPV.PrgVerId
                           ,AVG(GMLAPV.MedianAmount) AS MedianAmount
                    FROM    @getMedianLoanAmountbyProgramVersion AS GMLAPV
                    GROUP BY GMLAPV.PrgVerId;

        END; 

        BEGIN /**Step 7: Calculate the Median Amount by Program */
            INSERT  INTO @getMedianLoanAmountbyProgram
                    SELECT  X.ProgId
                           ,X.LoanAmount
                           ,X.rownumber
                           ,X.ProgramCount
                    FROM    (
                              SELECT    PVM.ProgId
                                       ,PVM.LoanAmount
                                       ,ROW_NUMBER() OVER ( PARTITION BY PVM.ProgId ORDER BY PVM.ProgId ) AS rownumber
                                       ,COUNT(*) OVER ( PARTITION BY PVM.ProgId ) AS ProgramCount
                              FROM      @PrgVersionMedian AS PVM
                            ) AS X
                    WHERE   X.rownumber IN ( ( X.ProgramCount + 1 ) / 2,( X.ProgramCount + 2 ) / 2 );

            INSERT  INTO @medianAmountbyProgram
                    SELECT  GMLAP.ProgId
                           ,AVG(GMLAP.MedianAmount) AS MedianAmount
                    FROM    @getMedianLoanAmountbyProgram AS GMLAP
                    GROUP BY GMLAP.ProgId;

        END;

        BEGIN /**Step 8: Return data to the report **/
            SELECT  *
                   ,(
                      SELECT    MLAP.MedianAmount
                      FROM      @medianAmountbyProgramVersion AS MLAP
                      WHERE     MLAP.PrgVerId = GFSL.PrgVerId
                    ) AS prgVersionMedianAmount
                   ,(
                      SELECT    MAP.MedianAmount
                      FROM      @medianAmountbyProgram AS MAP
                      WHERE     MAP.ProgId = GFSL.ProgId
                    ) AS programMedianAmount
            FROM    @getFinalStudentsList AS GFSL;
        END;
    END;		    
--=================================================================================================
-- END  --  USP_MedianLoanDebt_ProgramWeeks
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Dropping [dbo].[USP_MedianLoanDebt_Details]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_Details
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_MedianLoanDebt_Details]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_MedianLoanDebt_Details;
    END;
    
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
PRINT N'Creating [dbo].[USP_MedianLoanDebt_Details]';
GO
--=================================================================================================
-- USP_MedianLoanDebt_Details
--=================================================================================================
CREATE PROCEDURE dbo.USP_MedianLoanDebt_Details
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@RevGradDateType AS VARCHAR(50)
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        IF ( @RevGradDateType = 'fromprogram' )
            BEGIN
                EXEC dbo.USP_MedianLoanDebt_ProgramWeeks @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
        ELSE
            BEGIN
                EXEC dbo.USP_MedianLoanDebt_RevisedGradDate @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
    
    END;
--=================================================================================================
-- END  --  USP_MedianLoanDebt_Details
--=================================================================================================    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

COMMIT TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT;
SET @Success = 1;
SET NOEXEC OFF;
IF ( @Success = 1 )
    PRINT 'The database update succeeded';
ELSE
    BEGIN
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
        PRINT 'The database update failed';
    END;
GO
