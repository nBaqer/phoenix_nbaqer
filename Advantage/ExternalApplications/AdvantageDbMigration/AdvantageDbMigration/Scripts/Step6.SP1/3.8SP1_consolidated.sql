-- ===============================================================================================
-- Consolidated Script Version 3.8SP1
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- ===============================================================================================
-- =======================================================
--START DE13320 Urgent: Advantage Escalation User Permission (Beta)
-- =======================================================
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 837
                        AND Resource = 'Pell Recipients & Stafford Sub Recipients without Pell' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 837 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
                       ,ResourceTypeID = 5
                       ,ResourceURL = '~/sy/ParamReport.aspx'
                       ,SummListId = NULL
                       ,ChildTypeId = 5
                       ,ModDate = GETDATE()
                       ,ModUser = 'sa'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 1
                       ,UsedIn = 975
                       ,TblFldsId = NULL
                       ,DisplayName = NULL
                WHERE   ResourceID = 837;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
	                    )
                VALUES  (
                         837  -- ResourceID - smallint
                        ,'Pell Recipients & Stafford Sub Recipients without Pell'  -- Resource - varchar(200)
                        ,5  -- ResourceTypeID - tinyint
                        ,'~/sy/ParamReport.aspx'  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,5  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'sa'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,1  -- MRUTypeId - smallint
                        ,975  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,NULL  -- DisplayName - varchar(200)
	                    );
            END;	
    END;	
GO	
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 838
                        AND Resource = 'History' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 838 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'History'
                       ,ResourceTypeID = 3
                       ,ResourceURL = '~/AD/AleadHistory.aspx'
                       ,SummListId = NULL
                       ,ChildTypeId = NULL
                       ,ModDate = GETDATE()
                       ,ModUser = 'support'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 4
                       ,UsedIn = 975
                       ,TblFldsId = NULL
                       ,DisplayName = 'History'
                WHERE   ResourceID = 838;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
                        )
                VALUES  (
                         838  -- ResourceID - smallint
                        ,'History'  -- Resource - varchar(200)
                        ,3  -- ResourceTypeID - tinyint
                        ,'~/AD/AleadHistory.aspx'  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,NULL  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'support'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,4  -- MRUTypeId - smallint
                        ,975  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,'History'  -- DisplayName - varchar(200)
                        );
            END;	
    END;	
GO	

IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 839
                        AND Resource = 'Winter - Admissions' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 839 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'Winter - Admissions'
                       ,ResourceTypeID = 2
                       ,ResourceURL = NULL
                       ,SummListId = NULL
                       ,ChildTypeId = 5
                       ,ModDate = GETDATE()
                       ,ModUser = 'support'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 1
                       ,UsedIn = 512
                       ,TblFldsId = NULL
                       ,DisplayName = NULL
                WHERE   ResourceID = 839;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
	                    )
                VALUES  (
                         839  -- ResourceID - smallint
                        ,'Winter - Admissions'  -- Resource - varchar(200)
                        ,2  -- ResourceTypeID - tinyint
                        ,NULL  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,5  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'support'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,1  -- MRUTypeId - smallint
                        ,512  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,NULL  -- DisplayName - varchar(200)
	                    );
            END;	
    END;	
GO	
IF NOT EXISTS ( SELECT  *
                FROM    syReports
                WHERE   ReportName = 'PellRecipientsandStaffordDetailReport'
                        AND ResourceId = 837 )
    BEGIN
        UPDATE  syReports
        SET     ResourceId = 837
        WHERE   ReportName = 'PellRecipientsandStaffordDetailReport';
    END;	
GO	
DECLARE @ParentId UNIQUEIDENTIFIER
   ,@ResourceId INT
   ,@ParentResourceId INT;
-- IPEDS --> winter admissions
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'IPEDS - General Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            ParentId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );

--Set @ResourceId = (select Top 1 ResourceId from syResources where Resource='Winter - Admissions')
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Winter - Admissions'
                            AND ResourceTypeID = 2
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
-- IPEDS --> winter admissions --> All Inst - Test Scores - Detail
--Set @ParentResourceId=(select Top 1 ResourceId from syResources where Resource='Winter - Admissions')
SET @ParentResourceId = (
                          SELECT    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Winter - Admissions'
                                    AND ResourceTypeID = 2
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );

SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Detail'
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Summary'
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
----Spring - Fall Enrollment - All Institution Reports -->  Part A - Detail & Summary - Dist Ed
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Spring - Fall Enrollment - All Institution Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Part A - Detail & Summary - Dist. Ed'
                  );
 --done successfully
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
-----Reports > IPEDS Reports > Winter Student FinAid > Part II - Military Benefits - Detail Sumary
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Winter - Student Financial Aid Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Part II - Military Benefits - Detail & Summary'
                  );
 --done successfully
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
	GO	
--473;
 -- previously 207

--Reports > IPEDS Reports > Fall-Completions - All Institutions - Detail & Summary Reports > Detail
--Reports > IPEDS Reports > Fall-Completions - All Institutions - Detail & Summary Reports > Summary
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   Resource IN ( 'Detail','Summary' )
                        AND UsedIn = 975 )
    BEGIN	
        UPDATE  syResources
        SET     UsedIn = 975
               ,ModDate = GETDATE()
               ,ModUser = 'sa'
        WHERE   Resource IN ( 'Detail','Summary' );
    END;	
--( 444,445 );
-- previously where 0
--Reports > Academics > General Reports > Time Clock Exceptions
--Reports > Academics > General Reports > Time Clock Punches
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   Resource IN ( 'Time Clock Exceptions','Time Clock Punches' )
                        AND UsedIn = 975 )
    BEGIN	
        UPDATE  syResources
        SET     UsedIn = 975
               ,ModDate = GETDATE()
               ,ModUser = 'sa'
        WHERE   Resource IN ( 'Time Clock Exceptions','Time Clock Punches' );
    END;	
--( 585,586 );
 -- previously 512
 GO
DECLARE @hAcademic UNIQUEIDENTIFIER
   ,@actualHierarchyId UNIQUEIDENTIFIER
   ,@reqResourceId INT
   ,@academicresourceId INT
   ,@studentResourceId INT
   ,@actualParentId UNIQUEIDENTIFIER
   ,@attendanceResId INT;
SET @attendanceResId = (
                         SELECT ResourceID
                         FROM   syResources
                         WHERE  Resource = 'Attendance'
                                AND ResourceTypeID = 2
                       );
SET @academicresourceId = (
                            SELECT  ResourceID
                            FROM    syResources
                            WHERE   Resource = 'Academics'
                                    AND ResourceTypeID = 1
                          );
SET @reqResourceId = (
                       SELECT   ResourceID
                       FROM     syResources
                       WHERE    Resource = 'Post Externship Attendance'
                     );
SET @studentResourceId = (
                           SELECT   ResourceID
                           FROM     syResources
                           WHERE    Resource = 'Student'
                         );
SET @hAcademic = (
                   SELECT TOP 1
                            HierarchyId
                   FROM     syNavigationNodes
                   WHERE    ResourceId = @academicresourceId
                 );
SET @actualHierarchyId = (
                           SELECT   HierarchyId
                           FROM     syNavigationNodes
                           WHERE    ResourceId = @studentResourceId
                                    AND ParentId = @hAcademic
                         );
SET @actualParentId = (
                        SELECT  HierarchyId
                        FROM    syNavigationNodes
                        WHERE   ParentId = @actualHierarchyId
                                AND ResourceId = @attendanceResId
                      );

IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @reqResourceId
                        AND ParentId = @actualParentId )
    BEGIN	
--SELECT @actualParentId
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()  -- HierarchyId - uniqueidentifier
                ,4  -- HierarchyIndex - smallint
                ,@reqResourceId -- ResourceId - smallint
                ,@actualParentId  -- ParentId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,0  -- IsPopupWindow - bit
                ,1  -- IsShipped - bit
                );
    END;
GO
-- =======================================================
--END DE13320 Urgent: Advantage Escalation User Permission (Beta)
-- =======================================================
-- ********************************************************************************************************
-- CREATE support for KLASS APP
-- Author JAGG
-- Enter the module name for KLASS_APP_SERVICE
-- ********************************************************************************************************
IF NOT EXISTS ( SELECT  *
                FROM    dbo.syWapiExternalOperationMode
                WHERE   Code = 'KLASS_APP_SERVICE' )
    BEGIN

        INSERT  INTO dbo.syWapiExternalOperationMode
                (
                 Code
                ,Description
                ,IsActive
                )
        VALUES  (
                 'KLASS_APP_SERVICE'  -- Code - varchar(50)
                ,'Communication with KlassApp'  -- Description - varchar(100)
                ,1  -- IsActive - bit
                );
    END;
GO

-- ********************************************************************************************************
-- CREATE support for KLASS APP
-- Author JAGG
-- Populate syKlassOperationType Table
-- ********************************************************************************************************

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 1 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 1	 -- Id - int
                ,'locations'  -- Code - varchar(20)
                ,'the campus identification'  -- Description - varchar(100)
	            );
    END;
ELSE
    BEGIN
        UPDATE  dbo.syKlassOperationType
        SET     Code = 'locations'
        WHERE   KlassOperationTypeId = 1;
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 2 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 2	 -- Id - int
                ,'studentStatus'  -- Code - varchar(20)
                ,'student status'  -- Description - varchar(100)
                );
    END;
ELSE
    BEGIN
        UPDATE  dbo.syKlassOperationType
        SET     Code = 'studentStatus'
        WHERE   KlassOperationTypeId = 2;
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 3 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 3	 -- Id - int
                ,'department'  -- Code - varchar(20)
                ,'student program version'  -- Description - varchar(100)
	            );
    END;
ELSE
    BEGIN
        UPDATE  dbo.syKlassOperationType
        SET     Code = 'department'
        WHERE   KlassOperationTypeId = 3;
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 4 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 4	 -- Id - int
                ,'totalhours'  -- Code - varchar(20)
                ,'student total hours attendance'  -- Description - varchar(100)
	            );
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 5 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 5	 -- Id - int
                ,'absenthours'  -- Code - varchar(20)
                ,'total hours absent'  -- Description - varchar(100)
	            );
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 6 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 6	 -- Id - int
                ,'makeuphours'  -- Code - varchar(20)
                ,'student total make up hours'  -- Description - varchar(100)
	            );
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 7 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 7	 -- Id - int
                ,'lda'  -- Code - varchar(20)
                ,'last date of attendance'  -- Description - varchar(100)
	            );
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 8 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 8	 -- Id - int
                ,'attendancepercent'  -- Code - varchar(20)
                ,'attendance percent'  -- Description - varchar(100)
	            );
    END;

IF NOT EXISTS ( SELECT  KlassOperationTypeId
                FROM    dbo.syKlassOperationType
                WHERE   KlassOperationTypeId = 9 )
    BEGIN
        INSERT  INTO dbo.syKlassOperationType
                (
                 KlassOperationTypeId
                ,Code
                ,Description
                )
        VALUES  (
                 9	 -- Id - int
                ,'gpo'  -- Code - varchar(20)
                ,'gpo exams'  -- Description - varchar(100)
                );
    END;

GO

-- ********************************************************************************************************
-- CREATE support for KLASS APP
-- Author JAGG
-- Populate SyKlassEntity Table
-- ********************************************************************************************************
IF NOT EXISTS ( SELECT  KlassEntityId
                FROM    dbo.syKlassEntity
                WHERE   KlassEntityId = 1 )
    BEGIN
        INSERT  INTO dbo.syKlassEntity
                (
                 KlassEntityId
                ,Code
                ,Description
                )
        VALUES  (
                 1	 -- Id - int
                ,'students'  -- Code - varchar(20)
                ,'Parameter related to the class_type students'  -- Description - varchar(100)
	            );
    END;
GO

-- ********************************************************************************************************
-- CREATE support for KLASS APP. Configuration Settings
-- Author JAGG
-- Enter the configuration setting KlassApp_DaysToConsider and set to 30 days
-- ********************************************************************************************************
--DECLARE @KeyName VARCHAR(50) = 'KlassApp_DaysToConsider';
 
--IF NOT EXISTS ( SELECT  *
--                FROM    syConfigAppSettings
--                WHERE   KeyName = @KeyName )
--    BEGIN
--        BEGIN TRANSACTION Temp1;
--        DECLARE @MaxId INT = (
--                               SELECT   MAX(SettingId)
--                               FROM     dbo.syConfigAppSettings
--                             );
--        SET @MaxId = @MaxId + 1;
 
--        INSERT  INTO dbo.syConfigAppSettings
--                (
--                 SettingId
--                ,KeyName
--                ,Description
--                ,ModUser
--                ,ModDate
--                ,CampusSpecific
--                ,ExtraConfirmation
--                )
--        VALUES  (
--                 @MaxId    -- SettingId - int
--                ,@KeyName  -- KeyName - varchar(200)
--                ,'Integer, number of day to consider to see if the student change any variable considered in klass app'  -- Description - varchar(1000)
--                ,'Support'  -- ModUser - varchar(50)
--                ,GETDATE()  -- ModDate - datetime
--                ,0  -- CampusSpecific - bit
--                ,0  -- ExtraConfirmation - bit
--                ); 
 
--        INSERT  INTO dbo.syConfigAppSetValues
--                (
--                 ValueId
--                ,SettingId
--                ,CampusId
--                ,Value
--                ,ModUser
--                ,ModDate
--                ,Active
--		        )
--        VALUES  (
--                 NEWID()  -- ValueId - uniqueidentifier
--                ,@MaxId  -- SettingId - int
--                ,NULL  -- CampusId - uniqueidentifier
--                ,'0'  -- Value - varchar(1000)
--                ,'Support'  -- ModUser - varchar(50)
--                ,GETDATE()  -- ModDate - datetime
--                ,1  -- Active - bit
--		        );
 
--        COMMIT TRANSACTION TEMP1;
--    END;
--GO

--IF EXISTS ( SELECT  *
--            FROM    syConfigAppSettings
--            WHERE   KeyName = 'KlassApp_DaysToConsider' )
--    BEGIN
--        UPDATE  syConfigAppSetValues
--        SET     Value = '30'
--        WHERE   SettingId = (
--                              SELECT    SettingId
--                              FROM      dbo.syConfigAppSettings
--                              WHERE     KeyName = 'KlassApp_DaysToConsider'
--                            );
--    END;
--GO
-- ********************************************************************************************************
-- CREATE support for KLASS APP
-- Author JAGG
-- Enter the configuration setting KlassApp_UrlConfigCustomFields and set to 
-- ********************************************************************************************************
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigCustomFields';
-- insert field 
IF NOT EXISTS ( SELECT  *
                FROM    syConfigAppSettings
                WHERE   KeyName = @KeyName )
    BEGIN
        BEGIN TRANSACTION Temp1;
        DECLARE @MaxId INT = (
                               SELECT   MAX(SettingId)
                               FROM     dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;
 
        INSERT  INTO dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @MaxId    -- SettingId - int
                ,@KeyName  -- KeyName - varchar(200)
                ,'Integer, number of day to consider to see if the student change any variable considered in klass app'  -- Description - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,0  -- CampusSpecific - bit
                ,0  -- ExtraConfirmation - bit
                ); 
 
        INSERT  INTO dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
		        )
        VALUES  (
                 NEWID()  -- ValueId - uniqueidentifier
                ,@MaxId  -- SettingId - int
                ,NULL  -- CampusId - uniqueidentifier
                ,'0'  -- Value - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,1  -- Active - bit
		        );
 
        COMMIT TRANSACTION TEMP1;
    END;
GO
-- This part is optional and be use to pre-set a value in the field
-- Important change to https on production!!
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigCustomFields';

IF EXISTS ( SELECT  *
            FROM    syConfigAppSettings s
            JOIN    dbo.syConfigAppSetValues v ON v.SettingId = s.SettingId
            WHERE   KeyName = @KeyName
                    AND v.Value <> 'http://api.klassapp.com/customfields/1.0' )
    BEGIN
        UPDATE  syConfigAppSetValues
        SET     Value = 'http://api.klassapp.com/customfields/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
    END;
GO
-- ********************************************************************************************************
-- Enter the configuration setting KlassApp_UrlConfigLocations and set to 
-- ********************************************************************************************************
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigLocations';

-- insert field 
 
IF NOT EXISTS ( SELECT  *
                FROM    syConfigAppSettings
                WHERE   KeyName = @KeyName )
    BEGIN
        BEGIN TRANSACTION Temp1;
        DECLARE @MaxId INT = (
                               SELECT   MAX(SettingId)
                               FROM     dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;
 
        INSERT  INTO dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @MaxId    -- SettingId - int
                ,@KeyName  -- KeyName - varchar(200)
                ,'Integer, number of day to consider to see if the student change any variable considered in klass app'  -- Description - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,0  -- CampusSpecific - bit
                ,0  -- ExtraConfirmation - bit
                ); 
 
        INSERT  INTO dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
		        )
        VALUES  (
                 NEWID()  -- ValueId - uniqueidentifier
                ,@MaxId  -- SettingId - int
                ,NULL  -- CampusId - uniqueidentifier
                ,'0'  -- Value - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,1  -- Active - bit
		        );
 
        COMMIT TRANSACTION TEMP1;
    END;
GO
-- This part is optional and be use to pre-set a value in the field 
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigLocations';
IF EXISTS ( SELECT  *
            FROM    syConfigAppSettings s
            JOIN    dbo.syConfigAppSetValues v ON v.SettingId = s.SettingId
            WHERE   KeyName = @KeyName
                    AND v.Value <> 'http://api.klassapp.com/locations/1.0' )
    BEGIN
        UPDATE  syConfigAppSetValues
        SET     Value = 'http://api.klassapp.com/locations/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
    END;
GO
-- ********************************************************************************************************
-- CREATE support for KLASS APP. Configuration Settings
-- Author JAGG
-- Enter the configuration setting KlassApp_UrlConfigKeyValuePairs and set for a moment ALL
-- ********************************************************************************************************

DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigKeyValuePairs';

-- insert field 
 
IF NOT EXISTS ( SELECT  *
                FROM    syConfigAppSettings
                WHERE   KeyName = @KeyName )
    BEGIN
        BEGIN TRANSACTION Temp1;
        DECLARE @MaxId INT = (
                               SELECT   MAX(SettingId)
                               FROM     dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;
 
        INSERT  INTO dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @MaxId    -- SettingId - int
                ,@KeyName  -- KeyName - varchar(200)
                ,'Integer, number of day to consider to see if the student change any variable considered in klass app'  -- Description - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,0  -- CampusSpecific - bit
                ,0  -- ExtraConfirmation - bit
                ); 
 
        INSERT  INTO dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
		        )
        VALUES  (
                 NEWID()  -- ValueId - uniqueidentifier
                ,@MaxId  -- SettingId - int
                ,NULL  -- CampusId - uniqueidentifier
                ,'0'  -- Value - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,1  -- Active - bit
		        );
 
        COMMIT TRANSACTION TEMP1;
    END;
GO


-- This part is optional and be use to pre-set a value in the field
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigKeyValuePairs';
IF EXISTS ( SELECT  *
            FROM    syConfigAppSettings s
            JOIN    dbo.syConfigAppSetValues v ON v.SettingId = s.SettingId
            WHERE   KeyName = @KeyName
                    AND v.Value <> 'http://api.klassapp.com/keyvaluepairs/1.0' )
    BEGIN
        UPDATE  syConfigAppSetValues
        SET     Value = 'http://api.klassapp.com/keyvaluepairs/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
    END;
GO

-- ********************************************************************************************************
-- CREATE support for KLASS APP. Configuration Settings
-- Author JAGG
-- Enter the configuration setting KlassApp_CampusToConsider and set for a moment ALL
-- ********************************************************************************************************
DECLARE @KeyName VARCHAR(50) = 'KlassApp_CampusToConsider';
 
IF NOT EXISTS ( SELECT  *
                FROM    syConfigAppSettings
                WHERE   KeyName = @KeyName )
    BEGIN
        BEGIN TRANSACTION KlassApp_CampusToConsider;
        DECLARE @MaxId INT = (
                               SELECT   MAX(SettingId)
                               FROM     dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;
 
        INSERT  INTO dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @MaxId    -- SettingId - int
                ,@KeyName  -- KeyName - varchar(200)
                ,'0/1 Value Determine if the Campus student must be send to KLASS APP'  -- Description - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,1  -- CampusSpecific - bit
                ,0  -- ExtraConfirmation - bit
                ); 
 
        INSERT  INTO dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
		        )
        VALUES  (
                 NEWID()  -- ValueId - uniqueidentifier
                ,@MaxId  -- SettingId - int
                ,NULL  -- CampusId - uniqueidentifier
                ,'0'  -- Value - varchar(1000)
                ,'Support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,1  -- Active - bit
		        );
 
        COMMIT TRANSACTION KlassApp_CampusToConsider;
    END;
 
GO

-- ********************************************************************************************************
-- CREATE support for KLASS APP. Configuration Settings
-- Author JAGG
-- Enter the configuration setting KlassApp_UrlConfigMajors and set for a moment ALL
-- ********************************************************************************************************
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigMajors';
 
IF NOT EXISTS ( SELECT  *
                FROM    syConfigAppSettings
                WHERE   KeyName = @KeyName )
    BEGIN
        BEGIN TRANSACTION KlassApp_UrlConfigMajors;
        DECLARE @MaxId INT = (
                               SELECT   MAX(SettingId)
                               FROM     dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;
 
        INSERT  INTO dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @MaxId
                ,    -- SettingId - int
                 @KeyName
                ,  -- KeyName - varchar(200)
                 'Klass APP URL for majors operation'
                ,  -- Description - varchar(1000)
                 'Support'
                ,  -- ModUser - varchar(50)
                 GETDATE()
                ,  -- ModDate - datetime
                 0
                ,  -- CampusSpecific - bit
                 0  -- ExtraConfirmation - bit
                ); 
 
        INSERT  INTO dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
		        )
        VALUES  (
                 NEWID()
                ,  -- ValueId - uniqueidentifier
                 @MaxId
                , -- SettingId - int
                 NULL
                ,  -- CampusId - uniqueidentifier
                 '0'
                ,  -- Value - varchar(1000)
                 'Support'
                ,  -- ModUser - varchar(50)
                 GETDATE()
                ,  -- ModDate - datetime
                 1  -- Active - bit
		        );
 
        COMMIT TRANSACTION KlassApp_UrlConfigMajors;
    END;
GO
-- This part is optional and be use to pre-set a value in the field
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigMajors';
IF EXISTS ( SELECT  *
            FROM    syConfigAppSettings
            WHERE   KeyName = @KeyName )
    BEGIN
        UPDATE  syConfigAppSetValues
        SET     Value = 'https://api.klassapp.com/majors/1.1'
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
    END;
GO
-- ===============================================================================================
-- Create the KLASS APP Operations in WAPI Allowed Services Table
-- ===============================================================================================
DECLARE @KlassGetConfigurationCode VARCHAR(50) = 'KLASS_GET_CONFIGURATION_STATUS';

IF NOT EXISTS ( SELECT  *
                FROM    syWapiAllowedServices
                WHERE   Code = @KlassGetConfigurationCode )
    BEGIN
        INSERT  INTO dbo.syWapiAllowedServices
                (
                 Code
                ,Description
                ,Url
                ,IsActive
		        )
        VALUES  (
                 @KlassGetConfigurationCode
                ,'Service to get or set configuration settings from KLASS APP'
                ,'http://localhost/Advantage/Current/Services/api/SystemStuff/Mobile/KlassAppUi/Get'
                ,1
		        );
    END;
GO

DECLARE @KlassGetStudentsCode VARCHAR(50) = 'KLASS_GET_STUDENT_INFORMATION';

IF NOT EXISTS ( SELECT  *
                FROM    syWapiAllowedServices
                WHERE   Code = @KlassGetStudentsCode )
    BEGIN
        INSERT  INTO dbo.syWapiAllowedServices
                (
                 Code
                ,Description
                ,Url
                ,IsActive
		        )
        VALUES  (
                 @KlassGetStudentsCode
                ,'Service to get or set Student Information from Advantage to be send to KLASS APP'
                ,'http://localhost/Advantage/Current/Services/api/SystemStuff/Mobile/KlassApp/Get'
                ,1
		        );
    END;
GO

-- ===============================================================================================
-- Verify that the default user of advantage is in place...
-- if not create it
-- ===============================================================================================
DECLARE @WindowsServiceToken VARCHAR(50) = 'FAME_2154673_default';
IF NOT EXISTS ( SELECT  *
                FROM    syWapiExternalCompanies
                WHERE   Code = @WindowsServiceToken )
    BEGIN
        INSERT  INTO dbo.syWapiExternalCompanies
                (
                 Code
                ,Description
                ,IsActive
                )
        VALUES  (
                 @WindowsServiceToken -- Code - varchar(50)
                ,'The default token for the windows service'  -- Description - varchar(100)
                ,1  -- IsActive - bit
                );
    END;
GO

-- ===============================================================================================
-- Create the default user if not exists for the windows service
-- ===============================================================================================
--DECLARE @WindowsServiceToken VARCHAR(50) = 'FAME_2154673_default';
--IF NOT EXISTS ( SELECT  *
--                FROM    syWapiExternalCompanies
--                WHERE   Code = @WindowsServiceToken )
--    BEGIN
--       INSERT INTO dbo.syWapiExternalCompanies
--               ( Code,Description,IsActive )
--       VALUES  ( @WindowsServiceToken -- Code - varchar(50)
--                 ,'The default token for the windows service'  -- Description - varchar(100)
--                 ,1  -- IsActive - bit
--                 )
--    END;
--GO

-- ===============================================================================================
-- Authorize the windows service token to use the klass app operations
-- ===============================================================================================
--DECLARE @KlassGetConfigurationCode VARCHAR(50) = 'KLASS_GET_CONFIGURATION_STATUS';
--DECLARE @KlassGetStudentsCode VARCHAR(50) = 'KLASS_GET_STUDENT_INFORMATION';
--DECLARE @WindowsServiceToken VARCHAR(50) = 'FAME_2154673_default';
---- Get id for services to be allowed
--DECLARE @SERV1 INT = (SELECT Id FROM syWapiAllowedServices WHERE Code = @KlassGetConfigurationCode);
--DECLARE @SERV2 INT = (SELECT Id FROM syWapiAllowedServices WHERE Code = @KlassGetStudentsCode);
--DECLARE @TOKEN INT = (SELECT Id FROM syWapiExternalCompanies WHERE Code = @WindowsServiceToken);

--IF NOT EXISTS ( SELECT  *
--                FROM    syWapiBridgeExternalCompanyAllowedServices
--                WHERE   IdExternalCompanies = @TOKEN AND IdWapiBridgeAllowedService = @SERV1 )
--    BEGIN
--       INSERT INTO dbo.syWapiBridgeExternalCompanyAllowedServices
--               (
--                IdExternalCompanies
--               ,IdAllowedServices
--               )
--       VALUES  (
--                @TOKEN  -- IdExternalCompanies - int
--               ,@SERV1  -- IdAllowedServices - int
--               )
--    END;

--IF NOT EXISTS ( SELECT  *
--                FROM    syWapiBridgeExternalCompanyAllowedServices
--                WHERE   IdExternalCompanies = @TOKEN AND IdWapiBridgeAllowedService = @SERV2 )
--    BEGIN
--       INSERT INTO dbo.syWapiBridgeExternalCompanyAllowedServices
--               (
--                IdExternalCompanies
--               ,IdAllowedServices
--               )
--       VALUES  (
--                @TOKEN  -- IdExternalCompanies - int
--               ,@SERV2  -- IdAllowedServices - int
--               )
--    END;
--GO
-- ===============================================================================================
-- Create the type of operation for KLASS APP in the list of external services allowed.
-- ===============================================================================================
DECLARE @OperationModeCode VARCHAR(50) = 'KLASS_APP_SERVICE';
IF NOT EXISTS ( SELECT  *
                FROM    syWapiExternalOperationMode
                WHERE   Code = @OperationModeCode )
    BEGIN
        INSERT  INTO dbo.syWapiExternalOperationMode
                (
                 Code
                ,Description
                ,IsActive
                )
        VALUES  (
                 @OperationModeCode  --The operation code
                ,'Service to send student information to KlassApp'
                ,1
                );
    END;
GO
-- ===============================================================================================
-- Configure initial the windows service to send to klass app information.
-- ===============================================================================================
--DECLARE @KlassGetConfigurationCode VARCHAR(50) = 'KLASS_GET_CONFIGURATION_STATUS';
--DECLARE @KlassGetStudentsCode VARCHAR(50) = 'KLASS_GET_STUDENT_INFORMATION';
--DECLARE @WindowsServiceToken VARCHAR(50) = 'FAME_2154673_default';
--DECLARE @OperationModeCode VARCHAR(50) = 'KLASS_APP_SERVICE'
---- Get id for services to be allowed
--DECLARE @SERV1 INT = (SELECT Id FROM syWapiAllowedServices WHERE Code = @KlassGetConfigurationCode);
--DECLARE @SERV2 INT = (SELECT Id FROM syWapiAllowedServices WHERE Code = @KlassGetStudentsCode);
--DECLARE @TOKEN INT = (SELECT Id FROM syWapiExternalCompanies WHERE Code = @WindowsServiceToken);
--DECLARE @ExternalperationId INT = (SELECT Id FROM dbo.syWapiExternalOperationMode WHERE Code = @OperationModeCode)
--DECLARE @ExternalOperationModeCode VARCHAR(50) = 'KLASS_APP_STUDENT'
--IF NOT EXISTS ( SELECT  *
--                FROM    syWapiSettings
--                WHERE   CodeOperation = @OperationModeCode )
--    BEGIN
--       INSERT INTO dbo.syWapiSettings
--               (
--                CodeOperation
--               ,ExternalUrl
--               ,IdExtCompany
--               ,IdExtOperation
--               ,IdAllowedService
--               ,IdSecondAllowedService
--               ,FirstAllowedServiceQueryString
--               ,SecondAllowedServiceQueryString
--               ,ConsumerKey
--               ,PrivateKey
--               ,OperationSecondTimeInterval
--               ,PollSecondForOnDemandOperation
--               ,FlagOnDemandOperation
--               ,FlagRefreshConfiguration
--               ,IsActive
--               ,DateMod
--               ,DateLastExecution
--               ,UserMod
--               )
--       VALUES  (
--                'KLASS_APP_STUDENT'  -- CodeOperation - varchar(50)
--               ,'https://api.klassapp.com/'  -- ExternalUrl - varchar(2048)
--               ,@TOKEN  -- IdExtCompany - int
--               ,@ExternalperationId  -- IdExtOperation - int
--               ,@SERV1  -- IdAllowedService - int
--               ,@SERV2  -- IdSecondAllowedService - int
--               ,''  -- FirstAllowedServiceQueryString - varchar(200)
--               ,''  -- SecondAllowedServiceQueryString - varchar(200)
--               ,'fameadvantage'  -- ConsumerKey - varchar(100)
--               ,'bWh7jRf4trVg0Z1TbnVoIyC4'  -- PrivateKey - varchar(100)
--               ,1800  -- OperationSecondTimeInterval - int (30 minutos)
--               ,5  -- PollSecondForOnDemandOperation - int
--               ,0  -- FlagOnDemandOperation - bit
--               ,0  -- FlagRefreshConfiguration - bit
--               ,0  -- IsActive - bit
--               ,SYSDATETIME()  -- DateMod - datetime2(7)
--               ,SYSDATETIME()  -- DateLastExecution - datetime2(7)
--               ,''  -- UserMod - varchar(50)
--               )
--    END;


-- ===============================================================================================
-- JAGG END with KLASS APP
-- ===============================================================================================
-- ===============================================================================================
-- DE13483: Qa: Duplicate reports at the reports page
-- ===============================================================================================


--  Check if '200 Less than 4 Yr Inst Detail & Summary' menu option is active and repeated
IF (
     SELECT COUNT(SMI.MenuItemId)
     FROM   syMenuItems AS SMI
     WHERE  SMI.MenuName = '200 Less than 4 Yr Inst Detail & Summary'
            AND SMI.IsActive = 1
   ) > 1  -- means menu option is repeated
    BEGIN
		-- Check if one of the active and  repeated menu option is under "Spring Report" parent
        IF (
             SELECT COUNT(SMI.MenuItemId)
             FROM   syMenuItems AS SMI
             WHERE  SMI.MenuName = '200 Less than 4 Yr Inst Detail & Summary'
                    AND SMI.IsActive = 1
                    AND SMI.ParentId = (
                                         SELECT SMI2.MenuItemId
                                         FROM   syMenuItems AS SMI2
                                         WHERE  SMI2.MenuName = 'Spring Reports'
                                       )
           ) > 0 -- means one of repeated menu option is active and is under Spring Report 
            BEGIN
                UPDATE  SMI
                SET     SMI.IsActive = 0
                       ,SMI.ParentId = NULL
                       ,SMI.ModUser = 'support'
                       ,SMI.ModDate = GETDATE()
                FROM    syMenuItems AS SMI
                WHERE   SMI.MenuName = '200 Less than 4 Yr Inst Detail & Summary'
                        AND SMI.IsActive = 1
                        AND SMI.ParentId = (
                                             SELECT SMI2.MenuItemId
                                             FROM   syMenuItems AS SMI2
                                             WHERE  SMI2.MenuName = 'Spring Reports'
                                           );
            END;
    END;
GO
-- Check if "Spring Report" active have active menu items under
IF (
     SELECT COUNT(SMI.MenuItemId)
     FROM   syMenuItems AS SMI
     WHERE  SMI.IsActive = 1
            AND SMI.ParentId = (
                                 SELECT SMI2.MenuItemId
                                 FROM   syMenuItems AS SMI2
                                 WHERE  SMI2.MenuName = 'Spring Reports'
                               )
   ) = 0  -- means menu option is empty
    BEGIN
        UPDATE  SMI
        SET     SMI.IsActive = 0
               ,SMI.ModUser = 'support'
               ,SMI.ModDate = GETDATE()
               ,SMI.ParentId = NULL
        FROM    syMenuItems AS SMI
        WHERE   SMI.MenuName = 'Spring Reports'
                AND SMI.IsActive = 1;
    END;
GO



-- ===============================================================================================
-- END  --  DE13483: Qa: Duplicate reports at the reports page
-- ===============================================================================================
-- ===============================================================================================
-- START  --  US10360: Remove "Missing Items" link
-- ===============================================================================================
IF EXISTS ( SELECT  *
            FROM    syMenuItems
            WHERE   ResourceId = 369
                    AND IsActive = 1 )
    BEGIN
        UPDATE  syMenuItems
        SET     IsActive = 0
               ,ParentId = NULL
               ,ModUser = 'support'
               ,ModDate = GETDATE()
        WHERE   ResourceId = 369;
        IF EXISTS ( SELECT  *
                    FROM    syNavigationNodes
                    WHERE   ResourceId = 369 )
            BEGIN
                DELETE  FROM syNavigationNodes
                WHERE   ResourceId = 369;
            END;	
    END;	
GO	
-- ===============================================================================================
-- END  --  US10360: Remove "Missing Items" link
-- ===============================================================================================
-- ===============================================================================================
-- START  --  US10359: Relocate and Rename Missing Items Report
-- ===============================================================================================
DECLARE @parentId UNIQUEIDENTIFIER
   ,@menuItemId INT;
SET @parentId = (
                  SELECT    ParentId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = 202
                );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = 493
                        AND ParentId = @parentId )
    BEGIN
        UPDATE  syNavigationNodes
        SET     ModDate = GETDATE()
               ,ModUser = 'support'
               ,ParentId = @parentId
        WHERE   ResourceId = 493;
    END;
SET @menuItemId = (
                    SELECT  MenuItemId
                    FROM    syMenuItems
                    WHERE   MenuName = 'General Reports'
                            AND HierarchyId = @parentId
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syMenuItems
                WHERE   ResourceId = 493
                        AND MenuName = 'Overridden Enrollment Requirements'
                        AND ParentId = @menuItemId )
    BEGIN
        UPDATE  syMenuItems
        SET     DisplayName = 'Overridden Enrollment Requirements'
               ,MenuName = 'Overridden Enrollment Requirements'
               ,ModDate = GETDATE()
               ,ModUser = 'support'
               ,ParentId = @menuItemId
               ,DisplayOrder = 400
        WHERE   ResourceId = 493;
    END;
	GO	
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 493
                        AND Resource = 'Overridden Enrollment Requirements' )
    BEGIN
        UPDATE  syResources
        SET     Resource = 'Overridden Enrollment Requirements'
               ,ModDate = GETDATE()
               ,ModUser = 'support'
        WHERE   ResourceID = 493;    
    END;	
GO	
IF EXISTS ( SELECT  *
            FROM    syRptParams
            WHERE   ResourceId = 493
                    AND Required = 1
                    AND RptCaption IN ( 'Student Group','Lead Group' ) )
    BEGIN
        UPDATE  syRptParams
        SET     Required = 0
        WHERE   ResourceId = 493
                AND RptCaption IN ( 'Student Group','Lead Group' );
    END;	
GO
IF EXISTS ( SELECT  *
            FROM    syRptParams
            WHERE   ResourceId = 493
                    AND RptCaption = 'Student Group' )
    BEGIN
        UPDATE  syRptParams
        SET     RptCaption = 'Lead Group'
        WHERE   ResourceId = 493
                AND RptCaption = 'Student Group';
    END;	
GO

IF EXISTS ( SELECT  *
            FROM    syRptParams
            WHERE   ResourceId = 493
                    AND RptCaption IN ( 'Required For','Shift','Enrollment Status' ) )
    BEGIN
        DELETE  FROM syRptParams
        WHERE   ResourceId = 493
                AND RptCaption IN ( 'Required For','Shift','Enrollment Status' );
    END;	
GO
-- ===============================================================================================
-- END  --  US10359: Relocate and Rename Missing Items Report
-- ===============================================================================================
-- ===============================================================================================
-- START  --  DE13471: Beta - Support login does not have access to change lead status.
-- ===============================================================================================
DECLARE @userId UNIQUEIDENTIFIER;
DECLARE @admDirRole UNIQUEIDENTIFIER;
DECLARE @campusId UNIQUEIDENTIFIER;
SET @userId = (
                SELECT  UserId
                FROM    syUsers
                WHERE   FullName = 'support'
              );
SET @admDirRole = (
                    SELECT TOP 1
                            RoleId
                    FROM    syRoles
                    WHERE   SysRoleId = 8
                    ORDER BY ModDate
                  );
SET @campusId = (
                  SELECT    CampGrpId
                  FROM      syCampGrps
                  WHERE     CampGrpCode = 'All'
                );
IF NOT EXISTS ( SELECT  *
                FROM    syUsersRolesCampGrps
                WHERE   UserId = @userId
                        AND RoleId = @admDirRole )
    BEGIN
        INSERT  INTO syUsersRolesCampGrps
                (
                 UserRoleCampGrpId
                ,UserId
                ,RoleId
                ,CampGrpId
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()  -- UserRoleCampGrpId - uniqueidentifier
                ,@userId  -- UserId - uniqueidentifier
                ,@admDirRole  -- RoleId - uniqueidentifier
                ,@campusId  -- CampGrpId - uniqueidentifier
                ,GETDATE()  -- ModDate - datetime
                ,'sa'  -- ModUser - varchar(50)
                );
		
    END;	
	
DECLARE @admRepRole UNIQUEIDENTIFIER;	
SET @admRepRole = (
                    SELECT TOP 1
                            RoleId
                    FROM    syLeadStatusChangePermissions
                    WHERE   RoleId IN ( SELECT  RoleId
                                        FROM    syRoles
                                        WHERE   SysRoleId = 3 )
                            AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    ORDER BY ModDate
                  );

IF NOT EXISTS ( SELECT  *
                FROM    syLeadStatusChangePermissions
                WHERE   RoleId = @admDirRole )
    BEGIN
        DECLARE @mDate DATETIME; 
        SET @mDate = GETDATE();
        DECLARE admRep_Cursor CURSOR
        FOR
            SELECT  LeadStatusChangeId
            FROM    syLeadStatusChangePermissions
            WHERE   RoleId = @admRepRole
                    AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965';
        DECLARE @LeadStatusChangeId UNIQUEIDENTIFIER;
        OPEN admRep_Cursor;
        FETCH NEXT FROM admRep_Cursor INTO @LeadStatusChangeId;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                INSERT  INTO syLeadStatusChangePermissions
                        (
                         LeadStatusChangePermissionID
                        ,LeadStatusChangeId
                        ,RoleId
                        ,StatusId
                        ,ModDate
                        ,ModUser
			            )
                VALUES  (
                         NEWID()  -- LeadStatusChangePermissionID - uniqueidentifier
                        ,@LeadStatusChangeId  -- LeadStatusChangeId - uniqueidentifier
                        ,@admDirRole  -- RoleId - uniqueidentifier
                        ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'  -- StatusId - uniqueidentifier
                        ,@mDate  -- ModDate - datetime
                        ,'sa'  -- ModUser - varchar(50)
			            );
                FETCH NEXT FROM admRep_Cursor INTO @LeadStatusChangeId;
            END;
        CLOSE admRep_Cursor;
        DEALLOCATE admRep_Cursor;	 
    END;
    GO

-- ===============================================================================================
-- END  --  DE13471: Beta - Support login does not have access to change lead status.
-- ===============================================================================================
-- ===============================================================================================
-- START  --  US10513: Report filters:  Weekly Attendance Report by Class
-- ===============================================================================================
IF EXISTS ( SELECT  *
            FROM    syMenuItems
            WHERE   MenuName = 'Weekly Attendance - Multiple'
                    AND Url = '/SY/ReportParams1.aspx' )
    BEGIN
        UPDATE  syMenuItems
        SET     Url = '/SY/ParamReport.aspx'
        WHERE   MenuName = 'Weekly Attendance - Multiple';
    END;
DECLARE @resId INT;
SET @resId = (
               SELECT   ResourceID
               FROM     syResources
               WHERE    Resource = 'Weekly Attendance - Multiple'
             );
IF EXISTS ( SELECT  ResourceId
            FROM    syRptParams
            WHERE   ResourceId = @resId )
    BEGIN
        DELETE  FROM syRptParams
        WHERE   ResourceId = @resId;
    END;	
IF EXISTS ( SELECT  *
            FROM    syResources
            WHERE   ResourceID = @resId
                    AND ResourceURL = '~/SY/ReportParams1.aspx' )
    BEGIN	
        UPDATE  syResources
        SET     ResourceURL = '~/SY/ParamReport.aspx'
        WHERE   ResourceID = @resId;
    END;	
IF NOT EXISTS ( SELECT  *
                FROM    syReports
                WHERE   ResourceId = @resId )
    BEGIN	
        INSERT  INTO syReports
                (
                 ReportId
                ,ResourceId
                ,ReportName
                ,ReportDescription
                ,RDLName
                ,AssemblyFilePath
                ,ReportClass
                ,CreationMethod
                ,WebServiceUrl
                ,RecordLimit
                ,AllowedExportTypes
                ,ReportTabLayout
                ,ShowPerformanceMsg
                ,ShowFilterMode
                )
        VALUES  (
                 NEWID()  -- ReportId - uniqueidentifier
                ,@resId  -- ResourceId - int
                ,'Weekly Attendance - Multiple'  -- ReportName - varchar(50)
                ,'This report gives the attendance of active students by class,instructor for a given week'  -- ReportDescription - varchar(500)
                ,'Weekly Attendance/WeeklyAttendanceReport'  -- RDLName - varchar(200)
                ,'~/Bin/Reporting.dll'  -- AssemblyFilePath - varchar(200)
                ,'FAME.Advantage.Reporting.Logic.WeeklyAttendanceMultiple'  -- ReportClass - varchar(200)
                ,'BuildReport'  -- CreationMethod - varchar(200)
                ,'BuildReport'  -- WebServiceUrl - varchar(200)
                ,400  -- RecordLimit - bigint
                ,0  -- AllowedExportTypes - int
                ,1  -- ReportTabLayout - int
                ,0  -- ShowPerformanceMsg - bit
                ,0  -- ShowFilterMode - bit
                );
    END;	
IF NOT EXISTS ( SELECT  *
                FROM    ParamSet
                WHERE   SetName = 'WeeklyAttendanceMultipleFilterSet' )
    BEGIN
        INSERT  INTO ParamSet
                (
                 SetName
                ,SetDisplayName
                ,SetDescription
                ,SetType
	            )
        VALUES  (
                 N'WeeklyAttendanceMultipleFilterSet'  -- SetName - nvarchar(50)
                ,N'Weekly Attendance - Multiple Report Filters'  -- SetDisplayName - nvarchar(50)
                ,N'Custom settings for Weekly Attendance Multiple Report'  -- SetDescription - nvarchar(1000)
                ,N'Custom'  -- SetType - nvarchar(50)
	            );
    END;
DECLARE @setId INT;
SET @setId = (
               SELECT   SetId
               FROM     ParamSet
               WHERE    SetName = 'WeeklyAttendanceMultipleFilterSet'
             );
DECLARE @reportId UNIQUEIDENTIFIER;
SET @reportId = (
                  SELECT    ReportId
                  FROM      syReports
                  WHERE     ResourceId = @resId
                );
IF NOT EXISTS ( SELECT  *
                FROM    syReportTabs
                WHERE   SetId = @setId )
    BEGIN	
        INSERT  INTO syReportTabs
                (
                 ReportId
                ,PageViewId
                ,UserControlName
                ,SetId
                ,ControllerClass
                ,DisplayName
                ,ParameterSetLookUp
                ,TabName
	            )
        VALUES  (
                 @reportId -- ReportId - uniqueidentifier
                ,N'RadRptPage_Options'  -- PageViewId - nvarchar(200)
                ,N'ParamPanelBarSetCustomOptions'  -- UserControlName - nvarchar(200)
                ,@setId  -- SetId - bigint
                ,N'ParamSetPanelBarControl.ascx'  -- ControllerClass - nvarchar(200)
                ,N'Custom Options Set for Weekly Attendance Multiple Report'  -- DisplayName - nvarchar(200)
                ,N'WeeklyAttendanceMultipleFilterSet'  -- ParameterSetLookUp - nvarchar(200)
                ,N'Options'  -- TabName - nvarchar(200)
	            );
    END;	
IF NOT EXISTS ( SELECT  *
                FROM    ParamSection
                WHERE   SetId = @setId )
    BEGIN
        INSERT  INTO ParamSection
                (
                 SectionName
                ,SectionCaption
                ,SetId
                ,SectionSeq
                ,SectionDescription
                ,SectionType
		        )
        VALUES  (
                 N'Parameters for Weekly Attendance Multiple Report'  -- SectionName - nvarchar(50)
                ,N'Weekly Attendance - Multiple Parameters'  -- SectionCaption - nvarchar(100)
                ,@setId  -- SetId - bigint
                ,1  -- SectionSeq - int
                ,N'Choose the Weekly Attendance - Multiple Report Parameters'  -- SectionDescription - nvarchar(500)
                ,0  -- SectionType - int
		        );            
    END;
IF NOT EXISTS ( SELECT  *
                FROM    ParamItem
                WHERE   Caption = 'WeeklyAttendanceMultipleOptions' )
    BEGIN
        INSERT  INTO ParamItem
                (
                 Caption
                ,ControllerClass
                ,valueprop
                ,ReturnValueName
                ,IsItemOverriden
	            )
        VALUES  (
                 N'WeeklyAttendanceMultipleOptions'  -- Caption - nvarchar(50)
                ,N'WeeklyAttendanceMultiple.ascx'  -- ControllerClass - nvarchar(50)
                ,0  -- valueprop - int
                ,N'WeeklyAttendanceMultipleOptions'  -- ReturnValueName - nvarchar(50)
                ,0  -- IsItemOverriden - bit
	            );
    END;

DECLARE @sectionId INT
   ,@itemId INT;
SET @sectionId = (
                   SELECT   SectionId
                   FROM     ParamSection
                   WHERE    SetId = @setId
                 );  
SET @itemId = (
                SELECT  ItemId
                FROM    ParamItem
                WHERE   Caption = 'WeeklyAttendanceMultipleOptions'
              );  
IF NOT EXISTS ( SELECT  *
                FROM    ParamDetail
                WHERE   SectionId = @sectionId
                        AND ItemId = @itemId )
    BEGIN
        INSERT  INTO ParamDetail
                (
                 SectionId
                ,ItemId
                ,CaptionOverride
                ,ItemSeq
	            )
        VALUES  (
                 @sectionId  -- SectionId - bigint
                ,@itemId  -- ItemId - bigint
                ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                ,1  -- ItemSeq - int
	            );
    END;
   GO       
-- ===============================================================================================
-- END  --  US10513: Report filters:  Weekly Attendance Report by Class
-- ===============================================================================================
-- ===============================================================================================
-- START  --  DE13684: QA: Academics / Enter Class Sections With Periods page incorrect labeling
-- ===============================================================================================
BEGIN TRANSACTION UpdateCaption;
BEGIN TRY


    IF EXISTS ( SELECT  *
                FROM    dbo.syFldCaptions
                WHERE   Caption = 'Lead Assigned to'
                        AND FldId = (
                                      SELECT    FldId
                                      FROM      syFields
                                      WHERE     FldName = 'CampusId'
                                    ) )
        BEGIN
            UPDATE  dbo.syFldCaptions
            SET     Caption = 'Campus'
            WHERE   FldId = (
                              SELECT    FldId
                              FROM      syFields
                              WHERE     FldName = 'CampusId'
                            );
        END;

    DECLARE @DDLId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    syDDLS
                    WHERE   DDLName = 'LeadAssignedTo' )
        BEGIN
            SET @DDLId = (
                           SELECT   MAX(DDLId) + 1
                           FROM     syDDLS
                         );
            INSERT  INTO syDDLS
                    SELECT  @DDLId
                           ,'LeadAssignedTo'
                           ,TblId
                           ,DispFldId
                           ,ValFldId
                           ,ResourceId
                           ,CulDependent
                    FROM    syDDLS
                    WHERE   DDLName = 'Campuses';
        END;

    SET @DDLId = (
                   SELECT   DDLId
                   FROM     syDDLS
                   WHERE    DDLName = 'LeadAssignedTo'
                 );

    DECLARE @FldId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    syFields
                    WHERE   FldName = 'LeadAssignedToId' )
        BEGIN
            SET @FldId = (
                           SELECT   MAX(FldId) + 1
                           FROM     syFields
                         );
            INSERT  INTO syFields
                    SELECT  @FldId
                           ,'LeadAssignedToId'
                           ,FldTypeId
                           ,FldLen
                           ,@DDLId
                           ,DerivedFld
                           ,SchlReq
                           ,LogChanges
                           ,Mask
                    FROM    syFields
                    WHERE   FldName = 'CampusId';	
        END;

    SET @FldId = (
                   SELECT   FldId
                   FROM     syFields
                   WHERE    FldName = 'LeadAssignedToId'
                 );

    DECLARE @FldCapId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syFldCaptions
                    WHERE   Caption = 'Lead Assigned to'
                            AND FldId = @FldId )
        BEGIN
            SET @FldCapId = (
                              SELECT    MAX(FldCapId) + 1
                              FROM      syFldCaptions
                            );
            INSERT  INTO syFldCaptions
                    SELECT  @FldCapId
                           ,@FldId
                           ,LangId
                           ,'Lead Assigned To'
                           ,'Lead Assigned To'
                    FROM    syFldCaptions
                    WHERE   Caption = 'Campus'
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'CampusId'
                                        );
        END;

    UPDATE  adQuickLeadMap
    SET     fldName = 'LeadAssignedToId'
           ,propName = 'LeadAssignedToId'
    WHERE   fldName = 'CampusId'
            AND ctrlIdName = 'cbLeadAssign';


    UPDATE  adQuickLeadMap
    SET     parentCtrlId = 'LeadAssignedToId'
    WHERE   parentCtrlId = 'CampusId'; 

    UPDATE  adExpQuickLeadSections
    SET     FldId = (
                      SELECT    FldId
                      FROM      syFields
                      WHERE     FldName = 'LeadAssignedToId'
                    )
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      syFields
                      WHERE     FldName = 'CampusId'
                    );

    IF NOT EXISTS ( SELECT  *
                    FROM    syTblFlds
                    WHERE   FldId = @FldId )
        BEGIN
            DECLARE @TblFldsId INT;

            SET @TblFldsId = (
                               SELECT   MAX(TblFldsId) + 1
                               FROM     syTblFlds
                             );

            INSERT  INTO syTblFlds
                    SELECT  @TblFldsId
                           ,TblId
                           ,@FldId
                           ,CategoryId
                           ,FKColDescrip
                    FROM    syTblFlds
                    WHERE   FldId = (
                                      SELECT    FldId
                                      FROM      syFields
                                      WHERE     FldName = 'CampusId'
                                    )
                            AND TblId = (
                                          SELECT    TblId
                                          FROM      syTables
                                          WHERE     TblName = 'syCampuses'
                                        );
	
            DECLARE @ResourceId INT
               ,@Required BIT
               ,@SchlReq BIT
               ,@ControlName VARCHAR(50)
               ,@UsePageSetup BIT;

            DECLARE ResourceTableFields CURSOR
            FOR
                SELECT 
		--,
                        ResourceId
                       ,Required
                       ,SchlReq
                       ,CASE WHEN ControlName IS NULL THEN NULL
                             ELSE 'ddlLeadAssignedTo'
                        END AS ControlName
                       ,UsePageSetup
                FROM    syResTblFlds
                WHERE   TblFldsId IN ( SELECT   TblFldsId
                                       FROM     syTblFlds
                                       WHERE    FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'CampusId'
                                                        )
                                                AND TblId = (
                                                              SELECT    TblId
                                                              FROM      syTables
                                                              WHERE     TblName = 'syCampuses'
                                                            ) );
	
            OPEN ResourceTableFields;

            FETCH NEXT FROM ResourceTableFields   
	INTO @ResourceId,@Required,@SchlReq,@ControlName,@UsePageSetup;
            DECLARE @ResDefId INT;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @ResDefId = (
                                      SELECT    MAX(ResDefId) + 1
                                      FROM      syResTblFlds
                                    );
                    INSERT  INTO syResTblFlds
                            SELECT  @ResDefId
                                   ,@ResourceId
                                   ,@TblFldsId
                                   ,@Required
                                   ,@SchlReq
                                   ,@ControlName
                                   ,@UsePageSetup;
			
                    FETCH NEXT FROM ResourceTableFields   
		INTO @ResourceId,@Required,@SchlReq,@ControlName,@UsePageSetup;
                END;
            CLOSE ResourceTableFields;  
            DEALLOCATE ResourceTableFields;  
        END;

    UPDATE  adLeadFields
    SET     LeadID = @FldId
    WHERE   LeadID = (
                       SELECT   FldId
                       FROM     syFields
                       WHERE    FldName = 'CampusId'
                     );
    DELETE  FROM syResTblFlds
    WHERE   ResDefId IN ( SELECT    ResDefId
                          FROM      syResTblFlds resourceTableFields
                          INNER JOIN syTblFlds tableFields ON resourceTableFields.TblFldsId = tableFields.TblFldsId
                          INNER JOIN syFields fields ON fields.FldId = tableFields.FldId
                          WHERE     resourceTableFields.ResourceId = 1
                                    AND FldName = 'LeadAssignedToId' );
END TRY
BEGIN CATCH
    SELECT  ERROR_NUMBER() AS ErrorNumber
           ,ERROR_SEVERITY() AS ErrorSeverity
           ,ERROR_STATE() AS ErrorState
           ,ERROR_PROCEDURE() AS ErrorProcedure
           ,ERROR_LINE() AS ErrorLine
           ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION UpdateCaption;
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateCaption;
    END;

GO
-- ===============================================================================================
-- END  --  DE13684: QA: Academics / Enter Class Sections With Periods page incorrect labeling
-- ===============================================================================================

PRINT N'If not exist create schema RedGateLocal';
GO
IF SCHEMA_ID('RedGateLocal') IS NULL
    EXEC('create schema RedGateLocal');
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exist create table [RedGateLocal].[DeploymentMetadatal]';
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[RedGateLocal].[DeploymentMetadata]')
                        AND type IN ( N'U',N'PC' ) )
    BEGIN
        CREATE TABLE RedGateLocal.DeploymentMetadata
            (
             Id INT IDENTITY(1,1)
                    NOT NULL
            ,Name NVARCHAR(MAX) NOT NULL
            ,Type VARCHAR(50) NOT NULL
            ,Action VARCHAR(50) NOT NULL
            ,[By] NVARCHAR(128) NOT NULL
                                CONSTRAINT DF__DeploymentMe__By DEFAULT ( ORIGINAL_LOGIN() )
            ,[As] NVARCHAR(128) NOT NULL
                                CONSTRAINT DF__DeploymentMe__As DEFAULT ( SUSER_SNAME() )
            ,CompletedDate DATETIME NOT NULL
                                    CONSTRAINT DF__Deploymen__Compl DEFAULT ( GETDATE() )
            ,[With] NVARCHAR(128) NOT NULL
                                  CONSTRAINT DF__Deployment__With DEFAULT ( APP_NAME() )
            ,BlockId VARCHAR(50) NOT NULL
            ,InsertedSerial BINARY(8) NOT NULL
                                      CONSTRAINT DF__Deploymen__Inser DEFAULT ( @@dbts + ( 1 ) )
            ,UpdatedSerial TIMESTAMP NOT NULL
            ,MetadataVersion VARCHAR(50) NOT NULL
            ,Hash NVARCHAR(MAX) NULL
            ,PRIMARY KEY CLUSTERED ( Id ASC )
                WITH ( PAD_INDEX = OFF,STATISTICS_NORECOMPUTE = OFF,IGNORE_DUP_KEY = OFF,ALLOW_ROW_LOCKS = ON,ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY] TEXTIMAGE_ON [PRIMARY];
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO


-- ===============================================================================================
-- START  --  UPDATE RedGateLocal.DeploymentMetadata table with script already run on DB
-- ===============================================================================================
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 1 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 1
                ,N'Changes before migration script ''Script created at 2016-09-06 10:54.'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T21:01:21.183' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-09-06-110649 b8 auto'
                ,0x000000000007B3EA
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 2 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 2
                ,N'Migration script ''Script created at 2016-09-06 10:54.'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T21:01:21.247' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-09-06-110649 b9 user'
                ,0x000000000007B3EB
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 3 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 3
                ,N'Changes before migration script ''1.Leads_migratrion_script.sql -- Script created at 2016-12-05 14:41.'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T21:01:21.247' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-05-144434 n0 auto'
                ,0x000000000007B3EC
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 4 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 4
                ,N'Migration script ''1.Leads_migratrion_script.sql -- Script created at 2016-12-05 14:41.'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:07:39.780' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-05-144434 n1 user'
                ,0x000000000007B3ED
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 5 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 5
                ,N'Changes before migration script ''2.Students_data_migration_script.sql  --  Script created at 2016-12-05 17:43.'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:07:39.813' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-05-174507 jg auto'
                ,0x000000000007B3EE
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 6 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 6
                ,N'Migration script ''2.Students_data_migration_script.sql  --  Script created at 2016-12-05 17:43.'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:34:55.633' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-05-174507 jh user'
                ,0x000000000007B3EF
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 7 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 7
                ,N'Changes before migration script ''US9717: Migration Student Prior Education to Lead Education Table. 3.8 -- 12/06/2016'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:34:55.650' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-06-160407 vf auto'
                ,0x000000000007B3F0
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 8 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 8
                ,N'Migration script ''US9717: Migration Student Prior Education to Lead Education Table. 3.8 -- 12/06/2016'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:34:56.417' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-06-160407 vg user'
                ,0x000000000007B3F1
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 9 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 9
                ,N'Changes before migration script ''DE13102: Updating adLeadGrpReqGroups Status to active when is null'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:34:56.417' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-08-144120 nq auto'
                ,0x000000000007B3F2
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 10 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 10
                ,N'Migration script ''DE13102: Updating adLeadGrpReqGroups Status to active when is null'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\jguirado'
                ,N'INTERNAL\jguirado'
                ,CAST(N'2017-01-16T22:34:56.823' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2016-12-08-144120 nr user'
                ,0x000000000007B3F3
                ,N'5.60.0.72'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 11 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 11
                ,N'Changes before migration script ''Covers changes to: syInstitutions. Script created at 2017-01-26 18:29.'''
                ,N'Compare'
                ,N'Deployed'
                ,N'INTERNAL\TFSBuild'
                ,N'INTERNAL\TFSBuild'
                ,CAST(N'2017-01-27T11:39:10.373' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2017-01-26-191000 r4 auto'
                ,0x000000000007B3F4
                ,N'5.60.0.67'
                ,NULL
                );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata ON; 
IF NOT EXISTS ( SELECT  *
                FROM    RedGateLocal.DeploymentMetadata AS DM
                WHERE   DM.Id = 12 )
    BEGIN
        INSERT  RedGateLocal.DeploymentMetadata
                (
                 Id
                ,Name
                ,Type
                ,Action
                ,[By]
                ,[As]
                ,CompletedDate
                ,[With]
                ,BlockId
                ,InsertedSerial
                ,MetadataVersion
                ,Hash
                )
        VALUES  (
                 12
                ,N'Migration script ''Covers changes to: syInstitutions. Script created at 2017-01-26 18:29.'''
                ,N'Migration'
                ,N'Deployed'
                ,N'INTERNAL\TFSBuild'
                ,N'INTERNAL\TFSBuild'
                ,CAST(N'2017-01-27T11:39:10.483' AS DATETIME)
                ,N'Red Gate Software  - SQL Tools - Executor'
                ,N'2017-01-26-191000 r5 user'
                ,0x000000000007B3F5
                ,N'5.60.0.67'
                ,NULL
                );
    END;
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO

SET IDENTITY_INSERT RedGateLocal.DeploymentMetadata OFF;
GO
-- ===============================================================================================
-- END  -- UPDATE RedGateLocal.DeploymentMetadata table with script already run on DB
-- ===============================================================================================


-- ===============================================================================================
-- END  --   Consolidated Script Version 3.8SP1
-- ===============================================================================================
