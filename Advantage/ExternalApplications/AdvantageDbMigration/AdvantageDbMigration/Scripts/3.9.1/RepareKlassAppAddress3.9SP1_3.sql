﻿-- ----------------------------------------------------------------------------------------
-- Function: Change the address for Klass App to HTTPS protocol
-- 10/25/2017. Run database version 3.9.1.4
-- ----------------------------------------------------------------------------------------

--BEGIN TRANSACTION tito

DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigCustomFields';
UPDATE  syConfigAppSetValues
        SET     Value = 'https://api.klassapp.com/customfields/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
GO

DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigLocations';
UPDATE  syConfigAppSetValues
        SET     Value = 'https://api.klassapp.com/locations/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
GO
							
DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigKeyValuePairs';														
 UPDATE  syConfigAppSetValues
        SET     Value = 'https://api.klassapp.com/keyvaluepairs/1.0'
               ,ModDate = GETDATE()
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
GO

DECLARE @KeyName VARCHAR(50) = 'KlassApp_UrlConfigMajors';
 UPDATE  syConfigAppSetValues
        SET     Value = 'https://api.klassapp.com/majors/1.1'
        WHERE   SettingId = (
                              SELECT    SettingId
                              FROM      dbo.syConfigAppSettings
                              WHERE     KeyName = @KeyName
                            );
GO

-- SELECT * FROM [dbo].[syConfigAppSetValues] val
-- JOIN [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
--  WHERE KeyName IN ('KlassApp_UrlConfigCustomFields', 'KlassApp_UrlConfigLocations',  'KlassApp_UrlConfigKeyValuePairs', 'KlassApp_UrlConfigMajors')

--ROLLBACK TRANSACTION tito

