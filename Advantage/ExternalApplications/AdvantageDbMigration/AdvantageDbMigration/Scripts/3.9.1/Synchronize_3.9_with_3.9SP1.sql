-- =========================================================================================================
-- Synchronize_3.9_with_3.9SP1.sql
-- =========================================================================================================
/*                            Ver 001      2017-09-14
Run this script on:

 
to synchronize it with:

        3.9  with  3.9SP1 (TFS)

You are recommended to back up your database before running this script


*/
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'If Exists sp USP_GenerateStudentSequence, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_GenerateStudentSequence]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_GenerateStudentSequence'; 
        DROP PROCEDURE USP_GenerateStudentSequence; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_GenerateStudentSequence'; 
GO 
--=================================================================================================
-- USP_GenerateStudentSequence
--=================================================================================================
CREATE PROCEDURE dbo.USP_GenerateStudentSequence
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @stuId INT;
        SET @stuId = (
                     SELECT ISNULL(MAX(Student_SeqID), 0) AS Student_SeqID
                     FROM   syGenerateStudentID
                     ) + 1;
        INSERT INTO syGenerateStudentID (
                                        Student_SeqID
                                       ,ModDate
                                        )
        VALUES ( @stuId, GETDATE());
        SELECT @stuId AS SeqId;
    END;
--=================================================================================================
-- END  --  USP_GenerateStudentSequence
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

COMMIT TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT;
SET @Success = 1;
SET NOEXEC OFF;
IF ( @Success = 1 )
    PRINT 'The database update succeeded';
ELSE
    BEGIN
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
        PRINT 'The database update failed';
    END;
GO
-- =========================================================================================================
-- END  --  Synchronize_3.9_with_3.9SP1.sql
-- =========================================================================================================
