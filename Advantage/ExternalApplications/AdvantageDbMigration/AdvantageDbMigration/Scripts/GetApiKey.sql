﻿-- ===============================================
-- Get the API key for a specific tenant
-- ===============================================
SELECT api.[Key]
FROM   dbo.ApiAuthenticationKey api
JOIN   Tenant t ON t.TenantId = api.TenantId
WHERE  t.TenantName = @TenantName;
