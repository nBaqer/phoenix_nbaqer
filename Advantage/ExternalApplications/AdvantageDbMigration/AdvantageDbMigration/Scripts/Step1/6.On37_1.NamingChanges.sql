-- =================================================================================================
-- script to add PK if it do not existes 
-- =================================================================================================

IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[adQuickLeadMap]')
                    AND type IN ( N'U',N'PC' ) )
    BEGIN
        IF NOT EXISTS ( SELECT  1
                        FROM    sys.objects AS O
                        WHERE   object_id = OBJECT_ID(N'[dbo].[PK_adQuickLeadMap_mapId]')
                                AND type IN ( N'PK' ) )
            BEGIN
                ALTER TABLE dbo.adQuickLeadMap ADD  CONSTRAINT PK_adQuickLeadMap_mapId PRIMARY KEY CLUSTERED 
                (
                mapId ASC
                )  ON [PRIMARY];	    
            END;
    END;
GO
-- =================================================================================================
-- END  --  script to add PK if it do not existes 
-- =================================================================================================



-- =================================================================================================
-- script to Update all FK that do not complain with the A1 Standard naming to meet the standard
-- =================================================================================================
PRINT 'Renaming foreing key FK_saPmtPeriodBatchItems_saPmtPeriodBatchIHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId, if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.foreign_keys AS FK
            WHERE   FK.object_id = OBJECT_ID(N'FK_saPmtPeriodBatchItems_saPmtPeriodBatchIHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId')
                    AND parent_object_id = OBJECT_ID(N'saPmtPeriodBatchItems') )
    BEGIN 
        EXECUTE sp_rename 'FK_saPmtPeriodBatchItems_saPmtPeriodBatchIHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId',
            'FK_saPmtPeriodBatchItems_saPmtPeriodBatchHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId','OBJECT';
    END; 
GO  

PRINT 'Renaming foreing key FK_syStatusChangesDeleted_syStatusChangesDeleted_DeleteReasonsId_Id, if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.foreign_keys AS FK
            WHERE   FK.object_id = OBJECT_ID(N'FK_syStatusChangesDeleted_syStatusChangesDeleted_DeleteReasonsId_Id')
                    AND parent_object_id = OBJECT_ID(N'syStatusChangesDeleted') )
    BEGIN 
        EXECUTE sp_rename 'FK_syStatusChangesDeleted_syStatusChangesDeleted_DeleteReasonsId_Id',
            'FK_syStatusChangesDeleted_syStatusChangeDeleteReasons_DeleteReasonsId_Id','OBJECT';
    END; 
GO  

PRINT 'Renaming foreing key FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdFrom_SysStatusId, if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.foreign_keys AS FK
            WHERE   FK.object_id = OBJECT_ID(N'FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdFrom_SysStatusId')
                    AND parent_object_id = OBJECT_ID(N'sySystemStatusWorkflowRules') )
    BEGIN 
        EXECUTE sp_rename 'FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdFrom_SysStatusId',
            'FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdFrom_SysStatusId','OBJECT';
    END; 
GO  

PRINT 'Renaming foreing key FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdTo_SysStatusId, if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.foreign_keys AS FK
            WHERE   FK.object_id = OBJECT_ID(N'FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdTo_SysStatusId')
                    AND parent_object_id = OBJECT_ID(N'sySystemStatusWorkflowRules') )
    BEGIN 
        EXECUTE sp_rename 'FK_sySystemStatusWorkflowRules_syStudentStatusChanges_StatusIdTo_SysStatusId',
            'FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdTo_SysStatusId','OBJECT';
    END; 
GO  
-- =================================================================================================
-- END  --  script to Update all FK that do not complain with the A1 Standard naming to meet the standard
-- =================================================================================================




-- =================================================================================================
-- script to Update all index that do not complain with the A1 Standard naming to meet the standard
-- =================================================================================================
PRINT 'Renaming Index UIX_adAdvInterval_AdvIntervalDescrip_AdvIntervalCode if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_adAdvInterval_AdvIntervalDescrip_AdvIntervalCode'
                    AND I.object_id = OBJECT_ID(N'adAdvInterval') )
    BEGIN 
        EXECUTE sp_rename 'adAdvInterval.UIX_adAdvInterval_AdvIntervalDescrip_AdvIntervalCode','UIX_adAdvInterval_AdvIntervalCode_AdvIntervalDescrip';
    END; 
GO  

PRINT 'Renaming Index IX_adEntrTestOverRide_EntrTestId_OverRide_StudentId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_adEntrTestOverRide_EntrTestId_OverRide_StudentId'
                    AND I.object_id = OBJECT_ID(N'adEntrTestOverRide') )
    BEGIN 
        EXECUTE sp_rename 'adEntrTestOverRide.IX_adEntrTestOverRide_EntrTestId_OverRide_StudentId','IX_adEntrTestOverRide_EntrTestId_StudentId_OverRide';
    END; 
GO  

PRINT 'Renaming Index IX_adLeadByLeadGroups_LeadGrpId_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_adLeadByLeadGroups_LeadGrpId_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'adLeadByLeadGroups') )
    BEGIN 
        EXECUTE sp_rename 'adLeadByLeadGroups.IX_adLeadByLeadGroups_LeadGrpId_StuEnrollId','IX_adLeadByLeadGroups_StuEnrollId_LeadGrpId';
    END; 
GO  

PRINT 'Renaming Index UIX_adSourceCatagory_SourceCatagoryDescrip_SourceCatagoryCode if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_adSourceCatagory_SourceCatagoryDescrip_SourceCatagoryCode'
                    AND I.object_id = OBJECT_ID(N'adSourceCatagory') )
    BEGIN 
        EXECUTE sp_rename 'adSourceCatagory.UIX_adSourceCatagory_SourceCatagoryDescrip_SourceCatagoryCode',
            'UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip';
    END; 
GO  

PRINT 'Renaming Index UIX_adSourceType_SourceTypeDescrip_SourceTypeCode if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_adSourceType_SourceTypeDescrip_SourceTypeCode'
                    AND I.object_id = OBJECT_ID(N'adSourceType') )
    BEGIN 
        EXECUTE sp_rename 'adSourceType.UIX_adSourceType_SourceTypeDescrip_SourceTypeCode','UIX_adSourceType_SourceTypeCode_SourceTypeDescrip';
    END; 
GO  

PRINT 'Renaming Index IX_arClassSections_ClsSectionId_TermId_ReqId_InstrGrdBkWgtId_GrdScaleId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arClassSections_ClsSectionId_TermId_ReqId_InstrGrdBkWgtId_GrdScaleId'
                    AND I.object_id = OBJECT_ID(N'arClassSections') )
    BEGIN 
        EXECUTE sp_rename 'arClassSections.IX_arClassSections_ClsSectionId_TermId_ReqId_InstrGrdBkWgtId_GrdScaleId',
            'IX_arClassSections_ReqId_TermId_ClsSectionId_InstrGrdBkWgtId_GrdScaleId';
    END; 
GO  

PRINT 'Renaming Index IX_arClsSectMeetings_ClsSectionId_PeriodId_StartDate_EndDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arClsSectMeetings_ClsSectionId_PeriodId_StartDate_EndDate'
                    AND I.object_id = OBJECT_ID(N'arClsSectMeetings') )
    BEGIN 
        EXECUTE sp_rename 'arClsSectMeetings.IX_arClsSectMeetings_ClsSectionId_PeriodId_StartDate_EndDate',
            'IX_arClsSectMeetings_ClsSectionId_StartDate_EndDate_PeriodId';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkConversionResults_ConversionResultId_ReqId_GrdComponentTypeId_Score_StuEnrollId_MinResult_TermId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkConversionResults_ConversionResultId_ReqId_GrdComponentTypeId_Score_StuEnrollId_MinResult_TermId'
                    AND I.object_id = OBJECT_ID(N'arGrdBkConversionResults') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkConversionResults.IX_arGrdBkConversionResults_ConversionResultId_ReqId_GrdComponentTypeId_Score_StuEnrollId_MinResult_TermId',
            'IX_arGrdBkConversionResults_StuEnrollId_ReqId_TermId_GrdComponentTypeId_ConversionResultId_MinResult_Score';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_Score if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_Score'
                    AND I.object_id = OBJECT_ID(N'arGrdBkResults') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkResults.IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_Score',
            'IX_arGrdBkResults_Score_ClsSectionId_InstrGrdBkWgtDetailId';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkResults_ClsSectionId_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkResults_ClsSectionId_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arGrdBkResults') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkResults.IX_arGrdBkResults_ClsSectionId_StuEnrollId','IX_arGrdBkResults_StuEnrollId_ClsSectionId';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkResults_GrdBkResultId_ClsSectionId_InstrGrdBkWgtDetailId_Score_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkResults_GrdBkResultId_ClsSectionId_InstrGrdBkWgtDetailId_Score_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arGrdBkResults') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkResults.IX_arGrdBkResults_GrdBkResultId_ClsSectionId_InstrGrdBkWgtDetailId_Score_StuEnrollId',
            'IX_arGrdBkResults_ClsSectionId_GrdBkResultId_InstrGrdBkWgtDetailId_StuEnrollId_Score';
    END; 
GO  

PRINT 'Renaming Index UIX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_StuEnrollId_ResNum_PostDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_StuEnrollId_ResNum_PostDate'
                    AND I.object_id = OBJECT_ID(N'arGrdBkResults') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkResults.UIX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_StuEnrollId_ResNum_PostDate',
            'UIX_arGrdBkResults_StuEnrollId_InstrGrdBkWgtDetailId_ClsSectionId_ResNum_PostDate';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number_Required_MustPass if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number_Required_MustPass'
                    AND I.object_id = OBJECT_ID(N'arGrdBkWgtDetails') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkWgtDetails.IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number_Required_MustPass',
            'IX_arGrdBkWgtDetails_GrdComponentTypeId_Required_MustPass_InstrGrdBkWgtDetailId_Number';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtId_GrdComponentTypeId_Number if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtId_GrdComponentTypeId_Number'
                    AND I.object_id = OBJECT_ID(N'arGrdBkWgtDetails') )
    BEGIN 
        EXECUTE sp_rename 'arGrdBkWgtDetails.IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtId_GrdComponentTypeId_Number',
            'IX_arGrdBkWgtDetails_InstrGrdBkWgtId_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number';
    END; 
GO  

PRINT 'Renaming Index IX_arGrdComponentTypes_GrdComponentTypeId_Descrip_SysComponentTypeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arGrdComponentTypes_GrdComponentTypeId_Descrip_SysComponentTypeId'
                    AND I.object_id = OBJECT_ID(N'arGrdComponentTypes') )
    BEGIN 
        EXECUTE sp_rename 'arGrdComponentTypes.IX_arGrdComponentTypes_GrdComponentTypeId_Descrip_SysComponentTypeId',
            'IX_arGrdComponentTypes_SysComponentTypeId_GrdComponentTypeId_Descrip';
    END; 
GO  

PRINT 'Renaming Index IX_arReqs_ReqId_Code_Descrip_Credits_FinAidCredits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arReqs_ReqId_Code_Descrip_Credits_FinAidCredits'
                    AND I.object_id = OBJECT_ID(N'arReqs') )
    BEGIN 
        EXECUTE sp_rename 'arReqs.IX_arReqs_ReqId_Code_Descrip_Credits_FinAidCredits','IX_arReqs_ReqId_Descrip_Code_Credits_FinAidCredits';
    END; 
GO  

PRINT 'Renaming Index IX_arResults_ResultId_TestId_Score_GrdSysDetailId_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arResults_ResultId_TestId_Score_GrdSysDetailId_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arResults') )
    BEGIN 
        EXECUTE sp_rename 'arResults.IX_arResults_ResultId_TestId_Score_GrdSysDetailId_StuEnrollId',
            'IX_arResults_GrdSysDetailId_ResultId_StuEnrollId_TestId_Score';
    END; 
GO  

PRINT 'Renaming Index IX_arResults_TestId_Score_GrdSysDetailId_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arResults_TestId_Score_GrdSysDetailId_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arResults') )
    BEGIN 
        EXECUTE sp_rename 'arResults.IX_arResults_TestId_Score_GrdSysDetailId_StuEnrollId','IX_arResults_TestId_GrdSysDetailId_StuEnrollId_Score';
    END; 
GO  

PRINT 'Renaming Index IX_arResults_TestId_Score_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arResults_TestId_Score_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arResults') )
    BEGIN 
        EXECUTE sp_rename 'arResults.IX_arResults_TestId_Score_StuEnrollId','IX_arResults_StuEnrollId_TestId_Score';
    END; 
GO  

PRINT 'Renaming Index IX_arResults_TestId_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arResults_TestId_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'arResults') )
    BEGIN 
        EXECUTE sp_rename 'arResults.IX_arResults_TestId_StuEnrollId','IX_arResults_StuEnrollId_TestId';
    END; 
GO  

PRINT 'Renaming Index IX_arStudent_StudentId_FirstName_MiddleName_LastName if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStudent_StudentId_FirstName_MiddleName_LastName'
                    AND I.object_id = OBJECT_ID(N'arStudent') )
    BEGIN 
        EXECUTE sp_rename 'arStudent.IX_arStudent_StudentId_FirstName_MiddleName_LastName','IX_arStudent_StudentId_FirstName_LastName_MiddleName';
    END; 
GO  

PRINT 'Renaming Index IX_arStudent_StudentId_FirstName_MiddleName_LastName_SSN_DOB_Gender_Race_Citizen_StudentNumber_HousingId_admincriteriaid if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStudent_StudentId_FirstName_MiddleName_LastName_SSN_DOB_Gender_Race_Citizen_StudentNumber_HousingId_admincriteriaid'
                    AND I.object_id = OBJECT_ID(N'arStudent') )
    BEGIN 
        EXECUTE sp_rename 'arStudent.IX_arStudent_StudentId_FirstName_MiddleName_LastName_SSN_DOB_Gender_Race_Citizen_StudentNumber_HousingId_admincriteriaid',
            'IX_arStudent_StudentId_HousingId_admincriteriaid_Citizen_Race_Gender_DOB_FirstName_LastName_MiddleName_SSN_StudentNumber';
    END; 
GO  

PRINT 'Renaming Index IX_arStudentClockAttendance_StuEnrollId_RecordDate_isTardy if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStudentClockAttendance_StuEnrollId_RecordDate_isTardy'
                    AND I.object_id = OBJECT_ID(N'arStudentClockAttendance') )
    BEGIN 
        EXECUTE sp_rename 'arStudentClockAttendance.IX_arStudentClockAttendance_StuEnrollId_RecordDate_isTardy',
            'IX_arStudentClockAttendance_StuEnrollId_isTardy_RecordDate';
    END; 
GO  

PRINT 'Renaming Index IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours_isTardy if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours_isTardy'
                    AND I.object_id = OBJECT_ID(N'arStudentClockAttendance') )
    BEGIN 
        EXECUTE sp_rename 'arStudentClockAttendance.IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours_isTardy',
            'IX_arStudentClockAttendance_StuEnrollId_RecordDate_ActualHours_SchedHours_isTardy';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StudentId_PrgVerId_ShiftId_StatusCodeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StudentId_PrgVerId_ShiftId_StatusCodeId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StudentId_PrgVerId_ShiftId_StatusCodeId',
            'IX_arStuEnrollments_ShiftId_StatusCodeId_StudentId_PrgVerId';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId',
            'IX_arStuEnrollments_PrgVerId_ShiftId_StatusCodeId_StudentId_StartDate';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StudentId_ShiftId_StatusCodeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StudentId_ShiftId_StatusCodeId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StudentId_ShiftId_StatusCodeId','IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StuEnrollId_PrgVerId_CampusId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StuEnrollId_PrgVerId_CampusId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StuEnrollId_PrgVerId_CampusId','IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_CampusId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_CampusId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_CampusId',
            'IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId_StudentId';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ExpGradDate_CampusId_StatusCodeId_EdLvlId_degcertseekingid if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ExpGradDate_CampusId_StatusCodeId_EdLvlId_degcertseekingid'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ExpGradDate_CampusId_StatusCodeId_EdLvlId_degcertseekingid',
            'IX_arStuEnrollments_StatusCodeId_StartDate_CampusId_EdLvlId_degcertseekingid_StudentId_PrgVerId_StuEnrollId_ExpGradDate';
    END; 
GO  

PRINT 'Renaming Index IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId'
                    AND I.object_id = OBJECT_ID(N'arStuEnrollments') )
    BEGIN 
        EXECUTE sp_rename 'arStuEnrollments.IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId',
            'IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId_StuEnrollId_PrgVerId_StartDate';
    END; 
GO  

PRINT 'Renaming Index IX_arTerm_TermId_TermDescrip_StartDate_EndDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arTerm_TermId_TermDescrip_StartDate_EndDate'
                    AND I.object_id = OBJECT_ID(N'arTerm') )
    BEGIN 
        EXECUTE sp_rename 'arTerm.IX_arTerm_TermId_TermDescrip_StartDate_EndDate','IX_arTerm_TermId_StartDate_TermDescrip_EndDate';
    END; 
GO  

PRINT 'Renaming Index IX_arTransferGrades_TransferId_StuEnrollId_ReqId_TermId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_arTransferGrades_TransferId_StuEnrollId_ReqId_TermId'
                    AND I.object_id = OBJECT_ID(N'arTransferGrades') )
    BEGIN 
        EXECUTE sp_rename 'arTransferGrades.IX_arTransferGrades_TransferId_StuEnrollId_ReqId_TermId','IX_arTransferGrades_StuEnrollId_TransferId_TermId_ReqId';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_StuEnrollId',
            'IX_atClsSectAttendance_StuEnrollId_Actual_ClsSectionId_MeetDate_ClsSectAttId';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_Tardy_Excused_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_Tardy_Excused_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_Tardy_Excused_StuEnrollId',
            'IX_atClsSectAttendance_StuEnrollId_MeetDate_ClsSectAttId_ClsSectionId_Actual_Tardy_Excused';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_MeetDate_Actual_Excused_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_MeetDate_Actual_Excused_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_MeetDate_Actual_Excused_StuEnrollId',
            'IX_atClsSectAttendance_StuEnrollId_Actual_Excused_MeetDate';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_MeetDate_Actual_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_MeetDate_Actual_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_MeetDate_Actual_StuEnrollId','IX_atClsSectAttendance_StuEnrollId_Actual_MeetDate';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_MeetDate_Actual_Tardy_Excused_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_MeetDate_Actual_Tardy_Excused_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_MeetDate_Actual_Tardy_Excused_StuEnrollId',
            'IX_atClsSectAttendance_StuEnrollId_MeetDate_Actual_Excused_Tardy';
    END; 
GO  

PRINT 'Renaming Index IX_atClsSectAttendance_MeetDate_Actual_Tardy_StuEnrollId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_atClsSectAttendance_MeetDate_Actual_Tardy_StuEnrollId'
                    AND I.object_id = OBJECT_ID(N'atClsSectAttendance') )
    BEGIN 
        EXECUTE sp_rename 'atClsSectAttendance.IX_atClsSectAttendance_MeetDate_Actual_Tardy_StuEnrollId',
            'IX_atClsSectAttendance_Tardy_Actual_StuEnrollId_MeetDate';
    END; 
GO  

PRINT 'Renaming Index UIX_plAddressTypes_AddressDescrip_CampGrpId_AddressCode if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_plAddressTypes_AddressDescrip_CampGrpId_AddressCode'
                    AND I.object_id = OBJECT_ID(N'plAddressTypes') )
    BEGIN 
        EXECUTE sp_rename 'plAddressTypes.UIX_plAddressTypes_AddressDescrip_CampGrpId_AddressCode','UIX_plAddressTypes_AddressCode_AddressDescrip_CampGrpId';
    END; 
GO  

PRINT 'Renaming Index IX_plStudentDocs_StudentId_DocumentId_DocStatusId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_plStudentDocs_StudentId_DocumentId_DocStatusId'
                    AND I.object_id = OBJECT_ID(N'plStudentDocs') )
    BEGIN 
        EXECUTE sp_rename 'plStudentDocs.IX_plStudentDocs_StudentId_DocumentId_DocStatusId','IX_plStudentDocs_StudentId_DocStatusId_DocumentId';
    END; 
GO  

PRINT 'Renaming Index UIX_saBankAccounts_BankAcctNumber_BankRoutingNumber if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_saBankAccounts_BankAcctNumber_BankRoutingNumber'
                    AND I.object_id = OBJECT_ID(N'saBankAccounts') )
    BEGIN 
        EXECUTE sp_rename 'saBankAccounts.UIX_saBankAccounts_BankAcctNumber_BankRoutingNumber','UIX_saBankAccounts_BankRoutingNumber_BankAcctNumber';
    END; 
GO  

PRINT 'Renaming Index UIX_saBankAccounts_StatusId_BankAcctDescrip_CampGrpId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_saBankAccounts_StatusId_BankAcctDescrip_CampGrpId'
                    AND I.object_id = OBJECT_ID(N'saBankAccounts') )
    BEGIN 
        EXECUTE sp_rename 'saBankAccounts.UIX_saBankAccounts_StatusId_BankAcctDescrip_CampGrpId','UIX_saBankAccounts_BankAcctDescrip_StatusId_CampGrpId';
    END; 
GO  

PRINT 'Renaming Index UIX_saBankAccounts_StatusId_BankAcctNumber_CampGrpId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_saBankAccounts_StatusId_BankAcctNumber_CampGrpId'
                    AND I.object_id = OBJECT_ID(N'saBankAccounts') )
    BEGIN 
        EXECUTE sp_rename 'saBankAccounts.UIX_saBankAccounts_StatusId_BankAcctNumber_CampGrpId','UIX_saBankAccounts_BankAcctNumber_StatusId_CampGrpId';
    END; 
GO  

PRINT 'Renaming Index IX_saTransactions_TransactionId_StuEnrollId_TermId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_saTransactions_TransactionId_StuEnrollId_TermId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided'
                    AND I.object_id = OBJECT_ID(N'saTransactions') )
    BEGIN 
        EXECUTE sp_rename 'saTransactions.IX_saTransactions_TransactionId_StuEnrollId_TermId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided',
            'IX_saTransactions_TransTypeId_StuEnrollId_Voided_TransDate_TransactionId_TermId_TransCodeId_TransAmount';
    END; 
GO  

PRINT 'Renaming Index IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransAmount_TransTypeId_IsPosted_Voided if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransAmount_TransTypeId_IsPosted_Voided'
                    AND I.object_id = OBJECT_ID(N'saTransactions') )
    BEGIN 
        EXECUTE sp_rename 'saTransactions.IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransAmount_TransTypeId_IsPosted_Voided',
            'IX_saTransactions_TransTypeId_StuEnrollId_IsPosted_Voided_TransactionId_TransAmount_TransDate';
    END; 
GO  

PRINT 'Renaming Index IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided'
                    AND I.object_id = OBJECT_ID(N'saTransactions') )
    BEGIN 
        EXECUTE sp_rename 'saTransactions.IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided',
            'IX_saTransactions_TransTypeId_StuEnrollId_TransDate_Voided_TransactionId_TransCodeId_TransAmount';
    END; 
GO  

PRINT 'Renaming Index IX_AuditHistDetail_RowId_AuditHistId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_AuditHistDetail_RowId_AuditHistId'
                    AND I.object_id = OBJECT_ID(N'syAuditHistDetail') )
    BEGIN 
        EXECUTE sp_rename 'syAuditHistDetail.IX_AuditHistDetail_RowId_AuditHistId','IX_syAuditHistDetail_RowId_AuditHistId';
    END; 
GO  

PRINT 'Drop Index IX_syAuditHistDetail_AuditHistId_RowId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syAuditHistDetail_AuditHistId_RowId'
                    AND I.object_id = OBJECT_ID(N'syAuditHistDetail') )
    BEGIN 
         --EXECUTE sp_rename 'syAuditHistDetail.IX_syAuditHistDetail_AuditHistId_RowId', 'IX_syAuditHistDetail_RowId_AuditHistId'
        DROP INDEX  IX_syAuditHistDetail_AuditHistId_RowId ON syAuditHistDetail;
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_CreditsEarned_FinalScore_Completed_FinalGPA_coursecredits_FACreditsEarned_TermStartDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_CreditsEarned_FinalScore_Completed_FinalGPA_coursecredits_FACreditsEarned_TermStartDate'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_CreditsEarned_FinalScore_Completed_FinalGPA_coursecredits_FACreditsEarned_TermStartDate',
            'IX_syCreditSummary_StuEnrollId_TermStartDate_TermId_Completed_coursecredits_CreditsEarned_FACreditsEarned_FinalGPA_FinalScore';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_FinalScore_Completed_TermStartDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_FinalScore_Completed_TermStartDate'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_FinalScore_Completed_TermStartDate',
            'IX_syCreditSummary_StuEnrollId_Completed_TermStartDate_TermId_FinalScore';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA_TermId';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_TermId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA';
    END; 
GO  

PRINT 'Renaming Index IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits'
                    AND I.object_id = OBJECT_ID(N'syCreditSummary') )
    BEGIN 
        EXECUTE sp_rename 'syCreditSummary.IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits',
            'IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA';
    END; 
GO  

PRINT 'Renaming Index UIX_syFamilyIncome_FamilyIncomeDescrip_CampGrpId_FamilyIncomeCode if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_syFamilyIncome_FamilyIncomeDescrip_CampGrpId_FamilyIncomeCode'
                    AND I.object_id = OBJECT_ID(N'syFamilyIncome') )
    BEGIN 
        EXECUTE sp_rename 'syFamilyIncome.UIX_syFamilyIncome_FamilyIncomeDescrip_CampGrpId_FamilyIncomeCode',
            'UIX_syFamilyIncome_FamilyIncomeCode_FamilyIncomeDescrip_CampGrpId';
    END; 
GO  

PRINT 'Renaming Index UIX_syHolidays_StatusId_CampGrpId_HolidayStartDate if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_syHolidays_StatusId_CampGrpId_HolidayStartDate'
                    AND I.object_id = OBJECT_ID(N'syHolidays') )
    BEGIN 
        EXECUTE sp_rename 'syHolidays.UIX_syHolidays_StatusId_CampGrpId_HolidayStartDate','UIX_syHolidays_StatusId_HolidayStartDate_CampGrpId';
    END; 
GO  

PRINT 'Renaming Index UIX_syLeadStatusChanges_OrigStatusId_NewStatusId_CampGrpId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'UIX_syLeadStatusChanges_OrigStatusId_NewStatusId_CampGrpId'
                    AND I.object_id = OBJECT_ID(N'syLeadStatusChanges') )
    BEGIN 
        EXECUTE sp_rename 'syLeadStatusChanges.UIX_syLeadStatusChanges_OrigStatusId_NewStatusId_CampGrpId',
            'UIX_syLeadStatusChanges_CampGrpId_OrigStatusId_NewStatusId';
    END; 
GO  

PRINT 'Renaming Index IX_syPeriods_PeriodId_PeriodDescrip_StartTimeId_EndTimeId if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syPeriods_PeriodId_PeriodDescrip_StartTimeId_EndTimeId'
                    AND I.object_id = OBJECT_ID(N'syPeriods') )
    BEGIN 
        EXECUTE sp_rename 'syPeriods.IX_syPeriods_PeriodId_PeriodDescrip_StartTimeId_EndTimeId','IX_syPeriods_PeriodId_StartTimeId_EndTimeId_PeriodDescrip';
    END; 
GO  

PRINT 'Renaming Index IX_syResources_ResourceID_Resource_ResourceTypeID if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syResources_ResourceID_Resource_ResourceTypeID'
                    AND I.object_id = OBJECT_ID(N'syResources') )
    BEGIN 
        EXECUTE sp_rename 'syResources.IX_syResources_ResourceID_Resource_ResourceTypeID','IX_syResources_ResourceTypeID_ResourceID_Resource';
    END; 
GO  

PRINT 'Renaming Index IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualAbsentDays_ConvertTo_Hours if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualAbsentDays_ConvertTo_Hours'
                    AND I.object_id = OBJECT_ID(N'syStudentAttendanceSummary') )
    BEGIN 
        EXECUTE sp_rename 'syStudentAttendanceSummary.IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualAbsentDays_ConvertTo_Hours',
            'IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualAbsentDays_ConvertTo_Hours';
    END; 
GO  

PRINT 'Renaming Index IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualPresentDays_ConvertTo_Hours if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualPresentDays_ConvertTo_Hours'
                    AND I.object_id = OBJECT_ID(N'syStudentAttendanceSummary') )
    BEGIN 
        EXECUTE sp_rename 'syStudentAttendanceSummary.IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualPresentDays_ConvertTo_Hours',
            'IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualPresentDays_ConvertTo_Hours';
    END; 
GO  

PRINT 'Renaming Index IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualTardyDays_ConvertTo_Hours if it exists';
IF EXISTS ( SELECT  1
            FROM    sys.indexes AS I
            WHERE   I.name = 'IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualTardyDays_ConvertTo_Hours'
                    AND I.object_id = OBJECT_ID(N'syStudentAttendanceSummary') )
    BEGIN 
        EXECUTE sp_rename 'syStudentAttendanceSummary.IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualTardyDays_ConvertTo_Hours',
            'IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualTardyDays_ConvertTo_Hours';
    END; 
GO  
-- =================================================================================================
-- END  --  script to Update all index that do not complain with the A1 Standard naming to meet the standard
-- =================================================================================================


-- =================================================================================================
-- script to Update all  Constranint that do not complain with the A1 Standard naming
-- =================================================================================================

-- No found

-- =================================================================================================
-- END --  script to Update all  Constranint that do not complain with the A1 Standard naming
-- =================================================================================================



