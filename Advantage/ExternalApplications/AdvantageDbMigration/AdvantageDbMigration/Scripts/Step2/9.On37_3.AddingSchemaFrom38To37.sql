/* 
Run this script on: 
 
        TORRES\SQLDEV2016.A1_CHAT_38_Main    -  This database will be modified 
 
to synchronize it with: 
 
        TORRES\SQLDEV2016.AMC_3.8 
 
You are recommended to back up your database before running this script 
 
Script created by SQL Compare version 12.0.33.3389 from Red Gate Software Ltd at 2/1/2017 11:15:44 AM 
 
*/
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[PriorWorkAddress]';
GO
ALTER TABLE dbo.PriorWorkAddress DROP CONSTRAINT FK_PriorWorkAddress_adLeadEmployment_StEmploymentId_StEmploymentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[PriorWorkContact]';
GO
ALTER TABLE dbo.PriorWorkContact DROP CONSTRAINT FK_PriorWorkContact_adLeadEmployment_StEmploymentId_StEmploymentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeadDuplicates]';
GO
ALTER TABLE dbo.adLeadDuplicates DROP CONSTRAINT FK_adLeadDuplicates_adLeads_NewLeadGuid_LeadId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeadEmployment]';
GO
ALTER TABLE dbo.adLeadEmployment DROP CONSTRAINT FK_adLeadEmployment_adLeads_LeadId_LeadId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeadOtherContactsPhone]';
GO
ALTER TABLE dbo.adLeadOtherContactsPhone DROP CONSTRAINT FK_adLeadOtherContactsPhone_adLeadOtherContacts;
GO
ALTER TABLE dbo.adLeadOtherContactsPhone DROP CONSTRAINT FK_adLeadOtherContactsPhone_syStatuses;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel DROP CONSTRAINT FK_adLevel_syStatuses_StatusId_StatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[arStudAddressesOld]';
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT FK_arStudAddresses_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT FK_arStudAddresses_syStates_StateId_StateId;
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT FK_arStudAddresses_adCountries_CountryId_CountryId;
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT FK_arStudAddresses_plAddressTypes_AddressTypeId_AddressTypeId;
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT FK_arStudAddresses_syStatuses_StatusId_StatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[arStudentPhoneOld]';
GO
ALTER TABLE dbo.arStudentPhoneOld DROP CONSTRAINT FK_arStudentPhone_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.arStudentPhoneOld DROP CONSTRAINT FK_arStudentPhone_syPhoneType_PhoneTypeId_PhoneTypeId;
GO
ALTER TABLE dbo.arStudentPhoneOld DROP CONSTRAINT FK_arStudentPhone_syStatuses_StatusId_StatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[plStudentEducationOld]';
GO
ALTER TABLE dbo.plStudentEducationOld DROP CONSTRAINT FK_plStudentEducation_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.plStudentEducationOld DROP CONSTRAINT FK_plStudentEducation_arDegrees_CertificateId_DegreeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[plStudentEmploymentOld]';
GO
ALTER TABLE dbo.plStudentEmploymentOld DROP CONSTRAINT FK_plStudentEmployment_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.plStudentEmploymentOld DROP CONSTRAINT FK_plStudentEmployment_adTitles_JobTitleId_TitleId;
GO
ALTER TABLE dbo.plStudentEmploymentOld DROP CONSTRAINT FK_plStudentEmployment_plJobStatus_JobStatusId_JobStatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[plStudentExtraCurricularsOld]';
GO
ALTER TABLE dbo.plStudentExtraCurricularsOld DROP CONSTRAINT FK_plStudentExtraCurriculars_arStudent_StudentID_StudentId;
GO
ALTER TABLE dbo.plStudentExtraCurricularsOld DROP CONSTRAINT FK_plStudentExtraCurriculars_adExtraCurr_ExtraCurricularID_ExtraCurrId;
GO
ALTER TABLE dbo.plStudentExtraCurricularsOld DROP CONSTRAINT FK_plStudentExtraCurriculars_adExtraCurrGrp_ExtraCurricularGrpID_ExtraCurrGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[plStudentSkillsOld]';
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT FK_plStudentSkills_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT FK_plStudentSkills_plSkills_SkillId_SkillId;
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT FK_plStudentSkills_plSkillGroups_SkillGrpId_SkillGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syStudentContactsOld]';
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT FK_syStudentContacts_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT FK_syStudentContacts_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT FK_syStudentContacts_syPrefixes_PrefixId_PrefixId;
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT FK_syStudentContacts_sySuffixes_SuffixId_SuffixId;
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT FK_syStudentContacts_syRelations_RelationId_RelationId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syStudentNotesOld]';
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT FK_syStudentNotes_arStudent_StudentId_StudentId;
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT FK_syStudentNotes_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT FK_syStudentNotes_syUsers_UserId_UserId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[arStudentOld]';
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_syStatuses_StudentStatus_StatusId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_syPrefixes_Prefix_PrefixId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_sySuffixes_Suffix_SuffixId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adAgencySponsors_Sponsor_AgencySpId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adGenders_Gender_GenderId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adEthCodes_Race_EthCodeId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adMaritalStatus_MaritalStatus_MaritalStatId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_syFamilyIncome_FamilyIncome_FamilyIncomeID;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_arShifts_ShiftId_ShiftId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adNationalities_Nationality_NationalityId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adCitizenships_Citizen_CitizenshipId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_syStates_DrivLicStateId_StateId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adCounties_County_CountyId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adEdLvls_EdLvlId_EdLvlId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adDependencyTypes_DependencyTypeId_DependencyTypeId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adDegCertSeeking_DegCertSeekingId_DegCertSeekingId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adGeographicTypes_GeographicTypeId_GeographicTypeId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_arHousing_HousingId_HousingId;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_adAdminCriteria_admincriteriaid_admincriteriaid;
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT FK_arStudent_arAttendTypes_AttendTypeId_AttendTypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syStudentContactAddressesOld]';
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT FK_syStudentContactAddresses_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT FK_syStudentContactAddresses_syStudentContacts_StudentContactId_StudentContactId;
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT FK_syStudentContactAddresses_plAddressTypes_AddrTypId_AddressTypeId;
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT FK_syStudentContactAddresses_syStates_StateId_StateId;
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT FK_syStudentContactAddresses_adCountries_CountryId_CountryId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syStudentContactPhonesOld]';
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT FK_syStudentContactPhones_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT FK_syStudentContactPhones_syStudentContacts_StudentContactId_StudentContactId;
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT FK_syStudentContactPhones_syPhoneType_PhoneTypeId_PhoneTypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules DROP CONSTRAINT FK_syUniversalSearchModules_syUniversalSearchModules;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles DROP CONSTRAINT FK_AdVehicles_adLeads_LeadId_LeadId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLead_Notes]';
GO
ALTER TABLE dbo.adLead_Notes DROP CONSTRAINT FK_adLead_Notes_AllNotes_NotesId_NotesId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeadOtherContactsAddreses]';
GO
ALTER TABLE dbo.adLeadOtherContactsAddreses DROP CONSTRAINT FK_adLeadOtherContactsAddreses_adLeadOtherContacts_OtherContactId_OtherContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeadOtherContactsEmail]';
GO
ALTER TABLE dbo.adLeadOtherContactsEmail DROP CONSTRAINT FK_adLeadOtherContactsEmail_adLeadOtherContacts_OtherContactId_OtherContactId;
GO
ALTER TABLE dbo.adLeadOtherContactsEmail DROP CONSTRAINT FK_adLeadOtherContactsEmail_syEmailType_EmailTypeId_EmailTypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adExtraCurricular]';
GO
ALTER TABLE dbo.adExtraCurricular DROP CONSTRAINT FK_adExtraCurricular_adLevel_LevelId_LevelId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adSkills]';
GO
ALTER TABLE dbo.adSkills DROP CONSTRAINT FK_adSkills_adLevel_LevelId_LevelId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syInstitutions]';
GO
ALTER TABLE dbo.syInstitutions DROP CONSTRAINT FK_syInstitutions_adLevel_LevelId_LevelId;
GO
ALTER TABLE dbo.syInstitutions DROP CONSTRAINT FK_syInstitutions_syInstitutionImportTypes_ImportTypeId_ImportTypeID;
GO
ALTER TABLE dbo.syInstitutions DROP CONSTRAINT FK_syInstitutions_syInstitutionTypes_TypeId_TypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adLeads]';
GO
ALTER TABLE dbo.adLeads DROP CONSTRAINT FK_adLeads_adVendorCampaign_CampaignId_CampaignId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign DROP CONSTRAINT FK_adVendorCampaign_adVendors_VendorId_VendorId;
GO
ALTER TABLE dbo.adVendorCampaign DROP CONSTRAINT FK_adVendorCampaign_adVendorPayFor_PayForId_IdPayFor;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[arGrdBkResults]';
GO
ALTER TABLE dbo.arGrdBkResults DROP CONSTRAINT FK_arGrdBkResults_arStuEnrollments_StuEnrollId_StuEnrollId;
GO
ALTER TABLE dbo.arGrdBkResults DROP CONSTRAINT FK_arGrdBkResults_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtDetailId;
GO
ALTER TABLE dbo.arGrdBkResults DROP CONSTRAINT FK_arGrdBkResults_arClassSections_ClsSectionId_ClsSectionId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[plAddressTypes]';
GO
ALTER TABLE dbo.plAddressTypes DROP CONSTRAINT FK_plAddressTypes_syCampGrps_CampGrpId_CampGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[saBankAccounts]';
GO
ALTER TABLE dbo.saBankAccounts DROP CONSTRAINT FK_saBankAccounts_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.saBankAccounts DROP CONSTRAINT FK_saBankAccounts_syCampGrps_CampGrpId_CampGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[AdLeadEmail]';
GO
ALTER TABLE dbo.AdLeadEmail DROP CONSTRAINT FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syFamilyIncome]';
GO
ALTER TABLE dbo.syFamilyIncome DROP CONSTRAINT FK_syFamilyIncome_syCampGrps_CampGrpId_CampGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syHolidays]';
GO
ALTER TABLE dbo.syHolidays DROP CONSTRAINT FK_syHolidays_syStatuses_StatusId_StatusId;
GO
ALTER TABLE dbo.syHolidays DROP CONSTRAINT FK_syHolidays_syCampGrps_CampGrpId_CampGrpId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syLeadStatusChanges]';
GO
ALTER TABLE dbo.syLeadStatusChanges DROP CONSTRAINT FK_syLeadStatusChanges_syCampGrps_CampGrpId_CampGrpId;
GO
ALTER TABLE dbo.syLeadStatusChanges DROP CONSTRAINT FK_syLeadStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId;
GO
ALTER TABLE dbo.syLeadStatusChanges DROP CONSTRAINT FK_syLeadStatusChanges_syStatusCodes_NewStatusId_StatusCodeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[AllNotes]';
GO
ALTER TABLE dbo.AllNotes DROP CONSTRAINT FK_AllNotes_syModules_ModuleCode_ModuleID;
GO
ALTER TABLE dbo.AllNotes DROP CONSTRAINT FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping foreign keys from [dbo].[syResourceSdf]';
GO
ALTER TABLE dbo.syResourceSdf DROP CONSTRAINT FK_syResourceSdf_sySDF_sdfID_SDFId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[AdLeadEmail]';
GO
ALTER TABLE dbo.AdLeadEmail DROP CONSTRAINT PK_AdLeadEmail;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[AdLeadReqsReceived]';
GO
ALTER TABLE dbo.AdLeadReqsReceived DROP CONSTRAINT PK_AdLeadReqsReceived;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles DROP CONSTRAINT PK_AdVehicles;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles DROP CONSTRAINT IX_AdVehicles_LeadId_Position;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[AllNotes]';
GO
ALTER TABLE dbo.AllNotes DROP CONSTRAINT PK_AllNotes;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[PriorWorkAddress]';
GO
ALTER TABLE dbo.PriorWorkAddress DROP CONSTRAINT PK_PriorWorkAddress;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[PriorWorkContact]';
GO
ALTER TABLE dbo.PriorWorkContact DROP CONSTRAINT PK_PriorWorkContact;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adExtraCurricular]';
GO
ALTER TABLE dbo.adExtraCurricular DROP CONSTRAINT PK_adExtraCurricular;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadAddresses]';
GO
ALTER TABLE dbo.adLeadAddresses DROP CONSTRAINT PK_adLeadAddresses;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadDuplicates]';
GO
ALTER TABLE dbo.adLeadDuplicates DROP CONSTRAINT PK_adLeadDuplicates;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadOtherContactsAddreses]';
GO
ALTER TABLE dbo.adLeadOtherContactsAddreses DROP CONSTRAINT PK_adLeadOtherContactsAddreses;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadOtherContactsEmail]';
GO
ALTER TABLE dbo.adLeadOtherContactsEmail DROP CONSTRAINT PK_adLeadOtherContactsEmail;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadOtherContacts]';
GO
ALTER TABLE dbo.adLeadOtherContacts DROP CONSTRAINT IX_adLeadOtherContacts_OtherContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadTranReceived]';
GO
ALTER TABLE dbo.adLeadTranReceived DROP CONSTRAINT PK_adLeadTranReceived;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLead_Notes]';
GO
ALTER TABLE dbo.adLead_Notes DROP CONSTRAINT PK_adLead_Notes;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel DROP CONSTRAINT PK_adExtraCurricularLevel;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel DROP CONSTRAINT DF_adExtraCurricularLevel_StatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adSkills]';
GO
ALTER TABLE dbo.adSkills DROP CONSTRAINT PK_adSkills;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign DROP CONSTRAINT PK_adVendorCampaign;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign DROP CONSTRAINT UQ_adVendorCampaign_CampaignCode;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign DROP CONSTRAINT UQ_adVendorCampaign_CampaignId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor DROP CONSTRAINT PK_adVendorPayFor;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor DROP CONSTRAINT UQ_adVendorPayFor_IdPayFor;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor DROP CONSTRAINT UQ_adVendorPayFor_PayForCode;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendors]';
GO
ALTER TABLE dbo.adVendors DROP CONSTRAINT PK_adVendors;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adVendors]';
GO
ALTER TABLE dbo.adVendors DROP CONSTRAINT UQ_adVendors_VendorCode;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudAddressesOld]';
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT PK_arStudAddresses_StdAddressId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudAddressesOld]';
GO
ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT DF_arStudAddresses_StdAddressId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudAddressesOld]';
GO
IF EXISTS ( SELECT  1
            FROM    sys.default_constraints AS DC
            INNER JOIN sys.tables AS T ON T.object_id = DC.parent_object_id
            WHERE   DC.name = 'DF_arStudAddresses_ForeignZip'
                    AND T.name = 'arStudAddressesOld' )
    BEGIN
        ALTER TABLE dbo.arStudAddressesOld DROP CONSTRAINT DF_arStudAddresses_ForeignZip;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudentOld]';
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT PK_arStudent_StudentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudentOld]';
GO
ALTER TABLE dbo.arStudentOld DROP CONSTRAINT DF_arStudent_StudentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudentPhoneOld]';
GO
ALTER TABLE dbo.arStudentPhoneOld DROP CONSTRAINT PK_arStudentPhone_StudentPhoneId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[arStudentPhoneOld]';
GO
ALTER TABLE dbo.arStudentPhoneOld DROP CONSTRAINT DF_arStudentPhone_StudentPhoneId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentEducationOld]';
GO
ALTER TABLE dbo.plStudentEducationOld DROP CONSTRAINT PK_plStudentEducation_StuEducationId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentEducationOld]';
GO
ALTER TABLE dbo.plStudentEducationOld DROP CONSTRAINT DF_plStudentEducation_StuEducationId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentEmploymentOld]';
GO
ALTER TABLE dbo.plStudentEmploymentOld DROP CONSTRAINT PK_plStudentEmployment_StEmploymentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentEmploymentOld]';
GO
ALTER TABLE dbo.plStudentEmploymentOld DROP CONSTRAINT DF_plStudentEmployment_StEmploymentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentExtraCurricularsOld]';
GO
ALTER TABLE dbo.plStudentExtraCurricularsOld DROP CONSTRAINT PK_plStudentExtraCurriculars_StuExtraCurricularID;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentExtraCurricularsOld]';
GO
ALTER TABLE dbo.plStudentExtraCurricularsOld DROP CONSTRAINT DF_plStudentExtraCurriculars_StuExtraCurricularID;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentSkillsOld]';
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT PK_plStudentSkills_StudentSkillId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentSkillsOld]';
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT DF_plStudentSkills_StudentSkillId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[plStudentSkillsOld]';
GO
ALTER TABLE dbo.plStudentSkillsOld DROP CONSTRAINT DF_plStudentSkills_StudentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syEmailType]';
GO
ALTER TABLE dbo.syEmailType DROP CONSTRAINT PK_syEmailType;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syInstitutionContacts]';
GO
ALTER TABLE dbo.syInstitutionContacts DROP CONSTRAINT PK_syInstitutionContacts_InstitutionContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syInstitutionContacts]';
GO
ALTER TABLE dbo.syInstitutionContacts DROP CONSTRAINT DF_syInstitutionContacts_InstitutionId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syInstitutionImportTypes]';
GO
ALTER TABLE dbo.syInstitutionImportTypes DROP CONSTRAINT FK_syInstitutionImportTypes_ImportTypeID;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syInstitutionTypes]';
GO
ALTER TABLE dbo.syInstitutionTypes DROP CONSTRAINT FK_syInstitutionTypes_TypeId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syModules]';
GO
ALTER TABLE dbo.syModules DROP CONSTRAINT UN_syModules_ModuleCode;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syNotesPageFields]';
GO
ALTER TABLE dbo.syNotesPageFields DROP CONSTRAINT PK_syNotesPageFields;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syResourceSdf]';
GO
ALTER TABLE dbo.syResourceSdf DROP CONSTRAINT UC_syResorceSdf_ResourceId_sdfID_EntityId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactAddressesOld]';
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT PK_syStudentContactAddresses_StudentContactAddressId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactAddressesOld]';
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT DF_syStudentContactAddresses_StudentContactAddressId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactAddressesOld]';
GO
ALTER TABLE dbo.syStudentContactAddressesOld DROP CONSTRAINT DF_syStudentContactAddresses_ForeignZip;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactPhonesOld]';
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT PK_syStudentContactPhones_StudentContactPhoneId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactPhonesOld]';
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT DF_syStudentContactPhones_StudentContactPhoneId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactPhonesOld]';
GO
ALTER TABLE dbo.syStudentContactPhonesOld DROP CONSTRAINT DF_syStudentContactPhones_ForeignPhone;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactsOld]';
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT PK_syStudentContacts_StudentContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentContactsOld]';
GO
ALTER TABLE dbo.syStudentContactsOld DROP CONSTRAINT DF_syStudentContacts_StudentContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentNotesOld]';
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT PK_syStudentNotes_StudentNoteId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentNotesOld]';
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT DF_syStudentNotes_StudentNoteId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syStudentNotesOld]';
GO
ALTER TABLE dbo.syStudentNotesOld DROP CONSTRAINT DF_syStudentNotes_ModuleCode;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules DROP CONSTRAINT UN_syModules_Code;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules DROP CONSTRAINT DF_syUniversalSearchModules_ModDate;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadOtherContactsPhone]';
GO
ALTER TABLE dbo.adLeadOtherContactsPhone DROP CONSTRAINT DF_adLeadOtherContactsPhone_IsInternational;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeads]';
GO
ALTER TABLE dbo.adLeads DROP CONSTRAINT DF_adLeads_PreferedContact;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_adAdvInterval_AdvIntervalDescrip_AdvIntervalCode] from [dbo].[adAdvInterval]';
GO
DROP INDEX UIX_adAdvInterval_AdvIntervalDescrip_AdvIntervalCode ON dbo.adAdvInterval;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_adEntrTestOverRide_EntrTestId_OverRide_StudentId] from [dbo].[adEntrTestOverRide]';
GO
DROP INDEX IX_adEntrTestOverRide_EntrTestId_OverRide_StudentId ON dbo.adEntrTestOverRide;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_adLeadByLeadGroups_LeadGrpId_StuEnrollId] from [dbo].[adLeadByLeadGroups]';
GO
DROP INDEX IX_adLeadByLeadGroups_LeadGrpId_StuEnrollId ON dbo.adLeadByLeadGroups;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UDX_adLeads_LeadId_ModDate] from [dbo].[adLeads]';
GO
DROP INDEX UDX_adLeads_LeadId_ModDate ON dbo.adLeads;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [AdLead_StudentId] from [dbo].[adLeads]';
GO
DROP INDEX AdLead_StudentId ON dbo.adLeads;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UDX_adLeads_StudentId_NotZeros] from [dbo].[adLeads]';
GO
DROP INDEX UDX_adLeads_StudentId_NotZeros ON dbo.adLeads;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [AdLead_StudentNumber] from [dbo].[adLeads]';
GO
DROP INDEX AdLead_StudentNumber ON dbo.adLeads;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_adSourceCatagory_SourceCatagoryDescrip_SourceCatagoryCode] from [dbo].[adSourceCatagory]';
GO
DROP INDEX UIX_adSourceCatagory_SourceCatagoryDescrip_SourceCatagoryCode ON dbo.adSourceCatagory;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_adSourceType_SourceTypeDescrip_SourceTypeCode] from [dbo].[adSourceType]';
GO
DROP INDEX UIX_adSourceType_SourceTypeDescrip_SourceTypeCode ON dbo.adSourceType;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arClassSections_ClsSectionId_TermId_ReqId_InstrGrdBkWgtId_GrdScaleId] from [dbo].[arClassSections]';
GO
DROP INDEX IX_arClassSections_ClsSectionId_TermId_ReqId_InstrGrdBkWgtId_GrdScaleId ON dbo.arClassSections;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arClsSectMeetings_ClsSectionId_PeriodId_StartDate_EndDate] from [dbo].[arClsSectMeetings]';
GO
DROP INDEX IX_arClsSectMeetings_ClsSectionId_PeriodId_StartDate_EndDate ON dbo.arClsSectMeetings;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkConversionResults_ConversionResultId_ReqId_GrdComponentTypeId_Score_StuEnrollId_MinResult_TermId] from [dbo].[arGrdBkConversionResults]';
GO
DROP INDEX IX_arGrdBkConversionResults_ConversionResultId_ReqId_GrdComponentTypeId_Score_StuEnrollId_MinResult_TermId ON dbo.arGrdBkConversionResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkResults_GrdBkResultId_ClsSectionId_InstrGrdBkWgtDetailId_Score_StuEnrollId] from [dbo].[arGrdBkResults]';
GO
DROP INDEX IX_arGrdBkResults_GrdBkResultId_ClsSectionId_InstrGrdBkWgtDetailId_Score_StuEnrollId ON dbo.arGrdBkResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_Score] from [dbo].[arGrdBkResults]';
GO
DROP INDEX IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_Score ON dbo.arGrdBkResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkResults_ClsSectionId_StuEnrollId] from [dbo].[arGrdBkResults]';
GO
DROP INDEX IX_arGrdBkResults_ClsSectionId_StuEnrollId ON dbo.arGrdBkResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_StuEnrollId_ResNum_PostDate] from [dbo].[arGrdBkResults]';
GO
DROP INDEX UIX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId_StuEnrollId_ResNum_PostDate ON dbo.arGrdBkResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number_Required_MustPass] from [dbo].[arGrdBkWgtDetails]';
GO
DROP INDEX IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number_Required_MustPass ON dbo.arGrdBkWgtDetails;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtId_GrdComponentTypeId_Number] from [dbo].[arGrdBkWgtDetails]';
GO
DROP INDEX IX_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtId_GrdComponentTypeId_Number ON dbo.arGrdBkWgtDetails;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arGrdComponentTypes_GrdComponentTypeId_Descrip_SysComponentTypeId] from [dbo].[arGrdComponentTypes]';
GO
DROP INDEX IX_arGrdComponentTypes_GrdComponentTypeId_Descrip_SysComponentTypeId ON dbo.arGrdComponentTypes;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arReqs_ReqId_Code_Descrip_Credits_FinAidCredits] from [dbo].[arReqs]';
GO
DROP INDEX IX_arReqs_ReqId_Code_Descrip_Credits_FinAidCredits ON dbo.arReqs;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arResults_ResultId_TestId_Score_GrdSysDetailId_StuEnrollId] from [dbo].[arResults]';
GO
DROP INDEX IX_arResults_ResultId_TestId_Score_GrdSysDetailId_StuEnrollId ON dbo.arResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arResults_TestId_StuEnrollId] from [dbo].[arResults]';
GO
DROP INDEX IX_arResults_TestId_StuEnrollId ON dbo.arResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arResults_TestId_Score_StuEnrollId] from [dbo].[arResults]';
GO
DROP INDEX IX_arResults_TestId_Score_StuEnrollId ON dbo.arResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arResults_TestId_Score_GrdSysDetailId_StuEnrollId] from [dbo].[arResults]';
GO
DROP INDEX IX_arResults_TestId_Score_GrdSysDetailId_StuEnrollId ON dbo.arResults;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UDX_arStuEnrollments_StudentId_ModDate] from [dbo].[arStuEnrollments]';
GO
DROP INDEX UDX_arStuEnrollments_StudentId_ModDate ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StudentId_PrgVerId_ShiftId_StatusCodeId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StudentId_PrgVerId_ShiftId_StatusCodeId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ExpGradDate_CampusId_StatusCodeId_EdLvlId_degcertseekingid] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ExpGradDate_CampusId_StatusCodeId_EdLvlId_degcertseekingid ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StudentId_ShiftId_StatusCodeId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StudentId_ShiftId_StatusCodeId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_StartDate_ShiftId_StatusCodeId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StuEnrollId_PrgVerId_CampusId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StuEnrollId_PrgVerId_CampusId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_CampusId] from [dbo].[arStuEnrollments]';
GO
DROP INDEX IX_arStuEnrollments_StuEnrollId_StudentId_PrgVerId_CampusId ON dbo.arStuEnrollments;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudentClockAttendance_StuEnrollId_RecordDate_isTardy] from [dbo].[arStudentClockAttendance]';
GO
DROP INDEX IX_arStudentClockAttendance_StuEnrollId_RecordDate_isTardy ON dbo.arStudentClockAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours_isTardy] from [dbo].[arStudentClockAttendance]';
GO
DROP INDEX IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours_isTardy ON dbo.arStudentClockAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_StudentId] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_StudentId ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_StudentId_FirstName_LastName_SSN] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_StudentId_FirstName_LastName_SSN ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_StudentId_FirstName_LastName_MiddleName] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_StudentId_FirstName_LastName_MiddleName ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_StudentId_HousingId_admincriteriaid_Citizen_Race_Gender_DOB_FirstName_LastName_MiddleName_SSN_StudentNumber] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_StudentId_HousingId_admincriteriaid_Citizen_Race_Gender_DOB_FirstName_LastName_MiddleName_SSN_StudentNumber ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_FirstName] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_FirstName ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_arStudent_FirstName_LastName_MiddleName_SSN] from [dbo].[arStudentOld]';
GO
DROP INDEX UIX_arStudent_FirstName_LastName_MiddleName_SSN ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_LastName] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_LastName ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_SSN] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_SSN ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arStudent_StudentNumber] from [dbo].[arStudentOld]';
GO
DROP INDEX IX_arStudent_StudentNumber ON dbo.arStudentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arTerm_TermId_TermDescrip_StartDate_EndDate] from [dbo].[arTerm]';
GO
DROP INDEX IX_arTerm_TermId_TermDescrip_StartDate_EndDate ON dbo.arTerm;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_arTransferGrades_TransferId_StuEnrollId_ReqId_TermId] from [dbo].[arTransferGrades]';
GO
DROP INDEX IX_arTransferGrades_TransferId_StuEnrollId_ReqId_TermId ON dbo.arTransferGrades;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_MeetDate_Actual_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_MeetDate_Actual_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_MeetDate_Actual_Excused_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_MeetDate_Actual_Excused_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_MeetDate_Actual_Tardy_Excused_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_MeetDate_Actual_Tardy_Excused_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_Tardy_Excused_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_ClsSectAttId_ClsSectionId_MeetDate_Actual_Tardy_Excused_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_atClsSectAttendance_MeetDate_Actual_Tardy_StuEnrollId] from [dbo].[atClsSectAttendance]';
GO
DROP INDEX IX_atClsSectAttendance_MeetDate_Actual_Tardy_StuEnrollId ON dbo.atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_plAddressTypes_AddressDescrip_CampGrpId_AddressCode] from [dbo].[plAddressTypes]';
GO
DROP INDEX UIX_plAddressTypes_AddressDescrip_CampGrpId_AddressCode ON dbo.plAddressTypes;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_plStudentDocs_StudentId_DocumentId_DocStatusId] from [dbo].[plStudentDocs]';
GO
DROP INDEX IX_plStudentDocs_StudentId_DocumentId_DocStatusId ON dbo.plStudentDocs;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_plStudentEducation_StudentId_EducationInstId_GraduatedDate] from [dbo].[plStudentEducationOld]';
GO
DROP INDEX UIX_plStudentEducation_StudentId_EducationInstId_GraduatedDate ON dbo.plStudentEducationOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_plStudentEmployment_StudentId_JobTitleId_EmployerName_EmployerJobTitle] from [dbo].[plStudentEmploymentOld]';
GO
DROP INDEX UIX_plStudentEmployment_StudentId_JobTitleId_EmployerName_EmployerJobTitle ON dbo.plStudentEmploymentOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_saBankAccounts_StatusId_BankAcctDescrip_CampGrpId] from [dbo].[saBankAccounts]';
GO
DROP INDEX UIX_saBankAccounts_StatusId_BankAcctDescrip_CampGrpId ON dbo.saBankAccounts;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_saBankAccounts_StatusId_BankAcctNumber_CampGrpId] from [dbo].[saBankAccounts]';
GO
DROP INDEX UIX_saBankAccounts_StatusId_BankAcctNumber_CampGrpId ON dbo.saBankAccounts;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_saBankAccounts_BankAcctNumber_BankRoutingNumber] from [dbo].[saBankAccounts]';
GO
DROP INDEX UIX_saBankAccounts_BankAcctNumber_BankRoutingNumber ON dbo.saBankAccounts;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransAmount_TransTypeId_IsPosted_Voided] from [dbo].[saTransactions]';
GO
DROP INDEX IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransAmount_TransTypeId_IsPosted_Voided ON dbo.saTransactions;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided] from [dbo].[saTransactions]';
GO
DROP INDEX IX_saTransactions_TransactionId_StuEnrollId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided ON dbo.saTransactions;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_saTransactions_TransactionId_StuEnrollId_TermId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided] from [dbo].[saTransactions]';
GO
DROP INDEX IX_saTransactions_TransactionId_StuEnrollId_TermId_TransDate_TransCodeId_TransAmount_TransTypeId_Voided ON dbo.saTransactions;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_AuditHistDetail_RowId_AuditHistId] from [dbo].[syAuditHistDetail]';
GO
DROP INDEX IX_AuditHistDetail_RowId_AuditHistId ON dbo.syAuditHistDetail;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_FinalScore_Completed_TermStartDate] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_FinalScore_Completed_TermStartDate ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_SimpleAverage_Credits_GPA_Count_SimpleAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_ReqId_Product_WeightedAverage_Credits_GPA_Count_WeightedAverage_Credits ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syCreditSummary_StuEnrollId_TermId_CreditsEarned_FinalScore_Completed_FinalGPA_coursecredits_FACreditsEarned_TermStartDate] from [dbo].[syCreditSummary]';
GO
DROP INDEX IX_syCreditSummary_StuEnrollId_TermId_CreditsEarned_FinalScore_Completed_FinalGPA_coursecredits_FACreditsEarned_TermStartDate ON dbo.syCreditSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_syFamilyIncome_FamilyIncomeDescrip_CampGrpId_FamilyIncomeCode] from [dbo].[syFamilyIncome]';
GO
DROP INDEX UIX_syFamilyIncome_FamilyIncomeDescrip_CampGrpId_FamilyIncomeCode ON dbo.syFamilyIncome;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_syHolidays_StatusId_CampGrpId_HolidayStartDate] from [dbo].[syHolidays]';
GO
DROP INDEX UIX_syHolidays_StatusId_CampGrpId_HolidayStartDate ON dbo.syHolidays;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [UIX_syLeadStatusChanges_OrigStatusId_NewStatusId_CampGrpId] from [dbo].[syLeadStatusChanges]';
GO
DROP INDEX UIX_syLeadStatusChanges_OrigStatusId_NewStatusId_CampGrpId ON dbo.syLeadStatusChanges;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syPeriods_PeriodId_PeriodDescrip_StartTimeId_EndTimeId] from [dbo].[syPeriods]';
GO
DROP INDEX IX_syPeriods_PeriodId_PeriodDescrip_StartTimeId_EndTimeId ON dbo.syPeriods;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syResources_ResourceID_Resource_ResourceTypeID] from [dbo].[syResources]';
GO
DROP INDEX IX_syResources_ResourceID_Resource_ResourceTypeID ON dbo.syResources;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualAbsentDays_ConvertTo_Hours] from [dbo].[syStudentAttendanceSummary]';
GO
DROP INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualAbsentDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualPresentDays_ConvertTo_Hours] from [dbo].[syStudentAttendanceSummary]';
GO
DROP INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualPresentDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping index [IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualTardyDays_ConvertTo_Hours] from [dbo].[syStudentAttendanceSummary]';
GO
DROP INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate_ActualTardyDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudAddresses_Audit_Delete] from [dbo].[arStudAddressesOld]';
GO
DROP TRIGGER dbo.arStudAddresses_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudAddresses_Audit_Insert] from [dbo].[arStudAddressesOld]';
GO
DROP TRIGGER dbo.arStudAddresses_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudAddresses_Audit_Update] from [dbo].[arStudAddressesOld]';
GO
DROP TRIGGER dbo.arStudAddresses_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudent_Audit_Delete] from [dbo].[arStudentOld]';
GO
DROP TRIGGER dbo.arStudent_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudent_Audit_Insert] from [dbo].[arStudentOld]';
GO
DROP TRIGGER dbo.arStudent_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudent_Audit_Update] from [dbo].[arStudentOld]';
GO
DROP TRIGGER dbo.arStudent_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudentPhone_Audit_Delete] from [dbo].[arStudentPhoneOld]';
GO
DROP TRIGGER dbo.arStudentPhone_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudentPhone_Audit_Insert] from [dbo].[arStudentPhoneOld]';
GO
DROP TRIGGER dbo.arStudentPhone_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[arStudentPhone_Audit_Update] from [dbo].[arStudentPhoneOld]';
GO
DROP TRIGGER dbo.arStudentPhone_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEducation_Audit_Delete] from [dbo].[plStudentEducationOld]';
GO
DROP TRIGGER dbo.plStudentEducation_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEducation_Audit_Insert] from [dbo].[plStudentEducationOld]';
GO
DROP TRIGGER dbo.plStudentEducation_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEducation_Audit_Update] from [dbo].[plStudentEducationOld]';
GO
DROP TRIGGER dbo.plStudentEducation_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEmployment_Audit_Delete] from [dbo].[plStudentEmploymentOld]';
GO
DROP TRIGGER dbo.plStudentEmployment_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEmployment_Audit_Insert] from [dbo].[plStudentEmploymentOld]';
GO
DROP TRIGGER dbo.plStudentEmployment_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[plStudentEmployment_Audit_Update] from [dbo].[plStudentEmploymentOld]';
GO
DROP TRIGGER dbo.plStudentEmployment_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[syStudentContacts_Audit_Delete] from [dbo].[syStudentContactsOld]';
GO
DROP TRIGGER dbo.syStudentContacts_Audit_Delete; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[syStudentContacts_Audit_Insert] from [dbo].[syStudentContactsOld]';
GO
DROP TRIGGER dbo.syStudentContacts_Audit_Insert; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[syStudentContacts_Audit_Update] from [dbo].[syStudentContactsOld]';
GO
DROP TRIGGER dbo.syStudentContacts_Audit_Update; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
--PRINT N'Dropping [dbo].[syStudentNotesOld]';
--GO
--DROP TABLE dbo.syStudentNotesOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[plStudentSkillsOld]';
--GO
--DROP TABLE dbo.plStudentSkillsOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[plStudentExtraCurricularsOld]';
--GO
--DROP TABLE dbo.plStudentExtraCurricularsOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[syStudentContactPhonesOld]';
--GO
--DROP TABLE dbo.syStudentContactPhonesOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[syStudentContactAddressesOld]';
--GO
--DROP TABLE dbo.syStudentContactAddressesOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[arStudentPhoneOld]';
--GO
--DROP TABLE dbo.arStudentPhoneOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[arStudentOld]';
--GO
--DROP TABLE dbo.arStudentOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[arStudAddressesOld]';
--GO
--DROP TABLE dbo.arStudAddressesOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[plStudentEmploymentOld]';
--GO
--DROP TABLE dbo.plStudentEmploymentOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[plStudentEducationOld]';
--GO
--DROP TABLE dbo.plStudentEducationOld;
--GO
--IF @@ERROR <> 0
--    SET NOEXEC ON;
--GO
--PRINT N'Dropping [dbo].[syStudentContactsOld]';
--GO
--DROP TABLE dbo.syStudentContactsOld;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[arStudent]';
GO
EXEC sp_refreshview N'[dbo].[arStudent]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering [dbo].[syInstitutions]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
ALTER TABLE dbo.syInstitutions ALTER COLUMN ModDate DATETIME NOT NULL; 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[arStudAddresses]';
GO
EXEC sp_refreshview N'[dbo].[arStudAddresses]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[arStudentPhone]';
GO
EXEC sp_refreshview N'[dbo].[arStudentPhone]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[GlTransView1]';
GO
EXEC sp_refreshview N'[dbo].[GlTransView1]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[GlTransView2]';
GO
EXEC sp_refreshview N'[dbo].[GlTransView2]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_GetQuickLeadFields]';
GO
 
--================================================================================================= 
-- CREATE VIEW dbo.VIEW_GetQuickLeadFields 
--================================================================================================= 
CREATE VIEW dbo.VIEW_GetQuickLeadFields
AS
    SELECT DISTINCT
            t6.ResDefId
           ,t7.TblFldsId
           ,t3.FldId
           ,CASE WHEN t3.FldId IN (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName IN ( 'Comments','Sponsor','AdmissionsRep','AssignedDate','PreviousEducation','admincriteriaid','DateApplied',
                                             'HighSchool','AttendingHs','HighSchoolGradDate' ) ) THEN 5562
                 ELSE t1.SectionId
            END AS SectionId
           ,t3.FldName
           ,CASE WHEN t3.FldId = (
                                   SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'LeadStatus'
                                 ) THEN 1
                 ELSE t6.Required
            END AS Required
           ,t3.DDLId
           ,t3.FldLen
           ,CASE WHEN t3.FldName = 'AttendingHs' THEN 'Uniqueidentifier'
                 ELSE t8.FldType
            END AS FldType
           ,CASE WHEN t4.Caption = 'Created Date' THEN 'Date/Time'
                 WHEN t4.Caption = 'Source Category' THEN 'Category'
                 WHEN t4.Caption = 'Source Type' THEN 'Type'
                 ELSE t4.Caption
            END AS Caption
           ,t6.ControlName
           ,CASE WHEN t3.FldName = 'AttendingHs' THEN 72
                 ELSE t8.FldTypeId
            END AS FldTypeId
           ,t9.ctrlIdName
           ,t9.propName
           ,t9.parentCtrlId
           ,t9.sequence
    FROM    adQuickLeadSections t1
    JOIN    adExpQuickLeadSections t2 ON t1.SectionId = t2.SectionId
    JOIN    syFields t3 ON t2.FldId = t3.FldId
    JOIN    syFldCaptions t4 ON t3.FldId = t4.FldId
    JOIN    adLeadFields t5 ON t3.FldId = t5.LeadID
                               AND t5.LeadField = t1.SectionId
    JOIN    syTblFlds t7 ON t3.FldId = t7.FldId
    JOIN    syResTblFlds t6 ON t6.TblFldsId = t7.TblFldsId
    JOIN    syFieldTypes t8 ON t8.FldTypeId = t3.FldTypeId
    LEFT OUTER JOIN adQuickLeadMap t9 ON t9.fldName = t3.FldName
    WHERE   t7.TblId IN ( SELECT    TblId
                          FROM      dbo.syTables
                          WHERE     TblName IN ( 'syCampuses','AdLeads','adLeadPhone','syEmailType','AllNotes','adLeadAddresses','adleadPhone' ) )
            AND ResourceId = 170	--206 
            AND t7.TblFldsId NOT IN (
            SELECT  TblFldsId
            FROM    syTblFlds
            WHERE   TblId = 183
                    AND FldId IN (
                    SELECT  FldId
                    FROM    dbo.syFields
                    WHERE   FldName IN ( 'Comments','ShiftId','Nationality','PhoneStatus','WorkEmail','ForeignPhone','Address2','AddressStatus',
                                         'DegCertSeekingId' ) ) ); 
 
--================================================================================================= 
-- END  -- CREATE VIEW dbo.VIEW_GetQuickLeadFields 
--================================================================================================= 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]';
GO
EXEC sp_refreshview N'[dbo].[NewStudentSearch]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[postfinalgrades]';
GO
EXEC sp_refreshview N'[dbo].[postfinalgrades]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]';
GO
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[UnscheduledDates]';
GO
EXEC sp_refreshview N'[dbo].[UnscheduledDates]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[VIEW1]';
GO
EXEC sp_refreshview N'[dbo].[VIEW1]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering [dbo].[USP_SA_Get1098TDataForTaxYear]';
GO
  
ALTER PROCEDURE dbo.USP_SA_Get1098TDataForTaxYear
    @TaxYear AS CHAR(4)
   ,@CampusId AS UNIQUEIDENTIFIER  
  
	-------------------------------------------------------------------------------------------------  
	--Testing Purposes Only  
	--------------------------------------------------------------------------------------------------  
	-- Version Date 1.27.2017 : DE13327  
	--DECLARE @TaxYear AS CHAR(4)  
	--declare @CampusId AS UNIQUEIDENTIFIER  
    
	--SET @TaxYear='2016'   
	--SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'   
	-----------------------------------------------------------------------------------------------  
AS
    BEGIN  
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/' + CONVERT(VARCHAR(10),CONVERT(INT,@TaxYear) + 1);  
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;  
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT,@TaxYear) - 1;  
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT,@TaxYear);          
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;  
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;  
	  
        SELECT DISTINCT
                StuEnrollId
               ,t1.EnrollmentId
               ,CONVERT(CHAR(10),StartDate,101) AS StartDate
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(LDA),101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA
               ,(
                  SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se
                           ,syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status  
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(TransDate),101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate
               ,t2.FirstName
               ,t2.LastName
               ,t2.SSN
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.City,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(ss.StateCode,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                                   ,syStates ss
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.StateId = ss.StateId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode,''))
                              FROM      dbo.arStudAddresses sa2
                                       ,syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.Zip,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS TotalPaid
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A5.AwardTypeId = 1
                ) AS GrantRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                ) AS LoanRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS StudentRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 1
                            ) R
                ) AS GrantPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 2
                            ) R
                ) AS LoanPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId IN ( 3,4,5 )
                            ) R
                ) AS Scholarships
               ,(
                  SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 6
                            ) R
                ) AS StudentPayments
               ,(
                  SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS CurrentYrInstitutionalCharges
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYrInstitutionalCharges
        FROM    arStuEnrollments t1
        INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
        INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
        INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
        INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started  
            --before the end of the current year and must have attendance for the current year.  
                      (
                        t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR  
			--Student enrolled in the current year but starts in the first quarter of next year  
                      (
                        t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT,@TaxYear)
                      )
                      OR  
			--This part mostly handles schools that do not not take attendance  
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension  
			--Note that we include future starts.                                  
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7,9,10,11,20,21,22 )
                      )
                      OR  
			--Include no starts who started during the year. They will be added to the exclusions table.  
                      (
                        t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR  
			--Students who graduated during the current year and the school  
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )  
            --The student must have a record in the student addresses table  
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )  
			--Only return students with a SSN  
                AND (
                      t2.SSN IS NOT NULL
                      AND t2.SSN <> ''
                      AND t2.SSN <> '000000000'
                    )
        ORDER BY LastName;  
  
    END;  
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_AdmissionRepByCampus]';
GO
 
CREATE VIEW dbo.VIEW_AdmissionRepByCampus
AS
    SELECT  ROW_NUMBER() OVER ( ORDER BY us.FullName ) AS ID
           ,us.UserId
           ,us.FullName
           ,c.CampusId
    FROM    syUsers us
    JOIN    dbo.syUsersRolesCampGrps rcg ON rcg.UserId = us.UserId
    JOIN    syRoles roles ON roles.RoleId = rcg.RoleId
    JOIN    dbo.syCampGrps cg ON cg.CampGrpId = rcg.CampGrpId
    JOIN    dbo.syCmpGrpCmps cgc ON cgc.CampGrpId = cg.CampGrpId
    JOIN    dbo.syCampuses c ON c.CampusId = cgc.CampusId
    WHERE   roles.SysRoleId = 3
            AND us.FullName <> 'Support'
            AND us.AccountActive = 1
    GROUP BY us.UserId
           ,us.FullName
           ,c.CampusId; 
 
 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_LeadMru]';
GO
 
CREATE VIEW dbo.VIEW_LeadMru
AS
    SELECT  MRUId
           ,ChildId AS LeadId
           ,Counter
           ,MRUTypeId
           ,UserId
           ,CampusId
           ,ModUser
           ,ModDate
    FROM    dbo.syMRUS
    WHERE   ( MRUTypeId = 4 ); 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[View_LeadStatusChangesPerUser]';
GO
 
CREATE VIEW dbo.View_LeadStatusChangesPerUser
AS
    SELECT  NEWID() AS Id
           ,UserId
           ,CampusId
           ,OrigStatusId
           ,NewStatusId
    FROM    (
              SELECT DISTINCT
                        rolcam.UserId
                       ,cgc.CampusId
                       ,sta1.OrigStatusId
                       ,sta1.NewStatusId
              FROM      dbo.syLeadStatusChangePermissions AS per
              INNER JOIN dbo.syLeadStatusChanges AS sta1 ON sta1.LeadStatusChangeId = per.LeadStatusChangeId
              INNER JOIN dbo.syStatusCodes AS sc1 ON sc1.StatusCodeId = sta1.NewStatusId
              INNER JOIN dbo.syStatusCodes AS sc2 ON sc2.StatusCodeId = sta1.OrigStatusId
              INNER JOIN dbo.syRoles AS r ON r.RoleId = per.RoleId
              INNER JOIN dbo.syUsersRolesCampGrps AS rolcam ON rolcam.RoleId = r.RoleId
              INNER JOIN dbo.syCampGrps AS cg ON cg.CampGrpId = rolcam.CampGrpId
              INNER JOIN dbo.syCmpGrpCmps AS cgc ON cgc.CampGrpId = cg.CampGrpId
              INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = cgc.CampusId
              INNER JOIN dbo.syUsers AS u ON u.UserId = rolcam.UserId
              INNER JOIN dbo.syStatuses AS act ON act.StatusId = per.StatusId
              INNER JOIN dbo.syStatuses AS act1 ON act1.StatusId = cg.StatusId
              INNER JOIN dbo.syStatuses AS act2 ON act2.StatusId = camp.StatusId
              WHERE     ( act.StatusCode = 'A' )
                        AND ( act1.StatusCode = 'A' )
                        AND ( act2.StatusCode = 'A' )
            ) AS T; 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_LeadUserTask]';
GO
 
CREATE VIEW dbo.VIEW_LeadUserTask
AS
    SELECT  ut.UserTaskId
           ,ut.ReId AS LeadId
           ,task.Descrip AS Activity
           ,task.Code AS ActivityCode
           ,task.CampGroupId
           ,ut.Status
           ,ut.EndDate
           ,ut.StartDate
    FROM    dbo.tmUserTasks AS ut
    INNER JOIN dbo.adLeads AS l ON l.LeadId = ut.ReId
    INNER JOIN dbo.syStatusCodes AS sta ON sta.StatusCodeId = l.LeadStatus
    INNER JOIN dbo.tmTasks AS task ON task.TaskId = ut.TaskId
    WHERE   (
              sta.SysStatusId = 1
              OR sta.SysStatusId = 2
              OR sta.SysStatusId = 4
              OR sta.SysStatusId = 5
            )
            AND (
                  ut.Status = 0
                  OR ut.Status = 2
                ); 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[View_Messages]';
GO
 
CREATE VIEW dbo.View_Messages
AS
    SELECT  msg.MessageId
           ,msg.TemplateId
           ,msg.DeliveryType
           ,msg.DateDelivered
           ,msg.FromId
           ,msg.RecipientId
    FROM    dbo.msgMessages AS msg
    INNER JOIN dbo.adLeads AS l ON l.LeadId = msg.RecipientId; 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_SySDFModuleValue_Lead]';
GO
 
CREATE VIEW dbo.VIEW_SySDFModuleValue_Lead
AS
    SELECT  mod.SDFPKID
           ,mod.PgPKID AS LeadId
           ,mod.SDFID
           ,mod.SDFValue
           ,mod.ModUser
           ,mod.ModDate
    FROM    dbo.sySDFModuleValue mod
    JOIN    adLeads lead ON lead.LeadId = mod.PgPKID; 
 
 
 
--SELECT  mod.SDFPKID, mod.PgPKID AS LeadId, mod.SDFID, mod.SDFValue, mod.ModUser, mod.ModDate 
--FROM    dbo.sySDFModuleValue mod 
--JOIN  adLeads lead ON lead.LeadId = mod.PgPKID 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[View_TaskNotes]';
GO
 
CREATE VIEW dbo.View_TaskNotes
AS
    SELECT  ut.UserTaskId
           ,ut.ReId AS LeadId
           ,ut.Message
           ,dbo.tmTasks.Descrip AS Description
           ,ut.ModDate
           ,ut.AssignedById
    FROM    dbo.tmUserTasks AS ut
    INNER JOIN dbo.tmTasks ON dbo.tmTasks.TaskId = ut.TaskId
    INNER JOIN dbo.adLeads AS lead ON lead.LeadId = ut.ReId; 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentNotes]';
GO
EXEC sp_refreshview N'[dbo].[syStudentNotes]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContacts]';
GO
EXEC sp_refreshview N'[dbo].[syStudentContacts]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContactPhones]';
GO
EXEC sp_refreshview N'[dbo].[syStudentContactPhones]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContactAddresses]';
GO
EXEC sp_refreshview N'[dbo].[syStudentContactAddresses]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentSkills]';
GO
EXEC sp_refreshview N'[dbo].[plStudentSkills]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentExtraCurriculars]';
GO
EXEC sp_refreshview N'[dbo].[plStudentExtraCurriculars]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering [dbo].[Sp_UnenrollLeads]';
GO
ALTER PROCEDURE dbo.Sp_UnenrollLeads
    @leadid UNIQUEIDENTIFIER
   ,@leadstatus UNIQUEIDENTIFIER
AS
    BEGIN  
        DECLARE @stuenrollid UNIQUEIDENTIFIER
           ,@studentid UNIQUEIDENTIFIER
           ,@studentawardid UNIQUEIDENTIFIER;  
        DECLARE @paymentplanid UNIQUEIDENTIFIER
           ,@oldtransactionid UNIQUEIDENTIFIER
           ,@oldstudentawardid UNIQUEIDENTIFIER;  
        DECLARE @counter INT
           ,@timer INT
           ,@@transactionid VARCHAR(8000)
           ,@@studentawardid VARCHAR(8000);  
        DECLARE @@paymentawardid VARCHAR(8000)
           ,@oldpaymentplanid VARCHAR(8000)
           ,@@paymentplanid VARCHAR(8000);  
        DECLARE @errornumber INT
           ,@TransCount INT
           ,@StudentAwardCount INT
           ,@PaymentPlanCount INT;  
  
          
        SET @stuenrollid = (
                             SELECT DISTINCT
                                    StuEnrollId
                             FROM   arStuEnrollments
                             WHERE  LeadId = @leadid
                           );  
        SET @studentid = (
                           SELECT DISTINCT
                                    StudentId
                           FROM     arStuEnrollments
                           WHERE    StuEnrollId = @stuenrollid
                         );  
  
-- Begin Section 1   
--        delete from adEntrTestOverRide where LeadId=@leadid  
--        delete from adLeadByLeadGroups where LeadId=@leadid  
--        delete from adLeadDocsReceived where LeadId=@leadid  
--        delete from adLeadEducation where LeadId=@leadid  
--        delete from adLeadEmployment where LeadId=@leadid  
--        delete from adLeadEntranceTest where LeadId=@leadid  
--        delete from adLeadExtraCurriculars where LeadID=@leadid  
--        delete from adLeadNotes where LeadId=@leadid  
--        delete from adLeadPhone where LeadId=@leadid  
--        delete from adLeadSkills where LeadId=@leadid  
--        delete from cmActivityAssignment where LeadId=@leadid  
--        delete from syDocumentHistory where LeadId=@leadid  
--        delete from syLeadStatusesChanges where LeadId=@leadid  
-- End Section 1   
          
  
-- Begin Section 3  
        --delete from adLeadByLeadGroups where StuEnrollId=@stuenrollid  
        UPDATE  adLeadByLeadGroups
        SET     StuEnrollId = NULL
        WHERE   StuEnrollId = @stuenrollid;            
  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arExternshipAttendance' )
            BEGIN  
                DELETE  FROM arExternshipAttendance
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arGrdBkConversionResults' )
            BEGIN  
                DELETE  FROM arGrdBkConversionResults
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arGrdBkResults' )
            BEGIN  
                DELETE  FROM arGrdBkResults
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arOverridenPreReqs' )
            BEGIN  
                DELETE  FROM arOverridenPreReqs
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arResults' )
            BEGIN  
                DELETE  FROM arResults
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arSAPChkResults' )
            BEGIN  
                DELETE  FROM arSAPChkResults
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStdSuspensions' )
            BEGIN  
                DELETE  FROM arStdSuspensions
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentClockAttendance' )
            BEGIN  
                DELETE  FROM arStudentClockAttendance
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentLOAs' )
            BEGIN  
                DELETE  FROM arStudentLOAs
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentSchedules' )
            BEGIN  
                DELETE  FROM arStudentSchedules
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentTimeClockPunches' )
            BEGIN  
                DELETE  FROM arStudentTimeClockPunches
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStuProbWarnings' )
            BEGIN  
                DELETE  FROM arStuProbWarnings
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arTrackTransfer' )
            BEGIN  
                DELETE  FROM arTrackTransfer
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arTransferGrades' )
            BEGIN  
                DELETE  FROM arTransferGrades
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atClsSectAttendance' )
            BEGIN  
                DELETE  FROM atClsSectAttendance
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentClockAttendance' )
            BEGIN  
                DELETE  FROM arStudentClockAttendance
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atConversionAttendance' )
            BEGIN  
                DELETE  FROM atConversionAttendance
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atOnLineStudents' )
            BEGIN  
                DELETE  FROM atOnLineStudents
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'ComCourseStudents' )
            BEGIN  
                DELETE  FROM ComCourseStudents
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
  
  
        SET @StudentAwardCount = (
                                   SELECT   COUNT(*)
                                   FROM     faStudentAwards
                                   WHERE    StuEnrollId = @stuenrollid
                                 );  
        IF EXISTS ( SELECT DISTINCT
                            StudentAwardId
                    FROM    faStudentAwards
                    WHERE   StuEnrollId = @stuenrollid )
            BEGIN  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPmtDisbRel' )
                    BEGIN  
                        DELETE  FROM saPmtDisbRel
                        WHERE   AwardScheduleId IN ( SELECT DISTINCT
                                                            AwardScheduleId
                                                     FROM   faStudentAwardSchedule
                                                     WHERE  StudentAwardId IN ( SELECT DISTINCT
                                                                                        StudentAwardId
                                                                                FROM    faStudentAwards
                                                                                WHERE   StuEnrollId = @stuenrollid ) );  
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStudentAwardSchedule' )
                    BEGIN  
                        DELETE  FROM faStudentAwardSchedule
                        WHERE   StudentAwardId IN ( SELECT DISTINCT
                                                            StudentAwardId
                                                    FROM    faStudentAwards
                                                    WHERE   StuEnrollId = @stuenrollid );  
                    END;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'faStudentAwards' )
            BEGIN  
                DELETE  FROM faStudentAwards
                WHERE   StuEnrollId = @stuenrollid;  
            END;          
         
  
        SET @PaymentPlanCount = (
                                  SELECT    COUNT(*)
                                  FROM      faStudentPaymentPlans
                                  WHERE     StuEnrollId = @stuenrollid
                                );  
        IF ( @PaymentPlanCount >= 1 )
            BEGIN  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPmtDisbRel' )
                    BEGIN  
                        DELETE  FROM saPmtDisbRel
                        WHERE   PayPlanScheduleId IN ( SELECT DISTINCT
                                                                PayPlanScheduleId
                                                       FROM     faStuPaymentPlanSchedule
                                                       WHERE    PaymentPlanId IN ( SELECT DISTINCT
                                                                                            PaymentPlanId
                                                                                   FROM     faStudentPaymentPlans
                                                                                   WHERE    StuEnrollId = @stuenrollid ) );  
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStuPaymentPlanSchedule' )
                    BEGIN  
                        DELETE  FROM faStuPaymentPlanSchedule
                        WHERE   PaymentPlanId IN ( SELECT DISTINCT
                                                            PaymentPlanId
                                                   FROM     faStudentPaymentPlans
                                                   WHERE    StuEnrollId = @stuenrollid );  
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStudentPaymentPlans' )
                    BEGIN  
                        DELETE  FROM faStudentPaymentPlans
                        WHERE   StuEnrollId = @stuenrollid;  
                    END;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'PlStudentsPlaced' )
            BEGIN  
                DELETE  FROM PlStudentsPlaced
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'saAdmissionDeposits' )
            BEGIN  
                DELETE  FROM saAdmissionDeposits
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        SET @TransCount = (
                            SELECT  COUNT(*)
                            FROM    saTransactions
                            WHERE   StuEnrollId = @stuenrollid
                          );  
        IF ( @TransCount >= 1 )
            BEGIN  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saDeferredRevenues' )
                    BEGIN  
                        DELETE  FROM saDeferredRevenues
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );        
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saGLDistributions' )
                    BEGIN  
                        DELETE  FROM saGLDistributions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );     
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPayments' )
                    BEGIN  
                        DELETE  FROM saPayments
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );      
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saReversedTransactions' )
                    BEGIN  
                        DELETE  FROM saReversedTransactions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );        
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saReversedTransactions' )
                    BEGIN  
                        DELETE  FROM saReversedTransactions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );    
                    END;  
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saTransactions' )
                    BEGIN  
                        DELETE  FROM saTransactions
                        WHERE   StuEnrollId = @stuenrollid;        
                    END;  
            END;  
          
         
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syStudentStatusChanges' )
            BEGIN  
                DELETE  FROM syStudentStatusChanges
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'TranscriptWithIDS2' )
            BEGIN  
                DELETE  FROM TranscriptWithIDS2
                WHERE   StuEnrollId = @stuenrollid;  
            END;  
-- End Section 3  
  
          
-- Begin Section 4  
--        delete from adEntrTestOverRide where StudentId=@studentid  
--        delete from adLeadEntranceTest where StudentId=@studentid  
--        delete from adQuickLead where StudentID=@studentid  
  
        UPDATE  adEntrTestOverRide
        SET     StudentId = NULL
        WHERE   StudentId = @studentid;  
        UPDATE  adLeadEntranceTest
        SET     StudentId = NULL
        WHERE   StudentId = @studentid;  
        UPDATE  adQuickLead
        SET     StudentID = NULL
        WHERE   StudentID = @studentid;  
		  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'adResumeObjective' )
            BEGIN  
                DELETE  FROM adResumeObjective
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arClsSectStudents' )
            BEGIN  
                DELETE  FROM arClsSectStudents
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atClsSectAttendance' )
            BEGIN  
                DELETE  FROM atClsSectAttendance
                WHERE   StudentId = @studentid;  
            END;          
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmActivityAssignment' )
            BEGIN  
                DELETE  FROM cmActivityAssignment
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmRoleEmpStudent' )
            BEGIN  
                DELETE  FROM cmRoleEmpStudent
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmStudentPhoneType' )
            BEGIN  
                DELETE  FROM cmStudentPhoneType
                WHERE   StudentId = @studentid;  
            END;  
         
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'plDocsByStudent' )
            BEGIN  
                DELETE  FROM plDocsByStudent
                WHERE   StudentId = @studentid;  
            END;  
         
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'plStudentDocs' )
            BEGIN  
                DELETE  FROM plStudentDocs
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'References' )
            BEGIN  
                DELETE  FROM [References]
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syDocumentHistory' )
            BEGIN  
                DELETE  FROM syDocumentHistory
                WHERE   StudentId = @studentid;  
            END;  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syMessages' )
            BEGIN  
                DELETE  FROM syMessages
                WHERE   StudentId = @studentid;  
            END;  
        
-- End Section 4  
  
-- Begin Section 5  
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStuEnrollments' )
            BEGIN  
                DELETE  FROM arStuEnrollments
                WHERE   StudentId = @studentid;  
            END;  
        UPDATE  adLeads
        SET     LeadStatus = @leadstatus
               ,StudentId = '00000000-0000-0000-0000-000000000000'
        WHERE   LeadId = @leadid;  
          
-- End Section 5  
    END;  
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating [dbo].[VIEW_GetHistory]';
GO
 
CREATE VIEW dbo.VIEW_GetHistory
AS
    -- 
--Status History 
    SELECT  hd.AuditHistId HistoryId
           ,'AD' ModuleCode
           ,LeadId
           ,h.EventDate Date
           ,m.Resource Module
           ,'Status' Type
           ,'From: ' + scO.StatusCodeDescrip + '  To: ' + scN.StatusCodeDescrip Description
           ,ISNULL(h.UserName,'Unknown') ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    JOIN    syAuditHistDetail hd ON l.LeadId = hd.RowId
                                    AND hd.ColumnName = 'LeadStatus'
    JOIN    syAuditHist h ON hd.AuditHistId = h.AuditHistId
                             AND TableName = 'adLeads'
    INNER JOIN syStatusCodes AS scN ON hd.NewValue = scN.StatusCodeId
    INNER JOIN sySysStatus AS ssN ON scN.SysStatusId = ssN.SysStatusId
    INNER JOIN syStatusCodes AS scO ON hd.OldValue = scO.StatusCodeId
    INNER JOIN syAdvantageResourceRelations rr ON ssN.StatusLevelId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    WHERE   m.ResourceID = 189  
									--See "Entity, Module, Entity-Module tables"  comment below 
-- 
--Interview, Event, Tour History 
    UNION ALL
    SELECT  rr.LeadReqId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,rr.ModDate Date
           ,m.ModuleName Module
           ,rt.Descrip Type
           ,r.Descrip Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER JOIN AdLeadReqsReceived rr ON l.LeadId = rr.LeadId
    INNER JOIN adReqs r ON rr.DocumentId = r.adReqId
    INNER JOIN adReqTypes rt ON r.adReqTypeId = rt.adReqTypeId
                                AND rt.adReqTypeId BETWEEN 4 AND 6 -- Interview, Event, Tour  
    INNER JOIN syModules m ON r.ModuleId = m.ModuleID
                              AND m.ModuleID = 2
    LEFT JOIN syUsers u ON rr.ModUser = u.UserName 
-- 
--Fee History 
    UNION ALL
    SELECT  t.TransactionId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,t.ModDate Date
           ,m.ModuleName Module
           ,rt.Descrip Type
           ,r.Descrip + CASE WHEN Voided = 1 THEN ' (Voided)'
                             ELSE ''
                        END Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER  JOIN adLeadTransactions t ON t.LeadId = l.LeadId
    INNER JOIN adReqs r ON t.LeadRequirementId = r.adReqId
    INNER JOIN adReqTypes rt ON r.adReqTypeId = rt.adReqTypeId
                                AND rt.adReqTypeId = 7 --  Fee 
    INNER JOIN saTransTypes ty ON t.TransTypeId = ty.TransTypeId
                                  AND ty.Description = 'Payment'
    INNER JOIN syModules m ON r.ModuleId = m.ModuleID
                              AND m.ModuleID = 2
    LEFT JOIN syUsers u ON t.ModUser = u.UserName 
-- 
-- Task History 
    UNION ALL
    SELECT  ut.UserTaskId HistoryId
           ,'AD' ModuleCode
           ,l.LeadId
           ,ut.ModDate Date
           ,m.Resource Module
           ,'Task/Appointment' Type
           ,t.Descrip Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER JOIN tmUserTasks ut ON l.LeadId = ut.ReId
    INNER JOIN tmTasks t ON ut.TaskId = t.TaskId
    INNER JOIN syAdvantageResourceRelations rr ON t.ModuleEntityId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    LEFT JOIN syUsers u ON ut.ModUser = u.UserId
    WHERE   m.ResourceID = 189   
									--See "Entity, Module, Entity-Module tables"  comment below 
-- 
-- Note History 
    UNION ALL
    SELECT  n.UserId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,n.ModDate Date
           ,m.ModuleName Module
           ,'Note' Type
           ,n.NoteText Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    JOIN    adLead_Notes ln ON l.LeadId = ln.LeadId
    JOIN    AllNotes n ON ln.NotesId = n.NotesId
                          AND NoteType != 'Confidential'
    INNER JOIN syModules m ON n.ModuleCode = m.ModuleCode
    LEFT JOIN syUsers u ON n.ModUser = u.UserName 
-- 
--Email History 
    UNION ALL
    SELECT  ms.MessageId HistoryId
           ,'AD' ModuleCode
           ,l.LeadId
           ,ms.ModDate Date
           ,m.Resource Module
           ,'Email' Type
           ,mt.Descrip Description
           ,u.FullName ModUser
           ,ms.MsgContent AdditionalContent
    FROM    adLeads l
    INNER JOIN msgMessages ms ON l.LeadId = ms.RecipientId
    INNER JOIN msgTemplates mt ON ms.TemplateId = mt.TemplateId
    INNER JOIN syAdvantageResourceRelations rr ON mt.ModuleEntityId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    LEFT JOIN syUsers u ON ms.ModUser = u.UserId
    WHERE   m.ResourceID = 189
            AND DeliveryType = 'Email'; 
 
-------Entity, Module, Entity-Module tables comment 
 
		--There are 2 Groups of Entity, Module, Entity-Module tables. 
		-- Many tables (Task maybe Audit) are associated with Modules via the Modules represented in syResources where ResourceTypeID = 1 
		-- This is seperate and not fully consistant and not joinable(/easily mappable)  with the Modules  represented in syModules 
		-- In the "2 groups of Entity, Module, Entity-Module tables" comment (in US8105) are the tables that need to be bridged/mapped  
		-- however for the current assignment (US8105) it suffices to address just the Admissions module 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_AdLeadEmail_LeadEMailId] on [dbo].[AdLeadEmail]';
GO
ALTER TABLE dbo.AdLeadEmail ADD CONSTRAINT PK_AdLeadEmail_LeadEMailId PRIMARY KEY CLUSTERED  (LeadEMailId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_AdLeadReqsReceived_LeadReqId] on [dbo].[AdLeadReqsReceived]';
GO
ALTER TABLE dbo.AdLeadReqsReceived ADD CONSTRAINT PK_AdLeadReqsReceived_LeadReqId PRIMARY KEY CLUSTERED  (LeadReqId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_AdVehicles_VehicleId] on [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles ADD CONSTRAINT PK_AdVehicles_VehicleId PRIMARY KEY CLUSTERED  (VehicleId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_AllNotes_NotesId] on [dbo].[AllNotes]';
GO
ALTER TABLE dbo.AllNotes ADD CONSTRAINT PK_AllNotes_NotesId PRIMARY KEY CLUSTERED  (NotesId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_PriorWorkAddress_Id] on [dbo].[PriorWorkAddress]';
GO
ALTER TABLE dbo.PriorWorkAddress ADD CONSTRAINT PK_PriorWorkAddress_Id PRIMARY KEY CLUSTERED  (Id);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_PriorWorkContact_Id] on [dbo].[PriorWorkContact]';
GO
ALTER TABLE dbo.PriorWorkContact ADD CONSTRAINT PK_PriorWorkContact_Id PRIMARY KEY CLUSTERED  (Id);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adExtraCurricular_IdExtraCurricular] on [dbo].[adExtraCurricular]';
GO
ALTER TABLE dbo.adExtraCurricular ADD CONSTRAINT PK_adExtraCurricular_IdExtraCurricular PRIMARY KEY CLUSTERED  (IdExtraCurricular);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLeadAddresses_adLeadAddressId] on [dbo].[adLeadAddresses]';
GO
ALTER TABLE dbo.adLeadAddresses ADD CONSTRAINT PK_adLeadAddresses_adLeadAddressId PRIMARY KEY CLUSTERED  (adLeadAddressId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLeadDuplicates_IdDuplicates] on [dbo].[adLeadDuplicates]';
GO
ALTER TABLE dbo.adLeadDuplicates ADD CONSTRAINT PK_adLeadDuplicates_IdDuplicates PRIMARY KEY CLUSTERED  (IdDuplicates);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLeadOtherContactsAddreses_OtherContactsAddresesId] on [dbo].[adLeadOtherContactsAddreses]';
GO
ALTER TABLE dbo.adLeadOtherContactsAddreses ADD CONSTRAINT PK_adLeadOtherContactsAddreses_OtherContactsAddresesId PRIMARY KEY CLUSTERED  (OtherContactsAddresesId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLeadOtherContactsEmail_OtherContactsEmailId] on [dbo].[adLeadOtherContactsEmail]';
GO
ALTER TABLE dbo.adLeadOtherContactsEmail ADD CONSTRAINT PK_adLeadOtherContactsEmail_OtherContactsEmailId PRIMARY KEY CLUSTERED  (OtherContactsEmailId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLeadTranReceived_adLeadTranReceivedId] on [dbo].[adLeadTranReceived]';
GO
ALTER TABLE dbo.adLeadTranReceived ADD CONSTRAINT PK_adLeadTranReceived_adLeadTranReceivedId PRIMARY KEY CLUSTERED  (adLeadTranReceivedId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLead_Notes_id] on [dbo].[adLead_Notes]';
GO
ALTER TABLE dbo.adLead_Notes ADD CONSTRAINT PK_adLead_Notes_id PRIMARY KEY CLUSTERED  (id);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adLevel_LevelId] on [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel ADD CONSTRAINT PK_adLevel_LevelId PRIMARY KEY CLUSTERED  (LevelId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adSkills_SkillId] on [dbo].[adSkills]';
GO
ALTER TABLE dbo.adSkills ADD CONSTRAINT PK_adSkills_SkillId PRIMARY KEY CLUSTERED  (SkillId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adVendorCampaign_CampaignId] on [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign ADD CONSTRAINT PK_adVendorCampaign_CampaignId PRIMARY KEY CLUSTERED  (CampaignId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adVendorPayFor_IdPayFor] on [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor ADD CONSTRAINT PK_adVendorPayFor_IdPayFor PRIMARY KEY CLUSTERED  (IdPayFor);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_adVendors_VendorId] on [dbo].[adVendors]';
GO
ALTER TABLE dbo.adVendors ADD CONSTRAINT PK_adVendors_VendorId PRIMARY KEY CLUSTERED  (VendorId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_syEmailType_EMailTypeId] on [dbo].[syEmailType]';
GO
ALTER TABLE dbo.syEmailType ADD CONSTRAINT PK_syEmailType_EMailTypeId PRIMARY KEY CLUSTERED  (EMailTypeId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_syInstitutionContacts_InstitutionContactId] on [dbo].[syInstitutionContacts]';
GO
ALTER TABLE dbo.syInstitutionContacts ADD CONSTRAINT PK_syInstitutionContacts_InstitutionContactId PRIMARY KEY CLUSTERED  (InstitutionContactId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_syInstitutionImportTypes_ImportTypeID] on [dbo].[syInstitutionImportTypes]';
GO
ALTER TABLE dbo.syInstitutionImportTypes ADD CONSTRAINT PK_syInstitutionImportTypes_ImportTypeID PRIMARY KEY CLUSTERED  (ImportTypeID);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_syInstitutionTypes_TypeID] on [dbo].[syInstitutionTypes]';
GO
ALTER TABLE dbo.syInstitutionTypes ADD CONSTRAINT PK_syInstitutionTypes_TypeID PRIMARY KEY CLUSTERED  (TypeID);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating primary key [PK_syNotesPageFields_PageFieldId] on [dbo].[syNotesPageFields]';
GO
ALTER TABLE dbo.syNotesPageFields ADD CONSTRAINT PK_syNotesPageFields_PageFieldId PRIMARY KEY CLUSTERED  (PageFieldId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles ADD CONSTRAINT UIX_AdVehicles_LeadId_Position UNIQUE NONCLUSTERED  (LeadId, Position);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_adAdvInterval_AdvIntervalCode_AdvIntervalDescrip] on [dbo].[adAdvInterval]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_adAdvInterval_AdvIntervalCode_AdvIntervalDescrip ON dbo.adAdvInterval (AdvIntervalCode, AdvIntervalDescrip);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_adEntrTestOverRide_EntrTestId_StudentId_OverRide] on [dbo].[adEntrTestOverRide]';
GO
CREATE NONCLUSTERED INDEX IX_adEntrTestOverRide_EntrTestId_StudentId_OverRide ON dbo.adEntrTestOverRide (EntrTestId, StudentId, OverRide);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_adLeadByLeadGroups_StuEnrollId_LeadGrpId] on [dbo].[adLeadByLeadGroups]';
GO
CREATE NONCLUSTERED INDEX IX_adLeadByLeadGroups_StuEnrollId_LeadGrpId ON dbo.adLeadByLeadGroups (StuEnrollId, LeadGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adLeadOtherContacts]';
GO
ALTER TABLE dbo.adLeadOtherContacts ADD CONSTRAINT UIX_adLeadOtherContacts_OtherContactId UNIQUE NONCLUSTERED  (OtherContactId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_adLeads_CampusId_LeadId_ModDate] on [dbo].[adLeads]';
GO
CREATE NONCLUSTERED INDEX IX_adLeads_CampusId_LeadId_ModDate ON dbo.adLeads (CampusId) INCLUDE (LeadId, ModDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_adLeads_StudentId] on [dbo].[adLeads]';
GO
CREATE NONCLUSTERED INDEX IX_adLeads_StudentId ON dbo.adLeads (StudentId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_adLeads_StudentId_ExcludeStudentIdWithZeros] on [dbo].[adLeads]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_adLeads_StudentId_ExcludeStudentIdWithZeros ON dbo.adLeads (StudentId) WHERE (StudentId<>'00000000-0000-0000-0000-000000000000');
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_adLeads_StudentNumber] on [dbo].[adLeads]';
GO
CREATE NONCLUSTERED INDEX IX_adLeads_StudentNumber ON dbo.adLeads (StudentNumber);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip] on [dbo].[adSourceCatagory]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip ON dbo.adSourceCatagory (SourceCatagoryCode, SourceCatagoryDescrip);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_adSourceType_SourceTypeCode_SourceTypeDescrip] on [dbo].[adSourceType]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_adSourceType_SourceTypeCode_SourceTypeDescrip ON dbo.adSourceType (SourceTypeCode, SourceTypeDescrip);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign ADD CONSTRAINT UIX_adVendorCampaign_CampaignCode_VendorId UNIQUE NONCLUSTERED  (CampaignCode, VendorId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign ADD CONSTRAINT UIX_adVendorCampaign_CampaignId UNIQUE NONCLUSTERED  (CampaignId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor ADD CONSTRAINT UIX_adVendorPayFor_IdPayFor UNIQUE NONCLUSTERED  (IdPayFor);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adVendorPayFor]';
GO
ALTER TABLE dbo.adVendorPayFor ADD CONSTRAINT UIX_adVendorPayFor_PayForCode UNIQUE NONCLUSTERED  (PayForCode);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adVendors]';
GO
ALTER TABLE dbo.adVendors ADD CONSTRAINT UIX_adVendors_VendorCode UNIQUE NONCLUSTERED  (VendorCode);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arClassSections_ReqId_TermId_ClsSectionId_InstrGrdBkWgtId_GrdScaleId] on [dbo].[arClassSections]';
GO
CREATE NONCLUSTERED INDEX IX_arClassSections_ReqId_TermId_ClsSectionId_InstrGrdBkWgtId_GrdScaleId ON dbo.arClassSections (ReqId, TermId, ClsSectionId, InstrGrdBkWgtId, GrdScaleId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arClsSectMeetings_ClsSectionId_StartDate_EndDate_PeriodId] on [dbo].[arClsSectMeetings]';
GO
CREATE NONCLUSTERED INDEX IX_arClsSectMeetings_ClsSectionId_StartDate_EndDate_PeriodId ON dbo.arClsSectMeetings (ClsSectionId, StartDate, EndDate, PeriodId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkConversionResults_StuEnrollId_ReqId_TermId_GrdComponentTypeId_ConversionResultId_MinResult_Score] on [dbo].[arGrdBkConversionResults]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkConversionResults_StuEnrollId_ReqId_TermId_GrdComponentTypeId_ConversionResultId_MinResult_Score ON dbo.arGrdBkConversionResults (StuEnrollId, ReqId, TermId, GrdComponentTypeId, ConversionResultId) INCLUDE (MinResult, Score);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkResults_ClsSectionId_GrdBkResultId_InstrGrdBkWgtDetailId_StuEnrollId_Score] on [dbo].[arGrdBkResults]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkResults_ClsSectionId_GrdBkResultId_InstrGrdBkWgtDetailId_StuEnrollId_Score ON dbo.arGrdBkResults (ClsSectionId, GrdBkResultId, InstrGrdBkWgtDetailId, StuEnrollId) INCLUDE (Score);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkResults_Score_ClsSectionId_InstrGrdBkWgtDetailId] on [dbo].[arGrdBkResults]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkResults_Score_ClsSectionId_InstrGrdBkWgtDetailId ON dbo.arGrdBkResults (Score) INCLUDE (ClsSectionId, InstrGrdBkWgtDetailId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkResults_StuEnrollId_ClsSectionId] on [dbo].[arGrdBkResults]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkResults_StuEnrollId_ClsSectionId ON dbo.arGrdBkResults (StuEnrollId, ClsSectionId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_arGrdBkResults_StuEnrollId_InstrGrdBkWgtDetailId_ClsSectionId_ResNum_PostDate] on [dbo].[arGrdBkResults]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_arGrdBkResults_StuEnrollId_InstrGrdBkWgtDetailId_ClsSectionId_ResNum_PostDate ON dbo.arGrdBkResults (StuEnrollId, InstrGrdBkWgtDetailId, ClsSectionId, ResNum, PostDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkWgtDetails_GrdComponentTypeId_Required_MustPass_InstrGrdBkWgtDetailId_Number] on [dbo].[arGrdBkWgtDetails]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkWgtDetails_GrdComponentTypeId_Required_MustPass_InstrGrdBkWgtDetailId_Number ON dbo.arGrdBkWgtDetails (GrdComponentTypeId, Required, MustPass) INCLUDE (InstrGrdBkWgtDetailId, Number);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdBkWgtDetails_InstrGrdBkWgtId_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number] on [dbo].[arGrdBkWgtDetails]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdBkWgtDetails_InstrGrdBkWgtId_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number ON dbo.arGrdBkWgtDetails (InstrGrdBkWgtId, InstrGrdBkWgtDetailId, GrdComponentTypeId) INCLUDE (Number);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arGrdComponentTypes_SysComponentTypeId_GrdComponentTypeId_Descrip] on [dbo].[arGrdComponentTypes]';
GO
CREATE NONCLUSTERED INDEX IX_arGrdComponentTypes_SysComponentTypeId_GrdComponentTypeId_Descrip ON dbo.arGrdComponentTypes (SysComponentTypeId, GrdComponentTypeId, Descrip);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arReqs_ReqId_Descrip_Code_Credits_FinAidCredits] on [dbo].[arReqs]';
GO
CREATE NONCLUSTERED INDEX IX_arReqs_ReqId_Descrip_Code_Credits_FinAidCredits ON dbo.arReqs (ReqId, Descrip, Code, Credits, FinAidCredits);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arResults_GrdSysDetailId_ResultId_StuEnrollId_TestId_Score] on [dbo].[arResults]';
GO
CREATE NONCLUSTERED INDEX IX_arResults_GrdSysDetailId_ResultId_StuEnrollId_TestId_Score ON dbo.arResults (GrdSysDetailId, ResultId, StuEnrollId, TestId) INCLUDE (Score);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arResults_StuEnrollId_TestId] on [dbo].[arResults]';
GO
CREATE NONCLUSTERED INDEX IX_arResults_StuEnrollId_TestId ON dbo.arResults (StuEnrollId, TestId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arResults_StuEnrollId_TestId_Score] on [dbo].[arResults]';
GO
CREATE NONCLUSTERED INDEX IX_arResults_StuEnrollId_TestId_Score ON dbo.arResults (StuEnrollId, TestId, Score);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arResults_TestId_GrdSysDetailId_StuEnrollId_Score] on [dbo].[arResults]';
GO
CREATE NONCLUSTERED INDEX IX_arResults_TestId_GrdSysDetailId_StuEnrollId_Score ON dbo.arResults (TestId, GrdSysDetailId, StuEnrollId, Score);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_CampusId_ModDate_StudentId] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_CampusId_ModDate_StudentId ON dbo.arStuEnrollments (CampusId) INCLUDE (ModDate, StudentId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_PrgVerId_ShiftId_StatusCodeId_StudentId_StartDate] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_PrgVerId_ShiftId_StatusCodeId_StudentId_StartDate ON dbo.arStuEnrollments (PrgVerId, ShiftId, StatusCodeId, StudentId) INCLUDE (StartDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_ShiftId_StatusCodeId_StudentId_PrgVerId] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_ShiftId_StatusCodeId_StudentId_PrgVerId ON dbo.arStuEnrollments (ShiftId, StatusCodeId, StudentId, PrgVerId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_StatusCodeId_StartDate_CampusId_EdLvlId_degcertseekingid_StudentId_PrgVerId_StuEnrollId_ExpGradDate] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_StatusCodeId_StartDate_CampusId_EdLvlId_degcertseekingid_StudentId_PrgVerId_StuEnrollId_ExpGradDate ON dbo.arStuEnrollments (StatusCodeId, StartDate, CampusId, EdLvlId, degcertseekingid, StudentId, PrgVerId, StuEnrollId) INCLUDE (ExpGradDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId ON dbo.arStuEnrollments (StudentId, StatusCodeId, ShiftId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId_StuEnrollId_PrgVerId_StartDate] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId_StuEnrollId_PrgVerId_StartDate ON dbo.arStuEnrollments (StudentId, StatusCodeId, ShiftId, StuEnrollId, PrgVerId) INCLUDE (StartDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId ON dbo.arStuEnrollments (StuEnrollId) INCLUDE (CampusId, PrgVerId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId_StudentId] on [dbo].[arStuEnrollments]';
GO
CREATE NONCLUSTERED INDEX IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId_StudentId ON dbo.arStuEnrollments (StuEnrollId) INCLUDE (CampusId, PrgVerId, StudentId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStudentClockAttendance_StuEnrollId_isTardy_RecordDate] on [dbo].[arStudentClockAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_arStudentClockAttendance_StuEnrollId_isTardy_RecordDate ON dbo.arStudentClockAttendance (StuEnrollId, isTardy, RecordDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arStudentClockAttendance_StuEnrollId_RecordDate_ActualHours_SchedHours_isTardy] on [dbo].[arStudentClockAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_arStudentClockAttendance_StuEnrollId_RecordDate_ActualHours_SchedHours_isTardy ON dbo.arStudentClockAttendance (StuEnrollId, RecordDate, ActualHours, SchedHours, isTardy);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arTerm_TermId_StartDate_TermDescrip_EndDate] on [dbo].[arTerm]';
GO
CREATE NONCLUSTERED INDEX IX_arTerm_TermId_StartDate_TermDescrip_EndDate ON dbo.arTerm (TermId, StartDate, TermDescrip, EndDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_arTransferGrades_StuEnrollId_TransferId_TermId_ReqId] on [dbo].[arTransferGrades]';
GO
CREATE NONCLUSTERED INDEX IX_arTransferGrades_StuEnrollId_TransferId_TermId_ReqId ON dbo.arTransferGrades (StuEnrollId, TransferId, TermId, ReqId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_StuEnrollId_Actual_MeetDate] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_StuEnrollId_Actual_MeetDate ON dbo.atClsSectAttendance (StuEnrollId) INCLUDE (Actual, MeetDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_StuEnrollId_Actual_ClsSectionId_MeetDate_ClsSectAttId] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_StuEnrollId_Actual_ClsSectionId_MeetDate_ClsSectAttId ON dbo.atClsSectAttendance (StuEnrollId, Actual, ClsSectionId, MeetDate) INCLUDE (ClsSectAttId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_StuEnrollId_Actual_Excused_MeetDate] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_StuEnrollId_Actual_Excused_MeetDate ON dbo.atClsSectAttendance (StuEnrollId, Actual, Excused, MeetDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_StuEnrollId_MeetDate_Actual_Excused_Tardy] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_StuEnrollId_MeetDate_Actual_Excused_Tardy ON dbo.atClsSectAttendance (StuEnrollId, MeetDate, Actual, Excused, Tardy);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_StuEnrollId_MeetDate_ClsSectAttId_ClsSectionId_Actual_Tardy_Excused] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_StuEnrollId_MeetDate_ClsSectAttId_ClsSectionId_Actual_Tardy_Excused ON dbo.atClsSectAttendance (StuEnrollId, MeetDate, ClsSectAttId, ClsSectionId, Actual, Tardy, Excused);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_atClsSectAttendance_Tardy_Actual_StuEnrollId_MeetDate] on [dbo].[atClsSectAttendance]';
GO
CREATE NONCLUSTERED INDEX IX_atClsSectAttendance_Tardy_Actual_StuEnrollId_MeetDate ON dbo.atClsSectAttendance (Tardy, Actual, StuEnrollId, MeetDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_plAddressTypes_AddressCode_AddressDescrip_CampGrpId] on [dbo].[plAddressTypes]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_plAddressTypes_AddressCode_AddressDescrip_CampGrpId ON dbo.plAddressTypes (AddressCode, AddressDescrip, CampGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_plStudentDocs_StudentId_DocStatusId_DocumentId] on [dbo].[plStudentDocs]';
GO
CREATE NONCLUSTERED INDEX IX_plStudentDocs_StudentId_DocStatusId_DocumentId ON dbo.plStudentDocs (StudentId, DocStatusId) INCLUDE (DocumentId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_saBankAccounts_BankAcctDescrip_StatusId_CampGrpId] on [dbo].[saBankAccounts]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_saBankAccounts_BankAcctDescrip_StatusId_CampGrpId ON dbo.saBankAccounts (BankAcctDescrip, StatusId, CampGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_saBankAccounts_BankAcctNumber_StatusId_CampGrpId] on [dbo].[saBankAccounts]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_saBankAccounts_BankAcctNumber_StatusId_CampGrpId ON dbo.saBankAccounts (BankAcctNumber, StatusId, CampGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_saBankAccounts_BankRoutingNumber_BankAcctNumber] on [dbo].[saBankAccounts]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_saBankAccounts_BankRoutingNumber_BankAcctNumber ON dbo.saBankAccounts (BankRoutingNumber, BankAcctNumber);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_saTransactions_TransTypeId_StuEnrollId_IsPosted_Voided_TransactionId_TransAmount_TransDate] on [dbo].[saTransactions]';
GO
CREATE NONCLUSTERED INDEX IX_saTransactions_TransTypeId_StuEnrollId_IsPosted_Voided_TransactionId_TransAmount_TransDate ON dbo.saTransactions (TransTypeId, StuEnrollId, IsPosted, Voided, TransactionId, TransAmount, TransDate);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_saTransactions_TransTypeId_StuEnrollId_TransDate_Voided_TransactionId_TransCodeId_TransAmount] on [dbo].[saTransactions]';
GO
CREATE NONCLUSTERED INDEX IX_saTransactions_TransTypeId_StuEnrollId_TransDate_Voided_TransactionId_TransCodeId_TransAmount ON dbo.saTransactions (TransTypeId, StuEnrollId, TransDate, Voided, TransactionId, TransCodeId) INCLUDE (TransAmount);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_saTransactions_TransTypeId_StuEnrollId_Voided_TransDate_TransactionId_TermId_TransCodeId_TransAmount] on [dbo].[saTransactions]';
GO
CREATE NONCLUSTERED INDEX IX_saTransactions_TransTypeId_StuEnrollId_Voided_TransDate_TransactionId_TermId_TransCodeId_TransAmount ON dbo.saTransactions (TransTypeId, StuEnrollId, Voided, TransDate, TransactionId, TermId, TransCodeId) INCLUDE (TransAmount);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syAuditHistDetail_RowId_AuditHistId] on [dbo].[syAuditHistDetail]';
GO
CREATE NONCLUSTERED INDEX IX_syAuditHistDetail_RowId_AuditHistId ON dbo.syAuditHistDetail (RowId, AuditHistId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA ON dbo.syCreditSummary (StuEnrollId) INCLUDE (Count_SimpleAverage_Credits, Product_SimpleAverage_Credits_GPA);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA_TermId] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA_TermId ON dbo.syCreditSummary (StuEnrollId) INCLUDE (Count_SimpleAverage_Credits, Product_SimpleAverage_Credits_GPA, TermId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA ON dbo.syCreditSummary (StuEnrollId) INCLUDE (Count_WeightedAverage_Credits, Product_WeightedAverage_Credits_GPA);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_Completed_TermStartDate_TermId_FinalScore] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_Completed_TermStartDate_TermId_FinalScore ON dbo.syCreditSummary (StuEnrollId, Completed, TermStartDate, TermId) INCLUDE (FinalScore);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_TermId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_TermId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA ON dbo.syCreditSummary (StuEnrollId, TermId) INCLUDE (Count_WeightedAverage_Credits, Product_WeightedAverage_Credits_GPA);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_SimpleAverage_Credits_Product_SimpleAverage_Credits_GPA ON dbo.syCreditSummary (StuEnrollId, TermId, ReqId) INCLUDE (Count_SimpleAverage_Credits, Product_SimpleAverage_Credits_GPA);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_TermId_ReqId_Count_WeightedAverage_Credits_Product_WeightedAverage_Credits_GPA ON dbo.syCreditSummary (StuEnrollId, TermId, ReqId) INCLUDE (Count_WeightedAverage_Credits, Product_WeightedAverage_Credits_GPA);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syCreditSummary_StuEnrollId_TermStartDate_TermId_Completed_coursecredits_CreditsEarned_FACreditsEarned_FinalGPA_FinalScore] on [dbo].[syCreditSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syCreditSummary_StuEnrollId_TermStartDate_TermId_Completed_coursecredits_CreditsEarned_FACreditsEarned_FinalGPA_FinalScore ON dbo.syCreditSummary (StuEnrollId, TermStartDate, TermId) INCLUDE (Completed, coursecredits, CreditsEarned, FACreditsEarned, FinalGPA, FinalScore);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_syFamilyIncome_FamilyIncomeCode_FamilyIncomeDescrip_CampGrpId] on [dbo].[syFamilyIncome]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_syFamilyIncome_FamilyIncomeCode_FamilyIncomeDescrip_CampGrpId ON dbo.syFamilyIncome (FamilyIncomeCode, FamilyIncomeDescrip, CampGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_syHolidays_StatusId_HolidayStartDate_CampGrpId] on [dbo].[syHolidays]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_syHolidays_StatusId_HolidayStartDate_CampGrpId ON dbo.syHolidays (StatusId, HolidayStartDate, CampGrpId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [UIX_syLeadStatusChanges_CampGrpId_OrigStatusId_NewStatusId] on [dbo].[syLeadStatusChanges]';
GO
CREATE UNIQUE NONCLUSTERED INDEX UIX_syLeadStatusChanges_CampGrpId_OrigStatusId_NewStatusId ON dbo.syLeadStatusChanges (CampGrpId, OrigStatusId, NewStatusId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[syModules]';
GO
ALTER TABLE dbo.syModules ADD CONSTRAINT UIX_syModules_ModuleCode UNIQUE NONCLUSTERED  (ModuleCode);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syPeriods_PeriodId_StartTimeId_EndTimeId_PeriodDescrip] on [dbo].[syPeriods]';
GO
CREATE NONCLUSTERED INDEX IX_syPeriods_PeriodId_StartTimeId_EndTimeId_PeriodDescrip ON dbo.syPeriods (PeriodId, StartTimeId, EndTimeId) INCLUDE (PeriodDescrip);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[syResourceSdf]';
GO
ALTER TABLE dbo.syResourceSdf ADD CONSTRAINT UIX_syResourceSdf_ResourceId_sdfID_EntityId UNIQUE NONCLUSTERED  (ResourceId, sdfID, EntityId);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syResources_ResourceTypeID_ResourceID_Resource] on [dbo].[syResources]';
GO
CREATE NONCLUSTERED INDEX IX_syResources_ResourceTypeID_ResourceID_Resource ON dbo.syResources (ResourceTypeID, ResourceID, Resource);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualAbsentDays_ConvertTo_Hours] on [dbo].[syStudentAttendanceSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualAbsentDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary (StuEnrollId, TermStartDate, TermId) INCLUDE (ActualAbsentDays_ConvertTo_Hours);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualPresentDays_ConvertTo_Hours] on [dbo].[syStudentAttendanceSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualPresentDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary (StuEnrollId, TermStartDate, TermId) INCLUDE (ActualPresentDays_ConvertTo_Hours);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating index [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualTardyDays_ConvertTo_Hours] on [dbo].[syStudentAttendanceSummary]';
GO
CREATE NONCLUSTERED INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualTardyDays_ConvertTo_Hours ON dbo.syStudentAttendanceSummary (StuEnrollId, TermStartDate, TermId) INCLUDE (ActualTardyDays_ConvertTo_Hours);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules ADD CONSTRAINT UIX_syUniversalSearchModules_Code UNIQUE NONCLUSTERED  (Code);
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adLeadOtherContactsPhone]';
GO
ALTER TABLE dbo.adLeadOtherContactsPhone ADD CONSTRAINT DF_adLeadOtherContactsPhone_IsForeignPhone DEFAULT ((0)) FOR IsForeignPhone;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adLeads]';
GO
ALTER TABLE dbo.adLeads ADD CONSTRAINT DF_adLeads_PreferredContactId DEFAULT ((1)) FOR PreferredContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel ADD CONSTRAINT DF_adLevel_StatusId DEFAULT ('F23DE1E2-D90A-4720-B4C7-0F6FB09C9965') FOR StatusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[syInstitutionContacts]';
GO
ALTER TABLE dbo.syInstitutionContacts ADD CONSTRAINT DF_syInstitutionContacts_InstitutionContactId DEFAULT (NEWID()) FOR InstitutionContactId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding constraints to [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules ADD CONSTRAINT DF_syUniversalSearchModules_ModifiedDate DEFAULT (GETDATE()) FOR ModifiedDate;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[PriorWorkAddress]';
GO
ALTER TABLE dbo.PriorWorkAddress ADD CONSTRAINT FK_PriorWorkAddress_adLeadEmployment_StEmploymentId_StEmploymentId FOREIGN KEY (StEmploymentId) REFERENCES dbo.adLeadEmployment (StEmploymentId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[PriorWorkContact]';
GO
ALTER TABLE dbo.PriorWorkContact ADD CONSTRAINT FK_PriorWorkContact_adLeadEmployment_StEmploymentId_StEmploymentId FOREIGN KEY (StEmploymentId) REFERENCES dbo.adLeadEmployment (StEmploymentId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeadDuplicates]';
GO
ALTER TABLE dbo.adLeadDuplicates ADD CONSTRAINT FK_adLeadDuplicates_adLeads_NewLeadGuid_LeadId FOREIGN KEY (NewLeadGuid) REFERENCES dbo.adLeads (LeadId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeadEmployment]';
GO
ALTER TABLE dbo.adLeadEmployment ADD CONSTRAINT FK_adLeadEmployment_adLeads_LeadId_LeadId FOREIGN KEY (LeadId) REFERENCES dbo.adLeads (LeadId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeadOtherContactsPhone]';
GO
ALTER TABLE dbo.adLeadOtherContactsPhone ADD CONSTRAINT FK_adLeadOtherContactsPhone_adLeadOtherContacts_OtherContactId_OtherContactId FOREIGN KEY (OtherContactId) REFERENCES dbo.adLeadOtherContacts (OtherContactId); 
GO
ALTER TABLE dbo.adLeadOtherContactsPhone ADD CONSTRAINT FK_adLeadOtherContactsPhone_syStatuses_StatusId_StatusId FOREIGN KEY (StatusId) REFERENCES dbo.syStatuses (StatusId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLevel]';
GO
ALTER TABLE dbo.adLevel ADD CONSTRAINT FK_adLevel_syStatuses_StatusId_StatusId FOREIGN KEY (StatusId) REFERENCES dbo.syStatuses (StatusId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syUniversalSearchModules]';
GO
ALTER TABLE dbo.syUniversalSearchModules ADD CONSTRAINT FK_syUniversalSearchModules_syUniversalSearchModules_UniversalSearchId_UniversalSearchId FOREIGN KEY (UniversalSearchId) REFERENCES dbo.syUniversalSearchModules (UniversalSearchId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[AdVehicles]';
GO
ALTER TABLE dbo.AdVehicles ADD CONSTRAINT FK_AdVehicles_adLeads_LeadId_LeadId FOREIGN KEY (LeadId) REFERENCES dbo.adLeads (LeadId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLead_Notes]';
GO
ALTER TABLE dbo.adLead_Notes ADD CONSTRAINT FK_adLead_Notes_AllNotes_NotesId_NotesId FOREIGN KEY (NotesId) REFERENCES dbo.AllNotes (NotesId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeadOtherContactsAddreses]';
GO
ALTER TABLE dbo.adLeadOtherContactsAddreses ADD CONSTRAINT FK_adLeadOtherContactsAddreses_adLeadOtherContacts_OtherContactId_OtherContactId FOREIGN KEY (OtherContactId) REFERENCES dbo.adLeadOtherContacts (OtherContactId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeadOtherContactsEmail]';
GO
ALTER TABLE dbo.adLeadOtherContactsEmail ADD CONSTRAINT FK_adLeadOtherContactsEmail_adLeadOtherContacts_OtherContactId_OtherContactId FOREIGN KEY (OtherContactId) REFERENCES dbo.adLeadOtherContacts (OtherContactId); 
GO
ALTER TABLE dbo.adLeadOtherContactsEmail ADD CONSTRAINT FK_adLeadOtherContactsEmail_syEmailType_EmailTypeId_EmailTypeId FOREIGN KEY (EMailTypeId) REFERENCES dbo.syEmailType (EMailTypeId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adExtraCurricular]';
GO
ALTER TABLE dbo.adExtraCurricular ADD CONSTRAINT FK_adExtraCurricular_adLevel_LevelId_LevelId FOREIGN KEY (LevelId) REFERENCES dbo.adLevel (LevelId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adSkills]';
GO
ALTER TABLE dbo.adSkills ADD CONSTRAINT FK_adSkills_adLevel_LevelId_LevelId FOREIGN KEY (LevelId) REFERENCES dbo.adLevel (LevelId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syInstitutions]';
GO
ALTER TABLE dbo.syInstitutions ADD CONSTRAINT FK_syInstitutions_adLevel_LevelId_LevelId FOREIGN KEY (LevelID) REFERENCES dbo.adLevel (LevelId); 
GO
ALTER TABLE dbo.syInstitutions ADD CONSTRAINT FK_syInstitutions_syInstitutionImportTypes_ImportTypeId_ImportTypeID FOREIGN KEY (ImportTypeId) REFERENCES dbo.syInstitutionImportTypes (ImportTypeID); 
GO
ALTER TABLE dbo.syInstitutions ADD CONSTRAINT FK_syInstitutions_syInstitutionTypes_TypeId_TypeId FOREIGN KEY (TypeId) REFERENCES dbo.syInstitutionTypes (TypeID); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adLeads]';
GO
ALTER TABLE dbo.adLeads ADD CONSTRAINT FK_adLeads_adVendorCampaign_CampaignId_CampaignId FOREIGN KEY (CampaignId) REFERENCES dbo.adVendorCampaign (CampaignId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[adVendorCampaign]';
GO
ALTER TABLE dbo.adVendorCampaign ADD CONSTRAINT FK_adVendorCampaign_adVendors_VendorId_VendorId FOREIGN KEY (VendorId) REFERENCES dbo.adVendors (VendorId); 
GO
ALTER TABLE dbo.adVendorCampaign ADD CONSTRAINT FK_adVendorCampaign_adVendorPayFor_PayForId_IdPayFor FOREIGN KEY (PayForId) REFERENCES dbo.adVendorPayFor (IdPayFor); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[arGrdBkResults]';
GO
ALTER TABLE dbo.arGrdBkResults ADD CONSTRAINT FK_arGrdBkResults_arStuEnrollments_StuEnrollId_StuEnrollId FOREIGN KEY (StuEnrollId) REFERENCES dbo.arStuEnrollments (StuEnrollId); 
GO
ALTER TABLE dbo.arGrdBkResults ADD CONSTRAINT FK_arGrdBkResults_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtDetailId FOREIGN KEY (InstrGrdBkWgtDetailId) REFERENCES dbo.arGrdBkWgtDetails (InstrGrdBkWgtDetailId); 
GO
ALTER TABLE dbo.arGrdBkResults ADD CONSTRAINT FK_arGrdBkResults_arClassSections_ClsSectionId_ClsSectionId FOREIGN KEY (ClsSectionId) REFERENCES dbo.arClassSections (ClsSectionId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[plAddressTypes]';
GO
ALTER TABLE dbo.plAddressTypes ADD CONSTRAINT FK_plAddressTypes_syCampGrps_CampGrpId_CampGrpId FOREIGN KEY (CampGrpId) REFERENCES dbo.syCampGrps (CampGrpId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[saBankAccounts]';
GO
ALTER TABLE dbo.saBankAccounts ADD CONSTRAINT FK_saBankAccounts_syStatuses_StatusId_StatusId FOREIGN KEY (StatusId) REFERENCES dbo.syStatuses (StatusId); 
GO
ALTER TABLE dbo.saBankAccounts ADD CONSTRAINT FK_saBankAccounts_syCampGrps_CampGrpId_CampGrpId FOREIGN KEY (CampGrpId) REFERENCES dbo.syCampGrps (CampGrpId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[AdLeadEmail]';
GO
ALTER TABLE dbo.AdLeadEmail ADD CONSTRAINT FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId FOREIGN KEY (EMailTypeId) REFERENCES dbo.syEmailType (EMailTypeId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syFamilyIncome]';
GO
ALTER TABLE dbo.syFamilyIncome ADD CONSTRAINT FK_syFamilyIncome_syCampGrps_CampGrpId_CampGrpId FOREIGN KEY (CampGrpId) REFERENCES dbo.syCampGrps (CampGrpId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syHolidays]';
GO
ALTER TABLE dbo.syHolidays ADD CONSTRAINT FK_syHolidays_syStatuses_StatusId_StatusId FOREIGN KEY (StatusId) REFERENCES dbo.syStatuses (StatusId); 
GO
ALTER TABLE dbo.syHolidays ADD CONSTRAINT FK_syHolidays_syCampGrps_CampGrpId_CampGrpId FOREIGN KEY (CampGrpId) REFERENCES dbo.syCampGrps (CampGrpId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syLeadStatusChanges]';
GO
ALTER TABLE dbo.syLeadStatusChanges ADD CONSTRAINT FK_syLeadStatusChanges_syCampGrps_CampGrpId_CampGrpId FOREIGN KEY (CampGrpId) REFERENCES dbo.syCampGrps (CampGrpId); 
GO
ALTER TABLE dbo.syLeadStatusChanges ADD CONSTRAINT FK_syLeadStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId FOREIGN KEY (OrigStatusId) REFERENCES dbo.syStatusCodes (StatusCodeId); 
GO
ALTER TABLE dbo.syLeadStatusChanges ADD CONSTRAINT FK_syLeadStatusChanges_syStatusCodes_NewStatusId_StatusCodeId FOREIGN KEY (NewStatusId) REFERENCES dbo.syStatusCodes (StatusCodeId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[AllNotes]';
GO
ALTER TABLE dbo.AllNotes ADD CONSTRAINT FK_AllNotes_syModules_ModuleCode_ModuleID FOREIGN KEY (ModuleCode) REFERENCES dbo.syModules (ModuleCode); 
GO
ALTER TABLE dbo.AllNotes ADD CONSTRAINT FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId FOREIGN KEY (PageFieldId) REFERENCES dbo.syNotesPageFields (PageFieldId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Adding foreign keys to [dbo].[syResourceSdf]';
GO
ALTER TABLE dbo.syResourceSdf ADD CONSTRAINT FK_syResourceSdf_sySDF_sdfID_SDFId FOREIGN KEY (sdfID) REFERENCES dbo.sySDF (SDFId); 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[AdLeadEmail_Audit_Delete] on [dbo].[AdLeadEmail]';
GO
 
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Delete 
-- DELETE  add Audit History when delete records in AdLeadEmail
--========================================================================================== 
ALTER TRIGGER dbo.AdLeadEmail_Audit_Delete ON dbo.AdLeadEmail
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),Old.EMail)
                        FROM    Deleted AS Old;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                        FROM    Deleted AS Old;
                -- IsPreferred 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsPreferred'
                               ,CONVERT(VARCHAR(MAX),Old.IsPreferred)
                        FROM    Deleted AS Old;
                -- IsPortalUserName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsPortalUserName'
                               ,CONVERT(VARCHAR(MAX),Old.IsPortalUserName)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[AdLeadEmail_Audit_Insert] on [dbo].[AdLeadEmail]';
GO
 
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Insert 
-- INSERT  add Audit History when insert records in AdLeadEmail
--========================================================================================== 
ALTER TRIGGER dbo.AdLeadEmail_Audit_Insert ON dbo.AdLeadEmail
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),New.EMail)
                        FROM    Inserted AS New;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                        FROM    Inserted AS New;
                -- IsPreferred 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsPreferred'
                               ,CONVERT(VARCHAR(MAX),New.IsPreferred)
                        FROM    Inserted AS New;
                -- IsPortalUserName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsPortalUserName'
                               ,CONVERT(VARCHAR(MAX),New.IsPortalUserName)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[AdLeadEmail_Audit_Update] on [dbo].[AdLeadEmail]';
GO
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Update 
-- UPDATE  add Audit History when update fields in AdLeadEmail
--========================================================================================== 
ALTER TRIGGER dbo.AdLeadEmail_Audit_Update ON dbo.AdLeadEmail
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EMail 
                IF UPDATE(EMail)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'EMail'
                                       ,CONVERT(VARCHAR(MAX),Old.EMail)
                                       ,CONVERT(VARCHAR(MAX),New.EMail)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.EMail <> New.EMail
                                        OR (
                                             Old.EMail IS NULL
                                             AND New.EMail IS NOT NULL
                                           )
                                        OR (
                                             New.EMail IS NULL
                                             AND Old.EMail IS NOT NULL
                                           );
                    END;
                -- EMailTypeId 
                IF UPDATE(EMailTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'EMailTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.EMailTypeId <> New.EMailTypeId
                                        OR (
                                             Old.EMailTypeId IS NULL
                                             AND New.EMailTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.EMailTypeId IS NULL
                                             AND Old.EMailTypeId IS NOT NULL
                                           );
                    END;
                -- IsPreferred 
                IF UPDATE(IsPreferred)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsPreferred'
                                       ,CONVERT(VARCHAR(MAX),Old.IsPreferred)
                                       ,CONVERT(VARCHAR(MAX),New.IsPreferred)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsPreferred <> New.IsPreferred
                                        OR (
                                             Old.IsPreferred IS NULL
                                             AND New.IsPreferred IS NOT NULL
                                           )
                                        OR (
                                             New.IsPreferred IS NULL
                                             AND Old.IsPreferred IS NOT NULL
                                           );
                    END;
                -- IsPortalUserName 
                IF UPDATE(IsPortalUserName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsPortalUserName'
                                       ,CONVERT(VARCHAR(MAX),Old.IsPortalUserName)
                                       ,CONVERT(VARCHAR(MAX),New.IsPortalUserName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsPortalUserName <> New.IsPortalUserName
                                        OR (
                                             Old.IsPortalUserName IS NULL
                                             AND New.IsPortalUserName IS NOT NULL
                                           )
                                        OR (
                                             New.IsPortalUserName IS NULL
                                             AND Old.IsPortalUserName IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadAddresses_Audit_Delete] on [dbo].[adLeadAddresses]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadAddresses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadAddresses_Audit_Delete ON dbo.adLeadAddresses
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                        FROM    Deleted AS Old;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),Old.Address1)
                        FROM    Deleted AS Old;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),Old.Address2)
                        FROM    Deleted AS Old;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),Old.City)
                        FROM    Deleted AS Old;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),Old.StateId)
                        FROM    Deleted AS Old;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                        FROM    Deleted AS Old;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),Old.CountryId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),Old.State)
                        FROM    Deleted AS Old;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                        FROM    Deleted AS Old;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),Old.CountyId)
                        FROM    Deleted AS Old;
                -- ForeignCountyStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ForeignCountyStr'
                               ,CONVERT(VARCHAR(MAX),Old.ForeignCountyStr)
                        FROM    Deleted AS Old;
                -- ForeignCountryStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ForeignCountryStr'
                               ,CONVERT(VARCHAR(MAX),Old.ForeignCountryStr)
                        FROM    Deleted AS Old;
                -- AddressApto 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'AddressApto'
                               ,CONVERT(VARCHAR(MAX),Old.AddressApto)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadAddresses_Audit_Insert] on [dbo].[adLeadAddresses]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadAddresses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadAddresses_Audit_Insert ON dbo.adLeadAddresses
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                        FROM    Inserted AS New;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),New.Address1)
                        FROM    Inserted AS New;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),New.Address2)
                        FROM    Inserted AS New;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),New.City)
                        FROM    Inserted AS New;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),New.StateId)
                        FROM    Inserted AS New;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),New.ZipCode)
                        FROM    Inserted AS New;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),New.CountryId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),New.State)
                        FROM    Inserted AS New;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),New.IsInternational)
                        FROM    Inserted AS New;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),New.CountyId)
                        FROM    Inserted AS New;
                -- ForeignCountyStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ForeignCountyStr'
                               ,CONVERT(VARCHAR(MAX),New.ForeignCountyStr)
                        FROM    Inserted AS New;
                -- ForeignCountryStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ForeignCountryStr'
                               ,CONVERT(VARCHAR(MAX),New.ForeignCountryStr)
                        FROM    Inserted AS New;
                -- AddressApto 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'AddressApto'
                               ,CONVERT(VARCHAR(MAX),New.AddressApto)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadAddresses_Audit_Update] on [dbo].[adLeadAddresses]';
GO
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadAddresses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadAddresses_Audit_Update ON dbo.adLeadAddresses
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- AddressTypeId 
                IF UPDATE(AddressTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'AddressTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.AddressTypeId <> New.AddressTypeId
                                        OR (
                                             Old.AddressTypeId IS NULL
                                             AND New.AddressTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.AddressTypeId IS NULL
                                             AND Old.AddressTypeId IS NOT NULL
                                           );
                    END;
                -- Address1 
                IF UPDATE(Address1)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'Address1'
                                       ,CONVERT(VARCHAR(MAX),Old.Address1)
                                       ,CONVERT(VARCHAR(MAX),New.Address1)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.Address1 <> New.Address1
                                        OR (
                                             Old.Address1 IS NULL
                                             AND New.Address1 IS NOT NULL
                                           )
                                        OR (
                                             New.Address1 IS NULL
                                             AND Old.Address1 IS NOT NULL
                                           );
                    END;
                -- Address2 
                IF UPDATE(Address2)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'Address2'
                                       ,CONVERT(VARCHAR(MAX),Old.Address2)
                                       ,CONVERT(VARCHAR(MAX),New.Address2)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.Address2 <> New.Address2
                                        OR (
                                             Old.Address2 IS NULL
                                             AND New.Address2 IS NOT NULL
                                           )
                                        OR (
                                             New.Address2 IS NULL
                                             AND Old.Address2 IS NOT NULL
                                           );
                    END;
                -- City 
                IF UPDATE(City)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'City'
                                       ,CONVERT(VARCHAR(MAX),Old.City)
                                       ,CONVERT(VARCHAR(MAX),New.City)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.City <> New.City
                                        OR (
                                             Old.City IS NULL
                                             AND New.City IS NOT NULL
                                           )
                                        OR (
                                             New.City IS NULL
                                             AND Old.City IS NOT NULL
                                           );
                    END;
                -- StateId 
                IF UPDATE(StateId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'StateId'
                                       ,CONVERT(VARCHAR(MAX),Old.StateId)
                                       ,CONVERT(VARCHAR(MAX),New.StateId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.StateId <> New.StateId
                                        OR (
                                             Old.StateId IS NULL
                                             AND New.StateId IS NOT NULL
                                           )
                                        OR (
                                             New.StateId IS NULL
                                             AND Old.StateId IS NOT NULL
                                           );
                    END;
                -- ZipCode 
                IF UPDATE(ZipCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ZipCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                                       ,CONVERT(VARCHAR(MAX),New.ZipCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ZipCode <> New.ZipCode
                                        OR (
                                             Old.ZipCode IS NULL
                                             AND New.ZipCode IS NOT NULL
                                           )
                                        OR (
                                             New.ZipCode IS NULL
                                             AND Old.ZipCode IS NOT NULL
                                           );
                    END;
                -- CountryId 
                IF UPDATE(CountryId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'CountryId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountryId)
                                       ,CONVERT(VARCHAR(MAX),New.CountryId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.CountryId <> New.CountryId
                                        OR (
                                             Old.CountryId IS NULL
                                             AND New.CountryId IS NOT NULL
                                           )
                                        OR (
                                             New.CountryId IS NULL
                                             AND Old.CountryId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsMailingAddress 
                IF UPDATE(IsMailingAddress)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsMailingAddress'
                                       ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                                       ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsMailingAddress <> New.IsMailingAddress
                                        OR (
                                             Old.IsMailingAddress IS NULL
                                             AND New.IsMailingAddress IS NOT NULL
                                           )
                                        OR (
                                             New.IsMailingAddress IS NULL
                                             AND Old.IsMailingAddress IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- State 
                IF UPDATE(State)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'State'
                                       ,CONVERT(VARCHAR(MAX),Old.State)
                                       ,CONVERT(VARCHAR(MAX),New.State)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.State <> New.State
                                        OR (
                                             Old.State IS NULL
                                             AND New.State IS NOT NULL
                                           )
                                        OR (
                                             New.State IS NULL
                                             AND Old.State IS NOT NULL
                                           );
                    END;
                -- IsInternational 
                IF UPDATE(IsInternational)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsInternational'
                                       ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                                       ,CONVERT(VARCHAR(MAX),New.IsInternational)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsInternational <> New.IsInternational
                                        OR (
                                             Old.IsInternational IS NULL
                                             AND New.IsInternational IS NOT NULL
                                           )
                                        OR (
                                             New.IsInternational IS NULL
                                             AND Old.IsInternational IS NOT NULL
                                           );
                    END;
                -- CountyId 
                IF UPDATE(CountyId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'CountyId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountyId)
                                       ,CONVERT(VARCHAR(MAX),New.CountyId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.CountyId <> New.CountyId
                                        OR (
                                             Old.CountyId IS NULL
                                             AND New.CountyId IS NOT NULL
                                           )
                                        OR (
                                             New.CountyId IS NULL
                                             AND Old.CountyId IS NOT NULL
                                           );
                    END;
                -- ForeignCountyStr 
                IF UPDATE(ForeignCountyStr)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ForeignCountyStr'
                                       ,CONVERT(VARCHAR(MAX),Old.ForeignCountyStr)
                                       ,CONVERT(VARCHAR(MAX),New.ForeignCountyStr)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ForeignCountyStr <> New.ForeignCountyStr
                                        OR (
                                             Old.ForeignCountyStr IS NULL
                                             AND New.ForeignCountyStr IS NOT NULL
                                           )
                                        OR (
                                             New.ForeignCountyStr IS NULL
                                             AND Old.ForeignCountyStr IS NOT NULL
                                           );
                    END;
                -- ForeignCountryStr 
                IF UPDATE(ForeignCountryStr)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ForeignCountryStr'
                                       ,CONVERT(VARCHAR(MAX),Old.ForeignCountryStr)
                                       ,CONVERT(VARCHAR(MAX),New.ForeignCountryStr)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ForeignCountryStr <> New.ForeignCountryStr
                                        OR (
                                             Old.ForeignCountryStr IS NULL
                                             AND New.ForeignCountryStr IS NOT NULL
                                           )
                                        OR (
                                             New.ForeignCountryStr IS NULL
                                             AND Old.ForeignCountryStr IS NOT NULL
                                           );
                    END;
                -- AddressApto 
                IF UPDATE(AddressApto)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'AddressApto'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressApto)
                                       ,CONVERT(VARCHAR(MAX),New.AddressApto)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.AddressApto <> New.AddressApto
                                        OR (
                                             Old.AddressApto IS NULL
                                             AND New.AddressApto IS NOT NULL
                                           )
                                        OR (
                                             New.AddressApto IS NULL
                                             AND Old.AddressApto IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadEntranceTest_Audit_Delete] on [dbo].[adLeadEntranceTest]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadEntranceTest
--========================================================================================== 
ALTER TRIGGER dbo.adLeadEntranceTest_Audit_Delete ON dbo.adLeadEntranceTest
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                        FROM    Deleted AS Old;
                -- TestTaken 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'TestTaken'
                               ,CONVERT(VARCHAR(MAX),Old.TestTaken)
                        FROM    Deleted AS Old;
                -- ActualScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ActualScore'
                               ,CONVERT(VARCHAR(MAX),Old.ActualScore)
                        FROM    Deleted AS Old;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),Old.Comments)
                        FROM    Deleted AS Old;
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                        FROM    Deleted AS Old;
                -- Required 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Required'
                               ,CONVERT(VARCHAR(MAX),Old.Required)
                        FROM    Deleted AS Old;
                -- Pass 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Pass'
                               ,CONVERT(VARCHAR(MAX),Old.Pass)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),Old.OverRide)
                        FROM    Deleted AS Old;
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),Old.StudentId)
                        FROM    Deleted AS Old;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                        FROM    Deleted AS Old;
                -- CompletedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'CompletedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CompletedDate,121)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadEntranceTest_Audit_Insert] on [dbo].[adLeadEntranceTest]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadEntranceTest
--========================================================================================== 
ALTER TRIGGER dbo.adLeadEntranceTest_Audit_Insert ON dbo.adLeadEntranceTest
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                        FROM    Inserted AS New;
                -- TestTaken 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'TestTaken'
                               ,CONVERT(VARCHAR(MAX),New.TestTaken)
                        FROM    Inserted AS New;
                -- ActualScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ActualScore'
                               ,CONVERT(VARCHAR(MAX),New.ActualScore)
                        FROM    Inserted AS New;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),New.Comments)
                        FROM    Inserted AS New;
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                        FROM    Inserted AS New;
                -- Required 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Required'
                               ,CONVERT(VARCHAR(MAX),New.Required)
                        FROM    Inserted AS New;
                -- Pass 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Pass'
                               ,CONVERT(VARCHAR(MAX),New.Pass)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),New.OverRide)
                        FROM    Inserted AS New;
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),New.StudentId)
                        FROM    Inserted AS New;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                        FROM    Inserted AS New;
                -- CompletedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'CompletedDate'
                               ,CONVERT(VARCHAR(MAX),New.CompletedDate,121)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadEntranceTest_Audit_Update] on [dbo].[adLeadEntranceTest]';
GO
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadEntranceTest
--========================================================================================== 
ALTER TRIGGER dbo.adLeadEntranceTest_Audit_Update ON dbo.adLeadEntranceTest
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EntrTestId 
                IF UPDATE(EntrTestId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'EntrTestId'
                                       ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                                       ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.EntrTestId <> New.EntrTestId
                                        OR (
                                             Old.EntrTestId IS NULL
                                             AND New.EntrTestId IS NOT NULL
                                           )
                                        OR (
                                             New.EntrTestId IS NULL
                                             AND Old.EntrTestId IS NOT NULL
                                           );
                    END;
                -- TestTaken 
                IF UPDATE(TestTaken)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'TestTaken'
                                       ,CONVERT(VARCHAR(MAX),Old.TestTaken)
                                       ,CONVERT(VARCHAR(MAX),New.TestTaken)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.TestTaken <> New.TestTaken
                                        OR (
                                             Old.TestTaken IS NULL
                                             AND New.TestTaken IS NOT NULL
                                           )
                                        OR (
                                             New.TestTaken IS NULL
                                             AND Old.TestTaken IS NOT NULL
                                           );
                    END;
                -- ActualScore 
                IF UPDATE(ActualScore)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ActualScore'
                                       ,CONVERT(VARCHAR(MAX),Old.ActualScore)
                                       ,CONVERT(VARCHAR(MAX),New.ActualScore)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ActualScore <> New.ActualScore
                                        OR (
                                             Old.ActualScore IS NULL
                                             AND New.ActualScore IS NOT NULL
                                           )
                                        OR (
                                             New.ActualScore IS NULL
                                             AND Old.ActualScore IS NOT NULL
                                           );
                    END;
                -- Comments 
                IF UPDATE(Comments)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Comments'
                                       ,CONVERT(VARCHAR(MAX),Old.Comments)
                                       ,CONVERT(VARCHAR(MAX),New.Comments)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Comments <> New.Comments
                                        OR (
                                             Old.Comments IS NULL
                                             AND New.Comments IS NOT NULL
                                           )
                                        OR (
                                             New.Comments IS NULL
                                             AND Old.Comments IS NOT NULL
                                           );
                    END;
                -- ViewOrder 
                IF UPDATE(ViewOrder)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ViewOrder'
                                       ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                                       ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ViewOrder <> New.ViewOrder
                                        OR (
                                             Old.ViewOrder IS NULL
                                             AND New.ViewOrder IS NOT NULL
                                           )
                                        OR (
                                             New.ViewOrder IS NULL
                                             AND Old.ViewOrder IS NOT NULL
                                           );
                    END;
                -- Required 
                IF UPDATE(Required)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Required'
                                       ,CONVERT(VARCHAR(MAX),Old.Required)
                                       ,CONVERT(VARCHAR(MAX),New.Required)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Required <> New.Required
                                        OR (
                                             Old.Required IS NULL
                                             AND New.Required IS NOT NULL
                                           )
                                        OR (
                                             New.Required IS NULL
                                             AND Old.Required IS NOT NULL
                                           );
                    END;
                -- Pass 
                IF UPDATE(Pass)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Pass'
                                       ,CONVERT(VARCHAR(MAX),Old.Pass)
                                       ,CONVERT(VARCHAR(MAX),New.Pass)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Pass <> New.Pass
                                        OR (
                                             Old.Pass IS NULL
                                             AND New.Pass IS NOT NULL
                                           )
                                        OR (
                                             New.Pass IS NULL
                                             AND Old.Pass IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- OverRide 
                IF UPDATE(OverRide)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'OverRide'
                                       ,CONVERT(VARCHAR(MAX),Old.OverRide)
                                       ,CONVERT(VARCHAR(MAX),New.OverRide)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.OverRide <> New.OverRide
                                        OR (
                                             Old.OverRide IS NULL
                                             AND New.OverRide IS NOT NULL
                                           )
                                        OR (
                                             New.OverRide IS NULL
                                             AND Old.OverRide IS NOT NULL
                                           );
                    END;
                -- StudentId 
                IF UPDATE(StudentId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'StudentId'
                                       ,CONVERT(VARCHAR(MAX),Old.StudentId)
                                       ,CONVERT(VARCHAR(MAX),New.StudentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.StudentId <> New.StudentId
                                        OR (
                                             Old.StudentId IS NULL
                                             AND New.StudentId IS NOT NULL
                                           )
                                        OR (
                                             New.StudentId IS NULL
                                             AND Old.StudentId IS NOT NULL
                                           );
                    END;
                -- OverrideReason 
                IF UPDATE(OverrideReason)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'OverrideReason'
                                       ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                                       ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.OverrideReason <> New.OverrideReason
                                        OR (
                                             Old.OverrideReason IS NULL
                                             AND New.OverrideReason IS NOT NULL
                                           )
                                        OR (
                                             New.OverrideReason IS NULL
                                             AND Old.OverrideReason IS NOT NULL
                                           );
                    END;
                -- CompletedDate 
                IF UPDATE(CompletedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'CompletedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CompletedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CompletedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.CompletedDate <> New.CompletedDate
                                        OR (
                                             Old.CompletedDate IS NULL
                                             AND New.CompletedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CompletedDate IS NULL
                                             AND Old.CompletedDate IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadNotes_Audit_Delete] on [dbo].[adLeadNotes]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadNotes
--========================================================================================== 
ALTER TRIGGER dbo.adLeadNotes_Audit_Delete ON dbo.adLeadNotes
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','D',@EventRows,@EventDate,@UserName;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- LeadNoteDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'LeadNoteDescrip'
                               ,CONVERT(VARCHAR(MAX),Old.LeadNoteDescrip)
                        FROM    Deleted AS Old;
                -- ModuleCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModuleCode'
                               ,CONVERT(VARCHAR(MAX),Old.ModuleCode)
                        FROM    Deleted AS Old;
                -- UserId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'UserId'
                               ,CONVERT(VARCHAR(MAX),Old.UserId)
                        FROM    Deleted AS Old;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadNotes_Audit_Insert] on [dbo].[adLeadNotes]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadNotes
--========================================================================================== 
ALTER TRIGGER dbo.adLeadNotes_Audit_Insert ON dbo.adLeadNotes
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','I',@EventRows,@EventDate,@UserName;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- LeadNoteDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'LeadNoteDescrip'
                               ,CONVERT(VARCHAR(MAX),New.LeadNoteDescrip)
                        FROM    Inserted AS New;
                -- ModuleCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModuleCode'
                               ,CONVERT(VARCHAR(MAX),New.ModuleCode)
                        FROM    Inserted AS New;
                -- UserId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'UserId'
                               ,CONVERT(VARCHAR(MAX),New.UserId)
                        FROM    Inserted AS New;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadNotes_Audit_Update] on [dbo].[adLeadNotes]';
GO
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadNotes
--========================================================================================== 
ALTER TRIGGER dbo.adLeadNotes_Audit_Update ON dbo.adLeadNotes
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','U',@EventRows,@EventDate,@UserName;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- LeadNoteDescrip 
                IF UPDATE(LeadNoteDescrip)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'LeadNoteDescrip'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadNoteDescrip)
                                       ,CONVERT(VARCHAR(MAX),New.LeadNoteDescrip)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.LeadNoteDescrip <> New.LeadNoteDescrip
                                        OR (
                                             Old.LeadNoteDescrip IS NULL
                                             AND New.LeadNoteDescrip IS NOT NULL
                                           )
                                        OR (
                                             New.LeadNoteDescrip IS NULL
                                             AND Old.LeadNoteDescrip IS NOT NULL
                                           );
                    END;
                -- ModuleCode 
                IF UPDATE(ModuleCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModuleCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ModuleCode)
                                       ,CONVERT(VARCHAR(MAX),New.ModuleCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModuleCode <> New.ModuleCode
                                        OR (
                                             Old.ModuleCode IS NULL
                                             AND New.ModuleCode IS NOT NULL
                                           )
                                        OR (
                                             New.ModuleCode IS NULL
                                             AND Old.ModuleCode IS NOT NULL
                                           );
                    END;
                -- UserId 
                IF UPDATE(UserId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'UserId'
                                       ,CONVERT(VARCHAR(MAX),Old.UserId)
                                       ,CONVERT(VARCHAR(MAX),New.UserId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.UserId <> New.UserId
                                        OR (
                                             Old.UserId IS NULL
                                             AND New.UserId IS NOT NULL
                                           )
                                        OR (
                                             New.UserId IS NULL
                                             AND Old.UserId IS NOT NULL
                                           );
                    END;
                -- CreatedDate 
                IF UPDATE(CreatedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'CreatedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.CreatedDate <> New.CreatedDate
                                        OR (
                                             Old.CreatedDate IS NULL
                                             AND New.CreatedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CreatedDate IS NULL
                                             AND Old.CreatedDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate) 
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsAddreses_Audit_Delete] on [dbo].[adLeadOtherContactsAddreses]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsAddreses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsAddreses_Audit_Delete ON dbo.adLeadOtherContactsAddreses
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                        FROM    Deleted AS Old;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),Old.Address1)
                        FROM    Deleted AS Old;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),Old.Address2)
                        FROM    Deleted AS Old;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),Old.City)
                        FROM    Deleted AS Old;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),Old.StateId)
                        FROM    Deleted AS Old;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                        FROM    Deleted AS Old;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),Old.CountryId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),Old.State)
                        FROM    Deleted AS Old;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                        FROM    Deleted AS Old;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),Old.CountyId)
                        FROM    Deleted AS Old;
                -- County 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'County'
                               ,CONVERT(VARCHAR(MAX),Old.County)
                        FROM    Deleted AS Old;
                -- Country 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Country'
                               ,CONVERT(VARCHAR(MAX),Old.Country)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsAddreses_Audit_Insert] on [dbo].[adLeadOtherContactsAddreses]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsAddreses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsAddreses_Audit_Insert ON dbo.adLeadOtherContactsAddreses
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                        FROM    Inserted AS New;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),New.Address1)
                        FROM    Inserted AS New;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),New.Address2)
                        FROM    Inserted AS New;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),New.City)
                        FROM    Inserted AS New;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),New.StateId)
                        FROM    Inserted AS New;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),New.ZipCode)
                        FROM    Inserted AS New;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),New.CountryId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),New.State)
                        FROM    Inserted AS New;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),New.IsInternational)
                        FROM    Inserted AS New;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),New.CountyId)
                        FROM    Inserted AS New;
                -- County 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'County'
                               ,CONVERT(VARCHAR(MAX),New.County)
                        FROM    Inserted AS New;
                -- Country 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Country'
                               ,CONVERT(VARCHAR(MAX),New.Country)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsAddreses_Audit_Update] on [dbo].[adLeadOtherContactsAddreses]';
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsAddreses
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsAddreses_Audit_Update ON dbo.adLeadOtherContactsAddreses
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- AddressTypeId 
                IF UPDATE(AddressTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'AddressTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.AddressTypeId <> New.AddressTypeId
                                        OR (
                                             Old.AddressTypeId IS NULL
                                             AND New.AddressTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.AddressTypeId IS NULL
                                             AND Old.AddressTypeId IS NOT NULL
                                           );
                    END;
                -- Address1 
                IF UPDATE(Address1)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Address1'
                                       ,CONVERT(VARCHAR(MAX),Old.Address1)
                                       ,CONVERT(VARCHAR(MAX),New.Address1)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Address1 <> New.Address1
                                        OR (
                                             Old.Address1 IS NULL
                                             AND New.Address1 IS NOT NULL
                                           )
                                        OR (
                                             New.Address1 IS NULL
                                             AND Old.Address1 IS NOT NULL
                                           );
                    END;
                -- Address2 
                IF UPDATE(Address2)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Address2'
                                       ,CONVERT(VARCHAR(MAX),Old.Address2)
                                       ,CONVERT(VARCHAR(MAX),New.Address2)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Address2 <> New.Address2
                                        OR (
                                             Old.Address2 IS NULL
                                             AND New.Address2 IS NOT NULL
                                           )
                                        OR (
                                             New.Address2 IS NULL
                                             AND Old.Address2 IS NOT NULL
                                           );
                    END;
                -- City 
                IF UPDATE(City)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'City'
                                       ,CONVERT(VARCHAR(MAX),Old.City)
                                       ,CONVERT(VARCHAR(MAX),New.City)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.City <> New.City
                                        OR (
                                             Old.City IS NULL
                                             AND New.City IS NOT NULL
                                           )
                                        OR (
                                             New.City IS NULL
                                             AND Old.City IS NOT NULL
                                           );
                    END;
                -- StateId 
                IF UPDATE(StateId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'StateId'
                                       ,CONVERT(VARCHAR(MAX),Old.StateId)
                                       ,CONVERT(VARCHAR(MAX),New.StateId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.StateId <> New.StateId
                                        OR (
                                             Old.StateId IS NULL
                                             AND New.StateId IS NOT NULL
                                           )
                                        OR (
                                             New.StateId IS NULL
                                             AND Old.StateId IS NOT NULL
                                           );
                    END;
                -- ZipCode 
                IF UPDATE(ZipCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ZipCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                                       ,CONVERT(VARCHAR(MAX),New.ZipCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ZipCode <> New.ZipCode
                                        OR (
                                             Old.ZipCode IS NULL
                                             AND New.ZipCode IS NOT NULL
                                           )
                                        OR (
                                             New.ZipCode IS NULL
                                             AND Old.ZipCode IS NOT NULL
                                           );
                    END;
                -- CountryId 
                IF UPDATE(CountryId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'CountryId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountryId)
                                       ,CONVERT(VARCHAR(MAX),New.CountryId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.CountryId <> New.CountryId
                                        OR (
                                             Old.CountryId IS NULL
                                             AND New.CountryId IS NOT NULL
                                           )
                                        OR (
                                             New.CountryId IS NULL
                                             AND Old.CountryId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsMailingAddress 
                IF UPDATE(IsMailingAddress)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'IsMailingAddress'
                                       ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                                       ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.IsMailingAddress <> New.IsMailingAddress
                                        OR (
                                             Old.IsMailingAddress IS NULL
                                             AND New.IsMailingAddress IS NOT NULL
                                           )
                                        OR (
                                             New.IsMailingAddress IS NULL
                                             AND Old.IsMailingAddress IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- State 
                IF UPDATE(State)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'State'
                                       ,CONVERT(VARCHAR(MAX),Old.State)
                                       ,CONVERT(VARCHAR(MAX),New.State)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.State <> New.State
                                        OR (
                                             Old.State IS NULL
                                             AND New.State IS NOT NULL
                                           )
                                        OR (
                                             New.State IS NULL
                                             AND Old.State IS NOT NULL
                                           );
                    END;
                -- IsInternational 
                IF UPDATE(IsInternational)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'IsInternational'
                                       ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                                       ,CONVERT(VARCHAR(MAX),New.IsInternational)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.IsInternational <> New.IsInternational
                                        OR (
                                             Old.IsInternational IS NULL
                                             AND New.IsInternational IS NOT NULL
                                           )
                                        OR (
                                             New.IsInternational IS NULL
                                             AND Old.IsInternational IS NOT NULL
                                           );
                    END;
                -- CountyId 
                IF UPDATE(CountyId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'CountyId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountyId)
                                       ,CONVERT(VARCHAR(MAX),New.CountyId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.CountyId <> New.CountyId
                                        OR (
                                             Old.CountyId IS NULL
                                             AND New.CountyId IS NOT NULL
                                           )
                                        OR (
                                             New.CountyId IS NULL
                                             AND Old.CountyId IS NOT NULL
                                           );
                    END;
                -- County 
                IF UPDATE(County)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'County'
                                       ,CONVERT(VARCHAR(MAX),Old.County)
                                       ,CONVERT(VARCHAR(MAX),New.County)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.County <> New.County
                                        OR (
                                             Old.County IS NULL
                                             AND New.County IS NOT NULL
                                           )
                                        OR (
                                             New.County IS NULL
                                             AND Old.County IS NOT NULL
                                           );
                    END;
                -- Country 
                IF UPDATE(Country)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Country'
                                       ,CONVERT(VARCHAR(MAX),Old.Country)
                                       ,CONVERT(VARCHAR(MAX),New.Country)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Country <> New.Country
                                        OR (
                                             Old.Country IS NULL
                                             AND New.Country IS NOT NULL
                                           )
                                        OR (
                                             New.Country IS NULL
                                             AND Old.Country IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsEmail_Audit_Delete] on [dbo].[adLeadOtherContactsEmail]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsEmail
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsEmail_Audit_Delete ON dbo.adLeadOtherContactsEmail
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),Old.EMail)
                        FROM    Deleted AS Old;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsEmail_Audit_Insert] on [dbo].[adLeadOtherContactsEmail]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsEmail
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsEmail_Audit_Insert ON dbo.adLeadOtherContactsEmail
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),New.EMail)
                        FROM    Inserted AS New;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsEmail_Audit_Update] on [dbo].[adLeadOtherContactsEmail]';
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsEmail
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsEmail_Audit_Update ON dbo.adLeadOtherContactsEmail
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EMail 
                IF UPDATE(EMail)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'EMail'
                                       ,CONVERT(VARCHAR(MAX),Old.EMail)
                                       ,CONVERT(VARCHAR(MAX),New.EMail)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.EMail <> New.EMail
                                        OR (
                                             Old.EMail IS NULL
                                             AND New.EMail IS NOT NULL
                                           )
                                        OR (
                                             New.EMail IS NULL
                                             AND Old.EMail IS NOT NULL
                                           );
                    END;
                -- EMailTypeId 
                IF UPDATE(EMailTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'EMailTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.EMailTypeId <> New.EMailTypeId
                                        OR (
                                             Old.EMailTypeId IS NULL
                                             AND New.EMailTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.EMailTypeId IS NULL
                                             AND Old.EMailTypeId IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsPhone_Audit_Delete] on [dbo].[adLeadOtherContactsPhone]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsPhone_Audit_Delete ON dbo.adLeadOtherContactsPhone
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                        FROM    Deleted AS Old;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),Old.Phone)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),Old.Extension)
                        FROM    Deleted AS Old;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsPhone_Audit_Insert] on [dbo].[adLeadOtherContactsPhone]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsPhone_Audit_Insert ON dbo.adLeadOtherContactsPhone
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                        FROM    Inserted AS New;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),New.Phone)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),New.Extension)
                        FROM    Inserted AS New;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContactsPhone_Audit_Update] on [dbo].[adLeadOtherContactsPhone]';
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContactsPhone_Audit_Update ON dbo.adLeadOtherContactsPhone
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- PhoneTypeId 
                IF UPDATE(PhoneTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'PhoneTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.PhoneTypeId <> New.PhoneTypeId
                                        OR (
                                             Old.PhoneTypeId IS NULL
                                             AND New.PhoneTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PhoneTypeId IS NULL
                                             AND Old.PhoneTypeId IS NOT NULL
                                           );
                    END;
                -- Phone 
                IF UPDATE(Phone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'Phone'
                                       ,CONVERT(VARCHAR(MAX),Old.Phone)
                                       ,CONVERT(VARCHAR(MAX),New.Phone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.Phone <> New.Phone
                                        OR (
                                             Old.Phone IS NULL
                                             AND New.Phone IS NOT NULL
                                           )
                                        OR (
                                             New.Phone IS NULL
                                             AND Old.Phone IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- Extension 
                IF UPDATE(Extension)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'Extension'
                                       ,CONVERT(VARCHAR(MAX),Old.Extension)
                                       ,CONVERT(VARCHAR(MAX),New.Extension)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.Extension <> New.Extension
                                        OR (
                                             Old.Extension IS NULL
                                             AND New.Extension IS NOT NULL
                                           )
                                        OR (
                                             New.Extension IS NULL
                                             AND Old.Extension IS NOT NULL
                                           );
                    END;
                -- IsForeignPhone 
                IF UPDATE(IsForeignPhone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'IsForeignPhone'
                                       ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                                       ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.IsForeignPhone <> New.IsForeignPhone
                                        OR (
                                             Old.IsForeignPhone IS NULL
                                             AND New.IsForeignPhone IS NOT NULL
                                           )
                                        OR (
                                             New.IsForeignPhone IS NULL
                                             AND Old.IsForeignPhone IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContacts_Audit_Delete] on [dbo].[adLeadOtherContacts]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContacts
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContacts_Audit_Delete ON dbo.adLeadOtherContacts
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- RelationshipId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'RelationshipId'
                               ,CONVERT(VARCHAR(MAX),Old.RelationshipId)
                        FROM    Deleted AS Old;
                -- ContactTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ContactTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.ContactTypeId)
                        FROM    Deleted AS Old;
                -- PrefixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(MAX),Old.PrefixId)
                        FROM    Deleted AS Old;
                -- SufixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'SufixId'
                               ,CONVERT(VARCHAR(MAX),Old.SufixId)
                        FROM    Deleted AS Old;
                -- FirstName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(MAX),Old.FirstName)
                        FROM    Deleted AS Old;
                -- LastName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(MAX),Old.LastName)
                        FROM    Deleted AS Old;
                -- MiddleName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(MAX),Old.MiddleName)
                        FROM    Deleted AS Old;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),Old.Comments)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContacts_Audit_Insert] on [dbo].[adLeadOtherContacts]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContacts
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContacts_Audit_Insert ON dbo.adLeadOtherContacts
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- RelationshipId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'RelationshipId'
                               ,CONVERT(VARCHAR(MAX),New.RelationshipId)
                        FROM    Inserted AS New;
                -- ContactTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ContactTypeId'
                               ,CONVERT(VARCHAR(MAX),New.ContactTypeId)
                        FROM    Inserted AS New;
                -- PrefixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(MAX),New.PrefixId)
                        FROM    Inserted AS New;
                -- SufixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'SufixId'
                               ,CONVERT(VARCHAR(MAX),New.SufixId)
                        FROM    Inserted AS New;
                -- FirstName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(MAX),New.FirstName)
                        FROM    Inserted AS New;
                -- LastName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(MAX),New.LastName)
                        FROM    Inserted AS New;
                -- MiddleName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(MAX),New.MiddleName)
                        FROM    Inserted AS New;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),New.Comments)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadOtherContacts_Audit_Update] on [dbo].[adLeadOtherContacts]';
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContacts
--========================================================================================== 
ALTER TRIGGER dbo.adLeadOtherContacts_Audit_Update ON dbo.adLeadOtherContacts
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- RelationshipId 
                IF UPDATE(RelationshipId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'RelationshipId'
                                       ,CONVERT(VARCHAR(MAX),Old.RelationshipId)
                                       ,CONVERT(VARCHAR(MAX),New.RelationshipId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.RelationshipId <> New.RelationshipId
                                        OR (
                                             Old.RelationshipId IS NULL
                                             AND New.RelationshipId IS NOT NULL
                                           )
                                        OR (
                                             New.RelationshipId IS NULL
                                             AND Old.RelationshipId IS NOT NULL
                                           );
                    END;
                -- ContactTypeId 
                IF UPDATE(ContactTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ContactTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.ContactTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.ContactTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ContactTypeId <> New.ContactTypeId
                                        OR (
                                             Old.ContactTypeId IS NULL
                                             AND New.ContactTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.ContactTypeId IS NULL
                                             AND Old.ContactTypeId IS NOT NULL
                                           );
                    END;
                -- PrefixId 
                IF UPDATE(PrefixId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'PrefixId'
                                       ,CONVERT(VARCHAR(MAX),Old.PrefixId)
                                       ,CONVERT(VARCHAR(MAX),New.PrefixId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.PrefixId <> New.PrefixId
                                        OR (
                                             Old.PrefixId IS NULL
                                             AND New.PrefixId IS NOT NULL
                                           )
                                        OR (
                                             New.PrefixId IS NULL
                                             AND Old.PrefixId IS NOT NULL
                                           );
                    END;
                -- SufixId 
                IF UPDATE(SufixId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'SufixId'
                                       ,CONVERT(VARCHAR(MAX),Old.SufixId)
                                       ,CONVERT(VARCHAR(MAX),New.SufixId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.SufixId <> New.SufixId
                                        OR (
                                             Old.SufixId IS NULL
                                             AND New.SufixId IS NOT NULL
                                           )
                                        OR (
                                             New.SufixId IS NULL
                                             AND Old.SufixId IS NOT NULL
                                           );
                    END;
                -- FirstName 
                IF UPDATE(FirstName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'FirstName'
                                       ,CONVERT(VARCHAR(MAX),Old.FirstName)
                                       ,CONVERT(VARCHAR(MAX),New.FirstName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.FirstName <> New.FirstName
                                        OR (
                                             Old.FirstName IS NULL
                                             AND New.FirstName IS NOT NULL
                                           )
                                        OR (
                                             New.FirstName IS NULL
                                             AND Old.FirstName IS NOT NULL
                                           );
                    END;
                -- LastName 
                IF UPDATE(LastName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'LastName'
                                       ,CONVERT(VARCHAR(MAX),Old.LastName)
                                       ,CONVERT(VARCHAR(MAX),New.LastName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.LastName <> New.LastName
                                        OR (
                                             Old.LastName IS NULL
                                             AND New.LastName IS NOT NULL
                                           )
                                        OR (
                                             New.LastName IS NULL
                                             AND Old.LastName IS NOT NULL
                                           );
                    END;
                -- MiddleName 
                IF UPDATE(MiddleName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'MiddleName'
                                       ,CONVERT(VARCHAR(MAX),Old.MiddleName)
                                       ,CONVERT(VARCHAR(MAX),New.MiddleName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.MiddleName <> New.MiddleName
                                        OR (
                                             Old.MiddleName IS NULL
                                             AND New.MiddleName IS NOT NULL
                                           )
                                        OR (
                                             New.MiddleName IS NULL
                                             AND Old.MiddleName IS NOT NULL
                                           );
                    END;
                -- Comments 
                IF UPDATE(Comments)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'Comments'
                                       ,CONVERT(VARCHAR(MAX),Old.Comments)
                                       ,CONVERT(VARCHAR(MAX),New.Comments)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.Comments <> New.Comments
                                        OR (
                                             Old.Comments IS NULL
                                             AND New.Comments IS NOT NULL
                                           )
                                        OR (
                                             New.Comments IS NULL
                                             AND Old.Comments IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPayments_Audit_Delete] on [dbo].[adLeadPayments]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadPayments
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPayments_Audit_Delete ON dbo.adLeadPayments
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','D',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PaymentTypeId)
                        FROM    Deleted AS Old;
                -- CheckNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(MAX),Old.CheckNumber)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- PaymentReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'PaymentReference'
                               ,CONVERT(VARCHAR(MAX),Old.PaymentReference)
                        FROM    Deleted AS Old;
                -- IsDeposited 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(MAX),Old.IsDeposited)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPayments_Audit_Insert] on [dbo].[adLeadPayments]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadPayments
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPayments_Audit_Insert ON dbo.adLeadPayments
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','I',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PaymentTypeId)
                        FROM    Inserted AS New;
                -- CheckNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(MAX),New.CheckNumber)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- PaymentReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'PaymentReference'
                               ,CONVERT(VARCHAR(MAX),New.PaymentReference)
                        FROM    Inserted AS New;
                -- IsDeposited 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(MAX),New.IsDeposited)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPayments_Audit_Update] on [dbo].[adLeadPayments]';
GO
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadPayments
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPayments_Audit_Update ON dbo.adLeadPayments
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','U',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                IF UPDATE(PaymentTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'PaymentTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PaymentTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PaymentTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.PaymentTypeId <> New.PaymentTypeId
                                        OR (
                                             Old.PaymentTypeId IS NULL
                                             AND New.PaymentTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PaymentTypeId IS NULL
                                             AND Old.PaymentTypeId IS NOT NULL
                                           );
                    END;
                -- CheckNumber 
                IF UPDATE(CheckNumber)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CheckNumber'
                                       ,CONVERT(VARCHAR(MAX),Old.CheckNumber)
                                       ,CONVERT(VARCHAR(MAX),New.CheckNumber)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CheckNumber <> New.CheckNumber
                                        OR (
                                             Old.CheckNumber IS NULL
                                             AND New.CheckNumber IS NOT NULL
                                           )
                                        OR (
                                             New.CheckNumber IS NULL
                                             AND Old.CheckNumber IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- PaymentReference 
                IF UPDATE(PaymentReference)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'PaymentReference'
                                       ,CONVERT(VARCHAR(MAX),Old.PaymentReference)
                                       ,CONVERT(VARCHAR(MAX),New.PaymentReference)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.PaymentReference <> New.PaymentReference
                                        OR (
                                             Old.PaymentReference IS NULL
                                             AND New.PaymentReference IS NOT NULL
                                           )
                                        OR (
                                             New.PaymentReference IS NULL
                                             AND Old.PaymentReference IS NOT NULL
                                           );
                    END;
                -- IsDeposited 
                IF UPDATE(IsDeposited)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'IsDeposited'
                                       ,CONVERT(VARCHAR(MAX),Old.IsDeposited)
                                       ,CONVERT(VARCHAR(MAX),New.IsDeposited)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.IsDeposited <> New.IsDeposited
                                        OR (
                                             Old.IsDeposited IS NULL
                                             AND New.IsDeposited IS NOT NULL
                                           )
                                        OR (
                                             New.IsDeposited IS NULL
                                             AND Old.IsDeposited IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPhone_Audit_Delete] on [dbo].[adLeadPhone]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPhone_Audit_Delete ON dbo.adLeadPhone
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                        FROM    Deleted AS Old;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),Old.Phone)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- Position 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Position'
                               ,CONVERT(VARCHAR(MAX),Old.Position)
                        FROM    Deleted AS Old;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),Old.Extension)
                        FROM    Deleted AS Old;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                        FROM    Deleted AS Old;
                -- IsBest 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsBest'
                               ,CONVERT(VARCHAR(MAX),Old.IsBest)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPhone_Audit_Insert] on [dbo].[adLeadPhone]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPhone_Audit_Insert ON dbo.adLeadPhone
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                        FROM    Inserted AS New;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),New.Phone)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- Position 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Position'
                               ,CONVERT(VARCHAR(MAX),New.Position)
                        FROM    Inserted AS New;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),New.Extension)
                        FROM    Inserted AS New;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                        FROM    Inserted AS New;
                -- IsBest 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsBest'
                               ,CONVERT(VARCHAR(MAX),New.IsBest)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadPhone_Audit_Update] on [dbo].[adLeadPhone]';
GO
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadPhone
--========================================================================================== 
ALTER TRIGGER dbo.adLeadPhone_Audit_Update ON dbo.adLeadPhone
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- PhoneTypeId 
                IF UPDATE(PhoneTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'PhoneTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.PhoneTypeId <> New.PhoneTypeId
                                        OR (
                                             Old.PhoneTypeId IS NULL
                                             AND New.PhoneTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PhoneTypeId IS NULL
                                             AND Old.PhoneTypeId IS NOT NULL
                                           );
                    END;
                -- Phone 
                IF UPDATE(Phone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Phone'
                                       ,CONVERT(VARCHAR(MAX),Old.Phone)
                                       ,CONVERT(VARCHAR(MAX),New.Phone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Phone <> New.Phone
                                        OR (
                                             Old.Phone IS NULL
                                             AND New.Phone IS NOT NULL
                                           )
                                        OR (
                                             New.Phone IS NULL
                                             AND Old.Phone IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- Position 
                IF UPDATE(Position)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Position'
                                       ,CONVERT(VARCHAR(MAX),Old.Position)
                                       ,CONVERT(VARCHAR(MAX),New.Position)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Position <> New.Position
                                        OR (
                                             Old.Position IS NULL
                                             AND New.Position IS NOT NULL
                                           )
                                        OR (
                                             New.Position IS NULL
                                             AND Old.Position IS NOT NULL
                                           );
                    END;
                -- Extension 
                IF UPDATE(Extension)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Extension'
                                       ,CONVERT(VARCHAR(MAX),Old.Extension)
                                       ,CONVERT(VARCHAR(MAX),New.Extension)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Extension <> New.Extension
                                        OR (
                                             Old.Extension IS NULL
                                             AND New.Extension IS NOT NULL
                                           )
                                        OR (
                                             New.Extension IS NULL
                                             AND Old.Extension IS NOT NULL
                                           );
                    END;
                -- IsForeignPhone 
                IF UPDATE(IsForeignPhone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsForeignPhone'
                                       ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                                       ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsForeignPhone <> New.IsForeignPhone
                                        OR (
                                             Old.IsForeignPhone IS NULL
                                             AND New.IsForeignPhone IS NOT NULL
                                           )
                                        OR (
                                             New.IsForeignPhone IS NULL
                                             AND Old.IsForeignPhone IS NOT NULL
                                           );
                    END;
                -- IsBest 
                IF UPDATE(IsBest)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsBest'
                                       ,CONVERT(VARCHAR(MAX),Old.IsBest)
                                       ,CONVERT(VARCHAR(MAX),New.IsBest)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsBest <> New.IsBest
                                        OR (
                                             Old.IsBest IS NULL
                                             AND New.IsBest IS NOT NULL
                                           )
                                        OR (
                                             New.IsBest IS NULL
                                             AND Old.IsBest IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTranReceived_Audit_Delete] on [dbo].[adLeadTranReceived]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadTranReceived_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadTranReceived
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTranReceived_Audit_Delete ON dbo.adLeadTranReceived
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTranReceived','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                        FROM    Deleted AS Old;
                -- ReceivedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'ReceivedDate'
                               ,CONVERT(VARCHAR(MAX),Old.ReceivedDate,121)
                        FROM    Deleted AS Old;
                -- IsApproved 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'IsApproved'
                               ,CONVERT(VARCHAR(MAX),Old.IsApproved)
                        FROM    Deleted AS Old;
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),Old.Override)
                        FROM    Deleted AS Old;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadTranReceivedId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTranReceived_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTranReceived_Audit_Insert] on [dbo].[adLeadTranReceived]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadTranReceived_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadTranReceived
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTranReceived_Audit_Insert ON dbo.adLeadTranReceived
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTranReceived','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),New.DocumentId)
                        FROM    Inserted AS New;
                -- ReceivedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'ReceivedDate'
                               ,CONVERT(VARCHAR(MAX),New.ReceivedDate,121)
                        FROM    Inserted AS New;
                -- IsApproved 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'IsApproved'
                               ,CONVERT(VARCHAR(MAX),New.IsApproved)
                        FROM    Inserted AS New;
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),New.Override)
                        FROM    Inserted AS New;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadTranReceivedId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTranReceived_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTranReceived_Audit_Update] on [dbo].[adLeadTranReceived]';
GO
--========================================================================================== 
-- TRIGGER adLeadTranReceived_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadTranReceived
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTranReceived_Audit_Update ON dbo.adLeadTranReceived
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTranReceived','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- DocumentId 
                IF UPDATE(DocumentId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'DocumentId'
                                       ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                                       ,CONVERT(VARCHAR(MAX),New.DocumentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.DocumentId <> New.DocumentId
                                        OR (
                                             Old.DocumentId IS NULL
                                             AND New.DocumentId IS NOT NULL
                                           )
                                        OR (
                                             New.DocumentId IS NULL
                                             AND Old.DocumentId IS NOT NULL
                                           );
                    END;
                -- ReceivedDate 
                IF UPDATE(ReceivedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'ReceivedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ReceivedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ReceivedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.ReceivedDate <> New.ReceivedDate
                                        OR (
                                             Old.ReceivedDate IS NULL
                                             AND New.ReceivedDate IS NOT NULL
                                           )
                                        OR (
                                             New.ReceivedDate IS NULL
                                             AND Old.ReceivedDate IS NOT NULL
                                           );
                    END;
                -- IsApproved 
                IF UPDATE(IsApproved)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'IsApproved'
                                       ,CONVERT(VARCHAR(MAX),Old.IsApproved)
                                       ,CONVERT(VARCHAR(MAX),New.IsApproved)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.IsApproved <> New.IsApproved
                                        OR (
                                             Old.IsApproved IS NULL
                                             AND New.IsApproved IS NOT NULL
                                           )
                                        OR (
                                             New.IsApproved IS NULL
                                             AND Old.IsApproved IS NOT NULL
                                           );
                    END;
                -- ApprovalDate 
                IF UPDATE(ApprovalDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'ApprovalDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.ApprovalDate <> New.ApprovalDate
                                        OR (
                                             Old.ApprovalDate IS NULL
                                             AND New.ApprovalDate IS NOT NULL
                                           )
                                        OR (
                                             New.ApprovalDate IS NULL
                                             AND Old.ApprovalDate IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- Override 
                IF UPDATE(Override)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'Override'
                                       ,CONVERT(VARCHAR(MAX),Old.Override)
                                       ,CONVERT(VARCHAR(MAX),New.Override)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.Override <> New.Override
                                        OR (
                                             Old.Override IS NULL
                                             AND New.Override IS NOT NULL
                                           )
                                        OR (
                                             New.Override IS NULL
                                             AND Old.Override IS NOT NULL
                                           );
                    END;
                -- OverrideReason 
                IF UPDATE(OverrideReason)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadTranReceivedId
                                       ,'OverrideReason'
                                       ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                                       ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadTranReceivedId = New.adLeadTranReceivedId
                                WHERE   Old.OverrideReason <> New.OverrideReason
                                        OR (
                                             Old.OverrideReason IS NULL
                                             AND New.OverrideReason IS NOT NULL
                                           )
                                        OR (
                                             New.OverrideReason IS NULL
                                             AND Old.OverrideReason IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTranReceived_Audit_Update 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTransactions_Audit_Delete] on [dbo].[adLeadTransactions]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadTransactions
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTransactions_Audit_Delete ON dbo.adLeadTransactions
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- TransCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(MAX),Old.TransCodeId)
                        FROM    Deleted AS Old;
                -- TransReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransReference'
                               ,CONVERT(VARCHAR(MAX),Old.TransReference)
                        FROM    Deleted AS Old;
                -- TransDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransDescrip'
                               ,CONVERT(VARCHAR(MAX),Old.TransDescrip)
                        FROM    Deleted AS Old;
                -- TransAmount 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransAmount'
                               ,CONVERT(VARCHAR(MAX),Old.TransAmount)
                        FROM    Deleted AS Old;
                -- TransDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransDate'
                               ,CONVERT(VARCHAR(MAX),Old.TransDate,121)
                        FROM    Deleted AS Old;
                -- TransTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.TransTypeId)
                        FROM    Deleted AS Old;
                -- isEnrolled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'isEnrolled'
                               ,CONVERT(VARCHAR(MAX),Old.isEnrolled)
                        FROM    Deleted AS Old;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                        FROM    Deleted AS Old;
                -- Voided 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'Voided'
                               ,CONVERT(VARCHAR(MAX),Old.Voided)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),Old.CampusId)
                        FROM    Deleted AS Old;
                -- ReversalReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ReversalReason'
                               ,CONVERT(VARCHAR(MAX),Old.ReversalReason)
                        FROM    Deleted AS Old;
                -- DisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'DisplaySequence'
                               ,CONVERT(VARCHAR(MAX),Old.DisplaySequence)
                        FROM    Deleted AS Old;
                -- SecondDisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'SecondDisplaySequence'
                               ,CONVERT(VARCHAR(MAX),Old.SecondDisplaySequence)
                        FROM    Deleted AS Old;
                -- MapTransactionId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'MapTransactionId'
                               ,CONVERT(VARCHAR(MAX),Old.MapTransactionId)
                        FROM    Deleted AS Old;
                -- LeadRequirementId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'LeadRequirementId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadRequirementId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Delete 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTransactions_Audit_Insert] on [dbo].[adLeadTransactions]';
GO
 
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadTransactions
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTransactions_Audit_Insert ON dbo.adLeadTransactions
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- TransCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(MAX),New.TransCodeId)
                        FROM    Inserted AS New;
                -- TransReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransReference'
                               ,CONVERT(VARCHAR(MAX),New.TransReference)
                        FROM    Inserted AS New;
                -- TransDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransDescrip'
                               ,CONVERT(VARCHAR(MAX),New.TransDescrip)
                        FROM    Inserted AS New;
                -- TransAmount 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransAmount'
                               ,CONVERT(VARCHAR(MAX),New.TransAmount)
                        FROM    Inserted AS New;
                -- TransDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransDate'
                               ,CONVERT(VARCHAR(MAX),New.TransDate,121)
                        FROM    Inserted AS New;
                -- TransTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransTypeId'
                               ,CONVERT(VARCHAR(MAX),New.TransTypeId)
                        FROM    Inserted AS New;
                -- isEnrolled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'isEnrolled'
                               ,CONVERT(VARCHAR(MAX),New.isEnrolled)
                        FROM    Inserted AS New;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                        FROM    Inserted AS New;
                -- Voided 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'Voided'
                               ,CONVERT(VARCHAR(MAX),New.Voided)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),New.CampusId)
                        FROM    Inserted AS New;
                -- ReversalReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ReversalReason'
                               ,CONVERT(VARCHAR(MAX),New.ReversalReason)
                        FROM    Inserted AS New;
                -- DisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'DisplaySequence'
                               ,CONVERT(VARCHAR(MAX),New.DisplaySequence)
                        FROM    Inserted AS New;
                -- SecondDisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'SecondDisplaySequence'
                               ,CONVERT(VARCHAR(MAX),New.SecondDisplaySequence)
                        FROM    Inserted AS New;
                -- MapTransactionId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'MapTransactionId'
                               ,CONVERT(VARCHAR(MAX),New.MapTransactionId)
                        FROM    Inserted AS New;
                -- LeadRequirementId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'LeadRequirementId'
                               ,CONVERT(VARCHAR(MAX),New.LeadRequirementId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Insert 
--========================================================================================== 
 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Altering trigger [dbo].[adLeadTransactions_Audit_Update] on [dbo].[adLeadTransactions]';
GO
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadTransactions
--========================================================================================== 
ALTER TRIGGER dbo.adLeadTransactions_Audit_Update ON dbo.adLeadTransactions
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- TransCodeId 
                IF UPDATE(TransCodeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransCodeId'
                                       ,CONVERT(VARCHAR(MAX),Old.TransCodeId)
                                       ,CONVERT(VARCHAR(MAX),New.TransCodeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransCodeId <> New.TransCodeId
                                        OR (
                                             Old.TransCodeId IS NULL
                                             AND New.TransCodeId IS NOT NULL
                                           )
                                        OR (
                                             New.TransCodeId IS NULL
                                             AND Old.TransCodeId IS NOT NULL
                                           );
                    END;
                -- TransReference 
                IF UPDATE(TransReference)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransReference'
                                       ,CONVERT(VARCHAR(MAX),Old.TransReference)
                                       ,CONVERT(VARCHAR(MAX),New.TransReference)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransReference <> New.TransReference
                                        OR (
                                             Old.TransReference IS NULL
                                             AND New.TransReference IS NOT NULL
                                           )
                                        OR (
                                             New.TransReference IS NULL
                                             AND Old.TransReference IS NOT NULL
                                           );
                    END;
                -- TransDescrip 
                IF UPDATE(TransDescrip)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransDescrip'
                                       ,CONVERT(VARCHAR(MAX),Old.TransDescrip)
                                       ,CONVERT(VARCHAR(MAX),New.TransDescrip)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransDescrip <> New.TransDescrip
                                        OR (
                                             Old.TransDescrip IS NULL
                                             AND New.TransDescrip IS NOT NULL
                                           )
                                        OR (
                                             New.TransDescrip IS NULL
                                             AND Old.TransDescrip IS NOT NULL
                                           );
                    END;
                -- TransAmount 
                IF UPDATE(TransAmount)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransAmount'
                                       ,CONVERT(VARCHAR(MAX),Old.TransAmount)
                                       ,CONVERT(VARCHAR(MAX),New.TransAmount)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransAmount <> New.TransAmount
                                        OR (
                                             Old.TransAmount IS NULL
                                             AND New.TransAmount IS NOT NULL
                                           )
                                        OR (
                                             New.TransAmount IS NULL
                                             AND Old.TransAmount IS NOT NULL
                                           );
                    END;
                -- TransDate 
                IF UPDATE(TransDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransDate'
                                       ,CONVERT(VARCHAR(MAX),Old.TransDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.TransDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransDate <> New.TransDate
                                        OR (
                                             Old.TransDate IS NULL
                                             AND New.TransDate IS NOT NULL
                                           )
                                        OR (
                                             New.TransDate IS NULL
                                             AND Old.TransDate IS NOT NULL
                                           );
                    END;
                -- TransTypeId 
                IF UPDATE(TransTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.TransTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.TransTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransTypeId <> New.TransTypeId
                                        OR (
                                             Old.TransTypeId IS NULL
                                             AND New.TransTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.TransTypeId IS NULL
                                             AND Old.TransTypeId IS NOT NULL
                                           );
                    END;
                -- isEnrolled 
                IF UPDATE(isEnrolled)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'isEnrolled'
                                       ,CONVERT(VARCHAR(MAX),Old.isEnrolled)
                                       ,CONVERT(VARCHAR(MAX),New.isEnrolled)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.isEnrolled <> New.isEnrolled
                                        OR (
                                             Old.isEnrolled IS NULL
                                             AND New.isEnrolled IS NOT NULL
                                           )
                                        OR (
                                             New.isEnrolled IS NULL
                                             AND Old.isEnrolled IS NOT NULL
                                           );
                    END;
                -- CreatedDate 
                IF UPDATE(CreatedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CreatedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CreatedDate <> New.CreatedDate
                                        OR (
                                             Old.CreatedDate IS NULL
                                             AND New.CreatedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CreatedDate IS NULL
                                             AND Old.CreatedDate IS NOT NULL
                                           );
                    END;
                -- Voided 
                IF UPDATE(Voided)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'Voided'
                                       ,CONVERT(VARCHAR(MAX),Old.Voided)
                                       ,CONVERT(VARCHAR(MAX),New.Voided)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.Voided <> New.Voided
                                        OR (
                                             Old.Voided IS NULL
                                             AND New.Voided IS NOT NULL
                                           )
                                        OR (
                                             New.Voided IS NULL
                                             AND Old.Voided IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- CampusId 
                IF UPDATE(CampusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CampusId'
                                       ,CONVERT(VARCHAR(MAX),Old.CampusId)
                                       ,CONVERT(VARCHAR(MAX),New.CampusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CampusId <> New.CampusId
                                        OR (
                                             Old.CampusId IS NULL
                                             AND New.CampusId IS NOT NULL
                                           )
                                        OR (
                                             New.CampusId IS NULL
                                             AND Old.CampusId IS NOT NULL
                                           );
                    END;
                -- ReversalReason 
                IF UPDATE(ReversalReason)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ReversalReason'
                                       ,CONVERT(VARCHAR(MAX),Old.ReversalReason)
                                       ,CONVERT(VARCHAR(MAX),New.ReversalReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ReversalReason <> New.ReversalReason
                                        OR (
                                             Old.ReversalReason IS NULL
                                             AND New.ReversalReason IS NOT NULL
                                           )
                                        OR (
                                             New.ReversalReason IS NULL
                                             AND Old.ReversalReason IS NOT NULL
                                           );
                    END;
                -- DisplaySequence 
                IF UPDATE(DisplaySequence)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'DisplaySequence'
                                       ,CONVERT(VARCHAR(MAX),Old.DisplaySequence)
                                       ,CONVERT(VARCHAR(MAX),New.DisplaySequence)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.DisplaySequence <> New.DisplaySequence
                                        OR (
                                             Old.DisplaySequence IS NULL
                                             AND New.DisplaySequence IS NOT NULL
                                           )
                                        OR (
                                             New.DisplaySequence IS NULL
                                             AND Old.DisplaySequence IS NOT NULL
                                           );
                    END;
                -- SecondDisplaySequence 
                IF UPDATE(SecondDisplaySequence)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'SecondDisplaySequence'
                                       ,CONVERT(VARCHAR(MAX),Old.SecondDisplaySequence)
                                       ,CONVERT(VARCHAR(MAX),New.SecondDisplaySequence)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.SecondDisplaySequence <> New.SecondDisplaySequence
                                        OR (
                                             Old.SecondDisplaySequence IS NULL
                                             AND New.SecondDisplaySequence IS NOT NULL
                                           )
                                        OR (
                                             New.SecondDisplaySequence IS NULL
                                             AND Old.SecondDisplaySequence IS NOT NULL
                                           );
                    END;
                -- MapTransactionId 
                IF UPDATE(MapTransactionId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'MapTransactionId'
                                       ,CONVERT(VARCHAR(MAX),Old.MapTransactionId)
                                       ,CONVERT(VARCHAR(MAX),New.MapTransactionId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.MapTransactionId <> New.MapTransactionId
                                        OR (
                                             Old.MapTransactionId IS NULL
                                             AND New.MapTransactionId IS NOT NULL
                                           )
                                        OR (
                                             New.MapTransactionId IS NULL
                                             AND Old.MapTransactionId IS NOT NULL
                                           );
                    END;
                -- LeadRequirementId 
                IF UPDATE(LeadRequirementId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'LeadRequirementId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadRequirementId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadRequirementId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.LeadRequirementId <> New.LeadRequirementId
                                        OR (
                                             Old.LeadRequirementId IS NULL
                                             AND New.LeadRequirementId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadRequirementId IS NULL
                                             AND Old.LeadRequirementId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Update 
--========================================================================================== 
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping SP if exist  [dbo].[USP_SA_GetDeferredRevenueResults]';
GO
SET QUOTED_IDENTIFIER ON;
GO
SET ANSI_NULLS ON;
GO
-- =========================================================================================================
-- USP_SA_GetDeferredRevenueResults
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_SA_GetDeferredRevenueResults]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        PRINT N'Dropping Procedure [dbo].[USP_SA_GetDeferredRevenueResults]';
        DROP PROCEDURE USP_SA_GetDeferredRevenueResults;
    END;
    
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating SP  [dbo].[USP_SA_GetDeferredRevenueResults]';
GO

-- =========================================================================================================
-- USP_SA_GetDeferredRevenueResults
-- =========================================================================================================
CREATE PROCEDURE dbo.USP_SA_GetDeferredRevenueResults
/**
Author: Troy
Date: 6/8/2012
Comments: The original code was written by Anatoly when we were not using sprocs. I took the code he had in a class and moved all the logic here.
		  The UI page first allows the user to specify the report date they want to run. When they click build list, if there are no exceptions
		  then a summary by transcodes is displayed. If there are exceptions, they are displayed in a grid and the user cannot post until they have
		  cleared all the exceptions. If there are no exceptions, the user can click the Post Deferred Revenue button to do the actual posting.
		  
		  The original code returned a DataSet with several tables. However, the two tables are used in the UI are the TransCodes and the 
		  StudentExceptions. Since we can only return one result set here, the GetDeferredRevenueResults functionn in TuitionEarningsDB.vb
		  specifies the DataTable name as DeferredRevenueResultsTable. That table could be the contents of the TransCodes or the StudentExceptions
		  table. These two temp tables have a field called TableName that is set to TransCodes and StudentExceptions respectively. The UI code
		  can therefore check the TableName field of the first record to determine if the result set is for the TransCodes or the 
		  StudentExceptions.
		  
		  The mode parameter allows us to specify what function(s) we want the SP to perform. 
		  Mode=1: When the user clicks the Build List button we just want to get back a summary for each transcode. 
		  Mode=2: When the user clicks on a transcode after building the list, we want to show the details for that transcode.
		  Mode=3: When the user confirm that they want to post the deferred revenue the actual posting should be done.

**/ @ReportDate DATE = NULL
   ,@CampusId VARCHAR(50) = NULL
   ,@StudentIdentifierType VARCHAR(50) = NULL
   ,@Mode INTEGER = 1
   , --BuildList=1, BindTransCodeDetails=2 and PostDeferredRevenue=3
    @User VARCHAR(50) = NULL
   ,@CutoffTransDate DATE = NULL
AS --Testing Purposes----------------------------------------------------------------------------------------------------------
--DECLARE @ReportDate DATETIME
--DECLARE @CampusId VARCHAR(50)
--DECLARE @CutoffTransDate DATETIME
--DECLARE @StudentIdentifierType VARCHAR(50)
--DECLARE @PostDeferredRevenue BIT
--DECLARE @User VARCHAR(50)
--DECLARE @Mode INTEGER
-----------------------------------------------------------------------------------------------------------------------------

    DECLARE @TransactionId UNIQUEIDENTIFIER;
    DECLARE @StudentName VARCHAR(200);
    DECLARE @StudentIdentifier VARCHAR(50);
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @TransCodeId UNIQUEIDENTIFIER;
    DECLARE @TransAmount DECIMAL(18,2);
    DECLARE @LOAStartDate DATETIME;
    DECLARE @LOAEndDate DATETIME;
    DECLARE @Earned DECIMAL(18,2);
    DECLARE @FeeLevelId INT;
    DECLARE @TermStart DATETIME;
    DECLARE @TermEnd DATETIME;
    DECLARE @percentToEarnIdx VARCHAR(50);
    DECLARE @RemainingMethod INT;
    DECLARE @DeferredRevenue DECIMAL(18,2);


----------------Testing Purposes----------------------------------------------------------------------------------------------
--SET @ReportDate='3/31/2012'
--SET @CampusId='436BF45E-5420-4C1F-8687-CD32A60D64BE'
--SET @StudentIdentifier='ssn'
--SET @PostDeferredRevenue=1
--SET @User = 'sa'
--SET @Mode=2
-------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------
--This section creates the supporting temp tables
-----------------------------------------------------------------------------------------------------------------------------
--Transactions Table
    CREATE TABLE #Transactions
        (
         TransactionId UNIQUEIDENTIFIER NOT NULL
                                        PRIMARY KEY
        ,StudentName VARCHAR(200) NOT NULL
        ,StudentIdentifier VARCHAR(50) NULL
        ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
        ,TransCodeId UNIQUEIDENTIFIER NULL
        ,TransAmount DECIMAL(18,2) NOT NULL
        ,LOAStartDate DATETIME NULL
        ,LOAEndDate DATETIME NULL
        ,Earned DECIMAL(18,2) NULL
        ,FeeLevelId INT NULL
        ,TermStart DATETIME NULL
        ,TermEnd DATETIME NULL
        ,percentToEarnIdx INT NULL
        ,RemainingMethod INT NULL
        ,DeferredRevenue DECIMAL(18,2) NULL
        ); 


--TransCodes Table
    CREATE TABLE #TransCodes
        (
         TransCodeId UNIQUEIDENTIFIER NOT NULL
                                      PRIMARY KEY
        ,TransCodeDescrip VARCHAR(100) NOT NULL
        ,TotalDeferredRevenue DECIMAL(18,2) NULL
        ,AccruedAmount DECIMAL(18,2) NULL
        ,DeferredRevenueAmount DECIMAL(18,2) NULL
        ,TotalAmount DECIMAL(18,2) NULL
        ,TableName VARCHAR(50) NOT NULL
        );


--Enrollments Table
    CREATE TABLE #StuEnrollments
        (
         StuEnrollId UNIQUEIDENTIFIER NOT NULL
                                      PRIMARY KEY
        ,StudentId UNIQUEIDENTIFIER NOT NULL
        ,PrgVerId UNIQUEIDENTIFIER NOT NULL
        ,StartDate DATETIME NOT NULL
        ,ExpGradDate DATETIME NULL
        ,DropReasonId UNIQUEIDENTIFIER NULL
        ,DateDetermined DATETIME NULL
        ,SysSTatusID INT NULL
        ,LDA DATETIME NULL
        );


--Program Versions Table
    CREATE TABLE #PrgVersions
        (
         PrgVerId UNIQUEIDENTIFIER NOT NULL
                                   PRIMARY KEY
        ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
        ,Weeks SMALLINT NOT NULL
        ,Hours DECIMAL(9,2) NOT NULL
        ,Credits DECIMAL(9,2) NOT NULL
        );


--Tuition Earnings Table
    CREATE TABLE #TuitionEarnings
        (
         TuitionEarningId UNIQUEIDENTIFIER NOT NULL
                                           PRIMARY KEY
        ,FullMonthBeforeDay TINYINT NULL
        ,HalfMonthBeforeDay TINYINT NULL
        ,RevenueBasisIdx TINYINT NOT NULL
        ,PercentageRangeBasisIdx TINYINT NOT NULL
        ,PercentToEarnIdx TINYINT NOT NULL
        ,TakeHolidays BIT NULL
        ,RemainingMethod BIT NULL
        );


    CREATE TABLE #TuitionEarningsPercentageRanges
        (
         TuitionEarningsPercentageRangeId UNIQUEIDENTIFIER NOT NULL
                                                           PRIMARY KEY
        ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
        ,UpTo DECIMAL(5,2) NOT NULL
        ,EarnPercent DECIMAL(5,2) NOT NULL
        );


    CREATE TABLE #Results
        (
         ResultId UNIQUEIDENTIFIER NOT NULL
                                   PRIMARY KEY
        ,ClsSectionId UNIQUEIDENTIFIER NOT NULL
        ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
        ,Credits DECIMAL(9,2) NULL
        ,IsPass BIT NULL
        ,IsCreditsEarned BIT NULL
        ,IsCreditsAttempted BIT NULL
        );


    CREATE TABLE #Reqs
        (
         PrgVerId UNIQUEIDENTIFIER NOT NULL
        ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
        ,Descrip VARCHAR(150) NOT NULL
        ,IsRequired BIT NOT NULL
        );


--Exceptions Table
    CREATE TABLE #StudentExceptions
        (
         StudentName VARCHAR(200) NOT NULL
        ,StuEnrollId VARCHAR(50) NOT NULL
        ,FeeLevelId INT NULL
        ,TermStart DATETIME NULL
        ,TermEnd DATETIME NULL
        ,TransCode VARCHAR(100) NULL
        ,TransAmount VARCHAR(50) NULL
        ,Exception VARCHAR(250) NULL
        ,TableName VARCHAR(50) NOT NULL
        ); 

--Table to be used for inserting records into saDeferredRevenues
    CREATE TABLE #DeferredRevenues
        (
         DefRevenueId UNIQUEIDENTIFIER NOT NULL
                                       PRIMARY KEY
        ,DefRevenueDate DATETIME NOT NULL
        ,TransactionId UNIQUEIDENTIFIER NOT NULL
        ,Type BIT NOT NULL
        ,Source TINYINT NOT NULL
        ,Amount MONEY NOT NULL
        ,IsPosted BIT NOT NULL
        ,ModUser VARCHAR(50) NOT NULL
        ,ModDate DATETIME NOT NULL
        );


--Table to be used for displaying the details of a transcode selected on the left hand side



----------------------------------------------------------------------------------------------------------------------------------------------
--Populate the Temp Tables
-----------------------------------------------------------------------------------------------------------------------------------------------
--Transactions Table
    INSERT  INTO #Transactions
            SELECT  T.TransactionId
                   ,(
                      SELECT    FirstName + ' ' + LastName
                      FROM      arStudent S
                      WHERE     StudentId = (
                                              SELECT    StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = (
                                                                        SELECT  StuEnrollId
                                                                        FROM    saTransactions
                                                                        WHERE   TransactionId = T.TransactionId
                                                                                AND saTransactions.Voided = 0
                                                                      )
                                            )
                    ) AS StudentName
                   ,( CASE WHEN UPPER(@StudentIdentifier) = 'SSN' THEN (
                                                                         SELECT COALESCE(CASE LEN(S.SSN)
                                                                                           WHEN 9 THEN '***-**-' + SUBSTRING(SSN,6,4)
                                                                                           ELSE ' '
                                                                                         END,' ')
                                                                         FROM   arStudent S
                                                                         WHERE  StudentId = (
                                                                                              SELECT    StudentId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StuEnrollId = (
                                                                                                                        SELECT  StuEnrollId
                                                                                                                        FROM    saTransactions
                                                                                                                        WHERE   TransactionId = T.TransactionId
                                                                                                                                AND saTransactions.Voided = 0
                                                                                                                      )
                                                                                            )
                                                                       )
                           WHEN LOWER(@StudentIdentifier) = 'studentid'
                           THEN (
                                  SELECT    COALESCE(StudentNumber,' ')
                                  FROM      arStudent S
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = (
                                                                                    SELECT  StuEnrollId
                                                                                    FROM    saTransactions
                                                                                    WHERE   TransactionId = T.TransactionId
                                                                                            AND saTransactions.Voided = 0
                                                                                  )
                                                        )
                                )
                           ELSE (
                                  SELECT    ''
                                  FROM      arStudent S
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = (
                                                                                    SELECT  StuEnrollId
                                                                                    FROM    saTransactions
                                                                                    WHERE   TransactionId = T.TransactionId
                                                                                            AND saTransactions.Voided = 0
                                                                                  )
                                                        )
                                )
                      END ) AS StudentIdentifier
                   ,T.StuEnrollId
                   ,T.TransCodeId
                   ,T.TransAmount
                   ,(
                      SELECT TOP 1
                                StartDate
                      FROM      arStudentLOAs
                      WHERE     StuEnrollId = T.StuEnrollId
                      ORDER BY  StartDate DESC
                    ) AS LOAStartDate
                   ,(
                      SELECT TOP 1
                                EndDate
                      FROM      arStudentLOAs
                      WHERE     StuEnrollId = T.StuEnrollId
                      ORDER BY  EndDate DESC
                    ) AS LOAEndDate
                   ,COALESCE((
                               SELECT   SUM(Amount)
                               FROM     saDeferredRevenues
                               WHERE    TransactionId = T.TransactionId
                             ),0) Earned
                   ,T.FeeLevelId
                   ,(
                      SELECT    StartDate
                      FROM      arTerm tm
                      WHERE     tm.TermId = T.TermId
                    ) AS TermStart
                   ,(
                      SELECT    EndDate
                      FROM      arTerm tm
                      WHERE     tm.TermId = T.TermId
                    ) AS TermEnd
                   ,te.PercentToEarnIdx AS percentToEarnIdx
                   ,te.RemainingMethod AS RemainingMethod
                   ,0.00 AS DeferredRevenue
            FROM    saTransactions T
                   ,saTransCodes TC
                   ,arStuEnrollments SE
                   ,saTransTypes S
                   ,dbo.arPrgVersions pv
                   ,dbo.saTuitionEarnings te
            WHERE   ( T.TransCodeId = TC.TransCodeId )
                    AND TC.DefEarnings = 1
                    AND SE.StuEnrollId = T.StuEnrollId
                    AND T.Voided = 0
                    AND T.TransTypeId = S.TransTypeId
                    AND T.TransTypeId <> 2
                    AND SE.StartDate <= @ReportDate
                    AND T.TransDate <= @ReportDate
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId)
                    AND T.TransDate >= ISNULL(@CutoffTransDate,T.TransDate)
                    AND SE.PrgVerId = pv.PrgVerId
                    AND pv.TuitionEarningId = te.TuitionEarningId;
					--AND	SE.StuEnrollId='40375EF8-2CE3-464E-923C-2E2F8E38AD86'; --Scott Graham
		--AND	   T.TransactionId='C2CB1BDB-A6E3-4DC6-92D5-7935250CCD74' --Dawn Higgins Tuition Charged on 2012-03-05 00:00:00.000
		--AND		SE.StuEnrollId='B527B234-4961-46CA-A108-E5D6960F7E6D' --Ammanda Medina (Expelled on 8/2/2011 LDA 7/19/2011)
		--AND		T.TransactionId='8AAD7B80-C082-41E8-8F3D-9CF058929D67' --Ammanda Medina Tuition charged on 5/2/2011
		--And T.StuEnrollId='67887763-86CC-4829-90BA-B01C056191A0' --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.TransactionId='4805ED14-748D-4660-B60B-D16228BFF180'  --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.StuEnrollId='04F8E9F3-BA4C-4A3F-9C35-2365FD8BC5C7' --Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--And T.TransactionId='BCDA9AB4-C465-4B99-BDCC-9DE2F3ABFF7B'	--Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--AND T.StuEnrollId='B7FF0506-98E2-463E-97B9-1019AEC2F2A6'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.TransactionId='30E7EE3D-ACC2-47F4-BEEC-002EC4D09B20'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.StuEnrollId='CFE46667-C6EC-4EBB-AFB3-EA354881371F'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.TransactionId='78C35984-9EF3-4D3C-B5BC-001A862AA2C6'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.StuEnrollId='2923660B-5C3E-4962-B30B-920F05472D19'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400
		--AND T.TransactionId='04AD729A-F8C3-4821-A6F0-6EC6A7DD5ACA'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400

--TransCodes Table
    INSERT  INTO #TransCodes
            (
             TransCodeId
            ,TransCodeDescrip
            ,TableName
            ,TotalDeferredRevenue
            ,AccruedAmount
            ,DeferredRevenueAmount
            ,TotalAmount
            )
            SELECT  TC.TransCodeId
                   ,TC.TransCodeDescrip
                   ,'TransCodes' AS TableName
                   ,0.00 AS TotalDeferredRevenue
                   ,0.00 AS AccruedAmount
                   ,0.00 AS DeferredRevenueAmount
                   ,SUM(T.TransAmount) TotalAmount
            FROM    saTransCodes TC
                   ,saTransactions T
                   ,saTransTypes S
            WHERE   ( T.TransCodeId = TC.TransCodeId )
                    AND ( TC.DefEarnings = 1 )
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId)
                    AND T.Voided = 0
                    AND T.TransTypeId = S.TransTypeId
                    AND T.TransTypeId <> 2
            GROUP BY TC.TransCodeId
                   ,TC.TransCodeDescrip;
	
	--SELECT ((12074*90)/100)*100/736
--Enrollments Table
    INSERT  INTO #StuEnrollments
            SELECT DISTINCT
                    SE.StuEnrollId
                   ,SE.StudentId
                   ,SE.PrgVerId
                   ,SE.StartDate
                   ,SE.ExpGradDate
                   ,SE.DropReasonId
                   ,SE.DateDetermined
                   ,(
                      SELECT    SysStatusId
                      FROM      syStatusCodes
                      WHERE     StatusCodeId IN ( SELECT    StatusCodeId
                                                  FROM      arStuEnrollments
                                                  WHERE     StuEnrollId = SE.StuEnrollId )
                    ) SysSTatusID
                   ,(
                      SELECT    MAX(LDA)
                      FROM      (
                                  SELECT    MAX(AttendedDate) AS LDA
                                  FROM      arExternshipAttendance
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                  UNION ALL
                                  SELECT    MAX(MeetDate) AS LDA
                                  FROM      atClsSectAttendance
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND Actual >= 1
                                  UNION ALL
                                  SELECT    MAX(AttendanceDate) AS LDA
                                  FROM      atAttendance
                                  WHERE     EnrollId = SE.StuEnrollId
                                            AND Actual >= 1
                                  UNION ALL
                                  SELECT    MAX(RecordDate) AS LDA
                                  FROM      arStudentClockAttendance
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  ActualHours >= 1.00
                                                  AND ActualHours <> 99.00
                                                  AND ActualHours <> 999.00
                                                  AND ActualHours <> 9999.00
                                                )
                                  UNION ALL
                                  SELECT    MAX(MeetDate) AS LDA
                                  FROM      atConversionAttendance
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  Actual >= 1.00
                                                  AND Actual <> 99.00
                                                  AND Actual <> 999.00
                                                  AND Actual <> 9999.00
                                                )
                                  UNION ALL
                                  SELECT    LDA
                                  FROM      arStuEnrollments
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                ) T1
                    ) AS LDA
            FROM    arStuEnrollments SE
                   ,saTransactions T
                   ,saTransCodes TC
            WHERE   ( SE.StuEnrollId = T.StuEnrollId )
                    AND ( T.TransCodeId = TC.TransCodeId )
                    AND TC.DefEarnings = 1
                    AND T.Voided = 0
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId); 



--Program Versions Table
    INSERT  INTO #PrgVersions
            SELECT DISTINCT
                    PV.PrgVerId
                   ,PV.TuitionEarningId
                   ,PV.Weeks
                   ,PV.Hours
                   ,PV.Credits
            FROM    arPrgVersions PV
                   ,arStuEnrollments SE
                   ,saTransactions T
                   ,saTransCodes TC
            WHERE   ( PV.PrgVerId = SE.PrgVerId )
                    AND ( SE.StuEnrollId = T.StuEnrollId )
                    AND ( T.TransCodeId = TC.TransCodeId )
                    AND ( TC.DefEarnings = 1 )
                    AND T.Voided = 0
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId); 


--Tuition Earnings Table
    INSERT  INTO #TuitionEarnings
            SELECT DISTINCT
                    TE.TuitionEarningId
                   ,TE.FullMonthBeforeDay
                   ,TE.HalfMonthBeforeDay
                   ,TE.RevenueBasisIdx
                   ,TE.PercentageRangeBasisIdx
                   ,TE.PercentToEarnIdx
                   ,TE.TakeHolidays
                   ,TE.RemainingMethod
            FROM    saTuitionEarnings TE
                   ,arPrgVersions PV
                   ,arStuEnrollments SE
                   ,saTransactions T
                   ,saTransCodes TC
            WHERE   ( TE.TuitionEarningId = PV.TuitionEarningId )
                    AND ( PV.PrgVerId = SE.PrgVerId )
                    AND ( SE.StuEnrollId = T.StuEnrollId )
                    AND ( T.TransCodeId = TC.TransCodeId )
                    AND ( TC.DefEarnings = 1 )
                    AND T.Voided = 0
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId);


--TuitionEarningsPercentageRanges Table
    INSERT  INTO #TuitionEarningsPercentageRanges
            SELECT DISTINCT
                    TEPR.TuitionEarningsPercentageRangeId
                   ,TE.TuitionEarningId
                   ,TEPR.UpTo
                   ,TEPR.EarnPercent
            FROM    saTuitionEarningsPercentageRanges TEPR
                   ,saTuitionEarnings TE
                   ,arPrgVersions PV
                   ,arStuEnrollments SE
                   ,saTransactions T
                   ,saTransCodes TC
            WHERE   ( TEPR.TuitionEarningId = TE.TuitionEarningId )
                    AND ( TE.TuitionEarningId = PV.TuitionEarningId )
                    AND ( PV.PrgVerId = SE.PrgVerId )
                    AND ( SE.StuEnrollId = T.StuEnrollId )
                    AND ( T.TransCodeId = TC.TransCodeId )
                    AND ( TC.DefEarnings = 1 )
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId)
            ORDER BY TEPR.UpTo; 


--Results Table
    INSERT  INTO #Results
            SELECT DISTINCT
                    R.ResultId
                   ,CS.ClsSectionId
                   ,R.StuEnrollId
                   ,RQ.Credits
                   ,(
                      SELECT    IsPass
                      FROM      arGradeSystemDetails
                      WHERE     GrdSysDetailId = R.GrdSysDetailId
                    ) AS IsPass
                   ,(
                      SELECT    IsCreditsEarned
                      FROM      arGradeSystemDetails
                      WHERE     GrdSysDetailId = R.GrdSysDetailId
                    ) AS IsCreditsEarned
                   ,(
                      SELECT    IsCreditsAttempted
                      FROM      arGradeSystemDetails
                      WHERE     GrdSysDetailId = R.GrdSysDetailId
                    ) AS IsCreditsAttempted
            FROM    saTransactions T
                   ,saTransCodes TC
                   ,arResults R
                   ,arClassSections CS
                   ,arReqs RQ
            WHERE   T.TransCodeId = TC.TransCodeId
                    AND TC.DefEarnings = 1
                    AND T.Voided = 0
                    AND T.StuEnrollId = R.StuEnrollId
                    AND R.TestId = CS.ClsSectionId
                    AND CS.ReqId = RQ.ReqId
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId);


--Reqs Table
    INSERT  INTO #Reqs
            SELECT DISTINCT
                    PV.PrgVerId
                   ,PV.TuitionEarningId
                   ,R.Descrip
                   ,IsRequired
            FROM    arPrgVersions PV
                   ,arStuEnrollments SE
                   ,saTransactions T
                   ,saTransCodes TC
                   ,arProgVerDef PVD
                   ,arReqs R
            WHERE   PV.PrgVerId = SE.PrgVerId
                    AND SE.PrgVerId = PVD.PrgVerId
                    AND SE.StuEnrollId = T.StuEnrollId
                    AND T.TransCodeId = TC.TransCodeId
                    AND TC.DefEarnings = 1
                    AND T.Voided = 0
                    AND PVD.ReqId = R.ReqId
                    AND T.CampusId = ISNULL(@CampusId,T.CampusId); 





-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This section is where the calculations are performed.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    DECLARE transCursor CURSOR FAST_FORWARD FORWARD_ONLY
    FOR
        SELECT  *
        FROM    #Transactions;
	

    OPEN transCursor;
    FETCH NEXT FROM transCursor
INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,@TermStart,@TermEnd,
        @percentToEarnIdx,@RemainingMethod,@DeferredRevenue;


    WHILE @@FETCH_STATUS = 0
        BEGIN
	
		--Get the status of the student, if the student is of nostart status and it was changed from currently attending
		--Then get the date they modified the status
		--Post deferred revenue for the student for the remaining amount in the next month and then donot post deferred revenue at all, once they have earned all the revenue
		--Modified on july 27 2009
            DECLARE @nostartdate DATETIME;
            DECLARE @ExceptionNo INTEGER;
            DECLARE @TransCodeDescrip VARCHAR(100);
            DECLARE @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists AS BIT;
            DECLARE @ExitFor AS BIT;
            DECLARE @halfMonthBeforeDay AS INTEGER;
            DECLARE @fullMonthBeforeDay AS INTEGER;
		
            SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 0;
            SET @ExitFor = 0;
		
            SET @nostartdate = '1/1/1800';
		
            IF (
                 SELECT SysSTatusID
                 FROM   #StuEnrollments d
                 WHERE  d.StuEnrollId = @StuEnrollId
                        AND d.SysSTatusID = 8
               ) IS NOT NULL
                BEGIN
                    SET @nostartdate = (
                                         SELECT TOP 1
                                                ModDate
                                         FROM   syStudentStatusChanges
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND NewStatusId IN ( SELECT StatusCodeId
                                                                     FROM   syStatusCodes
                                                                     WHERE  SysStatusId = 8 )
                                                AND OrigStatusId IN ( SELECT    StatusCodeId
                                                                      FROM      syStatusCodes
                                                                      WHERE     SysStatusId = 9 )
                                       ); 
			
                END;
		
		--This is the calculated duration in months of the term
            DECLARE @termStart1 DATE;
            DECLARE @termEnd1 DATE;
		
            SET @termStart1 = '1/1/1800';
            SET @termEnd1 = '1/1/1800';
		
		--Calculate last date of attendance
            DECLARE @lda DATE;
		
            IF (
                 SELECT DropReasonId
                 FROM   #StuEnrollments
                 WHERE  StuEnrollId = @StuEnrollId
               ) IS NULL
                BEGIN
                    SET @lda = '1/1/5000'; --Use 1/1/5000 as a max date				
                END;	
				
            ELSE
                BEGIN
                    IF (
                         SELECT DateDetermined
                         FROM   #StuEnrollments
                         WHERE  StuEnrollId = @StuEnrollId
                       ) IS NULL
                        BEGIN
                            IF (
                                 SELECT LDA
                                 FROM   #StuEnrollments
                                 WHERE  StuEnrollId = @StuEnrollId
                               ) IS NOT NULL
                                BEGIN
                                    SET @lda = (
                                                 SELECT LDA
                                                 FROM   #StuEnrollments
                                                 WHERE  StuEnrollId = @StuEnrollId
                                               );
                                END;
                            ELSE
                                BEGIN
                                    SET @lda = '1/1/1800';
                                END;
				
                        END;
                    ELSE
                        BEGIN
                            SET @lda = (
                                         SELECT DateDetermined
                                         FROM   #StuEnrollments
                                         WHERE  StuEnrollId = @StuEnrollId
                                       );
                        END;
				
                END;
		
		
            IF @lda = '1/1/1800'
                BEGIN
				--Exception date determined is null for this student
				--Add a record to the exceptions table
                    SET @ExceptionNo = 3;
								
                    SET @TransCodeDescrip = (
                                              SELECT    TransCodeDescrip
                                              FROM      #TransCodes tc
                                              WHERE     tc.TransCodeId = @TransCodeId
                                            );
				
                    INSERT  INTO #StudentExceptions
                    VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                              'The student has a drop reason and no date determined.','StudentExceptions' );
			
                END;
		
            ELSE
                BEGIN			
		
                    IF @lda > @ReportDate
                        BEGIN
                            SET @lda = '1/1/5000';
                        END; 
				 
                    DECLARE @FeeLevelId1 INT;
                    SET @FeeLevelId1 = 0;
				
                    IF @FeeLevelId IS NOT NULL
                        BEGIN
                            SET @FeeLevelId1 = @FeeLevelId;
                        END;
				 
                    DECLARE @strTransactionID VARCHAR(50);
                    SET @strTransactionID = RTRIM(CONVERT(VARCHAR(50),@TransactionId));
				
                    DECLARE @defrevPostedDate DATE;
				
                    SET @defrevPostedDate = (
                                              SELECT    MAX(DefRevenueDate)
                                              FROM      saDeferredRevenues
                                              WHERE     TransactionId = @TransactionId
                                            );
				
				
				--Modified by Saraswathi On June 03 2009
				--1- Term and 3- Course 2- Program level
				-- Course added to function as term.
                    IF (
                         @FeeLevelId1 = 1
                         OR @FeeLevelId1 = 3
                       )
                        BEGIN
					
                            SET @termStart1 = @TermStart;
                            SET @termEnd1 = (
                                              SELECT    dbo.MinDate(@TermEnd,@lda)
                                            );
											
                            IF @nostartdate <> '1/1/1800'
                                BEGIN					
                                    SET @termEnd1 = (
                                                      SELECT    dbo.MinDate(@termEnd1,@nostartdate)
                                                    );				
                                END;
						
                        END;
					
				--Case 2 is program version
				--Modified by Saraswathi On June 3 2009
                    IF @FeeLevelId1 = 2
                        BEGIN
                            SET @termStart1 = (
                                                SELECT  StartDate
                                                FROM    #StuEnrollments
                                                WHERE   StuEnrollId = @StuEnrollId
                                              );
					
                            IF (
                                 SELECT ExpGradDate
                                 FROM   #StuEnrollments
                                 WHERE  StuEnrollId = @StuEnrollId
                               ) IS NULL
                                BEGIN
                                    DECLARE @programDurationInWeeks INT;
                                    SET @programDurationInWeeks = (
                                                                    SELECT  Weeks
                                                                    FROM    #StuEnrollments se
                                                                           ,#PrgVersions pv
                                                                    WHERE   se.PrgVerId = pv.PrgVerId
                                                                            AND se.StuEnrollId = @StuEnrollId
                                                                  );
							
                                    SET @termEnd1 = (
                                                      SELECT    dbo.MinDate(DATEADD(DAY,@programDurationInWeeks * 7,@termStart1),@lda)
                                                    ); 
							
                                END;
                            ELSE
                                BEGIN
                                    DECLARE @ExpGradDate DATE;
                                    SET @ExpGradDate = (
                                                         SELECT ExpGradDate
                                                         FROM   #StuEnrollments
                                                         WHERE  StuEnrollId = @StuEnrollId
                                                       );
                                    SET @termEnd1 = (
                                                      SELECT    dbo.MinDate(@ExpGradDate,@lda)
                                                    );
							
							
                                END;	
							
                            IF @nostartdate <> '1/1/1800'
                                BEGIN
                                    SET @termEnd1 = (
                                                      SELECT    dbo.MinDate(@termEnd1,@nostartdate)
                                                    );
                                END;
					
					
                        END;			
				
					
				
                    IF ( @FeeLevelId1 = 0 ) --No FeeLevelId is on the Transaction Record so add record to the exceptions table	
                        BEGIN
                            SET @ExceptionNo = 1;
										
                            SET @TransCodeDescrip = (
                                                      SELECT    TransCodeDescrip
                                                      FROM      #TransCodes tc
                                                      WHERE     tc.TransCodeId = @TransCodeId
                                                    );
						
                            INSERT  INTO #StudentExceptions
                            VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                                      'There is no Fee Level Id defined for the student','StudentExceptions' );
					
                        END;
			
                    ELSE
                        BEGIN
                            DECLARE @termDuration DECIMAL;
                            DECLARE @NewtermDuration DECIMAL;
                            DECLARE @RemainingMethod1 BIT;
						
                            SET @termDuration = 0;
                            SET @NewtermDuration = 0;
						
                            SET @RemainingMethod1 = (
                                                      SELECT    RemainingMethod
                                                      FROM      #StuEnrollments se
                                                               ,#PrgVersions pv
                                                               ,#TuitionEarnings te
                                                      WHERE     se.PrgVerId = pv.PrgVerId
                                                                AND pv.TuitionEarningId = te.TuitionEarningId
                                                                AND se.StuEnrollId = @StuEnrollId
                                                    );	
												
							
                            --IF @termStart1 = @termEnd1
                            --    BEGIN
                            --        SET @ExceptionNo = 2;
						
                            --        SET @TransCodeDescrip = (
                            --                                  SELECT    TransCodeDescrip
                            --                                  FROM      #TransCodes tc
                            --                                  WHERE     tc.TransCodeId = @TransCodeId
                            --                                );
						
                            --        INSERT  INTO #StudentExceptions
                            --        VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                            --                  'Term Start Date and Term End Date/Date Determined/LDA are the same','StudentExceptions' );
						
					
                            --    END;	
							
                            --ELSE
                            IF @termStart1 <> @termEnd1
                                BEGIN
								--Added by Saraswathi lakshmanan On July 1 2009
								--check whether to apply a straight percentage or a conversion table
                                    DECLARE @DeferredRevenuePostingBasedonHolidays VARCHAR(5);
								
                                    IF (
                                         SELECT TakeHolidays
                                         FROM   #StuEnrollments se
                                               ,#PrgVersions pv
                                               ,#TuitionEarnings te
                                         WHERE  se.StuEnrollId = @StuEnrollId
                                                AND se.PrgVerId = pv.PrgVerId
                                                AND pv.TuitionEarningId = te.TuitionEarningId
                                       ) IS NOT NULL
                                        BEGIN
                                            IF (
                                                 SELECT TakeHolidays
                                                 FROM   #StuEnrollments se
                                                       ,#PrgVersions pv
                                                       ,#TuitionEarnings te
                                                 WHERE  se.StuEnrollId = @StuEnrollId
                                                        AND se.PrgVerId = pv.PrgVerId
                                                        AND pv.TuitionEarningId = te.TuitionEarningId
                                               ) = 1
                                                BEGIN
                                                    SET @DeferredRevenuePostingBasedonHolidays = 'yes';																				
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @DeferredRevenuePostingBasedonHolidays = 'no';												
                                                END;
									
									
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @DeferredRevenuePostingBasedonHolidays = 'no';
									
                                        END;
									
									
                                    DECLARE @newStartDate AS DATE;
                                    SET @newStartDate = (
                                                          SELECT    dbo.MaxDate(@termStart1,DATEADD(DAY,1,@defrevPostedDate))
                                                        );
								
                                    DECLARE @startDate DATE; 
                                    SET @startDate = @termStart1;
								
								--Code modified by Saraswathi Lakshmanan on Sept 10 2010
								--INclude LOA to elapsed time and term duration only if LOA is between the elapsed time period.
                                    DECLARE @Trad_numberofHOlidaysbetweentheGivenPeriod AS INTEGER;
                                    DECLARE @Rem_numberofHOlidaysbetweentheGivenPeriod AS INTEGER;
                                    DECLARE @Trad_numberofLOAdaysbetweentheGivenPeriod AS INTEGER;
                                    DECLARE @Rem_numberofLOAdaysbetweentheGivenPeriod AS INTEGER; 								
                                    DECLARE @NumberOfHolidaysBetweenTerm AS INTEGER;
								
                                    SET @Trad_numberofHOlidaysbetweentheGivenPeriod = (
                                                                                        SELECT  dbo.NumberofHolidaysBetweentheGivenPeriod(@ReportDate,@startDate,
                                                                                                                                          @CampusId)
                                                                                      );					
                                    SET @Rem_numberofHOlidaysbetweentheGivenPeriod = (
                                                                                       SELECT   dbo.NumberofHolidaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                          @newStartDate,
                                                                                                                                          @CampusId)
                                                                                     );
                                    SET @Trad_numberofLOAdaysbetweentheGivenPeriod = (
                                                                                       SELECT   dbo.NumberofLOADaysBetweentheGivenPeriod(@ReportDate,@startDate,
                                                                                                                                         @StuEnrollId)
                                                                                     );
                                    SET @Rem_numberofLOAdaysbetweentheGivenPeriod = (
                                                                                      SELECT    dbo.NumberofLOADaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                         @newStartDate,
                                                                                                                                         @StuEnrollId)
                                                                                    );
                                    SET @NumberOfHolidaysBetweenTerm = (
                                                                         SELECT dbo.NumberofHolidaysBetweentheGivenPeriod(@TermEnd,@startDate,@CampusId)
                                                                       );
																								
                                    IF @termStart1 <> '1/1/1800'
                                        AND @termEnd1 <> '1/1/1800'
                                        BEGIN										
                                            IF @percentToEarnIdx = 1
                                                BEGIN												
												--If it is a equal percentage every month  Then there is no holidays into considereation
												--termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                                                    SET @termDuration = ( DATEDIFF(MONTH,@startDate,@termEnd1) + 1 ) * 100;
                                                    SET @NewtermDuration = ( DATEDIFF(MONTH,@newStartDate,@termEnd1) + 1 ) * 100;
                                                    SET @DeferredRevenuePostingBasedonHolidays = 'no';
											
                                                END;
                                            ELSE
                                                BEGIN												
                                                    IF ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                        BEGIN														
                                                            SET @termDuration = 0;
                                                        END;
                                                    ELSE
                                                        BEGIN														
                                                            SET @termDuration = ( ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0
                                                                                    - @Trad_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
														
                                                        END;
													
                                                    IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0 - @Rem_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                        BEGIN
                                                            SET @NewtermDuration = 0;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @NewtermDuration = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0
                                                                                       - @Rem_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
													
                                                        END;
												
                                                    IF LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
                                                        BEGIN
                                                            IF ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                 - @NumberOfHolidaysBetweenTerm ) <= 0
                                                                BEGIN
                                                                    SET @termDuration = 0;
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @termDuration = ( ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0
                                                                                            - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                                            - @NumberOfHolidaysBetweenTerm ) * 100 ) / 30;
                                                                END;
															
                                                            IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0 - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                 - @Rem_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                                BEGIN
                                                                    SET @NewtermDuration = 0;
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @NewtermDuration = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0
                                                                                               - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                                               - @Rem_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                END;
															
													
                                                        END;
													
													
												
                                                END;
											
											
									
									
                                        END;
									
                                    DECLARE @EarnedRevenue AS DECIMAL;
                                    DECLARE @UnearnedRevenue AS DECIMAL;
								
                                    SET @EarnedRevenue = ISNULL((
                                                                  SELECT    SUM(Amount)
                                                                  FROM      saDeferredRevenues
                                                                  WHERE     TransactionId = @TransactionId
                                                                ),0);
                                    SET @UnearnedRevenue = @TransAmount - @EarnedRevenue;								
								
								--ignore all calculations if the termDuration is zero or negative
								--If percentToEarnIdx = 0 And Not (termDuration > 0) Then Exit For
								--Modified by Saraswathi on July 1 2009								
                                    IF ( @RemainingMethod = 0 )
                                        BEGIN
                                            IF (
                                                 @percentToEarnIdx = 0
                                                 OR @percentToEarnIdx = 1
                                               )
                                                AND (
                                                      @termDuration <= 0
                                                      AND @UnearnedRevenue > 0
                                                    )
                                                BEGIN
                                                    SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1;
												
                                                END;
                                            ELSE
                                                BEGIN
                                                    IF (
                                                         @percentToEarnIdx = 0
                                                         OR @percentToEarnIdx = 1
                                                       )
                                                        AND NOT ( @termDuration > 0 )
                                                        AND ( @UnearnedRevenue < 0 )
                                                        BEGIN
                                                            SET @ExitFor = 1;	
                                                        END;
																				
                                                END;									
                                        END;
								
                                    IF @RemainingMethod = 1
                                        BEGIN
                                            IF (
                                                 @percentToEarnIdx = 0
                                                 OR @percentToEarnIdx = 1
                                               )
                                                AND (
                                                      @NewtermDuration <= 0
                                                      AND @UnearnedRevenue > 0
                                                    )
                                                BEGIN
                                                    SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1;									
                                                END;
                                            ELSE
                                                BEGIN
                                                    IF (
                                                         @percentToEarnIdx = 0
                                                         OR @percentToEarnIdx = 1
                                                       )
                                                        AND NOT ( @NewtermDuration > 0 )
                                                        AND ( @UnearnedRevenue < 0 )
                                                        BEGIN
                                                            SET @ExitFor = 1;
                                                        END;										
												
                                                END;
									
                                        END;
								
								--The rest of the code should only execute if @EXITFOR is still 0
                                    IF @ExitFor = 0
                                        BEGIN
										--this is the elapsed time in months from the beginning of the period to the report date
                                            DECLARE @elapsedTime AS DECIMAL = 0.00;
                                            DECLARE @newElapsedTime AS DECIMAL = 0.00;
										
                                            IF LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
                                                BEGIN
                                                    IF ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                         - @Trad_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                        BEGIN
                                                            SET @elapsedTime = 0;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @elapsedTime = ( ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 0
                                                                                   - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                                   - @Trad_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                        END;
													
                                                    IF @RemainingMethod = 1
                                                        BEGIN
                                                            IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 0 - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                 - @Rem_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                                BEGIN
                                                                    SET @newElapsedTime = 0;
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @newElapsedTime = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 0
                                                                                              - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                                              - @Rem_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                END;
                                                        END;
													
													
                                                END;
                                            ELSE
                                                BEGIN												
												
                                                    IF ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                        BEGIN														
                                                            SET @elapsedTime = 0;
                                                        END;
                                                    ELSE
                                                        BEGIN														
														--The original code from the VB class used (DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod)/100
														--However, in SQL Server the value returned is 0 rather than 0.90. For SQL Server we have to first mulitply by 100 and then remember
														--later on to divide by 100 since here we are now going to get 90.
                                                            SET @elapsedTime = ( ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 1
                                                                                   - @Trad_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
														
                                                        END;
													
                                                    IF @RemainingMethod = 1
                                                        BEGIN
                                                            IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                                BEGIN
                                                                    SET @newElapsedTime = 0;
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @newElapsedTime = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 1
                                                                                              - @Rem_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                END;
														 
                                                        END;
													
											
                                                END;
										
										 --elapsed time can not be negative
                                            IF @elapsedTime < 0
                                                BEGIN
                                                    SET @elapsedTime = 0.0;
                                                END;
											
                                            IF @newElapsedTime < 0
                                                BEGIN
                                                    SET @newElapsedTime = 0.0;
                                                END;	
										
										--if the student started before this day of the month .. round elapsed time to a half month
                                            DECLARE @roundedElapsedTime AS DECIMAL;
                                            DECLARE @NewroundedElapsedTime AS DECIMAL; 
										
                                            SET @roundedElapsedTime = @elapsedTime;
                                            SET @NewroundedElapsedTime = @newElapsedTime;
										
										--round elapsed time only if the elapsed time is > 0
                                            IF @elapsedTime > 0
                                                BEGIN
                                                    IF @percentToEarnIdx = 1
                                                        BEGIN
                                                            SET @roundedElapsedTime = ( DATEDIFF(MONTH,@startDate,@ReportDate) + 1 ) * 100;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            IF DATEPART(MONTH,@ReportDate) = DATEPART(MONTH,@startDate)
                                                                AND DATEPART(YEAR,@ReportDate) = DATEPART(YEAR,@startDate)
                                                                BEGIN
																
                                                                    SET @halfMonthBeforeDay = (
                                                                                                SELECT  te.HalfMonthBeforeDay
                                                                                                FROM    #StuEnrollments se
                                                                                                       ,#PrgVersions pv
                                                                                                       ,#TuitionEarnings te
                                                                                                WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                        AND se.PrgVerId = pv.PrgVerId
                                                                                                        AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                              );
																						   
                                                                    IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay
                                                                        AND @halfMonthBeforeDay > 0
                                                                        BEGIN
                                                                            SET @roundedElapsedTime = 0.5 * 100;
																	
                                                                        END;
																
																
                                                                    SET @fullMonthBeforeDay = (
                                                                                                SELECT  te.FullMonthBeforeDay
                                                                                                FROM    #StuEnrollments se
                                                                                                       ,#PrgVersions pv
                                                                                                       ,#TuitionEarnings te
                                                                                                WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                        AND se.PrgVerId = pv.PrgVerId
                                                                                                        AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                              );
																
																
                                                                    IF DATEPART(DAY,@startDate) <= @fullMonthBeforeDay
                                                                        AND @fullMonthBeforeDay > 0
                                                                        BEGIN
                                                                            SET @roundedElapsedTime = 1 * 100;
                                                                        END;	
																						   
																																
																					   
                                                                END;
                                                        END;
													
											
                                                END;
											
                                            IF @newElapsedTime > 0
                                                BEGIN
                                                    IF @percentToEarnIdx = 1
                                                        BEGIN
                                                            SET @NewroundedElapsedTime = ( DATEDIFF(MONTH,@newStartDate,@ReportDate) + 1 ) * 100;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            IF DATEPART(MONTH,@ReportDate) = DATEPART(MONTH,@startDate)
                                                                AND DATEPART(YEAR,@ReportDate) = DATEPART(YEAR,@startDate)
                                                                BEGIN
                                                                    SET @halfMonthBeforeDay = (
                                                                                                SELECT  te.HalfMonthBeforeDay
                                                                                                FROM    #StuEnrollments se
                                                                                                       ,#PrgVersions pv
                                                                                                       ,#TuitionEarnings te
                                                                                                WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                        AND se.PrgVerId = pv.PrgVerId
                                                                                                        AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                              );
                                                                    IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay
                                                                        AND @halfMonthBeforeDay > 0
                                                                        BEGIN
                                                                            SET @NewroundedElapsedTime = 0.5 * 100;
                                                                        END;
																	
                                                                    SET @fullMonthBeforeDay = (
                                                                                                SELECT  te.FullMonthBeforeDay
                                                                                                FROM    #StuEnrollments se
                                                                                                       ,#PrgVersions pv
                                                                                                       ,#TuitionEarnings te
                                                                                                WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                        AND se.PrgVerId = pv.PrgVerId
                                                                                                        AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                              );
																						   
                                                                    IF DATEPART(DAY,@startDate) <= @fullMonthBeforeDay
                                                                        AND @fullMonthBeforeDay > 0
                                                                        BEGIN
                                                                            SET @NewroundedElapsedTime = 1 * 100;
                                                                        END;						   
																
                                                                END;
													
													
                                                        END;
											
                                                END;											
											
											--check that rounded elapsed time does not exceed term duration
                                            IF @roundedElapsedTime > @termDuration
                                                BEGIN
                                                    SET @roundedElapsedTime = @termDuration;
                                                END; 
												
                                            IF @NewroundedElapsedTime > @NewtermDuration
                                                BEGIN
                                                    SET @NewroundedElapsedTime = @NewtermDuration;
                                                END; 
												
                                            IF (
                                                 @percentToEarnIdx = 0
                                                 OR @percentToEarnIdx = 1
                                               )
                                                BEGIN											
                                                    IF @RemainingMethod = 1
                                                        BEGIN
                                                            IF @NewtermDuration <= 0
                                                                AND @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
                                                                BEGIN
                                                                    UPDATE  #Transactions
                                                                    SET     DeferredRevenue = @UnearnedRevenue
                                                                    WHERE   TransactionId = @TransactionId
                                                                            AND StuEnrollId = @StuEnrollId;														
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    IF @NewtermDuration <> 0
                                                                        BEGIN																
																			
																			 --apply straight line percentage
                                                                            UPDATE  #Transactions
                                                                            SET     DeferredRevenue = ( @UnearnedRevenue * @NewroundedElapsedTime )
                                                                                    / ( @NewtermDuration )
                                                                            WHERE   TransactionId = @TransactionId
                                                                                    AND StuEnrollId = @StuEnrollId;	
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                                                                            UPDATE  #Transactions
                                                                            SET     DeferredRevenue = 0
                                                                                   ,Earned = 0
                                                                            WHERE   TransactionId = @TransactionId
                                                                                    AND StuEnrollId = @StuEnrollId;
                                                                        END;
																	 
																
                                                                END;
															
																
                                                        END;
													
                                                    ELSE
                                                        BEGIN
                                                            IF @termDuration <= 0
                                                                AND @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
                                                                BEGIN
                                                                    UPDATE  #Transactions
                                                                    SET     DeferredRevenue = @TransAmount
                                                                    WHERE   TransactionId = @TransactionId
                                                                            AND StuEnrollId = @StuEnrollId;	
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    IF @termDuration <> 0
                                                                        BEGIN																			
																			-- apply straight line percentage
                                                                            UPDATE  #Transactions
                                                                            SET     DeferredRevenue = ( @TransAmount * @roundedElapsedTime ) / ( @termDuration )
                                                                            WHERE   TransactionId = @TransactionId
                                                                                    AND StuEnrollId = @StuEnrollId;
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                                                                            UPDATE  #Transactions
                                                                            SET     DeferredRevenue = 0
                                                                                   ,Earned = 0
                                                                            WHERE   TransactionId = @TransactionId
                                                                                    AND StuEnrollId = @StuEnrollId;
                                                                        END;
																
                                                                END;
														
                                                        END;
													
													
                                                END;
												
                                            IF ( @percentToEarnIdx = 2 )
                                                BEGIN											
                                                    DECLARE @totalCredits AS INTEGER;
                                                    DECLARE @PercentageRangeBasisIdx AS INTEGER;
                                                    DECLARE @hoursScheduled AS DECIMAL = 0.0;
                                                    DECLARE @hoursCompleted AS DECIMAL = 0.0;
                                                    DECLARE @totalProgramHours AS DECIMAL = 0.00;
                                                    DECLARE @ClsSectionId VARCHAR(50);
													 
                                                    SET @PercentageRangeBasisIdx = (
                                                                                     SELECT PercentageRangeBasisIdx
                                                                                     FROM   #StuEnrollments se
                                                                                           ,#PrgVersions pv
                                                                                           ,#TuitionEarnings te
                                                                                     WHERE  se.StuEnrollId = @StuEnrollId
                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                   );
																				   
                                                    DECLARE @percentArgument AS INTEGER;
													
                                                    IF @PercentageRangeBasisIdx = 0
                                                        BEGIN													
															-- Program Length
															--Modified by Saraswathi lakshmanan
															--When the term duration is zero the percent argument is considered to be zero . Since it is giving divide by zero error
                                                            IF @termDuration > 0
                                                                BEGIN
                                                                    SET @percentArgument = @roundedElapsedTime * 100 / @termDuration;
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;														
                                                                END;														 
														
                                                        END;
														
                                                    IF @PercentageRangeBasisIdx = 1
                                                        BEGIN													
															--Credits Earned
															-- add up Credits Earned
                                                            DECLARE @creditsEarned AS INTEGER = 0;
															
                                                            SET @creditsEarned = (
                                                                                   SELECT   SUM(Credits)
                                                                                   FROM     #Results
                                                                                   WHERE    StuEnrollId = @StuEnrollId
                                                                                            AND IsCreditsEarned IS NOT NULL
                                                                                            AND IsCreditsEarned = 1
                                                                                 );
																				  
															-- this is the total number of credits for the program
                                                            SET @totalCredits = (
                                                                                  SELECT    pv.Credits
                                                                                  FROM      #StuEnrollments se
                                                                                           ,#PrgVersions pv
                                                                                  WHERE     se.StuEnrollId = @StuEnrollId
                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                );
																				  
															-- Credits Earned
                                                            IF @totalCredits > 0
                                                                BEGIN
                                                                    SET @percentArgument = @creditsEarned * 100.0 / @totalCredits; 
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;
                                                                END;
														
                                                        END;
														
                                                    IF @PercentageRangeBasisIdx = 2
                                                        BEGIN
															--Credits Attempted
															--add up Credits Attempted
                                                            DECLARE @creditsAttempted AS INTEGER = 0;
															 
                                                            SET @creditsAttempted = (
                                                                                      SELECT    SUM(Credits)
                                                                                      FROM      #Results
                                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                                                AND IsCreditsAttempted IS NOT NULL
                                                                                                AND IsCreditsAttempted = 1
                                                                                    );
																					  
															--this is the total number of credits for the program
                                                            SET @totalCredits = (
                                                                                  SELECT    pv.Credits
                                                                                  FROM      #StuEnrollments se
                                                                                           ,#PrgVersions pv
                                                                                  WHERE     se.StuEnrollId = @StuEnrollId
                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                );
																				  
															-- Credits Attempted
                                                            IF @totalCredits > 0
                                                                BEGIN
                                                                    SET @percentArgument = @creditsAttempted * 100.0 / @totalCredits;														
                                                                END; 
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;
                                                                END;								 
														
														
                                                        END;
														
														
                                                    IF @PercentageRangeBasisIdx = 3
                                                        BEGIN													
															-- Courses completed
															--get the courses for the program version
                                                            DECLARE @numberOfCourses AS INTEGER;
															 
                                                            SET @numberOfCourses = (
                                                                                     SELECT COUNT(*)
                                                                                     FROM   #StuEnrollments se
                                                                                           ,#PrgVersions pv
                                                                                           ,#Reqs rq
                                                                                     WHERE  se.StuEnrollId = @StuEnrollId
                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                            AND pv.PrgVerId = rq.PrgVerId
                                                                                   );
																					 
															--calculate number of courses taken
                                                            DECLARE @numberOfCoursesTaken AS INTEGER = 0;
															
                                                            SET @numberOfCoursesTaken = (
                                                                                          SELECT    COUNT(*)
                                                                                          FROM      #StuEnrollments se
                                                                                                   ,#Results rs
                                                                                          WHERE     se.StuEnrollId = @StuEnrollId
                                                                                                    AND se.StuEnrollId = rs.StuEnrollId
                                                                                                    AND rs.IsPass IS NOT NULL
                                                                                                    AND rs.IsPass = 1
                                                                                        );
																						 
															--'Percentage of courses completed
                                                            IF @numberOfCourses > 0
                                                                BEGIN
                                                                    SET @percentArgument = @numberOfCoursesTaken * 100.0 / @numberOfCourses; 
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;
                                                                END;
															
														
                                                        END;
														
														
                                                    IF @PercentageRangeBasisIdx = 4
                                                        BEGIN
															--get number of scheduled hours												
																
															--If attendance is being tracked by class then we need to get the total scheduled hours so far for those classes
															--If attendance is by day there is no need to use the cursor as we can go straight to the arStudentClockAttendance table.
                                                            IF LOWER((
                                                                       SELECT   dbo.AppSettings('TrackSapAttendance',@CampusId)
                                                                     )) = 'byclass'
                                                                BEGIN
                                                                    DECLARE ResultCursor CURSOR FORWARD_ONLY FAST_FORWARD
                                                                    FOR
                                                                        SELECT  ClsSectionId
                                                                        FROM    #Results rs
                                                                        WHERE   rs.StuEnrollId = @StuEnrollId;
																		
                                                                    OPEN ResultCursor;
                                                                    FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId;
																	
                                                                    WHILE @@FETCH_STATUS = 0
                                                                        BEGIN
                                                                            SET @hoursScheduled = @hoursScheduled + (
                                                                                                                      SELECT    SUM(Scheduled)
                                                                                                                      FROM      dbo.atClsSectAttendance
                                                                                                                      WHERE     ClsSectionId = @ClsSectionId
                                                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                                                AND MeetDate <= @ReportDate
                                                                                                                    );													 
																																						
                                                                            FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId;
                                                                        END;
																		
                                                                    CLOSE ResultCursor;
                                                                    DEALLOCATE ResultCursor;
																
																
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @hoursScheduled = @hoursScheduled + (
                                                                                                              SELECT    SUM(SchedHours)
                                                                                                              FROM      dbo.arStudentClockAttendance
                                                                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                                                                        AND RecordDate <= @ReportDate
                                                                                                            );
                                                                END;
																
															
															
															--get the total duration of the program in hours																										
                                                            SET @totalProgramHours = (
                                                                                       SELECT   Hours
                                                                                       FROM     #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                       WHERE    se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                     );
															
															--Percentage of scheduled hours
                                                            IF @totalProgramHours > 0
                                                                BEGIN
                                                                    SET @percentArgument = @hoursScheduled * 100.0 / @totalProgramHours; 
                                                                END; 
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;
                                                                END;
															
														
                                                        END;
														
														
                                                    IF @PercentageRangeBasisIdx = 5
                                                        BEGIN
															--get number of completed hours
                                                            IF LOWER((
                                                                       SELECT   dbo.AppSettings('TrackSapAttendance',@CampusId)
                                                                     )) = 'byclass'
                                                                BEGIN
                                                                    DECLARE ResultCursor CURSOR FAST_FORWARD FORWARD_ONLY
                                                                    FOR
                                                                        SELECT  ClsSectionId
                                                                        FROM    #Results rs
                                                                        WHERE   rs.StuEnrollId = @StuEnrollId;
																		
                                                                    OPEN ResultCursor;
                                                                    FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId;
																	
                                                                    WHILE @@FETCH_STATUS = 0
                                                                        BEGIN
                                                                            SET @hoursScheduled = @hoursScheduled + (
                                                                                                                      SELECT    SUM(Scheduled)
                                                                                                                      FROM      dbo.atClsSectAttendance
                                                                                                                      WHERE     ClsSectionId = @ClsSectionId
                                                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                                                AND MeetDate <= @ReportDate
                                                                                                                    );
																													 
                                                                            SET @hoursCompleted = @hoursCompleted + (
                                                                                                                      SELECT    SUM(Actual)
                                                                                                                      FROM      dbo.atClsSectAttendance
                                                                                                                      WHERE     ClsSectionId = @ClsSectionId
                                                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                                                AND MeetDate <= @ReportDate
                                                                                                                    );
																																 
																																						
                                                                            FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId;
                                                                        END;
																		
                                                                    CLOSE ResultCursor;
                                                                    DEALLOCATE ResultCursor;
																
																
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @hoursScheduled = @hoursScheduled + (
                                                                                                              SELECT    SUM(SchedHours)
                                                                                                              FROM      dbo.arStudentClockAttendance
                                                                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                                                                        AND RecordDate <= @ReportDate
                                                                                                            );
																											 
                                                                    SET @hoursCompleted = @hoursCompleted + (
                                                                                                              SELECT    SUM(ActualHours)
                                                                                                              FROM      dbo.arStudentClockAttendance
                                                                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                                                                        AND RecordDate <= @ReportDate
                                                                                                            );
																											 
                                                                END;
																
															--get the total duration of the program in hours																										
                                                            SET @totalProgramHours = (
                                                                                       SELECT   Hours
                                                                                       FROM     #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                       WHERE    se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                     );
																					  
															--Percentage of scheduled hours
                                                            IF @totalProgramHours > 0
                                                                BEGIN
                                                                    SET @percentArgument = @hoursCompleted * 100.0 / @totalProgramHours; 
                                                                END; 
                                                            ELSE
                                                                BEGIN
                                                                    SET @percentArgument = 0.0;	
                                                                END;
																
														
                                                        END;	
													
													--percent argument should never be grater that 100
                                                    IF @percentArgument > 100.0
                                                        BEGIN
                                                            SET @percentArgument = 100.0;
                                                        END;
														
														
                                                    DECLARE @percentResult AS INTEGER = 0; 
													
                                                    IF @percentArgument > 0
                                                        BEGIN
                                                            SET @percentResult = (
                                                                                   SELECT TOP 1
                                                                                            tepr.EarnPercent
                                                                                   FROM     #StuEnrollments se
                                                                                           ,#PrgVersions pv
                                                                                           ,#TuitionEarnings te
                                                                                           ,#TuitionEarningsPercentageRanges tepr
                                                                                   WHERE    se.StuEnrollId = @StuEnrollId
                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                            AND te.TuitionEarningId = tepr.TuitionEarningId
                                                                                            AND tepr.UpTo >= @percentArgument
                                                                                 );														
                                                        END;
																		 
													--apply percentage
                                                    UPDATE  #Transactions
                                                    SET     DeferredRevenue = ( TransAmount * @percentResult ) / 100
                                                    WHERE   TransactionId = @TransactionId
                                                            AND StuEnrollId = @StuEnrollId;																		  
												
                                                END;	
												
											--add this value to the totalDeferredRevenue in the transCodes table
											--add Earned Amount from each transaction to AccruedAmount in the transCodes table
                                            IF (
                                                 SELECT TotalDeferredRevenue
                                                 FROM   #TransCodes
                                                 WHERE  TransCodeId = @TransCodeId
                                               ) IS NOT NULL
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     TotalDeferredRevenue = TotalDeferredRevenue + (
                                                                                                            SELECT  DeferredRevenue
                                                                                                            FROM    #Transactions
                                                                                                            WHERE   TransactionId = @TransactionId
                                                                                                          )
                                                    WHERE   TransCodeId = @TransCodeId;										
                                                END;
                                            ELSE
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     TotalDeferredRevenue = (
                                                                                     SELECT DeferredRevenue
                                                                                     FROM   #Transactions
                                                                                     WHERE  TransactionId = @TransactionId
                                                                                   )
                                                    WHERE   TransCodeId = @TransCodeId;									
                                                END;
																				
																							  
                                            IF (
                                                 SELECT AccruedAmount
                                                 FROM   #TransCodes
                                                 WHERE  TransCodeId = @TransCodeId
                                               ) IS NOT NULL
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     AccruedAmount = AccruedAmount + (
                                                                                              SELECT    Earned
                                                                                              FROM      #Transactions
                                                                                              WHERE     TransactionId = @TransactionId
                                                                                            )
                                                    WHERE   TransCodeId = @TransCodeId;
                                                END;	
                                            ELSE
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     AccruedAmount = (
                                                                              SELECT    Earned
                                                                              FROM      #Transactions
                                                                              WHERE     TransactionId = @TransactionId
                                                                            )
                                                    WHERE   TransCodeId = @TransCodeId;
                                                END;								
											
											
											
											
                                            IF @RemainingMethod = 1
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     DeferredRevenueAmount = TotalDeferredRevenue
                                                    WHERE   TransCodeId = @TransCodeId;										
                                                END;	
                                            ELSE
                                                BEGIN
                                                    UPDATE  #TransCodes
                                                    SET     DeferredRevenueAmount = TotalDeferredRevenue - AccruedAmount
                                                    WHERE   TransCodeId = @TransCodeId;
                                                END;	
											
											--At this point we should populate the #DeferredRevenues table if the @PostDeferredRevenue was passed in as 1
                                            IF @Mode = 3
                                                BEGIN
                                                    DECLARE @Amount AS DECIMAL(18,2);
													
                                                    IF @RemainingMethod = 1
                                                        BEGIN
                                                            SET @Amount = (
                                                                            SELECT  DeferredRevenue
                                                                            FROM    #Transactions
                                                                            WHERE   TransactionId = @TransactionId
                                                                          );												
                                                        END;
                                                    ELSE
                                                        BEGIN
															
                                                            IF (
                                                                 SELECT DeferredRevenue
                                                                 FROM   #Transactions
                                                                 WHERE  TransactionId = @TransactionId
                                                               ) <> 0.0
                                                                BEGIN
                                                                    SET @Amount = (
                                                                                    SELECT  DeferredRevenue
                                                                                    FROM    #Transactions
                                                                                    WHERE   TransactionId = @TransactionId
                                                                                  ) - (
                                                                                        SELECT  Earned
                                                                                        FROM    #Transactions
                                                                                        WHERE   TransactionId = @TransactionId
                                                                                      );
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @Amount = 0;
                                                                END;
																
														
                                                        END;
													
													--Add row to the table only if amount is <> 0
                                                    IF @Amount <> 0
                                                        BEGIN
                                                            INSERT  INTO #DeferredRevenues
                                                                    (
                                                                     DefRevenueId
                                                                    ,DefRevenueDate
                                                                    ,TransactionId
                                                                    ,Type
                                                                    ,Source
                                                                    ,Amount
                                                                    ,IsPosted
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    )
                                                                    SELECT  NEWID()
                                                                           , -- DefRevenueId - uniqueidentifier
                                                                            @ReportDate
                                                                           , -- DefRevenueDate - datetime
                                                                            @TransactionId
                                                                           , -- TransactionId - uniqueidentifier
                                                                            0
                                                                           , -- Type - bit
                                                                            1
                                                                           , -- Source - tinyint
                                                                            @Amount
                                                                           , -- Amount - money
                                                                            1
                                                                           , -- IsPosted - bit
                                                                            @User
                                                                           , -- ModUser - varchar(50)
                                                                            CONVERT(SMALLDATETIME,GETDATE());  -- ModDate - datetime													 
														
                                                        END;											
														
														
														
												
                                                END;															 
											
											
									
                                        END;							
								
								
								
							
                                END;						
																	
						
					
                        END;
			
		
		
                END;
		
		
		
		
            FETCH NEXT FROM transCursor
		INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,@TermStart,
                @TermEnd,@percentToEarnIdx,@RemainingMethod,@DeferredRevenue;


        END;
	

    CLOSE transCursor;
    DEALLOCATE transCursor;


    ALTER TABLE dbo.saDeferredRevenues DISABLE TRIGGER ALL;
    ALTER TABLE dbo.saDeferredRevenues NOCHECK CONSTRAINT ALL;  

    IF @Mode = 3
        BEGIN
            BEGIN TRAN;	
            INSERT  INTO dbo.saDeferredRevenues
                    SELECT  *
                    FROM    #DeferredRevenues WITH ( NOLOCK );	 
            COMMIT TRAN;
	
        END;


    ALTER TABLE dbo.saDeferredRevenues CHECK CONSTRAINT ALL; 

--When Mode is 1 (BuildList) or 3 (Post)
--If there are student exceptions then that table should be returned
--Else we should return the transcodes table
    IF (
         @Mode = 1
         OR @Mode = 3
       )
        BEGIN
            IF (
                 SELECT ISNULL(COUNT(*),0)
                 FROM   #StudentExceptions
               ) > 0
                BEGIN
                    SELECT  *
                    FROM    #StudentExceptions;	
                END;
            ELSE
                BEGIN
                    SELECT  *
                    FROM    #TransCodes;
                END;	
        END;
	

--When Mode is 2 (BindTransCodeDetails)
--Return the #Transactions table
    IF @Mode = 2
        BEGIN
            SELECT  *
            FROM    #Transactions;
        END;




    DROP TABLE #Transactions;
    DROP TABLE #TransCodes;
    DROP TABLE #StuEnrollments;
    DROP TABLE #PrgVersions;
    DROP TABLE #TuitionEarnings;
    DROP TABLE #TuitionEarningsPercentageRanges;
    DROP TABLE #Results;
    DROP TABLE #Reqs;
    DROP TABLE #StudentExceptions;
    DROP TABLE #DeferredRevenues;

-- =========================================================================================================
-- END  --  USP_SA_GetDeferredRevenueResults
-- =========================================================================================================
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping SP if exist  [dbo].[USP_ManageSecurity_Tabs_Permissions]';
GO
-- =========================================================================================================
-- USP_ManageSecurity_Tabs_Permissions
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_ManageSecurity_Tabs_Permissions]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        PRINT N'Dropping Procedure [dbo].[USP_ManageSecurity_Tabs_Permissions]';
        DROP PROCEDURE USP_ManageSecurity_Tabs_Permissions;
    END;
    
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating SP  [dbo].[USP_ManageSecurity_Tabs_Permissions]';
GO
-- =========================================================================================================
-- USP_ManageSecurity_Tabs_Permissions
-- =========================================================================================================
--sp_helptext USP_ManageSecurity_Tabs_Permissions    
CREATE PROCEDURE dbo.USP_ManageSecurity_Tabs_Permissions
    @RoleId VARCHAR(50)
   ,@ModuleResourceId INT
   ,@TabId INT
   ,@SchoolEnumerator INT
   ,@campusId UNIQUEIDENTIFIER
AS --DE7659 - Updated --    
 -- declare local variables        
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50)
       ,@PostServicesByStudent BIT
       ,@PostServicesByClass BIT;    
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5);     
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5);    
    DECLARE @AR_ParentId UNIQUEIDENTIFIER
       ,@PL_ParentId UNIQUEIDENTIFIER
       ,@FA_ParentId UNIQUEIDENTIFIER;    
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
       ,@SA_ParentId UNIQUEIDENTIFIER
       ,@AD_ParentId UNIQUEIDENTIFIER;    
     
-- Get Values    
--SET @SchoolEnumerator = 1689    
--SET @ShowRossOnlyTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=68)    
--SET @SchedulingMethod = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=65)    
--SET @TrackSAPAttendance = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=72)    
--SET @ShowCollegeOfCourtReporting = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=118)    
--SET @FameESP = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=37)    
--SET @EdExpress = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=91)    
--SET @GradeBookWeightingLevel = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=43)    
--SET @ShowExternshipTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=71)    
    
    SET @AD_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 395
                                AND t2.ResourceId = 189
                       );    
      
      
 -- Get Values        
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 68
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @ShowRossOnlyTabs = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 68
                                                AND CampusId = @campusId
                                    );        
        
        END;       
    ELSE
        BEGIN      
            SET @ShowRossOnlyTabs = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 68
                                                AND CampusId IS NULL
                                    );      
        END;      
       
  --PRINT @ShowRossOnlyTabs      
       
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 65
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @SchedulingMethod = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 65
                                                AND CampusId = @campusId
                                    );        
        
        END;       
    ELSE
        BEGIN      
            SET @SchedulingMethod = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 65
                                                AND CampusId IS NULL
                                    );      
        END;                            
              
       
             
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 72
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @TrackSAPAttendance = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 72
                                                AND CampusId = @campusId
                                      );        
        
        END;       
    ELSE
        BEGIN      
            SET @TrackSAPAttendance = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 72
                                                AND CampusId IS NULL
                                      );      
        END;       
       
                
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 118
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT Value
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 118
                                                        AND CampusId = @campusId
                                               );        
        
        END;       
    ELSE
        BEGIN      
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT Value
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 118
                                                        AND CampusId IS NULL
                                               );      
        END;       
        
           
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 37
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @FameESP = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 37
                                    AND CampusId = @campusId
                           );        
        
        END;       
    ELSE
        BEGIN      
            SET @FameESP = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 37
                                    AND CampusId IS NULL
                           );      
        END;       
        
             
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 91
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @EdExpress = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 91
                                        AND CampusId = @campusId
                             );        
        
        END;       
    ELSE
        BEGIN      
            SET @EdExpress = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 91
                                        AND CampusId IS NULL
                             );      
        END;      
        
              
               
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 43
                AND CampusId = @campusId
       ) >= 1
        BEGIN  
            SET @GradeBookWeightingLevel = (
                                             SELECT Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 43
                                                    AND CampusId = @campusId
                                           );        
        
        END;       
    ELSE
        BEGIN      
            SET @GradeBookWeightingLevel = (
                                             SELECT Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 43
                                                    AND CampusId IS NULL
                                           );      
        END;      
       
             
             
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 71
                AND CampusId = @campusId
       ) >= 1
        BEGIN      
            SET @ShowExternshipTabs = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 71
                                                AND CampusId = @campusId
                                      );        
        
        END;       
    ELSE
        BEGIN      
            SET @ShowExternshipTabs = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 71
                                                AND CampusId IS NULL
                                      );      
        END;      
      
    SET @PostServicesByStudent = (
                                   SELECT   Value
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = (
                                                          SELECT    SettingId
                                                          FROM      dbo.syConfigAppSettings
                                                          WHERE     KeyName = 'PostServicesByStudent'
                                                        )
                                            AND CampusId IS NULL
                                 );      
    SET @PostServicesByClass = (
                                 SELECT Value
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = (
                                                      SELECT    SettingId
                                                      FROM      dbo.syConfigAppSettings
                                                      WHERE     KeyName = 'PostServicesByClass'
                                                    )
                                        AND CampusId IS NULL
                               );               
    
    SELECT  *
           ,CASE WHEN AccessLevel = 15 THEN 1
                 ELSE 0
            END AS FullPermission
           ,CASE WHEN AccessLevel IN ( 14,13,12,11,10,9,8 ) THEN 1
                 ELSE 0
            END AS EditPermission
           ,CASE WHEN AccessLevel IN ( 14,13,12,7,6,5,4 ) THEN 1
                 ELSE 0
            END AS AddPermission
           ,CASE WHEN AccessLevel IN ( 14,11,10,7,6,2 ) THEN 1
                 ELSE 0
            END AS DeletePermission
           ,CASE WHEN AccessLevel IN ( 13,11,9,7,5,3,1 ) THEN 1
                 ELSE 0
            END AS ReadPermission
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = RChild.ResourceID
                        ) AS AccessLevel
                       ,RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN (
                                    NNParent.ResourceId IN ( 395 )
                                    OR NNChild.ResourceId IN ( 737,740,741,792 )
                                  ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 737
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              NNChild.ResourceId = 740
                                              OR NNParent.ResourceId IN ( 740 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        NNChild.ResourceId = 741
                                                        OR NNParent.ResourceId IN ( 741 )
                                                      ) THEN 3
                                                 ELSE 4
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                             ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                       ELSE 3
                                  END
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 737 )
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeID = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceID NOT IN ( 394 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 395 )
                              OR NNChild.HierarchyId IN ( SELECT DISTINCT
                                                                    HierarchyId
                                                          FROM      syNavigationNodes
                                                          WHERE     ResourceId IN ( 737,740,741,792 )
                                                                    AND ParentId IN ( SELECT    HierarchyId
                                                                                      FROM      syNavigationNodes
                                                                                      WHERE     ResourceId = 395
                                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                                  FROM      syNavigationNodes
                                                                                                                  WHERE     ResourceId = 189 ) ) )
                            )
              UNION ALL
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE 7
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 738 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 738 )
                                    OR ParentResourceId IN ( 738 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 26 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL    
         
         
     --IPEDS TAB   - DE7659               
              SELECT    26 AS ModuleResourceId
                       ,689 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,689 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 409
                                    OR t1.ParentResourceId IN ( 409 )
                                  ) THEN 2
                             WHEN (
                                    t1.ChildResourceId = 719
                                    OR t1.ParentResourceId IN ( 719 )
                                  ) THEN 3
                             WHEN (
                                    t1.ChildResourceId = 720
                                    OR t1.ParentResourceId IN ( 720 )
                                  ) THEN 4
                             WHEN (
                                    t1.ChildResourceId = 721
                                    OR t1.ParentResourceId IN ( 721 )
                                  ) THEN 5
                             WHEN (
                                    t1.ChildResourceId = 725
                                    OR t1.ParentResourceId IN ( 725 )
                                  ) THEN 6
                             WHEN (
                                    t1.ChildResourceId = 722
                                    OR t1.ParentResourceId IN ( 722 )
                                  ) THEN 7
                             WHEN (
                                    t1.ChildResourceId = 723
                                    OR t1.ParentResourceId IN ( 723 )
                                  ) THEN 8
                             WHEN (
                                    t1.ChildResourceId = 724
                                    OR t1.ParentResourceId IN ( 724 )
                                  ) THEN 9    
        --IPEDS UPDATE    
                             WHEN (
                                    t1.ChildResourceId = 789
                                    OR t1.ParentResourceId IN ( 789 )
                                  ) THEN 10
                             WHEN (
                                    t1.ChildResourceId = 839
                                    OR t1.ParentResourceId IN ( 839 )
                                  ) THEN 11
                             ELSE 12
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 409 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,    
          
      --( SELECT TOP 1    
      --            HierarchyIndex    
      --  FROM      dbo.syNavigationNodes    
      --  WHERE     ResourceId = ChildResourceId    
      --) AS FirstSortOrder,    
                        CASE WHEN (
                                    ChildResourceId IN ( 409 )
                                    OR ParentResourceId IN ( 409 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 409,719,720,721,725,722,723,724,789,839 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
                                    AND ( RChild.ResourceID NOT IN ( 691,690,729,730,736,678,692,727,472,474 ) )   -- 394, 472, 474, 711 ,712    
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 26 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 409,719,720,721,722,723,724,725,789,839 ) )
                                          AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                        )
                        ) t1
              UNION ALL
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE CASE WHEN (
                                                                                      t1.ChildResourceId = 742
                                                                                      OR t1.ParentResourceId IN ( 742 )
                                                                                    ) THEN 7
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741,742 )
                                    OR ParentResourceId IN ( 737,738,740,741,742 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )    
          --  AND  RChild.ChildTypeId     
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 194 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE CASE WHEN (
                                                                                      t1.ChildResourceId = 742
                                                                                      OR t1.ParentResourceId IN ( 742 )
                                                                                    ) THEN 6
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,739 )
                                    OR ParentResourceId IN ( 737,739 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId IN ( 737,739,740,741,742 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 300 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE CASE WHEN (
                                                                                      t1.ChildResourceId = 742
                                                                                      OR t1.ParentResourceId IN ( 742 )
                                                                                    ) THEN 6
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741,742 )
                                    OR ParentResourceId IN ( 737,738,740,741,742 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 191 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE CASE WHEN (
                                                                                      t1.ChildResourceId = 742
                                                                                      OR t1.ParentResourceId IN ( 742 )
                                                                                    ) THEN 7
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741,742 )
                                    OR ParentResourceId IN ( 737,738,740,741,742 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 739
                                              OR t1.ParentResourceId IN ( 739 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 738
                                                        OR t1.ParentResourceId IN ( 738 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737 )
                                    OR ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 737 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 397
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737 )
                        ) t1
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = syResources.ResourceID
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT    MAX(AccessLevel)
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceID = t1.ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 396
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 192 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737 )
                        ) t1
            ) t2
    WHERE   t2.ModuleResourceId = @ModuleResourceId
            AND t2.TabId = @TabId    
   -- DE7545 Conversion pages not to be shown in Manage Security    
            AND ChildResourceId NOT IN ( 530,531,287,288 )
            AND    
   -- Hide resources based on Configuration Settings        
            (
              -- The following expression means if showross... is set to false, hide         
  -- ResourceIds 541,542,532,534,535,538,543,539        
  -- (Not False) OR (Condition)      
   -- US4330 removed Resourceid 541       
      (
        (
          NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
        )
        OR ( ChildResourceId NOT IN ( 542,532,538,543 ) )
      )
              AND        
  -- The following expression means if showross... is set to true, hide         
  -- ResourceIds 142,375,330,476,508,102,107,237        
  -- (Not True) OR (Condition)        
              (
                (
                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                )
                OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
              )
              AND      
  -- US4330 added        
  -- The following expression means if PostServices is set to false or showross is set to false, hide           
  -- ResourceId 541, 508 if ShowRossOnlyTabs is set to false      
  -- (Not False) OR (Condition)          
              (
                (
                  (
                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                  )
                  OR ( ChildResourceId NOT IN ( 541 ) )
                )
                OR (
                     (
                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                     )
                     OR ( ChildResourceId NOT IN ( 541 ) )
                   )
              )
              AND (
                    (
                      (
                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                      )
                      OR ( ChildResourceId NOT IN ( 508 ) )
                    )
                    OR (
                         (
                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                         )
                         OR ( ChildResourceId NOT IN ( 508 ) )
                       )
                  )
              AND           
  ---- The following expression means if SchedulingMethod=regulartraditional, hide         
  ---- ResourceIds 91 and 497        
  ---- (Not True) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                )
                OR ( ChildResourceId NOT IN ( 91,497 ) )
              )
              AND        
  -- The following expression means if TrackSAPAttendance=byday, hide         
  -- ResourceIds 633        
  -- (Not True) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                )
                OR ( ChildResourceId NOT IN ( 633,327 ) )
              )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                    )
                    OR ( ChildResourceId NOT IN ( 539 ) )
                  )
              AND      
  ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide         
  ---- ResourceIds 614,615        
  ---- (Not False) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 614,615 ) )
              )
              AND        
  -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide         
  -- ResourceIds 497        
  -- (Not True) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                )
                OR ( ChildResourceId NOT IN ( 497 ) )
              )
              AND        
  -- The following expression means if FAMEESP is set to false, hide         
  -- ResourceIds 517,523, 525        
  -- (Not False) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 517,523,525 ) )
              )
              AND        
  ---- The following expression means if EDExpress is set to false, hide         
  ---- ResourceIds 603,604,606,619        
  ---- (Not False) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
              )
              AND        
  ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide         
  ---- ResourceIds 107,96,222        
  ---- (Not False) OR (Condition)        
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                )
                OR ( ChildResourceId NOT IN ( 107,96,222 ) )
              )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                    )
                    OR ( ChildResourceId NOT IN ( 476 ) )
                  )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                    )      
      --OR ( ChildResourceId NOT IN ( 543, 538 ) )      
                    OR ( ChildResourceId NOT IN ( 543 ) )
                  )
            )
    ORDER BY GroupSortOrder
           ,ParentResourceId
           ,ChildResource;    

-- =========================================================================================================
-- END  --  USP_ManageSecurity_Tabs_Permissions
-- =========================================================================================================   
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping SP if exist  [dbo].[USP_SA_PostPayments_PrintReceipt]';
GO
--=================================================================================================
-- USP_SA_PostPayments_PrintReceipt
--=================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_SA_PostPayments_PrintReceipt]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        PRINT N'Dropping Procedure [dbo].[USP_SA_PostPayments_PrintReceipt]';
        DROP PROCEDURE USP_SA_PostPayments_PrintReceipt;
    END;
    
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating SP  [dbo].[USP_SA_PostPayments_PrintReceipt]';
GO
--=================================================================================================
-- USP_SA_PostPayments_PrintReceipt
--=================================================================================================
CREATE PROCEDURE dbo.USP_SA_PostPayments_PrintReceipt
    @CampusId UNIQUEIDENTIFIER
   ,@TransactionId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;  
    BEGIN  
   
        DECLARE @StudentReceiptData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NOT NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            ,StudentName VARCHAR(100) NOT NULL
            ,StudentAddress1 VARCHAR(200) NULL
            ,StudentAddress2 VARCHAR(200) NULL
            ,StudentCity VARCHAR(50) NULL
            ,StudentState VARCHAR(50) NULL
            ,StudentZip VARCHAR(20) NULL
            ,StudentCountry VARCHAR(50) NULL
            ,TransDate VARCHAR(10) NOT NULL
            ,TransDesc VARCHAR(50) NULL
            ,PaymentTypeDesc VARCHAR(50) NOT NULL
            ,CheckNumber VARCHAR(50) NULL
            ,TransAmount DECIMAL(19,4) NOT NULL
            );  
    
        DECLARE @SchoolData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            );  
    
        DECLARE @StudentData TABLE
            (
             StudentName VARCHAR(100) NOT NULL
            ,StudentAddress1 VARCHAR(200) NULL
            ,StudentAddress2 VARCHAR(200) NULL
            ,StudentCity VARCHAR(50) NULL
            ,StudentState VARCHAR(50) NULL
            ,StudentZip VARCHAR(20) NULL
            ,StudentCountry VARCHAR(50) NULL
            );  
    
      
  --Get School Address  
        DECLARE @AddressToBePrintedInReceipts VARCHAR(50);  
        SET @AddressToBePrintedInReceipts = (
                                              SELECT    Value
                                              FROM      syConfigAppSetValues v
                                              INNER JOIN syConfigAppSettings s ON s.SettingId = v.SettingId
                                              WHERE     KeyName = 'AddressToBePrintedInReceipts'
                                            );  
    
        BEGIN  
            IF @AddressToBePrintedInReceipts = 'CorporateAddress'
                BEGIN  
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry       
                            )
                            SELECT  (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateName'
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress1'
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress2'
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCity'
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateState'
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateZip'
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCountry'
                                    ) AS SchoolCountry;  
                END;  
      
            IF @AddressToBePrintedInReceipts = 'CampusAddress'
                BEGIN     
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry    
                            )
                            SELECT  (
                                      SELECT    CampDescrip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Address1
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Address2
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    City
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    StateCode
                                      FROM      syStates s
                                      INNER JOIN syCampuses c ON s.StateId = c.StateId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Zip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    CountryCode
                                      FROM      dbo.adCountries s
                                      INNER JOIN syCampuses c ON s.CountryId = c.CountryId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCountry;  
                END;   
    
        END;    
    
  --Get Student Address  
    
        DECLARE @DefaultStuAddressId UNIQUEIDENTIFIER;  
        SET @DefaultStuAddressId = ISNULL((
                                            SELECT TOP 1
                                                    adLeadAddressId
                                            FROM    saTransactions T
                                                   ,arStuEnrollments SE
                                                   ,adLeadAddresses SA
                                                   ,syStatuses ST
                                                   ,adLeads L
                                            WHERE   T.StuEnrollId = SE.StuEnrollId
                                                    AND L.StudentId = SE.StudentId
                                                    AND L.LeadId = SA.LeadId
                                                    AND SA.StatusId = ST.StatusId
                                                    AND ST.Status = 'Active'
                                                    AND TransactionId = @TransactionId
                                            ORDER BY IsShowOnLeadPage DESC
                                          ),'00000000-0000-0000-0000-000000000000');  
  
        IF @DefaultStuAddressId <> '00000000-0000-0000-0000-000000000000'
            BEGIN  
                INSERT  INTO @StudentData
                        (
                         StudentName
                        ,StudentAddress1
                        ,StudentAddress2
                        ,StudentCity
                        ,StudentState
                        ,StudentZip
                        ,StudentCountry   
                        )
                        SELECT  (
                                  SELECT    ( LastName + ', ' + FirstName ) AS StudentName
                                  FROM      adLeads
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments S
                                                          INNER JOIN saTransactions t ON S.StuEnrollId = t.StuEnrollId
                                                          WHERE     t.TransactionId = @TransactionId
                                                        )
                                ) AS StudentName
                               ,( CASE CCT.AddressApto
                                    WHEN '' THEN CCT.Address1
                                    ELSE CCT.AddressApto + ' ' + CCT.Address1
                                  END ) AS StudentAddress1
                               ,CCT.Address2 AS StudentAddress2
                               ,CCT.City AS StudentCity
                               ,(
                                  SELECT    StateCode
                                  FROM      syStates
                                  WHERE     StateId = CCT.StateId
                                ) AS StudentState
                               ,CCT.ZipCode AS StudentZip
                               ,(
                                  SELECT    CountryCode
                                  FROM      adCountries
                                  WHERE     CountryId = CCT.CountryId
                                ) AS StudentCountry
                        FROM    adLeadAddresses CCT
                        WHERE   CCT.adLeadAddressId = @DefaultStuAddressId;   
    
            END;   
      
        IF @DefaultStuAddressId = '00000000-0000-0000-0000-000000000000'
            BEGIN  
                INSERT  INTO @StudentData
                        (
                         StudentName
                        ,StudentAddress1
                        ,StudentAddress2
                        ,StudentCity
                        ,StudentState
                        ,StudentZip
                        ,StudentCountry   
                        )
                        SELECT  (
                                  SELECT    ( LastName + ', ' + FirstName ) AS StudentName
                                  FROM      adLeads
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments S
                                                          INNER JOIN saTransactions t ON S.StuEnrollId = t.StuEnrollId
                                                          WHERE     t.TransactionId = @TransactionId
                                                        )
                                ) AS StudentName
                               ,'' AS StudentAddress1
                               ,'' AS StudentAddress2
                               ,'' AS StudentCity
                               ,'' AS StudentState
                               ,'' AS StudentZip
                               ,'' AS StudentCountry;          
      
      
            END;        
     
    
    
    
        BEGIN  
            INSERT  INTO @StudentReceiptData
                    (
                     SchoolName
                    ,SchoolAddress1
                    ,SchoolAddress2
                    ,SchoolCity
                    ,SchoolState
                    ,SchoolZip
                    ,SchoolCountry
                    ,StudentName
                    ,StudentAddress1
                    ,StudentAddress2
                    ,StudentCity
                    ,StudentState
                    ,StudentZip
                    ,StudentCountry
                    ,TransDate
                    ,TransDesc
                    ,PaymentTypeDesc
                    ,CheckNumber
                    ,TransAmount      
                    )
                    SELECT  SchoolName AS SchoolName
                           ,SchoolAddress1 AS SchoolAddress1
                           ,SchoolAddress2 AS SchoolAddress2
                           ,SchoolCity AS SchoolCity
                           ,SchoolState AS SchoolState
                           ,SchoolZip AS SchoolZip
                           ,SchoolCountry AS SchoolCountry
                           ,StudentName AS StudentName
                           ,StudentAddress1 AS StudentAddress1
                           ,StudentAddress2 AS StudentAddress2
                           ,StudentCity AS StudentCity
                           ,StudentState AS StudentState
                           ,StudentZip AS StudentZip
                           ,StudentCountry AS StudentCountry
                           ,CONVERT(VARCHAR(10),TransDate,101) AS TransDate
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      dbo.saTransCodes
                              WHERE     TransCodeId = T.TransCodeId
                                        AND T.TransactionId = @TransactionId
                            ) AS TransDesc
                           ,(
                              SELECT    Description
                              FROM      dbo.saPaymentTypes PT
                              INNER JOIN dbo.saPayments P ON PT.PaymentTypeId = P.PaymentTypeId
                              WHERE     P.TransactionId = @TransactionId
                            ) AS PaymentTypeDesc
                           ,P.CheckNumber AS CheckNumber
                           ,T.TransAmount AS TransAmount
                    FROM    @SchoolData
                           ,@StudentData
                           ,dbo.saTransactions T
                           ,dbo.saPayments P
                    WHERE   T.TransactionId = @TransactionId
                            AND T.Voided = 0
                            AND P.TransactionId = @TransactionId;  
     
        END;  
    
        SELECT  SchoolName
               ,SchoolAddress1
               ,SchoolAddress2
               ,SchoolCity
               ,SchoolState
               ,SchoolZip
               ,CASE SchoolCountry
                  WHEN 'USA' THEN ''
                  ELSE SchoolCountry
                END AS SchoolCountry
               ,StudentName
               ,StudentAddress1
               ,StudentAddress2
               ,StudentCity
               ,StudentState
               ,StudentZip
               ,CASE StudentCountry
                  WHEN 'USA' THEN ''
                  ELSE StudentCountry
                END AS StudentCountry
               ,TransDate
               ,TransDesc
               ,PaymentTypeDesc
               ,CheckNumber
               ,TransAmount
        FROM    @StudentReceiptData;  
    
    END;  
--=================================================================================================
-- END  --  USP_SA_PostPayments_PrintReceipt
--=================================================================================================   
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
COMMIT TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT; 
SET @Success = 1; 
SET NOEXEC OFF; 
IF ( @Success = 1 )
    PRINT 'The database update succeeded'; 
ELSE
    BEGIN 
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION; 
        PRINT 'The database update failed'; 
    END;
GO
