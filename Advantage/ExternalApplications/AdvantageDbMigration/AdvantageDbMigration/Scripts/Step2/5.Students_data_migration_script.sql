-- Migration Script

-- ---------------------------------------------------------------------------------
-- File: Student_data_migration_script.sql
-- Step 2 Migration 3.8
-- Step 2: Data conversion:
-- All Student with Enrollment must be have a Lead and only one
-- The Lead Must exists if not we need to create it.
-- All Student must be have a Enrollment. if not the Student should be eliminated
-- All Student information must be copied in the assigned lead
-- Actions:
-- 1 Delete Students without Enrollments.........
-- 2 Insert in Lead Table the missing Leads
-- 3 Update StudentId in Lead Table
-- 4 Get all Address from Address Student Table (arStudAddresses)
-- 5 Emails from ArStudents --> AdLeadEmail
-- 6 Phones from arStudentPhones --> adLeadPhones
-- 7 Comment Field from Student Info Page from ArStudent --> AllNotes
-- 8 Migrate Student Skills from plStudentSkill --> AdSkill
-- 9 Migrate student notes from syStudentNotes --> AllNotes
-- 10 Migrate Prior Work from 
-- 11 Migrate Student Contact
-- 12 Migrate Student Contact Phone (other contact phone in Admission)
-- 13 Migrate Student Contact Address (other contact phone in Admission)
-- 14 Migrate Student Contact Email (other contact phone in Admission)
-- ---------------------------------------------------------------------------------


-- *************************************************************************************************
-- CREATE TEMPORALLY TABLES
-- *************************************************************************************************
SET NOCOUNT ON;

PRINT '-------- Data Migration Begin --------';

IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..studentToDelete') )
    BEGIN
        PRINT 'Dropping  temporal tables studentToDelete....';
        DROP TABLE tempdb..studentToDelete;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..leadInEnrollment') )
    BEGIN
        PRINT 'Dropping  temporal tables leadInEnrollment....';
        DROP TABLE tempdb..leadInEnrollment;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..UpdateLeadTable') )
    BEGIN
        PRINT 'Dropping  temporal tables UpdateLeadTable....';
        DROP TABLE tempdb..UpdateLeadTable;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..OrfanStudents') )
    BEGIN
        PRINT 'Dropping  temporal tables OrfanStudents....';
        DROP TABLE tempdb..OrfanStudents;
    END;
GO

CREATE TABLE tempdb..studentToDelete
    (
     StudentId UNIQUEIDENTIFIER
    );
GO
CREATE TABLE tempdb..leadInEnrollment
    (
     LeadId UNIQUEIDENTIFIER
    ,StudentId UNIQUEIDENTIFIER
    ,ModDate DATETIME
    );
GO
CREATE TABLE tempdb..UpdateLeadTable
    (
     LeadId UNIQUEIDENTIFIER
    ,StudentId UNIQUEIDENTIFIER
    );
GO
        -- Create a variable table to store the Student Without Lead
CREATE TABLE tempdb..OrfanStudents
    (
     StudentId UNIQUEIDENTIFIER
    ,LeadId UNIQUEIDENTIFIER
    ,FirstName VARCHAR(50)
    ,LastName VARCHAR(50)
    ,CampusId UNIQUEIDENTIFIER
    ,AssignedDate DATETIME
    ,AdmissionsRep UNIQUEIDENTIFIER
    ,ModDate DATETIME
    ,ModUser VARCHAR(50)
    );
GO

IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
        PRINT 'view no exist';
        DECLARE @studentId UNIQUEIDENTIFIER;
        DECLARE @leadId UNIQUEIDENTIFIER;
        DECLARE @printInfo VARCHAR(200);

        DECLARE student_cursor CURSOR
        FOR
            SELECT  AST.StudentId
            FROM    arStudent AS AST
            WHERE   AST.FirstName IS NOT NULL;
		-- because seem some student records  in QA DB have FirstName NULL

        OPEN student_cursor;

        FETCH NEXT FROM student_cursor INTO @studentId;
        PRINT 'Creating temporal Tables....';
        WHILE @@FETCH_STATUS = 0
            BEGIN

				--SET @printInfo = (
				--                   SELECT   CONCAT('Processing... ',FirstName,' ',LastName) AS Name
				--                   FROM     arStudent
				--                   WHERE    StudentId = @studentId
				--                 );
				--PRINT @printInfo;

					-- First Case Student must have at least one enrollment
                IF NOT EXISTS ( SELECT  StudentId
                                FROM    dbo.arStuEnrollments
                                WHERE   StudentId = @studentId )
                    BEGIN
						-- The student has not a enrollment. Put in the to delete table
                        INSERT  INTO tempdb.dbo.studentToDelete
                                ( StudentId )
                        VALUES  ( @studentId );
					 --  PRINT '-DELETE No Enrollment';
                        FETCH NEXT FROM student_cursor INTO @studentId;
                        CONTINUE;
                    END;

				-- Because Case 1 The Student has enrollment(s)
				-- Get all enrollments and put in variable table tempdb.dbo.leadInEnrollment
                DELETE  FROM tempdb.dbo.leadInEnrollment; -- clear table
                INSERT  INTO tempdb.dbo.leadInEnrollment  -- Get all student enrollments
                        (
                         LeadId
                        ,StudentId
                        ,ModDate
                        )
                        SELECT  LeadId
                               ,StudentId
                               ,ModDate
                        FROM    dbo.arStuEnrollments
                        WHERE   StudentId = @studentId;
				-- Second Case - The student has all lead declared in enrollment NULL: 
				-- CREATE a new Lead with studentId as LeadId
                IF NOT EXISTS ( SELECT  LeadId
                                FROM    tempdb.dbo.leadInEnrollment
                                WHERE   LeadId IS NOT NULL )
                    BEGIN
                        INSERT  INTO tempdb.dbo.OrfanStudents
                                (
                                 StudentId
                                ,LeadId
                                ,FirstName
                                ,LastName
                                ,CampusId
                                ,AssignedDate
                                ,AdmissionsRep
                                ,ModDate
                                ,ModUser
			                    )
                                SELECT TOP 1
                                        arStudent.StudentId
                                       ,NEWID()
                                       ,FirstName
                                       ,LastName
                                       ,e.CampusId
                                       ,AssignedDate
                                       ,e.AdmissionsRep
                                       ,GETDATE()
                                       ,'SUPPORT'
                                FROM    arStudent
                                JOIN    dbo.arStuEnrollments e ON e.StudentId = arStudent.StudentId
                                WHERE   arStudent.StudentId = @studentId
                                ORDER BY e.ModDate;
						-- PRINT 'NEW - All Lead in Null';
                        FETCH NEXT FROM student_cursor INTO @studentId;
                        CONTINUE;
                    END;
				-- Third Case  Student has lead(s) different of NULL but none exists in lead Table
				-- Create a New Lead with Student Id as Lead
                IF NOT EXISTS ( SELECT  ASE.LeadId
                                FROM    dbo.arStuEnrollments ASE
                                INNER JOIN adLeads AS AL ON AL.LeadId = ASE.LeadId
                                WHERE   ASE.StudentId = @studentId )
								--WHERE   StudentId = @studentId
								--        AND LeadId IN ( SELECT  LeadId
								--                        FROM    adLeads ) )
                    BEGIN
						-- The Lead or Leads does not exists in AdLeadTable, then create a new lead
                        INSERT  INTO tempdb.dbo.OrfanStudents
                                (
                                 StudentId
                                ,LeadId
                                ,FirstName
                                ,LastName
                                ,CampusId
                                ,AssignedDate
                                ,AdmissionsRep
                                ,ModDate
                                ,ModUser
			                    )
                                SELECT  arStudent.StudentId
                                       ,NEWID()
                                       ,FirstName
                                       ,LastName
                                       ,e.CampusId
                                       ,AssignedDate
                                       ,e.AdmissionsRep
                                       ,GETDATE()
                                       ,'SUPPORT'
                                FROM    arStudent
                                JOIN    dbo.arStuEnrollments e ON e.StudentId = arStudent.StudentId
                                WHERE   arStudent.StudentId = @studentId;
					 -- PRINT 'NEW Lead Does not Exists in Lead Table';
                        FETCH NEXT FROM student_cursor INTO @studentId;
                        CONTINUE;	 
                    END;

				--  Case 4: One of the lead in enrollment has a lead in table AdLead
				--  Get the Lead ID and store the info in tempdb.dbo.UpdateLeadTable variable table  
  				-- Analize all Leads in the enrollment to determinate if the lead exists in the lead table
                DECLARE leadEnrolled_cursor CURSOR
                FOR
                    SELECT  LeadId
                    FROM    tempdb.dbo.leadInEnrollment
                    WHERE   LeadId IS NOT NULL
                    ORDER BY ModDate;
                OPEN leadEnrolled_cursor;
                FETCH NEXT FROM leadEnrolled_cursor INTO @leadId;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
						-- if the lead exists then stored the lead in the table of update
						-- and also put all lead Id in the student enrollment with the same lead 
                        IF EXISTS ( SELECT  LeadId
                                    FROM    adLeads
                                    WHERE   LeadId = @leadId )
                            BEGIN
                                INSERT  INTO tempdb.dbo.UpdateLeadTable
                                        ( LeadId,StudentId )
                                VALUES  ( @leadId,@studentId );
								--PRINT 'UPDATE EXISTS THE LEAD';
                                BREAK; 
                            END;

                        FETCH NEXT FROM leadEnrolled_cursor INTO @leadId;
                    END;
                CLOSE leadEnrolled_cursor;
                DEALLOCATE leadEnrolled_cursor;

                FETCH NEXT FROM student_cursor INTO @studentId;
            END;
        CLOSE student_cursor;
        DEALLOCATE student_cursor;

        --DECLARE @ToCreateNew VARCHAR(200) = (
        --                                      SELECT    'Student That need created a new lead: ' + CONVERT(VARCHAR(10),COUNT(*))
        --                                      FROM      tempdb.dbo.OrfanStudents
        --                                    );
        --PRINT @ToCreateNew;

        --DECLARE @ToDelete VARCHAR(200)  = (
        --                                    SELECT  CONCAT('Student That has no enrollment and we need to delete:  ',COUNT(*))
        --                                    FROM    tempdb.dbo.studentToDelete
        --                                  );
        --PRINT @ToDelete;

        --DECLARE @ToUpdate VARCHAR(200)  = (
        --                                    SELECT  CONCAT('Student That need to update his lead:  ',COUNT(*))
        --                                    FROM    tempdb.dbo.UpdateLeadTable
        --                                  );
        --PRINT @ToUpdate;

        --DECLARE @TotalStudents VARCHAR(200)  = (
        --                                         SELECT CONCAT('Total Student:  ',COUNT(*))
        --                                         FROM   arStudent
        --                                       );
        --PRINT @TotalStudents;

    END;
-- *************************************************************************************************
-- END TEMPORALLY TABLES
-- *************************************************************************************************
--SELECT * FROM tempdb.dbo.OrfanStudents
--WHERE StudentId IN (SELECT studentId FROM tempdb.dbo.studentToDelete)

--SELECT * FROM tempdb.dbo.OrfanStudents
--WHERE StudentId IN (SELECT studentId FROM tempdb.dbo.UpdateLeadTable)

--SELECT * FROM tempdb.dbo.studentToDelete
--WHERE StudentId IN (SELECT studentId FROM tempdb.dbo.UpdateLeadTable)
PRINT 'Begin Transaction...........................';
-- *************************************************************************************************
-- BEGIN TRANSACTION FOR MIGRATIONS.................
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION MigrateStudents;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF EXISTS ( SELECT  1
            FROM    sys.objects AS O
            WHERE   O.name = 'arStudent'
                    AND O.type IN ( 'V' ) )
    BEGIN
        SET NOEXEC ON;
        PRINT 'student View already exist.........';
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- *************************************************************************************************
-- Begin processing Info
PRINT 'Delete Students without Enrollments.........';
GO

-- StudentId to be deleted. ................ Actions
-- Delete references in tables with StudentId as FK
-- Then delete from arStudent
DELETE  adEntrTestOverRide
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  adLeadByLeadGroups
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  adLeadEntranceTest
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  adQuickLead
WHERE   StudentID IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  adStuGrpStudents
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  arClsSectStudents
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  arFERPAPolicy
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql ' DELETE  arStudAddresses WHERE   StudentId IN ( SELECT   StudentIdFROM     tempdb.dbo.studentToDelete )'
        DELETE  arStudAddresses
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
	    --EXECUTE sp_executesql ' DELETE  arStudentPhone WHERE   StudentId IN ( SELECT   StudentId  FROM     tempdb.dbo.studentToDelete ); '
        DELETE  arStudentPhone
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

DELETE  atClsSectAttendance
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  plStudentDocs
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  plStudentEducation
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql 'DELETE  plStudentEmployment WHERE   StudentId IN ( SELECT   StudentId  FROM     tempdb.dbo.studentToDelete ); '
        DELETE  plStudentEmployment
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql 'DELETE  plStudentExtraCurriculars WHERE   StudentId IN ( SELECT   StudentId  FROM     tempdb.dbo.studentToDelete );'
        DELETE  plStudentExtraCurriculars
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql 'DELETE  plStudentSkills WHERE   StudentId IN ( SELECT   StudentId FROM  tempdb.dbo.studentToDelete );'
        DELETE  plStudentSkills
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

DELETE  [References]
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  syDocumentHistory
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  SyLoggerDualEnrollment
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DELETE  syMessages
WHERE   StudentId IN ( SELECT   StudentId
                       FROM     tempdb.dbo.studentToDelete );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql 'DELETE  syStudentContacts WHERE   StudentId IN ( SELECT   StudentId FROM     tempdb.dbo.studentToDelete );'
        DELETE  syStudentContacts
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
		--EXECUTE sp_executesql 'DELETE  syStudentNotes WHERE   StudentId IN ( SELECT   StudentId FROM     tempdb.dbo.studentToDelete );'
        DELETE  syStudentNotes
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- Delete from Students
IF NOT EXISTS ( SELECT  1
                FROM    sys.objects AS O
                WHERE   O.name = 'arStudent'
                        AND O.type IN ( 'V' ) )
    BEGIN
	    --EXECUTE sp_executesql 'DELETE  arStudent WHERE   StudentId IN ( SELECT   StudentId FROM     tempdb.dbo.studentToDelete );'
        DELETE  arStudent
        WHERE   StudentId IN ( SELECT   StudentId
                               FROM     tempdb.dbo.studentToDelete );
    END;
GO

IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- *************************************************************************************************

-- update adLeads.StudentId with '00000000-0000-0000-0000-000000000000'
PRINT 'Initially adLead.StudentId should be ''00000000-0000-0000-0000-000000000000''';												
UPDATE  AL
SET     AL.StudentId = '00000000-0000-0000-0000-000000000000'
FROM    adLeads AS AL;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- *************************************************************************************************
-- Student to Create a new Lead
-- All lead with student must be enrolled as status
--
-- 6 -- >ENROLLED 
DECLARE @Enrolled UNIQUEIDENTIFIER = (
                                       SELECT TOP 1
                                                SSC.StatusCodeId
                                       FROM     dbo.syStatusCodes AS SSC
                                       JOIN     sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                                       WHERE    SSS.SysStatusId = 6
                                     );
-- 6 -- >Future Start

DECLARE @StudentStatusActive UNIQUEIDENTIFIER = (
                                                  SELECT    SS.StatusId
                                                  FROM      syStatuses AS SS
                                                  WHERE     SS.Status = 'Active'
                                                );
-- Insert into AdLead Table the missing leads
PRINT 'Insert in Lead Table the missing Leads..........................';
INSERT  INTO dbo.adLeads
        (
         LeadId
        ,ProspectID
        ,FirstName
        ,LastName
        ,MiddleName
        ,SSN
        ,ModUser
        ,ModDate
        ,Phone
        ,HomeEmail
        ,Address1
        ,Address2
        ,City
        ,StateId
        ,Zip
        ,LeadStatus
        ,WorkEmail
        ,AddressType
        ,Prefix
        ,Suffix
        ,BirthDate
        ,Sponsor
        ,AdmissionsRep
        ,AssignedDate
        ,Gender
        ,Race
        ,MaritalStatus
        ,FamilyIncome
        ,Children
        ,PhoneType
        ,PhoneStatus
        ,SourceCategoryID
        ,SourceTypeID
        ,SourceDate
        ,AreaID
        ,ProgramID
        ,ExpectedStart
        ,ShiftID
        ,Nationality
        ,Citizen
        ,DrivLicStateID
        ,DrivLicNumber
        ,AlienNumber
        ,Comments
        ,SourceAdvertisement
        ,CampusId
        ,PrgVerId
        ,Country
        ,County
        ,Age
        ,PreviousEducation
        ,AddressStatus
        ,CreatedDate
        ,RecruitmentOffice
        ,OtherState
        ,ForeignPhone
        ,ForeignZip
        ,LeadgrpId
        ,DependencyTypeId
        ,DegCertSeekingId
        ,GeographicTypeId
        ,HousingId
        ,admincriteriaid
        ,DateApplied
        ,InquiryTime
        ,AdvertisementNote
        ,Phone2
        ,PhoneType2
        ,PhoneStatus2
        ,ForeignPhone2
        ,DefaultPhone
        ,IsDisabled
        ,IsFirstTimeInSchool
        ,IsFirstTimePostSecSchool
        ,EntranceInterviewDate
        ,CampaignId
        ,ProgramOfInterest
        ,CampusOfInterest
        ,TransportationId
        ,NickName
        ,AttendTypeId
        ,PreferredContactId
        ,AddressApt
        ,NoneEmail
        ,HighSchoolId
        ,HighSchoolGradDate
        ,AttendingHs
        ,ProgramScheduleId
        ,BestTime
        ,DistanceToSchool
        ,StudentId
        ,StudentNumber
        ,StudentStatusId
        ,ReasonNotEnrolledId
        ,EnrollStateId
        )
        SELECT  LeadId  -- LeadId - uniqueidentifier
               ,''  -- ProspectID - varchar(50)
               ,FirstName  -- FirstName - varchar(50)
               ,LastName  -- LastName - varchar(50)
               ,''  -- MiddleName - varchar(50)
               ,''  -- SSN - varchar(50)
               ,'SUPPORT'  -- ModUser - varchar(50)
               ,GETDATE()  -- ModDate - datetime
               ,''  -- Phone - varchar(50)
               ,''  -- HomeEmail - varchar(50)
               ,''  -- Address1 - varchar(50)
               ,''  -- Address2 - varchar(50)
               ,''  -- City - varchar(50)
               ,NULL  -- StateId - uniqueidentifier
               ,''  -- Zip - varchar(50)
               ,@Enrolled   -- LeadStatus - uniqueidentifier
               ,''  -- WorkEmail - varchar(50)
               ,NULL  -- AddressType - uniqueidentifier
               ,NULL  -- Prefix - uniqueidentifier
               ,NULL  -- Suffix - uniqueidentifier
               ,NULL  -- BirthDate - datetime
               ,NULL  -- Sponsor - uniqueidentifier
               ,AdmissionsRep  -- AdmissionsRep - uniqueidentifier
               ,AssignedDate  -- AssignedDate - datetime
               ,NULL  -- Gender - uniqueidentifier
               ,NULL  -- Race - uniqueidentifier
               ,NULL  -- MaritalStatus - uniqueidentifier
               ,NULL  -- FamilyIncome - uniqueidentifier
               ,NULL  -- Children - varchar(50)
               ,NULL  -- PhoneType - uniqueidentifier
               ,NULL  -- PhoneStatus - uniqueidentifier
               ,NULL  -- SourceCategoryID - uniqueidentifier
               ,NULL  -- SourceTypeID - uniqueidentifier
               ,NULL  -- SourceDate - datetime
               ,NULL  -- AreaID - uniqueidentifier
               ,NULL  -- ProgramID - uniqueidentifier
               ,NULL  -- ExpectedStart - datetime
               ,NULL  -- ShiftID - uniqueidentifier
               ,NULL  -- Nationality - uniqueidentifier
               ,NULL  -- Citizen - uniqueidentifier
               ,NULL  -- DrivLicStateID - uniqueidentifier
               ,NULL  -- DrivLicNumber - varchar(50)
               ,NULL  -- AlienNumber - varchar(50)
               ,NULL  -- Comments - varchar(240)
               ,NULL  -- SourceAdvertisement - uniqueidentifier
               ,CampusId  -- CampusId - uniqueidentifier
               ,NULL  -- PrgVerId - uniqueidentifier
               ,NULL  -- Country - uniqueidentifier
               ,NULL  -- County - uniqueidentifier
               ,NULL  -- Age - varchar(3)
               ,NULL  -- PreviousEducation - uniqueidentifier
               ,NULL  -- AddressStatus - uniqueidentifier
               ,NULL  -- CreatedDate - datetime
               ,NULL  -- RecruitmentOffice - varchar(50)
               ,NULL  -- OtherState - varchar(50)
               ,0  -- ForeignPhone - bit
               ,0  -- ForeignZip - bit
               ,NULL  -- LeadgrpId - uniqueidentifier
               ,NULL  -- DependencyTypeId - uniqueidentifier
               ,NULL  -- DegCertSeekingId - uniqueidentifier
               ,NULL  -- GeographicTypeId - uniqueidentifier
               ,NULL  -- HousingId - uniqueidentifier
               ,NULL  -- admincriteriaid - uniqueidentifier
               ,NULL  -- DateApplied - datetime
               ,NULL  -- InquiryTime - varchar(50)
               ,NULL  -- AdvertisementNote - varchar(50)
               ,NULL  -- Phone2 - varchar(50)
               ,NULL  -- PhoneType2 - uniqueidentifier
               ,NULL  -- PhoneStatus2 - uniqueidentifier
               ,NULL  -- ForeignPhone2 - bit
               ,0  -- DefaultPhone - tinyint
               ,NULL  -- IsDisabled - bit               
               ,1  -- IsFirstTimeInSchool - bit
               ,0  -- IsFirstTimePostSecSchool - bit
               ,NULL  -- EntranceInterviewDate - datetime
               ,NULL  -- CampaignId - int
               ,NULL  -- ProgramOfInterest - varchar(100)
               ,NULL  -- CampusOfInterest - varchar(100)
               ,NULL  -- TransportationId - uniqueidentifier
               ,''  -- NickName - varchar(50)
               ,NULL  -- AttendTypeId - uniqueidentifier
               ,0  -- PreferredContactId - int
               ,''  -- AddressApt - varchar(20)
               ,0  -- NoneEmail - bit
               ,NULL  -- HighSchoolId - uniqueidentifier
               ,NULL  -- HighSchoolGradDate - datetime
               ,NULL  -- AttendingHs - bit
               ,NULL  -- ProgramScheduleId - uniqueidentifier
               ,NULL  -- BestTime - datetime
               ,NULL  -- DistanceToSchool - int
               ,StudentId -- StudentId - uniqueidentifier
               ,N''  -- StudentNumber - nvarchar(50)
               ,@StudentStatusActive  -- StudentStatusId - uniqueidentifier
               ,NULL  -- ReasonNotEnrolledId - uniqueidentifier
               ,NULL  -- EnrollStateId - uniqueidentifier
        FROM    tempdb.dbo.OrfanStudents;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- Copy the OrphanLead in table UpdateLead()
PRINT 'Copy the Orphan Students to the lead Table to make the update only one time...';
-- In the next step the lead should be updated..........
INSERT  INTO tempdb.dbo.UpdateLeadTable
        (
         LeadId
        ,StudentId
        )
        SELECT  LeadId
               ,StudentId
        FROM    tempdb.dbo.OrfanStudents;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--Select * from tempdb.dbo.UpdateLeadTable

--SElect COUNT(*) as tot , StudentId 
--FROM tempdb.dbo.UpdateLeadTable 
--Group by StudentID
--Having COUNT(*) > 1
---- In the next step the lead should be updated..........
--INSERT  INTO tempdb.dbo.UpdateLeadTable
--       ( LeadId
--       ,StudentId
--       )
--       SELECT  LeadId
--              ,StudentId
--       FROM    tempdb.dbo.OrfanStudents;

--SELEct * from tempdb.dbo.OrfanStudents

--SElect COUNT(LeadId) as tot , StudentId 
--FROM tempdb.dbo.UpdateLeadTable 
--Group by StudentID
--Having COUNT(LeadId) > 1

--SElect COUNT(StudentId) as tot , leadId 
--FROM tempdb.dbo.UpdateLeadTable 
--Group by leadId
--Having COUNT(StudentId) > 1

--SELECT * From adLeads WHERe studentID in (Select studentId from tempdb.dbo.UpdateLeadTable)


--SELECT AL.leadId, AL.StudentId, U.* From adLeads AS AL
--INNEr JOIN  tempdb.dbo.UpdateLeadTable AS U ON U.StudentId = AL.StudentId
--WHERE AL.LeadId <> U.LeadId



--SELECT AL.leadId, AL.StudentId, U.* From adLeads AS AL
--INNEr JOIN  tempdb.dbo.UpdateLeadTable AS U ON U.leadId = AL.LeadId
--WHERE AL.StudentId <> U.StudentId


-- *************************************************************************************************
-- PARTE 3
-- *************************************************************************************************
-- Update Leads with the information of StudentId
PRINT 'Update StudentId in Lead Table...............';
UPDATE  AL
SET     AL.StudentId = ULT.StudentId
FROM    adLeads AS AL
INNER JOIN tempdb.dbo.UpdateLeadTable AS ULT ON ULT.LeadId = AL.LeadId;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- *************************************************************************************************
-- Student to Create a new Lead
-- All lead with student must be enrolled as status
--
-- 6 -- >ENROLLED 
DECLARE @Enrolled UNIQUEIDENTIFIER = (
                                       SELECT TOP 1
                                                SSC.StatusCodeId
                                       FROM     dbo.syStatusCodes AS SSC
                                       JOIN     sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                                       WHERE    SSS.SysStatusId = 6
                                     );
-- 6 -- >Future Start
-- Update the rest of the fields
PRINT 'Update the rest of the field in AdLead Table wit the Student Fields';
UPDATE  AL
SET     --ProspectId
        AL.FirstName = AST.FirstName
       ,AL.LastName = AST.LastName
       ,AL.MiddleName = AST.MiddleName
       ,AL.SSN = AST.SSN
       ,AL.ModUser = AST.ModUser
       ,AL.ModDate = GETDATE()  
	   --Phone
	   --HomeEmail
	   --Address1
	   --Address2
	   --City
	   --StateId
	   --Zip
       ,AL.LeadStatus = @Enrolled
	    --WorkEmail
		--AddressType
       ,AL.Prefix = AST.Prefix
       ,AL.Suffix = AST.Suffix
       ,AL.BirthDate = AST.DOB
       ,AL.Sponsor = AST.Sponsor
       ,AL.AdmissionsRep = ASE.AdmissionsRep
       ,AL.AssignedDate = AST.AssignedDate
       ,AL.Gender = AST.Gender
       ,AL.Race = AST.Race
       ,AL.MaritalStatus = AST.MaritalStatus
       ,AL.FamilyIncome = AST.FamilyIncome
       ,AL.Children = CONVERT(NVARCHAR(50),AST.Children)
	   --PhoneType
	   --PheoneStatus
	   --SourceCategoryID
	   --SourceTyepwID
       ,AL.SourceDate = AST.SourceDate 
	   --AreaID
       ,AL.ExpectedStart = AST.ExpectedStart
       ,AL.ShiftID = AST.ShiftId
       ,AL.Nationality = AST.Nationality
       ,AL.Citizen = AST.Citizen
       ,AL.DrivLicStateID = AST.DrivLicStateId
       ,AL.DrivLicNumber = AST.DrivLicNumber
       ,AL.AlienNumber = AST.AlienNumber
       -- ,AL.Comments = SUBSTRING(AST.Comments, 1, 240) 
       ,AL.CampusId = ASE.CampusId
	   --PrgVerId
	   --Country
	   --County
	   --Age
       ,AL.PreviousEducation = AST.EdLvlId
	   --AddressStatus
	   --CreatedDate
	   --RecruitmentOffice
	   --OtherState
	   --ForeignPhone
	   --ForeignZip
	   --Leadgrpid
       ,AL.DependencyTypeId = AST.DependencyTypeId
       ,AL.DegCertSeekingId = AST.DegCertSeekingId
       ,AL.GeographicTypeId = AST.GeographicTypeId
       ,AL.HousingId = AST.HousingId
       ,AL.admincriteriaid = AST.admincriteriaid
	   --DateApplied
	   --InquiryTime
	   --AdvertisementNote
	   --Phone2
	   --PhoneType2
	   --PhoneStatus2
	   --ForeignPhone2
	   --DefaultPhone
       ,AL.IsDisabled = ASE.IsDisabled
       ,AL.IsFirstTimeInSchool = ASE.IsFirstTimeInSchool
       ,AL.IsFirstTimePostSecSchool = ASE.IsFirstTimePostSecSchool
       ,AL.EntranceInterviewDate = ASE.EntranceInterviewDate
	   --CampaignId
	   --ProgramOfInterest
	   --TransportationId
	   --NickName
       ,AL.AttendTypeId = AST.AttendTypeId
	   --PreferredContactId
	   --AddressApt
	   --NoneEmail
	   --HighSchoolId
	   --HighSchoolGradDate
	   --AttendingHs
	   --ProgramSchedculeId
	   --BestTime
	   --DistanceToSchool
	   --StudentId
       ,AL.StudentNumber = CASE WHEN AST.StudentNumber IS NULL THEN ''
                                ELSE AST.StudentNumber
                           END
       ,AL.StudentStatusId = AST.StudentStatus
	   --AL.ReasonNotEnrolledId
	   --AL.EnrollStateId
FROM    adLeads AS AL
INNER JOIN arStudent AS AST ON AST.StudentId = AL.StudentId
INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
INNER JOIN (
             SELECT ROW_NUMBER() OVER ( PARTITION BY AST.StudentId ORDER BY ASE.EnrollDate ASC ) AS RowNumber
                   ,AST.StudentId
                   ,ASE.StuEnrollId
                   ,ASE.EnrollDate
             FROM   arStudent AS AST
             INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
           ) AS T ON T.StudentId = ASE.StudentId
                     AND T.StuEnrollId = ASE.StuEnrollId
                     AND T.RowNumber = 1;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO


-- ************************************************************************************************
-- Update adLeads  all without Enrollment records AND/OR without Student record  
--                 and with leadStatusId = 'Enrolled' change for first code found for 'New Lead'
-- ************************************************************************************************
DECLARE @NewLeadStatusCode AS UNIQUEIDENTIFIER;
SELECT TOP 1
        @NewLeadStatusCode = SSC.StatusCodeId
FROM    syStatusCodes AS SSC
INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
WHERE   SSS.SysStatusDescrip = 'New Lead';

IF ( (
       SELECT   COUNT(AL.LeadStatus)
       FROM     adLeads AS AL
       INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = AL.LeadStatus
       INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
       LEFT JOIN arStudent AS AST ON AST.StudentId = AL.StudentId
       WHERE    AST.StudentId IS NULL
                AND SSS.SysStatusDescrip = 'Enrolled'
     ) > 0 )
    BEGIN
        UPDATE  AL
        SET     AL.LeadStatus = @NewLeadStatusCode
        FROM    adLeads AS AL
        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = AL.LeadStatus
        INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
        LEFT JOIN arStudent AS AST ON AST.StudentId = AL.StudentId
        WHERE   AST.StudentId IS NULL
                AND SSS.SysStatusDescrip = 'Enrolled';

        IF @@ERROR <> 0
            SET NOEXEC ON;
    END;
IF ( (
       SELECT   COUNT(AL.LeadStatus)
       FROM     adLeads AS AL
       INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = AL.LeadStatus
       INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
       LEFT JOIN arStuEnrollments AS ASE ON ASE.StudentId = AL.StudentId
       WHERE    ASE.StudentId IS NULL
                AND SSS.SysStatusDescrip = 'Enrolled'
     ) > 0 )
    BEGIN
        UPDATE  AL
        SET     AL.LeadStatus = @NewLeadStatusCode
        FROM    adLeads AS AL
        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = AL.LeadStatus
        INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
        LEFT JOIN arStuEnrollments AS ASE ON ASE.StudentId = AL.StudentId
        WHERE   ASE.StudentId IS NULL
                AND SSS.SysStatusDescrip = 'Enrolled';

        IF @@ERROR <> 0
            SET NOEXEC ON;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
--SELECT  StudentNumber
--FROM    arStudent;

-- ************************************************************************
-- Migrate Student Address
-- ************************************************************************			
-- Get all address from Address Table
PRINT 'Migrate Addresses...........................';
-- Clear the info of Addresses for enrolled student
DELETE  adLeadAddresses
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Get all addresses
INSERT  dbo.adLeadAddresses
        (
         adLeadAddressId
        ,LeadId
        ,AddressTypeId
        ,Address1
        ,Address2
        ,City
        ,StateId
        ,ZipCode
        ,CountryId
        ,StatusId
        ,IsMailingAddress
        ,IsShowOnLeadPage
        ,ModDate
        ,ModUser
        ,State
        ,IsInternational
        ,CountyId
        ,ForeignCountyStr
        ,ForeignCountryStr
        )
        SELECT  NEWID()  -- adLeadAddressId - uniqueidentifier
               ,l.LeadId  -- LeadId - uniqueidentifier
               ,CASE WHEN s.AddressTypeId IS NULL THEN (
                                                         SELECT TOP 1
                                                                AddressTypeId
                                                         FROM   dbo.plAddressTypes
                                                       )
                     ELSE s.AddressTypeId
                END  -- AddressTypeId - uniqueidentifier
               ,s.Address1  -- Address1 - varchar(250)
               ,s.Address2  -- Address2 - varchar(250)
               ,s.City  -- City - varchar(250)
               ,s.StateId  -- StateId - uniqueidentifier
               ,SUBSTRING(LTRIM(RTRIM(s.Zip)),1,10)  -- ZipCode - varchar(10)
               ,s.CountryId  -- CountryId - uniqueidentifier
               ,s.StatusId  -- StatusId - uniqueidentifier
               ,s.default1  -- IsMailingAddress - bit
               ,s.default1  -- IsShowOnLeadPage - bit
               ,s.ModDate  -- ModDate - datetime
               ,s.ModUser  -- ModUser - varchar(50)
               ,s.OtherState  -- State - varchar(100)
               ,CASE WHEN s.OtherState IS NOT NULL THEN 1
                     ELSE 0
                END   -- IsInternational - bit
               ,(
                  SELECT    County
                  FROM      arStudent
                  WHERE     StudentId = l.StudentId
                )  -- CountyId - uniqueidentifier
               ,''  -- County - varchar(100)
               ,''  -- Country - varchar(100)
        FROM    dbo.arStudAddresses s
        JOIN    adLeads l ON l.StudentId = s.StudentId
        WHERE   s.Address1 IS NOT NULL;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

DECLARE @phoneNumber INTEGER = (
                                 SELECT COUNT(*)
                                 FROM   adLeadPhone
                               );
--PRINT CONCAT('Records IN Phone TABLE ',@phoneNumber);
-- Fill The Lead Email Table
-- Delete All Lead Email Information that has a Student

-- ************************************************************************
-- BEGIN CURSOR OVER adLeads LeadId & StudentID:
-- MIGRATE:
-- 1 Emails
-- 2 Phones
-- 3 Comment Field from Student Info Page
-- 4 Migrate Student Skills
-- 5 Migrate Student Prior Work
-- 6 Migrate Student Contact information (other contacts in Admission)
-- 7 Migrate Student Contact Phone (other contact phone in Admission)
-- 8 Migrate Student Contact Address (other contact phone in Admission)
-- 9 Migrate Student Contact Email (other contact phone in Admission)
-- ************************************************************************
PRINT 'Migrate Emails & Phones & Comment Field & Skills & Prior Work...';
DELETE  AdLeadEmail
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Delete all migrate previous information
DELETE  dbo.adLeadPhone
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Delete all migrate previous information
DELETE  dbo.adSkills
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Clear the adLEadEmployment for previous student migrations.
DELETE  dbo.adLeadEmployment
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Clear the adLeadOtherContactsPhone.
DELETE  dbo.adLeadOtherContactsPhone
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Clear the adLeadOtherContactsAddresses.
DELETE  dbo.adLeadOtherContactsAddreses
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Clear the adLeadOtherContactsEmail.
DELETE  dbo.adLeadOtherContactsEmail
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Clear the adLeadOtherContacts.
DELETE  dbo.adLeadOtherContacts
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

DECLARE @studentId AS UNIQUEIDENTIFIER;
DECLARE @leadId UNIQUEIDENTIFIER;
DECLARE @phoneId AS UNIQUEIDENTIFIER;
DECLARE @HomeEmailTypeId UNIQUEIDENTIFIER;
DECLARE @WorkEmailTypeId UNIQUEIDENTIFIER;
DECLARE @StatusIdActive UNIQUEIDENTIFIER;
DECLARE @ContactTypePrimary UNIQUEIDENTIFIER;
DECLARE @ContactTypeOther UNIQUEIDENTIFIER;
DECLARE @PhoneTypeUnknown UNIQUEIDENTIFIER;
DECLARE @AddressTypeOther UNIQUEIDENTIFIER;

-- Create Values used in all operation
SET @HomeEmailTypeId = (
                         SELECT TOP 1
                                EMailTypeId
                         FROM   dbo.syEmailType
                         WHERE  EMailTypeCode = 'Home'
                       );
SET @WorkEmailTypeId = (
                         SELECT TOP 1
                                EMailTypeId
                         FROM   dbo.syEmailType
                         WHERE  EMailTypeCode = 'Work'
                       );
SET @StatusIdActive = (
                        SELECT TOP 1
                                SS.StatusId
                        FROM    syStatuses AS SS
                        WHERE   SS.StatusCode = 'A'
                      );

SET @ContactTypePrimary = (
                            SELECT TOP 1
                                    ContactTypeId
                            FROM    dbo.syContactTypes
                            WHERE   ContactTypeCode = 'Primary'
                          );

SET @ContactTypeOther = (
                          SELECT TOP 1
                                    ContactTypeId
                          FROM      dbo.syContactTypes
                          WHERE     ContactTypeCode = 'Other'
                        );
SET @PhoneTypeUnknown = (
                          SELECT TOP 1
                                    PhoneTypeId
                          FROM      dbo.syPhoneType
                          WHERE     PhoneTypeCode = 'Unknown'
                        );
SET @AddressTypeOther = (
                          SELECT TOP 1
                                    AddressTypeId
                          FROM      dbo.plAddressTypes
                          WHERE     AddressCode = 'Other'
                        );
-- @AddresTypeOther is hard code because AMC AddressCode is not "Other"
SET @AddressTypeOther = 'CE4089CF-E578-45CF-A90C-E3040AE24407';


DECLARE lead_cursor CURSOR
FOR
    SELECT  LeadId
           ,StudentId
    FROM    adLeads
    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000'
    ORDER BY ModDate;
OPEN lead_cursor;
FETCH NEXT FROM lead_cursor INTO @leadId,@studentId;
WHILE @@FETCH_STATUS = 0
    BEGIN
		    -- See if the email exists
        IF (
             SELECT HomeEmail
             FROM   arStudent
             WHERE  StudentId = @studentId
           ) IS NOT NULL
            BEGIN
                -- Exists insert it
                INSERT  dbo.AdLeadEmail
                        (
                         LeadEMailId
                        ,LeadId
                        ,EMail
                        ,EMailTypeId
                        ,IsPreferred
                        ,IsPortalUserName
                        ,ModUser
                        ,ModDate
                        ,StatusId
                        ,IsShowOnLeadPage
                        )
                        SELECT  NEWID()  -- LeadEMailId - uniqueidentifier
                               ,@leadId  -- LeadId - uniqueidentifier
                               ,s.HomeEmail  -- EMail - varchar(100)
                               ,@HomeEmailTypeId  -- EMailTypeId - Home uniqueidentifier
                               ,1  -- IsPreferred - bit
                               ,0  -- IsPortalUserName - bit
                               ,'SUPPORT'  -- ModUser - varchar(50)
                               ,GETDATE()  -- ModDate - datetime
                               ,@StatusIdActive  -- StatusId A - uniqueidentifier
                               ,1  -- IsShowOnLeadPage - bit
                        FROM    arStudent s
                        WHERE   StudentId = @studentId;
                IF @@ERROR <> 0
                    SET NOEXEC ON;
            END;	
        IF (
             SELECT WorkEmail
             FROM   arStudent
             WHERE  StudentId = @studentId
           ) IS NOT NULL
            BEGIN
                -- Exists insert it
                INSERT  dbo.AdLeadEmail
                        (
                         LeadEMailId
                        ,LeadId
                        ,EMail
                        ,EMailTypeId
                        ,IsPreferred
                        ,IsPortalUserName
                        ,ModUser
                        ,ModDate
                        ,StatusId
                        ,IsShowOnLeadPage
                        )
                        SELECT  NEWID()  -- LeadEMailId - uniqueidentifier
                               ,@leadId  -- LeadId - uniqueidentifier
                               ,s.WorkEmail  -- EMail - varchar(100)
                               ,@WorkEmailTypeId  -- EMailTypeId - Work uniqueidentifier
                               ,0  -- IsPreferred - bit
                               ,0  -- IsPortalUserName - bit
                               ,'SUPPORT'  -- ModUser - varchar(50)
                               ,GETDATE()  -- ModDate - datetime
                               ,@StatusIdActive  -- StatusId A - uniqueidentifier
                               ,1  -- IsShowOnLeadPage - bit
                        FROM    arStudent s
                        WHERE   StudentId = @studentId;

                IF @@ERROR <> 0
                    SET NOEXEC ON;
            END;

	    -- See if we need to import Phones
        IF (
             SELECT COUNT(*)
             FROM   dbo.arStudentPhone
             WHERE  StudentId = @studentId
           ) > 0
            BEGIN
                DECLARE phone_cursor CURSOR
                FOR
                    SELECT  StudentPhoneId
                    FROM    dbo.arStudentPhone
                    WHERE   StudentId = @studentId
                    ORDER BY default1;
                OPEN phone_cursor;
                DECLARE @position INT = 1;
                FETCH NEXT FROM phone_cursor INTO @phoneId;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
						-- Insert phone
                        INSERT  dbo.adLeadPhone
                                (
                                 LeadPhoneId
                                ,LeadId
                                ,PhoneTypeId
                                ,Phone
                                ,ModDate
                                ,ModUser
                                ,Position
                                ,Extension
                                ,IsForeignPhone
                                ,IsBest
                                ,IsShowOnLeadPage
                                ,StatusId
                                )
                                SELECT  NEWID()  -- LeadPhoneId - uniqueidentifier
                                       ,@leadId  -- LeadId - uniqueidentifier
                                       ,ISNULL(PhoneTypeId,@PhoneTypeUnknown)  -- PhoneTypId - uniqueidentifier
                                       ,Phone  -- Phone - varchar(15)
                                       ,ModDate  -- ModDate - datetime
                                       ,ModUser  -- ModUser - varchar(50)
                                       ,@position  -- Position - int
                                       ,( CASE WHEN Ext IS NULL THEN ''
                                               ELSE  SUBSTRING(LTRIM(RTRIM(Ext)),1,10) --Ext
                                          END )  -- Extension - varchar(10)
                                       ,ForeignPhone  -- IsForeignPhone - bit
                                       ,( CASE WHEN @position = 1 THEN 1
                                               ELSE 0
                                          END ) AS IsBest  -- IsBest - bit
                                       ,CASE WHEN @position < 4 THEN 1
                                             ELSE 0
                                        END -- IsShowOnLeadPage - bit
                                       ,StatusId  -- StatusId - uniqueidentifier
                                FROM    arStudentPhone
                                WHERE   StudentPhoneId = @phoneId
                                        AND Phone IS NOT NULL;
                        IF @@ERROR <> 0
                            SET NOEXEC ON;
                        SET @position = @position + 1;

                        FETCH NEXT FROM phone_cursor INTO @phoneId;
                    END;
                CLOSE phone_cursor;
                DEALLOCATE phone_cursor;
            END;

-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Skills (level can not be migrated!)
-- ************************************************************************
        INSERT  INTO dbo.adSkills
                (
                 LeadId
                ,LevelId
                ,SkillGrpId
                ,Description
                ,Comment
                ,ModDate
                ,ModUser
                )
                SELECT  @leadId  -- LeadId - uniqueidentifier
                       ,1  -- LevelId - int
                       ,sk.SkillGrpId  -- SkillGrpId - uniqueidentifier
                       ,pl.SkillDescrip  -- Description - varchar(50)
                       ,g.SkillGrpName  -- Comment - varchar(100)
                       ,GETDATE()  -- ModDate - datetime
                       ,''  -- ModUser - varchar(50)
                FROM    dbo.plStudentSkills sk
                JOIN    dbo.plSkills pl ON pl.SkillId = sk.SkillId
                JOIN    dbo.plSkillGroups g ON g.SkillGrpId = sk.SkillGrpId
                WHERE   StudentId = @studentId;

        IF @@ERROR <> 0
            SET NOEXEC ON;
-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Info Comment Page to AllNotes
-- ************************************************************************      
	-- Put the comment in the AllNotes table and link it
        IF (
             SELECT Comments
             FROM   arStudent
             WHERE  StudentId = @studentId
           ) IS NOT NULL
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    adLead_Notes a
                                JOIN    AllNotes n ON n.NotesId = a.NotesId
                                WHERE   n.ModuleCode = 'AR'
                                        AND NoteType = 'Comment'
                                        AND LeadId = @leadId )
                    BEGIN
                        INSERT  INTO dbo.AllNotes
                                (
                                 ModuleCode
                                ,NoteType
                                ,UserId
                                ,NoteText
                                ,ModUser
                                ,ModDate
                                ,PageFieldId
			                    )
                        VALUES  (
                                 'AR'  -- ModuleCode - char(2)
                                ,'Comment'  -- NoteType - varchar(50)
                                ,(
                                   SELECT   UserId
                                   FROM     syUsers
                                   WHERE    UserName = 'SUPPORT'
                                 )  -- UserId - uniqueidentifier
                                ,(
                                   SELECT   Comments
                                   FROM     arStudent
                                   WHERE    StudentId = @studentId
                                 )  -- NoteText - varchar(2000)
                                ,'SUPPORT'  -- ModUser - varchar(50)
                                ,GETDATE()  -- ModDate - datetime
                                ,2  -- PageFieldId - int (INFO)
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;

                        INSERT  INTO dbo.adLead_Notes
                                (
                                 LeadId
                                ,NotesId
                                )
                        VALUES  (
                                 @leadId  -- LeadId - uniqueidentifier
                                ,IDENT_CURRENT('AllNotes')  -- NotesId - int
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;
                    END; 
            END;
-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Info Objective Page to AllNotes
-- ************************************************************************      
	-- Put the comment in the AllNotes table and link it
        IF (
             SELECT AST.Objective
             FROM   arStudent AS AST
             WHERE  AST.StudentId = @studentId
           ) IS NOT NULL
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    adLead_Notes AS ALN
                                INNER JOIN AllNotes AS AN ON AN.NotesId = ALN.NotesId
                                WHERE   AN.ModuleCode = 'AR'
                                        AND AN.NoteType = 'Objective'
                                        AND ALN.LeadId = @leadId )
                    BEGIN
                        INSERT  INTO dbo.AllNotes
                                (
                                 ModuleCode
                                ,NoteType
                                ,UserId
                                ,NoteText
                                ,ModUser
                                ,ModDate
                                ,PageFieldId
			                    )
                        VALUES  (
                                 'AR'  -- ModuleCode - char(2)
                                ,'Objective'  -- NoteType - varchar(50)
                                ,(
                                   SELECT   SU.UserId
                                   FROM     syUsers AS SU
                                   WHERE    SU.UserName = 'SUPPORT'
                                 )  -- UserId - uniqueidentifier
                                ,(
                                   SELECT   AST.Objective
                                   FROM     arStudent AS AST
                                   WHERE    AST.StudentId = @studentId
                                 )  -- NoteText - varchar(2000)
                                ,'SUPPORT'  -- ModUser - varchar(50)
                                ,GETDATE()  -- ModDate - datetime
                                ,2  -- PageFieldId - int (INFO)
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;

                        INSERT  INTO dbo.adLead_Notes
                                (
                                 LeadId
                                ,NotesId
                                )
                        VALUES  (
                                 @leadId  -- LeadId - uniqueidentifier
                                ,IDENT_CURRENT('AllNotes')  -- NotesId - int
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;
                    END; 
            END;

-- ********************************************************************
-- INSIDE CURSOR : MIGRATE syStudentNotes --> AllNotes
-- ********************************************************************

        IF (
             SELECT COUNT(*)
             FROM   dbo.syStudentNotes
             WHERE  StudentId = @studentId
           ) > 0
            BEGIN
                DECLARE @cursorId VARCHAR(50); --UNIQUEIDENTIFIER;
                DECLARE notes_cursor CURSOR
                FOR
                    SELECT  StudentNoteId
                    FROM    dbo.syStudentNotes
                    WHERE   StudentId = @studentId;
                OPEN notes_cursor;
                FETCH NEXT FROM notes_cursor INTO @cursorId;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        DECLARE @userId UNIQUEIDENTIFIER = (
                                                             SELECT UserId
                                                             FROM   syStudentNotes
                                                             WHERE  CONVERT(NVARCHAR(50),StudentNoteId) = @cursorId
                                                           );
					-- Insert Notes
                        INSERT  INTO AllNotes
                                (
                                 ModuleCode
                                ,NoteType
                                ,UserId
                                ,NoteText
                                ,ModUser
                                ,ModDate
                                ,PageFieldId
			                    )
                        VALUES  (
                                 'AR'  -- ModuleCode - char(2)
                                ,'Note'  -- NoteType - varchar(50)
                                ,@userId -- UserId - uniqueidentifier
                                ,(
                                   SELECT   StudentNoteDescrip
                                   FROM     syStudentNotes
                                   WHERE    CONVERT(NVARCHAR(50),StudentNoteId) = @cursorId
                                 )  -- NoteText - varchar(2000)
                                ,@userId  -- ModUser - varchar(50)
                                ,GETDATE()  -- ModDate - datetime
                                ,1  -- PageFieldId - int (INFO)
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;

                        INSERT  INTO dbo.adLead_Notes
                                (
                                 LeadId
                                ,NotesId
                                )
                        VALUES  (
                                 @leadId  -- LeadId - uniqueidentifier
                                ,IDENT_CURRENT('AllNotes')  -- NotesId - int
			                    );
                        IF @@ERROR <> 0
                            SET NOEXEC ON;					

                        FETCH NEXT FROM notes_cursor INTO @cursorId;
                    END;
                CLOSE notes_cursor;
                DEALLOCATE notes_cursor;
            END;
	-----------------------------------------------------------------
-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Prior Work from Student Table to Lead
-- plStudentEmployment -- > adLeadEmployment
-- ************************************************************************
        INSERT  INTO dbo.adLeadEmployment
                (
                 StEmploymentId
                ,LeadId
                ,JobTitleId
                ,JobStatusId
                ,StartDate
                ,EndDate
                ,Comments
                ,ModUser
                ,ModDate
                ,JobResponsibilities
                ,EmployerName
                ,EmployerJobTitle
                )
                SELECT  se.StEmploymentId  -- StEmploymentId - uniqueidentifier
                       ,(
                          SELECT    LeadId
                          FROM      dbo.adLeads
                          WHERE     StudentId = @studentId
                        )  -- LeadId - uniqueidentifier
                       ,se.JobTitleId  -- JobTitleId - uniqueidentifier
                       ,se.JobStatusId  -- JobStatusId - uniqueidentifier
                       ,se.StartDate  -- StartDate - datetime
                       ,se.EndDate  -- EndDate - datetime
                       ,se.Comments  -- Comments - varchar(300)
                       ,ISNULL(se.ModUser,'Support') AS ModUser  -- ModUser - varchar(50)
                       ,ISNULL(se.ModDate,GETDATE()) AS ModDate  -- ModDate - datetime
                       ,se.JobResponsibilities -- JobResponsibilities - varchar(300)
                       ,se.EmployerName  -- EmployerName - varchar(50)
                       ,se.EmployerJobTitle  -- EmployerJobTitle - varchar(50)
                FROM    dbo.plStudentEmployment se
                WHERE   se.StudentId = @studentId;  
        IF @@ERROR <> 0
            SET NOEXEC ON;	

-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Contact from Academics to Admission
-- syStudentContacts -- > adLeadOtherContacts
-- ************************************************************************
        INSERT  INTO dbo.adLeadOtherContacts
                (
                 OtherContactId
                ,LeadId
                ,StatusId
                ,RelationshipId
                ,ContactTypeId
                ,PrefixId
                ,SufixId
                ,FirstName
                ,LastName
                ,MiddleName
                ,Comments
                ,ModDate
                ,ModUser
                )
                SELECT  sc.StudentContactId  -- OtherContactId - uniqueidentifier
                       ,(
                          SELECT    LeadId
                          FROM      dbo.adLeads
                          WHERE     StudentId = @studentId
                        )  -- LeadId - uniqueidentifier  -- LeadId - uniqueidentifier
                       ,sc.StatusId  -- StatusId - uniqueidentifier
                       ,sc.RelationId  -- RelationshipId - uniqueidentifier
                       ,CASE WHEN EXISTS ( SELECT   1
                                           FROM     dbo.adLeadOtherContacts
                                           WHERE    LeadId = @leadId
                                                    AND ContactTypeId = @ContactTypePrimary ) THEN @ContactTypePrimary
                             ELSE @ContactTypeOther
                        END -- ContactTypeId - uniqueidentifier
                       ,sc.PrefixId  -- PrefixId - uniqueidentifier
                       ,sc.suffixId  -- SufixId - uniqueidentifier
                       ,sc.FirstName  -- FirstName - varchar(50)
                       ,sc.LastName  -- LastName - varchar(50)
                       ,sc.MI  -- MiddleName - varchar(50)
                       ,sc.Comments  -- Comments - varchar(500)
                       ,CASE WHEN sc.ModDate IS NULL THEN GETDATE()
                             ELSE sc.ModDate
                        END  -- ModDate - datetime
                       ,CASE WHEN sc.ModUser IS NULL THEN 'support'
                             ELSE sc.ModUser
                        END  -- ModUser - varchar(50)
                FROM    dbo.syStudentContacts sc
                WHERE   sc.StudentId = @studentId;
        IF @@ERROR <> 0
            SET NOEXEC ON;

-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Contact Phone from Academics to Admission
-- syStudentContactPhone -- > adLeadOtherContactsPhone
-- ************************************************************************

        INSERT  INTO dbo.adLeadOtherContactsPhone
                (
                 OtherContactsPhoneId
                ,OtherContactId
                ,LeadId
                ,PhoneTypeId
                ,Phone
                ,ModDate
                ,ModUser
                ,Extension
                ,IsForeignPhone
                ,StatusId
		        )
                SELECT  scp.StudentContactPhoneId  -- OtherContactsPhoneId - uniqueidentifier
                       ,sc.StudentContactId  -- OtherContactId - uniqueidentifier
                       ,@leadId  -- LeadId - uniqueidentifier
                       ,ISNULL(scp.PhoneTypeId,@PhoneTypeUnknown)    -- PhoneTypeId - uniqueidentifier
                       ,scp.Phone  -- Phone - varchar(15)
                       ,CASE WHEN scp.ModDate IS NULL THEN GETDATE()
                             ELSE scp.ModDate
                        END  -- ModDate - datetime
                       ,CASE WHEN scp.ModUser IS NULL THEN 'SUPPORT'
                             ELSE scp.ModUser
                        END  -- ModUser - varchar(50)
                       ,CASE WHEN scp.Ext IS NULL THEN ''
                             ELSE scp.Ext
                        END  -- Extension - varchar(10)
                       ,CASE WHEN scp.ForeignPhone IS NULL THEN 0
                             ELSE scp.ForeignPhone
                        END  -- IsForeignPhone - bit
                       ,scp.StatusId  -- StatusId - uniqueidentifier
                FROM    dbo.syStudentContactPhones scp
                JOIN    dbo.syStudentContacts sc ON sc.StudentContactId = scp.StudentContactId
                WHERE   sc.StudentId = @studentId
                        AND ISNULL(scp.Phone,'') <> '';
        IF @@ERROR <> 0
            SET NOEXEC ON;
-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Contact Address from Academics to Admission
-- syStudentContactAddresses -- > adLeadOtherContactsAddreses
-- ************************************************************************
        INSERT  INTO dbo.adLeadOtherContactsAddreses
                (
                 OtherContactsAddresesId
                ,OtherContactId
                ,LeadId
                ,AddressTypeId
                ,Address1
                ,Address2
                ,City
                ,StateId
                ,ZipCode
                ,CountryId
                ,StatusId
                ,IsMailingAddress
                ,ModDate
                ,ModUser
                ,State
                ,IsInternational
                ,CountyId
                ,County
                ,Country
		        )
                SELECT  sca.StudentContactAddressId -- OtherContactsAddresesId - uniqueidentifier
                       ,sc.StudentContactId  -- OtherContactId - uniqueidentifier
                       ,@leadId  -- LeadId - uniqueidentifier
                       ,CASE WHEN sca.AddrTypId IS NULL THEN @AddressTypeOther
                             ELSE sca.AddrTypId
                        END  -- AddressTypeId - uniqueidentifier
                       ,CASE WHEN sca.Address1 IS NULL THEN ''
                             ELSE sca.Address1
                        END   -- Address1 - varchar(250)
                       ,sca.Address2  -- Address2 - varchar(250)
                       ,sca.City  -- City - varchar(250)
                       ,sca.StateId  -- StateId - uniqueidentifier
                       ,SUBSTRING(LTRIM(RTRIM(sca.Zip)),1,10)  -- ZipCode - varchar(10)
                       ,sca.CountryId  -- CountryId - uniqueidentifier
                       ,sca.StatusId  -- StatusId - uniqueidentifier
                       ,0  -- IsMailingAddress - bit
                       ,CASE WHEN sca.ModDate IS NULL THEN GETDATE()
                             ELSE sca.ModDate
                        END  -- ModDate - datetime
                       ,CASE WHEN sca.ModUser IS NULL THEN 'SUPPORT'
                             ELSE sca.ModUser
                        END  -- ModUser - varchar(50)
                       ,CASE WHEN sca.ForeignZip IS NOT NULL THEN sca.OtherState
                             ELSE ''
                        END    -- State - varchar(100)
                       ,CASE WHEN sca.ForeignZip IS NOT NULL
                                  OR sca.OtherState IS NOT NULL THEN 1
                             ELSE 0
                        END  -- IsInternational - bit
                       ,NULL  -- CountyId - uniqueidentifier
                       ,NULL  -- County - varchar(100)
                       ,sca.CountryId  -- Country - varchar(100)
                FROM    dbo.syStudentContactAddresses sca
                JOIN    dbo.syStudentContacts sc ON sc.StudentContactId = sca.StudentContactId
                WHERE   sc.StudentId = @studentId
                        AND Address1 IS NOT NULL;			
        IF @@ERROR <> 0
            SET NOEXEC ON;

-- ************************************************************************
-- INSIDE CURSOR:  Migrate Student Contact Phone from Academics to Admission
-- syStudentContactPhone -- > adLeadOtherContactsPhone
-- ************************************************************************

        INSERT  INTO dbo.adLeadOtherContactsEmail
                (
                 OtherContactsEmailId
                ,OtherContactId
                ,LeadId
                ,EMail
                ,EMailTypeId
                ,ModUser
                ,ModDate
                ,StatusId
		        )
                SELECT  NEWID()  -- OtherContactsEmailId - uniqueidentifier
                       ,StudentContactId  -- OtherContactId - uniqueidentifier
                       ,@leadId  -- LeadId - uniqueidentifier
                       ,EMail  -- EMail - varchar(100)
                       ,@HomeEmailTypeId  -- EMailTypeId - uniqueidentifier
                       ,CASE WHEN ModUser IS NULL THEN 'SUPPORT'
                             ELSE ModUser
                        END  -- ModUser - varchar(50)
                       ,CASE WHEN ModDate IS NULL THEN GETDATE()
                             ELSE ModDate
                        END  -- ModDate - datetime
                       ,StatusId  -- StatusId - uniqueidentifier
                FROM    dbo.syStudentContacts
                WHERE   StudentId = @studentId
                        AND ISNULL(EMail,'') <> ''; 
        IF @@ERROR <> 0
            SET NOEXEC ON;

-- -------------------------------------------------------------------------

        FETCH NEXT FROM lead_cursor INTO @leadId,@studentId;		
    END;
CLOSE lead_cursor;
DEALLOCATE lead_cursor;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- ************************************************************************
-- END CURSOR OVER adLeads LeadId & StudentID:
-- MIGRATED:
-- 1 Emails
-- 2 Phones
-- 3 Comment Field from Student Info Page
-- 4 Migrate Student Skills
-- 5 Migrate Prior Work
-- 6 Migrate Contact
-- 7 Migrate Student Contact Phone (other contact phone in Admission)
-- 8 Migrate Student Contact Address (other contact phone in Admission)
-- 9 Migrate Student Contact Email (other contact phone in Admission)
-- ************************************************************************

DECLARE @phoneNumber INTEGER = (
                                 SELECT COUNT(*)
                                 FROM   adLeadPhone
                               );
--PRINT CONCAT('Records IN Phone TABLE ',@phoneNumber);	
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO	
-- Pass all contact information
PRINT 'TODO Migrate Contacts';
-- Pass all Messages

-- UPDATE ALL enrollment of the student with the new LeadId
PRINT 'Update all enrollment records with LeadId.................';
GO
UPDATE  arStuEnrollments
SET     LeadId = l.LeadId
FROM    dbo.adLeads l
WHERE   arStuEnrollments.StudentId = l.StudentId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT 'Add The Foreign key FK_arStuEnrollments_adLeads_LeadId_LeadId';
-- ADD the relation FK_arStuEnrollments_adLeads_LeadId_LeadId
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS
                WHERE   CONSTRAINT_NAME = 'FK_arStuEnrollments_adLeads_LeadId_LeadId' )
    BEGIN
        ALTER TABLE dbo.arStuEnrollments ADD CONSTRAINT FK_arStuEnrollments_adLeads_LeadId_LeadId
        FOREIGN KEY (LeadId)
        REFERENCES adLeads (LeadId);
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- *************************************************************************************************
-- END TRANSACTION
PRINT 'End Transaction';
GO
COMMIT TRANSACTION MigrateStudents;
GO
-- *************************************************************************************************
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT; 
SET @Success = 1; 
SET NOEXEC OFF; 
IF ( @Success = 1 )
    BEGIN
        PRINT '';
        PRINT 'The database update SUCCEEDED'; 
        PRINT '';
    END;
ELSE
    BEGIN 
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION MigrateStudents;
        PRINT '';
        PRINT 'The database update FAILED ';
        PRINT '';
    END;
GO
-- *************************************************************************************************

IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..studentToDelete') )
    BEGIN
        PRINT 'Dropping  temporal tables studentToDelete....';
        DROP TABLE tempdb..studentToDelete;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..leadInEnrollment') )
    BEGIN
        PRINT 'Dropping  temporal tables leadInEnrollment....';
        DROP TABLE tempdb..leadInEnrollment;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..UpdateLeadTable') )
    BEGIN
        PRINT 'Dropping  temporal tables UpdateLeadTable....';
        DROP TABLE tempdb..UpdateLeadTable;
    END;
GO
IF EXISTS ( SELECT  1
            FROM    tempdb.dbo.sysobjects AS S
            WHERE   S.xtype IN ( 'U' )
                    AND S.id = OBJECT_ID(N'tempdb..OrfanStudents') )
    BEGIN
        PRINT 'Dropping  temporal tables OrfanStudents....';
        DROP TABLE tempdb..OrfanStudents;
    END;
GO

