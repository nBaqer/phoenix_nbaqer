-- ******************************************************************************
-- Consolidated Script Version 3.8
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- ******************************************************************************

-- Preload Data Information..............................................
-- Clean Table
SET IDENTITY_INSERT dbo.adVendorPayFor ON; 
GO

IF (
     SELECT COUNT(*)
     FROM   dbo.adVendorPayFor
   ) < 1
    BEGIN
-- Enter new Values

        INSERT  dbo.adVendorPayFor
                (
                 IdPayFor
                ,PayForCode
                ,Description
                )
        VALUES  (
                 1
                ,N'LEADENROLL'
                ,N'Per Lead Enrolled'
                );

        INSERT  dbo.adVendorPayFor
                (
                 IdPayFor
                ,PayForCode
                ,Description
                )
        VALUES  (
                 2
                ,N'LEADALL'
                ,N'Per received Lead'
                );

        INSERT  dbo.adVendorPayFor
                ( IdPayFor,PayForCode,Description )
        VALUES  ( 3,N'PAYPERMONTH',N'Per Month' );

        INSERT  dbo.adVendorPayFor
                (
                 IdPayFor
                ,PayForCode
                ,Description
                )
        VALUES  (
                 4
                ,N'PAYPERCAMPAIGN'
                ,N'Per Campaign'
                );
    END;
GO
SET IDENTITY_INSERT dbo.adVendorPayFor OFF;
GO
SET IDENTITY_INSERT dbo.adVendors ON; 

GO
-- Input data from AdVendors this delete all previous vendor!!!!! <----------- Eliminate in production
IF (
     SELECT COUNT(*)
     FROM   adVendors
   ) < 1
    BEGIN
        INSERT  dbo.adVendors
                (
                 VendorId
                ,VendorName
                ,VendorCode
                ,Description
                ,DateOperationBegin
                ,DateOperationEnd
                ,IsActive
                ,IsDeleted
                )
        VALUES  (
                 1
                ,N'MDT Marketing'
                ,N'MDT001XDTY'
                ,N'MDT Marketing 805 E. Broward Blvd., Suite 301; Fort Lauderdale, FL 33301, P (954) 764-2630 '
                ,CAST(0x0780EAC5323875390B AS DATETIME2)
                ,CAST(0x0780B2780D1FFC390B AS DATETIME2)
                ,1
                ,0
                );
    END;
SET IDENTITY_INSERT dbo.adVendors OFF;
GO

IF NOT EXISTS ( SELECT  *
                FROM    dbo.adVendorCampaign
                WHERE   CampaignCode = 'TESTCAMP1' )
    BEGIN

        INSERT  dbo.adVendorCampaign
                (
                 VendorId
                ,CampaignCode
                ,AccountId
                ,IsActive
                ,DateCampaignBegin
                ,DateCampaignEnd
                ,Cost
                ,PayForId
                ,FilterRejectByCounty
                ,FilterRejectDuplicates
                ,IsDeleted
                )
        VALUES  (
                 1  -- VendorId - int
                ,'TESTCAMP1'  -- CampaignCode - varchar(50)
                ,'XRT'  -- AccountId - varchar(50)
                ,1  -- IsActive - bit
                ,GETDATE()  -- DateCampaignBegin - datetime2
                ,GETDATE()  -- DateCampaignEnd - datetime2
                ,2000.0  -- Cost - float
                ,1  -- PayForId - int
                ,0  -- FilterRejectByCounty - bit
                ,0  -- FilterRejectDuplicates - bit
                ,0  -- IsDeleted - bit
                );
    END;

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.adVendorCampaign
                WHERE   CampaignCode = 'TESTCAMP2' )
    BEGIN

        INSERT  dbo.adVendorCampaign
                (
                 VendorId
                ,CampaignCode
                ,AccountId
                ,IsActive
                ,DateCampaignBegin
                ,DateCampaignEnd
                ,Cost
                ,PayForId
                ,FilterRejectByCounty
                ,FilterRejectDuplicates
                ,IsDeleted
                )
        VALUES  (
                 1  -- VendorId - int
                ,'TESTCAMP2'  -- CampaignCode - varchar(50)
                ,'XXRT'  -- AccountId - varchar(50)
                ,1  -- IsActive - bit
                ,GETDATE()  -- DateCampaignBegin - datetime2
                ,GETDATE()  -- DateCampaignEnd - datetime2
                ,2000.0  -- Cost - float
                ,1  -- PayForId - int
                ,0  -- FilterRejectByCounty - bit
                ,0  -- FilterRejectDuplicates - bit
                ,0  -- IsDeleted - bit
                );


    END;

GO
--------------------------------------------------------------------------------------------------
-- JG - 04/16/2015 - Adding lead assignment page into menu
--------------------------------------------------------------------------------------------------
DECLARE @leadAssignREsourceId INT;
IF NOT EXISTS ( SELECT  ResourceID
                FROM    dbo.syResources
                WHERE   ResourceID = 823 )
    BEGIN

        INSERT  INTO dbo.syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 823
                ,'LeadAssignment'
                ,5
                ,'~/ad/leadassignment.aspx'
                ,NULL
                ,NULL
                ,GETDATE()
                ,'support'
                ,NULL
                ,0
                ,0
                ,0
                ,'Lead Assignment'
                );
    END;

DECLARE @leadassignparent INT; 
SET @leadassignparent = (
                          SELECT TOP 1
                                    m.MenuItemId
                          FROM      syMenuItems m
                          INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
                          INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
                          WHERE     m.MenuName = 'general options'
                                    AND p.MenuName = 'Common Tasks'
                                    AND pp.MenuName = 'admissions'
                        );

IF @leadassignparent IS NOT NULL
    BEGIN
        IF NOT EXISTS ( SELECT TOP 1
                                MenuName
                        FROM    syMenuItems
                        WHERE   MenuName = 'Lead Assignment'
                                AND Url = '/AD/leadassignment.aspx'
                                AND ParentId = @leadassignparent )
            BEGIN
                INSERT  INTO dbo.syMenuItems
                        (
                         MenuName
                        ,DisplayName
                        ,Url
                        ,MenuItemTypeId
                        ,ParentId
                        ,DisplayOrder
                        ,IsPopup
                        ,ModDate
                        ,ModUser
                        ,IsActive
                        ,ResourceId
                        ,HierarchyId
                        ,ModuleCode
                        ,MRUType
                        ,HideStatusBar
                        )
                VALUES  (
                         'Lead Assignment'
                        ,'Lead Assignment'
                        ,N'/AD/leadassignment.aspx'
                        ,4
                        ,@leadassignparent
                        ,3000
                        ,0
                        ,GETDATE()
                        ,'support'
                        ,1
                        ,823
                        ,NULL
                        ,NULL
                        ,NULL
                        ,NULL
                
                        );
            END;
    END; 



GO
-- ***************************************************************************
-- NEW Status Lead: Creation of Lead status Imported(25) and Duplicated(26) 
-- JAGG
-- ***************************************************************************
DECLARE @Active UNIQUEIDENTIFIER = (
                                     SELECT StatusId
                                     FROM   syStatuses
                                     WHERE  StatusCode = 'A'
                                   );

IF NOT EXISTS ( SELECT  *
                FROM    sySysStatus
                WHERE   SysStatusId = 25 )
    BEGIN
        INSERT  INTO dbo.sySysStatus
                (
                 SysStatusId
                ,SysStatusDescrip
                ,StatusId
                ,StatusLevelId
                ,ModUser
                ,ModDate
                ,InSchool
                ,GEProgramStatus
                )
        VALUES  (
                 25
                ,'Imported'
                ,@Active
                ,1
                ,'jguirado'
                ,GETDATE()
                ,0
                ,NULL
                );
    END;
        
IF NOT EXISTS ( SELECT  *
                FROM    sySysStatus
                WHERE   SysStatusId = 26 )
    BEGIN
        INSERT  INTO dbo.sySysStatus
                (
                 SysStatusId
                ,SysStatusDescrip
                ,StatusId
                ,StatusLevelId
                ,ModUser
                ,ModDate
                ,InSchool
                ,GEProgramStatus
                )
        VALUES  (
                 26
                ,'Duplicated'
                ,@Active
                ,1
                ,'jguirado'
                ,GETDATE()
                ,0
                ,NULL
                );
    END;
GO
-- ***************************************************************************
-- END NEW Status Lead
-- ***************************************************************************
-- ***************************************************************************
-- NEW Status Lead: Creation of Lead status Imported(25) and Duplicated(26) 
-- but for the school. This status are created in deploy moment. 
-- School has not possibility to enter other status assigned to 25 or 26
-- JAGG
-- **************************************************************************

--SELECT * FROM syStatusCodes

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syStatusCodes
                WHERE   SysStatusId = 25 )
    BEGIN
-- Create the two states
        INSERT  dbo.syStatusCodes
                (
                 --StatusCodeId
                 StatusCode
                ,StatusCodeDescrip
                ,StatusId
                ,CampGrpId
                ,SysStatusId
                ,ModDate
                ,ModUser
                ,AcadProbation
                ,DiscProbation
                ,IsDefaultLeadStatus
                )
        VALUES  (
                 --NULL  -- StatusCodeId - uniqueidentifier
                 'IMPORTED'  -- StatusCode - varchar(20)
                ,'Lead Imported from Vendor'  -- StatusCodeDescrip - varchar(80)
                ,(
                   SELECT   StatusId
                   FROM     syStatuses
                   WHERE    StatusCode = 'A'
                 )  -- StatusId - uniqueidentifier
                ,(
                   SELECT   CampGrpId
                   FROM     dbo.syCampGrps
                   WHERE    CampGrpCode = 'All'
                 )  -- CampGrpId - uniqueidentifier
                ,25  -- SysStatusId - int
                ,GETDATE()  -- ModDate - datetime
                ,'JGuirado'  -- ModUser - varchar(50)
                ,0  -- AcadProbation - bit
                ,0  -- DiscProbation - bit
                ,0  -- IsDefaultLeadStatus - bit
                );
    END;
GO

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syStatusCodes
                WHERE   SysStatusId = 26 )
    BEGIN
        INSERT  dbo.syStatusCodes
                (
                 --StatusCodeId
                 StatusCode
                ,StatusCodeDescrip
                ,StatusId
                ,CampGrpId
                ,SysStatusId
                ,ModDate
                ,ModUser
                ,AcadProbation
                ,DiscProbation
                ,IsDefaultLeadStatus
                )
        VALUES  (
                 --NULL  -- StatusCodeId - uniqueidentifier
                 'DUPLICATED'  -- StatusCode - varchar(20)
                ,'Duplicated lead'  -- StatusCodeDescrip - varchar(80)
                ,(
                   SELECT   StatusId
                   FROM     syStatuses
                   WHERE    StatusCode = 'A'
                 )  -- StatusId - uniqueidentifier
                ,(
                   SELECT   CampGrpId
                   FROM     dbo.syCampGrps
                   WHERE    CampGrpCode = 'All'
                 )  -- CampGrpId - uniqueidentifier
                ,26  -- SysStatusId - int
                ,GETDATE()  -- ModDate - datetime
                ,'JGuirado'  -- ModUser - varchar(50)
                ,0  -- AcadProbation - bit
                ,0  -- DiscProbation - bit
                ,0  -- IsDefaultLeadStatus - bit
                );
    END;
GO


  
---------------------------------------------------------------------------------------------------------------
-- JG - 06-01/2015 - Add new requirement types
---------------------------------------------------------------------------------------------------------------

IF NOT EXISTS ( SELECT  *
                FROM    dbo.adReqTypes
                WHERE   Descrip = 'Interview' )
    BEGIN
    
        DECLARE @newId INT;
        SET @newId = (
                       SELECT   MAX(adReqTypeId) + 1
                       FROM     dbo.adReqTypes
                     );
        IF @newId IS NOT NULL
            BEGIN
                INSERT  INTO dbo.adReqTypes
                        ( adReqTypeId,Descrip )
                VALUES  ( @newId,'Interview' );

            END;
    END;

GO

IF NOT EXISTS ( SELECT  *
                FROM    dbo.adReqTypes
                WHERE   Descrip = 'Event' )
    BEGIN
    
        DECLARE @newId INT;
        SET @newId = (
                       SELECT   MAX(adReqTypeId) + 1
                       FROM     dbo.adReqTypes
                     );
        IF @newId IS NOT NULL
            BEGIN
                INSERT  INTO dbo.adReqTypes
                        ( adReqTypeId,Descrip )
                VALUES  ( @newId,'Event' );

            END;
    END;

GO


IF NOT EXISTS ( SELECT  *
                FROM    dbo.adReqTypes
                WHERE   Descrip = 'Tour' )
    BEGIN
    
        DECLARE @newId INT;
        SET @newId = (
                       SELECT   MAX(adReqTypeId) + 1
                       FROM     dbo.adReqTypes
                     );
        IF @newId IS NOT NULL
            BEGIN
                INSERT  INTO dbo.adReqTypes
                        ( adReqTypeId,Descrip )
                VALUES  ( @newId,'Tour' );

            END;
    END;

GO

IF NOT EXISTS ( SELECT  *
                FROM    dbo.adReqTypes
                WHERE   Descrip = 'Fee' )
    BEGIN
    
        DECLARE @newId INT;
        SET @newId = (
                       SELECT   MAX(adReqTypeId) + 1
                       FROM     dbo.adReqTypes
                     );
        IF @newId IS NOT NULL
            BEGIN
                INSERT  INTO dbo.adReqTypes
                        ( adReqTypeId,Descrip )
                VALUES  ( @newId,'Fee' );

            END;
    END;

GO
---------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------------
-- JG - 06/25/2015 - insert role permissions for lead assignment for director of admissions
---------------------------------------------------------------------------------------------------------------

DECLARE @drAdmId UNIQUEIDENTIFIER;
SET @drAdmId = (
                 SELECT TOP 1
                        RoleId
                 FROM   syRoles
                 WHERE  SysRoleId = 8
               );

IF @drAdmId IS NOT NULL
    BEGIN
        IF NOT EXISTS ( SELECT  RRLId
                        FROM    dbo.syRlsResLvls
                        WHERE   RoleId = @drAdmId
                                AND ResourceID = 823 )
            BEGIN
                INSERT  INTO dbo.syRlsResLvls
                        (
                         RRLId
                        ,RoleId
                        ,ResourceID
                        ,AccessLevel
                        ,ModDate
                        ,ModUser
                        ,ParentId
                        )
                VALUES  (
                         NEWID()
                        ,@drAdmId
                        ,823
                        ,15
                        ,GETDATE()
                        ,'support'
                        ,NULL
                        );
            END;
    END;
GO
----------------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------------
-- JG - Move overide table records to adleadentrancetest table
----------------------------------------------------------------------------------------------------------
DECLARE @LeadId UNIQUEIDENTIFIER;
DECLARE @EntrTestId UNIQUEIDENTIFIER;


DECLARE EntranceTest CURSOR
FOR
    SELECT  LeadId
           ,EntrTestId
    FROM    dbo.adEntrTestOverRide;

OPEN EntranceTest;   
FETCH NEXT FROM EntranceTest INTO @LeadId,@EntrTestId;

WHILE @@FETCH_STATUS = 0
    BEGIN   
       
        IF NOT EXISTS ( SELECT  *
                        FROM    dbo.adLeadEntranceTest
                        WHERE   LeadId = @LeadId
                                AND EntrTestId = @EntrTestId )
            BEGIN
                INSERT  INTO dbo.adLeadEntranceTest
                        (
                         LeadEntrTestId
                        ,LeadId
                        ,EntrTestId
                        ,ModUser
                        ,ModDate
                        ,OverRide
                        ,StudentId
                        )
                        SELECT TOP 1
                                NEWID()
                               ,LeadId
                               ,EntrTestId
                               ,ModUser
                               ,ModDate
                               ,OverRide
                               ,StudentId
                        FROM    dbo.adEntrTestOverRide
                        WHERE   LeadId = @LeadId
                                AND EntrTestId = @EntrTestId;
            
            END;

        FETCH NEXT FROM EntranceTest INTO @LeadId,@EntrTestId;   
    END;   

CLOSE EntranceTest;   
DEALLOCATE EntranceTest;
GO

-- ********************************************************************************************************
-- Change Children Caption for Dependents Info Lead Page
-- JGUIRADO 8/12/2015
-- ********************************************************************************************************
 
UPDATE  dbo.syFldCaptions
SET     Caption = 'Dependents'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'Children'
                );
  GO
-- ********************************************************************************************************
-- Change CampusId Caption from Campus to Lead Assign to Info Lead Page
-- JGUIRADO 8/12/2015
-- ********************************************************************************************************

UPDATE  dbo.syFldCaptions
SET     Caption = 'Lead Assigned to'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'CampusId'
                );
  GO
  -- ********************************************************************************************************
-- Change DateAssigned Caption from DateAssigned to Adm Rep Assigned to Info Lead Page
-- JGUIRADO 8/12/2015
-- ********************************************************************************************************
UPDATE  dbo.syFldCaptions
SET     Caption = 'Adm Rep Assigned'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'AssignedDate'
                );
GO
-- ********************************************************************************************************
-- Change AreaID Caption from Area to Interest Area to Info Lead Page
-- JGUIRADO 8/12/2015
-- ********************************************************************************************************

UPDATE  dbo.syFldCaptions
SET     Caption = 'Interest Area'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'AreaId'
                );
  GO

-- ********************************************************************************************************
-- Change ProgramID Caption from Interested Program Area to Program
-- JGUIRADO 8/12/2015
-- ********************************************************************************************************

UPDATE  dbo.syFldCaptions
SET     Caption = 'Program'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'ProgramID'
                );
  GO

-- ********************************************************************************************************
-- Create or Update the relation between AttendTypeId with the table AdLeads and 
-- With the PAge Resource 170 (Lead Info Page)
-- JGUIRADO 8/13/2015
-- ********************************************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'AttendTypeId',@FldTypeId = 72,@FldLen = 36,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Attend',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Lead Main Table',@CategoryId = NULL,@FKColDescrip = NULL,
    @PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- ********************************************************************************************************
-- Create or Update the relation between PreferredContactId with the table AdLeads and 
-- With the Page Resource 170 (Lead Info Page)
-- Also update or create the caption for the preferredContactId page.
-- JGUIRADO 8/21/2015
-- ********************************************************************************************************

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'PreferredContactId',@FldTypeId = 72,@FldLen = 36,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Preferred Contact',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Lead Main Table',
    @CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- End Script
-- ***********************************************************************************
-- Create adLead Phone Table (adLeadPhone) resource 
-- and isForeignPhone and Extension resources for LeadInfoPage
-- Authors: JAGG
-- Dependencies: Stored Procedure DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
 
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'Extension',@FldTypeId = 200,@FldLen = 10,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Extension',@LanguageId = 1,@TblName = N'adLeadPhone',@TblDescription = N'Hold the phones for  each lead',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'IsForeignPhone',@FldTypeId = 11,@FldLen = 1,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Int''l',@LanguageId = 1,@TblName = N'adLeadPhone',@TblDescription = N'Hold the phones for  each lead',
    @CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO



-- ********************************************************************************************************
-- Change Phone and Phone 2 Caption from Primary Phone
-- JGUIRADO 8/28/2015
-- ********************************************************************************************************

UPDATE  dbo.syFldCaptions
SET     Caption = 'Phone - Best'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'Phone'
                );
  GO

UPDATE  dbo.syFldCaptions
SET     Caption = 'Phone'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'Phone2'
                );
  GO

-- ***********************************************************************************
-- Script to fill for fist time the AdEmailType catalog
-- Authors: JAGG
-- Dependencies: none
-- Minimal Version 3.8
-- ***********************************************************************************
-- ********************************************************************************************************************
-- syEmailType
--*********************************************************************************************************************
DECLARE @EMailTypeCode AS VARCHAR(12);
DECLARE @EmailTypeDesc AS VARCHAR(50);

SET @EMailTypeCode = 'Home';
SET @EmailTypeDesc = 'Personal';

IF NOT EXISTS ( SELECT  1
                FROM    syEmailType AS SETY
                WHERE   SETY.EMailTypeCode = @EMailTypeCode --  EMailTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syEmailType
                (
                 EMailTypeId
                ,EMailTypeCode
                ,EMailTypeDescription
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@EMailTypeCode
                ,@EMailTypeCode
                ,'SUPPORT'
                ,GETDATE()
                );
    END;
GO	
-- ********************************************************************************************************************	
DECLARE @EMailTypeCode AS VARCHAR(12);
DECLARE @EmailTypeDesc AS VARCHAR(50);
SET @EMailTypeCode = 'Work';
SET @EmailTypeDesc = 'Work';

IF NOT EXISTS ( SELECT  1
                FROM    syEmailType AS SETY
                WHERE   SETY.EMailTypeCode = @EMailTypeCode --  EMailTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syEmailType
                (
                 EMailTypeId
                ,EMailTypeCode
                ,EMailTypeDescription
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@EMailTypeCode
                ,@EmailTypeDesc
                ,'SUPPORT'
                ,GETDATE()
                );
    END;
GO	
-- ********************************************************************************************************************
  -- Also Added to be sure with Migration codes
-- ********************************************************************************************************************
-- syPhoneType
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Home';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Other';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Other'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Work';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Work'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Emergency';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Emergency'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Unknown';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Unknown'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************



-- ***********************************************************************************
-- Script to create resources for the tables syEmailType and AdLeadEmail.
-- Also the resource EmailTypeId was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: None
-- Minimal Version 3.8
-- ***********************************************************************************

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'EmailTypeId',@FldTypeId = 72,@FldLen = 50,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'E-mail type',@LanguageId = 1,@TblName = N'syEmailType',@TblDescription = N'Hold the types of email defined',
    @CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;

IF NOT EXISTS ( SELECT  TblName
                FROM    syTables
                WHERE   TblName = 'AdLeadEmail' )
    BEGIN 
        DECLARE @TableId INT = (
                                 SELECT MAX(TblId) + 1
                                 FROM   syTables
                               );

        INSERT  INTO dbo.syTables
                (
                 TblId
                ,TblName
                ,TblDescrip
                ,TblPK
                )
        VALUES  (
                 @TableId  -- TblId - int
                ,'AdLeadEmail'  -- TblName - varchar(50)
                ,'Hold entities email'  -- TblDescrip - varchar(100)
                ,991  -- TblPK - int
                );
    END;
GO
  -- ***********************************************************************************
-- Script update resources HomeEmail and WorkEmail
-- Authors: JAGG
-- Dependencies: none
-- Minimal Version 3.8
-- ***********************************************************************************

UPDATE  dbo.syFldCaptions
SET     Caption = 'Email - Best'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'HomeEmail'
                );
GO

UPDATE  dbo.syFldCaptions
SET     Caption = 'Email'
WHERE   FldId = (
                  SELECT    FldId
                  FROM      syFields
                  WHERE     FldName = 'WorkEmail'
                );
GO

-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field BestTimeId.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'BestTimeId',@FldTypeId = 135,@FldLen = 1,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Best Time',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field NoneEmail.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'NoneEmail',@FldTypeId = 11,@FldLen = 1,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'None',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,@FKColDescrip = NULL,
    @PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO
-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field HighSchool.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'HighSchool',@FldTypeId = 72,@FldLen = 36,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'High School',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO
-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field AttendingHs.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'AttendingHs',@FldTypeId = 11,@FldLen = 1,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Attending HS',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO
-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field NickName.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'NickName',@FldTypeId = 200,@FldLen = 50,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Nickname',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,@FKColDescrip = NULL,
    @PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field TransportationId.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'TransportationId',@FldTypeId = 200,@FldLen = 50,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Transportation',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',
    @CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- ***********************************************************************************
-- Script to delete resources for the table AdLeads new field MilesFromSchool.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: None. This field was substituted by TimeToSchool
-- Minimal Version 3.8
-- ***********************************************************************************
DECLARE @FldName VARCHAR(50) = 'MilesFromSchool';
DECLARE @FldId INT = (
                       SELECT   FldId
                       FROM     syFields
                       WHERE    FldName = @FldName
                     );
DECLARE @TblFldsId INT = (
                           SELECT   TblFldsId
                           FROM     dbo.syTblFlds
                           WHERE    FldId IN ( SELECT   FldId
                                               FROM     syFields
                                               WHERE    FldName = @FldName )
                         );
        
DELETE  dbo.syResTblFlds
WHERE   TblFldsId = @TblFldsId;
DELETE  syFldCaptions
WHERE   FldId = @FldId;
DELETE  syTblFlds
WHERE   FldId = @FldId;
DELETE  syFields
WHERE   FldId IN ( @FldId );

GO


-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field TimeToSchool.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'TimeToSchool',@FldTypeId = 135,@FldLen = 6,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
    @Mask = NULL,@FieldCaption = N'Time to School',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field ProgramScheduleId.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'ProgramScheduleId',@FldTypeId = 72,@FldLen = 36,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Schedule',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
GO

EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'DependencyTypeId',@FldTypeId = 72,@FldLen = 16,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Dependency',@LanguageId = 1,@TblName = N'AdLeads',@TblDescription = N'Main Leads Table',@CategoryId = NULL,
    @FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;

  
  -- ***********************************************************************************
-- Script to update resources for the page LeadMaster1.aspx.
-- This page was changed to ALeadInfoPage.aspx
-- Authors: JAGG
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
UPDATE  dbo.syResources
SET     ResourceURL = '~/AD/ALeadInfoPage.aspx'
WHERE   ResourceID = (
                       SELECT   ResourceID
                       FROM     dbo.syResources
                       WHERE    ResourceURL = '~/AD/LeadMaster1.aspx'
                     );
  GO
  -- ***********************************************************************************
-- Script to update the DateOfChange with the ModDate
-- The new field dateOfChange is updated with the value of ModDate
-- Authors: JAGG
-- Minimal Version 3.8
-- ***********************************************************************************

UPDATE  c1
SET     c1.DateOfChange = c2.ModDate
FROM    dbo.syLeadStatusesChanges c1
JOIN    syLeadStatusesChanges c2 ON c2.StatusChangeId = c1.StatusChangeId;
GO

  -- ***********************************************************************************
-- Script to update the syResource Table wit the new Lead Queue Page Resource 
-- Resource 825
-- Authors: JAGG
-- Minimal Version 3.8
-- ***********************************************************************************
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 825 )
    BEGIN
        INSERT  syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 825
                ,'Lead Queue'
                ,3
                ,'~/AD/aLeadQueue.aspx'
                ,NULL
                ,NULL
                ,GETDATE()
                ,'JAGG'
                ,0
                ,4
                ,975
                ,NULL
                ,'Lead Queue'
                );
    END;
GO


-- ********************************************************************************************************
-- JGUIRADO END
-- ********************************************************************************************************

-- ********************************************************************************************************
-- JGINZO ADD Lead REquirements to resources table
-- ********************************************************************************************************
IF NOT EXISTS ( SELECT  *
                FROM    dbo.syResources
                WHERE   ResourceID = 826 )
    BEGIN
        INSERT  INTO dbo.syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 826
                ,'Lead requirements'
                ,3
                ,'ad/leadrequirements.aspx'
                ,0
                ,0
                ,GETDATE()
                ,'support'
                ,0
                ,0
                ,0
                ,0
                ,'Lead Requirements' 
                );
    END;
        
-- SELECT * FROM dbo.syResources ORDER BY ResourceID DESC
GO

-- ********************************************************************************************************
-- JGINZO END
-- ********************************************************************************************************
  -- *********************************************************************************
-- Script Creation of setting RefreshQueueInterval forLead Queue page.
-- 
-- Authors: JAGG
-- Minimal Version 3.8
-- ***********************************************************************************
IF NOT EXISTS ( SELECT  val.Value
                FROM    dbo.syConfigAppSetValues val
                JOIN    dbo.syConfigAppSettings ON syConfigAppSettings.SettingId = val.SettingId
                WHERE   KeyName = 'RefreshQueueInterval' )
    BEGIN	
  
        BEGIN TRANSACTION InsertSetting;

        DECLARE @settingID INTEGER = (
                                       SELECT   MAX(SettingId) + 1
                                       FROM     dbo.syConfigAppSettings
                                     );
        INSERT  dbo.syConfigAppSettings
                (
                 SettingId
                ,KeyName
                ,Description
                ,ModUser
                ,ModDate
                ,CampusSpecific
                ,ExtraConfirmation
                )
        VALUES  (
                 @settingID  -- SettingId - int
                ,'RefreshQueueInterval'  -- KeyName - varchar(200)
                ,'Refresh time in second of the Lead Queue in Admission'  -- Description - varchar(1000)
                ,'JAGG'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - date time
                ,0  -- CampusSpecific - bit
                ,0  -- ExtraConfirmation - bit
                );

        INSERT  dbo.syConfigAppSetValues
                (
                 ValueId
                ,SettingId
                ,CampusId
                ,Value
                ,ModUser
                ,ModDate
                ,Active
                )
        VALUES  (
                 NEWID()  -- ValueId - uniqueidentifier
                ,@settingID  -- SettingId - int
                ,NULL  -- CampusId - uniqueidentifier
                ,'300'  -- Value - varchar(1000)
                ,'JAGG'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - date time
                ,1  -- Active - bit
                );
        COMMIT TRANSACTION InsertSetting;
    END;

GO

-- =============================================  
-- Author:	JAGG
-- 1098T Add Page Resource To Advantage
-- Modification  date: 11/18/2015  
-- Description:	added page 1098TService.aspx 
--              resource as 827 to SyResources table
-- Minimal version 3.7 SP3
-- =============================================


DECLARE @ResID INTEGER = 827;
 -- put here your resource ID for the page
DECLARE @PageResourceName VARCHAR(20) = '1098T Service';
 -- name of your page
DECLARE @ResourceTypeId INTEGER = 4;
 -- See table in wiki
DECLARE @PageUrl VARCHAR(100) = '~/SY/Service1098T.aspx';
 -- the url of page in advantage
DECLARE @MRUType INTEGER = NULL;
 -- NULL:None (page no referred to specific individual entity)
                             -- 1: Student 
                             -- 2:?, 3:?
                             -- 4: Admission
DECLARE @PageDisplayName VARCHAR(20) = '1098T Service';

-- Delete if exists to permit modifications
DELETE  syResources
WHERE   ResourceID = @ResID;

 -- Put the display name for your page
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = @ResID )
    BEGIN
        INSERT  syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 @ResID
                ,@PageResourceName
                ,@ResourceTypeId
                ,@PageUrl
                ,NULL
                ,NULL
                ,GETDATE()
                ,'JAGG'
                ,0
                ,@MRUType
                ,975
                ,NULL
                ,@PageDisplayName
                );
    END;
GO
-- =========================================================  
-- Author:	JAGG
-- 1098T Add Menu for Page at Maintenance -> Financial Aids
-- Modification  date: 11/20/2015  
-- =========================================================

DELETE  dbo.syMenuItems
WHERE   ResourceId = 828;

DELETE  dbo.syMenuItems
WHERE   ResourceId = 827;
 
-- put here your resource ID for the Menu
DECLARE @ResID INTEGER = 828;
-- name of your Menu
DECLARE @MenuName VARCHAR(20) = 'Service1098T';
 -- the url of page in advantage
DECLARE @UrlTarget VARCHAR(100) = '/SY/Service1098T.aspx';
  --Page (call a page in the menu item)
DECLARE @MenuTypeItem INTEGER = 4;
-- In the column menu the position of the menu item. 100 first...200 second
DECLARE @MenuDisplayOrder INTEGER = 200;
-- 1 pop up 0: no pop up
DECLARE @IsPopup BIT = 0;
DECLARE @MRUType INTEGER = NULL;
 -- NULL:None (page no referred to specific individual entity)
                             -- 1: Student 
                             -- 2:?, 3:?
                             -- 4: Admission
DECLARE @MenuDisplayName VARCHAR(20) = 'Service 1098T';
 -- Put the display name for your page
 
DECLARE @Parent INTEGER	= (
                            SELECT  MenuItemId
                            FROM    syMenuItems
                            WHERE   DisplayName = 'General Options'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- MenuItemId
                                                     FROM   dbo.syMenuItems
                                                     WHERE  DisplayName = 'Financial Aid'
                                                            AND MenuItemTypeId = 2
                                                   )
                          );
-- Create the su-bmenu in syMenuItem WAPI Services

INSERT  dbo.syMenuItems
        (
         MenuName
        ,DisplayName
        ,Url
        ,MenuItemTypeId
        ,ParentId
        ,DisplayOrder
        ,IsPopup
        ,ModDate
        ,ModUser
        ,IsActive
        ,ResourceId
        ,HierarchyId
        ,ModuleCode
        ,MRUType
        ,HideStatusBar
        )
VALUES  (
         @MenuName -- MenuName - varchar(250)
        ,@MenuDisplayName -- DisplayName - varchar(250)
        ,@UrlTarget -- Url - nvarchar(250)
        ,@MenuTypeItem   -- MenuItemTypeId - smallint
        ,@Parent -- ParentId - int
        ,@MenuDisplayOrder -- DisplayOrder - int
        ,@IsPopup   -- IsPopup - bit
        ,GETDATE() -- ModDate - datetime
        ,'jguirado'-- ModUser - varchar(50)
        ,1 -- IsActive - bit
        ,@ResID -- ResourceId - smallint
        ,NULL -- HierarchyId - uniqueidentifier
        ,NULL -- ModuleCode - varchar(5)
        ,NULL -- MRUType - int
        ,NULL  -- HideStatusBar - bit
        );
GO



-- ********************************************************************************************************
-- JGUIRADO END
-- ********************************************************************************************************



-- ********************************************************************************************************
-- JGINZO Build new advantage menu
-- ********************************************************************************************************

BEGIN TRY

    BEGIN TRANSACTION;
    PRINT 'Begin transaction Menu...................................................................';
 --   DECLARE @Menu TABLE ( MenuItemId INT );

 --   INSERT  INTO @Menu
 --           SELECT  m.MenuItemId
 --           FROM    syMenuItems m
 --           INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
 --           INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
 --           INNER JOIN syMenuItems ppp ON pp.ParentId = ppp.MenuItemId
 --           WHERE   p.MenuName = 'General Options'
 --                   AND pp.MenuName = 'Common Tasks'
 --                   AND ppp.MenuName = 'Admissions';

 --   INSERT  INTO @Menu
 --           SELECT  m.MenuItemId
 --           FROM    syMenuItems m
 --           INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
 --           INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
 --           INNER JOIN syMenuItems ppp ON pp.ParentId = ppp.MenuItemId
 --           WHERE   p.MenuName = 'Applicant Fee'
 --                   AND pp.MenuName = 'Common Tasks'
 --                   AND ppp.MenuName = 'Admissions';

 --   INSERT  INTO @Menu
 --           SELECT  m.MenuItemId
 --           FROM    syMenuItems m
 --           INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
 --           INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
 --           INNER JOIN syMenuItems ppp ON pp.ParentId = ppp.MenuItemId
 --           WHERE   p.MenuName = 'Leads'
 --                   AND pp.MenuName = 'Common Tasks'
 --                   AND ppp.MenuName = 'Admissions';

 --   INSERT  INTO @Menu
 --           SELECT  m.MenuItemId
 --           FROM    syMenuItems m
 --           INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
 --           INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
 --           WHERE   p.MenuName = 'Manage Leads'
 --                   AND pp.MenuName = 'Admissions';

 --   INSERT  INTO @Menu
 --           SELECT  m.MenuItemId
 --           FROM    syMenuItems m
 --           INNER JOIN syMenuItems p ON m.ParentId = p.MenuItemId
 --           INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
 --           WHERE   p.MenuName = 'Common Tasks'
 --                   AND pp.MenuName = 'Admissions';

 --   DECLARE @menuId INT;
 --   DECLARE DELETEMENU CURSOR
 --   FOR
 --       SELECT  MenuItemId
 --       FROM    @Menu;

 --   OPEN DELETEMENU;
 --   FETCH NEXT FROM DELETEMENU INTO @menuId;
	--PRINT 'Init Delete Menu ...........................................'
 --   WHILE @@FETCH_STATUS = 0
 --       BEGIN
 --           DELETE  FROM dbo.syMenuItems
 --           WHERE   MenuItemId = @menuId;
 --           FETCH NEXT FROM DELETEMENU INTO @menuId;
 --       END;
 --   CLOSE DELETEMENU;
 --   DEALLOCATE DELETEMENU;


--	PRINT 'End Delete Menu............................................'
        


        ----------------------------------------------------------
        -- MAKE SURE WE HAVE A SubMenuPage type
        ----------------------------------------------------------
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syMenuItemType
                    WHERE   MenuItemType = 'SubMenuPage' )
        BEGIN
            INSERT  INTO dbo.syMenuItemType
                    ( MenuItemTypeId,MenuItemType )
            VALUES  ( 6,'SubMenuPage' );
        END;

        ----------------------------------------------------------
        -- Get the parent menu for admissions
        ----------------------------------------------------------
    DECLARE @AdmissionsParentId INT;
    SET @AdmissionsParentId = (
                                SELECT TOP 1
                                        MenuItemId
                                FROM    dbo.syMenuItems
                                WHERE   ParentId IS NULL
                                        AND MenuName = 'Admissions'
                              );


        ----------------------------------------------------------
        -- If the admissions parent is found
        ----------------------------------------------------------
    IF @AdmissionsParentId IS NOT NULL
        BEGIN

                ----------------------------------------------------------
                -- Get the Manage leads menu
                ----------------------------------------------------------
            DECLARE @manageleads INT = (
                                         SELECT TOP 1
                                                MenuItemId
                                         FROM   dbo.syMenuItems
                                         WHERE  ParentId = @AdmissionsParentId
                                                AND MenuName = 'Manage Leads'
                                       );
                
                ----------------------------------------------------------
                -- Delete the children to manage leads and the manage leads
                -- menu item
                ----------------------------------------------------------
     --       PRINT 'First Delete.............................................................................';
			    
            IF @manageleads IS NOT NULL
                BEGIN
                    DECLARE @MenuTable TABLE
                        (
                         MenuItemId INT
                        ,deep INT
                        );

                    INSERT  INTO @MenuTable
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,1
                            FROM    syMenuItems
                            WHERE   MenuItemId = @manageleads;

                    INSERT  INTO @MenuTable
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,2
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable
                                                  WHERE     deep = 1 );

                    INSERT  INTO @MenuTable
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,3
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable
                                                  WHERE     deep = 2 );

                    INSERT  INTO @MenuTable
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,4
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable
                                                  WHERE     deep = 3 );

                    --SELECT  *
                    --FROM    @MenuTable;

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable
                                            WHERE   deep = 4 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable
                                            WHERE   deep = 3 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable
                                            WHERE   deep = 2 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable
                                            WHERE   deep = 1 );

                    --SELECT  *
                    --FROM    dbo.syMenuItems
                    --WHERE   ParentId IN ( SELECT    MenuItemId
                    --                      FROM      @MenuTable );
  



                    --DELETE  FROM dbo.syMenuItems
                    --WHERE   MenuItemId = @manageleads;

                    --DELETE  FROM dbo.syMenuItems
                    --WHERE   ParentId = @manageleads;
                      
                END;
		--		PRINT 'End First delete...................................................................'

    
                ----------------------------------------------------------
                -- Get the common tasks menu item				
                ----------------------------------------------------------
            DECLARE @CommonTasks INT = (
                                         SELECT TOP 1
                                                MenuItemId
                                         FROM   dbo.syMenuItems
                                         WHERE  ParentId = @AdmissionsParentId   --  8
                                                AND MenuName = 'Common Tasks'
                                       );


                ----------------------------------------------------------
                -- Delete the children to Common Tasks and the manage leads
                -- menu item
                ----------------------------------------------------------
            IF @CommonTasks IS NOT NULL
                BEGIN                      
                   
                    DECLARE @MenuTable1 TABLE
                        (
                         MenuItemId INT
                        ,deep INT
                        );

                    INSERT  INTO @MenuTable1
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,1
                            FROM    syMenuItems
                            WHERE   MenuItemId = @CommonTasks;

                    INSERT  INTO @MenuTable1
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,2
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable1
                                                  WHERE     deep = 1 );

                    INSERT  INTO @MenuTable1
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,3
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable1
                                                  WHERE     deep = 2 );

                    INSERT  INTO @MenuTable1
                            (
                             MenuItemId
                            ,deep
                            )
                            SELECT  MenuItemId
                                   ,4
                            FROM    syMenuItems
                            WHERE   ParentId IN ( SELECT    MenuItemId
                                                  FROM      @MenuTable1
                                                  WHERE     deep = 3 );

                    --SELECT  *
                    --FROM    @MenuTable1;

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable1
                                            WHERE   deep = 4 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable1
                                            WHERE   deep = 3 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable1
                                            WHERE   deep = 2 );

                    DELETE  dbo.syMenuItems
                    WHERE   MenuItemId IN ( SELECT  MenuItemId
                                            FROM    @MenuTable1
                                            WHERE   deep = 1 );

                    SELECT  *
                    FROM    dbo.syMenuItems
                    WHERE   ParentId IN ( SELECT    MenuItemId
                                          FROM      @MenuTable1 );



                    --DELETE  FROM dbo.syMenuItems
                    --WHERE   MenuItemId = @CommonTasks;

                    --DELETE  FROM dbo.syMenuItems
                    --WHERE   ParentId = @CommonTasks;

                   
                END;

                               
                
                ----------------------------------------------------------
                -- Insert the new menu items for leads
                ----------------------------------------------------------
            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 264 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Dashboard'
                            ,'Dashboard'
                            ,'/dash.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,100
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,264
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;

            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 170 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Leads'
                            ,'Leads'
                            ,'/AD/aLeadInfoPage.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,200
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,170
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;

            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 825 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Queue'
                            ,'Queue'
                            ,'/AD/aLeadQueue.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,200
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,825
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;

            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 576 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Lead Import'
                            ,'Lead Import'
                            ,'/AD/importleads_NEW.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,300
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,576
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;

            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 823 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Lead Assignment'
                            ,'Lead Assignment'
                            ,'/AD/leadassignment.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,400
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,823
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;

            IF NOT EXISTS ( SELECT  MenuItemId
                            FROM    dbo.syMenuItems
                            WHERE   ParentId = @AdmissionsParentId
                                    AND ResourceId = 689 )
                BEGIN
                    INSERT  INTO dbo.syMenuItems
                            (
                             MenuName
                            ,DisplayName
                            ,Url
                            ,MenuItemTypeId
                            ,ParentId
                            ,DisplayOrder
                            ,IsPopup
                            ,ModDate
                            ,ModUser
                            ,IsActive
                            ,ResourceId
                            ,HierarchyId
                            ,ModuleCode
                            ,MRUType
                            ,HideStatusBar
                            )
                    VALUES  (
                             'Reports'
                            ,'Reports'
                            ,'/Reports/ReportHome.aspx'
                            ,6
                            ,@AdmissionsParentId
                            ,500
                            ,0
                            ,GETDATE()
                            ,'support'
                            ,1
                            ,689
                            ,NULL
                            ,NULL
                            ,NULL
                            ,NULL
                            );
                END;
        END;

    COMMIT TRANSACTION;

END TRY
BEGIN CATCH
    PRINT 'ERROR updating lead menu items';
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;

GO
-- ********************************************************************************************************
-- JGINZO END
-- ********************************************************************************************************

-- =============================================
-- Author: Spencer Garrett
-- Create date: 12/08/2015
-- DE12193 QA: links to Listing (Detail) and Listing (Summary) report are switched between each other at the reports page
-- =============================================

BEGIN TRANSACTION;

BEGIN TRY

    DECLARE @busObjId INT
       ,@ResourceId INT;
    SET @busObjId = (
                      SELECT    BusObjectId
                      FROM      dbo.syBusObjects
                      WHERE     Descrip = 'JobListingObject'
                    );
    SET @ResourceId = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   Resource = 'Listing (Detail)'
                      );
    UPDATE  syRptProps
    SET     RptObjId = @busObjId
    WHERE   ResourceId = @ResourceId;
    SET @busObjId = (
                      SELECT    BusObjectId
                      FROM      dbo.syBusObjects
                      WHERE     Descrip = 'JobListSummaryObject'
                    );
    SET @ResourceId = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   Resource = 'Listing (Summary)'
                      );
    UPDATE  syRptProps
    SET     RptObjId = @busObjId
    WHERE   ResourceId = @ResourceId;

END TRY

BEGIN CATCH
    PRINT 'ERROR DE12193 QA:';
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;

COMMIT TRANSACTION;
GO

-- =============================================
-- END DE12193 QA: links to Listing (Detail) and Listing (Summary) report are switched between each other at the reports page
-- =============================================

-- =============================================
-- Author: Spencer Garrett
-- Create date: 01/06/2015
-- US8256 Update Maintenance System Area
-- =============================================

BEGIN TRANSACTION UpdateMenu;

BEGIN TRY

    DECLARE @ResourceId INT;
    SET @ResourceId = 834;
-------------------------------------------------------------------------------------------
-- Add Setup User Options on Maintenance page under Maintenance --> System menu.
-------------------------------------------------------------------------------------------
--PRINT '1 setup user...........................................'
/****** Insert new field for Setup Users in maintenance page for system resource ******/
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syResources
                    WHERE   Resource = 'Setup Users'
                            AND ResourceTypeID = 2
                            AND ResourceID = @ResourceId )
        BEGIN
            INSERT  INTO dbo.syResources
            VALUES  ( @ResourceId,'Setup Users',2,NULL,NULL,4,NULL,NULL,0,NULL,NULL,NULL,NULL );
        END;
    ELSE
        BEGIN
            PRINT CHAR(13) + '(0 row(s) affected)  Not Added ResoureId';
        END;
--PRINT '2 END setup user....................................... '
/****** Insert new field for Setup Users in maintenance page for system menu ******/
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syMenuItems
                    WHERE   DisplayName = 'Setup Users'
                            AND MenuItemTypeId = 3 )
        BEGIN
            INSERT  INTO dbo.syMenuItems
                    SELECT  r.Resource AS MenuName -- varchar(250)
                           ,r.Resource AS DisplayName -- varchar(250)
                           ,NULL AS Url -- nvarchar(250)
                           ,3 AS MenuItemTypeId -- smallint
                           ,mi2.MenuItemId AS ParentId -- int
                           ,200 AS DisplayOrder -- int
                           ,0 AS IsPopup -- bit
                           ,GETDATE() AS ModDate -- datetime
                           ,NULL AS ModUser -- varchar(50)
                           ,1 AS IsActive -- bit
                           ,r.ResourceID AS ResourceId -- smallint
                           ,NULL AS HierarchyId -- uniqueidentifier
                           ,NULL AS ModuleCode -- varchar(5)
                           ,NULL AS MRUType -- int
                           ,NULL AS HideStatusBar -- bit
                    FROM    dbo.syMenuItems mi1
                    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
                    INNER JOIN dbo.syResources r ON r.Resource = 'Setup Users'
                    WHERE   mi1.MenuName = 'Maintenance'
                            AND mi2.MenuName = 'System';
        END;
    ELSE
        BEGIN
            PRINT CHAR(13) + '(0 row(s) affected)';
        END;

--PRINT '3 END setup user....................................... '
-------------------------------------------------------------------------------------------
-- Move all submenu options related to Setup Users under Maintenance --> System --> Setup Users menu.
-------------------------------------------------------------------------------------------

/****** Move Manage Roles from Sytem menu to Maintenance page under Setup Users menu ******/

    UPDATE  mi1
    SET     mi1.ParentId = (
                             SELECT TOP 1
                                    MenuItemId -- Setup Users MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Setup Users'
                                    AND ParentId = (
                                                     SELECT TOP 1
                                                            MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT TOP 1
                                                                                    MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi1.DisplayOrder = 100
    FROM    dbo.syMenuItems mi1
    WHERE   mi1.MenuName = 'Manage Roles';

/****** Move Manage Security from Sytem menu to Maintenance page under Setup Users menu ******/
--PRINT '4 END setup user....................................... '

    UPDATE  mi1
    SET     mi1.ParentId = (
                             SELECT TOP 1
                                    MenuItemId -- Setup Users MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Setup Users'
                                    AND ParentId = (
                                                     SELECT TOP 1
                                                            MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT TOP 1
                                                                                    MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi1.DisplayOrder = 200
    FROM    dbo.syMenuItems mi1
    WHERE   mi1.MenuName = 'Manage Security';

/****** Move Manage Users from Sytem menu to Maintenance page under Setup Users menu ******/
--PRINT '5 END setup user....................................... '

    UPDATE  mi1
    SET     mi1.ParentId = (
                             SELECT TOP 1
                                    MenuItemId -- Setup Users MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Setup Users'
                                    AND ParentId = (
                                                     SELECT TOP 1
                                                            MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT TOP 1
                                                                                    MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi1.DisplayOrder = 300
    FROM    dbo.syMenuItems mi1
    WHERE   mi1.MenuName = 'Manage Users';

/****** Move User Impersonations from Sytem menu to Maintenance page under Setup Users menu ******/
--PRINT '6 END setup user....................................... '

    UPDATE  mi1
    SET     mi1.ParentId = (
                             SELECT TOP 1
                                    MenuItemId -- Setup Users MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Setup Users'
                                    AND ParentId = (
                                                     SELECT TOP 1
                                                            MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT TOP 1
                                                                                    MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi1.DisplayOrder = 400
    FROM    dbo.syMenuItems mi1
    WHERE   mi1.MenuName = 'User Impersonation';

/****** Re-order Status Codes menu in Maintenance page ******/
--PRINT '7 END setup user....................................... '
    UPDATE  mi3
    SET     mi3.DisplayOrder = 300
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi3.MenuName = 'Status Codes';

/****** Re-order WAPI Services menu in Maintenance page ******/

    UPDATE  mi3
    SET     mi3.DisplayOrder = 700
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi3.MenuName = 'WAPI Services';

-------------------------------------------------------------------------------------------
-- Move all submenu options related to Custom under Maintenance --> System --> Custom menu.
-------------------------------------------------------------------------------------------

/****** Move User Defined Fields from Sytem menu to Maintenance page and rename it to Custom ******/

    UPDATE  mi3
    SET     mi3.ParentId = (
                             SELECT MenuItemId -- System MenuItemId from Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 2
                                    AND MenuName = 'System'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuName = 'Maintenance'
                                                            AND MenuItemTypeId = 1
                                                   )
                           )
           ,mi3.DisplayOrder = 600
           ,mi3.DisplayName = 'Custom'
           ,mi3.MenuName = 'Custom'
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi3.MenuName = 'User Defined Fields';

/****** Change view order of Add UDF to a Page under Custom menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 100
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Add UDF to a Page';

/****** Change view order of User Defined Fields (UDF) under Custom menu and rename it to Create User Defined Fields (UDF) ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 200
           ,mi4.DisplayName = 'Create User Defined Fields (UDF)'
           ,mi4.MenuName = 'Create User Defined Fields (UDF)'
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'User Defined Fields (UDF)';

/****** Move Set Up Agency School Mappings from General Options to Custom menu in Maintenance page ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- Custom MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Custom'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 300
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Agency School Mappings';

/****** Move Quick Lead Fields from General Options to Custom menu in Maintenance page and rename it to Set Up Quick Leads Fields ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- Custom MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Custom'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 400
           ,mi4.DisplayName = 'Set Up Quick Leads Fields'
           ,mi4.MenuName = 'Set Up Quick Leads Fields'
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Quick Lead Fields';

-------------------------------------------------------------------------------------------
-- Move all submenu options related to General Options under Maintenance --> System --> General Options menu.
-------------------------------------------------------------------------------------------

/****** Change view order of Relations under General Options menu and rename it to Relationship Types ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 300
           ,mi4.MenuName = 'Relationship Types'
           ,mi4.DisplayName = 'Relationship Types'
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Relations';

/****** Change view order of Set Up Holidays under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 400
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Holidays';

/****** Move Set Up Input Masks from Sytem menu to Maintenance page under General Options menu ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT TOP 1
                                    MenuItemId -- General Options MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'General Options'
                                    AND ParentId = (
                                                     SELECT TOP 1
                                                            MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT TOP 1
                                                                                    MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 500
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi1.MenuName = 'System'
            AND mi2.MenuName = 'Common Tasks'
            AND mi3.MenuName = 'General Options'
            AND mi4.MenuName = 'Set Up Input Masks';

/****** Change view order of Set Up Periods under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 600
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Periods';

/****** Move Set Up Required Fields from Sytem menu to Maintenance page under General Options menu ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- General Options MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'General Options'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 700
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi1.MenuName = 'System'
            AND mi2.MenuName = 'Common Tasks'
            AND mi3.MenuName = 'General Options'
            AND mi4.MenuName = 'Set Up Required Fields';

/****** Change view order of Set Up Student Id under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 800
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Student Id';

/****** Change view order of Set Up Time Records under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 1000
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Time Records';

/****** Change view order of Manage Configuration Settings under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 1100
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Manage Configuration Settings';

/****** Change view order of Remove Local and Session Cache under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 1200
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Remove Local and Session Cache';

/****** Change view order of Upload Logo under General Options menu ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 1300
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Upload Logo';

/****** Change view order of View System Data Dictionary under General Options menu ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- General Options MenuItemId for Maintenance page that will be used instead of current System menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'General Options'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- System MenuItemId for Maintenance page that will be used instead of current System menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'System'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Maintenance page MenuItemId that current General Options will be moved into from current System menu.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 1300
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'View System Data Dictionary';

/****** Insert new field for Reports in Tools menu******/
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syMenuItems
                    WHERE   MenuName = 'Reports'
                            AND MenuItemTypeId = 3 )
        BEGIN
            INSERT  INTO dbo.syMenuItems
                    SELECT  r.Resource AS MenuName -- varchar(250)
                           ,r.Resource AS DisplayName -- varchar(250)
                           ,NULL AS Url -- nvarchar(250)
                           ,3 AS MenuItemTypeId -- smallint
                           ,mi2.MenuItemId AS ParentId -- int
                           ,100 AS DisplayOrder -- int
                           ,0 AS IsPopup -- bit
                           ,GETDATE() AS ModDate -- datetime
                           ,NULL AS ModUser -- varchar(50)
                           ,1 AS IsActive -- bit
                           ,r.ResourceID AS ResourceId -- smallint
                           ,NULL AS HierarchyId -- uniqueidentifier
                           ,NULL AS ModuleCode -- varchar(5)
                           ,NULL AS MRUType -- int
                           ,NULL AS HideStatusBar -- bit
                    FROM    dbo.syMenuItems mi1
                    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
                    INNER JOIN dbo.syResources r ON r.Resource = 'Reports'
                    WHERE   mi1.MenuName = 'Tools'
                            AND mi2.MenuName = 'Common Tasks';
        END;
    ELSE
        BEGIN
            PRINT CHAR(13) + '(0 row(s) affected)';
        END;

/****** Move Set Up AdHoc Reports from Sytem menu in Maintenance page to Tools --> Common Tasks --> Reports menu ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- Reports menu MenuItemId for Common Tasks menu under Tools menu.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'Reports'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- Common Tasks MenuItemId for Tools menu.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'Common Tasks'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Tools menu MenuItemId.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Tools'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 100
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Set Up Adhoc Reports';

END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT
       ,@LastLine INT;

    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE()
           ,@LastLine = ERROR_LINE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMenu;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateMenu;
		--print 'commit'
    END;

-- =============================================
-- END US8256 Update Maintenance System Area
-- =============================================


-- =============================================
-- Author: Spencer Garrett
-- Create date: 01/06/2015
-- US8274 UI - 1098-T Processing
-- =============================================

BEGIN TRANSACTION UpdateMenu;

BEGIN TRY

/****** Move Service 1098T menu item from Financial Aid menu to Student Accounts menu and make first in item list ******/

    UPDATE  mi4
    SET     mi4.ParentId = (
                             SELECT MenuItemId -- General Options menu MenuItemId for Financial Aid menu in Maintenance page.
                             FROM   dbo.syMenuItems
                             WHERE  MenuItemTypeId = 3
                                    AND MenuName = 'General Options'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- Financial Aid menu MenuItemId in Maintenance page.
                                                     FROM   dbo.syMenuItems
                                                     WHERE  MenuItemTypeId = 2
                                                            AND MenuName = 'Student Accounts'
                                                            AND ParentId = (
                                                                             SELECT MenuItemId -- Maintenance page MenuItemId.
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  MenuName = 'Maintenance'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                           )
           ,mi4.DisplayOrder = 100
           ,mi4.MenuName = '1098-T Processing'
           ,mi4.DisplayName = '1098-T Processing'
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi4.MenuName = 'Service1098T';

/****** Change order of Agency Sponsors menu item ******/

    UPDATE  mi4
    SET     mi4.DisplayOrder = 110
    FROM    dbo.syMenuItems mi1
    INNER JOIN dbo.syMenuItems mi2 ON mi2.ParentId = mi1.MenuItemId
    INNER JOIN dbo.syMenuItems mi3 ON mi3.ParentId = mi2.MenuItemId
    INNER JOIN dbo.syMenuItems mi4 ON mi4.ParentId = mi3.MenuItemId
    WHERE   mi1.MenuName = 'Maintenance'
            AND mi2.MenuName = 'Student Accounts'
            AND mi3.MenuName = 'General Options'
            AND mi4.MenuName = 'Agency Sponsors';

END TRY
BEGIN CATCH
    DECLARE @ErrorMessage1 NVARCHAR(MAX)
       ,@ErrorSeverity1 INT
       ,@ErrorState1 INT;
    SELECT  @ErrorMessage1 = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity1 = ERROR_SEVERITY()
           ,@ErrorState1 = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMenu;
        END;
    RAISERROR (@ErrorMessage1, @ErrorSeverity1, @ErrorState1);
END CATCH;
IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateMenu;
		--print 'commit'
    END;

GO

-- =============================================
-- END US8274 Update Maintenance System Area
-- =============================================
-- =========================================================
-- Author: Jose Alfredo
-- Create date: 01/06/2015
-- US8291 New table:  Admissions Level - Data Initialization
-- Updated to include Vo Tech -- ZL 9/28/2016
-- =========================================================
DECLARE @Rows INT = (
                      SELECT    COUNT(*)
                      FROM      adLevel
                    );
DECLARE @vt INT = (
                    SELECT  COUNT(*)
                    FROM    adLevel
                    WHERE   Description = 'Vo Tech'
                  );


IF @Rows = 0
    BEGIN
        DECLARE @StatusID UNIQUEIDENTIFIER = (
                                               SELECT   StatusId
                                               FROM     syStatuses
                                               WHERE    StatusCode = 'A'
                                             );

        INSERT  INTO adLevel
                (
                 LevelId
                ,Code
                ,Description
                ,ModDate
                ,ModUser
                ,StatusId
                )
        VALUES  (
                 1
                ,'College'
                ,'College'
                ,GETDATE()
                ,'JAGG'
                ,@StatusID
                );

        INSERT  INTO adLevel
                (
                 LevelId
                ,Code
                ,Description
                ,ModDate
                ,ModUser
                ,StatusId
                )
        VALUES  (
                 2
                ,'HSchool'
                ,'High School'
                ,GETDATE()
                ,'JAGG'
                ,@StatusID
                );

        INSERT  INTO adLevel
                (
                 LevelId
                ,Code
                ,Description
                ,ModDate
                ,ModUser
                ,StatusId
                )
        VALUES  (
                 3
                ,'LifeSkill'
                ,'Life Skills'
                ,GETDATE()
                ,'JAGG'
                ,@StatusID
                );

        INSERT  INTO adLevel
                (
                 LevelId
                ,Code
                ,Description
                ,ModDate
                ,ModUser
                ,StatusId
                )
        VALUES  (
                 4
                ,'VoTech'
                ,'Vo Tech'
                ,GETDATE()
                ,'JAGG'
                ,@StatusID
                );
    END;
ELSE
    IF @vt = 0
        BEGIN
            DECLARE @StatusID2 UNIQUEIDENTIFIER = (
                                                    SELECT  StatusId
                                                    FROM    syStatuses
                                                    WHERE   StatusCode = 'A'
                                                  );

            INSERT  INTO adLevel
                    (
                     LevelId
                    ,Code
                    ,Description
                    ,ModDate
                    ,ModUser
                    ,StatusId
                    )
                    SELECT  MAX(LevelId) + 1
                           ,'VoTech'
                           ,'Vo Tech'
                           ,GETDATE()
                           ,'JAGG'
                           ,@StatusID2
                    FROM    adLevel;
        END;
GO
-- =========================================================
-- End: Jose Alfredo
-- =========================================================

-----------------------------------------------------------------------
-- BEGIN DE1242 DATA CHANGES
-----------------------------------------------------------------------
-- ********************************************************************************************************
-- DE1242:"User is asked for not shown required fields when creates new enrollment at Enrollments page" 
-- FIX: Remove the Drop Reason Id and Date Determined from required fields in the Enrollment Page.
-- Author: hcordero
-- ********************************************************************************************************
BEGIN TRANSACTION RemoveRequiredFields;
BEGIN TRY

    DELETE  FROM syResTblFlds
    WHERE   ResDefId IN ( SELECT    ResDefId
                          FROM      syResTblFlds
                          WHERE     TblFldsId IN ( SELECT   TblFldsId
                                                   FROM     syTblFlds
                                                   WHERE    FldId IN ( SELECT   FldId
                                                                       FROM     syFields
                                                                       WHERE    FldName = 'DropReasonId' )
                                                            AND TblId IN ( SELECT   TblId
                                                                           FROM     syTables
                                                                           WHERE    TblName = 'arStuEnrollments' ) ) );

    DELETE  FROM syResTblFlds
    WHERE   ResDefId IN ( SELECT    ResDefId
                          FROM      syResTblFlds
                          WHERE     TblFldsId IN ( SELECT   TblFldsId
                                                   FROM     syTblFlds
                                                   WHERE    FldId IN ( SELECT   FldId
                                                                       FROM     syFields
                                                                       WHERE    FldName = 'DateDetermined' )
                                                            AND TblId IN ( SELECT   TblId
                                                                           FROM     syTables
                                                                           WHERE    TblName = 'arStuEnrollments' )
                                                            AND ResourceId IN ( SELECT TOP 1
                                                                                        ResourceID
                                                                                FROM    syResources
                                                                                WHERE   Resource = 'Enrollments'
                                                                                        AND ResourceURL = '~/AR/StudentEnrollments.aspx' ) ) );

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION CREATE_TABLE;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION RemoveRequiredFields;
        --PRINT 'TRANSACTION REMOVE_DROP_REASON_AND_DATE_DETERMINE_FROM_ENROLLMENT COMMITED!!!';
    END;

GO
-----------------------------------------------------------------------
-- END DE1242 DATA CHANGES
-----------------------------------------------------------------------
-----------------------------------------------------------------------
-- BEGIN US8403 DATA CHANGES
-----------------------------------------------------------------------
-- ********************************************************************************************************
-- US8403: Update Suspended and Suspension system Statusese
-- FIX: Remapped any existing syStatusCodes using the SysStatusId = 21 to be 11 (Suspended). 
--		Updated the GEProgramStatus to = E in sySysStatus  with id 11 (Suspended) .
--		Deleted sySysStatus with ID 21
--		Updated Description of SysStatusId with ID 11 (Suspended) to Suspension
-- Author: hcordero
-- ********************************************************************************************************

BEGIN TRANSACTION DROP_SYS_STATUS_SUSPENSION;
BEGIN TRY

    SET ANSI_NULLS ON;

    SET QUOTED_IDENTIFIER ON;

    SET ANSI_PADDING ON;

    UPDATE  syStatusCodes
    SET     SysStatusId = 11
    WHERE   StatusCodeId IN ( SELECT    StatusCodeId
                              FROM      syStatusCodes
                              WHERE     SysStatusId = 21 );

    UPDATE  sySysStatus
    SET     GEProgramStatus = 'E'
    WHERE   SysStatusId = 11;

    /*
    PRIOR DATA BEFORE DELETING
    SysStatusId	SysStatusDescrip	StatusId	StatusLevelId	ModUser	ModDate	InSchool	GEProgramStatus	PostAcademics
    21	Suspension	F23DE1E2-D90A-4720-B4C7-0F6FB09C9965	2	NULL	NULL	1	E	0
    */

    DELETE  FROM sySysStatus
    WHERE   SysStatusId = 21;

    UPDATE  sySysStatus
    SET     SysStatusDescrip = 'Suspension'
    WHERE   SysStatusId = 11;


END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION DROP_SYS_STATUS_SUSPENSION;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION DROP_SYS_STATUS_SUSPENSION;
        PRINT 'DROP_SYS_STATUS_SUSPENSION COMMITED';
    END;

GO
-----------------------------------------------------------------------
-- END US8403 DATA CHANGES
-----------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
--End Troy: 1098T Returning No Start students who started before the year
------------------------------------------------------------------------------------------------------------------

-- ===============================================================================================
-- US8289 Crate Contact Types table
-- JTorres 2016-02-03
--  
-- ===============================================================================================


-- ===============================================================================================
-- Adding Resource ID 830 "ContactTypes"  
-- ===============================================================================================
-- general fields
DECLARE @Error AS INTEGER; 
DECLARE @DisplayName AS NVARCHAR(250); 
DECLARE @ModDate AS DATETIME;
DECLARE @ModUser AS NVARCHAR(50); 

-- syContactTypes 
DECLARE @CampGrpId AS UNIQUEIDENTIFIER;
DECLARE @StatusId AS UNIQUEIDENTIFIER;
DECLARE @ContactTypeID AS UNIQUEIDENTIFIER;
DECLARE @ContactTypeCode AS NVARCHAR(12);
DECLARE @ContactTypeDescript AS NVARCHAR(50);

-- Resource Id
DECLARE @ResourceId AS INT;
DECLARE @Resource AS NVARCHAR(200);
DECLARE @ResourceTypeID AS INT;
DECLARE @ResourceURL AS NVARCHAR(100);
DECLARE @AllowSchReqFlds AS BIT;
DECLARE @MRUTypeId AS SMALLINT;
DECLARE @UsedIn AS INT;
DECLARE @TblFldsId AS INT;

-- MenuItem      
DECLARE @PPMenuName AS NVARCHAR(250);
DECLARE @PMenuName AS NVARCHAR(250);
DECLARE @mMenuName AS NVARCHAR(250);
DECLARE @MenuParent AS INT;
DECLARE @MenuItemId AS INT;
DECLARE @MenuName AS NVARCHAR(250);
DECLARE @Url AS NVARCHAR(250);
DECLARE @MenuItemTypeId AS SMALLINT;
DECLARE @DisplayOrder AS INT;
DECLARE @IsPopup AS BIT;
DECLARE @IsActive AS BIT;

--syNavigationNodes
DECLARE @HierarchyId AS UNIQUEIDENTIFIER;
DECLARE @ParentId AS UNIQUEIDENTIFIER;
DECLARE @HierarchyIndex AS SMALLINT;
DECLARE @IsShipped AS SMALLINT;

-- syRlsResLvls
DECLARE @AccessLevel AS INT;
DECLARE @RoleId AS UNIQUEIDENTIFIER;
DECLARE @Code AS NVARCHAR(12);

-- Tbl
DECLARE @TblId AS INTEGER;
DECLARE @TblName AS NVARCHAR(50);
DECLARE @TblDescrip AS NVARCHAR(100);
DECLARE @TblPk_Fld AS INTEGER;

-- syFields
DECLARE @FldId AS INTEGER;   
DECLARE @FLdName AS NVARCHAR(200);
DECLARE @FldTypeId AS INTEGER;
DECLARE @FldType AS NVARCHAR(20);
DECLARE @FldLen AS INTEGER;

-- syTblFlds
--DECLARE @TblNamexField      AS NVARCHAR(50)
--DECLARE @TblIdxField        AS INTEGER
--DECLARE @FKColDescrip       AS NVARCHAR(150)

-- syFldCaptions
DECLARE @FldCapId AS INTEGER;
DECLARE @Caption AS NVARCHAR(100);
DECLARE @LangId AS INTEGER;
DECLARE @LangName AS NVARCHAR(5);

--syResTblFlds
DECLARE @ResDefId AS INTEGER;
DECLARE @Required AS BIT;

--syDDLS
DECLARE @DispFldId AS INTEGER;  
DECLARE @DDLId AS INTEGER;
DECLARE @DDLName AS NVARCHAR(100);
DECLARE @DDLResourceId AS INTEGER;

--DECLARE @DispFldTypeId      AS INTEGER
--DECLARE @DispFldType        AS NVARCHAR(20)
--DECLARE @DispFldLen         AS INTEGER
--DECLARE @DispFldCapId       AS INTEGER
--DECLARE @DispCaption        AS NVARCHAR(100)
--DECLARE @DispTblFldsId      AS INTEGER
--DECLARE @DispResDefId       AS INTEGER
--DECLARE @DispResourceId     AS INTEGER
--DECLARE @DispResourceURL    AS NVARCHAR(100)
--
--DECLARE @LangName           AS NVARCHAR(5)
--DECLARE @DDLTblName         AS NVARCHAR(50)
--DECLARE @DDLTblId           AS INTEGER


-- general fields
SET @Error = 0;
SET @DisplayName = 'Contact Types'; 
SET @ModDate = GETDATE();
SET @ModUser = 'support';

-- syContactTypes 
SELECT  @CampGrpId = SCG.CampGrpId
FROM    syCampGrps AS SCG
WHERE   SCG.CampGrpCode = 'All';
SELECT  @StatusId = SS.StatusId
FROM    syStatuses AS SS
WHERE   SS.Status = 'Active';

-- Resource Id
SET @ResourceId = 830;
   -- Hard coded  -- standard for all DB's (Clients)
SET @Resource = 'ContactTypes';
SET @ResourceTypeID = 4;
    -- Maintenance --  SELECT * FROM syResourceTypes AS SRT
SET @ResourceURL = '~/ad/ContactTypes.aspx';
SET @AllowSchReqFlds = 0;
SET @MRUTypeId = 4;
 --  Leads        -- SELECT * FROM syMRUTypes AS SMT
SET @UsedIn = 975;
 -- page resource where is used (this is Report page)
SET @TblFldsId = NULL;

-- MenuItem 
SET @PPMenuName = 'Maintenance';
SET @PMenuName = 'System';
SET @mMenuName = 'General Options';
SET @MenuName = @DisplayName;
SET @Url = '/SY/MaintenanceBase.aspx';
SET @MenuItemTypeId = 4;
    -- Page  -- SELECT * FROM syMenuItemType AS SMIT
SET @DisplayOrder = 150;
  -- After Address Type  SELECT * FROM syMenuItems AS SMI WHERE SMI.ParentId = @MenuParent ORDER BY SMI.DisplayOrder
SET @IsPopup = 0;
    -- it is not popup menu
SET @IsActive = 1;
    --  Yes, it is active
SET @ResourceId = 830;
  -- Hard coded  -- standard for all DB's (Clients)  

-- syNavigationNodes
SET @HierarchyIndex = 8;
    -- Hierarchy Index???? 
SET @IsShipped = 1;

-- syRlsResLvls
SET @AccessLevel = 15;
   -- sysadmins role with full access (15)
SET @Code = 'SYSADMINS';
 -- SELECT SR.* FROM syRoles AS SR 
SET @RoleId = (
                SELECT TOP 1
                        SR.RoleId
                FROM    syRoles AS SR
                WHERE   SR.Code = @Code
              );

-- Tbl
SET @TblName = 'syContactTypes';
SET @TblDescrip = 'syContactTypes Type Lookup Table.';

-- syFldCaptions
SET @LangName = 'EN-US';
SELECT  @LangId = SL.LangId
FROM    syLangs AS SL
WHERE   SL.LangName = @LangName;

--syDDLS
SET @DDLName = 'Contact Types';

BEGIN TRANSACTION AddNewFields;
BEGIN TRY  
        -- ===============================================================================================
        -- Adding default records to  syContactTypes ('Emergency', 'Primary'  'Other') 
        -- ===============================================================================================
    IF ( @Error = 0 )  -- Insert 'Emergency'
        BEGIN
            SET @ContactTypeID = 'C5C2CE3E-3D9B-423B-B3A4-630748C08800'; 
            SET @ContactTypeCode = 'Emergency';
            SET @ContactTypeDescript = 'Emergency';
            IF NOT EXISTS ( SELECT  1
                            FROM    syContactTypes AS SCT
                            WHERE   SCT.ContactTypeId = @ContactTypeID )
                BEGIN
                    INSERT  INTO syContactTypes
                            (
                             ContactTypeId
                            ,ContactTypeCode
                            ,ContactTypeDescrip
                            ,StatusId
                            ,CampGrpId
                            ,ModUser
                            ,ModDate 
                            )
                    VALUES  (
                             @ContactTypeID
                            ,@ContactTypeCode
                            ,@ContactTypeDescript
                            ,@StatusId
                            ,@CampGrpId
                            ,@ModUser
                            ,@ModDate
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syContactTypes ' + @ContactTypeCode
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syContactTypes ' + @ContactTypeCode
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syContactTypes ' + @ContactTypeCode
                END;
        END; -- -- Insert 'Emergency' -- Ends Here --
    IF ( @Error = 0 )  -- Insert 'Primary'
        BEGIN
            SET @ContactTypeID = 'AF82CC8E-8742-4ECB-89F9-40EC7A75F0B6'; 
            SET @ContactTypeCode = 'Primary';
            SET @ContactTypeDescript = 'Primary';
            IF NOT EXISTS ( SELECT  1
                            FROM    syContactTypes AS SCT
                            WHERE   SCT.ContactTypeId = @ContactTypeID )
                BEGIN
                    INSERT  INTO syContactTypes
                            (
                             ContactTypeId
                            ,ContactTypeCode
                            ,ContactTypeDescrip
                            ,StatusId
                            ,CampGrpId
                            ,ModUser
                            ,ModDate 
                            )
                    VALUES  (
                             @ContactTypeID
                            ,@ContactTypeCode
                            ,@ContactTypeDescript
                            ,@StatusId
                            ,@CampGrpId
                            ,@ModUser
                            ,@ModDate
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syContactTypes ' + @ContactTypeCode
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syContactTypes ' + @ContactTypeCode
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syContactTypes ' + @ContactTypeCode
                END;
        END; -- -- Insert 'Primary' -- Ends Here --
    IF ( @Error = 0 )  -- Insert 'Other'
        BEGIN
            SET @ContactTypeID = '110AC0A1-FFED-4EA9-A5EF-3C03D829F6D3'; 
            SET @ContactTypeCode = 'Other';
            SET @ContactTypeDescript = 'Other';
            IF NOT EXISTS ( SELECT  1
                            FROM    syContactTypes AS SCT
                            WHERE   SCT.ContactTypeId = @ContactTypeID )
                BEGIN
                    INSERT  INTO syContactTypes
                            (
                             ContactTypeId
                            ,ContactTypeCode
                            ,ContactTypeDescrip
                            ,StatusId
                            ,CampGrpId
                            ,ModUser
                            ,ModDate 
                            )
                    VALUES  (
                             @ContactTypeID
                            ,@ContactTypeCode
                            ,@ContactTypeDescript
                            ,@StatusId
                            ,@CampGrpId
                            ,@ModUser
                            ,@ModDate
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syContactTypes ' + @ContactTypeCode
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syContactTypes ' + @ContactTypeCode
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syContactTypes ' + @ContactTypeCode
                END;
        END; -- -- Insert 'Other' -- Ends Here --
        -- ===============================================================================================
        -- Adding Resource ID 830 "ContactTypes"  
        -- ===============================================================================================
    IF ( @Error = 0 )  -- Insert ResourceID
        BEGIN
            IF NOT EXISTS ( SELECT  SR.ResourceID
                            FROM    syResources AS SR
                            WHERE   SR.ResourceID = @ResourceId )
                BEGIN
                    INSERT  INTO dbo.syResources
                            (
                             ResourceID
                            ,Resource
                            ,ResourceTypeID
                            ,ResourceURL
                            ,SummListId
                            ,ChildTypeId
                            ,ModDate
                            ,ModUser
                            ,AllowSchlReqFlds
                            ,MRUTypeId
                            ,UsedIn
                            ,TblFldsId
                            ,DisplayName
                            )
                    VALUES  (
                             @ResourceId
                            ,@Resource
                            ,@ResourceTypeID
                            ,@ResourceURL
                            ,NULL
                            ,NULL
                            ,@ModDate
                            ,@ModUser
                            ,@AllowSchReqFlds
                            ,@MRUTypeId
                            ,@UsedIn
                            ,@TblFldsId
                            ,@DisplayName
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syResources' 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResources' 
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syResources' 
                END;
        END; -- Adding Resource ID 830 "ContactTypes"  -- Ends Here --
        -- ===============================================================================================
        --  Adding MenuItem  "ContactTypes"  
        -- ===============================================================================================        
    IF ( @Error = 0 )  -- insert Menu item
        BEGIN
            SET @MenuParent = (
                                SELECT TOP 1
                                        SMI.MenuItemId
                                FROM    syMenuItems AS SMI
                                INNER JOIN syMenuItems p ON SMI.ParentId = p.MenuItemId
                                INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
                                WHERE   SMI.MenuName = @mMenuName
                                        AND p.MenuName = @PMenuName
                                        AND pp.MenuName = @PPMenuName
                              );

            IF @MenuParent IS NOT NULL
                BEGIN
                    IF NOT EXISTS ( SELECT  1
                                    FROM    syMenuItems AS SMI
                                    WHERE   SMI.MenuName = @MenuName
                                            AND SMI.Url = @Url
                                            AND SMI.ParentId = @MenuParent )
                        BEGIN
                            INSERT  INTO dbo.syMenuItems
                                    (
                                     MenuName
                                    ,DisplayName
                                    ,Url
                                    ,MenuItemTypeId
                                    ,ParentId
                                    ,DisplayOrder
                                    ,IsPopup
                                    ,ModDate
                                    ,ModUser
                                    ,IsActive
                                    ,ResourceId
                                    ,HierarchyId
                                    ,ModuleCode
                                    ,MRUType
                                    ,HideStatusBar
                                    )
                            VALUES  (
                                     @MenuName
                                    ,@DisplayName
                                    ,@Url
                                    ,@MenuItemTypeId
                                    ,@MenuParent
                                    ,@DisplayOrder
                                    ,@IsPopup
                                    ,@ModDate
                                    ,@ModUser
                                    ,1
                                    ,@ResourceId
                                    ,NULL
                                    ,NULL
                                    ,NULL
                                    ,NULL
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SELECT  @MenuItemId = SMI.MenuItemId
                                    FROM    syMenuItems AS SMI
                                    WHERE   SMI.MenuName = @MenuName
                                            AND SMI.Url = @Url
                                            AND SMI.ParentId = @MenuParent;
                                    SET @Error = 0;  
--                                        PRINT 'Successful syMenuItems' 
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
--                                        PRINT 'Failed syMenuItems' 
                                END;
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed 01 syMenuItems' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed Exists syMenuItems' 
                END;                     
                 
        END; -- Adding MenuItem  "ContactTypes"  -- Ends Here --
        -- ===============================================================================================
        --  Fill syNavigationNodes     --Add navigation node
        -- ===============================================================================================        
    IF ( @Error = 0 )  -- syNavigationNodes
        BEGIN
                --Add navigation node
            SELECT  @ParentId = SNN.ParentId
            FROM    syNavigationNodes AS SNN
            WHERE   SNN.ResourceId = (
                                       SELECT   MIN(SMI.ResourceId)
                                       FROM     syMenuItems AS SMI
                                       WHERE    SMI.ParentId = @MenuParent
                                     );
            IF NOT EXISTS ( SELECT  1
                            FROM    syNavigationNodes AS SNN
                            WHERE   SNN.ResourceId = @ResourceId
                                    AND SNN.ParentId = @ParentId )
                BEGIN
                    INSERT  INTO syNavigationNodes
                            (
                             HierarchyIndex
                            ,ResourceId
                            ,ParentId
                            ,ModDate
                            ,ModUser
                            ,IsPopupWindow
                            ,IsShipped
                            )
                    VALUES  (
                             @HierarchyIndex
                            ,@ResourceId
                            ,@ParentId
                            ,@ModDate
                            ,@ModUser
                            ,@IsPopup
                            ,@IsShipped
                            );
                    
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @HierarchyId = SNN.HierarchyId
                            FROM    syNavigationNodes AS SNN
                            WHERE   SNN.ResourceId = @ResourceId
                                    AND SNN.ParentId = @ParentId;
                            SET @Error = 0;  
--                                PRINT 'Successful syNavigationNodes' 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syNavigationNodes' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed Exists syNavigationNodes' 
                END;
        END;  --Add navigation node  -- Ends Here --
        -- ===============================================================================================
        --  Update MenuItem  "ContactTypes"  (with HierarchyId)
        -- ===============================================================================================
    IF ( @Error = 0 )  -- Update Table with Field (Id)
        BEGIN
            IF EXISTS ( SELECT  1
                        FROM    syMenuItems AS SMI
                        WHERE   SMI.MenuItemId = @MenuItemId )
                BEGIN
                    UPDATE  SMI
                    SET     SMI.HierarchyId = @HierarchyId
                    FROM    syMenuItems AS SMI
                    WHERE   SMI.MenuItemId = @MenuItemId;
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful Updates  syMenuItems with HierarchyId'
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed Updates  syMenuItems with HierarchyId' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 Updates  syMenuItems with HierarchyId' 
                END;
        END;  -- Update MenuItem  "ContactTypes"  (with HierarchyId) -- Ends Here -- 
        -- ===============================================================================================
        --  Fill syRlsResLvls    --Link new resource to sysadmins role with full access (15)
        -- ===============================================================================================        
    IF ( @Error = 0 )  -- syRlsResLvls  Link new resource to sysadmins role 
        BEGIN
                ----Link new resource to sysadmins role with full access (15)
            IF NOT EXISTS ( SELECT  1
                            FROM    syRlsResLvls
                            WHERE   ResourceID = @ResourceId
                                    AND AccessLevel = @AccessLevel )
                BEGIN
                    INSERT  INTO syRlsResLvls
                            (
                             RRLId
                            ,RoleId
                            ,ResourceID
                            ,AccessLevel
                            ,ModDate
                            ,ModUser
                            )
                    VALUES  (
                             NEWID()
                            ,@RoleId
                            ,@ResourceId
                            ,@AccessLevel
                            ,@ModDate
                            ,@ModUser
                            );	
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syRlsResLvls' 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syRlsResLvls' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed Exists syRlsResLvls' 
                END;
        END;   -- syRlsResLvls  Link new resource to sysadmins role -- Ends Here --
        -- ===============================================================================================
        --  Adding table record to syTables table
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert record of Table  
        BEGIN   
            IF NOT EXISTS ( SELECT  1
                            FROM    syTables AS ST
                            WHERE   ST.TblName = @TblName )
                BEGIN
                    SELECT  @TblId = MAX(ST.TblId) + 1
                    FROM    syTables AS ST;
                    INSERT  INTO syTables
                            ( TblId,TblName,TblDescrip,TblPK )
                    VALUES  ( @TblId,@TblName,@TblDescrip,0 );  -- TblPK should be updated after create syFields with @TblPk_Fld  FldId of ContactTypeID field
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syTables' 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTables' 
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTables' 
                END;
        END; --  Adding table record to syTables table -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syFields, syFldCaption, syTblFlds and syResTblflds for the field "ContactTypeID"
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert syFields 
        BEGIN 
                -- ===============================================================================================
                --  Adding records to syFields for the field "ContactTypeID"
                -- ===============================================================================================
            SET @FLdName = 'ContactTypeID';
            SET @FldType = 'Uniqueidentifier';
            SET @FldLen = 16;   
            SELECT  @FldTypeId = SFT.FldTypeId
            FROM    syFieldTypes AS SFT
            WHERE   SFT.FldType = @FldType; 

            IF NOT EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName )
                BEGIN
                    SELECT  @FldId = MAX(SF.FldId) + 1
                    FROM    dbo.syFields AS SF;
                    INSERT  dbo.syFields
                            (
                             FldId
                            ,FldName
                            ,FldTypeId
                            ,FldLen
                            ,DDLId
                            ,DerivedFld
                            ,SchlReq
                            ,LogChanges
                            ,Mask
                            )
                    VALUES  (
                             @FldId
                            ,@FLdName
                            ,@FldTypeId
                            ,@FldLen
                            ,NULL
                            ,0
                            ,0
                            ,1
                            ,NULL
                            ); 
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @FldId = SF.FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName;
                            SELECT  @TblPk_Fld = SF.FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName; 
                            SET @Error = 0;  
--                                PRINT 'Successful syFields ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields ' + @FLdName
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SELECT  @TblPk_Fld = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful Field exists  ' + @FLdName
                        --SET @Error = 1
                        --PRINT 'Failed syFields ' + @FLdName
                END;
        END; --  Adding records to syFields for the field "ContactTypeID"   -- Ends Here --
    IF ( @Error = 0 )  -- insert Caption Id 
        BEGIN
                -- ===============================================================================================
                --  Adding records to syFldCaptions for the field "ContactTypeID"
                -- ===============================================================================================
            SET @Caption = 'Contact Type ID';
            IF NOT EXISTS ( SELECT  1
                            FROM    syFldCaptions AS SFC
                            WHERE   SFC.FldId = @FldId
                                    AND SFC.Caption = @Caption )
                BEGIN
                    SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                    FROM    syFldCaptions AS SFC;
                    INSERT  INTO syFldCaptions
                            (
                             FldCapId
                            ,FldId
                            ,LangId
                            ,Caption
                            ,FldDescrip
                            )
                    VALUES  (
                             @FldCapId
                            ,@FldId
                            ,@LangId
                            ,@Caption
                            ,@Caption
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syFldCaptions' + @Caption
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFldCaptions' + @Caption
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 0;
--                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                        --SET @Error = 1
                        --PRINT 'Failed syFldCaptions ' +  @Caption
                END;
        END; --  Adding records to syFldCaptions for the field "ContactTypeID"   -- Ends Here --
    IF ( @Error = 0 )  -- insert Tbl vs Fields Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syTblFlds for the field "ContactTypeID"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    dbo.syTblFlds AS STF
                            WHERE   STF.FldId = @FldId
                                    AND STF.TblId = @TblId )
                BEGIN
                    SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                    FROM    syTblFlds AS STF;
                    INSERT  syTblFlds
                            (
                             TblFldsId
                            ,TblId
                            ,FldId
                            ,CategoryId
                            ,FKColDescrip
                            )
                    VALUES  (
                             @TblFldsId
                            ,@TblId
                            ,@FldId
                            ,NULL
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTblFlds ' + @FLdName
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                END;
        END; --  Adding records to syTblFlds for the field "ContactTypeID" -- Ends Here --
    IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syResTblflds for the field "ContactTypeID"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    syResTblFlds AS SRTF
                            WHERE   SRTF.TblFldsId = @TblFldsId
                                    AND SRTF.ResourceId = @ResourceId )
                BEGIN
                    SET @Required = 1;  -- Required Yes
                    SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                    FROM    syResTblFlds AS SRTF;
                    INSERT  INTO syResTblFlds
                            (
                             ResDefId
                            ,ResourceId
                            ,TblFldsId
                            ,Required
                            ,SchlReq
                            ,ControlName
                            ,UsePageSetup
                            )
                    VALUES  (
                             @ResDefId
                            ,@ResourceId
                            ,@TblFldsId
                            ,@Required
                            ,0
                            ,NULL
                            ,1
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syResTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResTblFlds ' + @FLdName 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syResTblFlds ' + @FLdName
                END;
        END; --  Adding records to syResTblflds for the field "ContactTypeID" -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syFields, syTblFlds and syResTblflds for the field "ContactTypeCode"
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insertsyFields
        BEGIN 
                -- ===============================================================================================
                --  Adding records to syFields for the field "ContactTypeCode"
                -- ===============================================================================================
            SET @FLdName = 'ContactTypeCode';
            SET @FldType = 'Varchar';
            SET @FldLen = 12;   
            SELECT  @FldTypeId = SFT.FldTypeId
            FROM    syFieldTypes AS SFT
            WHERE   SFT.FldType = @FldType; 

            IF NOT EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName )
                BEGIN
                    SELECT  @FldId = MAX(SF.FldId) + 1
                    FROM    dbo.syFields AS SF;
                    INSERT  dbo.syFields
                            (
                             FldId
                            ,FldName
                            ,FldTypeId
                            ,FldLen
                            ,DDLId
                            ,DerivedFld
                            ,SchlReq
                            ,LogChanges
                            ,Mask
                            )
                    VALUES  (
                             @FldId
                            ,@FLdName
                            ,@FldTypeId
                            ,@FldLen
                            ,NULL
                            ,0
                            ,0
                            ,1
                            ,NULL
                            ); 
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @FldId = SF.FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName;
                            SET @Error = 0; 
--                                PRINT 'Successful syFields ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields ' + @FLdName
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful Field exists  ' + @FLdName
                        --SET @Error = 1
                        --PRINT 'Failed syFields ' + @FLdName
                END;
        END; --  Adding records to syFields for the field "ContactTypeCode" -- Ends Here --
    IF ( @Error = 0 )  -- insert Caption Id 
        BEGIN
                -- ===============================================================================================
                --  Adding records to syFldCaptions for the field "ContactTypeCode"
                -- ===============================================================================================
            SET @Caption = 'Code';
            IF NOT EXISTS ( SELECT  1
                            FROM    syFldCaptions AS SFC
                            WHERE   SFC.FldId = @FldId
                                    AND SFC.Caption = @Caption )
                BEGIN
                    SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                    FROM    syFldCaptions AS SFC;
                    INSERT  INTO syFldCaptions
                            (
                             FldCapId
                            ,FldId
                            ,LangId
                            ,Caption
                            ,FldDescrip
                            )
                    VALUES  (
                             @FldCapId
                            ,@FldId
                            ,@LangId
                            ,@Caption
                            ,@Caption
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syFldCaptions' + @Caption
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFldCaptions' + @Caption
                        END;
                END; 
            ELSE
                BEGIN
                    SET @Error = 0;
--                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                        --SET @Error = 1
                        --PRINT 'Failed syFldCaptions ' +  @Caption
                END;
        END; --  Adding records to syFldCaptions for the field "ContactTypeCode"   -- Ends Here --
    IF ( @Error = 0 )  -- insert Tbl vs Fields Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syTblFlds for the field "ContactTypeCode"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    dbo.syTblFlds AS STF
                            WHERE   STF.FldId = @FldId
                                    AND STF.TblId = @TblId )
                BEGIN
                    SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                    FROM    syTblFlds AS STF;
                    INSERT  syTblFlds
                            (
                             TblFldsId
                            ,TblId
                            ,FldId
                            ,CategoryId
                            ,FKColDescrip
                            )
                    VALUES  (
                             @TblFldsId
                            ,@TblId
                            ,@FldId
                            ,NULL
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTblFlds ' + @FLdName
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTblFlds ' + @FLdName
                END;
        END; --  Adding records to syTblFlds for the field "ContactTypeCode"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syResTblflds for the field "ContactTypeCode"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    syResTblFlds AS SRTF
                            WHERE   SRTF.TblFldsId = @TblFldsId
                                    AND SRTF.ResourceId = @ResourceId )
                BEGIN
                    SET @Required = 1;  -- Required Yes
                    SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                    FROM    syResTblFlds AS SRTF;
                    INSERT  INTO syResTblFlds
                            (
                             ResDefId
                            ,ResourceId
                            ,TblFldsId
                            ,Required
                            ,SchlReq
                            ,ControlName
                            ,UsePageSetup
                            )
                    VALUES  (
                             @ResDefId
                            ,@ResourceId
                            ,@TblFldsId
                            ,@Required
                            ,0
                            ,NULL
                            ,1
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syResTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResTblFlds ' + @FLdName
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syResTblFlds ' + @FLdName
                END;
        END; --  Adding records to syResTblflds for the field "ContactTypeCode"  -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syFields, syTblFlds and syResTblflds for the field "StatusId"
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert syFields
        BEGIN 
                -- ===============================================================================================
                --  Adding records to syFields for the field "StatusId"
                -- ===============================================================================================
            SET @FLdName = 'StatusId';
            SET @FldType = 'Uniqueidentifier';
            SET @FldLen = 16;
            SELECT  @DDLId = SD.DDLId
            FROM    syDDLS AS SD
            WHERE   SD.DDLName = 'Statuses';
            SELECT  @FldTypeId = SFT.FldTypeId
            FROM    syFieldTypes AS SFT
            WHERE   SFT.FldType = @FldType; 

            IF NOT EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName )
                BEGIN
                    SELECT  @FldId = MAX(SF.FldId) + 1
                    FROM    dbo.syFields AS SF;
                    INSERT  dbo.syFields
                            (
                             FldId
                            ,FldName
                            ,FldTypeId
                            ,FldLen
                            ,DDLId
                            ,DerivedFld
                            ,SchlReq
                            ,LogChanges
                            ,Mask
                            )
                    VALUES  (
                             @FldId
                            ,@FLdName
                            ,@FldTypeId
                            ,@FldLen
                            ,@DDLId
                            ,0
                            ,0
                            ,1
                            ,NULL
                            ); 
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @FldId = SF.FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName;
                            SET @Error = 0;  
--                                PRINT 'Successful syFields ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields ' + @FLdName
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful Field exists  ' + @FLdName
                        --SET @Error = 1
                        --PRINT 'Failed syFields ' + @FLdName
                END;
        END; --  Adding records to syFields for the field "StatusId"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Caption Id 
        BEGIN
                -- ===============================================================================================
                --  Adding records to syFldCaptions for the field "StatusId"
                -- ===============================================================================================
            SET @Caption = 'Status';
            IF NOT EXISTS ( SELECT  1
                            FROM    syFldCaptions AS SFC
                            WHERE   SFC.FldId = @FldId
                                    AND SFC.Caption = @Caption )
                BEGIN
                    SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                    FROM    syFldCaptions AS SFC;
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    INSERT  INTO syFldCaptions
                            (
                             FldCapId
                            ,FldId
                            ,LangId
                            ,Caption
                            ,FldDescrip
                            )
                    VALUES  (
                             @FldCapId
                            ,@FldId
                            ,@LangId
                            ,@Caption
                            ,@Caption
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syFldCaptions' + @Caption
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFldCaptions' + @Caption
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                        --SET @Error = 1
                        --PRINT 'Failed syFldCaptions ' +  @Caption
                END;
        END; --  Adding records to syFldCaptions for the field "StatusId"   -- Ends Here --
    IF ( @Error = 0 )  -- insert Tbl vs Fields Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syTblFlds for the field "StatusId"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    dbo.syTblFlds AS STF
                            WHERE   STF.FldId = @FldId
                                    AND STF.TblId = @TblId )
                BEGIN
                    SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                    FROM    syTblFlds AS STF;
                    INSERT  syTblFlds
                            (
                             TblFldsId
                            ,TblId
                            ,FldId
                            ,CategoryId
                            ,FKColDescrip
                            )
                    VALUES  (
                             @TblFldsId
                            ,@TblId
                            ,@FldId
                            ,NULL
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTblFlds ' + @FLdName
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                END;
        END; --  Adding records to syTblFlds for the field "StatusId"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syResTblflds for the field "StatusId"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    syResTblFlds AS SRTF
                            WHERE   SRTF.TblFldsId = @TblFldsId
                                    AND SRTF.ResourceId = @ResourceId )
                BEGIN
                    SET @Required = 1;  -- Required Yes
                    SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                    FROM    syResTblFlds AS SRTF;
                    INSERT  INTO syResTblFlds
                            (
                             ResDefId
                            ,ResourceId
                            ,TblFldsId
                            ,Required
                            ,SchlReq
                            ,ControlName
                            ,UsePageSetup
                            )
                    VALUES  (
                             @ResDefId
                            ,@ResourceId
                            ,@TblFldsId
                            ,@Required
                            ,0
                            ,NULL
                            ,1
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syResTblFlds ' + @FLdName 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResTblFlds ' + @FLdName 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syResTblFlds ' + @FLdName 
                END;
        END; --  Adding records to syResTblflds for the field "StatusId"  -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syFields, syTblFlds and syResTblflds for the field "ContactTypeDescrip"
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert syFields
        BEGIN 
                -- ===============================================================================================
                --  Adding records to syFields for the field "ContactTypeDescrip"
                -- ===============================================================================================
            SET @FLdName = 'ContactTypeDescrip';
            SET @FldType = 'Varchar';
            SET @FldLen = 12;  
            SELECT  @DDLId = SD.DDLId
            FROM    syDDLS AS SD
            WHERE   SD.DDLName = 'Statuses';
            SELECT  @FldTypeId = SFT.FldTypeId
            FROM    syFieldTypes AS SFT
            WHERE   SFT.FldType = @FldType; 

            IF NOT EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName )
                BEGIN
                    SELECT  @FldId = MAX(SF.FldId) + 1
                    FROM    dbo.syFields AS SF;
                    INSERT  dbo.syFields
                            (
                             FldId
                            ,FldName
                            ,FldTypeId
                            ,FldLen
                            ,DDLId
                            ,DerivedFld
                            ,SchlReq
                            ,LogChanges
                            ,Mask
                            )
                    VALUES  (
                             @FldId
                            ,@FLdName
                            ,@FldTypeId
                            ,@FldLen
                            ,NULL
                            ,0
                            ,0
                            ,1
                            ,NULL
                            ); 
                    IF ( @@ERROR = 0 )
                        BEGIN 
                            SELECT  @DispFldId = FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName; 
                            SET @Error = 0;  
--                                PRINT 'Successful syFields ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields ' + @FLdName
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful Field exists  ' + @FLdName
                        --SET @Error = 1
                        --PRINT 'Failed syFields ' + @FLdName
                END;
        END; --  Adding records to syFields for the field "ContactTypeDescrip"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Caption Id 
        BEGIN
                -- ===============================================================================================
                --  Adding records to syFldCaptions for the field "ContactTypeDescrip"
                -- ===============================================================================================
            SET @Caption = 'Description';
            IF NOT EXISTS ( SELECT  1
                            FROM    syFldCaptions AS SFC
                            WHERE   SFC.FldId = @FldId
                                    AND SFC.Caption = @Caption )
                BEGIN
                    SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                    FROM    syFldCaptions AS SFC;
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    INSERT  INTO syFldCaptions
                            (
                             FldCapId
                            ,FldId
                            ,LangId
                            ,Caption
                            ,FldDescrip
                            )
                    VALUES  (
                             @FldCapId
                            ,@FldId
                            ,@LangId
                            ,@Caption
                            ,@Caption
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syFldCaptions' + @Caption
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFldCaptions' + @Caption
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                        --SET @Error = 1
                        --PRINT 'Failed syFldCaptions ' +  @Caption
                END;
        END; --  Adding records to syFldCaptions for the field "ContactTypeDescrip"   -- Ends Here --
    IF ( @Error = 0 )  -- insert Tbl vs Fields Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syTblFlds for the field "ContactTypeDescrip"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    dbo.syTblFlds AS STF
                            WHERE   STF.FldId = @FldId
                                    AND STF.TblId = @TblId )
                BEGIN
                    SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                    FROM    syTblFlds AS STF;
                    INSERT  syTblFlds
                            (
                             TblFldsId
                            ,TblId
                            ,FldId
                            ,CategoryId
                            ,FKColDescrip
                            )
                    VALUES  (
                             @TblFldsId
                            ,@TblId
                            ,@FldId
                            ,NULL
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTblFlds ' + @FLdName 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 0  syTblFlds ' + @FLdName 
                END;
        END; --  Adding records to syTblFlds for the field "ContactTypeDescrip"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syResTblflds for the field "ContactTypeDescrip"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    syResTblFlds AS SRTF
                            WHERE   SRTF.TblFldsId = @TblFldsId
                                    AND SRTF.ResourceId = @ResourceId )
                BEGIN
                    SET @Required = 1;  -- Required Yes
                    SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                    FROM    syResTblFlds AS SRTF;
                    INSERT  INTO syResTblFlds
                            (
                             ResDefId
                            ,ResourceId
                            ,TblFldsId
                            ,Required
                            ,SchlReq
                            ,ControlName
                            ,UsePageSetup
                            )
                    VALUES  (
                             @ResDefId
                            ,@ResourceId
                            ,@TblFldsId
                            ,@Required
                            ,0
                            ,NULL
                            ,1
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syResTblFlds ' + @FLdName 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResTblFlds ' + @FLdName 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syResTblFlds ' + @FLdName  
                END;
        END; --  Adding records to syResTblflds for the field "ContactTypeDescrip" -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syFields, syTblFlds and syResTblflds for the field "CampGrpId"
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert syFields 
        BEGIN 
                -- ===============================================================================================
                --  Adding records to syFields for the field "CampGrpId"
                -- ===============================================================================================
            SET @FLdName = 'CampGrpId';
            SET @FldType = 'Uniqueidentifier';
            SET @FldLen = 16;
            SELECT  @DDLId = SD.DDLId
            FROM    syDDLS AS SD
            WHERE   SD.DDLName = 'CampGrps';
            SELECT  @FldTypeId = SFT.FldTypeId
            FROM    syFieldTypes AS SFT
            WHERE   SFT.FldType = @FldType; 

            IF NOT EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName )
                BEGIN
                    SELECT  @FldId = MAX(SF.FldId) + 1
                    FROM    dbo.syFields AS SF;
                    INSERT  dbo.syFields
                            (
                             FldId
                            ,FldName
                            ,FldTypeId
                            ,FldLen
                            ,DDLId
                            ,DerivedFld
                            ,SchlReq
                            ,LogChanges
                            ,Mask
                            )
                    VALUES  (
                             @FldId
                            ,@FLdName
                            ,@FldTypeId
                            ,@FldLen
                            ,@DDLId
                            ,0
                            ,0
                            ,1
                            ,NULL
                            ); 
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @FldId = SF.FldId
                            FROM    syFields AS SF
                            WHERE   SF.FldName = @FLdName;
                            SET @Error = 0;  
--                                PRINT 'Successful syFields ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields ' + @FLdName 
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful Field exists  ' + @FLdName
                        --SET @Error = 1
                        --PRINT 'Failed syFields ' + @FLdName
                END;
        END; --  Adding records to syFields for the field "CampGrpId"  -- Ends Here --
    IF ( @Error = 0 )  -- insert Caption Id 
        BEGIN
                -- ===============================================================================================
                --  Adding records to syFldCaptions for the field "CampGrpId"
                -- ===============================================================================================
            SET @Caption = 'Campus Group';
            IF NOT EXISTS ( SELECT  1
                            FROM    syFldCaptions AS SFC
                            WHERE   SFC.FldId = @FldId
                                    AND SFC.Caption = @Caption )
                BEGIN
                    SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                    FROM    syFldCaptions AS SFC;
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    INSERT  INTO syFldCaptions
                            (
                             FldCapId
                            ,FldId
                            ,LangId
                            ,Caption
                            ,FldDescrip
                            )
                    VALUES  (
                             @FldCapId
                            ,@FldId
                            ,@LangId
                            ,@Caption
                            ,@Caption
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
--                                PRINT 'Successful syFldCaptions' + @Caption
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFldCaptions' + @Caption
                        END;
                END; 
            ELSE
                BEGIN
                    SELECT  @FldId = SF.FldId
                    FROM    syFields AS SF
                    WHERE   SF.FldName = @FLdName;
                    SET @Error = 0;
--                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                        --SET @Error = 1
                        --PRINT 'Failed syFldCaptions ' +  @Caption
                END;
        END; --  Adding records to syFldCaptions for the field "CampGrpId"   -- Ends Here -- 
    IF ( @Error = 0 )  -- insert Tbl vs Fields Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syTblFlds for the field "CampGrpId"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    dbo.syTblFlds AS STF
                            WHERE   STF.FldId = @FldId
                                    AND STF.TblId = @TblId )
                BEGIN
                    SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                    FROM    syTblFlds AS STF;
                    INSERT  syTblFlds
                            (
                             TblFldsId
                            ,TblId
                            ,FldId
                            ,CategoryId
                            ,FKColDescrip
                            )
                    VALUES  (
                             @TblFldsId
                            ,@TblId
                            ,@FldId
                            ,NULL
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTblFlds ' + @FLdName
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTblFlds ' + @FLdName
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                END;
        END; --  Adding records to syTblFlds for the field "CampGrpId" -- Ends Here --
    IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
        BEGIN
                -- ===============================================================================================
                --  Adding records to syResTblflds for the field "CampGrpId"
                -- ===============================================================================================
            IF NOT EXISTS ( SELECT  1
                            FROM    syResTblFlds AS SRTF
                            WHERE   SRTF.TblFldsId = @TblFldsId
                                    AND SRTF.ResourceId = @ResourceId )
                BEGIN
                    SET @Required = 1;  -- Required Yes
                    SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                    FROM    syResTblFlds AS SRTF;
                    INSERT  INTO syResTblFlds
                            (
                             ResDefId
                            ,ResourceId
                            ,TblFldsId
                            ,Required
                            ,SchlReq
                            ,ControlName
                            ,UsePageSetup
                            )
                    VALUES  (
                             @ResDefId
                            ,@ResourceId
                            ,@TblFldsId
                            ,@Required
                            ,0
                            ,NULL
                            ,1
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syResTblFlds ' + @FLdName 
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syResTblFlds ' + @FLdName 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01  ' + @FLdName
                END;
        END; --  Adding records to syResTblflds for the field "CampGrpId" -- Ends Here --
        -- ===============================================================================================
        --  Adding records to syDDLs for the "Contact Type"  field
        -- ===============================================================================================
    IF ( @Error = 0 )  -- insert DLL
        BEGIN
            IF NOT EXISTS ( SELECT  1
                            FROM    syDDLS AS SD
                            WHERE   SD.DDLName = @DDLName )
                BEGIN
                    SELECT  @DDLId = MAX(SD.DDLId) + 1
                    FROM    syDDLS AS SD;
                    INSERT  INTO syDDLS
                            (
                             DDLId
                            ,DDLName
                            ,TblId
                            ,DispFldId
                            ,ValFldId
                            ,ResourceId
                            ,CulDependent
                            )
                    VALUES  (
                             @DDLId
                            ,@DDLName
                            ,@TblId
                            ,@DispFldId
                            ,@FldId
                            ,@DDLResourceId
                            ,NULL
                            );
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SELECT  @DDLId = SD.DDLId
                            FROM    syDDLS AS SD
                            WHERE   SD.DDLName = @DDLName;
                            SET @Error = 0;   
--                                PRINT 'Successful syDDLS'
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'failed syDDLS' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syDDLS' 
                END;
        END;  --  Adding records to syDDLs for the "Contact Type"  field  -- Ends Here --
        -- ===============================================================================================
        --  Update syFields (@TblPk_Fld)  with DDLId
        -- ===============================================================================================
    IF ( @Error = 0 )  -- Update Field Id with DDLId
        BEGIN
            IF EXISTS ( SELECT  1
                        FROM    syFields AS SF
                        WHERE   SF.FldId = @TblPk_Fld )
                BEGIN
                    SELECT  @DDLId = SD.DDLId
                    FROM    syDDLS AS SD
                    WHERE   SD.DDLName = @DDLName;
                    UPDATE  SF
                    SET     SF.DDLId = @DDLId
                    FROM    syFields AS SF
                    WHERE   SF.FldId = @TblPk_Fld; 
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syFields DDLId'
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syFields DDLId '
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syFields DDLId' 
                END;
        END;  --  Update syFields with DDLId  -- Ends Here -- 
        -- ===============================================================================================
        --  Update syTables (tblPK with  @TblPk_Fld FldId of ContactTypeID field
        -- ===============================================================================================
    IF ( @Error = 0 )  -- Update Table with Field (Id)
        BEGIN
            IF EXISTS ( SELECT  1
                        FROM    syFields AS SF
                        WHERE   SF.FldId = @FldId )
                BEGIN
                    UPDATE  ST
                    SET     ST.TblPK = @TblPk_Fld  -- with FldId of ContactTypeID field
                    FROM    syTables AS ST
                    WHERE   ST.TblName = @TblName;
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;   
--                                PRINT 'Successful syTables DDLId'
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
--                                PRINT 'Failed syTables DDLId' 
                        END;
                END;
            ELSE
                BEGIN
                    SET @Error = 1;
--                        PRINT 'Failed 01 syTables DDLId' 
                END;
        END;  --  Update syTables (tblPK with  @TblPk_Fld FldId of ContactTypeID field  -- Ends Here -- 

 -- To Test
--  SET @Error = 1
 
    IF ( @Error = 0 )
        BEGIN
            COMMIT TRANSACTION  AddNewFields;
--                PRINT 'Successful COMMIT TRANSACTION'
        END;
    ELSE
        BEGIN
            ROLLBACK TRANSACTION AddNewFields;
--                PRINT 'Failed ROLLBACK TRANSACTION '     
        END;
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION AddNewFields;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
 GO
-- ===============================================================================================
-- END US8289 Crate Contact Types table
-- ===============================================================================================
-- ======================================================================================
-- Notes Admission Page. First Version
-- JAGG
-- ======================================================================================

-- ======================================================================================
-- Fill Catalog syNotesPageFields
-- WARNING The PageFieldId must be the same in all implementations!!!
-- ======================================================================================
 

BEGIN TRANSACTION T2;
BEGIN TRY 

    IF EXISTS ( SELECT  1
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AllNotes]')
                        AND type IN ( N'U',N'PC' ) )
        BEGIN
        -- cheking for Constraints and Forean keys
            IF EXISTS ( SELECT  1
                        FROM    sys.objects AS O
                        WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId]')
                                AND type IN ( N'F' ) )
                BEGIN
                    ALTER TABLE dbo.AllNotes 
                    DROP CONSTRAINT FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId; 
                    --PRINT ' TABLE  AllNotes   DROP CONSTRAINT FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId'
                END;

        END;


    TRUNCATE TABLE syNotesPageFields;


 
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             1  -- PageFieldId - int
            ,'Notes'  -- PageName - varchar(50)
            ,'Note'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             2  -- PageFieldId - int
            ,'Info'  -- PageName - varchar(50)
            ,'Comment'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
 
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             3  -- PageFieldId - int
            ,'Info'  -- PageName - varchar(50)
            ,'Notes'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
 
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             4  -- PageFieldId - int
            ,'Requirements'  -- PageName - varchar(50)
            ,'Override Reason'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             5  -- PageFieldId - int
            ,'Task'  -- PageName - varchar(50)
            ,'Message'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
 
    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             6  -- PageFieldId - int
            ,'Contact'  -- PageName - varchar(50)
            ,'Comment'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );

    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             7  -- PageFieldId - int
            ,'Skill'  -- PageName - varchar(50)
            ,'Comment'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );

    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             8  -- PageFieldId - int
            ,'Extra Curricular'  -- PageName - varchar(50)
            ,'Comment'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );

    INSERT  INTO dbo.syNotesPageFields
            (
             PageFieldId
            ,PageName
            ,FieldCaption
            ,ModUser
            ,ModDate
            )
    VALUES  (
             9  -- PageFieldId - int
            ,'Email'  -- PageName - varchar(50)
            ,'Template'  -- FieldCaption - varchar(50)
            ,'support'  -- ModUser - varchar(50)
            ,GETDATE()  -- ModDate - datetime
            );
 
 
    ALTER TABLE dbo.AllNotes  WITH CHECK ADD  CONSTRAINT FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId FOREIGN KEY(PageFieldId)
    REFERENCES dbo.syNotesPageFields (PageFieldId);


END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION T2;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION T2;
	--PRINT 'TranFixResources COMMITED'
    END; 
 

GO
--SELECT  *
--FROM    dbo.syNotesPageFields
-- **********************************************************************************
-- Enter Role "View Confidential" (16) in sysSysRoles tables US8257
-- this role allow the user to see confidential notes in notes page
-- JAGG
-- **********************************************************************************
IF NOT EXISTS ( SELECT  *
                FROM    sySysRoles
                WHERE   SysRoleId = 16 )
    BEGIN
        INSERT  INTO dbo.sySysRoles
                (
                 SysRoleId
                ,Descrip
                ,StatusId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 16  -- SysRoleId - int
                ,'View Confidential'  -- Descrip - varchar(80)
                ,(
                   SELECT   StatusId
                   FROM     syStatuses
                   WHERE    StatusCode = 'A'
                 )   -- StatusId - uniqueidentifier
                ,'JAGG'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO

-- **********************************************************************************
-- Create School View Confidential Role
-- **********************************************************************************
IF NOT EXISTS ( SELECT  *
                FROM    syRoles
                WHERE   SysRoleId = 16 )
    BEGIN

        INSERT  INTO dbo.syRoles
                (
                 RoleId
                ,Role
                ,Code
                ,StatusId
                ,SysRoleId
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()  -- RoleId - uniqueidentifier
                ,'View Confidential'  -- Role - varchar(60)
                ,'CONFIDENTIAL'  -- Code - varchar(12)
                ,(
                   SELECT   StatusId
                   FROM     syStatuses
                   WHERE    StatusCode = 'A'
                 )   -- StatusId - uniqueidentifier
                ,16  -- SysRoleId - int
                ,GETDATE()  -- ModDate - datetime
                ,'JAGG'  -- ModUser - varchar(50)
                );
    END;
GO

-- **********************************************************************************
-- Register the Advantage User View Confidential in Admission Module
-- **********************************************************************************
DELETE  dbo.syRolesModules
WHERE   RoleId IN ( SELECT  RoleId
                    FROM    dbo.syRoles
                    WHERE   SysRoleId = 16
                            AND Code = 'CONFIDENTIAL' );
GO

INSERT  INTO dbo.syRolesModules
        (
         RoleModuleId
        ,RoleId
        ,ModuleId
        )
VALUES  (
         NEWID()  -- RoleModuleId - uniqueidentifier
        ,(
           SELECT   RoleId
           FROM     dbo.syRoles
           WHERE    Code = 'CONFIDENTIAL'
                    AND SysRoleId = 16
         ) -- RoleId - uniqueidentifier
        ,189  -- ModuleId - smallint - Admission
        );

GO

-- ======================================================================================
--Notes pages JAGG finished
-- ======================================================================================


-----------------------------------------------------------------------
-- BEGIN US8517 DATA CHANGES
-----------------------------------------------------------------------
-- ********************************************************************************************************
-- US8517: Insert Resource of Other Contacts Page
-- Author: hcordero
-- ********************************************************************************************************

IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 831 )
    BEGIN
 
        DECLARE @ResID INTEGER = 831; -- put here your resource ID for the page
        DECLARE @PageResourceName VARCHAR(20) = 'Other Contacts'; -- name of your page
        DECLARE @PageUrl VARCHAR(100) = '~/AD/leadContacts.aspx'; -- the url of page in advantage
        DECLARE @MRUType INTEGER = 1; -- NULL:None (page no referred to specific individual entity)
                             -- 1: Student 
                             -- 2:?, 3:?
                             -- 4: Admission
        DECLARE @PageDisplayName VARCHAR(20) = 'Other Contacts'; -- Put the display name for your page
 
        INSERT  syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 @ResID
                ,@PageResourceName
                ,3
                ,@PageUrl
                ,NULL
                ,NULL
                ,GETDATE()
                ,'support'
                ,0
                ,@MRUType
                ,975
                ,NULL
                ,@PageDisplayName
                );
    END;
GO
-----------------------------------------------------------------------
-- END US8517 DATA CHANGES
-----------------------------------------------------------------------




-----------------------------------------------------------------------
-- START Fix to Menu Items of 1098-T Processing and Outcome Measures
-----------------------------------------------------------------------
BEGIN TRANSACTION TranFixResources;
BEGIN TRY
    DECLARE @ResourceId_1098TServices INT;
    SET @ResourceId_1098TServices = 827;

    DECLARE @DATE DATETIME;
    SET @DATE = GETDATE();

    UPDATE  syMenuItems
    SET     syMenuItems.ResourceId = @ResourceId_1098TServices
           ,ModDate = @DATE
           ,ModUser = 'support'
    FROM    syMenuItems syMenuItems
    WHERE   ResourceId = 828
            AND syMenuItems.MenuName = '1098-T Processing';

    DECLARE @ResourceId_OutComeMeasures INT;
    SET @ResourceId_OutComeMeasures = 828;

    IF NOT EXISTS ( SELECT  *
                    FROM    syMenuItems
                    WHERE   MenuName = 'Outcome Measures' )
        BEGIN
            DECLARE @MenuItemId INT
               ,@MenuParentId INT;
            SET @MenuParentId = (
                                  SELECT TOP 1
                                            MenuItemId
                                  FROM      syMenuItems
                                  WHERE     MenuName = 'Winter Student FinAid'
                                );
            INSERT  INTO syMenuItems
            VALUES  ( 'Outcome Measures','Outcome Measures','/sy/ParamReport.aspx',4,@MenuParentId,500,0,GETDATE(),NULL,1,@ResourceId_OutComeMeasures,NULL,NULL,
                      NULL,NULL );
        END;

END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION TranFixResources;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
  
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION TranFixResources;
	--PRINT 'TranFixResources COMMITED'
    END;
GO

-----------------------------------------------------------------------
-- END Fix to Menu Items of 1098-T Processing and Outcome Measures
-----------------------------------------------------------------------
-- *****************************************************************************
-- DE12894 :   Page change name form Extra Curricular to Extracurricular
-- JAGG
-- *****************************************************************************
UPDATE  syNotesPageFields
SET     PageName = 'Extracurricular'
WHERE   PageName = 'Extra Curricular';
-----------------------------------------------------------------------
-- END JAGG - DE12894 :   Page change name form Extra Curricular to Extracurricular
-----------------------------------------------------------------------


-- ===============================================================================================
-- US7547 Reason Not Enrolled Configuration 
-- JTorres 2016-07-12
-- ===============================================================================================
GO
SET ANSI_NULLS ON; 
GO
SET QUOTED_IDENTIFIER ON; 
GO
SET ANSI_PADDING ON; 
GO

BEGIN
-- ===============================================================================================
-- Adding Resource ID 835 "syReasonNotEnrolled"  
-- ===============================================================================================

-- Declare variables
    BEGIN
    -- general fields
        DECLARE @Error AS INTEGER;  
        DECLARE @DisplayName AS NVARCHAR(250);  
        DECLARE @ModDate AS DATETIME; 
        DECLARE @ModUser AS NVARCHAR(50);  

    -- syReasonNotEnrolled 
        DECLARE @ReasonNotEnrolledId AS UNIQUEIDENTIFIER; 
        DECLARE @Code AS NVARCHAR(50); 
        DECLARE @Description AS NVARCHAR(250); 
        DECLARE @StatusId AS UNIQUEIDENTIFIER; 
        DECLARE @CampGrpId AS UNIQUEIDENTIFIER; 
    -- to field    syReasonNotEnrolled 
        DECLARE @ActiveStatus AS UNIQUEIDENTIFIER; 
        DECLARE @AllCampGrpId AS UNIQUEIDENTIFIER; 

    -- Resource Id
        DECLARE @ResourceId AS INT; 
        DECLARE @Resource AS NVARCHAR(200); 
        DECLARE @ResourceTypeID AS INT; 
        DECLARE @ResourceURL AS NVARCHAR(100); 
        DECLARE @AllowSchReqFlds AS BIT; 
        DECLARE @MRUTypeId AS SMALLINT; 
        DECLARE @UsedIn AS INT; 
        DECLARE @TblFldsId AS INT; 

    -- MenuItem      
        DECLARE @PPMenuName AS NVARCHAR(250); 
        DECLARE @PMenuName AS NVARCHAR(250); 
        DECLARE @mMenuName AS NVARCHAR(250); 
        DECLARE @MenuParent AS INT; 
        DECLARE @MenuItemId AS INT; 
        DECLARE @MenuName AS NVARCHAR(250); 
        DECLARE @Url AS NVARCHAR(250); 
        DECLARE @MenuItemTypeId AS SMALLINT; 
        DECLARE @DisplayOrder AS INT; 
        DECLARE @IsPopup AS BIT; 
        DECLARE @IsActive AS BIT; 

    --syNavigationNodes
        DECLARE @HierarchyId AS UNIQUEIDENTIFIER; 
        DECLARE @ParentId AS UNIQUEIDENTIFIER; 
        DECLARE @HierarchyIndex AS SMALLINT; 
        DECLARE @IsShipped AS SMALLINT; 

    -- syRlsResLvls
        DECLARE @AccessLevel AS INT; 
        DECLARE @RoleId AS UNIQUEIDENTIFIER; 
        DECLARE @Code_RRL AS NVARCHAR(12); 

    -- Tbl
        DECLARE @TblId AS INTEGER; 
        DECLARE @TblName AS NVARCHAR(50); 
        DECLARE @TblDescrip AS NVARCHAR(100); 
        DECLARE @TblPk_Fld AS INTEGER; 

    -- syFields
        DECLARE @FldId AS INTEGER;    
        DECLARE @FLdName AS NVARCHAR(200); 
        DECLARE @FldTypeId AS INTEGER; 
        DECLARE @FldType AS NVARCHAR(20); 
        DECLARE @FldLen AS INTEGER; 
        DECLARE @DDLName_In_syFields AS NVARCHAR(100);
        DECLARE @DDLId_In_syFields AS INTEGER;

    -- syTblFlds
    --DECLARE @TblNamexField      AS NVARCHAR(50)
    --DECLARE @TblIdxField        AS INTEGER
    --DECLARE @FKColDescrip       AS NVARCHAR(150)

    -- syFldCaptions
        DECLARE @FldCapId AS INTEGER; 
        DECLARE @Caption AS NVARCHAR(100); 
        DECLARE @LangId AS INTEGER; 
        DECLARE @LangName AS NVARCHAR(5); 

    --syResTblFlds
        DECLARE @ResDefId AS INTEGER; 
        DECLARE @Required AS BIT; 

    --syDDLS
        DECLARE @DispFldId AS INTEGER;  
        DECLARE @ValFldId AS INTEGER; 
        DECLARE @DDLId AS INTEGER; 
        DECLARE @DDLName AS NVARCHAR(100); 
        DECLARE @DDLResourceId AS INTEGER; 

    --DECLARE @DispFldTypeId      AS INTEGER
    --DECLARE @DispFldType        AS NVARCHAR(20)
    --DECLARE @DispFldLen         AS INTEGER
    --DECLARE @DispFldCapId       AS INTEGER
    --DECLARE @DispCaption        AS NVARCHAR(100)
    --DECLARE @DispTblFldsId      AS INTEGER
    --DECLARE @DispResDefId       AS INTEGER
    --DECLARE @DispResourceId     AS INTEGER
    --DECLARE @DispResourceURL    AS NVARCHAR(100)
    --
    --DECLARE @LangName           AS NVARCHAR(5)
    --DECLARE @DDLTblName         AS NVARCHAR(50)
    --DECLARE @DDLTblId           AS INTEGER

    END;  -- END  -- Declare variables

-- Set initials Values 
    BEGIN
    -- general fields
        SET @Error = 0; 
        SET @DisplayName = 'Reason Not Enrolled';  
        SET @ModDate = GETDATE(); 
        SET @ModUser = 'support'; 

    -- syContactTypes 
        SELECT  @CampGrpId = SCG.CampGrpId
        FROM    syCampGrps AS SCG
        WHERE   SCG.CampGrpCode = 'All'; 
        SELECT  @StatusId = SS.StatusId
        FROM    syStatuses AS SS
        WHERE   SS.Status = 'Active'; 

    -- Resource Id
        SET @ResourceId = 835;    -- Hard coded  -- standard for all DB's (Clients)
        SET @Resource = 'ReasonNotEnrolled'; 
        SET @ResourceTypeID = 4;     -- Maintenance --  SELECT * FROM syResourceTypes AS SRT
        SET @ResourceURL = '~/sy/ReasonNotEnrolleds.aspx'; 
        SET @AllowSchReqFlds = 0; 
        SET @MRUTypeId = 4;  --  Leads        -- SELECT * FROM syMRUTypes AS SMT
        SET @UsedIn = 975;  -- page resource where is used (this is Report page)
        SET @TblFldsId = NULL; 

    -- MenuItem 
        SET @PPMenuName = 'Maintenance'; 
        SET @PMenuName = 'System'; 
        SET @mMenuName = 'General Options'; 
        SET @MenuName = @DisplayName; 
        SET @Url = '/SY/MaintenanceBase.aspx'; 
        SET @MenuItemTypeId = 4;     -- Page  -- SELECT * FROM syMenuItemType AS SMIT
        SET @DisplayOrder = 350;   -- After "Relationship Types"  SELECT * FROM syMenuItems AS SMI WHERE SMI.DisplayName = 'General Options'  ORDER BY SMI.ParentId, SMI.DisplayOrder
        SET @IsPopup = 0;     -- it is not popup menu
        SET @IsActive = 1;     --  Yes, it is active
 
    -- syNavigationNodes
        SET @HierarchyIndex = 8;     -- Hierarchy Index???? 
        SET @IsShipped = 1; 

    -- syRlsResLvls
        SET @AccessLevel = 15;    -- sysadmins role with full access (15)
        SET @Code_RRL = 'SYSADMINS';  -- SELECT SR.* FROM syRoles AS SR 
        SET @RoleId = (
                        SELECT TOP 1
                                SR.RoleId
                        FROM    syRoles AS SR
                        WHERE   SR.Code = @Code_RRL
                      ); 

    -- Tbl
        SET @TblName = 'syReasonNotEnrolled'; 
        SET @TblDescrip = 'Lead ReasonNotEnrolled Lookup Table.'; 

    -- syFldCaptions
        SET @LangName = 'EN-US'; 
        SELECT  @LangId = SL.LangId
        FROM    syLangs AS SL
        WHERE   SL.LangName = @LangName; 

    --syDDLS
        SET @DDLName = 'Reason Not Enrolled';    -- we need a DDL for Description
        SET @DDLResourceId = @ResourceId;
    END;   --  END set initial values

    BEGIN TRANSACTION AddNewFields; 
    BEGIN TRY  
        -- ===============================================================================================
        -- Adding default records to  syReasonNotEnrolled ('AddressChanged', '  'Other') 
        -- ===============================================================================================
--        IF (@Error = 0)  -- Insert 'AddressChanged'
--            BEGIN
--                SET @Code     = 'AddressChanged' 
--                SET @Description  = 'Lead has changed of Adderss'
--                SELECT  @ActiveStatus = SS.StatusId FROM syStatuses AS SS WHERE SS.StatusCode =  'A'  -- Status = 'Active'
--                SELECT  @AllCampGrpId = SCG.CampGrpId FROM syCampGrps AS SCG WHERE SCG.CampGrpCode = 'All' 
--                IF NOT EXISTS ( SELECT  1
--                                FROM    syReasonNotEnrolled AS SRNE
--                                WHERE   SRNE.Code <> @Code )
--                    BEGIN
--                        -- Insert record in new table

--                        INSERT syReasonNotEnrolled
--                                (
--                                 ReasonNotEnrolledId
--                               , Code
--                               , Description
--                               , StatusId
--                               , CampGrpId
--                               , ModUser
--                               , ModDate
--                                )
--                        VALUES  (
--                                 NEWID()        -- ReasonNotEnrolledId - uniqueidentifier
--                               , @Code          -- Code - varchar(50)
--                               , @Description   -- Description - varchar(250)
--                               , @ActiveStatus  -- StatusId - uniqueidentifier
--                               , @AllCampGrpId  -- CampGrpId - uniqueidentifier -- ALL
--                               , @ModUser       -- ModUser - varchar(50)
--                               , @ModDate       -- ModDate - datetime
--                                )
--                        IF (@@ERROR = 0)
--                            BEGIN
--                                SET @Error = 0  
----                              PRINT 'Successful syReasonNotEnrolled ' + @Code
--                            END
--                        ELSE
--                            BEGIN
--                                SET @Error = 1
----                                PRINT 'Failed syReasonNotEnrolled ' + @Code
--                            END
--                    END 
--                ELSE 
--                    BEGIN
--                        SET @Error = 1
----                        PRINT 'Failed 01 syReasonNotEnrolled ' + @Code
--                    END
--            END -- -- Insert @Code = 'AddressChanged'

         -- ===============================================================================================
        -- Adding Resource ID 835 "ReasonNotEnrolled"  
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Insert ResourceID
            BEGIN
                IF NOT EXISTS ( SELECT  SR.ResourceID
                                FROM    syResources AS SR
                                WHERE   SR.ResourceID = @ResourceId )
                    BEGIN
                        INSERT  INTO dbo.syResources
                                (
                                 ResourceID
                                ,Resource
                                ,ResourceTypeID
                                ,ResourceURL
                                ,SummListId
                                ,ChildTypeId
                                ,ModDate
                                ,ModUser
                                ,AllowSchlReqFlds
                                ,MRUTypeId
                                ,UsedIn
                                ,TblFldsId
                                ,DisplayName
                                )
                        VALUES  (
                                 @ResourceId
                                ,@Resource
                                ,@ResourceTypeID
                                ,@ResourceURL
                                ,NULL
                                ,NULL
                                ,@ModDate
                                ,@ModUser
                                ,@AllowSchReqFlds
                                ,@MRUTypeId
                                ,@UsedIn
                                ,@TblFldsId
                                ,@DisplayName
                                ); 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful syResources' 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syResources' 
                            END; 
                    END;  
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 syResources' 
                    END; 
            END;  -- Adding Resource ID 835 "ReasonNotEnrolled"  -- Ends Here --

        -- ===============================================================================================
        --  Adding MenuItem  "ReasonNotEnrolled"  
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- insert Menu item
            BEGIN
                SET @MenuParent = (
                                    SELECT TOP 1
                                            SMI.MenuItemId
                                    FROM    syMenuItems AS SMI
                                    INNER JOIN syMenuItems p ON SMI.ParentId = p.MenuItemId
                                    INNER JOIN syMenuItems pp ON p.ParentId = pp.MenuItemId
                                    WHERE   SMI.MenuName = @mMenuName
                                            AND p.MenuName = @PMenuName
                                            AND pp.MenuName = @PPMenuName
                                  ); 

                IF @MenuParent IS NOT NULL
                    BEGIN
                        IF NOT EXISTS ( SELECT  1
                                        FROM    syMenuItems AS SMI
                                        WHERE   SMI.MenuName = @MenuName
                                                AND SMI.Url = @Url
                                                AND SMI.ParentId = @MenuParent )
                            BEGIN
                                INSERT  INTO dbo.syMenuItems
                                        (
                                         MenuName
                                        ,DisplayName
                                        ,Url
                                        ,MenuItemTypeId
                                        ,ParentId
                                        ,DisplayOrder
                                        ,IsPopup
                                        ,ModDate
                                        ,ModUser
                                        ,IsActive
                                        ,ResourceId
                                        ,HierarchyId
                                        ,ModuleCode
                                        ,MRUType
                                        ,HideStatusBar
                                        )
                                VALUES  (
                                         @MenuName
                                        ,@DisplayName
                                        ,@Url
                                        ,@MenuItemTypeId
                                        ,@MenuParent
                                        ,@DisplayOrder
                                        ,@IsPopup
                                        ,@ModDate
                                        ,@ModUser
                                        ,1
                                        ,@ResourceId
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ); 
                                IF ( @@ERROR = 0 )
                                    BEGIN
                                        SELECT  @MenuItemId = SMI.MenuItemId
                                        FROM    syMenuItems AS SMI
                                        WHERE   SMI.MenuName = @MenuName
                                                AND SMI.Url = @Url
                                                AND SMI.ParentId = @MenuParent; 
                                        SET @Error = 0;   
--                                        PRINT 'Successful syMenuItems' 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
--                                        PRINT 'Failed syMenuItems' 
                                    END; 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed 01 syMenuItems' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed Exists syMenuItems' 
                    END;                      
                 
            END;  -- Adding MenuItem   "ReasonNotEnrolled"   -- Ends Here --

        -- ===============================================================================================
        --  Fill syNavigationNodes     --Add navigation node
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- syNavigationNodes
            BEGIN
                --Add navigation node
                SELECT  @ParentId = SNN.ParentId
                FROM    syNavigationNodes AS SNN
                WHERE   SNN.ResourceId = (
                                           SELECT   MIN(SMI.ResourceId)
                                           FROM     syMenuItems AS SMI
                                           WHERE    SMI.ParentId = @MenuParent
                                         ); 
                IF NOT EXISTS ( SELECT  1
                                FROM    syNavigationNodes AS SNN
                                WHERE   SNN.ResourceId = @ResourceId
                                        AND SNN.ParentId = @ParentId )
                    BEGIN
                        INSERT  INTO syNavigationNodes
                                (
                                 HierarchyIndex
                                ,ResourceId
                                ,ParentId
                                ,ModDate
                                ,ModUser
                                ,IsPopupWindow
                                ,IsShipped
                                )
                        VALUES  (
                                 @HierarchyIndex
                                ,@ResourceId
                                ,@ParentId
                                ,@ModDate
                                ,@ModUser
                                ,@IsPopup
                                ,@IsShipped
                                ); 
                    
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SELECT  @HierarchyId = SNN.HierarchyId
                                FROM    syNavigationNodes AS SNN
                                WHERE   SNN.ResourceId = @ResourceId
                                        AND SNN.ParentId = @ParentId; 
                                SET @Error = 0;   
--                                PRINT 'Successful syNavigationNodes' 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syNavigationNodes' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed Exists syNavigationNodes' 
                    END; 
            END;   --Add navigation node  -- Ends Here --

        -- ===============================================================================================
        --  Update MenuItem   "ReasonNotEnrolled"   (with HierarchyId)
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Update Table with Field (Id)
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    syMenuItems AS SMI
                            WHERE   SMI.MenuItemId = @MenuItemId )
                    BEGIN
                        UPDATE  SMI
                        SET     SMI.HierarchyId = @HierarchyId
                        FROM    syMenuItems AS SMI
                        WHERE   SMI.MenuItemId = @MenuItemId; 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;    
--                                PRINT 'Successful Updates  syMenuItems with HierarchyId'
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed Updates  syMenuItems with HierarchyId' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 Updates  syMenuItems with HierarchyId' 
                    END; 
            END;   -- Update MenuItem   "ReasonNotEnrolled"   (with HierarchyId) -- Ends Here -- 

        -- ===============================================================================================
        --  Fill syRlsResLvls    --Link new resource to sysadmins role with full access (15)
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- syRlsResLvls  Link new resource to sysadmins role 
            BEGIN
                ----Link new resource to sysadmins role with full access (15)
                IF NOT EXISTS ( SELECT  1
                                FROM    syRlsResLvls
                                WHERE   ResourceID = @ResourceId
                                        AND AccessLevel = @AccessLevel )
                    BEGIN
                        INSERT  INTO syRlsResLvls
                                (
                                 RRLId
                                ,RoleId
                                ,ResourceID
                                ,AccessLevel
                                ,ModDate
                                ,ModUser
                                )
                        VALUES  (
                                 NEWID()
                                ,@RoleId
                                ,@ResourceId
                                ,@AccessLevel
                                ,@ModDate
                                ,@ModUser
                                ); 	
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful syRlsResLvls' 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syRlsResLvls' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed Exists syRlsResLvls' 
                    END; 
            END;    -- syRlsResLvls  Link new resource to sysadmins role -- Ends Here --

        -- ===============================================================================================
        --  Adding table record to syTables table
        -- ===============================================================================================
        IF ( @Error = 0 )  -- insert record of Table  
            BEGIN   
                IF NOT EXISTS ( SELECT  1
                                FROM    syTables AS ST
                                WHERE   ST.TblName = @TblName )
                    BEGIN
                        SELECT  @TblId = MAX(ST.TblId) + 1
                        FROM    syTables AS ST; 
                        INSERT  INTO syTables
                                ( TblId,TblName,TblDescrip,TblPK )
                        VALUES  ( @TblId,@TblName,@TblDescrip,0 );   -- TblPK should be updated after create syFields with @TblPk_Fld  FldId of ReasonNotEnrolledId field
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful syTables' 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syTables' 
                            END; 
                    END;  
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 syTables' 
                    END; 
            END;  --  Adding table record to syTables table -- Ends Here --

        -- ===============================================================================================
        --  Adding records to syFields, syFldCaption, syTblFlds and syResTblflds for each field we want 
        --  displaty in table
        -- ===============================================================================================
        IF ( @Error = 0 )  -- insert syFields 
            BEGIN 

                -- ===============================================================================================
                --  Adding records to syFields, syFldCaption, syTblFlds and syResTblflds for the field "ReasonNotEnrolledId"
                -- ===============================================================================================
                IF ( @Error = 0 )  -- insert syFields 
                    BEGIN       
                        SET @FLdName = 'ReasonNotEnrolledId'; 
                        SET @FldType = 'Uniqueidentifier';  
                        SET @FldLen = 16; 
                        SET @Caption = 'Reason Not Enrolled Id'; 
                        SET @Required = 1;   -- Required Yes
                        SET @DDLName_In_syFields = NULL;
                        SET @DDLId_In_syFields = NULL;
                        IF ( @Error = 0 )  -- insert syFields 
                            BEGIN 
                                -- ===============================================================================================
                                --  Adding records to syFields for the field "ReasonNotEnrolledId"
                                -- ===============================================================================================
                                SELECT  @FldTypeId = SFT.FldTypeId
                                FROM    syFieldTypes AS SFT
                                WHERE   SFT.FldType = @FldType;  
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName )
                                    BEGIN
                                        SELECT  @FldId = MAX(SF.FldId) + 1
                                        FROM    dbo.syFields AS SF; 
                                        INSERT  dbo.syFields
                                                (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                )
                                        VALUES  (
                                                 @FldId
                                                ,@FLdName
                                                ,@FldTypeId
                                                ,@FldLen
                                                ,@DDLId_In_syFields
                                                ,0
                                                ,0
                                                ,1
                                                ,NULL
                                                );  
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SELECT  @ValFldId = SF.FldId                  -- value go in DDL display field   (ReasonNotEnrolledId)
                                                       ,@FldId = SF.FldId
                                                       ,@TblPk_Fld = SF.FldId
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName;  
                                                SET @Error = 0;   
--                                                PRINT 'Successful syFields ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
--                                              PRINT 'Failed syFields ' + @FLdName
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @ValFldId = SF.FldId                           -- value go in DDL display field   (ReasonNotEnrolledId)
                                               ,@FldId = SF.FldId
                                               ,@TblPk_Fld = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName;  
                                        SET @Error = 0; 
--                                        PRINT 'Successful Field exists  ' + @FLdName
--  SET @Error = 1
--  PRINT 'Failed syFields ' + @FLdName
                                    END; 
                            END;  --  Adding records to syFields for the field "ReasonNotEnrolledId"   -- Ends Here --
                        IF ( @Error = 0 )  -- insert Caption Id 
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syFldCaptions for the field "ReasonNotEnrolledId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFldCaptions AS SFC
                                                WHERE   SFC.FldId = @FldId
                                                        AND SFC.Caption = @Caption )
                                    BEGIN
                                        SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                                        FROM    syFldCaptions AS SFC; 
                                        INSERT  INTO syFldCaptions
                                                (
                                                 FldCapId
                                                ,FldId
                                                ,LangId
                                                ,Caption
                                                ,FldDescrip
                                                )
                                        VALUES  (
                                                 @FldCapId
                                                ,@FldId
                                                ,@LangId
                                                ,@Caption
                                                ,@Caption
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFldCaptions' + @Caption
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFldCaptions' + @Caption
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SET @Error = 0; 
                --                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                                        --SET @Error = 1
                                        --PRINT 'Failed syFldCaptions ' +  @Caption
                                    END; 
                            END;  --  Adding records to syFldCaptions for the field "ReasonNotEnrolledId"   -- Ends Here --
                        IF ( @Error = 0 )  -- insert Tbl vs Fields Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syTblFlds for the field "ReasonNotEnrolledId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    dbo.syTblFlds AS STF
                                                WHERE   STF.FldId = @FldId
                                                        AND STF.TblId = @TblId )
                                    BEGIN
                                        SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                                        FROM    syTblFlds AS STF; 
                                        INSERT  syTblFlds
                                                (
                                                 TblFldsId
                                                ,TblId
                                                ,FldId
                                                ,CategoryId
                                                ,FKColDescrip
                                                )
                                        VALUES  (
                                                 @TblFldsId
                                                ,@TblId
                                                ,@FldId
                                                ,NULL
                                                ,NULL
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syTblFlds ' + @FLdName
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                                    END; 
                            END;  --  Adding records to syTblFlds for the field "ReasonNotEnrolledId" -- Ends Here --
                        IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syResTblflds for the field "ReasonNotEnrolledId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syResTblFlds AS SRTF
                                                WHERE   SRTF.TblFldsId = @TblFldsId
                                                        AND SRTF.ResourceId = @ResourceId )
                                    BEGIN
                                        SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                                        FROM    syResTblFlds AS SRTF; 
                                        INSERT  INTO syResTblFlds
                                                (
                                                 ResDefId
                                                ,ResourceId
                                                ,TblFldsId
                                                ,Required
                                                ,SchlReq
                                                ,ControlName
                                                ,UsePageSetup
                                                )
                                        VALUES  (
                                                 @ResDefId
                                                ,@ResourceId
                                                ,@TblFldsId
                                                ,@Required
                                                ,0
                                                ,NULL
                                                ,1
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syResTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syResTblFlds ' + @FLdName 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syResTblFlds ' + @FLdName
                                    END; 
                            END;  --  Adding records to syResTblflds for the field "ReasonNotEnrolledId" -- Ends Here --
                    END;  --  Adding records to syFields, syFldCaption, syTblFlds and syResTblflds for the field "ReasonNotEnrolledId"   -- Ends Here --

                -- ===============================================================================================
                --  Adding records to syFields, syTblFlds and syResTblflds for the field "Code"
                -- ===============================================================================================
                IF ( @Error = 0 )  -- insertsyFields
                    BEGIN
                        SET @FLdName = 'Code'; 
                        SET @FldType = 'Varchar'; 
                        SET @FldLen = 12;       
                        SET @Caption = 'Code';   
                        SET @Required = 1;   -- Required Yes 
                        SET @DDLName_In_syFields = NULL;
                        SET @DDLId_In_syFields = NULL;          
                        IF ( @Error = 0 )  -- insert syFields 
                            BEGIN 
                                -- ===============================================================================================
                                --  Adding records to syFields for the field "Code"
                                -- ===============================================================================================
                                SELECT  @FldTypeId = SFT.FldTypeId
                                FROM    syFieldTypes AS SFT
                                WHERE   SFT.FldType = @FldType;  
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName )
                                    BEGIN
                                        SELECT  @FldId = MAX(SF.FldId) + 1
                                        FROM    dbo.syFields AS SF; 
                                        INSERT  dbo.syFields
                                                (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                )
                                        VALUES  (
                                                 @FldId
                                                ,@FLdName
                                                ,@FldTypeId
                                                ,@FldLen
                                                ,@DDLId_In_syFields
                                                ,0
                                                ,0
                                                ,1
                                                ,NULL
                                                );  
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SELECT  @FldId = SF.FldId
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName; 
                                                SET @Error = 0;  
                --                                PRINT 'Successful syFields ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFields ' + @FLdName
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful Field exists  ' + @FLdName
                                        --SET @Error = 1
                                        --PRINT 'Failed syFields ' + @FLdName
                                    END; 
                            END;  --  Adding records to syFields for the field "Code" -- Ends Here --
                        IF ( @Error = 0 )  -- insert Caption Id 
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syFldCaptions for the field "Code"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFldCaptions AS SFC
                                                WHERE   SFC.FldId = @FldId
                                                        AND SFC.Caption = @Caption )
                                    BEGIN
                                        SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                                        FROM    syFldCaptions AS SFC; 
                                        INSERT  INTO syFldCaptions
                                                (
                                                 FldCapId
                                                ,FldId
                                                ,LangId
                                                ,Caption
                                                ,FldDescrip
                                                )
                                        VALUES  (
                                                 @FldCapId
                                                ,@FldId
                                                ,@LangId
                                                ,@Caption
                                                ,@Caption
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFldCaptions' + @Caption
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFldCaptions' + @Caption
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SET @Error = 0; 
                --                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                                        --SET @Error = 1
                                        --PRINT 'Failed syFldCaptions ' +  @Caption
                                    END; 
                            END;  --  Adding records to syFldCaptions for the field "Code"   -- Ends Here --
                        IF ( @Error = 0 )  -- insert Tbl vs Fields Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syTblFlds for the field "Code"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    dbo.syTblFlds AS STF
                                                WHERE   STF.FldId = @FldId
                                                        AND STF.TblId = @TblId )
                                    BEGIN
                                        SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                                        FROM    syTblFlds AS STF; 
                                        INSERT  syTblFlds
                                                (
                                                 TblFldsId
                                                ,TblId
                                                ,FldId
                                                ,CategoryId
                                                ,FKColDescrip
                                                )
                                        VALUES  (
                                                 @TblFldsId
                                                ,@TblId
                                                ,@FldId
                                                ,NULL
                                                ,NULL
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syTblFlds ' + @FLdName
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syTblFlds ' + @FLdName
                                    END; 
                            END;  --  Adding records to syTblFlds for the field "Code"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syResTblflds for the field "Code"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syResTblFlds AS SRTF
                                                WHERE   SRTF.TblFldsId = @TblFldsId
                                                        AND SRTF.ResourceId = @ResourceId )
                                    BEGIN
                                        SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                                        FROM    syResTblFlds AS SRTF; 
                                        INSERT  INTO syResTblFlds
                                                (
                                                 ResDefId
                                                ,ResourceId
                                                ,TblFldsId
                                                ,Required
                                                ,SchlReq
                                                ,ControlName
                                                ,UsePageSetup
                                                )
                                        VALUES  (
                                                 @ResDefId
                                                ,@ResourceId
                                                ,@TblFldsId
                                                ,@Required
                                                ,0
                                                ,NULL
                                                ,1
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syResTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syResTblFlds ' + @FLdName
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syResTblFlds ' + @FLdName
                                    END; 
                            END;  --  Adding records to syResTblflds for the field "Code"  -- Ends Here --
                    END;  --  Adding records to syFields, syTblFlds and syResTblflds for the field "Code"

                -- ===============================================================================================
                --  Adding records to syFields, syTblFlds and syResTblflds for the field "StatusId"
                -- ===============================================================================================
                IF ( @Error = 0 )  -- insert syFields
                    BEGIN 
                        SET @FLdName = 'StatusId'; 
                        SET @FldType = 'Uniqueidentifier'; 
                        SET @FldLen = 16;                              
                        SET @Caption = 'Status';  
                        SET @Required = 1;   -- Required Yes
                        SET @DDLName_In_syFields = 'Statuses';
                        SELECT  @DDLId_In_syFields = SD.DDLId
                        FROM    syDDLS AS SD
                        WHERE   SD.DDLName = @DDLName_In_syFields;    
                        IF ( @Error = 0 )  -- insert syFields 
                            BEGIN 
                                -- ===============================================================================================
                                --  Adding records to syFields for the field "StatusId"
                                -- ===============================================================================================
                                SELECT  @FldTypeId = SFT.FldTypeId
                                FROM    syFieldTypes AS SFT
                                WHERE   SFT.FldType = @FldType;  
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName )
                                    BEGIN
                                        SELECT  @FldId = MAX(SF.FldId) + 1
                                        FROM    dbo.syFields AS SF; 
                                        INSERT  dbo.syFields
                                                (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                )
                                        VALUES  (
                                                 @FldId
                                                ,@FLdName
                                                ,@FldTypeId
                                                ,@FldLen
                                                ,@DDLId_In_syFields
                                                ,0
                                                ,0
                                                ,1
                                                ,NULL
                                                );  
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SELECT  @FldId = SF.FldId
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName; 
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFields ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFields ' + @FLdName
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful Field exists  ' + @FLdName
                                        --SET @Error = 1
                                        --PRINT 'Failed syFields ' + @FLdName
                                    END; 
                            END;  --  Adding records to syFields for the field "StatusId"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Caption Id 
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syFldCaptions for the field "StatusId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFldCaptions AS SFC
                                                WHERE   SFC.FldId = @FldId
                                                        AND SFC.Caption = @Caption )
                                    BEGIN
                                        SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                                        FROM    syFldCaptions AS SFC; 
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        INSERT  INTO syFldCaptions
                                                (
                                                 FldCapId
                                                ,FldId
                                                ,LangId
                                                ,Caption
                                                ,FldDescrip
                                                )
                                        VALUES  (
                                                 @FldCapId
                                                ,@FldId
                                                ,@LangId
                                                ,@Caption
                                                ,@Caption
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFldCaptions' + @Caption
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFldCaptions' + @Caption
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                                        --SET @Error = 1
                                        --PRINT 'Failed syFldCaptions ' +  @Caption
                                    END; 
                            END;  --  Adding records to syFldCaptions for the field "StatusId"   -- Ends Here --
                        IF ( @Error = 0 )  -- insert Tbl vs Fields Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syTblFlds for the field "StatusId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    dbo.syTblFlds AS STF
                                                WHERE   STF.FldId = @FldId
                                                        AND STF.TblId = @TblId )
                                    BEGIN
                                        SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                                        FROM    syTblFlds AS STF; 
                                        INSERT  syTblFlds
                                                (
                                                 TblFldsId
                                                ,TblId
                                                ,FldId
                                                ,CategoryId
                                                ,FKColDescrip
                                                )
                                        VALUES  (
                                                 @TblFldsId
                                                ,@TblId
                                                ,@FldId
                                                ,NULL
                                                ,NULL
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syTblFlds ' + @FLdName
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                                    END; 
                            END;  --  Adding records to syTblFlds for the field "StatusId"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syResTblflds for the field "StatusId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syResTblFlds AS SRTF
                                                WHERE   SRTF.TblFldsId = @TblFldsId
                                                        AND SRTF.ResourceId = @ResourceId )
                                    BEGIN
                                        
                                        SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                                        FROM    syResTblFlds AS SRTF; 
                                        INSERT  INTO syResTblFlds
                                                (
                                                 ResDefId
                                                ,ResourceId
                                                ,TblFldsId
                                                ,Required
                                                ,SchlReq
                                                ,ControlName
                                                ,UsePageSetup
                                                )
                                        VALUES  (
                                                 @ResDefId
                                                ,@ResourceId
                                                ,@TblFldsId
                                                ,@Required
                                                ,0
                                                ,NULL
                                                ,1
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syResTblFlds ' + @FLdName 
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syResTblFlds ' + @FLdName 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syResTblFlds ' + @FLdName 
                                    END; 
                            END;  --  Adding records to syResTblflds for the field "StatusId"  -- Ends Here --
                    END;   --  Adding records to syFields, syTblFlds and syResTblflds for the field "StatusId"    -- Ends Here --

                -- ===============================================================================================
                --  Adding records to syFields, syTblFlds and syResTblflds for the field "Description"
                -- ===============================================================================================
                IF ( @Error = 0 )  -- insert syFields
                    BEGIN                                     
                        SET @FLdName = 'Description'; 
                        SET @FldType = 'Varchar'; 
                        SET @FldLen = 12;             
                        SET @Caption = 'Description';   
                        SET @Required = 1;   -- Required Yes  
                        SET @DDLName_In_syFields = 'ReasonNotEnrolledId';
                        SELECT  @DDLId_In_syFields = SD.DDLId
                        FROM    syDDLS AS SD
                        WHERE   SD.DDLName = @DDLName_In_syFields;  
                        IF ( @Error = 0 )  -- insert syFields
                            BEGIN 
                                -- ===============================================================================================
                                --  Adding records to syFields for the field "Description"
                                -- ===============================================================================================
                                SELECT  @FldTypeId = SFT.FldTypeId
                                FROM    syFieldTypes AS SFT
                                WHERE   SFT.FldType = @FldType;  
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName )
                                    BEGIN
                                        SELECT  @FldId = MAX(SF.FldId) + 1
                                        FROM    dbo.syFields AS SF; 
                                        INSERT  dbo.syFields
                                                (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                )
                                        VALUES  (
                                                 @FldId
                                                ,@FLdName
                                                ,@FldTypeId
                                                ,@FldLen
                                                ,@DDLId_In_syFields
                                                ,0
                                                ,0
                                                ,1
                                                ,NULL
                                                );  
                                        IF ( @@ERROR = 0 )
                                            BEGIN 
                                                SELECT  @DispFldId = SF.FldId    -- value go in DDL display field   (Description)
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName;  
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFields ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFields ' + @FLdName
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                               ,@DispFldId = SF.FldId            -- value go in DDL display field  (Description)
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful Field exists  ' + @FLdName + '  ' + 
                                        --SET @Error = 1
                                        --PRINT 'Failed syFields ' + @FLdName
                                    END; 
                            END;  --  Adding records to syFields for the field "Description"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Caption Id 
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syFldCaptions for the field "Description"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFldCaptions AS SFC
                                                WHERE   SFC.FldId = @FldId
                                                        AND SFC.Caption = @Caption )
                                    BEGIN
                                        SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                                        FROM    syFldCaptions AS SFC; 
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        INSERT  INTO syFldCaptions
                                                (
                                                 FldCapId
                                                ,FldId
                                                ,LangId
                                                ,Caption
                                                ,FldDescrip
                                                )
                                        VALUES  (
                                                 @FldCapId
                                                ,@FldId
                                                ,@LangId
                                                ,@Caption
                                                ,@Caption
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFldCaptions' + @Caption
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFldCaptions' + @Caption
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                                        --SET @Error = 1
                                        --PRINT 'Failed syFldCaptions ' +  @Caption
                                    END; 
                            END;  --  Adding records to syFldCaptions for the field "Description"   -- Ends Here --
                        IF ( @Error = 0 )  -- insert Tbl vs Fields Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syTblFlds for the field "Description"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    dbo.syTblFlds AS STF
                                                WHERE   STF.FldId = @FldId
                                                        AND STF.TblId = @TblId )
                                    BEGIN
                                        SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                                        FROM    syTblFlds AS STF; 
                                        INSERT  syTblFlds
                                                (
                                                 TblFldsId
                                                ,TblId
                                                ,FldId
                                                ,CategoryId
                                                ,FKColDescrip
                                                )
                                        VALUES  (
                                                 @TblFldsId
                                                ,@TblId
                                                ,@FldId
                                                ,NULL
                                                ,NULL
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syTblFlds ' + @FLdName 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 0  syTblFlds ' + @FLdName 
                                    END; 
                            END;  --  Adding records to syTblFlds for the field "Description"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syResTblflds for the field "Description"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syResTblFlds AS SRTF
                                                WHERE   SRTF.TblFldsId = @TblFldsId
                                                        AND SRTF.ResourceId = @ResourceId )
                                    BEGIN
                                        
                                        SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                                        FROM    syResTblFlds AS SRTF; 
                                        INSERT  INTO syResTblFlds
                                                (
                                                 ResDefId
                                                ,ResourceId
                                                ,TblFldsId
                                                ,Required
                                                ,SchlReq
                                                ,ControlName
                                                ,UsePageSetup
                                                )
                                        VALUES  (
                                                 @ResDefId
                                                ,@ResourceId
                                                ,@TblFldsId
                                                ,@Required
                                                ,0
                                                ,NULL
                                                ,1
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syResTblFlds ' + @FLdName 
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syResTblFlds ' + @FLdName 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syResTblFlds ' + @FLdName  
                                    END; 
                            END;  --  Adding records to syResTblflds for the field "Description" -- Ends Here --
                    END;   --  Adding records to syFields, syTblFlds and syResTblflds for the field "Description"    -- Ends Here --

                -- ===============================================================================================
                --  Adding records to syFields, syTblFlds and syResTblflds for the field "CampGrpId"
                -- ===============================================================================================
                IF ( @Error = 0 )  -- insert syFields
                    BEGIN                                           
                        SET @FLdName = 'CampGrpId'; 
                        SET @FldType = 'Uniqueidentifier'; 
                        SET @FldLen = 16;           
                        SET @Caption = 'Campus Group';   
                        SET @Required = 1;   -- Required Yes      
                        SET @DDLName_In_syFields = 'CampGrps';                
                        SELECT  @DDLId_In_syFields = SD.DDLId
                        FROM    syDDLS AS SD
                        WHERE   SD.DDLName = @DDLName_In_syFields;   
                        IF ( @Error = 0 )  -- insert syFields 
                            BEGIN 
                                -- ===============================================================================================
                                --  Adding records to syFields for the field "CampGrpId"
                                -- ===============================================================================================
                                SELECT  @FldTypeId = SFT.FldTypeId
                                FROM    syFieldTypes AS SFT
                                WHERE   SFT.FldType = @FldType;  
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName )
                                    BEGIN
                                        SELECT  @FldId = MAX(SF.FldId) + 1
                                        FROM    dbo.syFields AS SF; 
                                        INSERT  dbo.syFields
                                                (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                )
                                        VALUES  (
                                                 @FldId
                                                ,@FLdName
                                                ,@FldTypeId
                                                ,@FldLen
                                                ,@DDLId_In_syFields
                                                ,0
                                                ,0
                                                ,1
                                                ,NULL
                                                );  
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SELECT  @FldId = SF.FldId
                                                FROM    syFields AS SF
                                                WHERE   SF.FldName = @FLdName; 
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFields ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFields ' + @FLdName 
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful Field exists  ' + @FLdName
                                        --SET @Error = 1
                                        --PRINT 'Failed syFields ' + @FLdName
                                    END; 
                            END;  --  Adding records to syFields for the field "CampGrpId"  -- Ends Here --
                        IF ( @Error = 0 )  -- insert Caption Id 
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syFldCaptions for the field "CampGrpId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syFldCaptions AS SFC
                                                WHERE   SFC.FldId = @FldId
                                                        AND SFC.Caption = @Caption )
                                    BEGIN
                                        SELECT  @FldCapId = MAX(SFC.FldCapId) + 1
                                        FROM    syFldCaptions AS SFC; 
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        INSERT  INTO syFldCaptions
                                                (
                                                 FldCapId
                                                ,FldId
                                                ,LangId
                                                ,Caption
                                                ,FldDescrip
                                                )
                                        VALUES  (
                                                 @FldCapId
                                                ,@FldId
                                                ,@LangId
                                                ,@Caption
                                                ,@Caption
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;   
                --                                PRINT 'Successful syFldCaptions' + @Caption
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syFldCaptions' + @Caption
                                            END; 
                                    END;  
                                ELSE
                                    BEGIN
                                        SELECT  @FldId = SF.FldId
                                        FROM    syFields AS SF
                                        WHERE   SF.FldName = @FLdName; 
                                        SET @Error = 0; 
                --                        PRINT 'Successful syFldCaptions Field exists ' + @Caption
                                        --SET @Error = 1
                                        --PRINT 'Failed syFldCaptions ' +  @Caption
                                    END; 
                            END;  --  Adding records to syFldCaptions for the field "CampGrpId"   -- Ends Here -- 
                        IF ( @Error = 0 )  -- insert Tbl vs Fields Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syTblFlds for the field "CampGrpId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    dbo.syTblFlds AS STF
                                                WHERE   STF.FldId = @FldId
                                                        AND STF.TblId = @TblId )
                                    BEGIN
                                        SELECT  @TblFldsId = MAX(STF.TblFldsId) + 1
                                        FROM    syTblFlds AS STF; 
                                        INSERT  syTblFlds
                                                (
                                                 TblFldsId
                                                ,TblId
                                                ,FldId
                                                ,CategoryId
                                                ,FKColDescrip
                                                )
                                        VALUES  (
                                                 @TblFldsId
                                                ,@TblId
                                                ,@FldId
                                                ,NULL
                                                ,NULL
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syTblFlds ' + @FLdName
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syTblFlds ' + @FLdName
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01 syTblFlds ' + @FLdName 
                                    END; 
                            END;  --  Adding records to syTblFlds for the field "CampGrpId" -- Ends Here --
                        IF ( @Error = 0 )  -- insert Res Vs TblFlds Id
                            BEGIN
                                -- ===============================================================================================
                                --  Adding records to syResTblflds for the field "CampGrpId"
                                -- ===============================================================================================
                                IF NOT EXISTS ( SELECT  1
                                                FROM    syResTblFlds AS SRTF
                                                WHERE   SRTF.TblFldsId = @TblFldsId
                                                        AND SRTF.ResourceId = @ResourceId )
                                    BEGIN
                                        SELECT  @ResDefId = MAX(SRTF.ResDefId) + 1
                                        FROM    syResTblFlds AS SRTF; 
                                        INSERT  INTO syResTblFlds
                                                (
                                                 ResDefId
                                                ,ResourceId
                                                ,TblFldsId
                                                ,Required
                                                ,SchlReq
                                                ,ControlName
                                                ,UsePageSetup
                                                )
                                        VALUES  (
                                                 @ResDefId
                                                ,@ResourceId
                                                ,@TblFldsId
                                                ,@Required
                                                ,0
                                                ,NULL
                                                ,1
                                                ); 
                                        IF ( @@ERROR = 0 )
                                            BEGIN
                                                SET @Error = 0;    
                --                                PRINT 'Successful syResTblFlds ' + @FLdName 
                                            END; 
                                        ELSE
                                            BEGIN
                                                SET @Error = 1; 
                --                                PRINT 'Failed syResTblFlds ' + @FLdName 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
                --                        PRINT 'Failed 01  ' + @FLdName
                                    END; 
                            END;  --  Adding records to syResTblflds for the field "CampGrpId" -- Ends Here --
                    END;   --  Adding records to syFields, syTblFlds and syResTblflds for the field "CampGrpId"    -- Ends Here --
            END;  --  Adding records to for files  -- Ends Here --

        -- ===============================================================================================
        --  Adding records to syDDLs for the "Description"  field
        -- ===============================================================================================
        IF ( @Error = 0 )  -- insert DLL
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    syDDLS AS SD
                                WHERE   SD.DDLName = @DDLName )
                    BEGIN
                        SELECT  @DDLId = MAX(SD.DDLId) + 1
                        FROM    syDDLS AS SD; 
                        INSERT  INTO syDDLS
                                (
                                 DDLId
                                ,DDLName
                                ,TblId
                                ,DispFldId
                                ,ValFldId
                                ,ResourceId
                                ,CulDependent
                                )
                        VALUES  (
                                 @DDLId
                                ,@DDLName
                                ,@TblId
                                ,@DispFldId               -- value came in field to be displayed (Description)
                                ,@ValFldId                -- value came in field of Id           (ReasonNotEnrolledId)
                                ,@DDLResourceId
                                ,NULL
                                ); 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SELECT  @DDLId = SD.DDLId
                                FROM    syDDLS AS SD
                                WHERE   SD.DDLName = @DDLName; 
                                SET @Error = 0;    
--                                PRINT 'Successful syDDLS'
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syDDLS' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 syDDLS' 
                    END; 
            END;   --  Adding records to syDDLs for the "Contact Type"  field  -- Ends Here --

        -- ===============================================================================================
        --  Update syFields (@TblPk_Fld)  with DDLId
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Update Field Id with DDLId
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldId = @TblPk_Fld )
                    BEGIN
                        SELECT  @DDLId = SD.DDLId
                        FROM    syDDLS AS SD
                        WHERE   SD.DDLName = @DDLName; 
                        UPDATE  SF
                        SET     SF.DDLId = @DDLId
                        FROM    syFields AS SF
                        WHERE   SF.FldId = @TblPk_Fld;  --  with FldId of ReasonNotEnrolledId field
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;    
--                                PRINT 'Successful syFields DDLId'
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syFields DDLId '
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 syFields DDLId' 
                    END; 
            END;   --  Update syFields with DDLId  -- Ends Here -- 

        -- ===============================================================================================
        --  Update syTables (tblPK with  @TblPk_Fld FldId of ReasonNotEnrolledId field
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Update Table with Field (Id)
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    syFields AS SF
                            WHERE   SF.FldId = @FldId )
                    BEGIN
                        UPDATE  ST
                        SET     ST.TblPK = @TblPk_Fld  -- with FldId of ReasonNotEnrolledId field
                        FROM    syTables AS ST
                        WHERE   ST.TblName = @TblName; 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;    
--                                PRINT 'Successful syTables DDLId'
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syTables DDLId' 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 1; 
--                        PRINT 'Failed 01 syTables DDLId' 
                    END; 
            END;   --  Update syTables (tblPK with  @TblPk_Fld FldId of ReasonNotEnrolledId field  -- Ends Here -- 


 -- To Test
--  SET @Error = 1
 
        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION  AddNewFields; 
--                PRINT 'Successful COMMIT TRANSACTION'
            END; 
        ELSE
            BEGIN
                ROLLBACK TRANSACTION AddNewFields; 
--                PRINT 'Failed ROLLBACK TRANSACTION '     
            END; 
    END TRY
    BEGIN CATCH

        DECLARE @ErrorMessage NVARCHAR(MAX)
           ,@ErrorSeverity INT
           ,@ErrorState INT;
        SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
               ,@ErrorSeverity = ERROR_SEVERITY()
               ,@ErrorState = ERROR_STATE();
        IF ( @@TRANCOUNT > 0 )
            BEGIN
                ROLLBACK TRANSACTION AddNewFields;
            END;
        RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
    END CATCH; 
END; 
-- ===============================================================================================
-- END  --  US7547 Reason Not Enrolled Configuration 
-- ===============================================================================================
GO

-- ===============================================================================================
-- DE12928 Error found in TC13614: QA: User is able to filter the leads by current campus
-- JAGG
-- Eliminate all ModDate in null
-- ===============================================================================================
IF EXISTS ( SELECT  1
            FROM    adLeads
            WHERE   ModDate IS NULL )
    BEGIN
        UPDATE  adLeads
        SET     ModDate = GETDATE()
        WHERE   ModDate IS NULL;
    END;
-- ===============================================================================================
-- END JAGG --   DE12928 QA: User is able to filter the leads by current campus
-- ===============================================================================================
GO

-- ===============================================================================================
-- US7546 Email Type Configuration
-- ===============================================================================================
BEGIN TRANSACTION UPDATE_HomeEmailDescription;
BEGIN TRY
	
    UPDATE  dbo.syEmailType
    SET     EMailTypeDescription = 'Personal'
    WHERE   EMailTypeDescription = 'Home';
	
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UPDATE_HomeEmailDescription;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
  
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UPDATE_HomeEmailDescription;
	--PRINT 'TABLE @TABLE_NAME_HERE@ COMMITED'
    END;

GO
-- ===============================================================================================
-- END US7546 Email Type Configuration
-- ===============================================================================================
-- ===============================================================================================
-- US8216 Maintenance\Quick Leads
-- ===============================================================================================
BEGIN TRANSACTION QUICK_LEAD_TRANSACTION;
BEGIN TRY
    DECLARE @tempFldId INT;
    DECLARE @expId INT;
    DECLARE @pk BIGINT;
    DECLARE @tblId BIGINT; 
    DECLARE @tblPk BIGINT;
-- ***********************************************************************************
-- Script to create resources for the table AdLeads new field HighSchool.
-- Also the resource was generated and assigned to resource 170 (lead info page)
-- Authors: Sthakkar
-- Dependencies: DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
    IF NOT EXISTS ( SELECT  FldName
                    FROM    dbo.syFields
                    WHERE   FldName = 'HighSchoolGradDate' )
        BEGIN
            SET @pk = (
                        SELECT  TblPK
                        FROM    dbo.syTables
                        WHERE   TblName = 'AdLeads'
                      );						  				                      
            IF @pk IS NOT NULL
                BEGIN
                    EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'HighSchoolGradDate',@FldTypeId = 135,@FldLen = 16,@DerivedField = NULL,
                        @SchlReq = NULL,@LogChanges = 1,@Mask = NULL,@FieldCaption = N'High School Grad Date',@LanguageId = 1,@TblName = N'AdLeads',
                        @TblDescription = N'Main Leads Table',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,@ControlName = NULL;
                END; 
        END;
  ------Adding "AllNotes" in the syTables
    IF NOT EXISTS ( SELECT  TblName
                    FROM    dbo.syTables
                    WHERE   TblName = 'AllNotes' )
        BEGIN
          
            SET @tblPk = (
                           SELECT   MAX(TblPK)
                           FROM     dbo.syTables
                         ) + 1;
            SET @tblId = (
                           SELECT   MAX(TblId)
                           FROM     dbo.syTables
                         ) + 1;
            INSERT  INTO dbo.syTables
                    (
                     TblId
                    ,TblName
                    ,TblDescrip
                    ,TblPK
                    )
            VALUES  (
                     @tblId
                    ,'AllNotes'
                    ,'Common Notes table for all the pages in the application'
                    ,@tblPk
                    );
        END;

  ------Adding "adLeadAddresses" in the syTables
    IF NOT EXISTS ( SELECT  TblName
                    FROM    dbo.syTables
                    WHERE   TblName = 'adLeadAddresses' )
        BEGIN 
            SET @tblPk = 0;
            SET @tblId = 0;
            SET @tblPk = (
                           SELECT   MAX(TblPK)
                           FROM     dbo.syTables
                         ) + 1;
            SET @tblId = (
                           SELECT   MAX(TblId)
                           FROM     dbo.syTables
                         ) + 1;
            INSERT  INTO dbo.syTables
                    (
                     TblId
                    ,TblName
                    ,TblDescrip
                    ,TblPK
                    )
            VALUES  (
                     @tblId
                    ,'adLeadAddresses'
                    ,'stores Address details for all leads'
                    ,@tblPk
                    );
        END;
-- ***********************************************************************************
-- Create adLead Addresses Table (adLeadAddresses) resource 
-- and AddressApto resources for LeadInfoPage
-- Authors: Sthakkar
-- Dependencies: Stored Procedure DEVELOPMENT_InsertUpdateFieldInResources
-- Minimal Version 3.8
-- ***********************************************************************************
    IF NOT EXISTS ( SELECT  FldName
                    FROM    dbo.syFields
                    WHERE   FldName = 'AddressApto' )
        BEGIN
             
            SET @pk = (
                        SELECT  TblPK
                        FROM    dbo.syTables
                        WHERE   TblName = 'adLeadAddresses'
                      );
            IF ( @pk IS NULL )
                BEGIN
                    SET @tblId = (
                                   SELECT   MAX(syTables.TblId)
                                   FROM     dbo.syTables
                                 ) + 1;
                    SET @pk = (
                                SELECT  MAX(syTables.TblPK)
                                FROM    dbo.syTables
                              ) + 1;
                    INSERT  INTO dbo.syTables
                            (
                             TblId
                            ,TblName
                            ,TblDescrip
                            ,TblPK
                            )
                    VALUES  (
                             @tblId
                            ,'adLeadAddresses'
                            ,'Table for Addresses'
                            ,@pk
                            );

                    EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'AddressApto',@FldTypeId = 200,@FldLen = 20,@DerivedField = NULL,
                        @SchlReq = NULL,@LogChanges = 1,@Mask = NULL,@FieldCaption = N'AddressApt',@LanguageId = 1,@TblName = N'adLeadAddresses',
                        @TblDescription = N'Hold the apartment number for  each lead',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,
                        @Required = 0,@ControlName = NULL;
                END;
            ELSE
                BEGIN
                    EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'AddressApto',@FldTypeId = 200,@FldLen = 20,@DerivedField = NULL,
                        @SchlReq = NULL,@LogChanges = 1,@Mask = NULL,@FieldCaption = N'AddressApt',@LanguageId = 1,@TblName = N'adLeadAddresses',
                        @TblDescription = N'Hold the apartment number for  each lead',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,
                        @Required = 0,@ControlName = NULL;
                END;
        END;

--to get the values for "Nick Name" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'NickName' )
        BEGIN
            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'NickName'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5555
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5555,@tempFldId );
                END;	
        END;
	  


--to get the values for "Distance to School" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'TimeToSchool' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'TimeToSchool'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5555
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5555,@tempFldId );
                    --print 'TimeToSchool added in adExpQuickLeadSections';
                END;	
        END;	  


--to get the values for "High School Grad Date" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'HighSchoolGradDate' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'HighSchoolGradDate'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5555
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5555,@tempFldId );
                END;
        END; 		  

--to get the values for "Schedule" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'ProgramScheduleId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'ProgramScheduleId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5559
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5559,@tempFldId );
                END;	
        END;		  


--to get the values for "Transportation" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'TransportationId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'TransportationId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5555
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5555,@tempFldId );
                END;	
        END;		  

-----------------------------------------
--todo check if this is double entry
-----------------------------------------
----to get the values for "High School Grad Date" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
--IF NOT EXISTS ( SELECT  [ExpId]
--                FROM    [adExpQuickLeadSections]
--                WHERE   [SectionId] = 5555
--                        AND [FldId] = 1223 )
--    BEGIN
--        INSERT  INTO [dbo].[adExpQuickLeadSections]
--                ( [ExpId], [SectionId], [FldId] )
--        VALUES  ( 85, 5555, 1223 )
--    END		  
--

--to get the values for "Attending High School" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'AttendingHs' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'AttendingHs'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5555
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5555,@tempFldId );
                END;		
        END;   

--to get the values for "Attend" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'AttendTypeId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'AttendTypeId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5559
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5559,@tempFldId );
                END;		
        END;   

--to get the values for "Best Time" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'BestTimeId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'BestTimeId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5557
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5557,@tempFldId );
                    --print 'BestTimeId added in adExpQuickLeadSections';
                END;	
        END; 	  

--to get the values for "PreferredContactId" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'PreferredContactId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'PreferredContactId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5557
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5557,@tempFldId );
                    --print 'PreferredContactId added in adExpQuickLeadSections';
                END;	
        END;		  

--to get the values for "Extension" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'Extension' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'Extension'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5557
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5557,@tempFldId );
                    --print 'Extension added in adExpQuickLeadSections';
                END;		
        END;	  

--to get the values for "IsForeignPhone" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'IsForeignPhone' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'IsForeignPhone'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5557
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5557,@tempFldId );
                    --print 'IsForeignPhone added in adExpQuickLeadSections';
                END;	
        END; 	  

--to get the values for "EmailType" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'EmailTypeId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'EmailTypeId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5557
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5557,@tempFldId );
                END;		  
        END; 

--to get the values for "AddressType" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'AddressTypeId' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'AddressTypeId'
                             );
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5556
                                    AND FldId = @tempFldId )
                BEGIN
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5556,@tempFldId );
                END;		  
        END;    

--to get the values for "AddressApto" in maintainance for "available" values" on the LHS to be slected for the Quick lead page
    IF EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'AddressApto' )
        BEGIN
            SET @tempFldId = 0;
            SET @expId = 0;

            SET @tempFldId = (
                               SELECT   FldId
                               FROM     syFields
                               WHERE    FldName = 'AddressApto'
                             );
            IF NOT EXISTS ( SELECT  ExpId
                            FROM    adExpQuickLeadSections
                            WHERE   SectionId = 5556
                                    AND FldId = @tempFldId )
                BEGIN	
                    SET @expId = (
                                   SELECT   MAX(ExpId)
                                   FROM     adExpQuickLeadSections
                                 ) + 1;
                    INSERT  INTO dbo.adExpQuickLeadSections
                            ( ExpId,SectionId,FldId )
                    VALUES  ( @expId,5556,@tempFldId );
                END;	
        END;		  

    UPDATE  dbo.syFldCaptions
    SET     Caption = 'Dependants'
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      syFields
                      WHERE     FldName = 'Children'
                    );
  
-- ********************************************************************************************************
-- Change "Time To School" Caption for "Dist to School" Info Lead Page
-- Sthakkar 5/9/2016
-- ********************************************************************************************************
 
    UPDATE  dbo.syFldCaptions
    SET     Caption = 'Dist to School'
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      syFields
                      WHERE     FldName = 'TimeToSchool'
                    );
  
    --print 'TimeToSchool caption changed to Dist to School';
  
-- ********************************************************************************************************
-- Change "Time To School" Field Name for "Dist to School" Info Lead Page
-- Sthakkar 5/9/2016
-- ********************************************************************************************************


    IF NOT EXISTS ( SELECT  *
                    FROM    syFields
                    WHERE   syFields.FldName = 'DistToSchool' )
        BEGIN
            UPDATE  dbo.syFields
            SET     FldName = 'DistToSchool'
            WHERE   FldName = 'TimeToSchool';
        END; 
  
    --print 'TimeToSchool field name changed to Dist to School';


----
-- ********************************************************************************************************
-- Create or Update the relation between "Comments" with the table "AllNotes" and 
-- With the Page Resource 170 (Lead Info Page)
-- SThakkar 5/10/2016
-- ********************************************************************************************************
    EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'Comments',@FldTypeId = 200,@FldLen = 200,@DerivedField = NULL,@SchlReq = NULL,
        @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Comments',@LanguageId = 1,@TblName = N'AllNotes',
        @TblDescription = N'Common Notes table for all the pages in the application',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,
        @ControlName = NULL;

----------------
--deleting the "Geographic Type" as it is no more in quick leads page
    IF EXISTS ( SELECT  *
                FROM    dbo.adExpQuickLeadSections
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      dbo.syFields
                                  WHERE     FldName = 'GeographicTypeId'
                                )
                        AND SectionId = 5555 )
        BEGIN
            DELETE  FROM adExpQuickLeadSections
            WHERE   SectionId = 5555
                    AND FldId = (
                                  SELECT    FldId
                                  FROM      dbo.syFields
                                  WHERE     FldName = 'GeographicTypeId'
                                ); 
        END;

----check this

--UPDATE syFields SET FldTypeId = 72 WHERE FldId = 1220 --- need to check if it is is firsttimeinschool
--
    UPDATE  syFields
    SET     FldTypeId = 3
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName = 'Children'
                    );
 -- for children/dependants

    UPDATE  syFields
    SET     FldTypeId = 3
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName = 'DistToSchool'
                    );

    --print 'FldTypeId of DistToSchool changed to 3';
    UPDATE  syFields
    SET     FldTypeId = 72
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName = 'EmailTypeId'
                    );

    --print 'FldTypeId of EmailTypeId changed to 72';
    UPDATE  syFields
    SET     FldTypeId = 72
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName = 'TransportationId'
                    );

    --print 'FldTypeId of TransportationId changed to 72';
--DELETE the entry for degree\certificate seeking type as its no more in Quick lead page. it is in the enrollment page
    DELETE  FROM adExpQuickLeadSections
    WHERE   FldId = (
                      SELECT    FldId
                      FROM      dbo.syFields
                      WHERE     FldName = 'DegCertSeekingId'
                    );
    --print 'DELETE the entry for degree\certificate seeking type as its no more in Quick lead page. it is in the enrollment page';

    IF EXISTS ( SELECT  FldId
                FROM    adExpQuickLeadSections
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      dbo.syFields
                                  WHERE     FldName = 'SourceDate'
                                ) ) -- changed the SOURCE date to created date in the source information
        BEGIN
            UPDATE  adExpQuickLeadSections
            SET     FldId = (
                              SELECT    FldId
                              FROM      dbo.syFields
                              WHERE     FldName = 'CreatedDate'
                            )
            WHERE   FldId = (
                              SELECT    FldId
                              FROM      dbo.syFields
                              WHERE     FldName = 'SourceDate'
                            );
        END;

    --print 'SourceDate is changed to CreatedDate';
--adding "Notes" field in the source info in the QuickLead page
----
-- ********************************************************************************************************
-- Create or Update the relation between "notes" with the table "AllNotes" and 
-- With the Page Resource 170 (Lead Info Page)
-- SThakkar 6/10/2016
-- ********************************************************************************************************
    EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'Notes',@FldTypeId = 200,@FldLen = 240,@DerivedField = NULL,@SchlReq = NULL,@LogChanges = 1,
        @Mask = NULL,@FieldCaption = N'Notes',@LanguageId = 1,@TblName = N'AllNotes',
        @TblDescription = N'Common Notes table for all the pages in the application',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,
        @ControlName = NULL;

    IF NOT EXISTS ( SELECT  *
                    FROM    adExpQuickLeadSections
                    WHERE   FldId = (
                                      SELECT    FldId
                                      FROM      dbo.syFields
                                      WHERE     FldName = 'Notes'
                                    ) )
        BEGIN
            SET @expId = 0;
            SET @expId = (
                           SELECT   MAX(ExpId)
                           FROM     adExpQuickLeadSections
                         ) + 1;
            INSERT  INTO adExpQuickLeadSections
                    (
                     ExpId
                    ,SectionId
                    ,FldId
                    )
            VALUES  (
                     @expId
                    ,5558
                    ,(
                       SELECT   FldId
                       FROM     dbo.syFields
                       WHERE    FldName = 'Notes'
                     )
                    );
        END;
	
 --DELETE the entry for AdvertisementNote as its no more in Quick lead page. it is in the enrollment page

    IF EXISTS ( SELECT  FldId
                FROM    adExpQuickLeadSections
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      dbo.syFields
                                  WHERE     FldName = 'AdvertisementNote'
                                ) ) -- changed the SOURCE date to created date in the source information
        BEGIN
            DELETE  FROM adExpQuickLeadSections
            WHERE   FldId = (
                              SELECT    FldId
                              FROM      dbo.syFields
                              WHERE     FldName = 'AdvertisementNote'
                            );
        END;

 --DELETE the entry for AdvertisementNote as its no more in Quick lead page. it is in the enrollment page

    IF EXISTS ( SELECT  FldId
                FROM    adExpQuickLeadSections
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      dbo.syFields
                                  WHERE     FldName = 'NoneEmail'
                                ) ) -- changed the SOURCE date to created date in the source information
        BEGIN
            DELETE  FROM adExpQuickLeadSections
            WHERE   FldId = (
                              SELECT    FldId
                              FROM      dbo.syFields
                              WHERE     FldName = 'NoneEmail'
                            );
        END;


    DELETE  FROM adExpQuickLeadSections
    WHERE   FldId IN (
            SELECT  FldId
            FROM    dbo.syFields
            WHERE   FldName IN ( 'Address2','AddressStatus','AddressTypeId','EntranceInterviewDate','ForeignPhone','graduatedorreceiveddate','IsDisabled',
                                 'IsFirstTimeInSchool','IsFirstTimePostSecSchool','Nationality','PhoneStatus','ShiftId','WorkEmail' ) );


--PRINT 'deleted few fields from adExpQuickLeadSections'

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION QUICK_LEAD_TRANSACTION;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION QUICK_LEAD_TRANSACTION;
    END;


GO

-- ===============================================================================================
-- END US8216 Maintenance\Quick Leads
-- ===============================================================================================
-- ===============================================================================================
-- US8216 Maintenance\Quick Leads
-- ===============================================================================================
BEGIN TRANSACTION QUICK_LEAD_TRANSACTION;
BEGIN TRY
    IF EXISTS ( SELECT  *
                FROM    adQuickLeadMap )
        BEGIN 
            TRUNCATE TABLE dbo.adQuickLeadMap;

            DBCC	CHECKIDENT ('adQuickLeadMap',RESEED,1);
        END;
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Age' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Age'
                    ,'textage'
                    ,'Age'
                    ,19
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AlienNumber' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AlienNumber'
                    ,'tbaliennumber'
                    ,'AlienNumber'
                    ,21
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'BirthDate' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'BirthDate'
                    ,'dtdob'
                    ,'Dob'
                    ,18
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Citizen' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Citizen'
                    ,'cbcitizenship'
                    ,'Citizenship'
                    ,20
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'FirstName' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'FirstName'
                    ,'txtFirstName'
                    ,'FirstName'
                    ,2
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Children' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Children'
                    ,'itextdependants'
                    ,'Dependants'
                    ,24
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'DependencyTypeId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'DependencyTypeId'
                    ,'cbdependency'
                    ,'Dependency'
                    ,22
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'DrivLicNumber' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'DrivLicNumber'
                    ,'tdriverlicensenumber'
                    ,'DriverLicenseNumber'
                    ,28
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'DrivLicStateID' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'DrivLicStateID'
                    ,'cbdriverlicensestate'
                    ,'DrvLicStateCode'
                    ,27
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'StateId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'StateId'
                    ,'Address'
                    ,'StateId'
                    ,44
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Race' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Race'
                    ,'cbRace'
                    ,'RaceId'
                    ,16
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'FamilyIncome' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'FamilyIncome'
                    ,'cbfamilyincome'
                    ,'FamilyIncoming'
                    ,25
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Gender' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Gender'
                    ,'cbgender'
                    ,'Gender'
                    ,15
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'HousingId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'HousingId'
                    ,'cbhousingtype'
                    ,'HousingType'
                    ,26
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'LastName' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'LastName'
                    ,'txtLastName'
                    ,'LastName'
                    ,5
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'MaritalStatus' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'MaritalStatus'
                    ,'cbmaritalstatus'
                    ,'MaritalStatus'
                    ,23
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'MiddleName' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'MiddleName'
                    ,'txtMiddleName'
                    ,'MiddleName'
                    ,3
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'NickName' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'NickName'
                    ,'tbNickName'
                    ,'NickName'
                    ,4
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Prefix' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Prefix'
                    ,'cbprefix'
                    ,'Prefix'
                    ,1
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'SSN' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'SSN'
                    ,'txtsocialSecurity'
                    ,'SSN'
                    ,17
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'TransportationId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'TransportationId'
                    ,'cbtransportation'
                    ,'Transportation'
                    ,29
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'DistToSchool' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'DistToSchool'
                    ,'tpTimeTo'
                    ,'DistanceToSchool'
                    ,30
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AreaId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AreaId'
                    ,'cbInterestArea'
                    ,'AreaId'
                    ,9
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AttendTypeId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AttendTypeId'
                    ,'cbattend'
                    ,'AttendTypeId'
                    ,13
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'CampusId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'CampusId'
                    ,'cbLeadAssign'
                    ,'CampusId'
                    ,8
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'ExpectedStart' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'ExpectedStart'
                    ,'cbexpectedStart'
                    ,'ExpectedStart'
                    ,12
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Suffix' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Suffix'
                    ,'cbsuffix'
                    ,'Suffix'
                    ,6
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'LeadStatus' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'LeadStatus'
                    ,'cbLeadStatus'
                    ,'LeadStatusId'
                    ,7
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'PrgVerId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'PrgVerId'
                    ,'cbprogramVersion'
                    ,'PrgVerId'
                    ,11
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'ProgramID' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'ProgramID'
                    ,'cbprogramLabel'
                    ,'ProgramId'
                    ,10
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'ProgramScheduleId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'ProgramScheduleId'
                    ,'cbschedule'
                    ,'ScheduleId'
                    ,14
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'CreatedDate' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'CreatedDate'
                    ,'dtDateSource'
                    ,'SourceDateTime'
                    ,52
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'SourceCategoryID' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'SourceCategoryID'
                    ,'cbcategorySource'
                    ,'SourceCategoryId'
                    ,49
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'SourceAdvertisement' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'SourceAdvertisement'
                    ,'cbAdvertisementSource'
                    ,'AdvertisementId'
                    ,51
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'SourceTypeID' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'SourceTypeID'
                    ,'cbtypeSource'
                    ,'SourceTypeId'
                    ,50
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'DateApplied' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'DateApplied'
                    ,'dtotherDateApplied'
                    ,'DateApplied'
                    ,54
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AssignedDate' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AssignedDate'
                    ,'dtotherAdmRepAssignDate'
                    ,'AssignedDate'
                    ,55
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AdmissionsRep' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AdmissionsRep'
                    ,'cbotherAdmRep'
                    ,'AdmissionRepId'
                    ,56
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Sponsor' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Sponsor'
                    ,'cbotherSponsor'
                    ,'AgencySponsorId'
                    ,57
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Comments' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Comments'
                    ,'tbOtherComment'
                    ,'Comments'
                    ,63
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'PreviousEducation' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'PreviousEducation'
                    ,'cbotherPreviousEducation'
                    ,'PreviousEducationId'
                    ,58
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'HighSchool' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'HighSchool'
                    ,'cbOtherHighSchool'
                    ,'HighSchoolId'
                    ,59
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'HighSchoolGradDate' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'HighSchoolGradDate'
                    ,'dtHsGradDate'
                    ,'HighSchoolGradDate'
                    ,60
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AttendingHs' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AttendingHs'
                    ,'ddAttendingHs'
                    ,'AttendingHs'
                    ,61
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'admincriteriaid' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'admincriteriaid'
                    ,'cbotherAdmCriteria'
                    ,'AdminCriteriaId'
                    ,62
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'IsForeignPhone' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'IsForeignPhone'
                    ,'Phone'
                    ,'IsForeignPhone'
                    ,36
                    );
        END;


--start change make [ctrlIdName] for all fields related to Phone here todo change the fieldName may be

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'PhoneType' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'PhoneType'
                    ,'Phone'
                    ,'PhoneTypeId'
                    ,33
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Phone' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Phone'
                    ,'Phone'
                    ,'Phone'
                    ,34
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Extension' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Extension'
                    ,'Phone'
                    ,'Extension'
                    ,35
                    );
        END;

--to do email
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'EmailTypeId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'EmailTypeId'
                    ,'Email'
                    ,'EmailType'
                    ,37
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'HomeEmail' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'HomeEmail'
                    ,'Email'
                    ,'Email'
                    ,38
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'PreferredContactId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'PreferredContactId'
                    ,'PreferredContactId'
                    ,'PreferredContactId'
                    ,31
                    );
        END;

---start address change
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Address1' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Address1'
                    ,'Address'
                    ,'Address1'
                    ,40
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AddressType' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AddressType'
                    ,'Address'
                    ,'AddressTypeId'
                    ,39
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'City' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'City'
                    ,'Address'
                    ,'City'
                    ,43
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'County' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'County'
                    ,'Address'
                    ,'CountyId'
                    ,47
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Country' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Country'
                    ,'Address'
                    ,'CountryId'
                    ,48
                    );
        END;


    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Zip' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Zip'
                    ,'Address'
                    ,'ZipCode'
                    ,46
                    );
        END;

--- to start from here
-- to do international
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'ForeignZip' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'ForeignZip'
                    ,'Address'
                    ,'IsInternational'
                    ,42
                    );
        END;

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'AddressApto' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'AddressApto'
                    ,'Address'
                    ,'AddressApto'
                    ,41
                    );
        END;
---run from here

    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'BestTimeId' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'BestTimeId'
                    ,'tpBestTime'
                    ,'BestTime'
                    ,32
                    );
        END;
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'Notes' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'Notes'
                    ,'txtNoteSource'
                    ,'Note'
                    ,53
                    );
        END;
    IF NOT EXISTS ( SELECT  fldName
                    FROM    dbo.adQuickLeadMap
                    WHERE   fldName = 'OtherState' )
        BEGIN
            INSERT  INTO dbo.adQuickLeadMap
                    (
                     fldName
                    ,ctrlIdName
                    ,propName
                    ,sequence
                    )
            VALUES  (
                     'OtherState'
                    ,'Address'
                    ,'StateInternational'
                    ,45
                    );
        END;
    UPDATE  dbo.adQuickLeadMap
    SET     parentCtrlId = 'CampusId'
    WHERE   propName IN ( 'LeadStatusId','AreaId','AttendTypeId','RaceId','AddressTypeId','CountryId','CountyId','AdmissionRepId','PreviousEducationId',
                          'HighSchoolId','AdminCriteriaId' );
    UPDATE  dbo.adQuickLeadMap
    SET     parentCtrlId = 'CampusId'
    WHERE   propName IN ( 'ProgramId','PrgVerId','ScheduleId','Sponsor' );
--- [adExpQuickLeadSections]
    DELETE  FROM adExpQuickLeadSections;
    DECLARE @fld BIGINT;
    SET @fld = (
                 SELECT FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AreaId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 1,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'CampusId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 2,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Address1' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 3,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'City' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 4,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'StateId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 5,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Zip' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 6,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Phone' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 7,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'PrgVerId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 8,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'LastName' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 9,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'FirstName' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 10,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'SSN' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 11,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'BirthDate' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 12,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Comments' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AllNotes'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 13,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'MiddleName' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 14,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'HomeEmail' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 15,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Notes' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AllNotes'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 16,5558,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'TransportationId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 17,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Age' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 18,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Country' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 19,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'LeadStatus' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 20,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AddressType' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 21,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Prefix' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 22,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Suffix' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 23,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Sponsor' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 24,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AdmissionsRep' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 25,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Gender' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 26,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Race' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 27,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'MaritalStatus' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 28,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'FamilyIncome' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 29,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Children' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 30,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'PhoneType' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 31,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'SourceCategoryID' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 32,5558,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'SourceTypeID' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 33,5558,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'ProgramID' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 34,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'ExpectedStart' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 35,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Citizen' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 36,5560,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'DrivLicStateID' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 37,5560,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'DrivLicNumber' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 38,5560,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AlienNumber' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 39,5560,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AssignedDate' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 40,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'SourceAdvertisement' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 41,5558,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'County' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 42,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'PreviousEducation' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 43,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'CreatedDate' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 44,5558,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'ForeignZip' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 45,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'OtherState' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 46,5556,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AttendTypeId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 47,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'DependencyTypeId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 48,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'HousingId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 49,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'admincriteriaid' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 50,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'DateApplied' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 51,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'PreferredContactId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 52,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'Extension' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleadPhone'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 53,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'IsForeignPhone' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adleadPhone'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 54,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'EmailTypeId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'syEmailType'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 55,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'BestTimeId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 56,5557,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AttendingHs' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 57,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'NickName' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 58,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'DistToSchool' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 59,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'ProgramScheduleId' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 60,5559,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'HighSchoolGradDate' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'AdLeads'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 61,5555,@fld );

    SET @fld = (
                 SELECT TOP 1
                        FldId
                 FROM   syTblFlds
                 WHERE  FldId IN ( SELECT   FldId
                                   FROM     dbo.syFields
                                   WHERE    FldName = 'AddressApto' )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      dbo.syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
               );
    INSERT  INTO dbo.adExpQuickLeadSections
            ( ExpId,SectionId,FldId )
    VALUES  ( 62,5556,@fld );
	
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION QUICK_LEAD_TRANSACTION;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION QUICK_LEAD_TRANSACTION;
    END;


GO
-- ===============================================================================================
-- END US8216 Maintenance\Quick Leads
-- ===============================================================================================
-- ******************************************************************************
-- Insert relation between page LeadInfo and field description
-- and table syReasonNotEnrolled
-- JAGG
-- ******************************************************************************
EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'Description',@FldTypeId = 200,@FldLen = 250,@DerivedField = NULL,@SchlReq = NULL,
    @LogChanges = 1,@Mask = NULL,@FieldCaption = N'Description',@LanguageId = 1,@TblName = N'syReasonNotEnrolled',
    @TblDescription = N'Reason Because the lead are not enrolled',@CategoryId = NULL,@FKColDescrip = NULL,@PageResourceId = 170,@Required = 0,
    @ControlName = NULL;
GO

-- ******************************************************************************
-- END JAGG
-- ******************************************************************************
-- ===============================================================================================
-- US8256 Update Maintenance System Area
-- ===============================================================================================
BEGIN TRANSACTION UpdateMaintenanceMenu;
BEGIN TRY

    UPDATE  menuItems
    SET     menuItems.MenuName = 'Status Codes for Leads & Students'
           ,menuItems.DisplayName = 'Status Codes for Leads & Students'
    FROM    syMenuItems AS menuItems
    WHERE   MenuName = 'Status Codes for Leads, Students, etc.';

    UPDATE  resources
    SET     resources.Resource = 'Relationship Types'
    FROM    syResources AS resources
    WHERE   ResourceID = 481
            AND Resource = 'Relations';

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMaintenanceMenu;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateMaintenanceMenu;
    END;

GO
-- ===============================================================================================
-- END US8256 Update Maintenance System Area
-- ===============================================================================================
-- ===============================================================================================
-- START US8216 Maintenance\Quick Leads
-- ===============================================================================================
IF NOT EXISTS ( SELECT  *
                FROM    adLeadFields
                WHERE   LeadID = 5561
                        AND LeadField = 5561 )
    BEGIN
        INSERT  INTO dbo.adLeadFields
                ( LeadID,ModDate,ModUser,LeadField )
        VALUES  ( 5561,GETDATE(),'Support',5561 );
    END;
GO

IF NOT EXISTS ( SELECT  *
                FROM    adQuickLeadSections
                WHERE   SectionId = 5561 )
    BEGIN
        INSERT  INTO dbo.adQuickLeadSections
                (
                 SectionId
                ,SectionName
                )
        VALUES  (
                 5561
                ,'Add Lead Groups Information'
                );
    END; 
GO
-- ===============================================================================================
-- END US8216 Maintenance\Quick Leads
-- ===============================================================================================
-- ===============================================================================================
-- START US7032 Add UDF to Quick Leads Page
-- ===============================================================================================
--delete the entry for 'Enter New Leads' as it is same as Lead Info page now
IF EXISTS ( SELECT  *
            FROM    syAdvantageResourceRelations
            WHERE   syAdvantageResourceRelations.RelatedResourceId = 206
                    AND syAdvantageResourceRelations.ResourceId = 395 )
    BEGIN
        DELETE  FROM syAdvantageResourceRelations
        WHERE   syAdvantageResourceRelations.RelatedResourceId = 206
                AND syAdvantageResourceRelations.ResourceId = 395;
    END;
GO
--add entry for Quick Lead page
IF NOT EXISTS ( SELECT  *
                FROM    syAdvantageResourceRelations
                WHERE   syAdvantageResourceRelations.RelatedResourceId = 268
                        AND syAdvantageResourceRelations.ResourceId = 395 )
    BEGIN
        DECLARE @ResRelId BIGINT;
        SET @ResRelId = (
                          SELECT    MAX(ResRelId)
                          FROM      syAdvantageResourceRelations
                        ) + 1;
        INSERT  INTO dbo.syAdvantageResourceRelations
                (
                 ResRelId
                ,ResourceId
                ,RelatedResourceId
                )
        VALUES  (
                 @ResRelId
                ,395
                ,268
                );
    END;
GO
--update sy resource for quick Lead page so that it displays in the list in the add udf to a page
-- also change "Resource" from "Quick Leads" to "Quick Lead"
UPDATE  syResources
SET     AllowSchlReqFlds = 1
       ,syResources.Resource = 'Quick Lead'
WHERE   ResourceID = 268;
GO
--change "Resource" from "Info" to "Lead"
UPDATE  syResources
SET     syResources.Resource = 'Lead'
WHERE   ResourceID = 170;
GO

UPDATE  syResources
SET     syResources.Resource = 'Prior Work'
WHERE   ResourceID = 92;
GO
UPDATE  syResources
SET     syResources.Resource = 'Prior Work'
WHERE   ResourceID = 146;
GO

-- ===============================================================================================
-- END US7032 Add UDF to Quick Leads Page
-- ===============================================================================================

-- ===============================================================================================
-- DE12535 Pre existing Phone numbers status is blank
-- Update Tables adLeadPhone () and adLeadEmails() replacing nulls values
-- because columns were added after migration script (Valid for QA and Dev DB's)
-- ===============================================================================================
    
-- Update and Changes in "adLeadPhone" table
-- update all StatusId with NULL value with StatusId of Active 
-- goes in Consolidated Script Before syncronize with new Constraints
IF EXISTS ( SELECT  1
            FROM    adLeadPhone AS ALP
            WHERE   ALP.StatusId IS NULL )
    BEGIN
        UPDATE  ALP
        SET     ALP.StatusId = dbo.UDF_GetSyStatusesValue('A')
        FROM    adLeadPhone AS ALP;
    END;
GO

IF EXISTS ( SELECT  1
            FROM    adLeadPhone AS ALP
            WHERE   ALP.IsBest IS NULL )
    BEGIN
        UPDATE  ALP
        SET     ALP.IsBest = 0
        FROM    adLeadPhone AS ALP;
    END;
GO

IF EXISTS ( SELECT  1
            FROM    adLeadPhone AS ALP
            WHERE   ALP.IsShowOnLeadPage IS NULL )
    BEGIN
        UPDATE  ALP
        SET     ALP.IsShowOnLeadPage = 0
        FROM    adLeadPhone AS ALP;
    END;
GO

-- Update and Changes in "adLeadEmail" table
-- update all StatusId with NULL value with StatusId of Active 
-- goes in Consolidated Script Before syncronize with new Constraints
IF EXISTS ( SELECT  1
            FROM    AdLeadEmail AS ALE
            WHERE   ALE.StatusId IS NULL )
    BEGIN
        UPDATE  ALE
        SET     ALE.StatusId = dbo.UDF_GetSyStatusesValue('A')
        FROM    AdLeadEmail AS ALE;
    END;
GO

IF EXISTS ( SELECT  1
            FROM    AdLeadEmail AS ALE
            WHERE   ALE.IsShowOnLeadPage IS NULL )
    BEGIN
        UPDATE  ALE
        SET     ALE.IsShowOnLeadPage = 0
        FROM    AdLeadEmail AS ALE;
    END;
GO


-- ===============================================================================================
-- END  --  DE12535 Pre existing Phone numbers status is blank
-- ===============================================================================================

-- ===============================================================================================
-- US8292: Ability to Sequence UDF fields on a page
-- Change Menu name for Maintenance UDF pages
-- JAGG 9/6/16 
-- ===============================================================================================
UPDATE  syResources
SET     Resource = 'Add Custom Field to Page'
WHERE   ResourceID = 244;
  --'Old Add UDF to a Page'

UPDATE  syMenuItems
SET     DisplayName = 'Add Custom Field to Page'
       ,MenuName = 'Add Custom Field to Page'
WHERE   ResourceId = 244;
  --'Old Add UDF to a Page'

UPDATE  syResources
SET     Resource = 'Create Custom Fields'
WHERE   ResourceID = 243;
  --'Old Add UDF Field to Page'

UPDATE  syMenuItems
SET     DisplayName = 'Create Custom Fields'
       ,MenuName = 'Create Custom Fields'
WHERE   ResourceId = 243;
  --'Old Add UDF Field to Page'
GO
-- ===============================================================================================
-- END JAGG  --  US8292: Ability to Sequence UDF fields on a page
-- ===============================================================================================
-- ===============================================================================================
-- US8292: Ability to Sequence UDF fields on a page
-- Change resources Prior Employment to Prior Work
-- JAGG 9/14/16 
-- ===============================================================================================
UPDATE  syResources
SET     syResources.Resource = 'Prior Work'
WHERE   ResourceID = 92;
GO
UPDATE  syResources
SET     syResources.Resource = 'Prior Work'
WHERE   ResourceID = 146;
GO
-- ===============================================================================================
-- END JAGG  --  US8292: Ability to Sequence UDF fields on a page
-- ===============================================================================================
-- ===============================================================================================
-- US8591: Admission - Notes: Display Comment and Advertisement notes from 
-- Lead Info page in Notes Page
-- JAGG 9/16/16 
-- ===============================================================================================

UPDATE  dbo.syNotesPageFields
SET     FieldCaption = 'Comments'
WHERE   PageFieldId = 2;
UPDATE  dbo.syNotesPageFields
SET     FieldCaption = 'Note'
WHERE   PageFieldId = 3;
UPDATE  dbo.syNotesPageFields
SET     FieldCaption = 'Comments'
WHERE   PageFieldId = 6;
UPDATE  dbo.syNotesPageFields
SET     FieldCaption = 'Comments'
WHERE   PageFieldId = 7;
UPDATE  dbo.syNotesPageFields
SET     FieldCaption = 'Comments'
WHERE   PageFieldId = 8;

GO
-- ===============================================================================================
-- END JAGG  --  US8591
-- ===============================================================================================
-- ===============================================================================================
-- START  --  US9298 : Enroll Lead Page
-- ===============================================================================================
DECLARE @NewResTblId INT
   ,@tblFldId INT;	
-----mapping for IsDisabled
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'IsDisabled'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'IsDisabled'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'IsDisabled'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for IsFirstTimeInSchool
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'IsFirstTimeInSchool'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'IsFirstTimeInSchool'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'IsFirstTimeInSchool'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;


-----mapping for IsFirstTimePostSecSchool
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'IsFirstTimePostSecSchool'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'IsFirstTimePostSecSchool'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'IsFirstTimePostSecSchool'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for CampusId
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'CampusId'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'CampusId'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'CampusId'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,1  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for PrgVersionTypeId
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'PrgVersionTypeId'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'PrgVersionTypeId'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'PrgVersionTypeId'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,1  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;


-----mapping for MidPtDate
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'MidPtDate'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'MidPtDate'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'MidPtDate'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for TransferDate
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'TransferDate'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'TransferDate'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'TransferDate'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;



-----mapping for EntranceInterviewDate
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'EntranceInterviewDate'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'EntranceInterviewDate'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'EntranceInterviewDate'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;


-----mapping for FAAdvisorId
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'FAAdvisorId'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'FAAdvisorId'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'FAAdvisorId'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for AcademicAdvisor
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'AcademicAdvisor'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'AcademicAdvisor'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'AcademicAdvisor'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;

-----mapping for badgenumber
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'badgenumber'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'badgenumber'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'badgenumber'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;
-----mapping for TransferHours
IF NOT EXISTS ( SELECT  *
                FROM    syResTblFlds
                WHERE   ResourceId = 174
                        AND TblFldsId IN ( SELECT   TblFldsId
                                           FROM     syTblFlds
                                           WHERE    FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'TransferHours'
                                                            ) ) )
    BEGIN
        IF EXISTS ( SELECT  TblFldsId
                    FROM    syTblFlds
                    WHERE   TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'arStuEnrollments'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'TransferHours'
                                        ) )
            BEGIN
                SET @tblFldId = (
                                  SELECT    TblFldsId
                                  FROM      syTblFlds
                                  WHERE     TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'arStuEnrollments'
                                                    )
                                            AND FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'TransferHours'
                                                        )
                                );
                SET @NewResTblId = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup

				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,174 -- ResourceId - smallint
                        ,@tblFldId  -- TblFldsId - int
                        ,0  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,NULL  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit

				        );	
            END;	
    END;
	GO
	--delete AdmissionsRep
IF EXISTS ( SELECT  *
            FROM    syResTblFlds
            WHERE   ResourceId = 174
                    AND TblFldsId IN ( SELECT   TblFldsId
                                       FROM     syTblFlds
                                       WHERE    FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'AdmissionsRep'
                                                        ) ) )
    BEGIN
        DELETE  FROM syResTblFlds
        WHERE   ResourceId = 174
                AND TblFldsId IN ( SELECT   TblFldsId
                                   FROM     syTblFlds
                                   WHERE    FldId = (
                                                      SELECT    FldId
                                                      FROM      syFields
                                                      WHERE     FldName = 'AdmissionsRep'
                                                    ) );
    END;
	
	GO

	---mandate tuition category
IF EXISTS ( SELECT  *
            FROM    syResTblFlds
            WHERE   ResourceId = 174
                    AND TblFldsId IN ( SELECT   TblFldsId
                                       FROM     syTblFlds
                                       WHERE    FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'TuitionCategoryId'
                                                        ) ) )
    BEGIN
        UPDATE  syResTblFlds
        SET     Required = 1
        WHERE   ResourceId = 174
                AND TblFldsId IN ( SELECT   TblFldsId
                                   FROM     syTblFlds
                                   WHERE    FldId = (
                                                      SELECT    FldId
                                                      FROM      syFields
                                                      WHERE     FldName = 'TuitionCategoryId'
                                                    ) );
    END;
	GO

-- ===============================================================================================
-- END  --  US9298 : Enroll Lead Page
-- ===============================================================================================

--  ==============================================================================================
--  US8629 Admission Notes Prior Work Comments in Notes Page 
--  Description: Add Prior Work to syNotesPageFields
--  JAGG
--  ==============================================================================================
IF NOT EXISTS ( SELECT  *
                FROM    dbo.syNotesPageFields
                WHERE   PageFieldId = 10 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 10  -- PageFieldId - int
                ,'Prior Work'  -- PageName - varchar(50)
                ,'Comments'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
--  ==============================================================================================
--  END US8629 TECH Synchronize Information between Lead and Student over the student Lead. 
--  END JAGG
--  ==============================================================================================

--  ==============================================================================================
--  START US9491 Move UnEnroll Leads to Academics 
--  ==============================================================================================
DECLARE @ParentId INT
   ,@MenuItemId INT
   ,@HierarchyId UNIQUEIDENTIFIER;
SET @ParentId = (
                  SELECT    ParentId
                  FROM      syMenuItems
                  WHERE     MenuName = 'Drop Student Courses'
                );
SET @HierarchyId = (
                     SELECT TOP 1
                            HierarchyId
                     FROM   dbo.syNavigationNodes
                     WHERE  ResourceId = 205
                   );
IF NOT EXISTS ( SELECT  *
                FROM    syMenuItems
                WHERE   MenuName = 'UnEnroll Students' )
    BEGIN
        INSERT  INTO syMenuItems
                (
                 MenuName
                ,DisplayName
                ,Url
                ,MenuItemTypeId
                ,ParentId
                ,DisplayOrder
                ,IsPopup
                ,ModDate
                ,ModUser
                ,IsActive
                ,ResourceId
                ,HierarchyId
                ,ModuleCode
                ,MRUType
                ,HideStatusBar
                )
        VALUES  (
                 'UnEnroll'
                ,'UnEnroll'
                ,'/AD/UnEnrollLeads.aspx'
                ,4
                ,@ParentId
                ,400
                ,0
                ,GETDATE()
                ,'support'
                ,1
                ,205
                ,@HierarchyId
                ,NULL
                ,NULL
                ,NULL
                );
    END;
GO
UPDATE  dbo.syResources
SET     Resource = 'UnEnroll Students'
WHERE   ResourceID = 205;
UPDATE  dbo.syMenuItems
SET     MenuName = 'UnEnroll Students'
       ,DisplayName = 'UnEnroll Students'
WHERE   Url = '/AD/UnEnrollLeads.aspx';
GO
-- ==============================================================================================
--  END US9491 Move UnEnroll Leads to Academics 
--  ==============================================================================================

--  ==============================================================================================
--  START US8366 Import Colleges to dbo.adColleges
--  ==============================================================================================
UPDATE  syMenuItems
SET     MenuName = 'High Schools & Colleges'
       ,DisplayName = 'High Schools & Colleges'
WHERE   Url = '/AD/HighSchools.aspx';
DELETE  FROM syMenuItems
WHERE   MenuName = 'Colleges';
UPDATE  syResources
SET     Resource = 'High Schools & Colleges'
WHERE   Resource = 'High Schools';
GO
IF NOT EXISTS ( SELECT  *
                FROM    syInstitutionTypes
                WHERE   TypeID = 1 )
    BEGIN
        INSERT  INTO syInstitutionTypes
        VALUES  ( 1,'Private' );
    END;
IF NOT EXISTS ( SELECT  *
                FROM    syInstitutionTypes
                WHERE   TypeID = 2 )
    BEGIN
        INSERT  INTO syInstitutionTypes
        VALUES  ( 2,'Public' );
    END;
GO
IF NOT EXISTS ( SELECT  *
                FROM    syInstitutionImportTypes
                WHERE   ImportTypeID = 1 )
    BEGIN
        INSERT  INTO syInstitutionImportTypes
        VALUES  ( 1,'School Defined' );
    END;
IF NOT EXISTS ( SELECT  *
                FROM    syInstitutionImportTypes
                WHERE   ImportTypeID = 2 )
    BEGIN
        INSERT  INTO syInstitutionImportTypes
        VALUES  ( 2,'Imported' );
    END;
GO
--  ==============================================================================================
--  END US8366 Import Colleges to dbo.adColleges

--  ==============================================================================================
--  ==============================================================================================
--  US9095 Migrate Admissions Pages to Student
--  Description: Update Menu to Prior Work and create menu for Student Contact
--               Eliminate menu for Phones. This functionality is cover by Student Contact
--  JAGG
--  ==============================================================================================
-- Create menu for Prior Work
UPDATE  dbo.syResources
SET     ResourceURL = '~/PL/PriorWorkPlacement.aspx'
WHERE   ResourceID = 92;

UPDATE  dbo.syMenuItems
SET     MenuName = 'Prior Work'
       ,DisplayName = 'Prior Work'
       ,Url = '/PL/PriorWorkPlacement.aspx'
WHERE   ResourceId = 92;
GO

-- Create menu for Student Contact in place of Address
UPDATE  dbo.syResources
SET     ResourceURL = '~/AR/StudentContacts.aspx'
       ,DisplayName = 'Student Contacts'
WHERE   ResourceID = 155;

UPDATE  dbo.syMenuItems
SET     MenuName = 'Student Contacts'
       ,DisplayName = 'Students Contacts'
       ,Url = '/AR/StudentContacts.aspx'
WHERE   ResourceId = 155;
GO

-- Eliminate menus for Phones
--DELETE  dbo.syMenuItems
--WHERE   ResourceId = 159; 
--GO
--  ==============================================================================================
--  END US9095 Migrate Admissions Pages to Student JAGG
--  ==============================================================================================
--  ==============================================================================================
--  START US9489 View UDF Sequence on Prior Work Page
--  ==============================================================================================
-- renaming "Prior Employment" as "Prior Work"
IF EXISTS ( SELECT  *
            FROM    syAdvantageResourceRelations
            WHERE   syAdvantageResourceRelations.RelatedResourceId = 146
                    AND syAdvantageResourceRelations.ResourceId = 395 )
    BEGIN
        UPDATE  syResources
        SET     syResources.Resource = 'Prior Work'
        WHERE   ResourceID = 146;
    END;
GO
--  ==============================================================================================
--  END US9489 View UDF Sequence on Prior Work Page
--  ==============================================================================================
--  ==============================================================================================
--  START US9637 Remove override requirements from the maintenance
--  ==============================================================================================
BEGIN
    DECLARE @MenuName VARCHAR(50);
    SET @MenuName = 'Override Requirements';
    DELETE  FROM dbo.syMenuItems
    WHERE   MenuName = @MenuName;
END;
GO
--  ==============================================================================================
--  END US9637 Remove override requirements from the maintenance
--  ==============================================================================================
--  ==============================================================================================
--  START US9744 State field on Enroll Page
--  ==============================================================================================
IF EXISTS ( SELECT  *
            FROM    syResTblFlds
            WHERE   ResourceId = 174
                    AND TblFldsId IN ( SELECT   TblFldsId
                                       FROM     syTblFlds
                                       WHERE    FldId = (
                                                          SELECT    FldId
                                                          FROM      syFields
                                                          WHERE     FldName = 'StateId'
                                                        ) ) )
    BEGIN 
        DELETE  FROM syResTblFlds
        WHERE   ResourceId = 174
                AND TblFldsId IN ( SELECT   TblFldsId
                                   FROM     syTblFlds
                                   WHERE    FldId = (
                                                      SELECT    FldId
                                                      FROM      syFields
                                                      WHERE     FldName = 'StateId'
                                                    ) ); 
    END;	
	GO
EXEC DEVELOPMENT_InsertUpdateFieldInResources 'EnrollStateId',72,16,NULL,NULL,1,NULL,'State',1,'adLeads','',NULL,'StateDescrip',174,0,'ddlStateId';
	GO
--  ==============================================================================================
--  END US9744 State field on Enroll Page
--  ==============================================================================================
--  ==============================================================================================
--  START US8045 Define Lead Groups
--  ==============================================================================================
BEGIN
    DECLARE @FldId INT
       ,@TblFldsId INT
       ,@TblId INT
       ,@ResTblFldsID INT; 
    SET @TblId = (
                   SELECT   TblId
                   FROM     syTables
                   WHERE    TblName = 'adLeadGroups'
                 );
    IF NOT EXISTS ( SELECT  *
                    FROM    syFields
                    WHERE   FldName = 'UseForStudentGroupTracking' )
        BEGIN
            SET @FldId = (
                           SELECT   MAX(FldId) + 1
                           FROM     syFields
                         );
            INSERT  INTO syFields
                    SELECT  @FldId
                           ,'UseForStudentGroupTracking'
                           ,FldTypeId
                           ,FldLen
                           ,DDLId
                           ,DerivedFld
                           ,SchlReq
                           ,LogChanges
                           ,Mask
                    FROM    syFields
                    WHERE   FldName = 'UseForScheduling';
        END;
    SET @FldId = (
                   SELECT TOP 1
                            FldId
                   FROM     syFields
                   WHERE    FldName = 'UseForStudentGroupTracking'
                 );
    IF NOT EXISTS ( SELECT  *
                    FROM    syTblFlds
                    WHERE   TblId = @TblId
                            AND FldId = @FldId )
        BEGIN
            SET @TblFldsId = (
                               SELECT   MAX(TblFldsId) + 1
                               FROM     syTblFlds
                             );
            INSERT  INTO dbo.syTblFlds
                    (
                     TblFldsId
                    ,TblId
                    ,FldId
                    ,CategoryId
                    ,FKColDescrip
	                )
            VALUES  (
                     @TblFldsId  -- TblFldsId - int
                    ,@TblId  -- TblId - int
                    ,@FldId -- FldId - int
                    ,NULL  -- CategoryId - int
                    ,NULL  -- FKColDescrip - varchar(50)
	                );
        END;
    SET @TblFldsId = (
                       SELECT DISTINCT
                                TblFldsId
                       FROM     syTblFlds
                       WHERE    TblId = @TblId
                                AND FldId = @FldId
                     );
    SET @ResTblFldsID = (
                          SELECT DISTINCT
                                    MAX(ResDefId) + 1
                          FROM      dbo.syResTblFlds
                        );
    IF NOT EXISTS ( SELECT  *
                    FROM    syResTblFlds
                    WHERE   ResourceId = 339
                            AND TblFldsId = @TblFldsId )
        BEGIN
            INSERT  INTO syResTblFlds
            VALUES  ( @ResTblFldsID,339,@TblFldsId,0,0,NULL,1 );
        END;
    DECLARE @FldCapId INT;
	
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syFldCaptions
                    WHERE   FldId = @FldId )
        BEGIN
            SET @FldCapId = (
                              SELECT    MAX(FldCapId) + 1
                              FROM      syFldCaptions
                            );
            INSERT  INTO dbo.syFldCaptions
                    (
                     FldCapId
                    ,FldId
                    ,LangId
                    ,Caption
                    ,FldDescrip
		            )
            VALUES  (
                     @FldCapId  -- FldCapId - int
                    ,@FldId  -- FldId - int
                    ,1  -- LangId - tinyint
                    ,'Use for Student Group Tracking'  -- Caption - varchar(100)
                    ,NULL  -- FldDescrip - varchar(150)
		            );
        END;
END;
GO
UPDATE  adLeadGroups
SET     UseForStudentGroupTracking = 0
WHERE   UseForStudentGroupTracking IS NULL;
GO

--  ==============================================================================================
--  END US8045 --  START US8045 Define Lead Groups

--  ==============================================================================================

-------------------------------------------------------------------------------------
--Troy: DE13101 - Normal-Chat-Adhoc report crashing the database
--------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--This section adds FK to saTransactions for the FundSourceId field
----------------------------------------------------------------------------------------------
--Note that for 3.8 the schema changes was added to the IDTI3.8 database in SQL Source Control
--IF NOT EXISTS ( SELECT  *
--                FROM    sys.foreign_keys
--                WHERE   object_id = OBJECT_ID(N'dbo.FK_saTransactions_saFundSources_FundSourceId_FundSourceId')
--                        AND parent_object_id = OBJECT_ID(N'dbo.saTransactions') )
--    ALTER TABLE dbo.saTransactions  WITH CHECK ADD  CONSTRAINT FK_saTransactions_saFundSources_FundSourceId_FundSourceId FOREIGN KEY(FundSourceId)
--    REFERENCES dbo.saFundSources (FundSourceId);
--GO


--------------------------------------------------------------------------------------
--This section exposes the FundSourceId field from saTransactions to the adhoc
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
--VARIABLE DECLARATIONS: DO NOT CHANGE
--------------------------------------------------------------------------------------
DECLARE @tblid INT;
DECLARE @fldid INT;
DECLARE @tablename VARCHAR(100);
DECLARE @fldname VARCHAR(100);
DECLARE @tblfldsid INT;
DECLARE @categoryname VARCHAR(100);
DECLARE @categoryid INT;
DECLARE @resourceid INT;
DECLARE @caption VARCHAR(100);
DECLARE @fkcoldescrip VARCHAR(50);

---------------------------------------------------------------------------------------------
--SET THE VARIABLES HERE FOR YOUR SPECIFIC REPORT
----------------------------------------------------------------------------------------------
SET @tablename = 'saTransactions';
SET @categoryname = 'Ledger';
SET @fldname = 'FundSourceId';
SET @caption = 'Fund Source';
SET @fkcoldescrip = 'FundSourceDescrip';


-------------------------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------------------------
SET @tblid = (
               SELECT   TblId
               FROM     dbo.syTables
               WHERE    TblName = @tablename
             );
SET @fldid = (
               SELECT   FldId
               FROM     dbo.syFields
               WHERE    FldName = @fldname
             );
SET @categoryid = (
                    SELECT  CategoryId
                    FROM    syFldCategories
                    WHERE   Descrip = @categoryname
                  );


IF NOT EXISTS ( SELECT  *
                FROM    dbo.syTblFlds
                WHERE   TblId = @tblid
                        AND FldId = @fldid )
    BEGIN
        SET @tblfldsid = (
                           SELECT   MAX(TblFldsId)
                           FROM     dbo.syTblFlds
                         ) + 1;

        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
					
                )
        VALUES  (
                 @tblfldsid
                , -- TblFldsId - int
                 @tblid
                , -- TblId - int
                 @fldid
                , -- FldId - int
                 @categoryid
                , -- CategoryId - int
                 @fkcoldescrip -- FKColDescrip - varchar(50)
					
                );
    END;  
	

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syFldCaptions
                WHERE   FldId = @fldid )
    BEGIN
        DECLARE @fldcapid INT;
        SET @fldcapid = (
                          SELECT    MAX(FldCapId)
                          FROM      dbo.syFldCaptions
                        ) + 1;

        INSERT  INTO dbo.syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                ,FldDescrip
		        )
        VALUES  (
                 @fldcapid
                , -- FldCapId - int
                 @fldid
                , -- FldId - int
                 1
                , -- LangId - tinyint
                 @caption
                , -- Caption - varchar(100)
                 NULL  -- FldDescrip - varchar(150)
		        );
    END; 
GO
-------------------------------------------------------------------------------------
--End Troy: DE13101 - Normal-Chat-Adhoc report crashing the database
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start US9492 - SPIKE:  Review Reports for Admissions table changes
--------------------------------------------------------------------------------------
BEGIN TRANSACTION ReportUpdate;
BEGIN TRY
    DECLARE @ResourceId INT;
    DECLARE @RptCaption VARCHAR(50);
    DECLARE @FieldName VARCHAR(50);
    DECLARE @oldFldName VARCHAR(50);
    DECLARE @FldTypeId INT;
    DECLARE @FldLen INT;
    DECLARE @DDLId INT;
    DECLARE @LogChanges BIT;
    DECLARE @TblName VARCHAR(50);
    DECLARE @CategoryId INT;
    DECLARE @FKColDescrip VARCHAR(100);
    DECLARE @FieldCaption VARCHAR(50);
    DECLARE @LanguageId INT;

--ZipCode (Lead Master(detail) Report)
    SET @ResourceId = 202;
    SET @RptCaption = 'Zip Code';
    SET @FieldName = 'ZipCode';
    SET @oldFldName = 'Zip';
    SET @FldTypeId = 200;
    SET @FldLen = 10;
    SET @DDLId = NULL;
    SET @LogChanges = 1;
    SET @TblName = 'adLeadAddresses';
    SET @CategoryId = NULL;
    SET @FKColDescrip = NULL;
    SET @FieldCaption = 'Zip Code';
    SET @LanguageId = 1;

    EXECUTE dbo.DEVELOPMENT_InsertUpdateFieldInRptParams @ResourceId,@RptCaption,@FieldName,@oldFldName,@FldTypeId,@FldLen,@DDLId,@LogChanges,@FieldCaption,
        @LanguageId,@TblName,@CategoryId,@FKColDescrip;

--- city (Lead Master(detail) Report)
    SET @ResourceId = 202; 
    SET @RptCaption = 'City';
    SET @FieldName = 'City';
    SET @oldFldName = 'City';
    SET @FldTypeId = 200;
    SET @FldLen = 80;
    SET @DDLId = NULL;
    SET @LogChanges = 1;
    SET @TblName = 'adLeadAddresses';
    SET @CategoryId = NULL;
    SET @FKColDescrip = NULL;
    SET @FieldCaption = 'City';
    SET @LanguageId = 1;

    EXECUTE dbo.DEVELOPMENT_InsertUpdateFieldInRptParams @ResourceId,@RptCaption,@FieldName,@oldFldName,@FldTypeId,@FldLen,@DDLId,@LogChanges,@FieldCaption,
        @LanguageId,@TblName,@CategoryId,@FKColDescrip;

   --- StateId (Lead Master(detail) Report)
    SET @ResourceId = 202;
    SET @RptCaption = 'State';
    SET @FieldName = 'StateId';
    SET @oldFldName = 'StateId';
    SET @FldTypeId = 72;
    SET @FldLen = 18;
    SET @DDLId = 1;
    SET @LogChanges = 1;
    SET @TblName = 'adLeadAddresses';
    SET @CategoryId = NULL;
    SET @FKColDescrip = NULL;
    SET @FieldCaption = 'State';
    SET @LanguageId = 1;

    EXECUTE dbo.DEVELOPMENT_InsertUpdateFieldInRptParams @ResourceId,@RptCaption,@FieldName,@oldFldName,@FldTypeId,@FldLen,@DDLId,@LogChanges,@FieldCaption,
        @LanguageId,@TblName,@CategoryId,@FKColDescrip;

--ZipCode (by Zip Code Detail Report)
    SET @ResourceId = 320;
    SET @RptCaption = 'Zip Code';
    SET @FieldName = 'ZipCode';
    SET @oldFldName = 'Zip';
    SET @FldTypeId = 200;
    SET @FldLen = 10;
    SET @DDLId = NULL;
    SET @LogChanges = 1;
    SET @TblName = 'adLeadAddresses';
    SET @CategoryId = NULL;
    SET @FKColDescrip = NULL;
    SET @FieldCaption = 'Zip Code';
    SET @LanguageId = 1;

    EXECUTE dbo.DEVELOPMENT_InsertUpdateFieldInRptParams @ResourceId,@RptCaption,@FieldName,@oldFldName,@FldTypeId,@FldLen,@DDLId,@LogChanges,@FieldCaption,
        @LanguageId,@TblName,@CategoryId,@FKColDescrip;    

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION ReportUpdate;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION ReportUpdate;
    END;
GO
-------------------------------------------------------------------------------------
--End US9492 - SPIKE:  Review Reports for Admissions table changes
--------------------------------------------------------------------------------------

-- BEGIN DE13077 DATA CHANGE
-----------------------------------------------------------------------
-- ********************************************************************************************************
-- DE13077: Search Bar redirecting to Lead info page
-- Author: zlesnik
-- ********************************************************************************************************

--IF NOT EXISTS ( SELECT  *
--                FROM    syResources
--                WHERE   ResourceID = 837 )
--    BEGIN
 
--        DECLARE @ResID INTEGER = 837; -- put here your resource ID for the page
--        DECLARE @PageResourceName VARCHAR(20) = 'History'; -- name of your page
--        DECLARE @PageUrl VARCHAR(100) = '~/AD/AleadHistory.aspx'; -- the url of page in advantage
--        DECLARE @MRUType INTEGER = 4; -- NULL:None (page no referred to specific individual entity)
--                             -- 1: Student 
--                             -- 2:?, 3:?
--                             -- 4: Admission
--        DECLARE @PageDisplayName VARCHAR(20) = 'History'; -- Put the display name for your page
 
--        INSERT  syResources
--                (
--                 ResourceID
--                ,Resource
--                ,ResourceTypeID
--                ,ResourceURL
--                ,SummListId
--                ,ChildTypeId
--                ,ModDate
--                ,ModUser
--                ,AllowSchlReqFlds
--                ,MRUTypeId
--                ,UsedIn
--                ,TblFldsId
--                ,DisplayName
--                )
--        VALUES  (
--                 @ResID
--                ,@PageResourceName
--                ,3
--                ,@PageUrl
--                ,NULL
--                ,NULL
--                ,GETDATE()
--                ,'support'
--                ,0
--                ,@MRUType
--                ,975
--                ,NULL
--                ,@PageDisplayName
--                );
--    END;
--GO
-----------------------------------------------------------------------
-- END DE13077 DATA CHANGE
-----------------------------------------------------------------------
--  ==============================================================================================
--  START US9487 View UDF Sequence on Education Page
--  ==============================================================================================
-- renaming "Education" as "Prior Education"
IF EXISTS ( SELECT  *
            FROM    syAdvantageResourceRelations
            WHERE   syAdvantageResourceRelations.RelatedResourceId = 145
                    AND syAdvantageResourceRelations.ResourceId = 395 )
    BEGIN
        UPDATE  syResources
        SET     syResources.Resource = 'Prior Education'
        WHERE   ResourceID = 145;
    END;
GO
--  ==============================================================================================
--  END US9487 View UDF Sequence on Education Page
--  ==============================================================================================
-----------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start US9301 - MRU Tool Allows Users to Bypass Permission Settings
--Start TA21707: Insert Values into syUniversalSearchModules
--------------------------------------------------------------------------------------
BEGIN TRANSACTION InsertUniversalSearchModules;
BEGIN TRY
    DECLARE @DateNow DATETIME;
    DECLARE @ResourceID TINYINT; 
    TRUNCATE TABLE syUniversalSearchModules;
    SET @DateNow = GETDATE();
   
    SET @ResourceID = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   ResourceTypeID = 1
                                AND Resource = 'Academics'
                      );
    INSERT  INTO syUniversalSearchModules
    VALUES  ( 1,'Student','Student',@DateNow,'sa',@ResourceID );
   
    SET @DateNow = GETDATE();
   
    SET @ResourceID = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   ResourceTypeID = 1
                                AND Resource = 'Admissions'
                      );
    INSERT  INTO syUniversalSearchModules
    VALUES  ( 2,'Lead','Lead',@DateNow,'sa',@ResourceID );
    SET @DateNow = GETDATE();
   
    SET @ResourceID = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   ResourceTypeID = 1
                                AND Resource = 'Placement'
                      );
    INSERT  INTO syUniversalSearchModules
    VALUES  ( 3,'Employer','Employer',@DateNow,'sa',@ResourceID );
    SET @DateNow = GETDATE();
   
    SET @ResourceID = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   ResourceTypeID = 1
                                AND Resource = 'Human Resources'
                      );
    INSERT  INTO syUniversalSearchModules
    VALUES  ( 4,'Employee','Employee',@DateNow,'sa',@ResourceID );
   
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION InsertUniversalSearchModules;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
 
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION InsertUniversalSearchModules;
    END;
GO
-------------------------------------------------------------------------------------
--End TA21707: Insert Values into syUniversalSearchModules
--End US9301 - MRU Tool Allows Users to Bypass Permission Settings
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start DE13092: Migration Lead Transaction Of Type Fee Prior 3.8
--------------------------------------------------------------------------------------
BEGIN TRANSACTION MigrateRequirementFee;
BEGIN TRY
    DECLARE @StatusId UNIQUEIDENTIFIER;
    DECLARE @TransCodeId UNIQUEIDENTIFIER;
    DECLARE @TransCodeDescrip VARCHAR(50);
    DECLARE @TransCodeCode VARCHAR(16);
    DECLARE @RequirementId UNIQUEIDENTIFIER;
    DECLARE @CampusGroupId UNIQUEIDENTIFIER;
    DECLARE @RequirementEffectiveDateId UNIQUEIDENTIFIER;

    SET @StatusId = (
                      SELECT    StatusId
                      FROM      syStatuses
                      WHERE     StatusCode = 'A'
                    );
    SET @CampusGroupId = (
                           SELECT TOP 1
                                    CampGrpId
                           FROM     syCampGrps
                           WHERE    IsAllCampusGrp = 1
                                    AND StatusId = @StatusId
                         );

    DECLARE TRANSACTION_CODES CURSOR
    FOR
        SELECT  TransCodeId
               ,TransCodeDescrip
               ,TransCodeCode
        FROM    saTransCodes
        WHERE   SysTransCodeId = (SELECT SysTransCodeId FROM saSysTransCodes WHERE Description = 'Applicant Fee');

    OPEN TRANSACTION_CODES;   
    FETCH NEXT FROM TRANSACTION_CODES INTO @TransCodeId,@TransCodeDescrip,@TransCodeCode;
    WHILE @@FETCH_STATUS = 0
        BEGIN   
			  
            IF NOT EXISTS ( SELECT  *
                            FROM    dbo.adReqs
                            WHERE   Code = @TransCodeCode
                                    AND Descrip = @TransCodeDescrip )
                BEGIN
                    --PRINT 'REQUIREMENT DOES NOT EXIST:' + @TransCodeDescrip;

                    SET @RequirementId = NEWID();
                    SET @RequirementEffectiveDateId = NEWID();

                    IF EXISTS ( SELECT  *
                                FROM    dbo.adReqs
                                WHERE   Descrip = '$50 Application Fee'
                                        AND adReqTypeId = 7 )
                        BEGIN
                            --PRINT '$50 Application Fee REQUIREMENT EXIST';
                            SET @RequirementId = (
                                                   SELECT TOP 1
                                                            adReqId
                                                   FROM     dbo.adReqs
                                                   WHERE    Descrip = '$50 Application Fee'
                                                            AND adReqTypeId = 7
                                                   ORDER BY ModDate DESC
                                                 );

                            UPDATE  adLeadTransactions
                            SET     LeadRequirementId = @RequirementId
                            WHERE   TransCodeId = @TransCodeId
                                    AND LeadRequirementId IS NULL;

                            INSERT  INTO adLeadTranReceived
                                    SELECT  NEWID()
                                           ,LeadId
                                           ,@RequirementId
                                           ,TransDate
                                           ,CASE Voided
                                              WHEN 0 THEN 1
                                              WHEN 1 THEN 0
                                            END
                                           ,TransDate
                                           ,ModDate
                                           ,ModUser
                                           ,0
                                           ,NULL
                                    FROM    adLeadTransactions
                                    WHERE   TransCodeId = @TransCodeId
                                            AND LeadRequirementId = @RequirementId;

                            --PRINT 'UPDATED TRANSACTIONS WITH $50 Application Fee REQUIREMENT';

                        END;
                    ELSE
                        BEGIN
                            --PRINT '$50 Application Fee DOES NOT REQUIREMENT EXIST, CREATE THE ' + @TransCodeDescrip + ' REQUIREMENT';
                            INSERT  INTO dbo.adReqs
                                    (
                                     adReqId
                                    ,Code
                                    ,Descrip
                                    ,StatusId
                                    ,adReqTypeId
                                    ,ModUser
                                    ,ModDate
                                    ,ReqforEnrollment
                                    ,ReqforFinancialAid
                                    ,ReqforGraduation
                                    ,CampGrpId
                                    ,ModuleId
								    )
                            VALUES  (
                                     @RequirementId
                                    ,@TransCodeCode
                                    ,@TransCodeDescrip
                                    ,@StatusId
                                    ,7
                                    ,'Support'
                                    ,GETDATE()
                                    ,1
                                    ,0
                                    ,0
                                    ,@CampusGroupId
                                    ,2
								    );

                            --PRINT 'REQUIREMENT INSERTED ';
							--PRINT @RequirementId

							/*(IS MANDATORY OR NOT?)*/
                            INSERT  INTO adReqsEffectiveDates
                                    (
                                     adReqEffectiveDateId
                                    ,adReqId
                                    ,StartDate
                                    ,EndDate
                                    ,MinScore
                                    ,ModUser
                                    ,ModDate
                                    ,MandatoryRequirement
                                    ,ValidDays
								    )
                            VALUES  (
                                     @RequirementEffectiveDateId
                                    ,@RequirementId
                                    ,CAST('19900101' AS DATETIME)
                                    ,NULL
                                    ,NULL
                                    ,'Support'
                                    ,GETDATE()
                                    ,0
                                    ,NULL
								    );

                            --PRINT 'EFFECTIVE DATE INSERTED:';
							--PRINT @RequirementEffectiveDateId

                            INSERT  INTO adReqLeadGroups
                                    SELECT  NEWID()
                                           ,LeadGrpId
                                           ,'Support'
                                           ,GETDATE()
                                           ,0/* (IS REQUIRED OR NOT?)*/
                                           , @RequirementEffectiveDateId
                                    FROM    adLeadGroups
                                    WHERE   StatusId = @StatusId;

                            --PRINT 'REQUIREMENT LEAD GROUP INSERTED';

							--INSERT INTO AdLeadTranReceived
				
                            UPDATE  adLeadTransactions
                            SET     LeadRequirementId = @RequirementId
                            WHERE   TransCodeId = @TransCodeId
                                    AND LeadRequirementId IS NULL;

                            INSERT  INTO adLeadTranReceived
                                    SELECT  NEWID()
                                           ,LeadId
                                           ,@RequirementId
                                           ,TransDate
                                           ,CASE Voided
                                              WHEN 0 THEN 1
                                              WHEN 1 THEN 0
                                            END
                                           ,TransDate
                                           ,ModDate
                                           ,ModUser
                                           ,0
                                           ,NULL
                                    FROM    adLeadTransactions
                                    WHERE   TransCodeId = @TransCodeId
                                            AND LeadRequirementId = @RequirementId;

                            --PRINT 'TRANSACTIONS UPDATED FOR: ' + @TransCodeDescrip;
                        END;
                END;
            ELSE
                BEGIN
                    IF EXISTS ( SELECT  *
                                FROM    dbo.adReqs
                                WHERE   Descrip = '$50 Application Fee'
                                        AND adReqTypeId = 7 )
                        BEGIN
                            --PRINT '$50 Application Fee REQUIREMENT EXIST';
                            SET @RequirementId = (
                                                   SELECT TOP 1
                                                            adReqId
                                                   FROM     dbo.adReqs
                                                   WHERE    Descrip = '$50 Application Fee'
                                                            AND adReqTypeId = 7
                                                   ORDER BY ModDate DESC
                                                 );
                            DECLARE @OldRequirementId UNIQUEIDENTIFIER;
                            SET @OldRequirementId = (
                                                      SELECT TOP 1
                                                                adReqId
                                                      FROM      dbo.adReqs
                                                      WHERE     Code = @TransCodeCode
                                                                AND Descrip = @TransCodeDescrip
                                                    );
                            SET @RequirementEffectiveDateId = (
                                                              SELECT TOP 1
                                                                        adReqEffectiveDateId
                                                              FROM      dbo.adReqsEffectiveDates
                                                              WHERE     adReqId = @OldRequirementId
                                                            );
						
						
                            UPDATE  adLeadTransactions
                            SET     LeadRequirementId = @RequirementId
                            WHERE   TransCodeId = @TransCodeId
                                    AND LeadRequirementId = @OldRequirementId;

                            UPDATE  adLeadTranReceived
                            SET     DocumentId = @RequirementId
                            WHERE   DocumentId = @OldRequirementId
                                    AND ModDate = (
                                                    SELECT  ModDate
                                                    FROM    adLeadTransactions
                                                    WHERE   TransCodeId = @TransCodeId
                                                            AND LeadRequirementId = @OldRequirementId
                                                  );
						
                            --PRINT 'UPDATED TRANSACTIONS WITH OLD REQUIREMENT OF ' + @TransCodeDescrip + ' WITH $50 Application Fee REQUIREMENT';

                            DELETE  FROM adReqLeadGroups
                            WHERE   adReqEffectiveDateId = @RequirementEffectiveDateId;

                            DELETE  FROM adReqsEffectiveDates
                            WHERE   adReqEffectiveDateId = @RequirementEffectiveDateId;
						
                            DELETE  FROM adReqs
                            WHERE   adReqId = @OldRequirementId;

                            --PRINT 'DELETED REQUIREMENT ' + @TransCodeDescrip;

                        END;
					----Find the Id for the $50 Applicant Fee of type of fee @ReqAppFee
					----Find the Id for Registration Fee with code Regfee @ReqRegFee
					----Update the adLeadTransactions and adLeadTranReceived where LeadRequirementId = ReqAppFee with the Id of ReqRegFee
                END;
			  
            FETCH NEXT FROM TRANSACTION_CODES INTO @TransCodeId,@TransCodeDescrip,@TransCodeCode;
        END;   
    CLOSE TRANSACTION_CODES;   
    DEALLOCATE TRANSACTION_CODES;
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION MigrateRequirementFee;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
  
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
		--PRINT 'COMMIT'
        COMMIT TRANSACTION MigrateRequirementFee;
    END;

GO
-------------------------------------------------------------------------------------
--End DE13092: Migration Lead Transaction Of Type Fee Prior 3.8
-------------------------------------------------------------------------------------
--  ==============================================================================================
--  US9095 Migrate Admissions Pages to Student
--  Description: Update Menu to Prior Work and create menu for Student Contact
--               Eliminate menu for Phones. This functionality is cover by Student Contact
--  JAGG
--  ==============================================================================================
-------------------------------------------------------------------------------------
--start US9492: SPIKE:  Review Reports for Admissions table changes
-------------------------------------------------------------------------------------
DECLARE @newTableName VARCHAR(50)
   ,@fldName VARCHAR(50)
   ,@oldTableName VARCHAR(50)
   --,@headerCaption VARCHAR(50)
   ,@newFldName VARCHAR(50);
 -- pass value only if the fld is different than @fldName else pass null
   --CountryId
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'CountryId'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'Country';
        SET @oldTableName = 'adLeads';
   --SET @headerCaption ='Country'
        SET @newFldName = 'CountryId';   
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
   --CountyId
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'CountyId'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'County';
        SET @oldTableName = 'adLeads';
   --SET @headerCaption ='County'
        SET @newFldName = 'CountyId';   
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
   --other state
IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'State'
                        AND FldTypeId = 200 )
    BEGIN
	
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'OtherState';
        SET @oldTableName = 'adLeads';
   --SET @headerCaption ='Other State Not Listed'
        SET @newFldName = 'State';
        IF NOT EXISTS ( SELECT  *
                        FROM    syFields
                        WHERE   FldName = 'State'
                                AND FldTypeId = 200 )
            BEGIN
                INSERT  INTO syFields
                        (
                         FldId
                        ,FldName
                        ,FldTypeId
                        ,FldLen
                        ,LogChanges
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldId)
                           FROM     syFields
                         ) + 1
                        ,'State'
                        ,200
                        ,100
                        ,1
                        );
                INSERT  INTO syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(TblFldsId)
                           FROM     syTblFlds
                         ) + 1
                        ,(
                           SELECT   TblId
                           FROM     syTables
                           WHERE    TblName = 'adLeadAddresses'
                         )
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = 'State'
                                    AND FldTypeId = 200
                         )
                        ,(
                           SELECT   CategoryId
                           FROM     syFldCategories
                           WHERE    EntityId = 395
                                    AND Descrip = 'General Information'
                         )
                        ,''  -- FKColDescrip - varchar(50)
                        );
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = 'State'
                                    AND FldTypeId = 200
                         )
                        ,1
                        ,'Other State Not Listed'
						
                        );
            END;    
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
      ---Address1 --uncomment
--	  PRINT ' start Address 1'
SET @newTableName = 'adLeadAddresses';
SET @fldName = 'Address1';
SET @oldTableName = 'adLeads';
--   SET @headerCaption ='Address 1'
SET @newFldName = NULL;
EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
--PRINT ' end Address 1'
      ---Address2 --uncomment
--	  PRINT 'start Address 2'
SET @newTableName = 'adLeadAddresses';
SET @fldName = 'Address2';
SET @oldTableName = 'adLeads';
--   SET @headerCaption ='Address 2'
SET @newFldName = NULL;
EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
--PRINT 'end Address 2'
   ---ForeignZip --uncomment
IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'IsInternational' )
    BEGIN 
        INSERT  INTO syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,DDLId
                ,DerivedFld
                ,SchlReq
                ,LogChanges
                ,Mask
                )
        VALUES  (
                 (
                   SELECT   MAX(FldId)
                   FROM     syFields
                 ) + 1
                ,'IsInternational'  -- FldName - varchar(200)
                ,11  -- FldTypeId - int
                ,2  -- FldLen - int
                ,NULL  -- DDLId - int
                ,0  -- DerivedFld - bit
                ,NULL  -- SchlReq - bit
                ,NULL  -- LogChanges - bit
                ,NULL  -- Mask - varchar(50)
                );
        INSERT  INTO syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
                )
        VALUES  (
                 (
                   SELECT   MAX(TblFldsId)
                   FROM     syTblFlds
                 ) + 1
                ,(
                   SELECT   TblId
                   FROM     syTables
                   WHERE    TblName = 'adLeadAddresses'
                 )
                ,(
                   SELECT   FldId
                   FROM     syFields
                   WHERE    FldName = 'IsInternational'
                 )
                ,(
                   SELECT   CategoryId
                   FROM     syFldCategories
                   WHERE    EntityId = 395
                            AND Descrip = 'General Information'
                 )
                ,''  -- FKColDescrip - varchar(50)
                );
        INSERT  INTO syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                )
        VALUES  (
                 (
                   SELECT   MAX(FldCapId)
                   FROM     syFldCaptions
                 ) + 1
                ,(
                   SELECT   FldId
                   FROM     syFields
                   WHERE    FldName = 'IsInternational'
                 )
                ,1
                ,'IsInternational'
						
                );
    END;	
--PRINT 'start ForeignZip'
IF NOT EXISTS ( SELECT  FldId
                FROM    syFields
                WHERE   FldName = 'IsInternational' )
    BEGIN
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'ForeignZip';
        SET @oldTableName = 'adLeads';
--   SET @headerCaption ='International'
        SET @newFldName = 'IsInternational';

        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
--PRINT 'end ForeignZip'
---StateId --uncomment
SET @newTableName = 'adLeadAddresses';
SET @fldName = 'StateId';
SET @oldTableName = 'adLeads';
--   SET @headerCaption ='State'
SET @newFldName = NULL;   
EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
---City 
--PRINT 'start city'--uncomment
SET @newTableName = 'adLeadAddresses';
SET @fldName = 'City';
SET @oldTableName = 'adLeads';
--   SET @headerCaption ='City'
SET @newFldName = NULL;
EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
--PRINT 'end city'
-----AddressType
--PRINT 'start AddressTypeId'
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'AddressTypeId'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN

        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'AddressType';
        SET @oldTableName = 'adLeads';
--   SET @headerCaption ='Address Type'
        SET @newFldName = 'AddressTypeId';  
        IF NOT EXISTS ( SELECT  *
                        FROM    syTblFlds
                        WHERE   TblId = (
                                          SELECT    TblId
                                          FROM      syTables
                                          WHERE     TblName = 'adLeadAddresses'
                                        )
                                AND FldId = (
                                              SELECT    FldId
                                              FROM      syFields
                                              WHERE     FldName = 'AddressTypeId'
                                            ) )
            BEGIN
                INSERT  INTO syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(TblFldsId)
                           FROM     syTblFlds
                         ) + 1
                        ,(
                           SELECT   TblId
                           FROM     syTables
                           WHERE    TblName = 'adLeadAddresses'
                         )
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = 'AddressTypeId'
                         )
                        ,(
                           SELECT   CategoryId
                           FROM     syFldCategories
                           WHERE    EntityId = 395
                                    AND Descrip = 'General Information'
                         )
                        ,''  -- FKColDescrip - varchar(50)
                        );
            END;
   
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;
--PRINT 'end AddressTypeId'
-----Zip
--PRINT 'start ZipCode'
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'ZipCode'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'Zip';
        SET @oldTableName = 'adLeads';
--   SET @headerCaption ='Zip Code'
        SET @newFldName = 'ZipCode';   
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;
--PRINT 'end ZipCode'
---AddressStatus --uncomment
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'StatusId'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadAddresses'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        SET @newTableName = 'adLeadAddresses';
        SET @fldName = 'AddressStatus';
        SET @oldTableName = 'adLeads';
--   SET @headerCaption ='Status'
        SET @newFldName = 'StatusId';
  
        IF NOT EXISTS ( SELECT  *
                        FROM    syTblFlds
                        WHERE   FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'StatusId'
                                        )
                                AND TblId = (
                                              SELECT    TblId
                                              FROM      syTables
                                              WHERE     TblName = 'adLeadAddresses'
                                            ) )
            BEGIN
                INSERT  INTO syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(TblFldsId)
                           FROM     syTblFlds
                         ) + 1
                        ,(
                           SELECT   TblId
                           FROM     syTables
                           WHERE    TblName = 'adLeadAddresses'
                         )
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = 'StatusId'
                         )
                        ,(
                           SELECT   CategoryId
                           FROM     syFldCategories
                           WHERE    EntityId = 395
                                    AND Descrip = 'General Information'
                         )
                        ,''  -- FKColDescrip - varchar(50)
                        );
            END;
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
-----ForeignPhone
--PRINT 'start IsForeignPhone'
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   FldId = (
                                  SELECT    FldId
                                  FROM      syFields
                                  WHERE     FldName = 'IsForeignPhone'
                                )
                        AND TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeadPhone'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        SET @newTableName = 'adLeadPhone';
        SET @fldName = 'ForeignPhone';
        SET @oldTableName = 'adLeads';
--   SET @headerCaption ='International'
        SET @newFldName = 'IsForeignPhone';   
        EXEC DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields @newTableName,@fldName,@oldTableName,@newFldName;
    END;	
--PRINT 'end IsForeignPhone'
-----PhoneType
--PRINT	'start PhoneTypeId' --todo ceheck if you need calculated SQL
--   SET @newTableName = 'adLeadPhone'
--   SET @fldName ='PhoneType'
--   SET @oldTableName ='adLeads'
----   SET @headerCaption ='Type'
--   SET @newFldName = 'PhoneTypeId'    
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = (
                                  SELECT    TblId
                                  FROM      syTables
                                  WHERE     TblName = 'adLeadPhone'
                                )
                        AND FldId = (
                                      SELECT    FldId
                                      FROM      syFields
                                      WHERE     FldName = 'PhoneTypeId'
                                    )
                        AND CategoryId IS NOT NULL )
    BEGIN
        INSERT  INTO syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
                )
        VALUES  (
                 (
                   SELECT   MAX(TblFldsId)
                   FROM     syTblFlds
                 ) + 1
                ,(
                   SELECT   TblId
                   FROM     syTables
                   WHERE    TblName = 'adLeadPhone'
                 )
                ,(
                   SELECT   FldId
                   FROM     syFields
                   WHERE    FldName = 'PhoneTypeId'
                 )
                ,(
                   SELECT   CategoryId
                   FROM     syFldCategories
                   WHERE    EntityId = 395
                            AND Descrip = 'General Information'
                 )
                ,''  -- FKColDescrip - varchar(50)
                );
        INSERT  INTO dbo.syFieldCalculation
                (
                 FldId
                ,CalculationSql
                )
        VALUES  (
                 (
                   SELECT   FldId
                   FROM     syFields
                   WHERE    FldName = 'PhoneTypeId'
                 )
                ,'(SELECT TOP 1 st.PhoneTypeDescrip FROM adLeadPhone as lp
INNER JOIN syPhoneType as st ON st.PhoneTypeId = lp.PhoneTypeId
INNER JOIN syStatuses AS ss ON ss.StatusId = lp.StatusId
WHERE lp.isbest = 1 AND lp.Position = 1 AND ss.StatusCode = ''A'' AND lp.LeadId = adleads.LeadId ORDER BY lp.ModDate DESC) AS PhoneTypeId'
                );
   
        UPDATE  syRptAdHocFields
        SET     TblFldsId = (
                              SELECT    TblFldsId
                              FROM      syTblFlds
                              WHERE     TblId = (
                                                  SELECT    TblId
                                                  FROM      syTables
                                                  WHERE     TblName = 'adLeadPhone'
                                                )
                                        AND FldId = (
                                                      SELECT    FldId
                                                      FROM      syFields
                                                      WHERE     FldName = 'PhoneTypeId'
                                                    )
                            )
        WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                                  FROM      syRptAdHocFields
                                  WHERE     TblFldsId = (
                                                          SELECT    TblFldsId
                                                          FROM      syTblFlds
                                                          WHERE     TblId = (
                                                                              SELECT    TblId
                                                                              FROM      syTables
                                                                              WHERE     TblName = 'adLeads'
                                                                            )
                                                                    AND FldId = (
                                                                                  SELECT    FldId
                                                                                  FROM      syFields
                                                                                  WHERE     FldName = 'PhoneType'
                                                                                )
                                                        )
                                            AND ResourceId IN ( SELECT DISTINCT
                                                                        UR.ResourceId
                                                                FROM    syUserResources UR
                                                                       ,syUserResPermissions UP
                                                                WHERE   UR.ResourceId = UP.UserResourceId
                                                                        AND UP.ResourceId = 189 ) );

    END;
IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = '[Phone - Best]' )
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    syTblFlds
                        WHERE   TblId = (
                                          SELECT    TblId
                                          FROM      syTables
                                          WHERE     TblName = 'adLeadPhone'
                                        )
                                AND FldId = (
                                              SELECT    FldId
                                              FROM      syFields
                                              WHERE     FldName = '[Phone - Best]'
                                            ) )
            BEGIN
                INSERT  INTO syFields
                        (
                         FldId
                        ,FldName
                        ,FldTypeId
                        ,FldLen
                        ,LogChanges
                        )
                VALUES  (
                         ( (
                             SELECT MAX(FldId)
                             FROM   syFields
                           ) + 1 )
                        ,'[Phone - Best]'
                        ,200
                        ,50
                        ,1
                        );
                INSERT  INTO syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(TblFldsId)
                           FROM     syTblFlds
                         ) + 1
                        ,(
                           SELECT   TblId
                           FROM     syTables
                           WHERE    TblName = 'adLeadPhone'
                         )
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Phone - Best]'
                         )
                        ,(
                           SELECT   CategoryId
                           FROM     syFldCategories
                           WHERE    EntityId = 395
                                    AND Descrip = 'General Information'
                         )
                        ,''  -- FKColDescrip - varchar(50)
                        );
                INSERT  INTO dbo.syFieldCalculation
                        (
                         FldId
                        ,CalculationSql
                        )
                VALUES  (
                         (
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Phone - Best]'
                         )
                        ,'(SELECT TOP 1 lp.Phone FROM adLeadPhone as lp
INNER JOIN syStatuses AS ss ON ss.StatusId = lp.StatusId
WHERE lp.isbest = 1 AND lp.Position = 1 AND ss.StatusCode = ''A'' AND lp.LeadId = adleads.LeadId ORDER BY lp.ModDate DESC) AS [Phone - Best]'
                        );
   
                UPDATE  syRptAdHocFields
                SET     TblFldsId = (
                                      SELECT    TblFldsId
                                      FROM      syTblFlds
                                      WHERE     TblId = (
                                                          SELECT    TblId
                                                          FROM      syTables
                                                          WHERE     TblName = 'adLeadPhone'
                                                        )
                                                AND FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = '[Phone - Best]'
                                                            )
                                    )
                WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                                          FROM      syRptAdHocFields
                                          WHERE     TblFldsId = (
                                                                  SELECT    TblFldsId
                                                                  FROM      syTblFlds
                                                                  WHERE     TblId = (
                                                                                      SELECT    TblId
                                                                                      FROM      syTables
                                                                                      WHERE     TblName = 'adLeads'
                                                                                    )
                                                                            AND FldId = (
                                                                                          SELECT    FldId
                                                                                          FROM      syFields
                                                                                          WHERE     FldName = 'Phone'
                                                                                        )
                                                                )
                                                    AND ResourceId IN ( SELECT DISTINCT
                                                                                UR.ResourceId
                                                                        FROM    syUserResources UR
                                                                               ,syUserResPermissions UP
                                                                        WHERE   UR.ResourceId = UP.UserResourceId
                                                                                AND UP.ResourceId = 189 ) );

                UPDATE  syTblFlds
                SET     CategoryId = NULL
                WHERE   TblFldsId = (
                                      SELECT    TblFldsId
                                      FROM      syTblFlds
                                      WHERE     TblId = (
                                                          SELECT    TblId
                                                          FROM      syTables
                                                          WHERE     TblName = 'adLeads'
                                                        )
                                                AND FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = 'Phone'
                                                            )
                                    );
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Phone - Best]'
                         )
                        ,1
                        ,'Phone - Best'
						 );

            END; 	
    END;



IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = '[Comments]' )
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    syTblFlds
                        WHERE   TblId = (
                                          SELECT    TblId
                                          FROM      syTables
                                          WHERE     TblName = 'adLeads'
                                        )
                                AND FldId = (
                                              SELECT    FldId
                                              FROM      syFields
                                              WHERE     FldName = '[Comments]'
                                            ) )
            BEGIN
                INSERT  INTO syFields
                        (
                         FldId
                        ,FldName
                        ,FldTypeId
                        ,FldLen
                        ,LogChanges
                        )
                VALUES  (
                         ( (
                             SELECT MAX(FldId)
                             FROM   syFields
                           ) + 1 )
                        ,'[Comments]'
                        ,200
                        ,2000
                        ,1
                        );
                INSERT  INTO syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(TblFldsId)
                           FROM     syTblFlds
                         ) + 1
                        ,(
                           SELECT   TblId
                           FROM     syTables
                           WHERE    TblName = 'adLeads'
                         )
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Comments]'
                         )
                        ,(
                           SELECT   CategoryId
                           FROM     syFldCategories
                           WHERE    EntityId = 395
                                    AND Descrip = 'General Information'
                         )
                        ,''  -- FKColDescrip - varchar(50)
                        );
                INSERT  INTO dbo.syFieldCalculation
                        (
                         FldId
                        ,CalculationSql
                        )
                VALUES  (
                         (
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Comments]'
                         )
                        ,'(SELECT TOP 1   NoteText
          FROM      AllNotes AS AN
          INNER JOIN adLead_Notes AS LN ON LN.NotesId = AN.NotesId
          INNER JOIN adLeads AS L  ON L.LeadId = LN.LeadId
		    WHERE adLeads.LeadId = L.LeadId 
		  ORDER BY AN.ModDate desc) AS Comments'
                        );
  
                UPDATE  syRptAdHocFields
                SET     TblFldsId = (
                                      SELECT    TblFldsId
                                      FROM      syTblFlds
                                      WHERE     TblId = (
                                                          SELECT    TblId
                                                          FROM      syTables
                                                          WHERE     TblName = 'adLeads'
                                                        )
                                                AND FldId = (
                                                              SELECT    FldId
                                                              FROM      syFields
                                                              WHERE     FldName = '[Comments]'
                                                            )
                                    )
                WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                                          FROM      syRptAdHocFields
                                          WHERE     TblFldsId = (
                                                                  SELECT    TblFldsId
                                                                  FROM      syTblFlds
                                                                  WHERE     TblId = (
                                                                                      SELECT    TblId
                                                                                      FROM      syTables
                                                                                      WHERE     TblName = 'adLeads'
                                                                                    )
                                                                            AND FldId IN ( SELECT   FldId
                                                                                           FROM     syFields
                                                                                           WHERE    FldName = 'Comments' )
                                                                )
                                                    AND ResourceId IN ( SELECT DISTINCT
                                                                                UR.ResourceId
                                                                        FROM    syUserResources UR
                                                                               ,syUserResPermissions UP
                                                                        WHERE   UR.ResourceId = UP.UserResourceId
                                                                                AND UP.ResourceId = 189 ) );


                UPDATE  syTblFlds
                SET     CategoryId = NULL
                WHERE   TblFldsId = (
                                      SELECT    TblFldsId
                                      FROM      syTblFlds
                                      WHERE     TblId = (
                                                          SELECT    TblId
                                                          FROM      syTables
                                                          WHERE     TblName = 'adLeads'
                                                        )
                                                AND FldId IN ( SELECT   FldId
                                                               FROM     syFields
                                                               WHERE    FldName = 'Comments' )
                                    );
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Comments]'
                         )
                        ,1
                        ,'Comments'
						 );
            END;
    END; 	
 --END



--start calculated fld for email-best
IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = '[Email - Best]' )
    BEGIN
        DECLARE @FieldId INT;--,@TblFldId INT,@CategoryId INT 
        SET @FieldId = (
                         SELECT MAX(FldId) + 1
                         FROM   syFields
                       ); 
        INSERT  INTO syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,LogChanges
                )
        VALUES  (
                 @FieldId
                ,'[Email - Best]'
                ,200
                ,50
                ,1
                );
        INSERT  INTO dbo.syFieldCalculation
                (
                 FldId
                ,CalculationSql
                )
        VALUES  (
                 @FieldId
                ,'(SELECT TOP 1 ALE.EMail FROM AdLeadEmail  AS ALE  INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId 
	 WHERE SS.Status = ''Active'' AND  ALE.EMailTypeId = (SELECT EMailTypeId FROM syEmailType WHERE EMailTypeCode = ''Home'')
	                                  AND ALE.LeadId = adLeads.LeadId ORDER BY ALE.ModDate DESC	) as [Email - Best]'
                );
		   
    END;
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = (
                                  SELECT    TblId
                                  FROM      syTables
                                  WHERE     TblName = 'AdLeadEmail'
                                )
                        AND FldId = (
                                      SELECT    FldId
                                      FROM      syFields
                                      WHERE     FldName = '[Email - Best]'
                                    ) )
    BEGIN
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
				 )
        VALUES  (
                 (
                   SELECT   MAX(TblFldsId)
                   FROM     syTblFlds
                 ) + 1 -- TblFldsId - int
                ,(
                   SELECT   TblId
                   FROM     syTables
                   WHERE    TblName = 'AdLeadEmail'
                 )  -- TblId - int
                ,@FieldId  -- FldId - int
                ,(
                   SELECT   CategoryId
                   FROM     syTblFlds
                   WHERE    TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeads'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'HomeEmail'
                                        )
                 )  -- CategoryId - int
                );	
        UPDATE  syTblFlds
        SET     CategoryId = NULL
        WHERE   TblId = (
                          SELECT    TblId
                          FROM      syTables
                          WHERE     TblName = 'adLeads'
                        )
                AND FldId = (
                              SELECT    FldId
                              FROM      syFields
                              WHERE     FldName = 'HomeEmail'
                            );
        INSERT  INTO syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                )
        VALUES  (
                 (
                   SELECT   MAX(FldCapId)
                   FROM     syFldCaptions
                 ) + 1
                ,@FieldId
                ,1
                ,'Email - Best'
						
                );
    END;
DECLARE @TblFldId INT;
SET @TblFldId = (
                  SELECT    TblFldsId
                  FROM      syTblFlds
                  WHERE     TblId = (
                                      (SELECT   TblId
                                       FROM     syTables
                                       WHERE    TblName = 'AdLeadEmail')
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = '[Email - Best]'
                                        )
                );
UPDATE  syRptAdHocFields
SET     TblFldsId = @TblFldId
WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                          FROM      syRptAdHocFields
                          WHERE     TblFldsId = (
                                                  SELECT    TblFldsId
                                                  FROM      syTblFlds
                                                  WHERE     TblId = (
                                                                      SELECT    TblId
                                                                      FROM      syTables
                                                                      WHERE     TblName = 'adLeads'
                                                                    )
                                                            AND FldId = (
                                                                          SELECT    FldId
                                                                          FROM      syFields
                                                                          WHERE     FldName = 'HomeEmail'
                                                                        )
                                                )
                                    AND ResourceId IN ( SELECT DISTINCT
                                                                UR.ResourceId
                                                        FROM    syUserResources UR
                                                               ,syUserResPermissions UP
                                                        WHERE   UR.ResourceId = UP.UserResourceId
                                                                AND UP.ResourceId = 189 ) );
 -- for all leads
--end calculated fld for email-best
GO	
--start calculated fld for email
IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = '[Email]' )
    BEGIN
        DECLARE @FieldId INT;--,@TblFldId INT,@CategoryId INT 
        SET @FieldId = (
                         SELECT MAX(FldId) + 1
                         FROM   syFields
                       ); 
        INSERT  INTO syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,LogChanges
                )
        VALUES  (
                 @FieldId
                ,'[Email]'
                ,200
                ,50
                ,1
                );
        INSERT  INTO dbo.syFieldCalculation
                (
                 FldId
                ,CalculationSql
                )
        VALUES  (
                 @FieldId
                ,'(SELECT TOP 1 ALE.EMail FROM AdLeadEmail AS ALE INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId 
	   WHERE SS.Status = ''Active'' AND  ALE.EMailTypeId = (SELECT EMailTypeId FROM syEmailType WHERE EMailTypeCode = ''Work'')
	                                  AND ALE.LeadId = adLeads.LeadId ORDER BY ALE.ModDate DESC) as Email'
                );
		   
    END;
IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = (
                                  SELECT    TblId
                                  FROM      syTables
                                  WHERE     TblName = 'AdLeadEmail'
                                )
                        AND FldId = (
                                      SELECT    FldId
                                      FROM      syFields
                                      WHERE     FldName = '[Email]'
                                    ) )
    BEGIN
        DECLARE @TblFldId INT;
        SET @TblFldId = (
                          SELECT    MAX(TblFldsId)
                          FROM      syTblFlds
                        ) + 1;
        SET @FieldId = (
                         SELECT FldId
                         FROM   syFields
                         WHERE  FldName = '[Email]'
                       );
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
				 )
        VALUES  (
                 @TblFldId  -- TblFldsId - int
                ,(
                   SELECT   TblId
                   FROM     syTables
                   WHERE    TblName = 'AdLeadEmail'
                 )  -- TblId - int
                ,@FieldId  -- FldId - int
                ,(
                   SELECT   CategoryId
                   FROM     syTblFlds
                   WHERE    TblId = (
                                      SELECT    TblId
                                      FROM      syTables
                                      WHERE     TblName = 'adLeads'
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'WorkEmail'
                                        )
                 )  -- CategoryId - int
                );	
        UPDATE  syTblFlds
        SET     CategoryId = NULL
        WHERE   TblId = (
                          SELECT    TblId
                          FROM      syTables
                          WHERE     TblName = 'adLeads'
                        )
                AND FldId = (
                              SELECT    FldId
                              FROM      syFields
                              WHERE     FldName = 'WorkEmail'
                            );
        IF NOT EXISTS ( SELECT  *
                        FROM    syFldCaptions
                        WHERE   FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = '[Email]'
                                        ) )
            BEGIN
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,(
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = '[Email]'
                         )
                        ,1
                        ,'Email'
						 );
            END;
    END;
SET @TblFldId = (
                  SELECT    TblFldsId
                  FROM      syTblFlds
                  WHERE     TblId = (
                                      (SELECT   TblId
                                       FROM     syTables
                                       WHERE    TblName = 'AdLeadEmail')
                                    )
                            AND FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = '[Email]'
                                        )
                );
UPDATE  syRptAdHocFields
SET     TblFldsId = @TblFldId
WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                          FROM      syRptAdHocFields
                          WHERE     TblFldsId = (
                                                  SELECT    TblFldsId
                                                  FROM      syTblFlds
                                                  WHERE     TblId = (
                                                                      SELECT    TblId
                                                                      FROM      syTables
                                                                      WHERE     TblName = 'adLeads'
                                                                    )
                                                            AND FldId = (
                                                                          SELECT    FldId
                                                                          FROM      syFields
                                                                          WHERE     FldName = 'WorkEmail'
                                                                        )
                                                )
                                    AND ResourceId IN ( SELECT DISTINCT
                                                                UR.ResourceId
                                                        FROM    syUserResources UR
                                                               ,syUserResPermissions UP
                                                        WHERE   UR.ResourceId = UP.UserResourceId
                                                                AND UP.ResourceId = 189 ) );
 -- for all leads
GO	
IF EXISTS ( SELECT  *
            FROM    syTblFlds
            WHERE   FldId = (
                              SELECT    FldId
                              FROM      syFields
                              WHERE     FldName = 'ForeignZip'
                            )
                    AND TblId = (
                                  SELECT    TblId
                                  FROM      syTables
                                  WHERE     TblName = 'adLeads'
                                )
                    AND CategoryId IS NOT NULL )
    BEGIN
        UPDATE  syTblFlds
        SET     CategoryId = NULL
        WHERE   TblFldsId = (
                              SELECT    TblFldsId
                              FROM      syTblFlds
                              WHERE     FldId = (
                                                  SELECT    FldId
                                                  FROM      syFields
                                                  WHERE     FldName = 'ForeignZip'
                                                )
                                        AND TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = 'adLeads'
                                                    )
                                        AND CategoryId IS NOT NULL
                            );
    END;	
GO	
-------------------------------------------------------------------------------------
--End US9492: SPIKE:  Review Reports for Admissions table changes
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start DE13208 Contacts Displayed Twice in Academics Menu
--replace the existing contacts/address/phones with just one page Student contacts
--delete contacts page from Menu as there is new page student contacts
--delete phones page from Menu as there is new page student contacts
-------------------------------------------------------------------------------------
CREATE TABLE #TempMenuItem
    (
     MenuItemId INT NOT NULL
    ,MenuName VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS
                           NOT NULL
    ,DisplayName VARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS
                              NOT NULL
    ,Url NVARCHAR(250) COLLATE SQL_Latin1_General_CP1_CI_AS
                       NULL
    ,MenuItemTypeId SMALLINT NOT NULL
    ,ParentId INT NULL
    ,DisplayOrder INT NULL
    ,IsPopup BIT NOT NULL
    ,ModDate DATETIME NULL
    ,ModUser VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS
                         NULL
    ,IsActive BIT NOT NULL
    ,ResourceId SMALLINT NULL
    ,HierarchyId UNIQUEIDENTIFIER NULL
    ,ModuleCode VARCHAR(5) COLLATE SQL_Latin1_General_CP1_CI_AS
                           NULL
    ,MRUType INT NULL
    ,HideStatusBar BIT NULL
    );
INSERT  INTO #TempMenuItem
        SELECT  MenuItemId
               ,MenuName
               ,DisplayName
               ,Url
               ,MenuItemTypeId
               ,ParentId
               ,DisplayOrder
               ,IsPopup
               ,ModDate
               ,ModUser
               ,IsActive
               ,ResourceId
               ,HierarchyId
               ,ModuleCode
               ,MRUType
               ,HideStatusBar
        FROM    syMenuItems
        WHERE   ResourceId IN ( 155,213,159 );
 --for contacts
IF EXISTS ( SELECT  *
            FROM    #TempMenuItem
            WHERE   ResourceId = 213 )
    BEGIN
        IF EXISTS ( SELECT  *
                    FROM    #TempMenuItem
                    WHERE   ResourceId = 155 )
            BEGIN
                IF EXISTS ( SELECT  *
                            FROM    #TempMenuItem
                            WHERE   ResourceId = 213
                                    AND ParentId NOT IN ( SELECT    ParentId
                                                          FROM      #TempMenuItem
                                                          WHERE     ResourceId = 155 ) )
                    BEGIN
		--insert if the student contacts is not in other menu 
                        INSERT  INTO syMenuItems
                                SELECT  'Student Contacts'
                                       ,'Student Contacts'
                                       ,'/AR/StudentContacts.aspx'
                                       ,MenuItemTypeId
                                       ,ParentId
                                       ,DisplayOrder
                                       ,IsPopup
                                       ,GETDATE()
                                       ,'support'
                                       ,IsActive
                                       ,155
                                       ,HierarchyId
                                       ,ModuleCode
                                       ,MRUType
                                       ,HideStatusBar
                                FROM    syMenuItems
                                WHERE   ResourceId = 213
                                        AND ParentId NOT IN ( SELECT    ParentId
                                                              FROM      #TempMenuItem
                                                              WHERE     ResourceId = 155 );
                    END;		
            END;
        ELSE
            BEGIN
		--insert for all the rows as there are no Student contacts
                INSERT  INTO syMenuItems
                        SELECT  'Student Contacts'
                               ,'Student Contacts'
                               ,'/AR/StudentContacts.aspx'
                               ,MenuItemTypeId
                               ,ParentId
                               ,DisplayOrder
                               ,IsPopup
                               ,GETDATE()
                               ,'support'
                               ,IsActive
                               ,155
                               ,HierarchyId
                               ,ModuleCode
                               ,MRUType
                               ,HideStatusBar
                        FROM    syMenuItems
                        WHERE   ResourceId = 213;
            END;
    END;
 --for phone
IF EXISTS ( SELECT  *
            FROM    #TempMenuItem
            WHERE   ResourceId = 159 )
    BEGIN
        IF EXISTS ( SELECT  *
                    FROM    #TempMenuItem
                    WHERE   ResourceId = 155 )
            BEGIN
                IF EXISTS ( SELECT  *
                            FROM    #TempMenuItem
                            WHERE   ResourceId = 159
                                    AND ParentId NOT IN ( SELECT    ParentId
                                                          FROM      #TempMenuItem
                                                          WHERE     ResourceId = 155 ) )
                    BEGIN
		--insert if the student contacts is not in other menu 
                        INSERT  INTO syMenuItems
                                SELECT  'Student Contacts'
                                       ,'Student Contacts'
                                       ,'/AR/StudentContacts.aspx'
                                       ,MenuItemTypeId
                                       ,ParentId
                                       ,DisplayOrder
                                       ,IsPopup
                                       ,GETDATE()
                                       ,'support'
                                       ,IsActive
                                       ,155
                                       ,HierarchyId
                                       ,ModuleCode
                                       ,MRUType
                                       ,HideStatusBar
                                FROM    syMenuItems
                                WHERE   ResourceId = 159
                                        AND ParentId NOT IN ( SELECT    ParentId
                                                              FROM      #TempMenuItem
                                                              WHERE     ResourceId = 155 );
                    END;		
            END;
        ELSE
            BEGIN
		--insert for all the rows as there are no Student contacts
                INSERT  INTO syMenuItems
                        SELECT  'Student Contacts'
                               ,'Student Contacts'
                               ,'/AR/StudentContacts.aspx'
                               ,MenuItemTypeId
                               ,ParentId
                               ,DisplayOrder
                               ,IsPopup
                               ,GETDATE()
                               ,'support'
                               ,IsActive
                               ,155
                               ,HierarchyId
                               ,ModuleCode
                               ,MRUType
                               ,HideStatusBar
                        FROM    syMenuItems
                        WHERE   ResourceId = 159;
            END;
    END;
IF ( OBJECT_ID('tempdb..#TempMenuItem') IS NOT NULL )
    BEGIN
        DROP TABLE #TempMenuItem;
    END;	
	GO
DELETE  dbo.syMenuItems
WHERE   ResourceId = 159;
DELETE  dbo.syMenuItems
WHERE   ResourceId = 213;
GO
-------------------------------------------------------------------------------------
--End DE13208 Contacts Displayed Twice in Academics Menu
-------------------------------------------------------------------------------------

-- =========================================================================================================
-- USUS9616 TECH Synchronize Information between Lead and Student over the student Lead:  STEP 4: Create the necessary Views to maintain compatibility
-- Create a plAddressType record for Other if it do not exists
-- =========================================================================================================
 
 -- also
 -------------------------------------------------------------------------------------------------------------
-- DE13321 3.8 Reg Defect- Unfriendly error pop up message is displayed when import Leads
-- JTorres  -- Correct the Catalog Table plAddressTypes to be compatible with SP usp_IL_ProcessImportLeadFile
-------------------------------------------------------------------------------------------------------------

-- ********************************************************************************************************************
--  plAddressTypes
-- ********************************************************************************************************************
DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);

SET @AddressCode = 'Home';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Home'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************

DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);


SET @AddressCode = 'Other';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Other'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);

SET @AddressCode = 'Work';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Home'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************

-- ********************************************************************************************************************

-- =========================================================================================================
-- END  --  USUS9616 TECH Synchronize Information between Lead and Student over the student Lead:  STEP 4: Create the necessary Views to maintain compatibility
-- =========================================================================================================
--  ==============================================================================================
--  US9718 Migrate Prior Education Page to Academics (Placement)
--  Description: Create the Prior Education Page in Academic, Placement
--               The database must have completed migration of leads and Student to work correctly!
--  JAGG
--  ==============================================================================================
DELETE  dbo.syMenuItems
WHERE   ResourceId = 90;
 
-- put here your resource ID for the Menu
DECLARE @ResID INTEGER = 90;
-- name of your Menu
DECLARE @MenuName VARCHAR(20) = 'Prior Education';
 -- the URL of page in advantage
DECLARE @UrlTarget VARCHAR(100) = '/PL/PriorEducation.aspx';
  --Page (call a page in the menu item)
DECLARE @MenuTypeItem INTEGER = 4;
-- In the column menu the position of the menu item. 100 first...200 second
DECLARE @MenuDisplayOrder INTEGER = 510;
-- 1 pop up 0: no pop up
DECLARE @IsPopup BIT = 0;
DECLARE @MenuDisplayName VARCHAR(20) = 'Prior Education';

-- Put the display name for your page
DECLARE @Parent INTEGER	= (
                            SELECT  MenuItemId
                            FROM    syMenuItems
                            WHERE   DisplayName = 'Academics'
                                    AND ParentId = (
                                                     SELECT MenuItemId -- MenuItemId
                                                     FROM   dbo.syMenuItems
                                                     WHERE  DisplayName = 'Manage Students'
                                                            AND MenuItemTypeId = 2
                                                            AND ParentId = (
                                                                             SELECT MenuItemId
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  DisplayName = 'Placement'
                                                                                    AND MenuItemTypeId = 1
                                                                           )
                                                   )
                          );
-- Create the sub-menu in syMenuItem WAPI Services
 
INSERT  dbo.syMenuItems
        (
         MenuName
        ,DisplayName
        ,Url
        ,MenuItemTypeId
        ,ParentId
        ,DisplayOrder
        ,IsPopup
        ,ModDate
        ,ModUser
        ,IsActive
        ,ResourceId
        ,HierarchyId
        ,ModuleCode
        ,MRUType
        ,HideStatusBar
		)
VALUES  (
         @MenuName -- MenuName - varchar(250)
        ,@MenuDisplayName -- DisplayName - varchar(250)
        ,@UrlTarget -- URL - nvarchar(250)
        ,@MenuTypeItem   -- MenuItemTypeId -(see table below the code) smallint
        ,@Parent -- ParentId - int
        ,@MenuDisplayOrder -- DisplayOrder - int
        ,@IsPopup   -- IsPopup - bit
        ,GETDATE() -- ModDate - datetime
        ,'support'-- ModUser - varchar(50)
        ,1 -- IsActive - bit
        ,@ResID -- ResourceId - smallint
        ,NULL -- HierarchyId - uniqueidentifier
        ,NULL -- ModuleCode - varchar(5)
        ,1 -- MRUType - int
        ,NULL  -- HideStatusBar - bit
		);
GO

UPDATE  dbo.syResources
SET     ResourceURL = '~/PL/PriorEducation.aspx'
       ,DisplayName = 'Prior Education'
       ,Resource = 'Prior Education'
WHERE   ResourceID = 90;
GO
-- =========================================================================================================
-- END  -- US9718 Migrate Prior Education Page to Academics (Placement)
-- =========================================================================================================
-- =========================================================================================================
-- START  -- DE13258 Duplicate Column values in Student Adhoc (General Information) Report
-- ========================================================================
IF EXISTS ( SELECT  1
            FROM    sysobjects
            WHERE   name = 'syRptAdhocRelations'
                    AND xtype = 'U' )
    BEGIN	
-- PRINT inserting data 1, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'adLeadEntranceTest'
                                AND FkColumn = 'EntrTestId'
                                AND PkTable = 'adReqs'
                                AND PkColumn = 'adReqId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'adLeadEntranceTest','EntrTestId','adReqs','adReqId' );
            END; 

-- PRINT inserting data 2, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'adLeadEntranceTest'
                                AND FkColumn = 'LeadId'
                                AND PkTable = 'adLeads'
                                AND PkColumn = 'LeadId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'adLeadEntranceTest','LeadId','adLeads','LeadId' );
            END; 

-- PRINT inserting data 3, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arBooks'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arBooks','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 4, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arBooks'
                                AND FkColumn = 'CategoryId'
                                AND PkTable = 'arBkCategories'
                                AND PkColumn = 'CategoryId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arBooks','CategoryId','arBkCategories','CategoryId' );
            END; 

-- PRINT inserting data 5, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arBooks'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arBooks','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 6, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'CampusId'
                                AND PkTable = 'syCampuses'
                                AND PkColumn = 'CampusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','CampusId','syCampuses','CampusId' );
            END; 

-- PRINT inserting data 7, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'GrdScaleId'
                                AND PkTable = 'arGradeScales'
                                AND PkColumn = 'GrdScaleId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','GrdScaleId','arGradeScales','GrdScaleId' );
            END; 

-- PRINT inserting data 8, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'InstrGrdBkWgtId'
                                AND PkTable = 'arGrdBkWeights'
                                AND PkColumn = 'InstrGrdBkWgtId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','InstrGrdBkWgtId','arGrdBkWeights','InstrGrdBkWgtId' );
            END; 

-- PRINT inserting data 9, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'InstructorId'
                                AND PkTable = 'syUsers'
                                AND PkColumn = 'UserId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','InstructorId','syUsers','UserId' );
            END; 

-- PRINT inserting data 10, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'LeadGrpId'
                                AND PkTable = 'adLeadGroups'
                                AND PkColumn = 'LeadGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','LeadGrpId','adLeadGroups','LeadGrpId' );
            END; 

-- PRINT inserting data 11, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'ReqId'
                                AND PkTable = 'arReqs'
                                AND PkColumn = 'ReqId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','ReqId','arReqs','ReqId' );
            END; 

-- PRINT inserting data 12, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'ShiftId'
                                AND PkTable = 'arShifts'
                                AND PkColumn = 'ShiftId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','ShiftId','arShifts','ShiftId' );
            END; 

-- PRINT inserting data 13, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arClassSections'
                                AND FkColumn = 'TermId'
                                AND PkTable = 'arTerm'
                                AND PkColumn = 'TermId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arClassSections','TermId','arTerm','TermId' );
            END; 

-- PRINT inserting data 14, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arGradeSystemDetails'
                                AND FkColumn = 'GrdSystemId'
                                AND PkTable = 'arGradeSystems'
                                AND PkColumn = 'GrdSystemId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arGradeSystemDetails','GrdSystemId','arGradeSystems','GrdSystemId' );
            END; 

-- PRINT inserting data 15, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'BillingMethodId'
                                AND PkTable = 'saBillingMethods'
                                AND PkColumn = 'BillingMethodId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','BillingMethodId','saBillingMethods','BillingMethodId' );
            END; 

-- PRINT inserting data 16, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 17, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'DegreeId'
                                AND PkTable = 'arDegrees'
                                AND PkColumn = 'DegreeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','DegreeId','arDegrees','DegreeId' );
            END; 

-- PRINT inserting data 18, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'DeptId'
                                AND PkTable = 'arDepartments'
                                AND PkColumn = 'DeptId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','DeptId','arDepartments','DeptId' );
            END; 

-- PRINT inserting data 19, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'GrdSystemId'
                                AND PkTable = 'arGradeSystems'
                                AND PkColumn = 'GrdSystemId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','GrdSystemId','arGradeSystems','GrdSystemId' );
            END; 

-- PRINT inserting data 20, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'PrgGrpId'
                                AND PkTable = 'arPrgGrp'
                                AND PkColumn = 'PrgGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','PrgGrpId','arPrgGrp','PrgGrpId' );
            END; 

-- PRINT inserting data 21, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'ProgId'
                                AND PkTable = 'arPrograms'
                                AND PkColumn = 'ProgId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','ProgId','arPrograms','ProgId' );
            END; 

-- PRINT inserting data 22, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'ProgTypId'
                                AND PkTable = 'arProgTypes'
                                AND PkColumn = 'ProgTypId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','ProgTypId','arProgTypes','ProgTypId' );
            END; 

-- PRINT inserting data 23, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'SAPId'
                                AND PkTable = 'arSAP'
                                AND PkColumn = 'SAPId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','SAPId','arSAP','SAPId' );
            END; 

-- PRINT inserting data 24, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'SchedMethodId'
                                AND PkTable = 'sySchedulingMethods'
                                AND PkColumn = 'SchedMethodId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','SchedMethodId','sySchedulingMethods','SchedMethodId' );
            END; 

-- PRINT inserting data 25, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 26, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'ThGrdScaleId'
                                AND PkTable = 'arGradeScales'
                                AND PkColumn = 'GrdScaleId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','ThGrdScaleId','arGradeScales','GrdScaleId' );
            END; 

-- PRINT inserting data 27, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arPrgVersions'
                                AND FkColumn = 'TuitionEarningId'
                                AND PkTable = 'saTuitionEarnings'
                                AND PkColumn = 'TuitionEarningId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arPrgVersions','TuitionEarningId','saTuitionEarnings','TuitionEarningId' );
            END; 

-- PRINT inserting data 28, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 29, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'CourseCategoryId'
                                AND PkTable = 'arCourseCategories'
                                AND PkColumn = 'CourseCategoryId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','CourseCategoryId','arCourseCategories','CourseCategoryId' );
            END; 

-- PRINT inserting data 30, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'DeptId'
                                AND PkTable = 'arDepartments'
                                AND PkColumn = 'DeptId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','DeptId','arDepartments','DeptId' );
            END; 

-- PRINT inserting data 31, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'GrdLvlId'
                                AND PkTable = 'adEdLvls'
                                AND PkColumn = 'EdLvlId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','GrdLvlId','adEdLvls','EdLvlId' );
            END; 

-- PRINT inserting data 32, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'ReqTypeId'
                                AND PkTable = 'arReqTypes'
                                AND PkColumn = 'ReqTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','ReqTypeId','arReqTypes','ReqTypeId' );
            END; 

-- PRINT inserting data 33, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 34, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arReqs'
                                AND FkColumn = 'UnitTypeId'
                                AND PkTable = 'arAttUnitType'
                                AND PkColumn = 'UnitTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arReqs','UnitTypeId','arAttUnitType','UnitTypeId' );
            END; 

-- PRINT inserting data 35, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arResults'
                                AND FkColumn = 'GrdSysDetailId'
                                AND PkTable = 'arGradeSystemDetails'
                                AND PkColumn = 'GrdSysDetailId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arResults','GrdSysDetailId','arGradeSystemDetails','GrdSysDetailId' );
            END; 

-- PRINT inserting data 36, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arResults'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arResults','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 37, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arResults'
                                AND FkColumn = 'TestId'
                                AND PkTable = 'arClassSections'
                                AND PkColumn = 'ClsSectionId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arResults','TestId','arClassSections','ClsSectionId' );
            END; 

-- PRINT inserting data 38, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAP'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAP','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 39, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAP'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAP','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 40, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAP'
                                AND FkColumn = 'TrigOffsetTypId'
                                AND PkTable = 'arTrigOffsetTyps'
                                AND PkColumn = 'TrigOffsetTypId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAP','TrigOffsetTypId','arTrigOffsetTyps','TrigOffsetTypId' );
            END; 

-- PRINT inserting data 41, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAP'
                                AND FkColumn = 'TrigUnitTypId'
                                AND PkTable = 'arTrigUnitTyps'
                                AND PkColumn = 'TrigUnitTypId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAP','TrigUnitTypId','arTrigUnitTyps','TrigUnitTypId' );
            END; 

-- PRINT inserting data 42, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAPChkResults'
                                AND FkColumn = 'SAPDetailId'
                                AND PkTable = 'arSAPDetails'
                                AND PkColumn = 'SAPDetailId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAPChkResults','SAPDetailId','arSAPDetails','SAPDetailId' );
            END; 

-- PRINT inserting data 43, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAPChkResults'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAPChkResults','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 44, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arSAPDetails'
                                AND FkColumn = 'SAPId'
                                AND PkTable = 'arSAP'
                                AND PkColumn = 'SAPId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arSAPDetails','SAPId','arSAP','SAPId' );
            END; 

-- PRINT inserting data 45, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudAddresses'
                                AND FkColumn = 'AddressTypeId'
                                AND PkTable = 'plAddressTypes'
                                AND PkColumn = 'AddressTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudAddresses','AddressTypeId','plAddressTypes','AddressTypeId' );
            END; 

-- PRINT inserting data 46, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudAddresses'
                                AND FkColumn = 'CountryId'
                                AND PkTable = 'adCountries'
                                AND PkColumn = 'CountryId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudAddresses','CountryId','adCountries','CountryId' );
            END; 

-- PRINT inserting data 47, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudAddresses'
                                AND FkColumn = 'StateId'
                                AND PkTable = 'syStates'
                                AND PkColumn = 'StateId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudAddresses','StateId','syStates','StateId' );
            END; 

-- PRINT inserting data 48, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudAddresses'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudAddresses','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 49, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudAddresses'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudAddresses','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 50, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'admincriteriaid'
                                AND PkTable = 'adAdminCriteria'
                                AND PkColumn = 'admincriteriaid' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','admincriteriaid','adAdminCriteria','admincriteriaid' );
            END; 

-- PRINT inserting data 51, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'AttendTypeId'
                                AND PkTable = 'arAttendTypes'
                                AND PkColumn = 'AttendTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','AttendTypeId','arAttendTypes','AttendTypeId' );
            END; 

-- PRINT inserting data 52, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Citizen'
                                AND PkTable = 'adCitizenships'
                                AND PkColumn = 'CitizenshipId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Citizen','adCitizenships','CitizenshipId' );
            END; 

-- PRINT inserting data 53, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'County'
                                AND PkTable = 'adCounties'
                                AND PkColumn = 'CountyId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','County','adCounties','CountyId' );
            END; 

-- PRINT inserting data 54, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'DegCertSeekingId'
                                AND PkTable = 'adDegCertSeeking'
                                AND PkColumn = 'DegCertSeekingId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','DegCertSeekingId','adDegCertSeeking','DegCertSeekingId' );
            END; 

-- PRINT inserting data 55, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'DependencyTypeId'
                                AND PkTable = 'adDependencyTypes'
                                AND PkColumn = 'DependencyTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','DependencyTypeId','adDependencyTypes','DependencyTypeId' );
            END; 

-- PRINT inserting data 56, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'DrivLicStateId'
                                AND PkTable = 'syStates'
                                AND PkColumn = 'StateId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','DrivLicStateId','syStates','StateId' );
            END; 

-- PRINT inserting data 57, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'EdLvlId'
                                AND PkTable = 'adEdLvls'
                                AND PkColumn = 'EdLvlId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','EdLvlId','adEdLvls','EdLvlId' );
            END; 

-- PRINT inserting data 58, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'FamilyIncome'
                                AND PkTable = 'syFamilyIncome'
                                AND PkColumn = 'FamilyIncomeID' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','FamilyIncome','syFamilyIncome','FamilyIncomeID' );
            END; 

-- PRINT inserting data 59, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Gender'
                                AND PkTable = 'adGenders'
                                AND PkColumn = 'GenderId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Gender','adGenders','GenderId' );
            END; 

-- PRINT inserting data 60, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'GeographicTypeId'
                                AND PkTable = 'adGeographicTypes'
                                AND PkColumn = 'GeographicTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','GeographicTypeId','adGeographicTypes','GeographicTypeId' );
            END; 

-- PRINT inserting data 61, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'HousingId'
                                AND PkTable = 'arHousing'
                                AND PkColumn = 'HousingId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','HousingId','arHousing','HousingId' );
            END; 

-- PRINT inserting data 62, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'MaritalStatus'
                                AND PkTable = 'adMaritalStatus'
                                AND PkColumn = 'MaritalStatId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','MaritalStatus','adMaritalStatus','MaritalStatId' );
            END; 

-- PRINT inserting data 63, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Nationality'
                                AND PkTable = 'adNationalities'
                                AND PkColumn = 'NationalityId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Nationality','adNationalities','NationalityId' );
            END; 

-- PRINT inserting data 64, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Prefix'
                                AND PkTable = 'syPrefixes'
                                AND PkColumn = 'PrefixId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Prefix','syPrefixes','PrefixId' );
            END; 

-- PRINT inserting data 65, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Race'
                                AND PkTable = 'adEthCodes'
                                AND PkColumn = 'EthCodeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Race','adEthCodes','EthCodeId' );
            END; 

-- PRINT inserting data 66, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'ShiftId'
                                AND PkTable = 'arShifts'
                                AND PkColumn = 'ShiftId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','ShiftId','arShifts','ShiftId' );
            END; 

-- PRINT inserting data 67, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Sponsor'
                                AND PkTable = 'adAgencySponsors'
                                AND PkColumn = 'AgencySpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Sponsor','adAgencySponsors','AgencySpId' );
            END; 

-- PRINT inserting data 68, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'StudentStatus'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','StudentStatus','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 69, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudent'
                                AND FkColumn = 'Suffix'
                                AND PkTable = 'sySuffixes'
                                AND PkColumn = 'SuffixId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudent','Suffix','sySuffixes','SuffixId' );
            END; 

-- PRINT inserting data 70, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudentLOAs'
                                AND FkColumn = 'LOAReasonId'
                                AND PkTable = 'arLOAReasons'
                                AND PkColumn = 'LOAReasonId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudentLOAs','LOAReasonId','arLOAReasons','LOAReasonId' );
            END; 

-- PRINT inserting data 71, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudentLOAs'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudentLOAs','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 72, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudentPhone'
                                AND FkColumn = 'PhoneTypeId'
                                AND PkTable = 'syPhoneType'
                                AND PkColumn = 'PhoneTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudentPhone','PhoneTypeId','syPhoneType','PhoneTypeId' );
            END; 

-- PRINT inserting data 73, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudentPhone'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudentPhone','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 74, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStudentPhone'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStudentPhone','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 75, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'AcademicAdvisor'
                                AND PkTable = 'syUsers'
                                AND PkColumn = 'UserId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','AcademicAdvisor','syUsers','UserId' );
            END; 

-- PRINT inserting data 76, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'AdmissionsRep'
                                AND PkTable = 'syUsers'
                                AND PkColumn = 'UserId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','AdmissionsRep','syUsers','UserId' );
            END; 

-- PRINT inserting data 77, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'attendtypeid'
                                AND PkTable = 'arAttendTypes'
                                AND PkColumn = 'AttendTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','attendtypeid','arAttendTypes','AttendTypeId' );
            END; 

-- PRINT inserting data 78, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'BillingMethodId'
                                AND PkTable = 'saBillingMethods'
                                AND PkColumn = 'BillingMethodId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','BillingMethodId','saBillingMethods','BillingMethodId' );
            END; 

-- PRINT inserting data 79, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'CampusId'
                                AND PkTable = 'syCampuses'
                                AND PkColumn = 'CampusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','CampusId','syCampuses','CampusId' );
            END; 

-- PRINT inserting data 80, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'degcertseekingid'
                                AND PkTable = 'adDegCertSeeking'
                                AND PkColumn = 'DegCertSeekingId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','degcertseekingid','adDegCertSeeking','DegCertSeekingId' );
            END; 

-- PRINT inserting data 81, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'DropReasonId'
                                AND PkTable = 'arDropReasons'
                                AND PkColumn = 'DropReasonId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','DropReasonId','arDropReasons','DropReasonId' );
            END; 

-- PRINT inserting data 82, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'EdLvlId'
                                AND PkTable = 'adEdLvls'
                                AND PkColumn = 'EdLvlId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','EdLvlId','adEdLvls','EdLvlId' );
            END; 

-- PRINT inserting data 83, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'FAAdvisorId'
                                AND PkTable = 'syUsers'
                                AND PkColumn = 'UserId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','FAAdvisorId','syUsers','UserId' );
            END; 

-- PRINT inserting data 84, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'LeadId'
                                AND PkTable = 'adLeads'
                                AND PkColumn = 'LeadId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','LeadId','adLeads','LeadId' );
            END; 

-- PRINT inserting data 85, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'PrgVerId'
                                AND PkTable = 'arPrgVersions'
                                AND PkColumn = 'PrgVerId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','PrgVerId','arPrgVersions','PrgVerId' );
            END; 

-- PRINT inserting data 86, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'PrgVersionTypeId'
                                AND PkTable = 'arProgramVersionType'
                                AND PkColumn = 'ProgramVersionTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','PrgVersionTypeId','arProgramVersionType','ProgramVersionTypeId' );
            END; 

-- PRINT inserting data 87, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'SAPId'
                                AND PkTable = 'arSAP'
                                AND PkColumn = 'SAPId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','SAPId','arSAP','SAPId' );
            END; 

-- PRINT inserting data 88, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'ShiftId'
                                AND PkTable = 'arShifts'
                                AND PkColumn = 'ShiftId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','ShiftId','arShifts','ShiftId' );
            END; 

-- PRINT inserting data 89, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'StatusCodeId'
                                AND PkTable = 'syStatusCodes'
                                AND PkColumn = 'StatusCodeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','StatusCodeId','syStatusCodes','StatusCodeId' );
            END; 

-- PRINT inserting data 90, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 91, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 92, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arStuEnrollments'
                                AND FkColumn = 'TuitionCategoryId'
                                AND PkTable = 'saTuitionCategories'
                                AND PkColumn = 'TuitionCategoryId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arStuEnrollments','TuitionCategoryId','saTuitionCategories','TuitionCategoryId' );
            END; 

-- PRINT inserting data 93, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arTerm'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arTerm','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 94, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arTerm'
                                AND FkColumn = 'ProgId'
                                AND PkTable = 'arPrograms'
                                AND PkColumn = 'ProgId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arTerm','ProgId','arPrograms','ProgId' );
            END; 

-- PRINT inserting data 95, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arTerm'
                                AND FkColumn = 'ShiftId'
                                AND PkTable = 'arShifts'
                                AND PkColumn = 'ShiftId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arTerm','ShiftId','arShifts','ShiftId' );
            END; 

-- PRINT inserting data 96, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arTerm'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arTerm','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 97, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'arTerm'
                                AND FkColumn = 'TermTypeId'
                                AND PkTable = 'syTermTypes'
                                AND PkColumn = 'TermTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'arTerm','TermTypeId','syTermTypes','TermTypeId' );
            END; 

-- PRINT inserting data 98, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'AcademicYearId'
                                AND PkTable = 'saAcademicYears'
                                AND PkColumn = 'AcademicYearId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','AcademicYearId','saAcademicYears','AcademicYearId' );
            END; 

-- PRINT inserting data 99, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'AwardTypeId'
                                AND PkTable = 'saFundSources'
                                AND PkColumn = 'FundSourceId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','AwardTypeId','saFundSources','FundSourceId' );
            END; 

-- PRINT inserting data 100, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'GuarantorId'
                                AND PkTable = 'faLenders'
                                AND PkColumn = 'LenderId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','GuarantorId','faLenders','LenderId' );
            END; 

-- PRINT inserting data 101, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'LenderId'
                                AND PkTable = 'faLenders'
                                AND PkColumn = 'LenderId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','LenderId','faLenders','LenderId' );
            END; 

-- PRINT inserting data 102, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'ServicerId'
                                AND PkTable = 'faLenders'
                                AND PkColumn = 'LenderId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','ServicerId','faLenders','LenderId' );
            END; 

-- PRINT inserting data 103, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwards'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwards','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 104, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'faStudentAwardSchedule'
                                AND FkColumn = 'StudentAwardId'
                                AND PkTable = 'faStudentAwards'
                                AND PkColumn = 'StudentAwardId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'faStudentAwardSchedule','StudentAwardId','faStudentAwards','StudentAwardId' );
            END; 

-- PRINT inserting data 105, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'AreaId'
                                AND PkTable = 'adCounties'
                                AND PkColumn = 'CountyId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','AreaId','adCounties','CountyId' );
            END; 

-- PRINT inserting data 106, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'BenefitsId'
                                AND PkTable = 'plJobBenefit'
                                AND PkColumn = 'JobBenefitId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','BenefitsId','plJobBenefit','JobBenefitId' );
            END; 

-- PRINT inserting data 107, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 108, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'ContactId'
                                AND PkTable = 'plEmployerContact'
                                AND PkColumn = 'EmployerContactId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','ContactId','plEmployerContact','EmployerContactId' );
            END; 

-- PRINT inserting data 109, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'EmployerId'
                                AND PkTable = 'plEmployers'
                                AND PkColumn = 'EmployerId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','EmployerId','plEmployers','EmployerId' );
            END; 

-- PRINT inserting data 110, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'ExpertiseId'
                                AND PkTable = 'adExpertiseLevel'
                                AND PkColumn = 'ExpertiseId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','ExpertiseId','adExpertiseLevel','ExpertiseId' );
            END; 

-- PRINT inserting data 111, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'FeeId'
                                AND PkTable = 'plFee'
                                AND PkColumn = 'FeeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','FeeId','plFee','FeeId' );
            END; 

-- PRINT inserting data 112, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'JobGroupId'
                                AND PkTable = 'plJobCats'
                                AND PkColumn = 'JobCatId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','JobGroupId','plJobCats','JobCatId' );
            END; 

-- PRINT inserting data 113, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'JobTitleId'
                                AND PkTable = 'adTitles'
                                AND PkColumn = 'TitleId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','JobTitleId','adTitles','TitleId' );
            END; 

-- PRINT inserting data 114, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'SalaryTypeID'
                                AND PkTable = 'plSalaryType'
                                AND PkColumn = 'SalaryTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','SalaryTypeID','plSalaryType','SalaryTypeId' );
            END; 

-- PRINT inserting data 115, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'ScheduleId'
                                AND PkTable = 'plJobSchedule'
                                AND PkColumn = 'JobScheduleId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','ScheduleId','plJobSchedule','JobScheduleId' );
            END; 

-- PRINT inserting data 116, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 117, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plEmployerJobs'
                                AND FkColumn = 'TypeId'
                                AND PkTable = 'plJobType'
                                AND PkColumn = 'JobGroupId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plEmployerJobs','TypeId','plJobType','JobGroupId' );
            END; 

-- PRINT inserting data 118, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'AreaId'
                                AND PkTable = 'adCounties'
                                AND PkColumn = 'CountyId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','AreaId','adCounties','CountyId' );
            END; 

-- PRINT inserting data 119, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'EnrollmentId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','EnrollmentId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 120, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'ExpertiseLevelId'
                                AND PkTable = 'adExpertiseLevel'
                                AND PkColumn = 'ExpertiseId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','ExpertiseLevelId','adExpertiseLevel','ExpertiseId' );
            END; 

-- PRINT inserting data 121, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'FullTimeId'
                                AND PkTable = 'adFullPartTime'
                                AND PkColumn = 'FullTimeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','FullTimeId','adFullPartTime','FullTimeId' );
            END; 

-- PRINT inserting data 122, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'ScStatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','ScStatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 123, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plExitInterview'
                                AND FkColumn = 'TransportationId'
                                AND PkTable = 'plTransportation'
                                AND PkColumn = 'TransportationId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plExitInterview','TransportationId','plTransportation','TransportationId' );
            END; 

-- PRINT inserting data 124, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentDocs'
                                AND FkColumn = 'DocStatusId'
                                AND PkTable = 'syDocStatuses'
                                AND PkColumn = 'DocStatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentDocs','DocStatusId','syDocStatuses','DocStatusId' );
            END; 

-- PRINT inserting data 125, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentDocs'
                                AND FkColumn = 'DocumentId'
                                AND PkTable = 'adReqs'
                                AND PkColumn = 'adReqId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentDocs','DocumentId','adReqs','adReqId' );
            END; 

-- PRINT inserting data 126, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentDocs'
                                AND FkColumn = 'ModuleID'
                                AND PkTable = 'syModules'
                                AND PkColumn = 'ModuleID' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentDocs','ModuleID','syModules','ModuleID' );
            END; 

-- PRINT inserting data 127, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentDocs'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentDocs','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 128, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentEducation'
                                AND FkColumn = 'CertificateId'
                                AND PkTable = 'arDegrees'
                                AND PkColumn = 'DegreeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentEducation','CertificateId','arDegrees','DegreeId' );
            END; 

-- PRINT inserting data 129, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'plStudentEducation'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'plStudentEducation','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 130, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'BenefitsId'
                                AND PkTable = 'plJobBenefit'
                                AND PkColumn = 'JobBenefitId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','BenefitsId','plJobBenefit','JobBenefitId' );
            END; 

-- PRINT inserting data 131, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'EmployerJobId'
                                AND PkTable = 'plEmployerJobs'
                                AND PkColumn = 'EmployerJobId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','EmployerJobId','plEmployerJobs','EmployerJobId' );
            END; 

-- PRINT inserting data 132, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'Fee'
                                AND PkTable = 'plFee'
                                AND PkColumn = 'FeeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','Fee','plFee','FeeId' );
            END; 

-- PRINT inserting data 133, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'FldStudyId'
                                AND PkTable = 'plFldStudy'
                                AND PkColumn = 'FldStudyId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','FldStudyId','plFldStudy','FldStudyId' );
            END; 

-- PRINT inserting data 134, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'HowPlacedId'
                                AND PkTable = 'plHowPlaced'
                                AND PkColumn = 'HowPlacedId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','HowPlacedId','plHowPlaced','HowPlacedId' );
            END; 

-- PRINT inserting data 135, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'InterviewId'
                                AND PkTable = 'plInterview'
                                AND PkColumn = 'InterviewId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','InterviewId','plInterview','InterviewId' );
            END; 

-- PRINT inserting data 136, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'JobStatusId'
                                AND PkTable = 'plJobStatus'
                                AND PkColumn = 'JobStatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','JobStatusId','plJobStatus','JobStatusId' );
            END; 

-- PRINT inserting data 137, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'PlacementRep'
                                AND PkTable = 'syUsers'
                                AND PkColumn = 'UserId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','PlacementRep','syUsers','UserId' );
            END; 

-- PRINT inserting data 138, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'SalaryTypeId'
                                AND PkTable = 'plSalaryType'
                                AND PkColumn = 'SalaryTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','SalaryTypeId','plSalaryType','SalaryTypeId' );
            END; 

-- PRINT inserting data 139, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'ScheduleId'
                                AND PkTable = 'plJobSchedule'
                                AND PkColumn = 'JobScheduleId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','ScheduleId','plJobSchedule','JobScheduleId' );
            END; 

-- PRINT inserting data 140, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 141, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'PlStudentsPlaced'
                                AND FkColumn = 'WorkDaysId'
                                AND PkTable = 'plJobWorkDays'
                                AND PkColumn = 'JobWorkDaysId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'PlStudentsPlaced','WorkDaysId','plJobWorkDays','JobWorkDaysId' );
            END; 

-- PRINT inserting data 142, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saPayments'
                                AND FkColumn = 'BankAcctId'
                                AND PkTable = 'saBankAccounts'
                                AND PkColumn = 'BankAcctId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saPayments','BankAcctId','saBankAccounts','BankAcctId' );
            END; 

-- PRINT inserting data 143, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saPayments'
                                AND FkColumn = 'PaymentTypeId'
                                AND PkTable = 'saPaymentTypes'
                                AND PkColumn = 'PaymentTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saPayments','PaymentTypeId','saPaymentTypes','PaymentTypeId' );
            END; 

-- PRINT inserting data 144, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saPayments'
                                AND FkColumn = 'TransactionId'
                                AND PkTable = 'saTransactions'
                                AND PkColumn = 'TransactionId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saPayments','TransactionId','saTransactions','TransactionId' );
            END; 

-- PRINT inserting data 145, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'AcademicYearId'
                                AND PkTable = 'saAcademicYears'
                                AND PkColumn = 'AcademicYearId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','AcademicYearId','saAcademicYears','AcademicYearId' );
            END; 

-- PRINT inserting data 146, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'BatchPaymentId'
                                AND PkTable = 'saBatchPayments'
                                AND PkColumn = 'BatchPaymentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','BatchPaymentId','saBatchPayments','BatchPaymentId' );
            END; 

-- PRINT inserting data 147, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'FundSourceId'
                                AND PkTable = 'saFundSources'
                                AND PkColumn = 'FundSourceId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','FundSourceId','saFundSources','FundSourceId' );
            END; 

-- PRINT inserting data 148, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'PaymentCodeId'
                                AND PkTable = 'saTransCodes'
                                AND PkColumn = 'TransCodeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','PaymentCodeId','saTransCodes','TransCodeId' );
            END; 

-- PRINT inserting data 149, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'PmtPeriodId'
                                AND PkTable = 'saPmtPeriods'
                                AND PkColumn = 'PmtPeriodId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','PmtPeriodId','saPmtPeriods','PmtPeriodId' );
            END; 

-- PRINT inserting data 150, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'StuEnrollId'
                                AND PkTable = 'arStuEnrollments'
                                AND PkColumn = 'StuEnrollId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','StuEnrollId','arStuEnrollments','StuEnrollId' );
            END; 

-- PRINT inserting data 151, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'StuEnrollPayPeriodId'
                                AND PkTable = 'arPrgChargePeriodSeq'
                                AND PkColumn = 'PrgChrPeriodSeqId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','StuEnrollPayPeriodId','arPrgChargePeriodSeq','PrgChrPeriodSeqId' );
            END; 

-- PRINT inserting data 152, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'TermId'
                                AND PkTable = 'arTerm'
                                AND PkColumn = 'TermId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','TermId','arTerm','TermId' );
            END; 

-- PRINT inserting data 153, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'TransCodeId'
                                AND PkTable = 'saTransCodes'
                                AND PkColumn = 'TransCodeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','TransCodeId','saTransCodes','TransCodeId' );
            END; 

-- PRINT inserting data 154, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'saTransactions'
                                AND FkColumn = 'TransTypeId'
                                AND PkTable = 'saTransTypes'
                                AND PkColumn = 'TransTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'saTransactions','TransTypeId','saTransTypes','TransTypeId' );
            END; 

-- PRINT inserting data 155, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syPeriods'
                                AND FkColumn = 'CampGrpId'
                                AND PkTable = 'syCampGrps'
                                AND PkColumn = 'CampGrpId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syPeriods','CampGrpId','syCampGrps','CampGrpId' );
            END; 

-- PRINT inserting data 156, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syPeriods'
                                AND FkColumn = 'EndTimeId'
                                AND PkTable = 'cmTimeInterval'
                                AND PkColumn = 'TimeIntervalId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syPeriods','EndTimeId','cmTimeInterval','TimeIntervalId' );
            END; 

-- PRINT inserting data 157, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syPeriods'
                                AND FkColumn = 'StartTimeId'
                                AND PkTable = 'cmTimeInterval'
                                AND PkColumn = 'TimeIntervalId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syPeriods','StartTimeId','cmTimeInterval','TimeIntervalId' );
            END; 

-- PRINT inserting data 158, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syPeriods'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syPeriods','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 159, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactAddresses'
                                AND FkColumn = 'AddrTypId'
                                AND PkTable = 'plAddressTypes'
                                AND PkColumn = 'AddressTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactAddresses','AddrTypId','plAddressTypes','AddressTypeId' );
            END; 

-- PRINT inserting data 160, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactAddresses'
                                AND FkColumn = 'CountryId'
                                AND PkTable = 'adCountries'
                                AND PkColumn = 'CountryId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactAddresses','CountryId','adCountries','CountryId' );
            END; 

-- PRINT inserting data 161, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactAddresses'
                                AND FkColumn = 'StateId'
                                AND PkTable = 'syStates'
                                AND PkColumn = 'StateId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactAddresses','StateId','syStates','StateId' );
            END; 

-- PRINT inserting data 162, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactAddresses'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactAddresses','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 163, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactAddresses'
                                AND FkColumn = 'StudentContactId'
                                AND PkTable = 'syStudentContacts'
                                AND PkColumn = 'StudentContactId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactAddresses','StudentContactId','syStudentContacts','StudentContactId' );
            END; 

-- PRINT inserting data 164, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactPhones'
                                AND FkColumn = 'PhoneTypeId'
                                AND PkTable = 'syPhoneType'
                                AND PkColumn = 'PhoneTypeId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactPhones','PhoneTypeId','syPhoneType','PhoneTypeId' );
            END; 

-- PRINT inserting data 165, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactPhones'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactPhones','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 166, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContactPhones'
                                AND FkColumn = 'StudentContactId'
                                AND PkTable = 'syStudentContacts'
                                AND PkColumn = 'StudentContactId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContactPhones','StudentContactId','syStudentContacts','StudentContactId' );
            END; 

-- PRINT inserting data 167, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContacts'
                                AND FkColumn = 'PrefixId'
                                AND PkTable = 'syPrefixes'
                                AND PkColumn = 'PrefixId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContacts','PrefixId','syPrefixes','PrefixId' );
            END; 

-- PRINT inserting data 168, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContacts'
                                AND FkColumn = 'RelationId'
                                AND PkTable = 'syRelations'
                                AND PkColumn = 'RelationId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContacts','RelationId','syRelations','RelationId' );
            END; 

-- PRINT inserting data 169, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContacts'
                                AND FkColumn = 'StatusId'
                                AND PkTable = 'syStatuses'
                                AND PkColumn = 'StatusId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContacts','StatusId','syStatuses','StatusId' );
            END; 

-- PRINT inserting data 170, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContacts'
                                AND FkColumn = 'StudentId'
                                AND PkTable = 'arStudent'
                                AND PkColumn = 'StudentId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContacts','StudentId','arStudent','StudentId' );
            END; 

-- PRINT inserting data 171, if it not exists
        IF NOT EXISTS ( SELECT  1
                        FROM    syRptAdhocRelations
                        WHERE   FkTable = 'syStudentContacts'
                                AND FkColumn = 'SuffixId'
                                AND PkTable = 'sySuffixes'
                                AND PkColumn = 'SuffixId' )
            BEGIN 
                INSERT  INTO syRptAdhocRelations
                VALUES  ( 'syStudentContacts','SuffixId','sySuffixes','SuffixId' );
            END; 

    END;	
	GO
-- =========================================================================================================
-- END  -- DE13258 Duplicate Column values in Student Adhoc (General Information) Report
-- ========================================================================
-------------------------------------------------------------------------------------
--Start DE13265: Lead/Student Campus Changed alert not shown
--------------------------------------------------------------------------------------
BEGIN TRANSACTION UpdateResourcesLead;
BEGIN TRY

    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadEducation.aspx'
    WHERE   ResourceID = 145
            AND ResourceURL = '~/AD/LeadEducation.aspx';
	
    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadPriorWork.aspx'
    WHERE   ResourceID = 146
            AND ResourceURL = '~/AD/LeadEmployment.aspx';

    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadSkills.aspx'
    WHERE   ResourceID = 147
            AND ResourceURL = '~/AD/LeadSkills.aspx';

    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadExtracurricular.aspx'
    WHERE   ResourceID = 148
            AND ResourceURL = '~/AD/LeadExtracurriculars.aspx';

    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadEnrollment.aspx'
    WHERE   ResourceID = 174
            AND ResourceURL = '~/AD/LeadEnrollments.aspx';
	
    UPDATE  syResources
    SET     ResourceURL = '~/AD/ALeadNotes.aspx'
    WHERE   ResourceID = 456
            AND ResourceURL = '~/AD/LeadNotes.aspx';
	
    UPDATE  syResources
    SET     ResourceURL = '~/AD/LeadRequirements.aspx'
    WHERE   ResourceID = 826
            AND ResourceURL = 'ad/leadrequirements.aspx';
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateResourcesLead;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
   
END CATCH;
IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateResourcesLead;
		--print 'commit'
    END;
-------------------------------------------------------------------------------------
--END DE13265: Lead/Student Campus Changed alert not shown
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start TC21344, US9883: Map Student Info Page fields to New Leads Table
--------------------------------------------------------------------------------------
BEGIN TRANSACTION UpdateResourcesLead;
BEGIN TRY

    DECLARE @FldId INT = (
                           SELECT   FldId
                           FROM     syFields
                           WHERE    FldName = 'Objective'
                         );
    DECLARE @TblFldsId INT = (
                               SELECT   TblFldsId
                               FROM     syTblFlds
                               WHERE    FldId = @FldId
                             );

    DELETE  FROM syRptParams
    WHERE   TblFldsId = @TblFldsId;

    DELETE  FROM syRptAdHocFields
    WHERE   TblFldsId = @TblFldsId;

    DELETE  FROM syResTblFlds
    WHERE   TblFldsId = @TblFldsId;

    DELETE  FROM syTblFlds
    WHERE   TblFldsId = @TblFldsId;

    DELETE  FROM syTblFlds
    WHERE   FldId = @FldId;

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage1 NVARCHAR(MAX)
       ,@ErrorSeverity1 INT
       ,@ErrorState1 INT;
    SELECT  @ErrorMessage1 = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity1 = ERROR_SEVERITY()
           ,@ErrorState1 = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateResourcesLead;
        END;
    RAISERROR (@ErrorMessage1, @ErrorSeverity1, @ErrorState1);
  
END CATCH;
IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION UpdateResourcesLead;
		--print 'commit'
    END;
-------------------------------------------------------------------------------------
--END TC21344, US9883: Map Student Info Page fields to New Leads Table
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Start DE13243 SP10 Reg Defect- Enrollment IDs Displayed as guids in 
--------------------------------------------------------------------------------------
BEGIN TRANSACTION FixEnrollmentId;

BEGIN TRY
	

    DECLARE @Sequence BIGINT;
    DECLARE @CharSequence VARCHAR(100);

    DECLARE @NewEnrollmentId VARCHAR(100);
    DECLARE @StudentId UNIQUEIDENTIFIER;
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;

    DECLARE CURSOR_ENROLLMENTS CURSOR
    FOR
        --SELECT  CONCAT(CONCAT(CONCAT(SUBSTRING(CONVERT(NVARCHAR(4),YEAR(enrollment.EnrollDate)),1,2),
        --                             SUBSTRING(CONVERT(NVARCHAR(2),MONTH(enrollment.EnrollDate)),1,1)),
        --                      SUBSTRING(CONVERT(NVARCHAR(2),DAY(enrollment.EnrollDate)),1,1)),
        --               CONCAT(UPPER(SUBSTRING(LTRIM(leads.LastName),1,2)),UPPER(SUBSTRING(LTRIM(leads.FirstName),1,1)))) + '0' AS NewEnrollmentId
        --       ,enrollment.StudentId
        --       ,enrollment.StuEnrollId
        --FROM    arStuEnrollments enrollment
        --INNER JOIN adLeads leads ON enrollment.StudentId = leads.StudentId
        --WHERE   enrollment.EnrollmentId = CONVERT(NVARCHAR(50),enrollment.StuEnrollId);
		
        SELECT  SUBSTRING(CONVERT(NVARCHAR(4),YEAR(ASE.EnrollDate)),1,2) + SUBSTRING(CONVERT(NVARCHAR(2),MONTH(ASE.EnrollDate)),1,1)
                + SUBSTRING(CONVERT(NVARCHAR(2),DAY(ASE.EnrollDate)),1,1) + UPPER(SUBSTRING(LTRIM(AL.LastName),1,2)) + UPPER(SUBSTRING(LTRIM(AL.FirstName),1,1))
                + '0' AS NewEnrollmentId
               ,ASE.StudentId
               ,ASE.StuEnrollId
        FROM    arStuEnrollments AS ASE
        INNER JOIN adLeads AS AL ON AL.StudentId = ASE.StudentId
        WHERE   ASE.EnrollmentId = CONVERT(NVARCHAR(50),ASE.StuEnrollId);

    OPEN CURSOR_ENROLLMENTS;
    FETCH NEXT FROM CURSOR_ENROLLMENTS INTO @NewEnrollmentId,@StudentId,@StuEnrollId;  
    WHILE @@FETCH_STATUS = 0
        BEGIN   		   
            SET @Sequence = (
                              SELECT    MAX(Student_SeqID) AS Student_SeqID
                              FROM      syGenerateStudentID
                            );
  
            SET @Sequence = @Sequence + 1; 

            INSERT  INTO syGenerateStudentID
                    ( Student_SeqID,ModDate )
            VALUES  ( @Sequence,GETDATE() );

            SET @CharSequence = CONVERT(NVARCHAR(100),@Sequence);
            SET @CharSequence = SUBSTRING(@CharSequence,LEN(@CharSequence) - 1,LEN(@CharSequence));

            UPDATE  arStuEnrollments
            SET     EnrollmentId = @NewEnrollmentId + @CharSequence
                   ,ModDate = GETDATE()
            WHERE   StudentId = @StudentId
                    AND StuEnrollId = @StuEnrollId;
		
            FETCH NEXT FROM CURSOR_ENROLLMENTS INTO @NewEnrollmentId,@StudentId,@StuEnrollId;  
        END;
    CLOSE CURSOR_ENROLLMENTS;   
    DEALLOCATE CURSOR_ENROLLMENTS;
END TRY
BEGIN CATCH

    DECLARE @ErrorMessage2 NVARCHAR(MAX)
       ,@ErrorSeverity2 INT
       ,@ErrorState2 INT;
    SELECT  @ErrorMessage2 = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity2 = ERROR_SEVERITY()
           ,@ErrorState2 = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION FixEnrollmentId;
        END;
    RAISERROR (@ErrorMessage2, @ErrorSeverity2, @ErrorState2);

END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION FixEnrollmentId;

    END;

GO

-------------------------------------------------------------------------------------
--END DE13243 SP10 Reg Defect- Enrollment IDs Displayed as guids in 
--------------------------------------------------------------------------------------
--Start DE13268 (SP10) Reg Defect - SAP Reports, SAP issues. Reports generate multiple lines for one result.
--Data clean up
--------------------------------------------------------------------------------------
BEGIN TRANSACTION DeleteDupSAPCheckResults;

BEGIN TRY
    DELETE  arSAPChkResults
    FROM    arSAPChkResults scr
    JOIN    (
              SELECT    StuEnrollId
                       ,Period
                       ,COUNT(StdRecKey) dupCount
                       ,MAX(ModDate) MaxModDate
              FROM      arSAPChkResults
              WHERE     IsMakingSAP = 1
              GROUP BY  StuEnrollId
                       ,Period
                       ,IsMakingSAP
              HAVING    COUNT(StdRecKey) > 1
            ) dups ON scr.StuEnrollId = dups.StuEnrollId
                      AND scr.Period = dups.Period
                      AND scr.IsMakingSAP = 1
                      AND scr.ModDate != dups.MaxModDate; 

    DELETE  arSAPChkResults
    FROM    arSAPChkResults scr
    JOIN    (
              SELECT    StuEnrollId
                       ,Period
                       ,ISNULL(Comments,'null value') Comments
                       ,COUNT(StdRecKey) dupCount
                       ,MAX(ModDate) MaxModDate
              FROM      arSAPChkResults
              WHERE     IsMakingSAP = 0
              GROUP BY  StuEnrollId
                       ,Period
                       ,ISNULL(Comments,'null value')
              HAVING    COUNT(StdRecKey) > 1
            ) dups ON scr.StuEnrollId = dups.StuEnrollId
                      AND scr.Period = dups.Period
                      AND scr.IsMakingSAP = 0
                      AND ISNULL(scr.Comments,'null value') = ISNULL(dups.Comments,'null value')
                      AND scr.ModDate != dups.MaxModDate; 


END TRY
BEGIN CATCH
    
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION DeleteDupSAPCheckResults;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION DeleteDupSAPCheckResults;

    END;

GO

-------------------------------------------------------------------------------------
--END DE13268 (SP10) Reg Defect - SAP Reports, SAP issues. Reports generate multiple lines for one result.
--------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
-- DE13321 3.8 Reg Defect- Unfriendly error pop up message is displayed when import Leads
-- JTorres  -- Correct the Catalog Table plAddressTypes to be compatible with SP usp_IL_ProcessImportLeadFile
-------------------------------------------------------------------------------------------------------------
 --Note  Code moved up 


-------------------------------------------------------------------------------------------------------------
-- END  --  DE13321 3.8 Reg Defect- Unfriendly error pop up message is displayed when import Leads
-------------------------------------------------------------------------------------------------------------
--Start DE13293 - 3.8 Reg Defect - Can't create or modify required fields (HIDE PAGE)
--------------------------------------------------------------------------------------
BEGIN TRANSACTION DeleteMaintReqFieldsPage;

BEGIN TRY
    DELETE  syMenuItems
    FROM    syMenuItems mi
    JOIN    syResources r ON mi.ResourceId = r.ResourceID
                             AND r.Resource = 'Set Up Required Fields';

END TRY
BEGIN CATCH

    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT;
    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION AddNewFields;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION DeleteMaintReqFieldsPage;

    END;

GO
-------------------------------------------------------------------------------------
--END DE13293 - 3.8 Reg Defect - Can't create or modify required fields (HIDE PAGE)
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--START DE13332 - (3.8 REGRESSION):User with full access to the academics has no access 
-- to the "un-enroll Student" at the academics/common tasks
--------------------------------------------------------------------------------------
UPDATE  syNavigationNodes
SET     ParentId = (
                     SELECT ParentId
                     FROM   syNavigationNodes
                     WHERE  ResourceId = (
                                           SELECT   ResourceID
                                           FROM     syResources
                                           WHERE    Resource = 'Register Students'
                                         )
                   )
WHERE   ResourceId = (
                       SELECT   ResourceID
                       FROM     syResources
                       WHERE    Resource = 'UnEnroll Students'
                     );
GO
-------------------------------------------------------------------------------------
--END DE13332 - (3.8 REGRESSION):User with full access to the academics has no access to the "Unenroll Student" at the academics/common tasks
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--START	DE13339 - Drop-downs options are not populated in QuickLead
--------------------------------------------------------------------------------------
DECLARE @FldID INT;
IF EXISTS ( SELECT  *
            FROM    adLeadFields
            WHERE   LeadField = 5557 )
    BEGIN
        SET @FldID = (
                       SELECT   FldId
                       FROM     syTblFlds
                       WHERE    FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'Extension'
                                        )
                                AND TblId = (
                                              SELECT    TblId
                                              FROM      syTables
                                              WHERE     TblName = 'adLeadPhone'
                                            )
                     );
        IF NOT EXISTS ( SELECT  *
                        FROM    adLeadFields
                        WHERE   LeadID = @FldID )
            BEGIN	
                INSERT  INTO adLeadFields
                VALUES  ( @FldID  -- LeadID - int
                          ,GETDATE()  -- ModDate - datetime
                          ,'Support'  -- ModUser - varchar(50)
                          ,5557  -- LeadField - int
                          );
            END;	
    END;	
IF EXISTS ( SELECT  *
            FROM    adLeadFields
            WHERE   LeadField = 5556 )
    BEGIN
        SET @FldID = (
                       SELECT   FldId
                       FROM     syTblFlds
                       WHERE    FldId = (
                                          SELECT    FldId
                                          FROM      syFields
                                          WHERE     FldName = 'AddressApto'
                                        )
                                AND TblId = (
                                              SELECT    TblId
                                              FROM      syTables
                                              WHERE     TblName = 'adLeadAddresses'
                                            )
                     );
        IF NOT EXISTS ( SELECT  *
                        FROM    adLeadFields
                        WHERE   LeadID = @FldID )
            BEGIN	
                INSERT  INTO adLeadFields
                VALUES  ( @FldID  -- LeadID - int
                          ,GETDATE()  -- ModDate - datetime
                          ,'Support'  -- ModUser - varchar(50)
                          ,5556  -- LeadField - int
                          );
            END;	
    END;	
GO

DECLARE @FldID INT;
IF EXISTS ( SELECT  *
            FROM    adLeadFields
            WHERE   LeadField = 5559 )
    BEGIN

        BEGIN TRANSACTION Quick1;

        BEGIN TRY
            SET @FldID = (
                           SELECT   FldId
                           FROM     syTblFlds
                           WHERE    FldId = (
                                              SELECT    FldId
                                              FROM      syFields
                                              WHERE     FldName = 'ProgramScheduleId'
                                            )
                                    AND TblId = (
                                                  SELECT    TblId
                                                  FROM      syTables
                                                  WHERE     TblName = 'adLeads'
                                                )
                         );
            IF NOT EXISTS ( SELECT  *
                            FROM    adLeadFields
                            WHERE   LeadID = @FldID )
                BEGIN	
                    INSERT  INTO adLeadFields
                    VALUES  ( @FldID  -- LeadID - int
                              ,GETDATE()  -- ModDate - datetime
                              ,'Support'  -- ModUser - varchar(50)
                              ,5559  -- LeadField - int
                              );
                END;	
            SET @FldID = (
                           SELECT   FldId
                           FROM     syTblFlds
                           WHERE    FldId = (
                                              SELECT    FldId
                                              FROM      syFields
                                              WHERE     FldName = 'AttendTypeId'
                                            )
                                    AND TblId = (
                                                  SELECT    TblId
                                                  FROM      syTables
                                                  WHERE     TblName = 'adLeads'
                                                )
                         );
            IF NOT EXISTS ( SELECT  *
                            FROM    adLeadFields
                            WHERE   LeadID = @FldID )
                BEGIN	
                    INSERT  INTO adLeadFields
                    VALUES  ( @FldID  -- LeadID - int
                              ,GETDATE()  -- ModDate - datetime
                              ,'Support'  -- ModUser - varchar(50)
                              ,5559  -- LeadField - int
                              );
                END;
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
               ,@ErrorSeverity INT
               ,@ErrorState INT;
            SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
                   ,@ErrorSeverity = ERROR_SEVERITY()
                   ,@ErrorState = ERROR_STATE();
            IF ( @@TRANCOUNT > 0 )
                BEGIN
                    ROLLBACK TRANSACTION Quick1;
                END;
            RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
        END CATCH;	
	
        IF @@TRANCOUNT > 0
            BEGIN
                COMMIT TRANSACTION Quick1;
            END;				
    END;
	
GO
-------------------------------------------------------------------------------------
--END	DE13339 - Dropdowns options are not populated in QuickLead
--------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start	DE13365 - 
--------------------------------------------------------------------------------------
BEGIN
    DECLARE @ParentId UNIQUEIDENTIFIER
       ,@ResourceId INT
       ,@ParentResourceId INT
       ,@studentcontactsparentid UNIQUEIDENTIFIER
       ,@hierarchyid UNIQUEIDENTIFIER;
    DECLARE @StudentContactsMenuItemId INT
       ,@PersonalInformationId INT
       ,@ManageStudentsId INT
       ,@AcademicsMenuItemId INT;
    SET @ResourceId = (
                        SELECT TOP 1
                                ResourceID
                        FROM    syResources
                        WHERE   ResourceURL = '~/AR/StudentContacts.aspx'
                      );
    SET @AcademicsMenuItemId = (
                                 SELECT TOP 1
                                        MenuItemId
                                 FROM   syMenuItems
                                 WHERE  MenuName = 'Academics'
                               );
    SET @ManageStudentsId = (
                              SELECT TOP 1
                                        MenuItemId
                              FROM      syMenuItems
                              WHERE     MenuName = 'Manage Students'
                                        AND ParentId = @AcademicsMenuItemId
                            );
    SET @PersonalInformationId = (
                                   SELECT TOP 1
                                            MenuItemId
                                   FROM     syMenuItems
                                   WHERE    MenuName = 'Personal Information'
                                            AND ParentId = @ManageStudentsId
                                 );
    SET @studentcontactsparentid = (
                                     SELECT TOP 1
                                            HierarchyId
                                     FROM   syMenuItems
                                     WHERE  MenuName = 'Personal Information'
                                            AND ParentId = @ManageStudentsId
                                   );
    SET @StudentContactsMenuItemId = (
                                       SELECT TOP 1
                                                MenuItemId
                                       FROM     syMenuItems
                                       WHERE    MenuName = 'Student Contacts'
                                                AND ParentId = @PersonalInformationId
                                     );
    IF NOT EXISTS ( SELECT  *
                    FROM    syNavigationNodes
                    WHERE   ResourceId = 155
                            AND ParentId = @studentcontactsparentid )
        BEGIN
            INSERT  INTO syNavigationNodes
                    (
                     HierarchyId
                    ,HierarchyIndex
                    ,ResourceId
                    ,ParentId
                    ,ModUser
                    ,ModDate
                    ,IsPopupWindow
                    ,IsShipped
                    )
            VALUES  (
                     NEWID()
                    ,4
                    ,155
                    ,@studentcontactsparentid
                    ,'sa'
                    ,GETDATE()
                    ,0
                    ,1
                    );
        END;
    SET @hierarchyid = (
                         SELECT TOP 1
                                HierarchyId
                         FROM   syNavigationNodes
                         WHERE  ResourceId = 155
                                AND ParentId = @studentcontactsparentid
                       );
    IF NOT EXISTS ( SELECT  *
                    FROM    syMenuItems
                    WHERE   MenuName = 'Student Contacts'
                            AND ParentId = @PersonalInformationId )
        BEGIN
            INSERT  INTO syMenuItems
                    (
                     MenuName
                    ,DisplayName
                    ,Url
                    ,MenuItemTypeId
                    ,ParentId
                    ,DisplayOrder
                    ,IsPopup
                    ,ModDate
                    ,ModUser
                    ,IsActive
                    ,ResourceId
                    ,HierarchyId
                    ,ModuleCode
                    ,MRUType
                    ,HideStatusBar
                    )
            VALUES  (
                     'Student Contacts'
                    ,'Student Contacts'
                    ,'/AR/StudentContacts.aspx'
                    ,4
                    ,@PersonalInformationId
                    ,200
                    ,0
                    ,GETDATE()
                    ,'support'
                    ,1
                    ,155
                    ,@hierarchyid
                    ,NULL
                    ,1
                    ,NULL
                    );
        END; 
END; 
GO

-------------------------------------------------------------------------------------
--End	DE13365 - 
--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------
--Mark transaction codes mapped to Applicant Fee as NOT 1098T
-------------------------------------------------------------------------------------------------------------
UPDATE  saTransCodes
SET     Is1098T = 0
WHERE   SysTransCodeId = 20
        AND Is1098T = 1;
GO
-- =======================================================
--START DE13320 Urgent: Advantage Escalation User Permission (Beta)
-- =======================================================
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 837
                        AND Resource = 'Pell Recipients & Stafford Sub Recipients without Pell' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 837 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
                       ,ResourceTypeID = 5
                       ,ResourceURL = '~/sy/ParamReport.aspx'
                       ,SummListId = NULL
                       ,ChildTypeId = 5
                       ,ModDate = GETDATE()
                       ,ModUser = 'sa'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 1
                       ,UsedIn = 975
                       ,TblFldsId = NULL
                       ,DisplayName = NULL
                WHERE   ResourceID = 837;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
	                    )
                VALUES  (
                         837  -- ResourceID - smallint
                        ,'Pell Recipients & Stafford Sub Recipients without Pell'  -- Resource - varchar(200)
                        ,5  -- ResourceTypeID - tinyint
                        ,'~/sy/ParamReport.aspx'  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,5  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'sa'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,1  -- MRUTypeId - smallint
                        ,975  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,NULL  -- DisplayName - varchar(200)
	                    );
            END;	
    END;	
GO	
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 838
                        AND Resource = 'History' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 838 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'History'
                       ,ResourceTypeID = 3
                       ,ResourceURL = '~/AD/AleadHistory.aspx'
                       ,SummListId = NULL
                       ,ChildTypeId = NULL
                       ,ModDate = GETDATE()
                       ,ModUser = 'support'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 4
                       ,UsedIn = 975
                       ,TblFldsId = NULL
                       ,DisplayName = 'History'
                WHERE   ResourceID = 838;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
                        )
                VALUES  (
                         838  -- ResourceID - smallint
                        ,'History'  -- Resource - varchar(200)
                        ,3  -- ResourceTypeID - tinyint
                        ,'~/AD/AleadHistory.aspx'  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,NULL  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'support'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,4  -- MRUTypeId - smallint
                        ,975  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,'History'  -- DisplayName - varchar(200)
                        );
            END;	
    END;	
GO	

IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 839
                        AND Resource = 'Winter - Admissions' )
    BEGIN 
        IF EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   ResourceID = 839 )
            BEGIN	
                UPDATE  syResources
                SET     Resource = 'Winter - Admissions'
                       ,ResourceTypeID = 2
                       ,ResourceURL = NULL
                       ,SummListId = NULL
                       ,ChildTypeId = 5
                       ,ModDate = GETDATE()
                       ,ModUser = 'support'
                       ,AllowSchlReqFlds = 0
                       ,MRUTypeId = 1
                       ,UsedIn = 512
                       ,TblFldsId = NULL
                       ,DisplayName = NULL
                WHERE   ResourceID = 839;
            END;	
        ELSE
            BEGIN	
                INSERT  INTO syResources
                        (
                         ResourceID
                        ,Resource
                        ,ResourceTypeID
                        ,ResourceURL
                        ,SummListId
                        ,ChildTypeId
                        ,ModDate
                        ,ModUser
                        ,AllowSchlReqFlds
                        ,MRUTypeId
                        ,UsedIn
                        ,TblFldsId
                        ,DisplayName
	                    )
                VALUES  (
                         839  -- ResourceID - smallint
                        ,'Winter - Admissions'  -- Resource - varchar(200)
                        ,2  -- ResourceTypeID - tinyint
                        ,NULL  -- ResourceURL - varchar(100)
                        ,NULL  -- SummListId - smallint
                        ,5  -- ChildTypeId - tinyint
                        ,GETDATE()  -- ModDate - datetime
                        ,'support'  -- ModUser - varchar(50)
                        ,0  -- AllowSchlReqFlds - bit
                        ,1  -- MRUTypeId - smallint
                        ,512  -- UsedIn - int
                        ,NULL  -- TblFldsId - int
                        ,NULL  -- DisplayName - varchar(200)
	                    );
            END;	
    END;	
GO	
IF NOT EXISTS ( SELECT  *
                FROM    syReports
                WHERE   ReportName = 'PellRecipientsandStaffordDetailReport'
                        AND ResourceId = 837 )
    BEGIN
        UPDATE  syReports
        SET     ResourceId = 837
        WHERE   ReportName = 'PellRecipientsandStaffordDetailReport';
    END;	
GO	
DECLARE @ParentId UNIQUEIDENTIFIER
   ,@ResourceId INT
   ,@ParentResourceId INT;
-- IPEDS --> winter admissions
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'IPEDS - General Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            ParentId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );

--Set @ResourceId = (select Top 1 ResourceId from syResources where Resource='Winter - Admissions')
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Winter - Admissions'
                            AND ResourceTypeID = 2
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
-- IPEDS --> winter admissions --> All Inst - Test Scores - Detail
--Set @ParentResourceId=(select Top 1 ResourceId from syResources where Resource='Winter - Admissions')
SET @ParentResourceId = (
                          SELECT    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Winter - Admissions'
                                    AND ResourceTypeID = 2
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );

SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Detail'
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Summary'
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
----Spring - Fall Enrollment - All Institution Reports -->  Part A - Detail & Summary - Dist Ed
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Spring - Fall Enrollment - All Institution Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Part A - Detail & Summary - Dist. Ed'
                  );
 --done successfully
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
-----Reports > IPEDS Reports > Winter Student FinAid > Part II - Military Benefits - Detail Sumary
SET @ParentResourceId = (
                          SELECT TOP 1
                                    ResourceID
                          FROM      syResources
                          WHERE     Resource = 'Winter - Student Financial Aid Reports'
                        );
SET @ParentId = (
                  SELECT TOP 1
                            HierarchyId
                  FROM      syNavigationNodes
                  WHERE     ResourceId = @ParentResourceId
                );
SET @ResourceId = (
                    SELECT TOP 1
                            ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Part II - Military Benefits - Detail & Summary'
                  );
 --done successfully
IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()
                ,4
                ,@ResourceId
                ,@ParentId
                ,'sa'
                ,GETDATE()
                ,0
                ,1
                );
    END;
	GO	
--473;
 -- previously 207

--Reports > IPEDS Reports > Fall-Completions - All Institutions - Detail & Summary Reports > Detail
--Reports > IPEDS Reports > Fall-Completions - All Institutions - Detail & Summary Reports > Summary
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   Resource IN ( 'Detail','Summary' )
                        AND UsedIn = 975 )
    BEGIN	
        UPDATE  syResources
        SET     UsedIn = 975
               ,ModDate = GETDATE()
               ,ModUser = 'sa'
        WHERE   Resource IN ( 'Detail','Summary' );
    END;	
--( 444,445 );
-- previously where 0
--Reports > Academics > General Reports > Time Clock Exceptions
--Reports > Academics > General Reports > Time Clock Punches
IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   Resource IN ( 'Time Clock Exceptions','Time Clock Punches' )
                        AND UsedIn = 975 )
    BEGIN	
        UPDATE  syResources
        SET     UsedIn = 975
               ,ModDate = GETDATE()
               ,ModUser = 'sa'
        WHERE   Resource IN ( 'Time Clock Exceptions','Time Clock Punches' );
    END;	
--( 585,586 );
 -- previously 512
 GO
DECLARE @hAcademic UNIQUEIDENTIFIER
   ,@actualHierarchyId UNIQUEIDENTIFIER
   ,@reqResourceId INT
   ,@academicresourceId INT
   ,@studentResourceId INT
   ,@actualParentId UNIQUEIDENTIFIER
   ,@attendanceResId INT;
SET @attendanceResId = (
                         SELECT ResourceID
                         FROM   syResources
                         WHERE  Resource = 'Attendance'
                                AND ResourceTypeID = 2
                       );
SET @academicresourceId = (
                            SELECT  ResourceID
                            FROM    syResources
                            WHERE   Resource = 'Academics'
                                    AND ResourceTypeID = 1
                          );
SET @reqResourceId = (
                       SELECT   ResourceID
                       FROM     syResources
                       WHERE    Resource = 'Post Externship Attendance'
                     );
SET @studentResourceId = (
                           SELECT   ResourceID
                           FROM     syResources
                           WHERE    Resource = 'Student'
                         );
SET @hAcademic = (
                   SELECT TOP 1
                            HierarchyId
                   FROM     syNavigationNodes
                   WHERE    ResourceId = @academicresourceId
                 );
SET @actualHierarchyId = (
                           SELECT   HierarchyId
                           FROM     syNavigationNodes
                           WHERE    ResourceId = @studentResourceId
                                    AND ParentId = @hAcademic
                         );
SET @actualParentId = (
                        SELECT  HierarchyId
                        FROM    syNavigationNodes
                        WHERE   ParentId = @actualHierarchyId
                                AND ResourceId = @attendanceResId
                      );

IF NOT EXISTS ( SELECT  *
                FROM    syNavigationNodes
                WHERE   ResourceId = @reqResourceId
                        AND ParentId = @actualParentId )
    BEGIN	
--SELECT @actualParentId
        INSERT  INTO syNavigationNodes
                (
                 HierarchyId
                ,HierarchyIndex
                ,ResourceId
                ,ParentId
                ,ModUser
                ,ModDate
                ,IsPopupWindow
                ,IsShipped
                )
        VALUES  (
                 NEWID()  -- HierarchyId - uniqueidentifier
                ,4  -- HierarchyIndex - smallint
                ,@reqResourceId -- ResourceId - smallint
                ,@actualParentId  -- ParentId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                ,0  -- IsPopupWindow - bit
                ,1  -- IsShipped - bit
                );
    END;
GO
IF NOT EXISTS ( SELECT  *
                FROM    syMenuItems
                WHERE   MenuName LIKE 'Pell Recipients & Stafford Sub Recipients without Pell'
                        AND ResourceId = 837 )
    BEGIN
        UPDATE  syMenuItems
        SET     ResourceId = 837
               ,ModDate = GETDATE()
               ,ModUser = 'sa'
        WHERE   MenuName LIKE 'Pell Recipients & Stafford Sub Recipients without Pell';
    END;
	GO
-- =======================================================
--END DE13320 Urgent: Advantage Escalation User Permission (Beta)
-- =======================================================