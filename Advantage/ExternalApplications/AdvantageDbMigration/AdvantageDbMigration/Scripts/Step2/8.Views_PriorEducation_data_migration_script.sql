--==========================================================================================
--  US9717 PRIOR Education. View Creation
-- JAGG 
--==========================================================================================
-- Rename original Table arStudent to arStudentOld
PRINT 'If arStudentOld doesn''t exist renaming original Table arStudent to arStudentOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'plStudentEducationOld' )
    BEGIN
        EXEC sp_rename 'dbo.plStudentEducation','plStudentEducationOld';
    END; 
GO

PRINT 'Dropping the view plStudentEducation if exists';
GO 
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'plStudentEducation' )
    BEGIN
        DROP VIEW plStudentEducation;       
    END;
GO
-- Create the view plStudentEducation
PRINT 'Creating the view plStudentEducation';
GO 
CREATE VIEW plStudentEducation
AS
    SELECT  ALE.LeadEducationId AS StuEducationId
           ,AL.StudentId
           ,ALE.EducationInstId
           ,ALE.EducationInstType
           ,ALE.GraduatedDate
           ,ALE.FinalGrade
           ,ALE.CertificateId
           ,ALE.Comments
           ,ALE.ModUser
           ,ALE.ModDate
           ,ALE.Major
    FROM    adLeadEducation AS ALE
    INNER JOIN adLeads AS AL ON AL.LeadId = ALE.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
 
