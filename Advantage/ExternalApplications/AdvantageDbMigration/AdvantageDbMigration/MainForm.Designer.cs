﻿namespace AdvantageDbMigration
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ltitle = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsLabelVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsLabelTenant = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslabelOperation = new System.Windows.Forms.ToolStripStatusLabel();
            this.tspProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.labelVersion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gJsonGrid1 = new GConfiguration.GJsonGrid();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDeleteTenant = new System.Windows.Forms.Button();
            this.btnNewTenant = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tbVerification = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnSaveTest = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnVerificationGeneral = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tbInstallation = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnPrintInstallation = new System.Windows.Forms.Button();
            this.btnSaveLogConfiguration = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.BtnCancelInstallation = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cbVersion = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.cbLevelComp = new System.Windows.Forms.CheckBox();
            this.btnInstallation = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportTenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportTenantConfigurationToFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importTenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addTenantConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.developerToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAdvantageSupportUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setAdvantagePathsSettingsLocalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reStartLocalIISServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.installationToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setDBInitialVersionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.newClientInstallationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notesMigrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.ltitle);
            this.panel1.Location = new System.Drawing.Point(0, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(925, 46);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::AdvantageDbMigration.Properties.Resources.FAME;
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(93, 37);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // ltitle
            // 
            this.ltitle.AutoSize = true;
            this.ltitle.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltitle.Location = new System.Drawing.Point(111, 12);
            this.ltitle.Name = "ltitle";
            this.ltitle.Size = new System.Drawing.Size(333, 28);
            this.ltitle.TabIndex = 0;
            this.ltitle.Text = "Advantage DB Migration Batch";
            this.ltitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabelVersion,
            this.toolStripStatusLabel2,
            this.tsLabelTenant,
            this.toolStripStatusLabel1,
            this.tslabelOperation,
            this.tspProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 613);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(925, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsLabelVersion
            // 
            this.tsLabelVersion.ForeColor = System.Drawing.Color.DarkBlue;
            this.tsLabelVersion.Name = "tsLabelVersion";
            this.tsLabelVersion.Size = new System.Drawing.Size(45, 17);
            this.tsLabelVersion.Text = "Version";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabel2.Text = " | ";
            // 
            // tsLabelTenant
            // 
            this.tsLabelTenant.Name = "tsLabelTenant";
            this.tsLabelTenant.Size = new System.Drawing.Size(119, 17);
            this.tsLabelTenant.Text = "Tenant Display Name";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabel1.Text = " | ";
            // 
            // tslabelOperation
            // 
            this.tslabelOperation.Name = "tslabelOperation";
            this.tslabelOperation.Size = new System.Drawing.Size(0, 17);
            // 
            // tspProgress
            // 
            this.tspProgress.Name = "tspProgress";
            this.tspProgress.Size = new System.Drawing.Size(100, 16);
            this.tspProgress.Visible = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 73);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(925, 540);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.BackgroundImage = global::AdvantageDbMigration.Properties.Resources.portada;
            this.tabPage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tabPage4.Controls.Add(this.labelVersion);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.pictureBox2);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(917, 511);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Welcome";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // labelVersion
            // 
            this.labelVersion.Location = new System.Drawing.Point(-1, 378);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(910, 25);
            this.labelVersion.TabIndex = 2;
            this.labelVersion.Text = "version";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(8, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(906, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Advantage DB Migration Batch Tool";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = global::AdvantageDbMigration.Properties.Resources.FAME;
            this.pictureBox2.Location = new System.Drawing.Point(154, 191);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(602, 184);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.splitter1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(917, 511);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Configuration";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.splitter2);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(13, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(901, 505);
            this.panel3.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gJsonGrid1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 41);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(901, 424);
            this.panel6.TabIndex = 5;
            // 
            // gJsonGrid1
            // 
            this.gJsonGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gJsonGrid1.Location = new System.Drawing.Point(0, 0);
            this.gJsonGrid1.Margin = new System.Windows.Forms.Padding(4);
            this.gJsonGrid1.Name = "gJsonGrid1";
            this.gJsonGrid1.ShowSetting1 = false;
            this.gJsonGrid1.ShowSetting2 = false;
            this.gJsonGrid1.Size = new System.Drawing.Size(901, 424);
            this.gJsonGrid1.TabIndex = 0;
            this.gJsonGrid1.TenantList = null;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 38);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(901, 3);
            this.splitter2.TabIndex = 4;
            this.splitter2.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnDeleteTenant);
            this.panel5.Controls.Add(this.btnNewTenant);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Controls.Add(this.btnCancel);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 465);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(901, 40);
            this.panel5.TabIndex = 3;
            // 
            // btnDeleteTenant
            // 
            this.btnDeleteTenant.Location = new System.Drawing.Point(325, 7);
            this.btnDeleteTenant.Name = "btnDeleteTenant";
            this.btnDeleteTenant.Size = new System.Drawing.Size(127, 25);
            this.btnDeleteTenant.TabIndex = 3;
            this.btnDeleteTenant.Text = "Delete Tenant";
            this.btnDeleteTenant.UseVisualStyleBackColor = true;
            this.btnDeleteTenant.Click += new System.EventHandler(this.BtnDeleteTenantClick);
            // 
            // btnNewTenant
            // 
            this.btnNewTenant.Location = new System.Drawing.Point(219, 7);
            this.btnNewTenant.Name = "btnNewTenant";
            this.btnNewTenant.Size = new System.Drawing.Size(100, 25);
            this.btnNewTenant.TabIndex = 2;
            this.btnNewTenant.Text = "New Tenant";
            this.btnNewTenant.UseVisualStyleBackColor = true;
            this.btnNewTenant.Click += new System.EventHandler(this.BtnNewTenantClick);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 25);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(113, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 25);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(901, 38);
            this.panel4.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Configuration Parameters:";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(3, 3);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 505);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(917, 511);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Verification";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tbVerification);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(229, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(685, 505);
            this.panel8.TabIndex = 1;
            // 
            // tbVerification
            // 
            this.tbVerification.AcceptsReturn = true;
            this.tbVerification.BackColor = System.Drawing.Color.Gainsboro;
            this.tbVerification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbVerification.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVerification.Location = new System.Drawing.Point(0, 24);
            this.tbVerification.Multiline = true;
            this.tbVerification.Name = "tbVerification";
            this.tbVerification.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbVerification.Size = new System.Drawing.Size(685, 441);
            this.tbVerification.TabIndex = 3;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.btnSaveTest);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel9.Location = new System.Drawing.Point(0, 465);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(685, 40);
            this.panel9.TabIndex = 2;
            // 
            // btnSaveTest
            // 
            this.btnSaveTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveTest.Location = new System.Drawing.Point(6, 6);
            this.btnSaveTest.Name = "btnSaveTest";
            this.btnSaveTest.Size = new System.Drawing.Size(102, 25);
            this.btnSaveTest.TabIndex = 0;
            this.btnSaveTest.Text = "Save to File";
            this.btnSaveTest.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(685, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "Tests Results:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnVerificationGeneral);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(226, 505);
            this.panel7.TabIndex = 0;
            // 
            // btnVerificationGeneral
            // 
            this.btnVerificationGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerificationGeneral.Location = new System.Drawing.Point(23, 16);
            this.btnVerificationGeneral.Name = "btnVerificationGeneral";
            this.btnVerificationGeneral.Size = new System.Drawing.Size(169, 25);
            this.btnVerificationGeneral.TabIndex = 0;
            this.btnVerificationGeneral.Text = "Begin Test General";
            this.btnVerificationGeneral.UseVisualStyleBackColor = true;
            this.btnVerificationGeneral.Click += new System.EventHandler(this.BtnVerificationDbClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tbInstallation);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.panel11);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(917, 511);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Installation";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tbInstallation
            // 
            this.tbInstallation.AcceptsReturn = true;
            this.tbInstallation.BackColor = System.Drawing.SystemColors.Menu;
            this.tbInstallation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbInstallation.Location = new System.Drawing.Point(200, 20);
            this.tbInstallation.Multiline = true;
            this.tbInstallation.Name = "tbInstallation";
            this.tbInstallation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbInstallation.Size = new System.Drawing.Size(717, 453);
            this.tbInstallation.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(200, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(717, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Installation Log:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnPrintInstallation);
            this.panel11.Controls.Add(this.btnSaveLogConfiguration);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel11.Location = new System.Drawing.Point(200, 473);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(717, 38);
            this.panel11.TabIndex = 1;
            // 
            // btnPrintInstallation
            // 
            this.btnPrintInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintInstallation.Location = new System.Drawing.Point(160, 7);
            this.btnPrintInstallation.Name = "btnPrintInstallation";
            this.btnPrintInstallation.Size = new System.Drawing.Size(75, 25);
            this.btnPrintInstallation.TabIndex = 1;
            this.btnPrintInstallation.Text = "Print Log";
            this.btnPrintInstallation.UseVisualStyleBackColor = true;
            this.btnPrintInstallation.Click += new System.EventHandler(this.BtnPrintInstallationClick);
            // 
            // btnSaveLogConfiguration
            // 
            this.btnSaveLogConfiguration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveLogConfiguration.Location = new System.Drawing.Point(6, 6);
            this.btnSaveLogConfiguration.Name = "btnSaveLogConfiguration";
            this.btnSaveLogConfiguration.Size = new System.Drawing.Size(130, 25);
            this.btnSaveLogConfiguration.TabIndex = 0;
            this.btnSaveLogConfiguration.Text = "Save Log to File";
            this.btnSaveLogConfiguration.UseVisualStyleBackColor = true;
            this.btnSaveLogConfiguration.Click += new System.EventHandler(this.BtnSaveLogConfigurationClick);
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.BtnCancelInstallation);
            this.panel10.Controls.Add(this.label4);
            this.panel10.Controls.Add(this.cbVersion);
            this.panel10.Controls.Add(this.textBox1);
            this.panel10.Controls.Add(this.cbLevel);
            this.panel10.Controls.Add(this.cbLevelComp);
            this.panel10.Controls.Add(this.btnInstallation);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 511);
            this.panel10.TabIndex = 0;
            // 
            // BtnCancelInstallation
            // 
            this.BtnCancelInstallation.Enabled = false;
            this.BtnCancelInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelInstallation.Location = new System.Drawing.Point(12, 304);
            this.BtnCancelInstallation.Name = "BtnCancelInstallation";
            this.BtnCancelInstallation.Size = new System.Drawing.Size(171, 23);
            this.BtnCancelInstallation.TabIndex = 6;
            this.BtnCancelInstallation.Text = "Cancel Installation";
            this.BtnCancelInstallation.UseVisualStyleBackColor = true;
            this.BtnCancelInstallation.Click += new System.EventHandler(this.BtnCancelInstallationClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Migrate to Version:";
            // 
            // cbVersion
            // 
            this.cbVersion.FormattingEnabled = true;
            this.cbVersion.Items.AddRange(new object[] {
            "3.8",
            "3.8 GePatch",
            "3.8 SP1",
            "3.9",
            "3.9 SP1",
            "3.9 SP2",
            "3.9 SP3"
            });
            this.cbVersion.Location = new System.Drawing.Point(9, 257);
            this.cbVersion.Name = "cbVersion";
            this.cbVersion.Size = new System.Drawing.Size(174, 24);
            this.cbVersion.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(9, 139);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(174, 80);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "100 - SQL 2008 - 2008R2\r\n110 - SQL 2012\r\n120 - SQL 2014\r\n130 - SQL 2016\r\n";
            // 
            // cbLevel
            // 
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Items.AddRange(new object[] {
            "100",
            "110",
            "120",
            "130",
            "140"});
            this.cbLevel.Location = new System.Drawing.Point(8, 99);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(175, 24);
            this.cbLevel.TabIndex = 2;
            // 
            // cbLevelComp
            // 
            this.cbLevelComp.AutoSize = true;
            this.cbLevelComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLevelComp.Location = new System.Drawing.Point(9, 73);
            this.cbLevelComp.Name = "cbLevelComp";
            this.cbLevelComp.Size = new System.Drawing.Size(174, 19);
            this.cbLevelComp.TabIndex = 1;
            this.cbLevelComp.Text = "Change Compatibility Level";
            this.cbLevelComp.UseVisualStyleBackColor = true;
            // 
            // btnInstallation
            // 
            this.btnInstallation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInstallation.Location = new System.Drawing.Point(12, 20);
            this.btnInstallation.Name = "btnInstallation";
            this.btnInstallation.Size = new System.Drawing.Size(171, 25);
            this.btnInstallation.TabIndex = 0;
            this.btnInstallation.Text = "Begin Installation";
            this.btnInstallation.UseVisualStyleBackColor = true;
            this.btnInstallation.Click += new System.EventHandler(this.BtnInstallationClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(925, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportTenantToolStripMenuItem,
            this.importTenantToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportTenantToolStripMenuItem
            // 
            this.exportTenantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportTenantConfigurationToFileToolStripMenuItem});
            this.exportTenantToolStripMenuItem.Name = "exportTenantToolStripMenuItem";
            this.exportTenantToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.exportTenantToolStripMenuItem.Text = "&Export Tenant";
            // 
            // exportTenantConfigurationToFileToolStripMenuItem
            // 
            this.exportTenantConfigurationToFileToolStripMenuItem.Name = "exportTenantConfigurationToFileToolStripMenuItem";
            this.exportTenantConfigurationToFileToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.exportTenantConfigurationToFileToolStripMenuItem.Text = "Export Tenant Configuration to File";
            this.exportTenantConfigurationToFileToolStripMenuItem.Click += new System.EventHandler(this.ExportTenantConfigurationToFileToolStripMenuItemClick);
            // 
            // importTenantToolStripMenuItem
            // 
            this.importTenantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTenantConfigurationToolStripMenuItem});
            this.importTenantToolStripMenuItem.Name = "importTenantToolStripMenuItem";
            this.importTenantToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.importTenantToolStripMenuItem.Text = "&Import Tenant";
            // 
            // addTenantConfigurationToolStripMenuItem
            // 
            this.addTenantConfigurationToolStripMenuItem.Name = "addTenantConfigurationToolStripMenuItem";
            this.addTenantConfigurationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.addTenantConfigurationToolStripMenuItem.Text = "&Import Tenant Configuration";
            this.addTenantConfigurationToolStripMenuItem.Click += new System.EventHandler(this.ImportTenantConfigurationToolStripMenuItemClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(146, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.BtnExitClick);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.developerToolsToolStripMenuItem,
            this.toolStripSeparator1,
            this.installationToolsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // developerToolsToolStripMenuItem
            // 
            this.developerToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setAdvantageSupportUserToolStripMenuItem,
            this.setAdvantagePathsSettingsLocalToolStripMenuItem,
            this.reStartLocalIISServerToolStripMenuItem});
            this.developerToolsToolStripMenuItem.Name = "developerToolsToolStripMenuItem";
            this.developerToolsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.developerToolsToolStripMenuItem.Text = "Developer tools";
            // 
            // setAdvantageSupportUserToolStripMenuItem
            // 
            this.setAdvantageSupportUserToolStripMenuItem.Name = "setAdvantageSupportUserToolStripMenuItem";
            this.setAdvantageSupportUserToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.setAdvantageSupportUserToolStripMenuItem.Text = "&Set Advantage Support user";
            this.setAdvantageSupportUserToolStripMenuItem.Click += new System.EventHandler(this.SetAdvantageSupportUserToolStripMenuItemClick);
            // 
            // setAdvantagePathsSettingsLocalToolStripMenuItem
            // 
            this.setAdvantagePathsSettingsLocalToolStripMenuItem.Name = "setAdvantagePathsSettingsLocalToolStripMenuItem";
            this.setAdvantagePathsSettingsLocalToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.setAdvantagePathsSettingsLocalToolStripMenuItem.Text = "Set Advantage Paths Settings Local";
            this.setAdvantagePathsSettingsLocalToolStripMenuItem.Click += new System.EventHandler(this.SetAdvantagePathsSettingsLocalToolStripMenuItemClick);
            // 
            // reStartLocalIISServerToolStripMenuItem
            // 
            this.reStartLocalIISServerToolStripMenuItem.Name = "reStartLocalIISServerToolStripMenuItem";
            this.reStartLocalIISServerToolStripMenuItem.Size = new System.Drawing.Size(258, 22);
            this.reStartLocalIISServerToolStripMenuItem.Text = "&Re-Start Local IIS Server";
            this.reStartLocalIISServerToolStripMenuItem.Click += new System.EventHandler(this.ReStartLocalIisServerToolStripMenuItemClick);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(160, 6);
            // 
            // installationToolsToolStripMenuItem
            // 
            this.installationToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setDBInitialVersionToolStripMenuItem1,
            this.toolStripMenuItem2,
            this.newClientInstallationToolStripMenuItem});
            this.installationToolsToolStripMenuItem.Name = "installationToolsToolStripMenuItem";
            this.installationToolsToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.installationToolsToolStripMenuItem.Text = "Installation Tools";
            // 
            // setDBInitialVersionToolStripMenuItem1
            // 
            this.setDBInitialVersionToolStripMenuItem1.Name = "setDBInitialVersionToolStripMenuItem1";
            this.setDBInitialVersionToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.setDBInitialVersionToolStripMenuItem1.Text = "&Set DB initial Version";
            this.setDBInitialVersionToolStripMenuItem1.Click += new System.EventHandler(this.SetDbInitialVersionToolStripMenuItemClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(190, 6);
            // 
            // newClientInstallationToolStripMenuItem
            // 
            this.newClientInstallationToolStripMenuItem.Name = "newClientInstallationToolStripMenuItem";
            this.newClientInstallationToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.newClientInstallationToolStripMenuItem.Text = "&New Client installation";
            this.newClientInstallationToolStripMenuItem.Click += new System.EventHandler(this.NewClientInstalallationToolStripMenuItemClick);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notesMigrationToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // notesMigrationToolStripMenuItem
            // 
            this.notesMigrationToolStripMenuItem.Name = "notesMigrationToolStripMenuItem";
            this.notesMigrationToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.notesMigrationToolStripMenuItem.Text = "Notes Migration";
            this.notesMigrationToolStripMenuItem.Click += new System.EventHandler(this.NotesMigrationToolStripMenuItemClick);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 635);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Advantage DB Migration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
            this.Shown += new System.EventHandler(this.MainFormShown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label ltitle;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnVerificationGeneral;
        private System.Windows.Forms.TextBox tbVerification;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button btnSaveTest;
        private System.Windows.Forms.TextBox tbInstallation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button btnSaveLogConfiguration;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnInstallation;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btnPrintInstallation;
        private System.Windows.Forms.ToolStripMenuItem notesMigrationToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cbLevel;
        private System.Windows.Forms.CheckBox cbLevelComp;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbVersion;
        private System.Windows.Forms.ToolStripStatusLabel tsLabelVersion;
        private System.Windows.Forms.ToolStripStatusLabel tsLabelTenant;
        private System.Windows.Forms.ToolStripProgressBar tspProgress;
        private System.Windows.Forms.Button BtnCancelInstallation;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tslabelOperation;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportTenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportTenantConfigurationToFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importTenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTenantConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem developerToolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setAdvantageSupportUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem installationToolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newClientInstallationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setDBInitialVersionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private GConfiguration.GJsonGrid gJsonGrid1;
        private System.Windows.Forms.Button btnNewTenant;
        private System.Windows.Forms.Button btnDeleteTenant;
        private System.Windows.Forms.ToolStripMenuItem setAdvantagePathsSettingsLocalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reStartLocalIISServerToolStripMenuItem;
    }
}

