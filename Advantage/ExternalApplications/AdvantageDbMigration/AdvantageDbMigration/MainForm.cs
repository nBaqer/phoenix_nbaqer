﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainForm.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the MainForm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDbMigration
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDbMigration.Helpers;

    using GConfiguration;

    using GLibrary;
    using GLibrary.Business;
    using GLibrary.Facade;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;
    using GLibrary.GTypes.VerificationType;

    using Newtonsoft.Json;

    /// <summary>
    /// The main form.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// The tenant configuration file.
        /// </summary>
        public const string TenantConfigurationFile = "tenants.json";

        /// <summary>
        /// The token cancel source.
        /// </summary>
        private CancellationTokenSource tokenCancel;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();

            // Get application title
            this.ltitle.Text = ConfigurationManager.AppSettings["AppTitle"];

            // Get tool version
            this.labelVersion.Text = $@"Tool Version {Assembly.GetExecutingAssembly().GetName().Version}";
            this.tsLabelVersion.Text = this.labelVersion.Text;

            // configure the grid
            this.gJsonGrid1.ShowSetting1 = false;
            this.gJsonGrid1.ShowSetting2 = false;

            // Set grid event to get active tenant name
            this.gJsonGrid1.ChangeTenant += this.GJsonGrid1ActiveTenantName;
            this.gJsonGrid1.InitializeJsonGrid((Tenants)Tenants.Factory());

            // Select 3.9.3 as default
            this.cbVersion.SelectedIndex = 6;
        }
        #endregion

        #region Configurations

        ///// <summary>
        ///// The combo box tenants selected index changed.
        ///// </summary>
        ///// <param name="sender">
        ///// The sender.
        ///// </param>
        ///// <param name="e">
        ///// The e.
        ///// </param>
        //////private void CbTenantsSelectedIndexChanged(object sender, EventArgs e)
        //////{
        //////    var tenant = (Tenants)this.cbTenants.SelectedItem;

        //////    if (tenant != null)
        //////    {
        //////        this.lTenantName.Text = tenant.TenantName;
        //////        this.lTenantDb.Text = tenant.TenantDataBaseConnectionString;
        //////        this.lAdvantageDb.Text = tenant.AdvantageDataBaseConnectionString;
        //////        this.tsLabelTenant.Text = $@"  Client: {this.cbTenants.Text}";
        //////    }
        //////}

        /// <summary>
        /// The button cancel click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.gJsonGrid1.CancelTenantChanges();
            ////var content = File.ReadAllText(TenantConfigurationFile);
            ////this.tbEdition.Text = content;
        }

        /// <summary>
        /// The button save click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                var tenant = this.gJsonGrid1.ActiveTenant;
                this.gJsonGrid1.SaveTenantChanges();
                ////this.gJsonGrid1.InitializeJsonGrid((Tenants)Tenants.Factory());

            }
            catch (Exception ex)
            {
                var message = Common.CollectAllExceptionsMessages(ex);
                MessageBox.Show(
                    this,
                    $@">> Error in Configuration Format. Please re-edit configuration: {message}",
                    @"Error en Edition",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The import tenant configuration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ImportTenantConfigurationToolStripMenuItemClick(object sender, EventArgs e)
        {
            // Open Dialog to get configuration
            var open = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                DefaultExt = "txt",
                SupportMultiDottedExtensions = true,
                Filter = @".text files|*.txt| .json files|*.json"
            };
            if (open.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    this.gJsonGrid1.ImportTenantFromFile(open.FileName);
                    MessageBox.Show(
                        this,
                        @"File Imported!",
                        @"Imported",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        this,
                        $@"Error open the file: {exception.Message}",
                        @"Error Importing File",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// The replace tenant configuration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ReplaceTenantConfigurationToolStripMenuItemClick(object sender, EventArgs e)
        {
            ////    // Open Dialog to get configuration
            ////    var open = new OpenFileDialog
            ////    {
            ////        CheckFileExists = true,
            ////        Multiselect = false,
            ////        DefaultExt = "txt",
            ////        SupportMultiDottedExtensions = true,
            ////        Filter = @".text files|*.txt| .json files|*.json"
            ////    };
            ////    if (open.ShowDialog(this) == DialogResult.OK)
            ////    {
            ////        try
            ////        {
            ////            this.tbEdition.Text = File.ReadAllText(open.FileName);
            ////            MessageBox.Show(
            ////                this,
            ////                @"File Imported. Press Save to validate and finish the import process",
            ////                @"Importing",
            ////                MessageBoxButtons.OK,
            ////                MessageBoxIcon.Information);
            ////        }
            ////        catch (Exception exception)
            ////        {
            ////            MessageBox.Show(
            ////                this,
            ////                $@"Error open the file: {exception.Message}",
            ////                @"Error Importing File",
            ////                MessageBoxButtons.OK,
            ////                MessageBoxIcon.Error);
            ////        }
            ////    }
        }

        #endregion

        #region Verifications

        /// <summary>
        /// The button verification DB click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnVerificationDbClick(object sender, EventArgs e)
        {
            try
            {
                // Initialize tests
                this.tbVerification.BackColor = Color.Gainsboro;
                this.tbVerification.Text = Common.WriteLine(@"Begin Verifications Test for DB");
                this.tbVerification.Refresh();
                var tenant = this.gJsonGrid1.ActiveTenant;

                // Check the compatibility level of the database
                Cursor.Current = Cursors.WaitCursor;
                var level = DbInstallation.GetDatabaseLevelOfCompatibility(tenant);
                var sql = CompatibilityDbDictionary.CompatibilityLevelDictionary[level];
                this.tbVerification.Text += Common.WriteLine(
                    $@"The level of compatibility of the DB {tenant.TenantName} is {level} ({sql})");
                this.tbVerification.Text += Common.WriteLine(@" ");
                this.tbVerification.Refresh();
                Application.DoEvents();

                // Check if the version table exists
                var exists = DbInstallation.ExistsVersionTable(tenant);
                if (exists)
                {
                    var version = DbInstallation.GetDbVersion(tenant);
                    this.tbVerification.Text +=
                        Common.WriteLine(
                            $@"Actual Db Version: {version.GetStringVersion()}");
                    this.tbVerification.Text += Common.WriteLine(@" ");
                    this.tbVerification.Refresh();
                }
                else
                {
                    this.tbVerification.Text += Common.WriteLine(@"This Database has not Version Table");

                    // Check probable version
                    if (DbInstallation.VerifyViewArStudAddress(tenant))
                    {
                        // The database has some of 3.8 migration script passed.
                        this.tbVerification.Text +=
                            Common.WriteLine(
                                $@"can not be migrate with this tool. To migrate it with this tool{
                                        Environment.NewLine
                                    } Use [Tool] -> [Set Initial Version] to create the version table and set a initial point to DB");
                        this.tbVerification.Text += Common.WriteLine(@" ");
                    }
                    else
                    {
                        this.tbVerification.Text +=
                            Common.WriteLine(
                                $@"But it is a version below 3.8. You can use this tool to migrate it to 3.8 all upper");
                        this.tbVerification.Text += Common.WriteLine(@" ");
                    }
                }

                // Expose the site configuration settings and partially the API Token
                this.tbVerification.Text += Common.WriteLine(@" ");
                var site = AdvantageConfigurationSetting.GetSingleConfigurationSetting("AdvantageSiteUri", tenant);
                var service = AdvantageConfigurationSetting.GetSingleConfigurationSetting("AdvantageServiceUri", tenant);
                this.tbVerification.Text += Common.WriteLine($"Advantage Site Uri = {site}");
                this.tbVerification.Text += Common.WriteLine($"Advantage Services Uri = {service}");

                // API key verification...
                var result = VerificationsBo.TestWebApiKey(tenant);
                this.tbVerification.Text += Common.WriteLine(" ");
                this.tbVerification.Text += Common.WriteLine(result.Item1);

                this.tbVerification.BackColor = (result.Item2) ? Color.LightGreen : Color.LightYellow;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                this.tbVerification.BackColor = Color.LightPink;
                this.tbVerification.Text += $@"Error: {exception.Message}";
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }

            this.tbVerification.Text += Common.WriteLine(@"---------------------------------------------------------------------------------");
            this.tbVerification.Text += Common.WriteLine(@"Test Finished......");
        }

        #endregion

        #region Installation

        /// <summary>
        /// The button installation click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private async void BtnInstallationClick(object sender, EventArgs e)
        {
            this.tbInstallation.BackColor = Color.Gainsboro;
            this.InitProgressOperation("Installation in Progress...", Color.Green);

            // Read from file the installation script
            var content = File.ReadAllText("ScriptConfig.json");
            var scriptList = JsonConvert.DeserializeObject<IList<Scripts>>(content);

            this.btnInstallation.Enabled = false;
            this.BtnCancelInstallation.Enabled = true;
            var initDateTime = Common.GetTimeSpan();
            DbVersion ver;
            try
            {
                var tenant = this.gJsonGrid1.ActiveTenant;
                if (tenant == null)
                {
                    MessageBox.Show(
                        this,
                        @"Please select a tenant before begin Installations!",
                        @"Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    this.btnInstallation.Enabled = true;
                    return;
                }

                // Clear log and re-initialization
                this.tbInstallation.Text = Common.WriteLine($@"{Common.GetTimeSpan()} Begin Installation Processing for client  {this.tsLabelTenant.Text}");
                this.tbInstallation.Text += this.labelVersion.Text;
                this.tbInstallation.Text += Environment.NewLine;
                this.tbInstallation.Text += $@"Installing on {tenant.TenantDisplayName}";
                this.tbInstallation.Text += Environment.NewLine;

                this.tbInstallation.Refresh();

                // Review if it is necessary change the compatibility version
                if (this.cbLevelComp.Checked)
                {
                    // Get the actual compatibility level...
                    var level = DbInstallation.GetDatabaseLevelOfCompatibility(tenant);
                    var setting = Convert.ToInt32(this.cbLevel.Text);
                    if (level != setting)
                    {
                        // Change database level...
                        DbInstallation.ChangeCompatibilityLevel(tenant, setting);
                        level = DbInstallation.GetDatabaseLevelOfCompatibility(tenant);
                        this.tbInstallation.Text += $@"The compatibility Level of DB was change to {level}";
                        this.tbInstallation.Text += Environment.NewLine;
                        this.tbInstallation.Refresh();
                    }
                }

                // Check if table version does not exists and DB is 3.8
                if (!DbInstallation.ExistsVersionTable(tenant))
                {
                    // Table does not exists, Check if DB is 3.8 or bigger
                    if (DbInstallation.VerifyViewArStudAddress(tenant))
                    {
                        // Table is 3.8 and does not exists DB
                        this.tbInstallation.Text +=
                            @"Table Version does not exists and DB was update to 3.8 without using this tool";
                        this.tbInstallation.Text += Environment.NewLine;
                        this.tbInstallation.Text += @"DB not possible to update. Please use Tool ->  Set Initial Version to create the version table and set a initial point to DB, before try to update";
                        this.tbInstallation.Text += Environment.NewLine;
                        this.tbInstallation.Text +=
                            @"----------------------------------------------------------------------";
                        this.tbInstallation.BackColor = Color.LightGoldenrodYellow;
                        return;
                    }
                }

                // Create the version table if not exists ....
                // Check the initial version in the application.config
                var verString = ConfigurationManager.AppSettings["versionInitial"];
                string[] verArray = verString.Split('.');
                var iniversion = new DbVersion
                {
                    Major = Convert.ToInt32(verArray[0]),
                    Minor = Convert.ToInt32(verArray[1]),
                    Build = Convert.ToInt32(verArray[2]),
                    Revision = Convert.ToInt32(verArray[3]),
                    Description = "Initial Version",
                    ModUser = "SUPPORT"
                };

                var rows = DbInstallation.CreateTableVersion(tenant, iniversion);

                ////rows = DbInstallation.RunScript("Scripts/CreateDatabaseVersion.sql", tenant);
                this.tbInstallation.Text += rows > 1 ? Common.WriteLine(@"Version Table created") : string.Empty;

                // Get the Actual Database Version...
                ver = DbInstallation.GetDbVersion(tenant);
                ver.Description = "Initial Status";
                this.tbInstallation.Text +=
                    Common.WriteLine($@"DB Initial Version {ver.GetStringVersion()} - {ver.Description}");
                this.tbInstallation.Refresh();

                // Create context UI
                var context = TaskScheduler.FromCurrentSynchronizationContext();

                // Create Cancellation token
                this.tokenCancel = new CancellationTokenSource();

                // Begin loop to install all necessary scripts
                await this.RunAllScript(scriptList, ver, tenant, this.cbVersion.Text, context, this.tokenCancel.Token).ContinueWith(
                   (taskResult) =>
                        {
                            var token1 = new CancellationToken();
                            if (taskResult.Status == TaskStatus.Faulted)
                            {
                                Task.Factory.StartNew(
                                    () =>
                                        {
                                            this.ExceptionEventHandler(taskResult);
                                            this.FinalDbStatus(tenant, initDateTime);
                                        },
                                    token1,
                                    TaskCreationOptions.None,
                                    context);
                            }
                            else
                            {
                                Task.Factory.StartNew(
                                    () =>
                                        {
                                            this.FinalDbStatus(tenant, initDateTime);
                                            this.tbInstallation.BackColor = Color.LightGreen;
                                        },
                                    token1,
                                    TaskCreationOptions.None,
                                    context);
                            }
                        });
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                this.tbInstallation.BackColor = Color.LightPink;
                this.tbInstallation.Text += $@"Error: {exception.Message}";
            }
            finally
            {
                this.EndProgressOperation();
                this.btnInstallation.Enabled = true;
                this.BtnCancelInstallation.Enabled = false;
            }
        }

        /// <summary>
        /// The exception event handler.
        /// </summary>
        /// <param name="task">
        /// The task.
        /// </param>
        private void ExceptionEventHandler(Task task)
        {
            Exception exception = task.Exception;
            string message = string.Empty;
            if (exception != null)
            {
                message = exception.Message;
            }

            Console.WriteLine(exception);
            while (exception != null && exception.InnerException != null)
            {
                exception = exception.InnerException;
                message += Environment.NewLine + exception.Message;
            }

            this.tbInstallation.BackColor = Color.LightPink;
            this.tbInstallation.Text += $@"Error: {message}";
        }

        /// <summary>
        /// Helper to enter the final information in text box.
        /// </summary>
        /// <param name="tenant">The tenant used</param>
        /// <param name="initDateTime">The time stamp when program begin to verify</param>
        private void FinalDbStatus(ITenants tenant, DateTime initDateTime)
        {
            this.tbInstallation.Text += Environment.NewLine;
            this.tbInstallation.Text +=
                @"----------------------------------------------------------------------";
            this.tbInstallation.Text += Environment.NewLine;

            var ver = DbInstallation.GetDbVersion(tenant);
            if (ver != null)
            {
                this.tbInstallation.Text +=
                      Common.WriteLine($@"DB Final Version {ver.GetStringVersion()}");
            }

            this.tbInstallation.Text += Environment.NewLine;
            this.tbInstallation.Text +=
                $@"Overall Time running: {(DateTime.Now - initDateTime)}";
            this.tbInstallation.Text += Environment.NewLine;
            this.tbInstallation.Text +=
                Common.WriteLine($@"Finished at: {Common.GetTimeSpan()}........");
        }

        /// <summary>
        /// The execute script.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="ver">
        /// The version object.
        /// </param>
        /// <param name="scriptFile">
        /// The script file.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="DbVersion"/>.
        /// </returns>
        private DbVersion ExecuteScript(ITenants tenant, DbVersion ver, string scriptFile, string description, TaskScheduler context)
        {
            try
            {
                var version = $"Running on {ver.Major}.{ver.Minor}.{ver.Build}.{ver.Revision}. Script: {description}";
                var token = Task.Factory.CancellationToken;
                Task.Factory.StartNew(
                    () =>
                        {
                            Cursor.Current = Cursors.AppStarting;
                            this.tbInstallation.Text += $@"{version}, Begin at: {Common.GetTimeSpan()}";
                            this.tbInstallation.Refresh();
                            this.tbInstallation.SelectionStart = this.tbInstallation.Text.Length;
                            this.tbInstallation.ScrollToCaret();
                        },
                    token,
                    TaskCreationOptions.None,
                    context);

                int rows = 0;
                var sw = Path.GetFileNameWithoutExtension(scriptFile);
                var res = sw.StartsWith("HOST_") ? "HOST" : "ADVANTAGE";
                res = sw.ToUpper().StartsWith("JOB_") ? "JOB" : res;
                switch (res)
                {
                    case "JOB":
                        {
                            // Special Job work need the name of the database
                            rows = DbInstallation.ExecuteBatchNonQueryAttendanceJob(scriptFile, tenant);
                            break;
                        }

                    case "HOST":
                        {
                            // Execute script on the Tenant Db Database
                            rows = DbInstallation.ExecuteBatchNonQuery(scriptFile, tenant.TenantDataBaseConnectionString);
                            break;
                        }

                    default:
                        {
                            // Execute normal scripts
                            rows = DbInstallation.ExecuteBatchNonQuery(scriptFile, tenant);
                            break;
                        }
                }
                ////if (scriptFile.Contains("AttendanceJob"))
                ////{
                ////    // Special Job work need the name of the database
                ////    rows = DbInstallation.ExecuteBatchNonQueryAttendanceJob(scriptFile, tenant);
                ////}
                ////else
                ////{
                ////    // Normal scripts 
                ////    rows = DbInstallation.ExecuteBatchNonQuery(scriptFile, tenant);
                ////}

                Task.Factory.StartNew(
                    () =>
                        {
                            this.tbInstallation.Text += Environment.NewLine;
                            this.tbInstallation.Text +=
                                Common.WriteLine($@"Finished with {rows} rows modified. Elapsed Time: {Common.TimeDifference()}");
                            Cursor.Current = Cursors.Default;
                        },
                    token,
                    TaskCreationOptions.None,
                    context);
                ver.Description = description;
                return ver;
            }
            finally
            {
                Application.DoEvents();
            }
        }

        /// <summary>
        /// The button save log configuration click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnSaveLogConfigurationClick(object sender, EventArgs e)
        {
            var content = this.tbInstallation.Text;
            var dialog = new SaveFileDialog();
            dialog.DefaultExt = "txt";
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                File.WriteAllText(dialog.FileName, content);
            }
        }

        /// <summary>
        /// The button print installation click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnPrintInstallationClick(object sender, EventArgs e)
        {
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += this.PrintDocumentOnPrintPage;
            printDocument.Print();
        }

        /// <summary>
        /// The print document on print page.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PrintDocumentOnPrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(this.tbInstallation.Text, this.tbInstallation.Font, Brushes.Black, 10, 25);
        }

        /// <summary>
        /// The is updated to version.
        /// </summary>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="maxUpdate">
        /// The max update.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// if they are not selected something in version combo box
        /// </exception>
        private bool IsUpdatedToVersion(DbVersion version, string maxUpdate)
        {
            var verweight = (version.Major * 20000) + (version.Minor * 2000) + (version.Build * 100) + version.Revision;
            switch (maxUpdate)
            {
                case "3.7.SP10":
                    {
                        return !(verweight < 76000);
                    }

                case "3.8": // 3.8.0.0 -- 3.8.0.99
                    {
                        return !(verweight < 76100);
                    }

                case "3.8 GePatch": // 3.8.1.0 -- 3.8.1.49
                    {
                        return !(verweight < 76150);
                    }

                case "3.8 SP1": // 3.8.1.50 -- 3.8.1.99
                    {
                        return !(verweight < 78000);
                    }

                case "3.9": // 3.9.0.0 -- 3.9.0.1
                    {
                        return !(verweight < 78100);
                    }

                case "3.9 SP1": // 3.9.1.0 -- 3.9.1.2
                    {
                        return !(verweight < 78200);
                    }

                case "3.9 SP2": // 3.9.2.0 -- 3.9.2.1
                    {

                        return !(verweight < 78300);
                    }

                case "3.9 SP3": // 3.9.3.0 -- 3.9.3.1
                    {

                        return false;
                    }
                default:
                    {
                        throw new ApplicationException("Version Does not Exists");
                    }
            }
        }

        #endregion

        #region Common Exit Application
        /// <summary>
        /// Application Exist <code>finito</code> exit!
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// The initialization progress operation.
        /// </summary>
        /// <param name="operationName">
        /// The operation name.
        /// </param>
        /// <param name="color">
        /// The color.
        /// </param>
        private void InitProgressOperation(string operationName, Color color)
        {
            this.tslabelOperation.Text = operationName;
            this.tspProgress.Visible = true;
            this.tspProgress.ForeColor = color;
            this.tspProgress.Style = ProgressBarStyle.Marquee;
            this.tspProgress.MarqueeAnimationSpeed = (color == Color.Yellow) ? 50 : 20;
        }

        /// <summary>
        /// The end progress operation.
        /// </summary>
        private void EndProgressOperation()
        {
            this.tslabelOperation.Text = string.Empty;
            this.tspProgress.Visible = false;
            this.tspProgress.Style = ProgressBarStyle.Continuous;
            this.tspProgress.MarqueeAnimationSpeed = 0;
        }

        #endregion

        #region Menu Items

        /// <summary>
        /// The about tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var about = new About();
            about.Show();
        }

        /// <summary>
        /// The notes migration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void NotesMigrationToolStripMenuItemClick(object sender, EventArgs e)
        {
            var help = new NotesMigrations();
            var content = File.ReadAllText("help.txt");
            help.LoadText(content);
            help.Show(this);
        }

        /// <summary>
        /// Export the tenant configuration to a file
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The e</param>
        private void ExportTenantConfigurationToFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            // Get Actual Tenant....
            var tenant = this.gJsonGrid1.ActiveTenant;

            // Save values of the Fast Report if exists
            if (tenant.ConfigurationSettingsObject == null)
            {
                tenant.ConfigurationSettingsObject = new AdvantageConfigurationSettings();
            }

            var tenantList = this.gJsonGrid1.TenantList;

            var content = JsonConvert.SerializeObject(tenantList);

            // Stored in a file
            var dialog = new SaveFileDialog
            {
                DefaultExt = ".json",
                Filter = @"txt file|*.txt|json file |*.json",
                Title = @"Select file to store Tenants"
            };
            var res = dialog.ShowDialog(this);
            if (res == DialogResult.OK)
            {
                // Store the info in the file
                File.WriteAllText(dialog.FileName, content);
                MessageBox.Show(this, $@"The Configuration was exported to file {dialog.FileName}", @"File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// New Client Installation Command
        /// </summary>
        /// <param name="sender">
        /// The sender
        /// </param>
        /// <param name="e">The e</param>
        private void NewClientInstalallationToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var newClient = new StartingClient();
                newClient.Show();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(
                    this,
                    exception.Message,
                    @"Error Box",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Set support use id in Advantage
        /// </summary>
        /// <param name="sender"> not used more</param>
        /// <param name="e">not used </param>
        private void SetAdvantageSupportUserToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var tenant = this.gJsonGrid1.ActiveTenant;
                var message = MessageBox.Show(
                    this,
                    $@"Do you want to change the support Id for {tenant.TenantDisplayName}?",
                    @"Confirmation Dialog",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation);

                if (message == DialogResult.Yes)
                {
                    DbInstallation.SetSupportUserInTenant(tenant);
                    MessageBox.Show(
                   this,
                   "Support User Tool run successful",
                   @"OK Box",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Information);
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                Console.WriteLine(ex.Message);
                MessageBox.Show(
                    this,
                    ex.Message,
                    @"Error Box",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Show security Window
        /// <summary>
        /// The main form shown.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MainFormShown(object sender, EventArgs e)
        {
            // Show the password form
            var wpass = new PasswordCheck(this);
            wpass.Show(this);
            this.Enabled = false;
        }
        #endregion

        #region Asynchrony Routines

        /// <summary>
        /// The run all script.
        /// </summary>
        /// <param name="scriptList">
        /// The script list.
        /// </param>
        /// <param name="ver">
        /// The version.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="maxversion">
        /// The maxim version.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private Task RunAllScript(IList<Scripts> scriptList, DbVersion ver, ITenants tenant, string maxversion, TaskScheduler context, CancellationToken token)
        {
            return Task.Run(
                () =>
                    {
                        // Begin loop to install all necessary scripts
                        foreach (Scripts scripts in scriptList)
                        {
                            // Token cancellation requested?
                            if (token.IsCancellationRequested)
                            {
                                break;
                            }

                            // This give the change to use only one program to make the updates.
                            var isupdated = this.IsUpdatedToVersion(scripts.VersionCreated, maxversion);
                            if (isupdated)
                            {
                                continue;
                            }

                            if (scripts.CompareMinor == "Major")
                            {
                                if (ver.Major < scripts.VersionCreated.Major)
                                {
                                    ver = this.ExecuteScript(
                                        tenant,
                                        ver,
                                        scripts.FileName,
                                        scripts.VersionCreated.Description,
                                        context);
                                    ver = DbInstallation.SetCurrentVersion(tenant, scripts.VersionCreated);
                                    continue;
                                }
                            }

                            if (scripts.CompareMinor == "Minor")
                            {
                                if (ver.Major == scripts.VersionCreated.Major
                                    && ver.Minor < scripts.VersionCreated.Minor)
                                {
                                    ver = this.ExecuteScript(
                                        tenant,
                                        ver,
                                        scripts.FileName,
                                        scripts.VersionCreated.Description,
                                        context);
                                    ver = DbInstallation.SetCurrentVersion(tenant, scripts.VersionCreated);
                                    continue;
                                }
                            }

                            if (scripts.CompareMinor == "Build")
                            {
                                if (ver.Major == scripts.VersionCreated.Major
                                    && ver.Minor == scripts.VersionCreated.Minor
                                    && ver.Build < scripts.VersionCreated.Build)
                                {
                                    ver = this.ExecuteScript(
                                        tenant,
                                        ver,
                                        scripts.FileName,
                                        scripts.VersionCreated.Description,
                                        context);
                                    ver = DbInstallation.SetCurrentVersion(tenant, scripts.VersionCreated);
                                    continue;
                                }
                            }

                            if (scripts.CompareMinor == "Revision")
                            {
                                if (ver.Major == scripts.VersionCreated.Major
                                    && ver.Minor == scripts.VersionCreated.Minor
                                    && ver.Build == scripts.VersionCreated.Build
                                    && ver.Revision < scripts.VersionCreated.Revision)
                                {
                                    ver = this.ExecuteScript(
                                        tenant,
                                        ver,
                                        scripts.FileName,
                                        scripts.VersionCreated.Description,
                                        context);
                                    ver = DbInstallation.SetCurrentVersion(tenant, scripts.VersionCreated);
                                }
                            }
                        }
                    },
                token);
        }

        /// <summary>
        /// The button cancel installation click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCancelInstallationClick(object sender, EventArgs e)
        {
            this.tokenCancel.Cancel();
            this.BtnCancelInstallation.Enabled = false;
            this.InitProgressOperation("Cancellation in Progress...", Color.Yellow);
        }

        /// <summary>
        /// The main form_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MainFormFormClosing(object sender, FormClosingEventArgs e)
        {
            // See if they are some process in progress
            if (this.Enabled == false)
            {
                e.Cancel = false;
                return;
            }

            if (this.btnInstallation.Enabled == false)
            {
                // They are a process you should not closed the program
                MessageBox.Show(
                    this,
                    @"They are a operation in progress. If you stop the program your Database can be damaged. Please use cancellation for stop the run script and wait to it before close the program",
                    @"DANGER",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
                e.Cancel = true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// Open form Version
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void SetDbInitialVersionToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                // Open Setting Screen
                var tenant = this.gJsonGrid1.ActiveTenant;
                var form = new FormSetDbVersion(tenant);
                form.ShowDialog(this);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show(
                    this,
                    exception.Message,
                    @"Error Box",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        #endregion

        /// <summary>
        /// Event to get the actual tenant and display in the foot bar
        /// </summary>
        /// <param name="sender">The parameter is not used</param>
        /// <param name="e">not used</param>
        private void GJsonGrid1ActiveTenantName(object sender, GJsonEventArgs e)
        {
            this.tsLabelTenant.Text = e.TenantDisplayName;
        }

        /// <summary>
        /// Button Create new tenant
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnNewTenantClick(object sender, EventArgs e)
        {
            var tenant = Tenants.Factory();
            tenant.IsNew = true;
            tenant.TenantDisplayName = "New Tenant";
            this.gJsonGrid1.InsertNewTenant(tenant);
        }

        /// <summary>
        /// Button delete Tenant
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnDeleteTenantClick(object sender, EventArgs e)
        {
            if (this.gJsonGrid1.ActiveTenant != null && this.gJsonGrid1.ActiveTenant.TenantDisplayName != null)
            {
                this.gJsonGrid1.DeleteSelectedTenant();
            }
            else
            {
                MessageBox.Show(
                    this,
                    @"There is no Tenant to Delete",
                    @"Error Box",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The set advantage paths settings local tool strip menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SetAdvantagePathsSettingsLocalToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var tenant = this.gJsonGrid1.ActiveTenant;
                Cursor.Current = Cursors.WaitCursor;
                
                // Set the sites URI
                AdvantageConfigurationSetting.SetSingleConfigurationSetting(tenant, "AdvantageSiteUri", "http://localhost/advantage/current/site/");
                AdvantageConfigurationSetting.SetSingleConfigurationSetting(tenant, "AdvantageServiceUri", "http://localhost/advantage/current/services/");

                // Set the Authentication API Key
                var apiKey = DbInstallation.GetApiKey(tenant);
                if (apiKey != null)
                {
                    AdvantageConfigurationSetting.SetSingleConfigurationSetting(tenant, "AdvantageServiceAuthenticationKey", apiKey);
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(
                       this,
                       $@"DB {tenant.TenantDisplayName} was configured successfulled ",
                       @"Error Box",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(
                       this,
                       @"Please define a API key in Host Create API key interface",
                       @"Error Box",
                       MessageBoxButtons.OK,
                       MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(
                   this,
                   $@"The following Exception was raised {ex.Message}",
                   @"Error Box",
                   MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The re start local iis server tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ReStartLocalIisServerToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var result = Notification.Question(this, @"This process re start your local IIS. Proceed?");
                if (result == DialogResult.Cancel)
                {
                    return;
                }

                ProcessStartInfo procStartInfo = new ProcessStartInfo()
                {
                    UseShellExecute = true,
                    
                    ////CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "iisreset.exe",
                };

                using (Process proc = new Process())
                {
                    proc.StartInfo = procStartInfo;
                    proc.Start();
                    proc.WaitForExit();

                    if (proc.ExitCode == 0)
                    {
                        Notification.Information(this, "Local IIS restarted");
                    }
                    else
                    {
                        Notification.Error(this, $"A error happen: program return status code {proc.ExitCode}");
                    }
                }
            }
            catch (Exception ex)
            {
               Notification.Error(this, $@"The following Exception was raised {ex.Message}");
            }
        }
    }
}