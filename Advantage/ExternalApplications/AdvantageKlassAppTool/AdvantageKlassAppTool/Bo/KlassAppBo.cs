﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppBo.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the KlassAppBo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageKlassAppTool.Bo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using GLibrary.Business;
    using GLibrary.Facade;
    using GLibrary.GTypes;
    using GLibrary.GTypes.DbTypes;

    public class KlassAppBo
    {
        /// <summary>
        /// The begin wizard 1: Configure The Company Code
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="IList&lt;ExternalCompanies&gt;"/>.
        /// </returns>
        public IList<ExternalCompanies> KlassAppWizard1Verifications(ITenants tenant)
        {
            // Clear table Bridge that can have repeated values
            DbInstallation.RunScript(@"Scripts/ClearBridgeTableExternalCoAllowedServices.sql", tenant);
            
            // Get existing company secret codes
            IList<ExternalCompanies> companiesKeysList = GLibrary.Facade.WapiServicesFacade.GetAllCompaniesCodeAdvantage(tenant);
            return companiesKeysList;
        }

        /// <summary>
        /// The configure company code for KLASSAPP
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public void ConfigureCompanyCodeForKlassApp(ITenants tenant)
        {
            // Get the list of company name in advantage with DEFAULT
            var advantageCompaniesList = WapiServicesFacade.GetAllCompaniesCodeAdvantage(tenant);

            // Get the list of company name in TenantDb with DEFAULT
            IList<ExternalCompanyTenantDb> tenantCompaniesList =
                WapiServicesFacade.GetCodeCompaniesTenants(tenant);

            // They are someone in Advantage
            if (advantageCompaniesList != null && advantageCompaniesList.Any(x => x.Code.ToUpper().Contains("DEFAULT")))
            {
                tenant.WindowsServiceSettingsObject.CompanyCode = advantageCompaniesList
                    .Single(x => x.Code.ToUpper().Contains("DEFAULT")).Code;

                // They are one in Advantage that are in TenantDb
                if (tenantCompaniesList.Any(
                    x => x.ExternalCompanyCode == tenant.WindowsServiceSettingsObject.CompanyCode))
                {
                    var tenantcompany = tenantCompaniesList.Single(
                        x => x.ExternalCompanyCode == tenant.WindowsServiceSettingsObject.CompanyCode);

                    // Get the Tenant Id
                    int tenantId = DbInstallation.GetTenantId(tenant);
                    if (tenantcompany.TenantId == tenantId)
                    {
                        // assign to klassAppOperations (if it is assigned do nothing)
                        WapiServicesFacade.SetCompanyCodeToKlassAppServices(tenant);
                        return;
                    }

                    // Secret Code is in Tenant but it is not assigned to this Advantage 
                    string renamedCompanyCode = this.CreateSecretCompanyCode(tenant);

                    // Rename the code in Advantage
                    WapiServicesFacade.UpdateAdvantageCompanyCode(renamedCompanyCode, tenant);

                    // Insert the new code in Tenant
                    tenant.WindowsServiceSettingsObject.CompanyCode = renamedCompanyCode;

                    // If the old code is in windows service delete it, put the new instead
                    // WindowsServiceConfigurationBo.RefreshConfiguration(tenant, "SecretCompanyKeys", renamedCompanyCode);
                    // WindowsServiceConfigurationBo.SetAppSettingInWindowService(tenant.WindowsServiceSettingsObject.WindowsServicePathToConfiguration, "SecretCompanyKeys",tenant.WindowsServiceSettingsObject.CompanyCode);
                }
            }
            else
            {
                // Created the code
                tenant.WindowsServiceSettingsObject.CompanyCode = this.CreateSecretCompanyCode(tenant);

                // Run the script to Create Company Code in Advantage
                WapiServicesFacade.InsertCompanyCodeInAdvantage(tenant);

                // If the old code is in windows service delete it, put the new instead
                // WindowsServiceConfigurationBo.RefreshConfiguration(tenant, "SecretCompanyKeys", string.Empty);
            }

            // Insert the new code in Tenant
            WapiServicesFacade.InsertCompanyCodeInTenant(tenant);

            // Assign the code to KLASSAPP Services
            WapiServicesFacade.SetCompanyCodeToKlassAppServices(tenant);
        }

        /// <summary>
        /// The configure KLASS-APP allowed operations.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ConfigureKlassAppAllowedOperations(ITenants tenant)
        {
            // configure correctly in the database
            string message = WapiServicesFacade.ConfigureAllowedOperationsPath(tenant);
            return message;
        }

        /// <summary>
        /// The verification allowed services configuration.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <exception cref="ApplicationException">
        /// Throw exception if configuration is bad
        /// </exception>
        public void VerificationAllowedServicesConfiguration(ITenants tenant)
        {
            // Read from configuration the assigned URL and show in control
            ////var basicPath = tenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath;

            // Test if the configuration is correct in the database
            string message = WapiServicesFacade.TestAllowedOperationsPath(tenant);
            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }

            throw new ApplicationException(message);
        }

        /// <summary>
        /// The check all relevant parameters for <code>klass app</code>.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="Tuple"/>.
        /// string return possible error messages
        /// boolean return true if it is OK
        /// </returns>
        /// <exception cref="ApplicationException">
        /// if the <code>WindowsServiceSttingsObject</code> is null
        /// </exception>
        public Tuple<string, bool> CheckAllRevelantParametersForKlassApp(ITenants tenant)
        {
            // Check if the parameters are not empty
            var ka = tenant.WindowsServiceSettingsObject;
            if (ka == null)
            {
                throw new ApplicationException("Internal Error in CheckAllRevelantParametersForKlassApp. WindowsServiceSettingsObject is null");
            }

            string message = string.Empty;

            // Verify input values are OK
            if (string.IsNullOrWhiteSpace(ka.ConsumerKey))
            {
                message += "Consumer key can not be empty";
            }

            if (string.IsNullOrWhiteSpace(ka.PrivateKey))
            {
                message += "Private key can not be empty";
            }

            if (string.IsNullOrWhiteSpace(ka.ScheduleTimeSendStudentSeconds))
            {
                message += "Schedule time key can not be empty";
            }
            else
            {
                if (Convert.ToInt32(ka.ScheduleTimeSendStudentSeconds) < 1800)
                {
                    message += "Schedule Time must be minimal 30 minutes";
                }
            }

            if (string.IsNullOrWhiteSpace(ka.WapiAllowedServicesBasicPath))
            {
                message += "WapiAllowedServicesBasicPath key can not be empty";
            }

            if (string.IsNullOrWhiteSpace(ka.ScheduleTimeSendStudentSeconds))
            {
                message += $" key ScheduleTimeSendStudentSeconds can not be empty{Environment.NewLine}";
            }

            if (string.IsNullOrWhiteSpace(ka.WindowsServiceContextUserName))
            {
                message += $" key WindowsServiceContextUserName can not be empty{Environment.NewLine}";
            }

            if (string.IsNullOrWhiteSpace(ka.WindowsServiceContextPassword))
            {
                message += $" key WindowsServiceContextPassword can not be empty{Environment.NewLine}";
            }

            if (string.IsNullOrWhiteSpace(tenant.AdvantagePhysicalBaseAddress))
            {
                message += $" key AdvantagePhysicalBaseAddress can not be empty{Environment.NewLine}";
            }

            if (string.IsNullOrWhiteSpace(ka.AdvantageProxyUrl))
            {
                message += $" key AdvantageProxyUrl can not be empty{Environment.NewLine}";
            }

            var isOk = message == string.Empty;
            return new Tuple<string, bool>(message, isOk);
        }

        /// <summary>
        /// Wizard External Operations
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="WapiOperationSettingOutputModel"/>.
        /// </returns>
        public Tuple<WapiOperationSettingOutputModel, string> InitialVerificationsExternalOperations(ITenants tenant)
        {
            // Create the tuple
            string message = string.Empty;

            // Get all operations
            IList<WapiOperationSettingOutputModel> oplist = WapiServicesFacade.GetExternalOperations(tenant);

            // Get the KLASSAPP STUDENT operation and show in controls
            var op = oplist.SingleOrDefault(x => x.CodeExtOperationMode == "KLASS_APP_SERVICE");

            if (op == null)
            {
                // Show error
                message += "KLASS-APP operation is not created. Please run the consolidated script"
                           + Environment.NewLine;
            }

            // Verify input values are OK
            if (string.IsNullOrWhiteSpace(tenant.WindowsServiceSettingsObject.ConsumerKey))
            {
                message += "Consumer key can not be empty";
            }

            if (string.IsNullOrWhiteSpace(tenant.WindowsServiceSettingsObject.PrivateKey))
            {
                message += "Private key can not be empty";
            }

            if (string.IsNullOrWhiteSpace(tenant.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds))
            {
                message += "Schedule time key can not be empty";
            }
            else
            {
                if (Convert.ToInt32(tenant.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds) < 1800)
                {
                    message += "Schedule Time must be minimal 30 minutes";
                }
            }

            //   Pre-configure Values
            //// op.DateLastExecution = DateTime.Now.AddDays(-30);

            var output = new Tuple<WapiOperationSettingOutputModel, string>(op, message);
            return output;
        }

        public void ConfigureExternalOperations(ITenants tenant)
        {
            // Update the operations with the new values
            var parameters =
                new Dictionary<string, object>
                    {
                        { "ConsumerKey", tenant.WindowsServiceSettingsObject.ConsumerKey },
                        { "PrivateKey", tenant.WindowsServiceSettingsObject.PrivateKey },
                        { "IsActive", tenant.WindowsServiceSettingsObject.IsActive },
                        {
                            "ScheduledTimeSeconds",
                            tenant.WindowsServiceSettingsObject
                                .ScheduleTimeSendStudentSeconds
                        },
                        { "LastOperationTime", tenant.WindowsServiceSettingsObject.LastExecutionDate },
                        { "CodeOperation", tenant.WindowsServiceSettingsObject.CodeOperation }
                    };

            var query = File.ReadAllText("Scripts/UpdateWapiSettings.sql");
            DbInstallation.RunScriptWithParameter(query, tenant.AdvantageDataBaseConnectionString, parameters);
        }

        /// <summary>
        /// The verify service layer parameters.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool VerifyServiceLayerParameters(ITenants tenant)
        {
            var output = true;
            output &= !string.IsNullOrWhiteSpace(tenant.WindowsServiceSettingsObject.WindowsServiceContextUserName);
            output &= !string.IsNullOrWhiteSpace(tenant.WindowsServiceSettingsObject.WindowsServiceContextPassword);
            return output;
        }

        /// <summary>
        /// The verification service layer configuration.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <exception cref="ApplicationException">
        /// System exception, parameter does not exists
        /// </exception>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool VerificationServiceLayerConfiguration(ITenants tenant)
        {
            if (tenant.WindowsServiceSettingsObject != null)
            {
                return WindowDomainAccess.CheckIfLocalUserExists(
                               tenant.WindowsServiceSettingsObject.WindowsServiceContextUserName);
            }

            throw new ApplicationException(@"System Error: Windows Service Setting can not be null!!!");
        }

        /// <summary>
        /// The create local account for windows service start/stop.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public void CreateLocalAccountForWindowsServiceStartStop(ITenants tenant)
        {
            if (tenant.WindowsServiceSettingsObject != null)
            {
                WindowDomainAccess.CreateNewLocalUser(
                    tenant.WindowsServiceSettingsObject.WindowsServiceContextUserName,
                    tenant.WindowsServiceSettingsObject.WindowsServiceContextPassword);
            }
            else
            {
                throw new ApplicationException(@"System Error: Windows Service Setting can not be null!!!");
            }
        }

        /// <summary>
        /// The create secret company code.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string CreateSecretCompanyCode(ITenants tenant)
        {
            var ticks = DateTime.Now.Ticks.ToString();
            var code = $"FAME_DEFAULT_{tenant.TenantName}_{ticks}";
            code = (code.Length > 50) ? code.Substring(0, 49) : code;
            return code;
        }
    }
}
