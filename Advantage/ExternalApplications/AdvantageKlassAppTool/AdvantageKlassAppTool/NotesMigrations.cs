﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NotesMigrations.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The notes migrations.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageKlassAppTool
{
    using System.IO;
    using System.Windows.Forms;

    /// <summary>
    /// The notes migrations.
    /// </summary>
    public partial class NotesMigrations : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotesMigrations"/> class.
        /// </summary>
        public NotesMigrations()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// The load text.
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        public void LoadText(string content)
        {
            this.tbhelp.Text = content;
        }

        /// <summary>
        /// The exit tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ExitToolStripMenuItemClick(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The button save click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ButtonSaveClick(object sender, System.EventArgs e)
        {
            var content = this.tbhelp.Text; 
            File.WriteAllText("help.txt", content);
            MessageBox.Show(this, @"Text was Saved", @"Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// The button close click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ButtonCloseClick(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}
