﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainForm.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   Defines the MainForm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageKlassAppTool
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;

    using AdvantageKlassAppTool.Bo;

    using GConfiguration;

    using GLibrary;
    using GLibrary.Business;
    using GLibrary.Facade;
    using GLibrary.GTypes;

    using Newtonsoft.Json;

    /// <summary>
    /// The main form.
    /// </summary>
    public partial class MainForm : Form
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();

            // Get application title
            this.ltitle.Text = ConfigurationManager.AppSettings["AppTitle"];

            // Get tool version
            this.tsLabelVersion.Text = $@"Tool Version {Assembly.GetExecutingAssembly().GetName().Version}";
            ////this.tsLabelVersion.Text = this.labelVersion.Text;

            // Load the browser with the image
            this.webBrowser1.Url = new Uri($"file:////{Application.StartupPath}/Pages/FastReport.html");

            this.gJsonConfiguration.ChangeTenant += this.GJsonConfigurationActiveTenantName;
            this.gJsonConfiguration.InitializeJsonGrid((Tenants39)Tenants39.Factory());
            this.gJsonConfiguration.ShowSetting1 = true;
            this.gJsonConfiguration.ShowSetting2 = true;

            // Load the browser with the image
            this.webBrowser2.Url = new Uri($"file:////{Application.StartupPath}/Pages/KlassApp.html");
            this.webBrowser3.Url = new Uri($"file:////{Application.StartupPath}/Pages/Instructions.html");
        }

        #endregion

        #region Configurations

        /// <summary>
        /// The button cancel click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.gJsonConfiguration.CancelTenantChanges();
        }

        /// <summary>
        /// The button save click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                this.gJsonConfiguration.SaveTenantChanges();
            }
            catch (Exception ex)
            {
                var message = Common.CollectAllExceptionsMessages(ex);
                MessageBox.Show(
                    this,
                    $@">> Error in Configuration Format. Please re-edit configuration: {message}",
                    @"Error en Edition",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The import tenant configuration tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ImportTenantConfigurationToolStripMenuItemClick(object sender, EventArgs e)
        {
            // Open Dialog to get configuration
            var open = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = false,
                DefaultExt = "txt",
                SupportMultiDottedExtensions = true,
                Filter = @".text files|*.txt| .json files|*.json"
            };
            if (open.ShowDialog(this) == DialogResult.OK)
            {
                try
                {
                    this.gJsonConfiguration.ImportTenantFromFile(open.FileName);
                    MessageBox.Show(
                        this,
                        @"File Imported!",
                        @"Imported",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(
                        this,
                        $@"Error open the file: {exception.Message}",
                        @"Error Importing File",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Export the tenant configuration to a file
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The e</param>
        private void ExportTenantConfigurationToFileToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                // Get Actual Tenant....
                var tenant = this.gJsonConfiguration.ActiveTenant;

                // Save values of the Fast Report if exists
                if (tenant.ConfigurationSettingsObject == null)
                {
                    tenant.ConfigurationSettingsObject = new AdvantageConfigurationSettings();
                }

                var tenantList = this.gJsonConfiguration.TenantList;

                var content = JsonConvert.SerializeObject(tenantList);

                // Stored in a file
                var dialog = new SaveFileDialog
                {
                    DefaultExt = ".json",
                    Filter = @"txt file|*.txt|json file |*.json",
                    Title = @"Select file to store Tenants"
                };
                var res = dialog.ShowDialog(this);
                if (res == DialogResult.OK)
                {
                    // Store the info in the file
                    File.WriteAllText(dialog.FileName, content);
                    MessageBox.Show(this, $@"The Configuration was exported to file {dialog.FileName}", @"File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    this,
                    $@"Error Exporting file: {ex.Message}",
                    @"Error Exporting File",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The exit tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// The tab control 1 click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TabControl1Click(object sender, EventArgs e)
        {
            this.tabControl2.SelectedIndex = 0;
            this.tbConsole.BackColor = Color.Gainsboro;
            this.tbConsole.Text = @"Welcome to the 3.9 configuration Wizard...";
            this.tbConsole.Text = Environment.NewLine;
        }

        #endregion

        #region Verification Status DB

        /// <summary>
        /// The button verify click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnVerifyClick(object sender, EventArgs e)
        {
            try
            {
                this.CleanInitializeOutputConsole("Initial Verifications...");

                // Get DB Version
                var dbversion = DbInstallation.GetDbVersion(this.gJsonConfiguration.ActiveTenant);
                this.WriteLineConsole($"Actual DB Version: {dbversion.GetStringVersion()}");
                var compatibility = DbInstallation.GetDatabaseLevelOfCompatibility(this.gJsonConfiguration.ActiveTenant);
                this.WriteLineConsole($"DB Compatibility: {compatibility}");

                // Detect if Version is OK (DB >= 3.9.0.0)
                this.VerifyDatabaseVersion(this.gJsonConfiguration.ActiveTenant);
                this.tbConsole.BackColor = Color.LightGreen;
            }
            catch (Exception ex)
            {
                this.tbConsole.BackColor = Color.LightPink;
                this.WriteLineConsole($@"The following Error happen: {ex.Message}");
                this.WriteLineConsole("Verification Fail!");
            }
        }
        #endregion

        #region Fast Report Configuration

        /// <summary>
        /// The button start click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnStartClick(object sender, EventArgs e)
        {
            try
            {
                this.CleanInitializeOutputConsole("Installation Fast Report");
                this.tabControl2.SelectedIndex = 1;

                // Get the tenant
                var tenant = this.gJsonConfiguration.ActiveTenant;

                // Verify database version
                this.VerifyDatabaseVersion(tenant);

                var bo = new FastReportBo();
                var fastreport = bo.GetSettingsFromConfiguration(tenant);

                // Fill the fields with the results
                this.tbUserImpersonator.Text = fastreport.ReportServerDbConnectionString;
                this.tbSsrsWebConexion.Text = fastreport.ReportServerWebUrl;
                this.VerificationFastReport(bo, tenant, fastreport);
                this.WriteLineConsole("Settings Values verified!");
                this.WriteLineConsole(@"Begin Settings Installation...");
                this.BeginWizard0();
                this.tbConsole.BackColor = Color.LightGreen;
                this.WriteLineConsole("Fast Report Settings Installed");
                this.WriteLineConsole(@"You need also to follow the step by step procedure");
                this.WriteLineConsole(@"Please Run Advantage to verify installation");
                this.tbUserImpersonator.Text = tenant.ReportUserImpersonator;
            }
            catch (Exception ex)
            {
                this.tbConsole.BackColor = Color.LightPink;
                this.WriteLineConsole($@"The following Error happen: {ex.Message}");
                this.WriteLineConsole("Installation Fast Report Fail!");
            }
        }

        /// <summary>
        /// The verification fast report.
        /// </summary>
        /// <param name="bo">
        /// The bo.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="fastreport">
        /// The fast report.
        /// </param>
        private void VerificationFastReport(FastReportBo bo, ITenants tenant, FastReportObject fastreport)
        {
            // Verify links
            var message = bo.VerificationApplicationSettingsFastReport(tenant, fastreport);
            if (string.IsNullOrWhiteSpace(message) == false)
            {
                throw new ApplicationException(message);
            }
        }

        /// <summary>
        /// The button previous 0 click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnPrevious0Click(object sender, EventArgs e)
        {
            this.tabControl2.SelectedIndex = 0;
            this.CleanInitializeOutputConsole("Ready to Installations");
        }

        /// <summary>
        /// The button go to initialization TAB 2 click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnGoToInit2Click(object sender, EventArgs e)
        {
            this.tabControl2.SelectedIndex = 0;
            this.CleanInitializeOutputConsole("Ready to Installations");
        }

        /// <summary>
        /// The button open IIS click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnOpenIisClick(object sender, EventArgs e)
        {
            var address = $"{Environment.SystemDirectory}\\inetsrv\\iis.msc";
            try
            {
                var p = new Process
                {
                    StartInfo =
                              {
                                 FileName = address,
                                 CreateNoWindow = false
                              }
                };

                p.Start();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
                this.WriteLineConsole($"Exception Occurred {ex.Message}, {ex.StackTrace}");
            }
        }

        /// <summary>
        /// The button open configuration click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnOpenConfigClick(object sender, EventArgs e)
        {
            // Get the file if it is configured
            var tenant = this.gJsonConfiguration.ActiveTenant;
            var file = $@"{tenant.AdvantagePhysicalBaseAddress}WapiWindowsService\bin\Debug\Configs\AppSettings.config";
            if (File.Exists(file) == false)
            {
                file = $@"{tenant.AdvantagePhysicalBaseAddress}WapiWindowsService\Configs\AppSettings.config";
            }

            try
            {
                var p = new Process { StartInfo = { FileName = "notepad", Arguments = file, CreateNoWindow = false } };
                p.Start();
            }
            catch (IOException io)
            {
                MessageBox.Show(this, $@"Exception Occurred: {io.Message} Path: {file} ", @"ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                this.WriteLineConsole($@"Exception Occurred {ex.Message}, {ex.StackTrace}");
            }
        }

        /// <summary>
        /// The button configuration advantage click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnConfigAdvantageClick(object sender, EventArgs e)
        {
            // Get the file if it is configured
            var tenant = this.gJsonConfiguration.ActiveTenant;
            var file = $"{tenant.AdvantagePhysicalBaseAddress}AdvWeb\\App_Data\\Configs\\system.web.identity.config";
            if (!File.Exists(file))
            {
                file = $"{tenant.AdvantagePhysicalBaseAddress}Site\\App_Data\\Configs\\system.web.identity.config";
            }

            try
            {
                var p = new Process { StartInfo = { FileName = "notepad", Arguments = file, CreateNoWindow = false } };
                p.Start();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
                this.WriteLineConsole($"Exception Occurred {ex.Message}, {ex.StackTrace}");
            }
        }

        /// <summary>
        /// The begin wizard 0. Fast Report Configuration
        /// </summary>
        private void BeginWizard0()
        {
            // Get the tenant
            var tenant = (Tenants39)this.gJsonConfiguration.ActiveTenant;
            var bo = new FastReportBo();
            var fastreport = bo.GetSettingsFromConfiguration(tenant);

            // Begin installation
            bo.SetFastReportConfigurationSetting(tenant, fastreport);
        }

        #endregion

        #region KLASS-APP Configuration Button
        /// <summary>
        /// The button KLASS-APP 1 click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnKlassApp1Click(object sender, EventArgs e)
        {
            try
            {
                var bo = new KlassAppBo();
                var tenant = this.gJsonConfiguration.ActiveTenant;
                this.CleanInitializeOutputConsole("KLASS-APP Installation");

                // Check all parameters are filled
                var messageStatus = bo.CheckAllRevelantParametersForKlassApp(tenant);
                if (messageStatus.Item2 == false)
                {
                    throw new ApplicationException(messageStatus.Item1);
                }

                // Detect if Version is OK (DB >= 3.9.0.0)
                this.VerifyDatabaseVersion(tenant);

                // Configure the Company Code
                this.WriteLineConsole("Begin configuration Company Code for KLASS-APP");
                bo.KlassAppWizard1Verifications(tenant);
                this.WriteLineConsole($"Clean Bridge Table was executed successfully");
                this.WriteLineConsole("Begin Configuration of Companies Code...");
                bo.ConfigureCompanyCodeForKlassApp(tenant);
                this.WriteLineConsole(
                    $@"The Secret Code {
                            tenant.WindowsServiceSettingsObject.CompanyCode
                        } was assigned to KlassApp Operations for {tenant.TenantDisplayName}");
                this.tbCompanyCode.Text = tenant.WindowsServiceSettingsObject.CompanyCode;
                this.tbWapiProxyUrl.Text = tenant.WindowsServiceSettingsObject.AdvantageProxyUrl;
                this.WriteLineConsole("Company Code phase successful");
                this.WriteLineConsole(string.Empty);
                this.WriteLineConsole("Please configure manually the windows service in the last step ");
                this.WriteLineConsole(string.Empty);

                // Begin Phase: Configure WAPI Allowed Services...
                this.WriteLineConsole("Begin configuration: Allowed Services...");
                var messages = bo.ConfigureKlassAppAllowedOperations(tenant);
                this.WriteLineConsole(messages);
                bo.VerificationAllowedServicesConfiguration(tenant);
                this.WriteLineConsole("Allowed Services successfully configured");

                // Begin Phase Configure Windows Service Operations for KlassApp
                this.WriteLineConsole("Begin configuration: WAPI External Operation...");
                var output = bo.InitialVerificationsExternalOperations(tenant);
                if (output.Item2 != string.Empty)
                {
                    throw new ApplicationException(output.Item2);
                }

                this.WriteLineConsole("Initial Verification successful: WAPI External Operation...");

                // Configure Values of the Windows Service operation...
                tenant.WindowsServiceSettingsObject.LastExecutionDate = DateTime.Now.AddDays(-30);
                tenant.WindowsServiceSettingsObject.CodeOperation = "KLASS_APP_STUDENT";
                this.WriteLineConsole("Configuration: WAPI External Operation...");
                bo.ConfigureExternalOperations(tenant);
                this.WriteLineConsole("Configuration WAPI External Operation Successful...");

                // Active all campus to send information to KLASS-APP
                this.WriteLineConsole("Begin Activation of all Campus to be send to KLASS-APP");
                DbInstallation.RunScript("Scripts/ActiveAllCampus.sql", tenant);
                this.WriteLineConsole("Campus Active! If you need only a specific Campus Active. Configure in Advantage configuration settings...");

                // Configure the account to use for start/stop Windows Service
                this.WriteLineConsole("Begin Configuration User to start/stop the windows Service...");

                // Create the account the access to start/stop windows service
                if (bo.VerifyServiceLayerParameters(tenant) == false)
                {
                    var param = tenant.WindowsServiceSettingsObject;
                    throw new ApplicationException($"Any of this parameters is/are empty! {param.WindowsServiceContextUserName}, {param.WindowsServiceContextPassword} ");
                }

                this.WriteLineConsole("Initial Verification successful: start/stop user for WS");

                // configure users
                bo.CreateLocalAccountForWindowsServiceStartStop(tenant);

                // Verify configuration
                if (bo.VerificationServiceLayerConfiguration(tenant) == false)
                {
                    throw new ApplicationException(@"Start/stop user was not configured");
                }

                this.WriteLineConsole("Configuration user to start/stop successful...");
                this.tbConsole.BackColor = Color.LightGreen;
                this.WriteLineConsole($@"Installation finished successfully");
                this.tabControl2.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                this.tbConsole.BackColor = Color.LightPink;
                this.WriteLineConsole($@"The following Error happen: {ex.Message}");
                this.WriteLineConsole("Installation KLASS-APP Fail!");
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// The <code>GJsonConfigurationActiveTenantName</code>.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void GJsonConfigurationActiveTenantName(object sender, GJsonEventArgs e)
        {
            this.tsLabelTenant.Text = e.TenantDisplayName;
        }

        /// <summary>
        /// Sow the password form
        /// </summary>
        /// <param name="sender">not used.</param>
        /// <param name="e">no used</param>
        private void MainFormShown(object sender, EventArgs e)
        {
            // Show the password form
            var wpass = new PasswordCheck(this);
            wpass.Show(this);
            this.Enabled = false;
        }

        #endregion

        #region Console Manipulations

        /// <summary>
        /// Initialize the console and clean it
        /// </summary>
        /// <param name="initText">The text to be show after cleaning the console</param>
        private void CleanInitializeOutputConsole(string initText)
        {
            this.tbConsole.Clear();
            this.tbConsole.BackColor = Color.Gainsboro;
            this.tbConsole.Text = $@"{initText}{Environment.NewLine}";
            this.tbConsole.Refresh();
        }

        /// <summary>
        /// Write a line in <code>tbConsole</code> control followed by a New Line
        /// </summary>
        /// <param name="text">The text to be show</param>
        private void WriteLineConsole(string text)
        {
            this.tbConsole.Text += $@"{text}{Environment.NewLine}";
            this.tbConsole.Refresh();
        }

        /// <summary>
        /// Verify Database Version
        /// </summary>
        /// <param name="tenant">
        /// Active Tenant
        /// </param>
        private void VerifyDatabaseVersion(ITenants tenant)
        {
            if (DbInstallation.ExistsVersionTable(tenant))
            {
                var v = DbInstallation.GetDbVersion(tenant);
                this.WriteLineConsole($"{tenant.TenantDisplayName} version is {v.Major}.{v.Minor}.{v.Revision}.{v.Build}");

                // DB Version must be 3.9 or higher
                if (v.Major <= 3 & v.Minor < 9)
                {
                    // You must update DB before proceed
                    throw new ApplicationException(
                        $"DB is version {v.GetStringVersion()}. Must be 3.9.0.1 or upper to be possible Update. Please update first");
                }
            }
            else
            {
                throw new ApplicationException(
                    $"DB for {tenant.TenantDisplayName} has not Table Version. Please upgrade first");
            }
        }

        #endregion

        #region Menu Strip

        /// <summary>
        /// Open about Form
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var about = new AboutKlassApp();
            about.ShowDialog(this);
        }

        /// <summary>
        /// Show Help form.
        /// </summary>
        /// <param name="sender">
        /// The parameter is not used.
        /// </param>
        /// <param name="e">
        /// The parameter is not used.
        /// </param>
        private void DataBaseVersionsTableToolStripMenuItemClick(object sender, EventArgs e)
        {
            try
            {
                var help = new NotesMigrations();
                var content = File.ReadAllText("help.txt");
                help.LoadText(content);
                help.Show(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $@"Exception Occurred {ex.Message}, {ex.StackTrace}", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Configuration Buttons

        /// <summary>
        /// Create a new Tenant
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnCreateNewTenantClick(object sender, EventArgs e)
        {
            var tenant = Tenants39.Factory();
            tenant.IsNew = true;
            tenant.TenantDisplayName = "New Tenant";
            this.gJsonConfiguration.InsertNewTenant(tenant);
        }

        /// <summary>
        /// Delete the selected tenant
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnDeleteTenantClick(object sender, EventArgs e)
        {
            this.gJsonConfiguration.DeleteSelectedTenant();
        }

        #endregion

        private void giveRightToWindowsServiceUtilityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Activate the security Editor
            var process = new Process();
            process.StartInfo.FileName = @"ServiceSecurityEditor.exe";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            process.Start();
            process.WaitForExit();
        }
    }
}