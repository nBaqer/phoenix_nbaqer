﻿-- Select WAPI External Operations  
SELECT  s.id
       ,s.CodeOperation
       ,s.IdExtCompany
       ,c.Code AS CompanyCode
       ,m.Code AS ExternalOperationMode
       ,s1.Code AS FirstAllowedService
       ,s2.Code AS SecondAllowedService
       ,s.ConsumerKey
       ,s.PrivateKey
       ,s.OperationSecondTimeInterval
       ,s.PollSecondForOnDemandOperation
       ,s.IsActive
       ,s.ExternalUrl
       ,s.DateLastExecution
       ,s.FlagOnDemandOperation
FROM    dbo.syWapiSettings s
JOIN    dbo.syWapiExternalCompanies c ON c.Id = s.IdExtCompany
JOIN    dbo.syWapiAllowedServices s1 ON s1.Id = s.IdAllowedService
JOIN    dbo.syWapiAllowedServices s2 ON s2.Id = s.IdSecondAllowedService
JOIN    dbo.syWapiExternalOperationMode m ON m.Id = s.IdExtOperation
WHERE   CodeOperation = 'KLASS_APP_STUDENT';
