﻿BEGIN TRANSACTION CompaniesCode;
BEGIN TRY
-- Get configuration Values
    DECLARE @CompanySecretId INT = (
                                     SELECT Id
                                     FROM   dbo.syWapiExternalCompanies
                                     WHERE  Code = @CompanySecret
                                   );

    DECLARE @ConfigurationServiceId INT = (
                                            SELECT  Id
                                            FROM    dbo.syWapiAllowedServices
                                            WHERE   Code = 'KLASS_GET_CONFIGURATION_STATUS'
                                          );
    DECLARE @StudentServiceId INT = (
                                      SELECT    Id
                                      FROM      dbo.syWapiAllowedServices
                                      WHERE     Code = 'KLASS_GET_STUDENT_INFORMATION'
                                    );
--  Associate the Company Code with the Klass App Configuration Operation
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syWapiBridgeExternalCompanyAllowedServices
                    WHERE   IdAllowedServices = @ConfigurationServiceId
                            AND IdExternalCompanies = @CompanySecretId )
        BEGIN
            INSERT  INTO dbo.syWapiBridgeExternalCompanyAllowedServices
                    (
                     IdExternalCompanies
                    ,IdAllowedServices
	                )
            VALUES  (
                     @CompanySecretId -- IdExternalCompanies - int
                    ,@ConfigurationServiceId  -- IdAllowedServices - int
	                );
        END;

	-- Associate the Company Code with the Klass App Student Operation
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syWapiBridgeExternalCompanyAllowedServices
                    WHERE   IdAllowedServices = @StudentServiceId
                            AND IdExternalCompanies = @CompanySecretId )
        BEGIN
            INSERT  INTO dbo.syWapiBridgeExternalCompanyAllowedServices
                    (
                     IdExternalCompanies
                    ,IdAllowedServices
	                )
            VALUES  (
                     @CompanySecretId -- IdExternalCompanies - int
                    ,@StudentServiceId  -- IdAllowedServices - int
	                );
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION CompaniesCode;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT  @msg = ERROR_MESSAGE() + ' Line Number:' + CONVERT(NVARCHAR(10),ERROR_LINE())
           ,@severity = ERROR_SEVERITY()
           ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
COMMIT TRANSACTION CompaniesCode
