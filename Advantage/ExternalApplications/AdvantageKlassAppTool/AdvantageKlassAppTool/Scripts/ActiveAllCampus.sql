﻿-- -------------------------------------------------------------------
-- Configure All Campus to be send to Klass-Appp
-- Peevious configuration is deleted.
-- -------------------------------------------------------------------

DECLARE @SettingId INT = (
                         SELECT SettingId
                         FROM   dbo.syConfigAppSettings
                         WHERE  KeyName = 'KlassApp_CampusToConsider'
                         );
IF  (
    SELECT COUNT(*)
    FROM   dbo.syConfigAppSetValues
    WHERE  SettingId = @SettingId
    ) > 1
    BEGIN
        DELETE dbo.syConfigAppSetValues
        WHERE SettingId = @SettingId
              AND CampusId IS NOT NULL;
    END;

UPDATE dbo.syConfigAppSetValues
SET    Value = 'Yes'
WHERE  SettingId = @SettingId;
