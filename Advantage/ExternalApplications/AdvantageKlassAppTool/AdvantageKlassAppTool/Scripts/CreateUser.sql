﻿IF NOT EXISTS (
              SELECT *
              FROM   master.dbo.syslogins
              WHERE  loginname = '@UserName'
              )
    BEGIN
        CREATE LOGIN [@UserName] FROM WINDOWS;
    END;

IF NOT EXISTS (
              SELECT *
              FROM   sys.database_principals
              WHERE  name = '@UserName'
              )
    BEGIN
        CREATE USER [@UserName];
    END;

IF IS_ROLEMEMBER('@Role', '@UserName') = 0
    BEGIN
        EXEC sp_addrolemember '@Role'
                             ,'@UserName';
    END;