﻿    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.syWapiExternalCompanies
                    WHERE   Code = @CompanySecret )
        BEGIN
            INSERT  INTO dbo.syWapiExternalCompanies
                    (
                     Code
                    ,Description
                    ,IsActive
                    )
            VALUES  (
                     @CompanySecret -- Code - varchar(50)
                    ,'Default User for Windows Service'  -- Description - varchar(100)
                    ,1 -- IsActive - bit
                    );
        END;