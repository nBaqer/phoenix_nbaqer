﻿DELETE  FROM f
FROM    syWapiBridgeExternalCompanyAllowedServices AS f
INNER JOIN syWapiBridgeExternalCompanyAllowedServices AS g ON g.IdExternalCompanies = f.IdExternalCompanies
                                                                AND g.IdAllowedServices = f.IdAllowedServices
                                                                AND f.IdWapiBridgeAllowedService < g.IdWapiBridgeAllowedService;
