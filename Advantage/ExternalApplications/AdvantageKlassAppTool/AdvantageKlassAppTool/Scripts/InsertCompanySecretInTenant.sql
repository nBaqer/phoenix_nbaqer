﻿ IF NOT EXISTS ( SELECT  *
                    FROM    dbo.WAPITenantCompanySecret
                    WHERE   ExternalCompanyCode = @CompanySecret )
        BEGIN

            INSERT INTO dbo.WAPITenantCompanySecret
                    ( ExternalCompanyCode,TenantId )
            VALUES  ( @CompanySecret  -- ExternalCompanyCode - varchar(50)
                      ,@TenantId  -- TenantId - int
                      )
        END;