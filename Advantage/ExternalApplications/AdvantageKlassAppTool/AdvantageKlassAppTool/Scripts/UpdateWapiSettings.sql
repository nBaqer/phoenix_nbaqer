﻿UPDATE  dbo.syWapiSettings
SET     ConsumerKey = @ConsumerKey
       ,PrivateKey = @PrivateKey
       ,IsActive = @IsActive
       ,OperationSecondTimeInterval = @ScheduledTimeSeconds
	   ,DateLastExecution = @LastOperationTime
       ,DateMod = GETDATE()
       ,UserMod = 'SUPPORT'
WHERE   CodeOperation = @CodeOperation;

