﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Utils.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The utils.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageKlassAppTool.Utilities
{
    using System.Security.Principal;
    
    /// <summary>
    /// The utils.
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// Return true if the user is administrator.
        /// </summary>
        /// <returns>
        /// True if the user is administrator <see cref="bool"/>.
        /// </returns>
        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
