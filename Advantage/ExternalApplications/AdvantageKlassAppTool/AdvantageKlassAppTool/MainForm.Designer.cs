﻿namespace AdvantageKlassAppTool
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportTenantConfigurationFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importTenantComfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hrlpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBaseVersionsTableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsLabelVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsLabelTenant = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tbWizard = new System.Windows.Forms.TabPage();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tbInit = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.webBrowser3 = new System.Windows.Forms.WebBrowser();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnVerify = new System.Windows.Forms.Button();
            this.btnKlassApp = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.tpFastReport = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbUserImpersonator = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbSsrsWebConexion = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnOpenIis = new System.Windows.Forms.Button();
            this.btnConfigAdvantage = new System.Windows.Forms.Button();
            this.btnPrevious0 = new System.Windows.Forms.Button();
            this.btnKlassApp1 = new System.Windows.Forms.Button();
            this.tbCompanyKey = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.webBrowser2 = new System.Windows.Forms.WebBrowser();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbWapiProxyUrl = new System.Windows.Forms.TextBox();
            this.tbCompanyCode = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnOpenConfig = new System.Windows.Forms.Button();
            this.btnCancelConfigureCompanyCode = new System.Windows.Forms.Button();
            this.btnGoToInit2 = new System.Windows.Forms.Button();
            this.tpConfiguration = new System.Windows.Forms.TabPage();
            this.gJsonConfiguration = new GConfiguration.GJsonGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDeleteTenant = new System.Windows.Forms.Button();
            this.btnCreateNewTenant = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tpMain = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ltitle = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.giveRightToWindowsServiceUtilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tbWizard.SuspendLayout();
            this.gbStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tbInit.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tpFastReport.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tbCompanyKey.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tpConfiguration.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tpMain.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.hrlpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1268, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportConfigurationToolStripMenuItem,
            this.importConfigurationToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exportConfigurationToolStripMenuItem
            // 
            this.exportConfigurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportTenantConfigurationFileToolStripMenuItem});
            this.exportConfigurationToolStripMenuItem.Name = "exportConfigurationToolStripMenuItem";
            this.exportConfigurationToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exportConfigurationToolStripMenuItem.Text = "&Export Configuration";
            // 
            // exportTenantConfigurationFileToolStripMenuItem
            // 
            this.exportTenantConfigurationFileToolStripMenuItem.Name = "exportTenantConfigurationFileToolStripMenuItem";
            this.exportTenantConfigurationFileToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.exportTenantConfigurationFileToolStripMenuItem.Text = "&Export Tenant Configuration File";
            this.exportTenantConfigurationFileToolStripMenuItem.Click += new System.EventHandler(this.ExportTenantConfigurationToFileToolStripMenuItemClick);
            // 
            // importConfigurationToolStripMenuItem
            // 
            this.importConfigurationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importTenantComfigurationToolStripMenuItem});
            this.importConfigurationToolStripMenuItem.Name = "importConfigurationToolStripMenuItem";
            this.importConfigurationToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.importConfigurationToolStripMenuItem.Text = "&Import Configuration";
            // 
            // importTenantComfigurationToolStripMenuItem
            // 
            this.importTenantComfigurationToolStripMenuItem.Name = "importTenantComfigurationToolStripMenuItem";
            this.importTenantComfigurationToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.importTenantComfigurationToolStripMenuItem.Text = "&Import Tenant Configuration";
            this.importTenantComfigurationToolStripMenuItem.Click += new System.EventHandler(this.ImportTenantConfigurationToolStripMenuItemClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(184, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItemClick);
            // 
            // hrlpToolStripMenuItem
            // 
            this.hrlpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataBaseVersionsTableToolStripMenuItem});
            this.hrlpToolStripMenuItem.Name = "hrlpToolStripMenuItem";
            this.hrlpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hrlpToolStripMenuItem.Text = "&Help";
            // 
            // dataBaseVersionsTableToolStripMenuItem
            // 
            this.dataBaseVersionsTableToolStripMenuItem.Name = "dataBaseVersionsTableToolStripMenuItem";
            this.dataBaseVersionsTableToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.dataBaseVersionsTableToolStripMenuItem.Text = "&Data Base Versions Table";
            this.dataBaseVersionsTableToolStripMenuItem.Click += new System.EventHandler(this.DataBaseVersionsTableToolStripMenuItemClick);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabelVersion,
            this.toolStripStatusLabel2,
            this.tsLabelTenant});
            this.statusStrip1.Location = new System.Drawing.Point(0, 689);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1268, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsLabelVersion
            // 
            this.tsLabelVersion.Name = "tsLabelVersion";
            this.tsLabelVersion.Size = new System.Drawing.Size(45, 17);
            this.tsLabelVersion.Text = "Version";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // tsLabelTenant
            // 
            this.tsLabelTenant.Name = "tsLabelTenant";
            this.tsLabelTenant.Size = new System.Drawing.Size(119, 17);
            this.tsLabelTenant.Text = "Tenant Display Name";
            // 
            // tbWizard
            // 
            this.tbWizard.Controls.Add(this.gbStatus);
            this.tbWizard.Controls.Add(this.splitter1);
            this.tbWizard.Controls.Add(this.groupBox1);
            this.tbWizard.Location = new System.Drawing.Point(4, 22);
            this.tbWizard.Name = "tbWizard";
            this.tbWizard.Size = new System.Drawing.Size(1260, 639);
            this.tbWizard.TabIndex = 2;
            this.tbWizard.Text = "Wizard Checking and Configuration";
            this.tbWizard.UseVisualStyleBackColor = true;
            // 
            // gbStatus
            // 
            this.gbStatus.Controls.Add(this.tbConsole);
            this.gbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbStatus.Location = new System.Drawing.Point(695, 0);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Size = new System.Drawing.Size(565, 639);
            this.gbStatus.TabIndex = 2;
            this.gbStatus.TabStop = false;
            this.gbStatus.Text = "Console";
            // 
            // tbConsole
            // 
            this.tbConsole.AcceptsReturn = true;
            this.tbConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbConsole.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbConsole.Location = new System.Drawing.Point(3, 16);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.ReadOnly = true;
            this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbConsole.Size = new System.Drawing.Size(559, 620);
            this.tbConsole.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(685, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 639);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(685, 639);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wizard";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tbInit);
            this.tabControl2.Controls.Add(this.tpFastReport);
            this.tabControl2.Controls.Add(this.tbCompanyKey);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 16);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(679, 620);
            this.tabControl2.TabIndex = 0;
            // 
            // tbInit
            // 
            this.tbInit.Controls.Add(this.groupBox5);
            this.tbInit.Controls.Add(this.panel8);
            this.tbInit.Location = new System.Drawing.Point(4, 22);
            this.tbInit.Name = "tbInit";
            this.tbInit.Size = new System.Drawing.Size(671, 594);
            this.tbInit.TabIndex = 5;
            this.tbInit.Text = "Init";
            this.tbInit.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.webBrowser3);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(0, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(671, 544);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Instructions:";
            // 
            // webBrowser3
            // 
            this.webBrowser3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser3.Location = new System.Drawing.Point(3, 16);
            this.webBrowser3.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser3.Name = "webBrowser3";
            this.webBrowser3.Size = new System.Drawing.Size(665, 525);
            this.webBrowser3.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnVerify);
            this.panel8.Controls.Add(this.btnKlassApp);
            this.panel8.Controls.Add(this.btnStart);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 544);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(671, 50);
            this.panel8.TabIndex = 0;
            // 
            // btnVerify
            // 
            this.btnVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerify.Location = new System.Drawing.Point(215, 17);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(141, 23);
            this.btnVerify.TabIndex = 2;
            this.btnVerify.Text = "Verifiy Version";
            this.btnVerify.UseVisualStyleBackColor = true;
            this.btnVerify.Click += new System.EventHandler(this.BtnVerifyClick);
            // 
            // btnKlassApp
            // 
            this.btnKlassApp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKlassApp.Location = new System.Drawing.Point(362, 17);
            this.btnKlassApp.Name = "btnKlassApp";
            this.btnKlassApp.Size = new System.Drawing.Size(137, 23);
            this.btnKlassApp.TabIndex = 1;
            this.btnKlassApp.Text = "ConfigureKlass App  >>";
            this.btnKlassApp.UseVisualStyleBackColor = true;
            this.btnKlassApp.Click += new System.EventHandler(this.BtnKlassApp1Click);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.Location = new System.Drawing.Point(505, 17);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(154, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Configure Fast Report >>";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.BtnStartClick);
            // 
            // tpFastReport
            // 
            this.tpFastReport.Controls.Add(this.groupBox4);
            this.tpFastReport.Controls.Add(this.groupBox3);
            this.tpFastReport.Controls.Add(this.groupBox6);
            this.tpFastReport.Controls.Add(this.panel10);
            this.tpFastReport.Location = new System.Drawing.Point(4, 22);
            this.tpFastReport.Name = "tpFastReport";
            this.tpFastReport.Size = new System.Drawing.Size(671, 594);
            this.tpFastReport.TabIndex = 7;
            this.tpFastReport.Text = "FastReport";
            this.tpFastReport.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.textBox1);
            this.groupBox4.Controls.Add(this.tbUserImpersonator);
            this.groupBox4.Location = new System.Drawing.Point(333, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(246, 94);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "2 Advantage Impersonator";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Pass:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 23);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "User:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(48, 42);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(180, 20);
            this.textBox1.TabIndex = 11;
            this.textBox1.Text = "s!sfame1234";
            // 
            // tbUserImpersonator
            // 
            this.tbUserImpersonator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbUserImpersonator.Location = new System.Drawing.Point(48, 16);
            this.tbUserImpersonator.Name = "tbUserImpersonator";
            this.tbUserImpersonator.ReadOnly = true;
            this.tbUserImpersonator.Size = new System.Drawing.Size(180, 20);
            this.tbUserImpersonator.TabIndex = 5;
            this.tbUserImpersonator.Text = "INTERNAL\\sisuser";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.tbSsrsWebConexion);
            this.groupBox3.Location = new System.Drawing.Point(13, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(314, 95);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "1 IIS Parameters";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(0, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Request  Path:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(87, 66);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(220, 20);
            this.textBox3.TabIndex = 13;
            this.textBox3.Text = "Reserved-ReportViewerWebControl-axd";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(87, 43);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(220, 20);
            this.textBox2.TabIndex = 12;
            this.textBox2.Text = "Microsoft.Reporting.WebForms.HttpHandler";
            // 
            // tbSsrsWebConexion
            // 
            this.tbSsrsWebConexion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSsrsWebConexion.Location = new System.Drawing.Point(87, 14);
            this.tbSsrsWebConexion.Name = "tbSsrsWebConexion";
            this.tbSsrsWebConexion.ReadOnly = true;
            this.tbSsrsWebConexion.Size = new System.Drawing.Size(208, 20);
            this.tbSsrsWebConexion.TabIndex = 6;
            this.tbSsrsWebConexion.Text = "Reserved.ReportViewerWebControl.axd";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.webBrowser1);
            this.groupBox6.Location = new System.Drawing.Point(13, 104);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(643, 440);
            this.groupBox6.TabIndex = 9;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Aditional Manual Configuration Settings:";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 16);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(637, 421);
            this.webBrowser1.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnOpenIis);
            this.panel10.Controls.Add(this.btnConfigAdvantage);
            this.panel10.Controls.Add(this.btnPrevious0);
            this.panel10.Controls.Add(this.btnKlassApp1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel10.Location = new System.Drawing.Point(0, 547);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(671, 47);
            this.panel10.TabIndex = 3;
            // 
            // btnOpenIis
            // 
            this.btnOpenIis.Location = new System.Drawing.Point(115, 12);
            this.btnOpenIis.Name = "btnOpenIis";
            this.btnOpenIis.Size = new System.Drawing.Size(75, 23);
            this.btnOpenIis.TabIndex = 6;
            this.btnOpenIis.Text = "Open IIS";
            this.btnOpenIis.UseVisualStyleBackColor = true;
            this.btnOpenIis.Click += new System.EventHandler(this.BtnOpenIisClick);
            // 
            // btnConfigAdvantage
            // 
            this.btnConfigAdvantage.Location = new System.Drawing.Point(16, 12);
            this.btnConfigAdvantage.Name = "btnConfigAdvantage";
            this.btnConfigAdvantage.Size = new System.Drawing.Size(81, 23);
            this.btnConfigAdvantage.TabIndex = 5;
            this.btnConfigAdvantage.Text = "Open Config";
            this.btnConfigAdvantage.UseVisualStyleBackColor = true;
            this.btnConfigAdvantage.Click += new System.EventHandler(this.BtnConfigAdvantageClick);
            // 
            // btnPrevious0
            // 
            this.btnPrevious0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrevious0.Location = new System.Drawing.Point(405, 12);
            this.btnPrevious0.Name = "btnPrevious0";
            this.btnPrevious0.Size = new System.Drawing.Size(75, 23);
            this.btnPrevious0.TabIndex = 4;
            this.btnPrevious0.Text = "Close";
            this.btnPrevious0.UseVisualStyleBackColor = true;
            this.btnPrevious0.Click += new System.EventHandler(this.BtnPrevious0Click);
            // 
            // btnKlassApp1
            // 
            this.btnKlassApp1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKlassApp1.Location = new System.Drawing.Point(495, 12);
            this.btnKlassApp1.Name = "btnKlassApp1";
            this.btnKlassApp1.Size = new System.Drawing.Size(161, 23);
            this.btnKlassApp1.TabIndex = 1;
            this.btnKlassApp1.Text = "KLASS APP Installation  >>";
            this.btnKlassApp1.UseVisualStyleBackColor = true;
            this.btnKlassApp1.Click += new System.EventHandler(this.BtnKlassApp1Click);
            // 
            // tbCompanyKey
            // 
            this.tbCompanyKey.Controls.Add(this.groupBox2);
            this.tbCompanyKey.Controls.Add(this.panel3);
            this.tbCompanyKey.Controls.Add(this.panel7);
            this.tbCompanyKey.Location = new System.Drawing.Point(4, 22);
            this.tbCompanyKey.Name = "tbCompanyKey";
            this.tbCompanyKey.Padding = new System.Windows.Forms.Padding(3);
            this.tbCompanyKey.Size = new System.Drawing.Size(671, 594);
            this.tbCompanyKey.TabIndex = 1;
            this.tbCompanyKey.Text = "KLASS-APP";
            this.tbCompanyKey.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.webBrowser2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(665, 452);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Additional Manual configuration:";
            // 
            // webBrowser2
            // 
            this.webBrowser2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser2.Location = new System.Drawing.Point(3, 16);
            this.webBrowser2.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser2.Name = "webBrowser2";
            this.webBrowser2.Size = new System.Drawing.Size(659, 433);
            this.webBrowser2.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.tbWapiProxyUrl);
            this.panel3.Controls.Add(this.tbCompanyCode);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(665, 105);
            this.panel3.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "WAPI Proxy URL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Company Key:";
            // 
            // tbWapiProxyUrl
            // 
            this.tbWapiProxyUrl.Location = new System.Drawing.Point(13, 72);
            this.tbWapiProxyUrl.Name = "tbWapiProxyUrl";
            this.tbWapiProxyUrl.ReadOnly = true;
            this.tbWapiProxyUrl.Size = new System.Drawing.Size(511, 20);
            this.tbWapiProxyUrl.TabIndex = 1;
            // 
            // tbCompanyCode
            // 
            this.tbCompanyCode.Location = new System.Drawing.Point(13, 27);
            this.tbCompanyCode.Name = "tbCompanyCode";
            this.tbCompanyCode.ReadOnly = true;
            this.tbCompanyCode.Size = new System.Drawing.Size(511, 20);
            this.tbCompanyCode.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnOpenConfig);
            this.panel7.Controls.Add(this.btnCancelConfigureCompanyCode);
            this.panel7.Controls.Add(this.btnGoToInit2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(3, 560);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(665, 31);
            this.panel7.TabIndex = 2;
            // 
            // btnOpenConfig
            // 
            this.btnOpenConfig.Location = new System.Drawing.Point(95, 4);
            this.btnOpenConfig.Name = "btnOpenConfig";
            this.btnOpenConfig.Size = new System.Drawing.Size(81, 23);
            this.btnOpenConfig.TabIndex = 3;
            this.btnOpenConfig.Text = "Open Config";
            this.btnOpenConfig.UseVisualStyleBackColor = true;
            this.btnOpenConfig.Click += new System.EventHandler(this.BtnOpenConfigClick);
            // 
            // btnCancelConfigureCompanyCode
            // 
            this.btnCancelConfigureCompanyCode.Enabled = false;
            this.btnCancelConfigureCompanyCode.Location = new System.Drawing.Point(13, 3);
            this.btnCancelConfigureCompanyCode.Name = "btnCancelConfigureCompanyCode";
            this.btnCancelConfigureCompanyCode.Size = new System.Drawing.Size(75, 23);
            this.btnCancelConfigureCompanyCode.TabIndex = 2;
            this.btnCancelConfigureCompanyCode.Text = "Test";
            this.btnCancelConfigureCompanyCode.UseVisualStyleBackColor = true;
            this.btnCancelConfigureCompanyCode.Visible = false;
            // 
            // btnGoToInit2
            // 
            this.btnGoToInit2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoToInit2.Location = new System.Drawing.Point(587, 3);
            this.btnGoToInit2.Name = "btnGoToInit2";
            this.btnGoToInit2.Size = new System.Drawing.Size(75, 23);
            this.btnGoToInit2.TabIndex = 1;
            this.btnGoToInit2.Text = "Close";
            this.btnGoToInit2.UseVisualStyleBackColor = true;
            this.btnGoToInit2.Click += new System.EventHandler(this.BtnGoToInit2Click);
            // 
            // tpConfiguration
            // 
            this.tpConfiguration.BackColor = System.Drawing.Color.White;
            this.tpConfiguration.Controls.Add(this.gJsonConfiguration);
            this.tpConfiguration.Controls.Add(this.panel5);
            this.tpConfiguration.Controls.Add(this.panel4);
            this.tpConfiguration.Controls.Add(this.panel2);
            this.tpConfiguration.Location = new System.Drawing.Point(4, 22);
            this.tpConfiguration.Name = "tpConfiguration";
            this.tpConfiguration.Padding = new System.Windows.Forms.Padding(3);
            this.tpConfiguration.Size = new System.Drawing.Size(1260, 639);
            this.tpConfiguration.TabIndex = 1;
            this.tpConfiguration.Text = "Configuration";
            // 
            // gJsonConfiguration
            // 
            this.gJsonConfiguration.BackColor = System.Drawing.Color.White;
            this.gJsonConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gJsonConfiguration.Location = new System.Drawing.Point(13, 41);
            this.gJsonConfiguration.Name = "gJsonConfiguration";
            this.gJsonConfiguration.ShowSetting1 = false;
            this.gJsonConfiguration.ShowSetting2 = false;
            this.gJsonConfiguration.Size = new System.Drawing.Size(1244, 555);
            this.gJsonConfiguration.TabIndex = 5;
            this.gJsonConfiguration.TenantList = null;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnDeleteTenant);
            this.panel5.Controls.Add(this.btnCreateNewTenant);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Controls.Add(this.btnCancel);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(13, 596);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1244, 40);
            this.panel5.TabIndex = 4;
            // 
            // btnDeleteTenant
            // 
            this.btnDeleteTenant.Location = new System.Drawing.Point(346, 8);
            this.btnDeleteTenant.Name = "btnDeleteTenant";
            this.btnDeleteTenant.Size = new System.Drawing.Size(116, 23);
            this.btnDeleteTenant.TabIndex = 8;
            this.btnDeleteTenant.Text = "Delete Tenant";
            this.btnDeleteTenant.UseVisualStyleBackColor = true;
            this.btnDeleteTenant.Click += new System.EventHandler(this.BtnDeleteTenantClick);
            // 
            // btnCreateNewTenant
            // 
            this.btnCreateNewTenant.Location = new System.Drawing.Point(219, 8);
            this.btnCreateNewTenant.Name = "btnCreateNewTenant";
            this.btnCreateNewTenant.Size = new System.Drawing.Size(121, 23);
            this.btnCreateNewTenant.TabIndex = 7;
            this.btnCreateNewTenant.Text = "Create New Tenant";
            this.btnCreateNewTenant.UseVisualStyleBackColor = true;
            this.btnCreateNewTenant.Click += new System.EventHandler(this.BtnCreateNewTenantClick);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(7, 6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 25);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSaveClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(113, 6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 25);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancelClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label3);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(13, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1244, 38);
            this.panel4.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Configuration Parameters:";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 633);
            this.panel2.TabIndex = 1;
            // 
            // tpMain
            // 
            this.tpMain.Controls.Add(this.panel6);
            this.tpMain.Controls.Add(this.panel1);
            this.tpMain.Controls.Add(this.label1);
            this.tpMain.Location = new System.Drawing.Point(4, 22);
            this.tpMain.Name = "tpMain";
            this.tpMain.Padding = new System.Windows.Forms.Padding(3);
            this.tpMain.Size = new System.Drawing.Size(1260, 639);
            this.tpMain.TabIndex = 0;
            this.tpMain.Text = "Main";
            this.tpMain.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 47);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1254, 589);
            this.panel6.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::AdvantageKlassAppTool.Properties.Resources.installer391;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1254, 589);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ltitle);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1254, 44);
            this.panel1.TabIndex = 6;
            // 
            // ltitle
            // 
            this.ltitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ltitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ltitle.Location = new System.Drawing.Point(140, 0);
            this.ltitle.Name = "ltitle";
            this.ltitle.Size = new System.Drawing.Size(1114, 44);
            this.ltitle.TabIndex = 5;
            this.ltitle.Text = "label4";
            this.ltitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = global::AdvantageKlassAppTool.Properties.Resources.FAME_Logo_DarkBlue_Small_TR;
            this.pictureBox2.InitialImage = global::AdvantageKlassAppTool.Properties.Resources.FAME_Logo_DarkBlue_Small_TR;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(140, 44);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(36, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(906, 28);
            this.label1.TabIndex = 4;
            this.label1.Text = "Advantage DB Migration Batch Tool";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpMain);
            this.tabControl1.Controls.Add(this.tpConfiguration);
            this.tabControl1.Controls.Add(this.tbWizard);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1268, 665);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.Click += new System.EventHandler(this.TabControl1Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.giveRightToWindowsServiceUtilityToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // giveRightToWindowsServiceUtilityToolStripMenuItem
            // 
            this.giveRightToWindowsServiceUtilityToolStripMenuItem.Name = "giveRightToWindowsServiceUtilityToolStripMenuItem";
            this.giveRightToWindowsServiceUtilityToolStripMenuItem.Size = new System.Drawing.Size(265, 22);
            this.giveRightToWindowsServiceUtilityToolStripMenuItem.Text = "&Give right to Windows Service Utility";
            this.giveRightToWindowsServiceUtilityToolStripMenuItem.Click += new System.EventHandler(this.giveRightToWindowsServiceUtilityToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1268, 711);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Advantage Klass App & Fast Report Configuration Tool ";
            this.Shown += new System.EventHandler(this.MainFormShown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tbWizard.ResumeLayout(false);
            this.gbStatus.ResumeLayout(false);
            this.gbStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tbInit.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.tpFastReport.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.tbCompanyKey.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.tpConfiguration.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tpMain.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportConfigurationToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem exportTenantConfigurationFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importTenantComfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel tsLabelVersion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tsLabelTenant;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabPage tbWizard;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.TextBox tbConsole;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tpConfiguration;
        private GConfiguration.GJsonGrid gJsonConfiguration;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabPage tpMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tbInit;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btnKlassApp;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabPage tpFastReport;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnPrevious0;
        private System.Windows.Forms.Button btnKlassApp1;
        private System.Windows.Forms.TabPage tbCompanyKey;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnCancelConfigureCompanyCode;
        private System.Windows.Forms.Button btnGoToInit2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.WebBrowser webBrowser2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbWapiProxyUrl;
        private System.Windows.Forms.TextBox tbCompanyCode;
        private System.Windows.Forms.Button btnOpenConfig;
        private System.Windows.Forms.Button btnConfigAdvantage;
        private System.Windows.Forms.Button btnOpenIis;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbUserImpersonator;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox tbSsrsWebConexion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem hrlpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataBaseVersionsTableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Button btnCreateNewTenant;
        private System.Windows.Forms.Button btnDeleteTenant;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.WebBrowser webBrowser3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label ltitle;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem giveRightToWindowsServiceUtilityToolStripMenuItem;
    }
}

