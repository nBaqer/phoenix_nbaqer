﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tester
{
    using GLibrary.Business;

    [TestClass]
    public class WriteConfigFileTester
    {
        [TestMethod]
        public void ChangeAppConfigValue()
        {
            var value = "FAME_TEST_TOKEN"; //FAME_DEFAULT_HMC_3.9_636335777011372451
            var key = "SecretCompanyKeys";
            var filename = "C:\\_tfs\\Advantage\\Development\\Dev\\Source\\WapiWindowsService\\Configs\\AppSettings.config";
            WindowsServiceConfigurationBo.SetAppSettingInWindowService(filename,key,value);
            
        }
    }
}
