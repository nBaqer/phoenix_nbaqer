﻿namespace GConfiguration
{
    partial class GJsonGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTenant = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbSetting2 = new System.Windows.Forms.GroupBox();
            this.gbSetting3 = new System.Windows.Forms.GroupBox();
            this.dgvSetting3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dgvSettings = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gbBasicStting = new System.Windows.Forms.GroupBox();
            this.dgvConfiguration = new System.Windows.Forms.DataGridView();
            this.Caption = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbSetting2.SuspendLayout();
            this.gbSetting3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetting3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettings)).BeginInit();
            this.gbBasicStting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguration)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbTenant);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(644, 50);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Tenant:";
            // 
            // cbTenant
            // 
            this.cbTenant.BackColor = System.Drawing.Color.MistyRose;
            this.cbTenant.DisplayMember = "TenantDisplayName";
            this.cbTenant.FormattingEnabled = true;
            this.cbTenant.Location = new System.Drawing.Point(3, 18);
            this.cbTenant.Name = "cbTenant";
            this.cbTenant.Size = new System.Drawing.Size(357, 21);
            this.cbTenant.TabIndex = 0;
            this.cbTenant.SelectedIndexChanged += new System.EventHandler(this.CbTenantSelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbSetting2);
            this.panel2.Controls.Add(this.gbBasicStting);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(644, 413);
            this.panel2.TabIndex = 1;
            // 
            // gbSetting2
            // 
            this.gbSetting2.Controls.Add(this.gbSetting3);
            this.gbSetting2.Controls.Add(this.dgvSettings);
            this.gbSetting2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSetting2.Location = new System.Drawing.Point(0, 156);
            this.gbSetting2.Name = "gbSetting2";
            this.gbSetting2.Size = new System.Drawing.Size(644, 257);
            this.gbSetting2.TabIndex = 1;
            this.gbSetting2.TabStop = false;
            this.gbSetting2.Text = "Fast Report Settings";
            // 
            // gbSetting3
            // 
            this.gbSetting3.Controls.Add(this.dgvSetting3);
            this.gbSetting3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSetting3.Location = new System.Drawing.Point(3, 108);
            this.gbSetting3.Name = "gbSetting3";
            this.gbSetting3.Size = new System.Drawing.Size(638, 146);
            this.gbSetting3.TabIndex = 2;
            this.gbSetting3.TabStop = false;
            this.gbSetting3.Text = "KlassApp";
            // 
            // dgvSetting3
            // 
            this.dgvSetting3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSetting3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.dgvSetting3.ContextMenuStrip = this.contextMenuStrip2;
            this.dgvSetting3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetting3.Location = new System.Drawing.Point(3, 16);
            this.dgvSetting3.Name = "dgvSetting3";
            this.dgvSetting3.Size = new System.Drawing.Size(632, 127);
            this.dgvSetting3.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Caption";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewTextBoxColumn3.HeaderText = "Caption";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 68;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn4.HeaderText = "Value";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // dgvSettings
            // 
            this.dgvSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvSettings.ContextMenuStrip = this.contextMenuStrip1;
            this.dgvSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvSettings.Location = new System.Drawing.Point(3, 16);
            this.dgvSettings.Name = "dgvSettings";
            this.dgvSettings.Size = new System.Drawing.Size(638, 92);
            this.dgvSettings.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Caption";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn1.HeaderText = "Caption";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 68;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Value";
            this.dataGridViewTextBoxColumn2.HeaderText = "Value";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // gbBasicStting
            // 
            this.gbBasicStting.Controls.Add(this.dgvConfiguration);
            this.gbBasicStting.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbBasicStting.Location = new System.Drawing.Point(0, 0);
            this.gbBasicStting.Name = "gbBasicStting";
            this.gbBasicStting.Size = new System.Drawing.Size(644, 156);
            this.gbBasicStting.TabIndex = 0;
            this.gbBasicStting.TabStop = false;
            this.gbBasicStting.Text = "Basic Configuration:";
            // 
            // dgvConfiguration
            // 
            this.dgvConfiguration.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvConfiguration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConfiguration.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Caption,
            this.Value});
            this.dgvConfiguration.ContextMenuStrip = this.contextMenuStripMain;
            this.dgvConfiguration.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvConfiguration.Location = new System.Drawing.Point(3, 16);
            this.dgvConfiguration.Name = "dgvConfiguration";
            this.dgvConfiguration.Size = new System.Drawing.Size(638, 137);
            this.dgvConfiguration.TabIndex = 0;
            // 
            // Caption
            // 
            this.Caption.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Caption.DataPropertyName = "Caption";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Caption.DefaultCellStyle = dataGridViewCellStyle3;
            this.Caption.HeaderText = "Caption";
            this.Caption.Name = "Caption";
            this.Caption.ReadOnly = true;
            this.Caption.Width = 68;
            // 
            // Value
            // 
            this.Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Value.DataPropertyName = "Value";
            this.Value.HeaderText = "Value";
            this.Value.Name = "Value";
            // 
            // contextMenuStripMain
            // 
            this.contextMenuStripMain.Name = "contextMenuStripMain";
            this.contextMenuStripMain.Size = new System.Drawing.Size(61, 4);
            // 
            // GJsonGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "GJsonGrid";
            this.Size = new System.Drawing.Size(644, 463);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbSetting2.ResumeLayout(false);
            this.gbSetting3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetting3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSettings)).EndInit();
            this.gbBasicStting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConfiguration)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cbTenant;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbSetting2;
        private System.Windows.Forms.DataGridView dgvSettings;
        private System.Windows.Forms.GroupBox gbBasicStting;
        private System.Windows.Forms.DataGridView dgvConfiguration;
        private System.Windows.Forms.DataGridViewTextBoxColumn Caption;
        private System.Windows.Forms.DataGridViewTextBoxColumn Value;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbSetting3;
        private System.Windows.Forms.DataGridView dgvSetting3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMain;
    }
}
