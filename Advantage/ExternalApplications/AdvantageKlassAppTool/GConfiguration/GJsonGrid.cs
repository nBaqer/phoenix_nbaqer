﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GJsonGrid.cs" company="FAME">
//  2017 
// </copyright>
// <summary>
//   Defines the GJsonGrid type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GConfiguration
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Linq;
    using System.Windows.Forms;

    using GLibrary.GTypes;

    using Newtonsoft.Json;

    /// <summary>
    /// The JSON grid custom control.
    /// </summary>
    public partial class GJsonGrid : UserControl
    {
        /// <summary>
        /// The tenant initial.
        /// </summary>
        private ITenants tenantInitial;

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GJsonGrid"/> class.
        /// </summary>
        public GJsonGrid()
        {
            this.InitializeComponent();
            this.dgvConfiguration.EnableHeadersVisualStyles = false;
        }
        #endregion

        #region Event Delegates
        /// <summary>
        /// The change tenant event handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        public delegate void ChangeTenantEventHandler(object sender, GJsonEventArgs e);

        /// <summary>
        /// The alarm.
        /// </summary>
        public event ChangeTenantEventHandler ChangeTenant;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the active tenant.
        /// </summary>
        public ITenants ActiveTenant
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the tenant list.
        /// that is the content of the grid
        /// </summary>
        public IList<ITenants> TenantList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show setting 1.
        /// </summary>
        public bool ShowSetting1
        {
            get => this.gbSetting2.Visible;

            set => this.gbSetting2.Visible = value;
        }

        /// <summary>
        /// Gets or sets a value indicating whether show setting 2.
        /// </summary>
        public bool ShowSetting2
        {
            get => this.gbSetting3.Visible;

            set => this.gbSetting3.Visible = value;
        }

        #endregion

        #region Initialization
        /// <summary>
        /// The initialize JSON grid.
        /// Mandatory before use the component
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public void InitializeJsonGrid(ITenants tenant)
        {
            this.tenantInitial = tenant;
            this.dgvConfiguration.AutoGenerateColumns = false;
            this.dgvSettings.AutoGenerateColumns = false;

            // Create a empty new Grid element
            this.TenantList = new List<ITenants>();
            var content = this.ReadIsolatedStorage();
            if (string.IsNullOrWhiteSpace(content))
            {
                this.TenantList.Add(tenant);
            }
            else
            {
                this.DeserializeJson(tenant, content);
            }

            this.SetTenantListToControls();
            this.SetTenantToContextMenu(this.contextMenuStripMain, this.MenuitemClickHandlerMain);
            this.SetTenantToContextMenu(this.contextMenuStrip1, this.MenuitemClickHandler);
            this.SetTenantToContextMenu(this.contextMenuStrip2, this.MenuitemClickHandler2);

            this.cbTenant.Tag = -1;
        }

        #endregion

        #region Manipulate Tenant (Create delete Save Import Export)

        /// <summary>
        /// The create new tenant.
        /// </summary>
        /// <param name="tenant">
        /// A empty Tenant
        /// </param>
        public void InsertNewTenant(ITenants tenant)
        {
            this.SaveTenantChanges();
            this.TenantList.Add(tenant);
            this.SetTenantListToControls();
            this.cbTenant.SelectedItem = tenant;
            this.ActiveTenant = tenant;
        }

        /// <summary>
        /// The delete selected tenant.
        /// </summary>
        public void DeleteSelectedTenant()
        {
            if (this.TenantList.Count > 1)
            {
                this.TenantList.Remove(this.ActiveTenant);
                this.ActiveTenant = this.TenantList[0];
                this.SetTenantListToControls(this.ActiveTenant);
                this.cbTenant.Text = this.ActiveTenant.TenantDisplayName;
                return;
            }

            MessageBox.Show(this, @"You can not delete the last Tenant!");
        }

        /// <summary>
        /// The save tenant changes.
        /// The save is only in memory
        /// </summary>
        public void SaveTenantChanges()
        {
            this.UpdateFromControls();
            var content = JsonConvert.SerializeObject(this.TenantList, Formatting.None);
            this.SaveChangesToIsolatedStorage(content);

            // Read from isolate storage
            this.SetTenantListToControls(this.ActiveTenant);
        }

        /// <summary>
        /// The cancel tenant changes.
        /// The grid is repopulated with the original object
        /// </summary>
        public void CancelTenantChanges()
        {
            string content = this.ReadIsolatedStorage();
            this.DeserializeJson(this.tenantInitial, content);
            ////this.TenantList = (IList<ITenants>)JsonConvert.DeserializeObject(content);
            this.SetTenantListToControls();
        }

        /// <summary>
        /// The export tenant list to file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        public void ExportTenantListToFile(string file)
        {
            ////this.UpdateTenantChanges(this.ActiveTenant);
            var content = JsonConvert.SerializeObject(this.TenantList, Formatting.None);
            File.WriteAllText(file, content);
        }

        /// <summary>
        /// The set active tenant.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public void SetActiveTenant(ITenants tenant)
        {
            this.ActiveTenant = tenant;
            this.SetTenantListToControls(tenant);
        }

        /// <summary>
        /// The import tenant from file.
        /// </summary>
        /// <param name="file">
        /// The file.
        /// </param>
        public void ImportTenantFromFile(string file)
        {
            if (File.Exists(file))
            {
                // Read file serialize and put it in the grid
                var jsonText = File.ReadAllText(file);
                this.DeserializeJson(this.tenantInitial, jsonText);

                // Clear the input
                foreach (ITenants ten in this.TenantList)
                {
                    ten.ConfigurationSettingsObject = ten.ConfigurationSettingsObject
                                                      ?? new AdvantageConfigurationSettings();
                    ten.WindowsServiceSettingsObject = ten.WindowsServiceSettingsObject
                                                       ?? new AdvantageWindowsServiceSettings();
                }

                // Fill the combo box
                this.SetTenantListToControls();
            }
        }

        #endregion

        #region Events Handlers
        /// <summary>
        /// The on change tenant.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected virtual void OnChangeTenant(GJsonEventArgs e)
        {
            this.ChangeTenant?.Invoke(this, e);
        }

        /// <summary>
        /// The combo box tenant selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CbTenantSelectedIndexChanged(object sender, EventArgs e)
        {
            // First update changes
            this.ActiveTenant = (ITenants)((ComboBox)sender).SelectedItem;
            if (this.TenantList.Count > 0 & this.ActiveTenant != null)
            {
                this.SetValuesToConfigurationControl();

                // Raise Event that the tenant selected changed
                GJsonEventArgs arg = new GJsonEventArgs(this.ActiveTenant.TenantDisplayName);
                this.OnChangeTenant(arg);
            }

            ((ComboBox)sender).Tag = ((ComboBox)sender).SelectedIndex;
        }

        #endregion

        #region Context Menu 

        /// <summary>
        /// The set tenant to context menu.
        /// </summary>
        /// <param name="menuStrip">
        /// The menu strip.
        /// </param>
        /// <param name="handler">
        /// The handler.
        /// </param>
        private void SetTenantToContextMenu(ContextMenuStrip menuStrip, EventHandler handler)
        {
            if (this.TenantList != null && this.TenantList.Count > 0)
            {
                var item = new ToolStripMenuItem("Clone From");
                foreach (ITenants tenants in this.TenantList)
                {
                    item.DropDownItems.Add(new ToolStripMenuItem(tenants.TenantDisplayName, null, handler));
                }

                menuStrip.Items.Add(item);
            }
        }

        /// <summary>
        /// The menu item click handler main.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MenuitemClickHandlerMain(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            this.UpdateFirstGridFromControl();
            var tenant = this.TenantList.Single(x => x.TenantDisplayName == item.Text);
            this.TenantList.Remove(this.ActiveTenant);

            this.ActiveTenant.TenantDataBaseConnectionString = tenant.TenantDataBaseConnectionString;
            this.ActiveTenant.AdvantageDataBaseConnectionString = tenant.AdvantageDataBaseConnectionString;
            this.ActiveTenant.AdvantagePhysicalBaseAddress = tenant.AdvantagePhysicalBaseAddress;

            this.TenantList.Add(this.ActiveTenant);
            this.SetTenantListToControls(this.ActiveTenant);
        }

        /// <summary>
        /// The menu item click handler for Settings1
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MenuitemClickHandler(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            this.UpdateFirstGridFromControl();
            var tenant = this.TenantList.Single(x => x.TenantDisplayName == item.Text);
            this.TenantList.Remove(this.ActiveTenant);
            this.ActiveTenant.ConfigurationSettingsObject.ReportServerConexion =
                tenant.ConfigurationSettingsObject.ReportServerConexion;
            this.ActiveTenant.ConfigurationSettingsObject.ReportServerInstance =
                tenant.ConfigurationSettingsObject.ReportServerInstance;
            this.ActiveTenant.ReportUserImpersonator = tenant.ReportUserImpersonator;
            this.TenantList.Add(this.ActiveTenant);
            this.SetTenantListToControls(this.ActiveTenant);
        }

        /// <summary>
        /// The menu item click handler 2.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MenuitemClickHandler2(object sender, EventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;
            this.UpdateFirstGridFromControl();
            var tenant = this.TenantList.Single(x => x.TenantDisplayName == item.Text);
            this.TenantList.Remove(this.ActiveTenant);
            this.ActiveTenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath =
                tenant.WindowsServiceSettingsObject.WapiAllowedServicesBasicPath;

            this.ActiveTenant.WindowsServiceSettingsObject.ConsumerKey =
                tenant.WindowsServiceSettingsObject.ConsumerKey;

            this.ActiveTenant.WindowsServiceSettingsObject.PrivateKey = tenant.WindowsServiceSettingsObject.PrivateKey;
            this.ActiveTenant.WindowsServiceSettingsObject.IsActive = tenant.WindowsServiceSettingsObject.IsActive;
            this.ActiveTenant.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds = tenant.WindowsServiceSettingsObject.ScheduleTimeSendStudentSeconds;
            this.ActiveTenant.WindowsServiceSettingsObject.WindowsServiceContextUserName = tenant.WindowsServiceSettingsObject.WindowsServiceContextUserName;
            this.ActiveTenant.WindowsServiceSettingsObject.WindowsServiceContextPassword = tenant.WindowsServiceSettingsObject.WindowsServiceContextPassword;
            this.ActiveTenant.WindowsServiceSettingsObject.AdvantageProxyUrl = tenant.WindowsServiceSettingsObject.AdvantageProxyUrl;

            this.TenantList.Add(this.ActiveTenant);
            this.SetTenantListToControls(this.ActiveTenant);
        }
        #endregion

        #region Manage Input Output Control to Configuration

        /// <summary>
        /// The set tenant list to controls.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        private void SetTenantListToControls(ITenants tenant = null)
        {
            this.cbTenant.DataSource = null;
            this.cbTenant.DataSource = this.TenantList;
            this.cbTenant.DisplayMember = "TenantDisplayName";
            this.cbTenant.Refresh();
            if (tenant == null)
            {
                this.cbTenant.SelectedIndex = 0;
            }
            else
            {
                this.cbTenant.SelectedItem = tenant;
            }

            // Assign to the grid the selected value
            this.ActiveTenant = (ITenants)this.cbTenant.SelectedItem;

            this.SetValuesToConfigurationControl();
        }

        /// <summary>
        /// Save Active Tenant values to configuration control.
        /// </summary>
        private void SetValuesToConfigurationControl()
        {
            this.dgvConfiguration.DataSource = null;
            this.dgvSettings.DataSource = null;
            this.dgvSetting3.DataSource = null;

            var properties = this.ActiveTenant.ConvertToProperties();
            this.dgvConfiguration.DataSource = properties;
            var settings = this.ActiveTenant.ConvertToSettings();
            this.dgvSettings.DataSource = settings;
            var setting2 = this.ActiveTenant.ConvertToSettings2();
            this.dgvSetting3.DataSource = setting2;
            this.dgvSetting3.Columns[0].DefaultCellStyle.BackColor = Color.LightGray;
            var font = this.dgvSetting3.Columns[0].DefaultCellStyle.Font;
            this.dgvSetting3.Columns[0].DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
            this.dgvSetting3.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dgvSetting3.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            
        }

        /// <summary>
        /// Save the tenant changes to memory
        /// </summary>
        private void UpdateFromControls()
        {
            ////var ten = this.TenantList.SingleOrDefault(x => x.TenantName == tenant.TenantName);
            /////this.TenantList.Remove(ten);

            // Select the tenant for control
            var index = this.cbTenant.SelectedIndex;

            // Move all field from interface to tenant
            var dp = (IList<Properties>)this.dgvConfiguration.DataSource;
            var ds = (IList<Properties>)this.dgvSettings.DataSource;
            var ds2 = (IList<Properties>)this.dgvSetting3.DataSource;
            var propertiesList = dp ?? new List<Properties>();
            var settingsList = ds ?? new List<Properties>();
            var setting2List = ds2 ?? new List<Properties>();
            if (index != -1)
            {
                this.TenantList[index].UpdateTenantProperties(propertiesList);
                this.TenantList[index].UpdateTenantSettings(settingsList);
                this.TenantList[index].UpdateTenantSettings2(setting2List);
            }

            this.cbTenant.DataSource = this.TenantList;
            this.cbTenant.SelectedIndex = index;
            this.ActiveTenant = (ITenants)this.cbTenant.SelectedItem;
        }

        /// <summary>
        /// The update first grid from control.
        /// </summary>
        private void UpdateFirstGridFromControl()
        {
            // Select the tenant for control
            var index = this.cbTenant.SelectedIndex;

            // Move all field from interface to tenant
            var dp = (IList<Properties>)this.dgvConfiguration.DataSource;
           var propertiesList = dp ?? new List<Properties>();
           
            if (index != -1)
            {
                this.TenantList[index].UpdateTenantProperties(propertiesList);
            }

            this.cbTenant.DataSource = this.TenantList;
            this.cbTenant.SelectedIndex = index;
            this.ActiveTenant = (ITenants)this.cbTenant.SelectedItem;
        }

        #endregion

        #region Isolated Storage Management

        /// <summary>
        /// The save changes to isolated storage.
        /// </summary>
        /// <param name="content">
        /// The content.
        /// </param>
        private void SaveChangesToIsolatedStorage(string content)
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(
                IsolatedStorageScope.User | IsolatedStorageScope.Assembly,
                null,
                null);
            using (IsolatedStorageFileStream isoStream =
                new IsolatedStorageFileStream("AdvantageToolConfiguration.json", FileMode.Create, isoStore))
            {
                // Write the information
                using (StreamWriter writer = new StreamWriter(isoStream))
                {
                    writer.Write(content);
                }
            }
        }

        /// <summary>
        /// The read isolated storage.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string ReadIsolatedStorage()
        {
            IsolatedStorageFile isoStore = IsolatedStorageFile.GetStore(
                IsolatedStorageScope.User | IsolatedStorageScope.Assembly,
                null,
                null);
            using (IsolatedStorageFileStream isoStream =
                new IsolatedStorageFileStream("AdvantageToolConfiguration.json", FileMode.OpenOrCreate, isoStore))
            {
                // Read the information
                using (StreamReader reader = new StreamReader(isoStream))
                {
                    var content = reader.ReadToEnd();
                    return content;
                }
            }
        }

        #endregion

        #region Helpers (JSON)

        /// <summary>
        /// The de-serialize JSON.
        /// value go direct to TenantList
        /// </summary>
        /// <param name="tenant">
        /// The Original tenant.
        /// </param>
        /// <param name="content">
        /// The content.
        /// </param>
        private void DeserializeJson(ITenants tenant, string content)
        {
            if (tenant.GetType() == typeof(Tenants39))
            {
                var ten = JsonConvert.DeserializeObject<List<Tenants39>>(content);
                this.TenantList = new List<ITenants>();
                foreach (Tenants39 tenants39 in ten)
                {
                    this.TenantList.Add(tenants39);
                }
            }
            else
            {
                var ten = JsonConvert.DeserializeObject<List<Tenants>>(content);
                this.TenantList = new List<ITenants>();
                foreach (Tenants tenan in ten)
                {
                    this.TenantList.Add(tenan);
                }
            }
        }
        #endregion
    }
}
