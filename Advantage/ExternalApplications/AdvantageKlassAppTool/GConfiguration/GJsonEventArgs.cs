﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GJsonEventArgs.cs" company="FAME">
//   2017
// </copyright>
// <summary>
//   The <code>GJsonEventArgs</code>.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace GConfiguration
{
    using System;

    /// <summary>
    /// The <code>GJsonEventArgs</code>.
    /// </summary>
    public class GJsonEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GJsonEventArgs"/> class.
        /// </summary>
        /// <param name="tenantDisplayName">
        /// The tenant display name.
        /// </param>
        public GJsonEventArgs(string tenantDisplayName)
        {
            this.TenantDisplayName = tenantDisplayName;
        }
        
        /// <summary>
        /// Gets or sets the current selected tenant display name.
        /// </summary>
        public string TenantDisplayName { get; set; }
    }
}
