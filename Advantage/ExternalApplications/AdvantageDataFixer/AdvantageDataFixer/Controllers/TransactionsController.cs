﻿using AdvantageDataFixer.Models;
using AdvantageDataFixer.Models.PrebuiltQueries;
using AdvantageDataFixer.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AdvantageDataFixer.Controllers
{
    using System.Globalization;
    using System.Web.Security;

    [Authorize]
    public class TransactionsController : AuthorizedController
    {
        private AdvantageDb db;

        // GET: Transactions/Index/guid
        public ActionResult Index(string id)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            var saTransactions = db.saTransactions
                .Include(s => s.arPrgChargePeriodSeq)
                .Include(s => s.arStuEnrollment)
                .Include(s => s.arTerm)
                .Include(s => s.saAcademicYear.AcademicYearDescrip)
                .Include(s => s.saBatchPayment)
                .Include(s => s.saFundSource)
                .Include(s => s.saPayment)
                .Include(s => s.saPmtPeriod)
                .Include(s => s.saRefund)
                .Include(s => s.saTransCode)
                .Include(s => s.saTransCode1)
                .Include(s => s.saTransType)
                .OrderBy(a => a.TransDate).ThenBy(a => a.ModDate).AsNoTracking();

            //if id is pased filter transactions by student number
            if (id != null)
            {
                id = id.Trim();
                var studentsEnrollments = db.GetEnrollmentsIdsByStudentNumber(id);
                saTransactions = saTransactions.Where(a => studentsEnrollments.Any(b => b.StuEnrollId == a.StuEnrollId) && a.Voided == false);

                var adLead = db.adLeads.Where(a => a.LeadId.ToString() == id).FirstOrDefault();
                ViewBag.Title = $"Ledger - {adLead.StudentNumber} - {adLead.FirstName} {adLead.LastName}";
            }
            else
            {
                ViewBag.Title = "All Ledger Transactions";
            }

            var data = saTransactions.Select(a => new TransactionVM()
            {
                StuEnrollId = a.StuEnrollId.ToString(),
                Date = a.TransDate,
                Reference = a.TransReference,
                DocumentId = a.saPayment.CheckNumber,
                TransCode = a.saTransCode1 != null ? a.saTransCode1.TransCodeDescrip : null,
                Description = a.TransDescrip,
                AcademicYear = a.saAcademicYear.AcademicYearDescrip,
                Term = a.arTerm != null ? a.arTerm.TermDescrip : null,
                TransType = a.saTransType != null ? a.saTransType.Description : null,
                User = a.ModUser,
                Amount = a.TransAmount.ToString(),
                Balance = string.Empty,
                PaymentPeriod = a.PaymentPeriodNumber.ToString(),
                PaymentTypeId = a.saPayment != null ? (int?)a.saPayment.PaymentTypeId : null,
                TransactionId = a.TransactionId.ToString(),
                HasPaymentTiedToTransaction = a.saPayment != null
            }).ToList();

            foreach (var item in data)
            {
                switch (item.PaymentTypeId)
                {
                    case 2:
                        item.DocumentId = "Check Number: " + item.DocumentId;
                        break;
                    case 3:
                        item.DocumentId = "C/C Authorization: " + item.DocumentId;
                        break;
                    case 4:
                        item.DocumentId = "EFT Number: " + item.DocumentId;
                        break;
                    case 5:
                        item.DocumentId = "Money Order Number: " + item.DocumentId;
                        break;
                    case 6:
                        item.DocumentId = "Non Cash Reference #: " + item.DocumentId;
                        break;
                }
            }

            ViewBag.StudentNumber = id;
            return View(data);
        }

        // GET: Transactions/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            saTransaction saTransaction = db.saTransactions.Find(id);
            if (saTransaction == null)
            {
                return HttpNotFound();
            }
            return View(saTransaction);
        }

        public ActionResult AwardLinking(Guid? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            TransactionAwardLinkVM saTransaction = db.saTransactions
                .Include(a => a.arStuEnrollment.adLead)
                .Where(a => a.TransactionId == id)
                .Select(t => new TransactionAwardLinkVM
                {
                    EnrollmentId = t.StuEnrollId,
                    TransactionId = t.TransactionId,
                    TransactionType = t.TransDescrip.ToLower().Contains("Refund") ? "Refund" : t.TransDescrip.ToLower().Contains("Disb") ? "Disbursement" : string.Empty
                }).FirstOrDefault();

            saTransaction saTransaction1 = db.saTransactions.Include(a => a.arStuEnrollment.adLead).Include(a => a.saTransCode1).Where(a => a.TransactionId == id).FirstOrDefault();
            ViewBag.Title = saTransaction1.TransDescrip + " - " + saTransaction1.saTransCode1.TransCodeDescrip + " - " + saTransaction1.TransAmount + " -----  Student: " +
                            saTransaction1.arStuEnrollment.adLead.StudentNumber + " - " +
                            saTransaction1.arStuEnrollment.adLead.FirstName + " " +
                            saTransaction1.arStuEnrollment.adLead.LastName + "";

            if (saTransaction == null)
                return HttpNotFound();


            System.Guid currentScheduleId = Guid.Empty;
            faStudentAwardSchedule currentSchedule = db.faStudentAwardSchedules
                .Where(s => s.TransactionId == saTransaction.TransactionId).FirstOrDefault();

            if (currentSchedule != null)
                currentScheduleId = currentSchedule.AwardScheduleId;


            if (saTransaction.TransactionType == "Refund")
            {
                var refund = this.db.saRefunds.Where(
                        r => r.TransactionId == saTransaction.TransactionId )
                    .FirstOrDefault();

                if (refund != null && refund.AwardScheduleId.HasValue)
                {
                    currentScheduleId = refund.AwardScheduleId.Value;
                }
            }


            if (saTransaction.TransactionType == "Disbursement")
            {
                var disb = this.db.saPmtDisbRels.Where(
                        r => r.TransactionId == saTransaction.TransactionId)
                    .FirstOrDefault();

                if (disb != null && disb.AwardScheduleId.HasValue)
                {
                    currentScheduleId = disb.AwardScheduleId.Value;
                }
            }

            var transactionTypes = new Dictionary<string, string>()
                                       {
                                           {string.Empty, "Select"},{"Disbursement", "Disbursement"}, { "Refund", "Refund"}
                                       };

            var schedules = this.db.faStudentAwardSchedules
                .Include(s=> s.faStudentAward.saFundSource)
                .Where(s => s.faStudentAward.StuEnrollId == saTransaction.EnrollmentId )
                .AsEnumerable()
                .Select(s => new KeyValuePair<Guid, string>
                    (s.AwardScheduleId, (s.faStudentAward.saFundSource.FundSourceDescrip) + " (" + s.faStudentAward.AwardStartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ") - " + (s.ExpectedDate?.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) ?? ""))).OrderBy(t => t.Value).ToList();


            ViewBag.TransactionType = new SelectList(transactionTypes, "Key", "Value", saTransaction.TransactionType);

            ViewBag.StudentAwardScheduleId = new SelectList(schedules, "Key", "Value", currentScheduleId);
            return View(saTransaction);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AwardLinking(TransactionAwardLinkVM link)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            faStudentAwardSchedule award = this.db.faStudentAwardSchedules
                .Where(a => a.AwardScheduleId == link.StudentAwardScheduleId).FirstOrDefault();

            saTransaction transaction = this.db.saTransactions.Include(a => a.arStuEnrollment.adLead).Where(t => t.TransactionId == link.TransactionId)
                .FirstOrDefault();

            if (link.TransactionType == "Refund")
            {

                var refund = this.db.saRefunds.Where(
                        r => r.TransactionId == link.TransactionId || r.AwardScheduleId == link.StudentAwardScheduleId)
                    .FirstOrDefault();

                if (refund != null)
                {
                    refund.AwardScheduleId = link.StudentAwardScheduleId;
                    refund.TransactionId = link.TransactionId;

                    //if fund source id has value
                    if (transaction != null && transaction.FundSourceId.HasValue && transaction.FundSourceId != Guid.Empty)
                    {
                        refund.FundSourceId = transaction.FundSourceId;
                    }

                    if (award != null)
                    {
                        refund.StudentAwardId = award.StudentAwardId;
                    }

                    this.db.Entry(refund).State = EntityState.Modified;
                    this.db.SaveChanges();

                }
            }

            if (link.TransactionType == "Disbursement")
            {
                var disbursement = this.db.saPmtDisbRels.Where(
                        r => r.TransactionId == link.TransactionId || r.AwardScheduleId == link.StudentAwardScheduleId)
                    .FirstOrDefault();

                if (disbursement != null)
                {
                    disbursement.AwardScheduleId = link.StudentAwardScheduleId;
                    disbursement.TransactionId = link.TransactionId;
                    if (award != null)
                    {
                        disbursement.StuAwardId = award.StudentAwardId;
                    }

                    this.db.Entry(disbursement).State = EntityState.Modified;
                    this.db.SaveChanges();
                }
                else
                {
                    var newPmtDisbRel = new saPmtDisbRel()
                    {
                        PmtDisbRelId = Guid.NewGuid(),
                        Amount = transaction.TransAmount * -1,
                        AwardScheduleId = link.StudentAwardScheduleId,
                        ModDate = DateTime.Now,
                        ModUser = "ADF",
                        PayPlanScheduleId = null,
                        StuAwardId = award != null ? (Guid?)award.StudentAwardId : null,
                        TransactionId = link.TransactionId,
                    };

                    this.db.saPmtDisbRels.Add(newPmtDisbRel);
                    this.db.SaveChanges();
                }
            }
            return RedirectToAction("Index", new { id = transaction.arStuEnrollment.adLead.LeadId });


            return View(link);
        }


        // GET: Transactions/Create
        public ActionResult Create(string StudentId = null)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            ViewBag.StuEnrollPayPeriodId = new SelectList(db.arPrgChargePeriodSeqs, "PrgChrPeriodSeqId", "PrgChrPeriodSeqId");
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser");
            ViewBag.TermId = new SelectList(db.arTerms, "TermId", "TermCode");
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears.Where(ay => ay.ModUser.ToLower() != "support").OrderBy(ay => ay.AcademicYearCode).ToList(), "AcademicYearId", "AcademicYearDescrip");
            ViewBag.BatchPaymentId = new SelectList(db.saBatchPayments, "BatchPaymentId", "BatchPaymentNumber");
            ViewBag.FundSourceId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode");
            ViewBag.TransactionId = new SelectList(db.saPayments, "TransactionId", "CheckNumber");
            ViewBag.PmtPeriodId = new SelectList(db.saPmtPeriods, "PmtPeriodId", "ModUser");
            ViewBag.TransactionId = new SelectList(db.saRefunds, "TransactionId", "ModUser");
            ViewBag.PaymentCodeId = new SelectList(db.saTransCodes, "TransCodeId", "TransCodeCode");
            ViewBag.TransCodeId = new SelectList(db.saTransCodes.OrderBy(a => a.TransCodeDescrip), "TransCodeId", "TransCodeDescrip");
            ViewBag.TransTypeId = new SelectList(db.saTransTypes, "TransTypeId", "Description");
            return View();
        }

        // POST: Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TransactionId,StuEnrollId,TermId,CampusId,TransDate,TransCodeId,TransReference,AcademicYearId,TransDescrip,TransAmount,TransTypeId,IsPosted,CreateDate,BatchPaymentId,ViewOrder,IsAutomatic,ModUser,ModDate,Voided,FeeLevelId,FeeId,PaymentCodeId,FundSourceId,StuEnrollPayPeriodId,DisplaySequence,SecondDisplaySequence,PmtPeriodId,PrgChrPeriodSeqId,PaymentPeriodNumber")] saTransaction saTransaction)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            if (ModelState.IsValid)
            {
                saTransaction.TransactionId = Guid.NewGuid();
                db.saTransactions.Add(saTransaction);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StuEnrollPayPeriodId = new SelectList(db.arPrgChargePeriodSeqs, "PrgChrPeriodSeqId", "PrgChrPeriodSeqId", saTransaction.StuEnrollPayPeriodId);
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", saTransaction.StuEnrollId);
            ViewBag.TermId = new SelectList(db.arTerms, "TermId", "TermCode", saTransaction.TermId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears.Where(ay => ay.ModUser.ToLower() != "support").OrderBy(ay => ay.AcademicYearCode).ToList(), "AcademicYearId", "AcademicYearDescrip", saTransaction.AcademicYearId);
            ViewBag.BatchPaymentId = new SelectList(db.saBatchPayments, "BatchPaymentId", "BatchPaymentNumber", saTransaction.BatchPaymentId);
            ViewBag.FundSourceId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", saTransaction.FundSourceId);
            ViewBag.TransactionId = new SelectList(db.saPayments, "TransactionId", "CheckNumber", saTransaction.TransactionId);
            ViewBag.PmtPeriodId = new SelectList(db.saPmtPeriods, "PmtPeriodId", "ModUser", saTransaction.PmtPeriodId);
            ViewBag.TransactionId = new SelectList(db.saRefunds, "TransactionId", "ModUser", saTransaction.TransactionId);
            ViewBag.PaymentCodeId = new SelectList(db.saTransCodes, "TransCodeId", "TransCodeCode", saTransaction.PaymentCodeId);
            ViewBag.TransCodeId = new SelectList(db.saTransCodes.OrderBy(a => a.TransCodeDescrip), "TransCodeId", "TransCodeDescrip", saTransaction.TransCodeId);
            ViewBag.TransTypeId = new SelectList(db.saTransTypes, "TransTypeId", "Description", saTransaction.TransTypeId);
            ViewBag.StudentNumber = saTransaction.arStuEnrollment.LeadId.ToString();
            return View(saTransaction);
        }

        // GET: Transactions/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            saTransaction saTransaction = db.saTransactions.Include(a => a.arStuEnrollment.adLead).Where(a => a.TransactionId == id).FirstOrDefault();

            if (saTransaction == null)
                return HttpNotFound();

            ViewBag.Title = "Editing transaction for student " +
                saTransaction.arStuEnrollment.adLead.StudentNumber + " - " +
                saTransaction.arStuEnrollment.adLead.FirstName + " " +
                saTransaction.arStuEnrollment.adLead.LastName;

            ViewBag.StuEnrollPayPeriodId = new SelectList(db.arPrgChargePeriodSeqs, "PrgChrPeriodSeqId", "PrgChrPeriodSeqId", saTransaction.StuEnrollPayPeriodId);
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", saTransaction.StuEnrollId);
            ViewBag.TermId = new SelectList(db.arTerms, "TermId", "TermCode", saTransaction.TermId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears.Where(ay => ay.ModUser.ToLower() != "support").OrderBy(ay => ay.AcademicYearCode).ToList(), "AcademicYearId", "AcademicYearDescrip", saTransaction.AcademicYearId);
            ViewBag.BatchPaymentId = new SelectList(db.saBatchPayments, "BatchPaymentId", "BatchPaymentNumber", saTransaction.BatchPaymentId);
            ViewBag.FundSourceId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", saTransaction.FundSourceId);
            ViewBag.TransactionId = new SelectList(db.saPayments, "TransactionId", "CheckNumber", saTransaction.TransactionId);
            ViewBag.PmtPeriodId = new SelectList(db.saPmtPeriods, "PmtPeriodId", "ModUser", saTransaction.PmtPeriodId);
            ViewBag.TransactionId = new SelectList(db.saRefunds, "TransactionId", "ModUser", saTransaction.TransactionId);
            ViewBag.PaymentCodeId = new SelectList(db.saTransCodes, "TransCodeId", "TransCodeCode", saTransaction.PaymentCodeId);
            ViewBag.TransCodeId = new SelectList(db.saTransCodes.OrderBy(a => a.TransCodeDescrip), "TransCodeId", "TransCodeDescrip", saTransaction.TransCodeId);
            ViewBag.TransTypeId = new SelectList(db.saTransTypes, "TransTypeId", "Description", saTransaction.TransTypeId);
            ViewBag.StudentNumber = saTransaction.arStuEnrollment.LeadId.ToString();
            return View(saTransaction);
        }

        // POST: Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TransactionId,StuEnrollId,TermId,CampusId,TransDate,TransCodeId,TransReference,AcademicYearId,TransDescrip,TransAmount,TransTypeId,IsPosted,CreateDate,BatchPaymentId,ViewOrder,IsAutomatic,ModUser,ModDate,Voided,FeeLevelId,FeeId,PaymentCodeId,FundSourceId,StuEnrollPayPeriodId,DisplaySequence,SecondDisplaySequence,PmtPeriodId,PrgChrPeriodSeqId,PaymentPeriodNumber")] saTransaction saTransaction)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            saTransaction saTransactionToEdit = db.saTransactions
                .Include(a => a.arStuEnrollment.adLead)
                .Where(a => a.TransactionId == saTransaction.TransactionId).FirstOrDefault();

            //enabled fields
            saTransactionToEdit.TransReference = saTransaction.TransReference ?? string.Empty;
            saTransactionToEdit.AcademicYearId = saTransaction.AcademicYearId;
            saTransactionToEdit.TransTypeId = saTransaction.TransTypeId;
            saTransactionToEdit.IsPosted = saTransaction.IsPosted;
            saTransactionToEdit.PaymentPeriodNumber = saTransaction.PaymentPeriodNumber;
            saTransactionToEdit.TransCodeId = saTransaction.TransCodeId;
            saTransactionToEdit.TransDescrip = saTransaction.TransDescrip;

            saTransactionToEdit.ModDate = DateTime.Now;

            //potentially disabled fields, must be enabled if enabled on view and vice-versa
            //saTransactionToEdit.TransDate = saTransaction.TransDate;
            //saTransactionToEdit.TransDescrip = saTransaction.TransDescrip;
            //saTransactionToEdit.TransAmount = saTransaction.TransAmount;
            //saTransactionToEdit.ModUser = saTransaction.ModUser;
            //saTransactionToEdit.PaymentPeriodNumber = saTransaction.PaymentPeriodNumber;
            //saTransactionToEdit.ModDate = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(saTransactionToEdit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = saTransactionToEdit.arStuEnrollment.adLead.LeadId });
            }

            ViewBag.StuEnrollPayPeriodId = new SelectList(db.arPrgChargePeriodSeqs, "PrgChrPeriodSeqId", "PrgChrPeriodSeqId", saTransaction.StuEnrollPayPeriodId);
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", saTransaction.StuEnrollId);
            ViewBag.TermId = new SelectList(db.arTerms, "TermId", "TermCode", saTransaction.TermId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears.Where(ay => ay.ModUser.ToLower() != "support").OrderBy(ay => ay.AcademicYearCode).ToList(), "AcademicYearId", "AcademicYearDescrip", saTransaction.AcademicYearId);
            ViewBag.BatchPaymentId = new SelectList(db.saBatchPayments, "BatchPaymentId", "BatchPaymentNumber", saTransaction.BatchPaymentId);
            ViewBag.FundSourceId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", saTransaction.FundSourceId);
            ViewBag.TransactionId = new SelectList(db.saPayments, "TransactionId", "CheckNumber", saTransaction.TransactionId);
            ViewBag.PmtPeriodId = new SelectList(db.saPmtPeriods, "PmtPeriodId", "ModUser", saTransaction.PmtPeriodId);
            ViewBag.TransactionId = new SelectList(db.saRefunds, "TransactionId", "ModUser", saTransaction.TransactionId);
            ViewBag.PaymentCodeId = new SelectList(db.saTransCodes, "TransCodeId", "TransCodeCode", saTransaction.PaymentCodeId);
            ViewBag.TransCodeId = new SelectList(db.saTransCodes, "TransCodeId", "TransCodeCode", saTransaction.TransCodeId);
            ViewBag.TransTypeId = new SelectList(db.saTransTypes, "TransTypeId", "Description", saTransaction.TransTypeId);
            return View(saTransaction);
        }

        // GET: Transactions/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            saTransaction saTransaction = db.saTransactions.Find(id);

            if (saTransaction == null)
                return HttpNotFound();

            ViewBag.HasPayment = saTransaction.saPayment != null;
            ViewBag.HasRefund = saTransaction.saRefund != null;
            ViewBag.HasPmtDis = db.saPmtDisbRels.Where(a => a.TransactionId == id).Any();
            ViewBag.HasAwardSchedule = db.faStudentAwardSchedules.Where(a => a.TransactionId == id).Any();

            ViewBag.StudentNumber = saTransaction.arStuEnrollment.LeadId.ToString();
            return View(saTransaction);
        }

        // POST: Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    // takes care of saTransaction, saPayment, saRefunds
                    var payments = db.saPayments.Where(a => a.TransactionId == id).ToList();

                    if (payments != null)
                        db.saPayments.RemoveRange(payments);

                    db.SaveChanges();

                    var refunds = db.saRefunds.Where(a => a.TransactionId == id).ToList();

                    if (refunds != null)
                        db.saRefunds.RemoveRange(refunds);

                    db.SaveChanges();

                    var saTransaction = db.saTransactions.Include(a => a.arStuEnrollment.adLead).Where(a => a.TransactionId == id).FirstOrDefault();
                    var leadId = saTransaction.arStuEnrollment.LeadId;

                    if (saTransaction != null)
                        db.saTransactions.Remove(saTransaction);

                    db.SaveChanges();

                    //deletes saPmtDisbRels
                    var saPmtDisbRelsToDelete = db.saPmtDisbRels.Where(a => a.TransactionId == id).ToList();

                    if (saPmtDisbRelsToDelete != null)
                        db.saPmtDisbRels.RemoveRange(saPmtDisbRelsToDelete);

                    db.SaveChanges();

                    //update faStudentAwardSchedule
                    var faStudentAwardSchedule = db.faStudentAwardSchedules.Where(a => a.TransactionId == id).ToList();

                    foreach (var item in faStudentAwardSchedule)
                    {
                        item.TransactionId = null;
                        db.Entry(item).State = EntityState.Modified;
                    }

                    db.SaveChanges();
                    dbContextTransaction.Commit();
                    return RedirectToAction("Index", "Transactions", new { id = leadId.ToString() });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback(); //Required according to MSDN article 
                    ViewBag.Error = "Could not delete record";
                    saTransaction saTransaction = db.saTransactions.Find(id);
                    ViewBag.HasPayment = saTransaction.saPayment != null;
                    ViewBag.HasRefund = saTransaction.saRefund != null;
                    ViewBag.HasPmtDis = db.saPmtDisbRels.Where(a => a.TransactionId == id).Any();
                    ViewBag.HasAwardSchedule = db.faStudentAwardSchedules.Where(a => a.TransactionId == id).Any();
                    ViewBag.StudentNumber = saTransaction.arStuEnrollment.LeadId.ToString();
                    return View(saTransaction);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
