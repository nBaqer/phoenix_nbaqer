﻿using AdvantageDataFixer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AdvantageDataFixer.Controllers
{
    using System.Web.Security;

    [Authorize]
    public class LeadsController : AuthorizedController
    {
        private AdvantageDb db;

        // GET: adLeads
        public ActionResult Index()
        {
            this.db = this.getAdvantageDb();

            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            var adLeads = db.adLeads.Include(a => a.adAdminCriteria).Include(a => a.adAgencySponsor).Include(a => a.adCitizenship).Include(a => a.adCounty).Include(a => a.adCountry).Include(a => a.adDegCertSeeking).Include(a => a.adDependencyType).Include(a => a.adEdLvl).Include(a => a.adEthCode).Include(a => a.adGender).Include(a => a.adGeographicType).Include(a => a.adLeadGroup).Include(a => a.adMaritalStatu).Include(a => a.adNationality).Include(a => a.adSourceAdvertisement).Include(a => a.adSourceCatagory).Include(a => a.adSourceType).Include(a => a.adVendorCampaign).Include(a => a.arAttendType).Include(a => a.arHousing).Include(a => a.arPrgGrp).Include(a => a.arPrgVersion).Include(a => a.arProgram).Include(a => a.arProgSchedule).Include(a => a.arShift).Include(a => a.plAddressType).Include(a => a.syCampus).Include(a => a.syFamilyIncome).Include(a => a.syInstitution).Include(a => a.syPrefix).Include(a => a.syState).Include(a => a.syState1).Include(a => a.syState2).Include(a => a.syStatusCode).Include(a => a.syStatus).Include(a => a.syStatus1).Include(a => a.syStatus2).Include(a => a.sySuffix).Include(a => a.syUser).Include(a => a.syUser1).Where(l => l.StudentId != Guid.Empty);
            return View(adLeads.ToList());
        }

        // GET: adLeads/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            adLead adLead = db.adLeads.Find(id);
            if (adLead == null)
            {
                return HttpNotFound();
            }
            return View(adLead);
        }

        // GET: adLeads/Create
        public ActionResult Create()
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code");
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode");
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode");
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode");
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode");
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code");
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code");
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode");
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode");
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode");
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code");
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip");
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode");
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode");
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip");
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip");
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip");
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode");
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code");
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code");
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode");
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode");
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode");
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code");
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode");
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip");
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode");
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip");
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode");
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode");
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode");
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode");
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode");
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode");
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status");
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status");
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status");
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode");
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName");
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName");
            return View();
        }

        // POST: adLeads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LeadId,ProspectID,FirstName,LastName,MiddleName,SSN,ModUser,ModDate,Phone,HomeEmail,Address1,Address2,City,StateId,Zip,LeadStatus,WorkEmail,AddressType,Prefix,Suffix,BirthDate,Sponsor,AdmissionsRep,AssignedDate,Gender,Race,MaritalStatus,FamilyIncome,Children,PhoneType,PhoneStatus,SourceCategoryID,SourceTypeID,SourceDate,AreaID,ProgramID,ExpectedStart,ShiftID,Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,SourceAdvertisement,CampusId,PrgVerId,Country,County,Age,PreviousEducation,AddressStatus,CreatedDate,RecruitmentOffice,OtherState,ForeignPhone,ForeignZip,LeadgrpId,DependencyTypeId,DegCertSeekingId,GeographicTypeId,HousingId,admincriteriaid,DateApplied,InquiryTime,AdvertisementNote,Phone2,PhoneType2,PhoneStatus2,ForeignPhone2,DefaultPhone,IsDisabled,IsFirstTimeInSchool,IsFirstTimePostSecSchool,EntranceInterviewDate,ProgramScheduleId,BestTime,DistanceToSchool,CampaignId,ProgramOfInterest,CampusOfInterest,TransportationId,NickName,AttendTypeId,PreferredContactId,AddressApt,NoneEmail,HighSchoolId,HighSchoolGradDate,AttendingHs,StudentId,StudentNumber,StudentStatusId,ReasonNotEnrolledId,EnrollStateId,VendorId,AfaStudentId")] adLead adLead)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            if (ModelState.IsValid)
            {
                adLead.LeadId = Guid.NewGuid();
                db.adLeads.Add(adLead);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code", adLead.admincriteriaid);
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode", adLead.Sponsor);
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode", adLead.Citizen);
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode", adLead.County);
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode", adLead.Country);
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code", adLead.DegCertSeekingId);
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code", adLead.DependencyTypeId);
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode", adLead.PreviousEducation);
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode", adLead.Race);
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode", adLead.Gender);
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code", adLead.GeographicTypeId);
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip", adLead.LeadgrpId);
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode", adLead.MaritalStatus);
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode", adLead.Nationality);
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip", adLead.SourceAdvertisement);
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip", adLead.SourceCategoryID);
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip", adLead.SourceTypeID);
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode", adLead.CampaignId);
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code", adLead.AttendTypeId);
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code", adLead.HousingId);
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode", adLead.AreaID);
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode", adLead.PrgVerId);
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode", adLead.ProgramID);
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code", adLead.ProgramScheduleId);
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode", adLead.ShiftID);
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip", adLead.AddressType);
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode", adLead.CampusId);
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip", adLead.FamilyIncome);
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode", adLead.HighSchoolId);
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode", adLead.Prefix);
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode", adLead.DrivLicStateID);
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.EnrollStateId);
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.StateId);
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode", adLead.LeadStatus);
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.AddressStatus);
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.PhoneStatus);
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status", adLead.StudentStatusId);
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode", adLead.Suffix);
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName", adLead.AdmissionsRep);
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName", adLead.VendorId);
            return View(adLead);
        }

        // GET: adLeads/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            adLead adLead = db.adLeads.Find(id);
            if (adLead == null)
            {
                return HttpNotFound();
            }
            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code", adLead.admincriteriaid);
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode", adLead.Sponsor);
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode", adLead.Citizen);
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode", adLead.County);
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode", adLead.Country);
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code", adLead.DegCertSeekingId);
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code", adLead.DependencyTypeId);
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode", adLead.PreviousEducation);
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode", adLead.Race);
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode", adLead.Gender);
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code", adLead.GeographicTypeId);
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip", adLead.LeadgrpId);
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode", adLead.MaritalStatus);
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode", adLead.Nationality);
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip", adLead.SourceAdvertisement);
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip", adLead.SourceCategoryID);
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip", adLead.SourceTypeID);
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode", adLead.CampaignId);
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code", adLead.AttendTypeId);
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code", adLead.HousingId);
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode", adLead.AreaID);
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode", adLead.PrgVerId);
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode", adLead.ProgramID);
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code", adLead.ProgramScheduleId);
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode", adLead.ShiftID);
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip", adLead.AddressType);
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode", adLead.CampusId);
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip", adLead.FamilyIncome);
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode", adLead.HighSchoolId);
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode", adLead.Prefix);
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode", adLead.DrivLicStateID);
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.EnrollStateId);
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.StateId);
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode", adLead.LeadStatus);
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.AddressStatus);
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.PhoneStatus);
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status", adLead.StudentStatusId);
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode", adLead.Suffix);
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName", adLead.AdmissionsRep);
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName", adLead.VendorId);

            ViewBag.Title = $"Ledger - {adLead.StudentNumber} - {adLead.FirstName} {adLead.LastName}";
            return View(adLead);
        }

        // GET: adLeads/Edit/5
        public ActionResult EditAfaStudentId(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            adLead adLead = db.adLeads.Find(id);
            if (adLead == null)
            {
                return HttpNotFound();
            }
            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code", adLead.admincriteriaid);
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode", adLead.Sponsor);
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode", adLead.Citizen);
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode", adLead.County);
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode", adLead.Country);
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code", adLead.DegCertSeekingId);
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code", adLead.DependencyTypeId);
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode", adLead.PreviousEducation);
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode", adLead.Race);
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode", adLead.Gender);
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code", adLead.GeographicTypeId);
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip", adLead.LeadgrpId);
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode", adLead.MaritalStatus);
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode", adLead.Nationality);
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip", adLead.SourceAdvertisement);
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip", adLead.SourceCategoryID);
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip", adLead.SourceTypeID);
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode", adLead.CampaignId);
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code", adLead.AttendTypeId);
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code", adLead.HousingId);
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode", adLead.AreaID);
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode", adLead.PrgVerId);
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode", adLead.ProgramID);
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code", adLead.ProgramScheduleId);
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode", adLead.ShiftID);
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip", adLead.AddressType);
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode", adLead.CampusId);
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip", adLead.FamilyIncome);
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode", adLead.HighSchoolId);
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode", adLead.Prefix);
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode", adLead.DrivLicStateID);
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.EnrollStateId);
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.StateId);
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode", adLead.LeadStatus);
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.AddressStatus);
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.PhoneStatus);
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status", adLead.StudentStatusId);
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode", adLead.Suffix);
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName", adLead.AdmissionsRep);
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName", adLead.VendorId);

            ViewBag.Title = $"Ledger - {adLead.StudentNumber} - {adLead.FirstName} {adLead.LastName}";
            return View("EditAfaStudentId", adLead);
        }


        // POST: adLeads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LeadId,ProspectID,FirstName,LastName,MiddleName,SSN,ModUser,ModDate,Phone,HomeEmail,Address1,Address2,City,StateId,Zip,LeadStatus,WorkEmail,AddressType,Prefix,Suffix,BirthDate,Sponsor,AdmissionsRep,AssignedDate,Gender,Race,MaritalStatus,FamilyIncome,Children,PhoneType,PhoneStatus,SourceCategoryID,SourceTypeID,SourceDate,AreaID,ProgramID,ExpectedStart,ShiftID,Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,SourceAdvertisement,CampusId,PrgVerId,Country,County,Age,PreviousEducation,AddressStatus,CreatedDate,RecruitmentOffice,OtherState,ForeignPhone,ForeignZip,LeadgrpId,DependencyTypeId,DegCertSeekingId,GeographicTypeId,HousingId,admincriteriaid,DateApplied,InquiryTime,AdvertisementNote,Phone2,PhoneType2,PhoneStatus2,ForeignPhone2,DefaultPhone,IsDisabled,IsFirstTimeInSchool,IsFirstTimePostSecSchool,EntranceInterviewDate,ProgramScheduleId,BestTime,DistanceToSchool,CampaignId,ProgramOfInterest,CampusOfInterest,TransportationId,NickName,AttendTypeId,PreferredContactId,AddressApt,NoneEmail,HighSchoolId,HighSchoolGradDate,AttendingHs,StudentId,StudentNumber,StudentStatusId,ReasonNotEnrolledId,EnrollStateId,VendorId,AfaStudentId")] adLead adLead)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }
            var adLeadItemToEdit = db.adLeads.Where(a => a.LeadId == adLead.LeadId).FirstOrDefault();
            var hasError = false;

            if (adLeadItemToEdit == null || adLead.StudentNumber == null || adLead.StudentNumber.Trim() == "")
            {
                ViewBag.Error = "Invalid student number entered";
                hasError = true;
            }

            var newStudentNumber = adLead.StudentNumber.Trim();


            var studentNumberIsUnique =
                !db.adLeads.Any(a => a.LeadId != adLead.LeadId && a.StudentNumber.Trim() == newStudentNumber)
            || !db.arStuEnrollments.Any(a => a.LeadId != adLead.LeadId && a.BadgeNumber.Trim() == newStudentNumber);

            if (!studentNumberIsUnique)
                ViewBag.Error = "Another student has this number";

            if (ModelState.IsValid && studentNumberIsUnique && !hasError)
            {

                //update lead
                adLeadItemToEdit.StudentNumber = newStudentNumber;
                db.Entry(adLeadItemToEdit).State = EntityState.Modified;

                //update enrollments
                var enrollments = db.arStuEnrollments.Where(a => a.LeadId == adLead.LeadId).ToList();
                foreach (var enrollment in enrollments)
                {
                    enrollment.BadgeNumber = newStudentNumber;
                    db.Entry(enrollment).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Index", new { id = adLead.LeadId });
            }
            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code", adLead.admincriteriaid);
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode", adLead.Sponsor);
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode", adLead.Citizen);
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode", adLead.County);
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode", adLead.Country);
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code", adLead.DegCertSeekingId);
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code", adLead.DependencyTypeId);
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode", adLead.PreviousEducation);
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode", adLead.Race);
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode", adLead.Gender);
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code", adLead.GeographicTypeId);
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip", adLead.LeadgrpId);
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode", adLead.MaritalStatus);
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode", adLead.Nationality);
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip", adLead.SourceAdvertisement);
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip", adLead.SourceCategoryID);
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip", adLead.SourceTypeID);
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode", adLead.CampaignId);
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code", adLead.AttendTypeId);
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code", adLead.HousingId);
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode", adLead.AreaID);
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode", adLead.PrgVerId);
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode", adLead.ProgramID);
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code", adLead.ProgramScheduleId);
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode", adLead.ShiftID);
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip", adLead.AddressType);
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode", adLead.CampusId);
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip", adLead.FamilyIncome);
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode", adLead.HighSchoolId);
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode", adLead.Prefix);
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode", adLead.DrivLicStateID);
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.EnrollStateId);
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.StateId);
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode", adLead.LeadStatus);
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.AddressStatus);
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.PhoneStatus);
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status", adLead.StudentStatusId);
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode", adLead.Suffix);
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName", adLead.AdmissionsRep);
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName", adLead.VendorId);
            ViewBag.Title = $"Ledger - {adLeadItemToEdit.StudentNumber} - {adLeadItemToEdit.FirstName} {adLeadItemToEdit.LastName}";
            return View(adLead);
        }

        // POST: adLeads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAfaStudentId([Bind(Include = "LeadId,ProspectID,FirstName,LastName,MiddleName,SSN,ModUser,ModDate,Phone,HomeEmail,Address1,Address2,City,StateId,Zip,LeadStatus,WorkEmail,AddressType,Prefix,Suffix,BirthDate,Sponsor,AdmissionsRep,AssignedDate,Gender,Race,MaritalStatus,FamilyIncome,Children,PhoneType,PhoneStatus,SourceCategoryID,SourceTypeID,SourceDate,AreaID,ProgramID,ExpectedStart,ShiftID,Nationality,Citizen,DrivLicStateID,DrivLicNumber,AlienNumber,Comments,SourceAdvertisement,CampusId,PrgVerId,Country,County,Age,PreviousEducation,AddressStatus,CreatedDate,RecruitmentOffice,OtherState,ForeignPhone,ForeignZip,LeadgrpId,DependencyTypeId,DegCertSeekingId,GeographicTypeId,HousingId,admincriteriaid,DateApplied,InquiryTime,AdvertisementNote,Phone2,PhoneType2,PhoneStatus2,ForeignPhone2,DefaultPhone,IsDisabled,IsFirstTimeInSchool,IsFirstTimePostSecSchool,EntranceInterviewDate,ProgramScheduleId,BestTime,DistanceToSchool,CampaignId,ProgramOfInterest,CampusOfInterest,TransportationId,NickName,AttendTypeId,PreferredContactId,AddressApt,NoneEmail,HighSchoolId,HighSchoolGradDate,AttendingHs,StudentId,StudentNumber,StudentStatusId,ReasonNotEnrolledId,EnrollStateId,VendorId,AfaStudentId")] adLead adLead)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }
            var adLeadItemToEdit = db.adLeads.Where(a => a.LeadId == adLead.LeadId).FirstOrDefault();

            if (ModelState.IsValid)
            {
                //update lead
                adLeadItemToEdit.AfaStudentId = adLead.AfaStudentId;
                db.Entry(adLeadItemToEdit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = adLead.LeadId });
            }
            ViewBag.admincriteriaid = new SelectList(db.adAdminCriterias, "admincriteriaid", "Code", adLead.admincriteriaid);
            ViewBag.Sponsor = new SelectList(db.adAgencySponsors, "AgencySpId", "AgencySpCode", adLead.Sponsor);
            ViewBag.Citizen = new SelectList(db.adCitizenships, "CitizenshipId", "CitizenshipCode", adLead.Citizen);
            ViewBag.County = new SelectList(db.adCounties, "CountyId", "CountyCode", adLead.County);
            ViewBag.Country = new SelectList(db.adCountries, "CountryId", "CountryCode", adLead.Country);
            ViewBag.DegCertSeekingId = new SelectList(db.adDegCertSeekings, "DegCertSeekingId", "Code", adLead.DegCertSeekingId);
            ViewBag.DependencyTypeId = new SelectList(db.adDependencyTypes, "DependencyTypeId", "Code", adLead.DependencyTypeId);
            ViewBag.PreviousEducation = new SelectList(db.adEdLvls, "EdLvlId", "EdLvlCode", adLead.PreviousEducation);
            ViewBag.Race = new SelectList(db.adEthCodes, "EthCodeId", "EthCode", adLead.Race);
            ViewBag.Gender = new SelectList(db.adGenders, "GenderId", "GenderCode", adLead.Gender);
            ViewBag.GeographicTypeId = new SelectList(db.adGeographicTypes, "GeographicTypeId", "Code", adLead.GeographicTypeId);
            ViewBag.LeadgrpId = new SelectList(db.adLeadGroups, "LeadGrpId", "Descrip", adLead.LeadgrpId);
            ViewBag.MaritalStatus = new SelectList(db.adMaritalStatus, "MaritalStatId", "MaritalStatCode", adLead.MaritalStatus);
            ViewBag.Nationality = new SelectList(db.adNationalities, "NationalityId", "NationalityCode", adLead.Nationality);
            ViewBag.SourceAdvertisement = new SelectList(db.adSourceAdvertisements, "SourceAdvId", "SourceAdvDescrip", adLead.SourceAdvertisement);
            ViewBag.SourceCategoryID = new SelectList(db.adSourceCatagories, "SourceCatagoryId", "SourceCatagoryDescrip", adLead.SourceCategoryID);
            ViewBag.SourceTypeID = new SelectList(db.adSourceTypes, "SourceTypeId", "SourceTypeDescrip", adLead.SourceTypeID);
            ViewBag.CampaignId = new SelectList(db.adVendorCampaigns, "CampaignId", "CampaignCode", adLead.CampaignId);
            ViewBag.AttendTypeId = new SelectList(db.arAttendTypes, "AttendTypeId", "Code", adLead.AttendTypeId);
            ViewBag.HousingId = new SelectList(db.arHousings, "HousingId", "Code", adLead.HousingId);
            ViewBag.AreaID = new SelectList(db.arPrgGrps, "PrgGrpId", "PrgGrpCode", adLead.AreaID);
            ViewBag.PrgVerId = new SelectList(db.arPrgVersions, "PrgVerId", "PrgVerCode", adLead.PrgVerId);
            ViewBag.ProgramID = new SelectList(db.arPrograms, "ProgId", "ProgCode", adLead.ProgramID);
            ViewBag.ProgramScheduleId = new SelectList(db.arProgSchedules, "ScheduleId", "Code", adLead.ProgramScheduleId);
            ViewBag.ShiftID = new SelectList(db.arShifts, "ShiftId", "ShiftCode", adLead.ShiftID);
            ViewBag.AddressType = new SelectList(db.plAddressTypes, "AddressTypeId", "AddressDescrip", adLead.AddressType);
            ViewBag.CampusId = new SelectList(db.syCampuses, "CampusId", "CampCode", adLead.CampusId);
            ViewBag.FamilyIncome = new SelectList(db.syFamilyIncomes, "FamilyIncomeID", "FamilyIncomeDescrip", adLead.FamilyIncome);
            ViewBag.HighSchoolId = new SelectList(db.syInstitutions, "HSId", "HSCode", adLead.HighSchoolId);
            ViewBag.Prefix = new SelectList(db.syPrefixes, "PrefixId", "PrefixCode", adLead.Prefix);
            ViewBag.DrivLicStateID = new SelectList(db.syStates, "StateId", "StateCode", adLead.DrivLicStateID);
            ViewBag.EnrollStateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.EnrollStateId);
            ViewBag.StateId = new SelectList(db.syStates, "StateId", "StateCode", adLead.StateId);
            ViewBag.LeadStatus = new SelectList(db.syStatusCodes, "StatusCodeId", "StatusCode", adLead.LeadStatus);
            ViewBag.AddressStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.AddressStatus);
            ViewBag.PhoneStatus = new SelectList(db.syStatuses, "StatusId", "Status", adLead.PhoneStatus);
            ViewBag.StudentStatusId = new SelectList(db.syStatuses, "StatusId", "Status", adLead.StudentStatusId);
            ViewBag.Suffix = new SelectList(db.sySuffixes, "SuffixId", "SuffixCode", adLead.Suffix);
            ViewBag.AdmissionsRep = new SelectList(db.syUsers, "UserId", "FullName", adLead.AdmissionsRep);
            ViewBag.VendorId = new SelectList(db.syUsers, "UserId", "FullName", adLead.VendorId);
            ViewBag.Title = $"Ledger - {adLeadItemToEdit.StudentNumber} - {adLeadItemToEdit.FirstName} {adLeadItemToEdit.LastName}";
            return View(adLead);
        }

        // GET: adLeads/Delete/5
        public ActionResult Delete(Guid? id)
        {
            this.db = this.getAdvantageDb();

            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            adLead adLead = db.adLeads.Find(id);
            if (adLead == null)
            {
                return HttpNotFound();
            }
            return View(adLead);
        }

        // POST: adLeads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            this.db = this.getAdvantageDb();

            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            adLead adLead = db.adLeads.Find(id);
            db.adLeads.Remove(adLead);
            db.SaveChanges();
            return this.RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.db != null)
                {
                    db.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
