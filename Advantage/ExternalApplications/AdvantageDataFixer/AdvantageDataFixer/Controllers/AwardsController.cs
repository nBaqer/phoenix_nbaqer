﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdvantageDataFixer.Models;

namespace AdvantageDataFixer.Controllers
{
    using System.Globalization;
    using System.Web.Security;

    using AdvantageDataFixer.Models.PrebuiltQueries;
    using AdvantageDataFixer.ViewModels;

    [Authorize]
    public class AwardsController : AuthorizedController
    {
        private AdvantageDb db;
        // GET: Awards
        public ActionResult Index(string id)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            var faStudentAwards = db.faStudentAwards
                .Include(f => f.arStuEnrollment)
                .Include(f => f.faLender)
                .Include(f => f.faLender1)
                .Include(f => f.faLender2)
                .Include(f => f.saAcademicYear)
                .Include(f => f.saFundSource)
                .AsNoTracking();

            if (id != null)
            {
                id = id.Trim();
                var studentsEnrollments = db.GetEnrollmentsIdsByStudentNumber(id);

                faStudentAwards = faStudentAwards.Where(a => studentsEnrollments.Any(b => b.StuEnrollId == a.StuEnrollId));

                var adLead = db.adLeads.Where(a => a.LeadId.ToString() == id).FirstOrDefault();
                ViewBag.Title = $"Awards - {adLead.StudentNumber} - {adLead.FirstName} {adLead.LastName}";

            }

            else
            {
                ViewBag.Title = "All Student Awards";
            }

            var data = faStudentAwards.AsEnumerable().Select(a => new AwardsVM()
            {
                FundSourceCode = a.saFundSource.FundSourceDescrip + " (" + a.AwardStartDate.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture) + ")",
                StartDate = a.AwardStartDate,
                EndDate = a.AwardEndDate,
                AcademicYearCode = a.saAcademicYear.AcademicYearDescrip,
                LoanId = a.LoanId,
                LenderCode = a.faLender?.LenderDescrip,
                ServicerCode = a.faLender1?.LenderDescrip,
                GuarantorCode = a.faLender2?.LenderDescrip,
                GrossAmount = a.GrossAmount,
                LoanFees = a.LoanFees ?? 0,
                NetAmount = a.GrossAmount - (a.LoanFees ?? 0),
                FAId = a.FA_Id,
                NumberOfDisbursements = a.Disbursements,
                Term = a.arStuEnrollment.arPrgVersion.PrgVerDescrip

            }).ToList();

            return View(data);
        }

        // GET: Awards/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            faStudentAward faStudentAward = db.faStudentAwards.Find(id);
            if (faStudentAward == null)
            {
                return HttpNotFound();
            }
            return View(faStudentAward);
        }

        // GET: Awards/Create
        public ActionResult Create()
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser");
            ViewBag.GuarantorId = new SelectList(db.faLenders, "LenderId", "Code");
            ViewBag.LenderId = new SelectList(db.faLenders, "LenderId", "Code");
            ViewBag.ServicerId = new SelectList(db.faLenders, "LenderId", "Code");
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears, "AcademicYearId", "AcademicYearCode");
            ViewBag.AwardTypeId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode");
            return View();
        }

        // POST: Awards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentAwardId,AwardId,StuEnrollId,AwardTypeId,AcademicYearId,LenderId,ServicerId,GuarantorId,GrossAmount,LoanFees,ModUser,ModDate,AwardStartDate,AwardEndDate,Disbursements,LoanId,EMASFundCode,FA_Id,AwardCode,AwardSubCode,AwardStatus")] faStudentAward faStudentAward)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            if (ModelState.IsValid)
            {
                faStudentAward.StudentAwardId = Guid.NewGuid();
                db.faStudentAwards.Add(faStudentAward);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", faStudentAward.StuEnrollId);
            ViewBag.GuarantorId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.GuarantorId);
            ViewBag.LenderId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.LenderId);
            ViewBag.ServicerId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.ServicerId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears, "AcademicYearId", "AcademicYearCode", faStudentAward.AcademicYearId);
            ViewBag.AwardTypeId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", faStudentAward.AwardTypeId);
            return View(faStudentAward);
        }

        // GET: Awards/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            faStudentAward faStudentAward = db.faStudentAwards.Find(id);
            if (faStudentAward == null)
            {
                return HttpNotFound();
            }
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", faStudentAward.StuEnrollId);
            ViewBag.GuarantorId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.GuarantorId);
            ViewBag.LenderId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.LenderId);
            ViewBag.ServicerId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.ServicerId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears, "AcademicYearId", "AcademicYearCode", faStudentAward.AcademicYearId);
            ViewBag.AwardTypeId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", faStudentAward.AwardTypeId);
            return View(faStudentAward);
        }

        // POST: Awards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentAwardId,AwardId,StuEnrollId,AwardTypeId,AcademicYearId,LenderId,ServicerId,GuarantorId,GrossAmount,LoanFees,ModUser,ModDate,AwardStartDate,AwardEndDate,Disbursements,LoanId,EMASFundCode,FA_Id,AwardCode,AwardSubCode,AwardStatus")] faStudentAward faStudentAward)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            if (ModelState.IsValid)
            {
                db.Entry(faStudentAward).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            ViewBag.StuEnrollId = new SelectList(db.arStuEnrollments, "StuEnrollId", "ModUser", faStudentAward.StuEnrollId);
            ViewBag.GuarantorId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.GuarantorId);
            ViewBag.LenderId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.LenderId);
            ViewBag.ServicerId = new SelectList(db.faLenders, "LenderId", "Code", faStudentAward.ServicerId);
            ViewBag.AcademicYearId = new SelectList(db.saAcademicYears, "AcademicYearId", "AcademicYearCode", faStudentAward.AcademicYearId);
            ViewBag.AwardTypeId = new SelectList(db.saFundSources, "FundSourceId", "FundSourceCode", faStudentAward.AwardTypeId);
            return View(faStudentAward);
        }

        // GET: Awards/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            faStudentAward faStudentAward = db.faStudentAwards.Find(id);
            if (faStudentAward == null)
            {
                return HttpNotFound();
            }
            return View(faStudentAward);
        }

        // POST: Awards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            this.db = this.getAdvantageDb();
            if (this.db == null)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }

            faStudentAward faStudentAward = db.faStudentAwards.Find(id);
            db.faStudentAwards.Remove(faStudentAward);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
