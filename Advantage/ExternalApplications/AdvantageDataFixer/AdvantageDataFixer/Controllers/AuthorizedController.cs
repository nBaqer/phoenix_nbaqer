﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvantageDataFixer.Controllers
{
    using System.Web.Mvc;
    using System.Web.Security;

    using AdvantageDataFixer.Models;

    public class AuthorizedController : Controller
    {
        protected AdvantageDb getAdvantageDb()
        {
            if (Session["AdvantageDbConnectionString"] != null)
            {
                AdvantageDb db = new AdvantageDb();
                db.Database.Connection.ConnectionString = Session["AdvantageDbConnectionString"].ToString();
                return db;
            }

            return null;
        }
    }
}