﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdvantageDataFixer.Controllers
{
    using System.Data.Entity;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Security;

    using AdvantageDataFixer.Api;
    using AdvantageDataFixer.Models;

    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "TenantName,UserName,Password")] Token token)
        {
            bool isLogged = false;
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(token.TenantName))
                {
                    ViewBag.ErroMessage = "Tenant name is required";
                }
                else if (string.IsNullOrEmpty(token.UserName))
                {
                    ViewBag.ErroMessage = "User name is required";
                }
                else if (string.IsNullOrEmpty(token.Password))
                {
                    ViewBag.ErroMessage = "Password is required";
                }
                else
                {
                    TenantAuthDb tenantAuthDb = new TenantAuthDb();
                    aspnet_Users user = tenantAuthDb.aspnet_Users.FirstOrDefault(
                        x => x.UserName.Equals(token.UserName.Trim(), StringComparison.InvariantCultureIgnoreCase)
                             && x.aspnet_Membership.IsApproved == true
                             && x.aspnet_Membership.IsLockedOut == false
                             && x.TenantUsers.Any(y => y.IsSupportUser == true && y.Tenant.TenantName.Equals(token.TenantName.Trim(), StringComparison.InvariantCultureIgnoreCase)));
                    if (user != null)
                    {
                        var passwordBytes = Encoding.Unicode.GetBytes(token.Password);
                        var saltBytes = Convert.FromBase64String(user.aspnet_Membership.PasswordSalt);
                        var combinedBytes = new byte[saltBytes.Length + passwordBytes.Length];

                        Buffer.BlockCopy(saltBytes, 0, combinedBytes, 0, saltBytes.Length);
                        Buffer.BlockCopy(passwordBytes, 0, combinedBytes, saltBytes.Length, passwordBytes.Length);

                        var sha1 = SHA1.Create();
                        var hash = sha1.ComputeHash(combinedBytes);

                        var hashedPassword = Convert.ToBase64String(hash);

                        aspnet_Membership membership =
                            tenantAuthDb.aspnet_Membership.FirstOrDefault(x => x.UserId == user.UserId);

                        if (hashedPassword.Equals(membership.Password))
                        {
                            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                                1, user.UserName, DateTime.Now, DateTime.Now.AddMinutes(20), false, user.UserId.ToString()
                            );

                            string enTicket = FormsAuthentication.Encrypt(authTicket);
                            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, enTicket);
                            cookie.Path = FormsAuthentication.FormsCookiePath;
                            Response.Cookies.Add(cookie);

                            Tenant tenant = tenantAuthDb.Tenants
                                .Where(
                                    x => x.TenantName.Equals(
                                        token.TenantName.Trim(),
                                        StringComparison.InvariantCultureIgnoreCase)
                                    && x.TenantUsers.Any(y=> y.UserId == user.UserId))
                                .FirstOrDefault();

                            if (tenant != null)
                            {
                                isLogged = true;

                                Session["AdvantageDbConnectionString"] = $"server={tenant.ServerName};Database={tenant.DatabaseName};Uid={tenant.UserName};password={tenant.Password};MultipleActiveResultSets=True;App=EntityFramework";
                            }
                        }
                    }
                }

            }

            if (isLogged)
            {
                return Redirect("Index");
            }
            else
            {
                ViewBag.ErroMessage = "Invalid User Account! Confirm the User is a support user with access to the tenant and that the tenant name is correct.";
                return View();
            }
        }

        


    }
}