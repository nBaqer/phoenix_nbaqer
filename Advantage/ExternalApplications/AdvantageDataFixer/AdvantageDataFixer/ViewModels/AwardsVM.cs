﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvantageDataFixer.ViewModels
{
    public class AwardsVM
    {
        public decimal GrossAmount { get; set; }

        public decimal LoanFees { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int NumberOfDisbursements { get; set; }

        public string LoanId { get; set; }

        public string FAId { get; set; }

        public string LenderCode { get; set; }

        public string ServicerCode { get; set; }

        public string GuarantorCode { get; set; }

        public string AcademicYearCode { get; set; }

        public string FundSourceCode { get; set; }
        public decimal NetAmount { get; set; }

        public string Term { get; set; }
        
    }
}