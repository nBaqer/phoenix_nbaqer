﻿using System;

namespace AdvantageDataFixer.ViewModels
{
    public class TransactionVM
    {
        public DateTime Date { get; set; }
        public string Reference { get; set; }
        public string DocumentId { get; set; }
        public string TransCode { get; set; }
        public string Description { get; set; }
        public string AcademicYear { get; set; }
        public string Term { get; set; }
        public string TransType { get; set; }
        public string User { get; set; }
        public string Amount { get; set; }
        public string Balance { get; set; }
        public string PaymentPeriod { get; set; }
        public string TransactionId { get; set; }
        public string StuEnrollId { get; set; }
        
        public bool HasPaymentTiedToTransaction { get; set; }

        public int? PaymentTypeId { get; set; }

        //public Guid TransactionId { get; set; }
        //public Guid StuEnrollId { get; set; }
        //public Nullable<Guid> TermId { get; set; }
        //public Nullable<Guid> CampusId { get; set; }
        //public Nullable<Guid> TransCodeId { get; set; }
        //public Nullable<Guid> AcademicYearId { get; set; }
        //public string TransDescrip { get; set; }
        //public string AcademicYearDescrip { get; set; }
        //public decimal TransAmount { get; set; }
        //public int TransTypeId { get; set; }
        //public bool IsPosted { get; set; }
        //public Nullable<DateTime> CreateDate { get; set; }
        //public Nullable<Guid> BatchPaymentId { get; set; }
        //public Nullable<int> ViewOrder { get; set; }
        //public bool IsAutomatic { get; set; }
        //public string ModUser { get; set; }
        //public Nullable<DateTime> ModDate { get; set; }
        //public bool Voided { get; set; }
        //public Nullable<byte> FeeLevelId { get; set; }
        //public Nullable<Guid> FeeId { get; set; }
        //public Nullable<Guid> PaymentCodeId { get; set; }
        //public Nullable<Guid> FundSourceId { get; set; }
        //public Nullable<Guid> StuEnrollPayPeriodId { get; set; }
        //public Nullable<int> DisplaySequence { get; set; }
        //public Nullable<int> SecondDisplaySequence { get; set; }
        //public Nullable<Guid> PmtPeriodId { get; set; }
        //public Nullable<Guid> PrgChrPeriodSeqId { get; set; }
        //public Nullable<short> PaymentPeriodNumber { get; set; }

        //public virtual arPrgChargePeriodSeq arPrgChargePeriodSeq { get; set; }
        //public virtual arStuEnrollment arStuEnrollment { get; set; }
        //public virtual arTerm arTerm { get; set; }
        //public virtual saAcademicYear saAcademicYear { get; set; }
        //public virtual saBatchPayment saBatchPayment { get; set; }
        //[Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<saDeferredRevenue> saDeferredRevenues { get; set; }
        //public virtual saFundSource saFundSource { get; set; }
        //[Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<saGLDistribution> saGLDistributions { get; set; }
        //public virtual saPayment saPayment { get; set; }
        //public virtual saPmtPeriod saPmtPeriod { get; set; }
        //public virtual saRefund saRefund { get; set; }
        //public virtual saTransCode saTransCode { get; set; }
        //public virtual saTransCode saTransCode1 { get; set; }
        //public virtual saTransType saTransType { get; set; }
    }
}