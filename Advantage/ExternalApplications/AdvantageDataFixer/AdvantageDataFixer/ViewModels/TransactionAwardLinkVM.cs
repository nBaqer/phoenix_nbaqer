﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvantageDataFixer.ViewModels
{
    public class TransactionAwardLinkVM
    {
        public Guid EnrollmentId { get; set; }

        public string TransactionType { get; set; }

        public Guid? StudentAwardId { get; set; }

        public Guid? StudentAwardScheduleId { get; set; }

        public Guid TransactionId { get; set; }
    }
}