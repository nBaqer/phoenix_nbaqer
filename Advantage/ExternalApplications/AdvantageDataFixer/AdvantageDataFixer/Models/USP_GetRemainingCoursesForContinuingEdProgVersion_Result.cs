//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_GetRemainingCoursesForContinuingEdProgVersion_Result
    {
        public System.Guid StuEnrollId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Nullable<System.DateTime> ExpGradDate { get; set; }
        public System.Guid PrgVerId { get; set; }
        public string PrgVerDescrip { get; set; }
        public System.Guid ReqId { get; set; }
        public string Req { get; set; }
        public string Code { get; set; }
        public Nullable<decimal> Credits { get; set; }
        public Nullable<decimal> Hours { get; set; }
        public string IsRequired { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public System.DateTime ClassStartDate { get; set; }
        public System.DateTime ClassEndDate { get; set; }
        public Nullable<System.DateTime> ExpStartDate { get; set; }
        public string CampDescrip { get; set; }
        public bool AllowCompletedCourseRetake { get; set; }
        public System.Guid ClsSectionId { get; set; }
    }
}
