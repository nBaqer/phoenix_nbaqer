//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syAdvBuild
    {
        public string BuildNumber { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public System.Guid AdvantageBuildId { get; set; }
    }
}
