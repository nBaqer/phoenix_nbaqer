//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GradeWeightsView
    {
        public System.Guid InstrGrdBkWgtId { get; set; }
        public string Code { get; set; }
        public string Descrip { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<int> Seq { get; set; }
        public Nullable<System.Guid> InstructorId { get; set; }
        public string Expr1 { get; set; }
    }
}
