//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_GetFERPAPages_Result
    {
        public System.Guid FERPAPageId { get; set; }
        public System.Guid FERPACategoryId { get; set; }
        public short ResourceId { get; set; }
    }
}
