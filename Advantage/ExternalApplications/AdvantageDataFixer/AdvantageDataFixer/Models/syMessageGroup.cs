//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syMessageGroup
    {
        public System.Guid MessageGroupId { get; set; }
        public System.Guid StatusId { get; set; }
        public System.Guid CampGrpId { get; set; }
        public string GroupDescrip { get; set; }
        public System.Guid MessageSchemaId { get; set; }
        public string XmlData { get; set; }
        public System.DateTime ModDate { get; set; }
        public string ModUser { get; set; }
    
        public virtual syCampGrp syCampGrp { get; set; }
        public virtual syMessageSchema syMessageSchema { get; set; }
        public virtual syStatus syStatus { get; set; }
    }
}
