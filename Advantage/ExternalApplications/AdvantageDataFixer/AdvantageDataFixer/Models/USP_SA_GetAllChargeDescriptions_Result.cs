//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_SA_GetAllChargeDescriptions_Result
    {
        public System.Guid TransCodeId { get; set; }
        public string TransCodeCode { get; set; }
        public string TransCodeDescrip { get; set; }
        public System.Guid StatusId { get; set; }
        public System.Guid CampGrpId { get; set; }
        public System.Guid StatusId1 { get; set; }
        public string Status { get; set; }
    }
}
