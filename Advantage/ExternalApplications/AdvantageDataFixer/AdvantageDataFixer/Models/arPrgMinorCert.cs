//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arPrgMinorCert
    {
        public System.Guid PrgMinorCertId { get; set; }
        public System.Guid ParentId { get; set; }
        public System.Guid ChildId { get; set; }
    
        public virtual arPrgVersion arPrgVersion { get; set; }
        public virtual arPrgVersion arPrgVersion1 { get; set; }
    }
}
