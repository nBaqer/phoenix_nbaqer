//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adLeadOtherContactsEmail
    {
        public System.Guid OtherContactsEmailId { get; set; }
        public System.Guid OtherContactId { get; set; }
        public System.Guid LeadId { get; set; }
        public string EMail { get; set; }
        public System.Guid EMailTypeId { get; set; }
        public string ModUser { get; set; }
        public System.DateTime ModDate { get; set; }
        public Nullable<System.Guid> StatusId { get; set; }
    
        public virtual adLeadOtherContact adLeadOtherContact { get; set; }
        public virtual adLead adLead { get; set; }
        public virtual syEmailType syEmailType { get; set; }
        public virtual syStatus syStatus { get; set; }
    }
}
