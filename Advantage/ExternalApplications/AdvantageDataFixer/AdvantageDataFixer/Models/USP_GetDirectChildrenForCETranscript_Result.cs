//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_GetDirectChildrenForCETranscript_Result
    {
        public System.Guid ReqId { get; set; }
        public string Req { get; set; }
        public string Code { get; set; }
        public Nullable<decimal> Credits { get; set; }
        public Nullable<decimal> Hours { get; set; }
        public short ReqTypeId { get; set; }
        public int DefCredits { get; set; }
        public int ProgCredits { get; set; }
        public int ProgHours { get; set; }
        public string PrgVerDescrip { get; set; }
        public Nullable<bool> IsContinuingEd { get; set; }
        public System.Guid PrgVerId { get; set; }
        public Nullable<decimal> FinAidCredits { get; set; }
        public System.Guid StuEnrollId { get; set; }
        public Nullable<decimal> ScheduledHours { get; set; }
    }
}
