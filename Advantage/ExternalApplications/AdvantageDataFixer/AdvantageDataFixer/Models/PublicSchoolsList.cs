//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PublicSchoolsList
    {
        public string Nces_Id { get; set; }
        public string School_Level { get; set; }
        public string School_District { get; set; }
        public string School_Name { get; set; }
        public string School_County { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string C_School_Type_ { get; set; }
        public string C_State_Fips_ { get; set; }
        public string C_Low_Grade_ { get; set; }
        public string High_Grade { get; set; }
        public string C_Students_Ug_ { get; set; }
        public string C_Students_Prek_ { get; set; }
        public string C_Students_K_ { get; set; }
        public string C_Students_1_ { get; set; }
        public string C_Students_2_ { get; set; }
        public string C_Students_3_ { get; set; }
        public string C_Students_4_ { get; set; }
        public string C_Students_5_ { get; set; }
        public string C_Students_6_ { get; set; }
        public string C_Students_7_ { get; set; }
        public string C_Students_8_ { get; set; }
        public string C_Students_9_ { get; set; }
        public string C_Students_10_ { get; set; }
        public string C_Students_11_ { get; set; }
        public string C_Students_12_ { get; set; }
        public string C_Operational_Status_ { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string C_State_School_Id_ { get; set; }
        public string C_State_District_Id_ { get; set; }
        public string C_Total_Male_ { get; set; }
        public string C_Total_Female_ { get; set; }
        public string C_Total_American_Indian_Alaskan_ { get; set; }
        public string C_Total_Asian_ { get; set; }
        public string C_Total_Black_ { get; set; }
        public string C_Total_Hispanic_ { get; set; }
        public string C_Total_White_ { get; set; }
        public string C_Free_Lunch_Eligible_ { get; set; }
        public string C_Reduce_Price_Lunch_Eligible_ { get; set; }
        public string C_Free___Reduced_Lunch_Total_ { get; set; }
        public string C_Student_Teacher_Ratio_ { get; set; }
        public string C_Full_Time_Equivalent_Teachers_ { get; set; }
        public string C_Local_Education_Agency_Id_ { get; set; }
        public string C_Local_School_Id_ { get; set; }
        public string C_Mailing_Address_ { get; set; }
        public string C_Mailing_City_ { get; set; }
        public string C_Mailing_State_ { get; set; }
        public string C_Mailing_Zip_ { get; set; }
        public string C_Mailing_Zip4_ { get; set; }
        public string Zip4 { get; set; }
        public string C_Union_Identification_Number_ { get; set; }
        public string C_Urban_Locale_Code_ { get; set; }
        public string C_Ansi_Fips_County_Number_ { get; set; }
        public string C_District_Code_113_ { get; set; }
        public string C_District_Code_112_ { get; set; }
        public string C_Bureau_Of_Indian_ { get; set; }
        public string C_Offered_Ug_ { get; set; }
        public string C_Offered_Prek_ { get; set; }
        public string C_Offered_K_ { get; set; }
        public string C_Offered_1_ { get; set; }
        public string C_Offered_2_ { get; set; }
        public string C_Offered_3_ { get; set; }
        public string C_Offered_4_ { get; set; }
        public string C_Offered_5_ { get; set; }
        public string C_Offered_6_ { get; set; }
        public string C_Offered_7_ { get; set; }
        public string C_Offered_8_ { get; set; }
        public string C_Offered_9_ { get; set; }
        public string C_Offered_10_ { get; set; }
        public string C_Offered_11_ { get; set; }
        public string C_Offered_12_ { get; set; }
        public string C_Title_1_Status_ { get; set; }
        public string C_Title_1_Eligible_ { get; set; }
        public string C_Title_1_School_Wide_ { get; set; }
        public string C_Magnet_School_ { get; set; }
        public string C_Charter_School_ { get; set; }
        public string C_Shared_Time_School_ { get; set; }
        public string C_Count_Free_Lunch_ { get; set; }
        public string C_Count_Reduced_Lunch_ { get; set; }
        public string C_Total_Free___Reduced_Lunch_ { get; set; }
        public string C_Total_Pacific_ { get; set; }
        public string C_Total_2race_ { get; set; }
        public string C_Total_Students_ { get; set; }
        public int InstId { get; set; }
        public Nullable<System.Guid> InstitutionId { get; set; }
    }
}
