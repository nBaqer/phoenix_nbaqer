//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adSourceCatagory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public adSourceCatagory()
        {
            this.adLeads = new HashSet<adLead>();
            this.adSourceTypes = new HashSet<adSourceType>();
        }
    
        public string SourceCatagoryDescrip { get; set; }
        public System.Guid StatusID { get; set; }
        public string SourceCatagoryCode { get; set; }
        public System.Guid SourceCatagoryId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public Nullable<System.Guid> CampGrpId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adLead> adLeads { get; set; }
        public virtual syCampGrp syCampGrp { get; set; }
        public virtual syStatus syStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adSourceType> adSourceTypes { get; set; }
    }
}
