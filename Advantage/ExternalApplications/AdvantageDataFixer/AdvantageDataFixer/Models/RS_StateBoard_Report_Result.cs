//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class RS_StateBoard_Report_Result
    {
        public string Last { get; set; }
        public string First { get; set; }
        public string Last_4_of_SS_ { get; set; }
        public System.DateTime EnrollDate { get; set; }
        public string TimeType { get; set; }
        public Nullable<decimal> AttendanceThisMonth { get; set; }
        public Nullable<decimal> AttendancePastMonth { get; set; }
        public Nullable<decimal> AttendanceTotal { get; set; }
        public string CourseStudiedDescription { get; set; }
        public string Course_of_Study { get; set; }
    }
}
