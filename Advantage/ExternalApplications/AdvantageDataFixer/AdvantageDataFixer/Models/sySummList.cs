//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class sySummList
    {
        public short SummListId { get; set; }
        public string SummListName { get; set; }
        public short TblId { get; set; }
        public short DispFldId { get; set; }
        public short ValFldId { get; set; }
    }
}
