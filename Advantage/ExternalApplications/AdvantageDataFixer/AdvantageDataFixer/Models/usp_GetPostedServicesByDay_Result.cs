//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_GetPostedServicesByDay_Result
    {
        public System.Guid GrdBkResultId { get; set; }
        public System.Guid StuEnrollId { get; set; }
        public System.Guid ClsSectionId { get; set; }
        public System.Guid InstrGrdBkWgtDetailId { get; set; }
        public Nullable<decimal> Score { get; set; }
        public string Comments { get; set; }
        public string ModUser { get; set; }
        public System.DateTime ModDate { get; set; }
        public int ResNum { get; set; }
        public Nullable<System.DateTime> PostDate { get; set; }
        public Nullable<bool> IsCompGraded { get; set; }
        public Nullable<bool> isCourseCredited { get; set; }
    }
}
