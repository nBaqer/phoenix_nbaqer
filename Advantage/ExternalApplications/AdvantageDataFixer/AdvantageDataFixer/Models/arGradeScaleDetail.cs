//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arGradeScaleDetail
    {
        public System.Guid GrdScaleDetailId { get; set; }
        public System.Guid GrdScaleId { get; set; }
        public short MinVal { get; set; }
        public short MaxVal { get; set; }
        public System.Guid GrdSysDetailId { get; set; }
        public int ViewOrder { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
    
        public virtual arGradeScale arGradeScale { get; set; }
        public virtual arGradeSystemDetail arGradeSystemDetail { get; set; }
    }
}
