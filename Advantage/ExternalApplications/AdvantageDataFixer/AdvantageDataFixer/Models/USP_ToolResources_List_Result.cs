//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_ToolResources_List_Result
    {
        public int ModuleResourceId { get; set; }
        public Nullable<short> ChildResourceId { get; set; }
        public string ChildResource { get; set; }
        public string ChildResourceURL { get; set; }
        public Nullable<short> ParentResourceId { get; set; }
        public string ParentResource { get; set; }
        public int FirstSortOrder { get; set; }
        public int DisplaySequence { get; set; }
    }
}
