//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_IPEDS_FALL_DisabilityService_Result
    {
        public string StudentIdentifier { get; set; }
        public string StudentName { get; set; }
        public string DisabledStudent { get; set; }
        public int IPEDSSequence { get; set; }
    }
}
