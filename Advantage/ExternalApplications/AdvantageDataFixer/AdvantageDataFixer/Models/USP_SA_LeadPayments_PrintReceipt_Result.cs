//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_SA_LeadPayments_PrintReceipt_Result
    {
        public string SchoolName { get; set; }
        public string SchoolAddress1 { get; set; }
        public string SchoolAddress2 { get; set; }
        public string SchoolCity { get; set; }
        public string SchoolState { get; set; }
        public string SchoolZip { get; set; }
        public string SchoolCountry { get; set; }
        public string LeadName { get; set; }
        public string LeadAddress1 { get; set; }
        public string LeadAddress2 { get; set; }
        public string LeadCity { get; set; }
        public string LeadState { get; set; }
        public string LeadZip { get; set; }
        public string LeadCountry { get; set; }
        public string TransDate { get; set; }
        public string TransDesc { get; set; }
        public string PaymentTypeDesc { get; set; }
        public string CheckNumber { get; set; }
        public Nullable<decimal> TransAmount { get; set; }
        public Nullable<bool> Voided { get; set; }
    }
}
