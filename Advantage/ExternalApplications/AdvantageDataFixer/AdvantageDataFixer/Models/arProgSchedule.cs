//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arProgSchedule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public arProgSchedule()
        {
            this.adLeads = new HashSet<adLead>();
            this.arProgScheduleDetails = new HashSet<arProgScheduleDetail>();
            this.arStudentSchedules = new HashSet<arStudentSchedule>();
        }
    
        public System.Guid ScheduleId { get; set; }
        public string Code { get; set; }
        public string Descrip { get; set; }
        public Nullable<System.Guid> PrgVerId { get; set; }
        public Nullable<bool> Active { get; set; }
        public Nullable<bool> UseFlexTime { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adLead> adLeads { get; set; }
        public virtual arPrgVersion arPrgVersion { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<arProgScheduleDetail> arProgScheduleDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<arStudentSchedule> arStudentSchedules { get; set; }
    }
}
