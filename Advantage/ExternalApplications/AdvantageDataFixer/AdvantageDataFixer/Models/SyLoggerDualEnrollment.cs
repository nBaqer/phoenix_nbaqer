//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SyLoggerDualEnrollment
    {
        public long LoggerId { get; set; }
        public string StudentId { get; set; }
        public string FullName { get; set; }
        public string ClassId { get; set; }
        public string ClassSection { get; set; }
        public string RequirementCode { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int Classification { get; set; }
        public string ModUser { get; set; }
        public System.DateTime ModDate { get; set; }
    }
}
