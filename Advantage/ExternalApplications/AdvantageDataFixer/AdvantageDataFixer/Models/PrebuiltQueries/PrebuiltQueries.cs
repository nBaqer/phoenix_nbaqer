﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvantageDataFixer.Models.PrebuiltQueries
{
    public static class PrebuiltQueries
    {

        public static IQueryable<arStuEnrollment> GetEnrollmentsIdsByStudentNumber(this AdvantageDb db, string leadId)
        {
            if (leadId == null || leadId.Trim() == "")
                throw new Exception("studentNumber cannot be null");

            var data = (from e in db.arStuEnrollments
                        where e.LeadId.HasValue && e.LeadId.ToString() == leadId
                        select e);

            return data;
        }

        public static IQueryable<adLead> GetLeadFromTransactionId(this AdvantageDb db, string transactionId)
        {
            if (transactionId == null || transactionId.Trim() == "")
                throw new Exception("transactionId cannot be null");

            var data = (from a in db.adLeads
                        join b in db.arStuEnrollments on a.LeadId equals b.LeadId
                        join c in db.saTransactions on b.StuEnrollId equals c.StuEnrollId
                        where  c.TransactionId.ToString() == transactionId
                        select a);

            return data;
        }
    }
}