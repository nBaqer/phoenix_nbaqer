//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_GetPendingDocsbyStudentandPrgVersion_Result
    {
        public Nullable<System.Guid> DocumentId { get; set; }
        public string DocumentDescrip { get; set; }
        public System.Guid ReqGrpId { get; set; }
        public string ReqGrpDescrip { get; set; }
        public Nullable<int> NumRequired { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int Required { get; set; }
        public Nullable<int> DocSubmittedCount { get; set; }
        public Nullable<bool> ReqforEnrollment { get; set; }
        public Nullable<bool> ReqforFinancialAid { get; set; }
        public Nullable<bool> ReqforGraduation { get; set; }
        public string DocStatusDescrip { get; set; }
    }
}
