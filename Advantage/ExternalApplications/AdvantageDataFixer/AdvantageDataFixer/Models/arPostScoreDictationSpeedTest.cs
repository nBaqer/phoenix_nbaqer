//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arPostScoreDictationSpeedTest
    {
        public System.Guid id { get; set; }
        public Nullable<System.Guid> stuenrollid { get; set; }
        public Nullable<System.Guid> termid { get; set; }
        public Nullable<System.DateTime> datepassed { get; set; }
        public Nullable<System.Guid> grdcomponenttypeid { get; set; }
        public Nullable<decimal> accuracy { get; set; }
        public Nullable<decimal> speed { get; set; }
        public Nullable<System.Guid> GrdSysDetailId { get; set; }
        public Nullable<System.Guid> InstructorId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string istestmentorproctored { get; set; }
    }
}
