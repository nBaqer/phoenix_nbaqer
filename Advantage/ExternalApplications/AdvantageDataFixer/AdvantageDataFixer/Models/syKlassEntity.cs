//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syKlassEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public syKlassEntity()
        {
            this.syKlassAppConfigurationSettings = new HashSet<syKlassAppConfigurationSetting>();
        }
    
        public int KlassEntityId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<syKlassAppConfigurationSetting> syKlassAppConfigurationSettings { get; set; }
    }
}
