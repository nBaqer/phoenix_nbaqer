//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adLeadEducation
    {
        public System.Guid LeadEducationId { get; set; }
        public System.Guid LeadId { get; set; }
        public System.Guid EducationInstId { get; set; }
        public string EducationInstType { get; set; }
        public Nullable<System.DateTime> GraduatedDate { get; set; }
        public string FinalGrade { get; set; }
        public Nullable<System.Guid> CertificateId { get; set; }
        public string Comments { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string Major { get; set; }
        public System.Guid StatusId { get; set; }
        public bool Graduate { get; set; }
        public Nullable<decimal> GPA { get; set; }
        public Nullable<int> Rank { get; set; }
        public Nullable<int> Percentile { get; set; }
        public string Certificate { get; set; }
    
        public virtual adLead adLead { get; set; }
        public virtual arDegree arDegree { get; set; }
        public virtual syStatus syStatus { get; set; }
    }
}
