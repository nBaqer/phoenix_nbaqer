//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class WapiTenantGetAdvantageOperationSettings_Result
    {
        public string AdvantageConnection { get; set; }
        public string Key { get; set; }
        public string ExternalCompanyCode { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
    }
}
