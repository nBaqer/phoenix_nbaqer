//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tmTask
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tmTask()
        {
            this.tmResultTasks = new HashSet<tmResultTask>();
            this.tmTaskResults = new HashSet<tmTaskResult>();
            this.tmUserTasks = new HashSet<tmUserTask>();
        }
    
        public System.Guid TaskId { get; set; }
        public string Code { get; set; }
        public string Descrip { get; set; }
        public Nullable<System.Guid> CampGroupId { get; set; }
        public Nullable<System.Guid> CategoryId { get; set; }
        public Nullable<int> ModuleEntityId { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public Nullable<System.Guid> ModUser { get; set; }
        public Nullable<byte> Active { get; set; }
        public Nullable<System.Guid> StatusCodeId { get; set; }
    
        public virtual syStatusCode syStatusCode { get; set; }
        public virtual tmCategory tmCategory { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmResultTask> tmResultTasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmTaskResult> tmTaskResults { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tmUserTask> tmUserTasks { get; set; }
    }
}
