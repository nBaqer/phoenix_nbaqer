//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class hrEmpHRInfo
    {
        public System.Guid EmpHRInfoId { get; set; }
        public System.Guid EmpId { get; set; }
        public Nullable<System.DateTime> HireDate { get; set; }
        public Nullable<System.Guid> PositionId { get; set; }
        public Nullable<System.Guid> DepartmentId { get; set; }
        public string ModUser { get; set; }
        public System.DateTime ModDate { get; set; }
    
        public virtual hrEmployee hrEmployee { get; set; }
        public virtual syHRDepartment syHRDepartment { get; set; }
        public virtual syPosition syPosition { get; set; }
    }
}
