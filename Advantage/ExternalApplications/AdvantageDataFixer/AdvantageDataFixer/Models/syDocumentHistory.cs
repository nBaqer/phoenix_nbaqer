//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syDocumentHistory
    {
        public System.Guid FileId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public string DocumentType { get; set; }
        public Nullable<System.Guid> StudentId { get; set; }
        public string FileNameOnly { get; set; }
        public Nullable<System.Guid> DocumentId { get; set; }
        public Nullable<int> ModuleId { get; set; }
        public Nullable<System.Guid> LeadId { get; set; }
        public string DisplayName { get; set; }
        public Nullable<bool> IsArchived { get; set; }
    
        public virtual adLead adLead { get; set; }
        public virtual adReq adReq { get; set; }
    }
}
