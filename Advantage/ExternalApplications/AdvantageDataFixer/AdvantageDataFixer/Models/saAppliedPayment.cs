//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class saAppliedPayment
    {
        public System.Guid AppliedPmtId { get; set; }
        public System.Guid TransactionId { get; set; }
        public System.Guid ApplyToTransId { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime ModDate { get; set; }
        public string ModUser { get; set; }
    }
}
