//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_SA_GetChargePeriodSequences_GetList_Result
    {
        public System.Guid PrgChrPeriodSeqId { get; set; }
        public System.Guid PrgVerId { get; set; }
        public byte FeeLevelID { get; set; }
        public short Sequence { get; set; }
        public decimal StartValue { get; set; }
        public decimal Endvalue { get; set; }
    }
}
