//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adSourceAdvertisement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public adSourceAdvertisement()
        {
            this.adLeads = new HashSet<adLead>();
        }
    
        public System.Guid SourceAdvId { get; set; }
        public string SourceAdvDescrip { get; set; }
        public System.DateTime startdate { get; set; }
        public System.DateTime enddate { get; set; }
        public decimal cost { get; set; }
        public System.Guid SourceTypeId { get; set; }
        public System.Guid AdvIntervalId { get; set; }
        public string SourceAdvCode { get; set; }
        public System.Guid StatusId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public Nullable<System.Guid> CampGrpId { get; set; }
    
        public virtual adAdvInterval adAdvInterval { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adLead> adLeads { get; set; }
        public virtual adSourceType adSourceType { get; set; }
        public virtual syCampGrp syCampGrp { get; set; }
        public virtual syStatus syStatus { get; set; }
    }
}
