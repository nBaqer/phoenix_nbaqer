//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_WeeklyAttendanceReport_Result
    {
        public Nullable<System.Guid> StuEnrollId { get; set; }
        public string StudentIdentifier { get; set; }
        public string StudentName { get; set; }
        public Nullable<decimal> Mon { get; set; }
        public Nullable<decimal> Tue { get; set; }
        public Nullable<decimal> Wed { get; set; }
        public Nullable<decimal> Thur { get; set; }
        public Nullable<decimal> Fri { get; set; }
        public Nullable<decimal> Sat { get; set; }
        public Nullable<decimal> Sun { get; set; }
        public Nullable<decimal> WeekTotal { get; set; }
        public Nullable<decimal> NewTotal { get; set; }
        public Nullable<decimal> NewTotalSched { get; set; }
        public Nullable<decimal> NewTotalDaysAbsent { get; set; }
        public Nullable<decimal> NewTotalMakeUpDays { get; set; }
        public Nullable<decimal> NewNetDaysAbsent { get; set; }
        public Nullable<decimal> NewSchedDaysatLDA { get; set; }
        public string LDA { get; set; }
        public Nullable<System.Guid> AttTypeID { get; set; }
        public string SuppressDate { get; set; }
        public System.Guid ClsSectionId { get; set; }
        public string ClsSection { get; set; }
        public string Descrip { get; set; }
        public string FullName { get; set; }
        public System.Guid TermId { get; set; }
        public string TermDescrip { get; set; }
        public System.Guid ReqId { get; set; }
        public string Code { get; set; }
        public Nullable<System.Guid> InstructorId { get; set; }
        public Nullable<System.Guid> shiftid { get; set; }
        public Nullable<System.Guid> StatusCodeId { get; set; }
        public string StatusCodeDescrip { get; set; }
    }
}
