//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_dictationspeedtestreport_getlist_Result
    {
        public string TermDescrip { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public System.Guid StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string SSN { get; set; }
        public string StudentNumber { get; set; }
        public string EnrollmentId { get; set; }
        public string Descrip { get; set; }
        public Nullable<decimal> Speed { get; set; }
        public Nullable<decimal> Accuracy { get; set; }
        public Nullable<System.DateTime> DatePassed { get; set; }
        public string InstructorName { get; set; }
        public string PrgVerDescrip { get; set; }
        public string campusdescrip { get; set; }
        public string campgrpdescrip { get; set; }
        public string StudentName { get; set; }
        public Nullable<int> StudentIdentifier { get; set; }
        public Nullable<int> StudentIdentifierCaption { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string StateDescrip { get; set; }
        public string PhoneNumber { get; set; }
        public string Zip { get; set; }
        public Nullable<bool> ForeignPhone { get; set; }
        public Nullable<bool> ForeignZip { get; set; }
        public string Grade { get; set; }
        public System.Guid GrdComponentTypeId { get; set; }
        public string istestmentorproctored { get; set; }
    }
}
