//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adLeadDocsReceived
    {
        public System.Guid LeadDocId { get; set; }
        public System.Guid LeadId { get; set; }
        public System.Guid DocumentId { get; set; }
        public Nullable<System.DateTime> ReceiveDate { get; set; }
        public Nullable<System.Guid> DocStatusId { get; set; }
        public System.DateTime ModDate { get; set; }
        public string ModUser { get; set; }
        public bool Override { get; set; }
        public string OverrideReason { get; set; }
        public Nullable<System.DateTime> ApprovalDate { get; set; }
    
        public virtual adLead adLead { get; set; }
        public virtual adReq adReq { get; set; }
        public virtual syDocStatus syDocStatus { get; set; }
    }
}
