//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class saGLAccount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public saGLAccount()
        {
            this.saGLDistributions = new HashSet<saGLDistribution>();
        }
    
        public System.Guid GLAccountId { get; set; }
        public System.Guid StatusId { get; set; }
        public string GLAccountCode { get; set; }
        public string GLAccountDescription { get; set; }
        public System.Guid CampGrpId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
    
        public virtual syCampGrp syCampGrp { get; set; }
        public virtual syStatus syStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<saGLDistribution> saGLDistributions { get; set; }
    }
}
