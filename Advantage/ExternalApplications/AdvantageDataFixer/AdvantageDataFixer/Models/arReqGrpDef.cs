//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arReqGrpDef
    {
        public System.Guid ReqGrpId { get; set; }
        public System.Guid GrpId { get; set; }
        public System.Guid ReqId { get; set; }
        public byte ReqSeq { get; set; }
        public bool IsRequired { get; set; }
        public Nullable<short> Cnt { get; set; }
        public Nullable<decimal> Hours { get; set; }
        public string ModUser { get; set; }
        public System.DateTime ModDate { get; set; }
    
        public virtual arReq arReq { get; set; }
        public virtual arReq arReq1 { get; set; }
    }
}
