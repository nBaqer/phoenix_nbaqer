//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_GetAttendancePercentageDTForReport_Result
    {
        public System.Guid ClsSectionId { get; set; }
        public Nullable<int> startTime { get; set; }
        public Nullable<int> endtime { get; set; }
        public System.DateTime MeetDate { get; set; }
        public decimal Actual { get; set; }
        public bool Tardy { get; set; }
        public bool Excused { get; set; }
        public string AttendanceType { get; set; }
        public Nullable<int> duration { get; set; }
        public Nullable<System.Guid> ClsSectMeetingId { get; set; }
        public Nullable<decimal> Scheduled { get; set; }
        public System.Guid InstructionTypeID { get; set; }
    }
}
