//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class RS_TranscriptIndividualStudentTerms_Result
    {
        public Nullable<int> TotalCourses { get; set; }
        public Nullable<int> TotalGpaCourses { get; set; }
        public Nullable<decimal> TotalCredits { get; set; }
        public Nullable<decimal> TotalPoints { get; set; }
        public Nullable<decimal> CGPA { get; set; }
        public Nullable<System.Guid> GrdSysDetailId { get; set; }
        public Nullable<System.DateTime> DropDate { get; set; }
        public string Grade { get; set; }
        public Nullable<bool> IsPass { get; set; }
        public Nullable<decimal> GPA { get; set; }
        public Nullable<bool> IsCreditsAttempted { get; set; }
        public Nullable<bool> IsCreditsEarned { get; set; }
        public Nullable<bool> IsInGPA { get; set; }
        public Nullable<bool> IsDrop { get; set; }
        public Nullable<System.Guid> TermId { get; set; }
        public Nullable<System.DateTime> ClassStartDate { get; set; }
        public Nullable<System.DateTime> ClassEndDate { get; set; }
        public string TermDescrip { get; set; }
        public string DescripXTranscript { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Code { get; set; }
        public string Descrip { get; set; }
        public Nullable<decimal> Credits { get; set; }
        public Nullable<decimal> Hours { get; set; }
        public Nullable<decimal> ScheduledHours { get; set; }
        public Nullable<System.Guid> CourseCategoryId { get; set; }
        public Nullable<decimal> FinAidCredits { get; set; }
        public string CourseCategory { get; set; }
        public Nullable<bool> IsTransferGrade { get; set; }
        public Nullable<System.DateTime> DateIssue { get; set; }
        public Nullable<int> seq { get; set; }
    }
}
