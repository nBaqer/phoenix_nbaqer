//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_ComputeWithCreditsPerService_Result
    {
        public Nullable<decimal> LabsCompletedByStudent { get; set; }
        public Nullable<bool> isClinicsSatisfied { get; set; }
        public System.Guid InstrGrdBkWgtDetailId { get; set; }
        public Nullable<decimal> RequiredNumberofLabs { get; set; }
        public Nullable<decimal> CreditsPerService { get; set; }
    }
}
