//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syVersionHistory
    {
        public int Id { get; set; }
        public Nullable<int> Major { get; set; }
        public Nullable<int> Minor { get; set; }
        public Nullable<int> Build { get; set; }
        public Nullable<int> Revision { get; set; }
        public Nullable<System.DateTime> VersionBegin { get; set; }
        public Nullable<System.DateTime> VersionEnd { get; set; }
        public string Description { get; set; }
        public string ModUser { get; set; }
    }
}
