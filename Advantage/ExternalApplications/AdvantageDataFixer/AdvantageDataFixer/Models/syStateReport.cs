//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syStateReport
    {
        public System.Guid StateReportId { get; set; }
        public System.Guid StateId { get; set; }
        public System.Guid ReportId { get; set; }
        public string ModifiedUser { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual syReport syReport { get; set; }
        public virtual syState syState { get; set; }
    }
}
