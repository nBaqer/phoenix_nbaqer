//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class syRolesModule
    {
        public System.Guid RoleModuleId { get; set; }
        public Nullable<System.Guid> RoleId { get; set; }
        public Nullable<short> ModuleId { get; set; }
    
        public virtual syResource syResource { get; set; }
    }
}
