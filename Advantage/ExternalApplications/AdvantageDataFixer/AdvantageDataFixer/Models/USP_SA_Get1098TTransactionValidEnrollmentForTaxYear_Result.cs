//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class USP_SA_Get1098TTransactionValidEnrollmentForTaxYear_Result
    {
        public System.DateTime TransDate { get; set; }
        public decimal TransAmount { get; set; }
        public string TransCodeCode { get; set; }
        public string EnrollmentId { get; set; }
        public Nullable<int> PayType { get; set; }
    }
}
