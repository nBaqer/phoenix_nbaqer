//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class saRefund
    {
        public System.Guid TransactionId { get; set; }
        public int RefundTypeId { get; set; }
        public Nullable<System.Guid> BankAcctId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public Nullable<System.Guid> FundSourceId { get; set; }
        public Nullable<System.Guid> StudentAwardId { get; set; }
        public bool IsInstCharge { get; set; }
        public Nullable<System.Guid> AwardScheduleId { get; set; }
        public Nullable<decimal> RefundAmount { get; set; }
    
        public virtual saBankAccount saBankAccount { get; set; }
        public virtual saFundSource saFundSource { get; set; }
        public virtual saTransaction saTransaction { get; set; }
    }
}
