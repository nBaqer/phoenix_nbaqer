//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_GetGBWResultsForCourseLevel_Result
    {
        public System.Guid InstrGrdBkWgtDetailId { get; set; }
        public Nullable<decimal> Score { get; set; }
        public int ResNum { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<int> GrdPolicyId { get; set; }
        public Nullable<int> Parameter { get; set; }
        public Nullable<System.DateTime> PostDate { get; set; }
    }
}
