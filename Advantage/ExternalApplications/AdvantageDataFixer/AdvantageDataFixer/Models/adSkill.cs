//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class adSkill
    {
        public int SkillId { get; set; }
        public System.Guid LeadId { get; set; }
        public int LevelId { get; set; }
        public System.Guid SkillGrpId { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public System.DateTime ModDate { get; set; }
        public string ModUser { get; set; }
    
        public virtual adLead adLead { get; set; }
        public virtual adLevel adLevel { get; set; }
        public virtual plSkillGroup plSkillGroup { get; set; }
    }
}
