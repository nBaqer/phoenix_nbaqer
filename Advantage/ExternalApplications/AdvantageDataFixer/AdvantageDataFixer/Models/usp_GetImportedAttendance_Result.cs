//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    
    public partial class usp_GetImportedAttendance_Result
    {
        public decimal Actual { get; set; }
        public decimal Scheduled { get; set; }
        public Nullable<decimal> Absent { get; set; }
        public Nullable<decimal> MakeUp { get; set; }
    }
}
