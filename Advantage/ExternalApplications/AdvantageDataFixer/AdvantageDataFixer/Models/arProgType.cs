//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AdvantageDataFixer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class arProgType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public arProgType()
        {
            this.arPrgVersions = new HashSet<arPrgVersion>();
            this.saPeriodicFees = new HashSet<saPeriodicFee>();
        }
    
        public System.Guid ProgTypId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public System.Guid StatusId { get; set; }
        public Nullable<System.Guid> CampGrpId { get; set; }
        public string ModUser { get; set; }
        public Nullable<System.DateTime> ModDate { get; set; }
        public Nullable<int> IPEDSSequence { get; set; }
        public Nullable<int> IPEDSValue { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<arPrgVersion> arPrgVersions { get; set; }
        public virtual syCampGrp syCampGrp { get; set; }
        public virtual syStatus syStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<saPeriodicFee> saPeriodicFees { get; set; }
    }
}
