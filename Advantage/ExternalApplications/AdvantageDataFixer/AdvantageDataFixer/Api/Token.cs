﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdvantageDataFixer.Api
{
    public class Token
    {
        public string TenantName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}