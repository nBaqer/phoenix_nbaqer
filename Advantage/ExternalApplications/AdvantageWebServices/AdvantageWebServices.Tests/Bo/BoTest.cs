﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BoTest.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
// JAGG  Defines the BoTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageWebServices.Tests.Bo
{
    using System;
    using AdvantageWebBusiness;
    using CommonEntities;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///  The BO test.
    /// </summary>
    [TestClass]
    public class BoTest
    {
        /// <summary>
        /// The send email test.
        /// </summary>
        [TestMethod]
        public void SendEmailTest()
        {
            var bo = new T1098Bo();
            var config = new SchoolConfiguration
            {
                SchoolId = "TESTID",
                Year = "2015"
            };

            var smtp = new SmtpConfiguration
            {
                SmtpHost = string.Empty,
                SmtpPort = "587",
                Recipients = "jguirado@fameinc.com",
                User = string.Empty, // ""advportaluser1@gmail.com",
                Password = string.Empty
            };
            try
            {
                bo.SendEmail(config, smtp);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Assert.Fail(ex.Message);
            }
        }
    }
}
