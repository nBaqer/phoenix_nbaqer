﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098ControllerTest.cs" company="FAME">
//   2015,2016
// </copyright>
// <summary>
//  JAGG - Defines the T1098ControllerTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AdvantageWebServices.Tests.Controllers
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Net.Http;

    using CommonEntities;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    ///  The T1098 controller test.
    /// </summary>
    [TestClass]
    public class T1098ControllerTest
    {
        /// <summary>
        ///  The service layer 1098T post.
        /// </summary>
        private readonly string serviceLayer1098TPost;

        /// <summary>
        /// Initializes a new instance of the <see cref="T1098ControllerTest"/> class.
        /// </summary>
        public T1098ControllerTest()
        {
            this.serviceLayer1098TPost = ConfigurationManager.AppSettings.Get("SERVICE_LAYER_1098T_POST");
        }

        /// <summary>
        /// The post with simulate info test.
        /// </summary>
        [TestMethod]
        public void PostWithSimulateInfoTest()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("SCHOOL_TOKEN", "00000000-0000-0000-0000-0000000000000");
            
            var payload = Fame1098InputModel.Factory("2015");
           
            var task = client.PostAsJsonAsync(this.serviceLayer1098TPost, payload).ContinueWith(taskwithmsg =>
            {
                var response = taskwithmsg.Result;
                Console.WriteLine(response.ReasonPhrase);
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
           });
            task.Wait();
        }
    }
}
