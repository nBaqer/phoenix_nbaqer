﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArchiveDto.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
// JAGG -  DTO Archive Table to be exported to JET 4.0
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommonEntities.DtoConvertion
{
    using System;

    /// <summary>
    /// DTO Archive Table to be exported to JET 4.0
    /// </summary>
    public class ArchiveDto
    {
        /// <summary>
        /// Gets or sets the school id.
        /// </summary>
        public string SchoolId { get; set; }

        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the student enroll ID.
        /// </summary>
        public string StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the extracted date.
        /// </summary>
        public DateTime ExtractedDate { get; set; }

        /// <summary>
        /// Gets or sets the tax year.
        /// </summary>
        public string TaxYear { get; set; }
    }
}
