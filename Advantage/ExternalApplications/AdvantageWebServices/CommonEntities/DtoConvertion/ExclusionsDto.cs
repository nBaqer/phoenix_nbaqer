﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExclusionsDto.cs" company="FAME">
//   2015 2016
// </copyright>
// <summary>
// JAGG -  Exclusion DTO to convert to JET 4.0 table
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommonEntities.DtoConvertion
{
    /// <summary>
    /// Exclusion DTO to convert to JET 4.0 table
    /// </summary>
    public class ExclusionsDto
    {
        /// <summary>
        /// Gets or sets the student account number.
        /// The account number is composed by
        /// <code>kissCode:EnrollmentNumber</code>
        /// </summary>
        public string StuAcctNum { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the social security number.
        /// </summary>
        public string SocSecNum { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Addr1 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the exclusion code.
        /// </summary>
        public int ExclusionCode { get; set; }
    }
}
