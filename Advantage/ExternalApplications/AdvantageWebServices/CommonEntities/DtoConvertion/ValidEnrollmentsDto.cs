﻿namespace CommonEntities.DtoConvertion
{
    public class ValidEnrollmentsDto
    {
        public string StuAcctNum { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SocSecNum { get; set; }
        public string Addr1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int Line1Data { get; set; }
        public int Line3Data { get; set; }
        public int Line4Data { get; set; }
        public int Line5Data { get; set; }
        public int Line6Data { get; set; }
        public int RfndGRs { get; set; }
        public int RfndLNs { get; set; }
        public int RfndXXs { get; set; }
        public int PaidGRs { get; set; }
        public int PaidSPs { get; set; }
        public int PaidSHs { get; set; }
        public int PaidLNs { get; set; }
        public int GrossPaid { get; set; }
        public int XHrsPerWeek { get; set; }
        public bool Line8Data { get; set; }
        public string Corrected { get; set; }
        public bool LimitedByRemainingCost { get; set; }
        public int TotalCost { get; set; }
        public int PriorYrsPdAmt { get; set; }
        public int UnpaidCosts { get; set; }
    }
}
