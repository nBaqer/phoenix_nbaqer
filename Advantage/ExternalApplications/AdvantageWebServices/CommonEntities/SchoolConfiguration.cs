﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolConfiguration.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   JAGG - The school configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommonEntities
{
    /// <summary>
    ///  The school configuration.
    /// </summary>
    public class SchoolConfiguration
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///  Gets or sets Filename of the database to be create
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        ///  Gets or sets Folder to put the created database
        /// </summary>
        public string Folder { get; set; }

        /// <summary>
        ///  Gets or sets Year to TAX
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        ///  Gets or sets School token from the service (GUID) that send the school through the Web service
        /// </summary>
        public string SchoolToken { get; set; }

        /// <summary>
        ///  Gets or sets School <code> School_xxxx_xxxxxx</code>
        /// </summary>
        public string School { get; set; }

        /// <summary>
        /// Gets or sets School <code>KissKey xxxxx_KissKey_xxxxxxx</code>
        /// </summary>
        public string KissKey { get; set; }

        /// <summary>
        ///  Gets or sets School Id Code xxxx_xxxx_SchoolIdCode
        /// </summary>
        public string SchoolId { get; set; }
    }
}
