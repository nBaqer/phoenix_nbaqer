﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Fame1098InputModel.cs" company="FAME">
//   2015, 2016
// </copyright>
// <summary>
// JAGG - Defines the Fame1098InputModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CommonEntities
{
    using System.Collections.Generic;

    using CommonEntities.DtoConvertion;

    /// <summary>
    /// The fame 1098 input model.
    /// </summary>
    public class Fame1098InputModel
    {
        /// <summary>
        /// Gets or sets the archive list.
        /// </summary>
        public IList<ArchiveDto> ArchiveList { get; set; }

        /// <summary>
        /// Gets or sets the exclusions list.
        /// </summary>
        public IList<ExclusionsDto> ExclusionsList { get; set; }

        /// <summary>
        /// Gets or sets the transactions list.
        /// </summary>
        public IList<TransactionsDto> TransactionsList { get; set; }

        /// <summary>
        /// Gets or sets the valid enrollments list.
        /// </summary>
        public IList<ValidEnrollmentsDto> ValidEnrollmentsList { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// The factory.
        /// </summary>
        /// <param name="year">
        /// The year of calculation of 1098.
        /// </param>
        /// <returns>
        /// The <see cref="Fame1098InputModel"/>.
        /// </returns>
        public static Fame1098InputModel Factory(string year)
        {
            var output = new Fame1098InputModel();
            output.ArchiveList = new List<ArchiveDto>();
            output.ExclusionsList = new List<ExclusionsDto>();
            output.TransactionsList = new List<TransactionsDto>();
            output.ValidEnrollmentsList = new List<ValidEnrollmentsDto>();
            output.Year = year;
            return output;
        }
    }
}
