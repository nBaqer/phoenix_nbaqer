﻿namespace CommonEntities
{
    /// <summary>
    /// Hold SMTP configuration
    /// </summary>
    public class SmtpConfiguration
    {
        public string SmtpHost { get; set; }
        public string SmtpPort { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Recipients { get; set; }
    }
}
