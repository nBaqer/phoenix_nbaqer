﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098Bo.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//  JAGG - Defines the T1098Bo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageWebBusiness
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Headers;
    using System.Net.Mail;
    using System.Text;

    using AdvantageWebDataAccess;

    using CommonEntities;

    /// <summary>
    ///  The T 1098 business object.
    /// </summary>
    public class T1098Bo
    {
        /// <summary>
        ///  The school info.
        /// </summary>
        private string schoolInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="T1098Bo"/> class.
        /// </summary>
        public T1098Bo()
        {
            // DTO fame = DTO list;
            this.schoolInfo = string.Empty;
        }

        /// <summary>
        /// Gets or sets Expose the configured school values
        /// </summary>
        public SchoolConfiguration SchoolConfig { get; set; }

        /// <summary>
        /// Authenticate the school. if 
        /// </summary>
        /// <param name="headers">
        /// The request headers
        /// </param>
        /// <returns>
        /// True if the user is authenticated
        /// </returns>
        public bool RequestHeaderAuthentication(HttpRequestHeaders headers)
        {
            if (headers == null)
            {
                return false; // header is not there. no Authenticate
            }

            // Get if exists the value returned by the Header..ShoolToken.............
            IEnumerable<string> headerValues;
            var schoolToken = string.Empty;
            if (headers.TryGetValues("SCHOOL_TOKEN", out headerValues))
            {
                schoolToken = headerValues.FirstOrDefault();
            }

            // If the value returned is empty no authenticate
            if (schoolToken == string.Empty)
            {
                return false;  // No authenticate
            }

            // Get the token from Web.Config if exists
            this.schoolInfo = ConfigurationManager.AppSettings[schoolToken];

            if (string.IsNullOrWhiteSpace(this.schoolInfo))
            {
                return false; // token to equal. No authenticate
            }

            // The value exists the information is stored in schoolInfo
            char[] sep = { '_' };
            string[] values = this.schoolInfo.Split(sep);
            this.SchoolConfig = new SchoolConfiguration
            {
                SchoolToken = schoolToken,
                School = values[0],
                KissKey = values[1],
                SchoolId = values[2],
            };
            return true;
        }

        /// <summary>
        /// Main procedure. 
        /// Store the list in a jet DB.
        /// </summary>
        /// <param name="info">
        /// The 1098 info to be processed
        /// </param>
        /// <returns>
        /// return Success if all is OK
        /// </returns>
        public string ConvertDtoToAccessFile(Fame1098InputModel info)
        {
            // Get the configuration file values to configure our environment for this particular school
            var basicPath = ConfigurationManager.AppSettings["CopyBlankDatabaseToLocation"];

            // Create the path
            this.SchoolConfig.Folder = basicPath + info.Year + @"\" + this.SchoolConfig.School + "_" + this.SchoolConfig.KissKey;

            // Create the file name
            this.SchoolConfig.FileName = "Sch1098T_" + info.Year + ".mdb";

            // Delete Old Db if exists in the JET DB ....
            var jet = new JetServices();
            jet.DeletePriorDataBaseIfExists(this.SchoolConfig);

            // Copy the new database to destiny...
            this.CopyBlankDatabaseToSchoolFolder(info.Year);

            // Convert DTO Tables to JET 4.0
            var result = jet.ConvertTablesToJet(this.SchoolConfig, info);
            if (result == string.Empty)
            {
                // Send a email to predefined receptors, if they exists
                var smtp = new SmtpConfiguration
                {
                    SmtpHost = ConfigurationManager.AppSettings["SMTPHost"],
                    SmtpPort = ConfigurationManager.AppSettings["SMTPPort"],
                    Recipients = ConfigurationManager.AppSettings["MAIL_RECIPIENTS"],
                    User = ConfigurationManager.AppSettings["SMTPUserName"],
                    Password = ConfigurationManager.AppSettings["SMTPPassword"]
                };
                this.SendEmail(this.SchoolConfig, smtp);
                result = "Success";
            }

            return result;
        }

        /// <summary>
        ///  The send email.
        /// </summary>
        /// <param name="sconfig">
        ///  The school configuration.
        /// </param>
        /// <param name="smtp">
        ///  The SMTP.
        /// </param>
        public void SendEmail(SchoolConfiguration sconfig, SmtpConfiguration smtp)
        {
            if (string.IsNullOrEmpty(smtp.SmtpHost) || string.IsNullOrEmpty(smtp.Recipients))
            {
                // Email no configured. returned
                return;
            }

            SmtpClient client = new SmtpClient
            {
                Port = int.Parse(smtp.SmtpPort),
                Host = smtp.SmtpHost,
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new System.Net.NetworkCredential(smtp.User, smtp.Password)
            };
            var title = string.Format("1098T Received - {0} - {1}", sconfig.SchoolId, sconfig.Year);
            var message = string.Format(
                "You are received a 1098T request of processing from {0}. Year: {1}",
                sconfig.SchoolId,
                sconfig.Year);

            MailMessage mm = new MailMessage(smtp.User, smtp.Recipients, title, message)
            {
                BodyEncoding = Encoding.UTF8,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            };

            client.Send(mm);
        }

        /// <summary>
        /// Copy a black database in the designed directory
        /// </summary>
        /// <param name="year">The tax year </param>
        private void CopyBlankDatabaseToSchoolFolder(string year)
        {
            string strSourceFile = ConfigurationManager.AppSettings["BlankDatabaseLocation"].Trim() + year + ".mdb";

            // string strTargetDirectory = ConfigurationManager.AppSettings("CopyBlankDatabaseToLocation").ToString.Trim + strYear + "\\" + SchoolKissCode;
            string strTargetFile = this.SchoolConfig.Folder + "\\" + this.SchoolConfig.FileName;
            DirectoryInfo dirNew = new DirectoryInfo(this.SchoolConfig.Folder);

            if (dirNew.Exists)
            {
                if (!File.Exists(strTargetFile))
                {
                    File.Copy(strSourceFile, strTargetFile);
                }
            }
            else
            {
                dirNew.Create();
                if (!File.Exists(strTargetFile))
                {
                    File.Copy(strSourceFile, strTargetFile);
                }
            }
        }
    }
}
