﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JetServices.cs" company="FAME">
//   2015
// </copyright>
// <summary>
//   Operations to access the JET DB
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageWebDataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Data.OleDb;
    using System.IO;
    using System.Linq;
    using System.Text;

    using CommonEntities;
    using CommonEntities.DtoConvertion;

    /// <summary>
    /// Operations to access the JET DB
    /// </summary>
    public class JetServices
    {
        /// <summary>
        /// Delete the data source from the JET DB if exists if not exists 
        /// nothing is done and return
        /// </summary>
        /// <param name="schoolConfig">School parameters</param>
        /// <exception cref="OleDbException">Re-throw exception if the <code>OleDb</code> Operation fail</exception>
        public void DeletePriorDataBaseIfExists(SchoolConfiguration schoolConfig)
        {
            var target = schoolConfig.Folder + "/" + schoolConfig.FileName;
            var strConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=  " + target + ";";

            // Test first if the file exits...
            if (File.Exists(target) == false)
            {
                return;
            }

            var objconn = new OleDbConnection(strConnString);
            var objCmd = new OleDbCommand();
            var sb = new StringBuilder();

            try
            {
                objconn.Open();
            }
            catch (Exception ex)
            {
                // The file was not register in the DB. This is not a error necessary 
                // The first time that school run taxes of the year is not register...
                if (ex.Message.ToLower().Contains("could not find file"))
                {
                    return;
                }

                throw;
            }

            sb.Append("Delete from exclusions");

            objCmd.Connection = objconn;
            objCmd.CommandText = sb.ToString();
            objCmd.ExecuteNonQuery();
            sb.Remove(0, sb.Length);

            sb.Append("Delete from StuTransData");

            objCmd.CommandText = sb.ToString();
            objCmd.ExecuteNonQuery();
            sb.Remove(0, sb.Length);

            sb.Append("Delete from IRSdata");

            objCmd.CommandText = sb.ToString();
            objCmd.ExecuteNonQuery();
            sb.Remove(0, sb.Length);

            objCmd.Dispose();
            objconn.Close();
        }

        /// <summary>
        /// Make the conversion to Jet
        /// </summary>
        /// <param name="schoolConfig">
        /// The school configuration
        /// </param>
        /// <param name="info">
        /// The 1098T information
        /// </param>
        /// <returns>
        /// Error if exists.
        /// </returns>
        public string ConvertTablesToJet(SchoolConfiguration schoolConfig, Fame1098InputModel info)
        {
            var strTargetFile = schoolConfig.Folder + "\\" + schoolConfig.FileName;
            var strConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strTargetFile + ";";
            string lastStudent = string.Empty; // Flag to report last processed record
            string status = string.Empty;
            List<ValidEnrollmentsDto> internationalExclusions = new List<ValidEnrollmentsDto>();

            OleDbConnection objconn = new OleDbConnection(strConnString);
            objconn.Open();
            var objTrans = objconn.BeginTransaction();
            try
            {
                // Build The Query
                StringBuilder sb = new StringBuilder();

                OleDbCommand objcmd;
                if (info.ValidEnrollmentsList.Any())
                {
                    sb.Append(" Insert into IRSData(StuAcctNum,FirstName,LastName,SocSecNum, Addr1,City,State,ZipCode, ");
                    sb.Append(" Line1Data,Line3Data,Line4Data,Line5Data,Line6Data,RfndGRs,RfndLNs,RfndXXs,PaidGRs,PaidSPs,PaidSHs, ");
                    sb.Append(" PaidLNs,GrossPaid,HrsPerWeek,Line8Data,Corrected,LimitedByRemainingCost,TotalCost,PriorYrsPdAmt,UnpaidCosts) ");
                    ////.Append(" PaidLNs,GrossPaid,HrsPerWeek,Line8Data,TotalCost,PriorYrsPdAmt,UnpaidCosts) " + vbCrLf)
                    sb.Append(" Values ( ");
                    sb.Append(" ?,?,?,?,?, ");
                    sb.Append(" ?,?,?,?,?, ");
                    sb.Append(" ?,?,?,?,?, ");
                    sb.Append(" ?,?,?,?,?, ");
                    sb.Append(" ?,?,?,?,?,?,?,?) ");

                    objcmd = new OleDbCommand(sb.ToString(), objconn, objTrans);

                    foreach (ValidEnrollmentsDto dr in info.ValidEnrollmentsList)
                    {
                        lastStudent = string.Format("{0} {1} - {2}", dr.LastName, dr.FirstName, dr.StuAcctNum);

                        if (string.IsNullOrEmpty(dr.State) || (dr.ZipCode != null && dr.ZipCode.Length == 0) || (dr.ZipCode != null && dr.ZipCode.Length > 5))
                        {
                            internationalExclusions.Add(dr);
                        }
                        else
                        {
                            objcmd.CommandText = sb.ToString();

                            dr.Addr1 = dr.Addr1 ?? string.Empty;
                            objcmd.Parameters.Add("@StuAcctNum", OleDbType.VarChar).Value = dr.StuAcctNum;
                            objcmd.Parameters.Add("@FirstName", OleDbType.VarChar).Value = dr.FirstName;
                            objcmd.Parameters.Add("@LastName", OleDbType.VarChar).Value = dr.LastName;
                            objcmd.Parameters.Add("@SSN", OleDbType.VarChar).Value = dr.SocSecNum;
                            objcmd.Parameters.Add("@address1", OleDbType.VarChar).Value = dr.Addr1.Length > 50 ? dr.Addr1.Substring(0, 50) : dr.Addr1;
                            objcmd.Parameters.Add("@city", OleDbType.VarChar).Value = dr.City ?? string.Empty;
                            objcmd.Parameters.Add("@State", OleDbType.VarChar).Value = dr.State;
                            objcmd.Parameters.Add("@Zip", OleDbType.VarChar).Value = dr.ZipCode ?? string.Empty;
                            objcmd.Parameters.Add("@Line1Data", OleDbType.Integer).Value = dr.Line1Data;
                            objcmd.Parameters.Add("@Line1Data", OleDbType.Integer).Value = dr.Line3Data;
                            objcmd.Parameters.Add("@Line4Data", OleDbType.Integer).Value = dr.Line4Data;
                            objcmd.Parameters.Add("@Line5Data", OleDbType.Integer).Value = dr.Line5Data;
                            objcmd.Parameters.Add("@Line6Data", OleDbType.Boolean).Value = dr.Line6Data;
                            objcmd.Parameters.Add("@RfndGRs", OleDbType.Integer).Value = dr.RfndGRs;
                            objcmd.Parameters.Add("@RfndLNs", OleDbType.Integer).Value = dr.RfndLNs;
                            objcmd.Parameters.Add("@RfndXXs", OleDbType.Integer).Value = dr.RfndXXs;
                            objcmd.Parameters.Add("@PaidGRs", OleDbType.Integer).Value = dr.PaidGRs;
                            objcmd.Parameters.Add("@PaidSPs", OleDbType.Integer).Value = dr.PaidSPs;
                            objcmd.Parameters.Add("@PaidSHs", OleDbType.Integer).Value = dr.PaidSHs;
                            objcmd.Parameters.Add("@PaidLNs", OleDbType.Integer).Value = dr.PaidLNs;
                            objcmd.Parameters.Add("@GrossPaid", OleDbType.Integer).Value = dr.GrossPaid;
                            objcmd.Parameters.Add("@HrsPerWeek", OleDbType.Integer).Value = 0;
                            objcmd.Parameters.Add("@Line8Data", OleDbType.Boolean).Value = true;
                            objcmd.Parameters.Add("@Corrected", OleDbType.Char).Value = "0";
                            objcmd.Parameters.Add("@LimitedByRemainingCost", OleDbType.Boolean).Value = false;
                            objcmd.Parameters.Add("@TotalCost", OleDbType.Integer).Value = dr.TotalCost;
                            objcmd.Parameters.Add("@PriorYrsPdAmt", OleDbType.Integer).Value = dr.PriorYrsPdAmt;
                            objcmd.Parameters.Add("@UnpaidCosts", OleDbType.Integer).Value = dr.UnpaidCosts;
                            objcmd.ExecuteNonQuery();
                            objcmd.Parameters.Clear();
                        }
                    }
                }

                sb.Remove(0, sb.Length);

                // Insert into StuTransData
                if (info.TransactionsList.Any())
                {
                    sb.Append(
                        " Insert into StuTransData(StuAcctNum,TransDate,TransAmt,TransCode,IRSDataLine,PayType,Comment) ");
                    sb.Append(" Values(?,?,?,?,?,?,?) ");
                    objcmd = new OleDbCommand(sb.ToString(), objconn, objTrans);
                    foreach (TransactionsDto dr in info.TransactionsList.Where(x => internationalExclusions.Count(y => y.StuAcctNum == x.StuAcctNum) == 0))
                    {
                        lastStudent = string.Format("student with Account Number {0}", dr.StuAcctNum);

                        objcmd.CommandText = sb.ToString();

                        objcmd.Parameters.Add("@StuAcctNum", OleDbType.VarChar).Value = dr.StuAcctNum;
                        objcmd.Parameters.Add("@TransDate", OleDbType.VarChar).Value = dr.TransDate.ToString("yyyyMMdd").Trim();
                        objcmd.Parameters.Add("@TransAmt", OleDbType.Integer).Value = dr.TransAmt;
                        objcmd.Parameters.Add("@TransCode", OleDbType.VarChar).Value = dr.TransCode;
                        objcmd.Parameters.Add("@IRSDataLine", OleDbType.Integer).Value = dr.IRSDataLine;
                        objcmd.Parameters.Add("@PayType", OleDbType.VarChar).Value = dr.PayType;
                        objcmd.Parameters.Add("@Comment", OleDbType.VarChar).Value = dr.Comment;

                        objcmd.ExecuteNonQuery();
                        objcmd.Parameters.Clear();
                    }

                    sb.Remove(0, sb.Length);
                }

                if (internationalExclusions.Any())
                {
                    status = "The following list of students have invalid addresses or international addresses and were not processed: ";
                    foreach (var exclusion in internationalExclusions)
                    {
                        info.ExclusionsList.Add(new ExclusionsDto()
                        {
                            ZipCode = exclusion.ZipCode == null ? string.Empty : exclusion.ZipCode.Length > 5 ? exclusion.ZipCode.Substring(0, 5) : exclusion.ZipCode,
                            Addr1 = exclusion.Addr1.Length > 50 ? exclusion.Addr1.Substring(0, 50) : exclusion.Addr1,
                            City = exclusion.City ?? string.Empty,
                            ExclusionCode = 99,
                            FirstName = exclusion.FirstName,
                            LastName = exclusion.LastName,
                            SocSecNum = exclusion.SocSecNum,
                            State = exclusion.State ?? string.Empty,
                            StuAcctNum = exclusion.StuAcctNum
                        });
                        status += ". " + $"{exclusion.LastName} {exclusion.FirstName} - {exclusion.StuAcctNum}";
                    }
                }

                // Insert into Exclusions
                if (info.ExclusionsList.Any())
                {
                    sb.Append(
                         "Insert into Exclusions(StuAcctNum,FirstName,LastName,SocSecNum,Addr1,City,State,ZipCode,ExclusionCode) ");
                    sb.Append(" values(?,?,?,?,?,?,?,?,?) ");
                    objcmd = new OleDbCommand(sb.ToString(), objconn, objTrans);

                    foreach (ExclusionsDto dr in info.ExclusionsList)
                    {
                        lastStudent = $"{dr.LastName} {dr.FirstName} - {dr.StuAcctNum}";
                        objcmd.CommandText = sb.ToString();

                        dr.Addr1 = dr.Addr1 ?? string.Empty;
                        objcmd.Parameters.Add("@StuAcctNum", OleDbType.VarChar).Value = dr.StuAcctNum;
                        objcmd.Parameters.Add("@FirstName", OleDbType.VarChar).Value = dr.FirstName;
                        objcmd.Parameters.Add("@LastName", OleDbType.VarChar).Value = dr.LastName;
                        objcmd.Parameters.Add("@SSN", OleDbType.VarChar).Value = dr.SocSecNum;
                        objcmd.Parameters.Add("@address1", OleDbType.VarChar).Value = dr.Addr1.Length > 50
                                                                                          ? dr.Addr1.Substring(0, 50)
                                                                                          : dr.Addr1;
                        objcmd.Parameters.Add("@city", OleDbType.VarChar).Value = dr.City ?? string.Empty;
                        objcmd.Parameters.Add("@State", OleDbType.VarChar).Value = dr.State;
                        objcmd.Parameters.Add("@ZipCode", OleDbType.VarChar).Value = dr.ZipCode ?? string.Empty;
                        objcmd.Parameters.Add("@ExclusionCode", OleDbType.Integer).Value = dr.ExclusionCode;

                        objcmd.ExecuteNonQuery();
                        objcmd.Parameters.Clear();
                    }

                    sb.Remove(0, sb.Length);
                }

                objTrans.Commit();
                return status;
            }
            catch (OleDbException ex)
            {
                objTrans.Rollback();

                // return an error to the client
                var error =
                    "Data transfer not successful. Please contact FAME Support at 800.327.5772 for further assistance -- --";
                error += "Record with Error: " + lastStudent + " --";
                throw new Exception(error + ex.Message);
            }
            finally
            {
                // Close Connection
                objconn.Close();
            }
        }
    }
}
