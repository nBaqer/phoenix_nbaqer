﻿using System.Web.Mvc;

namespace AdvantageWebServices.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
