﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="T1098Controller.cs" company="FAME">
//   2015,2016
// </copyright>
// <summary>
// JAGG -  Controller for 1098T Service
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageWebServices.Controllers
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using AdvantageWebBusiness;

    using CommonEntities;

    /// <summary>
    /// Controller for 1098T Service
    /// </summary>
    public class T1098Controller : ApiController
    {
        /// <summary>
        /// Get the information from Advantage And convert to Access
        /// </summary>
        /// <param name="info">
        /// JSON object with the information of 1098T
        /// from the school
        /// </param>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        [HttpPost]
        public HttpResponseMessage Post([FromBody] Fame1098InputModel info)
        {
            if (info == null
                || info.ValidEnrollmentsList == null
                || string.IsNullOrWhiteSpace(info.Year)
                || info.ArchiveList == null
                || info.ExclusionsList == null
                || info.TransactionsList == null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    ReasonPhrase = "The 1098T information can not be null"
                };

                return response;
            }

            try
            {
                // Create Business Object
                var bo = new T1098Bo();

                // Testing Header for Authentication...........................................
                bool passed = bo.RequestHeaderAuthentication(this.Request.Headers);
                if (passed == false)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Forbidden)
                    {
                        ReasonPhrase = "You have not permission to use the service"
                    };
                    return response;
                }

                // Authenticate OK. Continue to Convert Result to Access.....................
                string message = bo.ConvertDtoToAccessFile(info);
                
                var resOk = new HttpResponseMessage(HttpStatusCode.OK) { ReasonPhrase = message };
                return resOk;
            }
            catch (Exception ex)
            {
                var error = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = ex.Message
                };
                return error;
            }
        }
    }
}
