﻿using Fame.Orm;
using Fame.Orm.Helpers;
using FAME.Orm.AdvTenant.Interfaces;

namespace FAME.Orm.AdvTenant
{
    public class TenantOrm : FameOrm, ITenantOrm
    {
        public TenantOrm(string connectionString, bool useMiniProfiler) : base(new ConnectionFactory(
            connectionString,
            useMiniProfiler))
        {
        }

        public TenantOrm(string connectionString) : this(connectionString, false)
        {
        }
    }
}
