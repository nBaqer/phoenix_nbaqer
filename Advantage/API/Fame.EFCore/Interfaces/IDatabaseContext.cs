﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Fame.EFCore.Interfaces
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Infrastructure;
    using Microsoft.EntityFrameworkCore.Internal;
    using System.Data.Common;

    /// <summary>
    /// The DbContext interface.
    /// </summary>
    public interface IDatabaseContext 
    {
        /// <summary>
        /// Execute a sql query or stored proc for INSERTS, UPDATES, DELETES with no return data.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="timeout">
        /// The timeout.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        void ExecuteSqlCommand(string query, int timeout = 120, params object[] parameters);

        /// <summary>
        /// Execute a sql query or stored proc asynchronously for INSERTS, UPDATES, DELETES with no return data.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        Task ExecuteSqlCommandAsync(string query, params object[] parameters);

        DbCommand LoadStoredProc(string storedProcName);

        /// <summary>
        /// The get repository for entity.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        IRepository<T> GetRepositoryForEntity<T>()
            where T : class;


        /// <summary>
        /// Perform a raw sql on the database.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map);
    }
}
