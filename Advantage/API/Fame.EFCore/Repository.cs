﻿
namespace Fame.EFCore
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Fame.EFCore.Infrastructure;
    using Fame.EFCore.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The repository.
    /// </summary>
    /// <typeparam name="TEntity">
    /// Repository Implementation for generic entities using EF Core.
    /// </typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// The context.
        /// </summary>
        private DbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{TEntity}"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public Repository(DbContext context)
        {
            this.context = context;
        }


        /// <summary>
        /// The count.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Count(Expression<Func<TEntity, bool>> filter = null)
        {
            if (filter != null)
            {
                return this.context.Set<TEntity>().Count(filter);
            }
            else
            {
                return this.context.Set<TEntity>().Count();
            }
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Create(TEntity entity)
        {
            this.context.Set<TEntity>().Add(entity);
            return entity;
        }

        /// <summary>
        /// The include.
        /// </summary>
        /// <param name="properties">
        /// The properties.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TEntity> Include(string properties)
        {
            return this.context.Set<TEntity>().Include(properties);
        }

        /// <summary>
        /// The find.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="include">
        /// The include.
        /// </param>
        /// <typeparam name="TKey">
        /// </typeparam>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Find<TKey>(TKey key, string include = null)
        {
            var entityType = this.context.Model.FindEntityType(typeof(TEntity));

            // find primary key name
            var entityKeyName = entityType.FindPrimaryKey().Properties.Select(x => x.Name).FirstOrDefault();

            var parameter = Expression.Parameter(typeof(TEntity), "x");
            var set = this.context.Set<TEntity>();
            IQueryable<TEntity> query = set;

            if (!string.IsNullOrEmpty(include))
            {
                query = set.Include(include);
            }

            query = query.Where((Expression<Func<TEntity, bool>>)
                                            Expression.Lambda(
                                                   Expression.Equal(
                                                            Expression.Property(parameter, entityKeyName ?? "Id"),
                                                             Expression.Constant(key)),
                                                   parameter));

            // Look in the database
            return query.FirstOrDefault();
        }


        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await this.context.Set<TEntity>().AddAsync(entity);

            return entity;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        public void Create(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                this.context.Set<TEntity>().Add(entity);
            }
        }
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        public async Task<IEnumerable<TEntity>> CreateAsync(IEnumerable<TEntity> entities)
        {
            var ents = entities.ToList();
            foreach (var entity in ents)
            {
                await this.context.Set<TEntity>().AddAsync(entity);
            }
            return ents;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Delete(TEntity entity)
        {
            if (this.context.Entry(entity).State == EntityState.Detached)
            {
                this.context.Set<TEntity>().Attach(entity);
            }

            this.context.Set<TEntity>().Remove(entity);

            return entity;
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        public void Delete(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                this.Delete(entity);
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        public void Delete(Expression<Func<TEntity, bool>> filter)
        {
            List<TEntity> entities = this.context.Set<TEntity>().Where(filter).ToList();

            if (entities.Count > 0)
            {
                this.Delete(entities);
            }
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = null;

            if (filter != null)
            {
                query = this.context.Set<TEntity>().Where(filter);
            }

            if (orderBy != null)
            {
                if (query == null)
                {
                    query = this.context.Set<TEntity>();
                }

                orderBy(query);
            }

            return query ?? this.context.Set<TEntity>();
        }

        /// <summary>
        /// The first or default.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> filter = null)
        {
            TEntity query;

            if (filter != null)
            {
                query = this.context.Set<TEntity>().FirstOrDefault(filter);
            }
            else
            {
                query = this.context.Set<TEntity>().FirstOrDefault();
            }

            return query;
        }

        /// <summary>
        /// The first or default async.
        /// </summary>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            Task<TEntity> query;

            if (filter != null)
            {
                query = this.context.Set<TEntity>().FirstOrDefaultAsync(filter);
            }
            else
            {
                query = this.context.Set<TEntity>().FirstOrDefaultAsync();
            }

            return query;

        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="TKey">
        /// The key to search by.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity GetById<TKey>(TKey id)
        {
            return this.context.Set<TEntity>().Find(id);
        }

        /// <summary>
        /// The get by id async.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <typeparam name="TKey">
        /// The key to search by.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public Task<TEntity> GetByIdAsync<TKey>(TKey id)
        {
            return this.context.Set<TEntity>().FindAsync(id);
        }

        /// <summary>
        /// The get page.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="resultsPerPage">
        /// The results per page.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="orderBy">
        /// The order by.
        /// </param>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<TEntity> GetPage(int page, int resultsPerPage, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = this.context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query.Skip(page).Take(resultsPerPage).AsQueryable();
        }

        /// <summary>
        /// The detach.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        public virtual void Detach(TEntity entity)
        {
            this.context.Entry(entity).State = EntityState.Detached;
        }

        /// <summary>
        /// The save.
        /// </summary>
        public virtual void Save()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// The save async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public virtual async Task SaveAsync()
        {
            await this.context.SaveChangesAsync();
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity Update(TEntity entity)
        {
            this.context.Set<TEntity>().Attach(entity);
            this.context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="entities">
        /// The entities.
        /// </param>
        public void Update(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                this.Update(entity);
            }
        }

        /// <summary>
        /// The load.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        public void Load(TEntity entity, string propertyName)
        {
            this.context.Entry(entity).Reference(propertyName).Load();
        }

    }
}
