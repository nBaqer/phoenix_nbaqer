﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DbContextExtensions.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The db context extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Fame.EFCore.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// General Extensions for the db context.
    /// </summary>
    public static class DbContextExtensions
    {
        /// <summary>
        /// Load a stored procedure into a command object.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="storedProcName">
        /// The stored Proc Name.
        /// </param>
        /// <returns>
        /// The <see cref="DbCommand"/>.
        /// </returns>
        public static DbCommand LoadStoredProc(this DbContext context, string storedProcName)
        {
            var cmd = context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }

        /// <summary>
        /// Add a Sql Parameter to a db command.
        /// </summary>
        /// <param name="cmd">
        /// The cmd.
        /// </param>
        /// <param name="paramName">
        /// parameter name to add.
        /// </param>
        /// <param name="paramValue">
        /// parameter value to add.
        /// </param>
        /// <param name="dbType">
        /// parameter database type.
        /// </param>
        /// <param name="direction">
        /// Parameter direction (defaulted to input).
        /// </param>
        /// <param name="precision">
        /// Parameter precision (defaulted to 18).
        /// </param>
        /// <param name="scale">
        /// Parameter scale (defaulted to 2).
        /// </param>
        /// <returns>
        /// The <see cref="DbCommand"/>.
        /// </returns>
        public static DbCommand WithSqlParam(this DbCommand cmd, string paramName, object paramValue, DbType dbType, ParameterDirection direction = ParameterDirection.Input, byte precision = 18, byte scale = 2)
        {
            if (string.IsNullOrEmpty(cmd.CommandText))
            {
                throw new InvalidOperationException(
                    "Call LoadStoredProc before using this method");
            }

            var param = cmd.CreateParameter();
            param.ParameterName = paramName;
            param.DbType = dbType;
            param.Value = paramValue;
            param.Direction = direction;
            param.Precision = precision;
            param.Scale = scale;
            cmd.Parameters.Add(param);
            return cmd;
        }

        /// <summary>
        /// Execute a stored procedure and return a data reader asynchronously.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static async Task<List<T>> ExecuteReaderStoredProcAsync<T>(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();
                try
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        return reader.MapToList<T>();
                    }
                }
                catch (Exception e)
                {
                    throw (e);
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute a stored procedure and return a data reader.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<T> ExecuteReaderStoredProc<T>(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }

                try
                {
                    using (var reader = command.ExecuteReader())
                    {
                        return reader.MapToList<T>();
                    }
                }
                catch (Exception e)
                {
                    throw (e);
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Execute a stored procedure.
        /// This call does not close the connection.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="DbCommand"/>.
        /// </returns>
        public static DbCommand ExecuteStoredProc(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }

                try
                {

                    command.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    command.Connection.Close();
                    throw (e);
                }

                return command;
            }
        }

        /// <summary>
        /// Execute a stored procedure asynchronously.
        /// This call does not close the connection.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public static async Task<DbCommand> ExecuteStoredProcAsync(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }

                try
                {
                    command.CommandTimeout = 180;
                    await command.ExecuteNonQueryAsync();

                }
                catch (Exception e)
                {
                    command.Connection.Close();
                    throw (e);
                }

                return command;
            }
        }

        /// <summary>
        /// Get an output parameter after calling a stored proc.
        /// This call does not close the connection.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="outputParam">
        /// The output Param.
        /// </param>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public static T GetOutputParameter<T>(this DbCommand command, string outputParam)
        {
            Type typeParameterType = typeof(T);
            var isNullable = Nullable.GetUnderlyingType(typeParameterType) != null;

            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                try
                {
                    try
                    {
                        
                        var converter = TypeDescriptor.GetConverter(typeof(T));
                        if (converter != null)
                        {
                            var val = command.Parameters[outputParam].Value.ToString();
                            if (string.IsNullOrEmpty(val))
                            {
                                return default(T);
                            }
                            // Cast ConvertFromString(string text) : object to (T)
                            return (T)converter.ConvertFromString(val);
                        }
                    }
                    catch (NotSupportedException)
                    {
                        return default(T);
                    }
                }
                catch (Exception e)
                {
                    return default(T);
                }

            }

            return default(T);
        }

        /// <summary>
        /// Executes the query and returns the first column of the first row in the result set returned by the query. All other columns and rows are ignored.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <returns>
        /// The <see cref="DataTable"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public static object ExecuteScalarResultSet(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                try
                {
                    object resultSet = command.ExecuteScalar();
                    return resultSet;
                }
                catch (Exception e)
                {
                    throw e;
                }

            }

            return null;
        }

        /// <summary>
        /// Close the connection.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        public static void Close(this DbCommand command)
        {
            command.Connection.Close();
        }

        /// <summary>
        /// The execute stored proc with output parameters.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <typeparam name="T">
        /// The entity type to be returned
        /// </typeparam>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public static List<T> ExecuteStoredProcWithOutputParameters<T>(this DbCommand command)
        {
            using (command)
            {
                if (command.Connection.State == System.Data.ConnectionState.Closed)
                {
                    command.Connection.Open();
                }
                
                try
                {
                    var objList = new List<T>();
                    var props = typeof(T).GetRuntimeProperties();
                    command.ExecuteNonQuery();
                    foreach (DbParameter param in command.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Output)
                        {
                            T obj = Activator.CreateInstance<T>();
                            foreach (var prop in props)
                            {
                                var val =
                                    param.Value;
                                prop.SetValue(obj, val == DBNull.Value ? null : val);
                            }
                            objList.Add(obj);

                        }
                    }

                    return objList;
                }
                catch (Exception e)
                {
                    throw (e);
                }
                finally
                {
                    command.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Maps a data reader output to a list of generic objects.
        /// </summary>
        /// <typeparam name="T">
        /// The entity type to be returned
        /// </typeparam>
        /// <param name="dr">
        /// The dr.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private static List<T> MapToList<T>(this DbDataReader dr)
        {
            var objList = new List<T>();
            var props = typeof(T).GetRuntimeProperties();

            if (dr.CanGetColumnSchema())
            {
                var schema = dr.GetColumnSchema();
                var colMapping = schema
                    .Where(x => props.Any(y => y.Name.ToLower() == x.ColumnName.ToLower()))
                    .ToDictionary(key => key.ColumnName.ToLower());

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        T obj = Activator.CreateInstance<T>();
                        foreach (var prop in props)
                        {
                            var val =
                                dr.GetValue(colMapping[prop.Name.ToLower()].ColumnOrdinal.Value);
                            if (prop.CanWrite)
                            {
                                prop.SetValue(obj, val == DBNull.Value ? null : val);
                            }
                        }
                        objList.Add(obj);
                    }
                }
            }



            return objList;
        }

    }

}
