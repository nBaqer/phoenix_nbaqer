﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fame.EFCore.Infrastructure
{
    using Fame.EFCore.Interfaces;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using System.Data;
    using System.Data.Common;
    using System.Threading.Tasks;

    /// <summary>
    /// The database context.
    /// </summary>
    public class IdentityDatabaseContext : IdentityDbContext<ApplicationUser>, IDatabaseContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityDatabaseContext"/> class.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        public IdentityDatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityDatabaseContext"/> class.
        /// </summary>
        protected IdentityDatabaseContext()
        {

        }

        public void ExecuteSqlCommand(string query, int timeout = 120, params object[] parameters)
        {
            this.Database.ExecuteSqlCommand(query, timeout, parameters);
        }

        public Task ExecuteSqlCommandAsync(string query, params object[] parameters)
        {
            return this.Database.ExecuteSqlCommandAsync(query, parameters);
        }

        /// <summary>
        /// The get repository for entity.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public IRepository<T> GetRepositoryForEntity<T>()
            where T : class
        {
            return new Repository<T>(this);
        }

        public DbCommand LoadStoredProc(string storedProcName)
        {
            var cmd = this.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }

        /// <summary>
        /// Perform a raw sql on the database.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map)
        {
            using (var command = this.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                this.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    var entities = new List<T>();

                    while (result.Read())
                    {
                        entities.Add(map(result));
                    }

                    return entities;
                }
            }
        }
    }
}
