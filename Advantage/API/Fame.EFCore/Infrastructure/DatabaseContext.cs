﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Fame.EFCore.Infrastructure
{
    using Fame.EFCore.Interfaces;

    using Microsoft.EntityFrameworkCore;
    using System.Data;
    using System.Threading.Tasks;

    /// <summary>
    /// The database context.
    /// </summary>
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseContext"/> class.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        public DatabaseContext(DbContextOptions options)
            : base(options)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseContext"/> class.
        /// </summary>
        protected DatabaseContext()
        {

        }

        /// <summary>
        /// Execute a sql query or stored proc.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="timeout">
        /// The timeout.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        public void ExecuteSqlCommand(string query, int timeout = 120, params object[] parameters)
        {
            this.Database.SetCommandTimeout(timeout);

            this.Database.ExecuteSqlCommand(query, parameters);
        }


        /// <summary>
        /// Execute a sql query or stored proc asynchronously.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="parameters">
        /// The parameters.
        /// </param>
        public Task ExecuteSqlCommandAsync(string query, params object[] parameters)
        {
            return this.Database.ExecuteSqlCommandAsync(query, parameters);
        }
        public DbCommand LoadStoredProc(string storedProcName)
        {
            var cmd = this.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedProcName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            return cmd;
        }

        /// <summary>
        /// The get repository for entity.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public IRepository<T> GetRepositoryForEntity<T>()
            where T : class
        {
            return new Repository<T>(this);
        }

        /// <summary>
        /// Perform a raw sql on the database.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map)
        {
            using (var command = this.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;

                this.Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    var entities = new List<T>();

                    while (result.Read())
                    {
                        entities.Add(map(result));
                    }

                    return entities;
                }
            }
        }
    }
}
