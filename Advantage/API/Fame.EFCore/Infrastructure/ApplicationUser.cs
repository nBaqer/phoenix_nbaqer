﻿

namespace Fame.EFCore.Infrastructure
{
    using Microsoft.AspNetCore.Identity;

    /// <summary>
    /// The application user.
    /// </summary>
    public  class ApplicationUser : IdentityUser
    {
    }
}
