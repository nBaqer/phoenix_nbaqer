﻿
DECLARE @IsRoleCreated INT
SET @IsRoleCreated  = (SELECT COUNT(*) FROM [sySysRoles]
WHERE [Descrip] = 'Lead Api User')

IF (@IsRoleCreated = 0)
BEGIN
	INSERT INTO [dbo].[sySysRoles]
			  ( [SysRoleId] ,
				[Descrip] ,
				[StatusId] ,
				[ModUser] ,
				[ModDate]
			  )
	  VALUES  ( 17 , -- SysRoleId - int
				'Lead Api User' , -- Descrip - varchar(80)
				'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' , -- StatusId - uniqueidentifier
				'sa' , -- ModUser - varchar(50)
				GETDATE()  -- ModDate - datetime
			  )


	  INSERT INTO [dbo].[syRoles]
			  ( [RoleId] ,
				[Role] ,
				[Code] ,
				[StatusId] ,
				[SysRoleId] ,
				[ModDate] ,
				[ModUser]
			  )
	  VALUES  ( 'F2970D18-0D7F-4C80-B485-E636FB13DB2D' , -- RoleId - uniqueidentifier
				'Lead Api User' , -- Role - varchar(60)
				'LeadAPI' , -- Code - varchar(12)
				'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' , -- StatusId - uniqueidentifier
				17 , -- SysRoleId - int
				GETDATE() , -- ModDate - datetime
				'sa'  -- ModUser - varchar(50)
			  )


	INSERT INTO [dbo].[sySysRolePermission]
			( [SysRoleId], [Permissions] )
	VALUES  ( 17, -- SysRoleId - int
			  N'{"modules": [ {"name": "AR","level": "none"}, 
							{"name": "AR","level": "none"}, {"name": "AD","level": "Modify"}, 
							{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
							{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
							{"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}'  -- Permissions - nvarchar(max)
			  )

	DECLARE @UserId UNIQUEIDENTIFIER 
	SET @UserId = (SELECT UserId FROM syUsers where UserName = 'support' AND IsAdvantageSuperUser = 1)

	DECLARE @CampusGroupId UNIQUEIDENTIFIER
	SET @CampusGroupId = (SELECT TOP 1 CampGrpId FROM syCampGrps WHERE IsAllCampusGrp = 1)

	DECLARE @ApiRoleId UNIQUEIDENTIFIER
	SET @ApiRoleId = (SELECT TOP 1 [RoleId] FROM [syRoles] WHERE [Role] = 'Lead Api User')

	INSERT INTO syUsersRolesCampGrps VALUES(NEWID(), @UserId,@ApiRoleId,@CampusGroupId,GETDATE(), 'support')
END
