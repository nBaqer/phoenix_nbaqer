﻿using Fame.Orm;
using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Interfaces;

namespace FAME.Orm.Advantage
{
    public class AdvantageOrm : FameOrm ,IAdvantageOrm
    {
        public string ConString;

        public AdvantageOrm(string connectionString, bool useMiniProfiler)
            : base(new ConnectionFactory(connectionString, useMiniProfiler))
        {
            ConString = connectionString;
        }

        public AdvantageOrm(string connectionString) : this(connectionString, false)
        {
            ConString = connectionString;
        }
    }
}
