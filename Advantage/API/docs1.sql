﻿/*
1. First Name (max length: 50)
2. Last Name (max Length:50)
3. Lead Status 
 These are statuses that are mapped to Advantage System Status of "New Lead"
 Query: 
SELECT * FROM dbo.syStatusCodes SC INNER JOIN dbo.sySysStatus SS ON SS.StatusId = SC.StatusId WHERE SS.SysStatusId= 1 
4. Lead Assigned to
                 Value needs to be one of the Active Campus
5. Admission Rep Assigned
Default to current date
Format: mm/dd/yyyy
6. Admissions Rep
                      Should be assigned to an Admission Rep

7. Lead Group
    Lead needs to be assigned to minimum of one Lead Group that belongs to campus lead     is assigned to (Bullet point #4)
    Query: 
SELECT LG.LeadGrpId,LG.Descrip FROM dbo.adLeadGroups LG JOIN dbo.syCampGrps CG ON CG.CampGrpId = LG.CampGrpId 
INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = CG.CampGrpId
WHERE LG.StatusId='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' 
AND CGC.CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'

The rest are cust
*/