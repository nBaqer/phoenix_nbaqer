## Entity Framework Core on the API

Three core projects have been added to the solution

1. Fame.EFCore
2. Fame.EFCore.Advantage
3. Fame.EFCore.Tenant

#### Fame.EFCore
Contains the Database Context and Repository interfaces and their respective implementations

#### Fame.EFCore.Advantage
Contains all of the advantage database entities and the advantage context that were
generated as a result of the db scaffold.

#### Fame.EFCore.Tenant
Contains all of the tenant auth db entities and the tenant context that were
generated as a result of the db scaffold.

### Scaffolding the DB Entities
To scaffold the entities, run the command below in the nuget package console, pointing to 
the appropriate project (either the ef core advantage project or the ef core tenant project)

To do this first select the appropriate project from the nuget package console's Default project dropdown list.
The selected project will be used to update the entities and context:

   ![PackageConsole](_Documentation/images/selectProjectPackageConsole.PNG)

Then, paste the following command in the nuget package console with the correct database 
(replace **DatabaseName** with the name of the database you are trying to connect to)
 and press enter.

    Scaffold-DbContext "Server=LOCALHOST\SQLDEV2016;Database=DatabaseName;Trusted_Connection=True;Integrated Security=true;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -Force

#### Tenant Auth Db Context Scaffolding Clean up

When you scaffold the Tenant Auth database, it will create a class inside the *Fame.EFCore.Tenant.Entities* named **TenantAuthDBContext.cs** .

You need to cut that class and put it at the root folder of the *Fame.EFCore.Tenant* project (overriding the existing one) and proceed with the following edits:

1. Open the  **TenantAuthDBContext.cs** class
2. Remove from the namespace the Entities word to make the full namespace just **Fame.EFCore.Tenant**
3. Inherit from **DatabaseContext, ITenantDbContext** instead of ~~DbContext~~ 

   ![TenantAuthDbContext](_Documentation/images/tenantAuthDbContext.png)

4. Remove the **OnConfiguring** method.

   ![TenantAuthDbContextOnConfiguring](_Documentation/images/tenantAuthDbContextRemoveOnConfiguring.PNG)

5. Add the TenantAuthDBContext constructor:
        
        public TenantAuthDbContext(DbContextOptions options)
            : base(options)
        {
        }



#### Advantage Db Context Scaffolding Clean up

When you scaffold the Advantage database, it will create a class inside the *Fame.EFCore.Advantage.Entities* named with the same database name that you choosed in your connection string ending with Context. So if my database name in my connection string was ChatDev, then i will get a class named **CHATDevContext.cs** .

You need to cut that class and put it at the root folder of the *Fame.EFCore.Advantage* project (delete the existing AdvantageContext.cs class) and proceed with the following edits:

1. Rename the new generated context class name with the following name: **AdvantageContext.cs**
2. Remove from the namespace the Entities word to make the full namespace just **Fame.EFCore.Advantage**
3. Add the using to **Fame.EFCore.Advantage.Entities**
4. Rename the class name to be  **AdvantageContext.cs**
5. Inherit from **DatabaseContext, IAdvantageContext** instead of ~~DbContext~~ 

   ![AdvantageDbContext](_Documentation/images/advantageContextCleanUp1.PNG)

6. Add the *AdvantageContext* Constructor.

        public AdvantageContext(DbContextOptions options)
            : base(options)
        {
        }

   ![AdvantageDbContext](_Documentation/images/advantageContextCleanUp2.PNG)
7. Add the *SaveChanges* method.

        void IAdvantageContext.SaveChanges()
        {
            base.SaveChanges();
        }

8. Remove the **OnConfiguring** method.

   ![TenantAuthDbContextOnConfiguring](_Documentation/images/tenantAuthDbContextRemoveOnConfiguring.PNG)

#### Advantage Database Context Transactions:
Default transaction behavior (from EF Core Docummentation):

By default, if the database provider supports transactions, all changes in a single call to SaveChanges() are applied in a transaction. If any of the changes fail, then the transaction is rolled back and none of the changes are applied to the database. This means that SaveChanges() is guaranteed to either completely succeed, or leave the database unmodified if an error occurs.

For most applications, this default behavior is sufficient. You should only manually control transactions if your application requirements deem it necessary.

**What does this mean for us?**
1. If you do an Insert or Update to an entity the changes will be saved automatically when the action completes. The **UnitOfWorkActionFilter** handles calls to the advantageDbContext.SaveChanges() to commit the transaction, if any of the changes fails, the whole thing is rolled back.
2. If you need to manually commit an entity change, so in your instance of *IAdvantageDbContext* that you get in your service, you can manually call SaveChanges
3. **Be Aware of:**

    If you are watching an Entity on the Add Watch or Quick Watch, and you modified the value of that entity on the watcher, that value will be changed on the database once the action is completed.

### Troubleshooting
- If you get an error saying instance not found, there is an issue with the connection string.  Make sure both the server name and database name are correct.
- If nothing occurs after running the command, try to run it with the -verbose flag to see more detailed output.
- You may have to change the project output type to console application and add a program file with a main method to get the command to execute successfully.
- Also make sure to close SSMS when trying to run the command, or it might cause timeout issues when trying to scaffold.
- **Build Errors?**
1. Check if you have installed at least .net core SDK 2.0.2:

   1.  Navigate to *C:\Program Files (x86)\dotnet\sdk\*
   2.  If you have .net core SDK 2.0.2 or higher proceed to **step 2**. Otherwise install the .net core sdk and runtime 2.0.2 or higher. You can find the installer on the following google drive:
    https://drive.google.com/drive/folders/0B0TQ1q7zq_ZFSVhvMUNXcUlxejQ
2. Run dotnet restore command on the solution:

   1. Open a command prompt.
   2. On your command prompt, navigate to the API folder on your forked repository:
      - CD C:\_Git\Advantage_**ForkedRepository**\Advantage\API\
      - Type in: **dotnet restore**
3. Rebuild the solution.