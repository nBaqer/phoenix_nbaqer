﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4Input.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4Input type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.InputOutput
{
    using System.Collections.Generic;

    using Common;

    using Entities;

    using Interfaces;

    /// <summary>
    /// The r 2 t 4 input.
    /// </summary>
    public class R2T4Input : IR2T4Input
    {
        /// <summary>
        /// Gets or sets the student.
        /// </summary>
        public Student Student { get; set; }

        /// <summary>
        /// Gets or sets the grants.
        /// </summary>
        public List<TitleIVGrants> Grants { get; set; }

        /// <summary>
        /// Gets or sets the loans.
        /// </summary>
        public List<TitleIVLoans> Loans { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        public IR2T4Results Results { get; set; }

        /// <summary>
        /// Gets or sets the Program type 
        /// </summary>
        public ProgramType ProgramType { get; set; }

        /// <summary>
        /// Gets or sets the period type
        /// </summary>
        public PaymentTypes PaymentType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is calculation required.
        /// </summary>
        public bool IsCalculationRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 4 required.
        /// </summary>
        public bool IsStep4Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 9 required.
        /// </summary>
        public bool IsStep9Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 10 required.
        /// </summary>
        public bool IsStep10Required { get; set; }
    }
}