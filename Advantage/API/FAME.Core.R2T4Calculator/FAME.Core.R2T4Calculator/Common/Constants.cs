﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Constants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Common
{
    /// <summary>
    /// The steps.
    /// </summary>
    public enum Steps
    {
        /// <summary>
        /// The step 1.
        /// </summary>
        Step1 = 1,

        /// <summary>
        /// The step 2.
        /// </summary>
        Step2 = 2,

        /// <summary>
        /// The step 3.
        /// </summary>
        Step3 = 3,

        /// <summary>
        /// The step 4.
        /// </summary>
        Step4 = 4,

        /// <summary>
        /// The step 5.
        /// </summary>
        Step5 = 5,

        /// <summary>
        /// The step 6.
        /// </summary>
        Step6 = 6,

        /// <summary>
        /// The step 7.
        /// </summary>
        Step7 = 7,

        /// <summary>
        /// The step 8.
        /// </summary>
        Step8 = 8,

        /// <summary>
        /// The step 9.
        /// </summary>
        Step9 = 9,

        /// <summary>
        /// The step 10.
        /// </summary>
        Step10 = 10
    }

    /// <summary>
    /// enum for Program types
    /// </summary>
    public enum ProgramType
    {
        /// <summary>
        /// program type: clock hour
        /// </summary>
        ClockHour = 1,

        /// <summary>
        /// program type: Credit hour
        /// </summary>
        CreditHour = 0
    }

    /// <summary>
    /// enum for Payment types
    /// </summary>
    public enum PaymentTypes
    {
        /// <summary>
        /// Payment type: Payment Period
        /// </summary>
        PaymentPeriod = 1,

        /// <summary>
        /// Payment type: Period of Enrollment
        /// </summary>
        PeriodOfEnrollment = 2
    }

    /// <summary>
    /// The constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Actual Attendance
        /// </summary>
        public const string ResultKeyActualAttendance = "ActualAttendence";

        /// <summary>
        /// The step 1.
        /// </summary>
        public const int Step1 = 1;

        /// <summary>
        /// The step 2.
        /// </summary>
        public const int Step2 = 2;

        /// <summary>
        /// The step 3.
        /// </summary>
        public const int Step3 = 3;

        /// <summary>
        /// The step 4.
        /// </summary>
        public const int Step4 = 4;

        /// <summary>
        /// The step 5.
        /// </summary>
        public const int Step5 = 5;

        /// <summary>
        /// The step 6.
        /// </summary>
        public const int Step6 = 6;

        /// <summary>
        /// The step 7.
        /// </summary>
        public const int Step7 = 7;

        /// <summary>
        /// The step 8.
        /// </summary>
        public const int Step8 = 8;

        /// <summary>
        /// The step 9.
        /// </summary>
        public const int Step9 = 9;

        /// <summary>
        /// The step 10.
        /// </summary>
        public const int Step10 = 10;

        /// <summary>
        /// The minimum refund amount.
        /// </summary>
        public const decimal MinimumRefund = 0;

        /// <summary>
        /// The result key box a.
        /// </summary>
        public const string ResultKeyBoxA = "BoxA";

        /// <summary>
        /// The result key box b.
        /// </summary>
        public const string ResultKeyBoxB = "BoxB";

        /// <summary>
        /// The result key box c.
        /// </summary>
        public const string ResultKeyBoxC = "BoxC";

        /// <summary>
        /// The result key box d.
        /// </summary>
        public const string ResultKeyBoxD = "BoxD";

        /// <summary>
        /// The result key box e.
        /// </summary>
        public const string ResultKeyBoxE = "BoxE";

        /// <summary>
        /// The result key box f.
        /// </summary>
        public const string ResultKeyBoxF = "BoxF";

        /// <summary>
        /// The result key box g.
        /// </summary>
        public const string ResultKeyBoxG = "BoxG";

        /// <summary>
        /// The result key box h.
        /// </summary>
        public const string ResultKeyBoxH = "BoxH";

        /// <summary>
        /// The result key box I.
        /// </summary>
        public const string ResultKeyBoxI = "BoxI";

        /// <summary>
        /// The result key box J.
        /// </summary>
        public const string ResultKeyBoxJ = "BoxJ";

        /// <summary>
        /// The result key box K.
        /// </summary>
        public const string ResultKeyBoxK = "BoxK";

        /// <summary>
        /// The result key box L.
        /// </summary>
        public const string ResultKeyBoxL = "BoxL";

        /// <summary>
        /// The result key box M.
        /// </summary>
        public const string ResultKeyBoxM = "BoxM";

        /// <summary>
        /// The result key box N.
        /// </summary>
        public const string ResultKeyBoxN = "BoxN";

        /// <summary>
        /// The result key box O.
        /// </summary>
        public const string ResultKeyBoxO = "BoxO";

        /// <summary>
        /// The result key box P.
        /// </summary>
        public const string ResultKeyBoxP = "BoxP";

        /// <summary>
        /// The result key box Q.
        /// </summary>
        public const string ResultKeyBoxQ = "BoxQ";

        /// <summary>
        /// The result key box R.
        /// </summary>
        public const string ResultKeyBoxR = "BoxR";

        /// <summary>
        /// The result key box S.
        /// </summary>
        public const string ResultKeyBoxS = "BoxS";

        /// <summary>
        /// The result key box T.
        /// </summary>
        public const string ResultKeyBoxT = "BoxT";

        /// <summary>
        /// The result key box U.
        /// </summary>
        public const string ResultKeyBoxU = "BoxU";

        /// <summary>
        /// Value of 100% for calculations
        /// </summary>
        public const decimal TotalPercent = 100.0m;

        /// <summary>
        /// The totalinstitutional charges.
        /// </summary>
        public const string TotalinstitutionalCharges = "TotalinstitutionalCharges";

        /// <summary>
        /// The total grants returned by student.
        /// </summary>
        public const string ResultTotalGrantsReturnedByStudent = "ResultTotalGrantsReturnedByStudent";
        
        /// <summary>
        /// The pell grant.
        /// </summary>
        public const string PellGrant = "Pell";

        /// <summary>
        /// The fseog grant
        /// </summary>
        public const string Fseog = "FSEOG";

        /// <summary>
        /// The teach grant.
        /// </summary>
        public const string TeachGrant = "TEACHGrant";

        /// <summary>
        /// Iraq Afghanistan Service Grant
        /// </summary>
        public const string IraqAfghanistanServiceGrant = "IraqAfghanistanServiceGrant";

        /// <summary>
        /// The unsubsidized ffel or direct stafford loan.
        /// </summary>
        public const string UnsubsidizedFfelOrDirectStaffordLoan = "UnsubsidizedFFELDirectStaffordLoan";

        /// <summary>
        /// The subsidized ffel or direct stafford loan.
        /// </summary>
        public const string SubsidizedFfelOrDirectStaffordLoan = "SubsidizedFFELDirectStaffordLoan";

        /// <summary>
        /// Perkins Loan
        /// </summary>
        public const string PerkinsLoan = "PerkinsLoan";

        /// <summary>
        /// The ffel or direct plus student.
        /// </summary>
        public const string FfelOrDirectPlusStudent = "FFELDirectPLUSStudent";

        /// <summary>
        /// The ffel or direct plus parent.
        /// </summary>
        public const string FfelOrDirectPlusParent = "FFELDirectPLUSParent";
    }
}
