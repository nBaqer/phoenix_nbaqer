﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step3.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step3 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Steps
{
    using System;
    using Common;
    using Interfaces;

    /// <summary>
    /// The step 3.
    /// </summary>
    public class Step3 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step3;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step3;

        /// <summary>
        /// This method calculates the step 3 results for Box I
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            try
            {
                if (this.Validate(input))
                {
                    decimal boxGValue = input.Results.StepResults[Constants.ResultKeyBoxG];
                    decimal boxHValue = input.Results.StepResults[Constants.ResultKeyBoxH];
                    decimal boxIResult = Math.Round(boxGValue * (boxHValue / 100), 2);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxI, boxIResult);
                }
            }
            catch (Exception ex)
            {
                input.Results.ValidationResult = ValidationResult.Exception;
                input.Results.ValidationMessage = ex.Message;
            }

            return input.Results;
        }

        /// <summary>
        /// The validate. This method validates the input values of Box G and Box H whether the values are greater than 0 or not.
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results?.StepResults != null
                          && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxG)
                          && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxH)
                && input.Results.StepResults[Constants.ResultKeyBoxG] >= 0 && input.Results.StepResults[Constants.ResultKeyBoxH] >= 0;

            if (!isValid)
            {
                if (input.Results != null)
                {
                    input.Results.ValidationResult = ValidationResult.Warning;
                    input.Results.ValidationMessage = Messages.InputValuesNull;
                }
            }

            return isValid;
        }
    }
}
