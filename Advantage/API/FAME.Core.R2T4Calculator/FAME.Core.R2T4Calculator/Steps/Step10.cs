﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step10.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step10 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Steps
{
    using System;
    using System.Linq;

    using Common;
    using Interfaces;

    /// <summary>
    /// The step 10.
    /// </summary>
    public class Step10 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step10;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step10;

        /// <summary>
        /// The method calculates the step 10 results 
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            if (this.Validate(input))
            {
                try
                {
                    if (input.Results.StepResults[Constants.ResultKeyBoxU] > Constants.MinimumRefund)
                    {
                        decimal boxUValue = input.Results.StepResults[Constants.ResultKeyBoxU];
                        foreach (var grant in input.Grants.OrderBy(priority => priority.Priority))
                        {
                            if (grant.AmountDisbursed > 0)
                            {
                                if (boxUValue > Constants.MinimumRefund)
                                {
                                    if (boxUValue < grant.AmountDisbursed - grant.AmountToBeReturned )
                                    {
                                        grant.AmountToBeReturnedByStudent =  boxUValue;
                                        boxUValue = boxUValue - grant.AmountToBeReturnedByStudent;
                                    }
                                    else
                                    {
                                        grant.AmountToBeReturnedByStudent = (grant.AmountDisbursed - grant.AmountToBeReturned);
                                        boxUValue = boxUValue - grant.AmountToBeReturnedByStudent;
                                    }

                                }
                            }
                        }

                        decimal totalGrantsReturnedByStudent = input.Grants.Sum(charge => charge.AmountToBeReturnedByStudent);
                        if (totalGrantsReturnedByStudent > Constants.MinimumRefund)
                        {
                            input.Results.StepResults.Add(Constants.ResultTotalGrantsReturnedByStudent, totalGrantsReturnedByStudent);
                        }
                    }
                }
                catch (Exception ex)
                {
                    input.Results.ValidationResult = ValidationResult.Exception;
                    input.Results.ValidationMessage = ex.Message;
                }
            }

            return input.Results;
        }

        /// <summary>
        /// The method validates the provided input and boxU fields
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results.StepResults != null &&
                          input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxU);
            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}