﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step9.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step9 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Steps
{
    using System;

    using Common;

    using Interfaces;

    /// <summary>
    /// The step 9.
    /// </summary>
    public class Step9 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step9;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step9;

        /// <summary>
        /// This method calculates the step 9 results for Box S, T and U
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            try
            {
                input.IsStep9Required = true;
                input.IsStep10Required = true;
                decimal boxTResultValue = 0;

                if (this.Validate(input))
                {
                    decimal boxQResultValue = input.Results.StepResults[Constants.ResultKeyBoxQ];
                    decimal boxRResultValue = input.Results.StepResults[Constants.ResultKeyBoxR];
                    decimal boxFResultValue = input.Results.StepResults[Constants.ResultKeyBoxF];

                    if (boxQResultValue > boxRResultValue)
                    {
                        decimal boxSResultValue = Math.Round(boxQResultValue - boxRResultValue, 2);
                        input.Results.StepResults.Add(Constants.ResultKeyBoxS, boxSResultValue);
                        if (boxFResultValue > 0)
                        {
                            boxTResultValue = Math.Round(boxFResultValue * 0.5m, 2, MidpointRounding.AwayFromZero);
                            input.Results.StepResults.Add(Constants.ResultKeyBoxT, boxTResultValue);
                        }

                        decimal boxUValue = Math.Round(boxSResultValue - boxTResultValue, 2);
                        input.Results.StepResults.Add(Constants.ResultKeyBoxU, boxUValue);
                    }
                    else
                    {
                        input.IsStep9Required = false;
                        input.IsStep10Required = false;
                    }
                }
            }
            catch (Exception ex)
            {
                input.Results.ValidationResult = ValidationResult.Exception;
                input.Results.ValidationMessage = ex.Message;
            }

            return input.Results;
        }

        /// <summary>
        /// Validates the required fields in input for calculation of step 9
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results.StepResults != null && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxQ) &&
                          input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxR);
            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}