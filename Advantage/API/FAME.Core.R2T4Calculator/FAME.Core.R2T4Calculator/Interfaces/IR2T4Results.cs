﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IR2T4Results.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IR2T4Results type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The validation result.
    /// </summary>
    public enum ValidationResult
    {
        /// <summary>
        /// The pass.
        /// </summary>
        Pass = 1,

        /// <summary>
        /// The fail.
        /// </summary>
        Fail = 2,

        /// <summary>
        /// The exception.
        /// </summary>
        Exception = 3,

        /// <summary>
        /// The warning. 
        /// </summary>
        Warning = 4
    }

    /// <summary>
    /// The R2T4Results interface.
    /// </summary>
    public interface IR2T4Results
    {
        /// <summary>
        /// Gets or sets the validation result.
        /// </summary>
        ValidationResult ValidationResult { get; set; }

        /// <summary>
        /// Gets or sets the validation message.
        /// </summary>
        string ValidationMessage { get; set; }

        /// <summary>
        /// Gets or sets the step results.
        /// </summary>
        Dictionary<string, decimal> StepResults { get; set; }
    }
}
