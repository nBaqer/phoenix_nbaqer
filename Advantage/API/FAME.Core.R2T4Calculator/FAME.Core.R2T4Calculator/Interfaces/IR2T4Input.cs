﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IR2T4Input.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IR2T4Input type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Interfaces
{
    using System.Collections.Generic;

    using Common;

    using Entities;

    /// <summary>
    /// The R2T4Input interface.
    /// </summary>
    public interface IR2T4Input
    {
        /// <summary>
        /// Gets or sets the student.
        /// </summary>
        Student Student { get; set; }

        /// <summary>
        /// Gets or sets the grants.
        /// </summary>
        List<TitleIVGrants> Grants { get; set; }

        /// <summary>
        /// Gets or sets the grants.
        /// </summary>
        List<TitleIVLoans> Loans { get; set; }

        /// <summary>
        /// Gets or sets the results.
        /// </summary>
        IR2T4Results Results { get; set; }

        /// <summary>
        /// Gets or sets the Program type 
        /// </summary>
        ProgramType ProgramType { get; set; }

        /// <summary>
        /// Gets or sets the payment type.
        /// </summary>
        PaymentTypes PaymentType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is calculation required.
        /// </summary>
        bool IsCalculationRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 4 required.
        /// </summary>
        bool IsStep4Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 9 required.
        /// </summary>
        bool IsStep9Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is step 10 required.
        /// </summary>
        bool IsStep10Required { get; set; }
    }
}
