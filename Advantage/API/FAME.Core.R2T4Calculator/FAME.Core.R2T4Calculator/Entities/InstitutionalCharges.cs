﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstitutionalCharges.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The institutional charges.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Entities
{
    /// <summary>
    /// The institutional charges.
    /// </summary>
    public class InstitutionalCharges
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionalCharges"/> class.
        /// </summary>
        /// <param name="chargeName">
        /// The charge name.
        /// </param>
        /// <param name="amountCharged">
        /// The amount charged.
        /// </param>
        public InstitutionalCharges(string chargeName, decimal amountCharged)
        {
            this.ChargeName = chargeName;
            this.AmountCharged = amountCharged;
        }

        /// <summary>
        /// Gets the grant name.
        /// </summary>
        public string ChargeName { get; }

        /// <summary>
        /// Gets the amount charged.
        /// </summary>
        public decimal AmountCharged { get; }
    }
}
