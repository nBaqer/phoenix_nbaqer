﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Student.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Student type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Entities
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The student.
    /// </summary>
    public class Student
    {
        /// <summary>
        /// Gets or sets the student name.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets the ssn.
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// Gets or sets the withdraw date.
        /// </summary>
        public DateTime WithdrawDate { get; set; }

        /// <summary>
        /// Gets or sets the scheduled hours.
        /// </summary>
        public decimal ScheduledHours { get; set; }

        /// <summary>
        /// Gets or sets the total hours.
        /// </summary>
        public decimal TotalHours { get; set; }

        /// <summary>
        /// Gets or sets the scheduled hours.
        /// </summary>
        public decimal ScheduledDays { get; set; }

        /// <summary>
        /// Gets or sets the scheduled hours.
        /// </summary>
        public decimal TotalDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance required.
        /// </summary>
        public bool IsAttendanceRequired { get; set; }

        /// <summary>
        /// Gets or sets the institutional charges.
        /// </summary>
        public List<InstitutionalCharges> InstitutionalCharges { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is tution charged.
        /// </summary>
        public bool? IsTutionCharged { get;  set; }

        /// <summary>
        /// Gets or sets the credit balance refunded.
        /// </summary>
        public decimal CreditBalanceRefunded { get; set; }
    }
}
