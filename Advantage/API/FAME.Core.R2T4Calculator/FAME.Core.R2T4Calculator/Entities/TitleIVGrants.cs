﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVGrants.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the TitleIVGrants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Core.R2T4Calculator.Entities
{
    /// <summary>
    /// The title iv grants.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class TitleIVGrants
    {
        /// <summary>
        /// Gets or sets the grant name.
        /// </summary>
        public string GrantName { get; set; }

        /// <summary>
        /// Gets or sets the amount disbursed.
        /// </summary>
        public decimal AmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the amount expected.
        /// </summary>
        public decimal AmountExpected { get; set; }

        /// <summary>
        /// Gets or sets the Priority.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets the amount to be returned back towards grant.
        /// </summary>
        public decimal AmountToBeReturned { get; set; }

        /// <summary>
        /// Gets or sets the amount to be returned by student.
        /// </summary>
        public decimal AmountToBeReturnedByStudent { get; set; }
    }
}