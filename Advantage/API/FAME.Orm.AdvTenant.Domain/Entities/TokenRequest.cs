﻿
using System;

namespace FAME.Orm.AdvTenant.Domain.Entities
{
    public class TokenRequest
    {
        public string TenantName { set; get; }

        public string Username { set; get; }

        public string Password { set; get; }
    }

    public class TokenRequestImpersonate
    {
        public string TenantName { set; get; }

        public Guid UserId { set; get; }

    }
}
