﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApiUser.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The api user.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.AdvTenant.Domain.Entities
{
    using System;

    using Fame.EFCore.Tenant.Entities;
    using Fame.Orm;

    /// <summary>
    /// The api user.
    /// </summary>
    public class ApiUser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiUser"/> class.
        /// </summary>
        /// <param name="tenantUsers">
        /// The tenant users.
        /// </param>
        public ApiUser(TenantUsers tenantUsers)
        {
            if (tenantUsers?.User != null)
            {
                this.UserId = tenantUsers.UserId;
                this.ApplicationId = tenantUsers.User.AspnetMembership.ApplicationId;
                this.Password = tenantUsers.User.AspnetMembership.Password;
                this.PasswordSalt = tenantUsers.User.AspnetMembership.PasswordSalt;
                this.PasswordFormat = tenantUsers.User.AspnetMembership.PasswordFormat;
                this.MobilePin = tenantUsers.User.AspnetMembership.MobilePin;
                this.Email = tenantUsers.User.AspnetMembership.Email;
                this.LoweredEmail = tenantUsers.User.AspnetMembership.LoweredEmail;
                this.IsApproved = tenantUsers.User.AspnetMembership.IsApproved;
                this.IsLockedOut = tenantUsers.User.AspnetMembership.IsLockedOut;
                this.CreateDate = tenantUsers.User.AspnetMembership.CreateDate;
                this.LastLoginDate = tenantUsers.User.AspnetMembership.LastLoginDate;
                this.LastPasswordChangedDate = tenantUsers.User.AspnetMembership.LastPasswordChangedDate;
                this.LastLockoutDate = tenantUsers.User.AspnetMembership.LastLockoutDate;
                this.FailedPasswordAttemptCount = tenantUsers.User.AspnetMembership.FailedPasswordAttemptCount;
                this.FailedPasswordAttemptWindowStart =
                    tenantUsers.User.AspnetMembership.FailedPasswordAttemptWindowStart;
                this.FailedPasswordAnswerAttemptCount =
                    tenantUsers.User.AspnetMembership.FailedPasswordAnswerAttemptCount;
                this.FailedPasswordAnswerAttemptWindowStart =
                    tenantUsers.User.AspnetMembership.FailedPasswordAnswerAttemptWindowStart;
                this.Comment = tenantUsers.User.AspnetMembership.Comment;
                this.TenantName = tenantUsers.Tenant.TenantName;
                this.TenantServerName = tenantUsers.Tenant.ServerName;
                this.TenantDatabaseName = tenantUsers.Tenant.DatabaseName;
                this.TenantUser = tenantUsers.Tenant.UserName;
                this.TenantPassword = tenantUsers.Tenant.Password;
                this.IsSupportUser = tenantUsers.IsSupportUser ?? false;
                this.IsDefaultTenant = tenantUsers.IsDefaultTenant ?? false;
                this.EnvironmentName = tenantUsers.Tenant.Environment.EnvironmentName;
            }
        }

        /// <summary>
        /// Gets the user id.
        /// </summary>
        public Guid UserId { get; private set; }

        /// <summary>
        /// Gets the user membership application id.
        /// </summary>
        public Guid ApplicationId { get; private set; }

        /// <summary>
        /// Gets the user membership password.
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Gets the user membership password format.
        /// </summary>
        public int PasswordFormat { get; private set; }

        /// <summary>
        /// Gets the user membership password salt.
        /// </summary>
        public string PasswordSalt { get; private set; }

        /// <summary>
        /// Gets the user membership mobile pin.
        /// </summary>
        public string MobilePin { get; private set; }

        /// <summary>
        /// Gets the user membership email.
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Gets the user membership lowered email.
        /// </summary>
        public string LoweredEmail { get; private set; }

        /// <summary>
        /// Gets the user membership password question.
        /// </summary>
        public string PasswordQuestion { get; private set; }

        /// <summary>
        /// Gets the user membership password answer.
        /// </summary>
        public string PasswordAnswer { get; private set; }

        /// <summary>
        /// Gets a value indicating whether user membership is approved.
        /// </summary>
        public bool IsApproved { get; private set; }

        /// <summary>
        /// Gets a value indicating whether user membership is locked out.
        /// </summary>
        public bool IsLockedOut { get; private set; }

        /// <summary>
        /// Gets the user membership create date.
        /// </summary>
        public DateTime CreateDate { get; private set; }

        /// <summary>
        /// Gets the user membership last login date.
        /// </summary>
        public DateTime LastLoginDate { get; private set; }

        /// <summary>
        /// Gets the user membership last password changed date.
        /// </summary>
        public DateTime LastPasswordChangedDate { get; private set; }

        /// <summary>
        /// Gets the user membership last lockout date.
        /// </summary>
        public DateTime LastLockoutDate { get; private set; }

        /// <summary>
        /// Gets the user membership failed password attempt count.
        /// </summary>
        public int FailedPasswordAttemptCount { get; private set; }

        /// <summary>
        /// Gets the user membership failed password attempt window start.
        /// </summary>
        public DateTime FailedPasswordAttemptWindowStart { get; private set; }

        /// <summary>
        /// Gets the user membership failed password answer attempt count.
        /// </summary>
        public int FailedPasswordAnswerAttemptCount { get; private set; }

        /// <summary>
        /// Gets the user membership failed password answer attempt window start.
        /// </summary>
        public DateTime FailedPasswordAnswerAttemptWindowStart { get; private set; }

        /// <summary>
        /// Gets the user membership comment.
        /// </summary>
        public string Comment { get; private set; }

        /// <summary>
        /// Gets the tenant name.
        /// </summary>
        public string TenantName { get; private set; }

        /// <summary>
        /// Gets the tenant server name.
        /// </summary>
        public string TenantServerName { get; private set; }

        /// <summary>
        /// Gets the tenant database name.
        /// </summary>
        public string TenantDatabaseName { get; private set; }

        /// <summary>
        /// Gets the tenant user.
        /// </summary>
        public string TenantUser { get; private set; }

        /// <summary>
        /// Gets the tenant password.
        /// </summary>
        public string TenantPassword { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is support user.
        /// </summary>
        public bool IsSupportUser { get; private set; }

        /// <summary>
        /// Gets a value indicating whether is default tenant.
        /// </summary>
        public bool IsDefaultTenant { get; private set; }

        /// <summary>
        /// Gets the environment name.
        /// </summary>
        public string EnvironmentName { get; private set; }

        /// <summary>
        /// The build connection string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string BuildConnectionString()
        {
            return $"Server={this.TenantServerName};Database={this.TenantDatabaseName};UID={this.TenantUser};PWD={this.TenantPassword};Connect Timeout=600;";
        }
    }
}
