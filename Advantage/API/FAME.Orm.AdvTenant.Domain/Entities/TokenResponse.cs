﻿namespace FAME.Orm.AdvTenant.Domain.Entities
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public string Message { get; set; }
    }
}
