﻿using Fame.Orm.Infrastructure;
using FAME.Orm.AdvTenant.Domain.Entities;

namespace FAME.Orm.AdvTenant.Domain.Commands
{
    public class GetTenantUser : CommandBase<ApiUser>
    {
        public GetTenantUser(string username,string tenantName = null)
        {
            const string baseQuery = @"SELECT [M].[ApplicationId] ,[M].[UserId] ,[M].[Password] ,[M].[PasswordFormat] 
                                        ,[M].[PasswordSalt] ,[M].[MobilePIN] ,[M].[EmailAddress] ,[M].[LoweredEmail] ,[M].[PasswordQuestion] 
                                        ,[M].[PasswordAnswer] ,[M].[IsApproved] ,[M].[IsLockedOut] ,[M].[CreateDate] ,[M].[LastLoginDate],
                                        [M].[LastPasswordChangedDate] , [M].[LastLockoutDate] ,[M].[FailedPasswordAttemptCount] ,
                                        [M].[FailedPasswordAttemptWindowStart] , [M].[FailedPasswordAnswerAttemptCount] ,
                                        [M].[FailedPasswordAnswerAttemptWindowStart] , [M].[Comment] ,[T].[TenantName] ,[T].[ServerName] AS TenantServerName ,
                                        [T].[DatabaseName] AS TenantDatabaseName ,[T].[UserName] AS TenantUser , [T].[Password] as TenantPassword , [TU].[IsSupportUser],[TU].[IsDefaultTenant],
                                        [E].[EnvironmentName]
                                        FROM    [dbo].[aspnet_Users] AS [U]
                                                INNER JOIN [dbo].[aspnet_Membership] AS [M] ON [U].UserId = [M].UserId
                                                INNER JOIN [dbo].[TenantUsers] AS [TU] ON [M].[UserId] = [TU].[UserId]
                                                INNER JOIN [dbo].[Tenant] AS [T] ON [TU].[TenantId] = [T].[TenantId]
                                                INNER JOIN [dbo].[Environment] AS [E] ON [T].[EnvironmentID] = [E].[EnvironmentId]
                                        WHERE[U].[Username] = @Username";

            const string optionalPredicate = @"AND [T].[TenantName] = @TenantName";

            Parameters = new {Username = username, TenantName = tenantName};

            Command = string.IsNullOrWhiteSpace(tenantName) ? $"{baseQuery}" : $"{baseQuery} {optionalPredicate}";
        }
    }
}
