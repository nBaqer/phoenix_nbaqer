@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL

SET LoopCounter=0
SET LoopCounterLimit=30

GOTO Start

:Usage
ECHO. 
ECHO DeployWebsite:
ECHO   Deploys a drop build website to a target share
ECHO.
ECHO Usage:
ECHO   DeployWebsite DropWebsite TargetWebsite [Environment] [LogFile]
ECHO.
ECHO Arguments:
ECHO   DropWebsite     Optional: Path to the _PublishedWebSites folder
ECHO                   Ex: 
ECHO                   C:\Builds\eSolutions\Phase3\1.3.1104.4\
ECHO                   _PublishedWebSites\FaaWeb
ECHO                   Default: current directory
ECHO                   Note: Typically DeployWebsite.bat would be in the 
ECHO                   _PublishedWebsites folder, in which case you only need
ECHO                   to specify subfolder (i.e. FaaWeb)
ECHO   TargetWebsite   UNC of the target website
ECHO                   Ex: \\qafa\inetpub\wwwroot\eSolutions\Dev\Phase3
ECHO   TargetServerName  Name of the target server where the service will be installed.
ECHO		Default: none - local server
ECHO.
ECHO   TargetServerUserName   Domain User Name used for authentication to the target server
ECHO.                
ECHO   TargetServerPassword   Password for ServiceUser Domain User Name used for authentication to the target server
ECHO.
ECHO   Environment 	   Optional: Name of the Environment from appsettings.[Environment param Value].json 
ECHO				   to identify json file for renaming to appsettings.json
ECHO.
ECHO   LogFile         Optional: Y/N
ECHO                   Y=Send output to log file
ECHO                   N=Send output to console
ECHO                   Default: N
GOTO End


:whileIISIsStopping 
   for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServerName% query "W3SVC" ^| findstr "        STATE"') do (
		if /I "%%M" == "STOP_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :whileIISIsStopping
			) ELSE (
				SET LoopCounter=0
			)
		)
		SET LoopCounter=%LoopCounter%+1;
   )
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%
	

:waitWhileIISIsStarted
	for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServerName% query "W3SVC" ^| findstr "        STATE"') do (
		if /I "%%M" == "START_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :waitWhileIISIsStarted
			) ELSE (
				SET LoopCounter=0
			)
		) else (
			if /I "%%M" == "STOPPED" (
				ECHO "Unable to start the IIS automatically. Check Service configuratoin" %RedirLog%
				SET ExitCode=2
			)
		)
		SET LoopCounter=%LoopCounter%+1;
	)
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%

:Start
SET DropWebSite=%~1
SET TargetWebsite=%~2
SET TargetServerName=%~3
SET TargetServerUserName=%~4
SET TargetServerPassword=%~5
SET Environment=%~6
SET LogFile=%~7
SET ExitCode=0

IF "%DropWebSite%"=="?" GOTO Usage
IF "%DropWebSite%"=="/?" GOTO Usage

IF "%DropWebSite%"=="" SET DropWebSite=%~dp0
IF "%TargetWebsite%"=="" GOTO Usage
IF "%Environment%"=="" SET Environment=Development
IF "%LogFile%"=="" SET LogFile=N

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log

IF EXIST \\%TargetServerName% (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use /delete \\%TargetServerName%
)

ECHO Connecting to Target server %TargetServerName% ...  %LogFileName%

IF "%TargetServerUserName%"=="" ( 
	IF "%TargetServerPassword%"=="" (
		net use \\%TargetServerName%
	)
) ELSE (
	net use \\%TargetServerName% %TargetServerPassword% /user:%TargetServerUserName%
)

ECHO Stopping w3svc Service on Target server %TargetServerName% ...  %LogFileName%

SC \\%TargetServerName% stop w3svc

ECHO Waitting for w3svc Service on Target server %TargetServerName% to stop ...  %LogFileName%
CALL :whileIISIsStopping

IF NOT "%DropWebSite:~-1%"=="\" SET DropWebSite=%DropWebSite%\
IF NOT "%TargetWebsite:~-1%"=="\" SET TargetWebsite=%TargetWebsite%\

SET DelWeb=IF EXIST "%TargetWebsite%" DEL "%TargetWebsite%*.*" /f/s/q
SET PubWeb=XCOPY "%DropWebSite%*.*" "%TargetWebsite%" /q/i/s/e/k/r/h/y

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO Publishing Website %DropWebSite% To %TargetWebsite% %RedirLog%
ECHO Deleting contents of target folder... %RedirLog%
%DelWeb% > NUL
ECHO Copying drop files to target folder... %RedirLog%
%PubWeb% %RedirLog%
ECHO. %RedirLog%

SET EnviromentConfig="%TargetWebsite%appsettings.%Environment%.json"
ECHO "%EnviromentConfig%"

IF NOT EXIST "%EnviromentConfig%" (
	SET ExitCode=1
	ECHO File does not exist %EnviromentConfig% %RedirLog%
	EXIT /B %ExitCode%
) ELSE (
	ECHO Renaming apsettings.json files %RedirLog%
	RENAME "%TargetWebsite%appsettings.json" "appsettings.local.json"
	RENAME %EnviromentConfig% appsettings.json
	ECHO apsettings.json were swapped %RedirLog%
)

ECHO Starting w3svc Service on  Target server %TargetServerName% ...  %LogFileName%

sc \\%TargetServerName% start w3svc

CALL :waitWhileIISIsStarted

IF EXIST \\%TargetServerName% (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use /delete \\%TargetServerName%
)

GOTO :END


:End
EXIT /B %ExitCode%


