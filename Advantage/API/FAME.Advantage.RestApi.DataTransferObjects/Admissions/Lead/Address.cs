﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Address.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead address.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.AccessControl;
    using System.Text;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The lead address.
    /// </summary>
    public class Address : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the lead address id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        [Required]
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the address type id.
        /// </summary>
        [Required]
        public Guid? AddressTypeId { get; set; }

        /// <summary>
        /// Gets or sets the address1.
        /// </summary>
        [MaxLength(250)]
        [Required]
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address2.
        /// </summary>
        [MaxLength(250)]
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        [MaxLength(250)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public Guid? StateId { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        [MaxLength(10)]
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the country id.
        /// </summary>
        public Guid? CountryId { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        /// 
        public Guid? StatusId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the address is the mailing address.
        /// </summary>
        [Required]
        public bool? IsMailingAddress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the address is address to show on the lead page.
        /// </summary>
        [Required]
        public bool? IsShowOnLeadPage { get; set; }

        /// <summary>
        /// Gets or sets the Modified Date.
        /// </summary>
        [SwaggerExclude]
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedUser.
        /// </summary>
        [MaxLength(50)]
        [SwaggerExclude]
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// [MaxLength(100)]
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the IsInternational.
        /// </summary>
        public bool? IsInternational { get; set; }

        /// <summary>
        /// Gets or sets the county id.
        /// </summary>
        public Guid? CountyId { get; set; }

        /// <summary>
        /// Gets or sets the county.
        /// </summary>
        /// [MaxLength(100)]
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the foreign county string
        /// </summary>
        [MaxLength(100)]
        public string ForeignCountyStr { get; set; }

        /// <summary>
        /// Gets or sets the foreign country string
        /// </summary>
        [MaxLength(100)]
        public string ForeignCountryStr { get; set; }

        /// <summary>
        /// Gets or sets the address apartment number
        /// </summary>
        [MaxLength(20)] public string ApartmentNumber { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

    }
}
