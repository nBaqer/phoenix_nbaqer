﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadBase.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead base.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;

    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Constants;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The lead base class contains the demographic information of the lead that can be shared.
    /// </summary>
    public class LeadBase : IActionResultStatus
    {

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        [MaxLength(50)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the interest area id.
        /// </summary>
        public Guid? InterestAreaId { get; set; }

        /// <summary>
        /// Gets or sets the best time to contact the lead.
        /// </summary>
        [MaxLength(20)]
        public string BestTime { get; set; }

        /// <summary>
        /// Gets or sets the birth date.
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the admissions rep id.
        /// </summary>
        [RequiredIfOtherPropertyValueIs("LeadStatusId", Constants.GuidEmpty, ComparisonType.HasValue)]
        public Guid? AdmissionsRepId { get; set; }

        /// <summary>
        /// Gets or sets the date applied.
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime? DateApplied { get; set; }

        /// <summary>
        /// Gets or sets the lead status id.
        /// </summary>
        public Guid? LeadStatusId { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        public DateTime? CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the ssn.
        /// </summary>
        [MaxLength(50)]
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the student status id.
        /// </summary>
        public Guid? StudentStatusId { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the reason not enrolled id.
        /// </summary>
        public Guid? ReasonNotEnrolledId { get; set; } = null;

        /// <summary>
        /// Gets or sets the enroll state id.
        /// </summary>
        public Guid? EnrollStateId { get; set; } = null;

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        public Guid? VendorId { get; set; } = null;

        /// <summary>
        /// Gets or sets the source category id.
        /// </summary>
        public Guid? SourceCategoryId { get; set; } = null;

        /// <summary>
        /// Gets or sets the source type id.
        /// </summary>
        public Guid? SourceTypeId { get; set; } = null;

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        [RequiredIfOtherPropertyValueIs("LeadStatusId", Constants.GuidEmpty, ComparisonType.HasValue)]
        public Guid? CampusId { get; set; }

        /// <summary>
        /// Gets or sets the is official name change.
        /// If the value is null, the first name or last name will not be updated.
        /// If the value is true, the first name or last name change will be updated and the old name be tracked in a history table.
        /// If the value is false, the first name or last name will be updated without tracking the change.
        /// </summary>
        public bool? IsOfficialNameChange { get; set; }

        /// <summary>
        /// Gets or sets the AFA student id.
        /// </summary>
        public long? AfaStudentId { get; set; }

        /// <summary>
        /// Gets or sets the Client Management Software id.
        /// </summary>
        public string CmsId { get; set; }
    }
}

