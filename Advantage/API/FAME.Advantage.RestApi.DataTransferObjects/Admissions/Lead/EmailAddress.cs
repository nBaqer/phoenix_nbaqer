﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailAddress.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The email.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The email.
    /// </summary>
    public class EmailAddress : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        [Required]
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is preferred.
        /// Providing a true for IsPreferred will make this email the preferred email and if another preferred email exists, that other preferred email will be set to false.
        /// </summary>
        [Required]
        public bool? IsPreferred { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is portal user name.
        /// </summary>
        public bool? IsPortalUserName { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        [SwaggerExclude]
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        [SwaggerExclude]
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the email type id.
        /// </summary>
        [Required]
        public Guid? EmailTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid? StatusId { get; set; }

        /// <summary>
        /// Gets or sets the is shown on lead page.
        /// </summary>
        [Required]
        public bool? IsShowOnLeadPage { get; set; }
    }
}
