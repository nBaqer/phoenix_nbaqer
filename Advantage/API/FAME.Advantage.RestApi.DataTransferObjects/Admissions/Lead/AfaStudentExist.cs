﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaStudentExist.cs" company="FAME Inc.">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The AFA student exist.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;

    /// <summary>
    /// The AFA student exist.
    /// </summary>
    public class AfaStudentExist : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the AFA student id.
        /// </summary>
        public long? AfaStudentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether AFA student id exists.
        /// </summary>
        public bool AfaStudentIdExists { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }
    }
}
