﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Lead.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The lead.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection.Emit;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Constants;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;
    using FAME.Extensions;

    /// <summary>
    /// The lead.
    /// </summary>
    public class Lead : LeadBase, IValidatableObject
    {

        /// <summary>
        /// Gets or sets the phone type id.
        /// </summary>
        [RequiredIfOtherPropertyValueIs("Phone", null, ComparisonType.HasValue, ValidationRequestType.Post)]
        public Guid? PhoneTypeId { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        [RegexMatch(RegexType.Phone, "IsUsaPhoneNumber", null)]
        [RequiredIfOtherPropertyValueIs("PhoneTypeId", Constants.GuidEmpty, ComparisonType.HasValue, ValidationRequestType.Post)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the phone id.
        /// </summary>
        public Guid? PhoneId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is USA phone number.
        /// If a non US phone number is given, set this value to false, otherwise you'll receive a bad request. 
        /// </summary>
        public bool IsUsaPhoneNumber { get; set; } = true;

        /// <summary>
        /// Gets or sets the email type id.
        /// </summary>
        [RequiredIfOtherPropertyValueIs("Email", null, ComparisonType.HasValue, ValidationRequestType.Post)]
        public Guid? EmailTypeId { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [RegexMatch(RegexType.Email)]
        [RequiredIfOtherPropertyValueIs("EmailTypeId", Constants.GuidEmpty, ComparisonType.HasValue, ValidationRequestType.Post)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the email id.
        /// </summary>
        public Guid? EmailId { get; set; }

        /// <summary>
        /// Gets or sets the address type id for best address.
        /// </summary>
        [RequiredIfOtherPropertyValueIs("Address1", null, ComparisonType.HasValue, ValidationRequestType.Post)]
        public Guid? AddressTypeId { get; set; }

        /// <summary>
        /// Gets or sets the address id.
        /// </summary>
        public Guid? AddressId { get; set; }

        /// <summary>
        /// Gets or sets the address 1 for best address.
        /// </summary>
        [MaxLength(50)]
        [RequiredIfOtherPropertyValueIs("AddressTypeId", Constants.GuidEmpty, ComparisonType.HasValue, ValidationRequestType.Post)]
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address 2 for best address.
        /// </summary>
        [MaxLength(50)]
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city for best address.
        /// </summary>
        [MaxLength(250)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state id for best address.
        /// </summary>
        public Guid? StateId { get; set; }

        /// <summary>
        /// Gets or sets the zip for best address.
        /// </summary>
        [MaxLength(10)]
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the county id for best address.
        /// </summary>
        public Guid? CountyId { get; set; }

        /// <summary>
        /// Gets or sets the country id for best address.
        /// </summary>
        public Guid? CountryId { get; set; }

        /// <summary>
        /// Gets or sets the driver license number.
        /// </summary>
        public string DriverLicenseNumber { get; set; }

        /// <summary>
        /// Gets or sets the state in which license was issued.
        /// </summary>
        public Guid? LicenseState { get; set; }

        /// <summary>
        /// Gets or sets the citizen ship status.
        /// </summary>
        public Guid? CitizenShipStatus { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        public Guid? MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public Guid? Gender { get; set; }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            if (this.Email.IsNullOrEmpty() && this.Phone.IsNullOrEmpty())
            {
                results.Add(new ValidationResult("At least an email or a phone must be provided", new List<string>() { "EmailAddress or Phone" }));
            }

            return results;
        }
    }
}
