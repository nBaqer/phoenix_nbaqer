﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceCategory.cs" company="FAMEInc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the SourceCategory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The source category.
    /// </summary>
    public class SourceCategory : IListItem<string, Guid>
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public Guid Value { get; set; }

        /// <summary>
        /// Gets or sets the lists of source type description and source type Id 
        /// </summary>
        public IList<IListItem<string, Guid>> SourceType { get; set; }
    }
}
