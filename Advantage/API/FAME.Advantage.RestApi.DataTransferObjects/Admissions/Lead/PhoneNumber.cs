﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Phone.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The phone.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The phone.
    /// </summary>
    public class PhoneNumber : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        [Required]
        public Guid LeadId { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the extension.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is best.
        /// Providing a true for IsBest will make this phone number and if another best number exist, that other best number will be set to false.
        /// </summary>
        [Required]
        public bool? IsBest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is foreign phone.
        /// </summary>
        public bool? IsForeignPhone { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        [SwaggerExclude]
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        [SwaggerExclude]
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the phone type id.
        /// </summary>
        [Required]
        public Guid? PhoneTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid? StatusId { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        [Required]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the position in which the phone is presented.
        /// </summary>
        public int? Position { get; set; }

        /// <summary>
        /// Gets or sets the is shown on lead page.
        /// </summary>
        [Required]
        public bool? IsShowOnLeadPage { get; set; }
    }
}
