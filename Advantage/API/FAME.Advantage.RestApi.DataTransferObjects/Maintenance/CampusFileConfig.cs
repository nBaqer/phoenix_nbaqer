﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusFileConfig.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the CampusFileConfig type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Maintenance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;

    /// <summary>
    /// The campus file configuration dto
    /// </summary>
    public class CampusFileConfig
    {
        /// <summary>
        /// The Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The campus group id
        /// </summary>
        public Guid CampusGroupId { get; set; }

        /// <summary>
        /// The file storage type
        /// </summary>
        public FileStorageType FileStorageType { get; set; }

        /// <summary>
        /// Applies to all features flag
        /// </summary>
        public bool AppliesToAllFeatures { get; set; }
        /// <summary>
        /// The username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The cloud key
        /// </summary>
        public string CloudKey { get; set; }

        /// <summary>
        /// The modified user
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// The modified date
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// List of custom feature configurations
        /// </summary>
        public IEnumerable<CustomFileFeatureConfig> CustomFeatureConfigurations { get; set; }
    }
}
