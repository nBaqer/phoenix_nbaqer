﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;

namespace FAME.Advantage.RestApi.DataTransferObjects.Maintenance
{
    /// <summary>
    /// Custom file storage configuration for feature dto
    /// </summary>
    public class CustomFileFeatureConfig
    {
        /// <summary>
        /// The Id
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// The campus configuration id
        /// </summary>
        public Guid CampusFileConfigurationId { get; set; }
        
        /// <summary>
        /// The file storage type
        /// </summary>
        public FileStorageType FileStorageType { get; set; }

        /// <summary>
        /// The feature id
        /// </summary>
        public int FeatureId { get; set; }

          /// <summary>
        /// The username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The path
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The cloud key
        /// </summary>
        public string CloudKey { get; set; }

        /// <summary>
        /// The modified user
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// The modified date
        /// </summary>
        public DateTime ModDate { get; set; }
    }
}
