﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiSettings.cs" company="FAME Inc.">
//   FAME Inc.
// </copyright>
// <summary>
//   Defines the WapiSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The wapi settings.
    /// </summary>
    public class WapiSettings : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the consumer key.
        /// </summary>
        public string ConsumerKey { get; set; }

        /// <summary>
        /// Gets or sets the private key.
        /// </summary>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the external url.
        /// </summary>
        public string ExternalUrl { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code operation.
        /// </summary>
        public string CodeOperation { get; set; }

        /// <summary>
        /// Gets or sets the date last execution.
        /// </summary>
        public DateTime DateLastExecution { get; set; }
    }
}
