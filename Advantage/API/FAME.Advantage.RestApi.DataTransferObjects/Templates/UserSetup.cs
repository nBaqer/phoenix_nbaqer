﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserSetup.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserSetup type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.Templates
{
    /// <summary>
    /// The user setup.
    /// </summary>
    public class UserSetup
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public string Link { get; set; }
    }
}
