﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserPassword.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserPassword type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Security
{
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The user password.
    /// </summary>
    public class UserPassword
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the confirm new password.
        /// </summary>
        [Required]
        [MatchValue(MatchType.String, "Password")]
        public string ConfirmNewPassword { get; set; }

        /// <summary>
        /// Gets or sets the security question.
        /// </summary>
        [Required]
        public string SecurityQuestion { get; set; }

        /// <summary>
        /// Gets or sets the security answer.
        /// </summary>
        [Required]
        public string SecurityAnswer { get; set; }

        /// <summary>
        /// Gets or sets the hashed security question.
        /// </summary>
        public string HashedSecurityQuestion { get; set; }

        /// <summary>
        /// Gets or sets the hashed security answer.
        /// </summary>
        public string HashedSecurityAnswer { get; set; }

        /// <summary>
        /// Gets or sets the hashed password.
        /// </summary>
        public string HashedPassword { get; set; }

        /// <summary>
        /// Gets or sets the tenant name.
        /// </summary>
        public string TenantName { get; set; }

        /// <summary>
        /// Gets or sets the advantage version.
        /// </summary>
        public string HasLatestVersion { get; set; }
    }
}
