﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AspNetApplication.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Asp Net Application type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.Security
{
    using System;

    /// <summary>
    /// The asp net application.
    /// </summary>
    public class AspNetApplication
    {
        /// <summary>
        /// Gets or sets the application id.
        /// </summary>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the application name.
        /// </summary>
        public string ApplicationName { get; set; }
    }
}
