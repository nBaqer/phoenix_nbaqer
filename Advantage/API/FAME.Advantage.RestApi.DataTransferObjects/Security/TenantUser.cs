﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantUser.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Tenant User type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.Security
{
    using System;

    /// <summary>
    /// The Tenant User user.
    /// </summary>
    public class TenantUser
    {
        /// <summary>
        /// Gets or sets the tenant user id.
        /// </summary>
        public int TenantUserId { get; set; } 

        /// <summary>
        /// Gets or sets the tenant id.
        /// </summary>
        public int TenantId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is support user.
        /// </summary>
        public bool IsSupportUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is the default tenant.
        /// </summary>
        public bool IsDefaultTenant { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
