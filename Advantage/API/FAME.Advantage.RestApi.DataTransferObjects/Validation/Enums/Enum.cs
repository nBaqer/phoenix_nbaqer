﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Enum.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The validation request type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums
{
    /// <summary>
    /// The validation request type.
    /// </summary>
    public enum ValidationRequestType
    {
        /// <summary>
        /// The get.
        /// </summary>
        Get,

        /// <summary>
        /// The post.
        /// </summary>
        Post,

        /// <summary>
        /// The put.
        /// </summary>
        Put,

        /// <summary>
        /// The delete.
        /// </summary>
        Delete,

        /// <summary>
        /// The all.
        /// </summary>
        All
    }

    /// <summary>
    /// The regex type.
    /// </summary>
    public enum RegexType
    {
        /// <summary>
        /// The phone.
        /// </summary>
        Phone,
        
        /// <summary>
        /// The zip.
        /// </summary>
        Zip,

        /// <summary>
        /// The ssn.
        /// </summary>
        Ssn,

        /// <summary>
        /// The email.
        /// </summary>
        Email
    }

    /// <summary>
    /// The comparison type.
    /// </summary>
    public enum ComparisonType
    {
        /// <summary>
        /// The equal.
        /// </summary>
        Equal,

        /// <summary>
        /// The not equal.
        /// </summary>
        NotEqual,

        /// <summary>
        /// The has value.
        /// </summary>
        HasValue
    }

    /// <summary>
    /// The list content type.
    /// </summary>
    public enum ListContentType
    {
        /// <summary>
        /// The emails.
        /// </summary>
        Emails
    }

    /// <summary>
    /// The match type.
    /// </summary>
    public enum MatchType
    {
        /// <summary>
        /// The string.
        /// </summary>
        String,

        /// <summary>
        /// The integer.
        /// </summary>
        Integer,

        /// <summary>
        /// The boolean.
        /// </summary>
        Boolean
    }
}
