﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Constants
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation.Constants
{
    using System;

    /// <summary>
    /// The constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The email regex.
        /// </summary>
        public const string EmailRegex = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\s*$";

        /// <summary>
        /// The zip regex.
        /// </summary>
        public const string ZipRegex = @"^\d{5}$|^\d{5}-\d{4}$";

        /// <summary>
        /// The us phone regex.
        /// Accepts 10 Digits Number, permitted delimiters are: spaces, dashes, or periods, it allows + and/or 1 leading values to form a number like +1800-000-0000.
        /// </summary>
        public const string UsPhoneRegex = @"^[+]?1?[-\. ]?(\(\d{3}\)?[-\. ]?|\d{3}?[-\. ]?)?\d{3}?[-\. ]?\d{4}$";

        /// <summary>
        /// The international phone regex.
        /// </summary>
        public const string InternationalPhoneRegex = @"^\s*(?:\+?(\d{1,3}))?[- (]*(\d{3})[- )]*(\d{3})[- ]*(\d{4})(?: *[x/#]{1}(\d+))?\s*$";

        /// <summary>
        /// The remove none phone characters regex.
        /// This regex wil remove any character that is no numeric or special character allowed on a phone like are dot, dash, parenthesis, plus sign and space.
        /// </summary>
        public const string RemoveNonePhoneCharactersRegex = @"[^.\-() +0-9]";

        /// <summary>
        /// The ssn regex.
        /// </summary>
        public const string SsnRegex = @"^\d{3}-\d{2}-\d{4}$";

        /// <summary>
        /// The guid empty.
        /// </summary>
        public const string GuidEmpty = "00000000-0000-0000-0000-000000000000";
    }
}