﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListContenttTypeMatchValidation.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ListElementTypeMatch type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Drawing.Text;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The list content type match validation validate that all element in a list are of a given type i.e. emails
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class ListContenttTypeMatchValidation : ValidationAttribute
    {
        /// <summary>
        /// The type.
        /// </summary>
        private ListContentType type;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListContenttTypeMatchValidation"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        public ListContenttTypeMatchValidation(ListContentType type, string errorMessage = null)
        {
            this.type = type;
            this.errorMessage = errorMessage;
        }


        /// <summary>
        /// The is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueType = value.GetType();
            var typeOfIListString = typeof(IList<string>);
            if (value is IList<string> castedValue)
            {
                switch (this.type)
                {
                    case ListContentType.Emails:
                        foreach (var element in castedValue)
                        {
                            try
                            {
                                var addr = new System.Net.Mail.MailAddress(element);
                                if (addr.Address != element)
                                {
                                    this.errorMessage = element + " is not a valid email address";
                                    return new ValidationResult(this.errorMessage);
                                }
                            }
                            catch
                            {
                                this.errorMessage = element + " is not a valid email address";
                                return new ValidationResult(this.errorMessage);
                            }
                        }
                        break;
                    default:
                        break;
                }

                return ValidationResult.Success;
            }
            else
            {
                this.errorMessage = "List of element does not match type.";
                return new ValidationResult(errorMessage: this.errorMessage);
            }
        }
    }
}
