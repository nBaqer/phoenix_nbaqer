﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MatchValue.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the MatchValue type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The match value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class MatchValue : ValidationAttribute
    {
        /// <summary>
        /// The type.
        /// </summary>
        private MatchType type;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The other property name.
        /// </summary>
        private string otherPropertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="MatchValue"/> class. 
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="otherPropertyName"></param>
        /// <param name="errorMessage">
        /// The error message.
        /// </param>
        public MatchValue(MatchType type, string otherPropertyName, string errorMessage = null)
        {
            this.type = type;
            this.errorMessage = errorMessage;
            this.otherPropertyName = otherPropertyName;
        }

        /// <summary>
        /// The is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var regex = string.Empty;
            var otherProperty = validationContext.ObjectType.GetProperty(this.otherPropertyName);

            if (value != null && otherProperty != null)
            {
                var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);
                var otherPropertyType = otherPropertyValue.GetType();

                switch (this.type)
                {
                    case MatchType.String:
                        var castedStringValue = (string)value;
                        if (otherPropertyType != typeof(string))
                        {
                            this.errorMessage = this.otherPropertyName + " type must be an string.";
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }

                        var otherPropertyStringValue = (string)otherPropertyValue;

                        if (castedStringValue == otherPropertyStringValue)
                        {
                            return ValidationResult.Success;
                        }
                        else
                        {
                            this.errorMessage = "Value does not match with " + this.otherPropertyName;
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }
                    case MatchType.Integer:
                        var castedIntValue = (int)value;
                        if (otherPropertyType != typeof(int))
                        {
                            this.errorMessage = this.otherPropertyName + " type must be an integer.";
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }
                        var otherPropertyIntValue = (int)otherPropertyValue;

                        if (castedIntValue == otherPropertyIntValue)
                        {
                            return ValidationResult.Success;
                        }
                        else
                        {
                            this.errorMessage = "Value does not match with " + this.otherPropertyName;
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }
                    case MatchType.Boolean:
                        var castedBoolValue = (bool)value;
                        if (otherPropertyType != typeof(bool))
                        {
                            this.errorMessage = this.otherPropertyName + " type must be an boolean.";
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }

                        var otherPropertyBoolValue = (bool)otherPropertyValue;

                        if (castedBoolValue == otherPropertyBoolValue)
                        {
                            return ValidationResult.Success;
                        }
                        else
                        {
                            this.errorMessage = "Value does not match with " + this.otherPropertyName;
                            return new ValidationResult(errorMessage: this.errorMessage);
                        }
                    default:
                        break;
                }
            }
            else if (value == null && otherProperty == null)
            {
                // both values are null return success
                return ValidationResult.Success;
            }
            else
            {
                // values are different
                this.errorMessage = "Value does not match " + this.otherPropertyName + " value";
                return new ValidationResult(errorMessage: this.errorMessage);
            }

            return new ValidationResult(errorMessage: this.errorMessage);
        }
    }
}
