﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequiredIfOtherPropertyValueIs.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The required if other property value is.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;
    using FAME.Extensions;

    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// The required if other property value is.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequiredIfOtherPropertyValueIs : ValidationAttribute
    {
        /// <summary>
        /// The requests.
        /// </summary>
        private List<ValidationRequestType> requests = new List<ValidationRequestType>();

        /// <summary>
        /// The property name.
        /// </summary>
        private string propertyName;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The property value to match.
        /// </summary>
        private object propertyValueToMatch;

        /// <summary>
        /// The comparison type.
        /// </summary>
        private ComparisonType comparisonType;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequiredIfOtherPropertyValueIs"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="propertyValueToMatch">
        /// The property value to match.
        /// </param>
        /// <param name="comparisonType">
        /// The comparison type.
        /// </param>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="errorMessage">
        /// </param>
        public RequiredIfOtherPropertyValueIs(string propertyName, object propertyValueToMatch, ComparisonType comparisonType, ValidationRequestType request = ValidationRequestType.All, string errorMessage = null)
        {
            this.propertyName = propertyName;
            this.propertyValueToMatch = propertyValueToMatch;
            this.comparisonType = comparisonType;
            this.errorMessage = errorMessage;
            this.requests.Add(request);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequiredIfOtherPropertyValueIs"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        /// <param name="propertyValueToMatch">
        /// The property value to match.
        /// </param>
        /// <param name="comparisonType">
        /// The comparison type.
        /// </param>
        /// <param name="requests">
        /// The requests.
        /// </param>
        /// <param name="errorMessage">
        /// </param>
        public RequiredIfOtherPropertyValueIs(string propertyName, object propertyValueToMatch, ComparisonType comparisonType, ValidationRequestType[] requests, string errorMessage = null)
        {
            this.propertyName = propertyName;
            this.propertyValueToMatch = propertyValueToMatch;
            this.comparisonType = comparisonType;
            this.errorMessage = errorMessage;
            this.requests = requests.ToList();
        }

        /// <summary>
        /// The is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var service = (IHttpContextAccessor)validationContext
                .GetService(typeof(IHttpContextAccessor));

            var requestMethod = service.HttpContext.Request.Method;

            if (requestMethod == null)
            {
                return new ValidationResult(errorMessage: "Cannot define method type");
            }

            var requestList = this.requests.Select(x => x.ToString().ToLower()).ToList();

            // if actual request type matches the request type for validations, or request type is all, or list of method types is empty
            if (requestList.Contains(requestMethod.ToLower()) || requestList.Count == 0 || requestList.Contains("all"))
            {
                var otherProperty = validationContext.ObjectType.GetProperty(this.propertyName);
                this.errorMessage = string.Empty;
                if (otherProperty != null)
                {
                    var otherPropertyValue = otherProperty.GetValue(validationContext.ObjectInstance, null);

                    // check if other property does not exist, if it does NOT, then all good.
                    if (otherPropertyValue == null)
                    {
                        return ValidationResult.Success;
                    }


                    var otherPropertyType = otherPropertyValue.GetType();

                    if (otherPropertyType == typeof(string))
                    {
                        string otherPropertyValueCasted = (string)otherPropertyValue;
                        string matchValue = (string)this.propertyValueToMatch;
                        switch (this.comparisonType)
                        {
                            case ComparisonType.NotEqual:
                                if (otherPropertyValueCasted != matchValue && this.IsValidObject(value))
                                {
                                    return ValidationResult.Success;
                                }
                                else
                                {
                                    if (this.errorMessage.IsNullOrEmpty())
                                    {
                                        if (matchValue == null)
                                        {
                                            this.errorMessage = $"Required when {this.propertyName} property has a value";
                                        }
                                        else
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property is not equal to {this.propertyValueToMatch.ToString()}";
                                        }
                                    }
                                }
                                break;
                            case ComparisonType.Equal:
                                if (otherPropertyValueCasted == matchValue && this.IsValidObject(value))
                                {
                                    return ValidationResult.Success;
                                }
                                else
                                {
                                    if (this.errorMessage.IsNullOrEmpty())
                                    {
                                        if (matchValue == null)
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property does not have a value";
                                        }
                                        else
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property is equal to {this.propertyValueToMatch.ToString()}";
                                        }
                                    }
                                }
                                break;
                            case ComparisonType.HasValue:
                                if (!string.IsNullOrEmpty(otherPropertyValueCasted))
                                {
                                    if (this.IsValidObject(value))
                                    {
                                        return ValidationResult.Success;

                                    }
                                    else
                                    {
                                        if (this.errorMessage.IsNullOrEmpty())
                                        {

                                            this.errorMessage =
                                                $"Required when {this.propertyName} property has a value";
                                        }
                                    }
                                }
                                else
                                {
                                    return ValidationResult.Success;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    if (otherPropertyType == typeof(Guid))
                    {
                        Guid otherPropertyValueCasted = (Guid)otherProperty.GetValue(validationContext.ObjectInstance, null);
                        Guid matchValue = Guid.Parse((string)this.propertyValueToMatch);
                        switch (this.comparisonType)
                        {
                            case ComparisonType.NotEqual:
                                if ((otherPropertyValueCasted != matchValue && this.IsValidObject(value)) || (matchValue == Guid.Empty && this.IsValidObject(value)))
                                {
                                    return ValidationResult.Success;
                                }
                                else
                                {
                                    if (this.errorMessage.IsNullOrEmpty())
                                    {
                                        if (matchValue == Guid.Empty)
                                        {
                                            this.errorMessage = $"Required when {this.propertyName} property has a value";
                                        }
                                        else
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property is equal to {this.propertyValueToMatch.ToString()}";
                                        }
                                    }
                                }
                                break;
                            case ComparisonType.Equal:
                                if (otherPropertyValueCasted == matchValue && this.IsValidObject(value))
                                {
                                    return ValidationResult.Success;
                                }
                                else
                                {
                                    if (this.errorMessage.IsNullOrEmpty())
                                    {
                                        if (matchValue == Guid.Empty)
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property does not have a value";
                                        }
                                        else
                                        {
                                            this.errorMessage =
                                                $"Required when {this.propertyName} property is equal to {this.propertyValueToMatch.ToString()}";
                                        }
                                    }
                                }
                                break;
                            case ComparisonType.HasValue:
                                if (otherPropertyValueCasted != Guid.Empty)
                                {

                                    if (this.IsValidObject(value))
                                    {
                                        return ValidationResult.Success;
                                    }
                                    else
                                    {
                                        if (this.errorMessage.IsNullOrEmpty())
                                        {
                                            this.errorMessage =
                                                  $"Required when {this.propertyName} property has a value";

                                        }
                                    }

                                }
                                else
                                {
                                    return ValidationResult.Success;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return new ValidationResult(errorMessage: this.errorMessage);
        }

        /// <summary>
        /// The is valid object.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsValidObject(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            var type = obj.GetType();

            if (type == typeof(string))
            {
                var castedObj = (string)obj;
                return !castedObj.IsNullOrEmpty();
            }

            if (type == typeof(Guid))
            {
                var castedObj = (Guid)obj;
                return castedObj != Guid.Empty;
            }

            return false;
        }
    }
}
