﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegexMatch.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The regex based validation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation
{
    using System;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;
    using FAME.Extensions;

    /// <summary>
    /// The regex based validation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RegexMatch : ValidationAttribute
    {
        /// <summary>
        /// The type.
        /// </summary>
        private RegexType type;

        /// <summary>
        /// The error message.
        /// </summary>
        private string errorMessage;

        /// <summary>
        /// The is usa property name.
        /// </summary>
        private readonly string isUsaPropertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexMatch"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="errorMessage">
        /// The error Message.
        /// </param>
        public RegexMatch(RegexType type, string errorMessage = null)
        {
            this.type = type;
            this.isUsaPropertyName = string.Empty;
            this.errorMessage = errorMessage;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegexMatch"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="isUsaPropertyName">
        /// The is us property name.
        /// </param>
        /// <param name="errorMessage">
        /// The error Message.
        /// </param>
        public RegexMatch(RegexType type, string isUsaPropertyName, string errorMessage = null)
        {
            this.type = type;
            this.isUsaPropertyName = isUsaPropertyName;
            this.errorMessage = errorMessage;
        }

        /// <summary>
        /// The is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var regex = string.Empty;

            switch (this.type)
            {
                case RegexType.Phone:
                    var isUsaProperty = validationContext.ObjectType.GetProperty(this.isUsaPropertyName);

                    // check if property has been passed
                    if (isUsaProperty != null)
                    {
                        // get value of property being passed
                        var isUsaPropertyValue = (bool)isUsaProperty.GetValue(validationContext.ObjectInstance, null);

                        // if property is from usa  is true
                        if (isUsaPropertyValue)
                        {
                            regex = Constants.Constants.UsPhoneRegex;
                            this.errorMessage = string.Format("Invalid USA Phone Number.");
                        }
                        else
                        {
                            regex = Constants.Constants.InternationalPhoneRegex;
                            this.errorMessage = string.Format("Invalid Phone Number.");
                        }
                    }
                    else // default US phone regex
                    {
                        regex = Constants.Constants.UsPhoneRegex;
                        this.errorMessage = string.Format("Invalid USA Phone Number.");
                    }

                    break;
                case RegexType.Zip:
                    {
                        this.errorMessage = string.Format("Invalid Zip Code.");
                        regex = Constants.Constants.ZipRegex;
                        break;
                    }
                case RegexType.Ssn:
                    {
                        this.errorMessage = string.Format("Invalid SSN.");
                        regex = Constants.Constants.SsnRegex;
                        break;
                    }
                case RegexType.Email:
                    {
                        this.errorMessage = string.Format("Invalid EmailAddress.");
                        regex = Constants.Constants.EmailRegex;
                        break;
                    }
                default:
                    break;
            }

            if (value != null)
            {
                var valueType = value.GetType();

                // only applies for string properties
                if (valueType == typeof(string))
                {
                    var stringToValidate = (string)value;
                    if (stringToValidate.IsNullOrEmpty())
                    {
                        return ValidationResult.Success;

                    }

                    if (!regex.IsNullOrEmpty() && stringToValidate.IsMatch(regex))
                    {
                        this.errorMessage = string.Empty;
                        return ValidationResult.Success;
                    }
                }
            }
            else
            {
                return ValidationResult.Success;
            }

            if (this.errorMessage.IsNullOrEmpty())
            {
                this.errorMessage = string.Format("Value does not match regex");
            }

            return new ValidationResult(errorMessage: this.errorMessage);
        }
    }
}
