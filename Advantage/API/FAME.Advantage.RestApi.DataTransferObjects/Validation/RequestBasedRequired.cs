﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequestBasedRequired.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The custom required attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Validation
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Runtime.CompilerServices;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;
    using FAME.Extensions;

    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// The custom required attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RequestBasedRequired : ValidationAttribute
    {
        /// <summary>
        /// The requests.
        /// </summary>
        private List<ValidationRequestType> requests = new List<ValidationRequestType>();

        /// <summary>
        /// The property name.
        /// </summary>
        private readonly string propertyName;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestBasedRequired"/> class.
        /// </summary>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        public RequestBasedRequired([CallerMemberName] string propertyName = null)
        {
            this.requests.Add(ValidationRequestType.All);
            this.propertyName = propertyName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestBasedRequired"/> class.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        public RequestBasedRequired(ValidationRequestType request, [CallerMemberName] string propertyName = null)
        {
            this.requests.Add(request);
            this.propertyName = propertyName;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestBasedRequired"/> class.
        /// </summary>
        /// <param name="requests">
        /// The requests.
        /// </param>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        public RequestBasedRequired(ValidationRequestType[] requests, [CallerMemberName] string propertyName = null)
        {
            this.requests = this.requests.ToList();
            this.propertyName = propertyName;
        }

        /// <summary>
        /// The is valid.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="validationContext">
        /// The validation context.
        /// </param>
        /// <returns>
        /// The <see cref="ValidationResult"/>.
        /// </returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var service = (IHttpContextAccessor)validationContext
                .GetService(typeof(IHttpContextAccessor));

            var requestMethod = service.HttpContext.Request.Method;

            var requestList = this.requests.Select(x => x.ToString().ToLower()).ToList();

            if (requestMethod == null)
            {
                return new ValidationResult(errorMessage: "Error: request method could not be identified.");
            }

            // if actual request type matches the request type for validations
            if (requestList.Contains(requestMethod.ToLower()) || requestList.Count == 0 || requestList.Contains("all"))
            {
                if (object.Equals(value, null))
                {
                    return new ValidationResult(errorMessage: $"The {this.propertyName ?? ""} field is required.");
                }

                var type = value.GetType();

                if (type == typeof(Guid))
                {
                    Guid valueToValidate = (Guid)value;

                    if (valueToValidate != Guid.Empty)
                    {
                        return ValidationResult.Success;
                    }

                    return new ValidationResult(errorMessage: $"The {this.propertyName ?? ""} field is required.");
                }

                if (type == typeof(string))
                {
                    string valueToValidate = (string)value;

                    if (!valueToValidate.IsNullOrEmpty())
                    {
                        return ValidationResult.Success;
                    }
                    return new ValidationResult(errorMessage: $"The { this.propertyName ?? ""} field  is required.");
                }
            }
            else
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(errorMessage: "Error: Could not validate required field.");
        }
    }
}
