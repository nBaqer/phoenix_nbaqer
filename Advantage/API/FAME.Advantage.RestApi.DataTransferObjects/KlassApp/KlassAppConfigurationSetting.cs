﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppConfigurationSetting.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the KlassAppConfigurationSetting type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.KlassApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The klass app configuration setting.
    /// </summary>
    public class KlassAppConfigurationSetting
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the advantage id.
        /// </summary>
        public string AdvantageId { get; set; }

        /// <summary>
        /// Gets or sets the klass app id.
        /// </summary>
        public int KlassAppId { get; set; }

        /// <summary>
        /// Gets or sets the klass entity id.
        /// </summary>
        public int KlassEntityId { get; set; }

        /// <summary>
        /// Gets or sets the klass operation type id.
        /// </summary>
        public int KlassOperationTypeId { get; set; }

        /// <summary>
        /// Gets or sets the klass operation type code.
        /// </summary>
        public string KlassOperationTypeCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is custom field.
        /// </summary>
        public bool IsCustomField { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the item status.
        /// </summary>
        public char ItemStatus { get; set; }

        /// <summary>
        /// Gets or sets the item value.
        /// </summary>
        public string ItemValue { get; set; }

        /// <summary>
        /// Gets or sets the item label.
        /// </summary>
        public string ItemLabel { get; set; }

        /// <summary>
        /// Gets or sets the item value type.
        /// </summary>
        public string ItemValueType { get; set; }

        /// <summary>
        /// Gets or sets the creation date.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the last operation log.
        /// </summary>
        public string LastOperationLog { get; set; }
    }
}
