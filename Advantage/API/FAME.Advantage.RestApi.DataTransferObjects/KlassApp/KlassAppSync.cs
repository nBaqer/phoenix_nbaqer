﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppSync.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The klass app student summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.KlassApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    /// <summary>
    /// The KlassAppSync interface.
    /// </summary>
    public class KlassAppSync
    {
        /// <summary>
        /// Gets or sets enrollment id. 
        /// </summary>
        public string EnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets campus Id
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Gets or sets TimeStamp
        /// </summary>
        public string TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets StudentNumber
        /// </summary>
        public string StudentNumber { get; set; }

        /// <summary>
        /// Gets or sets Email
        /// </summary>
        public string Email { get; set; }
    }
}
