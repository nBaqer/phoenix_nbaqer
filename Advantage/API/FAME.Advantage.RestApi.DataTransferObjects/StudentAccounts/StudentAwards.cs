﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwards.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentAwards type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts
{
    using System;

    /// <summary>
    /// The student awards.
    /// </summary>
    public class StudentAwards
    {
        /// <summary>
        /// Gets or sets the student award id.
        /// </summary>
        public Guid StudentAwardId { get; set; }

        /// <summary>
        /// Gets or sets the award id.
        /// </summary>
        public int? AwardId { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the award type id.
        /// </summary>
        public Guid AwardTypeId { get; set; }

        /// <summary>
        /// Gets or sets the academic year id.
        /// </summary>
        public Guid? AcademicYearId { get; set; }

        /// <summary>
        /// Gets or sets the lender id.
        /// </summary>
        public Guid? LenderId { get; set; }

        /// <summary>
        /// Gets or sets the servicer id.
        /// </summary>
        public Guid? ServicerId { get; set; }

        /// <summary>
        /// Gets or sets the guarantor id.
        /// </summary>
        public Guid? GuarantorId { get; set; }

        /// <summary>
        /// Gets or sets the gross amount.
        /// </summary>
        public decimal GrossAmount { get; set; }

        /// <summary>
        /// Gets or sets the loan fees.
        /// </summary>
        public decimal? LoanFees { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the award start date.
        /// </summary>
        public DateTime AwardStartDate { get; set; }

        /// <summary>
        /// Gets or sets the award end date.
        /// </summary>
        public DateTime AwardEndDate { get; set; }

        /// <summary>
        /// Gets or sets the disbursements.
        /// </summary>
        public int Disbursements { get; set; }

        /// <summary>
        /// Gets or sets the loan id.
        /// </summary>
        public string LoanId { get; set; }

        /// <summary>
        /// Gets or sets the emasfund code.
        /// </summary>
        public string EmasfundCode { get; set; }

        /// <summary>
        /// Gets or sets the financial aid id.
        /// </summary>
        public string FinancialAidId { get; set; }

        /// <summary>
        /// Gets or sets the award code.
        /// </summary>
        public string AwardCode { get; set; }

        /// <summary>
        /// Gets or sets the award sub code.
        /// </summary>
        public string AwardSubCode { get; set; }

        /// <summary>
        /// Gets or sets the award status.
        /// </summary>
        public string AwardStatus { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
