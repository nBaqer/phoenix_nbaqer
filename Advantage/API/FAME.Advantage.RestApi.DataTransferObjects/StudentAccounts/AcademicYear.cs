﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicYear.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AcademicYear type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts
{
    using System;

    /// <summary>
    /// The academic year.
    /// </summary>
    public class AcademicYear
    {
        /// <summary>
        /// Gets or sets the academic year id.
        /// </summary>
        public Guid AcademicYearId { get; set; }

        /// <summary>
        /// Gets or sets the academic year code.
        /// </summary>
        public string AcademicYearCode { get; set; }

        /// <summary>
        /// Gets or sets the academic year description.
        /// </summary>
        public string AcademicYearDescription { get; set; }
        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public Guid CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModDate { get; set; }

    }
}
