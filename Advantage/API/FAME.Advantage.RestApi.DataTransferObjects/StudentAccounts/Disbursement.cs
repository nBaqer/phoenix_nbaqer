﻿
namespace FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The disbursement object
    /// </summary>
    public class Disbursement
    {
        /// <summary>
        /// The disbusement id
        /// </summary>
        public Guid DisbursementId { get; set; }
        /// <summary>
        /// The student award id
        /// </summary>
        public Guid StudentAwardId { get; set; }
        /// <summary>
        /// The expected date
        /// </summary>
        public DateTime? ExpectedDate { get; set; }
        /// <summary>
        /// The amount
        /// </summary>
        public decimal? Amount { get; set; }
        /// <summary>
        /// The reference identifier
        /// </summary>
        public string Reference { get; set; }
        /// <summary>
        /// The mod user
        /// </summary>
        public string ModUser { get; set; }
        /// <summary>
        /// The mod date
        /// </summary>
        public DateTime? ModDate { get; set; }
        /// <summary>
        /// The sequence number
        /// </summary>
        public int SequenceNumber { get; set; }

    }
}
