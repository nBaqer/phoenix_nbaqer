﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts.AFA
{
    /// <summary>
    /// The student awards with custom fields for AFA
    /// </summary>
    public class StudentAwards : StudentAccounts.StudentAwards
    {
        /// <summary>
        /// The afa student id
        /// </summary>
        public string AfaStudentId { get; set; }

        /// <summary>
        /// Title IV Award type
        /// </summary>
        public string TitleIvAwardType { get; set; }
        /// <summary>
        /// The award year
        /// </summary>
        public string AwardYear { get; set; }
        /// <summary>
        /// The deleted flag
        /// </summary>
        public bool Deleted { get; set; }
    }
}
