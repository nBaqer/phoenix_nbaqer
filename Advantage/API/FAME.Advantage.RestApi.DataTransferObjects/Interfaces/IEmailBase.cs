﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailBase.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IEmailBase interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The EmailBase interface.
    /// </summary>
    public interface IEmailBase
    {
        /// <summary>
        /// Gets or sets the from.
        /// </summary>
        string From { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        string Body { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        string Link { get; set; }

        /// <summary>
        /// Gets or sets the to.
        /// </summary>
        IEnumerable<string> To { get; set; }
    }
}
