﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodsWorkDays.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The periods work days.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The periods work days.
    /// </summary>
    public class PeriodsWorkDays
    {
        /// <summary>
        /// Gets or sets the period id work day id.
        /// </summary>
        public Guid PeriodIdWorkDayId { get; set; }

        /// <summary>
        /// Gets or sets the period id.
        /// </summary>
        public Guid PeriodId { get; set; }

        /// <summary>
        /// Gets or sets the work day id.
        /// </summary>
        public Guid WorkDayId { get; set; }

        /// <summary>
        /// Gets or sets the work day description.
        /// </summary>
        public string WorkDayDescription { get; set; }

        /// <summary>
        /// Gets or sets the view order.
        /// </summary>
        public int ViewOrder { get; set; }

    }
}
