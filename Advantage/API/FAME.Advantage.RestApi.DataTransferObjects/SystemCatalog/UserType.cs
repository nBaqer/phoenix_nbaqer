﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserType.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    /// <summary>
    /// The user type.
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }
    }
}
