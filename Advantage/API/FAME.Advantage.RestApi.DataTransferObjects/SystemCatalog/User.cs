﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="User.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the User Dto
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The user.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the user type.
        /// </summary>
        public int UserTypeId { get; set; }

        /// <summary>
        /// Gets or sets the user type code.
        /// </summary>
        public string UserTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the confirm password.
        /// </summary>
        public string ConfirmPassword { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the user status.
        /// </summary>
        public Guid UserStatusId { get; set; }

        /// <summary>
        /// Gets or sets the user status.
        /// </summary>
        public string UserStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether account active.
        /// </summary>
        public bool AccountActive { get; set; }

        /// <summary>
        /// Gets or sets the default module.
        /// </summary>
        public int DefaultModuleId { get; set; }

        /// <summary>
        /// Gets or sets the user roles.
        /// </summary>
        public List<UserRole> UserRoles { get; set; } = new List<UserRole>();

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has passed.
        /// </summary>
        public bool HasPassed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to send email.
        /// </summary>
        public bool SendEmail { get; set; }
    }
}
