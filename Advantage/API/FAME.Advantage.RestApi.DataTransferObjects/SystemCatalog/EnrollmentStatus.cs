﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentStatus.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Enrollment Status type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The system status class will have the status types like Active and Droped which is gives the student enrollment details.
    /// </summary>
    /// <remarks>
    /// The object of enrollment status is a data transfer object which holds the parameters to display the enrollments.
    /// </remarks>
    public class EnrollmentStatus
    {
        /// <summary>
        /// Gets or sets the Student id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is Active Enrollments.
        /// </summary>
        public bool IsActiveEnrollments { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is Dropped Enrollments.
        /// </summary>
        public bool IsDroppedEnrollments { get; set; }
    }
}
