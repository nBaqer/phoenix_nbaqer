﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusInformation.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusInformation type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The campus information.
    /// </summary>
    public class CampusInformation
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the country id.
        /// </summary>
        public string CountryId { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public string StateId { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Gets or sets the  code.
        /// </summary>
        public string Code { get; set; }
    }
}
