﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditSummary.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The credit summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The credit summary.
    /// </summary>
    public class CreditSummary
    {
        /// <summary>
        /// Gets or sets the student enroll id.
        /// </summary>
        public Guid StudentEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the term description.
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// Gets or sets the req id.
        /// </summary>
        public Guid ReqId { get; set; }

        /// <summary>
        /// Gets or sets the req description.
        /// </summary>
        public string ReqDescription { get; set; }

        /// <summary>
        /// Gets or sets the cls section id.
        /// </summary>
        public string ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the credits earned.
        /// </summary>
        public decimal? CreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets the credits attempted.
        /// </summary>
        public decimal? CreditsAttempted { get; set; }

        /// <summary>
        /// Gets or sets the current score.
        /// </summary>
        public decimal? CurrentScore { get; set; }

        /// <summary>
        /// Gets or sets the current grade.
        /// </summary>
        public string CurrentGrade { get; set; }

        /// <summary>
        /// Gets or sets the final score.
        /// </summary>
        public decimal? FinalScore { get; set; }

        /// <summary>
        /// Gets or sets the final grade.
        /// </summary>
        public string FinalGrade { get; set; }

        /// <summary>
        /// Gets or sets the completed.
        /// </summary>
        public string Completed { get; set; }

        /// <summary>
        /// Gets or sets the final GPA.
        /// </summary>
        public decimal? FinalGpa { get; set; }

        /// <summary>
        /// Gets or sets the product weighted average credits GPA.
        /// </summary>
        public decimal? ProductWeightedAverageCreditsGpa { get; set; }

        /// <summary>
        /// Gets or sets the count weighted average credits.
        /// </summary>
        public decimal? CountWeightedAverageCredits { get; set; }

        /// <summary>
        /// Gets or sets the product simple average credits GPA.
        /// </summary>
        public decimal? ProductSimpleAverageCreditsGpa { get; set; }

        /// <summary>
        /// Gets or sets the count simple average credits.
        /// </summary>
        public decimal? CountSimpleAverageCredits { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the term GPA simple.
        /// </summary>
        public decimal? TermGpaSimple { get; set; }

        /// <summary>
        /// Gets or sets the term GPA weighted.
        /// </summary>
        public decimal? TermGpaWeighted { get; set; }

        /// <summary>
        /// Gets or sets the course credits.
        /// </summary>
        public decimal? CourseCredits { get; set; }

        /// <summary>
        /// Gets or sets the cumulative GPA.
        /// </summary>
        public decimal? CumulativeGpa { get; set; }

        /// <summary>
        /// Gets or sets the cumulative GPA simple.
        /// </summary>
        public decimal? CumulativeGpaSimple { get; set; }

        /// <summary>
        /// Gets or sets the FA credits earned.
        /// </summary>
        public decimal? FaCreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets the cum average.
        /// </summary>
        public decimal? CumAverage { get; set; }

        /// <summary>
        /// Gets or sets the average.
        /// </summary>
        public decimal? Average { get; set; }

        /// <summary>
        /// Gets or sets the term start date.
        /// </summary>
        public DateTime? TermStartDate { get; set; }
    }
}
