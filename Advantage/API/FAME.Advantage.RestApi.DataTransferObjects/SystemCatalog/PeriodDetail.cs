﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodDetail.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PeriodDetail type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The period detail.
    /// </summary>
    public class PeriodDetail
    {
        /// <summary>
        /// Gets or sets the period id.
        /// </summary>
        public Guid PeriodId { get; set; }

        /// <summary>
        /// Gets or sets the schedule by workday.
        /// </summary>
        public List<IListItem<string, decimal>> ScheduleByWorkday { get; set; }

        /// <summary>
        /// The start time interval.
        /// </summary>
        public DateTime StartTimeInterval { get; set; }

        /// <summary>
        /// The end time interval.
        /// </summary>
        public DateTime EndTimeInterval { get; set; }
    }
}