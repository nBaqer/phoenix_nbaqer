﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Catalog.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the catalog for the API.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The catalog.
    /// </summary>
    public class Catalog
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the phone type list.
        /// </summary>
        public IList<IListItem<string, Guid>> PhoneTypes { get; set; }

        /// <summary>
        /// Gets or sets the address type list.
        /// </summary>
        public IList<IListItem<string, Guid>> AddressTypes { get; set; }

        /// <summary>
        /// Gets or sets the countries type list.
        /// </summary>
        public IList<IListItem<string, Guid>> CountriesTypes { get; set; }

        /// <summary>
        /// Gets or sets the states list.
        /// </summary>
        public IList<IListItem<string, Guid>> States { get; set; }

        /// <summary>
        /// Gets or sets the interest area list.
        /// </summary>
        public IList<IListItem<string, Guid>> InterestAreas { get; set; }

        /// <summary>
        /// Gets or sets the source category list.
        /// </summary>
        public IList<SourceCategory> SourceCategories { get; set; }

        /// <summary>
        /// Gets or sets the email type list.
        /// </summary>
        public IList<IListItem<string, Guid>> EmailTypes { get; set; }

        /// <summary>
        /// Gets or sets the admissions reps.
        /// </summary>
        public IList<IListItem<string, Guid>> AdmissionsReps { get; set; }

        /// <summary>
        /// Gets or sets the lead statuses.
        /// </summary>
        public IList<IListItem<string, Guid>> LeadStatuses { get; set; }

        /// <summary>
        /// Gets or sets the genders.
        /// </summary>
        public IList<Gender> Genders { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        public IList<MaritalStatus> MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the citizen ship.
        /// </summary>
        public IList<CitizenShip> CitizenShip { get; set; }
    }
}
