﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTermsOfUse.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserTermsOfUse type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The user terms of use.
    /// </summary>
    public class UserTermsOfUse
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        [Required]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        [Required]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether has accepted latest terms of use.
        /// </summary>
        public bool HasAcceptedLatestTermsOfUse { get; set; }

        /// <summary>
        /// Gets or sets the last version accepted.
        /// </summary>
        public string LastVersionAccepted { get; set; }

        /// <summary>
        /// Gets or sets the last version accepted date.
        /// </summary>
        public DateTime? LastVersionAcceptedDate { get; set; }

    }
}
