﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatus.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DropStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    
    /// <summary>
    /// The DropStatus class will have the properties that allows you to find a list of status.
    /// </summary>
    /// <remarks>
    /// The object of DropStatus is data transfer object which holds the parameters to search the status
    /// </remarks>
    public class DropStatus
    {
        /// <summary>
        /// Gets or sets the campus id of type guid in which status needs to be searched.
        /// </summary>
        public Guid CampusId { get; set; }
    }
}
