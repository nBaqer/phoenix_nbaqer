﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaritalStatus.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the MaritalStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The marital status.
    /// </summary>
    public class MaritalStatus : IListItem<string, Guid>
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public Guid Value { get; set; }

        /// <summary>
        /// Gets or sets the AFA value (The number that they get from ISIR).
        /// </summary>
        public string AfaValue { get; set; }
    }
}
