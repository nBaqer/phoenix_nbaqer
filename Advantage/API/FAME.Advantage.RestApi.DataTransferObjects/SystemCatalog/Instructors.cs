﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Instructors.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//  Instructors info.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// Instructors info.
    /// </summary>
    public class Instructors
    {
        /// <summary>
        /// Gets or sets User id.
        /// </summary>
        public Guid UserID { get; set; }

        /// <summary>
        /// Gets or sets Instructors FullName.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets Instructors DisplayName.
        /// </summary>
        public string DisplayName { get; set; }
    }
}
