﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccreditingAgency.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// 
//   Defines the AccreditingAgency type.
// 
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// 
    /// The AccreditingAgency.
    /// 
    public class AccreditingAgency
    {
        /// <summary>
        /// Gets or sets the AccreditingAgencyId.
        /// </summary>
        public int AccreditingAgencyId { get; set; }
        /// <summary>
        /// Gets or sets the Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }
    }
}
