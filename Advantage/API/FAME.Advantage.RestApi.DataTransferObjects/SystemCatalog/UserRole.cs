﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRole.cs" company="FAME Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserRole type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The user role.
    /// </summary>
    public class UserRole
    {
        /// <summary>
        /// Gets or sets the campus group user role id.
        /// </summary>
        public Guid CampusGroupUserRoleId { get; set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public Guid CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public int SysRoleId { get; set; }

        /// <summary>
        /// Gets or sets the role id.
        /// </summary>
        public Guid UserId { get; set; }
    }
}
