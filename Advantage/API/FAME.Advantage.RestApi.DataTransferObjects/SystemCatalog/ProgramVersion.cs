﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersion.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the program version type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The program version.
    /// </summary>
    public class ProgramVersion
    {
        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the program version code.
        /// </summary>
        public string ProgramVersionCode { get; set; }

        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string ProgramVersionDescription { get; set; }

        /// <summary>
        /// Gets or sets the camp group id.
        /// </summary>
        public Guid CampusGroupId{ get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the type of the registration for the program.
        /// Given you set this value to by Class, then the user have to manually configure terms and class section, additionally it needs to register he students manually
        /// Given you set this value to By Program, then the user does not have to configure term or class section, since those are configured during the set up of the program version and program version definition.
        /// Upon enrollment the students are automatically registered into the term and class section.
        /// </summary>
        public Enums.ProgramRegistrationType ProgramRegistrationType { get; set; }
    }
}
