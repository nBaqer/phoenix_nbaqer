﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhoneType.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PhoneType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The lead phone.
    /// </summary>
    public class PhoneType
    {
        /// <summary>
        /// Gets or sets the phone type id.
        /// </summary>
        public Guid PhoneTypeId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public string StatusId { get; set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public string CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }
    }
}
