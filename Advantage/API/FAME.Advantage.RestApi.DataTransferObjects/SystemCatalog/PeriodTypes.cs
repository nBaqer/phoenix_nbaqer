﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodTypes.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the PeriodTypes type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The period types data transfer object.
    /// </summary>
    public class PeriodTypes
    {
        /// <summary>
        /// Gets or sets the calculation period type's id.
        /// </summary>
        public Guid CalculationPeriodTypeId { get; set; }

        /// <summary>
        /// Gets or sets the period type's code.
        /// </summary>
        public string Description { get; set; }
    }
}
