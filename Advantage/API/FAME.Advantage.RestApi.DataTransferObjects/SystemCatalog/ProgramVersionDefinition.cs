﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionDefinition.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramVersionDefinition type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The program version definition.
    /// </summary>
    public class ProgramVersionDefinition
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the course id.
        /// </summary>
        public Guid CourseId { get; set; }

        /// <summary>
        /// Gets or sets the course sequence.
        /// </summary>
        public int CourseSequence { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is required.
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets the count.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the hours.
        /// </summary>
        public decimal Hours { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is track for completion.
        /// </summary>
        public bool IsTrackForCompletion { get; set; }

        /// <summary>
        /// Gets or sets the credits.
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// Gets or sets the grade system detail id.
        /// </summary>
        public Guid GradeSystemDetailId { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the term number.
        /// </summary>
        public int TermNumber { get; set; }

        /// <summary>
        /// Gets or sets the course weight.
        /// </summary>
        public float CourseWeight { get; set; }
    }
}
