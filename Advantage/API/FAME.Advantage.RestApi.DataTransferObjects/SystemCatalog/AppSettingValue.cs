﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppSettingValue.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The app setting value.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The app setting value.
    /// </summary>
    public class AppSettingValue
    {
        /// <summary>
        /// Gets or sets the value id.
        /// </summary>
        public Guid ValueId { get; set; }

        /// <summary>
        /// Gets or sets the setting id.
        /// </summary>
        public int SettingId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid? CampusId { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the Modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the Modified date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether active.
        /// </summary>
        public bool Active { get; set; }
    }
}
