﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupsWithCampus.cs" company="Fame Inc">
//   Fame Inc. 2019
// </copyright>
// <summary>
//   Defines the CampusGroup type with campus.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The campus group.
    /// </summary>
    public class CampusGroupsWithCampus
    {
        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public Guid CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the camp group Name.
        /// </summary>
        public string CampusGroupName { get; set; }

        /// <summary>
        /// Gets or sets the Campuses.
        /// </summary>
        public List<CampusDTO> Campuses { get; set; }

    }
    public class CampusDTO
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid? CampusId { get; set; }

        /// <summary>
        /// Gets or sets the camp group Name.
        /// </summary>
        public string CampusName { get; set; }
    }
}
