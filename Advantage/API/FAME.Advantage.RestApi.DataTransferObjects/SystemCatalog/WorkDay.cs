﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkDay.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The work day.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The work day.
    /// </summary>
    public class WorkDay
    {
        /// <summary>
        /// Gets or sets the work days id.
        /// </summary>
        public Guid WorkDaysId { get; set; }

        /// <summary>
        /// Gets or sets the work days description.
        /// </summary>
        public string WorkDaysDescription { get; set; }

        /// <summary>
        /// Gets or sets the view order.
        /// </summary>
        public int ViewOrder { get; set; }
    }
}
