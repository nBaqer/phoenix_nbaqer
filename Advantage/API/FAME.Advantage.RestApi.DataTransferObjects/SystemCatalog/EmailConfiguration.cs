﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailConfiguration.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The email configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The email configuration.
    /// </summary>
    public class EmailConfiguration
    {
        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the passwrod.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is ssl.
        /// </summary>
        public bool IsSSL { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is o auth.
        /// </summary>
        public bool IsOAuth { get; set; }

        /// <summary>
        /// Gets or sets the oath client secret.
        /// </summary>
        public string OathClientSecret { get; set; }

        /// <summary>
        /// Gets or sets the token.
        /// </summary>
        public string Token { get; set; }

    }
}
