﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Widget.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Widget type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// A widget
    /// </summary>
    public class Widget
    {
        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        public Guid WidgetId { get; set; }

        /// <summary>
        /// Gets or sets the Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the Rows.
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// Gets or sets the Columns.
        /// </summary>
        public int Columns { get; set; }

        /// <summary>
        /// Gets or sets the HasSetting.
        /// </summary>
        public bool HasSetting { get; set; }

        /// <summary>
        /// Gets or sets the PositionX.
        /// </summary>
        public int PositionX { get; set; }

        /// <summary>
        /// Gets or sets the PositionY.
        /// </summary>
        public int PositionY { get; set; }

        /// <summary>
        /// Gets or sets the IsVisible.
        /// </summary>
        public bool IsVisible { get; set; }
    }
}
