﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusCode.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusGroup type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The campus group.
    /// </summary>
    public class StatusCode
    {
        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public string StatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets status code description.
        /// </summary>
        public string StatusCodeDescription { get; set; }

        /// <summary>
        /// Gets or sets the system status code id.
        /// </summary>
        public int SystemStatusCodeId { get; set; }
    }
}
