﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserSearchInput.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserSearchInput type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The UserSearchInput class will have the search filter properties that allows you to find a list of system user(s).
    /// </summary>
    /// <remarks>
    /// The object of UserSearchInput is data transfer object which holds the parameters to search the users
    /// </remarks>
    public class UserSearchInput
    {
        /// <summary>
        /// Gets or sets the user type id of type integer which will filter by system user type if given
        /// </summary>
        public int? UserTypeId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public bool? Status { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// Filter will be used as the text to search the user based on Full Name, EmailAddress or Display Name and it should be of minimum 3 letter and maximum of 50 letters
        /// </summary>
        [MinLength(3, ErrorMessage = "Minimum number of characters allowed is 3")]
        [MaxLength(50, ErrorMessage = "Maximum number of characters allowed is 50")]
        public string Filter { get; set; }

    }
}
