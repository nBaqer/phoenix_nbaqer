﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserSearchOutput.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the User search output type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The UserSearchOutput class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of UserSearchOutput is data transfer object which holds the parameters to return the system users
    /// </remarks>
    public class UserSearchOutput
    {
        /// <summary>
        /// Gets or sets the value for the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the value for the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the value for the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the Type
        /// </summary>
        public string UserType { get; set; }
      
        /// <summary>
        /// Gets or sets the Status
        /// </summary>
        public string Status { get; set; }
    }
}
