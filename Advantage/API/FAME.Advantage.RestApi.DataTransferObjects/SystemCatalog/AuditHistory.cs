﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditHistory.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AuditHistory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The audit history.
    /// </summary>
    public class AuditHistory
    {
        /// <summary>
        /// Gets or sets the audit hist id.
        /// </summary>
        public Guid AuditHistId { get; set; }

        /// <summary>
        /// Gets or sets the table name.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the event.
        /// </summary>
        public string Event { get; set; }

        /// <summary>
        /// Gets or sets the event rows.
        /// </summary>
        public int EventRows { get; set; }

        /// <summary>
        /// Gets or sets the event date.
        /// </summary>
        public DateTime? EventDate { get; set; }

        /// <summary>
        /// Gets or sets the app name.
        /// </summary>
        public string AppName { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}
