﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroup.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusGroup type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System;

    /// <summary>
    /// The campus group.
    /// </summary>
    public class CampusGroup
    {
        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public Guid CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the camp group code.
        /// </summary>
        public string CampusGroupCode { get; set; }

        /// <summary>
        /// Gets or sets the camp group description.
        /// </summary>
        public string CampusGroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets the is all campus group.
        /// </summary>
        public bool? IsAllCampusGroup { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid? CampusId { get; set; }
    }
}
