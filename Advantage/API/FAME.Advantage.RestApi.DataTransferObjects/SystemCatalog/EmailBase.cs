﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailBase.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The email base type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog
{
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

    /// <summary>
    /// The email base.
    /// </summary>
    public class EmailBase: IEmailBase

    {
        /// <summary>
        /// Gets or sets the from.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Gets or sets the to.
        /// </summary>
        [ListContenttTypeMatchValidation(ListContentType.Emails)]
        public IEnumerable<string> To { get; set; }

        /// <summary>
        /// Gets or sets the user type.
        /// </summary>
        public string UserTypeCode { get; set; }
    }
}
