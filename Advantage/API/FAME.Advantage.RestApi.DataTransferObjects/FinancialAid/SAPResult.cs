﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SAPResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SAP Result Object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.FinancialAid
{
    using System;

    /// <summary>
    /// The SAP Result.
    /// </summary>
    public class SAPResult
    {
        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }
        /// <summary>
        /// Gets or sets the qualitative.
        /// </summary>
        public decimal Qualitative { get; set; }
        /// <summary>
        /// Gets or sets the quantitative.
        /// </summary>
        public decimal Quantitative { get; set; }
        /// <summary>
        /// Gets or sets the actual hours.
        /// </summary>
        public decimal ActualHours { get; set; }
        /// <summary>
        /// Gets or sets the scheduled hours.
        /// </summary>
        public decimal ScheduledHours { get; set; }
        /// <summary>
        /// Gets or sets the passed.
        /// </summary>
        public bool Passed { get; set; }
        /// <summary>
        /// Gets or sets the passed message.
        /// </summary>
        public string PassedMessage { get; set; }
        /// <summary>
        /// Gets or sets the failed message.
        /// </summary>
        public string FailedMessage { get; set; }
        /// <summary>
        /// Gets or sets the sap detail.
        /// </summary>
        public SAPPolicyDetail SapDetail { get; set; }
        /// <summary>
        /// Gets or sets the checkpoint date.
        /// </summary>
        public DateTime CheckpointDate { get; set; }
    }
}
