﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionSAP.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SAP core object for program version.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.FinancialAid
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The ProgramVersionSAP.
    /// </summary>
    public class ProgramVersionSAP
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the enrollment id.
        /// </summary>
        public Guid EnrollmentId { get; set; }
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }
        /// <summary>
        /// Gets or sets the sap id.
        /// </summary>
        public Guid? SapId { get; set; }
        /// <summary>
        /// Gets or sets the title IV sap id.
        /// </summary>
        public Guid? TitleIVSapId { get; set; }
        /// <summary>
        /// Gets or sets the system status id.
        /// </summary>
        public int SysStatusId { get; set; }
        /// <summary>
        /// Gets or sets the enrollment start date.
        /// </summary>
        public DateTime EnrollmentStartDate { get; set; }
        /// <summary>
        /// Gets or sets the credits.
        /// </summary>
        public decimal Credits { get; set; }
        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string ProgramVersionDescription { get; set; }
        /// <summary>
        /// Gets or sets the trigger unit type id.
        /// </summary>
        public int TriggerUnitTypeId { get; set; }
        /// <summary>
        /// Gets or sets the trigger offset type id.
        /// </summary>
        public int TriggerOffsetTypeId { get; set; }
        /// <summary>
        /// Gets or sets the minimum term gpa.
        /// </summary>
        public decimal MinimumTermGPA { get; set; }
        /// <summary>
        /// Gets or sets the term gpa over.
        /// </summary>
        public decimal TermGPAOver { get; set; }
        /// <summary>
        /// Gets or sets the track externship attendance.
        /// </summary>
        public bool TrackExternshipAttendance { get; set; }
        /// <summary>
        /// Gets or sets the include transfer hours.
        /// </summary>
        public bool IncludeTransferHours { get; set; }
        /// <summary>
        /// Gets or sets the transfer hours.
        /// </summary>
        public decimal TransferHours { get; set; }
        /// <summary>
        /// Gets or sets the attendance type.
        /// </summary>
        public string AttendanceType { get; set; }
        /// <summary>
        /// Gets or sets the clock hour program.
        /// </summary>
        public int? ClockHourProgram { get; set; }
        /// <summary>
        /// Gets or sets the SAP policy details.
        /// </summary>
        public List<SAPPolicyDetail> PolicyDetails { get; set; }
        /// <summary>
        /// Program registration type.
        /// </summary>
        public Enums.ProgramRegistrationType ProgramRegistrationType { get; set; }
        /// <summary>
        /// Gets or sets the is clock hour program.
        /// </summary>
        public bool IsClockHourProgram => this.ClockHourProgram.HasValue;
        /// <summary>
        /// Gets or sets the is title IV.
        /// </summary>
        public bool IsTitleIV => this.TitleIVSapId.HasValue;
        /// <summary>
        /// Is by program registration.
        /// </summary>
        public bool IsByProgramRegistration => ProgramRegistrationType == Enums.ProgramRegistrationType.ByProgram;

    }
}
