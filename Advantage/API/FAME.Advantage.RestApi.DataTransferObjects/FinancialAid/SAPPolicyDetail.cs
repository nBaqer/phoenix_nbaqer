﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SAPPolicyDetail.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SAP policy detail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.DataTransferObjects.FinancialAid
{
    /// <summary>
    /// The SAP Policy Detail class.
    /// </summary>
    public class SAPPolicyDetail
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the qualitative minimum value.
        /// </summary>
        public decimal? QualitativeMinimumValue { get; set; }
        /// <summary>
        /// Gets or sets the quantitative minimum value.
        /// </summary>
        public decimal? QuantitativeMinimumValue { get; set; }
        /// <summary>
        /// Gets or sets the quantitative minimum unit type id.
        /// </summary>
        public int? QuantitativeMinimumUnitTypeId { get; set; }
        /// <summary>
        /// Gets or sets the qualitative minimum type id.
        /// </summary>
        public int? QualitativeMinimumTypeId { get; set; }
        /// <summary>
        /// Gets or sets the increment.
        /// </summary>
        public int Increment { get; set; }
        /// <summary>
        /// Gets or sets the trigger value.
        /// </summary>
        public int? TriggerValue { get; set; }
        /// <summary>
        /// Gets or sets the trigger unit type id.
        /// </summary>
        public int? TriggerUnitTypeId { get; set; }
        /// <summary>
        /// Gets or sets the trigger offset type id.
        /// </summary>
        public int? TriggerOffsetTypeId { get; set; }
        /// <summary>
        /// Gets or sets the trigger offset sequence.
        /// </summary>
        public int? TriggerOffsetSequence { get; set; }
        /// <summary>
        /// Gets or sets the minimum credits completed.
        /// </summary>
        public decimal? MinimumCreditsCompleted { get; set; }
        /// <summary>
        /// Gets or sets the minimum attendance value.
        /// </summary>
        public decimal? MinimumAttendanceValue { get; set; }
        /// <summary>
        /// Gets or sets the trigger unit type description.
        /// </summary>
        public string TriggerUnitTypeDescription { get; set; }
        /// <summary>
        /// Gets or sets the trigger offset type description.
        /// </summary>
        public string TriggerOffsetTypeDescription { get; set; }




    }
}
