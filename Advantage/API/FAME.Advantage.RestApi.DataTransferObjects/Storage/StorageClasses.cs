﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StorageClasses.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the classes related to storage operations.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Storage
{
    using Fame.EFCore.Advantage.Entities;
    using Microsoft.AspNetCore.Http;
    using System;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;

    /// <summary>
    /// The UploadFileParams implementation
    /// </summary>
    public class UploadFileParams
    {
        public IFormFile FileToUpload { get; set; }
        public Guid CampusId { get; set; }
        public FileConfigurationFeature FeatureType { get; set; }
        public Guid? StudentId { get; set; }
    }

    /// <summary>
    /// The UpdateFileParams implementation
    /// </summary>
    public class UpdateFileParams
    {
        public IFormFile FileToUpload { get; set; }
        public Guid CampusId { get; set; }
        public FileConfigurationFeature FeatureType { get; set; }
        public Guid? StudentId { get; set; }
        public string ExistingFileName { get; set; }
    }

    /// <summary>
    /// The DeleteFileParams implementation
    /// </summary>
    public class DeleteFileParams
    {
        public Guid? DocumentId { get; set; }
        public string FileName { get; set; }
        public Guid? StudentId { get; set; }
        public Guid CampusId { get; set; }
        public FileConfigurationFeature FeatureType { get; set; }
    }

    /// <summary>
    /// The GetFileParams implementation
    /// </summary>
    public class GetFileParams
    {
        public IFormFile FileToUpload { get; set; }
        public Guid CampusId { get; set; }
        public Guid? StudentId { get; set; }
        public string FileName { get; set; }
        public FileConfigurationFeature FeatureType { get; set; }

    }

    /// <summary>
    /// The RenameFileParams implementation
    /// </summary>
    public class RenameFileParams
    {
        public Guid CampusId { get; set; }
        public string NewFileName { get; set; }
        public FileConfigurationFeature FeatureType { get; set; }
        public Guid? StudentId { get; set; }
    }

    /// <summary>
    /// The MoveFileParams implementation
    /// </summary>
    public class MoveFileParams
    { ///The get and set for FileStorageType///
        public FileStorageType FileStorageType { get; set; }
        ///The get and set for UserName///
        public string UserName { get; set; }
        ///The get and set for Password///
        public string Password { get; set; }

        ///The get and set for CloudKey///
        public string CloudKey { get; set; }

        ///The get and set for Path///
        public string FromPath { get; set; }

        public string ToPath { get; set; }
        ///The get and set for CloudKey///
        public string CloudKey2 { get; set; }
    }

    /// <summary>
    /// The ExploreDirectoryParams implementation
    /// </summary>
    public class ExploreDirectoryParams
    {
        ///The get and set for CampusId///
        public Guid CampusId { get; set; }

        ///The get and set for Path///
        public string Path { get; set; }

        ///The get and set for FeatureType///
        public FileConfigurationFeature FeatureType { get; set; }
    }

    /// <summary>
    /// The ReadFileContentsParams implementation
    /// </summary>
    public class ReadFileContentsParams
    {
        ///The get and set for CampusId///
        public Guid CampusId { get; set; }

        ///The get and set for FileName///
        public string FileName { get; set; }

        ///The get and set for FeatureType///
        public FileConfigurationFeature FeatureType { get; set; }
    }

    /// <summary>
    /// The ExploreDirectoryParams implementation
    /// </summary>
    public class FileGetResult
    {
        ///The get and set for FileContents///
        public byte[] FileContents { get; set; }

        ///The get and set for CampusId///
        public Guid CampusId { get; set; }

        ///The get and set for FileType///
        public string FileType { get; set; }

        ///The get and set for Extension///
        public string Extension { get; set; }

        ///The get and set for DisplayName///
        public string DisplayName { get; set; }
    }

    /// <summary>
    /// The BuildPathParams implementation
    /// </summary>
    public class BuildPathParams
    {
        ///The get and set for CampusId///
        public Guid? CampusId { get; set; }

        ///The get and set for FeatureType///
        public FileConfigurationFeature FeatureType { get; set; }

        ///The get and set for StuEnrollId///
        public Guid? StuEnrollId { get; set; }

        ///The get and set for StudentId///
        public Guid? StudentId { get; set; }

        ///The get and set for FileName///
        public string FileName { get; set; }
    }

    /// <summary>
    /// The build path result
    /// </summary>
    public class BuildPathResult
    {
        ///The get and set for Path///
        public string Path { get; set; }
    }

    /// <summary>
    /// Campus Storage Settings
    /// </summary>
    public class StorageSettings
    {
        ///The get and set for FileStorageType///
        public FileStorageType FileStorageType { get; set; }

        ///The get and set for UserName///
        public string UserName { get; set; }

        ///The get and set for Password///
        public string Password { get; set; }

        ///The get and set for Path///
        public string Path { get; set; }

        ///The get and set for CloudKey///
        public string CloudKey { get; set; }
    }

    /// <summary>
    /// FileInformation definition
    /// </summary>
    public class FileInformation
    {
        ///The get and set for FileName///
        public string FileName { get; set; }

        ///The get and set for Password///
        public string Extension { get; set; }

        ///The get and set for FilePath///
        public string FilePath { get; set; }

        ///The get and set for ModifiedDate///
        public DateTime ModifiedDate { get; set; }
    }

    /// <summary>
    /// AutomatedTimeClockImportPaths class definition
    /// </summary>
    public class AutomatedTimeClockLocation
    {
        ///The get and set for Path///
        public string Path { get; set; }

        ///The get and set for CampusId///
        public Guid CampusId { get; set; }

        ///The get and set for Username///
        public string Username { get; set; }

        ///The get and set for Password///
        public string Password { get; set; }
    }
}
