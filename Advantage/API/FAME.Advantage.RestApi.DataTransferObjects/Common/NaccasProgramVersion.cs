﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NaccasProgramVersion.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Naccas Program Version type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;

    /// <summary>
    /// The naccas program version.
    /// </summary>
    public class NaccasProgramVersion
    {
        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string ProgramVersionDescription { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is approved.
        /// </summary>
        public bool IsApproved { get; set; }
    }
}
