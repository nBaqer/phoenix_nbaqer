﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IListActionResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IListActionResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The ListActionResult interface.
    /// </summary>
    /// <typeparam name="T">
    /// The entity type.
    /// </typeparam>
    public interface IListActionResult<T> : IList<T>, IActionResultStatus
    {
    }
}
