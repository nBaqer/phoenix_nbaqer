﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeInterval.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The time interval.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;

    /// <summary>
    /// The time interval.
    /// </summary>
    public class TimeInterval
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Datetime.
        /// </summary>
        public DateTime? Datetime { get; set; }
    }
}
