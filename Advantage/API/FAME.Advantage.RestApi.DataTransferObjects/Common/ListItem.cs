﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListItem.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The list item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    /// <summary>
    /// The list item.
    /// </summary>
    /// <typeparam name="TText">
    /// The type of the text, usually a string with the Code or Description that will be shown 
    /// </typeparam>
    /// <typeparam name="TValue">
    /// The type of the value, usually this is the id of the item, an Int or Guid
    /// </typeparam>
    public class ListItem<TText, TValue> : IListItem<TText, TValue>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public TText Text { get; set; }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        public TValue Value { get; set; }
    }
}
