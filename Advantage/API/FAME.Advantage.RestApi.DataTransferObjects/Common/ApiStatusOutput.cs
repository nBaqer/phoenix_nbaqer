﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenericOutput.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The generic output.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    /// <summary>
    /// The generic output.
    /// </summary>
    public class ApiStatusOutput
    {
        /// <summary>
        /// Gets or sets a value indicating whether has passed.
        /// </summary>
        public bool HasPassed { get; set; }

        /// <summary>
        /// Gets or sets the note.
        /// </summary>
        public string ShortNote { get; set; }

        /// <summary>
        /// Gets or sets the extra information.
        /// </summary>
        public string GeneralInformation { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
