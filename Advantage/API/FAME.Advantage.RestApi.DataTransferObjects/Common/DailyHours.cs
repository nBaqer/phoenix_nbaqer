﻿
namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    public class DailyHours
    {
        public DayOfWeek DayOfWeek { get; set; }

        public decimal Hours { get; set; }
    }
}
