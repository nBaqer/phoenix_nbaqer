﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListActionResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The list action result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System.Collections.Generic;

    /// <summary>
    /// The list action result.
    /// </summary>
    /// <typeparam name="T">
    /// The entity type
    /// </typeparam>
    public class ListActionResult<T> : List<T>, IListActionResult<T>
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
