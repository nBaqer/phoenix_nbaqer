﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Requirement.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Requirement DTO.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;

    /// <summary>
    /// The Requirement.
    /// </summary>
    public class Requirement : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the ad req id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the descrip.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid? StatusId { get; set; }

        /// <summary>
        /// Gets or sets the camp grp id.
        /// </summary>
        public Guid? CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the module id.
        /// </summary>
        public int? ModuleId { get; set; }

        /// <summary>
        /// Gets or sets the ad req type id.
        /// </summary>
        public int? RequirementTypeId { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ed lvl id.
        /// </summary>
        public Guid? EducationLevelId { get; set; }

        /// <summary>
        /// Gets or sets the reqfor enrollment.
        /// </summary>
        public bool? IsRequiredforEnrollment { get; set; }

        /// <summary>
        /// Gets or sets the reqfor financial aid.
        /// </summary>
        public bool? IsRequiredforFinancialAid { get; set; }

        /// <summary>
        /// Gets or sets the reqfor graduation.
        /// </summary>
        public bool? IsRequiredforGraduation { get; set; }

        /// <summary>
        /// Gets or sets the ipedsvalue.
        /// </summary>
        public int? IpedsValue { get; set; }

        /// <summary>
        /// Gets or sets the is system requirement.
        /// </summary>
        public bool? IsSystemRequirement { get; set; }

        /// <summary>
        /// Gets or sets the required for termination.
        /// </summary>
        public bool? IsRequiredForTermination { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}