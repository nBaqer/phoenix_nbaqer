﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IActionResultStatus.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IActionResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{

    /// <summary>
    /// The Action Result Status interface.
    /// Implement this interface when you want to return a different type of response status if the ResultStatus property have a value.
    /// By default the response code will be a status code 400.
    /// </summary>
    public interface IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        Enums.ResultStatus ResultStatus { get; set; }
    }
}
