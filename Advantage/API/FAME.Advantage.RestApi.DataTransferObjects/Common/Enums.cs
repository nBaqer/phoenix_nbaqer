﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Enums.cs" company="Fame Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Enums type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    /// <summary>
    /// The enum.
    /// </summary>
    public class Enums
    {
        /// <summary>
        /// The result status.
        /// </summary>
        public enum ResultStatus
        {
            /// <summary>
            /// The success.
            /// </summary>
            Success,

            /// <summary>
            /// The warning.
            /// </summary>
            Warning,

            /// <summary>
            /// The error.
            /// </summary>
            Error,

            /// <summary>
            /// The not found.
            /// </summary>
            NotFound
        }

        /// <summary>
        /// The program registration type.
        /// </summary>
        public enum ProgramRegistrationType
        {
            /// <summary>
            /// The by class.
            /// When By Class is used, the user have to manually configure terms and class section, additionally it needs to register yhe students manually
            /// </summary>
            ByClass = 0,

            /// <summary>
            /// The by program.
            /// When By Program is used, the user does not have to configure term or class section, since those are configured during the set up of the program version and program version definition.
            /// Upon enrollment the students are automatically registered into the term and class section.
            /// </summary>
            ByProgram = 1
        }


        /// <summary>
        /// The file storage types.
        /// </summary>
        public enum FileStorageType
        {
            /// <summary>
            /// The cloud storage.
            /// </summary>
            Cloud = 1,

            /// <summary>
            /// The file storage
            /// </summary>
            File,

            /// <summary>
            /// The network storage
            /// </summary>
            Network
        }

        /// <summary>
        /// The FileConfigurationFeatures enum
        /// </summary>
        public enum FileConfigurationFeature
        {
            ///  Timeclock Archive ///
            TCA = 1,
            ///  Lead Import ///
            LI,
            ///  Student Docs ///
            DOC,
            ///  Photos ///
            PHOTO,
            ///  R2T4 ///
            R2T4,
            ///  Timeclock Import ///
            TCI
        }
    }
}
