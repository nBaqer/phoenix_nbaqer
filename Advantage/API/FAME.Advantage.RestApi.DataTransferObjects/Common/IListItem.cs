﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IListItem.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the ListItem type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    /// <summary>
    /// The ListItem interface.
    /// </summary>
    /// <typeparam name="TText">
    /// The type of the text, usually a string with the Code or Description that will be shown 
    /// </typeparam>
    /// <typeparam name="TValue">
    /// The type of the value, usually this is the id of the item, an Int or Guid
    /// </typeparam>
    public interface IListItem<TText, TValue>
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        TText Text { get; set; }

        /// <summary>
        /// Gets or sets the Value.
        /// </summary>
        TValue Value { get; set; }
    }
}
