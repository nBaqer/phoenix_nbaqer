﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolDetail.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The School Detail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;

    /// <summary>
    /// The school detail.
    /// </summary>
    public class SchoolDetail
    {
        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the address 1.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state code.
        /// </summary>
        public string StateCode { get; set; }

        /// <summary>
        /// Gets or sets the state description.
        /// </summary>
        public string StateDescription { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the country description.
        /// </summary>
        public string CountryDescription { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the phone number1.
        /// </summary>
        public string PhoneNumber1 { get; set; }

        /// <summary>
        /// Gets or sets the phone number2.
        /// </summary>
        public string PhoneNumber2 { get; set; }

        /// <summary>
        /// Gets or sets the phone number3.
        /// </summary>
        public string PhoneNumber3 { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the fax number.
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the website.
        /// </summary>
        public string Website { get; set; }

        /// <summary>
        /// Gets or sets the school logo.
        /// </summary>
        public byte[] SchoolLogo { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        public Guid StateId { get; set; }

        /// <summary>
        /// Gets or sets the country id.
        /// </summary>
        public Guid CountryId { get; set; }
    }
}
