﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Document.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Document DTO.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;

    /// <summary>
    /// The document.
    /// </summary>
    public class Document : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the Campus Id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the FileId.
        /// </summary>
        public Guid FileId { get; set; }

        /// <summary>
        /// Gets or sets the FileName.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the FileExtension.
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the Document Status Id.
        /// </summary>
        public Guid DocumentStatusId { get; set; }

        /// <summary>
        /// Gets or sets the document status.
        /// </summary>
        public int DocumentStatus { get; set; }

        /// <summary>
        /// Gets or sets the Document Type.
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public Guid StudentId { get; set; }
        
        /// <summary>
        /// Gets or sets the File Name Only.
        /// </summary>
        public string FileNameOnly { get; set; }

        /// <summary>
        /// Gets or sets the Document Id.
        /// </summary>
        public Guid DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the Module Id.
        /// </summary>
        public int ModuleId { get; set; }

        /// <summary>
        /// Gets or sets the Lead Id.
        /// </summary>
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the Display Name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the DocHistoryStatus.
        /// </summary>
        public string DocumentHistoryStatus { get; set; }

        /// <summary>
        /// Gets or sets the DocumentSubtype.
        /// </summary>
        public string DocumentSubtype { get; set; }


        /// <summary>
        /// Gets or sets the Document content.
        /// </summary>
        public byte[] DocumentContent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is archived.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets or sets the RequestDate.
        /// </summary>
        public DateTime? RequestDate { get; set; }

        /// <summary>
        /// Gets or sets the ReceiveDate.
        /// </summary>
        public DateTime? ReceiveDate { get; set; }

        /// <summary>
        /// Gets or sets the Scanned Id.
        /// </summary>
        public int? ScannedId { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <inheritdoc />
        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
