﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradDateCalculation.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The grad date calculation parameters.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Common
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// The grad date calculation data.
    /// </summary>
    public class GradDateCalculation
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }
        /// <summary>
        /// Gets or sets the student start date.
        /// </summary>
        public DateTime StudentStartDate { get; set; }

        /// <summary>
        /// Gets or sets the total program hours.
        /// </summary>
        public decimal TotalProgramHours { get; set; }

        /// <summary>
        /// Gets or sets the daily hours
        /// </summary>
        public List<DailyHours> DailyHours { get; set; }
        /// <summary>
        /// Gets or sets the monday hours.
        /// </summary>
        public decimal MondayHours { get; set; }
        /// <summary>
        /// Gets or sets the tuesday hours.
        /// </summary>
        public decimal TuesdayHours { get; set; }
        /// <summary>
        /// Gets or sets the wednesday hours.
        /// </summary>
        public decimal WednesdayHours { get; set; }
        /// <summary>
        /// Gets or sets the thursday hours.
        /// </summary>
        public decimal ThursdayHours { get; set; }
        /// <summary>
        /// Gets or sets the friday hours.
        /// </summary>
        public decimal FridayHours { get; set; }
        /// <summary>
        /// Gets or sets the saturday hours.
        /// </summary>
        public decimal SaturdayHours { get; set; }
        /// <summary>
        /// Gets or sets the sunday hours.
        /// </summary>
        public decimal SundayHours { get; set; }

    }
}
