﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusChange.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusChange type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The student status change.
    /// </summary>
    public class StudentStatusChange
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the enrollments to update.
        /// </summary>
        public List<ListItem<Guid, Guid>> EnrollmentsToUpdate { get; set; }
    }
}
