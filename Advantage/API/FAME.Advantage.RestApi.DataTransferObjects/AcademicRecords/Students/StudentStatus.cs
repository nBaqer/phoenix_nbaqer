﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatus.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Student Status
    /// </summary>
    public class StudentStatus
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }
               
    }
}
