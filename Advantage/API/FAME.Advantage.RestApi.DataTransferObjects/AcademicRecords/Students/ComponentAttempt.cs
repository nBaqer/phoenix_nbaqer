﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ComponentAttempt.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ComponentAttempt type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The ComponentAttempt class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of ComponentAttempt is data transfer object which holds the parameters to return the ComponentAttempt.
    /// </remarks>
    public class ComponentAttempt
    {
        /// <summary>
        /// Gets or sets the Score.
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets ResultNumber.
        /// </summary>
        public int ResultNumber { get; set; }

        /// <summary>
        /// Gets or sets ResultNumber.
        /// </summary>
        public DateTime? PostDate { get; set; }
    }
}
