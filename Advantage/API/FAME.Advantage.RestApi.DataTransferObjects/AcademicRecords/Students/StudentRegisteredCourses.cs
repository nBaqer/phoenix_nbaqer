﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentRegisteredCourses.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentRegisteredCourses type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The StudentRegisteredCourses class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of StudentRegisteredCourses is data transfer object which holds the parameters to return the StudentRegisteredCourses
    /// </remarks>
    public class StudentRegisteredCourses
    {
        /// <summary>
        /// Gets or sets the value for the Courses name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Courses Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Courses Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets Courses Grade.
        /// </summary>
        public string Grade { get; set; }

        /// <summary>
        /// Gets or sets Courses Grade.
        /// </summary>
        public Guid? TestId { get; set; }

    }


    }
