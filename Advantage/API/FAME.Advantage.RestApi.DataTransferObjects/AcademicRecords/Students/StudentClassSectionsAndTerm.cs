﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClassSectionsAndTerm.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentClassSectionsAndTerm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The StudentClassSectionsAndTerm class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of StudentClassSectionsAndTerm is data transfer object which holds the parameters to return the StudentClassSectionsAndTerm
    /// </remarks>
    public class StudentClassSectionsAndTerm
    {
        /// <summary>
        /// Gets or sets the value for the Class Sections name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Class Sections Id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the Code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the Start Date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the End Date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the Instructor Id.
        /// </summary>
        public Guid? InstructorId { get; set; }

        /// <summary>
        /// Gets or sets the Period Id.
        /// </summary>
        public List<Guid?> PeriodId { get; set; }

        /// <summary>
        /// Gets or sets the Course Id.
        /// </summary>
        public Guid CourseId { get; set; }

        /// <summary>
        /// Gets or sets the Term Name.
        /// </summary>
        public string TermName { get; set; }

        /// <summary>
        /// Gets or sets the Term Id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the Term Start Date.
        /// </summary>
        public DateTime? TermStartDate { get; set; }

        /// <summary>
        /// Gets or sets the Term End Date.
        /// </summary>
        public DateTime? TermEndDate { get; set; }
    }


    }
