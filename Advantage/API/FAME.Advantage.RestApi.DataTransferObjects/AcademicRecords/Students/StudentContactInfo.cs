﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentContactInfo.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentContactInfo type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    /// <summary>
    /// The student contact info.
    /// </summary>
    public class StudentContactInfo
    {
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the phone other.
        /// </summary>
        public string PhoneOther { get; set; }

        /// <summary>
        /// Gets the full address.
        /// </summary>
        public string FullAddress
        {
            get
            {
                return this.Address + (string.IsNullOrEmpty(this.Address2) ? " " : " - " + this.Address2) + ", "
                       + this.City + ", " + this.State + " " + this.Zip;
            }
        }
    }
}
