﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentScheduleInfo.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Student Schedule Info type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;

    /// <summary>
    /// The StudentScheduleInfo class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of StudentScheduleInfo is data transfer object which holds the parameters to return the students
    /// </remarks>
    public class StudentScheduleInfo
    {
        /// <summary>
        /// Gets or sets the student schedule id.
        /// </summary>
        public Guid StudentScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public Guid ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the enrollment id.
        /// </summary>
        public Guid EnrollmentId { get; set; }
    }
}
