﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentComponentGrades.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentComponentGrades type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The StudentComponentGrades class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of StudentComponentGrades is data transfer object which holds the parameters to return the StudentComponentGrades.
    /// </remarks>
    public class StudentComponentGrades
    {
        /// <summary>
        /// Gets or sets class section id.
        /// </summary>
        public Guid ClsSectionId { get; set; }

        /// <summary>
        /// Gets or sets the value for the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the value for the componentId.
        /// </summary>
        public Guid ComponentId { get; set; }

        /// <summary>
        /// Gets or sets Courses Grade.
        /// </summary>
        public List<ComponentAttempt> Grades { get; set; }
    }

}

