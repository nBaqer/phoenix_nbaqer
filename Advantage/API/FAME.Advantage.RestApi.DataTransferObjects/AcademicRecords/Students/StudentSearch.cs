﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSearch.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentSearch type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using FAME.Orm.Advantage.Domain.Common;

    /// <summary>
    /// The studentSearch class will have the search filter properties that allows you to find a list of student(s).
    /// </summary>
    /// <remarks>
    /// The object of StudentSearch is data transfer object which holds the parameters to search the students
    /// </remarks>
    public class StudentSearch
    {
        /// <summary>
        /// Gets or sets the campus id of type guid in which student details needs to be searched
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the program version id the student belongs to
        /// </summary>
        public Guid? ProgramVersionId { get; set; }
        /// <summary>
        /// Gets or sets the filter.
        /// Filter will be used as the text to search the student based on First Name, Last Name or SSN and it should be of minimum 3 letter and maximum of 50 letters
        /// </summary>
        public string Filter { get; set; }
        /// <summary>
        /// Gets or sets the student statuses.
        /// List of student statuses that the search will use to filter students.
        /// </summary>
        public List<Constants.SystemStatus> StudentStatuses { get; set; }
        /// <summary>
        /// Gets or sets the show all flag.
        /// </summary>
        public bool ShowAll { get; set; } = false;
        
    }
}

