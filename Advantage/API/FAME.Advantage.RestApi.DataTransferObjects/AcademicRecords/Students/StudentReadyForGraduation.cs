﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentReadyForGraduation.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentReadyForGraduation type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The student ready for graduation.
    /// </summary>
    public class StudentReadyForGraduation
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the missing grades.
        /// </summary>
        public string MissingGrades { get; set; }

        /// <summary>
        /// Gets or sets the ledger balance.
        /// </summary>
        public string LedgerBalance { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the total required for graduation.
        /// </summary>
        public decimal TotalRequiredForGraduation { get; set; }

        /// <summary>
        /// Gets or sets the actual credits or hours.
        /// </summary>
        public decimal ActualCreditsOrHours { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is clock hour.
        /// If false is considered the enrollment belongs to a credit program version.
        /// </summary>
        public bool IsClockHour { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }
     
    }
}
