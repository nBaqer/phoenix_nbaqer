﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PostFinalGrade.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the PostFinalGrade type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The PostFinalGrade class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of PostFinalGrade is data transfer object which holds the parameters to return the PostFinalGrade
    /// </remarks>
    public class PostFinalGrade
    {
        /// <summary>
        /// Gets or sets the value for student enrollment id name.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the Class Sections Id.
        /// </summary>
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the students final grade for letter grade schools.
        /// </summary>
        public string LetterGrade { get; set; }

        /// <summary>
        /// Gets or sets the Score.
        /// </summary>
        public decimal? Score { get; set; }
    }
}
