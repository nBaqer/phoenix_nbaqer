﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentPostGradesInput.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentPostGradesInput type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The StudentPostGradesInput class contains the properties that are needed to input a grade for a student.
    /// </summary>
    /// <remarks>
    /// Contains the properties for posting a grade by class.
    /// </remarks>
    public class StudentPostGradesInput
    {
        /// <summary>
        /// The student Enrollment Id.
        /// </summary>
        [Required]
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// The class section Id.
        /// </summary>
        [Required]
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// The component id.
        /// </summary>
        [Required]
        public Guid ComponentId { get; set; }

        /// <summary>
        /// The score.
        /// </summary>
        [Required]
        [Range(0.00, Double.MaxValue)]
        public decimal Score { get; set; }

        /// <summary>
        /// The result number.
        /// </summary>
        [Range(1, int.MaxValue)]
        public int? ResultNumber { get; set; }

        /// <summary>
        /// The post date.
        /// </summary>
        public DateTime? PostDate { get; set; }
    }
}
