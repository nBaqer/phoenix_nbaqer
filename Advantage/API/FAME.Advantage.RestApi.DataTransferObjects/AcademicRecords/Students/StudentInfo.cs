﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentInfo.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentSearch type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students
{
    using System;

    /// <summary>
    /// The studentSearchOutput class contains the properties that are need to be returned.
    /// </summary>
    /// <remarks>
    /// The object of StudentSearchOutput is data transfer object which holds the parameters to return the students
    /// </remarks>
    public class StudentInfoForReports
    {
        /// <summary>
        /// Gets or sets the value for the display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the SSN
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// Gets or sets the StudentId
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the dod.
        /// </summary>
        public string Dod { get; set; }

        /// <summary>
        /// Gets or sets the form completed.
        /// </summary>
        public string FormCompleted { get; set; }

        /// <summary>
        /// Gets or sets the caluclation period type.
        /// </summary>
        public string CaluclationPeriodType { get; set; }

        /// <summary>
        /// Gets or sets the isClockHour.
        /// </summary>
        public bool IsClockHour { get; set; }
    }
}
