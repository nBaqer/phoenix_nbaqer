﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Term.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Term.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Terms
{
    using System;

    /// <summary>
    /// The Term.
    /// </summary>
    public class Term
    {
        /// <summary>
        /// Gets or sets the  id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid? StatusId { get; set; }

        /// <summary>
        /// Gets or sets the campus group id.
        /// </summary>
        public Guid? CampusGroupId { get; set; }

        /// <summary>
        /// Gets or sets the shift id.
        /// </summary>
        public Guid? ShiftId { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public string ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is module.
        /// </summary>
        public bool IsModule { get; set; }

        /// <summary>
        /// Gets or sets the term type id.
        /// </summary>
        public int? TermTypeId { get;set;}

        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public Guid? ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the mid point date.
        /// </summary>
        public DateTime? MidPointDate { get; set; }

        /// <summary>
        /// Gets or sets the maximum graduation date.
        /// </summary>
        public DateTime? MaximumGraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid? ProgramVersionId { get; set; }
    }
}
