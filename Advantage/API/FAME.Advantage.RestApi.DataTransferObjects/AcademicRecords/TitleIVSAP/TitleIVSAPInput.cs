﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVSAPInput.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the TitleIVSAPInput type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP
{
    using System;

    /// <summary>
    /// The title iv sap input.
    /// </summary>
    public class TitleIVSAPInput
    {
        /// <summary>
        /// Gets or sets the campus id
        /// </summary>
        public Guid? CampusId { get; set; }
        /// <summary>
        /// Gets or sets the program version id
        /// </summary>
        public Guid? ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the enrollments.
        /// </summary>
        public List<Guid> Enrollments { get; set; }

        /// <summary>
        /// Gets or sets the filter.
        /// Filter will be used as the text to search the Title IV results based on lead name
        /// </summary>
        [MinLength(3, ErrorMessage = "Minimum number of characters allowed is 3")]
        [MaxLength(50, ErrorMessage = "Maximum number of characters allowed is 50")]
        public string Filter { get; set; }


    }
}
