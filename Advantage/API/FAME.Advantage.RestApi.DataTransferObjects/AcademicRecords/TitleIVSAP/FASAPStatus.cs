﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FASAP.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The fasap.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP
{
    using System;

    /// <summary>
    /// The fasap.
    /// </summary>
    public class FASAPStatus
    {
        /// <summary>
        /// Gets or sets the student id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the enrollment id.
        /// </summary>
        public Guid EnrollmentId { get; set; }
    }
}
