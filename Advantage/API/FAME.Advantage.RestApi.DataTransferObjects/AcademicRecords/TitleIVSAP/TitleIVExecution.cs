﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVExecution.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the TitleIVExecution type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The Title IV execution.
    /// </summary>
    public class TitleIVExecution
    {
        /// <summary>
        /// The campus id.
        /// </summary>
        public Guid? CampusId { get; set; }
        /// <summary>
        /// The program version id.
        /// </summary>
        public Guid? ProgramVersionId { get; set; }
        /// <summary>
        /// The student enrollments
        /// </summary>
        public List<Guid> StudentEnrollments { get; set; }
        /// <summary>
        /// The increment to run.
        /// </summary>
        public int? IncrementToRun { get; set; }
        /// <summary>
        /// The run all increments.
        /// </summary>
        public bool RunAllIncrements { get; set; }
        /// <summary>
        /// Allow any school status.
        /// </summary>
        public bool AllowAnyStatus { get; set; } = false;
    }
}
