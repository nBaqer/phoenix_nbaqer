﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVSAPResult.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the TitleIVSAP type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP
{
    using System;

    /// <summary>
    /// The title ivsap.
    /// </summary>
    public class TitleIVSAPResult
    {

        /// <summary>
        /// Gets or sets the FASAPCheckResultsId that in the 
        /// DB is called StdRecKey and is the table key.
        /// </summary>
        public Guid FASAPCheckResultsId { get; set; }


        /// <summary>
        /// Gets or sets the increment.
        /// </summary>
        public int Increment { get; set; }

        /// <summary>
        /// Gets or sets the triggered at.
        /// </summary>
        public string TriggeredAt { get; set; }

        /// <summary>
        /// Gets or sets the sap check date.
        /// </summary>
        public DateTime? SAPCheckDate { get; set; }

        /// <summary>
        /// Gets or sets the sap status.
        /// </summary>
        public string SAPStatus { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// Gets or sets the active flag.
        /// </summary>
        public bool Active { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether is making sap.
        /// </summary>
        public bool IsMakingSAP { get; set; }
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid? LeadId { get; set; }
        /// <summary>
        /// Gets or sets the student name.
        /// </summary>
        public string StudentName { get; set; }
        /// <summary>
        /// Gets the sap check date string.
        /// </summary>
        public string SAPCheckDateString
        {
            get
            {
                return this.SAPCheckDate?.ToString("MM/dd/yyyy") ?? string.Empty;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether it can be moved to Probation.
        /// </summary>
        public bool canMoveToProbation { get; set; }
        /// <summary>
        /// The get and set for Student Enrollment Identification.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }


        /// <summary>
        /// The get and set for DatePerformed
        /// </summary>
        public DateTime DatePerformed { get; set; }

        /// <summary>
        /// Gets or sets the title ivsap id code.
        /// </summary>
        public int TitleIVSAPId { get; set; }

        /// <summary>
        /// Gets or sets the check point date.
        /// </summary>
        public DateTime? CheckPointDate { get; set; }

        /// <summary>
        /// Gets the check point date string.
        /// </summary>
        public string CheckPointDateString
        {
            get
            {
                return this.CheckPointDate?.ToString("MM/dd/yyyy") ?? string.Empty;
            }
        }

    }
}
