﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4CompletionStatus.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The R2T4 completion details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input
{
    /// <summary>
    /// The R2T4 completion details. This is a DTO model to represent R2T4CompletionStatus entity in the client side.
    /// This DTO model will have IsR2T4InputCompleted, IsR2T4ResultsCompleted, IsR2T4OverrideResultsCompleted which gives the status of the completion of R2T4 student termination tabs.
    /// </summary>
    /// <remarks>
    /// The object of R2T4CompletionStatus is a data transfer object which holds the parameters to determine the completion of different tabs in R2T4 student termination page.
    /// </remarks>
    public class R2T4CompletionStatus
    {
        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 input completed.
        /// </summary>
        public bool IsR2T4InputCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 results completed.
        /// </summary>
        public bool IsR2T4ResultsCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 override results completed.
        /// </summary>
        public bool IsR2T4OverrideResultsCompleted { get; set; }
    }
}