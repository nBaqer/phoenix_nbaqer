﻿

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using static FAME.Orm.Advantage.Domain.Common.Constants;

    public class StudentLoas
    {
        public DateTime? LoaStartDate { get; set; }
        public DateTime? LoaEndDate { get; set; }
        public Guid? StuEnrollId { get; set; }

    }
}
