﻿

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using static FAME.Orm.Advantage.Domain.Common.Constants;

    public class EnrollmentProgram
    {
        public Guid? StatusCodeId { get; set; }
        public int SysStatusId { get; set; }
        public string Status { get; set; }
        public DateTime? DropDate { get; set; }
        public string DropReason { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public DateTime? ReEnrollmentDate { get; set; }
        public DateTime? GraduationDate { get; set; }
        public DateTime? RevisedGraduationDate { get; set; }
        public string BadgeId { get; set; }
        public List<string> StudentGroups { get; set; }
        public bool IsDropped { get { return SysStatusId == (int)SystemStatus.Dropped; } }
        public double OverallGPA { get; set; }
        public int ACId { get; set; }

        public Guid CampusId { get; set; }

        public bool DoCourseWeightOverallGPA { get; set; }
        public DateTime? LoaStartDate { get; set; }
        public DateTime? LoaEndDate { get; set; }
        public Guid? StuEnrollId { get; set; }

        public string OverallGPAString
        {
            get
            {
                return this.OverallGPA.ToString();
            }
        }

        public string StartDateString { get { return StartDate.HasValue ? StartDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string DropDateString { get { return DropDate.HasValue ? DropDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string EnrollmentDateString { get { return EnrollmentDate.ToString("MM/dd/yyyy"); } }
        public string ReEnrollmentDateString { get { return ReEnrollmentDate.HasValue ? ReEnrollmentDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string GraduationDateString { get { return GraduationDate.HasValue ? GraduationDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string RevisedGraduationDateString { get { return RevisedGraduationDate.HasValue ? RevisedGraduationDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string LoaStartDateString { get { return LoaStartDate.HasValue ? LoaStartDate.Value.ToString("MM/dd/yyyy") : ""; } }
        public string LoaEndDateString { get { return LoaEndDate.HasValue ? LoaEndDate.Value.ToString("MM/dd/yyyy") : ""; } }



    }
}
