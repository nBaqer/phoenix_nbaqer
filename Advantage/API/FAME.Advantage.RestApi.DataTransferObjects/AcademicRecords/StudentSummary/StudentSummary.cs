﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSummary.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student summary DTO.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    /// <summary>
    /// The student summary DTO.
    /// </summary>
    public class StudentSummary
    {
        /// <summary>
        /// Gets or sets the StudentNumber.
        /// </summary>
        public string StudentNumber { get; set; }

        /// <summary>
        /// Gets or sets the first_name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last_name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// The role for now must be student
        /// </summary>
        public string Role { get; set; }


        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// Gets or sets the ProgramDescription.
        /// </summary>
        public string ProgramDescription { get; set; }

        /// <summary>
        /// Gets or sets the ProgramVersionDescription.
        /// </summary>
        public string ProgramVersionDescription { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the start_date.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the graduation_date.
        /// </summary>
        public string ExpectedGraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the date_of_birth.
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone_other.
        /// </summary>
        public string PhoneOther { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the address_city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address_state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the address_zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the TotalHours.
        /// </summary>
        public string TotalHours { get; set; }

        /// <summary>
        /// Gets or sets the AbsentHours.
        /// </summary>
        public string AbsentHours { get; set; }

        /// <summary>
        /// Gets or sets the MakeupHours.
        /// </summary>
        public string MakeupHours { get; set; }

        /// <summary>
        /// Gets or sets the LastDateAttended.
        /// </summary>
        public string LastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the AttendancePercentage.
        /// </summary>
        public string AttendancePercentage { get; set; }

        /// <summary>
        /// Gets or sets the OverallGPA.
        /// </summary>
        public string OverallGPA { get; set; }

        /// <summary>
        /// Gets or sets the enrollment id
        /// </summary>
        public string EnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the CampusId.
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// Gets or sets the ProgramId.
        /// </summary>
        public string ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the ProgramVersionId.
        /// </summary>
        public string ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the StatusId.
        /// </summary>
        public string StatusCodeId { get; set; }
    }
}
