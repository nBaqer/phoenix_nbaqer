﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Program dto.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Program
{
    using System;

    /// <summary>
    /// The program dto class.
    /// </summary>
    public class ProgramDetails
    {
        /// <summary>
        /// The program description
        /// </summary>
        public string ProgDescrip { get; set; }
    }
}
