﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionMeetingDetail.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The class section meeting detail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSectionMeeting
{
    using System;

    /// <summary>
    /// The class section meeting detail.
    /// </summary>
    public class ClassSectionMeetingDetail
    {
        /// <summary>
        /// Gets or sets the class section meeting id.
        /// </summary>
        public Guid ClassSectionMeetingId { get; set; }

        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the class period id.
        /// </summary>
        public Guid PeriodId { get; set; }
    }
}
