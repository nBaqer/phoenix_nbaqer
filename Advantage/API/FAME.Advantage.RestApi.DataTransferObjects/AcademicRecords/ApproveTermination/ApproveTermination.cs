﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApproveTermination.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the Approve Termination type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination
{
    using System;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The approve termination.
    /// </summary>
    [Serializable]
    public class ApproveTermination
    {
        #region Header Information

        /// <summary>
        /// Gets or sets the student name.
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// Gets or sets the student ssn.
        /// </summary>
        public string StudentSsn { get; set; } 

        /// <summary>
        /// Gets or sets the student identifier.
        /// </summary>
        public string StudentIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the student identifier id.
        /// </summary>
        public string StudentIdentifierId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        #endregion Header Information

        #region Termination Detail Information

        /// <summary>
        /// Gets or sets the enrollment name.
        /// </summary>
        public string EnrollmentName { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the drop reason.
        /// </summary>
        public string DropReason { get; set; }

        /// <summary>
        /// Gets or sets the last date attended.
        /// </summary>
        public string LastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal date.
        /// </summary>
        public string WithdrawalDate { get; set; }

        /// <summary>
        /// Gets or sets the date of determination.
        /// </summary>
        public string DateOfDetermination { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 calculation summary.
        /// </summary>
        public bool IsR2T4 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is additional info required.
        /// </summary>
        public bool IsAdditionalInfoRequired { get; set; }

        /// <summary>
        /// Gets or sets a value of unit type description.
        /// </summary>
        public string UnitTypeDescription { get; set; }

        #endregion Termination Detail Information

        /// <summary>
        /// Gets or sets the R2T4 calculation summary detail.
        /// </summary>
        public R2T4CalculationSummary R2T4CalculationSummaryDetail { get; set; }

        #region Additional Section Information 

        /// <summary>
        /// Gets or sets the additional information detail.
        /// </summary>
        public AdditionalInformation AdditionalInformationDetail { get; set; }

        #endregion Additional Section Information 
    }
}