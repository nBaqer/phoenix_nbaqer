﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4CalculationSummary.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   The R2T4 Calculation summary.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination
{
    using System.Collections.Generic;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The R2T4 Calculation summary.
    /// </summary>
    public class R2T4CalculationSummary
    {
        /// <summary>
        /// Gets or sets the total charges.
        /// </summary>
        public string TotalCharges { get; set; }

        /// <summary>
        /// Gets or sets the total title iv aid.
        /// </summary>
        public string TotalTitleIvAid { get; set; }

        /// <summary>
        /// Gets or sets the total title iv aid disbursed.
        /// </summary>
        public string TotalTitleIvAidDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the percentage of title iv aid earned.
        /// </summary>
        public string PercentageOfTitleIvAidEarned { get; set; }

        /// <summary>
        /// Gets or sets the post withdrawal disbursement.
        /// </summary>
        public string PostWithdrawalDisbursement { get; set; }

        /// <summary>
        /// Gets or sets the total title iv aid earned.
        /// </summary>
        public string TotalTitleIvAidEarned { get; set; }

        /// <summary>
        /// Gets or sets the total title iv aid to return.
        /// </summary>
        public string TotalTitleIvAidToReturn { get; set; }

        /// <summary>
        /// Gets or sets the amount to be returned by school.
        /// </summary>
        public List<ListItem<string, string>> AmountToBeReturnedBySchool { get; set; }

        /// <summary>
        /// Gets or sets the total amount to be returned by school.
        /// </summary>
        public string TotalAmountToBeReturnedBySchool { get; set; }

        /// <summary>
        /// Gets or sets the amount to be returned by student.
        /// </summary>
        public List<ListItem<string, string>> AmountToBeReturnedByStudent { get; set; }

        /// <summary>
        /// Gets or sets the total amount to be returned by student.
        /// </summary>
        public string TotalAmountToBeReturnedByStudent { get; set; }
    }
}