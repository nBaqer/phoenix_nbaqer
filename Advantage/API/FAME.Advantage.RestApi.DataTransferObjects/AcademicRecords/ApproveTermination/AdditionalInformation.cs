﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdditionalInformation.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   The additional information.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.Common; 

    /// <summary>
    /// The additional information.
    /// </summary>
    public class AdditionalInformation
    {
        /// <summary>
        /// Gets or sets the R2T4 input user name.
        /// </summary>
        public string R2T4InputUserName { get; set; }

        /// <summary>
        /// Gets or sets the overridden user name.
        /// </summary>
        public string OverriddenUserName { get; set; }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        public string TicketNumber { get; set; }

        /// <summary>
        /// Gets or sets the title iv grant less than 50 dollar.
        /// </summary>
        public List<ListItem<string, string>> TitleIvGrantLessThan50Dollar { get; set; }

        /// <summary>
        /// Gets or sets the  R2T4 result fields with overridden values.
        /// </summary>
        public List<OverriddenListItem> R2T4ResultFieldsWithOverriddenValues { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is not required to take aattendance.
        /// The value of this property will be true in case attendance is 50% marked else false.
        /// </summary>
        public bool IsNotRequiredToTakeAattendance { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance 100 percent.
        /// </summary>
        public bool IsAttendance100Percent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is clock hour.
        /// </summary>
        public bool IsClockHour { get; set; } 

        /// <summary>
        /// Gets or sets a value indicating whether is additional info required.
        /// </summary>
        public bool IsAdditionalInfoRequired { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether Is Tuition Charged By Payment Period.
        /// </summary>
        public bool IsTuitionByPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets a value of Credit balance refunded.
        /// </summary>
        public string CreditBalanceRefunded { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type id.
        /// </summary>
        public Guid? CalculationPeriodTypeId { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type.
        /// </summary>
        public string CalculationPeriodType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is additional info step3 is required.
        /// </summary>
        public bool IsAdditionalInfoStep3Required { get; set; }
    }
}