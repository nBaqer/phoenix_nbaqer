﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OverriddenListItem.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   The overridden item list.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination
{
    /// <summary>
    /// The overridden list item.
    /// </summary>
    public class OverriddenListItem
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the result value.
        /// </summary>
        public string ResultValue { get; set; }

        /// <summary>
        /// Gets or sets the overridden result value.
        /// </summary>
        public string OverriddenResultValue { get; set; }
    }
}