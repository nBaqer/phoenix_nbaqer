﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusChanges.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusChanges type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination
{
    using System;

    /// <summary>
    /// The StudentStatusChanges class will have the StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
    /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
    /// which is gives Student Status Changes details.
    /// </summary>
    /// <remarks>
    /// The object of StudentStatusChanges is a data transfer object which holds the parameters to display the Student Status Changes.
    /// </remarks>
    public class StudentStatusChanges
    {
        /// <summary>
        /// Gets or sets the student status change id.
        /// </summary>
        public Guid StudentStatusChangeId { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the original status id.
        /// </summary>
        public Guid? OriginalStatusId { get; set; }

        /// <summary>
        /// Gets or sets the new status id.
        /// </summary>
        public Guid? NewStatusId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is reversal.
        /// </summary>
        public bool IsReversal { get; set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public Guid? DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the date of change.
        /// </summary>
        public DateTime? DateOfChange { get; set; }

        /// <summary>
        /// Gets or sets the lastdate attended.
        /// </summary>
        public DateTime? LastdateAttended { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the requested by.
        /// </summary>
        public string RequestedBy { get; set; }

        /// <summary>
        /// Gets or sets the have backup.
        /// </summary>
        public bool? HaveBackup { get; set; }

        /// <summary>
        /// Gets or sets the have client confirmation.
        /// </summary>
        public bool? HaveClientConfirmation { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
