﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduledHoursAdjustment.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ScheduledHoursAdjustmentParameters type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ScheduledHours
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// ScheduledHoursAdjustment class
    /// </summary>
    public class ScheduledHoursAdjustment
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid campusId { get; set; }

        /// <summary>
        /// Gets or sets the scheduled day;
        /// </summary>
        public DateTime adjustmentDate { get; set; }

        /// <summary>
        /// Gets or sets the studentGroupId;
        /// </summary>
        public Guid? studentGroupId { get; set; }

        /// <summary>
        /// Gets or sets the programVersionId;
        /// </summary>
        public Guid? programVersionId { get; set; }

        /// <summary>
        /// Gets or sets the AmountToAdjust;
        /// </summary>
        public decimal? AmountToAdjust { get; set; }

        /// <summary>
        /// Gets or sets the AdjustmentMethod;
        /// </summary>
        public AdjustmentMethod AdjustmentMethod { get; set; }
    }

    public enum AdjustmentMethod
    {
        RemoveScheduledHours = 1,
        AdjustScheduledHoursByAmount = 2,
        SetScheduledHoursToActualHours = 3,
        RevertScheduledHoursPriorToAnyAdjustment = 4
    }
}
