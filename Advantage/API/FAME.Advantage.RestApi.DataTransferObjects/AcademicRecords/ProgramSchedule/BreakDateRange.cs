﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BreakDateRange.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The break date range.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule
{
    using System;

    /// <summary>
    /// The break date range.
    /// </summary>
    public class BreakDateRange
    {
        /// <summary>
        /// Gets or sets the break start date.
        /// </summary>
        public DateTime BreakStartDate { get; set; }

        /// <summary>
        /// Gets or sets the break end date.
        /// </summary>
        public DateTime BreakEndDate { get; set; } 
    }
}
