﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClockAttendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student clock attendance.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule
{
    using System;

    /// <summary>
    /// The student clock attendance.
    /// </summary>
    public class StudentClockAttendance
    {
        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public Guid ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the record date.
        /// </summary>
        public DateTime RecordDate { get; set; }

        /// <summary>
        /// Gets or sets the record end date.
        /// </summary>
        public DateTime RecordEndDate { get; set; }

        /// <summary>
        /// Gets or sets the Schedule Hours.
        /// </summary>
        public decimal? ScheduleHours { get; set; }

        /// <summary>
        /// Gets or sets the actual hours.
        /// </summary>
        public decimal? ActualHours { get; set; }

        /// <summary>
        /// Gets or sets the Modified Date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the Modified User.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the is tardy.
        /// </summary>
        public bool? IsTardy { get; set; }

        /// <summary>
        /// Gets or sets the post by exception.
        /// </summary>
        public string PostByException { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the tardy processed.
        /// </summary>
        public bool? TardyProcessed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether converted.
        /// </summary>
        public bool Converted { get; set; }
        /// <summary>
        /// Gets or sets the scheduled hours on termination.
        /// </summary>
        public decimal? SchedHoursOnTermination { get; set; }
    }
}