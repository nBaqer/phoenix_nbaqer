﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule
{
    using System; 
    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The program schedule details.
    /// </summary>
    public class ProgramScheduleDetails
    {
        /// <summary>
        /// Gets or sets the schedule detail id.
        /// </summary>
        public Guid ScheduleDetailId { get; set; }

        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public Guid? ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the Day Of Week.
        /// </summary>
        public short? DayOfWeek { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public decimal? Total { get; set; }

        /// <summary>
        /// Gets or sets the timein.
        /// </summary>
        public DateTime? Timein { get; set; }

        /// <summary>
        /// Gets or sets the lunchin.
        /// </summary>
        public DateTime? Lunchin { get; set; }

        /// <summary>
        /// Gets or sets the lunchout.
        /// </summary>
        public DateTime? Lunchout { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        public DateTime? Timeout { get; set; }

        /// <summary>
        /// Gets or sets the Maximum Number Of Lunch.
        /// </summary>
        public decimal? MaximumNumberOfLunch { get; set; }

        /// <summary>
        /// Gets or sets the allow earlyin.
        /// </summary>
        public bool? AllowEarlyin { get; set; }

        /// <summary>
        /// Gets or sets the allow lateout.
        /// </summary>
        public bool? AllowLateout { get; set; }

        /// <summary>
        /// Gets or sets the allow extrahours.
        /// </summary>
        public bool? AllowExtraHours { get; set; }

        /// <summary>
        /// Gets or sets the check tardyin.
        /// </summary>
        public bool? CheckTardyin { get; set; }

        /// <summary>
        /// Gets or sets the max beforetardy.
        /// </summary>
        public DateTime? MaxBeforeTardy { get; set; }

        /// <summary>
        /// Gets or sets the tardy intime.
        /// </summary>
        public DateTime? TardyIntime { get; set; }

        /// <summary>
        /// Gets or sets the schedule.
        /// </summary>
        public ArProgSchedules Schedule { get; set; }
    }
}
