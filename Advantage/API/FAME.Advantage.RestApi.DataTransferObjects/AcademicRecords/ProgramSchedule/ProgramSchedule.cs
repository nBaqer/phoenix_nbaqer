﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramSchedule.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule
{
    using System;
    using System.Collections.Generic;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The program schedule.
    /// </summary>
    public class ProgramSchedule
    {
        /// <summary>
        /// Gets or sets the schedule id.
        /// </summary>
        public Guid ScheduleId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Program Version Id.
        /// </summary>
        public Guid? ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the active.
        /// </summary>
        public bool? Active { get; set; }

        /// <summary>
        /// Gets or sets the use flex time.
        /// </summary>
        public bool? UseFlexTime { get; set; }

        /// <summary>
        /// Gets or sets the Modified User.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the Modified Date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the Program Version.
        /// </summary>
        public ArPrgVersions ProgramVersion { get; set; }

        /// <summary>
        /// Gets or sets the prg schedule details.
        /// </summary>
        public List<ProgramScheduleDetails> ProgramScheduleDetails { get; set; }
    }
}
