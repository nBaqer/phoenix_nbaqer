﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourProgramSchedule.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Credit Hour Program Schedule.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The program schedule.
    /// </summary>
    public class CreditHourProgramSchedule
    {
        /// <summary>
        /// Gets or sets the total scheduled break days.
        /// </summary>
        public int TotalScheduledBreakDays { get; set; }

        /// <summary>
        /// Gets or sets the total scheduled break days.
        /// </summary>
        public IList<BreakDateRange> BreakDateList { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    } 
}
