﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LMSEnrollmentDetails.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the LMSEnrollmentDetails type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment
{
    using System;
    /// <summary>
    /// Enrollment details for LMS API call
    /// </summary>
    public class LMSEnrollmentDetails
    {
        /// <summary>
        /// arstuEnrollmentId
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }
        /// <summary>
        /// first name of the student
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// last name of the student
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// program name
        /// </summary>
        public string ProgramName { get; set; }
        /// <summary>
        /// program id
        /// </summary>
        public Guid ProgramId { get; set; }
        /// <summary>
        /// Program version name
        /// </summary>
        public string ProgramVersionName { get; set; }
        /// <summary>
        /// program version Id
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the StatusId.
        /// </summary>
        public string StatusCodeId { get; set; }
    }
}
