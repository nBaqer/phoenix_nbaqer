﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Enrollment.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Enrollment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment
{
    using System;

    /// <summary>
    /// The enrollment class will have the EnrollmentId,ProgramVersionDescription,StatusDescription,EffectiveDate,Status,EnrollmentDate which is gives the student enrollment details.
    /// </summary>
    /// <remarks>
    /// The object of Enrollment is a data transfer object which holds the parameters to display the enrollments.
    /// </remarks>
    public class Enrollment
    {
        /// <summary>
        /// Gets or sets the date determined. This is of type string.
        /// </summary>
        public DateTime? DateDetermined { get; set; }

        /// <summary>
        /// Gets or sets the effective date.
        /// </summary>
        public DateTime? EffectiveDate { get; set; }

        /// <summary>
        /// Gets or sets the enrollment date. This is of type string.
        /// </summary>
        public DateTime EnrollmentDate { get; set; }

        /// <summary>
        /// Gets or sets the last date attended. This is of type string.
        /// </summary>
        public DateTime? LastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the program version description.
        /// This is the description of the program which student has enrolled.
        /// </summary>
        public string ProgramVersionDescription { get; set; }

        /// <summary>
        /// Gets or sets the start date. This is of type string.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// The Status contains the whether the student enrollment is active or inactive.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the status code description.
        /// Status code description refers to the status of the program which student has enrolled.
        /// </summary>
        public string StatusCodeDescription { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id. This is of type Guid.
        /// </summary>
        public Guid EnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the student id. This is of type Guid.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the unit type description.
        /// </summary>
        public string UnitTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the ssn.
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the system status id. This is of type integer.
        /// </summary>
        public int SystemStatusId { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public Guid? DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public Guid? StatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public Guid PrgVerId { get; set; }

        /// <summary>
        /// Gets or sets the transfer hours.
        /// </summary>
        public decimal? TransferHours { get; set; }

        /// <summary>
        /// Gets or sets the transfer date.
        /// </summary>
        public DateTime? TransferDate { get; set; }

        /// <summary>
        /// Gets or sets the expected graduation date.
        /// </summary>
        public DateTime ExpectedGraduationDate { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type id.
        /// </summary>
        public Guid? CalculationPeriodTypeId { get; set; }
    }
}