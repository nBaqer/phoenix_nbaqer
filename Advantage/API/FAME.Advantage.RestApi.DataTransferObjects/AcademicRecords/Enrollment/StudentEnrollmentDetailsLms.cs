﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentEnrollmentDetailsLms.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentEnrollmentDetailsLms type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment
{
    using System;

    /// <summary>
    /// The student enrollment details lms.
    /// </summary>
    public class StudentEnrollmentDetailsLms : LMSEnrollmentDetails
    {
        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the best email.
        /// </summary>
        public string BestEmail { get; set; }

        /// <summary>
        /// Gets or sets the best phone.
        /// </summary>
        public string BestPhone { get; set; }

        /// <summary>
        /// Gets or sets the best address.
        /// </summary>
        public string BestAddress { get; set; }

        /// <summary>
        /// Gets or sets the address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the studentId.
        /// </summary>
        public string StudentId { get; set; }

        /// <summary>
        /// Gets or sets the StartDate.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the StatusId.
        /// </summary>
        public string StatusCodeId { get; set; }
    }
}
