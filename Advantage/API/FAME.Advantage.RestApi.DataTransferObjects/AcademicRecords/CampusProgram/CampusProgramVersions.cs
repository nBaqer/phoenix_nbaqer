﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusProgramVersions.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the CampusProgramVersions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.CampusProgram
{
    using System;

    /// <summary>
    /// The campusprogramversions class will have the CampusProgramVersionId,CampusId,ProgramVersionId,IsTitleIV,IsFAMEApproved,IsSelfPaced,CalculationPeriodTypeId,AllowExcusAbsPerPayPeriod,TermSubEqualInLength,ModUser,ModDate which is gives the Campus program version details.
    /// </summary>
    /// <remarks>
    /// The object of CampusProgramVersions is a data transfer object which holds the parameters to display the CampusProgramVersion.
    /// </remarks>
    public class CampusProgramVersions
    {
        /// <summary>
        /// Gets or sets the campus program version id.
        /// </summary>
        public Guid CampusProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid? CampusId { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is title IV.
        /// </summary>
        public bool IsTitleIv { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is FAME approved.
        /// </summary>
        public bool IsFameApproved { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is self paced.
        /// </summary>
        public bool? IsSelfPaced { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type id.
        /// </summary>
        public Guid? CalculationPeriodTypeId { get; set; }

        /// <summary>
        /// Gets or sets the allow excused abscence per payment period.
        /// </summary>
        public decimal? AllowExcusAbsPerPayPrd { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is term substantially equal length or not.
        /// </summary>
        public bool? TermSubEqualInLen { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether result status is true or not.
        /// </summary>
        public bool ResultStatus { get; set; }
    }
}