﻿

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course
{
    using System;

    /// <summary>
    /// The program version definition course lms.
    /// </summary>
    public class ProgramVersionDefinitionCourseLms : CourseLms
    {
        /// <summary>
        /// Gets or sets the program version definition id.
        /// </summary>
        public Guid ProgramVersionDefinitionId { get; set; }
    }
}
