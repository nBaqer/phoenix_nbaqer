﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CourseGroupLms.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Course Group LMS type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The course group lms.
    /// </summary>
    public class CourseGroupLms
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is required.
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets the program version definition id.
        /// </summary>
        public Guid ProgramVersionDefinitionId { get; set; }

        /// <summary>
        /// Gets or sets the courses.
        /// </summary>
        public List<CourseLms> Courses { get; set; }
    }
}
