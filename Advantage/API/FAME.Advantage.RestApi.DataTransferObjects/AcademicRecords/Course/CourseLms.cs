﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CourseLms.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the CourseLms type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course
{
    using System;

    /// <summary>
    /// The course lms.
    /// </summary>
    public class CourseLms
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is required.
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName {
            get
            {
                return Name + (IsRequired ? " - R" : " - E");
            }
        }

        

    }
}
