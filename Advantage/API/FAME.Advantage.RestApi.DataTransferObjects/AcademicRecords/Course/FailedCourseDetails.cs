﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FailedCourseDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The failed course details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course
{
    using System.Collections.Generic;

    /// <summary>
    /// The failed course details.
    /// </summary>
    public class FailedCourseDetails
    {
        /// <summary>
        /// Gets or sets the days required to complete failed course.
        /// </summary>
        public double DaysRequiredToCompleteFailedCourse { get; set; }

        /// <summary>
        /// Gets or sets the failed course list.
        /// </summary>
        public List<string> FailedCourseList { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
