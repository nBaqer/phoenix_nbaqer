﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CourseDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CourseDetails type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course
{
    using System;

    /// <summary>
    /// The course details.
    /// </summary>
    public class CourseDetails
    {
        /// <summary>
        /// Gets or sets the req id.
        /// </summary>
        public Guid ReqId { get; set; }

        /// <summary>
        /// Gets or sets the course id.
        /// </summary>
        public int? CourseId { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Descrip { get; set; }

        /// <summary>
        /// Gets or sets the status id.
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the req type id.
        /// </summary>
        public short ReqTypeId { get; set; }

        /// <summary>
        /// Gets or sets the credits.
        /// </summary>
        public decimal? Credits { get; set; }

        /// <summary>
        /// Gets or sets the fin aid credits.
        /// </summary>
        public decimal? FinAidCredits { get; set; }

        /// <summary>
        /// Gets or sets the hours.
        /// </summary>
        public decimal? Hours { get; set; }

        /// <summary>
        /// Gets or sets the cnt.
        /// </summary>
        public short? Cnt { get; set; }

        /// <summary>
        /// Gets or sets the grd lvl id.
        /// </summary>
        public Guid? GrdLvlId { get; set; }

        /// <summary>
        /// Gets or sets the unit type id.
        /// </summary>
        public Guid? UnitTypeId { get; set; }

        /// <summary>
        /// Gets or sets the camp grp id.
        /// </summary>
        public Guid CampGrpId { get; set; }

        /// <summary>
        /// Gets or sets the course catalog.
        /// </summary>
        public string CourseCatalog { get; set; }

        /// <summary>
        /// Gets or sets the course comments.
        /// </summary>
        public string CourseComments { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime? ModDate { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the su.
        /// </summary>
        public bool? Su { get; set; }

        /// <summary>
        /// Gets or sets the pf.
        /// </summary>
        public bool? Pf { get; set; }

        /// <summary>
        /// Gets or sets the course category id.
        /// </summary>
        public Guid? CourseCategoryId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether track tardies.
        /// </summary>
        public bool TrackTardies { get; set; }

        /// <summary>
        /// Gets or sets the tardies making absence.
        /// </summary>
        public int? TardiesMakingAbsence { get; set; }

        /// <summary>
        /// Gets or sets the dept id.
        /// </summary>
        public Guid? DeptId { get; set; }

        /// <summary>
        /// Gets or sets the min date.
        /// </summary>
        public DateTime? MinDate { get; set; }

        /// <summary>
        /// Gets or sets the is com course.
        /// </summary>
        public bool? IsComCourse { get; set; }

        /// <summary>
        /// Gets or sets the is on line.
        /// </summary>
        public bool? IsOnLine { get; set; }

        /// <summary>
        /// Gets or sets the completed date.
        /// </summary>
        public DateTime? CompletedDate { get; set; }

        /// <summary>
        /// Gets or sets the is externship.
        /// </summary>
        public bool? IsExternship { get; set; }

        /// <summary>
        /// Gets or sets the use time clock.
        /// </summary>
        public bool? UseTimeClock { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance only.
        /// </summary>
        public bool IsAttendanceOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow completed course retake.
        /// </summary>
        public bool AllowCompletedCourseRetake { get; set; }
    }
}
