﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentTermination.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentTermination type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination
{
    using System;

    /// <summary>
    /// The student termination.
    /// </summary>
    public class StudentTermination
    {
        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public Guid? StatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public Guid? DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the date withdrawal determined.
        /// </summary>
        public DateTime? DateWithdrawalDetermined { get; set; }

        /// <summary>
        /// Gets or sets the last date attended.
        /// </summary>
        public DateTime? LastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is performing R2T4 calculator is allowed.
        /// </summary>
        public bool? IsPerformingR2T4Calculator { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type.
        /// </summary>
        public Guid? CalculationPeriodType { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        public Guid CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        public Guid? UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the method type.
        /// </summary>
        public int MethodType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether R2T4 approve tab is enabled.
        /// </summary>
        public bool IsR2T4ApproveTabEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether undo termination is enabled.
        /// </summary>
        public bool IsTerminationReversed { get; set; }
    }
}