﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionDetail.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The class section detail.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection
{
    using System;

    /// <summary>
    /// The class section detail.
    /// </summary>
    public class ClassSectionDetail
    {
        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the req id.
        /// </summary>
        public Guid ReqId { get; set; }    

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime EndDate { get; set; }  
    }
}
