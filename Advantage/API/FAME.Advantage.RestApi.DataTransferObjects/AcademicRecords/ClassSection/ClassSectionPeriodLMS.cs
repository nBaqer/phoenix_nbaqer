﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionPeriodLMS.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ClassSectionPeriodLMS type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The class section period lms.
    /// </summary>
    public class ClassSectionPeriodLMS
    {
        /// <summary>
        /// Gets or sets the period id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the period stat date.
        /// </summary>
        public string StatTime { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// Gets or sets the days of week.
        /// </summary>
        public List<string> DaysOfWeek { get; set; }
    }
}
