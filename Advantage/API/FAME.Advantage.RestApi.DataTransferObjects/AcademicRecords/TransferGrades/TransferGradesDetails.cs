﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransferGradesDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TransferGradesDetails type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TransferGrades
{
    using System;

    /// <summary>
    /// The transfer grades details.
    /// </summary>
    public class TransferGradesDetails
    {
        /// <summary>
        /// Gets or sets the transfer id.
        /// </summary>
        public Guid TransferId { get; set; }

        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the req id .
        /// </summary>
        public Guid ReqId { get; set; }

        /// <summary>
        /// Gets or sets the grd sys detail id.
        /// </summary>
        public Guid? GrdSysDetailId { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the mod date.
        /// </summary>
        public DateTime ModDate { get; set; }

        /// <summary>
        /// Gets or sets the mod user.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the is transferred.
        /// </summary>
        public bool? IsTransferred { get; set; }

        /// <summary>
        /// Gets or sets the completed date.
        /// </summary>
        public DateTime? CompletedDate { get; set; }

        /// <summary>
        /// Gets or sets the is clinics satisfied.
        /// </summary>
        public bool? IsClinicsSatisfied { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is course completed.
        /// </summary>
        public bool IsCourseCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is grade overridden.
        /// </summary>
        public bool IsGradeOverridden { get; set; }

        /// <summary>
        /// Gets or sets the grade overridden by.
        /// </summary>
        public string GradeOverriddenBy { get; set; }

        /// <summary>
        /// Gets or sets the grade overridden date.
        /// </summary>
        public DateTime? GradeOverriddenDate { get; set; }
    }
}
