﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentScheduledHours.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentScheduledHours type. Used for calling db function and mapping data 
//   Function : StudentScheduledHours
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;

    /// <summary>
    /// The StudentScheduledHours.
    /// </summary>
    public class StudentScheduledHours
    {
        /// <summary>
        /// Gets or sets the StuEnrollId.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the CalculatedScheduledHours.
        /// </summary>
        public decimal CalculatedScheduledHours { get; set; }

        /// <summary>
        /// Gets or sets the ScheduleId.
        /// </summary>
        public Guid ScheduleId { get; set; }
    }
}
