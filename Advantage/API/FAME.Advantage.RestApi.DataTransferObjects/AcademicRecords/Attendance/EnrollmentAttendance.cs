﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentAttendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The enrollment attendance object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The enrollment attendance encapsulation.
    /// </summary>
    public class EnrollmentAttendance
    {
        /// <summary>
        /// The student enrollment id
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }
        /// <summary>
        /// The attendance summary list
        /// </summary>
        public IEnumerable<Attendance> AttendanceSummary { get; set; }
        /// <summary>
        /// Is makeup included in actual 
        /// </summary>
        public bool MakeupIncluded { get; set; } = false;
        /// <summary>
        /// Is attendance represented in hours
        /// </summary>
        public bool InHours { get; set; } = false;
        /// <summary>
        /// The scheduled hours per week according to time clock schedule.
        /// </summary>
        public decimal ScheduledHoursPerWeek { get; set; }
        public decimal ScheduledHoursLastDate { get; set; }
        public DateTime? LastDateAttended { get; set; }
        public string LastDateAttendedString { get { return LastDateAttended.HasValue ? LastDateAttended.Value.ToString("MM/dd/yyyy") : ""; } }
        public decimal ActualHours
        {
            get
            {
                if (AttendanceSummary.Any())
                {
                    //if makeup is included == true then actual already equals actual + makeup
                    //otherwise actual needs to be set to actual = actual + makeup
                    if (this.MakeupIncluded)
                    {
                        return !InHours
                            ? AttendanceSummary.Max(ats => ats.Actual) / (decimal)60
                            : AttendanceSummary.Max(ats => ats.Actual);
                    }

                    return !InHours
                        ? (AttendanceSummary.Max(ats => ats.Actual) + AttendanceSummary.Max(ats => ats.Makeup)) / (decimal)60
                        : AttendanceSummary.Max(ats => ats.Actual) + AttendanceSummary.Max(ats => ats.Makeup);
                }

                return 0;
            }
        }
        public decimal ScheduledHours
        {
            get
            {
                if (AttendanceSummary.Any())
                {

                    return !InHours
                        ? (AttendanceSummary.Max(ats => ats.Scheduled)) / (decimal)60
                        : AttendanceSummary.Max(ats => ats.Scheduled);
                }

                return 0;
            }
        }
        public decimal TardyHours
        {
            get
            {
                if (AttendanceSummary.Any())
                {

                    return !InHours
                        ? (AttendanceSummary.Max(ats => ats.Tardy)) / (decimal)60
                        : AttendanceSummary.Max(ats => ats.Tardy);
                }

                return 0;
            }
        }
        public decimal AbsentHours
        {
            get
            {
                if (AttendanceSummary.Any())
                {

                    return !InHours
                        ? (AttendanceSummary.Max(ats => ats.Absent)) / (decimal)60
                        : AttendanceSummary.Max(ats => ats.Absent);
                }

                return 0;
            }
        }
        public decimal MakeupHours
        {
            get
            {
                if (AttendanceSummary.Any())
                {

                    return !InHours
                        ? (AttendanceSummary.Max(ats => ats.Makeup)) / (decimal)60
                        : AttendanceSummary.Max(ats => ats.Makeup);
                }

                return 0;
            }
        }

        public decimal TransferHours { get; set; }

        public decimal AttendancePercentage
        {
            get
            {
                if (ActualHours != decimal.Zero && ScheduledHours != decimal.Zero)
                {
                    return (ActualHours / ScheduledHours) * 100;
                }
                return 0;
            }



        }
        public decimal AttendancePercentageFormatted
        {
            get
            {

                return (Decimal.Round(AttendancePercentage, 2));

            }



        }
    }
}
