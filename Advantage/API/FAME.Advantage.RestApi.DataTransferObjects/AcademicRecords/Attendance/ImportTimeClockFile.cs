﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImportTimeClockFile.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The ImportTimeClockFile params object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The ImportTimeClockFile encapsulation.
    /// </summary>
    public class ImportTimeClockFile
    {
        /// <summary>
        /// The CampusId
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// The FilePath
        /// </summary>
        public string FilePath { get; set; }
    }
}
