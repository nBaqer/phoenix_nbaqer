﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Attendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The attendance object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The attendance encapsulation.
    /// </summary>
    public class Attendance
    {
        /// <summary>
        /// The class section id.
        /// </summary>
        public Guid? ClassSectionId { get; set; }
        /// <summary>
        /// Gets or sets the actual.
        /// </summary>
        public decimal Actual { get; set; }

        /// <summary>
        /// Gets or sets the scheduled.
        /// </summary>
        public decimal Scheduled { get; set; }

        /// <summary>
        /// Gets or sets the tardy.
        /// </summary>
        public decimal Tardy { get; set; }
        /// <summary>
        /// Gets or sets the absent.
        /// </summary>
        public decimal Absent { get; set; }
        /// <summary>
        /// Gets or sets the make up.
        /// </summary>
        public decimal Makeup { get; set; }

        /// <summary>
        /// The date attended.
        /// </summary>
        public DateTime AttendedDate { get; set; }

        /// <summary>
        /// Is makeup included in actual 
        /// </summary>
        public bool MakeupIncluded { get; set; } = false;

        /// <summary>
        /// Is attendance represented in hours
        /// </summary>
        public bool InHours { get; set; } = false;

        /// <summary>
        /// The hours attended
        /// </summary>
        public decimal ActualHours
        {
            get
            {
                //if makeup is included == true then actual already equals actual + makeup
                //otherwise actual needs to be set to actual = actual + makeup
                if (!MakeupIncluded)
                {
                    return !InHours ? (Actual + Makeup) / 60 : (Actual + Makeup);
                }

                return !InHours ? Actual / 60 : Actual;
            }
        }
    }
}
