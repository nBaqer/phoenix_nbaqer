﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeClockLogParams.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The TimeClockLogParams params object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;

    /// <summary>
    /// The TimeClockLogParams encapsulation.
    /// </summary>
    public class TimeClockLogParams
    {
        /// <summary>
        /// TimeClockImportLogId
        /// </summary>
        public Guid? TimeClockImportLogId { get; set; }

        /// <summary>
        /// CampusId
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// FileName
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Message Property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Status Property
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// IsAutoImport Property
        /// </summary>
        public bool? IsAutoImport { get; set; }
    }
}
