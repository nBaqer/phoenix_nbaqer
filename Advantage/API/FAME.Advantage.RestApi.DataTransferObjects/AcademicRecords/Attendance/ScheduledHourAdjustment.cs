﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduledHourAdjustment.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ScheduledHourAdjustment params object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;

    /// <summary>
    /// The ScheduledHourAdjustment encapsulation.
    /// </summary>
    public class ScheduledHourAdjustment
    {
        /// <summary>
        /// The LeadId
        /// </summary>
        public Guid LeadId { get; set; }

        /// <summary>
        /// The StudentName
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// The ScheduledHours
        /// </summary>
        public decimal? ScheduledHours { get; set; }

        /// <summary>
        /// The TotalPriorAdjustments
        /// </summary>
        public decimal TotalPriorAdjustments { get; set; }

        /// <summary>
        /// The ActualHours
        /// </summary>
        public decimal ActualHours { get; set; }

        /// <summary>
        /// The ProgramVersion
        /// </summary>
        public string ProgramVersion { get; set; }

        /// <summary>
        /// The StudentGroups
        /// </summary>
        public string StudentGroups { get; set; }

        /// <summary>
        /// The StuEnrollId
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// The ScheduleId
        /// </summary>
        public Guid ScheduleId { get; set; }
    }
}
