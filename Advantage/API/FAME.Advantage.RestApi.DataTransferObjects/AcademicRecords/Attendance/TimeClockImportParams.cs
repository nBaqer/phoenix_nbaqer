﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeClockImportParams.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The parameters for sending to the service method aspx class request
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    /// <summary>
    /// The TimeClockImportParams encapsulation.
    /// </summary>
    public class TimeClockImportParams
    {
        /// <summary>
        /// The Username property
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The TenantUserId property
        /// </summary>
        public string TenantUserId { get; set; }

        /// <summary>
        /// The TenantId property
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// The AdvantageApiToken property
        /// </summary>
        public string AdvantageApiToken { get; set; }

        /// <summary>
        /// The FileName property
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// The CampusId property
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// The ApiUrl property
        /// </summary>
        public string ApiUrl { get; set; }
    }
}
