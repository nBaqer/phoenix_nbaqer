﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PostAttendanceZero.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The post attendance zero object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The post attendance zero encapsulation.
    /// </summary>
    public class PostAttendanceZero
    {
        /// <summary>
        /// The filter campus id
        /// </summary>
        public string CampusId { get; set; }

        /// <summary>
        /// The dates to post to zero
        /// </summary>
        public List<DateTime> Dates { get; set; }

        /// <summary>
        /// The program filter
        /// </summary>
        public string Program { get; set; }

        /// <summary>
        /// The program filter
        /// </summary>
        public string ProgramVersion { get; set; }

        /// <summary>
        /// The program version filter
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// The student group id filter
        /// </summary>
        public string StudentGroupId { get; set; }
        /// <summary>
        /// The badge number filter
        /// </summary>
        public string BadgeNumber { get; set; }

        /// <summary>
        /// The start date filter
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// The end date filter
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// The in school status flag
        /// </summary>
        public bool InSchoolStatus { get; set; }

        /// <summary>
        /// The currently attending flag
        /// </summary>
        public string CurrentlyAttending { get; set; }

        /// <summary>
        /// The future start flag
        /// </summary>
        public string FutureStart { get; set; }

        /// <summary>
        /// The leave of absense flag
        /// </summary>
        public string LeaveOfAbsense { get; set; }

        /// <summary>
        /// The out of school status flag
        /// </summary>
        public bool OutOfSchoolStatus { get; set; }

        /// <summary>
        /// The dropped out flag
        /// </summary>
        public string DroppedOut { get; set; }

        /// <summary>
        /// The expelled flag
        /// </summary>
        public string Expelled { get; set; }

        /// <summary>
        /// The graduated flag
        /// </summary>
        public string Graduated { get; set; }

        /// <summary>
        /// The no show flag
        /// </summary>
        public string NoShow { get; set; }
    }
}
