﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PostZeroParams.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The post attendance zero params object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The post attendance zero encapsulation.
    /// </summary>
    public class PostZeroParams
    {
        /// <summary>
        /// The list of student enrollment ids
        /// </summary>
        public List<Guid> StuEnrollIdList { get; set; }

        /// <summary>
        /// The date to post to zeros for
        /// </summary>
        public DateTime Date { get; set; }
    }
}
