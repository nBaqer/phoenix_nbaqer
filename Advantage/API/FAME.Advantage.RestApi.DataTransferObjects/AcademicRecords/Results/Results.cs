﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Results.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Results type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Results
{ 
    using System;

    /// <summary>
    /// The results.
    /// </summary>
    public class Results
    {
        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        public Guid ResultId { get; set; }

        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public Guid? ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets the grade system detail id.
        /// </summary>
        public Guid? GradeSystemDetailId { get; set; }

        /// <summary>
        /// Gets or sets the cnt.
        /// </summary>
        public int? Cnt { get; set; }

        /// <summary>
        /// Gets or sets the hours.
        /// </summary>
        public int? Hours { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the is in complete.
        /// </summary>
        public bool? IsInComplete { get; set; }

        /// <summary>
        /// Gets or sets the dropped in add drop.
        /// </summary>
        public bool? DroppedInAddDrop { get; set; }

        /// <summary>
        /// Gets or sets the modified user.
        /// </summary>
        public string ModifiedUser { get; set; }

        /// <summary>
        /// Gets or sets the modified date.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the is transferred.
        /// </summary>
        public bool? IsTransferred { get; set; }

        /// <summary>
        /// Gets or sets the is clinics satisfied.
        /// </summary>
        public bool? IsClinicsSatisfied { get; set; }

        /// <summary>
        /// Gets or sets the date determined.
        /// </summary>
        public DateTime? DateDetermined { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is course completed.
        /// </summary>
        public bool IsCourseCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is grade overridden.
        /// </summary>
        public bool IsGradeOverridden { get; set; }

        /// <summary>
        /// Gets or sets the grade overridden by.
        /// </summary>
        public string GradeOverriddenBy { get; set; }

        /// <summary>
        /// Gets or sets the grade overridden date.
        /// </summary>
        public DateTime? GradeOverriddenDate { get; set; }

        /// <summary>
        /// Gets or sets the date completed.
        /// </summary>
        public DateTime? DateCompleted { get; set; } 
    }
}
