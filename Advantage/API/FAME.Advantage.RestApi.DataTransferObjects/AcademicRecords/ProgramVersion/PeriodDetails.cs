﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Period Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    /// <summary>
    /// The period details.
    /// </summary>
    public class PeriodDetails
    {
        /// <summary>
        /// Gets or sets the period number.
        /// </summary>
        public int PeriodNumber { get; set; }

        /// <summary>
        /// Gets or sets the period length.
        /// </summary>
        public decimal PeriodLength { get; set; }
    }
}
