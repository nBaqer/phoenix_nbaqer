﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourPaymentPeriodDaysDetail.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The credit hour payment period days details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The credit hour payment period days detail.
    /// </summary>
    public class CreditHourPaymentPeriodDaysDetail
    {
        /// <summary>
        /// Gets or sets the completed days.
        /// </summary>
        public double CompletedDays { get; set; }

        /// <summary>
        /// Gets or sets the total days.
        /// </summary>
        public double TotalDays { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
