﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduleBreakDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The schedule break details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System.Collections.Generic;

    /// <summary>
    /// The schedule break details.
    /// </summary>
    public class ScheduleBreakDetails
    {
        /// <summary>
        /// Gets or sets the schedule breaks count.
        /// </summary>
        public int ScheduleBreaksCount { get; set; }
        
        /// <summary>
        /// Gets or sets the total schedule break days.
        /// </summary>
        public int TotalScheduleBreakDays { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }

    }
}
