﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourProgramDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Credit Hour Program Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    /// <summary>
    /// The credit hour program details.
    /// </summary>
    public class CreditHourProgramDetails
    {
        /// <summary>
        /// Gets or sets the number of credits.
        /// </summary>
        public decimal Credits { get; set; }
        
        /// <summary>
        /// Gets or sets the number of weeks.
        /// </summary>
        public short Weeks { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
     }
}
