﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentPeriods.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Enrollment Periods Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    /// <summary>
    /// The enrollment period details.
    /// </summary>
    public class EnrollmentPeriods
    {
        /// <summary>
        /// Gets or sets the enrollment period.
        /// </summary>
        public int EnrollmentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the enrollment period length.
        /// </summary>
        public decimal EnrollmentLength { get; set; }

        /// <summary>
        /// Gets or sets the allowed excused absence.
        /// </summary>
        public decimal AllowedExcusedAbsence { get; set; }
    }
}
