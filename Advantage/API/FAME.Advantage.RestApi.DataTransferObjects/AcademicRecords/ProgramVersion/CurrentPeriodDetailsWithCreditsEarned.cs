﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CurrentPeriodDetailsWithCreditsEarned.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The current payment period with credits earned.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The current payment period with credits earned.
    /// </summary>
    public class CurrentPeriodDetailsWithCreditsEarned
    {
        /// <summary>
        /// Gets or sets the credits earned.
        /// </summary>
        public decimal CreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets the weeks completed.
        /// </summary>
        public int WeeksCompleted { get; set; }

        /// <summary>
        /// Gets or sets the credits earned on date.
        /// </summary>
        public DateTime? CreditsEarnedOn { get; set; }

        /// <summary>
        /// Gets or sets the weeks completed on date.
        /// </summary>
        public DateTime? WeeksCompletedOn { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal period.
        /// </summary>
        public int WithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the start date of withdrawal period.
        /// </summary>
        public DateTime? StartDateOfWithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the end date of withdrawal period.
        /// </summary>
        public DateTime? EndDateOfWithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
 }
