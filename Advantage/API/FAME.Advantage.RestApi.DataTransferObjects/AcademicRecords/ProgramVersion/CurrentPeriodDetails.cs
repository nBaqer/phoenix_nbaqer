﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CurrentPeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The current period details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The current period details. class will have the StartDateOfWithdrawalPeriod, PeriodNumber, ResultStatus which is gives the student's current period details.
    /// </summary>
    /// <remarks>
    /// The object of CurrentPeriodDetails is a data transfer object which holds the parameters to display the CurrentPeriodDetails.
    /// </remarks>
    public class CurrentPeriodDetails
    {
        /// <summary>
        /// Gets or sets the start date of withdrawal period.
        /// </summary>
        public DateTime? StartDateOfWithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal period.
        /// </summary>
        public int WithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the hour debited to previous period.
        /// </summary>
        public decimal HourDebitedToPreviousPeriod { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
