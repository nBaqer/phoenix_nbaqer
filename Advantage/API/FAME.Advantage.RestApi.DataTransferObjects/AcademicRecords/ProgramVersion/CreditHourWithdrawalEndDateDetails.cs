﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourWithdrawalEndDateDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Credit Hour Withdrawal EndDate Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The credit hour withdrawal end date details.
    /// </summary>
    public class CreditHourWithdrawalEndDateDetails
    {
        /// <summary>
        /// Gets or sets the end date of withdrawal period.
        /// </summary>
        public DateTime? EndDateOfWithdrawalPeriod { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
