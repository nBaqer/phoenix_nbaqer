﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourPaymentPeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The credit hour payment period details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The credit hour payment period details.
    /// </summary>
    public class CreditHourPaymentPeriodDetails
    {
        /// <summary>
        /// Gets or sets the withdrawal payment period start date.
        /// </summary>
        public DateTime WithdrawalPaymentPeriodStartDate { get; set; }

        /// <summary>
        /// Gets or sets the weeks required for the withdrawal payment period.
        /// </summary>
        public int WeeksRequiredForTheWithdrawalPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the weeks completed in withdrawal payment period.
        /// </summary>
        public int WeeksCompletedInWithdrawalPaymentPeriod { get; set; }
        
        /// <summary>
        /// Gets or sets the withdrawal payment period.
        /// </summary>
        public int WithdrawalPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the credits earned in withdrawal payment period.
        /// </summary>
        public decimal CreditsEarnedInWithdrawalPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the credits required for the withdrawal payment period.
        /// </summary>
        public decimal CreditsRequiredForWithdrawalPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the term of withdrawal.
        /// </summary>
        public string TermOfWithdrawal { get; set; }

        /// <summary>
        /// Gets or sets the term start date.
        /// </summary>
        public DateTime TermStartDate { get; set; }

        /// <summary>
        /// Gets or sets the term end date.
        /// </summary>
        public DateTime TermEndDate { get; set; }

        /// <summary>
        /// Gets or sets the term name.
        /// </summary>
        public string TermName { get; set; }

        /// <summary>
        /// Gets or sets the payment period term type.
        /// </summary>
        public int PaymentPeriodTermType { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}