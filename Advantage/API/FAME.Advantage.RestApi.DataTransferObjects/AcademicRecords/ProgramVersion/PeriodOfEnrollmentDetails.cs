﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodOfEnrollmentDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PeriodOfEnrollmentDetails type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{ 
    using System.Collections.Generic;

    /// <summary>
    /// The period of enrollment details.
    /// </summary>
    public class PeriodOfEnrollmentDetails
    {
        /// <summary>
        /// Gets or sets the period of enrollments.
        /// </summary>
        public IList<EnrollmentPeriods> PeriodOfEnrollments { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
