﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentPeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Enrollment Period Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System.Collections.Generic;

    /// <summary>
    /// The enrollment period details.
    /// </summary>
    public class EnrollmentPeriodDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentPeriodDetails"/> class.
        /// </summary>
        public EnrollmentPeriodDetails()
        {
            this.EnrollmentPeriodsList = new List<EnrollmentPeriods>();
        }

        /// <summary>
        /// Gets or sets the enrollment period list.
        /// </summary>
        public IList<EnrollmentPeriods> EnrollmentPeriodsList { get; set; }

        /// <summary>
        /// Gets or sets the number of periods of enrollment.
        /// </summary>
        public int? NumberOfPeriodOfEnrollment { get; set; }

        /// <summary>
        /// Gets or sets the Number Of Full Academic Years.
        /// </summary>
        public int NumberOfFullAcademicYears { get; set; }

        /// <summary>
        /// Gets or sets the length of last period of enrollment.
        /// </summary>
        public decimal LastEnrollmentPeriodLength { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
