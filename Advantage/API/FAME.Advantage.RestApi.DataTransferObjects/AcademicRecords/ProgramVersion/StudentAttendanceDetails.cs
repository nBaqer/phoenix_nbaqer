﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAttendanceDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student attendance details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System.Collections.Generic;

    /// <summary>
    /// The student attendance details.
    /// </summary>
    public class StudentAttendanceDetails
    {
        /// <summary>
        /// Gets or sets the actual attendance list.
        /// </summary>
        public List<AttendanceWithDate> ActualAttendanceList { get; set; }

        /// <summary>
        /// Gets or sets the excused absence list.
        /// </summary>
        public List<AttendanceWithDate> ExcusedAbsenceList { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
