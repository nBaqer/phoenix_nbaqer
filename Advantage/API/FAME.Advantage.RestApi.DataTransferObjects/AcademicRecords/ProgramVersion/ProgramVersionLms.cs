﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionCourseLms.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the ProgramVersionCourseLms type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;

    /// <summary>
    /// The program version course lms.
    /// </summary>
    public class ProgramVersionLms
    {
        /// <summary>
        /// Gets or sets the program name.
        /// </summary>
        public string ProgramName { get; set; }

        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public Guid ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the courses.
        /// </summary>
        public List<ProgramVersionDefinitionCourseLms> Courses { get; set; }

        /// <summary>
        /// Gets or sets the course groups.
        /// </summary>
        public List<CourseGroupLms> CourseGroups { get; set; }

    }
}
