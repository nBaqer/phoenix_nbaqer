﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduledAndTotalHours.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ScheduledAndTotalHours type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    /// <summary>
    /// The scheduled and total hours.
    /// </summary>
    public class ScheduledAndTotalHours
    {
        /// <summary>
        /// Gets or sets the total hours in period.
        /// </summary>
        public decimal TotalHoursInPeriod { get; set; }

        /// <summary>
        /// Gets or sets the hours scheduled to complete.
        /// </summary>
        public decimal HoursScheduledToComplete { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
