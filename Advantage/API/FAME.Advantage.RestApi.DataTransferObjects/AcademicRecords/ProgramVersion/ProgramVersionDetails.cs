﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Program Version Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The program version details.
    /// </summary>
    public class ProgramVersionDetails
    {
        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public Guid? ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the academic calendar id.
        /// </summary>
        public int? AcademicCalendarId { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal date.
        /// </summary>
        public DateTime? WithdrawalDate { get; set; }

        /// <summary>
        /// Gets or sets the program Campus id.
        /// </summary>
        public Guid CampusId { get; set; }
    }
}
