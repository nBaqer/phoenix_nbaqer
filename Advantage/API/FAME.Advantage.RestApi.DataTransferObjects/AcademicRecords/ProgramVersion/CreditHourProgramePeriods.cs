﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourProgramePeriods.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the credit hour programe periods.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    /// <summary>
    /// The credit hour programe.
    /// </summary>
    public class CreditHourProgramePeriods
    {
        /// <summary>
        /// Gets or sets the period.
        /// </summary>
        public int Period { get; set; }

        /// <summary>
        /// Gets or sets the number of credits.
        /// </summary>
        public decimal Credits { get; set; }

        /// <summary>
        /// Gets or sets the number of weeks.
        /// </summary>
        public short Weeks { get; set; }
     }
}
