﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionDetails.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Program Version Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    /// <summary>
    /// The program and Program version details.
    /// </summary>
    public class ProgramAndProgramVersionName
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string ProgramName { get; set; }
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the ProgramVersions list.
        /// </summary>
        public IList<IListItem<string, Guid>> ProgramVersions { get; set; }
    }
}
