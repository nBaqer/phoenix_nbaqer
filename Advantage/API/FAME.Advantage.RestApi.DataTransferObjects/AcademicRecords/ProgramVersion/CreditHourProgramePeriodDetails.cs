﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditHourProgramePeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the credit hour programe period details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System.Collections.Generic;

    /// <summary>
    /// The credit hour programe period details.
    /// </summary>
    public class CreditHourProgramePeriodDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditHourProgramePeriodDetails"/> class.
        /// </summary>
        public CreditHourProgramePeriodDetails()
        {
            this.CreditHourProgramePeriodDetailsList = new List<CreditHourProgramePeriods>();
        }

        /// <summary>
        /// Gets or sets the credit hour programe details list.
        /// </summary>
        public IList<CreditHourProgramePeriods> CreditHourProgramePeriodDetailsList { get; set; }

        /// <summary>
        /// Gets or sets the number of payment periods.
        /// </summary>
        public int? NumberOfPaymentPeriods { get; set; }

        /// <summary>
        /// Gets or sets the Number Of Full Academic Years.
        /// </summary>
        public int NumberOfFullAcademicYears { get; set; }

        /// <summary>
        /// Gets or sets the last payment period credits.
        /// </summary>
        public decimal LastPaymentPeriodCredits { get; set; }

        /// <summary>
        /// Gets or sets the last payment period weeks.
        /// </summary>
        public decimal LastPaymentPeriodWeeks { get; set; }

        /// <summary>
        /// Gets or sets the length of last period of enrollment.
        /// </summary>
        public decimal LastEnrollmentPeriodLength { get; set; }

        /// <summary>
        /// Gets or sets the last week of enrollment.
        /// </summary>
        public decimal LastEnrollmentWeek { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
