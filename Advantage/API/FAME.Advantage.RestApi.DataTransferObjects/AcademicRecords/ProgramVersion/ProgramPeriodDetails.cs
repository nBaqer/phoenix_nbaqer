﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramPeriodDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Program Period Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System.Collections.Generic;

    /// <summary>
    /// The program period details.
    /// </summary>
    public class ProgramPeriodDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramPeriodDetails"/> class.
        /// </summary>
        public ProgramPeriodDetails()
        {
            this.PeriodDetailsList = new List<PeriodDetails>();
        }

        /// <summary>
        /// Gets or sets the period details list.
        /// </summary>
        public IList<PeriodDetails> PeriodDetailsList { get; set; }

        /// <summary>
        /// Gets or sets the number of payment periods.
        /// </summary>
        public int? NumberOfPaymentPeriods { get; set; }
        
        /// <summary>
        /// Gets or sets the Number Of Full Academic Years.
        /// </summary>
        public int NumberOfFullAcademicYears { get; set; }

        /// <summary>
        /// Gets or sets the Number Of Full Academic Years.
        /// </summary>
        public decimal LastPaymentPeriodLength { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
