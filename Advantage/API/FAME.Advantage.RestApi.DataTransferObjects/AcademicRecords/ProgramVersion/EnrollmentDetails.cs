﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentDetails.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The enrollment Details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;

    /// <summary>
    /// The enrollment Details.
    /// </summary>
    public class EnrollmentDetails
    {
        /// <summary>
        /// Gets or sets the start date time.
        /// </summary>
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end date time.
        /// </summary>
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the enrollment.
        /// </summary>
        public Enrollment Enrollment { get; set; }

        /// <summary>
        /// Gets or sets the allowed excused absence per payment period.
        /// </summary>
        public decimal AllowExcusedAbsPerPayPrd { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is Self Paced.
        /// </summary>
        public bool IsSelfPaced { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is payment period.
        /// </summary>
        public bool IsPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the academic calendars.
        /// </summary>
        public int? AcademicCalendarId { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}
