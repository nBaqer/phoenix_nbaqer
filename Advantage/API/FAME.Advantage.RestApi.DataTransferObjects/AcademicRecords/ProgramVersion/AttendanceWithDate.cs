﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttendanceWithDate.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AttendanceWithDate type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion
{
    using System;

    /// <summary>
    /// The attendance with date.
    /// </summary>
    public class AttendanceWithDate
    {
        /// <summary>
        /// Gets or sets the meeting id.
        /// </summary>
        public Guid MeetingId { get; set; }

        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public Guid ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the meeting date.
        /// </summary>
        public DateTime MeetingDate { get; set; }

        /// <summary>
        /// Gets or sets the attendance value.
        /// </summary>
        public decimal AttendanceValue { get; set; }
    }
}
