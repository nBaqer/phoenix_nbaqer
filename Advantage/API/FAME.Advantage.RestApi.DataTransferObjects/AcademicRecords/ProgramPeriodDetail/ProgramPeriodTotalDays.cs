﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramPeriodTotalDays.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program period total days.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramPeriodDetail
{
    /// <summary>
    /// The program period total days.
    /// </summary>
    public class ProgramPeriodTotalDays
    {
        /// <summary>
        /// Gets or sets the total days.
        /// </summary>
        public int TotalDays { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }
    }
}