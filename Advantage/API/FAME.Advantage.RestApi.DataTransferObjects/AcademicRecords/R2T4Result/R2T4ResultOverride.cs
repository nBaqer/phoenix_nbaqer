﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4ResultOverride.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 result override.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result
{
    using System;

    /// <summary>
    /// The R2T4 result override .
    /// </summary>
    public class R2T4ResultOverride
    {
        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets the tab id.
        /// </summary>
        public short TabId { get; set; }
    }
}
