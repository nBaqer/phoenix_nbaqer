﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4ResultsCompletedStatus.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The R2T4 results completed status .
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result
{
    using System;

    /// <summary>
    /// The R2T4 results completed status.
    /// </summary>
    public class R2T4ResultsCompletedStatus
    {
        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether status.
        /// </summary>
        public bool Status { get; set; }
    }
}
