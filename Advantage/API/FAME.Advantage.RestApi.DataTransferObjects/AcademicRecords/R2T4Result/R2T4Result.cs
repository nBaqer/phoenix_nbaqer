﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4Result.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result
{
    using System;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;

    /// <summary>
    /// The R2T4 result. This is a DTO model to represent R2T4Result entity in the client side
    /// </summary>
    public class R2T4Result
    {
        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        /// <summary>
        /// Gets or sets the R2T4 results id.
        /// </summary>
        public Guid? R2T4ResultsId { get; set; }

        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets the amount disbursed subtotal A.
        /// </summary>
        public decimal? SubTotalAmountDisbursedA { get; set; }

        /// <summary>
        /// Gets or sets the amount that could have been disbursed subtotal C.
        /// </summary>
        public decimal? SubTotalAmountCouldDisbursedC { get; set; }

        /// <summary>
        /// Gets or sets the net amount disbursed sub total b.
        /// </summary>
        public decimal? SubTotalNetAmountDisbursedB { get; set; }

        /// <summary>
        /// Gets or sets the net amount that could have been disbursed subtotal D.
        /// </summary>
        public decimal? SubTotalNetAmountDisbursedD { get; set; }

        /// <summary>
        /// Gets or sets the total title IV aid disbursed for the period box E result.
        /// </summary>
        public decimal? BoxEResult { get; set; }

        /// <summary>
        /// Gets or sets the total title IV grant aid disbursed and that could have been disbursed for the period box F result.
        /// </summary>
        public decimal? BoxFResult { get; set; }

        /// <summary>
        /// Gets or sets the total title IV aid disbursed and that could have been disbursed for the period box G result.
        /// </summary>
        public decimal? BoxGResult { get; set; }

        /// <summary>
        /// Gets or sets the percentage of actual attendance.
        /// </summary>
        public decimal? PercentageOfActualAttendance { get; set; }

        /// <summary>
        /// Gets or sets the box H result.
        /// </summary>
        public decimal? BoxHResult { get; set; }

        /// <summary>
        /// Gets or sets the box I result.
        /// </summary>
        public decimal? BoxIResult { get; set; }

        /// <summary>
        /// Gets or sets the box J result.
        /// </summary>
        public decimal? BoxJResult { get; set; }

        /// <summary>
        /// Gets or sets the box K result.
        /// </summary>
        public decimal? BoxKResult { get; set; }

        /// <summary>
        /// Gets or sets the box L result.
        /// </summary>
        public decimal? BoxLResult { get; set; }

        /// <summary>
        /// Gets or sets the box M result.
        /// </summary>
        public decimal? BoxMResult { get; set; }

        /// <summary>
        /// Gets or sets the box N result.
        /// </summary>
        public decimal? BoxNResult { get; set; }

        /// <summary>
        /// Gets or sets the box O result.
        /// </summary>
        public decimal? BoxOResult { get; set; }

        /// <summary>
        /// Gets or sets the unsubsidized direct loan school return.
        /// </summary>
        public decimal? UnsubDirectLoanSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the subsidized loan school return.
        /// </summary>
        public decimal? SubDirectLoanSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the perkins loan school return.
        /// </summary>
        public decimal? PerkinsLoanSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the direct graduate plus loan school return.
        /// </summary>
        public decimal? DirectGraduatePlusLoanSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the direct parent plus loan school return.
        /// </summary>
        public decimal? DirectParentPlusLoanSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the box P result.
        /// </summary>
        public decimal? BoxPResult { get; set; }

        /// <summary>
        /// Gets or sets the pell grant school return.
        /// </summary>
        public decimal? PellGrantSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the FSEOG school return.
        /// </summary>
        public decimal? FseogSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the teach grant school return.
        /// </summary>
        public decimal? TeachGrantSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the Iraq Afghanistan grant school return.
        /// </summary>
        public decimal? IraqAfgGrantSchoolReturn { get; set; }

        /// <summary>
        /// Gets or sets the box Q result.
        /// </summary>
        public decimal? BoxQResult { get; set; }

        /// <summary>
        /// Gets or sets the box R result.
        /// </summary>
        public decimal? BoxRResult { get; set; }

        /// <summary>
        /// Gets or sets the box S result.
        /// </summary>
        public decimal? BoxSResult { get; set; }

        /// <summary>
        /// Gets or sets the box T result.
        /// </summary>
        public decimal? BoxTResult { get; set; }

        /// <summary>
        /// Gets or sets the box U result.
        /// </summary>
        public decimal? BoxUResult { get; set; }

        /// <summary>
        /// Gets or sets the pell grant amount to return.
        /// </summary>
        public decimal? PellGrantAmountToReturn { get; set; }

        /// <summary>
        /// Gets or sets the FSEOG amount to return.
        /// </summary>
        public decimal? FseogAmountToReturn { get; set; }

        /// <summary>
        /// Gets or sets the teach grant amount to return.
        /// </summary>
        public decimal? TeachGrantAmountToReturn { get; set; }

        /// <summary>
        /// Gets or sets the Iraq Afghanistan grant amount to return.
        /// </summary>
        public decimal? IraqAfgGrantAmountToReturn { get; set; }

        /// <summary>
        /// Gets or sets the board fee.
        /// </summary>
        public decimal? BoardFee { get; set; }

        /// <summary>
        /// Gets or sets the completed time.
        /// </summary>
        public decimal? CompletedTime { get; set; }

        /// <summary>
        /// Gets or sets the direct graduate plus loan could disbursed.
        /// </summary>
        public decimal? DirectGraduatePlusLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct graduate plus loan disbursed.
        /// </summary>
        public decimal? DirectGraduatePlusLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct parent plus loan could disbursed.
        /// </summary>
        public decimal? DirectParentPlusLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct parent plus loan disbursed.
        /// </summary>
        public decimal? DirectParentPlusLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the fseog could disbursed.
        /// </summary>
        public decimal? FseogCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the fseog disbursed.
        /// </summary>
        public decimal? FseogDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the iraq afg grant could disbursed.
        /// </summary>
        public decimal? IraqAfgGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the iraq afg grant disbursed.
        /// </summary>
        public decimal? IraqAfgGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance not required.
        /// </summary>
        public bool IsAttendanceNotRequired { get; set; }

       /// <summary>
        /// Gets or sets the other fee.
        /// </summary>
        public decimal? OtherFee { get; set; }

        /// <summary>
        /// Gets or sets the pell grant could disbursed.
        /// </summary>
        public decimal? PellGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the pell grant disbursed.
        /// </summary>
        public decimal? PellGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the perkins loan could disbursed.
        /// </summary>
        public decimal? PerkinsLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the credit balance refunded.
        /// </summary>
        public decimal? CreditBalanceRefunded { get; set; }

        /// <summary>
        /// Gets or sets the perkins loan disbursed.
        /// </summary>
        public decimal? PerkinsLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the room fee.
        /// </summary>
        public decimal? RoomFee { get; set; }

        /// <summary>
        /// Gets or sets the scheduled end date.
        /// </summary>
        public DateTime? ScheduledEndDate { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the sub loan net amount could disbursed.
        /// </summary>
        public decimal? SubLoanNetAmountCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the sub loan net amount disbursed.
        /// </summary>
        public decimal? SubLoanNetAmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the teach grant could disbursed.
        /// </summary>
        public decimal? TeachGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the teach grant disbursed.
        /// </summary>
        public decimal? TeachGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the total time.
        /// </summary>
        public decimal? TotalTime { get; set; }

        /// <summary>
        /// Gets or sets the tuition fee.
        /// </summary>
        public decimal? TuitionFee { get; set; }

        /// <summary>
        /// Gets or sets the unsub loan net amount could disbursed.
        /// </summary>
        public decimal? UnsubLoanNetAmountCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the unsub loan net amount disbursed.
        /// </summary>
        public decimal? UnsubLoanNetAmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal date.
        /// </summary>
        public DateTime? WithdrawalDate { get; set; }

        /// <summary>
        /// Gets or sets the IsInputIncluded flag.
        /// </summary>
        public bool IsInputIncluded { get; set; }

        /// <summary>
        /// Gets or sets the created by id.
        /// </summary>
        public Guid? CreatedById { get; set; }

        /// <summary>
        /// Gets or sets the program unit type id.
        /// </summary>
        public int ProgramUnitTypeId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        public Guid? UpdatedById { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ticket number.
        /// </summary>
        public long? TicketNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether isoverride or not.
        /// </summary>
        public bool Isoverride { get; set; }

        /// <summary>
        /// Gets or sets the full name of created or updated by user.
        /// </summary>
        public string UpdatedByFullName { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public string ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the Post withdrawal data
        /// </summary>
        public string PostWithdrawalData { get; set; }

        /// <summary>
        /// Gets or sets the Overridden data
        /// </summary>
        public string OverriddenData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 results completed.
        /// </summary>
        public bool IsR2T4ResultsCompleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is R2T4 override results completed.
        /// </summary>
        public bool IsR2T4OverrideResultsCompleted { get; set; }

        #region Duplicated result fields

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtEa { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtEb { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtFa { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtFc { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtStep1Fa { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtStep1Fb { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtStep1Fc { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtStep1Fd { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtBoxh { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtBoxg { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt4Ji { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt4Je { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt4Ke { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt4Ki { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt5Boxh { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt5Boxl { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt5Boxm { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt7k { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt7o { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt8boxb { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt8p { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt9Boxq { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt9Boxr { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt9Boxf { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt9Boxs { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string Txt9Boxt { get; set; }

        #endregion

        #region Post Withdrawal fields
        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtPWD { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtPWDBox2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtPWDOffered { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtPWDBox2Offered { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string TxtPWDBox3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPell3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPell6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdFSEOG3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdFSEOG6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTeach3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTeach6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdIASG3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdIASG6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdPerkins6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdSub6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdUnSub6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdGrad6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdParent6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal1 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal2 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal3 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal4 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal5 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string PwdTotal6 { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string DtPostWithdrwal { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string DtDeadline { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string ChkResponseReceived { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string DtResponseReceived { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string ChkResponseNotReceived { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string ChkNotAccept { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string DtGrantTransferred { get; set; }

        /// <summary>
        /// Gets or sets the box TxtEa result.
        /// </summary>
        public string DtLoanTransferred { get; set; }
        #endregion

        /// <summary>
        /// Gets or sets the student info.
        /// </summary>
        public StudentInfoForReports StudentInfo { get; set; }
        }
}
