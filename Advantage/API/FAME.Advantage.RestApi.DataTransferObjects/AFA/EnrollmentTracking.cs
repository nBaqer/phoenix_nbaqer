﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentTracking.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The enrollment tracking parent object.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The enrollment tracking DTO.
    /// </summary>
    public class EnrollmentTracking
    {
        /// <summary>
        /// The student enrollment id.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// The enrollment status description.
        /// </summary>
        public string EnrollmentStatus { get; set; }

        /// <summary>
        /// The modified date.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// The retry count
        /// </summary>
        public int RetryCount { get; set; }

        /// <summary>
        /// The enrollment data to send to AFA
        /// </summary>
        public AdvStagingEnrollmentV2 Enrollment { get; set; }
    }
}
