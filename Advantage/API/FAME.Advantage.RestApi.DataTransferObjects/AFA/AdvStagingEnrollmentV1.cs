﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Newtonsoft.Json;

    public class AdvStagingEnrollmentV1 : EntityBase, IActionResultStatus
    {
        public long AdvStagingEnrollmentID { get; set; }
        public string LocationCMSID { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public string ProgramName { get; set; }
        public Guid ProgramVersionID { get; set; }
        public long IDStudent { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime GradDate { get; set; }
        public bool Processed { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
