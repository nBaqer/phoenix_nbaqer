﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.DataTransferObjects.Common;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using Newtonsoft.Json;

    public class AdvStagingAwardV1: EntityBase, IActionResultStatus
    {
        public long IDAdvStagingAward { get; set; }
        public string LocationCMSID { get; set; }
        public long IDStudent { get; set; }
        public string FundCode { get; set; }
        public string AwardID { get; set; }
        public string LoanID { get; set; }
        public string AwardStatus { get; set; }
        public string AwardYear { get; set; }
        public DateTime AcadYrStart { get; set; }
        public DateTime AcadYrEnd { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public bool Deleted { get; set; }
        public bool Processed { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
