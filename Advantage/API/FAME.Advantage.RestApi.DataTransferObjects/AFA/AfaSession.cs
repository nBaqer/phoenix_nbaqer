﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaSession.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Afa Session.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;

    /// <summary>
    /// The AFA session.
    /// </summary>
    public class AfaSession : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the client key.
        /// </summary>
        public string ClientKey { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the session key.
        /// </summary>
        public string SessionKey { get; set; }

        /// <summary>
        /// Gets or sets the id student.
        /// </summary>
        public long IdStudent { get; set; }

        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
