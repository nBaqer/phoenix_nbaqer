﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationLogger.cs" company="FAME Inc">
//   2019
// </copyright>
// <summary>
//   Defines the WapiOperationLogger type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    public class WapiOperationLogger : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }
        /*<summary>The get and set for Id</summary>*/
        public long Id { get; set; }
        /*<summary>The get and set for ServiceCode</summary>*/
        public string ServiceCode { get; set; }
        /*<summary>The get and set for CompanyCode</summary>*/
        public string CompanyCode { get; set; }
        /*<summary>The get and set for DateExecution</summary>*/
        public DateTime DateExecution { get; set; }
        /*<summary>The get and set for NextPlanningDate</summary>*/
        public DateTime? NextPlanningDate { get; set; }
        /*<summary>The get and set for IsError</summary>*/
        public bool IsError { get; set; }
        /*<summary>The get and set for Comment</summary>*/
        public string Comment { get; set; }

    }
}

