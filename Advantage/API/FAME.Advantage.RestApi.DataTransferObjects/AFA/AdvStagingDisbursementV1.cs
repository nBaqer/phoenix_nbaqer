﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.DataTransferObjects.Common;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using Newtonsoft.Json;

    public class AdvStagingDisbursementV1 : EntityBase, IActionResultStatus
    {
        public long IDAdvStagingDisbursement { get; set; }
        public string LocationCMSID { get; set; }
        public int DisbursementSequenceNumber { get; set; }
        public string AwardID { get; set; }
        public long IDStudent { get; set; }
        public DateTime AnticipatedDisbursementDate { get; set; }
        public decimal AnticipatedGrossAmount { get; set; }
        public decimal AnticipatedNetDisbursementAmt { get; set; }
        public decimal FeeAmount { get; set; }
        public DateTime ActualDisbursementDate { get; set; }
        public decimal ActualNetDisbursementAmt { get; set; }
        public decimal ActualGrossAmount { get; set; }
        public decimal ActualFeeAmount { get; set; }
        public string DisbursementStatus { get; set; }
        public string PaymentPeriodName { get; set; }
        public DateTime PaymentPeriodStartDate { get; set; }
        public DateTime PaymentPeriodEndDate { get; set; }
        public decimal OriginationFeePercentageUsed { get; set; }
        public bool Deleted { get; set; }
        public bool Processed { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
