﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Newtonsoft.Json;

    public class AdvStagingEnrollmentV2 : EntityBase, IActionResultStatus
    {
        public DateTime? LeaveOfAbsenceDate { get; set; }
        public DateTime? DroppedDate { get; set; }
        public DateTime? LastDateAttendance { get; set; }
        public DateTime? StatusEffectiveDate { get; set; }
        public string AdmissionsCriteria { get; set; }
        public string EnrollmentStatus { get; set; }
        public string SSN { get; set; }
        public DateTime GradDate { get; set; }
        public DateTime StartDate { get; set; }
        public long IDStudent { get; set; }
        public Guid ProgramVersionID { get; set; }
        public string ProgramName { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public string LocationCMSID { get; set; }
        public long AdvStagingEnrollmentID { get; set; }
        public DateTime? ReturnFromLOADate { get; set; }
        public bool Processed { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
