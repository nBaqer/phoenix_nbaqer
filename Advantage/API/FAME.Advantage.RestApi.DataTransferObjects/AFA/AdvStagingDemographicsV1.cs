﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvStagingDemographicsV1.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the AdvStagingDemographicsV1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using System;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;

    /// <summary>
    /// The Advantage staging demographics v 1.
    /// </summary>
    public class AdvStagingDemographicsV1 : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        [SwaggerExclude]
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        [SwaggerExclude]
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the marital status.
        /// </summary>
        public string MaritialStatus { get; set; }

        /// <summary>
        /// Gets or sets the citizenship status.
        /// </summary>
        public string CitizenshipStatus { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the license state.
        /// </summary>
        public string LicenseState { get; set; }

        /// <summary>
        /// Gets or sets the license number.
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the dob.
        /// </summary>
        public DateTime DOB { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the middle initial.
        /// </summary>
        public string MiddleInitial { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Social security Number.
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// Gets or sets the id student.
        /// </summary>
        public long IDStudent { get; set; }

        /// <summary>
        /// Gets or sets the location Campus Management software ID.
        /// </summary>
        public string LocationCMSID { get; set; }

        /// <summary>
        /// Gets or sets the advantage staging demographic id.
        /// </summary>
        public long AdvStagingDemographicID { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether processed.
        /// </summary>
        public bool Processed { get; set; }
    }
}
