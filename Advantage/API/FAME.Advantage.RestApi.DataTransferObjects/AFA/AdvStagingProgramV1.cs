﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Newtonsoft.Json;

    public class AdvStagingProgramV1 : IActionResultStatus
    {
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
       public Guid ProgramVersionID { get; set; }
        public string ProgramName { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public string UserUpdated { get; set; }
        public string LocationCMSID { get; set; }
    }
}
