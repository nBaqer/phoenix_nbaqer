﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaLeadSyncException.cs" company="FAME Inc">
//   2018
// </copyright>
// <summary>
//   Defines the AfaLeadSyncException type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using System;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The AFA lead sync exception.
    /// </summary>
    public class AFALeadSyncException : IActionResultStatus
    {
        /// <summary>
        /// Gets or sets the result status message.
        /// </summary>
        public string ResultStatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the result status.
        /// </summary>
        public Enums.ResultStatus ResultStatus { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        public Guid? LeadId { get; set; }

        /// <summary>
        /// Gets or sets the AFA student id.
        /// </summary>
        public long AfaStudentId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the exception reason.
        /// </summary>
        public string ExceptionReason { get; set; }
    }
}
