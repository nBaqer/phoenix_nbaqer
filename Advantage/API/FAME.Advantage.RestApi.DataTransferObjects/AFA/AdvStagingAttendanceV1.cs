﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.DataTransferObjects.Common;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using Newtonsoft.Json;

    public class AdvStagingAttendanceV1 : EntityBase, IActionResultStatus
    {
        public long AdvStagingAttendanceID { get; set; }
        public string LocationCMSID { get; set; }
        public long IDStudent { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public string SSN { get; set; }
        public string PaymentPeriodName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal HoursCreditEarned { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string SAP { get; set; }
        public bool Processed { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
