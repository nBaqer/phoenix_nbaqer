﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.DataTransferObjects.Common;

namespace FAME.Advantage.RestApi.DataTransferObjects.AFA
{
    using Newtonsoft.Json;

    public class AdvStagingRefundV1 : EntityBase, IActionResultStatus
    {
        public long IDAdvStagingRefund { get; set; }
        public string LocationCMSID { get; set; }
        public Guid SISEnrollmentID { get; set; }
        public Guid FundSourceId { get; set; }
        public int DisbursementSequenceNumber { get; set; }
        public string AwardID { get; set; }
        public string SSN { get; set; }
        public DateTime RefundDate { get; set; }
        public decimal RefundAmount { get; set; }
        [JsonIgnore]
        public string ResultStatusMessage { get; set; }
        [JsonIgnore]
        public Enums.ResultStatus ResultStatus { get; set; }
    }
}
