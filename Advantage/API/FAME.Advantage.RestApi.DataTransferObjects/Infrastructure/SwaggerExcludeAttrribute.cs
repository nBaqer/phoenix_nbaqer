﻿

namespace FAME.Advantage.RestApi.DataTransferObjects.Infrastructure
{
    using System;

    /// <summary>
    /// The swagger exclude attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public class SwaggerExcludeAttribute : Attribute
    {
    }
}
