﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportConnection.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ReportConnection interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports
{
    /// <summary>
    /// The ReportConnection interface.
    /// </summary>
    public interface IReportConnection
    {
        /// <summary>
        /// Gets the api url.
        /// </summary>
        string ApiUrl { get; }

        /// <summary>
        /// Gets the token.
        /// </summary>
        string Token { get; }

        /// <summary>
        /// Gets or sets the sql server.
        /// </summary>
        string SqlServer { get; set; }

        /// <summary>
        /// Gets or sets the sql password.
        /// </summary>
        string SqlPassword { get; set; }

        /// <summary>
        /// Gets or sets the sql user name.
        /// </summary>
        string SqlUserName { get; set; }

        /// <summary>
        /// Gets or sets the sql database name.
        /// </summary>
        string SqlDatabaseName { get; set; }

        /// <summary>
        /// The initialize report connection.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="sqlServer">
        /// The sql server.
        /// </param>
        /// <param name="sqlUserName">
        /// The sql user name.
        /// </param>
        /// <param name="sqlPassword">
        /// The sql password.
        /// </param>
        /// <param name="sqlDatabaseName">
        /// The sql database name.
        /// </param>
        void InitializeReportConnection(string apiUrl, string token, string sqlServer,string sqlUserName, string sqlPassword,  string sqlDatabaseName);
    }
}
