﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HoursAccrued.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Hours Accrued.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using FAME.Extensions;

    /// <summary>
    /// The hours accrued.
    /// </summary>
    public class HoursAccrued
    {
        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public Guid? StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the hours.
        /// </summary>
        public decimal? Hours { get; set; }

        /// <summary>
        /// Gets or sets the lda.
        /// </summary>
        public DateTime LDA { get; set; }
    }
}
