﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IndianaStateBoardReportResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the indiana state board report.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using FAME.Extensions;

    /// <summary>
    /// The indiana state board report result.
    /// </summary>
    public class IndianaStateBoardReportResult
    {
        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public Guid ProgramVersionId { get; set; }

        /// <summary>
        /// Gets or sets the date of change.
        /// </summary>
        public DateTime? DateOfChange { get; set; }

        /// <summary>
        /// Gets or sets the from status.
        /// </summary>
        public string FromStatus { get; set; }

        /// <summary>
        /// Gets or sets the from sys status code.
        /// </summary>
        public int FromSysStatusCode { get; set; }

        /// <summary>
        /// Gets or sets the to status.
        /// </summary>
        public string ToStatus { get; set; }

        /// <summary>
        /// Gets or sets the to sys status code.
        /// </summary>
        public int ToSysStatusCode { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether is in school status.
        /// </summary>
        public bool IsInSchoolStatus { get; set; }

        /// <summary>
        /// Gets or sets the enrollment start.
        /// </summary>
        public DateTime? EnrollmentStart { get; set; }

        /// <summary>
        /// Gets or sets the badge studentumber.
        /// </summary>
        public string BadgeStudentumber { get; set; }

        /// <summary>
        /// Gets or sets the report start.
        /// </summary>
        public DateTime ReportStart { get; set; }

        /// <summary>
        /// Gets or sets the report end.
        /// </summary>
        public DateTime ReportEnd { get; set; }

        /// <summary>
        /// Gets or sets the enrollment id.
        /// </summary>
        public string EnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the course code.
        /// </summary>
        public string CourseCode { get; set; }

        /// <summary>
        /// Gets or sets the tuition owed.
        /// </summary>
        public string TuitionOwed { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the program version code.
        /// </summary>
        public string ProgramVersionCode { get; set; }

        /// <summary>
        /// Gets or sets the lda.
        /// </summary>
        public string LDA { get; set; }

        /// <summary>
        /// Gets or sets the hours accrued.
        /// </summary>
        public decimal? HoursAccrued { get; set; }

        /// <summary>
        /// Gets or sets the student information.
        /// </summary>
        public string StudentInformation
        {
             get
             {
                 
                return " #" + BadgeStudentumber.Trim() + " " + LastName + ", " + FirstName
                       + " " + MiddleName;
            }
            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the enrollment start formatted.
        /// </summary>
        public string EnrollmentStartFormatted
        {
            get
            {
                return EnrollmentStart?.ToString("MM/dd/yyyy");
            }
            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the lda formatted.
        /// </summary>
        public string LDAFormatted { get; set; }
       
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code
        {
            get
            {
                if (ToSysStatusCode == 12)
                {
                    return "DO";
                }
                if (ToSysStatusCode == 14)
                {
                    return "G";
                }
                if (EnrollmentStart >= ReportStart && EnrollmentStart <= ReportEnd)
                {
                    return "N";
                }
               
                return "";
            }
            set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether has status change.
        /// </summary>
        public bool HasStatusChange
        {
            get
            {
                return ToStatus != FromStatus;
            }
        }
    }
}
