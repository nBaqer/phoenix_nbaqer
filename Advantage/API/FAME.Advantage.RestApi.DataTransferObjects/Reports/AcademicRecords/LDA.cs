﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LAD.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines Last Day of Attendance.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using FAME.Extensions;

    /// <summary>
    /// The Last Day of Attendance.
    /// </summary>
    public class LDA
    {
        /// <summary>
        /// Gets or sets the stu enrollment id.
        /// </summary>
        public Guid StuEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the lda date.
        /// </summary>
        public string LDADate { get; set; }
    }
}
