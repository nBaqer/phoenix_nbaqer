﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tuition.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Tuition.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using FAME.Extensions;

    /// <summary>
    /// The tuition.
    /// </summary>
    public class Tuition
    {
        /// <summary>
        /// Gets or sets the stu enrollment id.
        /// </summary>
        public Guid StuEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether owe tuition.
        /// </summary>
        public bool OweTuition
        {
            get
            {
                return Amount > 0;
            }
            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the owe tuition description.
        /// </summary>
        public string OweTuitionDescription
        {
            get
            {
                return OweTuition ? "Y" : "N";
            }
            set
            {
            }
        }
    }
}
