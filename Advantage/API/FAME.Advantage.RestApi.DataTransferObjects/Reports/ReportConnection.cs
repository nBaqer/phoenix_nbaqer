﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportConnection.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ReportConnection type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports
{
    /// <summary>
    /// The report connection.
    /// </summary>
    public class ReportConnection : IReportConnection
    {
        /// <summary>
        /// Gets the api url.
        /// </summary>
        public string ApiUrl { get; private set; }

        /// <summary>
        /// Gets the token.
        /// </summary>
        public string Token { get; private set; }

        /// <summary>
        /// Gets or sets the sql server.
        /// </summary>
        public string SqlServer { get; set; }

        /// <summary>
        /// Gets or sets the sql password.
        /// </summary>
        public string SqlPassword { get; set; }

        /// <summary>
        /// Gets or sets the sql user name.
        /// </summary>
        public string SqlUserName { get; set; }

        /// <summary>
        /// Gets or sets the sql database name.
        /// </summary>
        public string SqlDatabaseName { get; set; }

        /// <summary>
        /// The initialize report connection.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="sqlServer">
        /// The sql server.
        /// </param>
        /// <param name="sqlUserName">
        /// The sql user name.
        /// </param>
        /// <param name="sqlPassword">
        /// The sql password.
        /// </param>
        /// <param name="sqlDatabaseName">
        /// The sql database name.
        /// </param>
        public void InitializeReportConnection(
            string apiUrl,
            string token,
            string sqlServer,
            string sqlUserName,
            string sqlPassword,
            string sqlDatabaseName)
        {
            this.ApiUrl = apiUrl;
            this.Token = token;
            this.SqlServer = sqlServer;
            this.SqlPassword = sqlPassword;
            this.SqlUserName = sqlUserName;
            this.SqlDatabaseName = sqlDatabaseName;
        }
    }
}
