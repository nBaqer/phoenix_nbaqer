﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IReportRequest interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The report data source type.
    /// </summary>
    public enum ReportDataSourceType
    {
        /// <summary>
        /// The api.
        /// Use this when the report uses the rest API as Data Source
        /// </summary>
        RestApi = 1,

        /// <summary>
        /// The sql server.
        /// Use this option when the report uses SQL Server as Data Source.
        /// </summary>
        SqlServer = 2
    }


    /// <summary>
    /// The report output.
    /// </summary>
    public enum ReportOutput
    {
        /// <summary>
        /// The pdf.
        /// </summary>
        Pdf = 1
    }

    /// <summary>
    /// The Report Request interface. 
    /// Implement the Report Input Parameter on models that you will use to pass parameters into sql sever reports.
    /// </summary>
    public interface IReportRequest
    {
        /// <summary>
        /// Gets or sets the report location.
        /// Example: FolderName/ReportName
        /// </summary>
        string ReportLocation { get; set; }

        /// <summary>
        /// Gets or sets the report parameters.
        /// </summary>
        Dictionary<string, object> Parameters { get; set; }

        /// <summary>
        /// Gets or sets the report data source type.
        /// </summary>
        ReportDataSourceType ReportDataSourceType { get; set; }

        /// <summary>
        /// Gets or sets the report output.
        /// </summary>
        ReportOutput ReportOutput { get; set; }
        /// <summary>
        /// Gets or sets the time out.
        /// </summary>
        TimeSpan? Timeout { get; set; }
    }
}
