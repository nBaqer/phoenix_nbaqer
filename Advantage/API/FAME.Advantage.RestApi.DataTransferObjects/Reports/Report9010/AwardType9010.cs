﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AwardType9010.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AwardType9010 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.AwardType9010
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The AwardType9010 dto class.
    /// </summary>
    public class MappingType9010
    {
        /// <summary>
        /// Gets or sets the 9010 award type id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the award type description.
        /// </summary>
        public string Description { get; set; } = null;

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
