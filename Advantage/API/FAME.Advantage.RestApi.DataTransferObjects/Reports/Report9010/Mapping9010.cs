﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Mapping9010.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Mapping9010 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.Report9010
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The Mapping9010 dto class.
    /// </summary>
    public class Mapping9010
    {
        /// <summary>
        /// Gets or sets the 9010 entity Type.
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets the entity id.
        /// </summary>
        public Guid EntityId { get; set; }

        /// <summary>
        /// Gets or sets the entity description.
        /// </summary>
        public string EntityDescription { get; set; }

        /// <summary>
        /// Gets or sets entity code.
        /// </summary>
        public string EntityCode { get; set; }

        /// <summary>
        /// Gets or sets the award type description.
        /// </summary>
        public string AwardTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets fund source is active flag.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the 9010 mapping id.
        /// </summary>
        public Guid? MappingType9010Id { get; set; }

        /// <summary>
        /// Gets or sets the 9010 mapping description.
        /// </summary>
        public string MappingType9010Description { get; set; }

        /// <summary>
        /// Gets or sets the entity type enum value
        /// </summary>
        public EntityTypes9010 EntityTypeEnum { get; set; }

        /// <summary>
        /// Gets or sets the entity type is title iv
        /// </summary>
        public bool? IsTitleIV { get; set; }

        /// <summary>
        /// Gets or sets has tied mapping.
        /// </summary>
        public bool EntityHas9010Mapping { get { return this.MappingType9010Id.HasValue && this.MappingType9010Id != Guid.Empty; } }
    }

    /// <summary>
    /// The Mapping9010 dto class.
    /// </summary>
    public enum EntityTypes9010
    {

        /// <summary>
        /// Fund source enum.
        /// </summary>
        FundSource = 0,

        /// <summary>
        /// TransactionType enum.
        /// </summary>
        TransactionType = 1
    }
}
