﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataExport9010Param.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DataExport9010Param type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.Report9010
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The data export param.
    /// </summary>
    public class DataExport9010Param
    {
        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid? StuEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the list of student group ids.
        /// </summary>
        public List<string> GroupIds { get; set; } = null;

        /// <summary>
        /// Gets or sets the list of student program ids.
        /// </summary>
        public List<string> ProgramIds { get; set; } = null;

        /// <summary>
        /// Gets or sets the campus ids.
        /// </summary>
        public List<string> CampusIds { get; set; }

        /// <summary>
        /// Gets or sets fiscal year start.
        /// </summary>
        public DateTime FiscalYearStart { get; set; }

        /// <summary>
        /// Gets or sets fiscal year end.
        /// </summary>
        public DateTime FiscalYearEnd { get; set; }

        /// <summary>
        /// Gets or sets fiscal year end.
        /// </summary>
        public decimal ActivitiesConductedAmount { get; set; }
    }
}
