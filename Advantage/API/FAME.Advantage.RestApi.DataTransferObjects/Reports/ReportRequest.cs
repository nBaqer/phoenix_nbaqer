﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Report Request type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.DataTransferObjects.Reports
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The report request.
    /// </summary>
    public class ReportRequest : IReportRequest
    {
        /// <summary>
        /// Gets or sets the report location.
        /// Example: FolderName/ReportName
        /// </summary>
        [Required]
        public string ReportLocation { get; set; }

        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        public Dictionary<string, object> Parameters { get; set; }

        /// <summary>
        /// Gets or sets the report data source type.
        /// Expected Value:
        ///  RestApi = 1,
        ///  SqlServer = 2
        /// </summary>
        [Required]
        public ReportDataSourceType ReportDataSourceType { get; set; }

        /// <summary>
        /// Gets or sets the report output.
        /// Expected Value:
        ///  Pdf = 1
        /// </summary>
        [Required]
        public ReportOutput ReportOutput { get; set; }

        /// <summary>
        /// Gets or sets the time out.
        /// </summary>
        public TimeSpan? Timeout { get; set; }
    }
}
