﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressReportParam.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgressReportParam type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.Progress
{
    using System;

    /// <summary>
    /// The progress report param.
    /// </summary>
    public class ProgressReportParam
    {
        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the campus groups id.
        /// </summary>
        public string CampusGroupsId { get; set; } = null;

        /// <summary>
        /// Gets or sets the program version id.
        /// </summary>
        public string ProgramVersionId { get; set; } = null;

        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public string StatusCodeId { get; set; } = null;

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public string TermId { get; set; } = null;

        /// <summary>
        /// Gets or sets the student group id.
        /// </summary>
        public string StudentGroupId { get; set; } = null;

        /// <summary>
        /// Gets or sets the grade format.
        /// </summary>
        public string GradeFormat { get; set; }

        /// <summary>
        /// Gets or sets the gpa method.
        /// </summary>
        public string GPAMethod { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show work unit grouping.
        /// </summary>
        public bool ShowWorkUnitGrouping { get; set; }

        /// <summary>
        /// Gets or sets the sys component type id.
        /// </summary>
        public string SysComponentTypeId { get; set; }

        /// <summary>
        /// Gets or sets the order by.
        /// </summary>
        public string OrderBy { get; set; } = "LastName Asc,FirstName Asc,MiddleName Asc";

        /// <summary>
        /// Gets or sets a value indicating whether show finance calculations.
        /// </summary>
        public bool ShowFinanceCalculations { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show weekly schedule.
        /// </summary>
        public bool ShowWeeklySchedule { get; set; }

        /// <summary>
        /// Gets or sets the grade rounding.
        /// </summary>
        public bool GradeRounding { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show student signature line.
        /// </summary>
        public bool ShowStudentSignatureLine { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether show school signature line.
        /// </summary>
        public bool ShowSchoolSignatureLine { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether show page number.
        /// </summary>
        public bool ShowPageNumber { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether show heading.
        /// </summary>
        public bool ShowHeading { get; set; } = true;

        /// <summary>
        /// Gets or sets the start date modifier.
        /// </summary>
        public string StartDateModifier { get; set; } = "<=";

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the expected grad date modifier.
        /// </summary>
        public string ExpectedGradDateModifier { get; set; } = "<=";

        /// <summary>
        /// Gets or sets the expected grad date.
        /// </summary>
        public string ExpectedGradDate { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        public string SchoolName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the track attendance by.
        /// </summary>
        public string TrackAttendanceBy { get; set; }

        /// <summary>
        /// Gets or sets the set grade book at.
        /// </summary>
        public string SetGradeBookAt { get; set; }

        /// <summary>
        /// Gets or sets the display hours.
        /// </summary>
        public string DisplayHours { get; set; } = "presentabsent";

        /// <summary>
        /// Gets or sets a value indicating whether show term module.
        /// </summary>
        public bool ShowTermModule { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show date in footer.
        /// </summary>
        public bool ShowDateInFooter { get; set; } = false;

        /// <summary>
        /// Gets or sets the student identifier.
        /// </summary>
        public string StudentIdentifier { get; set; } = "ssn";

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show all enrollments.
        /// </summary>
        public bool ShowAllEnrollments { get; set; } = false;
    }
}
