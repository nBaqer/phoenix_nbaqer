﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReasonMapping.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Drop Reason Mapping
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.Naccas
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The naccas drop reason mapping.
    /// </summary>
    public class DropReasonMapping
    {
        /// <summary>
        /// Gets or sets the naccas reason id.
        /// </summary>
        public Guid NaccasReasonId { get; set; }

        /// <summary>
        /// Gets or sets the advantage reasons.
        /// </summary>
        public List<Guid> AdvantageReasons { get; set; }
    }
}
