﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NaccasSettings.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the NaccasSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.Naccas
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The naccas settings.
    /// </summary>
    public class NaccasSettings
    {
        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow graduate and owe money.
        /// </summary>
        public bool AllowGraduateAndOweMoney { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether allow graduate without full completion.
        /// </summary>
        public bool AllowGraduateWithoutFullCompletion { get; set; }

        /// <summary>
        /// Gets or sets the approved program version.
        /// </summary>
        public List<Guid> ApprovedProgramVersions { get; set; }

        /// <summary>
        /// Gets or sets the drop reason mappings.
        /// </summary>
        public List<DropReasonMapping> DropReasonMappings { get; set; }

        /// <summary>
        /// Gets or sets the ModeBy id.
        /// </summary>
        public Guid ModUserId { get; set; }

        /// <summary>
        /// Gets or sets the ModeBy id.
        /// </summary>
       public DateTime  ModDate { get; set; }

    }
}
