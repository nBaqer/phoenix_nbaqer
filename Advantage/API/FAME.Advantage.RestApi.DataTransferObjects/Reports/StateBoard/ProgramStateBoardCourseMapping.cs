﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardReportSetting.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramStateBoardCourseMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    /// <summary>
    /// The program state board course mapping class.
    /// </summary>
    public class ProgramStateBoardCourseMapping
    {
        /// <summary>
        /// Gets or sets the program id.
        /// </summary>
        public Guid ProgramId { get; set; }

        /// <summary>
        /// Gets or sets the state course code id.
        /// </summary>
        public int StateBoardCourseId { get; set; }


    }
}
