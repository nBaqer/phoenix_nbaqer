﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardAgency.cs" company="Fame Inc.">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The state board agency.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The state board agency.
    /// </summary>
    public class StateBoardAgency
    {
        /// <summary>
        /// Gets or sets the licensing address 1.
        /// </summary>
        [Required]
        public string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the licensing address 2.
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the licensing country id.
        /// </summary>
        [Required]
        public Guid CountryId { get; set; }

        /// <summary>
        /// Gets or sets the licensing city.
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the licensing state id.
        /// </summary>
        [Required]
        public Guid StateId { get; set; }

        /// <summary>
        /// Gets or sets the licensing zip code.
        /// </summary>
        [Required]
        public string ZipCode { get; set; }
    }
}
