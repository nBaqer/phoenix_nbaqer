﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardReportSetting.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardReportSetting type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FAME.Advantage.RestApi.DataTransferObjects.Validation;
using FAME.Advantage.RestApi.DataTransferObjects.Validation.Enums;

namespace FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard
{
    using System;

    /// <summary>
    /// The state board report setting.
    /// </summary>
    public class StateBoardReportSetting
    {
        /// <summary>
        /// Gets or sets the lead id.
        /// </summary>
        [RequestBasedRequired(ValidationRequestType.Put)]
        public Guid SchoolStateBoardReportId { get; set; }

        /// <summary>
        /// Gets or sets the state id.
        /// </summary>
        [Required]
        public Guid StateId { get; set; }

        /// <summary>
        /// Gets or sets the campus id.
        /// </summary>
        [Required]
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the report id.
        /// </summary>
        [Required]
        public Guid ReportId { get; set; }
        /// <summary>
        /// Gets or sets the owner name.
        /// </summary>
        [Required]
        public string OwnerName { get; set; }
        /// <summary>
        /// Gets or sets the school name.
        /// </summary>
        [Required]
        public string SchoolName { get; set; }
        /// <summary>
        /// Gets or sets the school address 1.
        /// </summary>
        [Required]
        public string SchoolAddress1 { get; set; }
        /// <summary>
        /// Gets or sets the school address 2.
        /// </summary>
        public string SchoolAddress2 { get; set; }
        /// <summary>
        /// Gets or sets the campus city.
        /// </summary>
        [Required]
        public string CampusCity { get; set; }
        /// <summary>
        /// Gets or sets the campus country id.
        /// </summary>
        [Required]
        public Guid CampusCountryId { get; set; }
        /// <summary>
        /// Gets or sets the campus state id.
        /// </summary>
        [Required]
        public Guid CampusStateId { get; set; }

        /// <summary>
        /// Gets or sets the Campus state zip code.
        /// </summary>
        public string CampusState { get; set; }

        /// <summary>
        /// Gets or sets the campus zip code.
        /// </summary>
        [Required]
        public string CampusZipCode { get; set; }
        /// <summary>
        /// Gets or sets the owner license number.
        /// </summary>
        [Required]
        public string OwnerLicenseNumber { get; set; }
        /// <summary>
        /// Gets or sets the officer name 1.
        /// </summary>
        public string OfficerName1 { get; set; }
        /// <summary>
        /// Gets or sets the officer name 2.
        /// </summary>
        public string OfficerName2 { get; set; }
        /// <summary>
        /// Gets or sets the officer name 3.
        /// </summary>
        public string OfficerName3 { get; set; }
        /// <summary>
        /// Gets or sets the licensing agency id.
        /// </summary>
        [Required]
        public int LicensingAgencyId { get; set; }
        /// Gets or sets the licensing agency name.
        /// </summary>
        public string LicensingAgencyName { get; set; }
        /// <summary>

        /// <summary>
        /// Gets or sets the licensing address 1.
        /// </summary>
        [Required]
        public string LicensingAddress1 { get; set; }
        /// <summary>
        /// Gets or sets the licensing address 2.
        /// </summary>
        public string LicensingAddress2 { get; set; }
        /// <summary>
        /// Gets or sets the licensing country id.
        /// </summary>
        [Required]
        public Guid LicensingCountryId { get; set; }
        /// Gets or sets the licensing country name.
        /// </summary>
        public string LicensingCountry { get; set; }
        /// <summary>
        /// <summary>
        /// Gets or sets the licensing city.
        /// </summary>
        [Required]
        public string LicensingCity { get; set; }
        /// <summary>
        /// Gets or sets the licensing state id.
        /// </summary>
        [Required]
        public Guid LicensingStateId { get; set; }
        /// <summary>
        /// Gets or sets the licensing state name.
        /// </summary>
        public string LicensingState { get; set; }
        /// <summary>
        /// Gets or sets the licensing zip code.
        /// </summary>
        [Required]
        public string LicensingZipCode { get; set; }
        /// <summary>
        /// Gets or sets the program/state board course mappings.
        /// </summary>
        public List<ProgramStateBoardCourseMapping> ProgramStateBoardCourseMappings { get; set; }
    }
}
