﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationPeriodType.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the CalculationPeriod Type type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities
{
    using System;
    using Fame.Orm;

    /// <summary>
    /// The calculation period type .
    /// </summary>
    public class CalculationPeriodType : IEntity
    {
        /// <summary>
        /// Gets or sets the calculation period type's id. This is of type Guid.
        /// </summary>
        public Guid CalculationPeriodTypeId { get; set; }

        /// <summary>
        /// Gets or sets the period type's description. This is of type string.
        /// </summary>
        public string Description { get; set; }
    }
}
