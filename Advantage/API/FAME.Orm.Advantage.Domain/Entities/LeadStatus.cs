﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class LeadStatus : IEntity
    {
        public Guid StatusCodeId { get; set; }
        public string StatusCode { get; set; }
        public string StatusCodeDescrip { get; set; }
    }
}
