﻿namespace FAME.Orm.Advantage.Domain.Entities
{
    public class UserRights
    {
        public ModuleRights[] Modules { get; set; }
    }
}
