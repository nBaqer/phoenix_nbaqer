﻿namespace FAME.Orm.Advantage.Domain.Entities
{
    public class ModuleRights
    {
       public string Name { get; set; }
       public string Level { get; set; }
    }
}
