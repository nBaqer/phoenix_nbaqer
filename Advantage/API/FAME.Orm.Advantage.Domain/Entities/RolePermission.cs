﻿using Fame.EFCore.Advantage.Entities;
using Fame.Orm;
using Newtonsoft.Json;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class RolePermission
    {
        public RolePermission(SySysRoles systemRoles)
        {
            this.RoleId = systemRoles.SysRoleId;
            this.Permission = systemRoles.Permission;
        }

        public int RoleId { get; set; }

        public string Permission { get; set; }

        public UserRights GetUserRights()
        {
            return JsonConvert.DeserializeObject<UserRights>(this.Permission);
        }
    }
}
