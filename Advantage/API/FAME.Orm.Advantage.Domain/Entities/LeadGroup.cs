﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class LeadGroup : IEntity
    {
        public Guid LeadGrpId { get; set; }
        public string Descrip { get; set; }
    }
}
