﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class County: IEntity
    {
        public Guid CountyId { get; set; }
        public string CountyCode { get; set; }
        public string CountyDescrip { get; set; }
    }
}
