﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class Country: IEntity
    {
        public Guid CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryDescrip { get; set; }
    }
}
