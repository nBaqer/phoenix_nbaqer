﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatusCode.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Status type. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities
{
    using System;
    using Fame.Orm;

    /// <summary>
    /// Status list
    /// </summary>
    public class DropStatusCode : IEntity
    {
        /// <summary>
        /// Gets or sets the status description.
        /// </summary>
        public string StatusCodeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        public Guid StatusCodeId { get; set; }
       
    }
}
