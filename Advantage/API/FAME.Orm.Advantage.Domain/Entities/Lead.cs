﻿using System;
using Fame.Orm;


// ReSharper disable InconsistentNaming
// The names are the actual AdLeads column names

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class Lead : IEntity
    {
        public Lead()
        {
            NickName = string.Empty;
            PreferredContactId = 1;
            AddressApt = string.Empty;
            CreatedDate = DateTime.Now;
            StudentId = Guid.NewGuid();
        }

        public Guid LeadId { get; set; }

        public string ProspectID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string SSN { get; set; }
        public string ModUser { get; set; }
        public DateTime? ModDate { get; set; }
        public string Phone { get; set; }
        public string HomeEmail { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public Guid? StateId { get; set; }
        public string Zip { get; set; }
        public Guid LeadStatus { get; set; }
        public string WorkEmail { get; set; }
        public Guid? AddressType { get; set; }
        public Guid? Prefix { get; set; }
        public Guid? Suffix { get; set; }
        public DateTime? BirthDate { get; set; }
        public Guid? Sponsor { get; set; }
        public Guid? AdmissionsRep { get; set; }
        public DateTime? AssignedDate { get; set; }
        public Guid? Gender { get; set; }
        public Guid? Race { get; set; }
        public Guid? MaritalStatus { get; set; }
        public Guid? FamilyIncome { get; set; }
        public string Children { get; set; }
        public Guid? PhoneType { get; set; }
        public Guid? PhoneStatus { get; set; }
        public Guid? SourceCategoryID { get; set; }
        public Guid? SourceTypeID { get; set; }
        public DateTime? SourceDate { get; set; }
        public Guid? AreaID { get; set; }
        public Guid? ProgramID { get; set; }
        public DateTime? ExpectedStart { get; set; }
        public Guid? ShiftID { get; set; }
        public Guid? Nationality { get; set; }
        public Guid? Citizen { get; set; }
        public Guid? DrivLicStateID { get; set; }
        public string DrivLicNumber { get; set; }
        public string AlienNumber { get; set; }
        public string Comments { get; set; }
        public Guid? SourceAdvertisement { get; set; }
        public Guid CampusId { get; set; }
        public Guid? PrgVerId { get; set; }
        public Guid? Country { get; set; }
        public Guid? County { get; set; }
        public string Age { get; set; }
        public Guid? PreviousEducation { get; set; }
        public Guid? AddressStatus { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string RecruitmentOffice { get; set; }
        public string OtherState { get; set; }
        public bool ForeignPhone { get; set; }
        public bool ForeignZip { get; set; }
        public Guid? LeadgrpId { get; set; }
        public Guid? DependencyTypeId { get; set; }
        public Guid? DegCertSeekingId { get; set; }
        public Guid? GeographicTypeId { get; set; }
        public Guid? HousingId { get; set; }
        public Guid? admincriteriaid { get; set; }
        public DateTime? DateApplied { get; set; }
        public string InquiryTime { get; set; }
        public string AdvertisementNote { get; set; }
        public string Phone2 { get; set; }
        public Guid? PhoneType2 { get; set; }
        public Guid? PhoneStatus2 { get; set; }
        public bool? ForeignPhone2 { get; set; }
        public int? DefaultPhone { get; set; }
        public bool? IsDisabled { get; set; }
        public bool IsFirstTimeInSchool { get; set; }
        public bool IsFirstTimePostSecSchool { get; set; }
        public DateTime? EntranceInterviewDate { get; set; }
        public Guid? ProgramScheduleId { get; set; }
        public string BestTime { get; set; }
        public int? DistanceToSchool { get; set; }
        public int? CampaignId { get; set; }
        public string ProgramOfInterest { get; set; }
        public string CampusOfInterest { get; set; }
        public Guid? TransportationId { get; set; }
        public string NickName { get; set; }
        public Guid? AttendTypeId { get; set; }
        public int? PreferredContactId { get; set; }
        public string AddressApt { get; set; }
        public bool NoneEmail { get; set; }
        public Guid? HighSchoolId { get; set; }
        public DateTime? HighSchoolGradDate { get; set; }
        public bool? AttendingHs { get; set; }
        public Guid? StudentId { get; set; }
        public string StudentNumber { get; set; }
        public Guid? StudentStatusId { get; set; }
        public Guid? ReasonNotEnrolledId { get; set; }
        public Guid? EnrollStateId { get; set; }
    }
}