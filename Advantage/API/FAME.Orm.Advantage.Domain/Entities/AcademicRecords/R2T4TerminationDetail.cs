﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4TerminationDetail.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The r2t4 termination details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Orm.Advantage.Domain.Entities.AcademicRecords
{
    using System;

    using Fame.Orm;

    /// <summary>
    /// The r2t4 termination details.
    /// </summary>
    public class R2T4TerminationDetail : IEntity
    {
        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets the stu enrollment id.
        /// </summary>
        public Guid StuEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the status code id.
        /// </summary>
        public Guid? StatusCodeId { get; set; }

        /// <summary>
        /// Gets or sets the drop reason id.
        /// </summary>
        public Guid? DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the date withdrawal determined.
        /// </summary>
        public DateTime? DateWithdrawalDetermined { get; set; }

        /// <summary>
        /// Gets or sets the last date attended.
        /// </summary>
        public DateTime? LastDateAttended { get; set; }

        /// <summary>
        /// Gets or sets the is performing R2T4 calculator.
        /// </summary>
        public bool? IsPerformingR2T4Calculator { get; set; }

        /// <summary>
        /// Gets or sets the calculation period type id.
        /// </summary>
        public Guid? CalculationPeriodTypeId { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the created by id.
        /// </summary>
        public Guid CreatedById { get; set; }

        /// <summary>
        /// Gets or sets the updated by id.
        /// </summary>
        public Guid? UpdatedById { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }
    }
}


