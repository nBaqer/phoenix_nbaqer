﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Student.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The Student Entity.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities.AcademicRecords
{
    using System;
    using Fame.Orm;

    /// <summary>
    /// The student entity
    /// </summary>
    public class Student : IEntity
    {
        /// <summary>
        /// Gets or sets the Student's id.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the student's display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the student's first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the student's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the student's middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the student's social security number.
        /// </summary>
        public string Ssn { get; set; }

        /// <summary>
        /// Gets or sets the student's Birth Date.
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// Gets or sets the student's Sponsor.
        /// </summary>
        public Guid Sponsor { get; set; }

        /// <summary>
        /// Gets or sets the student's Assigned Date.
        /// </summary>
        public string AssignedDate { get; set; }

        /// <summary>
        /// Gets or sets the student's Gender.
        /// </summary>
        public Guid Gender { get; set; }

        /// <summary>
        /// Gets or sets the student's Race.
        /// </summary>
        public Guid Race { get; set; }

        /// <summary>
        /// Gets or sets the student's Marital Status.
        /// </summary>
        public Guid MaritalStatus { get; set; }

        /// <summary>
        /// Gets or sets the student's Family Income.
        /// </summary>
        public Guid FamilyIncome { get; set; }

        /// <summary>
        /// Gets or sets the student's Expected Start Date.
        /// </summary>
        public string ExpectedStart { get; set; }

        /// <summary>
        /// Gets or sets the student's Shift Identification.
        /// </summary>
        public Guid ShiftId { get; set; }

        /// <summary>
        /// Gets or sets the student's Nationality.
        /// </summary>
        public Guid Nationality { get; set; }

        /// <summary>
        /// Gets or sets the student's Citizen.
        /// </summary>
        public Guid Citizen { get; set; }

        /// <summary>
        /// Gets or sets the student's Drivers License State Identification.
        /// </summary>
        public Guid DrivLicStateId { get; set; }

        /// <summary>
        /// Gets or sets the student's Driving License Number.
        /// </summary>
        public string DrivLicNumber { get; set; }

        /// <summary>
        /// Gets or sets the student's Source Date.
        /// </summary>
        public string SourceDate { get; set; }

        /// <summary>
        /// Gets or sets the student's Dependency Type Id.
        /// </summary>
        public Guid DependencyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the student's Student Number.
        /// </summary>
        public string StudentNumber { get; set; }

        /// <summary>
        /// Gets or sets the student's Degree Certificate Seeking Identification.
        /// </summary>
        public Guid DegCertSeekingId { get; set; }

        /// <summary>
        /// Gets or sets the student's Geographical Type Identification.
        /// </summary>
        public Guid GeographicTypeId { get; set; }

        /// <summary>
        /// Gets or sets the student's Housing Identification.
        /// </summary>
        public Guid HousingId { get; set; }

        /// <summary>
        /// Gets or sets the student's  administration criteria identification.
        /// </summary>
        public Guid AdminCriteriaId { get; set; }

        /// <summary>
        /// Gets or sets the student's Attendence Type Identification.
        /// </summary>
        public Guid AttendTypeId { get; set; }

        /// <summary>
        /// Gets or sets Student Status Id
        /// </summary>
        public Guid StudentStatusId { get; set; }

        /// <summary>
        /// Gets or sets the student's campus id.
        /// </summary>
        public Guid CampusId { get; set; }

        /// <summary>
        /// Gets or sets the student's Prefix.
        /// </summary>
        public Guid Prefix { get; set; }

        /// <summary>
        /// Gets or sets the student's Suffix.
        /// </summary>
        public Guid Suffix { get; set; }

        /// <summary>
        /// Gets or sets the student's record  Modified by User Id.
        /// </summary>
        public string ModUser { get; set; }

        /// <summary>
        /// Gets or sets the student's Record modification date.
        /// </summary>
        public string ModDate { get; set; }
    }
}
