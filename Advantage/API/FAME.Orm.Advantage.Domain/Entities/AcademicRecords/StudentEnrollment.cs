﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentEnrollment.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The enrollment.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities.AcademicRecords
{
    using System;
    using Fame.Orm;

    /// <summary>
    /// The student enrollment.
    /// </summary>
    public class StudentEnrollment : IEntity
    {
        /// <summary>
        /// Gets or sets the date determined. This is of type string.
        /// </summary>
        public string DateDetermined { get; set; }

        /// <summary>
        /// Gets or sets the date of change.
        /// </summary>
        public DateTime? DateOfChange { get; set; }

        /// <summary>
        /// Gets or sets the enrollment date. This is of type string.
        /// </summary>
        public DateTime EnrollDate { get; set; }

        /// <summary>
        /// Gets or sets the last date attended. This is of type string.
        /// </summary>
        public string LDA { get; set; }

        /// <summary>
        /// Gets or sets the program version description.
        /// </summary>
        public string PrgVerDescrip { get; set; }

        /// <summary>
        /// Gets or sets the start date. This is of type string.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the status code.
        /// </summary>
        public string StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the student id. This is of type Guid.
        /// </summary>
        public Guid StudentId { get; set; }

        /// <summary>
        /// Gets or sets the student enrollment id. This is of type Guid.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the status code description.
        /// </summary>
        public string StatusCodeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the unit type description.
        /// </summary>
        public string UnitTypeDescrip { get; set; }

        /// <summary>
        /// Gets or sets the ssn.
        /// </summary>
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the sys status id.
        /// </summary>
        public int SysStatusId { get; set; }
    }
}