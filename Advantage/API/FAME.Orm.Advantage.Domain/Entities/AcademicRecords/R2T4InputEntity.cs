﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4Input.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4Input type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities.AcademicRecords
{
    using System;

    using Fame.Orm;

    /// <summary>
    /// The R2T4 input detail is a entity model to represent R2T4Input record.
    /// </summary>
    public class R2T4InputEntity : IEntity
    {
        /// <summary>
        /// Gets or sets the R2T4 input id.
        /// </summary>
        public Guid R2T4InputId { get; set; }

        /// <summary>
        /// Gets or sets the termination id.
        /// </summary>
        public Guid TerminationId { get; set; }

        /// <summary>
        /// Gets or sets the board fee.
        /// </summary>
        public decimal? BoardFee { get; set; }

        /// <summary>
        /// Gets or sets the completed time.
        /// </summary>
        public int? CompletedTime { get; set; }

        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        public Guid CreatedById { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the credit balance refunded.
        /// </summary>
        public decimal? CreditBalanceRefunded { get; set; }

        /// <summary>
        /// Gets or sets the direct graduate plus loan could disbursed.
        /// </summary>
        public decimal? DirectGraduatePlusLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct graduate plus loan disbursed.
        /// </summary>
        public decimal? DirectGraduatePlusLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct parent plus loan could disbursed.
        /// </summary>
        public decimal? DirectParentPlusLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the direct parent plus loan disbursed.
        /// </summary>
        public decimal? DirectParentPlusLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the fseog could disbursed.
        /// </summary>
        public decimal? FseogCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the fseog disbursed.
        /// </summary>
        public decimal? FseogDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the iraq afg grant could disbursed.
        /// </summary>
        public decimal? IraqAfgGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the iraq afg grant disbursed.
        /// </summary>
        public decimal? IraqAfgGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance not required.
        /// </summary>
        public bool IsAttendanceNotRequired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is tuition charged by payment period.
        /// </summary>
        public bool IsTuitionChargedByPaymentPeriod { get; set; }

        /// <summary>
        /// Gets or sets the other fee.
        /// </summary>
        public decimal? OtherFee { get; set; }

        /// <summary>
        /// Gets or sets the pell grant could disbursed.
        /// </summary>
        public decimal? PellGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the pell grant disbursed.
        /// </summary>
        public decimal? PellGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the perkins loan could disbursed.
        /// </summary>
        public decimal? PerkinsLoanCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the perkins loan disbursed.
        /// </summary>
        public decimal? PerkinsLoanDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the program unit type id.
        /// </summary>
        public int? ProgramUnitTypeId { get; set; }

        /// <summary>
        /// Gets or sets the room fee.
        /// </summary>
        public decimal? RoomFee { get; set; }

        /// <summary>
        /// Gets or sets the scheduled end date.
        /// </summary>
        public DateTime? ScheduledEndDate { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the sub loan net amount could disbursed.
        /// </summary>
        public decimal? SubLoanNetAmountCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the sub loan net amount disbursed.
        /// </summary>
        public decimal? SubLoanNetAmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the teach grant could disbursed.
        /// </summary>
        public decimal? TeachGrantCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the teach grant disbursed.
        /// </summary>
        public decimal? TeachGrantDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the total time.
        /// </summary>
        public int? TotalTime { get; set; }

        /// <summary>
        /// Gets or sets the tuition fee.
        /// </summary>
        public decimal? TuitionFee { get; set; }

        /// <summary>
        /// Gets or sets the unsub loan net amount could disbursed.
        /// </summary>
        public decimal? UnsubLoanNetAmountCouldDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the unsub loan net amount disbursed.
        /// </summary>
        public decimal? UnsubLoanNetAmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        public Guid? UpdatedById { get; set; }

        /// <summary>
        /// Gets or sets the updated date.
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the withdrawal date.
        /// </summary>
        public DateTime? WithdrawalDate { get; set; }
    }
}
