﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class Gender : IEntity
    {
        public Guid GenderId { get; set; }
        public string GenderCode { get; set; }
        public string GenderDescrip { get; set; }
    }
}
