﻿using System;
using System.Collections.Generic;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class Campus : IEntity
    {
        public Guid CampusId { get; set; }
        public string CampCode { get; set; }
        public string CampDescrip { get; set; }
        public IEnumerable<AdmissionRep> AdmissionReps { get; set; }
        public IEnumerable<LeadGroup> LeadGroups { get; set; }
        public IEnumerable<Gender> Genders { get; set; }
        public IEnumerable<State> States { get; set; }
        public IEnumerable<Country> Countries { get; set; }
        public IEnumerable<County> Counties { get; set; }

    }
}
