﻿using System;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class DeleteResponse
    {
        public string Id { get; set; }
        public string Msg { get; set; }
    }
}
