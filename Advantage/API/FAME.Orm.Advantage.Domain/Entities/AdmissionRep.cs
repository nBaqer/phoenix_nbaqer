﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class AdmissionRep : IEntity
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
    }
}
