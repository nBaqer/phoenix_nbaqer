﻿using System;
using Fame.Orm;

namespace FAME.Orm.Advantage.Domain.Entities
{
    public class State : IEntity
    {
        public Guid StateId { get; set; }
        public string StateCode { get; set; }
        public string StateDescrip { get; set; }
    }
}
