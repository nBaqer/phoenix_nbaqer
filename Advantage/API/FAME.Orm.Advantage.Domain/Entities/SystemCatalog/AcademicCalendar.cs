﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicCalendar.cs" company="FAME Inc.">
//   FAME Inc 2017
// </copyright>
// <summary>
//   Defines the Academic Calendar type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities.SystemCatalog
{
    using Fame.Orm;

    /// <summary>
    /// The Academic Calendar that indicates that the specified enrollment is of program type credit hour or clock hour.
    /// </summary>
    public class AcademicCalendar : IEntity
    {
        /// <summary>
        /// Gets or sets the ac id. This integer that indicates if the enrollment is of program type credit hour or clock hour.
        /// </summary>
        public int ACId { get; set; }

        /// <summary>
        /// Gets or sets the ac descrip. This string that represents the description of the program type is of credit hour or clock hour.
        /// </summary>
        public string ACDescrip { get; set; }
    }
}
