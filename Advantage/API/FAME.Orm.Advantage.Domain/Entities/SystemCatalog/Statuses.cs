﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Statuses.cs" company="FAME Inc">
//   2017
// </copyright>
// <summary>
//   Defines the SyStatuses type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Entities.SystemCatalog
{
    using System;

    using Fame.Orm;

    /// <summary>
    /// The system statuses that indicates that the status is active or inactive
    /// </summary>
    public class Statuses : IEntity
    {
        /// <summary>
        /// Gets or sets the status id. The GUID that indicates if the status is active or inactive
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the status. This is the status description i.e. active or inactive
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the status code. this is the single character status code i.e. 'A' or 'I'
        /// </summary>
        public string StatusCode { get; set; }
    }
}
