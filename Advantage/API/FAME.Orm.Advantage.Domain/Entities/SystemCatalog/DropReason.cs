﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReason.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DropReason type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Orm.Advantage.Domain.Entities.SystemCatalog
{
    using System;
    using Fame.Orm;

    /// <summary>
    /// The drop reason.
    /// </summary>
    public class DropReason : IEntity
    {
        /// <summary>
        /// Gets or sets the drop reason description and drop reason id .
        /// </summary>
        public Guid DropReasonId { get; set; }

        /// <summary>
        /// Gets or sets the descrip.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the status id .
        /// </summary>
        public Guid StatusId { get; set; }

        /// <summary>
        /// Gets or sets the cmp grp cmp id .
        /// </summary>
        public Guid CampusGroupId { get; set; }
    }
}
