﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetGenderList:CommandBase<Gender>
    {
        public GetGenderList(Guid campusId)
        {
            const string query = @"SELECT DISTINCT G.[GenderId] ,G.[GenderCode],G.[GenderDescrip]  
			FROM [dbo].[adGenders] AS G
			INNER JOIN dbo.syCmpGrpCmps CG ON G.CampGrpId = CG.CampGrpId
			INNER JOIN dbo.syUsersRolesCampGrps URC ON [CG].[CampGrpId] = [URC].[CampGrpId]
			INNER JOIN [dbo].[syCampuses] AS C ON [C].[CampusId] = [CG].[CampusId]
			WHERE c.[CampusId] = @CampusId
			ORDER BY G.[GenderCode] DESC";

            Parameters = new { CampusId = campusId };
            Command = $"{query}";
        }
    }
}
