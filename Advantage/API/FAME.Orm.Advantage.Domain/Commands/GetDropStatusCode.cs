﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetDropStatusCode.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the GetStatusList type. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Fame.Orm.Infrastructure;

    using FAME.Orm.Advantage.Domain.Common;
    using FAME.Orm.Advantage.Domain.Entities;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get status for campus.
    /// </summary>
    public class GetDropStatusCode : CommandBase<DropStatusCode>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetDropStatusCode"/> class.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        public GetDropStatusCode(Guid campusId)
        {
            const string Query = @"SELECT     SC.StatusCodeId
                                ,SC.StatusCodeDescrip
                                FROM       syStatusCodes SC
                                INNER JOIN syCmpGrpCmps CGC ON SC.CampGrpId = CGC.CampGrpId
                                INNER JOIN syStatuses SS ON SS.StatusId = SC.StatusId
                                WHERE      SC.SysStatusId = @StatusId
                                AND SS.StatusCode = @StatusCode
                                AND CGC.CampusId = @CampusId
                                ORDER BY   SC.StatusCodeDescrip";

            this.Parameters = new
                                  {
                                      CampusId = campusId,
                                      StatusId = Constants.SystemStatus.Dropped,
                                      StatusCode = Constants.StatusCode
                                  };
            this.Command = $"{Query}";
        }
    }
}
