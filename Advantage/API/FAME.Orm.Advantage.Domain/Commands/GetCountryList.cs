﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetCountryList:CommandBase<Country>
    {
        public GetCountryList(Guid campusId)
        {
            const string query = @"SELECT DISTINCT CY.[CountryId],CY.[CountryCode],CY.[CountryDescrip] 
			                        FROM [dbo].[adCountries] AS CY 
			                        INNER JOIN dbo.syCmpGrpCmps CG ON CY.CampGrpId = CG.CampGrpId
			                        INNER JOIN dbo.syUsersRolesCampGrps URC ON [CG].[CampGrpId] = [URC].[CampGrpId]
			                        INNER JOIN [dbo].[syCampuses] AS C ON [C].[CampusId] = [CG].[CampusId]
			                        WHERE c.[CampusId] = @CampusId
			                        ORDER BY CY.[CountryDescrip]";

            Parameters = new { CampusId = campusId };
            Command = $"{query}";
        }
    }
}
