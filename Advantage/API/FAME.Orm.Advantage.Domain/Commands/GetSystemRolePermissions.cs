﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
   public class GetSystemRolePermissions : CommandBase<RolePermission>
    {
        public GetSystemRolePermissions(Guid userId)
        {
            const string query = @"SELECT [SR].[SysRoleId] as RoleId,[SR].[Permission]
                                    FROM [dbo].[sySysRoles] AS SR
                                    INNER JOIN [dbo].[syRoles] AS R ON [R].[SysRoleId] = [SR].[SysRoleId]
                                    INNER JOIN [dbo].[syUsersRolesCampGrps] AS URCG ON [URCG].[RoleId] = [R].[RoleId]
                                    INNER JOIN [dbo].[syUsers] AS U ON [U].[UserId] = [URCG].[UserId]
                                    WHERE U.[UserId] = @UserId
                                    GROUP BY [SR].[SysRoleId],[SR].[Permission]";

            Parameters = new { UserId = userId};

            Command = $"{query}";
        }
    }
}
