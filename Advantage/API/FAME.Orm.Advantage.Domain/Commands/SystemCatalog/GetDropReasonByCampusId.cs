﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetDropReasonByCampusId.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The get drop reasons.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.SystemCatalog
{
    using System;
    using Fame.Orm.Infrastructure;

    using FAME.Orm.Advantage.Domain.Entities.SystemCatalog;

    /// <summary>
    /// The get drop reasons.
    /// </summary>
    public class GetDropReasonByCampusId : CommandBase<DropReason>
   {
       /// <summary>
       /// Initializes a new instance of the GetDropReasonByCampusId class . 
       /// </summary>
       /// <param name="campusId">
       /// The campus Id .
       /// </param>
       public GetDropReasonByCampusId(Guid campusId)
       {
           const string Query = @"SELECT     DR.DropReasonId
                                            ,DR.Descrip AS Description
                                            ,DR.Code
                                            ,DR.StatusId
                                            ,CGC.CmpGrpCmpId
                                            ,S.Status
                                FROM       arDropReasons DR
                                INNER JOIN syCmpGrpCmps CGC ON DR.CampGrpId = CGC.CampGrpId
                                INNER JOIN syStatuses S ON DR.StatusId = S.StatusId
                                WHERE      CGC.CampusId = @CampusId
                                            AND DR.StatusId = S.StatusId
                                            AND S.Status = 'Active'
                                ORDER BY   DR.Descrip;";
            this.Parameters = new { CampusId = campusId };
            this.Command = $"{Query}";
       }
   }
}
