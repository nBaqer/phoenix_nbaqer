﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetAcademicCalendarTypesByEnrollmentId.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
// The command class for academic calendar types by enrollmentId.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.SystemCatalog
{
    using System;
    using Fame.Orm.Infrastructure;
    using FAME.Orm.Advantage.Domain.Entities.SystemCatalog;

    /// <summary>
    /// The get academic calendar types.
    /// </summary>
    public class GetAcademicCalendarTypesByEnrollmentId : CommandBase<AcademicCalendar>
    {
        /// <summary>
        /// Initializes a new instance of the GetAcademicCalendarTypesByEnrollmentId class. 
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        public GetAcademicCalendarTypesByEnrollmentId(Guid studentEnrollmentId)
        {
            const string Query = @"SELECT     ac.ACId
                                   FROM       dbo.syAcademicCalendars ac
                                   INNER JOIN dbo.arPrograms p ON p.ACId = ac.ACId
                                   INNER JOIN dbo.arPrgVersions pv ON pv.ProgId = p.ProgId
                                   INNER JOIN dbo.arStuEnrollments se ON se.PrgVerId = pv.PrgVerId
                                   WHERE      se.StuEnrollId = @StudentEnrollmentId;";
            this.Parameters = new { StudentEnrollmentId = studentEnrollmentId };
            this.Command = $"{Query}";
        }
    }
}
