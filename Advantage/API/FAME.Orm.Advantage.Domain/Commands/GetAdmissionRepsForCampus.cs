﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetAdmissionRepsForCampus : CommandBase<AdmissionRep>
    {
        public GetAdmissionRepsForCampus(Guid campusId)
        {
            const string query = @"SELECT DISTINCT U.UserId,U.FullName 
                                    FROM syUsers U 
                                    INNER JOIN dbo.syUsersRolesCampGrps URC ON URC.UserId = U.UserId
                                    INNER JOIN dbo.syCmpGrpCmps C ON C.CampGrpId = URC.CampGrpId
                                    INNER JOIN syRoles R ON R.RoleId = URC.RoleId
                                    WHERE R.SysRoleId = 3
                                    AND [U].[AccountActive] = 1
                                    AND C.CampusId= @CampusId
                                    ORDER BY U.FullName";

            Parameters = new { CampusId = campusId};
            Command = $"{query}";
        }
    }
}
