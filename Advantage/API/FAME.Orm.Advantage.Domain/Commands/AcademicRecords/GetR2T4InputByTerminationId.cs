﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetR2T4InputByTerminationId.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the GetEnrollmentList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.AcademicRecords
{
    using System;
    using Fame.Orm.Infrastructure;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get R2T4Input By TerminationId returns matching R2T4 input record for given Termination Id.
    /// </summary>
    public class GetR2T4InputByTerminationId : CommandBase<R2T4InputEntity>
    {
        /// <summary>
        /// Initializes a new instance of the GetR2T4InputByTerminationId class.  
        /// </summary>
        /// <param name="terminationId">
        /// The termination id.
        /// </param>
        public GetR2T4InputByTerminationId(Guid terminationId)
        { 
            const string Query = @"SELECT *
                                FROM dbo.arR2T4Input
                                WHERE arR2T4Input.TerminationId = @TerminationId";

            this.Parameters = new { TerminationId = terminationId };
            this.Command = $"{Query}";
        }
    }
}
