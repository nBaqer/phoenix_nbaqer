﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetStudentsByCampusAndFilter.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The command class for getting students by filter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.AcademicRecords
{
    using System;
    using Fame.Orm.Infrastructure;

    using FAME.Orm.Advantage.Domain.Common;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get students for campus.
    /// </summary>
    public class GetStudentsByCampusAndFilter : CommandBase<Student>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetStudentsByCampusAndFilter"/> class.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        public GetStudentsByCampusAndFilter(Guid campusId, string filter)
        {
            const string Query = @"SELECT     students.[StudentId]
                                  ,RTRIM(ISNULL(students.LastName, '') + ', ' +students.FirstName + ' ' + ISNULL(students.MiddleName , '')) AS DisplayName
                                  ,students.FirstName
                                  ,students.MiddleName
                                  ,students.LastName
                                  ,students.SSN
                                  ,students.StudentStatusId
                                  ,students.BirthDate
                                  ,students.ModUser
                                  ,students.ModDate
                                  ,students.Prefix
                                  ,students.Suffix
                                  ,students.Sponsor
                                  ,students.AssignedDate
                                  ,students.Gender
                                  ,students.Race
                                  ,students.MaritalStatus
                                  ,students.FamilyIncome
                                  ,students.ExpectedStart
                                  ,students.ShiftID AS ShiftId
                                  ,students.Nationality
                                  ,students.Citizen
                                  ,students.DrivLicStateID AS DrivLicStateId
                                  ,students.DrivLicNumber
                                  ,students.AlienNumber
                                  ,students.SourceDate AS SourceDate
                                  ,students.PreviousEducation AS EdLvlId
                                  ,students.StudentNumber
                                  ,students.DependencyTypeId
                                  ,students.DegCertSeekingId
                                  ,students.GeographicTypeId
                                  ,students.HousingId
                                  ,students.admincriteriaid
                                  ,students.AttendTypeId
                        FROM       adLeads AS students
                        INNER JOIN dbo.syStatusCodes leadStatus ON leadStatus.StatusCodeId = students.LeadStatus
                        INNER JOIN dbo.sySysStatus systemStatus ON systemStatus.SysStatusId = leadStatus.SysStatusId
                        WHERE      systemStatus.SysStatusId = @EnrollLeadStatus
                                   AND students.StudentId <> @DefaultStudentId
                                   AND students.CampusId = @CampusId
                                   AND (                                         
                                        students.SSN LIKE REPLACE(@FilterLike,'-','')                                        
                                        OR students.FirstName LIKE  @FilterLike 
                                        OR students.LastName LIKE @FilterLike 
                                        OR students.MiddleName LIKE @FilterLike 
                                        OR (ISNULL(students.LastName, '') + ' ' + ISNULL(students.FirstName , '') LIKE @FilterLike )
                                        OR (ISNULL(students.LastName, '') + ' ' +students.FirstName + ' ' + ISNULL(students.MiddleName , '') LIKE @FilterLike )
                                   ) order by students.LastName asc;";

            this.Parameters = new
                                  {
                                      CampusId = campusId,
                                      Filter = filter,
                                      FilterLike = "%" + filter + "%",
                                      DefaultStudentId = Guid.Empty,
                                      EnrollLeadStatus = Constants.LeadStatus.Enrolled
                                  };
            this.Command = $"{Query}";
        }
    }
}