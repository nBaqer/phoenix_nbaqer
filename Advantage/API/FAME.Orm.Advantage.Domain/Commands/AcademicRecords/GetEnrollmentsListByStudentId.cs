﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetEnrollmentsListByStudentId.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the GetEnrollmentList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using Fame.Orm.Infrastructure;
    using FAME.Orm.Advantage.Domain.Common;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get enrollments list by student id.
    /// </summary>
    public class GetEnrollmentsListByStudentId : CommandBase<StudentEnrollment>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetEnrollmentsListByStudentId"/> class.
        /// </summary>
        /// <param name="studentId">
        /// The student id.
        /// </param>
        public GetEnrollmentsListByStudentId(Guid studentId)
        {
            var activeStatusIds =
                new List<int>
                    {
                        (int)Constants.SystemStatus.FutureStart,
                        (int)Constants.SystemStatus.CurrentlyAttending,
                        (int)Constants.SystemStatus.LeaveOfAbsence,
                        (int)Constants.SystemStatus.Suspension,
                        (int)Constants.SystemStatus.AcademicProbation,
                        (int)Constants.SystemStatus.Externship,
                        (int)Constants.SystemStatus.DisciplinaryProbation,
                        (int)Constants.SystemStatus.WarningProbation
                    };

            var inActiveStatusIds =
                new List<int>
                    {
                        (int)Constants.SystemStatus.NoStart,
                        (int)Constants.SystemStatus.Dropped,
                        (int)Constants.SystemStatus.Graduated,
                        (int)Constants.SystemStatus.TransferOut
                    };

            string query = string.Format(@"SELECT     DISTINCT se.StuEnrollId
                                                     ,pv.PrgVerDescrip
                                                     ,ss.SysStatusId
                                                     ,sc.StatusCodeDescrip
                                                     ,ssc.DateOfChange
                                                     ,( CASE WHEN ss.SysStatusId IN ({0}) THEN 'Active'
                                                              WHEN ss.SysStatusId IN ({1}) THEN 'Inactive'
                                                        END
                                                      ) AS 'Status'
                                                     ,se.EnrollDate
                                                     ,se.StartDate
                                                     ,s.SSN
                                           FROM       dbo.arPrgVersions pv
                                           INNER JOIN dbo.arStuEnrollments se ON pv.PrgVerId = se.PrgVerId
                                           INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                                           INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                                           INNER JOIN dbo.syStatuses st ON st.StatusId = pv.StatusId
                                           INNER JOIN dbo.syStudentStatusChanges ssc ON se.StuEnrollId = ssc.StuEnrollId
                                           INNER JOIN dbo.adLeads s ON s.StudentId = se.StudentId
                                           WHERE      se.StudentId = @StudentId
                                                      AND ssc.DateOfChange = (
                                                                             SELECT CASE WHEN MAX(DateOfChange) IS NULL THEN MAX(ModDate)
                                                                                         ELSE MAX(DateOfChange)
                                                                                    END
                                                                             FROM   syStudentStatusChanges
                                                                             WHERE  StuEnrollId = se.StuEnrollId
                                                                             )
                                           ORDER BY   [Status]
                                                     ,pv.PrgVerDescrip;",
                string.Join(",", activeStatusIds),
                string.Join(",", inActiveStatusIds));

            this.Parameters = new { StudentId = studentId };
            this.Command = $"{query}";
        }
    }
}