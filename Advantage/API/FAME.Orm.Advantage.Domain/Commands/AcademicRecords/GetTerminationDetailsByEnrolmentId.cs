﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetTerminationDetailsByEnrolmentId.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The get termination details.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.AcademicRecords
{
    using System;
    using Fame.Orm.Infrastructure;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get termination details
    /// </summary>
    public class GetTerminationDetailsByEnrolmentId : CommandBase<R2T4TerminationDetail>
    {
        /// <summary>
        /// Initializes a new instance of the GetTerminationDetailsByEnrolmentId class. 
        /// </summary>
        /// <param name="enrollmentId">
        /// The EnrollId.
        /// </param>
        public GetTerminationDetailsByEnrolmentId(Guid enrollmentId)
        {
            const string Query = @"select top 1 RTD.TerminationId
                ,RTD.StatusCodeId 
                ,RTD.DropReasonId 
                ,RTD.DateWithdrawalDetermined 
                ,RTD.LastDateAttended 
                ,RTD.IsPerformingR2T4Calculator 
                ,RTD.CalculationPeriodTypeId
                ,RTD.UpdatedById
                ,RTD.UpdatedDate
                ,RTD.CreatedDate
                ,RTD.CreatedById
                from arR2T4TerminationDetails RTD where RTD.StuEnrollmentId = @StuEnrolmentId; ";

            this.Parameters = new { StuEnrolmentId = enrollmentId };
            this.Command = $"{ Query}";
        }
    }
}
