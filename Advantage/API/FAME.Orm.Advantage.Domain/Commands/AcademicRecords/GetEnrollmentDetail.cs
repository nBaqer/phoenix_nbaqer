﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetEnrollmentDetail.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the GetEnrollmentList type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Commands.AcademicRecords
{
    using System;
    using Fame.Orm.Infrastructure;
    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The get detail by enrollment id.
    /// </summary>
    public class GetEnrollmentDetail : CommandBase<StudentEnrollment>
    {
        /// <summary>
        /// Initializes a new instance of the GetEnrollmentDetail class.
        /// </summary>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        public GetEnrollmentDetail(Guid studentId, Guid enrollmentId)
        {
            const string Query = @"SELECT *
                                    FROM arStuEnrollments AS enrollments
                                        INNER JOIN arPrgVersions AS programVersions
                                            ON enrollments.PrgVerId = programVersions.PrgVerId
                                        INNER JOIN arAttUnitType AS unitTypes
                                            ON unitTypes.UnitTypeId = programVersions.UnitTypeId
                                        INNER JOIN syStatusCodes AS StatusCodes
                                            ON StatusCodes.StatusCodeId = enrollments.StatusCodeId
                                        INNER JOIN dbo.sySysStatus AS SysStatus
	                                        ON StatusCodes.SysStatusId = SysStatus.SysStatusId
                                    WHERE enrollments.StudentId = @StudentId
                                        AND enrollments.StuEnrollId = @EnrollmentId";

            this.Parameters = new { StudentId = studentId, EnrollmentId = enrollmentId };
            this.Command = $"{Query}";
        }
    }
}
