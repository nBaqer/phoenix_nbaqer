﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetCountyList:CommandBase<County>
    {
        public GetCountyList(Guid campusId)
        {
            const string query = @"SELECT DISTINCT [CountyId] , [CountyCode] ,[CountyDescrip] 
				FROM [dbo].[adCounties] AS CN
				INNER JOIN dbo.syCmpGrpCmps CG ON CN.CampGrpId = CG.CampGrpId
				INNER JOIN dbo.syUsersRolesCampGrps URC ON [CG].[CampGrpId] = [URC].[CampGrpId]
				INNER JOIN [dbo].[syCampuses] AS C ON [C].[CampusId] = [CG].[CampusId]
				WHERE c.[CampusId] = @CampusId
				ORDER BY [CountyDescrip]";

            Parameters = new { CampusId = campusId };
            Command = $"{query}";
        }
    }
}
