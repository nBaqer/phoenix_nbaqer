﻿using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetAvailableNewLeadStatusList : CommandBase<LeadStatus>
    {
        public GetAvailableNewLeadStatusList()
        {
            const string query = @"SELECT DISTINCT [SC].[StatusCodeId] ,[SC].[StatusCode] ,[SC].[StatusCodeDescrip] 
		                            FROM dbo.syStatusCodes SC 
		                            INNER JOIN dbo.sySysStatus SS ON SS.StatusId = SC.StatusId 
		                            WHERE SS.SysStatusId= 1
                                    ORDER BY [SC].[StatusCodeDescrip]";
            Command = $"{query}";
        }
    }
}
