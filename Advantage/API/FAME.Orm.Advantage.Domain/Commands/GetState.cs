﻿using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetState : CommandBase<State>
    {
        public GetState(string lookup)
        {
            const string query = @"SELECT DISTINCT [StateId] ,[StateCode] ,[StateDescrip] 
	                                                FROM [dbo].[syStates] AS ST
	                                                INNER JOIN [dbo].[syStatuses] AS SY ON [SY].[StatusId] = [ST].[StatusId]
	                                                WHERE SY.[Status] = 'Active'
	                                                AND ([ST].[StateCode] = @StateCode)";

            Parameters = new { StateCode = lookup };

            Command = $"{query}";
        }
    }
}
