﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetCampusListForUser : CommandBase<Campus>
    {
        public GetCampusListForUser(Guid userId)
        {
            const string query = @"SELECT DISTINCT C.[CampusId],[C].[CampCode],C.[CampDescrip] 
                                    FROM [dbo].[syCampuses] AS C
                                    INNER JOIN [dbo].[syCmpGrpCmps] AS CG ON [CG].[CampusId] = [C].[CampusId]
                                    INNER JOIN [dbo].[syUsersRolesCampGrps] AS URC ON CG.[CampGrpId] = URC.[CampGrpId]
                                    INNER JOIN [dbo].[syRoles] AS R ON [R].[RoleId] = [URC].[RoleId]
                                    INNER JOIN [dbo].[syUsers] AS U ON [U].[UserId] = [URC].[UserId]
                                    WHERE U.[UserId] = @UserId
                                    ORDER BY C.[CampDescrip]";

            Parameters = new { UserId = userId };
            Command = $"{query}";
        }
    }
}
