﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetStateList:CommandBase<State>
    {
        public GetStateList(Guid campusId)
        {
            const string query = @"SELECT DISTINCT ST.[StateId] ,ST.[StateCode] ,ST.[StateDescrip] 
	                                FROM [dbo].[syStates] AS ST
	                                INNER JOIN dbo.syCmpGrpCmps CG ON ST.CampGrpId = CG.CampGrpId
	                                INNER JOIN dbo.syUsersRolesCampGrps URC ON [CG].[CampGrpId] = [URC].[CampGrpId]
	                                INNER JOIN [dbo].[syCampuses] AS C ON [C].[CampusId] = [CG].[CampusId]
	                                INNER JOIN [dbo].[syStatuses] AS SY ON [SY].[StatusId] = [ST].[StatusId]
	                                WHERE SY.[Status] = 'Active' 
	                                AND c.[CampusId] = @CampusId
	                                ORDER BY [StateCode]";
            Parameters = new { CampusId = campusId };
            Command = $"{query}";
        }
    }
}
