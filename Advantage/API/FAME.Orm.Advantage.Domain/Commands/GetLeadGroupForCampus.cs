﻿using System;
using Fame.Orm.Infrastructure;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Commands
{
    public class GetLeadGroupForCampus : CommandBase<LeadGroup>
    {
        public GetLeadGroupForCampus(Guid campusId)
        {
            const string query = @"
				SELECT DISTINCT LG.[LeadGrpId],[LG].[Descrip]
					                FROM [dbo].[adLeadGroups] AS LG 
					                INNER JOIN dbo.syCampGrps CG ON CG.CampGrpId = LG.CampGrpId
					                INNER JOIN dbo.syCmpGrpCmps C ON C.CampGrpId = CG.CampGrpId
									INNER JOIN [dbo].[syStatuses] AS SY ON [LG].[StatusId] = [SY].[StatusId]
					                WHERE C.CampusId= @CampusId
									AND  SY.[Status] = 'active'";

            Parameters = new { CampusId = campusId };
            Command = $"{query}";
        }
    }
}
