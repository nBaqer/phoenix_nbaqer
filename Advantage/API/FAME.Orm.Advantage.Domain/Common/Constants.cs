﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The Constants class to provide the constant values to use in domain project.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Common
{
    using System.Xml.Linq;

    /// <summary>
    /// The constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// Status code type for active status
        /// </summary>
        public const string StatusCode = "A";

        /// <summary>
        /// The have met options.
        /// </summary>
        public enum HaveMetOptions
        {
            /// <summary>
            /// The actual hours.
            /// </summary>
            ActualHours = 1,

            /// <summary>
            /// The scheduled hours.
            /// </summary>
            ScheduledHours = 2,

            /// <summary>
            /// The actual scheduled hours.
            /// </summary>
            ActualScheduledHours = 3,

            /// <summary>
            /// The credits earned.
            /// </summary>
            CreditsEarned = 4

        }
        /// <summary>
        /// The lead status.
        /// </summary>
        public enum LeadStatus
        {
            /// <summary>
            /// The new lead.
            /// </summary>
            NewLead = 1,

            /// <summary>
            /// The application received.
            /// </summary>
            ApplicationReceived = 2,

            /// <summary>
            /// The application not accepted.
            /// </summary>
            ApplicationNotAccepted = 3,

            /// <summary>
            /// The interview scheduled.
            /// </summary>
            InterviewScheduled = 4,

            /// <summary>
            /// The interviewed.
            /// </summary>
            Interviewed = 5,

            /// <summary>
            /// The enrolled.
            /// </summary>
            Enrolled = 6,

            /// <summary>
            /// The dead lead.
            /// </summary>
            DeadLead = 17,

            /// <summary>
            /// The will enroll in the future.
            /// </summary>
            WillEnrollInTheFuture = 18,

            /// <summary>
            /// The imported.
            /// </summary>
            Imported = 25,

            /// <summary>
            /// The duplicated.
            /// </summary>
            Duplicated = 26
        }

        /// <summary>
        /// The system status.
        /// </summary>
        public enum SystemStatus
        {
            /// <summary>
            /// The NewLead
            /// </summary>
            NewLead = 1,

            /// <summary>
            /// The lead Application Received.
            /// </summary>
            ApplicationReceived = 2,

            /// <summary>
            /// The lead Application Not Accepted.
            /// </summary>
            ApplicationNotAccepted = 3,

            /// <summary>
            /// The lead Interview Scheduled.
            /// </summary>
            InterviewScheduled = 4,

            /// <summary>
            /// The lead Interviewed.
            /// </summary>
            Interviewed = 5,

            /// <summary>
            /// The lead Enrolled.
            /// </summary>
            Enrolled = 6,

            /// <summary>
            /// The lead Future Start.
            /// </summary>
            FutureStart = 7,

            /// <summary>
            /// The lead No Start.
            /// </summary>
            NoStart = 8,

            /// <summary>
            /// The lead Currently Attending.
            /// </summary>
            CurrentlyAttending = 9,

            /// <summary>
            /// The lead Leave Of Absence.
            /// </summary>
            LeaveOfAbsence = 10,

            /// <summary>
            /// The lead Suspension.
            /// </summary>
            Suspension = 11,

            /// <summary>
            /// The lead Dropped.
            /// </summary>
            Dropped = 12,

            /// <summary>
            /// The lead Graduated.
            /// </summary>
            Graduated = 14,

            /// <summary>
            /// The lead Active.
            /// </summary>
            Active = 15,

            /// <summary>
            /// The lead InActive.
            /// </summary>
            InActive = 16,

            /// <summary>
            /// The Dead Lead.
            /// </summary>
            DeadLead = 17,

            /// <summary>
            /// The lead Will Enroll in the Future.
            /// </summary>
            WillEnrollintheFuture = 18,

            /// <summary>
            /// The lead Transfer Out.
            /// </summary>
            TransferOut = 19,

            /// <summary>
            /// The lead Academic Probation.
            /// </summary>
            AcademicProbation = 20,

            /// <summary>
            /// The lead Externship.
            /// </summary>
            Externship = 22,

            /// <summary>
            /// The lead Disciplinary Probation.
            /// </summary>
            DisciplinaryProbation = 23,

            /// <summary>
            /// The lead Warning Probation.
            /// </summary>
            WarningProbation = 24,

            /// <summary>
            /// The lead Imported.
            /// </summary>
            Imported = 25,

            /// <summary>
            /// The lead Duplicated.
            /// </summary>
            Duplicated = 26
        }

        /// <summary>
        /// The academic calendars.
        /// </summary>
        public enum AcademicCalendars
        {
            /// <summary>
            /// The non standard term. This is of program type credit hour.
            /// </summary>
            NonStandardTerm = 1,

            /// <summary>
            /// The quarter. This is of program type credit hour.
            /// </summary>
            Quarter = 2,

            /// <summary>
            /// The semester. This is of program type credit hour.
            /// </summary>
            Semester = 3,

            /// <summary>
            /// The trimester. This is of program type credit hour.
            /// </summary>
            Trimester = 4,

            /// <summary>
            /// The clock hour. This is of program type clock hour.
            /// </summary>
            ClockHour = 5,

            /// <summary>
            /// The non term. This is of program type credit hour.
            /// </summary>
            NonTerm = 6
        }

        /// <summary>
        /// The method type is to differentiate between the methods Save and others in order to validate the model of student termination.
        /// </summary>
        public enum MethodType
        {
            /// <summary>
            /// The save.
            /// </summary>
            Save = 1,

            /// <summary>
            /// The others.
            /// </summary>
            Others = 2
        }

        /// <summary>
        /// The system role.
        /// </summary>
        public enum SystemRole
        {
            /// <summary>
            /// The system administrator.
            /// </summary>
            SystemAdministrator = 1,

            /// <summary>
            /// The instructors.
            /// </summary>
            Instructors = 2,

            /// <summary>
            /// The admission reps.
            /// </summary>
            AdmissionReps = 3,

            /// <summary>
            /// The academic advisors.
            /// </summary>
            AcademicAdvisors = 4,

            /// <summary>
            /// The other.
            /// </summary>
            Other = 5,

            /// <summary>
            /// The director of admissions.
            /// </summary>
            DirectorOfAdmissions = 8,

            /// <summary>
            /// The director of academics.
            /// </summary>
            DirectorOfAcademics = 13
        }

        public enum StatusLevel
        {
            Lead = 1,
            StudentEnrollments = 2,
            Student = 3
        }

        /// <summary>
        /// The award types.
        /// </summary>
        public enum AwardTypes
        {
            /// <summary>
            /// The grant.
            /// </summary>
            Grant = 1,

            /// <summary>
            /// The loan.
            /// </summary>
            Loan = 2,

            /// <summary>
            /// The school scholarship.
            /// </summary>
            SchoolScholarship = 3,

            /// <summary>
            /// The private scholarship.
            /// </summary>
            PrivateScholarship = 4,

            /// <summary>
            /// The state scholarship.
            /// </summary>
            StateScholarship = 5,

            /// <summary>
            /// The other.
            /// </summary>
            Other = 6
        }

        /// <summary>
        /// The fund sources.
        /// </summary>
        public enum FundSources
        {
            /// <summary>
            /// The va.
            /// </summary>
            VA = 1,

            /// <summary>
            /// The ac g_ grant.
            /// </summary>
            ACG_Grant = 2,

            /// <summary>
            /// The tap.
            /// </summary>
            TAP_Active = 3,

            /// <summary>
            /// The empire.
            /// </summary>
            Empire = 4,

            /// <summary>
            /// The stafford un.
            /// </summary>
            StaffordUN = 5,

            /// <summary>
            /// The vietnam vets.
            /// </summary>
            VietnamVets = 6,

            /// <summary>
            /// The deceased_ pol.
            /// </summary>
            Deceased_Pol = 7,

            /// <summary>
            /// The mha.
            /// </summary>
            MHA = 8,

            /// <summary>
            /// The perkins.
            /// </summary>
            Perkins = 9,

            /// <summary>
            /// The plus direct.
            /// </summary>
            PLUSDirect = 10,

            /// <summary>
            /// The veterans_911.
            /// </summary>
            Veterans_911 = 11,

            /// <summary>
            /// The cash.
            /// </summary>
            Cash = 12,

            /// <summary>
            /// The wi a_ s.
            /// </summary>
            WIA_S = 13,

            /// <summary>
            /// The reg_ dis_ vets.
            /// </summary>
            Reg_Dis_Vets = 14,

            /// <summary>
            /// The arts.
            /// </summary>
            ARTS = 15,

            /// <summary>
            /// The child_disabl.
            /// </summary>
            Child_disabl = 16,

            /// <summary>
            /// The direct loan un.
            /// </summary>
            DirectLoanUN = 17,

            /// <summary>
            /// The seog.
            /// </summary>
            SEOG = 18,

            /// <summary>
            /// The direct loan sb.
            /// </summary>
            DirectLoanSb = 19,

            /// <summary>
            /// The etv.
            /// </summary>
            ETV = 20,

            /// <summary>
            /// The lift.
            /// </summary>
            Lift = 21,

            /// <summary>
            /// The wi a_ ob.
            /// </summary>
            WIA_OB  = 22,

            /// <summary>
            /// The regents schol.
            /// </summary>
            RegentsSchol = 23,

            /// <summary>
            /// The native americ.
            /// </summary>
            NativeAmeric = 24,

            /// <summary>
            /// The deceased_ civ.
            /// </summary>
            Deceased_Civ = 25,

            /// <summary>
            /// The burea indian.
            /// </summary>
            BureaIndian = 26,

            /// <summary>
            /// The do l_ suffolk.
            /// </summary>
            DOL_Suffolk = 27,

            /// <summary>
            /// The pell.
            /// </summary>
            Pell = 28,

            /// <summary>
            /// The stafford sub.
            /// </summary>
            StaffordSub = 29,

            /// <summary>
            /// The stafford plus.
            /// </summary>
            StaffordPlus = 30,

            /// <summary>
            /// The sallie mae.
            /// </summary>
            SallieMae = 31,

            /// <summary>
            /// The idt i_ scholar.
            /// </summary>
            IDTI_Scholar = 32,

            /// <summary>
            /// The outside_scho.
            /// </summary>
            Outside_scho = 33,

            /// <summary>
            /// The tap.
            /// </summary>
            TAP_Inactive = 34,

            /// <summary>
            /// The do l_ hemp.
            /// </summary>
            DOL_Hemp = 35,

            /// <summary>
            /// The acces.
            /// </summary>
            ACCES = 36
        }

        public enum SystemFundSources
        {
            MiscCashPayments = 1,
            Pell = 2,
            SEOG = 3,
            Perkins = 4,
            FWS = 5,
            DL_PLUS = 6,
            DL_SUB = 7,
            DL_UNSUB = 8,
            Other = 19
        }

        public enum SystemTransactionCodes
        {
            FinancialAidPayment = 11
        }

        /// <summary>
        /// The payment period term type.
        /// </summary>
        public enum PaymentPeriodTermType
        {
            /// <summary>
            /// The direct loan.
            /// </summary>
            DirectLoan = 1,

            /// <summary>
            /// The non standard.
            /// </summary>
            NonStandard = 2
        }
    }
}