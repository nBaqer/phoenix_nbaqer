﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4TerminationDetailMapper.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4TerminationDetail mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Orm.Advantage.Domain.Persistence
{
    using Fame.Orm.Helpers;

    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <summary>
    /// The termination mapper.
    /// </summary>
    public class R2T4TerminationDetailMapper : FameAutoClassMapper<R2T4TerminationDetail>
    {
        /// <summary>
        /// Initializes a new instance of the R2T4TerminationDetailMapper class.
        /// </summary>
        public R2T4TerminationDetailMapper() : base(true)
        {
            this.Table("ARR2T4TerminationDetails");
            this.AutoMap();
        }
    }
}
