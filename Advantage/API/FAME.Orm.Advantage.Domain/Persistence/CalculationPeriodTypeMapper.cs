﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationPeriodTypeMapper.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The calculation period type mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Persistence
{
    using Fame.Orm.Helpers;
    using FAME.Orm.Advantage.Domain.Entities;

    /// <summary>
    /// The calculation period type mapper.
    /// </summary>
    public sealed class CalculationPeriodTypeMapper : FameAutoClassMapper<CalculationPeriodType>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationPeriodTypeMapper"/> class. 
        /// Calculation Period Type mapper.
        /// </summary>
        public CalculationPeriodTypeMapper() : base(true)
        {
            this.Table("syR2T4CalculationPeriodTypes");
            this.AutoMap();
        }
    }
}
