﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;
namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class CampusMapper :FameAutoClassMapper<Campus>
    {
        public CampusMapper() : base(true)
        {
            Table("syCampuses");
            AutoMap();
        }
    }
}
