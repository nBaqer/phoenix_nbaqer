﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class AdmissionRepMapper : FameAutoClassMapper<AdmissionRep>
    {
        public AdmissionRepMapper() : base(true)
        {
            Table("syUsers");
            AutoMap();
        }
    }
}
