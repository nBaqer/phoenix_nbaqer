﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class LeadsMapper : FameAutoClassMapper<Lead>
    {
        public LeadsMapper() : base(true)
        {
            Table("AdLeads");
            AutoMap();
        }
    }
}

