﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class LeadStatusMapper : FameAutoClassMapper<LeadStatus>
    {
        public LeadStatusMapper() : base(true)
        {
            Table("syStatusCodes");
            AutoMap();
        }
    }
}
