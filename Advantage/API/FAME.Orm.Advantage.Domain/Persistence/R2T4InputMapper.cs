﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4InputMapper.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 input entity mapper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Persistence
{
    using Fame.Orm.Helpers;

    using FAME.Orm.Advantage.Domain.Entities.AcademicRecords;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 input entity mapper.
    /// </summary>
    public class R2T4InputMapper : FameAutoClassMapper<R2T4InputEntity>
    {
        /// <summary>
        /// Initializes a new instance of the R2T4InputMapper class.
        /// </summary>
        public R2T4InputMapper() : base(true)
        {
            this.Table("ARR2T4Input");
            this.AutoMap();
        }
    }
}