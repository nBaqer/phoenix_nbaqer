﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusesMapper.cs" company="FAME Inc">
//   2017
// </copyright>
// <summary>
//   Defines the StatusesMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Orm.Advantage.Domain.Persistence.SystemCatalog
{
    using Fame.Orm.Helpers;

    using FAME.Orm.Advantage.Domain.Entities.SystemCatalog;

    /// <summary>
    /// The system statuses mapper that will map to the table that indicates if the status is active or inactive.
    /// </summary>
    public sealed class StatusesMapper : FameAutoClassMapper<Statuses>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatusesMapper"/> class.
        /// </summary>
        public StatusesMapper()
            : base(true)
        {
            this.Table("syStatuses");
            this.AutoMap();
        }
    }
}
