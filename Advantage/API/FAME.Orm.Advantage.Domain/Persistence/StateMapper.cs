﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class StateMapper : FameAutoClassMapper<State>
    {
        public StateMapper() : base(true)
        {
            Table("syStates");
            AutoMap();
        }
    }
}
