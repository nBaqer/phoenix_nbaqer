﻿using Fame.Orm.Helpers;
using FAME.Orm.Advantage.Domain.Entities;

namespace FAME.Orm.Advantage.Domain.Persistence
{
    public sealed class LeadGroupMapper : FameAutoClassMapper<LeadGroup>
    {
    public  LeadGroupMapper() : base(true)
        {
            Table("adLeadGroups");
            AutoMap();
        }
    }
}
