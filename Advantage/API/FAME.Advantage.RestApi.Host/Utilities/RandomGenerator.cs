﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RandomGenerator.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the RandomGenerator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Utilities
{
    using System;
    using System.Text;

    /// <summary>
    /// The random generator.
    /// </summary>
    public class RandomGenerator
    {
        /// <summary>
        /// The random string.
        /// </summary>
        /// <param name="size">
        /// The size.
        /// </param>
        /// <param name="useLowerCase">
        /// The use lower case.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string RandomString(int size, bool useLowerCase)
        {
            Random random = new Random();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < size - 1; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32((26 * random.NextDouble()) + 65));
                builder.Append(ch);
            }

            if (useLowerCase)
            {
                return builder.ToString().ToLower();
            }

            return builder.ToString();
        }

        /// <summary>
        /// The random number.
        /// </summary>
        /// <param name="min">
        /// The min.
        /// </param>
        /// <param name="max">
        /// The max.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }
    }
}
