﻿
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomUnauthorizedResult.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the CustomUnauthorizedResult type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The CustomUnauthorizedResult class.
    /// </summary>
    public class CustomUnauthorizedResult : JsonResult
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">
        /// The error message to return
        /// </param>
        public CustomUnauthorizedResult(string message)
            : base(new { error = message })
        {
            StatusCode = StatusCodes.Status401Unauthorized;
        }
    }
}
