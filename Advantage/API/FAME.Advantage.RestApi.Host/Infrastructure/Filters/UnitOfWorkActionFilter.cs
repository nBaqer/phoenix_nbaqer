﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitOfWorkActionFilter.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the UnitOfWorkActionFilter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using Fame.EFCore.Advantage;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Initializer;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using FAME.Orm.AdvTenant.Domain.Entities;
    using Jose;
    using Microsoft.ApplicationInsights.Extensibility;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The unit of work action filter.
    /// </summary>
    public class UnitOfWorkActionFilter : ActionFilterAttribute
    {
        /// <summary>
        /// The _logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The auth service.
        /// </summary>
        private readonly IAuthService authService;

        /// <summary>
        /// The configuration.
        /// </summary>
        private readonly IConfigurationRoot configuration;

        /// <summary>
        /// The report connection.
        /// </summary>
        private readonly IReportConnection reportConnection;

        /// <summary>
        /// The report input parameter.
        /// </summary>
        private readonly IReportRequest reportInputParameter;

        /// <summary>
        /// The report server settings.
        /// </summary>
        private IReportServerSettings reportServerSettings;

        /// <summary>
        /// The is tenant request.
        /// </summary>
        private bool isTenantRequest;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWorkActionFilter"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="authService">
        /// The auth service.
        /// </param>
        /// <param name="reportConnection">
        /// The report Connection.
        /// </param>
        /// <param name="reportInputParameter">
        /// The report Input Parameter.
        /// </param>
        /// <param name="reportServerSettings">
        /// The report Server Settings.
        /// </param>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        public UnitOfWorkActionFilter(
            ILogger<UnitOfWorkActionFilter> logger,
            IAdvantageDbContext advantageDbContext,
            IAuthService authService,
            IReportConnection reportConnection,
            IReportRequest reportInputParameter,
            IReportServerSettings reportServerSettings,
            IConfigurationRoot configuration)
        {
            this.logger = logger;
            this.advantageDbContext = advantageDbContext;
            this.authService = authService;

            this.reportConnection = reportConnection;
            this.reportInputParameter = reportInputParameter;
            this.configuration = configuration;
            this.reportServerSettings = reportServerSettings;
        }

        private bool useLazyLoading => this.configuration.GetSection("UseLazyLoading").Get<bool>();
        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {

                TokenInfo info = this.GetTokenInfo(context);
                this.isTenantRequest = info.IsValid;

                if (!this.isTenantRequest)
                {
                    return;
                }

                ApiUser user = this.GetUser(info) ?? this.GetUser(info);

                DbContextOptionsBuilder<AdvantageContext> option = new DbContextOptionsBuilder<AdvantageContext>();

                if (this.useLazyLoading)
                {
                    option.UseLazyLoadingProxies();
                }
                option.UseSqlServer(user.BuildConnectionString());

                this.advantageDbContext.SetContext(new AdvantageContext(option.Options), SimpleCacheValues.KEY + "_" + user.TenantName);

                SyUserType userType = this.GetAdvantageUserType(user.UserId);

                TenantUser tenantUser = new TenantUser(
                    user.UserId,
                    user.IsSupportUser,
                    user.Email,
                    user.TenantName,
                    userType.Code);

                this.advantageDbContext.SetTenantUser(tenantUser);

                this.InitializeReportSettings(context, user);
            }
            catch (Exception e)
            {
                e.TrackException();
                // TODO Log the exceptions some where
                this.logger.LogError($"{ApiMsgs.UOW_CONTAINER_ERROR} {e}");
                throw;
            }
        }

        /// <summary>
        /// The on action executed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (!this.isTenantRequest)
            {
                return;
            }

            if (context.HasException())
            {
                return;
            }

            try
            {
                this.advantageDbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                this.logger.LogError($"{ApiMsgs.UOW_COMMIT_ERROR} {ex}");
                throw;
            }
        }

        /// <summary>
        /// The get advantage user type.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private SyUserType GetAdvantageUserType(Guid userId)
        {
            return SimpleCache.GetOrAddExisting(
                $"{this.advantageDbContext.GetSimpleCacheConnectionString()}:AdvantageUserType:{userId}",
                () => this.advantageDbContext.GetRepositoryForEntity<SyUserType>().Get(x => x.SyUsers.Any(y => y.UserId == userId)).FirstOrDefault(),
                CacheExpiration.LONG);
        }

        /// <summary>
        /// The get token info.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="TokenInfo"/>.
        /// </returns>
        private TokenInfo GetTokenInfo(ActionExecutingContext context)
        {
            Claim[] userClaims = null;
            string tenant = string.Empty;
            string userName = string.Empty;

            if (context.HttpContext.Request.Query.ContainsKey("token"))
            {
                string token = context.HttpContext.Request.Query["token"];
                if (!string.IsNullOrEmpty(token))
                {
                    AuthServiceSettings authSettings = this.configuration.GetSection("AuthService").Get<AuthServiceSettings>();

                    try
                    {
                        string jwt = string.Empty;

                        if (token.ToLower().Contains("bearer "))
                        {
                            string[] tokenString = token.Split(' ');
                            token = tokenString[tokenString.Length - 1];
                        }

                        jwt = JWT.Decode(token, Encoding.ASCII.GetBytes(authSettings.SecretKey), JwsAlgorithm.HS256);

                        if (!string.IsNullOrEmpty(jwt))
                        {
                            AuthToken authorization = AuthToken.FromDictionary(JsonConvert.DeserializeObject<Dictionary<string, object>>(jwt));
                            if (authorization.Expiration >= DateTime.UtcNow)
                            {
                                AuthorizeReport authorizeReport =
                                    (AuthorizeReport)
                                    ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context
                                        .ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(AuthorizeReport), false).FirstOrDefault();

                                if (authorizeReport != null)
                                {
                                    if (authorization.Roles.Any(x => x == authorizeReport.Roles))
                                    {
                                        tenant = authorization.Tenant;
                                        userName = authorization.Subject;
                                    }
                                }
                                else
                                {
                                    context.Result = new CustomUnauthorizedResult("Authorize report attribute not found on method.");
                                }

                            }
                            else
                            {
                                context.Result = new CustomUnauthorizedResult("Token is expired.");
                            }
                        }
                        else
                        {
                            context.Result = new CustomUnauthorizedResult("Decoded token is empty.");
                        }
                    }
                    catch (Exception e)
                    {
                        context.Result = new CustomUnauthorizedResult($"Exception thrown : {e.Message}");
                    }
                }
                else
                {
                    context.Result = new CustomUnauthorizedResult("Token is empty.");
                }
            }
            else
            {
                userClaims = context.HttpContext.User.Claims.ToArray();

                Claim subClaim = userClaims.SingleOrDefault(x => x.Type == JwtClaims.USER_NAME_CLAIM);
                Claim tenantClaim = userClaims.SingleOrDefault(x => x.Type == JwtClaims.TENANT_CLAIM);

                tenant = tenantClaim?.Value;
                userName = subClaim?.Value;
            }

            return new TokenInfo
            {
                Tenant = tenant,
                UserName = userName
            };
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        private ApiUser GetUser(TokenInfo info)
        {
            try
            {
                ApiUser user = this.authService.GetUser(info.UserName, info.Tenant);

                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }


        /// <summary>
        /// The initialize report settings.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="tenantApiUser">
        /// The tenant Api User.
        /// </param>
        private void InitializeReportSettings(ActionExecutingContext context, ApiUser tenantApiUser)
        {
            Fame.EFCore.Interfaces.IRepository<SyConfigAppSetValues> appSettingValueRepository = this.advantageDbContext.GetRepositoryForEntity<SyConfigAppSetValues>();
            Fame.EFCore.Interfaces.IRepository<SyConfigAppSettings> appSettingRepository = this.advantageDbContext.GetRepositoryForEntity<SyConfigAppSettings>();

            string value = string.Empty;
            SyConfigAppSettings appSetting = appSettingRepository.Get(entity => entity.KeyName.ToLower() == ConfigurationAppSettingsKeys.AdvantageApiUrl)
                .FirstOrDefault();

            if (appSetting != null)
            {
                SyConfigAppSetValues appSettingValue = appSettingValueRepository.Get(entity => entity.SettingId.HasValue && entity.SettingId == appSetting.SettingId).FirstOrDefault();

                if (appSettingValue != null)
                {
                    value = appSettingValue.Value;
                }
            }

            this.reportConnection.InitializeReportConnection(value, context.HttpContext.Request.Headers["Authorization"], tenantApiUser.TenantServerName, tenantApiUser.TenantUser, tenantApiUser.Password, tenantApiUser.TenantDatabaseName);
        }

    }

    /// <summary>
    /// The token info.
    /// </summary>
    public class TokenInfo
    {
        /// <summary>
        /// Gets or sets the tenant.
        /// </summary>
        public string Tenant { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The is valid.
        /// </summary>
        public bool IsValid => !this.Tenant.IsNullOrEmpty() && !this.UserName.IsNullOrEmpty();
    }
}
