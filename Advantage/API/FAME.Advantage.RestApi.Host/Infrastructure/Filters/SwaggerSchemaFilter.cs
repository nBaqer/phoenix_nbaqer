﻿

namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Web.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;

    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;

    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    /// <summary>
    /// The swagger schema filter.
    /// </summary>
    public class SwaggerSchemaFilter : ISchemaFilter
    {
       

        /// <summary>
        /// The apply.
        /// </summary>
        /// <param name="schema">
        /// The schema.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Apply(Schema schema, SchemaFilterContext context)
        {
            var excludedProperties = context.SystemType.GetProperties().Where(t => t.GetCustomAttribute<SwaggerExcludeAttribute>() != null);
            foreach (var excludedProperty in excludedProperties)
            {
                var camelProp = char.ToLowerInvariant(excludedProperty.Name[0]) + excludedProperty.Name.Substring(1);
                if (schema.Properties.ContainsKey(camelProp))
                {
                    schema.Properties.Remove(camelProp);
                }
            }
        }

    }
}
