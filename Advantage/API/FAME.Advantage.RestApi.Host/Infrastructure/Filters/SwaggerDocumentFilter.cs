﻿

namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Extensions;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Abstractions;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.AspNetCore.Mvc.ApplicationModels;

    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    /// <summary>
    /// The swagger document filter.
    /// </summary>
    public class SwaggerDocumentFilter : IDocumentFilter
    {

        private readonly IApiDescriptionGroupCollectionProvider apiExplorer;

        /// <summary>
        /// Initializes a new instance of the <see cref="SwaggerDocumentFilter"/> class.
        /// </summary>
        /// <param name="apiExplorer">
        /// The api explorer.
        /// </param>
        public SwaggerDocumentFilter(IApiDescriptionGroupCollectionProvider apiExplorer)
        {
            this.apiExplorer = apiExplorer;
        }

        /// <summary>
        /// The apply.
        /// </summary>
        /// <param name="swaggerDoc">
        /// The swagger doc.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
        {
            var apiDescriptionGroups = this.apiExplorer.ApiDescriptionGroups.Items;
            foreach (var apiDescriptionGroup in apiDescriptionGroups)
            {
                foreach (var apiDescription in apiDescriptionGroup.Items)
                {
                    var test = apiDescription.ActionDescriptor.GetProperty<IActionResult>();
                    var test2 = apiDescription.ActionDescriptor.GetPropertyValue("MethodInfo");
                    var test3 = test2.GetPropertyValue("CustomAttributes");
                }
                var apiDescriptions = apiDescriptionGroup.Items.Where(
                    apiDescription =>
                        apiDescription.ActionDescriptor.GetType().GetCustomAttributes<SwaggerExcludeAttribute>().ToList().Count > 0);
                foreach (var apiDescription in apiDescriptions)
                {
                    var route = "/" + apiDescription.RelativePath.TrimEnd('/');
                    swaggerDoc.Paths.Remove(route);
                }
            }


        }
    }
}
