﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthorizeReport.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The authorize report attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The authorize report attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class AuthorizeReport : System.Attribute
    {
        /// <summary>
        /// The roles.
        /// </summary>
        private readonly string roles;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizeReport"/> class.
        /// </summary>
        /// <param name="roles">
        /// The roles.
        /// </param>
        public AuthorizeReport(string roles)
        {
            this.roles = roles;
        }

        /// <summary>
        /// The roles.
        /// </summary>
        public string Roles => this.roles;
    }
}
