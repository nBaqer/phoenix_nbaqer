﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidateModelAttribute.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the ValidateModelAttribute type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.RestApi.Host.Validations;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Filters;

    /// <summary>
    /// The validate model attribute.
    /// </summary>
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// The on action executing.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!DoesModelExist(context))
            {
                var validationErrors = this.GetAllValidationErrorsWithCustomMessage(context);

                context.Result = validationErrors.Any() ? new BadRequestObjectResult(validationErrors) : new BadRequestObjectResult(ApiMsgs.MALFORMED_MODEL_ERROR);

                return;
            }

            if (!IsModelValid(context))
            {
                context.Result = new BadRequestObjectResult(this.GetAllValidationErrorsWithCustomMessage(context));
            }
        }

        /// <summary>
        /// The get all validation errors with custom message.
        /// This functions will return a custom error message when the Validation Error fails to get a message from the model State.
        /// Default Error Message: Unable to parse FieldName field value. If field is an Id of type Guid, an empty Guid (Empty Guid) should be provided.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ValidationError> GetAllValidationErrorsWithCustomMessage(ActionExecutingContext context)
        {
            var contextValidationErrors = this.GetAllValidationErrors(context);
            foreach (var error in contextValidationErrors)
            {
                if (string.IsNullOrEmpty(error.Message))
                {
                    error.Message = $"Unable to parse {error.Field} field value. If field is an Id of type Guid, an empty Guid ({Guid.Empty.ToString()}) should be provided.";
                }
            }

            return contextValidationErrors;
        }


        /// <summary>
        /// The does model exist.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool DoesModelExist(ActionExecutingContext context)
        {
            if (context.HttpContext != null)
            {
                if (context.HttpContext.Request.Method.ToUpper() == "GET")
                {
                    return true;
                }
            }

            return context.ActionArguments.ContainsKey(ApiDefaults.INCOMING_MODEL_CONVENTION);
        }

        /// <summary>
        /// The is model valid.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsModelValid(ActionExecutingContext context)
        {
            return context.ModelState.IsValid;
        }

        /// <summary>
        /// The get all validation errors.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<ValidationError> GetAllValidationErrors(ActionExecutingContext context)
        {
            return context.ModelState.Keys.SelectMany(key =>
                context.ModelState[key].Errors
                    .Select(x => new ValidationError(key, x.ErrorMessage))).ToList();
        }
    }
}
