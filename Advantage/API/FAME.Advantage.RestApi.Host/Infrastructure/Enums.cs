﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Enums.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The system status level.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Infrastructure
{
    /// <summary>
    /// The system status level.
    /// </summary>
    public enum SystemStatusLevel
    {
        /// <summary>
        /// The lead.
        /// </summary>
        Lead = 1,

        /// <summary>
        /// The student enrollments.
        /// </summary>
        StudentEnrollments = 2,

        /// <summary>
        /// The student.
        /// </summary>
        Student = 3
    }

    /// <summary>
    /// The sys document statuses.
    /// </summary>
    public enum SysDocumentStatuses
    {
        /// <summary>
        /// The approved.
        /// </summary>
        Approved = 1,

        /// <summary>
        /// The not approved.
        /// </summary>
        NotApproved = 2
    }

    /// <summary>
    /// The sy academic calendar program.
    /// </summary>
    public enum SyAcademicCalendarProgram
    {
        /// <summary>
        /// The non standard term.
        /// </summary>
        NonStandardTerm = 1,

        /// <summary>
        /// The quarter.
        /// </summary>
        Quarter = 2,

        /// <summary>
        /// The semester.
        /// </summary>
        Semester = 3,

        /// <summary>
        /// The trimester.
        /// </summary>
        Trimester = 4,

        /// <summary>
        /// The clock hour.
        /// </summary>
        ClockHour = 5,

        /// <summary>
        /// The non term.
        /// </summary>
        NonTerm = 6
    }

    /// <summary>
    /// The SAP trigger units.
    /// </summary>
    public enum SAPTriggerUnits
    {
        /// <summary>
        /// The days.
        /// </summary>
        Days = 1,

        /// <summary>
        /// The weeks.
        /// </summary>
        Weeks = 2,

        /// <summary>
        /// The credits attempted.
        /// </summary>
        CreditsAttempted = 3,

        /// <summary>
        /// The actual hours.
        /// </summary>
        ActualHours = 4,

        /// <summary>
        /// The scheduled hours.
        /// </summary>
        ScheduledHours = 5,

        /// <summary>
        /// The months.
        /// </summary>
        Months = 6
    }

    /// <summary>
    /// The sap trigger offsets.
    /// </summary>
    public enum SAPTriggerOffsets
    {
        /// <summary>
        /// The start of term.
        /// </summary>
        StartOfTerm = 1,

        /// <summary>
        /// The end of term.
        /// </summary>
        EndOfTerm = 2,

        /// <summary>
        /// The start date.
        /// </summary>
        StartDate = 3
    }

    /// <summary>
    /// The sap quantitative units.
    /// </summary>
    public enum SAPQuantitativeUnits
    {

        /// <summary>
        /// The credits attempted quantitative unit.
        /// </summary>
        CreditsAttempted = 1,


        /// <summary>
        /// The courses attempted quantitative unit.
        /// </summary>
        CoursesAttempted = 2,


        /// <summary>
        /// The scheduled hours per day quantitative unit.
        /// </summary>
        ScheduledHoursPerDay = 3
    }

    /// <summary>
    /// The Title IV Statuses.
    /// </summary>
    public enum TitleIVStatuses
    {
        /// <summary>
        /// Title IV Passed id 1.
        /// </summary>
        Passed = 1,

        /// <summary>
        /// TTitle IV Warning id 2.
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Title IV Ineligible id 3.
        /// </summary>
        Ineligible = 3,

        /// <summary>
        /// Title IV Probation id 4.
        /// </summary>
        Probation = 4,
    }

    /// <summary>
    /// The grade book components.
    /// </summary>
    public enum GradeBookComponents
    {
        /// <summary>
        /// Home work.
        /// </summary>
        Homework = 499,

        /// <summary>
        /// Lab work.
        /// </summary>
        LabWork = 500,

        /// <summary>
        /// The Exams.
        /// </summary>
        Exam = 501,

        /// <summary>
        /// The Final.
        /// </summary>
        Final = 502,

        /// <summary>
        /// Lab hours.
        /// </summary>
        LabHours = 503,

        /// <summary>
        /// Practical Exams.
        /// </summary>
        PracticalExams = 533,

        /// <summary>
        ///  The externship.
        /// </summary>
        Externships = 544
    }

    /// <summary>
    /// The student statuses.
    /// </summary>
    public enum StudentStatuses
    {
        /// <summary>
        /// The future start.
        /// </summary>
        FutureStart = 7,

        /// <summary>
        /// The no start.
        /// </summary>
        NoStart = 8,

        /// <summary>
        /// The currently attending.
        /// </summary>
        CurrentlyAttending = 9,

        /// <summary>
        /// The leave of absence.
        /// </summary>
        LeaveOfAbsence = 10,

        /// <summary>
        /// The suspension.
        /// </summary>
        Suspension = 11,

        /// <summary>
        /// The dropped.
        /// </summary>
        Dropped = 12,

        /// <summary>
        /// The graduated.
        /// </summary>
        Graduated = 14,

        /// <summary>
        /// The transfer out.
        /// </summary>
        TransferOut = 19,

        /// <summary>
        /// The academic probation.
        /// </summary>
        AcademicProbation = 20,

        /// <summary>
        /// The externship.
        /// </summary>
        Externship = 22,

        /// <summary>
        /// The disciplinary probation.
        /// </summary>
        DisciplinaryProbation = 23,

        /// <summary>
        /// The warning probation.
        /// </summary>
        WarningProbation = 24
    }

    /// <summary>
    /// The in school student statuses.
    /// </summary>
    public enum InSchoolStudentStatuses
    {
        /// <summary>
        /// The currently attending.
        /// </summary>
        CurrentlyAttending = 9,

        /// <summary>
        /// The leave of absence.
        /// </summary>
        LeaveOfAbsence = 10,

        /// <summary>
        /// The suspension.
        /// </summary>
        Suspension = 11,

        /// <summary>
        /// The academic probation.
        /// </summary>
        AcademicProbation = 20,

        /// <summary>
        /// The externship.
        /// </summary>
        Externship = 22,

        /// <summary>
        /// The disciplinary probation.
        /// </summary>
        DisciplinaryProbation = 23,

        /// <summary>
        /// The warning probation.
        /// </summary>
        WarningProbation = 24
    }

    /// <summary>
    /// The academic calendar.
    /// </summary>
    public enum AcademicCalendar
    {
        /// <summary>
        /// The non standard term.
        /// </summary>
        NonStandardTerm = 1,

        /// <summary>
        /// The quarter.
        /// </summary>
        Quarter = 2,

        /// <summary>
        /// The semester.
        /// </summary>
        Semester = 3,

        /// <summary>
        /// The trimester.
        /// </summary>
        Trimester = 4,

        /// <summary>
        /// The clock hour.
        /// </summary>
        ClockHour = 5,

        /// <summary>
        /// The non term.
        /// </summary>
        NonTerm = 6
    }

    /// <summary>
    /// The file storage types.
    /// </summary>
    public enum FileStorageType
    {
        /// <summary>
        /// The cloud storage.
        /// </summary>
        Cloud = 1,

        /// <summary>
        /// The file storage
        /// </summary>
        File,

        /// <summary>
        /// The network storage
        /// </summary>
        Network
    }

    /// <summary>
    /// The FileConfigurationFeatures enum
    /// </summary>
    public enum FileConfigurationFeature
    {
        ///  Timeclock ///
        TC = 1,
        ///  Lead Import ///
        LI,
        ///  Student Docs ///
        DOC,
        ///  Photos ///
        PHOTO,
        ///  R2T4 ///
        R2T4
    }
}
