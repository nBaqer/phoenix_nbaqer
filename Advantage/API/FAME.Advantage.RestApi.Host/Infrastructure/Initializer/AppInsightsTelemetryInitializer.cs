﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppInsightsTelemetryInitializer.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AppInsightsTelemetryInitializer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Initializer
{
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Orm.AdvTenant.Domain.Entities;
    using Jose;
    using Microsoft.ApplicationInsights.Channel;
    using Microsoft.ApplicationInsights.DataContracts;
    using Microsoft.ApplicationInsights.Extensibility;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The app insights telemetry initializer.
    /// </summary>
    public class AppInsightsTelemetryInitializer : ITelemetryInitializer
    {
        /// <summary>
        /// The http context accessor
        /// </summary>
        IHttpContextAccessor httpContextAccessor;

        /// <summary>
        /// The configuration.
        /// </summary>
        private readonly IConfigurationRoot configuration;

        /// <summary>
        /// The auth service.
        /// </summary>
        private readonly IAuthService authService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppInsightsTelemetryInitializer"/> class.
        /// </summary>
        /// <param name="httpContextAccessor">
        /// The http content accessor.
        /// </param>
        /// <param name="authService">
        /// The auth service.
        /// </param>
        /// <param name="configuration">
        /// The configuration root.
        /// </param>
        public AppInsightsTelemetryInitializer(IHttpContextAccessor httpContextAccessor, IConfigurationRoot configuration, IAuthService authService)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.configuration = configuration;
            this.authService = authService;
        }

        /// <summary>
        /// The initialize.
        /// </summary>
        /// <param name="telemetry">
        /// The telemetry.
        /// </param>
        public void Initialize(ITelemetry telemetry)
        {
            try
            {
                if (!(telemetry is RequestTelemetry reqTelemetry))
                {
                    return;
                }

                ISupportProperties requestTelemetry = (ISupportProperties)telemetry;

                if (!reqTelemetry.Properties.ContainsKey("Advantage_App"))
                    requestTelemetry.Properties.Add("Advantage_App", "API");
                else
                    requestTelemetry.Properties["Advantage_App"] = "API";

                TokenInfo tokenInfo = this.ParseToken(this.httpContextAccessor.HttpContext.Request);

                if (!tokenInfo.IsValid)
                {
                    return;
                }

                ApiUser user = this.GetUser(tokenInfo);
                TenantUser tenantUser = new TenantUser(
                        user.UserId,
                        user.IsSupportUser,
                        user.Email,
                        user.TenantName,
                        null);

                if (tenantUser != null)
                {
                    //   'User ID'
                    if (tenantUser.Id != Guid.Empty)
                    {
                        telemetry.Context.User.Id = tenantUser?.Id.ToString() ?? "";
                        telemetry.Context.User.AuthenticatedUserId = tenantUser?.Id.ToString() ?? "";
                    }

                    if (!requestTelemetry.Properties.ContainsKey("tenant_Name"))
                    {
                        requestTelemetry.Properties.Add("tenant_Name", tenantUser.TenantName);
                    }
                    else
                    {
                        requestTelemetry.Properties["tenant_Name"] = tenantUser.TenantName;
                    }

                    if (!requestTelemetry.Properties.ContainsKey("tenant_UserId"))
                    {
                        requestTelemetry.Properties.Add("tenant_UserId", tenantUser.Id.ToString());
                    }
                    else
                    {
                        requestTelemetry.Properties["tenant_UserId"] = tenantUser.Id.ToString();
                    }

                    if (!requestTelemetry.Properties.ContainsKey("tenant_IsSupport"))
                    {
                        requestTelemetry.Properties.Add("tenant_IsSupport", tenantUser.IsSupportUser.ToString());
                    }
                    else
                    {
                        requestTelemetry.Properties["tenant_IsSupport"] = tenantUser.IsSupportUser.ToString();
                    }


                    if (!requestTelemetry.Properties.ContainsKey("tenant_UserType"))
                    {
                        requestTelemetry.Properties.Add("tenant_UserType", tenantUser.UserType);
                    }
                    else
                    {
                        requestTelemetry.Properties["tenant_UserType"] = tenantUser.UserType;
                    }
                }


            }
            catch (Exception e)
            {
                //log error, something went wrong when logging telemetry
                Exception a = e;
            }
        }

        public TokenInfo ParseToken(HttpRequest request)
        {
            try
            {
                Claim[] userClaims = null;
                TokenInfo tokenInfo = new TokenInfo();
                string tenant = string.Empty;
                string userName = string.Empty;
                string token = null;

                if (this.httpContextAccessor.HttpContext.Request.Query.ContainsKey("token"))
                    token = this.httpContextAccessor.HttpContext.Request.Query["token"];

                if (!string.IsNullOrEmpty(token))
                {
                    AuthServiceSettings authSettings = this.configuration.GetSection("AuthService").Get<AuthServiceSettings>();

                    string jwt = string.Empty;

                    if (token.ToLower().Contains("bearer "))
                    {
                        string[] tokenString = token.Split(' ');
                        token = tokenString[tokenString.Length - 1];
                    }

                    jwt = JWT.Decode(token, Encoding.ASCII.GetBytes(authSettings.SecretKey), JwsAlgorithm.HS256);

                    if (!string.IsNullOrEmpty(jwt))
                    {
                        AuthToken authorization = AuthToken.FromDictionary(JsonConvert.DeserializeObject<Dictionary<string, object>>(jwt));
                        tenant = authorization.Tenant;
                        userName = authorization.Subject;

                    }
                }
                else
                {
                    userClaims = this.httpContextAccessor.HttpContext.User.Claims.ToArray();

                    Claim subClaim = userClaims.SingleOrDefault(x => x.Type == JwtClaims.USER_NAME_CLAIM);
                    Claim tenantClaim = userClaims.SingleOrDefault(x => x.Type == JwtClaims.TENANT_CLAIM);

                    tenant = tenantClaim?.Value;
                    userName = subClaim?.Value;
                }

                return new TokenInfo
                {
                    Tenant = tenant,
                    UserName = userName
                };
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="info">
        /// The info.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        private ApiUser GetUser(TokenInfo info)
        {
            try
            {
                ApiUser user = this.authService.GetUser(info.UserName, info.Tenant);

                return user;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
