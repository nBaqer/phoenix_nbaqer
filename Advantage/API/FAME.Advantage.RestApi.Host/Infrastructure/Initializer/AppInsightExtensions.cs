﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppInsightExtensions.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AppInsightExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.ApplicationInsights;

    /// <summary>
    /// The app insights extensions class.
    /// </summary>
    public static class AppInsightExtensions
    {
        /// <summary>
        /// Extension method that when executing begins tracking the exception via application insights
        /// </summary>
        /// <param name="e">
        /// The exception itself
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        public static void TrackException(this Exception e)
        {
            if (e != null)
            {
                TelemetryClient t = new TelemetryClient();
                t.TrackException(e);
            }
        }
    }
}
