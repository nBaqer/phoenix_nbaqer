﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GoogleOAuthSettings.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the GoogleOAuthSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The google o auth settings.
    /// </summary>
    public class GoogleOAuthSettings
    {
        /// <summary>
        /// Gets or sets the gmail o auth path.
        /// </summary>
        public string Path { get; set; }
    }
}
