﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportServerCredentials.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ReportServerCredentials interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The ReportServerCredentials interface.
    /// </summary>
    public interface IReportServerCredentials
    {
        /// <summary>
        /// Gets or sets a value indicating whether is impersonation enabled.
        /// </summary>
        bool IsImpersonationEnabled { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        string Domain { get; set; }
    }
}
