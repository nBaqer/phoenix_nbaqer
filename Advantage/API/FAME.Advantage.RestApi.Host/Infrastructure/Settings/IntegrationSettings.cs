﻿
namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Integration settings class
    /// </summary>
    public class IntegrationSettings : IIntegrationSettings
    {
        /// <summary>
        /// The maximum number of retry counts
        /// </summary>
        public int MaximumRetryCount { get; set; }
    }
}
