﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportServerSettings.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ReportServerSettings interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The ReportServerSettings interface.
    /// </summary>
    public interface IReportServerSettings
    {
        /// <summary>
        /// Gets or sets the report server url.
        /// </summary>
        string ReportServerUrl { get; set; }

        /// <summary>
        /// Gets or sets the report server folder.
        /// </summary>
        string ReportServerFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is connection secured to reporting services.
        /// </summary>
        bool IsConnectionSecuredToReportingServices { get; set; }

        /// <summary>
        /// Gets or sets the credentials.
        /// </summary>
        ReportServerCredentials Credentials { get; set; }

        /// <summary>
        /// Gets or sets the max report output size when is generated from Report Service. 
        /// Default is 104857600 bytes (100 MB)
        /// </summary>
        long? MaxReportOutputSize { get; set; }

        /// <summary>
        /// Gets or sets the format language. Example: en-US
        /// </summary>
        string FormatLanguge { get; set; }

        /// <summary>
        /// Gets or sets the Debug Mode (if true exceptions will be raised instead of user friendly message)
        /// </summary>
        bool EnableDebugMode { get; set; }
    }
}
