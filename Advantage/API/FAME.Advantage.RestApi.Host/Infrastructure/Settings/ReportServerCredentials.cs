﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportServerCredentials.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ReportServerCredentials type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The Report Server Credentials.
    /// </summary>
    public class ReportServerCredentials : IReportServerCredentials
    {
        /// <summary>
        /// Gets or sets a value indicating whether is impersonation enabled.
        /// </summary>
        public bool IsImpersonationEnabled { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        public string Domain { get; set; }
    }
}
