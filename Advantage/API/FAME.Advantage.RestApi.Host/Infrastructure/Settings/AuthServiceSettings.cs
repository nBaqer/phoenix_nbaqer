﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthServiceSettings.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the AuthServiceSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The auth service settings.
    /// </summary>
    public class AuthServiceSettings
    {
        /// <summary>
        /// Gets or sets the secret key.
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// Gets or sets the token lifetime.
        /// </summary>
        public int TokenLifetime { get; set; }
    }
}
