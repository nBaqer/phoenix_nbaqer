﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// Integration settings interface
    /// </summary>
    public interface IIntegrationSettings
    {
        /// <summary>
        /// The maximum number of retry counts
        /// </summary>
        int MaximumRetryCount { get; set; }
    }
}
