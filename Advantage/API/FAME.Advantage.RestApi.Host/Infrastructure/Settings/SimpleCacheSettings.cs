﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SimpleCacheSettings.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the SimpleCacheSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The simple cache settings.
    /// </summary>
    public class SimpleCacheSettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether enable.
        /// </summary>
        public bool Enable { get; set; }
    }
}
