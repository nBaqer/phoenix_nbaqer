﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportServerSettings.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ReportServerSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    /// <summary>
    /// The report server settings.
    /// </summary>
    public class ReportServerSettings : IReportServerSettings
    {
        /// <summary>
        /// Gets or sets the report server url.
        /// </summary>
        public string ReportServerUrl { get; set; }

        /// <summary>
        /// Gets or sets the report server folder.
        /// </summary>
        public string ReportServerFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is connection secured to reporting services.
        /// </summary>
        public bool IsConnectionSecuredToReportingServices { get; set; }

        /// <summary>
        /// Gets or sets the credentials.
        /// </summary>
        public ReportServerCredentials Credentials { get; set; }

        /// <summary>
        /// Gets or sets the max report output size when is generated from Report Service. 
        /// Default is 104857600 bytes (100 MB)
        /// </summary>
        public long? MaxReportOutputSize { get; set; }


        /// <summary>
        /// Gets or sets the format language. Example: en-US
        /// </summary>
        public string FormatLanguge { get; set; }

        /// <summary>
        /// Gets or sets the Debug Mode (if true exceptions will be raised instead of user friendly message)
        /// </summary>
        public bool EnableDebugMode { get; set; }
    }
}
