﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantDbSettings.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the TenantDbSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Settings
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The tenant db settings.
    /// </summary>
    public class TenantDbSettings
    {
        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// </summary>
        public string Database { get; set; }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Gets or sets the pass.
        /// </summary>
        public string Pass { get; set; }

        /// <summary>
        /// Gets or sets the timeout.
        /// </summary>
        public string Timeout { get; set; }

        /// <summary>
        /// Gets or sets the multiple active result sets.
        /// </summary>
        public string MultipleActiveResultSets { get; set; } = "True";

        /// <summary>
        /// The build connection string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string BuildConnectionString()
        {
            return $"Server={this.Server};Database={this.Database};UID={this.User};PWD={this.Pass};Connect Timeout={this.Timeout};MultipleActiveResultSets={this.MultipleActiveResultSets};";
        }

        /// <summary>
        /// The build connect context options builder.
        /// </summary>
        /// <returns>
        /// The <see cref="DbContextOptionsBuilder"/>.
        /// </returns>
        public DbContextOptionsBuilder BuildConnectContextOptionsBuilder()
        {
            var option = new DbContextOptionsBuilder();
            option.UseSqlServer(this.BuildConnectionString());
            return option;
        }
    }
}
