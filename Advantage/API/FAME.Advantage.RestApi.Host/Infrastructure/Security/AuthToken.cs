﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthToken.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the AuthToken type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Security
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;

    using FAME.Advantage.RestApi.Host.Extensions;

    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The auth token.
    /// </summary>
    public class AuthToken
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthToken"/> class.
        /// </summary>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <param name="issuer">
        /// The issuer.
        /// </param>
        /// <param name="audience">
        /// The audience.
        /// </param>
        /// <param name="created">
        /// The created.
        /// </param>
        /// <param name="expiration">
        /// The expiration.
        /// </param>
        /// <param name="roles">
        /// The roles.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public AuthToken(string subject, string issuer, string audience, DateTime created, DateTime expiration, List<string> roles, string userId, string tenant)
        {
            this.Subject = subject;
            this.Issuer = issuer;
            this.Audience = audience;
            this.TimeCreated = created;
            this.Expiration = expiration;
            this.Roles = roles;
            this.UserId = userId;
            this.Tenant = tenant;
        }

        /// <summary>
        /// Gets or sets the subject.
        /// </summary>
        public string Subject { get; protected set; }

        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        public string Issuer { get; protected set; }

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        public string Audience { get; protected set; }

        /// <summary>
        /// Gets or sets the time created.
        /// </summary>
        public DateTime TimeCreated { get; protected set; }

        /// <summary>
        /// Gets or sets the expiration.
        /// </summary>
        public DateTime Expiration { get; protected set; }

        /// <summary>
        /// Gets or sets the roles.
        /// </summary>
        public List<string> Roles { get; protected set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public string UserId { get; protected set; }

        /// <summary>
        /// Gets or sets the tenant.
        /// </summary>
        public string Tenant { get; protected set; }

        /// <summary>
        /// The from dictionary.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <returns>
        /// The <see cref="AuthToken"/>.
        /// </returns>
        public static AuthToken FromDictionary(IDictionary<string, object> payload)
        {
            var subject = payload[JwtClaims.SUB_CLAIM] is string ? payload[JwtClaims.SUB_CLAIM].ToString() : string.Empty;
            var issuer = payload[JwtClaims.ISS_CLAIM] is string ? payload[JwtClaims.ISS_CLAIM].ToString() : string.Empty;
            var audience = payload[JwtClaims.AUD_CLAIM] is string ? payload[JwtClaims.AUD_CLAIM].ToString() : string.Empty;
            
            var roles = ParseRoles(payload);
            var userId = payload[JwtClaims.USER_ID_CLAIM] is string ? payload[JwtClaims.USER_ID_CLAIM].ToString() : string.Empty;
            var tenant = payload[JwtClaims.TENANT_CLAIM] is string ? payload[JwtClaims.TENANT_CLAIM].ToString() : string.Empty;

            var timeCreated = payload[JwtClaims.IAT_CLAIM] as long? ?? 0;
            var expirationTimestamp = payload[JwtClaims.EXP_CLAIM] as long? ?? 0;
            
            return new AuthToken(subject, issuer, audience, timeCreated.ToDateTime(), expirationTimestamp.ToDateTime(), roles, userId, tenant);
        }


        /// <summary>
        /// The to dictionary.
        /// </summary>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public Dictionary<string, object> ToDictionary()
        {
            return new Dictionary<string, object>
            {
                {
                    JwtClaims.SUB_CLAIM, this.Subject 
                },
                {
                    JwtClaims.ISS_CLAIM, this.Issuer 
                },
                {
                    JwtClaims.AUD_CLAIM, this.Audience
                },
                {
                    JwtClaims.IAT_CLAIM, this.TimeCreated.ToTimestamp()
                },
                {
                    JwtClaims.EXP_CLAIM, this.Expiration.ToTimestamp()
                },
                {
                    JwtClaims.ROLE_CLAIM, this.Roles
                },
                {
                    JwtClaims.TENANT_CLAIM, this.Tenant
                },
                {
                    JwtClaims.USER_ID_CLAIM, this.UserId
                }
            };
        }

        /// <summary>
        /// The parse roles.
        /// </summary>
        /// <param name="payload">
        /// The payload.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private static List<string> ParseRoles(IDictionary<string, object> payload)
        {
            try
            {
                var payloadRoles = (JArray)payload[JwtClaims.ROLE_CLAIM];
                List<string> roles = new List<string>();
                for (int index = 0; index <= payloadRoles.Count - 1; index++)
                {
                    roles.Add(payloadRoles[index].Value<string>());
                }

                return roles;
            }
            catch
            {
                return new List<string> { JwtClaims.NO_ROLES };
            }
        }
    }
}
