﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITenantUser.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the ITenantUser type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces
{
    using System;

    /// <summary>
    /// The TenantUser interface.
    /// </summary>
    public interface ITenantUser
    {
        /// <summary>
        /// Gets  the id.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets a value indicating whether is support user.
        /// </summary>
        bool IsSupportUser { get; }

        /// <summary>
        /// Gets  the email.
        /// </summary>
        string Email { get; }

        /// <summary>
        /// Gets the tenant name.
        /// </summary>
        string TenantName { get; }

        /// <summary>
        /// Gets the user type.
        /// </summary>
        string UserType { get; }
    }
}
