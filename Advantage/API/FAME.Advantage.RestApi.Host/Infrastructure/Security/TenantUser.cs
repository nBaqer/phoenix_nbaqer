﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantUser.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TenantUser type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Security
{
    using System;

    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    
    /// <summary>
    /// The tenant user.
    /// </summary>
    public class TenantUser : ITenantUser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TenantUser"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="isSupportUser">
        /// The is support user.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="tenantName">
        /// The tenant Name.
        /// </param>
        /// <param name="userType">
        /// The user Type.
        /// </param>
        public TenantUser(Guid id, bool isSupportUser, string email, string tenantName, string userType)
        {
            this.Id = id;
            this.Email = email;
            this.IsSupportUser = isSupportUser;
            this.TenantName = tenantName;
            this.UserType = userType;
        }

        /// <summary>
        /// Gets the id.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Gets a value indicating whether is support user.
        /// </summary>
        public bool IsSupportUser { get; }

        /// <summary>
        /// Gets the email
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Gets the Tenant Name
        /// </summary>
        public string TenantName { get; }

        /// <summary>
        /// Gets the user type.
        /// </summary>
        public string UserType { get; }
    }
}
