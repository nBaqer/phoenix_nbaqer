﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadStatus type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Infrastructure
{
    /// <summary>
    /// The lead status.
    /// </summary>
    internal static class LeadStatus
    {
        /// <summary>
        /// The imported.
        /// </summary>
        public const string Imported = "Imported";

        /// <summary>
        /// The enrolled
        /// </summary>
        public const string Enrolled = "Enrolled";
    }

    /// <summary>
    /// The System Resources.
    /// </summary>
    internal static class SystemResources
    {
        /// <summary>
        /// The resourceId for Program Version Page.
        /// </summary>
        public const int ProgramVersionPage = 61;
    }

    /// <summary>
    /// The Access Level.
    /// </summary>
    internal static class AccessLevel
    {
        /// <summary>
        /// The resourceId for Program Version Page.
        /// </summary>
        public const int FullAccess = 15;
    }

    /// <summary>
    /// The system status - Active or Inactive.
    /// </summary>    
    internal static class SystemStatusCodes
    {
        /// <summary>
        /// The active.
        /// </summary>
        public const string Active = "A";

        /// <summary>
        /// The inactive.
        /// </summary>
        public const string Inactive = "I";
    }

    internal static class CacheExpiration
    {
        /// <summary>
        /// The very short will give an expiration that last 2 minutes.
        /// </summary>
        public const int VERY_SHORT = 120; // 120 sec

        /// <summary>
        /// The short will give an expiration that last 10 minutes.
        /// </summary>
        public const int SHORT = 600; // 60 sec * 10 min

        /// <summary>
        /// The medium will give an expiration that last 1 hour
        /// </summary>
        public const int MEDIUM = 3600; // 60 sec * 60 min

        /// <summary>
        /// The long will give an expiration that last 2 hours
        /// </summary>
        public const int LONG = 7200; // 60 sec * 60 min * 2 hours

        /// <summary>
        /// The very long will give an expiration that last 8 hours.
        /// </summary>
        public const int VERY_LONG = 28800; // 60 sec * 60 min * 8 hours
    }

    internal static class SwashbuckleInfo
    {
        public const string DOCUMENT_TITLE = "Advantage API";
        public const string DOCUMENT_DESCRIPTION = "Welcome to the Advantage API....";
        public const string DOCUMENT_VERSION_V1 = "v1";
        public const string DOCUMENT_VERSION_V2 = "v2";

        public const string TERMS_OF_SERVICE =
            @"Advantage API Terms of Use can be found at: https://support.fameinc.com/hc/en-us/articles/115001146071.";

        public const string CONTACT_EMAIL = "support@fameinc.com";
        public const string CONTACT_NAME = "FAME Development Team";
        public const string CONTACT_URL = "http://fameinc.com";
        public const string AUTH_NBP_NAME = "Authorization";
        public const string AUTH_NBP_IN = "header";
        public const string AUTH_NBP_DESCRIPTION = "Example: bearer [put token string here]";
        public const bool AUTH_NBP_REQUIRED = true;
        public const string AUTH_NBP_TYPE = "string";
    }

    internal static class JwtClaims
    {
        public const string SUB_CLAIM = "sub";
        public const string ISS_CLAIM = "iss";
        public const string AUD_CLAIM = "aud";
        public const string IAT_CLAIM = "iat";
        public const string EXP_CLAIM = "exp";
        public const string ROLE_CLAIM = "role";
        public const string USER_ID_CLAIM = "uid";
        public const string TENANT_CLAIM = "tenant";
        public const string NO_ROLES = "No Roles";
        public const string USER_NAME_CLAIM = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";

    }

    internal static class WapiSettingCodes
    {
        public const string TC_FILEWATCH_SERVICE = "TC_FILEWATCH_SERVICE";
        public const string ADVANTAGE_API = "ADVANTAGE_API";
    }

    internal static class ApiUserRights
    {
        public const string READ = "Read";
        public const string CREATE = "Create";
        public const string MODIFY = "Modify";
        public const string DELETE = "Delete";
        public const string ADMIN = "Admin";
    }
    internal static class ApiDefaults
    {
        public const string INCOMING_MODEL_CONVENTION = "model";
    }

    internal static class ServiceDefaults
    {
        public const string DEFAULT_PAGE_PARAM_NAME = "page";
        public const string DEFAULT_SORT_PARAM_NAME = "sort";
        public const int RESULTS_PER_PAGE = 25;
        public const string SORT_DESC = " desc";

        /// <summary>
        /// The authorization.
        /// </summary>
        public const string AUTHORIZATION = "Authorization";
    }

    internal static class ApiMsgs
    {
        public const string RESULT_DETAILS_NOT_FOUND = "Result details not found for enrollment : {0}.";
        public const string FAILED_COURSE_DETAIL_MESSAGE = "The student has failed {0} courses within the payment period. Please specify the total days required to complete these courses.";
        public const string FAILED_COURSE_NOT_FOUND_MESSAGE = "No failed course found in withdrawal period.";
        public const string COURSES_NOT_FOUND = "Courses not found.";
        public const string NO_FAILED_COURSE_FOUND = "No failed course found for the given enrollment.";
        public const string PAYMENT_PERIOD_START_END_DATE_INCORRECT = "Payment Period start date is incorrect.";
        public const string PERIOD_OF_ENROLLMENT_START_END_DATE_INCORRECT = "Period of Enrollment start date is incorrect.";
        public const string SCHEDULE_START_DATE_NOT_FOUND = "Schedule start date not found.";
        public const string SELF_PACED_PROGRAM = "This is a self paced program.";
        public const string UNSUCCESSFUL_LOGIN = "Unsuccessful login attempt:";
        public const string UNAUTHORIZED_LOGIN = "Unauthorized exception.";
        public const string MISSING_REQUIRED_PARAMS_ERROR = "Missing required parameter:";
        public const string UOW_CONTAINER_ERROR = "Error getting UnitOfWork from Container. Error details:\n\n";
        public const string UOW_COMMIT_ERROR = "Error committing UnitOfWork. Error details:\n\n";
        public const string MALFORMED_MODEL_ERROR = "Missing or malformed model data.";
        public const string DELETE_RECORD_NOT_FOUND = "Record was not found. Delete command terminated. ID:";
        public const string DELETE_VERIFICATION_FAILED = "Delete verification failed. Delete command terminated ID:";
        public const string DELETE_SUCCESSFUL = "Record deletion was successful.";
        public const string DELETE_FAILED = "Delete failed. ID:";
        public const string UPDATE_IDS_NO_MATCH = "The model id does not match the id parameter from the route IDs:";
        public const string TOKEN_SUCCESS = "Login Successful.";
        public const string CREATE_TOKEN_FAILURE = "Could not create token.";
        public const string VALIDATE_TOKEN_FAILURE = "Could not validate token.";
        public const string PASSWORD_FAILURE = "Incorrect password attempt for user:";
        public const string USER_RIGHTS_FAILURE = "Failed to get Advantage User Rights:";
        public const string USER_NOT_FOUND = "Username not found.";
        public const string ERROR_ON_ERROR_HANDLER = "An exception was thrown attempting to execute the error handler.";
        public const string STATE_CODE_INVALID = "State Code must be 2 characters.";
        public const string UPDATE_SUCCESS = "Record updated successfully.";
        public const string UPDATE_UNSUCCESSFULL = "Failed updating Record.";
        public const string SAVE_SUCCESS = "Record saved successfully.";
        public const string SAVE_UNSUCCESSFULL = "Failed saving Record.";
        public const string NOT_FOUND = "Record Not Found.";
        public const string UNDO_TERMINATION_SUCCESS = "Undo termination done successfully.";
        public const string NOT_CLOCK_HOUR_PROGRAM = "Enrollment is not a clock hour program.";
        public const string NOT_CREDIT_HOUR_PROGRAM = "Enrollment is not a credit hour program.";
        public const string NOT_STANDARD_PROGRAM = "The enrolled program is not a standard term program.";
        public const string WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL = "Withdrawal payment period calculated successfully.";
        public const string WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_FAILED = "Withdrawal payment period calculation failed.";
        public const string PAYMENT_PERIOD_DETAILS_NOT_FOUND = "Payment period details not found.";
        public const string EXCUSED_ABS_FOR_PER_OF_ENROLL_CALC_SUCCESSFUL = "Excused absence for period of enrollment(s) calculated successfully.";
        public const string WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL = "Withdrawal period of enrollment calculated successfully.";
        public const string WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_FAILED = "Withdrawal period of enrollment calculation failed.";
        public const string PERIOD_OF_ENROLLMENT_DETAILS_NOT_FOUND = "Period of enrollment details not found.";
        public const string INVALID_START_WITHDRAWAL_DATE = "Start or withdrawal date is invalid";
        public const string RECORD_NOT_FOUND_IN_TENANT_USERS_FOR_TENANT = "Failed to Reset the Password due to server error.";
        public const string COMPLETED_TOTAL_DAYS_SUCCESSFULL = "Completed and total days calculated successfully.";
        public const string SCHEDULE_TOTAL_PE_HOURS_CALCULATED_SUCCESSFUL =
            "Scheduled hours to complete and total hours in period of enrollment calculated successfully.";
        public const string SCHEDULE_TOTAL_PP_HOURS_CALCULATED_SUCCESSFUL =
            "Scheduled hours to complete and total hours in payment period calculated successfully.";
        public const string SCHEDULE_BREAKS_CALCULATED_SUCCESSFUL =
            "Scheduled breaks calculated successfully.";

        public const string PROGRAM_TYPE_NOT_FOUND = "The program is not a non standard term or programs with terms of substantially not equal in length";
        public const string COMPLETED_DAYS_CALCULATED_SUCCESSFUL = "Completd and total days calculated successfully.";
        public const string PROGRAM_NOT_SUBSTANTIALLY_EQUAL = "The non standard term program is substantially equal in length";
        public const string TOTAL_DAYS_FOR_NONTERM_NOTSELFPACED_PGM = "Total Days for non-term, not self paced program calculated successfully.";
        public const string REQUIRED_ENDDATE_MESSAGE = "Please enter the withdrawal period end date and try again.";
        public const string CAMPUS_PROGRAM_VERSION_NOT_FOUND = "Campus program version not found.";
        public const string INVALID_START_OR_END_DATE = "Start or end date are invalid";
        public const string UNABLE_TO_RETRIEVE_9010_TYPES = "Unable to retrieve 9010 award types";
        public const string UNABLE_TO_RETRIEVE_9010_MAPPINGS = "Unable to retrieve 9010 mappings for campus";
        public const string UNABLE_TO_UPDATE_9010_MAPPINGS = "Unable to update 9010 mappings for campus";

        public const string UNABLE_TO_RETRIEVE_ACCREDITING_AGENCIES = "Unable to retrieve accrediting agencies.";
        public const string UNABLE_TO_ADJUST_SCHED_HOURS = "Unable to adjust scheduled hours.";


        /// <summary>
        /// The active import status not found.
        /// </summary>
        public const string ActiveImportStatusNotFound = "An active import status cannot be found in Advantage";

        /// <summary>
        /// Cannot update since there are enrollments tied to the given program version.
        /// </summary>
        public const string EnrollmentExistsForPV = "Cannot save Registration Type since there are enrollments tied to the given program version.";

        /// <summary>
        /// Cannot update since there are enrollments tied to teh given program version.
        /// </summary>
        public const string SaveAccessPV = "Cannot save Registration Type since the user do not have full permissions.";

        /// <summary>
        /// The lead status not found for campus.
        /// </summary>
        public const string LeadStatusNotFoundForCampus = "The lead status could not be found for the specified campus";

        /// <summary>
        /// The lead status cannot be enrolled.
        /// </summary>
        public const string LeadStatusCannotBeEnrolled = "The lead status cannot be an \"Enrolled\" status";

        /// <summary>
        /// The lead status cannot be enrolled.
        /// </summary>
        public const string LeadIsEnrolled = "The lead is already enrolled.";

        /// <summary>
        /// The lead address only one mailing.
        /// </summary>
        public const string LeadAddressOnlyOneMailing =
            "Only one address can be marked as the mailing address for this lead.";

        /// <summary>
        /// The lead address only one mailing.
        /// </summary>
        public const string LeadAddressOnlyOneShownOnLeadPage =
            "Only one address can be shown on lead page for this lead.";

        /// <summary>
        /// The lead email no additions to lead page.
        /// </summary>
        public const string LeadEmailNoRoomOnLeadPage =
            "Additional emails cannot be shown on the lead info page for this lead.";

        /// <summary>
        /// The lead email only one preferred email.
        /// </summary>
        public const string LeadEmailOnlyOnePreferredEmail =
            "Only one email can be marked as preferred for this lead.";

        /// <summary>
        /// The lead phone no additions to lead page.
        /// </summary>
        public const string LeadPhoneNoRoomOnLeadPage =
            "Additional phones cannot be shown on the lead info page for this lead.";

        /// <summary>
        /// The lead email only one preferred email.
        /// </summary>
        public const string LeadPhoneOnlyOneBestPhone =
            "Only one phone can be marked as best for this lead.";

        /// <summary>
        /// The source category was not supplied.
        /// </summary>
        public const string SourceCategoryNotSupplied = "The source category must be supplied when the source type is given.";

        /// <summary>
        /// The source does not match the source category.
        /// </summary>
        public const string SourceTypeDoesNotBelongToSourceCategory = "The source type does not belong to the source category for the given campus.";

        /// <summary>
        /// The active import status not found.
        /// </summary>
        public const string ActiveStatusNotFound = "An active status cannot be found in Advantage";

        public const string SAVE_STUDENT_TERMINATION_SUCCESS = "All the student termination details specified is saved";
        public const string SAVE_STUDENT_TERMINATION_UNSUCCESS = "Failed saving student termination details";
        public const string PAYMENT_PERIOD = "Payment Period";
        public const string PERIOD_USED_NOT_POE = "Period used for calculation is not a period of enrollment.";
        public const string PERIOD_USED_NOT_PP = "Period used for calculation is not a payment period.";
        public const string FAILED_GETTING_RECORD = "Failed getting Record";
        public const string OVERRIDE_R2T4RESULTS_NOT_SUPPORT_USER = "Failed saving record, only support user can override R2T4 Results.";
        public const string APPROVE_STUDENT_TERMINATION_SUCCESS = "Student is terminated from the specified enrollment";
        public const string APPROVE_STUDENT_TERMINATION_UNSUCCESS = "Failed terminating student.";
        public const string SAVE_STUDENT_STATUS_CHANGES_SUCCESS = "Student status changes is saved successfully.";
        public const string SAVE_STUDENT_STATUS_CHANGES_UNSUCCESS = "Failed saving student status changes.";
        public const string SAVE_STUDENT_DOCUMENT_DETAILS_UNSUCCESS = "Failed saving student document details.";
        public const string SAVE_STUDENT_DOCUMENT_DETAILS_SUCCESS = "Student termination document details is saved successfully.";
        public const string SAVE_STUDENT_DOCUMENT_HISTORY_UNSUCCESS = "Failed saving student document history.";
        public const string SAVE_STUDENT_DOCUMENT_HISTORY_SUCCESS = "Student termination document history is saved successfully.";
        public const string PAYMENT_PERIOD_CALCULATION_SUCCESS = "Payment period calculated successfully.";
        public const string PAYMENT_PERIOD_LENGTH_GREATER = "Program length is greater than the academic year length with remaining period equal to or less than half an academic year.";
        public const string PAYMENT_PERIOD_LENGTH_GREATER_AND_REMAINING_GREATER = "Program length is greater than the academic year length with remaining period greater than half an academic year.";
        public const string PAYMENT_PERIOD_LENGTH_GREATER_THAN_ACADEMIC_YEAR = "Program length is greater than the academic year length.";
        public const string ACADEMIC_PERIOD_ZERO_LENGTH = "Academic year length is: 0.00";
        public const string ENROLLMENT_LENGTH_SUCCESS = "Length of the enrolled period calculated successfully.";
        public const string NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH = "This is not a Non-standard term program with terms not substantially equal in length.";
        public const string CREDIT_HOUR_PROGRAM_LENGTH = "Length of the credit hour program calculated successfully.";
        public const string NOT_NONTRM_OR_NON_STANDARD_TERM_PROGRAM = "Enrollment is not a Non-Term or Non-Standard Term program.";
        public const string WITHDRAWAL_DATE_NOT_FOUND = "Withdrawal date not found for enrollment Id:";
        public const string ATTENDANCE_FETCHED_SUCCESS = "Attendance fetched successfully.";
        public const string FAILED_FETCH_TO_ATTENDANCE = "Failed to fetch attendance.";
        public const string ENROLLMENT_DETAILS_CALC_SUCCESS = "Enrollment details calculated successfully.";
        public const string CREDIT_HOUR_PROGRAM_LENGTH_ZERO = "Length of the credit hour program is: 0.00.";
        public const string NO_DIRECT_LOAN = "The student is not awarded direct loan";
        public const string NONSTANDARD_AND_SUBSTANTIALLY_NOT_EQUAL = "This enrollment is not non-standard term program with terms substantially not equal in length.";
        public const string DIRECT_LOAN_PAYMENT_PERIOD = "The payment period used for calculation period is Direct Loan Payment Period.";
        public const string NON_STANDARD_PAYMENT_PERIOD = "The payment period used for calculation period is Non Standard Payment Period.";
        public const string NOT_NONSTANDARD_PROGRAM = "The program is not a Non Standard Program.";
        public const string NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH_NON_TERM_NOT_SELFPACED_TERM = "Enrollment is neither a Nonstandard term program with terms not substantially equal in length nor a Non term not self paced program.";
        public const string END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD = "The End date of non term not self paced program calculated successfully.";
        public const string END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD = "The End date of Nonstandard term programs with terms not substantially equal in length calculated successfully.";
        public const string FAILED_CALCULATING_ENDDATE = "Failed to calculate End date.";
        public const string FAILED_CALCULATING_CREDITS_ENDDATE = "Student's schedule is not set to earn [credits] credits defined for the period. Please specify an end date when the student is expected to complete the period.";
        public const string FAILED_CALCULATING_WEEKS_ENDDATE = "Student's schedule is not set to complete [weeks] weeks defined for the period. Please specify an end date when the student is expected to complete the period.";
        public const string TOTAL_SCHEDULE_IS_LESS_THAN_POE_SCHEDULE = "The total scheduled weeks are less than required weeks to complete period of enrollment.";
        public const string TOTAL_SCHEDULE_IS_LESS_THAN_PP_SCHEDULE = "The total scheduled weeks are less than required weeks to complete payment period.";
        public const string NOT_NON_TERM_PROGRAM = "Enrollment is not a Non-Term program.";
        public const string PERIOD_USED_FOR_CALCULATION = "Period used for calculation is not period of enrollment.";
        public const string PERIOD_FOR_CALCULATION = "Period used for calculation is not payment period.";
        public const string NOT_PERIOD_USED_FOR_CALCULATION = "Please select period used for R2T4 calculation.";
        public const string CALCULATING_FAILED_COURSE_SUCCESSFUL = "Calculation of days required for failed course successful.";
        public const string ACADEMIC_CALENDAR_NOT_FOUND_FOR_ENROLLMENT = "Academic calendar not found for enrollment.";
        public const string NOT_NONTERM_PROGRAM = "The enrolled program is not a non-term program";
        public const string SELFPACED_PROGRAM = "The enrolled program is a self-paced program";
        public const string NON_SELFPACED_PROGRAM = "The enrolled program must be a non-self paced program ";
        public const string NO_DIRECT_LOANS = "There are no direct loans awarded for this student and the payment period used for calculation period is Non Standard Payment Period.";
        public const string NO_AWARDS = "There are no title iv awards for this student.";
        public const string TERM_DETAILS_FOUND = "Term details for the specified enrollment id found.";
        public const string PROGRAM_VERSION_NOT_FOUND = "Program version not found for this enrollment id.";
        public const string AWARDS_FOUND = "There are title iv awards for this student.";
        public const string DIRECT_LOANS_FOUND = "There are direct loans for this student.";

        /// <summary>
        /// The username exist.
        /// </summary>
        public const string FailedUpdatingRecord = "Failed updating record.";

        /// <summary>
        /// The username exist.
        /// </summary>
        public const string UsernameExist = "Username already exists in the database.";

        /// <summary>
        /// The username exist.
        /// </summary>
        public const string PasswordSetUpdatedSuccessfully = "Password set updated successfully.";

        /// <summary>
        /// The username does not exist.
        /// </summary>
        public const string UsernameDoesNotExist = "User does not exist.";

        /// <summary>
        /// The email exist.
        /// </summary>
        public const string EmailExist = "EmailAddress already exists in the database";

        /// <summary>
        /// The email exist for tenant.
        /// </summary>
        public const string EmailExistForTenant = "EmailAddress already exists in the Advantage";

        /// <summary>
        /// The save unsuccessful official name not provided.
        /// </summary>
        public const string SAVE_UNSUCCESSFUL_OFFICIAL_NAME_NOT_PROVIDED = "Unable to update Last Name, an official name change was not provided";

        /// <summary>
        /// The Unable to update a lead that is enrolled.
        /// </summary>
        public const string UNABLE_TO_UPDATE_LEAD_ENROLLED = @"Unable to update a lead that is enrolled.";

        /// <summary>
        /// The update terms of use failed.
        /// </summary>
        public const string FailedUpdateTermsOfUse = "Could not update user's terms of use.";

        /// <summary>
        /// The password needs special char.
        /// </summary>
        public const string PasswordNeedsSpecialChar = "The password should have at least {0} special characters.";

        /// <summary>
        /// The password needs minimum length.
        /// </summary>
        public const string PasswordNeedsMinimumLength = "The password should have a minimum length of {0}.";

        /// <summary>
        /// The schedule details not found.
        /// </summary>
        public const string ScheduleDetailsNotFound = "The schedules details were not found.";

        /// <summary>
        /// The unable to calculate grad date due to missing hours.
        /// </summary>
        public const string UnableToCalculateGradDateDueToMissingHours =
            "Unable to calculate the contracted graduation date due to missing program version total hours.";

        /// <summary>
        /// The unable to calculate grad date program is not time clock.
        /// </summary>
        public const string UnableToCalculateGradDateProgramIsNotTimeClock =
            "Unable to calculate the contracted graduation date. The selected program version is not Time Clock based.";

        /// <summary>
        /// The document save success.
        /// </summary>
        public const string DocumentSaveSuccess = "Document saved successfully";

        /// <summary>
        /// The document save success.
        /// </summary>
        public const string DocumentSaveFailed = "Saving Document is unsuccessfull";

        /// <summary>
        /// The un-handle exception.
        /// </summary>
        public const string UnhandleException = "An Unhandled exception has occurred. ";

        /// <summary>
        /// The program registration insert warning.
        /// </summary>
        public const string ProgramRegistrationInsertWarning = "Unable to set Program level registration type. ";

        /// <summary>
        /// The fail to delete program level registration due to enrollments.
        /// </summary>
        public const string FailToDeleteProgramLevelRegistrationDueToEnrollments = "Unable to remove the program level registation from the program version. There are student enrollments associated with it.";
        /// <summary>
        /// The lead not found.
        /// </summary>
        public const string LeadNotFound = "Lead not found.";

        /// <summary>
        /// The AFA student id not found.
        /// </summary>
        public const string AfaStudentIdNotFound = "AFA StudentId not found.";

        /// <summary>
        /// The duplicate lead exists.
        /// </summary>
        public const string DuplicateLeadExists = "Duplicate Lead exists.";

        /// <summary>
        /// The AFA student id exists.
        /// </summary>
        public const string AfaStudentIdExists = "Afa StudentId exists.";

        /// <summary>
        /// The AFA student id not found but lead found.
        /// </summary>
        public const string AfaStudentIdNotFoundButLeadFound = "Afa StudentId not found and Lead found.";

        /// <summary>
        /// The AFA student id not found but lead found no SSN.
        /// </summary>
        public const string AfaStudentIdNotFoundButLeadFoundNoSsn = "Afa StudentId not found and Lead found with no ssn.";

        /// <summary>
        /// The AFA student id not found but lead found with different SSN.
        /// </summary>
        public const string AfaStudentIdNotFoundButLeadFoundDiffSsn = "Afa StudentId not found and Lead found with different ssn.";

        /// <summary>
        /// New lead created because Lead found with different ssn and afa studentId.
        /// </summary>
        public const string NewLeadCreatedWithDifferentAfaStudentId = "New lead created because Lead found with different ssn and afa studentId.";

        /// <summary>
        /// The AFA student id not found but duplicate lead with SSN.
        /// </summary>
        public const string AfaStudentIdNotFoundButDuplicateLeadWithSsn = "Afa StudentId not found but duplicate Lead found with no ssn.";

        /// <summary>
        /// The failed to delete class section results exist.
        /// </summary>
        public const string FailedToDeleteClassSectionResultsExist = "Unable to delete class sections, there are student results posted!";

        /// <summary>
        /// Error retrieving widgets.
        /// </summary>
        public const string UnableToRetrieveWidgets = "Unable to retrieve widgets.";

        /// <summary>
        /// Incorrect parameters passed to api call.
        /// </summary>
        public const string IncorrectParametersPassed = "Incorrect parameters passed to api method call.";

        /// <summary>
        /// The wapi service setting not found.
        /// </summary>
        public const string WapiServiceSettingNotFound =
            "The WapiServiceSetting was not found. Set up the wapi setting in Maintenance for the service.";

        /// <summary>
        /// The klass app is not configured for campuses.
        /// </summary>
        public const string KlassAppNotConfiguredForCampuses = "Klass App is not configured for any of the campuses.";

        /// <summary>
        /// The klass app configuration is not initialized.
        /// </summary>
        public const string KlassAppConfigurationIsNotInitialized =
            "Klass App Configuration has been initialized, initial sync between klass app and advantage is required.";
    }

    internal static class AdvRegex
    {
        public const string REPLACE_NON_ALPHA = @"[^a-zA-Z0-9]+";
    }

    internal static class Global
    {
        public const string ERROR_500 = @"   
                     ._______________  _______    ___________                           
                     |   ____/\   _  \ \   _  \   \_   _____/_____________  ___________ 
                     |____  \ /  /_\  \/  /_\  \   |    __)_\_  __ \_  __ \/  _ \_  __ \
                     /       \\  \_/   \  \_/   \  |        \|  | \/|  | \(  <_> )  | \/
                    /______  / \_____  /\_____  / /_______  /|__|   |__|   \____/|__|   
                           \/        \/       \/          \/  
                    
                    Congratulations! YOU ARE............................................
                    ___________.__               _________.__                             
                    \__    ___/|  |__   ____    /   _____/|  | _____  ___.__. ___________ 
                      |    |   |  |  \_/ __ \   \_____  \ |  | \__  \<   |  |/ __ \_  __ \
                      |    |   |   Y  \  ___/   /        \|  |__/ __ \\___  \  ___/|  | \/
                      |____|   |___|  /\___  > /_______  /|____(____  / ____|\___  >__|   
                                    \/     \/          \/           \/\/         \/       
                            _____    _________                                        ._.
                      _____/ ____\  /   _____/ ______________  __ ___________  ______ | |
                     /  _ \   __\   \_____  \_/ __ \_  __ \  \/ // __ \_  __ \/  ___/ | |
                    (  <_> )  |     /        \  ___/|  | \/\   /\  ___/|  | \/\___ \   \|
                     \____/|__|    /_______  /\___  >__|    \_/  \___  >__|  /____  >  __
                                           \/     \/                 \/           \/   \/
                
                   ....that's SOOOO METAL of you....ROCK ON!";

        /// <summary>
        /// The error missing configuration.
        /// </summary>
        public const string ErrorMissingConfiguration =
            "Unable to save the record. Missing Configuration App Setting ";
    }

    /// <summary>
    /// The simple cache values.
    /// </summary>
    internal static class SimpleCacheValues
    {
        /// <summary>
        /// The key.
        /// </summary>
        public const string KEY = "SimpleCacheKey";
    }

    internal static class SsnMask
    {
        public const string Ssn = "XXX-XX-";
    }

    /// <summary>
    /// The constant decimals.
    /// </summary>
    internal static class ConstantDecimals
    {
        /// <summary>
        /// The zero decimal.
        /// </summary>
        public const decimal ZeroDecimal = 0.00m;

        /// <summary>
        /// The greater than one decimal.
        /// </summary>
        public const decimal GreaterThanOneDecimal = 0.01m;

        /// <summary>
        /// The attendance upper limit.
        /// </summary>
        public const decimal AttendanceUpperLimit = 9999m;
    }

    internal static class ConstantInstitutionalCharges
    {
        public const string Tution = "Tution";
        public const string Room = "Room";
        public const string Board = "Board";
        public const string Other = "Other";
    }

    /// <summary>
    /// The EmailAddress Service Configuration Keys constant class.
    /// </summary>
    internal static class EmailServiceConfigurationKeys
    {
        /// <summary>
        /// The smtp server.
        /// </summary>
        public const string SmtpServer = "SMTPServer";

        /// <summary>
        /// The fame smtp server.
        /// </summary>
        public const string FameSmtpServer = "FameSMTPServer";

        /// <summary>
        /// The from email address.
        /// </summary>
        public const string FromEmailAddress = "FromEmailAddress";

        /// <summary>
        /// The fame smtp port.
        /// </summary>
        public const string FameSmtpPort = "FameSMTPPort";

        /// <summary>
        /// The fame smtp username.
        /// </summary>
        public const string FameSmtpUsername = "FameSMTPUsername";

        /// <summary>
        /// The fame smtp password.
        /// </summary>
        public const string FameSmtpPassword = "FameSMTPPassword";

        /// <summary>
        /// The fame smtp ssl.
        /// </summary>
        public const string FameSmtpSsl = "FameSMTPSSL";

        /// <summary>
        /// The fame smtp o auth.
        /// </summary>
        public const string FameSmtpOAuth = "FameSMTPOAuth";

        /// <summary>
        /// The fame smtp o auth client secret.
        /// </summary>
        public const string FameSmtpOAuthClientSecret = "FameSMTPOAuthClientSecret";

        /// <summary>
        /// The fame smtp o auth token.
        /// </summary>
        public const string FameSmtpOAuthToken = "FameSMTPOAuthToken";
    }

    /// <summary>
    /// The Sap Check Configuration Keys constant class.
    /// </summary>
    internal static class SapCheckConfigurationKeys
    {
        /// <summary>
        /// The grade course repetitions method key.
        /// </summary>
        public const string GradeCourseRepetitionsMethod = "GradeCourseRepetitionsMethod";

        /// <summary>
        /// The include hours for failing grade key.
        /// </summary>
        public const string IncludeHoursForFailingGrade = "IncludeHoursForFailingGrade";

        /// <summary>
        /// The transcript type key.
        /// </summary>
        public const string TranscriptType = "TranscriptType";

        /// <summary>
        /// The sap uses credit ranges key.
        /// </summary>
        public const string SAPUsesCreditRanges = "SAPUsesCreditRanges";

        /// <summary>
        /// The track sap attendance key.
        /// </summary>
        public const string TrackSapAttendance = "TrackSapAttendance";

        /// <summary>
        /// The use imported attendance key.
        /// </summary>
        public const string UseImportedAttendance = "UseImportedAttendance";

        /// <summary>
        /// The scheduling method key.
        /// </summary>
        public const string SchedulingMethod = "SchedulingMethod";

        public const string GradesFormat = "GradesFormat";

    }
    /// <summary>
    /// The Constant R2T4 Key class contains all the constant used for R2T4 Calculation in Approve termiantion mapper
    /// </summary>
    internal static class ConstantR2T4Key
    {
        /// <summary>
        /// The Board Fee.
        /// </summary>
        public const string BoardFee = "Step 05: Board Fee";

        /// <summary>
        /// The box E result.
        /// </summary>
        public const string BoxEresult = "Step 01: Box E";

        /// <summary>
        /// The Box A of E.
        /// </summary>
        public const string BoxEa = "Step 01: Box A of E";

        /// <summary>
        /// The Box B of E.
        /// </summary>
        public const string BoxEb = "Step 01: Box B of E";

        /// <summary>
        /// The box F result.
        /// </summary>
        public const string BoxFresult = "Step 01: Box F";

        /// <summary>
        /// The Box A of F.
        /// </summary>
        public const string BoxFa = "Step 01: Box A of F";

        /// <summary>
        /// The Box C of F.
        /// </summary>
        public const string BoxFc = "Step 01: Box C of F";

        /// <summary>
        /// The box G result.
        /// </summary>
        public const string BoxGresult = "Step 01: Box G";

        /// <summary>
        /// The Step 01: Box A of G.
        /// </summary>
        public const string BoxGa = "Step 01: Box A of G";

        /// <summary>
        /// The Step 01: Box B of G.
        /// </summary>
        public const string BoxGb = "Step 01: Box B of G";

        /// <summary>
        /// The Step 01: Box C of G.
        /// </summary>
        public const string BoxGc = "Step 01: Box C of G";

        /// <summary>
        /// The Step 01: Box D of G.
        /// </summary>
        public const string BoxGd = "Step 01: Box D of G";

        /// <summary>
        /// The box H result.
        /// </summary>
        public const string BoxHresult = "Step 02: Box H";

        /// <summary>
        /// The Step 03: Box H of I.
        /// </summary>
        public const string BoxH = "Step 03: Box H of I";

        /// <summary>
        /// The Step 03: Box G of I.
        /// </summary>
        public const string BoxG = "Step 03: Box G of I";

        /// <summary>
        /// The Box I result.
        /// </summary>
        public const string BoxIresult = "Step 03: Box I";

        /// <summary>
        /// The Box J result.
        /// </summary>
        public const string BoxJresult = "Step 04: Box J";

        /// <summary>
        /// The Step 04: Box I of J.
        /// </summary>
        public const string Box4Ji = "Step 04: Box I of J";

        /// <summary>
        /// The Step 04: Box E of J.
        /// </summary>
        public const string Box4Je = "Step 04: Box E of J";

        /// <summary>
        /// The Box K result.
        /// </summary>
        public const string BoxKresult = "Step 04: Box K";

        /// <summary>
        /// The Step 04: Box I of K.
        /// </summary>
        public const string Box4Ki = "Step 04: Box I of K";

        /// <summary>
        /// The Step 04: Box E of K.
        /// </summary>
        public const string Box4Ke = "Step 04: Box E of K";

        /// <summary>
        /// The Box L result.
        /// </summary>
        public const string BoxLresult = "Step 05: Box L";

        /// <summary>
        /// The Box M result.
        /// </summary>
        public const string BoxMresult = "Step 05: Box M";

        /// <summary>
        /// The Step 05: Box H of M.
        /// </summary>
        public const string Box5H = "Step 05: Box H of M";

        /// <summary>
        /// The Box N result.
        /// </summary>
        public const string BoxNresult = "Step 05: Box N";

        /// <summary>
        /// The Step 05: Box L of N.
        /// </summary>
        public const string Box5L = "Step 05: Box L of N";

        /// <summary>
        /// The Step 05: Box M of N.
        /// </summary>
        public const string Box5M = "Step 05: Box M of N";

        /// <summary>
        /// The Box O result.
        /// </summary>
        public const string BoxOresult = "Step 05: Box O";

        /// <summary>
        /// The Box P result.
        /// </summary>
        public const string BoxPresult = "Step 06: Box P";

        /// <summary>
        /// The Box Q result.
        /// </summary>
        public const string BoxQresult = "Step 07: Box Q";

        /// <summary>
        /// The Step 07: Box K of Q.
        /// </summary>
        public const string Box7K = "Step 07: Box K of Q";

        /// <summary>
        /// The Step 07: Box O of Q.
        /// </summary>
        public const string Box7O = "Step 07: Box O of Q";

        /// <summary>
        /// The Box R result.
        /// </summary>
        public const string BoxRresult = "Step 08: Box R";

        /// <summary>
        /// The Step 08: Box B of R.
        /// </summary>
        public const string Box8B = "Step 08: Box B of R";

        /// <summary>
        /// The Step 08: Box P of R.
        /// </summary>
        public const string Box8P = "Step 08: Box P of R";

        /// <summary>
        /// The Box S result.
        /// </summary>
        public const string BoxSresult = "Step 09: Box S";

        /// <summary>
        /// The Step 09: Box Q of S.
        /// </summary>
        public const string Box9Q = "Step 09: Box Q of S";

        /// <summary>
        /// The Step 09: Box R of S result.
        /// </summary>
        public const string Box9R = "Step 09: Box R of S";

        /// <summary>
        /// The Box T result.
        /// </summary>
        public const string BoxTresult = "Step 09: Box T";

        /// <summary>
        /// The Step 09: Box F of T.
        /// </summary>
        public const string Box9F = "Step 09: Box F of T";

        /// <summary>
        /// The Box U result.
        /// </summary>
        public const string BoxUresult = "Step 09: Box U";

        /// <summary>
        /// The Step 09: Box S of U.
        /// </summary>
        public const string Box9S = "Step 09: Box S of U";

        /// <summary>
        /// The Step 09: Box T of U.
        /// </summary>
        public const string Box9T = "Step 09: Box T of U";

        /// <summary>
        /// The completed time.
        /// </summary>
        public const string CompletedTime = "Step 02: Hours scheduled to complete";

        /// <summary>
        /// The credit balance refunded.
        /// </summary>
        public const string CreditBalanceRefunded = "Credit Balance Refunded";

        /// <summary>
        /// The direct graduate plus loan could disbursed.
        /// </summary>
        public const string DirectGraduatePlusLoanCouldDisbursed = "Step 01: Direct Graduate Plus Loan Could Have Been Disbursed";

        /// <summary>
        /// The direct graduate plus loan disbursed.
        /// </summary>
        public const string DirectGraduatePlusLoanDisbursed = "Step 01: Direct Graduate Plus Loan Disbursed";

        /// <summary>
        /// The direct parent plus loan could disbursed.
        /// </summary>
        public const string DirectParentPlusLoanCouldDisbursed = "Step 01: Direct Parent Plus Loan Could Have Been Disbursed";

        /// <summary>
        /// The direct parent plus loan disbursed.
        /// </summary>
        public const string DirectParentPlusLoanDisbursed = "Step 01: Direct Parent Plus Loan Disbursed";

        /// <summary>
        /// The FSEOG amount to return.
        /// </summary>
        public const string FseogamountToReturn = "Step 10: FSEOG Amount Student Has To Return";

        /// <summary>
        /// The FSEOG could disbursed.
        /// </summary>
        public const string FseogcouldDisbursed = "Step 01: FSEOG Amount Could Have Been Disbursed";

        /// <summary>
        /// The FSEOG disbursed.
        /// </summary>
        public const string Fseogdisbursed = "Step 01: FSEOG Disbursed";

        /// <summary>
        /// The Iraq Afghanistan grant service amount to return.
        /// </summary>
        public const string IraqAfgGrantAmountToReturn = "Step 10: Iraq Afghanistan Service Grant Amount Student Has To Return";

        /// <summary>
        /// The Iraq Afghanistan grant could disbursed.
        /// </summary>
        public const string IraqAfgGrantCouldDisbursed = "Step 01: Iraq Afghanistan Grant Could Have Been Disbursed";

        /// <summary>
        /// The Iraq Afghanistan grant disbursed.
        /// </summary>
        public const string IraqAfgGrantDisbursed = "Step 01: Iraq Afghanistan Grant Disbursed";

        /// <summary>
        /// The Other Fee.
        /// </summary>
        public const string OtherFee = "Step 05: Other Fee";

        /// <summary>
        /// The pell grant amount to return.
        /// </summary>
        public const string PellGrantAmountToReturn = "Step 10: Pell Grant Amount Student Has To Return";

        /// <summary>
        /// The pell grant could disbursed.
        /// </summary>
        public const string PellGrantCouldDisbursed = "Step 01: Pell Grant Could Have Been Disbursed";

        /// <summary>
        /// The pell grant disbursed.
        /// </summary>
        public const string PellGrantDisbursed = "Step 01: Pell Grant Disbursed";

        /// <summary>
        /// The percentage of actual attendence.
        /// </summary>
        public const string PercentageOfActualAttendence = "Step 02: Percentage Of Actual Attendance";

        /// <summary>
        /// The perkins loan could disbursed.
        /// </summary>
        public const string PerkinsLoanCouldDisbursed = "Step 01: Perkins Loan Could Have Been Disbursed";

        /// <summary>
        /// The perkins loan disbursed.
        /// </summary>
        public const string PerkinsLoanDisbursed = "Step 01: Perkins Loan Disbursed";

        /// <summary>
        /// The Room Fee.
        /// </summary>
        public const string RoomFee = "Step 05: Room Fee";

        /// <summary>
        /// The scheduled end date.
        /// </summary>
        public const string ScheduledEndDate = "Step 02: Scheduled End Date";

        /// <summary>
        /// The start date.
        /// </summary>
        public const string StartDate = "Step 02: Start Date";

        /// <summary>
        /// The sub direct loan school return.
        /// </summary>
        public const string SubDirectLoanSchoolReturn = "Step 06: Subsidized Direct Loan School Has To Return";

        /// <summary>
        /// The teach grant school return.
        /// </summary>
        public const string TeachGrantSchoolReturn = "Step 06: Teach Grant School Has To Return";

        /// <summary>
        /// The unsub direct loan school return.
        /// </summary>
        public const string UnsubDirectLoanSchoolReturn = "Step 06: Unsubsidized Direct Loan School Has To Return";

        /// <summary>
        /// The perkins loan school return.
        /// </summary>
        public const string PerkinsLoanSchoolReturn = "Step 06: Perkins Loan School Has To Return";

        /// <summary>
        /// The Iraq Afghanistan grant school return.
        /// </summary>
        public const string IraqAfgGrantSchoolReturn = "Step 06: Iraq Afghanistan Grant School Has To Return";

        /// <summary>
        /// The pell grant school return.
        /// </summary>
        public const string PellGrantSchoolReturn = "Step 06: Pell Grant School Has To Return";

        /// <summary>
        /// The FSEOG school return.
        /// </summary>
        public const string FseogschoolReturn = "Step 06: FSEOG School Has To Return";

        /// <summary>
        /// The direct parent plus loan school return.
        /// </summary>
        public const string DirectParentPlusLoanSchoolReturn = "Step 06: Direct Parent Plus Loan School Has To Return";

        /// <summary>
        /// The direct graduate plus loan school return.
        /// </summary>
        public const string DirectGraduatePlusLoanSchoolReturn = "Step 06: Direct Graduate Plus Loan School Has To Return";

        /// <summary>
        /// The sub loan net amount disbursed.
        /// </summary>
        public const string SubLoanNetAmountDisbursed = "Step 01: Subsidized Loan Net Amount Disbursed";

        /// <summary>
        /// The sub loan net amount could disbursed.
        /// </summary>
        public const string SubLoanNetAmountCouldDisbursed = "Step 01: Subsidized Loan Net Amount Could Have Been Disbursed";

        /// <summary>
        /// The Subtotal amount disbursed A.
        /// </summary>
        public const string SubTotalAmountDisbursedA = "Step 01: Subtotal Box A";

        /// <summary>
        /// The Subtotal amount could disbursed C.
        /// </summary>
        public const string SubTotalAmountCouldDisbursedC = "Step 01: Subtotal Box C";

        /// <summary>
        /// The Subtotal net amount disbursed B.
        /// </summary>
        public const string SubTotalNetAmountDisbursedB = "Step 01: Subtotal Box B";

        /// <summary>
        /// The Subtotal net amount disbursed D.
        /// </summary>
        public const string SubTotalNetAmountDisbursedD = "Step 01: Subtotal Box D";

        /// <summary>
        /// The teach grant amount to return.
        /// </summary>
        public const string TeachGrantAmountToReturn = "Step 10: Teach Grant Amount Student Has To Return";

        /// <summary>
        /// The teach grant could disbursed.
        /// </summary>
        public const string TeachGrantCouldDisbursed = "Step 01: Teach Grant Could Have Been Disbursed";

        /// <summary>
        /// The teach grant disbursed.
        /// </summary>
        public const string TeachGrantDisbursed = "Step 01: Teach Grant Disbursed";

        /// <summary>
        /// The total time.
        /// </summary>
        public const string TotalTime = "Step 02: Total hours in period";

        /// <summary>
        /// The tuition fee.
        /// </summary>
        public const string TuitionFee = "Step 05: Tuition Fee";

        /// <summary>
        /// The unsub loan net amount could disbursed.
        /// </summary>
        public const string UnsubLoanNetAmountCouldDisbursed = "Step 01: Unsubsidized Loan Net Amount Could Have Been Disbursed";

        /// <summary>
        /// The unsub loan net amount disbursed.
        /// </summary>
        public const string UnsubLoanNetAmountDisbursed = "Step 01: Unsubsidized Loan Net Amount Disbursed";

        /// <summary>
        /// The withdrawal date.
        /// </summary>
        public const string WithdrawalDate = "Step 02: Withdrawal Date";

        /// <summary>
        /// The pwd box 1.
        /// </summary>
        public const string TxtPwd = "Step 11: Box 1";

        /// <summary>
        /// The pwd box 2.
        /// </summary>
        public const string TxtPwdBox2 = "Step 11: Box 2";

        /// <summary>
        /// The pwd box 3.
        /// </summary>
        public const string TxtPwdBox3 = "Step 11: Box 3";

        /// <summary>
        /// The pwd box 31.
        /// </summary>
        public const string TxtPwdOffered = "Step 11: Box 1 of 3";

        /// <summary>
        /// The pwd box 31.
        /// </summary>
        public const string TxtPwdBox2Offered = "Step 11: Box 2 of 3";

        /// <summary>
        /// The pwd pell grant title iv aid credited.
        /// </summary>
        public const string PwdPell3 = "Step 11: Pell Grant Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd pell grant title iv disbursed.
        /// </summary>
        public const string PwdPell6 = "Step 11: Pell Grant Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd fseog title iv aid credited.
        /// </summary>
        public const string PwdFseog3 = "Step 11: FSEOG Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd fseog title iv disbursed.
        /// </summary>
        public const string PwdFseog6 = "Step 11: FSEOG Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd teach grant title iv aid credited.
        /// </summary>
        public const string PwdTeach3 = "Step 11: TEACH Grant Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd teach grant title iv disbursed.
        /// </summary>
        public const string PwdTeach6 = "Step 11: TEACH Grant Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd iraq and afg ser grant title iv aid credited.
        /// </summary>
        public const string PwdIasg3 = "Step 11: Iraq and Afganistan Service Grant Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd iraq and afg ser grant title iv disbursed.
        /// </summary>
        public const string PwdIasg6 = "Step 11: Iraq and Afganistan Service Grant Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd perkins loan school seeks to credit.
        /// </summary>
        public const string PwdPerkins1 = "Step 11: Perkins Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd perkins loan authorized to credit.
        /// </summary>
        public const string PwdPerkins2 = "Step 11: Perkins Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd perkins loan title iv aid credited.
        /// </summary>
        public const string PwdPerkins3 = "Step 11: Perkins Loan Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd perkins loan amount offered direct disbursement.
        /// </summary>
        public const string PwdPerkins4 = "Step 11: Perkins Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd perkins loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdPerkins5 = "Step 11: Perkins Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd perkins loan title iv disbursed directly.
        /// </summary>
        public const string PwdPerkins6 = "Step 11: Perkins Loan Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd sub direct loan school seeks to credit.
        /// </summary>
        public const string PwdSub1 = "Step 11: Subsidized Direct Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd sub direct loan authorized to credit.
        /// </summary>
        public const string PwdSub2 = "Step 11: Subsidized Direct Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd sub direct loan title iv aid credited.
        /// </summary>
        public const string PwdSub3 = "Step 11: Subsidized Direct Loan Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd sub direct loan amount offered direct disbursement.
        /// </summary>
        public const string PwdSub4 = "Step 11: Subsidized Direct Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd sub direct loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdSub5 = "Step 11: Subsidized Direct Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd sub direct loan title iv disbursed directly.
        /// </summary>
        public const string PwdSub6 = "Step 11: Subsidized Direct Loan Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd unsub direct loan school seeks to credit.
        /// </summary>
        public const string PwdUnSub1 = "Step 11: Unsubsidized Direct Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd unsub direct loan authorized to credit.
        /// </summary>
        public const string PwdUnSub2 = "Step 11: Unsubsidized Direct Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd unsub direct loan title iv aid credited.
        /// </summary>
        public const string PwdUnSub3 = "Step 11: Unsubsidized Direct Loan Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd unsub direct loan amount offered direct disbursement.
        /// </summary>
        public const string PwdUnSub4 = "Step 11: Unsubsidized Direct Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd unsub direct loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdUnSub5 = "Step 11: Unsubsidized Direct Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd unsub direct loan title iv disbursed directly.
        /// </summary>
        public const string PwdUnSub6 = "Step 11: Unsubsidized Direct Loan Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd direct graduate plus loan school seeks to credit.
        /// </summary>
        public const string PwdGrad1 = "Step 11: Direct Graduate Plus Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd direct graduate plus loan authorized to credit.
        /// </summary>
        public const string PwdGrad2 = "Step 11: Direct Graduate Plus Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd direct graduate plus loan title iv aid credited.
        /// </summary>
        public const string PwdGrad3 = "Step 11: Direct Graduate Plus Loan Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd direct graduate plus loan amount offered direct disbursement.
        /// </summary>
        public const string PwdGrad4 = "Step 11: Direct Graduate Plus Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd direct graduate plus loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdGrad5 = "Step 11: Direct Graduate Plus Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd direct graduate plus loan title iv disbursed directly.
        /// </summary>
        public const string PwdGrad6 = "Step 11: Direct Graduate Plus Loan Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd direct parent plus loan school seeks to credit.
        /// </summary>
        public const string PwdParent1 = "Step 11: Direct Parent Plus Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd direct parent plus loan authorized to credit.
        /// </summary>
        public const string PwdParent2 = "Step 11: Direct Parent Plus Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd direct parent plus loan title iv aid credited.
        /// </summary>
        public const string PwdParent3 = "Step 11: Direct Parent Plus Loan Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd direct parent plus loan amount offered direct disbursement.
        /// </summary>
        public const string PwdParent4 = "Step 11: Direct Parent Plus Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd direct parent plus loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdParent5 = "Step 11: Direct Parent Plus Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd direct parent plus loan title iv disbursed directly.
        /// </summary>
        public const string PwdParent6 = "Step 11: Direct Parent Plus Loan Title IV Disbursed Directly to Student";

        /// <summary>
        /// The Pwd total loan amount school seeks to credit.
        /// </summary>
        public const string PwdTotal1 = "Step 11: Total Loan Amount School Seeks to Credit to Account";

        /// <summary>
        /// The Pwd total loan amount authorized to credit.
        /// </summary>
        public const string PwdTotal2 = "Step 11: Total Loan Amount Authorized to Credit to Account";

        /// <summary>
        /// The Pwd total title iv aid credited.
        /// </summary>
        public const string PwdTotal3 = "Step 11: Total Title IV Aid Credited to Account";

        /// <summary>
        /// The Pwd total loan amount offered direct disbursement.
        /// </summary>
        public const string PwdTotal4 = "Step 11: Total Loan Amount Offered as Direct Disbursement";

        /// <summary>
        /// The Pwd total loan amount accepted direct disbursement.
        /// </summary>
        public const string PwdTotal5 = "Step 11: Total Loan Amount Accepted as Direct Disbursement";

        /// <summary>
        /// The Pwd total title iv disbursed directly.
        /// </summary>
        public const string PwdTotal6 = "Step 11: Total Title IV Disbursed Directly to Student";

        /// <summary>
        /// The date post withdrwal.
        /// </summary>
        public const string DtPostWithdrwal = "Step 11: Post-withdrawal disbursement loan notification sent to student and/or parent on";

        /// <summary>
        /// The date deadline.
        /// </summary>
        public const string DtDeadline = "Step 11: Deadline for student and/or parent to respond";

        /// <summary>
        /// The date response received.
        /// </summary>
        public const string DtResponseReceived = "Step 11: Response received from Student and/or parent on";

        /// <summary>
        /// The date grant transferred.
        /// </summary>
        public const string DtGrantTransferred = "Step 11: Date Direct Disbursement mailed or transferred Grant";

        /// <summary>
        /// The date loan transferred.
        /// </summary>
        public const string DtLoanTransferred = "Step 11: Date Direct Disbursement mailed or transferred Loan";

        /// <summary>
        /// The checkbox Response received from Student and/or parent.
        /// </summary>
        public const string ChkResponseReceived = "Step 11: Response received from Student and/or parent";

        /// <summary>
        /// The checkbox Response not received.
        /// </summary>
        public const string ChkResponseNotReceived = "Step 11: Response not received";

        /// <summary>
        /// The checkbox School does not accept late response.
        /// </summary>
        public const string ChkNotAccept = "Step 11: School does not accept late response";
    }

    /// <summary>
    /// The constant loan disbursed.
    /// </summary>
    internal static class ConstantLoanDisbursed
    {
        /// <summary>
        /// The unsub loan net amount disbursed.
        /// </summary>
        public const string UnsubLoanNetAmountDisbursed = "Unsubsidized direct loan";

        /// <summary>
        /// The sub loan net amount disbursed.
        /// </summary>
        public const string SubLoanNetAmountDisbursed = "Subsidized direct loan";

        /// <summary>
        /// The perkins loan disbursed.
        /// </summary>
        public const string PerkinsLoanDisbursed = "Perkins loan";

        /// <summary>
        /// The direct graduate plus loan disbursed.
        /// </summary>
        public const string DirectGraduatePlusLoanDisbursed = "Direct Grad PLUS loan";

        /// <summary>
        /// The direct parent plus loan disbursed.
        /// </summary>
        public const string DirectParentPlusLoanDisbursed = "Direct Parent PLUS loan";
    }

    /// <summary>
    /// The constant loan to return by school.
    /// </summary>
    internal static class ConstantLoanToReturnBySchool
    {
        /// <summary>
        /// The unsub direct loan school return.
        /// </summary>
        public const string UnsubDirectLoanSchoolReturn = "Unsubsidized direct loan";

        /// <summary>
        /// The sub direct loan school return.
        /// </summary>
        public const string SubDirectLoanSchoolReturn = "Subsidized direct loan";

        /// <summary>
        /// The perkins loan school return.
        /// </summary>
        public const string PerkinsLoanSchoolReturn = "Perkins loan";

        /// <summary>
        /// The direct graduate plus loan school return.
        /// </summary>
        public const string DirectGraduatePlusLoanSchoolReturn = "Direct Grad PLUS loan";

        /// <summary>
        /// The direct parent plus loan school return.
        /// </summary>
        public const string DirectParentPlusLoanSchoolReturn = "Direct Parent PLUS loan";

        /// <summary>
        /// The pell grant school return.
        /// </summary>
        public const string PellGrantSchoolReturn = "Pell Grant";

        /// <summary>
        /// The fseogschool return.
        /// </summary>
        public const string FseogschoolReturn = "FSEOG Grant";

        /// <summary>
        /// The teach grant school return.
        /// </summary>
        public const string TeachGrantSchoolReturn = "TEACH Grant";

        /// <summary>
        /// The iraq afg grant school return.
        /// </summary>
        public const string IraqAfgGrantSchoolReturn = "Iraq Afghanistan Service Grant";
    }

    /// <summary>
    /// The constant loan to return by school.
    /// </summary>
    internal static class ConstantGrantToReturnByStudent
    {
        /// The pell grant school return.
        /// </summary>
        public const string PellGrantAmountToReturn = "Pell Grant";

        /// <summary>
        /// The fseogschool return.
        /// </summary>
        public const string FseogamountToReturn = "FSEOG Grant";

        /// <summary>
        /// The teach grant school return.
        /// </summary>
        public const string TeachGrantAmountToReturn = "TEACH Grant";

        /// <summary>
        /// The iraq afg grant school return.
        /// </summary>
        public const string IraqAfgGrantAmountToReturn = "Iraq Afghanistan Service Grant";
    }

    /// <summary>
    /// The data type for parsing values.
    /// </summary>
    internal static class FieldType
    {
        /// <summary>
        /// The percentage.
        /// </summary>
        public const string Percentage = "Percentage";

        /// <summary>
        /// The date.
        /// </summary>
        public const string Date = "Date";

        /// <summary>
        /// The date.
        /// </summary>
        public const string Number = "Number";
    }

    /// <summary>
    /// The configuration app settings keys.
    /// </summary>
    internal static class ConfigurationAppSettingsKeys
    {
        /// <summary>
        /// The report services url key.
        /// </summary>
        public const string ReportServicesUrl = "ReportServices";

        /// <summary>
        /// The report execution url key.
        /// </summary>
        public const string ReportExecutionUrl = "ReportExecutionServices";

        /// <summary>
        /// The advantage api url.
        /// </summary>
        public const string AdvantageApiUrl = "AdvantageApiURL";

        /// <summary>
        /// The standard logo.
        /// </summary>
        public const string StandardLogo = "StandardLogo";

        /// <summary>
        /// The terms of use version number.
        /// </summary>
        public const string TermsOfUseVersionNumber = "TermsOfUseVersionNumber";

        /// <summary>
        /// The track sap attendance.
        /// </summary>
        public const string TrackSapAttendance = "TrackSapAttendance";

        /// <summary>
        /// The allow course weighting.
        /// </summary>
        public const string AllowCourseWeighting = "AllowCourseWeighting";

        /// <summary>
        /// The grade course repetitions method key.
        /// </summary>
        public const string GradeCourseRepetitionsMethod = "GradeCourseRepetitionsMethod";

        /// <summary>
        /// The include hours for failing grade key.
        /// </summary>
        public const string IncludeHoursForFailingGrade = "IncludeHoursForFailingGrade";

        /// <summary>
        /// The transcript type key.
        /// </summary>
        public const string TranscriptType = "TranscriptType";

        /// <summary>
        /// The use imported attendance key.
        /// </summary>
        public const string UseImportedAttendance = "UseImportedAttendance";

        /// <summary>
        /// The scheduling method key.
        /// </summary>
        public const string SchedulingMethod = "SchedulingMethod";

        /// <summary>
        /// The grades format key.
        /// </summary>
        public const string GradesFormat = "GradesFormat";

        /// <summary>
        /// The GPA Method key.
        /// </summary>
        public const string GPAMethod = "GPAMethod";

        /// <summary>
        /// The grade rounding key.
        /// </summary>
        public const string GradeRounding = "GradeRounding";

        /// <summary>
        /// The grade book weighting level key.
        /// </summary>
        public const string GradeBookWeightingLevel = "GradeBookWeightingLevel";

        /// <summary>
        /// The school name.
        /// </summary>
        public const string SchoolName = "SchoolName";

        /// <summary>
        /// The enable AFA Integration key.
        /// </summary>
        public const string EnableAFAIntegration = "EnableAFAIntegration";
        /// <summary>
        /// The student identifier key.
        /// </summary>
        public const string StudentIdentifier = "StudentIdentifier";

        /// <summary>
        /// The student identifier key.
        /// </summary>
        public const string TitleIVWidgetFilter = "TitleIVWidgetFilter";

        /// <summary>
        /// The klass app campus to consider.
        /// </summary>
        public const string KlassAppCampusToConsider = "KlassApp_CampusToConsider";

        /// <summary>
        /// The students ready for graduation widget.
        /// </summary>
        public const string GraduationWidget = "GraduationWidget";

        /// <summary>
        /// The time clock class section.
        /// </summary>
        public const string TimeClockClassSection = "TimeClockClassSection";

        /// <summary>
        /// The automated time clock import setting
        /// </summary>
        public const string AutomatedTimeClockImport = "AutomatedTimeClockImport";

    }

    /// <summary>
    /// The configuration app settings values.
    /// </summary>
    internal static class ConfigurationAppSettingsValues
    {
        /// <summary>
        /// The yes constant.
        /// </summary>
        public const string Yes = "Yes";

        /// <summary>
        /// The no constant.
        /// </summary>
        public const string No = "No";
    }

    /// <summary>
    /// The document type.
    /// </summary>
    internal static class DocumentType
    {
        /// <summary>
        /// The R2T4 document type.
        /// </summary>
        public const string R2T4DocumentType = "R2T4";

        /// <summary>
        /// The R2T4 document type.
        /// </summary>
        public const string TerminatiionDetail = "Termination details";
    }

    /// <summary>
    /// The document status.
    /// </summary>
    internal static class DocumentStatus
    {
        /// <summary>
        /// The approved.
        /// </summary>
        public const string Approved = "Approved";
    }

    /// <summary>
    /// The user types.
    /// </summary>
    internal static class UserTypes
    {
        /// <summary>
        /// The Advantage.
        /// </summary>
        public const string Advantage = "Advantage";

        /// <summary>
        /// The vendor.
        /// </summary>
        public const string Vendor = "Vendor";
    }

    /// <summary>
    /// The attendance unit types.
    /// </summary>
    internal static class AttendanceUnitTypes
    {
        /// <summary>
        /// The present absent.
        /// </summary>
        public const string PresentAbsent = "Present Absent";

        /// <summary>
        /// The clock hours.
        /// </summary>
        public const string ClockHours = "Clock Hours";

        /// <summary>
        /// The minutes.
        /// </summary>
        public const string Minutes = "Minutes";

        /// <summary>
        /// The none.
        /// </summary>
        public const string None = "None";
    }

    /// <summary>
    /// The r 2 t 4 report parameters.
    /// </summary>
    internal static class R2T4ReportParameters
    {
        /// <summary>
        /// The report name.
        /// </summary>
        public const string ReportName = "reportName";

        /// <summary>
        /// The report category.
        /// </summary>
        public const string ReportCategory = "R2T4";

        /// <summary>
        /// The student termination id.
        /// </summary>
        public const string StudentTerminationId = "StudentTerminationId";

        /// <summary>
        /// The report output.
        /// </summary>
        public const string ReportOutput = "reportOutput";
    }

    /// <summary>
    /// The file extensions.
    /// </summary>
    internal static class FileExtensions
    {
        /// <summary>
        /// The pdf.
        /// </summary>
        public const string Pdf = ".pdf";

        /// <summary>
        /// The excel.
        /// </summary>
        public const string Excel = ".xlsx";
    }

    /// <summary>
    /// The ConstantStrings.
    /// </summary>
    internal static class ConstantStrings
    {
        /// <summary>
        /// The underscore.
        /// </summary>
        public const string Underscore = "_";
    }

    /// <summary>
    /// The calculation period types.
    /// </summary>
    internal static class CalculationPeriodTypes
    {
        /// <summary>
        /// The Payment Period.
        /// </summary>
        public const string PAYMENT_PERIOD = "Payment Period";

        /// <summary>
        /// The Period of Enrollment.
        /// </summary>
        public const string PERIOD_OF_ENROLLMENT = "Period of Enrollment";
    }

    /// <summary>
    /// The student configuration keys.
    /// </summary>
    internal static class StudentConfigurationKeys
    {
        /// <summary>
        /// The student identifier.
        /// </summary>
        public const string StudentIdentifier = "StudentIdentifier";
    }


    /// <summary>
    /// The Student Identifier Keys.
    /// </summary>
    internal static class StudentIdentifierKeys
    {
        /// <summary>
        /// The ssn.
        /// </summary>
        public const string Ssn = "SSN";

        /// <summary>
        /// The enrollment id.
        /// </summary>
        public const string EnrollmentId = "EnrollmentId";

        /// <summary>
        /// The student id.
        /// </summary>
        public const string StudentId = "StudentId";
    }

    /// <summary>
    /// The track sap attendance.
    /// </summary>
    internal static class TrackSapAttendance
    {
        /// <summary>
        /// The by day.
        /// </summary>
        public const string ByDay = "byday";

        /// <summary>
        /// The by class.
        /// </summary>
        public const string ByClass = "byclass";
    }

    /// <summary>
    /// The Title IV Sap statuses.
    /// </summary>
    internal static class TitleIVSapStatus
    {
        /// <summary>
        /// The passed status.
        /// </summary>
        public const string Passed = "Passed";

        /// <summary>
        /// The warning status.
        /// </summary>
        public const string Warning = "Warning";

        /// <summary>
        /// The ineligible status.
        /// </summary>
        public const string Ineligible = "Ineligible";
        /// <summary>
        /// The probation status.
        /// </summary>
        public const string Probation = "Probation";
    }
    /// <summary>
    /// The Title IV Sap result messages
    /// </summary>
    internal static class TitleIVSapResult
    {
        /// <summary>
        /// The passed result.
        /// </summary>
        public const string Passed = "Passed";

        /// <summary>
        /// The failed result.
        /// </summary>
        public const string Failed = "Failed";

    }
    /// <summary>
    /// The Title IV result messages.
    /// </summary>
    internal static class TitleIVResultMessages
    {
        /// <summary>
        /// The CGPA prefix.
        /// </summary>
        public const string CGPA_PREFIX = "Student has a Cumulative GPA of ";
        /// <summary>
        /// The numeric prefix.
        /// </summary>
        public const string NUMERIC_PREFIX = "Student has an Overall Average of ";
        /// <summary>
        /// The Title IV generic prefix.
        /// </summary>
        public const string PREFIX = "Student has completed ";
        /// <summary>
        /// The Title IV passed suffix.
        /// </summary>
        public const string PASSED_SUFFIX = " which is more than the minimum of ";
        /// <summary>
        /// The Title IV failed suffix.
        /// </summary>
        public const string FAILED_SUFFIX = " which is less than the minimum of ";
        /// <summary>
        /// The Title IV credits attempted quantifier message.
        /// </summary>
        public const string CREDITS_ATTEMPTED_QUANT = " % of credits attempted";
        /// <summary>
        /// The Title IV scheduled hours quantifier message.
        /// </summary>
        public const string SCHEDULED_HOURS_QUANT = " % of scheduled hours/days";
    }

    internal static class TitleIV
    {
        internal static class PlaceHolders
        {
            /// <summary>
            /// The minimum quantity placeholder.
            /// </summary>
            public const string Minimum_Quantity = "<<Minimum quantity>>";
            /// <summary>
            /// The minimum GPA placeholder.
            /// </summary>
            public const string Minimum_GPA = "<<MinimumGPA>>";
        }
    }
    /// <summary>
    /// The Grades format constants.
    /// </summary>
    internal static class GradesFormat
    {
        /// <summary>
        /// The numeric grade format.
        /// </summary>
        public const string Numeric = "Numeric";

        /// <summary>
        /// The letter grade format.
        /// </summary>
        public const string Letter = "Letter";
    }

    internal static class GradeBookWeightLevel
    {
        /// <summary>
        /// The course level.
        /// </summary>
        public const string CourseLevel = "CourseLevel";
    }
    /// <summary>
    /// The report location constants.
    /// </summary>
    internal static class ReportLocations
    {
        /// <summary>
        /// The progress report location.
        /// </summary>
        public const string ProgressReport = "ProgressReport/PR_Main_Sub1";

        /// <summary>
        /// The title iv notice report.
        /// </summary>
        public const string TitleIvNoticeReport = "TitleIVNotice/TitleIVNotice_Main";

        /// <summary>
        /// The title iv notice report.
        /// </summary>
        public const string InvoiceReport = "Invoice/Invoice_Main";
    }

    /// <summary>
    /// The statuses.
    /// </summary>
    internal static class Statuses
    {
        /// <summary>
        /// The active.
        /// </summary>
        public const string Active = "A";

        /// <summary>
        /// The inactive.
        /// </summary>
        public const string Inactive = "I";
    }

    /// <summary>
    /// The transaction types.
    /// </summary>
    internal static class TransactionTypes
    {
        /// <summary>
        /// The charge.
        /// </summary>
        public const string Charge = "Charge";
        /// <summary>
        /// The adjustment.
        /// </summary>
        public const string Adjustment = "Adjustment";
        /// <summary>
        /// The payment.
        /// </summary>
        public const string Payment = "Payment";
        /// <summary>
        /// The reversal.
        /// </summary>
        public const string Reversal = "Reversal";
    }
    /// <summary>
    /// The AFA Constants Encapsulation
    /// </summary>
    internal static class AFA
    {
        /// <summary>
        /// The payment period statuses for AFA
        /// </summary>
        internal static class PaymentPeriodStatus
        {
            /// <summary>
            /// The graduate status.
            /// </summary>
            public const string Graduate = "Graduate";

            /// <summary>
            /// The no show status.
            /// </summary>
            public const string NoShow = "No Show";

            /// <summary>
            /// The leave of absence status.
            /// </summary>
            public const string LeaveOfAbsence = "Leave of Absence";

            /// <summary>
            /// The drop status.
            /// </summary>
            public const string Drop = "Drop";

            /// <summary>
            /// The enrolled status
            /// </summary>
            public const string Enrolled = "Enrolled";
        }
        /// <summary>
        /// The Title IV Sap status for AFA
        /// </summary>
        internal static class TitleIVSapStatus
        {
            /// <summary>
            /// The passed result.
            /// </summary>
            public const string Passed = "Y";

            /// <summary>
            /// The failed result.
            /// </summary>
            public const string Failed = "N";
        }

        /// <summary>
        /// The fund sources.
        /// </summary>
        internal static class FundSource
        {
            /// <summary>
            /// The PELL fund source.
            /// </summary>
            public const string Pell = "PELL";
            /// <summary>
            /// The DLPLUS fund source.
            /// </summary>
            public const string DLPLUS = "DLPLUS";
            /// <summary>
            /// The DLSUB fund source.
            /// </summary>
            public const string DLSUB = "DLSUB";
            /// <summary>
            /// The DLUNSUB fund source.
            /// </summary>
            public const string DLUNSUB = "DLUNSUB";
            /// <summary>
            /// The OTHER fund source.
            /// </summary>
            public const string OTHER = "OTHER";
            /// <summary>
            /// The FSEOG fund source.
            /// </summary>
            public const string FSEOG = "FSEOG";

            internal static class Other
            {
                /// <summary>
                /// The "SLM Smart Opt" fund source.
                /// </summary>
                public const string SLMSmartOpt = "SLM Smart Opt";
                /// <summary>
                /// The post 9/11 GI Bill rights fund source.
                /// </summary>
                public const string Post911GIBillRights = "Post 9/11 GI Bill Rights";
                /// <summary>
                /// The SEOG match fund source.
                /// </summary>
                public const string SEOGMatch = "SEOG Match";
                /// <summary>
                /// The TUBC Special Grant fund source.
                /// </summary>
                public const string TUBCSpecGrant = "TUBC Spec Grant";
                /// <summary>
                /// The other aid fund source.
                /// </summary>
                public const string OtherAid = "Other Aid";
            }

        }
    }

    /// <summary>
    /// The fund sources.
    /// </summary>
    internal static class FundSource
    {
        /// <summary>
        /// The SLM Smart Option fund source.
        /// </summary>
        public const string SLMSmartOption = "SLM Smart Option";
        /// <summary>
        /// The post 9/11 GI Bill benifits fund source.
        /// </summary>
        public const string Post911GIBillBenifits = "Post 9/11 GI Bill Benefits";
        /// <summary>
        /// The FSEOG fund source.
        /// </summary>
        public const string FSEOG = "Federal SEOG";
        /// <summary>
        /// The FSEOG Match fund source.
        /// </summary>
        public const string FSEOGMatch = "FSEOG Match";
        /// <summary>
        /// The TUBC special grant fund source.
        /// </summary>
        public const string TUBCSpecialGrant = "TUBC Special Grant";
        /// <summary>
        /// The other aid fund source.
        /// </summary>
        public const string OtherAid = "Other Miscellaneous Aid";
        
    }

    /// <summary>
    /// The wapi service operations.
    /// </summary>
    internal static class WapiServiceOperations
    {
        /// <summary>
        /// The wapi service name.
        /// </summary>
        public const string WapiServiceName = "Advantage Wapi Service";

        /// <summary>
        /// The klass app service.
        /// </summary>
        public const string KlassAppService = "KLASS_APP_STUDENT";
    }

    /// <summary>
    /// The widgets codes.
    /// </summary>
    internal static class WidgetsCodes
    {
        /// <summary>
        /// The students ready for graduation.
        /// </summary>
        public const string StudentsReadyForGraduation = "StudentsReadyForGraduation";
    }
}