﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace FAME.Advantage.RestApi.Host.Infrastructure.Swagger
{
    public class AddAuthorizationHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var filterPipeline = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            var isAuthorized = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is AuthorizeFilter);
            var allowAnonymous = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is IAllowAnonymousFilter);
            if (!isAuthorized || allowAnonymous) return;
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();
            operation.Parameters.Add(new NonBodyParameter
            {
                Name = SwashbuckleInfo.AUTH_NBP_NAME,
                In = SwashbuckleInfo.AUTH_NBP_IN,
                Description = SwashbuckleInfo.AUTH_NBP_DESCRIPTION,
                Required = SwashbuckleInfo.AUTH_NBP_REQUIRED,
                Type = SwashbuckleInfo.AUTH_NBP_TYPE
            });
        }
    }
}