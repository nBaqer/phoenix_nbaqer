﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomExceptionHandlerMiddleware.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CustomExceptionHandlerMiddleware type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Middleware
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.ApplicationInsights;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;

    public sealed class CustomExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public CustomExceptionHandlerMiddleware(
            RequestDelegate next,
            ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.
                CreateLogger<CustomExceptionHandlerMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
              
                await _next(context);
            }
            catch (Exception e)
            {
                try
                {
                    e.TrackException();
                    _logger.LogError(e.ToString());
                }
                catch (Exception ex2)
                {
                    _logger.LogError(
                        0, ex2,
                        ApiMsgs.ERROR_ON_ERROR_HANDLER);
                }
                throw;
            }
        }
    }
}
