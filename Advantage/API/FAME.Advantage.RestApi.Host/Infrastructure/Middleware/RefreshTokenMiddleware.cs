﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RefreshTokenMiddleware.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the RefreshTokenMiddleware type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure.Middleware
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Infrastructure.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;

    using Jose;

    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;

    using Newtonsoft.Json;

    /// <summary>
    /// The refresh token middleware will take the current token provided on the Context Request Header and will return a Refreshed Token on the Response Headers.
    /// </summary>
    public sealed class RefreshTokenMiddleware
    {
        /// <summary>
        /// The next.
        /// </summary>
        private readonly RequestDelegate next;

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshTokenMiddleware"/> class.
        /// </summary>
        /// <param name="next">
        /// The next.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory dependency.
        /// </param>
        public RefreshTokenMiddleware(
            RequestDelegate next,
            ILoggerFactory loggerFactory)
        {
            this.next = next;
            this.logger = loggerFactory.CreateLogger<CustomExceptionHandlerMiddleware>();
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task Invoke(HttpContext context, IConfigurationRoot configuration)
        {
            try
            {
                if (context.Request.Headers.ContainsKey(ServiceDefaults.AUTHORIZATION) || context.Request.Headers.ContainsKey(ServiceDefaults.AUTHORIZATION.ToLower()))
                {
                    string token = context.Request.Headers[ServiceDefaults.AUTHORIZATION];

                    if (!string.IsNullOrEmpty(token))
                    {
                        var authSettings = configuration.GetSection("AuthService").Get<AuthServiceSettings>();

                        if (authSettings != null)
                        {
                            if (token.ToLower().Contains("bearer "))
                            {
                                var tokenString = token.Split(' ');
                                token = tokenString[tokenString.Length - 1];
                            }
                            var decodedToken = JWT.Decode(token, Encoding.ASCII.GetBytes(authSettings.SecretKey), JwsAlgorithm.HS256);

                            if (!string.IsNullOrEmpty(decodedToken))
                            {
                                var authorization = AuthToken.FromDictionary(
                                    JsonConvert.DeserializeObject<Dictionary<string, object>>(decodedToken));

                                if (authorization != null)
                                {
                                    var refreshedTokenDictionary = new AuthToken(
                                        authorization.Subject,
                                        authorization.Issuer,
                                        authorization.Audience,
                                        DateTime.UtcNow,
                                        DateTime.UtcNow.AddMinutes(authSettings.TokenLifetime),
                                        authorization.Roles,
                                        authorization.UserId,
                                        authorization.Tenant).ToDictionary();

                                   var refreshedToken = JWT.Encode(
                                        refreshedTokenDictionary,
                                        Encoding.ASCII.GetBytes(authSettings.SecretKey),
                                        JwsAlgorithm.HS256);

                                    context.Response.Headers.Add(ServiceDefaults.AUTHORIZATION, refreshedToken);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (this.logger != null)
                {
                    this.logger.LogError(e.ToString());
                }
                else
                {
                    throw e;
                }
            }

            await this.next(context);
        }
    }
}
