﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SiteRegistry.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the SiteRegistry type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Infrastructure
{
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;

    using Fame.EFCore.Tenant;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Orm.AdvTenant;
    using FAME.Orm.AdvTenant.Interfaces;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;

    using Newtonsoft.Json;

    using StructureMap;

    /// <summary>
    /// The site registry.
    /// </summary>
    public class SiteRegistry : Registry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteRegistry"/> class.
        /// </summary>
        public SiteRegistry()
        {
            this.For<TenantDbSettings>().Use(c => c.GetInstance<IConfigurationRoot>().GetSection("TenantDb").Get<TenantDbSettings>());

            this.For<IAdvantageUnitOfWork>().Use<AdvantageUnitOfWork>().ContainerScoped();

            /* Entity Framework Dependency Injection for Tenant Auth Db*/
            this.ForSingletonOf<ITenantDbContext>().Use<TenantAuthDbContext>().Transient()
                .Ctor<DbContextOptions>("options")
                .Is(c => c.GetInstance<TenantDbSettings>().BuildConnectContextOptionsBuilder().Options);

            this.For<IAdvantageDbContext>().Use<AdvantageDbContext>().ContainerScoped();
            this.For<IReportServerCredentials>().Use(new ReportServerCredentials()).ContainerScoped();
            this.For<IReportConnection>().Use(new ReportConnection()).ContainerScoped();
            this.For<IReportRequest>().Use(new ReportRequest()).ContainerScoped();
            this.For<IReportServerSettings>().Use(c => this.GetInstance<ReportServerSettings>(c, "ReportServer"));
            this.For<IIntegrationSettings>().Use(c => this.GetInstance<IntegrationSettings>(c, "AFAIntegration"));
            this.For<IContext>().Use(c => c);
        }

        /// <summary>
        /// The get instance of a config section.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="configKeyName">
        /// The config key name.
        /// </param>
        /// <typeparam name="T">
        /// The type to return  while de-serializing the config value
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private T GetInstance<T>(IContext context, string configKeyName)
        {
            var section = context.GetInstance<IConfigurationRoot>().GetSection(configKeyName).Get<T>();

            if (section == null)
            {
                section = context.GetInstance<T>();
            }

            return section;
        }
    }
}
