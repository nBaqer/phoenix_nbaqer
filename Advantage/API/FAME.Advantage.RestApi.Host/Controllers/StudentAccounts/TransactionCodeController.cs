﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransactionCodeController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The TransactionCode Controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;
using FAME.Advantage.RestApi.Host.Services.StudentAccounts;

namespace FAME.Advantage.RestApi.Host.Controllers.StudentAccounts
{
    using FAME.Advantage.RestApi.Host.Extensions;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Authorization;
    /// <summary>
    /// The TransactionCode controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/FinancialAid/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class TransactionCodeController : BaseController
    {
        /// <summary>
        /// the transction code service
        /// </summary>
        private readonly ITransactionCodeService transactionCodeService;

        /// <summary>
        /// transcation code controller constructor
        /// </summary>
        /// <param name="transactionCodeService"></param>
        public TransactionCodeController(ITransactionCodeService transactionCodeService)
        {
            this.transactionCodeService = transactionCodeService;
        }

        /// <summary>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair.
        /// </summary>
        /// <remarks>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair,
        /// suitable for a dropdown.
        /// </remarks>
        /// <response code="200">Returns program versions as value/text</response>
        /// <response code="404">Returns no content when there are no program versions found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "FA-Read")]
        [Route("GetTransactionCodeForChargingMethod")]
        public async Task<IActionResult> GetTransactionCodeForChargingMethod(Guid campusId)
        {
            return campusId.IsEmpty() ? this.MissingRequiredParamResult("CampusId") : this.Ok(await this.transactionCodeService.GetTransactionCodeForChargingMethod(campusId));
        }
    }
}
