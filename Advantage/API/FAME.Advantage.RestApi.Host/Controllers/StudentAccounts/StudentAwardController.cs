﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwardController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentAwardController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
using FAME.Advantage.RestApi.Host.Services.Interfaces.FinancialAid;

namespace FAME.Advantage.RestApi.Host.Controllers.FinancialAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;

    /// <summary>
    /// The student award controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/FinancialAid/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentAwardController : BaseController
    {
        /// <summary>
        /// The student award service.
        /// </summary>
        private readonly IStudentAwardsService studentAwardService;

        /// <summary>
        /// Initializes a new instance of the StudentAwardController class.
        /// </summary>
        /// <param name="studentAwardService">
        /// The student award service.
        /// </param>
        public StudentAwardController(IStudentAwardsService studentAwardService)
        {
            this.studentAwardService = studentAwardService;
        }

        /// <summary>
        /// Updates the student awards in advantage.
        /// </summary>
        /// <remarks>
        /// Requires the list of student awards to post.
        /// </remarks>
        /// <response code="200">Returns the list of results that could not be posted</response>
        /// <response code="404">Data not found</response> 
        /// <param name="studentAwards">
        /// The collection of student awards to create or update.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of key value pairs for all failed transactions where the key is the enrollment id and the value is the list of associated student awards.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "FA-Modify")]
        [Route("CreateUpdateStudentAwards")]
        public async Task<IActionResult> CreateUpdateStudentAwards([FromBody]IEnumerable<DataTransferObjects.StudentAccounts.AFA.StudentAwards> studentAwards)
        {
            var enumerable = studentAwards.ToList();
            return !enumerable.Any() ? this.Ok(this.MissingRequiredParamResult("student awards"))
                       : this.Ok(await this.studentAwardService.CreateUpdateStudentAwards(enumerable));
        }

        /// <summary>
        /// Updates the student disbursements in advantage.
        /// </summary>
        /// <remarks>
        /// Requires the list of student disbursements to post.
        /// </remarks>
        /// <response code="200">Returns the list of results that could not be posted</response>
        /// <response code="404">Data not found</response> 
        /// <param name="disbursements">
        /// The collection of student disbursements to create or update.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of key value pairs for all failed transactions where the key is the enrollment id and the value is the list of associated student disbursements.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "FA-Modify")]
        [Route("CreateUpdateStudentDisbursements")]
        public async Task<IActionResult> CreateUpdateStudentDisbursements([FromBody]IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements)
        {
            var enumerable = disbursements.ToList();
            return !enumerable.Any() ? this.Ok(this.MissingRequiredParamResult("disbursements"))
                : this.Ok(await this.studentAwardService.CreateUpdateStudentDisbursements(enumerable));
        }

    }
}