﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigCampusController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The file configuration campus controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Maintenance;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The file config feature controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Maintenance/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class FileConfigCampusController : BaseController
    {
        /// <summary>
        /// The file config campus service.
        /// </summary>
        private readonly IFileConfigCampusService fileConfigCampusService;

        /// <summary>
        /// Initializes a new instance of the FileConfigCampusController class.
        /// </summary>
        /// <param name="fileConfigCampusService">
        /// The file config campus service.
        /// </param>
        public FileConfigCampusController(IFileConfigCampusService fileConfigCampusService)
        {
            this.fileConfigCampusService = fileConfigCampusService;
        }

        /// <summary>
        /// Gets a list of all active features for system file configuration.
        /// </summary>
        /// <returns>
        /// Returns list of features for file configuration. 
        /// </returns>
        [HttpGet]
        [Route("GetByCampusGroup")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByCampusGroup(Guid campusGroupId)
        {
            var result = await this.fileConfigCampusService.Get(campusGroupId);
            if(result == null)
            {
                return this.Ok();
            }
            return this.Ok(result);
        }


        /// <summary>
        /// saves system file configuration for a campus group.
        /// </summary>
        /// <returns>
        /// Returns an object representing the saved file configuration. 
        /// </returns>
        [HttpPost]
        [Route("Save")]
        [Authorize(Roles = "SY-Modify")]
        public async Task<IActionResult> Save([FromBody]CampusFileConfig CampusFileConfig)
        {
            return this.Ok(await this.fileConfigCampusService.Save(CampusFileConfig));
        }

        /// <summary>
        /// Deletes the system file configuration.
        /// </summary>
        /// <returns>
        /// Returns boolean based on result of deletion, true if successful. 
        /// </returns>
        [HttpPost]
        [Route("Delete")]
        [Authorize(Roles = "SY-Delete")]
        public async Task<IActionResult> Delete([FromBody]Guid campusFileConfigurationId)
        {
            return this.Ok(await this.fileConfigCampusService.Delete(campusFileConfigurationId));
        }
    }
}