﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigFeaturesController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The file configuration features controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Maintenance;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The file config feature controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Maintenance/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class FileConfigFeaturesController : BaseController
    {
        /// <summary>
        /// The file config feature service.
        /// </summary>
        private readonly IFileConfigFeatureService fileConfigFeatureService;

        /// <summary>
        /// Initializes a new instance of the FileConfigFeaturesController class.
        /// </summary>
        /// <param name="fileConfigFeatureService">
        /// The file config feature service.
        /// </param>
        public FileConfigFeaturesController(IFileConfigFeatureService fileConfigFeatureService)
        {
            this.fileConfigFeatureService = fileConfigFeatureService;
        }

        /// <summary>
        /// Gets a list of all active features for system file configuration.
        /// </summary>
        /// <returns>
        /// Returns list of features for file configuration. 
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.fileConfigFeatureService.Get());
        }
    }
}