﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Controllers.Maintenance.Wapi
{
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Maintenance.Wapi;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/v1/Maintenance/Wapi/[controller]")]
    public class WapiController : BaseController
    {
        private IWapiServiceService wapiService;

        public WapiController(IWapiServiceService wapiService)
        {
            this.wapiService = wapiService;
        }

        [HttpGet]
        [Route("GetWapiSettings")]
        [Authorize(Roles = "SY-Read")]
        public WapiSettings GetWapiSettings(string codeOperation)
        {
            return this.wapiService.GetWapiSettings(codeOperation);
        }
    }
}
