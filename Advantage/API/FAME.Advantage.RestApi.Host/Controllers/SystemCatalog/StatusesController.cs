﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusesController.cs" company="Fame Inc">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StatusesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The system statuses controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/Statuses")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StatusesController : BaseController
    {
        /// <summary>
        /// The system status service that works with the system statuses that shows if the status is active or inactive.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusesController"/> class.
        /// </summary>
        /// <param name="statusesService">
        /// The system statuses service.
        /// </param>
        public StatusesController(IStatusesService statusesService)
        {
            this.statusService = statusesService;
        }

        /// <summary>
        /// The get status list that indicates the value of active/inactive statuses.
        /// </summary>
        /// <response code="200">Returns the list of students found</response>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show the status (Active/Inactive) and TValue is of type Guid and will have the StatusId
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.statusService.GetAllSystemStatuses());
        }
    }
}
