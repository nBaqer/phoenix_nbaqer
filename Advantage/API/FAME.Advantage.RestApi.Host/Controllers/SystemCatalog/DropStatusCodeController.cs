﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatusCodeController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DropStatusController type. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The status controller
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/DropStatus")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class DropStatusCodeController : BaseController
    {
        /// <summary>
        /// The dropStatus service.
        /// </summary>
        private readonly IDropStatusCodeService dropStatusCodeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropStatusCodeController"/> class.
        /// </summary>
        /// <param name="dropStatusCodeService">
        /// The dropStatus service.
        /// </param>
        public DropStatusCodeController(IDropStatusCodeService dropStatusCodeService)
        {
            this.dropStatusCodeService = dropStatusCodeService;
        }

        /// <summary>
        /// The Get drop status action allows you to find a list of status on a given campus.
        /// </summary>
        /// <remarks>
        /// The Get drop status action requires drop Status object with campusId.
        /// </remarks>
        /// <response code="200">Returns the list of status found</response>
        /// <response code="400">Returns the Validation error messages when the search data provided is invalid</response>
        /// <response code="0">Returns when there is no response from server</response>
        /// <param name="dropStatus">
        /// The drop status search filter includes the campusId.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="IListItem{TText,TValue}"/>  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        [HttpGet("GetDropStatusCode")]
        [Authorize(Roles = "AR-Read")]
        [ValidateModel]
        public async Task<IActionResult> Get(DropStatus dropStatus)
        {
            return this.Ok(await this.dropStatusCodeService.GetDropStatusByCampusId(dropStatus));
        }
    }
}
