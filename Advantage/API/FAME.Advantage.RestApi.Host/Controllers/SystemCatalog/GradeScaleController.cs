﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradeScaleController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GradeScaleController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The grade scale controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class GradeScaleController : BaseController
    {
        /// <summary>
        /// The grade scale service.
        /// </summary>
        private readonly IGradeScaleService gradeScaleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradeScaleController"/> class.
        /// </summary>
        /// <param name="gradeScaleService">
        /// The grade scale service.
        /// </param>
        public GradeScaleController(IGradeScaleService gradeScaleService)
        {
            this.gradeScaleService = gradeScaleService;
        }

        /// <summary>
        /// The get grade scales by grade system id.
        /// </summary>
        /// <param name="gradeSystemId">
        /// The grade system id.
        /// </param>
        /// <returns>
        /// The <see cref="IList{T}"/> where T is of type <code>IListItem{text,value}</code>.
        /// </returns>
        [HttpGet]
        [Route("GetGradeScales")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetGradeScales(Guid gradeSystemId)
        {
            if (gradeSystemId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("gradeSystemId"));
            }

            return this.Ok(await this.gradeScaleService.GetGradeScales(gradeSystemId));
        }
    }
}
