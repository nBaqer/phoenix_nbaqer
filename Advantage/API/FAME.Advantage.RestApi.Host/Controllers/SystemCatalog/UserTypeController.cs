﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTypeController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user type controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The user type controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class UserTypeController : BaseController
    {
        /// <summary>
        /// The user type service.
        /// </summary>
        private readonly IUserTypeService userTypeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserTypeController"/> class.
        /// </summary>
        /// <param name="userTypeService">
        /// The user type service.
        /// </param>
        public UserTypeController(IUserTypeService userTypeService)
        {
            this.userTypeService = userTypeService;
        }

        /// <summary>
        /// The get returns a list of user types for a dropdown.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetUserTypes")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetUserTypes()
        {

            return this.Ok(await this.userTypeService.GetUserTypes());
        }
    }
}
