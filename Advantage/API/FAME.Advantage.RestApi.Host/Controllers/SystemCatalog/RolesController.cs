﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Roles Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The roles controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class RolesController : BaseController
    {
        /// <summary>
        /// The roles service.
        /// </summary>
        private IRolesService rolesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RolesController"/> class.
        /// </summary>
        /// <param name="rolesService">
        /// The roles service.
        /// </param>
        public RolesController(IRolesService rolesService)
        {
            this.rolesService = rolesService;
        }

        /// <summary>
        /// The Get All Roles return all roles in the database
        /// </summary>
        /// <remarks>
        /// The Get All Roles return all roles found in the  database
        /// </remarks> 
        /// <response code="200">Returns the list of all roles in the database.</response>
        /// <response code="400">If the list of roles is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAllRoles")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.rolesService.GetAllRoles());
        }

        /// <summary>
        /// The Get Roles by User Type returns all active roles for the specificed user type
        /// </summary>
        /// <param name="userTypeId">
        /// The user Type Id.
        /// </param>
        /// <remarks>
        /// Returns all active roles for the specificed user type
        /// </remarks>
        /// <response code="200">Returns the list of roles for the given user type</response>
        /// <response code="400">If the list of roles is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the role type and TValue is of type Guid and will have the RoleId
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetRolesByUserType")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByUserType(int? userTypeId)
        {
            return this.Ok(await this.rolesService.GetRolesByUserType(userTypeId));
        }
    }
}