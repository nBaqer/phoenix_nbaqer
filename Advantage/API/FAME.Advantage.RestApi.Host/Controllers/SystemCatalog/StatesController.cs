﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatesController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The states controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The states controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StatesController : Controller
    {
        /// <summary>
        /// The states service.
        /// </summary>
        private readonly IStatesService statesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatesController"/> class.
        /// </summary>
        /// <param name="statesService">
        /// The states service.
        /// </param>
        public StatesController(IStatesService statesService)
        {
            this.statesService = statesService;
        }


        /// <summary>
        /// The Get return all state board states in the database
        /// </summary>
        /// <remarks>
        /// The Get return all state board states found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all state board states in the database.</response>
        /// <response code="400">If the list of state board states is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the state board state name and TValue is of type Guid and will have the state board state id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetStateBoardStates")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.statesService.GetStateBoardStates());
        }


        /// <summary>
        /// The Get return all states in the database
        /// </summary>
        /// <remarks>
        /// The Get return all states found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all states in the database.</response>
        /// <response code="400">If the list of states is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the states name and TValue is of type Guid and will have the states id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAllStates")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetAllStates()
        {
            return this.Ok(await this.statesService.GetAllStates());
        }

        /// <summary>
        /// The Get return all states short descriptions in the database
        /// </summary>
        /// <remarks>
        /// The Get return all states short descriptions found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all states short descriptions in the database.</response>
        /// <response code="400">If the list of states is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the states short name and TValue is of type Guid and will have the states id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAllStatesShortDescription")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetAllStatesShortDescription()
        {
            return this.Ok(await this.statesService.GetAllStatesShortDescription());
        }

        /// <summary>
        /// The get states by country id.
        /// </summary>
        /// <param name="countryId">
        /// The country id.
        /// </param>
        /// <response code="200">Returns the list of all states that belong to a country in the database based on the country id.</response>
        /// <response code="400">If the list of states for a country is null</response>  
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetStatesByCountryId")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetStatesByCountryId(Guid? countryId)
        {
            return this.Ok(await this.statesService.GetStatesByCountryId(countryId));
        }
    }
}