﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardCourseController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The state board course controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The state board course controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StateBoardCourseController : BaseController
    {
        /// <summary>
        /// The state board course service.
        /// </summary>
        private readonly IStateBoardCourseService stateBoardCourseService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardCourseController"/> class.
        /// </summary>
        /// <param name="stateBoardCourseService">
        /// The state board course service.
        /// </param>
        public StateBoardCourseController(IStateBoardCourseService stateBoardCourseService)
        {
            this.stateBoardCourseService = stateBoardCourseService;
        }

        /// <summary>
        /// The GetByStateBoardId return all active state board courses by the state id.
        /// </summary>
        /// <param name="stateId">
        /// The state Id.
        /// </param>
        /// <remarks>
        /// The GetByStateBoardId return all active state board courses by the state id.
        /// </remarks>
        /// <response code="200">Returns the list of all active state board courses by the state id.</response>
        /// <response code="400">If the list of state board courses is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the state board course description and TValue is of type Guid and will have the state board course Id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetByStateId")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByStateId(Guid stateId)
        {
            return this.Ok(await this.stateBoardCourseService.GetByStateId(stateId));
        }
    }
}