﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The campus controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The campus controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class CampusController : BaseController
    {
        /// <summary>
        /// The campus service.
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusController"/> class.
        /// </summary>
        /// <param name="campusService">
        /// The campus service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus group service.
        /// </param>
        public CampusController(ICampusService campusService, ICampusGroupService campusGroupService)
        {
            this.campusService = campusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The Get return all campuses in the database
        /// </summary>
        /// <remarks>
        /// The Get return all campuses found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all campuses in the database.</response>
        /// <response code="400">If the list of campuses is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.campusService.GetAllCampuses());
        }

        /// <summary>
        /// The Get return all campuses in the database for the logged in UserId.
        /// </summary>
        /// <remarks>
        /// The Get return all campuses found in the database for the logged in UserId.
        /// </remarks> 
        /// <response code="200">Returns the list of all campuses in the database for the given UserId.</response>
        /// <response code="400">If the list of campuses is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAllCampusesByUserId")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetAllCampusesByUserId()
        {
            return this.Ok(await this.campusService.GetAllCampusesByUserId());
        }

        /// <summary>
        /// The get campus id for CMS id.
        /// </summary>
        /// <param name="cmsId">
        /// The Client Management Software id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCampusIdForCmsId")]
        [Authorize(Roles = "SY-Read")]
        public Guid GetCampusIdForCmsId(string cmsId)
        {
            return this.campusService.GetCampusIdForCmsId(cmsId);
        }

        /// <summary>
        /// The get cms ids to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCmsIdsToSync")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IList<string>> GetCmsIdsToSync()
        {
            return await this.campusService.GetCmsIdsToSync();
        }



        /// <summary>
        /// The get Client Management Software id for campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCmsIdForCampusId")]
        [Authorize(Roles = "SY-Read")]
        public string GetCmsIdForCampusId(string campusId)
        {
            return this.campusService.GetCmsIdForCampusId(campusId);
        }

        /// <summary>
        /// Returns school name given campusId
        /// </summary>
        /// <param name="campusId">
        /// The status id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "REPORTSAPI-Read")]
        [Route("GetCampusById")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetCampusById(string campusId)
        {
            if (campusId == null)
                return this.MissingRequiredParamResult(campusId);

            var actionResult = await this.campusService.GetCampusById(campusId);

            if (actionResult.Result == null)
                return this.NotFound(campusId);

            return Ok(actionResult);
        }
        /// <summary>
        /// Returns school name given campusId
        /// </summary>
        /// <param name="campusId">
        /// The status id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("SY-Read")]
        [Route("GetCampusByIdXML")]
        [Produces("application/xml")]
        public async Task<IActionResult> GetCampusByIdXML(string token, string campusId)
        {
            return await GetCampusById(campusId);
        }


        /// <summary>
        /// Returns Campus Groups With Campuses.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetCampusGroupsWithCampus")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetCampusGroupsWithCampus()
        {
            var actionResult = await this.campusService.GetCampusGroupsWithCampus();
            return Ok(actionResult);
        }
    }

}