﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The program controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ProgramController : BaseController
    {
        /// <summary>
        /// The program service.
        /// </summary>
        private readonly IProgramService programService;

       

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramController"/> class.
        /// </summary>
        /// <param name="programService">
        /// The program service.
        /// </param>
        public ProgramController(IProgramService programService)
        {
            this.programService = programService;
        }

        /// <summary>
        /// The GetClockHourProgramsByCampus return all active clock hour programs for the specified campus
        /// </summary>
        /// <remarks>
        /// The GetClockHourProgramsByCampus return all active clock hour programs for the specified campus
        /// </remarks> 
        /// <response code="200">Returns the list of all active clock hour programs for the specified campus.</response>
        /// <response code="400">If the list of clock hour programs is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the program description and TValue is of type Guid and will have the Program Id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetClockHourProgramsByCampus")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetClockHourProgramsByCampus(Guid campusId)
        {
            return this.Ok(await this.programService.GetClockHourProgramsByCampus(campusId));
        }

        /// <summary>
        /// The get all active Program and Program version by campus id method returns the program and program versions by given campus Id as a text/value pair.
        /// </summary>
        /// <response code="200">Returns program and program versions versions as value/text</response>
        /// <response code="404">Returns no content when there are no program versions found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramAndProgramVersionByCampusGrpId")]
        public async Task<IActionResult> GetProgramAndProgramVersionByCampusGrpId(Guid campusGroupId)
        {
            return campusGroupId.IsEmpty() ? this.MissingRequiredParamResult("CampusGroupId") : this.Ok(await this.programService.GetProgramAndProgramVersionForCampGrp(campusGroupId));
        }

        /// <summary>
        /// Returns program description by programId
        /// </summary>
        /// <param name="programId">
        /// The program id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "REPORTSAPI-Read")]
        [Route("GetProgramById")]
        public async Task<IActionResult> GetProgramById(string programId)
        {
            if (programId == null)
                return this.NotFound(programId);

            var actionResult = await this.programService.GetProgramById(programId);
            return Ok(actionResult);
        }
    }
}