﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusCodesController.cs" company="Fame Inc">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StatusCodesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// The system statuses controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/StatusCodes")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StatusCodesController : BaseController
    {
        /// <summary>
        /// The status codes service
        /// </summary>
        private readonly IStatusCodesService statusCodesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusCodesController"/> class.
        /// </summary>
        /// <param name="statusCodesService">
        /// The system statuses service.
        /// </param>
        public StatusCodesController(IStatusCodesService statusCodesService)
        {
            this.statusCodesService = statusCodesService;
        }

        /// <summary>
        /// Returns status code description given statusCodeId
        /// </summary>
        /// <param name="statusCodeId">
        /// The status id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "REPORTSAPI-Read")]
        [Route("GetStatusCodeById")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetStatusCodeById(string statusCodeId)
        {
            if (statusCodeId == null)
                return this.MissingRequiredParamResult(statusCodeId);

            var actionResult = await this.statusCodesService.GetStatusCodeById(statusCodeId);

            if (actionResult.Result == null)
                return this.NotFound(statusCodeId);

            return Ok(actionResult);
        }

        /// <summary>
        /// Get status codes for tenant, or filter by campus group id or campus id.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAllStatusCodes")]
        public async Task<IActionResult> GetAllStatusCodes(Guid? campusGroupId = null, Guid? campusId = null)
        {
            var actionResult = await this.statusCodesService.GetStatusCodes(campusGroupId, campusId);

            if (actionResult.ResultStatus != DataTransferObjects.Common.Enums.ResultStatus.Success)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult);
        }
    }
}
