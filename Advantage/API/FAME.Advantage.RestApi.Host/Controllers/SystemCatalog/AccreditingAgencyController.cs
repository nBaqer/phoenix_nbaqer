﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccreditingAgencyController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The accrediting agency controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;

    /// <summary>
    /// The accrediting agency controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class AccreditingAgencyController : BaseController
    {
        /// <summary>
        /// The accrediting agency service.
        /// </summary>
        private readonly IAccreditingAgencyService accreditingAgencyService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccreditingAgencyController"/> class.
        /// </summary>
        /// <param name="accreditingAgencyService">
        /// The accrediting agency service.
        /// </param>
        public AccreditingAgencyController(IAccreditingAgencyService accreditingAgencyService)
        {
            this.accreditingAgencyService = accreditingAgencyService;
        }

        /// <summary>
        /// The GetAccreditingAgenciesByCampusId returns all accrediting agencies available for a given campus
        /// </summary>
        /// <returns>
        /// Returns a list of accrediting agencies
        /// <see cref="IActionResult"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAccreditingAgencies")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetAccreditingAgencies()
        {
            var actionResult = await this.accreditingAgencyService.GetAccreditingAgencies();

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult.Result);
        }
    }
}