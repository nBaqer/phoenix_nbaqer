﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolController.cs" company="Fame Inc">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SchoolController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The system statuses controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class SchoolController : BaseController
    {
        /// <summary>
        /// The system status service that works with the system statuses that shows if the status is active or inactive.
        /// </summary>
        private readonly ISchoolService schoolService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolController"/> class.
        /// </summary>
        /// <param name="schoolService">
        /// The system statuses service.
        /// </param>
        public SchoolController(ISchoolService schoolService)
        {
            this.schoolService = schoolService;
        }


        /// <summary>
        /// The Get School Detail service takes campus id as input and returns all the information related to campus.
        /// </summary>
        /// <remarks>
        /// The Get School Detail  service requires campus Id object of type Guid.
        /// </remarks>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <response code="200">Returns the school detail found</response> 
        /// <returns>
        /// The SchoolDetail, this object contails school name, address, phone numbers, fax, email, website and school logo details
        /// </returns>
        [HttpGet]
        [AuthorizeReport("SY-Read")]
        [Route("GetDetail")]
        [Produces("application/xml")]
        public async Task<IActionResult> GetDetail(string token, Guid campusId)
        {
            return await GetSchoolDetails(token, campusId);
        }
        /// <summary>
        /// The Get School Detail service takes campus id as input and returns all the information related to campus.
        /// </summary>
        /// <remarks>
        /// The Get School Detail  service requires campus Id object of type Guid.
        /// </remarks>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <response code="200">Returns the school detail found</response> 
        /// <returns>
        /// The SchoolDetail, this object contails school name, address, phone numbers, fax, email, website and school logo details as a Json.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("SY-Read")]
        [Route("GetDetailJson")]

        public async Task<IActionResult> GetDetailJson(string token, Guid campusId)
        {
            return await GetSchoolDetails(token, campusId);
        }
        private async Task<IActionResult> GetSchoolDetails(string token, Guid campusId)
        {
            var missingParam = new List<BadRequestObjectResult>();
            if (token == string.Empty)
            {
                missingParam.Add(this.MissingRequiredParamResult("token"));
            }

            if (campusId.IsEmpty())
            {
                missingParam.Add(this.MissingRequiredParamResult("Campus Id"));
            }

            if (missingParam.Count > 0)
            {
                return this.Ok(missingParam);
            }

            return this.Ok(await this.schoolService.GetDetail(campusId));

        }

        /// <summary>
        /// The clear school cash.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("SY-Read")]
        [Route("ClearSchoolCash")]

        public async Task<IActionResult> ClearSchoolCash(string token, Guid campusId)
        {
            return this.Ok(await this.schoolService.ClearSchoolCash(campusId));
        }


    }
}
