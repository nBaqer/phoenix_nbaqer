﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstructorsController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Instructors Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The Instructors controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class InstructorsController : BaseController
    {
        /// <summary>
        /// The Instructors service.
        /// </summary>
        private readonly IInstructorsService instructorsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructorsController"/> class.
        /// </summary>
        /// <param name="InstructorsService">
        /// The widget service.
        /// </param>
        public InstructorsController(IInstructorsService instructorsService) //Instructors
        {
            this.instructorsService = instructorsService;
        }


        /// <summary>
        /// Returns Get All Instructors.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetAllInstructors")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetAllInstructors(Guid campusGroupId)
        {
            if (campusGroupId == null || campusGroupId == Guid.Empty)
                return this.MissingRequiredParamResult("campusGroupId");

            var actionResult = await this.instructorsService.GetAllInstructors(campusGroupId);
            return Ok(actionResult);
        }
    }
}