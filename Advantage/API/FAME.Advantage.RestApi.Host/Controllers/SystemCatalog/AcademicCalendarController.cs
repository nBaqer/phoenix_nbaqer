﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicCalendarController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The academic calendar controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The academic calendar controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class AcademicCalendarController : BaseController
    {
        /// <summary>
        /// The academic calendar service.
        /// </summary>
        private readonly IAcademicCalendarService academicCalendarService;

        /// <summary>
        /// Initializes a new instance of the AcademicCalendarController class.
        /// </summary>
        /// <param name="academicCalendarService">
        /// The academic calendar service.
        /// </param>
        public AcademicCalendarController(IAcademicCalendarService academicCalendarService)
        {
            this.academicCalendarService = academicCalendarService;
        }

        /// <summary>
        /// The IsClockHour action allows you to validate whether the selected enrollment is of program type clock hour or credit hour.
        /// </summary>
        /// <remarks>
        /// The IsClockHour action requires studentEnrollmentId to check whether it is of program type clock hour or credit hour.
        /// </remarks>
        /// <response code="200">Returns true if the Enrolled program is of type clock hour else returns false.</response>
        /// <response code="400">Returns the Validation error message when the input student enrollment id is empty.</response>
        /// <param name="studentEnrollmentId">
        /// The studentEnrollmentId is of type Guid is the selected enrollment id of the specified student.
        /// </param>
        /// <returns>
        /// Returns true if the given enrollment is of program type clock hour else returns false if it is of program type credit hour. 
        /// </returns>
        [HttpGet]
        [Route("IsClockHour")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> IsClockHour(Guid studentEnrollmentId)
        {
            if (studentEnrollmentId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("EnrollmentId"));
            }

            return this.Ok(await this.academicCalendarService.IsClockHour(studentEnrollmentId));
        }
    }
}