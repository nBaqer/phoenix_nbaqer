﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The state board agency controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The state board agency controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StateBoardAgencyController : BaseController
    {
        /// <summary>
        /// The state board agency service.
        /// </summary>
        private readonly IStateBoardAgencyService stateBoardAgencyService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardAgencyController"/> class.
        /// </summary>
        /// <param name="stateBoardAgencyService">
        /// The state board agency service.
        /// </param>
        public StateBoardAgencyController(IStateBoardAgencyService stateBoardAgencyService)
        {
            this.stateBoardAgencyService = stateBoardAgencyService;
        }

        /// <summary>
        /// The GetByStateId returns all state board agencies for the specified state id in the database
        /// </summary>
        /// <remarks>
        /// The GetByStateId returns all state board agencies for the specified state id in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all state board agencies for the specified state id in the database.</response>
        /// <response code="400">If the list of state board agencies is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the state board agency and TValue is of type Guid and will have the State board agency id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetByStateId")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByStateId(Guid stateId)
        {
            return this.Ok(await this.stateBoardAgencyService.GetStateBoardAgenciesByStateId(stateId));
        }

        [HttpGet]
        [Route("GetStateBoardAgency")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetStateBoardAgency(int? agencyId)
        {
            return this.Ok(await this.stateBoardAgencyService.GetStateBoardAgency(agencyId));
        }
    }
}