﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The campus group controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The campus group controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class CampusGroupController : BaseController
    {
        /// <summary>
        /// The campus service.
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroupController"/> class.
        /// </summary>
        /// <param name="campusService">
        /// The campus service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus group service
        /// </param>
        public CampusGroupController(ICampusService campusService, ICampusGroupService campusGroupService)
        {
            this.campusService = campusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The Get return all campus groups in the database
        /// </summary>
        /// <remarks>
        /// The Get return all campus groups found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all campus groups in the database.</response>
        /// <response code="400">If the list of campus groups is null</response>  
        /// <returns>
        /// Returns an IEnumerable of ""  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.campusGroupService.GetAllCampusGroups());
        }
    }
}