﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WidgetController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Widget Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The widget controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class WidgetController : BaseController
    {
        /// <summary>
        /// The widget service.
        /// </summary>
        private readonly IWidgetService widgetService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetController"/> class.
        /// </summary>
        /// <param name="widgetService">
        /// The widget service.
        /// </param>
        public WidgetController(IWidgetService widgetService)
        {
            this.widgetService = widgetService;
        }

        /// <summary>
        /// Returns list of available widgets along with settings (if exists) for a user given the resouce and campus a user is currently accessing.
        /// </summary>
        /// <param name="resourceId">
        /// Resouce id of the user.
        /// </param>
        /// <param name="campusId">
        /// Campus id of the user.
        /// </param>
        /// <response code="200">Returns list of available widgets along with settings (if exists) for a user.</response>
        /// <response code="400">Bad request returned if missing parameters.</response>  
        /// <response code="500">If null is returned from service. Exception was thrown.</response>  
        /// <returns>
        /// The <see cref="IEnumerable{Widget}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetUserDashboardWidgets")]
        [Authorize(Roles = "AR-Read, SY-Read")]
        public async Task<IActionResult> GetUserDashboardWidgets(int resourceId, Guid campusId)
        {
            if (resourceId == 0 || campusId == Guid.Empty)
            {
                return this.BadRequest($"{ApiMsgs.IncorrectParametersPassed} Parameters resourceId and campusId required.");
            }

            IEnumerable<Widget> returnData = await this.widgetService.GetUserResourceWidgets(resourceId, campusId);

            if (returnData == null)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, ApiMsgs.UnableToRetrieveWidgets);
            }
            else
            {
                return this.Ok(returnData);
            }
        }
    }
}