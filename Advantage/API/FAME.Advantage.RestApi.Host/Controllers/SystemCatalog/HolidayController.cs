﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HolidayController.cs" company="Fame Inc">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the HolidayController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The system statuses controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class HolidayController : BaseController
    {
        /// <summary>
        /// The holiday service.
        /// </summary>
        private readonly IHolidayService holidayService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HolidayController"/> class.
        /// </summary>
        /// <param name="holidayService">
        /// The holiday service.
        /// </param>
        public HolidayController(IHolidayService holidayService)
        {
            this.holidayService = holidayService;
        }


     
        /// <summary>
        /// The get holiday list by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("SY-Read")]
        [Route("GetHolidayListByCampus")]
        public async Task<IActionResult> GetHolidayListByCampus(Guid campusId)
        {
            return this.Ok(await this.holidayService.GetHolidayListByCampus(campusId));
        }


    }
}
