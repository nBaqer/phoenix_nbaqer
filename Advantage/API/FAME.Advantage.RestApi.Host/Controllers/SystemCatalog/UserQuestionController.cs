﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserQuestionController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user question controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The user type controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class UserQuestionController : BaseController
    {
        /// <summary>
        /// The user question service.
        /// </summary>
        private readonly IUserQuestionService userQuestionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserQuestionController"/> class.
        /// </summary>
        /// <param name="userQuestionService">
        /// The user question service.
        /// </param>
        public UserQuestionController(IUserQuestionService userQuestionService)
        {
            this.userQuestionService = userQuestionService;
        }

        /// <summary>
        /// The get returns a list of user questions for a dropdown.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("GetUserQuestions")]
        public async Task<IActionResult> GetUserQuestions()
        {
            return this.Ok(await this.userQuestionService.GetUserQuestions());
        }
    }
}
