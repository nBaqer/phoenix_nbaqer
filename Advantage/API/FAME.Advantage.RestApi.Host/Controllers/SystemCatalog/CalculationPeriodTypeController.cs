// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationPeriodTypeController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The calculation period type controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The calculation period type controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class CalculationPeriodTypeController : BaseController
    {
        /// <summary>
        /// The calculation period type service.
        /// </summary>
        private readonly ICalculationPeriodTypeService calculationPeriodTypeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationPeriodTypeController"/> class.
        /// </summary>
        /// <param name="calculationPeriodTypeService">
        /// The calculationPeriodType Service.
        /// </param>
        public CalculationPeriodTypeController(ICalculationPeriodTypeService calculationPeriodTypeService)
        {
            this.calculationPeriodTypeService = calculationPeriodTypeService;
        }

        /// <summary>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations.
        /// </summary>
        /// <remarks>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations. Period types are Payment Period and Period of Enrollment.
        /// R2T4 claculations are performed based on the selected period.
        /// </remarks> 
        /// <response code="200">Returns the list of period types found.</response>
        /// <response code="400">If the list of period types are null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.calculationPeriodTypeService.GetPeriodTypes());
        }

        /// <summary>
        /// The is payment period type action method returns boolen value.
        /// </summary>
        /// <remarks>
        /// The Is Payment Period action method returns boolean value, if the results value match with Payment period then it returns the true other wise the return value is false.
        /// </remarks>
        /// <response code="200">Returns the boolean value true or false</response>
        /// <response code="400">If the periodid is null</response>  
        /// <param name="periodTypeId">
        /// The period type id is the Guid.
        /// </param>
        /// <returns>
        ///  Returns boolean value based on the matching value. If the period type is Payment Period then the return value is true else it is Period of Enrollment then it returns false.
        /// </returns>
        [HttpGet]
        [Route("IsPaymentPeriod")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> IsPaymentPeriodType(Guid periodTypeId)
        {
            if (periodTypeId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("PeriodTypeId"));
            }

            return this.Ok(await this.calculationPeriodTypeService.IsPaymentPeriodType(periodTypeId));
        }
    }
}