﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountriesController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The states controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The countries controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class CountriesController : Controller
    {
        /// <summary>
        /// The countries service.
        /// </summary>
        private readonly ICountriesService countriesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesController"/> class.
        /// </summary>
        /// <param name="countriesService">
        /// The countries service.
        /// </param>
        public CountriesController(ICountriesService countriesService)
        {
            this.countriesService = countriesService;
        }

        /// <summary>
        /// The Get return all countries  in the database
        /// </summary>
        /// <remarks>
        /// The Get return all countries found in the database
        /// </remarks> 
        /// <response code="200">Returns the list of all countries in the database.</response>
        /// <response code="400">If the list of countries is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the countries names and TValue is of type Guid and will have the states id
        /// <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetAllCountries")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetAllCountries()
        {
            return this.Ok(await this.countriesService.GetAllCountries());
        }
    }
}