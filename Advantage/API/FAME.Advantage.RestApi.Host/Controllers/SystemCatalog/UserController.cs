﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the User Controller
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The user controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class UserController : BaseController
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        /// <param name="userService">
        /// The user service.
        /// </param>
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary>
        /// The get returns a list of users.
        /// </summary>
        /// <param name="searchParams">
        /// The Search Parameters.
        /// </param>
        /// <response code="200">Returns the list of all users in the database that met the search paramaeter criteria.</response>
        /// <response code="204">If the list of users is null</response>  
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("SearchUsers")]
        [Authorize(Roles = "SY-Read")]
        [ValidateModel]
        public async Task<IActionResult> SearchUsers(UserSearchInput searchParams)
        {

            return this.Ok(await this.userService.Search(searchParams));
        }

        /// <summary>
        /// The Post action allows your to create a new user into Advantage validating the required.
        /// </summary>
        /// <remarks>
        /// The Post action requires an user object with a user object
        /// </remarks>
        /// <response code="200">Returns the user saved in the system</response>
        /// <response code="400">Returns the Validation error messages when the user to be inserted is invalid</response>
        /// <param name="model">
        /// The user to be inserted.
        /// </param>
        /// <returns>
        /// Returns a user of <see cref="User"/>  
        /// </returns>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = "SY-Create")]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] User model)
        {
            var result = await this.userService.Create(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The Put action allows your to update user's information int Advantage validating the required fields.
        /// </summary>
        /// <remarks>
        /// The Post action requires an user object with a user object
        /// </remarks>
        /// <response code="200">Returns the updated user in the system</response>
        /// <response code="400">Returns the Validation error messages when the user to be updated is invalid</response>
        /// <param name="model">
        /// The user to be inserted.
        /// </param>
        /// <returns>
        /// Returns a user of <see cref="User"/>  
        /// </returns>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = "SY-Modify")]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody]User model)
        {
            var result = await this.userService.Update(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The get user by id, that returns information such as username, user roles, account status
        /// </summary>
        /// <remarks>
        /// The get action requires the user id
        /// </remarks>
        /// <response code="200"/>Returns the information of the user/response>
        /// <response code="204"/>Returns when there is no user matching the id/response>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="User"/>.
        /// </returns>
        [HttpGet]
        [Route("GetById")]
        [Authorize(Roles = "SY-Read")]
        [ValidateModel]
        public async Task<IActionResult> GetById(Guid userId)
        {
            var result = await this.userService.GetUserbyId(userId);
            return this.Ok(result);
        }

        /// <summary>
        /// The get current user type.
        /// </summary>
        /// <remarks>
        /// The get current user type
        /// </remarks>
        /// <returns>
        /// <response code="200"/>Returns the information of the user/response>
        /// <response code="204"/>Returns when there is not information available to return /response>
        /// The <see cref="UserType"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCurrentUserType")]
        [Authorize(Roles = "SY-Read")]
        [ValidateModel]
        public async Task<IActionResult> GetCurrentUserType()
        {
            var result = this.userService.GetCurrentUserType();
            return this.Ok(result);
        }

        /// <summary>
        /// The get current user information i.e. user type and its terms of use.
        /// </summary>
        /// <remarks>
        /// The get current user information i.e. user type and its terms of use
        /// </remarks>
        /// <returns>
        /// <response code="200"/>Returns the information of the user i.e. user type and user terms of use /response>
        /// <response code="204"/>Returns when there is not information available to return /response>
        /// The <see cref="UserType"/>.
        /// </returns>
        [HttpGet]
        [Route("GetCurrentUserInformation")]
        [Authorize(Roles = "SY-Read")]
        [ValidateModel]
        public async Task<IActionResult> GetCurrentUserInformation()
        {
            var result = this.userService.GetCurrentUserInformation();
            return this.Ok(result);
        }

        /// <summary>
        /// The Send New User EmailAddress  action allows you to send the email once a user has been created with a link so that they can reset their account password
        /// </summary>
        /// <remarks>
        /// The send new user email action receives an smtp single email object
        /// </remarks>
        /// The Send New User EmailAddress action requires an smtp object
        /// <response code="200">Returns the smtp object</response>
        /// <response code="400">Returns the Validation error messages when email could not be sent</response>
        /// <param name="model">
        /// The information need to send an email
        /// </param>
        /// <returns>
        /// Returns a smtp of <see cref="ApiStatusOutput"/>  
        /// </returns>
        [HttpPost]
        [Route("SendNewUserEmail")]
        [Authorize(Roles = "SY-Create")]
        [ValidateModel]
        public async Task<IActionResult> SendNewUserEmail([FromBody]EmailBase model)
        {
            var result = await this.userService.SendNewUserEmail(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The Send reset password email  action allows you to send the email to the user so that they can reset their password
        /// </summary>
        /// <remarks>
        /// The send reset password  email action receives an smtp single email object
        /// </remarks>
        /// The Send reset password email action requires an smtp object
        /// <response code="200">Returns the smtp object</response>
        /// <response code="400">Returns the Validation error messages when email could not be sent</response>
        /// <param name="model">
        /// The information need to send an email
        /// </param>
        /// <returns>
        /// Returns a smtp of <see cref="ApiStatusOutput"/>  
        /// </returns>
        [HttpPost]
        [Route("SendResetPasswordEmail")]
        [Authorize(Roles = "SY-Create")]
        [ValidateModel]
        public async Task<IActionResult> SendResetPasswordEmail([FromBody]EmailBase model)
        {
            var result = await this.userService.SendResetPasswordEmail(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The Update Password Set action allows you to reset the password set of a user i.e. password, security question and answer.
        /// </summary>
        /// <remarks>
        /// The Update Password Set  email action receives an UserPassword object
        /// </remarks>
        /// The Update Password Set  email action receives an UserPassword object
        /// <response code="200">Returns the generic output object</response>
        /// <response code="400">Returns the Validation error messages when password could not be updated</response>
        /// <param name="model">
        /// The password Set.
        /// </param>
        /// <returns>
        /// Returns a generic output of <see cref="GenericOutput"/>  
        /// </returns>
        [HttpPut]
        [Route("UpdatePasswordSet")]
        [AllowAnonymous]
        [ValidateModel]
        public async Task<IActionResult> UpdatePasswordSet([FromBody]UserPassword model)
        {
            var result = await this.userService.UpdatePasswordSet(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The accept terms of use.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Route("UpdateTermsOfUse")]
        [Authorize(Roles = "SY-Read")]
        [ValidateModel]
        public async Task<IActionResult> AcceptTermsOfUse([FromBody] UserTermsOfUse model)
        {
            var result = await this.userService.UpdateUserTermsOfUse(model.UserId, model.Version);
            return this.Ok(result);
        }

    }
}