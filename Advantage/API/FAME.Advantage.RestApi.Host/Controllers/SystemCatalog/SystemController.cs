﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemController.cs" company="Fame Inc">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SystemController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The system statuses controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/System")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class SystemController : BaseController
    {
        /// <summary>
        /// The system service that works with the system cache.
        /// </summary>
        private readonly ISystemService systemService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemController"/> class.
        /// </summary>
        /// <param name="systemService">
        /// The system service.
        /// </param>
        public SystemController(ISystemService systemService)
        {
            this.systemService = systemService;
        }

        /// <summary>
        /// Clears all the data stored on the API cache.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Delete")]
        [Route("ClearCache")]
        public IActionResult ClearCache()
        {
            this.systemService.ClearCache();
            return this.Ok();
        }

        /// <summary>
        /// Clears all the data stored on the API cache for the tenant the logged in administrator belongs to.
        /// </summary>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Delete")]
        [Route("ClearCacheForTenant")]
        public IActionResult ClearCacheForTenant()
        {
            this.systemService.ClearCacheForTenant();
            return this.Ok();
        }
    }
}
