﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the catalog controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The catalog controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
    public class CatalogController : BaseController
    {
        /// <summary>
        /// The catalog service.
        /// </summary>
        private ICatalogService catalogService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CatalogController"/> class.
        /// </summary>
        /// <param name="catalogService">
        /// The catalog service.
        /// </param>
        public CatalogController(ICatalogService catalogService )
        {
            this.catalogService = catalogService;
        }

        /// <summary>
        /// The get catalog action returns system catalog information by campus.
        /// </summary>
        /// <remarks>
        /// The get catalog action returns system data for all active campuses which includes
        /// the phone types, the email types, the addresses, the states, the countries, the interest areas,
        /// and the source categories associated with each campus
        /// </remarks>
        /// <response code="200">Returns a list of campuses with their corresponding system information.</response>
        /// <response code="404">Returns no content when there are no campuses</response>
        /// <returns>
        /// The <see cref="IList&lt;Catalog&gt;"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read,AD-Read")]
        [Route("GetCatalog")]
        public async Task<IActionResult> GetCatalog()
        {
            return this.Ok(await Task.Run(() => this.catalogService.GetCatalog()));
        }

    }
}