﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReasonController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The student controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The drop reason controller .
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class DropReasonController : BaseController
    {
        /// <summary>
        /// The services.
        /// </summary>
        private readonly IDropReasonService dropReasonService;

        /// <summary>
        /// Initializes a new instance of the DropReasonController class .
        /// </summary>
        /// <param name="dropReasonService">
        /// The services .
        /// </param>
        public DropReasonController(IDropReasonService dropReasonService)
        {
            this.dropReasonService = dropReasonService;
        }

        /// <summary>
        /// The DropReason action allows you to find a list of drop reasons.
        /// </summary>
        /// <remarks>
        /// The DropReason action requires a DropReason object with campusId .
        /// </remarks>
        /// <response code="200">Returns the list of drop reasons found</response>
        /// <response code="401">Returns the zero records when no results found</response>
        /// <param name="campusId">
        /// The studentdropreason.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="IListItem{TText,TValue}"/>  where TText holds the desciption of Drop Reason of type string and TValue is of type Guid and will have the dropreason Id.
        /// </returns>
        [HttpGet]
        [Route("GetByCampus")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByCampus(Guid campusId)
        {
            if (campusId != Guid.Empty)
            {
                return this.Ok(await this.dropReasonService.GetByCampus(campusId));
            }

            return this.Ok(this.MissingRequiredParamResult("CampusId"));
        }

        /// <summary>
        /// The get naccas drop reasons.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetNaccasDropReasons")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetNaccasDropReasons()
        {

            return this.Ok(await this.dropReasonService.GetNaccasDropReasons());
        }

    }
}
