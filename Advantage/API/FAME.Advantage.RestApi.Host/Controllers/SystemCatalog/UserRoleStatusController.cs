﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRoleStatusController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The user role status controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The user role status controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class UserRoleStatusController : BaseController
    {
        /// <summary>
        /// The academic calendar service.
        /// </summary>
        private readonly IUserRoleStatusService userRoleStatusService;

        /// <summary>
        /// Initializes a new instance of the UserRoleStatusController class.
        /// </summary>
        /// <param name="userRoleStatusService">
        /// The user role status service.
        /// </param>
        public UserRoleStatusController(IUserRoleStatusService userRoleStatusService)
        {
            this.userRoleStatusService = userRoleStatusService;
        }

        /// <summary>
        /// The Is Support User action allows you to validate whether the logged in user is a support user or not.
        /// </summary>
        /// <remarks>
        /// The IsSupportUser action allows you to validate whether the logged in user is a support user or not.
        /// </remarks>
        /// <response code="200">Returns true if the selected user is a logged in user else returns false.</response>
        /// <response code="400">Returns the Validation error message when the input token is empty.</response>
        /// <returns>
        /// Returns true if the selected user is a support user else returns false. 
        /// </returns>
        [HttpGet]
        [Route("IsSupportUser")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> IsSupportUser()
        {
            return this.Ok(await this.userRoleStatusService.IsSupportUser());
        }
    }
}
