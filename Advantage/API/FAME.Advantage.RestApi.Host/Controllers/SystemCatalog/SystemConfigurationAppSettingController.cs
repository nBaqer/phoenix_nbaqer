﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemConfigurationAppSettingController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The system configuration app setting controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System;
    using System.Collections.Generic;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore.Internal;

    /// <summary>
    /// The system configuration app setting controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class SystemConfigurationAppSettingController : BaseController
    {
        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemConfigurationAppSettingController"/> class.
        /// </summary>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        public SystemConfigurationAppSettingController(
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService)
        {
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
        }

        /// <summary>
        /// GetAppSettingValue action method will returns the Application Setting Value by given keyName.
        /// </summary>
        /// <remarks>
        /// This action method will returns the Application Setting Value by given keyName.
        /// </remarks>
        /// <response code="200">Returns the Application Setting Value found.</response>
        /// <response code="400">If the Application Setting Value not found</response>  
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetAppSettingValue")]
        public IActionResult GetAppSettingValue(string keyName)
        {
            return string.IsNullOrEmpty(keyName)
                       ? this.MissingRequiredParamResult("KeyName")
                       : this.Ok(this.systemConfigurationAppSettingService.GetAppSettingValue(keyName));
        }

        /// <summary>
        /// GetAppSettingValueByCampus action method will returns the Application Setting Value by given keyName and campusId.
        /// </summary>
        /// <remarks>
        /// This action method will returns the Application Setting Value by given keyName and campusId.
        /// </remarks>
        /// <response code="200">Returns the Application Setting Value found.</response>
        /// <response code="400">If the Application Setting Value not found</response>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetAppSettingValueByCampus")]
        public IActionResult GetAppSettingValueByCampus(string keyName, Guid campusId)
        {
            var validationError = new List<BadRequestObjectResult>();
            if (string.IsNullOrEmpty(keyName))
            {
                validationError.Add(this.MissingRequiredParamResult("KeyName"));
            }

            if (campusId.IsEmpty())
            {
                validationError.Add(this.MissingRequiredParamResult("CampusId"));
            }

            if (validationError.Any())
            {
                return this.Ok(validationError);
            }

            return this.Ok(this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(keyName, campusId));
        }

        /// <summary>
        /// SetAppSettingValue action method saves application setting value by given key name and value.
        /// </summary>
        /// <remarks>
        /// SetAppSettingValue action method saves application setting value by given key name and value
        /// </remarks>
        /// <response code="200">Returns the Application Setting Value save successfully.</response>
        /// <response code="400">If the Application Setting Value failed to save.</response>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Modify")]
        [Route("SetAppSettingValue")]
        public IActionResult SetAppSettingValue(string keyName, string value)
        {
            var validationError = new List<BadRequestObjectResult>();
            if (string.IsNullOrEmpty(keyName))
            {
                validationError.Add(this.MissingRequiredParamResult("KeyName"));
            }

            if (string.IsNullOrEmpty(value))
            {
                validationError.Add(this.MissingRequiredParamResult("Value"));
            }

            if (validationError.Any())
            {
                return this.Ok(validationError);
            }

            return this.Ok(this.systemConfigurationAppSettingService.SetAppSettingValue(keyName, value));
        }
    }
}
