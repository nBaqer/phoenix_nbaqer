﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The module controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The module controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ModuleController : BaseController
    {
        /// <summary>
        /// The module service.
        /// </summary>
        private readonly IModuleService moduleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleController"/> class.
        /// </summary>
        /// <param name="moduleService">
        /// The module service.
        /// </param>
        public ModuleController(IModuleService moduleService)
        {
            this.moduleService = moduleService;
        }

        /// <summary>
        /// The get returns a list of modules for a dropdown.
        /// </summary>
        /// <response code="200">Returns the list of all modules in the database.</response>
        /// <response code="400">If the list of modules is null</response>  
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {

            return this.Ok(await this.moduleService.GetModules());
        }
    }
}
