﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The resource controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The resource controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/SystemCatalog/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ResourceController : BaseController
    {
        /// <summary>
        /// The resource service.
        /// </summary>
        private readonly IResourceService resourceService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResourceController"/> class.
        /// </summary>
        /// <param name="resourceService">
        /// The resource service.
        /// </param>
        public ResourceController(IResourceService resourceService)
        {
            this.resourceService = resourceService;
        }

        /// <summary>
        /// The get by type id returns a list of resources based on the resource type id for a dropdown.
        /// </summary>
        /// <param name="resourceTypeId">
        /// The resource Type Id.
        /// </param>
        /// <response code="200">Returns the list of all resources in the database base on resource type id.</response>
        /// <response code="400">If the list of resources is null</response>  
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string,int}}"/>  
        /// </returns>
        [HttpGet]
        [Route("GetByTypeId")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetByTypeId(int resourceTypeId)
        {
            return this.Ok(await this.resourceService.GetResources(resourceTypeId));
        }

        /// <summary>
        /// The get returns a list of resources for a dropdown.
        /// </summary>
        /// <response code="200">Returns the list of all resources in the database.</response>
        /// <response code="400">If the list of resources is null</response>  
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get()
        {
            return this.Ok(await this.resourceService.GetResources(null));
        }
    }
}
