﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GraduationCalculatorController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GraduationCalculatorController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Common
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <inheritdoc />
    /// <summary>
    /// The approve termination controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Common/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class GraduationCalculatorController : BaseController
    {
        /// <summary>
        /// The graduation calculator service.
        /// </summary>
        private readonly IGraduationCalculatorService graduationCalculatorService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraduationCalculatorController"/> class.
        /// </summary>
        /// <param name="graduationCalculatorService">
        /// The graduation Calculator Service.
        /// </param>
        public GraduationCalculatorController(IGraduationCalculatorService graduationCalculatorService)
        {
            this.graduationCalculatorService = graduationCalculatorService;
        }

        /// <summary>
        /// The get contracted graduation date for a clock hour program based on a schedule id and start date.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="campusId">
        /// The campus id
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetCalculatedContractedGraduationDate")]
        public IActionResult GetCalculatedContractedGraduationDate([Required]Guid scheduleId, [Required]DateTime startDate, Guid? campusId)
        {
            var result = this.graduationCalculatorService.CalculateContractedGraduationDate(scheduleId, startDate, campusId);
            result.AsString = result.Result.ToShortDateString();
            return this.Ok(result);
        }

        /// <summary>
        /// The calculate contracted graduation date for a clock hour program.
        /// </summary>
        /// <param name="gradDateCalculation">
        /// The grad date calculation data.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("CalculateContractedGraduationDate")]
        public IActionResult CalculateContractedGraduationDate([FromBody]GradDateCalculation gradDateCalculation)
        {
            var result = this.graduationCalculatorService.CalculateContractedGraduationDate(gradDateCalculation);
            result.AsString = result.Result.ToShortDateString();
            return this.Ok(result);
        }

        /// <summary>
        /// The calculate contracted graduation date for a clock hour program.
        /// </summary>
        /// <param name="gradDateCalculation">
        /// The grad date calculation data.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("UpdateGraduationDate")]
        public IActionResult UpdateGraduationDate([FromBody]List<string> StuEnrollIdList)
        {

            if (StuEnrollIdList.Count() > 0)
            {
                var result = this.graduationCalculatorService.UpdateGraduationDate(StuEnrollIdList);

                if (result == null)
                    return this.BadRequest("Error occured");

                if (result.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                    return this.BadRequest(result);

                return Ok(result);
            }
            else { return BadRequest(ApiMsgs.IncorrectParametersPassed); }

        }
    }
}