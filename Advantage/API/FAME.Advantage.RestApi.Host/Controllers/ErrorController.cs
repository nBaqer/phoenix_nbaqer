﻿using FAME.Advantage.RestApi.Host.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace FAME.Advantage.RestApi.Host.Controllers
{
    public class ErrorController : Controller
    {
        [HttpGet]
        [Route("/error")]
        public IActionResult Get()
        {
            return GetServerError();
        }
        [HttpPost]
        [Route("/error")]
        public IActionResult Post()
        {
            return GetServerError();
        }
        [HttpPut]
        [Route("/error")]
        public IActionResult Put()
        {
            return GetServerError();
        }
        [HttpDelete]
        [Route("/error")]
        public IActionResult Delete()
        {
            return GetServerError();
        }

        private IActionResult GetServerError()
        {
            return StatusCode(StatusCodes.Status500InternalServerError, Global.ERROR_500);
        }
    }
}

