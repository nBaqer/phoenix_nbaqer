﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppController.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The Klass App controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.KlassApp
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.KlassApp;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.KlassApp;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The Klass App controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
    public class KlassAppController : BaseController
    {
        /// <summary>
        /// The klass app service.
        /// </summary>
        private readonly IKlassAppService klassAppService;

        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppController"/> class.
        /// </summary>
        /// <param name="klassAppService">
        /// The klass app service.
        /// </param>
        public KlassAppController(IKlassAppService klassAppService)
        {
            this.klassAppService = klassAppService;
        }

        /// <summary>
        /// The get students to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read, REPORTSAPI-Read")]
        [Route("GetStudentsToSync")]
        public async Task<IListActionResult<KlassAppSync>> GetStudentsToSync()
        {
            return await this.klassAppService.GetStudentsToSync();
        }

    }
}