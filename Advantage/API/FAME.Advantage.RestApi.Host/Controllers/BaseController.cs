﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the BaseController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FAME.Advantage.RestApi.Host.Infrastructure;

    using Fame.Orm;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The base controller.
    /// </summary>
    public class BaseController : Controller
    {
        /// <summary>
        /// The missing required param result.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="BadRequestObjectResult"/>.
        /// </returns>
        protected BadRequestObjectResult MissingRequiredParamResult(string param)
        {
            return this.BadRequest($"{ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR} {param}");
        }

        /// <summary>
        /// The send record response.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity type that will be returned
        /// </typeparam>
        /// <returns>
        /// The <see cref="ObjectResult"/>.
        /// </returns>
        protected ObjectResult SendRecordResponse<T>(T value) where T : IEntity
        {
            return value == null ? (ObjectResult)new NotFoundObjectResult(null) : base.Ok(value);
        }

        /// <summary>
        /// The send collection response.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity type that will be returned
        /// </typeparam>
        /// <returns>
        /// The <see cref="ObjectResult"/>.
        /// </returns>
        protected ObjectResult SendCollectionResponse<T>(T value) where T : IEnumerable<IEntity>
        {
            return value.Any() ? (ObjectResult)base.Ok(value) : new NotFoundObjectResult(null);
        }

        /// <summary>
        /// The ok.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity type that will be returned
        /// </typeparam>
        /// <returns>
        /// The <see cref="ObjectResult"/>.
        /// </returns>
        protected ObjectResult Ok<T>(T value)
        {
            if (value == null)
            {
                return (ObjectResult)new NotFoundObjectResult(null);
            }
            else
            {
                var valueType = value.GetType();
                var actionResultStatusType = typeof(DataTransferObjects.Common.IActionResultStatus);

                if (actionResultStatusType.IsAssignableFrom(valueType))
                {
                    var dto = (DataTransferObjects.Common.IActionResultStatus)value;

                    if (dto.ResultStatus == Enums.ResultStatus.NotFound)
                    {
                        return this.NotFound(dto.ResultStatusMessage);
                    }

                    if (dto.ResultStatus == Enums.ResultStatus.Error)
                    {
                        return this.BadRequest(dto.ResultStatusMessage);
                    }

                }
                return base.Ok(value);
            }
        }
    }
}
