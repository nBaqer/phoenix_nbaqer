// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the LeadsController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Admissions
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Net.Http;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The leads controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class LeadsController : BaseController
    {
        /// <summary>
        /// The leads service.
        /// </summary>
        private readonly ILeadService leadsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsController"/> class.
        /// </summary>
        /// <param name="leadsService">
        /// The leads service.
        /// </param>
        public LeadsController(ILeadService leadsService)
        {
            this.leadsService = leadsService;
        }

        /// <summary>
        /// The Post action allows your to insert a new lead into Advantage validating the required fields ahead such as first name, last name, phone, phone type, email and emailtype.
        /// </summary>
        /// <remarks>
        /// The Post action requires a lead object with at least first name, last name, phone, phone type, email and emailtype in order to be successfully inserted in Advantage, if any duplicates is found, then a record per duplicate will be inserted in the duplicate leads table in advantage
        /// </remarks>
        /// <response code="200">Returns the lead saved in the system</response>
        /// <response code="400">Returns the Validation error messages when the lead to be inserted is invalid</response>
        /// <param name="model">
        /// The lead to be inserted.
        /// </param>
        /// <returns>
        /// Returns a Lead of <see cref="Lead"/>  
        /// </returns>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = "AD-Create")]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody]Lead model)
        {
            var result = await this.leadsService.Create(model,false);
            return this.Ok(result);
        }

        /// <summary>
        /// The Post action allows your to insert a new lead into Advantage validating the required fields ahead such as first name, last name, phone, phone type, email and emailtype.
        /// </summary>
        /// <remarks>
        /// The Post action requires a lead object with at least first name, last name, phone, phone type, email and emailtype in order to be successfully inserted in Advantage, if any duplicates is found, then a record per duplicate will be inserted in the duplicate leads table in advantage
        /// </remarks>
        /// <response code="200">Returns the lead saved in the system</response>
        /// <response code="400">Returns the Validation error messages when the lead to be inserted is invalid</response>
        /// <param name="model">
        /// The lead to be inserted.
        /// </param>
        /// <returns>
        /// Returns a Lead of <see cref="Lead"/>  
        /// </returns>
        [HttpPost]
        [Route("PostLeadAfa")]
        [Authorize(Roles = "AD-Create")]
        public async Task<IActionResult> PostLeadAfa([FromBody]Lead model)
        {
            var result = await this.leadsService.Create(model,true);
            return this.Ok(result);
        }

        /// <summary>
        /// The update allows you to update an existing lead in advantage. Required fields such as First Name, Last Name are validated ahead of time.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <remarks>
        /// The update requires a <see cref="LeadBase"/> object with at least first name, last name, phone, phone type and/or email and email type ir order to be successfully updated in advantage. If any duplicate by the update is found, then the record will be updated and marked as duplicate.
        /// Changing the Lead last name:
        /// ** If the last name is updated, the api will return you on the response a ResultStatus of "Unable to update Last Name, an official name change was not provided", if the IsOfficialNameChange field is null or empty. Setting the IsOfficialNameChange to Yes will track Prior Last Name.
        /// ** Setting the IsOfficialNameChange to  No will still update the Last Name field but not update the Prior Last name field
        /// Updating the email:
        /// ** To update an email use the Lead/{leadId}/EmailAddress/{id}/Put action
        /// Updating the phone:
        /// ** To update a phone use the Lead/{leadId}/Phone/{id}/Put action
        /// Updating the address:
        /// ** To update an address use the Lead/{leadId}/Address/{id}/Put action
        /// </remarks>
        /// <returns>
        /// The <see cref="LeadBase"/>.
        /// </returns>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = "AD-Modify")]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody]LeadBase model)
        {
            var result = await this.leadsService.Update(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The put AFA student id.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPut]
        [Route("PutAfaStudentId")]
        [Authorize(Roles = "AD-Modify")]
        public async Task<IActionResult> PutAfaStudentId([FromBody]LeadBase model)
        {
            var result = await this.leadsService.UpdateAfaStudentId(model);
            return this.Ok(result);
        }

        /// <summary>
        /// The get lead by it's Id.
        /// If the lead is on an enrolled status a status code 400 will be returned.
        /// </summary>
        /// <param name="id">
        /// The lead id.
        /// </param>
        /// <response code="200">Returns a lead if it's not enrolled</response>
        /// <response code="404">Data not found in case if the lead does not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        /// <returns>
        /// The <see cref="DataTransferObjects.Admissions.Lead.V2.Lead"/>.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "AD-Read")]
        [ValidateModel]
        public async Task<IActionResult> Get([Required] Guid id)
        {
            return this.Ok(await this.leadsService.Get(id));
        }

        /// <summary>
        /// The check if AFA student id exists.
        /// </summary>
        /// <param name="firstName">
        /// The first Name.
        /// </param>
        /// <param name="lastName">
        /// The last Name.
        /// </param>
        /// <param name="ssn">
        /// The SSN.
        /// </param>
        /// <param name="cmsId">
        /// The Client Management Software Id.
        /// </param>
        /// <param name="afaStudentId">
        /// The AFA Student Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("CheckIfAfaStudentIdExists")]
        [Authorize(Roles = "AD-Read")]
        [ValidateModel]
        public async Task<IActionResult> CheckIfAfaStudentIdExists(string firstName, string lastName, string ssn, string cmsId, long afaStudentId)
        {
            LeadBase model = new LeadBase();
            model.FirstName = firstName;
            model.LastName = lastName;
            model.SSN = ssn;
            model.CmsId = cmsId;
            model.AfaStudentId = afaStudentId;
            var result = await this.leadsService.CheckIfAfaStudentIdExists(model);
            return this.Ok(result);
        }
    }
}
