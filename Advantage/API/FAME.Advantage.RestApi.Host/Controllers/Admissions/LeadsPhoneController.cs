﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPhoneController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the LeadPhoneController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Admissions
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The lead phone controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/leads/{leadId:guid}/phone")]
    public class LeadsPhoneController : BaseController
    {
        /// <summary>
        /// The lead phone service.
        /// </summary>
        private readonly ILeadsPhoneService leadPhoneService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsPhoneController"/> class.
        /// </summary>
        /// <param name="leadPhoneService">
        /// The lead phone service.
        /// </param>
        public LeadsPhoneController(ILeadsPhoneService leadPhoneService)
        {
            this.leadPhoneService = leadPhoneService;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the are no phones.</response>
        [HttpGet]
        [Authorize(Roles = RolePermissions.AdmissionsRead)]
        public async Task<IActionResult> GetAll(Guid leadId)
        {
            return this.Ok(await this.leadPhoneService.GetAll(leadId));
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the phone not exists</response>
        [HttpGet("{id:guid}")]
        [Authorize(Roles = RolePermissions.AdmissionsRead)]
        public async Task<IActionResult> Get(Guid leadId, Guid id)
        {
            return this.Ok(await this.leadPhoneService.Get(leadId, id));
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead does or phone not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = RolePermissions.AdmissionsCreate)]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody]PhoneNumber model)
        {
            return this.Ok(await this.leadPhoneService.Create(model));
        }

        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="model">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead does or phone not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = RolePermissions.AdmissionsModify)]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody]PhoneNumber model)
        {
            return this.Ok(await this.leadPhoneService.Update(model));
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead or phone does not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = RolePermissions.AdmissionsDelete)]
        public async Task<IActionResult> Delete(Guid leadId, Guid id)
        {
            return this.Ok(await this.leadPhoneService.Delete(leadId, id));
        }
    }
}
