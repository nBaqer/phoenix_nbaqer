﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsAddressController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadsAddressController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Admissions
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The lead phone controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/leads/{leadId:guid}/address")]

    public class LeadsAddressController : BaseController
    {
        /// <summary>
        /// The lead address service.
        /// </summary>
        private readonly ILeadsAddressService leadAddressService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsAddressController"/> class.
        /// </summary>
        /// <param name="leadAddressService">
        /// The lead address service.
        /// </param>
        public LeadsAddressController(ILeadsAddressService leadAddressService)
        {
            this.leadAddressService = leadAddressService;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the are no addresses.</response>
        [HttpGet]
        [Authorize(Roles = RolePermissions.AdmissionsRead)]
        public async Task<IActionResult> GetAll(Guid leadId)
        {
            return this.Ok(await this.leadAddressService.GetAll(leadId));
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the address not exists</response>
        [HttpGet("{id:guid}")]
        [Authorize(Roles = RolePermissions.AdmissionsRead)]
        public async Task<IActionResult> Get(Guid leadId, Guid id)
        {
            return this.Ok(await this.leadAddressService.Get(leadId, id));
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead does or address not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = RolePermissions.AdmissionsCreate)]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody]Address model)
        {
            return this.Ok(await this.leadAddressService.Create(model));
        }

        /// <summary>
        /// The put.
        /// </summary>
        /// <param name="model">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead does or address not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = RolePermissions.AdmissionsModify)]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody]Address model)
        {
            return this.Ok(await this.leadAddressService.Update(model));
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <response code="404">Data not found in case if the lead does or address not exists</response>
        /// <response code="400">If the lead is on enrolled status</response>
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = RolePermissions.AdmissionsDelete)]
        public async Task<IActionResult> Delete(Guid leadId, Guid id)
        {
            return this.Ok(await this.leadAddressService.Delete(leadId, id));
        }
    }
}
