// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentTerminationController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The student termination controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Services.Interfaces.AcademicRecords;

    /// <summary>
    /// The Student Termination controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentTerminationController : BaseController
    {
        /// <summary>
        /// The student termination service.
        /// </summary>
        private readonly IStudentTerminationService studentTerminationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentTerminationController"/> class.
        /// </summary>
        /// <param name="studentTerminationService">
        /// The student termination service.
        /// </param>
        public StudentTerminationController(IStudentTerminationService studentTerminationService)
        {
            this.studentTerminationService = studentTerminationService;
        }

        /// <summary>
        /// The Post action allows to save the student termination details.
        /// </summary>
        /// <remarks>
        /// The Post action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <response code="200">Returns the saved student termination details</response>
        /// <response code="400">Returns the Validation error messages when the Student Termination data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type studentTerminationDetails.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled
        /// </returns>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = "AR-Create")]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] StudentTermination model)
        {
            return this.Ok(await this.studentTerminationService.Create(model));
        }

        /// <summary>
        /// The Put action allows you to a update the student termination details.
        /// </summary>
        /// <remarks>
        /// The Put action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined,
        ///  LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <response code="200">Returns the saved student termination details</response>
        /// <response code="400">Returns the Validation error messages when the Student Termination data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type studentTerminationDetails.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId,
        ///  DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = "AR-Modify")]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody] StudentTermination model)
        {
            return this.Ok(await this.studentTerminationService.Update(model));
        }

        /// <summary>
        /// The delete action is to delete all the termination details from database by providing the termination terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related termination details by providing the termination terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <response code="200">Returns 200 if the delete is successful</response>
        /// <response code="400">Returns 400, if the delete operation failed</response>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = "AR-Delete")]
        public async Task<IActionResult> Delete(Guid terminationId)
        {
            if (terminationId.IsEmpty())
            {
                return new OkObjectResult(new { message = $"{ApiMsgs.DELETE_VERIFICATION_FAILED} {terminationId}" });
            }

            var msg = await this.studentTerminationService.Delete(terminationId);
            return new OkObjectResult(new { message = msg });
        }

        /// <summary>
        /// The Get action is to get all the termination details from database by providing the enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get action is to get the all saved termination details for an incompleted termination by providing the enrollmentId.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="400">Returns the Validation error messages when the enrollment id provided is invalid</response>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid .
        /// </param>
        /// <returns>
        /// Returns all saved termination details for an incompleted termination based on the enrollmentId.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> Get(Guid enrollmentId)
        {
            return this.Ok(await this.studentTerminationService.Get(enrollmentId));
        }


        /// <summary>
        /// The Get student termination status codes action allows you to find a list of status codes used for student termination on a given campus.
        /// </summary>
        /// <remarks>
        /// The Get student termination status codes
        /// </remarks>
        /// <response code="200">Returns the list of status found</response>
        /// <response code="400">Returns the Validation error messages when the search data provided is invalid</response>
        /// <response code="0">Returns when there is no response from server</response>
        /// <param name="studentEnrollmentId">
        /// The student Enrollment Id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="IListItem{TText,TValue}"/> 
        /// </returns>
        [HttpGet("GetStudentTerminationStatusCode")]
        [Authorize(Roles = "AR-Read")]
        [ValidateModel]
        public async Task<IActionResult> GetStudentTerminationStatusCode(Guid studentEnrollmentId, Guid campusId)
        {
            return this.Ok(await this.studentTerminationService.GetStudentTerminationStatusCode(studentEnrollmentId,campusId));
        }
    }
}