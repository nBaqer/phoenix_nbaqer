// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the EnrollmentController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The enrollment controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class EnrollmentController : BaseController
    {
        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the EnrollmentController class.
        /// </summary>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        public EnrollmentController(IEnrollmentService enrollmentService)
        {
            this.enrollmentService = enrollmentService;
        }

        /// <summary>
        ///  The Post Enrollments By Student Id Gets the list of enrollment(s) for the given studentId and enrollmentType
        ///  and each enrollment listed is displayed as StudentEnrollment name Status Effective date of the current status'. Example: 'Cosmetology - Enrolled 05/01/2017'
        /// </summary>
        /// <remarks>
        /// The  Post Enrollments By Student Id  action requires studentId object which is a Guid and enrollmentType.
        /// </remarks>
        /// <response code="200">Returns the list of enrollments of the given student</response>
        /// <response code="404">Data not found in case if the studentId does not exists</response> 
        /// <param name="enrollmentType">
        /// The enrollmentType is required field and is a type of enrollment.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="Enrollment"/> where StudentEnrollment has the EnrollmentId,ProgramVersionDescription,StatusDescription,EffectiveDate,Status,EnrollmentDate
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEnrollmentsByStudentId")]
        public async Task<IActionResult> GetEnrollmentsByStudentId([FromBody]EnrollmentStatus enrollmentType)
        {
            return enrollmentType.StudentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("Student Id"))
                       : this.Ok(await this.enrollmentService.GetEnrollmentsByStudentId(enrollmentType));
        }

        /// <summary>
        /// The get enrollment detail action returns enrollment detail of student by enrollment.
        /// </summary>
        /// <remarks>
        /// The get enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId. 
        /// Based on the EnrollmentId and StudentId, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </remarks>
        /// <response code="200">Returns matching students enrollment details based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEnrollmentDetail")]
        public async Task<IActionResult> GetEnrollmentDetail(Guid enrollmentId, Guid studentId)
        {
            var validationError = new List<BadRequestObjectResult>();
            if (enrollmentId.IsEmpty())
            {
                validationError.Add(this.MissingRequiredParamResult("EnrollmentId"));
            }

            if (studentId.IsEmpty())
            {
                validationError.Add(this.MissingRequiredParamResult("StudentId"));
            }

            if (validationError.Any())
            {
                return this.Ok(validationError);
            }

            return this.Ok(await this.enrollmentService.GetEnrollmentDetail(enrollmentId, studentId));
        }

        /// <summary>
        /// gets all the enrollment details for enrollments active in last 60 days
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="programVersionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAllActiveEnrollmentForCampusId")]
        public async Task<IActionResult> GetAllActiveEnrollmentForCampusId(Guid campusId, Guid? programVersionId)
        {
            return campusId.IsEmpty() ? Ok(MissingRequiredParamResult("campusId"))
                : Ok(await enrollmentService.GetAllActiveEnrollmentForCampusId(campusId, programVersionId ?? Guid.Empty));
        }

        /// <summary>
        /// The get enrollments for afa sync.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAllLeadEnrollmentsForAfaSync")]
        public async Task<IActionResult> GetAllLeadEnrollmentsForAfaSync(Guid leadId)
        {
            return this.Ok(await this.enrollmentService.GetAllLeadEnrollmentsForAfaSync(leadId));
        }

        /// <summary>
        /// The get enrollments for afa sync.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEnrollmentsForAfaSync")]
        public async Task<IActionResult> GetEnrollmentsForAfaSync(Guid campusId)
        {
            return this.Ok(await this.enrollmentService.GetAllEnrollmentsForAfaSync(campusId));
        }

        /// <summary>
        /// The have attendance or grade posted.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("HaveAttendanceOrGradePosted")]
        public async Task<IActionResult> HaveAttendanceOrGradePosted(Guid campusId)
        {
            return this.Ok(await this.enrollmentService.HaveAttendanceOrGradePosted(campusId));
        }

        /// <summary>
        /// The get lms student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetLmsStudentEnrollmentDetails")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetLmsStudentEnrollmentDetails(Guid studentEnrollmentId)
        {
            if (studentEnrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("StudentEnrollmentId");
            }

            var rv = await this.enrollmentService.GetLmsStudentEnrollmentDetails(studentEnrollmentId);
            return this.Ok(rv);
        }

        /// <summary>
        /// The get lms student enrollments details for a set of students.
        /// </summary>
        /// <param name="studentEnrollmentIds">
        /// The list of student enrollment ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("GetMultipleLmsStudentEnrollmentDetails")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetMultipleLmsStudentEnrollmentDetails([FromBody]List<Guid> studentEnrollmentIds)
        {
            if (studentEnrollmentIds == null || (studentEnrollmentIds != null && !studentEnrollmentIds.Any()))
            {
                return this.MissingRequiredParamResult("studentEnrollmentIds");
            }
            var rv = await this.enrollmentService.GetMultipleLmsStudentEnrollmentDetails(studentEnrollmentIds);
            return this.Ok(rv);
        }

        /// <summary>
        /// The get afa student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAfaStudentEnrollmentDetails")]
        public async Task<IActionResult> GetAfaStudentEnrollmentDetails(Guid studentEnrollmentId)
        {
            if (studentEnrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("StudentEnrollmentId");
            }
            return this.Ok(await this.enrollmentService.GetAfaStudentEnrollmentDetails(studentEnrollmentId));
        }
    }
}