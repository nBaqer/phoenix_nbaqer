// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradesController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the GradesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Extensions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The grades controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class GradesController : BaseController
    {
        /// <summary>
        /// The grades service.
        /// </summary>
        private readonly IGradesService gradesService;

        /// <summary>
        /// Initializes a new instance of the GradesController class.
        /// </summary>
        /// <param name="gradesService">
        /// The grades service.
        /// </param>
        public GradesController(IGradesService gradesService)
        {
            this.gradesService = gradesService;
        }

        /// <summary>
        /// Given parameters posts final grade for a class for a single student.
        /// </summary>
        /// <param name="parameters">
        /// <response code="200">Returns success value and message.</response>
        /// The data export parameters.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostFinalGrade")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> PostFinalGrade([FromBody]PostFinalGrade parameters)
        {
            if (parameters == null)
                return BadRequest(ApiMsgs.IncorrectParametersPassed);

            var actionResult = await this.gradesService.PostFinalGradeByStudentEnrollmentId(parameters);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Post a students grade for a specific component.
        /// </summary>
        /// <param name="studentPostGradesInput">
        /// The dto for posting a component grade requiring a student enrollment id, a class section id, a score and a component id.
        /// </param>
        /// <returns>
        /// The api status output with status message.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostStudentGradeForComponent")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public ActionResult PostStudentGradeForComponent([FromBody]StudentPostGradesInput studentPostGradesInput)
        {
            return this.Ok(this.gradesService.PostStudentGradeForComponent(studentPostGradesInput));
        }

        /// <summary>
        /// Returns student component grades.
        /// </summary>
        ///  <param name="stuEnrollId">
        /// The students enrollment id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStudentComponentGrades")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetStudentComponentGrades(Guid stuEnrollId)
        {
            if (stuEnrollId == Guid.Empty)
                return BadRequest(ApiMsgs.IncorrectParametersPassed);

            var actionResult = await this.gradesService.GetStudentComponentGradesById(stuEnrollId);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }
    }
}