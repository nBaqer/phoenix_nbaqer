﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentGroupsController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The Student Groups Controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using Extensions;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Services.Interfaces.AcademicRecords;

    /// <inheritdoc />
    /// <summary>
    /// The student groups controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentGroupsController : BaseController
    {
        /// <summary>
        /// The student groups service.
        /// </summary>
        private readonly IStudentGroupsService studentGroupsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentGroupsController"/> class.
        /// </summary>
        /// <param name="studentGroupsService">
        /// The student groups service.
        /// </param>
        public StudentGroupsController(IStudentGroupsService studentGroupsService)
        {
            this.studentGroupsService = studentGroupsService;
        }
        /// <summary>
        /// The get student groups by campus id method returns the student groups by given campus Id as a text/value pair.
        /// </summary>
        /// <remarks>
        /// The get student groups by campus id method returns the student groups by given campus Id as a text/value pair,
        /// suitable for a dropdown.
        /// </remarks>
        /// <response code="200">Returns student groups as value/text</response>
        /// <response code="404">Returns no content when there are no student groups found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStudentGroupsByCampus")]
        public async Task<IActionResult> GetStudentGroupsByCampus(Guid campusId)
        {
            return campusId.IsEmpty() ? this.MissingRequiredParamResult("CampusId") : this.Ok(await this.studentGroupsService.GetStudentGroupsByCampus(campusId));
        }

    }
}