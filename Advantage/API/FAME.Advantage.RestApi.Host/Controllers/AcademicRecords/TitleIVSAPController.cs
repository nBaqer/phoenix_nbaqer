﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVSAPController.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The title IV SAP controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FAME.Advantage.RestApi.Host.Services.FinacialAid;
using FAME.Advantage.RestApi.Host.Services.Interfaces;
using FAME.Advantage.RestApi.Host.Services.Interfaces.FinancialAid;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The title IV SAP controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
    public class TitleIVSAPController : BaseController
    {
        /// <summary>
        /// The title iv sap service.
        /// </summary>
        private readonly ITitleIVSAPService titleIVSAPService;
        /// <summary>
        /// The title IV sap check Service.
        /// </summary>
        private readonly ITitleIVSapCheckService titleIVSapCheckService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIVSAPController"/> class.
        /// </summary>
        /// <param name="titleIVSAPService">
        /// The title ivsap service.
        /// </param>
        public TitleIVSAPController(ITitleIVSapCheckService titleIvSapCheckService, ITitleIVSAPService titleIVSAPService)
        {
            this.titleIVSapCheckService = titleIvSapCheckService;
            this.titleIVSAPService = titleIVSAPService;
        }

        /// <summary>
        /// The get title iv results for student.
        /// </summary>
        /// <param name="fasap">
        /// The student id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("GetTitleIVResultsForStudent")]
        public async Task<IActionResult> GetTitleIVResultsForStudent([FromBody]FASAPStatus fasap)
        {
            return this.Ok(await this.titleIVSAPService.GetTitleIVResultsForStudent(fasap));
        }

        /// <summary>
        /// The get Title IV results for multiple student.
        /// </summary>
        /// <param name="input">
        /// The Title IV SAP input.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("GetTitleIVResultsForMultipleStudents")]
        public async Task<IActionResult> GetTitleIVResultsForMultipleStudents([FromBody][Required]TitleIVSAPInput input)
        {
            return this.Ok(await this.titleIVSAPService.GetTitleIVResultsForMultipleStudents(input));
        }
        /// <summary>
        /// Executes the title IV Process.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="studentEnrollments">
        /// The list of student enrollment id.
        /// </param>
        /// <param name="incrementToRun">
        /// The increment to run (optional).
        /// </param>
        /// <param name="runAllIncrements">
        /// The run all increments flag.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("ExecuteTitleIVSap")]
        public async Task<IActionResult> ExecuteTitleIVSap([FromBody]TitleIVExecution execution)
        {

            return this.Ok(await this.titleIVSapCheckService
                .Execute(execution.CampusId, execution.ProgramVersionId, execution.StudentEnrollments, execution.IncrementToRun, execution.RunAllIncrements,execution.AllowAnyStatus));
        }

        /// <summary>
        /// The update title iv results for student.
        /// </summary>
        /// <param name="FASAPCheckResultsId "></param>
        /// <response code="200">Returns true </response>
        /// <response code="400">Returns false </response>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>

        [HttpGet]
        [Authorize(Roles = "AR-Create")]
        [Route("PlaceStudentOnTitleIVPrbation")]
        public async Task<IActionResult> PlaceStudentOnTitleIVPrbation(string FASAPCheckResultsId)
        {

            return this.Ok(await titleIVSAPService.PlaceStudentOnTitleIVPrbation(FASAPCheckResultsId));

        }

        /// <summary>
        /// The run iv sap check for students.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Create")]
        [Route("RunIVSapCheckForStudents")]
        public async Task<IActionResult> RunIVSapCheckForStudents()
        {
            return this.Ok(await titleIVSapCheckService.RunIVSapCheckForStudents());
        }


        /// <summary>
        /// Get Title IV notices ready for printing based on Title IV Result.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read, SY-Read")]
        [Route("GetTitleIVNoticesForPrinting")]
        public async Task<IActionResult> GetTitleIVNoticesForPrinting(Guid campusId, DateTime? asOfDate = null)
        {

            return this.Ok(await this.titleIVSAPService
                .GetTitleIVNoticesForPrinting(campusId, asOfDate));
        }
    }
}