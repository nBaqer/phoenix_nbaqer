﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriod.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PaymentPeriodController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The payment period controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class PaymentPeriodController : BaseController
    {
        /// <summary>
        /// The payment period service.
        /// </summary>
        private readonly IPaymentPeriodService paymentPeriodService;

        /// <summary>
        /// The afa integration service.
        /// </summary>
        private readonly IAfaService afaService;
        /// <summary>
        /// Initializes a new instance of the PaymentPeriodController class.
        /// </summary>
        /// <param name="paymentPeriodService">
        /// The payment period service.
        /// </param>
        public PaymentPeriodController(IPaymentPeriodService paymentPeriodService, IAfaService afaService)
        {
            this.paymentPeriodService = paymentPeriodService;
            this.afaService = afaService;
        }

        /// <summary>
        /// Updates the AFA staging table for payment periods in advantage.
        /// </summary>
        /// <remarks>
        /// Requires the list of payment periods to post.
        /// </remarks>
        /// <response code="200">Returns the list of results that could not be posted</response>
        /// <response code="404">Data not found</response> 
        /// <param name="paymentPeriods">
        /// The collection of payment periods to update.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of key value pairs for all failed transactions where the key is the enrollment id and the value is the list of associated payment periods.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("UpdateAfaPaymentPeriodsStaging")]
        public async Task<IActionResult> UpdateAfaPaymentPeriodsStaging(
            [FromBody] IEnumerable<PaymentPeriod> paymentPeriods)
        {
            var enumerable = paymentPeriods.ToList();
            return !enumerable.Any()
                       ? this.Ok(this.MissingRequiredParamResult("payment periods"))
                       : this.Ok(await this.paymentPeriodService.UpdateAfaPaymentPeriodStaging(enumerable));
        }

        /// <summary>
        /// Calculates if the student has reached the hours for a specific payment period.
        /// </summary>
        /// <remarks>
        /// Requires the student enrollment id and corresponding payment period records in the AFA staging table.
        /// </remarks>
        /// <response code="200">Return the payment period attendance information for a student enrollment</response>
        /// <response code="404">Data not found</response> 
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// Returns an object containing the payment period attendance information for a student enrollment.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("CalculatePaymentPeriodAttendance")]
        public async Task<IActionResult> CalculatePaymentPeriodAttendance([FromBody] Guid studentEnrollmentId)
        {
            return studentEnrollmentId == Guid.Empty
                       ? this.Ok(this.MissingRequiredParamResult("studentEnrollmentId"))
                       : this.Ok(await this.paymentPeriodService.CalculatePaymentPeriodAttendance(studentEnrollmentId));
        }

        /// <summary>
        /// Calculates the cumulative attendance hours for a student.
        /// </summary>
        /// <remarks>
        /// Requires the student enrollment id.
        /// </remarks>
        /// <response code="200">Return the attendance information for a student enrollment</response>
        /// <response code="404">Data not found</response> 
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// Returns an object containing the payment period attendance information for a student enrollment.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("CalculateCumulativePaymentPeriodAttendance")]
        public async Task<IActionResult> CalculateCumulativePaymentPeriodAttendance([FromBody] Guid studentEnrollmentId)
        {
            await this.afaService.TurnWapiServiceOnIfIntegrationIsEnabled();

            return studentEnrollmentId == Guid.Empty
                       ? this.Ok(this.MissingRequiredParamResult("studentEnrollmentId"))
                       : this.Ok(await this.paymentPeriodService.CalculateCumulativePaymentPeriodAttendance(studentEnrollmentId));
        }
        /// <summary>
        /// Calculates if the student has reached the hours for a specific payment period.
        /// </summary>
        /// <remarks>
        /// Requires the student enrollment id and corresponding payment period records in the AFA staging table.
        /// </remarks>
        /// <response code="200">Return the payment period attendance information for a student enrollment</response>
        /// <response code="404">Data not found</response> 
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// Returns an object containing the payment period attendance information for a student enrollment.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetPaymentPeriodNumberByName")]
        public async Task<IActionResult> GetPaymentPeriodNumberByName(Guid studentEnrollmentId, string paymentPeriodName)
        {
            return studentEnrollmentId == Guid.Empty
                       ? this.Ok(this.MissingRequiredParamResult("studentEnrollmentId"))
                       : this.Ok(this.paymentPeriodService.GetPaymentPeriodNumberByName(studentEnrollmentId, paymentPeriodName));
        }
    }
}