﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UndoTerminationController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The undo termination controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <inheritdoc />
    /// <summary>
    /// The undo termination controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class UndoTerminationController : BaseController
    {
        /// <summary>
        /// The undo termination service.
        /// </summary>
        private readonly IUndoTerminationService undoTerminationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UndoTerminationController"/> class.
        /// </summary>
        /// <param name="undoTerminationService">
        /// The undo termination service.
        /// </param>
        public UndoTerminationController(IUndoTerminationService undoTerminationService)
        {
            this.undoTerminationService = undoTerminationService;
        }

        /// <summary>
        /// The Undotermination action allows to reverse the termination when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Undotermination action allows to reverse the termination when the student is terminated from the specified enrollment from undotermination page.        
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="400">Returns the Validation error messages when the enrollmentId provided is invalid</response>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns the success or faliure message determine whether the record has been fetched and updated based on enrollmentId.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("UndoStudentTermintion")]
        public async Task<IActionResult> UndoTermintion(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("enrollmentId")) : this.Ok(await this.undoTerminationService.UndoTermintion(enrollmentId));
        }
    }
}