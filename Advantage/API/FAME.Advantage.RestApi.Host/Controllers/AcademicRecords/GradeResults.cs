﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradeResults.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GradeResults type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The grade results.
    /// </summary>
    public class GradeResults
    {
        /// <summary>
        /// Gets or sets the student enrollment id.
        /// </summary>
        public Guid StudentEnrollmentId { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the term description.
        /// </summary>
        public string TermDescription { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the course code.
        /// </summary>
        public string CourseCode { get; set; }

        /// <summary>
        /// Gets or sets the course description.
        /// </summary>
        public string CourseDescription { get; set; }

        /// <summary>
        /// Gets or sets the course id.
        /// </summary>
        public Guid CourseId { get; set; }

        /// <summary>
        /// Gets or sets the credits.
        /// </summary>
        public decimal? Credits { get; set; }

        /// <summary>
        /// Gets or sets the fin aid credits.
        /// </summary>
        public decimal? FinAidCredits { get; set; }

        /// <summary>
        /// Gets or sets the class section id.
        /// </summary>
        public Guid? ClassSectionId { get; set; }

        /// <summary>
        /// Gets or sets the grade.
        /// </summary>
        public string Grade { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is pass.
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is credits attempted.
        /// </summary>
        public bool IsCreditsAttempted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is credits earned.
        /// </summary>
        public bool IsCreditsEarned { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is attendance only.
        /// </summary>
        public bool IsAttendanceOnly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is lab course.
        /// </summary>
        public bool IsLabCourse { get; set; }
    }
}
