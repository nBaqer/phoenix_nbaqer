﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The student controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System.Threading.Tasks;
    using DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Services.Interfaces.AcademicRecords;
    using FAME.Orm.Advantage.Domain.Common;
    using System;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The student controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/Student")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentController : BaseController
    {
        /// <summary>
        /// The student service.
        /// </summary>
        private readonly IStudentService studentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentController"/> class.
        /// </summary>
        /// <param name="studentService">
        /// The student service.
        /// </param>
        public StudentController(IStudentService studentService)
        {
            this.studentService = studentService;
        }

        /// <summary>
        /// The Search student action allows you to find a list of student(s) on a campus given a filter.
        /// </summary>
        /// <remarks>
        /// The Search student action requires StudentSearch object with campusId and Filter.
        /// </remarks>
        /// <response code="200">Returns the list of students found</response>
        /// <response code="400">Returns the Validation error messages when the search data provided is invalid</response>
        /// <param name="studentSearchFilter">
        /// The student search filter includes the campusId and the minimum 3 letters of the student First name, Last Name or SSN.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        [HttpGet]
        [Route("Search")]
        [Authorize(Roles = "AR-Read")]
        [ValidateModel]
        public async Task<IActionResult> Search(StudentSearch studentSearchFilter)
        {
            return this.Ok(await this.studentService.Search(studentSearchFilter));
        }

        /// <summary>
        /// The Search student action allows you to find a list of student(s) on a campus given a filter.
        /// </summary>
        /// <remarks>
        /// The Search student action requires StudentSearch object with campusId and Filter.
        /// </remarks>
        /// <response code="200">Returns the list of students found</response>
        /// <response code="400">Returns the Validation error messages when the search data provided is invalid</response>
        /// <param name="studentSearchFilter">
        /// The student search filter includes the campusId and the minimum 3 letters of the student First name, Last Name or SSN.
        /// </param>
        /// <param name="statuses">
        /// The list of valid statuses for searching a student..
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        [HttpPost]
        [Route("SearchWithStatuses")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> SearchWithStatuses([FromBody] StudentSearch studentSearchFilter)
        {
            return this.Ok(await this.studentService.Search(studentSearchFilter, studentSearchFilter.StudentStatuses));
        }

        [HttpGet]
        [Route("GetHoldStatus")]
        [Authorize(Roles = "AR-Read")]
        [ValidateModel]
        public async Task<IActionResult> GetHoldStatus(System.Guid? studentId)
        {
            return this.Ok(await this.studentService.GetHoldStatus(studentId));
        }

        /// <summary>
        /// Returns Student Class Sections.
        /// </summary>
        ///  <param name="stuEnrollId">
        /// The stuEnrollId
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetStudentClassSections")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetStudentClassSections(System.Guid stuEnrollId )
        {
            var actionResult = await this.studentService.GetStudentClassSections(stuEnrollId);
            return Ok(actionResult);
        }


        /// <summary>
        /// Returns Student Registered Courses.
        /// </summary>
        ///  <param name="stuEnrollId">
        /// The stuEnrollId
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetStudentRegisteredCourses")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetStudentRegisteredCourses(System.Guid stuEnrollId)
        {
            var actionResult = await this.studentService.GetStudentRegisteredCourses(stuEnrollId);
            return Ok(actionResult);
        }
    }
}