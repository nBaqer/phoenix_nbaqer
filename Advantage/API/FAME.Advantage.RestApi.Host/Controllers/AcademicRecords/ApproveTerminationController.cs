﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApproveTerminationController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The approve termination controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <inheritdoc />
    /// <summary>
    /// The approve termination controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ApproveTerminationController : BaseController
    {
        /// <summary>
        /// The approve termination service.
        /// </summary>
        private readonly IApproveTerminationService approveTerminationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApproveTerminationController"/> class.
        /// </summary>
        /// <param name="approveTerminationService">
        /// The approve termination service.
        /// </param>
        public ApproveTerminationController(IApproveTerminationService approveTerminationService)
        {
            this.approveTerminationService = approveTerminationService;
        }

        /// <summary>
        /// The GetDetails action method returns student termination details for the given termination id.
        /// The details also contain the R2T4 calculation along with overridden values
        /// </summary>
        /// <remarks>
        /// The GetDetails action method requires termination Id object of type Guid.
        /// </remarks>
        /// <response code="200">Returns the list of enrollments of the given student</response>
        /// <response code="404">Data not found in case if the studentId does not exists</response>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="ApproveTermination"/> where ApproveTermination has following information :
        /// EnrollmentName,Status,Dropreason,LastDateAttended,WithdrawalDate,DateOfDetermination,
        /// TotalCharges, TotalTitleIvAid, TotalTitleIvAidDisbursed, PercentageOfTitleIvAidEarned, PostWithdrawalDisbursement, TotalTitleIvAidEarned, AmountToBeReturnedBySchool,
        /// AmountToBeReturnedByStudent, R2T4InputUserName, OverriddenUserName, TicketNumber, TitleIvGrantLessThan50Dollar, R2T4ResultFieldsWithOverriddenValues
        /// </returns>
        [HttpGet]
        [AuthorizeReport("AR-Read")]
        [Route("GetDetails")]
        [Produces("application/xml")]
        public async Task<IActionResult> GetDetails(string token, Guid terminationId)
        {
            var missingParam = new List<BadRequestObjectResult>();
            if (token == string.Empty)
            {
                missingParam.Add(this.MissingRequiredParamResult("token"));
            }

            if (terminationId.IsEmpty())
            {
                missingParam.Add(this.MissingRequiredParamResult("Termination Id"));
            }

            if (missingParam.Count > 0)
            {
                return this.Ok(missingParam);
            }

            return this.Ok(await this.approveTerminationService.GetDetails(terminationId));
        }

        /// <summary>
        /// The GetTerminationDetails action method returns student termination details for the given termination id.
        /// The details also contain the R2T4 calculation along with overridden values
        /// </summary>
        /// <remarks>
        /// The GetTerminationDetails action method requires termination Id object of type Guid.
        /// </remarks>
        /// <response code="200">Returns the list of enrollments of the given student</response>
        /// <response code="404">Data not found in case if the studentId does not exists</response> 
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="ApproveTermination"/> where ApproveTermination has following information :
        /// EnrollmentName,Status,Dropreason,LastDateAttended,WithdrawalDate,DateOfDetermination,
        /// TotalCharges, TotalTitleIvAid, TotalTitleIvAidDisbursed, PercentageOfTitleIvAidEarned, PostWithdrawalDisbursement, TotalTitleIvAidEarned, AmountToBeReturnedBySchool,
        /// AmountToBeReturnedByStudent, R2T4InputUserName, OverriddenUserName, TicketNumber, TitleIvGrantLessThan50Dollar, R2T4ResultFieldsWithOverriddenValues
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetTerminationDetails")] 
        public async Task<IActionResult> GetTerminationDetails(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("Termination Id")) : this.Ok(await this.approveTerminationService.GetDetails(terminationId));
        }

        /// <summary>
        /// The Put action allows to update the Enrollment status details when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Put action requires Enrollment object which has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </remarks>
        /// <response code="200">Returns the saved Enrollment details</response>
        /// <response code="400">Returns the Validation error messages when the Enrollment data provided is invalid</response>
        /// <param name="model">
        /// The model object of type Enrollment.
        /// </param>
        /// <returns>
        /// Returns an object of Enrollment where Enrollment has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </returns>
        [HttpPut]
        [Route("PutEnrollmentDetails")]
        [Authorize(Roles = "AR-Modify")]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody]Enrollment model)
        {
            return model.EnrollmentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("EnrollmentId")) : this.Ok(await this.approveTerminationService.Update(model));
        }

        /// <summary>
        /// The Post action allows to save the student status change details.
        /// </summary>
        /// <remarks>
        /// The Post action requires StudentStatusChanges object which has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </remarks>
        /// <response code="200">Returns the saved student status change details</response>
        /// <response code="400">Returns the Validation error messages when the Student status changes data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type StudentStatusChanges.
        /// </param>
        /// <returns>
        /// Returns an object of StudentStatusChanges where StudentStatusChanges has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </returns>
        [HttpPost]
        [Route("PostStudentStatusChanges")]
        [Authorize(Roles = "AR-Create")]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] StudentStatusChanges model)
        {
            return this.Ok(await this.approveTerminationService.Create(model));
        }

        /// <summary>
        /// The GenerateStudentSummaryReport action will generate the report and allows to save the student termination document details in the database.
        /// </summary>
        /// <param name="reportRequest">
        /// The PostTerminationDocHistory action method accepts a parameter of type TerminationDocumentHistory.
        /// </param>
        /// <returns>
        /// Returns an object of TerminationDocumentHistory where TerminationDocumentHistory has the FileId, FileName, FileExtension, ModDate, ModUser, DocumentType, StudentId, FileNameOnly, DocumentId, ModuleId
        ///  LeadId, DisplayName .   
        /// </returns>
        [HttpPost]
        [Route("GenerateStudentSummaryReport")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GenerateStudentSummaryReport([Required][FromBody]ReportRequest reportRequest)
        {
            var actionResult = await this.approveTerminationService.GenerateStudentSummaryReport(reportRequest);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);

                    if (reportRequest.ReportOutput == ReportOutput.Pdf)
                    {
                        var response = this.File(stream, "application/pdf", "Report.pdf");

                        return response;
                    }

                    return this.BadRequest(reportRequest);
                }
            }
            else
            {
                return this.NotFound(reportRequest);
            }
        }
     }
}