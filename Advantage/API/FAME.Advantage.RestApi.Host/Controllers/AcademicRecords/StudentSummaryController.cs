﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSummaryController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student summary controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Orm.Advantage.Domain.Common;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Services.Interfaces.AcademicRecords;
    using System;
    using System.Threading.Tasks;

    /// <summary>
    /// The student controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentSummaryController : BaseController
    {
        /// <summary>
        /// The attendance service.
        /// </summary>
        private readonly IAttendanceService attendanceService;

        /// <summary>
        /// The student summary service.
        /// </summary>
        private readonly IStudentSummaryService studentSummaryService;

        /// <summary>
        /// The student service.
        /// </summary>
        private readonly IStudentService studentService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentSummaryController"/> class.
        /// </summary>
        /// <param name="attendanceService">
        /// The attendance service.
        /// </param>
        /// <param name="studentSummaryService"></param>
        /// <param name="studentService"></param>
        public StudentSummaryController(IAttendanceService attendanceService, IStudentSummaryService studentSummaryService, IStudentService studentService, IEnrollmentService enrollmentService)
        {
            this.attendanceService = attendanceService;
            this.studentSummaryService = studentSummaryService;
            this.studentService = studentService;
            this.enrollmentService = enrollmentService;
        }

        /// <summary>
        /// The GetAttendanceSummary returns the attendance for a specific student enrollment.
        /// </summary>
        /// <param name="enrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// Returns the attendance summary for a student enrollment
        /// </returns>
        [HttpGet]
        [Route("GetAttendanceSummary")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetAttendanceSummary([FromQuery]Guid enrollmentId)
        {
            return this.Ok(await this.attendanceService.CalculateAttendance(enrollmentId, true, true, true));
        }
        /// <summary>
        /// The GetAttendanceSummary returns the attendance for a specific student enrollment.
        /// </summary>
        /// <param name="enrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// Returns the attendance summary for a student enrollment
        /// </returns>
        [HttpGet]
        [Route("GetStudentContactSummary")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetStudentContactSummary([FromQuery]Guid enrollmentId)
        {
            return this.Ok(await this.studentService.GetStudentContactInfo(enrollmentId));
        }

        /// <summary>
        /// The GetEnrollmentProgramSummary returns the enrollment program section for the student summary.
        /// </summary>
        /// <param name="enrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// Returns the enrollment program summary for a student enrollment
        /// </returns>
        [HttpGet]
        [Route("GetEnrollmentProgramSummary")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetEnrollmentProgramSummary([FromQuery]Guid enrollmentId)
        {
            return this.Ok(await this.enrollmentService.GetEnrollmentProgramSummary(enrollmentId));
        }

        [HttpGet]
        [Route("GetNewBadgeNumber")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetNewBadgeNumber(Guid campusId)
        {
            return this.Ok(this.enrollmentService.GetNewBadgeNumber(campusId));

        }

        /// <summary>
        /// Returns student summary data given enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id for the student.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "REPORTSAPI-Read")]
        [Route("GetStudentSummaryByEnrollmentId")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetStudentSummaryByEnrollmentId(string enrollmentId)
        {
            if (enrollmentId == null)
                return this.MissingRequiredParamResult(enrollmentId);

            var actionResult = await this.studentSummaryService.GetStudentSummaryByEnrollmentId(enrollmentId);

            if (actionResult.Result == null)
                return this.NotFound(enrollmentId);

            return Ok(actionResult);
        }
    }
}