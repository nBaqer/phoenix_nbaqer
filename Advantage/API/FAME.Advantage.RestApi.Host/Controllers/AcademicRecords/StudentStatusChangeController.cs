﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusChangeController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusChangeController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The graduation controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class StudentStatusChangeController : BaseController
    {
        /// <summary>
        /// The graduation service.
        /// </summary>
        private readonly IStudentStatusChangeService studentStatusChangeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentStatusChangeController"/> class.
        /// </summary>
        /// <param name="studentStatusChangeService">
        /// The student status change service.
        /// </param>
        public StudentStatusChangeController(IStudentStatusChangeService studentStatusChangeService)
        {
            this.studentStatusChangeService = studentStatusChangeService;
        }

        /// <summary>
        /// The get graduation statuses.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetGraduationStatuses")]
        public async Task<IActionResult> GetGraduationStatuses(Guid campusId)
        {
            return this.Ok(await this.studentStatusChangeService.GetGraduationStatuses(campusId));
        }

        /// <summary>
        /// The get statuses to filter.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStatusesToFilter")]
        public async Task<IActionResult> GetStatusesToFilter(Guid campusId)
        {
            return this.Ok(await this.studentStatusChangeService.GetStatusesToFilter(campusId));
        }

        /// <summary>
        /// The get have met options.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetHaveMetOptions")]
        public async Task<IActionResult> GetHaveMetOptions()
        {
            return this.Ok(await this.studentStatusChangeService.GetHaveMetOptions());
        }

        /// <summary>
        /// The get students ready for graduation.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="filterByStatus">
        /// The filter by status.
        /// </param>
        /// <param name="haveMetOption">
        /// The have Met Option.
        /// </param>
        /// <param name="pageSize">
        /// The Page Size.
        /// </param>
        /// <param name="page">
        /// The Page.
        /// </param>
        /// <param name="hardLoad">
        /// The hard Load.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStudentsThatHaveMetProgramLength")]
        public async Task<IActionResult> GetStudentsThatHaveMetProgramLength(Guid campusId, Guid filterByStatus, int haveMetOption, int pageSize, int page, bool hardLoad = false)
        {
            return this.Ok(await this.studentStatusChangeService.GetStudentsThatHaveMetProgramLength(campusId, filterByStatus, haveMetOption, pageSize, page, hardLoad));
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="studentStatusChange">
        /// The student Status Change.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("ChangeStatus")]
        public async Task<IActionResult> ChangeStatus([FromBody]StudentStatusChange studentStatusChange)
        {
            return this.Ok(await this.studentStatusChangeService.ChangeStatus(studentStatusChange.CampusId, studentStatusChange.EnrollmentsToUpdate));
        }
    }
}
