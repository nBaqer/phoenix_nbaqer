// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4ResultController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 Result controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The R2T4 result controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class R2T4ResultController : BaseController
    {
        /// <summary>
        /// The R2T4 Result service.
        /// </summary>
        private readonly IR2T4ResultService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4ResultController"/> class. 
        /// </summary>
        /// <param name="service">
        /// The R2T4 Result Service .
        /// </param>
        public R2T4ResultController(IR2T4ResultService service)
        {
            this.service = service;
        }

        /// <summary>
        ///  The GetR2T4Result action method calculates and returns the R2T4 Results for the given R2T4Input data.
        /// </summary>
        /// <remarks>
        /// Post requires the R2T4 input model object which contains the mandatory properties.
        /// Atleast one loan or grant could be disbursed or could be disbursed
        /// Program type, Hours/Days Scheduled and Total hours/days and Withdrawal date.
        /// </remarks> 
        /// <param name="model">
        /// R2T4Input model
        /// </param>
        /// <returns>
        /// Returns an Fame.Core.R2T4Calucaltor.R2T4Input object which contains R2T4 results.
        /// </returns>
        [HttpPost]
        [Route("GetR2T4Result")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetR2T4Result([FromBody] R2T4Input model)
        {
            return this.Ok(await this.service.GetR2T4Results(model));
        }

        /// <summary>
        /// The post action allows to save the R2T4 Results details.
        /// </summary>
        /// <remarks>
        /// The post action requires R2T4Results object. This object contains the properties of all the R2T4 Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Results</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Results data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Results with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = "AR-Create")]
        public async Task<IActionResult> Post([FromBody] R2T4Result model)
        {
            return this.Ok(await this.service.Create(model));
        }

        /// <summary>
        /// The PostOverrideR2T4Result action allows to save the R2T4 Override Results details.
        /// </summary>
        /// <remarks>
        /// The PostOverrideR2T4Result action requires R2T4Results object. This object contains the properties of all the R2T4 Override Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 override results</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Results data provided is invalid</response>
        /// <param name="model">
        /// The PostOverrideR2T4Result action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPost]
        [Route("PostOverrideR2T4Result")]
        [Authorize(Roles = "AR-Create")]
        public async Task<IActionResult> PostOverrideR2T4Result([FromBody] R2T4Result model)
        {
            return this.Ok(await this.service.SaveOverrideR2T4Result(model));
        }

        /// <summary>
        /// The put action allows to update the existing R2T4 Result details.
        /// </summary>
        /// <remarks>
        /// The put action requires R2T4Result object. This object contains the properties of all the R2T4 Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Result</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Result data provided is invalid</response>
        /// <param name="model">
        /// The put action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = "AR-Modify")]
        public async Task<IActionResult> Put([FromBody] R2T4Result model)
        {
            return model.TerminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.Update(model));
        }

        /// <summary>
        /// The PutOverrideResults action allows to update the existing R2T4 Override Result details.
        /// </summary>
        /// <remarks>
        /// The PutOverrideResults action requires R2T4Result object. This object contains the properties of all the R2T4 Override Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Result</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Result data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPut]
        [Route("PutOverrideResults")]
        [Authorize(Roles = "AR-Modify")]
        public async Task<IActionResult> PutOverrideResults([FromBody] R2T4Result model)
        {
            return model.TerminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.UpdateOverrideResult(model));
        }

        /// <summary>
        /// The GetR2T4ResultId action allows to check the terminationid exists in R2T4 results or not.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultId Results action requires R2T4ResultOverride and TerminationId and based on the Ids provided, it returns the matching R2T4 Result Override record.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Result Id</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// Returns the results id based on the matching value. If matching record found then the return empty string.
        /// </returns>
        [HttpPost]
        [Route("GetR2T4ResultId")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetR2T4ResultId([FromBody] R2T4ResultOverride model)
        {
            if (model.TerminationId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("TerminationId"));
            }

            return this.Ok(await this.service.GetR2T4ResultId(model));
        }

        /// <summary>
        /// The get action allows to fetch the R2T4 Result detail.
        /// </summary>
        /// <remarks>
        /// The get action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Results</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> Get(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.Get(terminationId));
        }

        /// <summary>
        /// The IsResultsExists action allows to check if the R2T4 results are existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultsExists action requires terminationId and based on the Id provided, it returns boolean if the R2T4 result record exists or not.
        /// </remarks>
        /// <response code="200">Returns the boolean as true or false</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        [HttpGet]
        [Route("IsResultsExists")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> IsResultsExists(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.IsResultsExists(terminationId));
        }

        /// <summary>
        /// The GetR2T4OverrideResults action allows to fetch the R2T4 Override Results.
        /// </summary>
        /// <remarks>
        /// The GetR2T4OverrideResults action requires terminationId and based on the Id provided, it returns the matching R2T4 Override Result record.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Override Results</response>
        /// <response code="400">Returns the validation error messages when the Termination Id provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Override Result.
        /// </returns>
        [HttpGet]
        [Route("GetR2T4OverrideResults")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetR2T4OverrideResults(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.GetR2T4OverrideResults(terminationId));
        }

        /// <summary>
        /// The GetR2T4ResultsByTerminationId action allows to fetch the R2T4 Result.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsByTerminationId action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Results</response>
        /// <response code="400">Returns the validation error messages when the Termination Id provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        [HttpGet]
        [Route("GetR2T4ResultsByTerminationId")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetR2T4ResultsByTerminationId(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.GetR2T4ResultsByTerminationId(terminationId));
        }

        /// <summary>
        /// The delete action is to delete the R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the R2T4Results, R2T4OverrideResults by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting the R2T4Results, R2T4OverrideResults
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <response code="200">Returns 200 if the delete is successful</response>
        /// <response code="400">Returns 400, if the delete operation failed</response>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = "AR-Delete")]
        public async Task<IActionResult> Delete(Guid terminationId)
        {
            if (terminationId.IsEmpty())
            {
                return new OkObjectResult(new { message = $"{ApiMsgs.DELETE_VERIFICATION_FAILED} {terminationId}" });
            }

            var msg = await this.service.Delete(terminationId);
            return new OkObjectResult(new { message = msg });
        }

        /// <summary>
        /// The IsResultOverridden action allows to check if the R2T4 results is overridden or not for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultOverridden action requires terminationId and based on the Id provided, it returns true if the R2T4 result record is overridden else false.
        /// </remarks>
        /// <response code="200">Returns the boolean as true or false</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        [HttpGet]
        [Route("IsResultOverridden")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> IsResultOverridden(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.IsResultOverridden(terminationId));
        }

        /// <summary>
        /// The GetR2T4ResultsForReport action method returns R2T4 result details for the given termination id.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsForReport action method requires termination Id of type Guid.
        /// </remarks>
        /// <response code="200">Returns the R2T4 Results object in xml for the given termination Id</response>
        /// <response code="404">Data not found in case if the studentId does not exists</response>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object in XML format.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("AR-Read")]
        [Route("GetR2T4ResultsForReport")]
        [Produces("application/xml")]
        public async Task<IActionResult> GetR2T4ResultsForReport(string token, Guid terminationId)
        {
            var missingParam = new List<BadRequestObjectResult>();
            if (token == string.Empty)
            {
                missingParam.Add(this.MissingRequiredParamResult("token"));
            }

            if (terminationId.IsEmpty())
            {
                missingParam.Add(this.MissingRequiredParamResult("Termination Id"));
            }

            if (missingParam.Count > 0)
            {
                return this.Ok(missingParam);
            }

            return this.Ok(await this.service.GetR2T4ResultsForReport(token, terminationId));
        }

        /// <summary>
        /// The UpdateR2T4ResultStatus action allows to update the existing R2T4 Result completion status.
        /// </summary>
        /// <remarks>
        /// The UpdateR2T4ResultStatus action requires termination Id and the status.
        /// Mandatory parameters required : TerminationId, status
        /// </remarks>
        /// <response code="200">Returns the boolean as true or false</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="model">
        /// The UpdateOverrideResults action method requires model object of type R2T4ResultStatus.
        /// </param>
        /// <returns>
        /// Returns update status boolean as true or false.
        /// </returns>
        [HttpPut]
        [Route("UpdateR2T4ResultStatus")]
        [Authorize(Roles = "AR-Modify")]
        public async Task<IActionResult> UpdateR2T4ResultStatus([FromBody] R2T4ResultsCompletedStatus model)
        {
            return model.TerminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.UpdateR2T4ResultStatus(model.TerminationId, model.Status));
        }
    }
}