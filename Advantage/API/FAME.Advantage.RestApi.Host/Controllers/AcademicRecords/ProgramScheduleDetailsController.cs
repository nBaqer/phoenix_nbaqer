﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleDetailsController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule details controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore.Internal;

    /// <inheritdoc />
    /// <summary>
    /// The program schedule details controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ProgramScheduleDetailsController : BaseController
    {
        /// <summary>
        /// The program schedule detail service.
        /// </summary>
        private readonly IProgramScheduleDetailService programScheduleDetailService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramScheduleDetailsController"/> class.
        /// </summary>
        /// <param name="programScheduleDetailService">
        /// The program Schedule Detail Service.
        /// </param>
        public ProgramScheduleDetailsController(IProgramScheduleDetailService programScheduleDetailService)
        {
            this.programScheduleDetailService = programScheduleDetailService;
        }

        /// <summary>
        /// The get credit hour scheduled break returns the scheduled breaks within a specific start and end date.
        /// </summary>
        /// <remarks>
        /// The GetCreditHourScheduledBreak action method returns the returns the scheduled breaks count for the given start date and scheduleId.
        /// It calculate the scheduled breaks for the specific date range.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="404">Returns no content when there are no scheduled break found</response>
        /// <response code="400">Returns the Validation error messages when the enrollmentId provided is invalid</response>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param> 
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// end Date
        /// </param>
        /// <returns>
        /// Returns the success or failure message determine whether the record has been fetched and updated based on enrollmentId.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHourScheduledBreak")]
        public async Task<IActionResult> GetCreditHourScheduledBreak(Guid enrollmentId, DateTime startDate, DateTime endDate)
        {
            var validationError = new List<BadRequestObjectResult>();
            if (enrollmentId.IsEmpty())
            {
                validationError.Add(this.MissingRequiredParamResult("enrollmentId"));
            }

            if (endDate == DateTime.MinValue)
            {
                validationError.Add(this.MissingRequiredParamResult("endDate"));
            }

            if (startDate == DateTime.MinValue)
            {
                validationError.Add(this.MissingRequiredParamResult("startDate"));
            }

            if (validationError.Any())
            {
                return this.Ok(validationError);
            }

            return this.Ok(await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, endDate, startDate));
        }

        /// <summary>
        /// The get credit hour completed days action return the completed days for the specified enrollment and date range.
        /// </summary>
        /// <remarks>
        /// The get credit Hour completed days by EnrollmentId action returns completed days in the period.
        /// It requires enrollmentId.
        /// Based on the EnrollmentId and date range, it searches all the enrolled program details and returns the completed days for the selected period. 
        /// </remarks>
        /// <response code="200">Returns completed days for the specified enrollment and date range</response>
        /// <response code="404">Returns no content when there are no enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// Returns completed days for the selected period.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHourCompletedDays")]
        public async Task<IActionResult> GetCreditHourCompletedDays(Guid enrollmentId, DateTime? endDate)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programScheduleDetailService.GetCreditHourCompletedDaysByEnrollmentId(enrollmentId, endDate));
        }

        /// <summary>
        /// The Get EndDate Of CreditHour PaymentPeriod action returns the End date of the payment period when a student withdraws from a non term not self paced program or when DL period is used for calculation for Nonstandard term programs with terms not substantially equal in length.
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour PaymentPeriod by EnrollmentId action returns End date of the payment period when a student withdraws from a non term not self paced program or when DL period is used for calculation for Nonstandard term programs with terms not substantially equal in length.
        /// it requires enrollmentId .
        /// </remarks>
        /// <response code="200">Returns End date for the specified enrollmentId</response>
        /// <response code="404">Returns no content when there are no enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns End date of the payment period when a student withdraws.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEndDateOfCreditHourPaymentPeriod")]
        public async Task<IActionResult> GetEndDateOfCreditHourPaymentPeriod(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programScheduleDetailService.GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(enrollmentId));
        } 

        /// <summary>
        /// The Get EndDate Of CreditHour Period Of Enrollment action returns the End date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard term program.
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour Period Of Enrollment by EnrollmentId action returns End date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard term program.
        /// it requires enrollmentId .
        /// </remarks>
        /// <response code="200">Returns End date for the specified enrollmentId</response>
        /// <response code="404">Returns no content when there are no enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns End date of the period of enrollment when a student withdraws.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEndDateOfCreditHourPeriodOfEnrollment")]
        public async Task<IActionResult> GetEndDateOfCreditHourPeriodOfEnrollment(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programScheduleDetailService.GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(enrollmentId));
        }
    }
}