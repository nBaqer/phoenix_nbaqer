// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the EnrollmentController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ScheduledHours;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The enrollment controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class AttendanceController : BaseController
    {
        /// <summary>
        /// The attendance service.
        /// </summary>
        private readonly IAttendanceService attendanceService;

        /// <summary>
        /// Initializes a new instance of the EnrollmentController class.
        /// </summary>
        /// <param name="attendanceService">
        /// The enrollment service.
        /// </param>
        public AttendanceController(IAttendanceService attendanceService)
        {
            this.attendanceService = attendanceService;
        }


        /// <summary>
        /// Posts zero for attendance for specific students on specific day given parameters
        /// </summary>
        /// <param name="param">
        /// The student id list and date to post zero's for attendance for
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read, SY-Read")]
        [Route("PostZerosForSingleDay")]
        public async Task<IActionResult> PostZerosForSingleDay([FromBody]PostZeroParams param)
        {
            if (param.StuEnrollIdList == null || !param.StuEnrollIdList.Any())
                return BadRequest(new ActionResult<bool>()
                {
                    ResultStatus = Enums.ResultStatus.Error,
                    ResultStatusMessage = "StuEnrollIdList and Date are required parameters"
                });

            var actionResult = await this.attendanceService.PostZerosForAttendance(param);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Imports time clock file given parameters
        /// </summary>
        /// <param name="param">
        /// The parameters required for importing a time clock file
        /// </param>
        /// <returns>
        /// The <see cref="TimeClockImportResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("ImportTimeClockFile")]
        public async Task<IActionResult> ImportTimeClockFile([FromBody]ImportTimeClockFile param)
        {
            if (param.CampusId == null || param.FilePath == null)
                return BadRequest(new ActionResult<bool>()
                {
                    ResultStatus = Enums.ResultStatus.Error,
                    ResultStatusMessage = "CampusId and FilePath are required parameters"
                });

            var actionResult = await this.attendanceService.ImportTimeClockFile(param);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Inserts or updates time clock import log record
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("UpsertTimeClockLog")]
        public async Task<IActionResult> UpsertTimeClockLog([FromBody]TimeClockLogParams param)
        {
            var actionResult = await this.attendanceService.UpsertTimeClockLog(param);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Returns list of imported time clock log records given campus identifier.
        /// </summary>
        /// <param name="campusId">
        /// Campus Identifier</param>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetTimeClockImportLogsByCampus")]
        public async Task<IActionResult> GetTimeClockImportLogsByCampus(Guid campusId, int count = 25)
        {
            var actionResult = await this.attendanceService.GetTimeClockImportLogsByCampus(campusId, count);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == Enums.ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult.Result);
        }


        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStudentsScheduledForDay")]
        public async Task<IActionResult> GetStudentsScheduledForDayAsync(Guid campusId, DateTime scheduledDay, Guid? studentGroupId, Guid? programVersionId)
        {
            //validate if parameters are sufficient
            if (campusId.IsEmpty() || scheduledDay == DateTime.MinValue)
            {
                return BadRequest("Incorrect Parameters");
            }

            var rv = await this.attendanceService.GetStudentsScheduledForDay(campusId, scheduledDay, studentGroupId, programVersionId);
            return Ok(rv);
        }


        /// <summary>
        /// Given parameters, adjusts scheduled hours for students
        /// </summary>
        /// <response code="200">Given 9010 mappings and campusId, updates records.</response>
        /// <param name="parameters">
        /// Adjustment parameters
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("AdjustScheduledHours")]
        public async Task<IActionResult> AdjustScheduledHours([FromBody]ScheduledHoursAdjustment parameters)
        {
            var parameterValidationFailed = (parameters == null)
                || (parameters.campusId == Guid.Empty)
                || (parameters.AdjustmentMethod == AdjustmentMethod.AdjustScheduledHoursByAmount && !parameters.AmountToAdjust.HasValue);

            if (parameterValidationFailed)
            {
                return BadRequest(ApiMsgs.IncorrectParametersPassed);
            }

            var actionResult = await this.attendanceService.AdjustScheduledHours(parameters);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult);
        }
    }
}