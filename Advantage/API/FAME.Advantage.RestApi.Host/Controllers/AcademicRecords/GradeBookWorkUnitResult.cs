﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradeBookWorkUnitResult.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The grade book work unit result.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;

    /// <summary>
    /// The grade book work unit result.
    /// </summary>
    public class GradeBookWorkUnitResult
    {
        /// <summary>
        /// Gets or sets the result id.
        /// </summary>
        public Guid ResultId { get; set; }

        /// <summary>
        /// Gets or sets the course id.
        /// </summary>
        public Guid CourseId { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid TermId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// Gets or sets the min result.
        /// </summary>
        public decimal? MinResult { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether required.
        /// </summary>
        public bool? Required { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether must pass.
        /// </summary>
        public bool? MustPass { get; set; }

        /// <summary>
        /// Gets or sets the remaining.
        /// </summary>
        public decimal? Remaining { get; set; }

        /// <summary>
        /// Gets or sets the work unit type.
        /// </summary>
        public short? WorkUnitType { get; set; }

        /// <summary>
        /// Gets or sets the work unit description.
        /// </summary>
        public string WorkUnitDescription { get; set; }

        /// <summary>
        /// Gets or sets the instr grd bk wgt detail id.
        /// </summary>
        public Guid? InstrGrdBkWgtDetailId { get; set; }
    }
}



