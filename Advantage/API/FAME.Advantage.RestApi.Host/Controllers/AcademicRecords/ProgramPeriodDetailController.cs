﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramPeriodDetailController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the ProgramPeriodDetailController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The program period detail controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ProgramPeriodDetailController : BaseController
    {
        /// <summary>
        /// The program period detail service.
        /// </summary>
        private readonly IProgramPeriodDetailService programPeriodDetailService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramPeriodDetailController"/> class.
        /// </summary>
        /// <param name="programPeriodDetailService">
        /// The program period detail service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        public ProgramPeriodDetailController(IProgramPeriodDetailService programPeriodDetailService, IEnrollmentService enrollmentService)
        {
            this.programPeriodDetailService = programPeriodDetailService;
            this.enrollmentService = enrollmentService;
        }

        /// <summary>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// </summary>
        /// <remarks>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// Based on the enrollment id it searches all the enrolled program details and returns the total days for the period. 
        /// </remarks>
        /// <response code="200">Returns total days for the specified enrollment id</response>
        /// <response code="404">Returns no content when there is no content</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is of type DateTime is required when the total days is 0.
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number of days required to complete failed courses.
        /// </param>
        /// <returns>
        /// Returns total days for the period.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetTotalDaysForNonTermNotSelfPacedProgram")]
        public async Task<IActionResult> GetTotalDaysForNonTermNotSelfPacedProgram(Guid enrollmentId, DateTime? endDate, int numberOfDaysRequired)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programPeriodDetailService.GetTotalDaysForNonTermNotSelfPacedProgram(enrollmentId, endDate, numberOfDaysRequired));
        }

        /// <summary>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// </summary>
        /// <remarks>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength action method returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="404">Returns no content when there are no days found</response>
        /// <response code="400">Returns the Validation error messages when the enrollmentId provided is invalid</response>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length based on enrollmentId.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetDetailsForNonStandardTermSubstantiallyEqualLength")]
        public async Task<IActionResult> GetDetailsForNonStandardTermSubstantiallyEqualLength(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("enrollmentId")) : this.Ok(await this.programPeriodDetailService.GetNonStandardTermProgramDaysByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The GetDetailsForNonStandardTermNotSubstantiallyEqualLength returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// </summary>
        /// <remarks>
        /// The GetDetailsForNonStandardTermNotSubstantiallyEqualLength action method returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="404">Returns no content when there are no days found</response>
        /// <response code="400">Returns the Validation error messages when the enrollmentId provided is invalid</response>
        /// <param name="enrollmentId">
        /// EnrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is an optional parameter and will considered as end date to calculate Scheduled Breaks and Approved LoA's in case of direct loan payment period.
        /// </param>
        /// <returns>
        /// Returns the success or failure message determine whether the record has been fetched based on enrollmentId.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetDetailsForNonStandardTermNotSubstantiallyEqualLength")]
        public async Task<IActionResult> GetDetailsForNonStandardTermNotSubstantiallyEqualLength(Guid enrollmentId, DateTime? endDate)
        {
            return enrollmentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("enrollmentId")) : this.Ok(await this.programPeriodDetailService.GetNonStandardProgramDaysForNotSubstantiallyEqual(enrollmentId, endDate));
        }

        /// <summary>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <response code="200">Returns completed and total days for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are is no content.</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCompletedAndTotalDaysForStandardTermPrograms")]
        public async Task<IActionResult> GetCompletedAndTotalDaysForStandardTermPrograms(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programPeriodDetailService.GetCompletedAndTotalDaysForStandardTermPrograms(enrollmentId));
        }

        /// <summary>
        /// The GetNonStandardProgramDaysDetail returns the completed days and total days in a payment period for Non-Standard term programs.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardProgramDaysDetail action method returns the completed days and total days in a payment period for Non-Standard term programs.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="404">Returns no content when there are no days found</response>
        /// <response code="400">Returns the Validation error messages when the enrollmentId provided is invalid</response>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// Returns the success or failure message determine whether the record has been fetched based on enrollmentId.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetNonStandardProgramDaysDetail")]
        public async Task<IActionResult> GetNonStandardProgramDaysDetail(Guid enrollmentId, DateTime? endDate)
        {
            return enrollmentId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("enrollmentId")) : this.Ok(await this.programPeriodDetailService.GetNonStandardProgramDaysByEnrollmentId(enrollmentId, endDate));
        }

        /// <summary>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <response code="200">Returns completed and total days for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are is no content.</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is the end date of the period and is of type DateTime.
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number Of days required to complete the failed courses.
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment")]
        public async Task<IActionResult> GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment(Guid enrollmentId, DateTime? endDate, int numberOfDaysRequired)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programPeriodDetailService.GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment(enrollmentId, endDate ?? DateTime.MinValue, numberOfDaysRequired));
        }

        /// <summary>
        /// The GetDaysRequiredToCompleteFailedCourse returns the number of days required to complete failed courses for given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the number of days required to complete failed courses for a given enrollment id
        /// </remarks>
        /// <response code="200">Returns failed course details for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are is no failed course data found.</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is the end date of the period and is of type DateTime.
        /// </param>
        /// <returns>
        /// Returns the details of number of days required to complete failed courses.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetDaysRequiredToCompleteFailedCourse")]
        public async Task<IActionResult> GetDaysRequiredToCompleteFailedCourse(Guid enrollmentId, DateTime? endDate)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("enrollmentId");
            }

            return this.Ok(await this.programPeriodDetailService.GetDaysRequiredToCompleteFailedCourse(enrollmentId, endDate ?? DateTime.MinValue));
        }

        /// <summary>
        /// The get withdrawal period term details returns the term details of the withdrawal period.
        /// </summary>
        /// <remarks>
        /// The get withdrawal period term details returns the term details of the withdrawal period based on the provided enrollment Id.
        /// </remarks>
        /// <response code="200">Returns term details for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are is no terms found.</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <returns>
        /// Returns the term details of the withdrawal period of the enrolled program based on the enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetWithdrawalPeriodTermDetails")]
        public async Task<IActionResult> GetWithdrawalPeriodTermDetails(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programPeriodDetailService.GetWithdrawalPeriodTermDetails(enrollmentId));
        }

        /// <summary>
        /// The GetLastDateAttended returns last date attended from attendance table.
        /// </summary> 
        /// <response code="200">Returns last date attended for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are no attendance record found the specified enrollment id.</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The last date attended.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetLastDateAttended")]
        public async Task<IActionResult> GetLastDateAttended(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.enrollmentService.GetLDA(enrollmentId));
        }
    }
}