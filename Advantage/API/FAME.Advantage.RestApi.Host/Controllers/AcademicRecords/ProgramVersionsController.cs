﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionsController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Program Versions Controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Threading.Tasks;

    using Extensions;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using Services.Interfaces.AcademicRecords;

    /// <inheritdoc />
    /// <summary>
    /// The program versions controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class ProgramVersionsController : BaseController
    {
        /// <summary>
        /// The program versions service.
        /// </summary>
        private readonly IProgramVersionsService programVersionsService;

        /// <summary>
        /// The program schedule service.
        /// </summary>
        private readonly IProgramScheduleService programScheduleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionsController"/> class.
        /// </summary>
        /// <param name="programVersionsService">
        /// The program versions service.
        /// </param>
        /// <param name="programScheduleService">
        /// The program schedule service.
        /// </param>
        public ProgramVersionsController(IProgramVersionsService programVersionsService, IProgramScheduleService programScheduleService)
        {
            this.programVersionsService = programVersionsService;
            this.programScheduleService = programScheduleService;
        }

        /// <summary>
        /// The get program length action returns program length for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The get program length action returns program length for Title IV  clock hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program length.
        /// </remarks>
        /// <response code="200">Returns matching students enrollment details based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program length.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramLength")]
        public async Task<IActionResult> GetProgramLength(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetProgramLengthByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The get payment periods action returns payment periods for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The payment periods action returns payment periods for Title IV  clock hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it searches all the enrolled program details and returns payment periods.
        /// </remarks>
        /// <response code="200">Returns students payment periods details based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment periods.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetPaymentPeriod")]
        public async Task<IActionResult> GetPaymentPeriod(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetPaymentPeriodsByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The get enrollment periods length action returns the length of payment periods for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The enrollment periods action returns returns the length enrollment periods for Title IV  clock hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it searches all the enrolled program details and returns payment periods length.
        /// </remarks>
        /// <response code="200">Returns students enrollment periods length based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns enrollment periods length.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetEnrollmentPeriodsLength")]
        public async Task<IActionResult> GetEnrollmentPeriodsLength(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetEnrollmentPeriodsLengthByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The get credit hour program length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks> 
        /// <response code="200">Returns program credits and weeks based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHourProgramLength")]
        public async Task<IActionResult> GetCreditHourProgramLength(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetCreditHourProgramLengthByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The GetHoursScheduledToComplete action returns Hours Scheduled To Complete for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetHoursScheduledToComplete action returns Hours Scheduled To Complete for Title IV  clock hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns Hours Scheduled To Complete.
        /// </remarks> 
        /// <response code="200">Returns students hours scheduled to complete based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns ScheduledAndTotalHours object with scheduled hours and total hours.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetHoursScheduledToComplete")]
        public async Task<IActionResult> GetHoursScheduledToComplete(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetHoursScheduledToComplete(enrollmentId));
        }

        /// <summary>
        /// The get withdrawal payment period action method returns the payment period by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The get withdrawal payment period action method returns the payment period by given enrollment Id,
        /// It calculate the scheduled hours and the total hours in the payment period for a clock hour Title 4 eligible program from which the student has withdrawn.
        /// </remarks>
        /// <response code="200">Returns payment period based on the completed and scheduled hrs</response>
        /// <response code="404">Returns no content when there is no payment period found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetWithdrawalPaymentPeriod")]
        public async Task<IActionResult> GetWithdrawalPaymentPeriod(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetWithdrawalPaymentPeriod(enrollmentId));
        }

        /// <summary>
        /// The get period of enrollment with excused absence returns List period of enrollments details by enrollment id.
        /// </summary>
        /// <remarks>
        /// The get period of enrollment with excused absence returns List period of enrollments details by enrollment id.
        /// This action method calculates allowed excused absence per period of enrollment and return a list of period
        /// of enrollments for given enrollment id
        /// </remarks>
        /// <response code="200">Returns list period of enrollment based on given enrollment id</response>
        /// <response code="404">Returns no content when there is no period of enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// This method returns PeriodOfEnrollmentDetails with list of PeriodOfEnrollments and a ResultStatus .
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetPeriodOfEnrollmentWithExcusedAbsence")]
        public async Task<IActionResult> GetPeriodOfEnrollmentWithExcusedAbsence(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetPeriodOfEnrollmentWithExcusedAbsence(enrollmentId));
        }

        /// <summary>
        /// The get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns payment periods.
        /// </remarks>
        /// <response code="200">Returns students credit hour program payment periods details based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns credit hour program payment periods.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHourPaymentPeriod")]
        public async Task<IActionResult> GetCreditHourPaymentPeriodsByEnrollmentId(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetCreditHourPaymentPeriodsByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id,
        /// It calculates the scheduled hours and the total hours in the period of enrollment for the clock hour Title VI eligible program from which the student has withdrawn.
        /// </remarks>
        /// <response code="200">Returns period of enrollment based on the completed and scheduled hrs</response>
        /// <response code="404">Returns no content when there is no period of enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the withdrawal details of period of enrollment of the student based on the enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetWithdrawalPeriodOfEnrollment")]
        public async Task<IActionResult> GetWithdrawalPeriodOfEnrollment(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetWithdrawalPeriodOfEnrollment(enrollmentId));
        }

        /// <summary>
        /// The get credit hour period length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour period length by enrollment id action returns program period length and weeks for a credit Hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks> 
        /// <response code="200">Returns program credits and weeks based on the input parameter</response>
        /// <response code="404">Returns no content when there is no students enrollment details based on the input parameter</response>
        /// <response code="400">Returns the input parameter is required and is missing in the request body</response> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program periods credits and weeks.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHourPeriodsLength")]
        public async Task<IActionResult> GetCreditHourPeriodsLength(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetCreditHourPeriodsLengthByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The get scheduled and total hours for period of enrollments by given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the scheduled hours and total hours per period of enrollment for a given enrollment id
        /// </remarks>
        /// <response code="200">Returns scheduled and total hours based on given enrollment id</response>
        /// <response code="404">Returns no content when there is no period of enrollment found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetScheduledAndTotalHoursForPeriodOfEnrollment")]
        public async Task<IActionResult> GetScheduledAndTotalHoursForPeriodOfEnrollment(Guid enrollmentId)
        {
            return enrollmentId.IsEmpty() ? this.MissingRequiredParamResult("EnrollmentId") : this.Ok(await this.programVersionsService.GetScheduledAndTotalHoursForPeriodOfEnrollment(enrollmentId));
        }

        /// <summary>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair.
        /// </summary>
        /// <remarks>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair,
        /// suitable for a dropdown.
        /// </remarks>
        /// <response code="200">Returns program versions as value/text</response>
        /// <response code="404">Returns no content when there are no program versions found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetTitleIVProgramVersionsByCampus")]
        public async Task<IActionResult> GetTitleIvProgramVersionsByCampus(Guid campusId)
        {
            return campusId.IsEmpty() ? this.MissingRequiredParamResult("CampusId") : this.Ok(await this.programVersionsService.GetTitleIvProgramVersionsByCampus(campusId));
        }

        /// <summary>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair.
        /// </summary>
        /// <remarks>
        /// The get program version by campus id method returns the program versions by given campus Id as a text/value pair,
        /// suitable for a dropdown.
        /// </remarks>
        /// <response code="200">Returns program versions as value/text</response>
        /// <response code="404">Returns no content when there are no program versions found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramVersionsByCampus")]
        public async Task<IActionResult> GetProgramVersionsByCampus(Guid campusId)
        {
            return campusId.IsEmpty() ? this.MissingRequiredParamResult("CampusId") : this.Ok(await this.programVersionsService.GetProgramVersionsByCampus(campusId));
        }

        /// <summary>
        /// The set program registration type.
        /// </summary>
        /// <param name="programRegistrationType">
        /// The program registration type.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="gradeScaleId">
        /// The grade Scale Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Create")]
        [Route("SetProgramRegistrationType")]
        public async Task<IActionResult> SetProgramRegistrationType(string programRegistrationType, Guid programVersionId, Guid? gradeScaleId)
        {
            //ByClass ByProgram
            var programRegistrationTypeEnums = Enums.ProgramRegistrationType.ByClass;
            if (programRegistrationType != "ByClass")
            {
                programRegistrationTypeEnums = Enums.ProgramRegistrationType.ByProgram;
            }
           

            if (programVersionId.IsEmpty())
            {
                return this.MissingRequiredParamResult("programVersionId");
            }

            if (programRegistrationTypeEnums != Enums.ProgramRegistrationType.ByClass && programRegistrationTypeEnums != Enums.ProgramRegistrationType.ByProgram)
            {
                return this.BadRequest($"{ApiMsgs.MALFORMED_MODEL_ERROR}");
            }

            return this.Ok(await this.programVersionsService.SetProgramRegistrationType(programRegistrationTypeEnums, programVersionId, gradeScaleId));
        }

        /// <summary>
        /// The have enrollments.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Create")]
        [Route("HaveEnrollments")]
        public async Task<IActionResult> HaveEnrollments(Guid programVersionId)
        {
            return programVersionId.IsEmpty() ? this.MissingRequiredParamResult("ProgramVersionId") : this.Ok(await this.programVersionsService.HaveEnrollments(programVersionId));
        }

        /// <summary>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the payment period and credits earned by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the returns the payment period and credits earned by given enrollment Id,
        /// It calculate the scheduled hours and the total hours in the payment period for a credit hour Title 4 eligible program from which the student has withdrawn.
        /// </remarks>
        /// <response code="200">Returns payment period and credits earned based on the completed and schedules by given enrollment Id</response>
        /// <response code="404">Returns no content when there is no payment period and credits earned found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCreditHoursWithdrawalPaymentPeriod")]
        public async Task<IActionResult> GetCreditHoursWithdrawalPaymentPeriod(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetCreditHoursWithdrawalPaymentPeriod(enrollmentId));
        }

        /// <summary>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// </summary>
        /// <remarks>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// It requires enrollmentId. Based on the EnrollmentId, it searches all the enrolled program details 
        /// and returns the payment period details. 
        /// </remarks>
        /// <response code="200">Returns payment period details for the specified enrollment id</response>
        /// <response code="404">Returns no content when there are no payment period details found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment period details for the specified enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetPaymentPeriodForNotSubstantiallyEqualInLength")]
        public async Task<IActionResult> GetPaymentPeriodForNotSubstantiallyEqualInLength(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetPaymentPeriodForNotSubstantiallyEqualInLength(enrollmentId));
        }

        /// <summary>
        /// The get program version registration type 
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <response code="200">Returns the Registration Type for the given program version (0 for By Class, 1 for By Program) </response>
        /// <response code="404">Returns no content when the enrollment or program version is not found.</response>
        /// <response code="400">Returns missing input parameter if the enrollment id is empty or null</response>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetRegistrationType")]
        public async Task<IActionResult> GetRegistrationType([Required]Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetRegistrationType(enrollmentId));
        }

        /// <summary>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the withdrawal period of enrollment for credit hours by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the period of enrollment on which student has withdrawn from the program for credit hours by given enrollment id.
        /// </remarks>
        /// <response code="200">Returns the Non Standard Withdrawal Period Of Enrollment details</response>
        /// <response code="404">Returns no content when the Withdrawal Period is not found.</response>
        /// <response code="400">Returns missing input parameter if the enrollment id is empty or null</response>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The CurrentPeriodDetailsWithCreditsEarned with data like credit earned, weeks completed, withdrawal period etc.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetNonStandardWithdrawalPeriodOfEnrollment")]
        public async Task<IActionResult> GetNonStandardWithdrawalPeriodOfEnrollment([Required]Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetNonStandardWithdrawalPeriodOfEnrollment(enrollmentId));
        }

        /// <summary>
        /// The GetCampusProgramVersionDetailsByEnrollmentId action method returns campus program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the campus program version details based on enrollment id and the selected campus id.
        /// </remarks>
        /// <response code="200">Returns campus program version details based on enrollment id</response>
        /// <response code="404">Returns no content when there is no campus program version details found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the campus program version details based on enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetCampusProgramVersionDetailsByEnrollmentId")]
        public async Task<IActionResult> GetCampusProgramVersionDetailsByEnrollmentId(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetCampusProgramVersion(enrollmentId));
        }

        /// <summary>
        /// The get all program version by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAllProgramVersionByCampus")]
        public async Task<IActionResult> GetAllProgramVersionByCampus(Guid campusId)
        {
            if (campusId.IsEmpty())
            {
                return this.MissingRequiredParamResult("campusId");
            }

            return this.Ok(await this.programVersionsService.GetAllProgramVersionByCampus(campusId));
        }

        /// <summary>
        /// The GetProgramVersionByEnrollmentId action method returns program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetProgramVersionByEnrollmentId action method returns the program version details based on enrollment id and the selected campus id.
        /// </remarks>
        /// <response code="200">Returns program version details based on enrollment id</response>
        /// <response code="404">Returns no content when there is no program version details found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the program version details based on enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramVersionDetailByEnrollmentId")]
        public async Task<IActionResult> GetProgramVersionDetailByEnrollmentId(Guid enrollmentId)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetProgramVersionDetailByEnrollmentId(enrollmentId));
        }

        /// <summary>
        /// The GetStudentAwardsByEnrollmentId action fetches student awards details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the student awards details based on enrollment id.
        /// </remarks>
        /// <response code="200">Returns student awards details based on enrollment id</response>
        /// <response code="404">Returns no content when there is no student awards details found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="lastAttendedDate">        
        /// The last Attended Date is a nullable date field and is of type datetime.
        /// </param>
        /// <returns>
        /// Returns the student awards details based on enrollment id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetStudentAwardsByEnrollmentId")]
        public async Task<IActionResult> GetStudentAwardsByEnrollmentId(Guid enrollmentId, DateTime? lastAttendedDate)
        {
            if (enrollmentId.IsEmpty())
            {
                return this.MissingRequiredParamResult("EnrollmentId");
            }

            return this.Ok(await this.programVersionsService.GetStudentAwardsByEnrollmentId(enrollmentId, lastAttendedDate));
        }

        /// <summary>
        /// Returns program version description given programVersionId
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "REPORTSAPI-Read")]
        [Route("GetProgramVersionById")]
        [ApiExplorerSettings(IgnoreApi = false)]
        public async Task<IActionResult> GetProgramVersionById(string programVersionId)
        {
            if (programVersionId == null)
                return this.MissingRequiredParamResult(programVersionId);

            var actionResult = await this.programVersionsService.GetProgramVersionById(programVersionId);

            if (actionResult.Result == null)
                return this.NotFound(programVersionId);

            return Ok(actionResult);
        }
        /// <summary>
        /// The GetScheduleDetailsByProgramVersionId action method returns program version schedule details based on program version id.
        /// </summary>
        /// <remarks>
        /// The GetScheduleDetailsByProgramVersionId action method returns the program version schedule details based on program version id.
        /// </remarks>
        /// <response code="200">Returns program version schedule details based on program version id</response>
        /// <response code="404">Returns no content when there is no program version schedule details found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="programVersionId">
        /// The programVersion id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the program version schedule details based on program version id.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetScheduleDetailsByProgramVersionId")]
        public async Task<IActionResult> GetScheduleDetailsByProgramVersionId(Guid programVersionId)
        {
            if (programVersionId.IsEmpty())
            {
                return this.MissingRequiredParamResult("ProgramVersionId");
            }

            return this.Ok(await this.programScheduleService.GetDetailByProgramVersionId(programVersionId));
        }

        /// <summary>
        /// The GetProgramVersionTotalHours action method returns program version total hours based on program version id.
        /// </summary>
        /// <remarks>
        /// The GetProgramVersionTotalHours action method returns the program version total hours based on program version id.
        /// </remarks>
        /// <response code="200">Returns program version total hours based on program version id</response>
        /// <response code="404">Returns no content when there is no program version found</response>
        /// <response code="400">Returns missing input parameter in the request body</response>
        /// <param name="programVersionId">
        /// The programVersion id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the program version total hours based on program version id.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramVersionTotalHours")]
        public async Task<IActionResult> GetProgramVersionTotalHours(Guid programVersionId)
        {
            if (programVersionId.IsEmpty())
            {
                return this.MissingRequiredParamResult("ProgramVersionId");
            }

            return this.Ok(await this.programVersionsService.GetProgramVersionTotalHours(programVersionId));
        }

        /// <summary>
        /// The get program version total hours.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
         [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetProgramVersionsForNaccas")]
        public async Task<IActionResult> GetProgramVersionsForNaccas(Guid? campusId)
        {

            if (!campusId.HasValue)
            {
                return this.MissingRequiredParamResult("CampusId");
            }

            return this.Ok(await this.programVersionsService.GetProgramVersionsForNaccas(campusId.Value));
        }

        /// <summary>
        /// The get program version definition for lms including courses and course groups.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetLmsProgramVersionDefinitionWithCourses")]
        public async Task<IActionResult> GetLmsProgramVersionDefinitionWithCourses(Guid programVersionId)
        {

            if (programVersionId.IsEmpty())
            {
                return this.MissingRequiredParamResult("ProgramVersionId");
            }

            return this.Ok(await this.programVersionsService.GetLmsProgramVersionDefinition(programVersionId));
        }
    }
}