﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The class section controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The class section controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
    public class ClassSectionController : BaseController
    {
        /// <summary>
        /// The class section service.
        /// </summary>
        private readonly IClassSectionService classSectionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionController"/> class.
        /// </summary>
        /// <param name="classSectionService">
        /// The class section service.
        /// </param>
        public ClassSectionController(IClassSectionService classSectionService)
        {
            this.classSectionService = classSectionService;
        }

        /// <summary>
        /// The get lms class section period.
        /// </summary>
        /// <param name="campusGroupdId">
        /// The campus groupd id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetLmsClassSectionPeriods")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetLmsClassSectionPeriods(Guid campusGroupdId)
        {
            if (campusGroupdId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("CampusGroupId"));
            }
            return this.Ok(await this.classSectionService.GetLmsPeriods(campusGroupdId));
        }
    }
}