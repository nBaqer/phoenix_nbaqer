// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2t4InputController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 Input controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc; 

    /// <summary>
    /// The R2T4 input controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AcademicRecords/[controller]")]
#if !DEBUG
    [ApiExplorerSettings(IgnoreApi = true)]
#endif
    public class R2T4InputController : BaseController
    {
        /// <summary>
        /// The r2T4 input service.
        /// </summary>
        private readonly IR2T4InputService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4InputController"/> class.
        /// </summary>
        /// <param name="service">
        /// The R2T4 input service.
        /// </param>
        public R2T4InputController(IR2T4InputService service)
        {
            this.service = service; 
        }

        /// <summary>
        /// The get action allows to fetch the R2T4 Input.
        /// </summary>
        /// <remarks>
        /// The get action requires R2T4InputId and TerminationId and based on the Ids provided, it returns the matching R2T4 Input record.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Input</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Input data provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Input where R2T4 Input contains all the information related to R2T4 Input.
        /// </returns>
        [HttpGet]
        [Route("Get")]
        [Authorize(Roles = "AR-Read")]
        [ValidateModel]
        public async Task<IActionResult> Get(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.Get(terminationId));
        }

        /// <summary>
        /// The post action allows to save the R2T4 Input details.
        /// </summary>
        /// <remarks>
        /// The post action requires R2T4Input object. This object contains the properties of all the R2T4 Input data. 
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Input</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Input data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPost]
        [Route("Post")]
        [Authorize(Roles = "AR-Create")]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] R2T4Input model)
        {
            return this.Ok(await this.service.Create(model));
        }

        /// <summary>
        /// The put action allows to update the existing R2T4 Input details.
        /// </summary>
        /// <remarks>
        /// The put action requires R2T4Input object. This object contains the properties of all the R2T4 Input data.
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks>
        /// <response code="200">Returns the saved R2T4 Input</response>
        /// <response code="400">Returns the Validation error messages when the R2T4 Input data provided is invalid</response>
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        [HttpPut]
        [Route("Put")]
        [Authorize(Roles = "AR-Modify")]
        [ValidateModel]
        public async Task<IActionResult> Put([FromBody] R2T4Input model)
        {
            return this.Ok(await this.service.Update(model));
        }

        /// <summary>
        /// The delete action is to delete all the R2T4 details like R2T4Input, R2T4OverrideInput, R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related R2T4 details by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <response code="200">Returns 200 if the delete is successful</response>
        /// <response code="400">Returns 400, if the delete operation failed</response>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        [HttpDelete]
        [Route("Delete")]
        [Authorize(Roles = "AR-Delete")]
        public async Task<IActionResult> Delete(Guid terminationId)
        {
            if (terminationId.IsEmpty())
            {
                return new OkObjectResult(new { message = $"{ApiMsgs.DELETE_VERIFICATION_FAILED} {terminationId}" });
            }

            var msg = await this.service.Delete(terminationId);
            return new OkObjectResult(new { message = msg });
        }

        /// <summary>
        /// The IsR2T4InputExists action allows to check if the R2T4 Input data is existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsR2T4InputExists action requires terminationId and based on the terminationId provided, it returns boolean value if the R2T4 Input record exists or not.
        /// </remarks>
        /// <response code="200">Returns the boolean as true or false</response>
        /// <response code="400">Returns the Validation error messages when the Termination Id data provided is invalid</response>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        [HttpGet]
        [Route("IsR2T4InputExists")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> IsR2T4InputExists(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.IsR2T4InputExists(terminationId));
        }

        /// <summary>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// </summary>
        /// <remarks>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// It requires terminationId based on which the completion status of each tabs is determined.
        /// </remarks>
        /// <response code="200">Returns 200 if the data is fetched successfully</response>
        /// <response code="400">Returns the Validation error messages when the termination id provided is invalid</response>
        /// <param name="terminationId">
        /// terminationId is a required field and it is of type guid .
        /// </param>
        /// <returns>
        /// Returns the boolean values of the tabs to determine whether the tab is completed or not based on terminationId.
        /// </returns>
        [HttpGet]
        [Route("GetR2T4CompletedStatus")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GetR2T4CompletedStatus(Guid terminationId)
        {
            return terminationId.IsEmpty() ? this.Ok(this.MissingRequiredParamResult("TerminationId")) : this.Ok(await this.service.GetR2T4CompletedStatus(terminationId));
        }
    }
}