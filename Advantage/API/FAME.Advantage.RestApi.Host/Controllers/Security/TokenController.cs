// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TokenController.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the TokenController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Security
{
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Orm.AdvTenant.Domain.Entities;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System.Threading.Tasks;

    /// <summary>
    /// The token controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class TokenController : BaseController
    {
        /// <summary>
        /// The auth service.
        /// </summary>
        private readonly IAuthService authService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenController"/> class.
        /// </summary>
        /// <param name="authService">
        /// The auth service.
        /// </param>
        public TokenController(IAuthService authService)
        {
            this.authService = authService;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody]TokenRequest model)
        {
            var token = await this.authService.GetToken(model.Username, model.Password, model.TenantName);
            return token.Message == ApiMsgs.TOKEN_SUCCESS ? (IActionResult)this.Ok(token) : this.Unauthorized();
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Route("ImpersonatedToken")]
        [Authorize(Roles = "SY-Delete")]
        [ValidateModel]
        public async Task<IActionResult> ImpersonatedToken([FromBody]TokenRequestImpersonate model)
        {
            var token = await this.authService.GetToken(model.UserId, model.TenantName);
            return token.Message == ApiMsgs.TOKEN_SUCCESS ? (IActionResult)this.Ok(token) : this.Unauthorized();
        }

        /// <summary>
        /// Empty request for refreshing token. New token is sent back as header 'Authorization'.
        /// </summary>
        [HttpPost]
        [Route("RefreshToken")]
        [Authorize]
        public IActionResult RefreshToken()
        {
            return Ok("");
        }
    }
}