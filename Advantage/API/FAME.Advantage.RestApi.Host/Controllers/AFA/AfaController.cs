﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaController.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the AfaController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Controllers.AFA
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;
    using FAME.Extensions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    using AfaLeadSyncException = Fame.EFCore.Advantage.Entities.AfaLeadSyncException;

    /// <summary>
    /// The AFA controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/AFA/[controller]")]
    public class AfaController : BaseController
    {
        /// <summary>
        /// The AFA service.
        /// </summary>
        private IAfaService afaService;

        private IAfaLeadSyncExceptionService afaLeadSyncExceptionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AfaController"/> class.
        /// </summary>
        /// <param name="afaService">
        /// The AFA service.
        /// </param>
        public AfaController(IAfaService afaService, IAfaLeadSyncExceptionService afaLeadSyncExceptionService)
        {
            this.afaService = afaService;
            this.afaLeadSyncExceptionService = afaLeadSyncExceptionService;
        }

        // get afaSessionKey

        /// <summary>
        /// The get AFA session key.
        /// </summary>
        /// <param name="afaSession">
        /// The AFA session.
        /// </param>
        /// <returns>
        /// The <see cref="AfaSession"/>.
        /// </returns>
        [HttpPost]
        [Route("GetAfaSessionKey")]
        public AfaSession GetAfaSessionKey([FromBody]AfaSession afaSession)
        {
            return this.afaService.GetAfaSessionKey(afaSession);
        }

        // get allDemographics

        /// <summary>
        /// The get all lead demographics.
        /// </summary>
        /// <param name="sessionKey">
        /// The session key.
        /// </param>
        /// <param name="campusId">
        /// The school campusId.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "AD-Read")]
        [Route("GetAllLeadDemographics")]
        public async Task<IList<AdvStagingDemographicsV1>> GetAllLeadDemographicsAsync(string sessionKey, Guid campusId)
        {
            return await this.afaService.GetAllLeadDemographicsAsync(sessionKey, campusId);
        }

        // sync all demographics wil b in afaintegrationVM.ts

        // populate the demographics excpetion grid

        /// <summary>
        /// The insert AFA lead sync exception.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AD-Modify")]
        [Route("InsertAfaLeadSyncException")]
        public async Task<IActionResult> InsertAfaLeadSyncException([FromBody] DataTransferObjects.AFA.AFALeadSyncException record)
        {
            return this.Ok(await this.afaLeadSyncExceptionService.InsertAfaLeadSyncException(record));
        }

        [HttpGet]
        [Authorize(Roles = "AD-Read")]
        [Route("GetAfaLeadSyncException")]
        public async Task<IActionResult> GetAfaLeadSyncException()
        {
            return this.Ok(await this.afaLeadSyncExceptionService.GetAfaLeadSyncException());
        }

        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostAllProgramVersionToAfa")]
        public async Task<IActionResult> PostAllProgramVersionToAfa([FromBody]IEnumerable<AdvStagingProgramV1> prgVersions)
        {
            return this.Ok(await this.afaService.PostAllProgramVersionToAfa(prgVersions));
        }

        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostAllEnrollmentsToAfa")]
        public async Task<IActionResult> PostAllEnrollmentsToAfa([FromBody]IEnumerable<AdvStagingEnrollmentV2> enrollments)
        {
            return this.Ok(await this.afaService.PostAllEnrollmentsToAfa(enrollments));
        }

        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAllPaymentPeriods")]
        public async Task<IActionResult> GetAllPaymentPeriods(Guid campusId)
        {
            return this.Ok(await this.afaService.GetAllPaymentPeriodsFromAfa(campusId));
        }

        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostPaymentPeriodsToAdvantage")]
        public async Task<IActionResult> PostPaymentPeriodsToAdvantage([FromBody]IEnumerable<AdvStagingPaymentPeriodV1> paymentPeriods)
        {
            return this.Ok(await this.afaService.PostPaymentPeriodsToAdvantage(paymentPeriods));
        }

        [HttpGet]
        [Authorize(Roles = "AR-Read")]
        [Route("GetAfaEnrollmentsForCampus")]
        public async Task<IActionResult> GetAfaEnrollmentsForCampus(Guid campusId)
        {
            return this.Ok(this.afaService.GetAfaStudentEnrollmentsForCampus(campusId));
        }

        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("UpdateEnrollmentsStatus")]
        public async Task<IActionResult> UpdateEnrollmentsStatus([FromBody]string cmsId)
        {
            try
            {
                await this.afaService.UpdateEnrollmentsStatus(cmsId);
                return this.Ok();

            }
            catch
            {
                return this.StatusCode(500);
            }
        }

        [HttpGet]
        [Authorize(Roles = "AR-Modify")]
        [Route("GetAfaEnrollmentDetails")]
        public async Task<IActionResult> GetAfaEnrollmentDetails(Guid enrollmentId)
        {
            try
            {
                return this.Ok(await this.afaService.GetAfaEnrollmentDetails(enrollmentId));

            }
            catch
            {
                return this.StatusCode(500);
            }
        }


        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("GetAfaEnrollmentDetails")]
        public async Task<IActionResult> GetAfaEnrollmentDetails([FromBody]IEnumerable<Guid> enrollmentIds)
        {
            try
            {
                return this.Ok(await this.afaService.GetAfaEnrollmentDetails(enrollmentIds));

            }
            catch
            {
                return this.StatusCode(500);
            }
        }

        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("CalculateAttendanceUpdatesForEnrollments")]
        public async Task<IActionResult> CalculateAttendanceUpdatesForEnrollments([FromBody]IEnumerable<Guid> enrollmentIds)
        {
            return this.Ok(await this.afaService.CalculateAttendanceUpdatesForEnrollments(enrollmentIds));
        }
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("CalculateAttendanceUpdatesForEnrollment")]
        public async Task<IActionResult> CalculateAttendanceUpdatesForEnrollment([FromBody]Guid enrollmentId)
        {
            return this.Ok(await this.afaService.CalculateAttendanceUpdatesForEnrollment(enrollmentId));
        }
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("CalculateAllAttendanceUpdatesForCampus")]
        public async Task<IActionResult> CalculateAllAttendanceUpdatesForCampus([FromBody]Guid campusId)
        {
            return this.Ok(await this.afaService.CalculateAllAttendanceUpdatesForCampus(campusId));
        }
        [HttpPost]
        [Authorize(Roles = "AR-Modify")]
        [Route("PostAttendanceUpdatesToAfa")]
        public async Task<IActionResult> PostAttendanceUpdatesToAfa([FromBody]IEnumerable<AdvStagingAttendanceV1> attendanceUpdates)
        {
            return this.Ok(await this.afaService.PostAttendanceUpdatesToAfa(attendanceUpdates));
        }

        [HttpGet]
        [Authorize(Roles = "SA-Read")]
        [Route("GetAllAwards")]
        public async Task<IActionResult> GetAllAwards(Guid campusId)
        {
            return this.Ok(await this.afaService.GetAllTitleIVAwardsFromAfa(campusId));
        }
        [HttpPost]
        [Authorize(Roles = "SA-Modify")]
        [Route("PostAwardsToAdvantage")]
        public async Task<IActionResult> PostAwardsToAdvantage([FromBody]IEnumerable<AdvStagingAwardV1> awards)
        {
            return this.Ok(await this.afaService.PostTitleIVAwardsToAdvantage(awards));
        }
        [HttpPost]
        [Authorize(Roles = "SA-Modify")]
        [Route("SyncAwardsToAdvantage")]
        public async Task<IActionResult> SyncAwardsToAdvantage([FromBody]IEnumerable<AdvStagingAwardV1> awards)
        {
            return this.Ok(await this.afaService.SyncAwardsToAdvantage(awards));
        }
        [HttpGet]
        [Authorize(Roles = "SA-Read")]
        [Route("GetAllDisbursements")]
        public async Task<IActionResult> GetAllDisbursements(Guid campusId)
        {   
            return this.Ok(await this.afaService.GetAllTitleIVDisbursementsFromAfa(campusId));
        }
        [HttpGet]
        [Authorize(Roles = "SA-Modify")]
        [Route("UpdateDisbursementTransactionReference")]
        public async Task<IActionResult> UpdateDisbursementTransactionReference(Guid campusId)
        {
            return this.Ok(await this.afaService.UpdateDisbursementTransactionReference(campusId));
        }

        [HttpPost]
        [Authorize(Roles = "SA-Modify")]
        [Route("PostDisbursementsToAdvantage")]
        public async Task<IActionResult> PostDisbursementsToAdvantage([FromBody]IEnumerable<AdvStagingDisbursementV1> disbursements)
        {
            return this.Ok(await this.afaService.PostTitleIVDisbursementsToAdvantage(disbursements));
        }
        [HttpPost]
        [Authorize(Roles = "SA-Modify")]
        [Route("SyncDisbursementsToAdvantage")]
        public async Task<IActionResult> SyncDisbursementsToAdvantage([FromBody]IEnumerable<AdvStagingDisbursementV1> disbursements)
        {
            return this.Ok(await this.afaService.SyncDisbursementsToAdvantage(disbursements));
        }
        [HttpPost]
        [Authorize(Roles = "SA-Modify")]
        [Route("PostAllRefundsToAfa")]
        public async Task<IActionResult> PostAllRefundsToAfa([FromBody]Guid campusId)
        {
            return this.Ok(await this.afaService.PostAllRefundsToAfa(campusId));
        }
    }
}
