﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusCheckController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The status check controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Controllers.Tools
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Controllers.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;
    using FAME.Orm.AdvTenant.Domain.Entities;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The status check controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ValidateModel]
    public class StatusCheckController : BaseController
    {
        /// <summary>
        /// The status check service.
        /// </summary>
        private readonly IStatusCheckService statusCheckService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusCheckController"/> class.
        /// </summary>
        /// <param name="statusCheckService">
        /// The status check service.
        /// </param>
        public StatusCheckController(IStatusCheckService statusCheckService)
        {
            this.statusCheckService = statusCheckService;
        }

        /// <summary>
        /// The post.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            return this.Ok(this.statusCheckService.GetStatus());
        }
    }
}