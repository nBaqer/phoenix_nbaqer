﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StorageController.cs" company="Fame Inc.">
//   Fame Inc. 2019
// </copyright>
// <summary>
//   The Storage controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.KlassApp
{
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;
    using FileConfigurationFeature = DataTransferObjects.Common.Enums.FileConfigurationFeature;

    /// <summary>
    /// The storage controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Maintenance/[controller]")]
    public class StorageController : BaseController
    {
        /// <summary>
        /// The klass app service.
        /// </summary>
        private readonly IStorageService storageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageController"/> class.
        /// </summary>
        /// <param name="storageService">
        /// The storage service.
        /// </param>
        public StorageController(IStorageService storageService)
        {
            this.storageService = storageService;
        }

        /// <summary>
        /// Upload file
        /// </summary>
        /// <response code="200">On successful file upload</response>
        /// <response code="400">Bad request</response>
        /// <param name="requestData">
        /// File upload parameters
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("UploadFile")]
        public async Task<IActionResult> UploadFile(UploadFileParams requestData)
        {
            var actionResult = await this.storageService.UploadFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Update file
        /// </summary>
        /// <response code="200">On successful file update</response>
        /// <response code="400">Bad request</response>
        /// <param name="requestData">
        /// File update parameters
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("UpdateFile")]
        public async Task<IActionResult> UpdateFile(UpdateFileParams requestData)
        {
            var actionResult = await this.storageService.UpdateFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Delete file
        /// </summary>
        /// <response code="200">On successful file deletion</response>
        /// <response code="400">Bad request</response>
        /// <param name="requestData">
        /// File delete parameters
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("DeleteFile")]
        public async Task<IActionResult> DeleteFile(DeleteFileParams requestData)
        {
            var actionResult = await this.storageService.DeleteFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Returns file 
        /// </summary>
        /// <param name="token">token</param>
        /// <param name="featureType"></param>
        /// <param name="fileName">fileName</param>
        /// <param name="campusId">campusId</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetFile")]
        public async Task<IActionResult> GetFile(string token, FileConfigurationFeature featureType, string fileName, Guid campusId)
        {
            var actionResult = await this.storageService.GetFile(token, featureType, fileName, campusId);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            MemoryStream stream = new MemoryStream(actionResult.Result.FileContents);
            stream.Flush();
            stream.Position = 0;
            var response = this.File(stream, "application/pdf", "Report.pdf");
            return response;
        }

        /// <summary>
        /// Returns file 
        /// </summary>
        /// <response code="200">Returns file as byte array</response>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("GetFile")]
        public async Task<IActionResult> GetFile(GetFileParams requestData)
        {
            var actionResult = await this.storageService.GetFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            if (actionResult.ResultStatus == ResultStatus.NotFound)
            {
                return this.NotFound(actionResult);
            }
            MemoryStream stream = new MemoryStream(actionResult.Result.FileContents);
            stream.Flush();
            stream.Position = 0;
            var response = this.File(stream, actionResult.Result.FileType, actionResult.Result.Extension);
            return response;
        }

        /// <summary>
        /// Rename file 
        /// </summary>
        /// <response code="200">Successfully renamed file</response>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("RenameFile")]
        public async Task<IActionResult> RenameFile(RenameFileParams requestData)
        {
            var actionResult = await this.storageService.RenameFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Move file 
        /// </summary>
        /// <response code="200">Successfully moved file</response>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("MoveFile")]
        public async Task<IActionResult> MoveFile(MoveFileParams requestData)
        {
            var actionResult = await this.storageService.MoveFile(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }



        /// <summary>
        /// Explore directory 
        /// </summary>
        /// <response code="200">Returns directory contents</response>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Modify")]
        [Route("ExploreDirectory")]
        public async Task<IActionResult> ExploreDirectory(ExploreDirectoryParams requestData)
        {
            var actionResult = await this.storageService.ExploreDirectory(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Get file path given campus and feature
        /// </summary>
        /// <response code="200">Returns directory contents</response>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("BuildStoragePath")]
        public async Task<IActionResult> BuildStoragePath(BuildPathParams requestData)
        {
            var actionResult = await this.storageService.BuildStoragePath(requestData);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }


        /// <summary>
        /// Get file path given campus and feature
        /// </summary>
        /// <response code="200">Returns directory contents</response>
        /// <param name="storageFeatureType"></param>
        /// <param name="campusId"></param>
        /// <param name="requestData">requestData</param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetStorageSettingByCampus")]
        public async Task<IActionResult> GetStorageSettingByCampus(FileConfigurationFeature storageFeatureType, Guid campusId)
        {
            var actionResult = await this.storageService.GetStorageSettingByCampus(storageFeatureType, campusId);
            return Ok(actionResult);
        }

        /// <summary>
        /// Returns automated time clock locations for a given tenant. Tenant extracted from authentication token.
        /// </summary>
        /// <response code="200">Returns automated time clock locations per campus for a given tenant</response>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "SY-Read")]
        [Route("GetAutomatedTimeClockLocations")]
        public async Task<IActionResult> GetAutomatedTimeClockLocations()
        {
            var actionResult = await this.storageService.GetAutomatedTimeClockLocations();

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }

        /// <summary>
        /// Returns contents of a text file given parameters
        /// </summary>
        /// <response code="200">Returns text file contents</response>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "SY-Read")]
        [Route("ReadFileContents")]
        public async Task<IActionResult> ReadFileContents([FromBody] ReadFileContentsParams readFileParams)
        {
            var actionResult = await this.storageService.ReadFileContents(readFileParams);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == ResultStatus.Error)
                return this.BadRequest(actionResult);

            return Ok(actionResult);
        }
    }
}