﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The report controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Net.Http.Headers;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// The report controller.
    /// </summary>
    [Route("api/v1/[controller]")]
    public class ReportController : BaseController
    {
        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportController"/> class.
        /// </summary>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        public ReportController(IReportService reportService)
        {
            this.reportService = reportService;
        }

        /// <summary>
        /// The generate report.
        /// </summary>
        /// <param name="reportRequest">
        /// The report request.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Route("GenerateReport")]
        [Authorize(Roles = "AR-Read")]
        public async Task<IActionResult> GenerateReport([Required][FromBody]ReportRequest reportRequest)
        {
            var actionResult = await this.reportService.GenerateReport(reportRequest);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);

                    if (reportRequest.ReportOutput == ReportOutput.Pdf)
                    {
                        var response = this.File(stream, "application/pdf", "Report.pdf");

                        return response;
                    }

                    return this.BadRequest(reportRequest);
                }
            }
            else
            {
                return this.NotFound(reportRequest);
            }
        }
    }
}
