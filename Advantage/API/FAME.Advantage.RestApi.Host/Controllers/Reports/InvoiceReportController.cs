﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVNoticeReportController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Title IV Notice Report controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Invoice;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/v1/Reports/[controller]")]
    public class InvoiceReportController : Controller
    {
        private readonly IInvoiceReportService invoiceReportService;

        public InvoiceReportController(IInvoiceReportService invoiceReportService)
        {
            this.invoiceReportService = invoiceReportService;
        }

        [HttpPost]
        [Authorize(Roles = "SA-Read,SY-Read")]
        [Route("GetInvoiceReport")]
        public async Task<IActionResult> GetInvoiceReportForStudentEnrollments([FromBody]InvoiceInputParam model)
        {
            var actionResult = await this.invoiceReportService.GetInvoiceReport(model.StuEnrollId, model.CampusId, model.RefDate);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);
                    stream.Flush();
                    stream.Position = 0;
                    var response = this.File(stream, "application/pdf", "Report.pdf");

                    return response;
                }
            }
            else
            {
                return this.NotFound(model);
            }
        }
    }
}
