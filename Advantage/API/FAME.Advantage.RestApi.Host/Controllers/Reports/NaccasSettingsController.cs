﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NaccasSettingsController.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   The naccas settings controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Naccas;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Net.Http.Headers;

    /// <summary>
    /// The naccas settings controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class NaccasSettingsController : BaseController
    {
        /// <summary>
        /// The state board settings service.
        /// </summary>
        private readonly INaccasSettingService naccasSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NaccasSettingsController"/> class. 
        /// </summary>
        /// <param name="naccasSettingsService">
        /// The naccas Settings Service.
        /// </param>
        public NaccasSettingsController(INaccasSettingService naccasSettingsService)
        {
            this.naccasSettingsService = naccasSettingsService;
        }

        

        /// <summary>
        /// The save or update naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Route("SaveOrUpdateNaccasReportSetting")]
        [Authorize(Roles = "SY-Modify")]
        public async Task<IActionResult> SaveOrUpdateNaccasReportSetting([FromBody]NaccasSettings model)
        {
            return this.Ok(await this.naccasSettingsService.SaveOrUpdateNaccasReportSetting(model));
        }

        /// <summary>
        /// The get naccas report setting.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetNaccasReportSetting")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetNaccasReportSetting(Guid campusId)
        {
            if (campusId.IsEmpty())
            {
                return this.Ok(this.MissingRequiredParamResult("CampusId"));
            }
            return this.Ok(await this.naccasSettingsService.GetNaccasReportSetting(campusId));
        }

        /// <summary>
        /// The delete naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpDelete]
        [Route("DeleteNaccasReportSetting")]
        [Authorize(Roles = "SY-Delete")]
        public async Task<IActionResult> DeleteNaccasReportSetting([FromBody]NaccasSettings model)
        {
            return this.Ok(await this.naccasSettingsService.DeleteNaccasReportSetting(model));
        }

    }
}