﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IndianaStateBoardController.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The Indiana State Board controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;
using AutoMapper;
using FAME.Advantage.RestApi.Host.Services.FinacialAid;
using FAME.Advantage.RestApi.Host.Services.Interfaces;
using FAME.Advantage.RestApi.Host.Services.Interfaces.FinancialAid;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

namespace FAME.Advantage.RestApi.Host.Controllers.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports.AcademicRecords;
    using FAME.Extensions;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The  Indiana State Board controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Reports/AcademicRecords/[controller]")]
    public class IndianaStateBoardController : BaseController
    {
        /// <summary>
        /// The IndianaStateBoard service.
        /// </summary>
        private readonly IIndianaStateBoardService iiIndianaStateBoardService;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndianaStateBoardController"/> class.
        /// </summary>
        /// <param name="iiIndianaStateBoardService">
        /// The  Indiana State Board service.
        /// </param>
        public IndianaStateBoardController(IIndianaStateBoardService iiIndianaStateBoardService)
        {
            this.iiIndianaStateBoardService = iiIndianaStateBoardService; 
        }

        /// <summary>
        /// The get indiana state board report data.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="month">
        /// The month.
        /// </param>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <param name="studentGroup">
        /// The student group.
        /// </param>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [AuthorizeReport("AR-Read")]
        [Route("GetIndianaStateBoardReportData")]
        [Produces("application/xml")]
        public async Task<IActionResult> GetIndianaStateBoardReportData(string token, Guid campusId, int month, int year, Guid? studentGroup, Guid? programVersion)
        {
            return this.Ok(await this.iiIndianaStateBoardService.GetIndianaStateBoardReportData(campusId, month, year, studentGroup, programVersion));
        }


    }
}