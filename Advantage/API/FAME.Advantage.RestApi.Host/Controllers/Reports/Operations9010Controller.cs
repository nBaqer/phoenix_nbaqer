﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Operations9010Controller.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The 9010 Report/Data controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Report9010;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Extensions;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// The Operations9010Controller controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Reports/[controller]")]
    public class Operations9010Controller : Controller
    {
        /// <summary>
        /// The progress report service.
        /// </summary>
        private readonly IDataExport9010Service DataExport9010Service;

        /// <summary>
        /// Initializes a new instance of the <see cref="Operations9010Controller"/> class.
        /// </summary>
        /// <param name="DataExport9010Service">
        /// The progress report service.
        /// </param>
        public Operations9010Controller(IDataExport9010Service DataExport9010Service)
        {
            this.DataExport9010Service = DataExport9010Service;
        }

        /// <summary>
        /// Returns csv file containing the data export for 9010 given parameters.
        /// </summary>
        /// <param name="parameters">
        /// <response code="200">Returns csv file containing the data export for 9010 given parameters.</response>
        /// The data export parameters.
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "FA-Read, SY-Read")]
        [Route("GetDataExport9010")]
        public async Task<IActionResult> GetDataExport9010([FromBody]DataExport9010Param parameters)
        {
            var actionResult = await this.DataExport9010Service.Get9010SpreadsheetData(parameters);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                return this.BadRequest(actionResult.ResultStatus);

            MemoryStream stream = new MemoryStream(actionResult.Data);
            stream.Flush();
            stream.Position = 0;
            var response = this.File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report.xlsx");

            return response;
        }

        /// <summary>
        /// Returns all active fund sources along with mappings, if any, by campus id.
        /// </summary>
        /// <response code="200">Returns all active fund sources along with mappings, if any, by campus id.</response>
        /// <returns>
        /// <see cref="IActionResult"/>  
        /// </returns>
        [HttpGet]
        [Route("Get9010MappingsByCampusId")]
        [Authorize(Roles = "FA-Read, SY-Read")]
        public async Task<IActionResult> Get9010MappingsByCampusId(Guid? campusId)
        {
            if (campusId == null || campusId == Guid.Empty)
                return Ok(new List<string>());

            var actionResult = await this.DataExport9010Service.Get9010MappingsByCampusId(campusId.Value);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult.Result);
        }

        /// <summary>
        /// The Get9010AwardTypes return all active 9010 award types
        /// </summary>
        /// <response code="200">Returns all active 9010 award types.</response>
        /// <returns>
        /// <see cref="IActionResult"/>  
        /// </returns>
        [HttpGet]
        [Route("Get9010AwardTypes")]
        [Authorize(Roles = "FA-Read, SY-Read")]
        public async Task<IActionResult> Get9010AwardTypes()
        {
            var actionResult = await this.DataExport9010Service.Get9010AwardTypes();

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult.Result);
        }

        /// <summary>
        /// Given 9010 mappings and campusId, updates records.
        /// </summary>
        /// <response code="200">Given 9010 mappings and campusId, updates records.</response>
        /// <param name="mappings">
        /// Fund source to 9010 mappings array.
        /// </param>
        /// <param name="campusId">
        /// Campus Id
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "FA-Modify, SY-Modify")]
        [Route("UpdateCampusAwardMappings9010")]
        public async Task<IActionResult> UpdateCampusAwardMappings9010([FromBody]List<Mapping9010> mappings, Guid campusId)
        {
            if (mappings == null || campusId == Guid.Empty)
                return BadRequest(ApiMsgs.IncorrectParametersPassed);

            var actionResult = await this.DataExport9010Service.UpdateCampusAwardMappings9010(mappings, campusId);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (actionResult.ResultStatus == DataTransferObjects.Common.Enums.ResultStatus.Error)
                return this.BadRequest(actionResult.ResultStatusMessage);

            return Ok(actionResult);
        }


        /// <summary>
        /// Given 9010 mappings and campusId, updates records.
        /// </summary>
        /// <response code="200">Given 9010 mappings and campusId, updates records.</response>
        /// <param name="mappings">
        /// Fund source to 9010 mappings array.
        /// </param>
        /// <param name="campusId">
        /// Campus Id
        /// </param>
        /// <returns>
        /// The <see cref="IActionResult"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "FA-Modify, SY-Modify")]
        [Route("PrintCampus9010Mappings")]
        public async Task<IActionResult> PrintCampus9010Mappings(Guid campusId)
        {
            var actionResult = await this.DataExport9010Service.Get9010MappingsConfigurationByCampus(campusId);

            if (actionResult == null)
                return this.BadRequest("Error occured");

            if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                return this.BadRequest(actionResult.ResultStatus);

            MemoryStream stream = new MemoryStream(actionResult.Data);
            stream.Flush();
            stream.Position = 0;
            var response = this.File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Report.xlsx");

            return response;
        }
    }
}