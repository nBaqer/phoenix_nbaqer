﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressReportController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The progress report controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The progress report controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/Reports/[controller]")]
    public class ProgressReportController : Controller
    {
        /// <summary>
        /// The progress report service.
        /// </summary>
        private readonly IProgressReportService progressReportService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressReportController"/> class.
        /// </summary>
        /// <param name="progressReportService">
        /// The progress report service.
        /// </param>
        public ProgressReportController(IProgressReportService progressReportService)
        {
            this.progressReportService = progressReportService;
        }

        /// <summary>
        /// The get progress report for klass app.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The klass app student id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read, REPORTSAPI-Read")]
        [Route("GetProgressReport")]
        public async Task<IActionResult> GetProgressReport([FromBody]Guid studentEnrollmentId)
        {
            var actionResult = await this.progressReportService.GetProgressReport(studentEnrollmentId);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);
                    stream.Flush();
                    stream.Position = 0;
                    var response = this.File(stream, "application/pdf", "Report.pdf");
                    return response;
                }
            }
            else
            {
                return this.NotFound(studentEnrollmentId);
            }
        }


        /// <summary>
        /// The get progress report for klass app.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The klass app student id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read, REPORTSAPI-Read")]
        [Route("GetProgressReportNoGrouping")]
        public async Task<IActionResult> GetProgressReportNoGrouping([FromBody]Guid studentEnrollmentId)
        {
            var actionResult = await this.progressReportService.GetProgressReport(studentEnrollmentId, false, false, false, true);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);
                    stream.Flush();
                    stream.Position = 0;
                    var response = this.File(stream, "application/pdf", "Report.pdf");
                    return response;
                }
            }
            else
            {
                return this.NotFound(studentEnrollmentId);
            }
        }
    }
}