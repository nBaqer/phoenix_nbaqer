﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVNoticeReportController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Title IV Notice Report controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;

    [Produces("application/json")]
    [Route("api/v1/Reports/[controller]")]
    public class TitleIVNoticeReportController : Controller
    {
        /// <summary>
        /// The title iv notice report service.
        /// </summary>
        private readonly ITitleIvNoticeReportService titleIvNoticeReportService;

       

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIVNoticeReportController"/> class.
        /// </summary>
        /// <param name="titleIvNoticeReportService">
        /// The title iv notice report service.
        /// </param>
        public TitleIVNoticeReportController(ITitleIvNoticeReportService titleIvNoticeReportService)
        {
            this.titleIvNoticeReportService = titleIvNoticeReportService;
        }

        /// <summary>
        /// The get title iv notice report for student enrollments.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "AR-Read,SY-Read")]
        [Route("GetTitleIVNoticeReport")]
        public async Task<IActionResult> GetTitleIVNoticeReportForStudentEnrollments([FromBody]List<Guid> model)
        {
            var actionResult = await this.titleIvNoticeReportService.GetTitleIvNoticeReport(model);
            if (actionResult != null)
            {
                if (!string.IsNullOrWhiteSpace(actionResult.ResultStatus))
                {
                    return this.BadRequest(actionResult.ResultStatus);
                }
                else
                {
                    MemoryStream stream = new MemoryStream(actionResult.Data);
                    stream.Flush();
                    stream.Position = 0;
                    var response = this.File(stream, "application/pdf", "Report.pdf");
                    
                    return response;
                }
            }
            else
            {
                return this.NotFound(model);
            }
        }
    }
}