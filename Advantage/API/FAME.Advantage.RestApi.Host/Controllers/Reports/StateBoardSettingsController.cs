﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardSettingsController.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The state board settings controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Controllers.Reports
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Net.Http.Headers;

    /// <summary>
    /// The state board settings controller.
    /// </summary>
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    public class StateBoardSettingsController : BaseController
    {
        /// <summary>
        /// The state board settings service.
        /// </summary>
        private readonly IStateBoardSettingsService stateBoardSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardSettingsController"/> class.
        /// </summary>
        /// <param name="stateBoardSettingsService">
        /// The State Board Settings service.
        /// </param>
        public StateBoardSettingsController(IStateBoardSettingsService stateBoardSettingsService)
        {
            this.stateBoardSettingsService = stateBoardSettingsService;
        }

        /// <summary>
        /// The Get return all state board reports in the database
        /// </summary>
        /// <param name="stateId">
        /// The state Id.
        /// </param>
        /// <remarks>
        /// The Get return all state board reports found in the database
        /// </remarks>
        /// <response code="200">Returns the list of all state board reports in the database.</response>
        /// <response code="400">If the list of state board reports is null</response>  
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the state board reports name and TValue is of type Guid and will have the state board reports id
        /// </returns>
        [HttpGet]
        [Route("GetStateBoardReports")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> Get(Guid? stateId)
        {
            return this.Ok(await this.stateBoardSettingsService.GetStateBoardReports(stateId));
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetIndianaStateBoardReportSettingsForCampus")]
        [AuthorizeReport("SY-Read")]
        [Produces("application/xml")]
        public async Task<IActionResult> Get(string token, Guid campusId)
        {
            return this.Ok(await this.stateBoardSettingsService.GetIndianaStateBoardReportSettings(campusId));
        }

        /// <summary>
        /// The delete state board report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpDelete]
        [Route("DeleteStateBoardReportSetting")]
        [Authorize(Roles = "SY-Delete")]
        public async Task<IActionResult> DeleteStateBoardReportSetting([FromBody]StateBoardReportSetting model)
        {
            return this.Ok(await this.stateBoardSettingsService.DeleteStateBoardSetting(model));
        }

        /// <summary>
        /// The save or update state board report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpPost]
        [Route("SaveOrUpdateStateBoardReportSetting")]
        [Authorize(Roles = "SY-Modify")]
        public async Task<IActionResult> SaveOrUpdateStateBoardReportSetting([FromBody]StateBoardReportSetting model)
        {
            if (!(model.ProgramStateBoardCourseMappings.Count > 0))
            {
                return this.Ok(this.MissingRequiredParamResult("ProgramStateBoardCourseMappings"));
            }

            return this.Ok(await this.stateBoardSettingsService.SaveOrUpdateStateBoardReportSetting(model));
        }

        /// <summary>
        /// The get state board report setting.
        /// </summary>
        /// <param name="stateId">
        /// The state id.
        /// </param>
        /// <param name="reportId">
        /// The report id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [HttpGet]
        [Route("GetStateBoardReportSetting")]
        [Authorize(Roles = "SY-Read")]
        public async Task<IActionResult> GetStateBoardReportSetting(Guid stateId, Guid reportId, Guid campusId)
        {
            return this.Ok(await this.stateBoardSettingsService.GetStateBoardReportSetting(stateId, reportId, campusId));
        }
    }
}