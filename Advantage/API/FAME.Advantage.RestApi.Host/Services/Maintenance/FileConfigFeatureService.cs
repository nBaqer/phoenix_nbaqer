﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigFeatureService.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the FileConfigFeatureService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Maintenance
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The genders service.
    /// </summary>
    public class FileConfigFeatureService : IFileConfigFeatureService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileConfigFeatureService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">The status service.</param>
        public FileConfigFeatureService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }

        /// <summary>
        /// The syFileConfigurationFeatures repository.
        /// </summary>
        private IRepository<SyFileConfigurationFeatures> FileConfigurationFeatureRepository => this.advantageContext.GetRepositoryForEntity<SyFileConfigurationFeatures>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get all active file feature configurations.
        /// </summary>
        /// <returns>
        /// The <see>
        ///  <cref>IList</cref>
        /// </see>
        /// </returns>
        public async Task<IList<IListItem<string,string>>> Get()
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:FileConfigFeatures",
                       () => this.mapper.Map< IList<IListItem<string,string>>>(this.FileConfigurationFeatureRepository.Get(fcf => fcf.StatusId == this.ActiveId)).ToList(),
                       CacheExpiration.SHORT));
        }


    }
}
