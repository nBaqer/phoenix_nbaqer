﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigCampusService.cs" company="Fame Inc">
//   2019
// </copyright>
// <summary>
//   Defines the FileConfigCampusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Maintenance
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Utilities;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The file configuration campus service.
    /// </summary>
    public class FileConfigCampusService : IFileConfigCampusService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The encryption service.
        /// </summary>
        private readonly IEncryptionService encryptionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileConfigCampusService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">The status service.</param>
        public FileConfigCampusService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService, IEncryptionService encryptionService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
            this.encryptionService = encryptionService;
        }

        /// <summary>
        /// The syFileConfigurationFeatures repository.
        /// </summary>
        private IRepository<SyCampusFileConfiguration> CampusFileConfigurationRepository => this.advantageContext.GetRepositoryForEntity<SyCampusFileConfiguration>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// Method for retrieving a campus configuration for file storage
        /// </summary>
        /// <param name="CampusGroupId"></param>
        /// <returns></returns>
        public async Task<CampusFileConfig> Get(Guid CampusGroupId)
        {

            var configToRetrieve = CampusFileConfigurationRepository.Get(cfcr => cfcr.CampusGroupId == CampusGroupId).Include(cfcr => cfcr.SyCustomFeatureFileConfiguration).FirstOrDefault();

            if (configToRetrieve != null)
            {
                var campusConfig = this.mapper.Map<CampusFileConfig>(configToRetrieve);
                campusConfig.Password = this.encryptionService.Decrypt(campusConfig.Password);
                foreach(var customConfig in campusConfig.CustomFeatureConfigurations)
                {
                    if(customConfig.FileStorageType == Enums.FileStorageType.Network && !string.IsNullOrEmpty(customConfig.Password))
                    {
                        customConfig.Password = this.encryptionService.Decrypt(customConfig.Password);
                    }
                }
                return await Task.Run(() => campusConfig);

            }
            return null;
        }


        /// <summary>
        /// Method for deleting a campus configuration for file storage
        /// </summary>
        /// <param name="CampusConfigurationId"></param>
        /// <returns></returns>
        public async Task<bool> Delete(Guid CampusConfigurationId)
        {

            var configToDelete = CampusFileConfigurationRepository.Get(cfcr => cfcr.CampusFileConfigurationId == CampusConfigurationId).Include(cfcr => cfcr.SyCustomFeatureFileConfiguration).FirstOrDefault();

            if (configToDelete != null)
            {
                if (configToDelete.SyCustomFeatureFileConfiguration.Count > 0)
                {
                    configToDelete.SyCustomFeatureFileConfiguration.ToList().ForEach(p => configToDelete.SyCustomFeatureFileConfiguration.Remove(p));
                }


                CampusFileConfigurationRepository.Delete(configToDelete);

                await CampusFileConfigurationRepository.SaveAsync();

                return true;

            }
            return false;
        }

        /// <summary>
        /// Method for saving a campus configuration for file storage
        /// </summary>
        /// <param name="campusFileConfig"></param>
        /// <returns></returns>
        public async Task<ActionResult<CampusFileConfig>> Save(CampusFileConfig campusFileConfig)
        {
            return await System.Threading.Tasks.Task.Run(
                      () =>
                      {
                          ActionResult<CampusFileConfig> actionResult = new ActionResult<CampusFileConfig>();
                          actionResult.ResultStatus = Enums.ResultStatus.Success;
                          actionResult.Result = campusFileConfig;

                          var modDate = DateTime.Now;

                          try
                          {
                              // If state board report id is guid.empty then this is a new save
                              if (campusFileConfig.Id == Guid.Empty)
                              {

                                  var configToSave = new SyCampusFileConfiguration()
                                  {

                                      CampusGroupId = campusFileConfig.CampusGroupId,
                                      AppliesToAllFeatures = campusFileConfig.AppliesToAllFeatures,
                                      FileStorageType = (int)campusFileConfig.FileStorageType,
                                      CloudKey = campusFileConfig.CloudKey,
                                      UserName = campusFileConfig.UserName,
                                      Password = this.encryptionService.Encrypt(campusFileConfig.Password),
                                      Path = campusFileConfig.Path,
                                      ModDate = modDate,
                                      ModUser = campusFileConfig.ModUser,
                                  };

                                  if (!configToSave.AppliesToAllFeatures)
                                  {
                                      foreach (var customConfig in campusFileConfig.CustomFeatureConfigurations)
                                      {
                                          configToSave.SyCustomFeatureFileConfiguration.Add(new SyCustomFeatureFileConfiguration()
                                          {
                                              FileStorageType = (int)customConfig.FileStorageType,
                                              CloudKey = customConfig.CloudKey,
                                              Path = customConfig.Path,
                                              FeatureId = customConfig.FeatureId,
                                              UserName = customConfig.UserName,
                                              Password = this.encryptionService.Encrypt(customConfig.Password),
                                              ModDate = modDate,
                                              ModUser = customConfig.ModUser


                                          });
                                      }
                                  }

                                  CampusFileConfigurationRepository.Create(configToSave);
                                  CampusFileConfigurationRepository.Save();
                                  campusFileConfig.Id = configToSave.CampusFileConfigurationId;
                              }

                              // we are updating
                              else
                              {
                                  var settingToUpdate = this.CampusFileConfigurationRepository.Get(
                                          s => s.CampusGroupId == campusFileConfig.CampusGroupId)
                                      .Include(sett => sett.SyCustomFeatureFileConfiguration).FirstOrDefault();

                                  if (settingToUpdate != null)
                                  {
                                      settingToUpdate.CampusGroupId = campusFileConfig.CampusGroupId;
                                      settingToUpdate.AppliesToAllFeatures = campusFileConfig.AppliesToAllFeatures;
                                      settingToUpdate.FileStorageType = (int)campusFileConfig.FileStorageType;
                                      settingToUpdate.CloudKey = campusFileConfig.CloudKey;
                                      settingToUpdate.UserName = campusFileConfig.UserName;
                                      settingToUpdate.Password = this.encryptionService.Encrypt(campusFileConfig.Password);
                                      settingToUpdate.Path = campusFileConfig.Path;
                                      settingToUpdate.ModDate = modDate;
                                      settingToUpdate.ModUser = campusFileConfig.ModUser;

                                      var customConfigs = campusFileConfig.CustomFeatureConfigurations.Select(x => new SyCustomFeatureFileConfiguration()
                                      {
                                          CustomFeatureConfigurationId = x.Id,
                                          CampusConfigurationId = x.CampusFileConfigurationId,
                                          CloudKey = x.CloudKey,
                                          Path = x.Path,
                                          UserName = x.UserName,
                                          Password = this.encryptionService.Encrypt(x.Password),
                                          FeatureId = x.FeatureId,
                                          FileStorageType = (int)x.FileStorageType,
                                          ModDate = modDate,
                                          ModUser = x.ModUser


                                      });



                                      var dupComparer = new InlineComparer<SyCustomFeatureFileConfiguration>(
                                                                                (i1, i2) => i1.CampusConfigurationId == i2.CampusConfigurationId &&
                                                                                            i1.FeatureId == i2.FeatureId,
                                                                                i => i.CampusConfigurationId.GetHashCode() + i.FeatureId.GetHashCode());


                                      settingToUpdate.SyCustomFeatureFileConfiguration = settingToUpdate.SyCustomFeatureFileConfiguration
                                          .Union(customConfigs, dupComparer).ToList();
                                      var deletedMappings =
                                          settingToUpdate.SyCustomFeatureFileConfiguration.Except(customConfigs,
                                              dupComparer);
                                      var addedMappings = customConfigs.Except(
                                          settingToUpdate.SyCustomFeatureFileConfiguration, dupComparer);

                                      foreach (var mapping in deletedMappings)
                                      {
                                          settingToUpdate.SyCustomFeatureFileConfiguration.Remove(mapping);
                                      }

                                      foreach (var mapping in addedMappings)
                                      {
                                          settingToUpdate.SyCustomFeatureFileConfiguration.Add(mapping);

                                      }

                                      this.CampusFileConfigurationRepository.Update(settingToUpdate);
                                      this.CampusFileConfigurationRepository.Save();

                                  }

                              }


                              return actionResult;
                          }
                          catch (Exception e)
                          {
                              e.TrackException();
                              actionResult.ResultStatus = Enums.ResultStatus.Error;
                              actionResult.ResultStatusMessage =
                                  "An error occured when trying to save the file storage settings.";
                              return actionResult;
                          }

                      });
        }
    }
}
