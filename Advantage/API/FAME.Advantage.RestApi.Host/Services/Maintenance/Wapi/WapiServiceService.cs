﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiServiceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the WapiServiceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Maintenance.Wapi
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Maintenance.Wapi;
    using FAME.Extensions.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceProcess;
    using System.Threading.Tasks;

    /// <summary>
    /// The wapi settings service.
    /// </summary>
    public class WapiServiceService : IWapiServiceService
    {
        /// <summary>
        /// The mapper is used to map the Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiServiceService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage Context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public WapiServiceService(IAdvantageDbContext advantageContext, IMapper mapper)
        {
            this.advantageContext = advantageContext;
            this.mapper = mapper;
        }

        /// <summary>
        /// The wapi settings repository.
        /// </summary>
        private IRepository<SyWapiSettings> WapiSettingsRepository =>
            this.advantageContext.GetRepositoryForEntity<SyWapiSettings>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get wapi settings.
        /// </summary>
        /// <param name="codeOperation">
        /// The code operation.
        /// </param>
        /// <returns>
        /// The <see cref="WapiSettings"/>.
        /// </returns>
        public WapiSettings GetWapiSettings(string codeOperation)
        {
            var temp = this.WapiSettingsRepository
                .Get(x => x.CodeOperation == codeOperation).FirstOrDefault();
            var result = this.mapper.Map<WapiSettings>(temp);
            if (result != null)
            {
                result.ResultStatus = Enums.ResultStatus.Success;
            }
            else
            {
                result = new WapiSettings();
                result.ResultStatus = Enums.ResultStatus.NotFound;
            }

            return result;
        }

        /// <summary>
        /// Returns status of Wapi service.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        public ServiceControllerStatus GetWapiServiceStatus()
        {
            try
            {
                using (ServiceController sc = new ServiceController(WapiServiceOperations.WapiServiceName))
                {
                    return sc.Status;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                throw e;
            }
        }

        /// <summary>
        /// Attempts to turn serice on/off.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        public bool TurnWapiServiceOff()
        {
            try
            {
                using (ServiceController sc = new ServiceController(WapiServiceOperations.WapiServiceName))
                {
                    if (sc.Status == ServiceControllerStatus.Running && sc.CanStop)
                        sc.Stop();

                    return true;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                throw e;
            }
        }

        /// <summary>
        /// Attempts to turn serice on.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        public bool TurnWapiServiceOn()
        {
            try
            {
                using (ServiceController sc = new ServiceController(WapiServiceOperations.WapiServiceName))
                {
                    if (sc.Status != ServiceControllerStatus.Running)
                        sc.Start();

                    return true;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                throw e;
            }
        }
    }
}
