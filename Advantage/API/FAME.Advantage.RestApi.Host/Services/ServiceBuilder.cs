﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceBuilder.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ServiceBuilder type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services
{
    using FAME.Advantage.RestApi.Host.Services.Interfaces;

    using StructureMap;

    /// <summary>
    /// The service builder.
    /// </summary>
    public class ServiceBuilder : IServiceBuilder
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBuilder"/> class.
        /// </summary>
        /// <param name="context">
        /// The structure map context.
        /// </param>
        public ServiceBuilder(IContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// The get service instance.
        /// Retrieve a service instance dependency. Use in cases where Circular dependency exist.
        /// Example: The Lead Service needs a dependency to the phone service and vice versa.
        /// </summary>
        /// <typeparam name="TServiceInstance">
        /// The type of service you want to get the instance off.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TServiceInstance"/>.
        /// </returns>
        public TServiceInstance GetServiceInstance<TServiceInstance>()
        {
            return this.context.GetInstance<TServiceInstance>();
        }
    }
}
