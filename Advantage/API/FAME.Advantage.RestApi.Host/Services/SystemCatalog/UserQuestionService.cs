﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserQuestionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the User Question service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The user question service .
    /// </summary>
    public class UserQuestionService : IUserQuestionService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The tenant db context.
        /// </summary>
        private readonly ITenantDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserQuestionService"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public UserQuestionService(ITenantDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The user questions repository.
        /// </summary>
        private IRepository<TenantQuestions> UserQuestionsRepository => this.context.GetRepositoryForEntity<TenantQuestions>();

        /// <summary>
        /// The get user questions for system.
        /// </summary>
        /// <remarks>
        /// The get user questions action returns all system user questions .
        /// </remarks>
        /// <response code="200">Returns the list of user questions found</response>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetUserQuestions()
        {
            return await Task.Run(() =>
            {
                try
                {
                    return
                         this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                             this.UserQuestionsRepository
                                 .Get().ToList());
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<IListItem<string, int>>();
                }

            });
        }
    }
}
