﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemStatusService.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the SysStatusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The sys status service.
    /// </summary>
    public class SystemStatusService : ISystemStatusService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemStatusService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        public SystemStatusService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The system status repository that will give if the status is Active/Inactive.
        /// </summary>
        private IRepository<SySysStatus> SysStatusRepository => this.advantageContext.GetRepositoryForEntity<SySysStatus>();

        /// <summary>
        /// The get system status code id by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<int> GetSystemStatusCodeIdByName(string name, SystemStatusLevel level)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SysStatusCode:{name}",
                 () =>
                     {
                         var levelId = (int)level;
                         var singleOrDefault = this.SysStatusRepository
                             .Get(_ => _.SysStatusDescrip.ToLower() == name.ToLower() && _.StatusId == activeStatusId && _.StatusLevelId.HasValue && _.StatusLevelId.Value == levelId)
                             .FirstOrDefault();
                         if (singleOrDefault != null)
                         {
                             return singleOrDefault.SysStatusId;
                         }
                         else
                         {
                             return 0;
                         }
                     },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get in school system statuses.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Constants.SystemStatus>> GetInSchoolSystemStatuses()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SysStatus:InSchool",
                () =>
                {
                    return this.SysStatusRepository
                        .Get(_ => _.InSchool == 1 && _.StatusId == activeStatusId && _.StatusLevelId.HasValue && _.StatusLevelId.Value == (int)SystemStatusLevel.StudentEnrollments).Select(ss => (Constants.SystemStatus)ss.SysStatusId)
                        .ToList();
                },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get active system statuses.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Constants.SystemStatus>> GetActiveSystemStatuses()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SysStatus:Active",
                () =>
                {
                    return this.SysStatusRepository
                        .Get(_ => _.PostAcademics && _.StatusId == activeStatusId && _.StatusLevelId.HasValue && _.StatusLevelId.Value == (int)SystemStatusLevel.StudentEnrollments).Select(ss => (Constants.SystemStatus)ss.SysStatusId)
                        .ToList();
                },
                CacheExpiration.VERY_LONG);
        }
    }
}
