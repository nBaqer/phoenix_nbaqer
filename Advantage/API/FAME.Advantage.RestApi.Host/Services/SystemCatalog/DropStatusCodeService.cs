﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatusCodeService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DropStatusService type. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;
    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Common;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    ///  The DropStatusService interface
    /// </summary>
    public class DropStatusCodeService : IDropStatusCodeService
    {
        /// <summary>
        /// The _unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropStatusCodeService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unitOfWork.
        /// </param>
        ///  /// <param name="mapper">
        /// The mapper.
        /// </param>
        public DropStatusCodeService(IAdvantageDbContext unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The status repository.
        /// </summary>
        private IRepository<SyStatusCodes> StatusRepository => this.unitOfWork.GetRepositoryForEntity<SyStatusCodes>();

        /// <summary>
        /// The campus group campus repository repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CampusGroupCampusRepositoryRepository => this.unitOfWork.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The Search action methods returns the list of system status for the provided campus.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the status for a given campus of type GUID 
        /// </remarks>
        /// <param name="dropStatus">
        /// The status search filter object includes the campusId .
        /// </param>
        /// <returns>
        /// Returns an IEnumerable which will show you the status name of the student and the StatusId.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetDropStatusByCampusId(DropStatus dropStatus)
        {
            return await Task.Run(() =>
                {
                    try
                    {
                        var statusId = (int)Constants.SystemStatus.Dropped;

                        var getStatusListQuery = this.CampusGroupCampusRepositoryRepository
                            .Get(e => e.CampusId == dropStatus.CampusId).SelectMany(_ => _.CampGrp.SyStatusCodes)
                            .Include(e => e.Status).Where(e => e.SysStatusId == statusId && e.Status.StatusCode == "A");

                        var result = SimpleCache.GetOrAddExisting(
                            $"{this.CacheKeyFirstSegment}:studentStatus" + dropStatus.CampusId.ToString(),
                            () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                getStatusListQuery.ToList().OrderBy(status => status.StatusCodeDescrip)),
                            CacheExpiration.LONG);

                        return result;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return new List<IListItem<string, Guid>>();
                    }
                    
                });
        }

        /// <summary>
        /// The drop Status Search validations.
        /// </summary>
        /// <returns>
        /// The <see cref="DynamicValidationRuleSet"/>.
        /// </returns>
        public DynamicValidationRuleSet GetDropStatusValidations()
        {
            var set = new DynamicValidationRuleSet("DropStatusSearch");
            set.Rules.Add("CampusId", new DynamicValidationRule("CampusId", true, true));
            return set;
        }
    }

}
