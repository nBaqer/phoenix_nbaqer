﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstructorsService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Instructors Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The user service.
    /// </summary>
    public class InstructorsService : IInstructorsService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="userService">
        /// The user service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="mapper">
        /// The mapper class.
        /// </param>
        /// <param name="appSettingsService">
        /// The app Settings Service.
        /// </param>
        public InstructorsService(IAdvantageDbContext advantageContext, IUserService userService, IStatusesService statusService, IMapper mapper, ISystemConfigurationAppSettingService appSettingsService)
        {
            this.advantageContext = advantageContext;
            this.userService = userService;
            this.statusService = statusService;
            this.mapper = mapper;
            this.appSettingsService = appSettingsService;
        }

        /// <summary>
        /// The Users Roles Campus Groups repository.
        /// </summary>
        private IRepository<SyUsersRolesCampGrps> syUsersRolesCampGrps => this.advantageContext.GetRepositoryForEntity<SyUsersRolesCampGrps>();

        /// <summary>
        /// The Roles repository.
        /// </summary>
        private IRepository<SySysRoles> sySysRoles => this.advantageContext.GetRepositoryForEntity<SySysRoles>();

        /// <summary>
        /// The Roles repository.
        /// </summary>
        private IRepository<SyRoles> syRoles => this.advantageContext.GetRepositoryForEntity<SyRoles>();

        /// <summary>
        /// The Users repository.
        /// </summary>
        private IRepository<SyUsers> syUsers => this.advantageContext.GetRepositoryForEntity<SyUsers>();

        /// <summary>
        /// Get All Instructors.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<List<Instructors>>> GetAllInstructors( Guid campusGroupId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<List<Instructors>>();

                           try
                           {
                               var sysroleId = this.sySysRoles.Get(x => x.Descrip.ToLower() == "instructors").Select(x => x.SysRoleId).FirstOrDefault();
                               var roleInstructor = this.syRoles.Get(x => x.SysRoleId == sysroleId).Select(x => x.RoleId).ToList();
                               var usersRolesCampGrp = this.syUsersRolesCampGrps.Get(x => roleInstructor.Contains(x.RoleId) && x.CampGrpId == campusGroupId).Select(x => x.UserId).ToList();


                               var instructorsAll = this.syUsers.Get(x => usersRolesCampGrp.Contains(x.UserId) && x.AccountActive == true)
                               .Select(a => new Instructors()
                               {
                                   UserID = a.UserId,
                                   FullName = a.FullName,
                                   DisplayName = a.UserName

                               }).ToList();

                               result.Result = instructorsAll;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }
    }
}
