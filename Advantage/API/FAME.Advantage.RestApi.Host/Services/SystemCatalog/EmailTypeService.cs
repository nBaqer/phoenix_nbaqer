﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailTypeService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the email type service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The email type service.
    /// </summary>
    public class EmailTypeService : IEmailTypeService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailTypeService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        public EmailTypeService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }

        /// <summary>
        /// The email type repository that will return email types.
        /// </summary>
        private IRepository<SyEmailType> EmailTypeRepository => this.advantageContext.GetRepositoryForEntity<SyEmailType>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> Get()
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:EmailTypeAll",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.EmailTypeRepository.Get().ToList()),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampus(Guid campusId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:EmailTypeByCampus:{campusId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.EmailTypeRepository.Get().ToList()),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task< IList<IListItem<string, Guid>>> GetByCampusGroup(Guid campusGroupId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:EmailTypeByCampusGroup:{campusGroupId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.EmailTypeRepository.Get().ToList()),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The type of the entity id.
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetById<TEntId>(TEntId entityId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:EmailTypeById:{entityId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.EmailTypeRepository.GetById(entityId)),
                CacheExpiration.VERY_LONG));
        }

    }
}
