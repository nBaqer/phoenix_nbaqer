﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceCategoryService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the source category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The source category service.
    /// </summary>
    public class SourceCategoryService : ISourceCategoryService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceCategoryService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">The status service.</param>
        public SourceCategoryService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService, ICampusGroupService campusGroupService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The source category repository.
        /// </summary>
        private IRepository<AdSourceCatagory> SourceCategoryRepository => this.advantageContext.GetRepositoryForEntity<AdSourceCatagory>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<SourceCategory>> GetByCampus(Guid campusId)
        {
            var result = await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SourceCategoryByCampus:{campusId}",
                () => this.mapper.Map<IList<SourceCategory>>(this.SourceCategoryRepository.Get(pt => pt.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && pt.StatusId == this.ActiveId).Include(x => x.AdSourceType).ThenInclude(x => x.Status)).ToList(),
                CacheExpiration.VERY_LONG));
            return result;
        }

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampusGroup(Guid campusGroupId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SourceCategoryByCampusGroup:{campusGroupId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.SourceCategoryRepository.Get(pt => pt.CampGrpId == campusGroupId && pt.StatusId == this.ActiveId)).ToList(),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The type of entity id.
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetById<TEntId>(TEntId entityId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SourceCategoryById:{entityId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.SourceCategoryRepository.GetById(entityId)),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The has source type.
        /// </summary>
        /// <param name="sourceCategoryId">
        /// The source Category Id.
        /// </param>
        /// <param name="sourceTypeId">
        /// The source Type Id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> HasSourceType(Guid sourceCategoryId, Guid sourceTypeId, Guid? campusId = null)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            if (campusId == null || campusId == Guid.Empty)
            {
                campusId = await this.campusGroupService.GetCampusGroupAllId();
            }

            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SourceTypes:{sourceTypeId}:{sourceCategoryId}",
                () =>
                    {
                        var hasSourceType = this.SourceCategoryRepository
                            .Get(_ => _.SourceCatagoryId == sourceCategoryId && _.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId) && _.StatusId == activeStatusId
                                      && _.AdSourceType.Any(sc => sc.SourceTypeId == sourceTypeId && sc.StatusId == activeStatusId))
                            .Any();

                        return hasSourceType;
                    },
                CacheExpiration.VERY_LONG);
        }
    }
}
