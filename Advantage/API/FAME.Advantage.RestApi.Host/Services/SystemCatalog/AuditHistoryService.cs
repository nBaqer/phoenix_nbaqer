﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuditHistoryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AuditHistoryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The audit history service.
    /// </summary>
    public class AuditHistoryService : IAuditHistoryService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditHistoryService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public AuditHistoryService(IAdvantageDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// The audit history repository.
        /// </summary>
        private IRepository<SyAuditHist> AuditHistoryRepository =>
            this.context.GetRepositoryForEntity<SyAuditHist>();

        private IRepository<SyAuditHistDetail> AuditHistoryDetailsRepository =>
            this.context.GetRepositoryForEntity<SyAuditHistDetail>();

        /// <summary>
        /// The find audit history event date.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity (table name) to search for on the Audit History.
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<DateTime?> FindAuditHistoryEventDate<T>(string eventType, string columnName, string value)
        {
            return await Task.Run(() =>
            {
                {
                    return this.AuditHistoryRepository.Get(
                        x => x.Event == eventType && x.TableName.ToUpper() == typeof(T).Name.ToUpper()
                                                  && x.SyAuditHistDetail.Any(
                                                      y => y.ColumnName.ToUpper() == columnName.ToUpper()
                                                           && y.NewValue == value)).Select(x => x.EventDate).FirstOrDefault();
                }
            });
        }

        /// <summary>
        /// The get audit history.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity (table name) to search for on the Audit History.
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<AuditHistory>> GetAuditHistory<T>(string eventType, string columnName, List<string> value)
        {
            return await Task.Run(() =>
                {
                    {
                        return this.AuditHistoryDetailsRepository.Get(
                            x => x.AuditHist.Event == eventType && x.AuditHist.TableName.ToUpper() == typeof(T).Name.ToUpper()
                                                                && value.Any(y => y == x.NewValue) && x.ColumnName.ToUpper() == columnName.ToUpper())
                                            .Include(x => x.AuditHist).Select(_ => new AuditHistory()
                                            {
                                                Event = _.AuditHist.Event,
                                                TableName = _.AuditHist.TableName,
                                                EventDate = _.AuditHist.EventDate,
                                                UserName = _.AuditHist.UserName,
                                                AppName = _.AuditHist.AppName,
                                                AuditHistId = _.AuditHistId,
                                                EventRows = _.AuditHist.EventRows,
                                                Value = _.NewValue
                                            }).ToList();
                    }
                });
        }
    }
}