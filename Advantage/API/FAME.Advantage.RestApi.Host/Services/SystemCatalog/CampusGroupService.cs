﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the CampusGroupService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper; 

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The CampusGroupService service.
    /// </summary>
    public class CampusGroupService : ICampusGroupService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork; 

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroupService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        public CampusGroupService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The campus group repository.
        /// </summary>
        private IRepository<SyCampGrps> CampusGroupRepository => this.unitOfWork.GetRepositoryForEntity<SyCampGrps>();

        /// <summary>
        /// The campus group campus repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CampusGroupCampusRepository => this.unitOfWork.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The get campus group all.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public async Task<Guid> GetCampusGroupAllId()
        {
            return await Task.Run(
                       () =>
                           {
                               var campusGroupAll = this.CampusGroupRepository.Get(
                                       _ => _.IsAllCampusGrp.HasValue && _.IsAllCampusGrp.Value)
                                   .FirstOrDefault();

                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:GetCampusGroupAll",
                                   () =>
                                       {
                                           if (campusGroupAll != null)
                                           {
                                               return campusGroupAll.CampGrpId;
                                           }
                                           else
                                           {
                                               return Guid.Empty;
                                           }
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get all campus groups.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IEnumerable{IListItem{string, Guid}}</cref>
        ///     </see>
        ///     .
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllCampusGroups()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:AllCampusGroups",
                                   () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                       this.CampusGroupRepository.Get(c => c.StatusId == activeStatusId)),
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get campus groups for campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<Guid>> GetCampusGroupsForCampus(Guid campusId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:CampusGroupsByCampus:{campusId}",
                                   () =>
                                       {
                                           IList<Guid> campusGroupIdList = this.CampusGroupCampusRepository.Get(
                                               x => x.CampusId == campusId && x.Campus.StatusId == activeStatusId
                                                                           && x.CampGrp.StatusId == activeStatusId)
                                           .Select(x => x.CampGrpId).ToList();
                                           return campusGroupIdList;
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get all the campuses associated with a campus group id
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<Guid>> GetAllTheCampusesByCampusGroup(Guid campusGroupId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:CampusesByCampusGroup:{campusGroupId}",
                                   () =>
                                       {
                                           IList<Guid> campusGroupIdList = this.CampusGroupCampusRepository.Get(
                                                   x => x.CampGrp.CampGrpId == campusGroupId && x.Campus.StatusId == activeStatusId
                                                                               && x.CampGrp.StatusId == activeStatusId)
                                               .Select(x => x.CampusId).ToList();
                                           return campusGroupIdList;
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }
    }
}
