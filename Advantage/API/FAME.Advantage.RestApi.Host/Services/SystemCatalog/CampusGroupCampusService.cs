﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupCampusService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusGroupService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    /// <summary>
    /// The CampusGroupService service.
    /// </summary>
    public class CampusGroupCampusService : ICampusGroupCampusService
    {  
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusGroupCampusService"/> class.  
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public CampusGroupCampusService(IAdvantageDbContext unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The campus group Campus repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CampusGroupCampusRepository => this.unitOfWork.GetRepositoryForEntity<SyCmpGrpCmps>();
           
        /// <summary>
        /// The get campus group by campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<SyCampGrps> GetCampusGroupByCampusId(Guid campusId)
        { 
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:CampusGroupsByCampusId:{campusId}",
                                   () =>
                                       {
                                           var campusGroupCampuses = this.CampusGroupCampusRepository.Get(c => c.CampusId == campusId)
                                               .Include(x => x.CampGrp);
                                           return campusGroupCampuses.FirstOrDefault(cmpgrpCmp => cmpgrpCmp.CampGrp.IsAllCampusGrp != true)?.CampGrp
                                                         ?? campusGroupCampuses.FirstOrDefault()?.CampGrp;
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get campus group by campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<SyCampGrps>> GetCampusGroupsByCampusId(Guid campusId)
        {
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:CampusGroupsByCampusId:{campusId}",
                                   () =>
                                       {
                                           var campusGroupCampuses = this.CampusGroupCampusRepository.Get(c => c.CampusId == campusId)
                                               .Include(x => x.CampGrp);

                                           return campusGroupCampuses.Select(x => x.CampGrp).ToList();
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }
    }
}
