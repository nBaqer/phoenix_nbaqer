﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemService.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the SystemService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The system service.
    /// </summary>
    public class SystemService : ISystemService
    {
        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        public SystemService(IAdvantageDbContext advantageContext)
        {
            this.advantageContext = advantageContext;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The clear cache.
        /// </summary>
        public void ClearCache()
        {
            SimpleCache.Clear();
        }

        /// <summary>
        /// The clear cache for tenant.
        /// </summary>
        public void ClearCacheForTenant()
        {
            SimpleCache.RemoveLike(this.CacheKeyFirstSegment);
        }
    }
}
