﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodsWorkDaysService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The periods work days service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The periods work days service.
    /// </summary>
    public class PeriodsWorkDaysService : IPeriodsWorkDaysService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The work days service.
        /// </summary>
        private readonly IWorkDaysService workDaysService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PeriodsWorkDaysService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="workDaysService">
        /// The work Days Service.
        /// </param>
        public PeriodsWorkDaysService(IAdvantageDbContext context, IWorkDaysService workDaysService)
        {
            this.context = context;
            this.workDaysService = workDaysService;
        }

        /// <summary>
        /// The periods repository.
        /// </summary>
        private IRepository<SyPeriodsWorkDays> PeriodsWorkDaysRepository => this.context.GetRepositoryForEntity<SyPeriodsWorkDays>();

        /// <summary>
        /// The get periods work days details returns the list of periodWorkDays based on the given period Ids.
        /// </summary>
        /// <param name="periodIds">
        /// The period ids.
        /// </param>
        /// <returns>
        /// The List of PeriodsWorkDays.
        /// </returns>
        public async Task<IEnumerable<PeriodsWorkDays>> GetPeriodsWorkDaysDetails(IList<Guid> periodIds)
        {
            return await Task.Run(
                        async () =>
                            {
                                var workDays = await this.workDaysService.GetAllWorkDays();

                                return this.PeriodsWorkDaysRepository.Get(x => periodIds.Contains(x.PeriodId)).Select(
                                x => new PeriodsWorkDays()
                                {
                                    PeriodId = x.PeriodId,
                                    WorkDayId = x.WorkDayId,
                                    PeriodIdWorkDayId = x.PeriodIdWorkDayId,
                                    WorkDayDescription = workDays.FirstOrDefault(w => w.WorkDaysId == x.WorkDayId).WorkDaysDescription,
                                    ViewOrder = workDays.FirstOrDefault(w => w.WorkDaysId == x.WorkDayId).ViewOrder
                                });
                            });
        }
    }
}
