﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusesService.cs" company="FAME Inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ISyStatusesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The System Statuses Service interface to get the info regarding active/inactive status.
    /// </summary>
    public class StatusesService : IStatusesService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusesService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        public StatusesService(IMapper mapper, IAdvantageDbContext advantageContext)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
        }

        /// <summary>
        /// The system statuses repository that will give if the status is Active/Inactive.
        /// </summary>
        private IRepository<SyStatuses> StatusesRepository => this.advantageContext.GetRepositoryForEntity<SyStatuses>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get all system statuses.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllSystemStatuses()
        {
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:SyStatuses",
                           () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(this.StatusesRepository.Get()),
                           CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get active status guid.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetActiveStatusId()
        {
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:ActiveStatus",
                           () =>
                               {
                                   var singleOrDefault = this.StatusesRepository.Get(stat => stat.StatusCode.Equals("A", StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                                   if (singleOrDefault != null)
                                   {
                                       return singleOrDefault.StatusId;
                                   }
                                   else
                                   {
                                       return Guid.Empty;
                                   }
                               },
                       CacheExpiration.VERY_LONG));
        }
        

        /// <summary>
        /// The get in active status guid.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetInactiveStatusId()
        {
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:InActiveStatus",
                           () =>
                               {
                                   var singleOrDefault = this.StatusesRepository.Get(stat => stat.StatusCode.Equals("I", StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();
                                   if (singleOrDefault != null)
                                   {
                                       return singleOrDefault.StatusId;
                                   }
                                   else
                                   {
                                       return Guid.Empty;
                                   }
                               },
                           CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The convert status.
        /// </summary>
        /// <param name="statusId">
        /// The status id.
        /// </param>
        /// <returns>
        /// The <see cref="bool?"/>.
        /// </returns>
        public async Task<bool?> ConvertStatus(Guid statusId)
        {
            var activeStatusId = await this.GetActiveStatusId();
            var inactiveStatusId = await this.GetInactiveStatusId();

            if (statusId == activeStatusId)
            {
                return true;
            }

            if (statusId == inactiveStatusId)
            {
                return false;
            }

            return null;
        }
    }
}
