﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the CampusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The campus service.
    /// </summary>
    public class CampusService : ICampusService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;
        private IRepository<SyUsers> UserRepository => this.unitOfWork.GetRepositoryForEntity<SyUsers>();
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;


        /// <summary>
        /// Initializes a new instance of the <see cref="CampusService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        ///     The unit of work.
        /// </param>
        /// <param name="mapper">
        ///     The mapper.
        /// </param>
        /// <param name="statusService"></param>
        /// <param name="systemConfigurationAppSettingService"></param>
        public CampusService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
            this.mapper = mapper;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The campus group repository.
        /// </summary>
        private IRepository<SyCampGrps> CampusGroupRepository => this.unitOfWork.GetRepositoryForEntity<SyCampGrps>();

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.unitOfWork.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The campus group and campus relation repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CmpGrpCmps => this.unitOfWork.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The Statuses repository.
        /// </summary>
        private IRepository<SyStatuses> Statuses => this.unitOfWork.GetRepositoryForEntity<SyStatuses>();
        /// <summary>
        /// the USer Role Cmpus Group repository
        /// </summary>
        private IRepository<SyUsersRolesCampGrps> usrRoleCampGrp => this.unitOfWork.GetRepositoryForEntity<SyUsersRolesCampGrps>();

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.unitOfWork.GetTenantUser();
        /// <summary>
        /// The get campus group all.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetCampusGroupAllId()
        {
            return await Task.Run(
                       () =>
                           {
                               var campusGroupAll = this.CampusGroupRepository.Get(
                                       _ => _.IsAllCampusGrp.HasValue && _.IsAllCampusGrp.Value)
                                   .FirstOrDefault();

                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:GetCampusGroupAll",
                                   () =>
                                       {
                                           if (campusGroupAll != null)
                                           {
                                               return campusGroupAll.CampGrpId;
                                           }
                                           else
                                           {
                                               return Guid.Empty;
                                           }
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get campus id for CMS id.
        /// </summary>
        /// <param name="cmsId">
        /// The Client Management Software id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Guid GetCampusIdForCmsId(string cmsId)
        {
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:GetCampusIdForCmsId:{cmsId}",
                () =>
                    {
                        return this.CampusRepository.Get(x => x.CmsId == cmsId).FirstOrDefault().CampusId;
                    },
                CacheExpiration.VERY_LONG);
        }
        /// <summary>
        /// Returns if AFA Integration is enabled for given campus.
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public bool IsAFAIntegrationEnabled(Guid campusId)
        {

            return this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.EnableAFAIntegration, campusId).Equals("yes", StringComparison.InvariantCultureIgnoreCase);

        }
        /// <summary>
        /// The get cms ids to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<string>> GetCmsIdsToSync()
        {
            //var campusIds = await this.systemConfigurationAppSettingService.GetCampusIdForGivenValue("EnableAFAIntegration", "yes");
            var AfaEnabledValue = await this.systemConfigurationAppSettingService.GetAppSettingValues("EnableAFAIntegration");
            if (AfaEnabledValue == null || AfaEnabledValue.All(x => x.Value != ConfigurationAppSettingsValues.Yes))
            {
                return null;
            }

            bool isAllCampuses = false;
            isAllCampuses = AfaEnabledValue.All(
                x => x.Value == ConfigurationAppSettingsValues.Yes && x.CampusId == null);

            List<Guid> campuses = new List<Guid>();
            IList<string> cmsIds = new List<string>();
            if (!isAllCampuses)
            {
                campuses = AfaEnabledValue
                    .Where(x => x.Value == ConfigurationAppSettingsValues.Yes && x.CampusId != null)
                    .Select(x => x.CampusId.Value).ToList();
                foreach (var campusId in campuses)
                {
                    cmsIds.Add(this.CampusRepository.Get(x => x.CampusId == campusId && !string.IsNullOrEmpty(x.CmsId)).FirstOrDefault()?.CmsId);
                }
            }

            if (isAllCampuses)
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();
                var campus = this.CampusRepository.Get(x => x.StatusId == activeStatusId).Select(x => x.CampusId).ToList();
                foreach (var campusId in campus)
                {
                    cmsIds.Add(this.CampusRepository.Get(x => x.CampusId == campusId && !string.IsNullOrEmpty(x.CmsId)).FirstOrDefault()?.CmsId);
                }
            }

            return cmsIds;
        }


        /// <summary>
        /// The get Client Management Software id for campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCmsIdForCampusId(string campusId)
        {
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:GetCmsIdForCampusId:{campusId}",
                () =>
                    {
                        return this.CampusRepository.Get(x => x.CampusId == Guid.Parse(campusId)).FirstOrDefault()?.CmsId;
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get all campuses.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllCampuses()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:AllCampuses",
                                   () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                       this.CampusRepository.Get(c => c.StatusId == activeStatusId)),
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get all campuses belonging to the logged in UserId.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllCampusesByUserId()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                () =>
                {
                    // get the campusGrps to which the user and its role belong
                    //now get the list of campus from the campus group
                    var roleCampGrp = this.usrRoleCampGrp.Get(g => g.UserId == this.TenantUser.Id).Select(r => r.CampGrpId).ToList();
                    var campuses = this.CmpGrpCmps.Get(c => roleCampGrp.Contains(c.CampGrpId)).Select(a => a.CampusId).ToList();

                    return this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                        this.CampusRepository.Get(c => c.StatusId == activeStatusId && campuses.Contains(c.CampusId))).Distinct();
                });
        }

        /// <summary>
        /// Get school name by campus id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<CampusInformation>> GetCampusById(string campusId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<CampusInformation>();

                           try
                           {
                               var dataItem = await this.CampusRepository.Get()
                               .Where(a => a.CampusId.ToString() == campusId)
                               .Select(a => new CampusInformation()
                               {
                                   SchoolName = a.SchoolName != null ? a.SchoolName.Trim() : "",
                                   Address = a.Address1 != null ? a.Address1.Trim() : "",
                                   Address2 = a.Address2 != null ? a.Address2.Trim() : "",
                                   CampusId = a.CampusId,
                                   City = a.City != null ? a.City.Trim() : "",
                                   CountryId = a.CountryId != null ? a.CountryId.Value.ToString().Trim() : Guid.Empty.ToString(),
                                   StateId = a.StateId != null ? a.StateId.Value.ToString().Trim() : Guid.Empty.ToString(),
                                   ZipCode = a.Zip,
                                   Code = a.CampCode
                               }).FirstOrDefaultAsync();

                               if (dataItem == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"School with campusId of {campusId} not found.";
                                   return result;
                               }

                               result.Result = dataItem;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }



        /// <summary>
        /// The get campus group all.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<List<CampusGroupsWithCampus>>> GetCampusGroupsWithCampus()
        {
            return await Task.Run(
                       async () =>
                       {

                           var result = new ActionResult<List<CampusGroupsWithCampus>>();

                           try
                           {
                               var activeStatusId = await this.statusService.GetActiveStatusId();
                               var cmpGrpCmpsQueryable = this.CmpGrpCmps.Get();
                               var campusRepository = this.CampusRepository.Get();

                               var roleCampGrp = this.usrRoleCampGrp.Get(g => g.UserId == this.TenantUser.Id).Select(r => r.CampGrpId).ToList();

                               var campusGroupAll = this.CampusGroupRepository.Get(x => x.StatusId == activeStatusId && roleCampGrp.Contains(x.CampGrpId))
                               .Select(a => new CampusGroupsWithCampus()
                               {
                                   CampusGroupId = a.CampGrpId,
                                   CampusGroupName = a.CampGrpDescrip,
                                   Campuses = cmpGrpCmpsQueryable.Where(x => a.CampGrpId == x.CampGrpId && x.Campus.StatusId == activeStatusId)
                                   .Select(y => new CampusDTO()
                                   {
                                       CampusId = y.CampusId,
                                       CampusName = campusRepository.Where(x => x.CampusId == y.CampusId).Select(e => e.CampDescrip).FirstOrDefault()
                                   }).ToList()
                               }).ToList();

                               result.Result = campusGroupAll;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }
    }
}
