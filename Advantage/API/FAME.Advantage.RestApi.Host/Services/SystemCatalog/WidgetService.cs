﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WidgetService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the WidgetService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The user service.
    /// </summary>
    public class WidgetService : IWidgetService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="WidgetService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="userService">
        /// The user service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="mapper">
        /// The mapper class.
        /// </param>
        /// <param name="appSettingsService">
        /// The app Settings Service.
        /// </param>
        public WidgetService(IAdvantageDbContext advantageContext, IUserService userService, IStatusesService statusService, IMapper mapper, ISystemConfigurationAppSettingService appSettingsService)
        {
            this.advantageContext = advantageContext;
            this.userService = userService;
            this.statusService = statusService;
            this.mapper = mapper;
            this.appSettingsService = appSettingsService;
        }

        /// <summary>
        /// The widgets repository.
        /// </summary>
        private IRepository<SyWidgets> WidgetRepository => this.advantageContext.GetRepositoryForEntity<SyWidgets>();

        /// <summary>
        /// The widget resource roles repository.
        /// </summary>
        private IRepository<SyWidgetResourceRoles> WidgetResourceRolesRepository => this.advantageContext.GetRepositoryForEntity<SyWidgetResourceRoles>();

        /// <summary>
        /// The widget user resource settings repository.
        /// </summary>
        private IRepository<SyWidgetUserResourceSettings> WidgetUserResourceSettings => this.advantageContext.GetRepositoryForEntity<SyWidgetUserResourceSettings>();

        /// <summary>
        /// The campus group relation repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> SyCmpGrpCmpsRepository => this.advantageContext.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.advantageContext.GetTenantUser();

        /// <summary>
        /// Returns all available widgets along with settings if they exist for a user depending on users system role and current module/&gt; class.
        /// </summary>
        /// <param name="resourceId">
        /// The resource the user is currently accessing
        /// </param>
        /// <param name="campusId">
        /// The users current campus id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Widget>> GetUserResourceWidgets(int resourceId, Guid campusId)
        {
            return await Task.Run(
                async () =>
                {
                    try
                    {
                        Guid activeStatusId = await this.statusService.GetActiveStatusId();
                        User userDetails = await this.userService.GetUserbyId(this.advantageContext.GetTenantUser().Id);

                        // based on campus user is logged into, get campus group ids
                        List<Guid> campusGroupIds = this.SyCmpGrpCmpsRepository.Get()
                                                        .Where(a => a.CampusId == campusId)
                                                        .Select(a => a.CampGrpId).ToList();

                        // get sytem roles user is part of on these campuses
                        List<int> userSystemRoleIds = userDetails.UserRoles
                                                        .Where(a => campusGroupIds.Any(b => a.CampusGroupId == b))
                                                        .Select(a => a.SysRoleId).Distinct().ToList();

                        // tables
                        IQueryable<SyWidgets> widgets = this.WidgetRepository.Get();
                        IQueryable<SyWidgetResourceRoles> widgetResourceRoles = this.WidgetResourceRolesRepository.Get();
                        IQueryable<SyWidgetUserResourceSettings> widgetUserResourceSettings = this.WidgetUserResourceSettings.Get();
                        List<Widget> availableWidgets;

                        if (this.TenantUser.IsSupportUser)
                        {
                            availableWidgets = await (from a in widgets
                                                                   join b in widgetResourceRoles on a.WidgetId equals b.WidgetId
                                                                   join c in widgetUserResourceSettings on new { w = (Guid?)b.WidgetId, r = b.ResourceId } equals new { w = c.WidgetId, r = c.ResourceId } into bc
                                                                   from userSetting in bc.DefaultIfEmpty()
                                                                   where b.ResourceId == resourceId &&
                                                                         a.StatusId == activeStatusId &&
                                                                         b.StatusId == activeStatusId &&
                                                                         (userSetting != null ? userSetting.UserId == userDetails.UserId : true) &&
                                                                         (userSetting != null ? userSetting.StatusId == activeStatusId : true) &&
                                                                         (userSetting != null ? userSetting.ResourceId == b.ResourceId : true)
                                                                   select new Widget
                                                                   {
                                                                       WidgetId = a.WidgetId,
                                                                       Code = a.Code,
                                                                       Rows = a.Rows,
                                                                       Columns = a.Columns,
                                                                       HasSetting = userSetting != null,
                                                                       PositionX = userSetting != null ? userSetting.PositionX : 0,
                                                                       PositionY = userSetting != null ? userSetting.PositionY : 0,
                                                                       IsVisible = userSetting != null ? userSetting.IsVisible : false,
                                                                              }).GroupBy(a => a.WidgetId)
                                                                .Select(a => a.FirstOrDefault()).AsNoTracking()
                                                                .ToListAsync();
                        }
                        else
                        {
                          availableWidgets = await (from a in widgets
                                                                   join b in widgetResourceRoles on a.WidgetId equals b.WidgetId
                                                                   join c in widgetUserResourceSettings on new { w = (Guid?)b.WidgetId, r = b.ResourceId } equals new { w = c.WidgetId, r = c.ResourceId } into bc
                                                                   from userSetting in bc.DefaultIfEmpty()
                                                                   where userSystemRoleIds.Any(id => id == b.SysRoleId) &&
                                                                         b.ResourceId == resourceId &&
                                                                         a.StatusId == activeStatusId &&
                                                                         b.StatusId == activeStatusId &&
                                                                         (userSetting != null ? userSetting.UserId == userDetails.UserId : true) &&
                                                                         (userSetting != null ? userSetting.StatusId == activeStatusId : true) &&
                                                                         (userSetting != null ? userSetting.ResourceId == b.ResourceId : true)
                                                                   select new Widget
                                                                   {
                                                                       WidgetId = a.WidgetId,
                                                                       Code = a.Code,
                                                                       Rows = a.Rows,
                                                                       Columns = a.Columns,
                                                                       HasSetting = userSetting != null,
                                                                       PositionX = userSetting != null ? userSetting.PositionX : 0,
                                                                       PositionY = userSetting != null ? userSetting.PositionY : 0,
                                                                       IsVisible = userSetting != null ? userSetting.IsVisible : false,
                                                                              }).GroupBy(a => a.WidgetId)
                                                                .Select(a => a.FirstOrDefault()).AsNoTracking()
                                                                .ToListAsync();

                        }

                        var gradesFormat = this.appSettingsService.GetAppSettingValueByCampus(
                            ConfigurationAppSettingsKeys.GradesFormat,
                            campusId);

                        if (string.IsNullOrEmpty(gradesFormat))
                        {
                            gradesFormat = this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GradesFormat);
                        }

                        var studentsReadyForGraduation = this.appSettingsService.GetAppSettingValueByCampus(
                            ConfigurationAppSettingsKeys.GraduationWidget,
                            campusId);

                        if (string.IsNullOrEmpty(studentsReadyForGraduation))
                        {
                            studentsReadyForGraduation = this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GraduationWidget);
                        }

                        if (!gradesFormat.Equals(GradesFormat.Numeric, StringComparison.InvariantCultureIgnoreCase) || !studentsReadyForGraduation.Equals("true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            availableWidgets.Remove(
                                availableWidgets.FirstOrDefault(
                                    x => x.Code == WidgetsCodes.StudentsReadyForGraduation));
                        }

                        return availableWidgets;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return null;
                    }
                });
        }
    }
}
