﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WorkDaysService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The work days service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <inheritdoc />
    /// <summary>
    /// The work days service.
    /// </summary>
    public class WorkDaysService : IWorkDaysService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkDaysService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public WorkDaysService(IAdvantageDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// The WorkDays repository.
        /// </summary>
        private IRepository<PlWorkDays> WorkDaysRepository => this.context.GetRepositoryForEntity<PlWorkDays>();

        /// <summary>
        /// The get all work days.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<WorkDay>> GetAllWorkDays()
        {
            return await Task.Run(
                       () =>
                           {
                               return this.WorkDaysRepository.Get().Select(
                                   x => new WorkDay()
                                            {
                                                ViewOrder = x.ViewOrder,
                                                WorkDaysDescription = x.WorkDaysDescrip,
                                                WorkDaysId = x.WorkDaysId
                                            }).ToList();
                           });
        }
    }
}
