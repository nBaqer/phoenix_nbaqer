﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRoleStatusService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the User Role Status Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System.Threading.Tasks;
    using AutoMapper;

    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The user role status service.
    /// </summary>
    public class UserRoleStatusService : IUserRoleStatusService
    {
        /// <summary>
        /// The mapper is used to map the AcademicCalendar Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRoleStatusService"/> class. 
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public UserRoleStatusService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The Is supportUser action allows you to validate whether the logged in user is a support user or not.
        /// </summary>
        /// <remarks>
        /// The IsSupportUser action allows you to validate whether the logged in user is a support user or not.
        /// </remarks>
        /// <returns>
        /// Returns true if the logged in user is a support user else returns false. 
        /// </returns>
        public async Task<bool> IsSupportUser()
        {
            return await Task.Run(() => this.mapper.Map<bool>(this.ValidateUser()));
        }

        /// <summary>
        /// The Validaet user allows you to validate whether the logged in user is a support user or not.
        /// </summary>
        /// <remarks>
        /// The ValidateUser allows you to validate whether the logged in user is a support user or not.
        /// </remarks>
        /// <returns>
        /// Returns true if the logged in user is a support user else returns false. 
        /// </returns>
        private bool ValidateUser()
        {
            var result = this.context.GetTenantUser().IsSupportUser;

            return result;
        }
    }
}
