﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Period Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The period service.
    /// </summary>
    public class PeriodService : IPeriodService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The periods work days service.
        /// </summary>
        private readonly IPeriodsWorkDaysService periodsWorkDaysService;

        /// <summary>
        /// The time interval service.
        /// </summary>
        private readonly ITimeIntervalService timeIntervalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="PeriodService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="periodsWorkDaysService">
        /// The periods work days service.
        /// </param>
        /// <param name="timeIntervalService">
        /// The time interval service.
        /// </param>
        public PeriodService(IAdvantageDbContext context, IPeriodsWorkDaysService periodsWorkDaysService, ITimeIntervalService timeIntervalService)
        {
            this.context = context;
            this.periodsWorkDaysService = periodsWorkDaysService;
            this.timeIntervalService = timeIntervalService;
        }

        /// <summary>
        /// The SyPeriods repository.
        /// </summary>
        private IRepository<SyPeriods> PeriodsRepository => this.context.GetRepositoryForEntity<SyPeriods>();

        /// <summary>
        /// The get period detail.
        /// </summary>
        /// <param name="periodId">
        /// The period id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IListItem<Guid, Guid>> GetPeriodDetail(Guid? periodId)
        {
            return await Task.Run(
                       () =>
                           {
                               var period = this.PeriodsRepository.Get(x => x.PeriodId == periodId).FirstOrDefault();
                               if (period != null)
                               {
                                   return new ListItem<Guid, Guid>
                                              {
                                                  Text = period.StartTimeId,
                                                  Value = period.EndTimeId
                                              };
                               }

                               return null;
                           });
        }

        /// <summary>
        /// The get period and scheduled hours by ids.
        /// </summary>
        /// <param name="periodIds">
        /// The period ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<PeriodDetail>> GetPeriodAndScheduledHoursByIds(List<Guid> periodIds)
        {
            return await Task.Run(
                       async () =>
                           {
                               var periodList = new List<PeriodDetail>();
                               var timeIntervals = await this.timeIntervalService.GetAllTimeIntervals();
                               timeIntervals = timeIntervals.ToList();
                               var periodsWorkDaysList = await this.periodsWorkDaysService.GetPeriodsWorkDaysDetails(periodIds); 
                               var periods = this.PeriodsRepository.Get(x => periodIds.Contains(x.PeriodId)).ToList();
                               periods.ForEach(
                                   period =>
                                       {
                                           var duration = 0m;
                                           var periodsWorkDays = periodsWorkDaysList.Where(x => x.PeriodId == period.PeriodId); 
                                           var startTimeInterval = timeIntervals.FirstOrDefault(x => x.Id == period.StartTimeId)?.Datetime;
                                           var endTimeInterval = timeIntervals.FirstOrDefault(x => x.Id == period.EndTimeId)?.Datetime;
                                           if (endTimeInterval != null && startTimeInterval != null)
                                           {
                                               duration = Convert.ToDecimal((endTimeInterval - startTimeInterval).Value.TotalHours);
                                           }

                                           var periodDetail =
                                               new PeriodDetail
                                                   {
                                                       PeriodId = period.PeriodId,
                                                       EndTimeInterval = endTimeInterval ?? DateTime.MinValue,
                                                       StartTimeInterval = startTimeInterval ?? DateTime.MinValue,
                                                       ScheduleByWorkday = 
                                                           new List<IListItem<string, decimal>>()
                                                   };
                                          
                                           foreach (var periodsWorkDay in periodsWorkDays)
                                           {
                                               periodDetail.ScheduleByWorkday.Add(new ListItem<string, decimal> { Text = periodsWorkDay.WorkDayDescription, Value = duration });
                                           }

                                           periodList.Add(periodDetail);
                                       });

                               return periodList;
                           });
        }
    }
}