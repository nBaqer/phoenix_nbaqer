﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusCodesService.cs" company="FAME Inc">
//   2017
// </copyright>
// <summary>
//   Defines the ISyStatusesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Common;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The System Statuses Service interface to get the info regarding active/inactive status.
    /// </summary>
    public class StatusCodesService : IStatusCodesService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status codes service.
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The sys status codes service.
        /// </summary>
        private readonly ISystemStatusService sysStatusService;

        /// <summary>
        /// The sys status codes service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusCodesService"/> class. 
        /// Initializes a new instance of the <see cref="StatusesService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="campusService">
        /// The campus Service.
        /// </param>
        /// <param name="sysStatusService">
        /// The sys Status Service.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus group service
        /// </param>
        public StatusCodesService(IMapper mapper, IAdvantageDbContext advantageContext, ICampusService campusService, ISystemStatusService sysStatusService, IStatusesService statusService, ICampusGroupService campusGroupService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.campusService = campusService;
            this.sysStatusService = sysStatusService ?? throw new ArgumentNullException(nameof(sysStatusService));
            this.statusService = statusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The system statuses repository that will give if the status is Active/Inactive.
        /// </summary>
        private IRepository<SyStatusCodes> StatusCodesRepository => this.advantageContext.GetRepositoryForEntity<SyStatusCodes>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get status code id by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="campusgroupId">
        /// The campusgroup Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetStatusCodeIdByName(string name, SystemStatusLevel level, Guid? campusgroupId = null)
        {
            var systemStatusId = await this.sysStatusService.GetSystemStatusCodeIdByName(name, level);
            var activeStatusId = await this.statusService.GetActiveStatusId();
            if (campusgroupId == null || campusgroupId == Guid.Empty)
            {
                campusgroupId = await this.campusGroupService.GetCampusGroupAllId();
            }

            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:StatusCode:{name}",
                () =>
                    {

                        var singleOrDefault = this.StatusCodesRepository
                            .Get(_ => _.SysStatusId == systemStatusId && _.CampGrpId == campusgroupId && _.StatusId == activeStatusId)
                            .FirstOrDefault();
                        if (singleOrDefault != null)
                        {
                            return singleOrDefault.StatusCodeId;
                        }
                        else
                        {
                            return Guid.Empty;
                        }
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get all status codes id by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<Guid>> GetAllStatusCodesIdByName(string name, SystemStatusLevel level)
        {
            var systemStatusId = await this.sysStatusService.GetSystemStatusCodeIdByName(name, level);
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:StatusCode:{name}",
                () =>
                    {
                        var singleOrDefault = this.StatusCodesRepository
                            .Get(_ => _.SysStatusId == systemStatusId && _.StatusId == activeStatusId).Select(x => x.StatusCodeId);

                        return singleOrDefault.ToList();
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The exists.
        /// </summary>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="campusId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> Exists(Guid statusCodeId, SystemStatusLevel level, Guid? campusId = null)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            if (campusId == null || campusId == Guid.Empty)
            {
                campusId = await this.campusGroupService.GetCampusGroupAllId();
            }

            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:StatusCodeExists:{statusCodeId}:{level}",
                () =>
                    {

                        var possibleCampusGroups = this.StatusCodesRepository
                            .Get(_ => _.StatusCodeId == statusCodeId && (_.CampGrp.StatusId == activeStatusId && _.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId)) && _.StatusId == activeStatusId)
                            .ToList();

                        return possibleCampusGroups.Count > 0;
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The is system status.
        /// </summary>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <param name="systemStatus">
        /// The system status.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> IsSystemStatus(Guid statusCodeId, Constants.SystemStatus systemStatus)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:StatusCodeSysStatus:{statusCodeId}:{systemStatus}",
                () =>
                    {
                        var statusCode = this.StatusCodesRepository
                            .Get(_ => _.StatusCodeId == statusCodeId && _.SysStatusId == (int)systemStatus && _.StatusId == activeStatusId)
                            .FirstOrDefault();

                        return statusCode != null;
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get active not enroll lead status.
        /// </summary>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetActiveNotEnrollLeadStatusByCampus(Guid campusId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:LeadStatus:{campusId}",
                           () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(this.StatusCodesRepository.Get(x => x.SysStatusId != (int)Constants.SystemStatus.Enrolled && x.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && x.StatusId == activeStatusId && x.SysStatus.StatusLevelId == (int)Constants.StatusLevel.Lead)).ToList(),
                           CacheExpiration.VERY_LONG));
        }


        /// <summary>
        /// Get status description by status id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<StatusCode>> GetStatusCodeById(string statusCodeId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<StatusCode>();

                           try
                           {
                               var dataItem = await this.StatusCodesRepository.Get().Where(a => a.StatusCodeId.ToString() == statusCodeId)
                               .Select(i => new StatusCode()
                               {
                                   StatusCodeDescription = i.StatusCodeDescrip,
                                   StatusCodeId = i.StatusCodeId.ToString()
                               }).AsNoTracking().FirstOrDefaultAsync();

                               if (dataItem == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"Status with statusId of {statusCodeId} not found.";
                                   return result;
                               }

                               result.Result = dataItem;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        /// <summary>
        /// The get student status codes.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="studentStatuses">
        /// The student statuses.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStudentStatusCodes(Guid campusId, StudentStatuses studentStatuses)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:StudentStatusCodes:{studentStatuses}:{campusId}",
                           () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(this.StatusCodesRepository.Get(x => (x.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId)
                                                                                                                             || x.CampGrp.IsAllCampusGrp == true)
                                                                                                                           && x.StatusId == activeStatusId
                                                                                                                           && x.SysStatus.SysStatusId == (int)studentStatuses).OrderBy(x => x.StatusCodeDescrip)).ToList(),
                           CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// Get status codes for tenant, or filter by campus group id or campus id.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="StatusCode"/>.
        /// </returns>
        public async Task<ActionResult<List<StatusCode>>> GetStatusCodes(Guid? campusGroupId = null, Guid? campusId = null)
        {
            var result = new ActionResult<List<StatusCode>>();

            try
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();

                var qry = this.StatusCodesRepository.Get().Where(a => a.SysStatus.StatusId == activeStatusId);

                if (campusGroupId.HasValue && !campusId.HasValue)
                {
                    qry = qry.Where(x => x.CampGrpId == campusGroupId);
                }

                if (campusId.HasValue)
                {
                    qry = qry.Where(x => x.CampGrp.SyCmpGrpCmps.Any(y => y.CampusId == campusId));
                }

                var dataResult = await SimpleCache.GetOrAddExisting(
                            $"{this.CacheKeyFirstSegment}:GetAllStatusCodes:{campusGroupId}:{campusId}",
                            async () =>
                            {
                                return await qry
                                .Select(i => new StatusCode()
                                {
                                    StatusCodeDescription = i.StatusCodeDescrip,
                                    StatusCodeId = i.StatusCodeId.ToString(),
                                    SystemStatusCodeId = i.SysStatusId
                                }).AsNoTracking().ToListAsync();
                            },
                            CacheExpiration.MEDIUM);

                if (dataResult == null || !dataResult.Any())
                {
                    result.Result = dataResult;
                    result.ResultStatus = Enums.ResultStatus.Success;
                    result.ResultStatusMessage = $"No statuses found.";
                    return result;
                }

                result.Result = dataResult;
                result.ResultStatus = Enums.ResultStatus.Success;
                result.ResultStatusMessage = "Successfully returned status codes";
                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = e.Message;
                return result;
            }
        }

        /// <summary>
        /// The get status codes.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="systemStatusCodeId">
        /// The system status code id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStatusCodes(Guid campusId, List<int> systemStatusCodeId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();


            return await Task.Run(
                       () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(this.StatusCodesRepository.Get(x => (x.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId)
                                                                                                                             || x.CampGrp.IsAllCampusGrp == true)
                                                                                                                           && x.StatusId == activeStatusId
                                                                                                                       && systemStatusCodeId
                                                                                                                           .Any(
                                                                                                                               m =>
                                                                                                                                   m == x
                                                                                                                                       .SysStatus
                                                                                                                                       .SysStatusId))
                           .OrderBy(x => x.StatusCodeDescrip)).ToList());
        }
    }
}
