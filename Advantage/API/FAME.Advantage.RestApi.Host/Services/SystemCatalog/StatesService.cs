﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatesService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the states service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The states service.
    /// </summary>
    public class StatesService : IStatesService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatesService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">The status service.</param>
        public StatesService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }

        /// <summary>
        /// The states repository.
        /// </summary>
        private IRepository<SyStates> StatesRepository => this.advantageContext.GetRepositoryForEntity<SyStates>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampus(Guid campusId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SyStatesByCampus:{campusId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.StatesRepository.Get(pt => pt.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && pt.StatusId == this.ActiveId)).ToList(),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampusGroup(Guid campusGroupId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SyStatesByCampusGroup:{campusGroupId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.StatesRepository.Get(pt => pt.CampGrpId == campusGroupId && pt.StatusId == this.ActiveId)).ToList(),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The type of entity id.
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetById<TEntId>(TEntId entityId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:SyStatesById:{entityId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.StatesRepository.GetById(entityId)),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get state board states.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStateBoardStates()
        {
            return await Task.Run(() =>
                {
                    var stateBoardStates = this.StatesRepository
                        .Get(s => s.IsStateBoard.HasValue && s.IsStateBoard.Value && s.CampGrp.IsAllCampusGrp.HasValue && s.CampGrp.IsAllCampusGrp.Value).OrderBy(s => s.StateDescrip).Select(
                            s => new ListItem<string, Guid>() { Value = s.StateId, Text = s.StateDescrip }).ToList();

                    return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:SyStateBoardStates",
                                   () => stateBoardStates,
                                   CacheExpiration.VERY_LONG);
                });
        }

        /// <summary>
        /// The get all states.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllStates()
        {
            return await Task.Run(() =>
                {
                    var states = this.StatesRepository
                        .Get(s => s.CampGrp.IsAllCampusGrp.HasValue && s.CampGrp.IsAllCampusGrp.Value).OrderBy(s => s.StateDescrip).Select(
                            s => new ListItem<string, Guid>() { Value = s.StateId, Text = s.StateDescrip }).ToList();

                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:SyAllStates",
                        () => states,
                        CacheExpiration.VERY_LONG);
                });
        }

        /// <summary>
        /// The get all states short descriptions.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllStatesShortDescription()
        {
            return await Task.Run(() =>
                {
                    var states = this.StatesRepository
                        .Get(s => s.CampGrp.IsAllCampusGrp.HasValue && s.CampGrp.IsAllCampusGrp.Value).OrderBy(s => s.StateCode).ToList();

                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:SyAllStatesShortDescription",
                        () => this.mapper.Map<IList<IListItem<string, Guid>>>(states),
                        CacheExpiration.VERY_LONG);
                });
        }


        /// <summary>
        /// The get all states by country id.
        /// </summary>
        /// <param name="countryId">
        /// The country Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStatesByCountryId(Guid? countryId)
        {
            return await Task.Run(() =>
                {
                    var states = this.StatesRepository
                        .Get(s => s.CampGrp.IsAllCampusGrp.HasValue && s.CampGrp.IsAllCampusGrp.Value && s.CountryId.HasValue && s.CountryId.Value == countryId).OrderBy(s => s.StateCode).ToList();

                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:SyStatesByCountryId:{countryId}",
                        () => this.mapper.Map<IList<IListItem<string, Guid>>>(states),
                        CacheExpiration.VERY_LONG);
                });
        }

        /// <summary>
        /// The get id by name.
        /// </summary>
        /// <param name="stateName">
        /// The state name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetIdByName(string stateName)
        {
            return await Task.Run(
                       () =>
                           {
                               var state = this.StatesRepository
                                   .Get(s => s.StateDescrip.ToLower() == stateName.ToLower()).FirstOrDefault();

                               var result = state != null ? state.StateId : Guid.Empty;

                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:SyStatesByName:{stateName}",
                                   () => result,
                                   CacheExpiration.VERY_LONG);
                           });
        }
    }
}
