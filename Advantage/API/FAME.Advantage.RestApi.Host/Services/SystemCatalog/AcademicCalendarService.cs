﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicCalendarService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the AcademicCalendarService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The academic calendar service.
    /// </summary>
    public class AcademicCalendarService : IAcademicCalendarService
    {
        /// <summary>
        /// The mapper is used to map the AcademicCalendar Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the AcademicCalendarService class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AcademicCalendarService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The academic calendar repository.
        /// </summary>
        private IRepository<SyAcademicCalendars> AcademicCalendarTypeRepository => this.context.GetRepositoryForEntity<SyAcademicCalendars>();


        /// <summary>
        /// The student enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The IsClockHour action allows you to validate whether the selected enrollment is of program type clock hour or credit hour.
        /// </summary>
        /// <remarks>
        /// The IsClockHour action requires studentEnrollmentId to check whether it is of program type clock hour or credit hour.
        /// </remarks>
        /// <param name="studentEnrollmentId">
        /// The studentEnrollmentId is of type Guid is the selected enrollment id of the specified student.
        /// </param>
        /// <returns>
        /// Returns true if the given enrollment is of program type clock hour else returns false if it is of program type credit hour. 
        /// </returns>
        public async Task<bool> IsClockHour(Guid studentEnrollmentId)
        {
            return await Task.Run(
                       () => SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:AcademicCalendar:{studentEnrollmentId}",
                           () => this.mapper.Map<bool>(this.ValidateAcademicCalendarByEnrollmentId(studentEnrollmentId)),
                           CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The GetAllAcademicCalendars action allows you to get all the academic calendar programs.
        /// </summary>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the academic caledar program type and TValue is of type Guid and will have the academic calendar program.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, string>>> GetAllAcademicCalendars()
        {
            return await Task.Run(() =>
            {
                try
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:AcademicCalendar",
                        () => this.mapper.Map<IEnumerable<IListItem<string, string>>>(
                            this.AcademicCalendarTypeRepository
                                .Get().ToList().OrderBy(type => type.Acid)),
                        CacheExpiration.LONG);
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<IListItem<string, string>>();
                }
            });
        }

        /// <summary>
        /// This method validates the selected enrollment whether it is of program type clock hour or program hour.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// Returns true if the given enrollment is of program type clock hour else returns false if it is of program type credit hour. 
        /// </returns>
        private bool ValidateAcademicCalendarByEnrollmentId(Guid studentEnrollmentId)
        {
            var academicCalendarIds = this.StudentEnrollmentRepository.Get(stud => stud.StuEnrollId == studentEnrollmentId)
                .Select(programVersion => programVersion.PrgVer.Prog.Acid).ToList();

            var clockHour =
                new List<int>
                    {
                        (int)Constants.AcademicCalendars.ClockHour
                    };

            var result = academicCalendarIds.Any(x => x.HasValue && clockHour.Contains(x.Value));

            return result;
        }
    }
}