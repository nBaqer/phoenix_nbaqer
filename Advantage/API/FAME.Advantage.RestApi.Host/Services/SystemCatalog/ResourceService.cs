﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ResourceService.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The resource service .
    /// </summary>
    public class ResourceService : IResourceService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleService"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public ResourceService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The system resource repository.
        /// </summary>
        private IRepository<SyResources> ResourceRepository => this.context.GetRepositoryForEntity<SyResources>();

        /// <summary>
        /// The get resource with optional resource type parameter.
        /// </summary>
        /// <param name="resourceTypeId">
        /// The optional resource Type Id.
        /// </param>
        /// <remarks>
        /// The get resource action returns a list of resources which can be filtered by the resource type.
        /// </remarks>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string,int}}"/>  
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetResources(int? resourceTypeId)
        {
            return await Task.Run(() =>
            {
                try
                {
                    return SimpleCache.GetOrAddExisting(
                         $"{this.CacheKeyFirstSegment}:SyResources:{resourceTypeId}",
                         () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                             this.ResourceRepository
                                 .Get(r => !resourceTypeId.HasValue || r.ResourceTypeId == resourceTypeId).ToList().OrderBy(res => res.Resource)),
                         CacheExpiration.LONG);

                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<IListItem<string, int>>();
                }

            });
        }
    }
}
