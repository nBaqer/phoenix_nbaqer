﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ModuleService.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The module service .
    /// </summary>
    public class ModuleService : IModuleService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleService"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public ModuleService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The system module repository.
        /// </summary>
        private IRepository<SyModules> ModuleRepository => this.context.GetRepositoryForEntity<SyModules>();

        /// <summary>
        /// The get modules.
        /// </summary>
        /// <remarks>
        /// The get modules action returns a list of system modules.
        /// </remarks>
        /// <response code="200">Returns the list of modules found</response>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetModules()
        {
            return await Task.Run(() =>
            {
                try
                {
                    return SimpleCache.GetOrAddExisting(
                         $"{this.CacheKeyFirstSegment}:SyModules",
                         () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                             this.ModuleRepository
                                 .Get().ToList().OrderBy(type => type.ModuleName)),
                         CacheExpiration.LONG);

                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<IListItem<string, int>>();
                }

            });
        }
    }
}
