﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccreditingAgencyService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the AccreditingAgencyService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The accrediting agency service.
    /// </summary>
    public class AccreditingAgencyService : IAccreditingAgencyService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext db;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccreditingAgencyService"/> class.
        /// </summary>
        /// <param name="db">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">
        ///  The status service
        /// </param>
        public AccreditingAgencyService(IAdvantageDbContext db, IStatusesService statusService)
        {
            this.db = db;
            this.statusService = statusService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.db.GetSimpleCacheConnectionString();

        /// <summary>
        /// The State Board Agency repository.
        /// </summary>
        private IRepository<SyStateBoardAgencies> StateBoardAgenciesRepository => this.db.GetRepositoryForEntity<SyStateBoardAgencies>();

        /// <summary>
        /// The accrediting agency repository.
        /// </summary>
        private IRepository<SyAccreditingAgencies> AccreditingAgencyRepository => this.db.GetRepositoryForEntity<SyAccreditingAgencies>();

        /// <summary>
        /// Returns list of accrediting agencies
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult{List{AccreditingAgency}}"/>.
        /// </returns>
        public async Task<ActionResult<List<AccreditingAgency>>> GetAccreditingAgencies()
        {
            var rv = new ActionResult<List<AccreditingAgency>>();

            try
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();

                return await SimpleCache.GetOrAddExisting(
                               $"{this.CacheKeyFirstSegment}:GetAccreditingAgenciesByCampusId",
                                async () =>
                                {
                                    var accreditingAgencies = this.AccreditingAgencyRepository.Get();
                                    rv.Result = await accreditingAgencies.Where(a => a.StatusId == activeStatusId)
                                    .Select(a => new AccreditingAgency()
                                    {
                                        AccreditingAgencyId = a.AccreditingAgencyId,
                                        Code = a.Code,
                                        Description = a.Description
                                    }).ToListAsync();

                                    rv.ResultStatus = Enums.ResultStatus.Success;
                                    return rv;
                                },
                               CacheExpiration.VERY_LONG);
            }
            catch (Exception e)
            {
                e.TrackException();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ApiMsgs.UNABLE_TO_RETRIEVE_ACCREDITING_AGENCIES;
                return rv;
            }
        }

    }
}
