﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the catalog service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The catalog service.
    /// </summary>
    public class CatalogService : ICatalogService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The phone type service.
        /// </summary>
        private readonly IPhoneTypeService phoneTypeService;

        /// <summary>
        /// The address type service.
        /// </summary>
        private readonly IAddressTypeService addressTypeService;

        /// <summary>
        /// The countries type service.
        /// </summary>
        private readonly ICountriesTypeService countriesTypeService;

        /// <summary>
        /// The email type service.
        /// </summary>
        private readonly IEmailTypeService emailTypeService;

        /// <summary>
        /// The states service.
        /// </summary>
        private readonly IStatesService statesService;

        /// <summary>
        /// The interest areas service.
        /// </summary>
        private readonly IInterestAreasService interestAreasService;

        /// <summary>
        /// The gender service.
        /// </summary>
        private readonly IGendersService gendersService;

        /// <summary>
        /// The citizen ship service.
        /// </summary>
        private readonly ICitizenShipService citizenShipService;

        /// <summary>
        /// The source category service.
        /// </summary>
        private readonly ISourceCategoryService sourceCategoryService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The User service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The status code service.
        /// </summary>
        private readonly IStatusCodesService statusCodeService;

        /// <summary>
        /// The marital status service.
        /// </summary>
        private readonly IMaritalStatusService maritalStatusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CatalogService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="phoneTypeService">
        /// The phone type service.
        /// </param>
        /// <param name="addressTypeService">
        /// The address type service.
        /// </param>
        /// <param name="countriesTypeService">
        /// the countries type service.
        /// </param>
        /// <param name="emailTypeService">
        /// the email type service.
        /// </param>
        /// <param name="statesService">
        /// the states service.
        /// </param>
        /// <param name="interestAreasService">
        /// the interest area service.
        /// </param>
        /// <param name="sourceCategoryService">
        /// the source category service.
        /// </param>
        /// <param name="statusService">
        /// the status service.
        /// </param>
        /// <param name="userService">
        /// the user service
        /// </param>
        /// <param name="statusCodeService">
        /// the status code service
        /// </param>
        /// <param name="gendersService">
        /// The gender Service.
        /// </param>
        /// <param name="maritalStatusService">
        /// The marital Status Service.
        /// </param>
        /// <param name="citizenShipService">
        /// The citizen Ship Service.
        /// </param>
        /// <param name="context">
        /// the database context.
        /// The context.
        /// </param>
        public CatalogService(
            IMapper mapper,
            IPhoneTypeService phoneTypeService,
            IAddressTypeService addressTypeService,
            ICountriesTypeService countriesTypeService,
            IEmailTypeService emailTypeService,
            IStatesService statesService,
            IInterestAreasService interestAreasService,
            ISourceCategoryService sourceCategoryService,
            IStatusesService statusService,
            IUserService userService,
            IStatusCodesService statusCodeService,
            IGendersService gendersService,
            IMaritalStatusService maritalStatusService,
            ICitizenShipService citizenShipService,
            IAdvantageDbContext context)
        {
            this.mapper = mapper;
            this.phoneTypeService = phoneTypeService;
            this.addressTypeService = addressTypeService;
            this.countriesTypeService = countriesTypeService;
            this.emailTypeService = emailTypeService;
            this.statesService = statesService;
            this.interestAreasService = interestAreasService;
            this.sourceCategoryService = sourceCategoryService;
            this.statusService = statusService;
            this.userService = userService;
            this.statusCodeService = statusCodeService;
            this.gendersService = gendersService;
            this.maritalStatusService = maritalStatusService;
            this.citizenShipService = citizenShipService;
            this.context = context;
        }

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.context.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The get catalogs.
        /// </summary>
        /// <returns>
        /// The <see>
        ///         <cref>IList</cref>
        ///     </see>
        ///     .
        /// </returns>
        public async Task<IList<Catalog>> GetCatalog()
        {
            var catalogs = new List<Catalog>();

            var campuses = this.mapper.Map<IList<IListItem<string, Guid>>>(this.CampusRepository.Get(c => c.StatusId == this.ActiveId).ToList());

            foreach (var campus in campuses)
            {
                var campusId = campus.Value;
                var campusName = campus.Text;

                var catalog = new Catalog
                {
                    Id = campusId,
                    Name = campusName,
                    PhoneTypes = await Task.Run(() => this.phoneTypeService.GetByCampus(campusId)),
                    AddressTypes = await Task.Run(() => this.addressTypeService.GetByCampus(campusId)),
                    CountriesTypes = await Task.Run(() => this.countriesTypeService.GetByCampus(campusId)),
                    States = await Task.Run(() => this.statesService.GetByCampus(campusId)),
                    InterestAreas = await Task.Run(() => this.interestAreasService.GetByCampus(campusId)),
                    SourceCategories = await Task.Run(() => this.sourceCategoryService.GetByCampus(campusId)),
                    EmailTypes = await Task.Run(() => this.emailTypeService.Get()),
                    AdmissionsReps = await Task.Run(() => this.userService.GetAdminRepsByCampus(campusId)),
                    LeadStatuses = await Task.Run(() => this.statusCodeService.GetActiveNotEnrollLeadStatusByCampus(campusId)),
                    Genders = await Task.Run(() => this.gendersService.GetByCampus(campusId)),
                    MaritalStatus = await Task.Run(() => this.maritalStatusService.GetByCampus(campusId)),
                    CitizenShip = await Task.Run(() => this.citizenShipService.GetByCampus(campusId)),
                };

                catalogs.Add(catalog);
            }
           
            return catalogs;
        }
    }
}
