﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HolidayService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the HolidayService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.EntityFrameworkCore; 

    /// <summary>
    /// The holiday service.
    /// </summary>
    public class HolidayService : IHolidayService
    {
        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The campus group campus service.
        /// </summary>
        private readonly ICampusGroupCampusService campusGroupCampusService;

        /// <summary>
        /// The time interval service.
        /// </summary>
        private readonly ITimeIntervalService timeIntervalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HolidayService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="campusGroupCampusService">
        /// The campus Group Campus Service.
        /// </param>
        /// <param name="timeIntervalService">
        /// The time Interval Service.
        /// </param>
        public HolidayService(IAdvantageDbContext advantageDbContext, IMapper mapper, ICampusGroupCampusService campusGroupCampusService, ITimeIntervalService timeIntervalService)
        {
            this.advantageDbContext = advantageDbContext;
            this.mapper = mapper;
            this.campusGroupCampusService = campusGroupCampusService;
            this.timeIntervalService = timeIntervalService;
        }

        /// <summary>
        /// The holidays repository.
        /// </summary>
        private IRepository<SyHolidays> HolidaysRepository => this.advantageDbContext.GetRepositoryForEntity<SyHolidays>();
         
        /// <summary>
        /// The get holiday list.
        /// </summary>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task"/>.
        /// </returns>
        public async Task<IEnumerable<Holiday>> GetHolidayListByCampus(Guid campusId)
        {
            return await Task.Run(
                       async () =>
                           {
                               var holidayList = new List<Holiday>();
                               var campusGroups = await this.campusGroupCampusService.GetCampusGroupsByCampusId(campusId);
                               var syHolidays = this.HolidaysRepository.Get(x => campusGroups.Any(cmp => cmp.CampGrpId == x.CampGrpId))
                                   .Include(x => x.Status)
                                   .Where(x => string.Equals(x.Status.StatusCode, Statuses.Active, StringComparison.CurrentCultureIgnoreCase));
                              
                               var holidays  = this.mapper.Map<IEnumerable<Holiday>>(syHolidays).ToList(); 
                               var timeIntervalList = await this.timeIntervalService.GetAllTimeIntervals();
                               timeIntervalList = timeIntervalList.ToList();
                              
                               if (timeIntervalList.Any())
                               {
                                   holidays.ForEach(
                                       holiday =>
                                           {
                                               if (holiday.AllDay == false)
                                               { 
                                                   if (holiday.StartTimeId != null && holiday.EndTimeId != null)
                                                   {
                                                       var startTimeInterval = timeIntervalList.FirstOrDefault(x => x.Id == holiday.StartTimeId);
                                                       var endTimeInterval = timeIntervalList.FirstOrDefault(x => x.Id == holiday.EndTimeId);
                                                       var startTime = startTimeInterval?.Datetime ?? DateTime.MinValue;
                                                       if (startTime != DateTime.MinValue)
                                                       {
                                                           holiday.StartTime = new TimeSpan(startTime.Hour, startTime.Minute, startTime.Second);
                                                       }

                                                       var endTime = endTimeInterval?.Datetime ?? DateTime.MinValue;
                                                       if (endTime != DateTime.MinValue)
                                                       {
                                                           holiday.EndTime = new TimeSpan(endTime.Hour, endTime.Minute, endTime.Second);
                                                       }
                                                   }

                                                   for (var startDate = holiday.StartDate.Date; startDate <= holiday.EndDate;)
                                                   {
                                                       var newHoliday = new Holiday()
                                                                            {
                                                                                StartDate = holiday.StartDate,
                                                                                EndDate = holiday.EndDate,
                                                                                AllDay = holiday.AllDay,
                                                                                Code = holiday.Code,
                                                                                Description = holiday.Description,
                                                                                EndTimeId = holiday.EndTimeId,
                                                                                Id = holiday.Id,
                                                                                ModifiedDate = holiday.ModifiedDate,
                                                                                ModifiedUser = holiday.ModifiedUser,
                                                                                EndTime = holiday.EndTime,
                                                                                StartTime = holiday.StartTime,
                                                                                StartTimeId = holiday.StartTimeId,
                                                                                StatusId = holiday.StatusId
                                                                            };

                                                       var startTime = holiday.StartTime?.TotalHours ?? 0.0;
                                                       var endTime = holiday.EndTime?.TotalHours ?? 0.0;

                                                       newHoliday.StartDate = startDate.AddHours(startTime);
                                                       newHoliday.EndDate = startDate.AddHours(endTime);
                                                       holidayList.Add(newHoliday);
                                                       startDate = startDate.AddDays(1);
                                                   }
                                               }
                                               else
                                               {
                                                   holidayList.Add(holiday);
                                               } 
                                           });
                               }

                               return holidayList;
                           });
        }
    }
}
