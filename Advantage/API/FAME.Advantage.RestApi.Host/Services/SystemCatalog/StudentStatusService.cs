﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusService.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The student status service.
    /// </summary>
    public class StudentStatusService : IStudentStatusService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;
     

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentStatusService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        public StudentStatusService(IMapper mapper, IAdvantageDbContext advantageContext)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;           
        }
        
        /// <summary>
        /// The sy student status changes repository.
        /// </summary>
        private IRepository<SyStudentStatusChanges> SyStudentStatusChangesRepository => this.advantageContext.GetRepositoryForEntity<SyStudentStatusChanges>();


        /// <summary>
        /// The get student status by enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param> 
        /// <returns>
        /// The <see cref="SyStudentStatusChanges"/>.
        /// </returns>
        public async Task<SyStudentStatusChanges> GetStudentStatus(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var studentStatusOriginal = this.SyStudentStatusChangesRepository
                                   .Get(se => se.StuEnrollId == enrollmentId).OrderByDescending(x => x.DateOfChange).FirstOrDefault();
                               return studentStatusOriginal;
                           });
        }

        /// <summary>
        /// Delete student status by enrollmentId from syStudentStatusChanges table.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<bool> Delete(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               try
                               {
                                   var studentStatus = this.SyStudentStatusChangesRepository
                                       .Get(se => se.StuEnrollId == enrollmentId).OrderByDescending(x => x.ModDate)
                                       .FirstOrDefault();
                                   this.SyStudentStatusChangesRepository.Delete(studentStatus);

                                   return true;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   return false;
                               }
                           });
        }
    }
}
