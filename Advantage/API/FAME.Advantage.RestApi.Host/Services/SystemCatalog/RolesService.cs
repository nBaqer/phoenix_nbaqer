﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesService.cs" company="FAME">
//   Fame Inc, 2018
// </copyright>
// <summary>
//   Defines the RolesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Extensions.Helpers;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The roles service.
    /// </summary>
    public class RolesService : IRolesService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private readonly string cacheKeyFirstSegment;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="RolesService"/> class. 
        /// Initializes a new instance of the <see cref="LeadService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="statusCodesService">
        /// The status Codes Service.
        /// </param>
        public RolesService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService, IStatusCodesService statusCodesService)
        {
            this.unitOfWork = unitOfWork;
            this.cacheKeyFirstSegment = unitOfWork.GetSimpleCacheConnectionString();
            this.mapper = mapper;
            this.statusService = statusService;
        }


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();


        /// <summary>
        /// The roles repository.
        /// </summary>
        private IRepository<SyRoles> RolesRepository => this.unitOfWork.GetRepositoryForEntity<SyRoles>();

        /// <summary>
        /// The roles repository.
        /// </summary>
        private IRepository<SyUsersRolesCampGrps> UserRolesCampusGroupsRepository => this.unitOfWork.GetRepositoryForEntity<SyUsersRolesCampGrps>();

        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllRoles()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:AllRoles",
                                   () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                       this.RolesRepository.Get(c => c.StatusId == activeStatusId)),
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The get roles by user type.
        /// </summary>
        /// <param name="userTypeId">
        /// The user Type Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetRolesByUserType(int? userTypeId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            Expression<Func<SyRoles, bool>> query = c => c.StatusId == activeStatusId;

            if (userTypeId.HasValue)
            {
                query = query.And(c => c.SysRole.RoleTypeId == userTypeId);
            }
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:RolesByUserType:{userTypeId}",
                                   () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                       this.RolesRepository.Get(query).OrderBy(r => r.Role)),
                                   CacheExpiration.VERY_LONG);
                           });
        }

        /// <summary>
        /// The create user roles.
        /// </summary>
        /// <param name="roles">
        /// The roles.
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<UserRole>> CreateUserRoles(IEnumerable<UserRole> roles, Guid userId)
        {
            try
            {
                var modUser = this.unitOfWork.GetTenantUser();

                List<SyUsersRolesCampGrps> rolesToInsert = new List<SyUsersRolesCampGrps>();

                foreach (var r in roles)
                {
                    var userRole = new SyUsersRolesCampGrps();
                    userRole.CampGrpId = r.CampusGroupId;
                    userRole.UserRoleCampGrpId = Guid.NewGuid();
                    userRole.UserId = userId;
                    userRole.RoleId = r.RoleId;
                    userRole.ModUser = modUser.Email;
                    userRole.ModDate = DateTime.Now;
                    rolesToInsert.Add(userRole);
                }

                this.UserRolesCampusGroupsRepository.Create(rolesToInsert);
                await this.UserRolesCampusGroupsRepository.SaveAsync();

                var insertedRoles = new List<UserRole>();

                foreach (var r in rolesToInsert)
                {
                    var insertedRole = this.mapper.Map<UserRole>(r);
                    insertedRoles.Add(insertedRole);
                }

                return insertedRoles;
            }
            catch (Exception e)
            {
                e.TrackException();
                return new List<UserRole>();
            }

        }

        /// <summary>
        /// The get roles for user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<UserRole>> GetRolesForUser(Guid userId)
        {
            return await Task.Run(
                       () =>
                           {
                               var userRoles = new List<UserRole>();
                               var roles = this.UserRolesCampusGroupsRepository.Get(role => role.UserId == userId).Include(x => x.Role)
                                               .ToList();

                               foreach (var r in roles)
                               {
                                   var role = this.mapper.Map<UserRole>(r);
                                   userRoles.Add(role);
                               }

                               return userRoles;
                           });
        }
    }
}
