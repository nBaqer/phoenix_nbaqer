﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountriesService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the countries service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The countries service.
    /// </summary>
    public class CountriesService : ICountriesService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;


        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CountriesService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">
        /// The countries service.
        /// </param>
        public CountriesService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }

        /// <summary>
        /// The states repository.
        /// </summary>
        private IRepository<AdCountries> CountriesRepository => this.advantageContext.GetRepositoryForEntity<AdCountries>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampus(Guid campusId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:AdCountriesByCampus:{campusId}",
                       () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.CountriesRepository.Get(pt => pt.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && pt.StatusId == this.ActiveId)).ToList(),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByCampusGroup(Guid campusGroupId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:AdCountriesByCampusGroup:{campusGroupId}",
                       () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.CountriesRepository.Get(pt => pt.CampGrpId == campusGroupId && pt.StatusId == this.ActiveId)).ToList(),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetById<TEntId>(TEntId entityId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:AdCountriesById:{entityId}",
                       () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.CountriesRepository.GetById(entityId)),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get all countries.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetAllCountries()
        {
            return await Task.Run(() =>
                {
                    var stateBoardStates = this.CountriesRepository
                        .Get(s => s.CampGrp.IsAllCampusGrp.HasValue && s.CampGrp.IsAllCampusGrp.HasValue && s.CountryCode.ToLower() == "usa").OrderBy(s => s.CountryCode).Select(
                            s => new ListItem<string, Guid>() { Value = s.CountryId, Text = s.CountryCode }).ToList();

                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:AdCountriesAll",
                        () => stateBoardStates,
                        CacheExpiration.VERY_LONG);
                });
        }
    }
}
