﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardAgencyService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardAgencyService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The state board agency service.
    /// </summary>
    public class StateBoardAgencyService : IStateBoardAgencyService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardAgencyService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        ///     The unit of work.
        /// </param>
        /// <param name="mapper">
        ///     The mapper.
        /// </param>
        /// <param name="statusService"></param>
        public StateBoardAgencyService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The State Board Agency repository.
        /// </summary>
        private IRepository<SyStateBoardAgencies> StateBoardAgenciesRepository => this.unitOfWork.GetRepositoryForEntity<SyStateBoardAgencies>();


        /// <summary>
        /// The get all state board agencies.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetAllStateBoardAgencies()
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                       () =>
                       {
                           return SimpleCache.GetOrAddExisting(
                               $"{this.CacheKeyFirstSegment}:AllStateBoardAgencies",
                               () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                                   this.StateBoardAgenciesRepository.Get(sba => sba.StatusId == activeStatusId)),
                               CacheExpiration.VERY_LONG);
                       });
        }


        /// <summary>
        /// The get state board agencies by state board report id.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetStateBoardAgenciesByStateId(Guid stateId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:StateBoardAgencies:{stateId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                            this.StateBoardAgenciesRepository.Get(sba => sba.StatusId == activeStatusId && sba.StateId == stateId)),
                        CacheExpiration.VERY_LONG);
                });
        }

        /// <summary>
        /// The get state board agency.
        /// </summary>
        /// <param name="agencyId">
        /// The agency id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<StateBoardAgency> GetStateBoardAgency(int? agencyId)
        {
            return await Task.Run(
                       () =>
                           {
                               agencyId = agencyId ?? 0;
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:StateBoardAgenciesById:{agencyId}",
                                   () =>
                                       {
                                           var agency = this.StateBoardAgenciesRepository
                                               .Get(sba => agencyId.HasValue && sba.StateBoardAgencyId == agencyId.Value)
                                               .Select(sba => new StateBoardAgency
                                               {
                                                   CountryId = sba.LicensingCountryId,
                                                   Address1 = sba.LicensingAddress1,
                                                   StateId = sba.LicensingStateId,
                                                   City = sba.LicensingCity,
                                                   Address2 = sba.LicensingAddress2,
                                                   ZipCode = sba.LicensingZipCode
                                               }).FirstOrDefault();
                                           return agency;
                                       },
                                   CacheExpiration.VERY_LONG);
                           });
        }
    }
}
