﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GraduationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The graduation service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog.StatusChanges
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Advantage.StoreProcedureEntities;
    using Fame.EFCore.Extensions;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Controllers.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The graduation service.
    /// </summary>
    public class GraduationService : IGraduationService
    {
        /// <summary>
        /// The advantage unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageUnitOfWork;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraduationService"/> class.
        /// </summary>
        /// <param name="advantageUnitOfWork">
        /// The advantage unit of work.
        /// </param>
        /// <param name="appSettingsService">
        /// The app Settings Service.
        /// </param>
        public GraduationService(IAdvantageDbContext advantageUnitOfWork, ISystemConfigurationAppSettingService appSettingsService)
        {
            this.advantageUnitOfWork = advantageUnitOfWork;
            this.appSettingsService = appSettingsService;
        }

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The grade book conversion result repository.
        /// </summary>
        private IRepository<ArGrdBkConversionResults> GradeBookConversionResultRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdBkConversionResults>();

        /// <summary>
        /// The grade book component types repository.
        /// </summary>
        private IRepository<ArGrdComponentTypes> GradeBookComponentTypesRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdComponentTypes>();

        /// <summary>
        /// The resources repository.
        /// </summary>
        private IRepository<SyResources> ResourcesRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SyResources>();

        /// <summary>
        /// The grade book results repository.
        /// </summary>
        private IRepository<ArGrdBkResults> GradeBookResultsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdBkResults>();

        /// <summary>
        /// The class section repository.
        /// </summary>
        private IRepository<ArClassSections> ClassSectionRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArClassSections>();

        /// <summary>
        /// The grade book weight details repository.
        /// </summary>
        private IRepository<ArGrdBkWgtDetails> GradeBookWeightDetailsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdBkWgtDetails>();

        /// <summary>
        /// The grade scale details repository.
        /// </summary>
        private IRepository<ArGradeScaleDetails> GradeScaleDetailsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGradeScaleDetails>();

        /// <summary>
        /// The student status change repository.
        /// </summary>
        private IRepository<SyStudentStatusChanges> StudentStatusChangeRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SyStudentStatusChanges>();

        /// <summary>
        /// The status changes workflow repository.
        /// </summary>
        private IRepository<SySystemStatusWorkflowRules> StatusChangesWorkflowRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SySystemStatusWorkflowRules>();

        /// <summary>
        /// The can change.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> CanChange(Guid campusId, Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
            {
                var enrollment = this.EnrollmentRepository.Get(x => x.StuEnrollId == enrollmentId && x.CampusId == campusId)
                    .Include(x => x.SyStudentStatusChanges).ThenInclude(x => x.NewStatus).Include(x => x.SaTransactions).FirstOrDefault();

                var currentStatus = enrollment?.SyStudentStatusChanges.OrderByDescending(x => x.DateOfChange).ThenByDescending(x=>x.ModDate)
                    .FirstOrDefault();

                ActionResult<bool> actionResult = new ActionResult<bool>();
                actionResult.ResultStatusMessage = string.Empty;
                actionResult.ResultStatus = Enums.ResultStatus.Success;
                actionResult.Result = false;

                if (currentStatus != null)
                {
                    var statusAllowed = this.StatusChangesWorkflowRepository.Get(
                        x => x.StatusIdFrom == currentStatus.NewStatus.SysStatusId
                             && x.StatusIdTo == (int)StudentStatuses.Graduated).FirstOrDefault();
                    if (statusAllowed != null)
                    {
                        actionResult.Result = statusAllowed.AllowInsert || statusAllowed.AllowedInBatch;

                        if (statusAllowed.AllowInsert || statusAllowed.AllowedInBatch)
                        {
                            actionResult = await this.HaveMetGraduationRequirements(enrollment);
                            return actionResult;
                        }

                    }
                }
                else
                {
                    actionResult.ResultStatusMessage = "Could not find status history ";

                }

                return actionResult;
            });
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> ChangeStatus(Guid campusId, Guid enrollmentId, Guid statusCodeId)
        {
            return await Task.Run(() =>
            {
                ActionResult<bool> actionResult = new ActionResult<bool>();
                actionResult.ResultStatusMessage = string.Empty;
                actionResult.ResultStatus = Enums.ResultStatus.Success;
                actionResult.Result = true;

                var enrollment = this.EnrollmentRepository.Get(x => x.StuEnrollId == enrollmentId && x.CampusId == campusId).FirstOrDefault();
                if (enrollment != null)
                {
                    enrollment.StatusCodeId = statusCodeId;
                    enrollment.ModDate = DateTime.Now;
                    enrollment.ModUser = this.advantageUnitOfWork.GetTenantUser().Email;

                    var currentStatus = enrollment?.SyStudentStatusChanges.OrderByDescending(x => x.DateOfChange)
                        .FirstOrDefault();

                    SyStudentStatusChanges statusChange = new SyStudentStatusChanges();
                    statusChange.StuEnrollId = enrollmentId;
                    statusChange.NewStatusId = statusCodeId;
                    if (currentStatus != null)
                    {
                        statusChange.OrigStatusId = currentStatus.NewStatusId;
                    }

                    statusChange.CampusId = campusId;
                    statusChange.DateOfChange = DateTime.Now;
                    statusChange.ModUser = this.advantageUnitOfWork.GetTenantUser().Email;
                    statusChange.ModDate = DateTime.Now;
                    this.StudentStatusChangeRepository.Create(statusChange);
                    this.EnrollmentRepository.Update(enrollment);
                }
                else
                {
                    actionResult.ResultStatusMessage = "Enrollment " + ApiMsgs.NOT_FOUND;
                    actionResult.ResultStatus = Enums.ResultStatus.Warning;
                    actionResult.Result = false;
                }

                return actionResult;
            });
        }

        /// <summary>
        /// The have met graduation requirements.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ActionResult<bool>> HaveMetGraduationRequirements(ArStuEnrollments enrollment)
        {
            return await Task.Run(
                 () =>
                {
                    ActionResult<bool> actionResult = new ActionResult<bool>();
                    actionResult.ResultStatusMessage = string.Empty;

                    var minGradScaleValue = this.GradeScaleDetailsRepository.Get(x => x.GrdSysDetail.IsPass == true)
                        .Min(x => x.MinVal);

                    var grabookWorkConversionUnitResults = from gradebookConversionResult in this.GradeBookConversionResultRepository.Get()
                                                           join componentTypes in this.GradeBookComponentTypesRepository.Get() on gradebookConversionResult.GrdComponentTypeId equals componentTypes.GrdComponentTypeId
                                                           join resources in this.ResourcesRepository.Get() on componentTypes.SysComponentTypeId equals resources.ResourceId
                                                           where gradebookConversionResult.StuEnrollId == enrollment.StuEnrollId
                                                           select new GradeBookWorkUnitResult()
                                                           {
                                                               ResultId = gradebookConversionResult.ConversionResultId,
                                                               CourseId = gradebookConversionResult.ReqId,
                                                               Description = gradebookConversionResult.ResNum == 0 ? string.Empty : gradebookConversionResult.ResNum.ToString(),
                                                               Score = gradebookConversionResult.Score,
                                                               MinResult = gradebookConversionResult.MinResult,
                                                               Required = gradebookConversionResult.Required,
                                                               MustPass = gradebookConversionResult.MustPass,
                                                               Remaining = componentTypes.SysComponentTypeId == 500 || componentTypes.SysComponentTypeId == 503 || componentTypes.SysComponentTypeId == 544 ?
                                                                                gradebookConversionResult.Score != null
                                                                                    ? gradebookConversionResult.Score > gradebookConversionResult.MinResult
                                                                                          ? null
                                                                                          : gradebookConversionResult.Score == gradebookConversionResult.MinResult ? 0
                                                                                    : gradebookConversionResult.MinResult > gradebookConversionResult.Score ? gradebookConversionResult.MinResult - gradebookConversionResult.Score
                                                                                 : null : null
                                                                           : null,
                                                               WorkUnitType = componentTypes.SysComponentTypeId,
                                                               WorkUnitDescription = resources.Resource
                                                           };
                    var grabookWorkUnitResults = from grabookResults in this.GradeBookResultsRepository.Get()
                                                 join studentEnrollment in this.EnrollmentRepository.Get() on grabookResults.StuEnrollId equals studentEnrollment.StuEnrollId
                                                 join gradebookWeightDetails in this.GradeBookWeightDetailsRepository.Get() on grabookResults.InstrGrdBkWgtDetailId equals gradebookWeightDetails.InstrGrdBkWgtDetailId
                                                 join componentTypes in this.GradeBookComponentTypesRepository.Get() on gradebookWeightDetails.GrdComponentTypeId equals componentTypes.GrdComponentTypeId
                                                 join classSections in this.ClassSectionRepository.Get() on grabookResults.ClsSectionId equals classSections.ClsSectionId
                                                 join resources in this.ResourcesRepository.Get() on componentTypes.SysComponentTypeId equals resources.ResourceId
                                                 let totalExternshipHours = studentEnrollment.ArExternshipAttendance.Sum(x => x.HoursAttended)
                                                 where grabookResults.StuEnrollId == enrollment.StuEnrollId
                                                 select new GradeBookWorkUnitResult()
                                                 {
                                                     ResultId = grabookResults.GrdBkResultId,
                                                     CourseId = classSections.ReqId,
                                                     Description = grabookResults.ResNum == 0 ? string.Empty : grabookResults.ResNum.ToString(),
                                                     Score = componentTypes.SysComponentTypeId == 544 ? totalExternshipHours : grabookResults.Score,
                                                     MinResult = componentTypes.SysComponentTypeId == 500 || componentTypes.SysComponentTypeId == 503 || componentTypes.SysComponentTypeId == 544 ? gradebookWeightDetails.Number : minGradScaleValue,
                                                     Required = gradebookWeightDetails.Required,
                                                     MustPass = gradebookWeightDetails.MustPass,
                                                     Remaining = this.GetWorkUnitRemaining(componentTypes.SysComponentTypeId, componentTypes.SysComponentTypeId == 544 ? totalExternshipHours : grabookResults.Score, componentTypes.SysComponentTypeId == 500 || componentTypes.SysComponentTypeId == 503 || componentTypes.SysComponentTypeId == 544 ? gradebookWeightDetails.Number : minGradScaleValue),
                                                     WorkUnitType = componentTypes.SysComponentTypeId,
                                                     WorkUnitDescription = resources.Resource
                                                 };


                    bool areRequirementsMet = true;

                    var workUnitResults = grabookWorkConversionUnitResults.Union(grabookWorkUnitResults).ToList();

                    if (workUnitResults.Any(x => x.Required == true))
                    {
                        areRequirementsMet = workUnitResults.Count(
                            x => x.Required == true && x.Score > 0
                                                    && (x.MinResult != null && x.Score >= x.MinResult)) == workUnitResults.Count(
                                                  x => x.Required == true);
                    }

                    if (areRequirementsMet && workUnitResults.Any(x => x.MustPass == true))
                    {
                        areRequirementsMet = workUnitResults.Count(
                            x => x.MustPass == true && x.Score > 0
                                                    && (x.MinResult != null && x.Score >= x.MinResult)) == workUnitResults.Count(
                                                  x => x.MustPass == true);
                    }

                    if (!areRequirementsMet)
                    {
                        actionResult.ResultStatusMessage += "Grades required for graduation have not being satisfied.";
                    }


                    if (areRequirementsMet)
                    {
                        areRequirementsMet = !(enrollment.SaTransactions.Where(t=>t.Voided==false).Sum(x => x.TransAmount) > 0);
                    }

                    if (!areRequirementsMet)
                    {
                        actionResult.ResultStatusMessage += "Ledger Balance have an outstanding balance, unable to graduate student.";
                    }


                    if (areRequirementsMet)
                    {
                        // Check if track attendance by day (TrackSapAttendance)


                        var trackAttendance = this.appSettingsService.GetAppSettingValueByCampus(
                            SapCheckConfigurationKeys.TrackSapAttendance,
                            enrollment.CampusId);

                        if (trackAttendance.Equals(TrackSapAttendance.ByDay, StringComparison.InvariantCultureIgnoreCase))
                        {
                            object haveMetGraduationHoursRequirements = this.advantageUnitOfWork
                                .LoadStoredProc("USP_HasStudentMetGraduationHoursRequirement")
                                .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                                .ExecuteScalarResultSet();

                            areRequirementsMet = haveMetGraduationHoursRequirements != null
                                                 && haveMetGraduationHoursRequirements.ToString()
                                                     .Equals("Satisfied");

                            if (!areRequirementsMet)
                            {
                                actionResult.ResultStatusMessage += "Hours required for graduation have not been satisfied.";
                            }
                        }

                        var timeClockClassSection = this.appSettingsService.GetAppSettingValueByCampus(
                            ConfigurationAppSettingsKeys.TimeClockClassSection,
                            enrollment.CampusId);

                        if (timeClockClassSection.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                        {
                            object haveMetGraduationHoursRequirementsByClass = this.advantageUnitOfWork
                              .LoadStoredProc("USP_HasStudentMetGraduationHoursRequirementByClass")
                              .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                              .WithSqlParam("@CampusId", enrollment.CampusId.ToString(), DbType.String)
                              .ExecuteScalarResultSet();

                            areRequirementsMet = haveMetGraduationHoursRequirementsByClass != null
                                                 && !haveMetGraduationHoursRequirementsByClass.ToString()
                                                     .Equals("UnSatisfied");

                            if (!areRequirementsMet)
                            {
                                actionResult.ResultStatusMessage += "Hours requirements by class for graduation have not been satisfied.";
                            }
                        }

                    }


                    if (areRequirementsMet)
                    {
                        /*
                         * MISSING GRADUATION TEST REQUIREMENT
                         */
                        List<MissingTestRequirements> listOfMissingRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_ListMissingGradTestRequirements")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingTestRequirements>();

                        if (listOfMissingRequirements?.Count > 0)
                        {
                            areRequirementsMet = false;
                            actionResult.ResultStatusMessage += "Missing the following graduation test requirement(s): \n";
                            foreach (var missingRequirement in listOfMissingRequirements)
                            {
                                actionResult.ResultStatusMessage += missingRequirement.Descrip + ". \n";
                            }
                        }

                        /*
                         * MISSING GRADUATION DOCUMENT REQUIREMENT
                         */
                        List<MissingDocumentRequirements> listOfMissingDocumentsRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_ListMissingGradDocumentRequirements")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingDocumentRequirements>();

                        if (listOfMissingDocumentsRequirements?.Count > 0)
                        {
                            areRequirementsMet = false;
                            actionResult.ResultStatusMessage += "Missing the following graduation document requirement(s): \n";
                            foreach (var missingRequirement in listOfMissingDocumentsRequirements)
                            {
                                actionResult.ResultStatusMessage += missingRequirement.Descrip + ". \n";
                            }
                        }


                        /*
                         * MISSING GRADUATION GROUP REQUIREMENTS
                         */
                        List<MissingGroupRequirements> listOfMissingGroupRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_DoesReqGroupMeetsConditionsforGraduation")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingGroupRequirements>();

                        if (listOfMissingGroupRequirements?.Count > 0)
                        {
                            string missingReqs = string.Empty;
                            foreach (var missingRequirement in listOfMissingGroupRequirements)
                            {
                                if (missingRequirement.AttemptedReqs < missingRequirement.NumReqs)
                                {
                                    missingReqs += missingRequirement.Descrip + ". \n";
                                    areRequirementsMet = false;
                                }
                                else
                                {
                                    listOfMissingRequirements = this.advantageUnitOfWork
                                        .LoadStoredProc("USP_ListReqRequirementsExistWithinGroup")
                                        .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                                        .WithSqlParam("@ReqGrpId", missingRequirement.ReqGrpId.ToString(), DbType.String)
                                        .ExecuteReaderStoredProc<MissingTestRequirements>();

                                    if (listOfMissingRequirements?.Count > 0)
                                    {
                                        areRequirementsMet = false;

                                        foreach (var missingSubRequirement in listOfMissingRequirements)
                                        {
                                            missingReqs += missingSubRequirement.Descrip + ". \n";
                                        }
                                    }
                                }
                            }

                            if (missingReqs != string.Empty)
                            {
                                actionResult.ResultStatusMessage += "Missing the following graduation group requirement(s): \n";
                                actionResult.ResultStatusMessage += missingReqs;
                            }
                        }


                        /*
                         * MISSING ENROLLMENT TEST REQUIREMENT
                         */
                        listOfMissingRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_ListMissingEnrollmentTestRequirements")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingTestRequirements>();

                        if (listOfMissingRequirements?.Count > 0)
                        {
                            areRequirementsMet = false;
                            actionResult.ResultStatusMessage += "Missing the following enrollment test Requirement(s): \n";
                            foreach (var missingRequirement in listOfMissingRequirements)
                            {
                                actionResult.ResultStatusMessage += missingRequirement.Descrip + ". \n";
                            }
                        }


                        /*
                         * MISSING ENROLLMENT DOCUMENT REQUIREMENT
                         */
                        listOfMissingDocumentsRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_ListMissingEnrollmentDocumentRequirements")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingDocumentRequirements>();

                        if (listOfMissingDocumentsRequirements?.Count > 0)
                        {
                            areRequirementsMet = false;
                            actionResult.ResultStatusMessage += "Missing the following enrollment document requirement(s): \n";
                            foreach (var missingRequirement in listOfMissingDocumentsRequirements)
                            {
                                actionResult.ResultStatusMessage += missingRequirement.Descrip + ". \n";
                            }
                        }


                        /*
                         * MISSING ENROLLMENT GROUP REQUIREMENT
                         */
                        listOfMissingGroupRequirements = this.advantageUnitOfWork
                            .LoadStoredProc("USP_DoesReqGroupMeetsConditionsforEnrollment")
                            .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                            .ExecuteReaderStoredProc<MissingGroupRequirements>();

                        if (listOfMissingGroupRequirements?.Count > 0)
                        {
                            string missingReqs = string.Empty;
                            foreach (var missingRequirement in listOfMissingGroupRequirements)
                            {
                                if (missingRequirement.AttemptedReqs < missingRequirement.NumReqs)
                                {
                                    missingReqs += missingRequirement.Descrip + ". \n";
                                    areRequirementsMet = false;
                                }
                                else
                                {
                                    listOfMissingRequirements = this.advantageUnitOfWork
                                        .LoadStoredProc("USP_ListReqRequirementsExistWithinGroup")
                                        .WithSqlParam("@StuEnrollId", enrollment.StuEnrollId.ToString(), DbType.String)
                                        .WithSqlParam("@ReqGrpId", missingRequirement.ReqGrpId.ToString(), DbType.String)
                                        .ExecuteReaderStoredProc<MissingTestRequirements>();

                                    if (listOfMissingRequirements?.Count > 0)
                                    {
                                        areRequirementsMet = false;

                                        foreach (var missingSubRequirement in listOfMissingRequirements)
                                        {
                                            missingReqs += missingSubRequirement.Descrip + ". \n";
                                        }
                                    }
                                }
                            }

                            if (missingReqs != string.Empty)
                            {
                                actionResult.ResultStatusMessage += "Missing the following enrollment group requirement(s): \n";
                                actionResult.ResultStatusMessage += missingReqs;
                            }
                        }
                    }

                    if (!areRequirementsMet)
                    {
                        actionResult.ResultStatus = Enums.ResultStatus.Warning;
                    }
                    else
                    {
                        actionResult.ResultStatus = Enums.ResultStatus.Success;
                    }

                    actionResult.Result = areRequirementsMet;

                    return actionResult;
                });
        }

        /// <summary>
        /// The get work unit remaining.
        /// </summary>
        /// <param name="sysComponentTypeId">
        /// The sys component type id.
        /// </param>
        /// <param name="score">
        /// The score.
        /// </param>
        /// <param name="minResult">
        /// The min result.
        /// </param>
        /// <returns>
        /// The <see cref="decimal?"/>.
        /// </returns>
        private decimal? GetWorkUnitRemaining(short? sysComponentTypeId, decimal? score, decimal? minResult)
        {
            if (sysComponentTypeId != null)
            {
                if (sysComponentTypeId == 500 || sysComponentTypeId == 503 || sysComponentTypeId == 544)
                {
                    if (score != null && minResult != null)
                    {
                        if (score == minResult)
                        {
                            return 0;
                        }
                        else if (minResult > score)
                        {
                            return minResult - score;
                        }
                    }
                }
            }

            return null;
        }

    }
}





