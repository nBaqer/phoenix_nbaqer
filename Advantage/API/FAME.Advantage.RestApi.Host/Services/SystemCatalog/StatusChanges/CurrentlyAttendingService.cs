﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CurrentlyAttendingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CurrentlyAttendingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog.StatusChanges
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The currently attending service.
    /// </summary>
    public class CurrentlyAttendingService : ICurrentlyAttendingService
    {
        /// <summary>
        /// The advantage unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageUnitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentlyAttendingService"/> class.
        /// </summary>
        /// <param name="advantageUnitOfWork">
        /// The advantage unit of work.
        /// </param>
        public CurrentlyAttendingService(IAdvantageDbContext advantageUnitOfWork)
        {
            this.advantageUnitOfWork = advantageUnitOfWork;
        }

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The student status change repository.
        /// </summary>
        private IRepository<SyStudentStatusChanges> StudentStatusChangeRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SyStudentStatusChanges>();

        /// <summary>
        /// The status changes workflow repository.
        /// </summary>
        private IRepository<SySystemStatusWorkflowRules> StatusChangesWorkflowRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SySystemStatusWorkflowRules>();

        /// <summary>
        /// The can change.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> CanChange(Guid campusId, Guid enrollmentId)
        {
            return await Task.Run(() =>
            {
                var enrollment = this.EnrollmentRepository.Get(x => x.StuEnrollId == enrollmentId && x.CampusId == campusId)
                    .Include(x => x.SyStudentStatusChanges).ThenInclude(x => x.NewStatus).FirstOrDefault();

                var currentStatus = enrollment?.SyStudentStatusChanges.OrderByDescending(x => x.DateOfChange)
                    .FirstOrDefault();

                ActionResult<bool> actionResult = new ActionResult<bool>();
                actionResult.ResultStatusMessage = string.Empty;
                actionResult.ResultStatus = Enums.ResultStatus.Success;
                actionResult.Result = false;

                if (currentStatus != null)
                {
                    var statusAllowed = this.StatusChangesWorkflowRepository.Get(
                        x => x.StatusIdFrom == currentStatus.NewStatus.SysStatusId
                             && x.StatusIdTo == (int)StudentStatuses.CurrentlyAttending).FirstOrDefault();
                    if (statusAllowed != null)
                    {
                        actionResult.Result = statusAllowed.AllowInsert;
                    }
                }
                else
                {
                    actionResult.ResultStatusMessage = "Could not find status history ";

                }

                return actionResult;
            });
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="statusCodeId">
        /// The status Code Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> ChangeStatus(Guid campusId, Guid enrollmentId, Guid statusCodeId)
        {
            return await Task.Run(() =>
            {
                ActionResult<bool> actionResult = new ActionResult<bool>();
                actionResult.ResultStatusMessage = string.Empty;
                actionResult.ResultStatus = Enums.ResultStatus.Success;
                actionResult.Result = true;

                var enrollment = this.EnrollmentRepository.Get(x => x.StuEnrollId == enrollmentId && x.CampusId == campusId).FirstOrDefault();
                if (enrollment != null)
                {
                    enrollment.StatusCodeId = statusCodeId;
                    enrollment.ModDate = DateTime.Now;
                    enrollment.ModUser = this.advantageUnitOfWork.GetTenantUser().Email;

                    var currentStatus = enrollment?.SyStudentStatusChanges.OrderByDescending(x => x.DateOfChange)
                        .FirstOrDefault();

                    SyStudentStatusChanges statusChange = new SyStudentStatusChanges();
                    statusChange.StuEnrollId = enrollmentId;
                    statusChange.NewStatusId = statusCodeId;
                    if (currentStatus != null)
                    {
                        statusChange.OrigStatusId = currentStatus.NewStatusId;
                    }

                    statusChange.CampusId = campusId;
                    statusChange.DateOfChange = DateTime.Now;
                    statusChange.ModUser = this.advantageUnitOfWork.GetTenantUser().Email;
                    statusChange.ModDate = DateTime.Now;

                    this.StudentStatusChangeRepository.Create(statusChange);
                    this.EnrollmentRepository.Update(enrollment);
                }
                else
                {
                    actionResult.ResultStatusMessage = "Enrollment " + ApiMsgs.NOT_FOUND;
                    actionResult.ResultStatus = Enums.ResultStatus.Warning;
                    actionResult.Result = false;
                }

                return actionResult;
            });
        }
    }
}

