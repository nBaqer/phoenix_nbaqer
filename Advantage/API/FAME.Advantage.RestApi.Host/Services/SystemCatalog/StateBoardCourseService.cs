﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardCourseService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardCourseService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The state board course service.
    /// </summary>
    public class StateBoardCourseService : IStateBoardCourseService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardCourseService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        ///     The mapper.
        /// </param>
        /// <param name="statusService"></param>
        public StateBoardCourseService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The state board course repository.
        /// </summary>
        private IRepository<SyStateBoardCourses> StateBoardCourseRepository => this.unitOfWork.GetRepositoryForEntity<SyStateBoardCourses>();

        /// <summary>
        /// The get clock hour programs.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetByStateId(Guid stateId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();

            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:StateBoardCourseByState:{stateId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                            this.StateBoardCourseRepository.Get(sbc => sbc.StatusId == activeStatusId && sbc.StateId == stateId)),
                        CacheExpiration.VERY_LONG);
                });
        }
    }
}
