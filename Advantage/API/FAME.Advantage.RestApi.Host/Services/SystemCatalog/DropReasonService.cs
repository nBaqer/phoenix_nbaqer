﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReasonService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DropReasonService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The drop reason service .
    /// </summary>
    public class DropReasonService : IDropReasonService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext uow;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropReasonService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public DropReasonService(IAdvantageDbContext unitOfWork, IMapper mapper)
        {
            this.uow = unitOfWork;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.uow.GetSimpleCacheConnectionString();

        /// <summary>
        /// The campus group campus repository repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CampusGroupCampusRepositoryRepository => this.uow.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The naccas drop reason.
        /// </summary>
        private IRepository<SyNaccasdropReasons> NaccasDropReason => this.uow.GetRepositoryForEntity<SyNaccasdropReasons>();

        /// <summary>
        /// The get drop reason list for current user.
        /// </summary>
        /// <remarks>
        /// The drop reason action requires object with campusId .
        /// </remarks>
        /// <response code="200">Returns the list of drop reasons found</response>
        /// <param name="campusId">
        /// The student drop reason .
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetByCampus(Guid campusId)
        {
            return await Task.Run(() =>
                {
                    try
                    {
                        var getByCampusIdQuery = this.CampusGroupCampusRepositoryRepository
                            .Get(e => e.CampusId == campusId).SelectMany(_ => _.CampGrp.ArDropReasons)
                            .Where(e => e.Status.Status == "Active").Distinct();

                        var result = SimpleCache.GetOrAddExisting(
                            $"{this.CacheKeyFirstSegment}:DropReasonByCampus" + campusId.ToString(),
                            () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                getByCampusIdQuery.ToList().OrderBy(dropReason => dropReason.Descrip)),
                            CacheExpiration.LONG);

                        return result;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return new List<IListItem<string, Guid>>();
                    }
                    
                });
        }

        /// <summary>
        /// The get naccas drop reasons.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetNaccasDropReasons()
        {
            return await Task.Run(() =>
                {
                    try
                    {
                        

                        var result = SimpleCache.GetOrAddExisting(
                            $"{this.CacheKeyFirstSegment}:NaccasDropReasons",
                            () =>
                                {
                                    return this.NaccasDropReason.Get().Select(
                                        x => new ListItem<string, Guid> { Text = x.Name, Value = x.NaccasdropReasonId }).OrderBy(x=> x.Text).ToList();
                                  },
                            CacheExpiration.LONG);

                        return result;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return new List<ListItem<string, Guid>>();
                    }

                });
        }
    }
}
