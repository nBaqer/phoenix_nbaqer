﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Program;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The program service.
    /// </summary>
    public class ProgramService : IProgramService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The phone type service.
        /// </summary>
        private readonly IProgramVersionsService programVersionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        ///     The unit of work.
        /// </param>
        /// <param name="mapper">
        ///     The mapper.
        /// </param>
        /// <param name="programVersionService">
        ///     The program Version Service.
        /// </param>
        /// <param name="statusService"></param>
        public ProgramService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService,IProgramVersionsService programVersionService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
            this.mapper = mapper;
            this.programVersionService = programVersionService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The program repository.
        /// </summary>
        private IRepository<ArPrograms> ProgramRepository => this.unitOfWork.GetRepositoryForEntity<ArPrograms>();

        /// <summary>
        /// The get clock hour programs.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetClockHourProgramsByCampus(Guid campusId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();


            var clockHour =
                new List<int>
                {
                    (int)Constants.AcademicCalendars.ClockHour
                };

            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:ClockHourProgramByCampus:{campusId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                            this.ProgramRepository.Get(p => p.StatusId == activeStatusId && p.CampGrp.SyCmpGrpCmps.Any(cg => cg.CampusId == campusId) && p.Acid.HasValue && clockHour.Contains(p.Acid.Value))),
                        CacheExpiration.VERY_LONG);
                });
        }


        /// <summary>
        /// Get Program Description by Id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<ProgramDetails>> GetProgramById(string progId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<ProgramDetails>();

                           try
                           {
                               var dataItem = await this.ProgramRepository.Get()
                               .Where(a => a.ProgId.ToString() == progId)
                               .Select(a => new ProgramDetails()
                               {
                                   ProgDescrip = a.ProgDescrip
                               }).FirstOrDefaultAsync();

                               if (dataItem == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"Program with progId of {progId} not found.";
                                   return result;
                               }

                               result.Result = dataItem;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        /// <summary>
        /// Gets the list of active Program and Program versions given the CampusGrpId
        /// </summary>
        /// <param name="campGrpId"></param>
        /// <returns></returns>
        public async Task<IList<ProgramAndProgramVersionName>> GetProgramAndProgramVersionForCampGrp( Guid campGrpId)
        {
            var activeStatusId = await this.statusService.GetActiveStatusId();
            var Programs = new List<ProgramAndProgramVersionName>();
            var programList = this.ProgramRepository.Get(p => p.CampGrpId == campGrpId && p.StatusId == activeStatusId).OrderBy(p=>p.ProgDescrip);
            foreach (var prog in programList)
            {
                var progId = prog.ProgId;
                var progName = prog.ProgDescrip;
                var programItem = new ProgramAndProgramVersionName
                {
                    ProgramId = progId,
                    ProgramName = progName,
                    ProgramVersions = await Task.Run(() => this.programVersionService.GetByProgramId(progId))
                };
                Programs.Add(programItem);
            }

            return Programs;
        }
    }
}
