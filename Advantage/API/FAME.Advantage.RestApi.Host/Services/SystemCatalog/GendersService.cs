﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GendersService.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the GendersService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The genders service.
    /// </summary>
    public class GendersService : IGendersService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GendersService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The unit of work.
        /// </param>
        /// <param name="statusService">The status service.</param>
        public GendersService(IMapper mapper, IAdvantageDbContext advantageContext, IStatusesService statusService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.statusService = statusService;
        }

        /// <summary>
        /// The gender repository.
        /// </summary>
        private IRepository<AdGenders> GenderRepository => this.advantageContext.GetRepositoryForEntity<AdGenders>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;
        
        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IList</cref>
        ///     </see>
        ///     .
        /// </returns>
        public async Task<IList<Gender>> GetByCampus(Guid campusId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:GenderByCampus:{campusId}",
                       () => this.mapper.Map<IList<Gender>>(this.GenderRepository.Get(pt => pt.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && pt.StatusId == this.ActiveId).Include(x => x.AfaMapping)).ToList(),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>IList</cref>
        ///     </see>
        ///     .
        /// </returns>
        public async Task<IList<Gender>> GetByCampusGroup(Guid campusGroupId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:GenderByCampusGroup:{campusGroupId}",
                       () => this.mapper.Map<IList<Gender>>(this.GenderRepository.Get(pt => pt.CampGrpId == campusGroupId && pt.StatusId == this.ActiveId).Include(x => x.AfaMapping)).ToList(),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The type of entity id.
        /// </typeparam>
        /// <returns>
        /// The <see>
        ///         <cref>IList</cref>
        ///     </see>
        ///     .
        /// </returns>
        public async Task<IList<Gender>> GetById<TEntId>(TEntId entityId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:GenderById:{entityId}",
                       () => this.mapper.Map<IList<Gender>>(this.GenderRepository.GetById(entityId)),
                       CacheExpiration.VERY_LONG));
        }
    }
}
