﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditSummaryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CreditSummaryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <inheritdoc />
    /// <summary>
    /// The credit summary service.
    /// </summary>
    public class CreditSummaryService : ICreditSummaryService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditSummaryService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public CreditSummaryService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
         
        /// <summary>
        /// The credit summary repository.
        /// </summary>
        private IRepository<SyCreditSummary> CreditSummaryRepository => this.context.GetRepositoryForEntity<SyCreditSummary>();

        /// <summary>
        /// The get credit summary detail.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollmentId.
        /// </param>
        /// <param name="reqIds">
        /// The req Ids.
        /// </param>
        /// <param name="termIds">
        /// The term Ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<CreditSummary>> GetCreditSummaryDetail(Guid enrollmentId, IList<Guid> reqIds, IList<Guid> termIds)
        {
            return await Task.Run(
                       () =>
                           {
                               var creditSummaries = this.CreditSummaryRepository.Get(x => x.StuEnrollId == enrollmentId && (bool)x.Completed);
                               if (reqIds != null && reqIds.Any())
                               {
                                   creditSummaries = creditSummaries.Where(x => reqIds.Contains(x.ReqId ?? Guid.Empty));
                               }

                               if (termIds != null && termIds.Any())
                               {
                                   creditSummaries = creditSummaries.Where(x => termIds.Contains(x.TermId ?? Guid.Empty));
                               }

                               return this.mapper.Map<IEnumerable<CreditSummary>>(creditSummaries);
                           });
        }

        /// <summary>
        /// The get credit summary by enrollment id returns all the credit summary for given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The list of CreditSummary.
        /// </returns>
        public async Task<IEnumerable<CreditSummary>> GetCreditSummaryByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var creditSummaries = this.CreditSummaryRepository.Get(x => x.StuEnrollId == enrollmentId);  
                               return this.mapper.Map<IEnumerable<CreditSummary>>(creditSummaries);
                           });
        }
    }
}
