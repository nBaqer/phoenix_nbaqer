﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTypeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DropReasonService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The user type service .
    /// </summary>
    public class UserTypeService : IUserTypeService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserTypeService"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public UserTypeService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The user types repository.
        /// </summary>
        private IRepository<SyUserType> UserTypesRepository => this.context.GetRepositoryForEntity<SyUserType>();

        /// <summary>
        /// The get user type list for current user.
        /// </summary>
        /// <remarks>
        /// The get user types action returns all system user types .
        /// </remarks>
        /// <response code="200">Returns the list of user types found</response>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, int>>> GetUserTypes()
        {
            return await Task.Run(() =>
            {
                try
                {
                    return SimpleCache.GetOrAddExisting(
                         $"{this.CacheKeyFirstSegment}:SyUserTypes",
                         () => this.mapper.Map<IEnumerable<IListItem<string, int>>>(
                             this.UserTypesRepository
                                 .Get().ToList().OrderBy(type => type.Code)),
                         CacheExpiration.LONG);

                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<IListItem<string, int>>();
                }
            });
        }
    }
}
