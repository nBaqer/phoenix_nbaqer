﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Immutable;

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Security;

    using AutoMapper;

    using Fame.EFCore.Advantage;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;
    using FAME.Advantage.RestApi.DataTransferObjects.Templates;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;
    using FAME.Advantage.RestApi.Host.Utilities;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using FAME.Orm.AdvTenant.Domain.Entities;

    using Microsoft.EntityFrameworkCore;

    using FAME.Orm.Advantage.Domain.Common;

    /// <summary>
    /// The user service.
    /// </summary>
    public class UserService : IUserService, ISetContext
    {
        /// <summary>
        /// The salt length limit.
        /// </summary>
        private const int SaltLengthLimit = 32;

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;

        /// <summary>
        /// The aspnet user service.
        /// </summary>
        private readonly IAspNetUserService aspnetUserService;

        /// <summary>
        /// The application service.
        /// </summary>
        private readonly IAspNetApplicationService applicationService;

        /// <summary>
        /// The membership service.
        /// </summary>
        private readonly IAspNetMembershipService membershipservice;


        /// <summary>
        /// The tenat service.
        /// </summary>
        private readonly ITenantService tenantService;

        /// <summary>
        /// The tenat service.
        /// </summary>
        private readonly ITenantUserService tenantUserService;

        /// <summary>
        /// The roles service.
        /// </summary>
        private readonly IRolesService rolesService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The smtp service.
        /// </summary>
        private readonly IEmailService emailService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The view render service.
        /// </summary>
        private readonly IRazorViewRenderService viewRenderService;

        /// <summary>
        /// The auth service
        /// </summary>
        private readonly IAuthService authService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="tenantDb">
        /// The tenant Db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="aspnetUserService">
        /// The aspnet User Service.
        /// </param>
        /// <param name="applicationService">
        /// The application Service.
        /// </param>
        /// <param name="membershipservice">
        /// </param>
        /// <param name="tenantService">
        /// </param>
        /// <param name="tenantUserService">
        /// </param>
        /// <param name="rolesService">
        /// The roles Service.
        /// </param>
        /// <param name="statusService">
        /// </param>
        /// <param name="emailService">
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// </param>
        /// <param name="viewRenderService">
        /// The view Render Service.
        /// </param>
        /// <param name="authService">
        /// The auth Service.
        /// </param>
        public UserService(IAdvantageDbContext unitOfWork, ITenantDbContext tenantDb, IMapper mapper, IAspNetUserService aspnetUserService, IAspNetApplicationService applicationService, IAspNetMembershipService membershipservice, ITenantService tenantService, ITenantUserService tenantUserService, IRolesService rolesService, IStatusesService statusService, IEmailService emailService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService, IRazorViewRenderService viewRenderService, IAuthService authService)
        {
            this.unitOfWork = unitOfWork;
            this.tenantDb = tenantDb;
            this.mapper = mapper;
            this.aspnetUserService = aspnetUserService;
            this.applicationService = applicationService;
            this.membershipservice = membershipservice;
            this.tenantService = tenantService;
            this.tenantUserService = tenantUserService;
            this.rolesService = rolesService;
            this.statusService = statusService;
            this.emailService = emailService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.viewRenderService = viewRenderService;
            this.authService = authService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The user repository.
        /// </summary>
        private IRepository<SyUsers> UserRepository => this.unitOfWork.GetRepositoryForEntity<SyUsers>();

        /// <summary>
        /// The user roles campus groups repository.
        /// </summary>
        private IRepository<SyUsersRolesCampGrps> UserRolesCampusGroupsRepository => this.unitOfWork.GetRepositoryForEntity<SyUsersRolesCampGrps>();

        /// <summary>
        /// The aspnet membership repository.
        /// </summary>
        private IRepository<AspnetMembership> AspnetMembershipRepository => this.tenantDb.GetRepositoryForEntity<AspnetMembership>();

        /// <summary>
        /// The aspnet user repository.
        /// </summary>
        private IRepository<AspnetUsers> AspnetUserRepository => this.tenantDb.GetRepositoryForEntity<AspnetUsers>();

        /// <summary>
        /// The tenat user repository.
        /// </summary>
        private IRepository<TenantUsers> TenantUserRepository => this.tenantDb.GetRepositoryForEntity<TenantUsers>();

        /// <summary>
        /// The user terms of use repository.
        /// </summary>
        private IRepository<SyUserTermsOfUse> TermsOfUseRepository => this.unitOfWork.GetRepositoryForEntity<SyUserTermsOfUse>();

        private IRepository<SyRlsResLvls> resourceAccess => this.unitOfWork.GetRepositoryForEntity<SyRlsResLvls>();

        /// <summary>
        /// The Search action methods returns the list of student(s) for the provided campus and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given campus of type GUID and Search filter of type string
        /// </remarks>
        /// <param name="userSearchFilter">
        /// The student search filter object includes the campusId and a Filter string which should of minimum 3 letters of the student First name or Last Name or SSN.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of"IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// The <see cref="IEnumerable{IListItem{string,Guid}}"/>  
        /// </returns>
        public async Task<IEnumerable<IListItem<string, UserSearchOutput>>> Search(UserSearchInput userSearchFilter)
        {
            Expression<Func<SyUsers, bool>> query = s => !s.IsAdvantageSuperUser && (userSearchFilter.Filter == null || userSearchFilter.Filter == string.Empty
                                                                                          || s.FullName.Contains(userSearchFilter.Filter)
                                                                                          || s.Email.Contains(userSearchFilter.Filter)
                                                                                          || s.UserName.Contains(userSearchFilter.Filter));

            if (userSearchFilter.UserTypeId.HasValue)
            {
                query = query.And(s => s.UserTypeId == userSearchFilter.UserTypeId);
            }

            if (userSearchFilter.Status.HasValue)
            {
                query = query.And(s => s.AccountActive == userSearchFilter.Status);
            }

            return await Task.Run(
                       () =>
                           {
                               var userSearch = this.UserRepository.Get(query).Include(u => u.UserType).ToList();
                               return this.mapper.Map<IEnumerable<IListItem<string, UserSearchOutput>>>(userSearch.OrderByDescending(u => u.FullName));
                           });
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<User> Create(User model)
        {

            var result = new User();
            Guid? userId = Guid.NewGuid();
            try
            {
                IEnumerable<UserRole> rolesInserted = new List<UserRole>();
                var modUser = this.unitOfWork.GetTenantUser();
                var applicationId = this.applicationService.GetApplicationId("advantage");
                var tenantId = this.tenantService.GetTenantId(modUser.TenantName);
                bool loginExist = false;

                // make sure all roles are distinct, if not, remove duplicates
                model.UserRoles = model.UserRoles
                    .GroupBy(x => new { x.CampusGroupId, x.RoleId })
                    .Select(x => new UserRole
                    {
                        CampusGroupId = x.Key.CampusGroupId,
                        RoleId = x.Key.RoleId
                    })
                    .ToList();

                var username = model.DisplayName.IsNullOrEmpty() ? model.Email : model.DisplayName;

                if (model.UserTypeId == 1)
                {
                    // make sure username does not exist
                    if (this.CheckIfUsernameExist(username))
                    {
                        result.ResultStatus = ApiMsgs.UsernameExist;
                        result.HasPassed = false;
                        return result;
                    }
                }


                // make sure email does not exist in the syUser table, if it does, then return email exist for tenant; otherwise, continue
                if (this.CheckIfEmailExistInAdvantage(model.Email))
                {
                    result.ResultStatus = ApiMsgs.EmailExistForTenant;
                    result.HasPassed = false;
                    return result;
                }

                // make sure it does not exist on the tenant db for a given tenant
                if (this.CheckIfEmailExistForTenant(model.Email, modUser.TenantName, out loginExist))
                {
                    result.ResultStatus = ApiMsgs.EmailExistForTenant;
                    result.HasPassed = false;
                    return result;
                }

                /*Advantage DB User Setup */

                if (loginExist)
                {
                    userId = this.AspnetUserRepository.Get(u => u.UserName.ToLower() == model.Email.ToLower())
                        .FirstOrDefault()?.UserId;
                }

                // create system user
                SyUsers userEntity = this.mapper.Map<SyUsers>(model);
                userEntity.UserId = userId ?? Guid.NewGuid();
                userEntity.UserName = username;
                userEntity.AccountActive = await this.statusService.ConvertStatus(model.UserStatusId);
                userEntity.ModUser = modUser.Email;
                userEntity.ModDate = DateTime.Now;
                userEntity = await this.UserRepository.CreateAsync(userEntity);

                // load the navigation property
                this.UserRepository.Load(userEntity, "UserType");

                try
                {
                    this.unitOfWork.SaveChanges();

                    // syUser already saved records, proceed to create roles in Advantage
                    if (model.UserRoles.Count > 0)
                    {
                        rolesInserted = await this.rolesService.CreateUserRoles(model.UserRoles, userEntity.UserId);
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    // roll back if any issues saving syUser or roles in Advantage delete syUser ( no need to delete roles Inserted roles since it is the last line)
                    this.UserRepository.Delete(x => x.UserId == userEntity.UserId);
                    await this.UserRepository.SaveAsync();
                    result.ResultStatus = ApiMsgs.SAVE_UNSUCCESSFULL;
                    result.HasPassed = false;
                    return result;
                }


                /*Tenant DB User Setup */
                bool shouldCreateLogin = !loginExist;
                AspnetUsers aspnetUser = new AspnetUsers();
                AspnetMembership aspnetMembership = new AspnetMembership();
                TenantUsers tenantUser = new TenantUsers();
                try
                {
                    if (shouldCreateLogin)
                    {
                        // create aspnet user
                        var newAspnetUser = new AspNetUser()
                        {
                            UserName = model.Email,
                            ApplicationId = applicationId,
                            UserId = userEntity.UserId
                        };

                        var psalt = Convert.ToBase64String(this.GetSalt());

                        // create aspnet membership
                        var aspNetMembership = new AspNetMembership()
                        {
                            ApplicationId = applicationId,
                            Email = model.Email,
                            PasswordFormat = 1,
                            Password = string.Empty,
                            PasswordSalt = psalt,
                            CreateDate = DateTime.Now,
                            UserId = userEntity.UserId
                        };

                        // add user to aspnet user table, and set id for membership and tenant user
                        var aspNetUserCreated = await this.aspnetUserService.Create(newAspnetUser);

                        // add membership to db
                        var membershipEntityCreated = await this.membershipservice.Create(aspNetMembership);
                        aspnetMembership.UserId = membershipEntityCreated.UserId;
                    }

                    // create tenant User
                    var newTenantUser = new TenantUser()
                    {
                        UserId = userEntity.UserId,
                        IsDefaultTenant = shouldCreateLogin,
                        IsSupportUser = false,
                        TenantId = tenantId
                    };

                    // add tenant user records
                    var tenantUserEntityCreated = await this.tenantUserService.Create(newTenantUser);
                    tenantUser.TenantUserId = tenantUserEntityCreated.TenantUserId;
                }
                catch (Exception e)
                {
                    e.TrackException();
                    /* roll back i.e. delete records on adv db and tenant db */

                    // delete records on advantage
                    this.UserRolesCampusGroupsRepository.Delete(r => r.UserId == userEntity.UserId);
                    this.unitOfWork.SaveChanges();
                    this.UserRepository.Delete(u => u.UserId == userEntity.UserId);
                    this.unitOfWork.SaveChanges();

                    // delete records on tenant db
                    this.TenantUserRepository.Delete(tu => tu.UserId == aspnetUser.UserId);
                    this.AspnetMembershipRepository.Delete(m => m.UserId == aspnetUser.UserId);
                    this.AspnetUserRepository.Delete(u => u.UserId == aspnetUser.UserId);
                    this.tenantDb.SaveChanges();
                    result.HasPassed = false;
                    return result;
                }

                result = this.mapper.Map<User>(userEntity);
                result.UserRoles = rolesInserted.ToList();
                result.UserStatusId = model.UserStatusId;
                result.ResultStatus = ApiMsgs.SAVE_SUCCESS;
                result.HasPassed = true;
                result.SendEmail = shouldCreateLogin;


            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = ApiMsgs.SAVE_UNSUCCESSFULL;
                result.HasPassed = false;
                return result;
            }

            return result;

        }

        /// <summary>
        /// The get salt.
        /// </summary>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private byte[] GetSalt()
        {
            return GetSalt(SaltLengthLimit);
        }

        /// <summary>
        /// The get salt.
        /// </summary>
        /// <param name="maximumSaltLength">
        /// The maximum salt length.
        /// </param>
        /// <returns>
        /// The <see cref="byte[]"/>.
        /// </returns>
        private byte[] GetSalt(int maximumSaltLength)
        {
            var salt = new byte[maximumSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return salt;
        }

        /// <summary>
        /// The get user by id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<User> GetUserbyId(Guid userId)
        {
            var userEntity = await this.UserRepository.GetByIdAsync(userId);
            this.UserRepository.Load(userEntity, "UserType");
            var user = this.mapper.Map<User>(userEntity);
            user.UserStatusId = userEntity.AccountActive.HasValue && userEntity.AccountActive.Value
                                    ? await this.statusService.GetActiveStatusId()
                                    : await this.statusService.GetInactiveStatusId();
            user.UserRoles = (await this.rolesService.GetRolesForUser(userId)).ToList();
            return user;
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<User> Update(User model)
        {
            var result = new User();

            try
            {
                // make sure all roles are distinct, if not, remove duplicates
                model.UserRoles = model.UserRoles
                    .GroupBy(x => new { x.CampusGroupId, x.RoleId })
                    .Select(x => new UserRole
                    {
                        CampusGroupId = x.Key.CampusGroupId,
                        RoleId = x.Key.RoleId
                    })
                    .ToList();

                var username = model.DisplayName.IsNullOrEmpty() ? model.Email : model.DisplayName;

                var userToUpdate = this.UserRepository.Get(user => user.UserId == model.UserId).Include(usr => usr.UserType).FirstOrDefault();

                if (userToUpdate != null)
                {
                    if (this.CheckIfEmailExist(model.Email) && userToUpdate.Email != model.Email)
                    {
                        result.ResultStatus = ApiMsgs.EmailExist;
                        result.HasPassed = false;
                        return result;
                    }

                    if (model.UserTypeId == 1)
                    {
                        if (this.CheckIfUsernameExist(username) && userToUpdate.UserName != username)
                        {
                            result.ResultStatus = ApiMsgs.UsernameExist;
                            result.HasPassed = false;
                            return result;
                        }
                    }

                    IEnumerable<UserRole> newRolesInserted = new List<UserRole>();

                    // delete current user roles
                    this.UserRolesCampusGroupsRepository.Delete(r => r.UserId == model.UserId);
                    await this.UserRolesCampusGroupsRepository.SaveAsync();

                    var accountStatus = await this.statusService.ConvertStatus(model.UserStatusId);
                    string oldEmail = userToUpdate.Email;
                    userToUpdate.AccountActive = accountStatus;
                    userToUpdate.FullName = model.FullName;
                    userToUpdate.Email = model.Email;
                    userToUpdate.UserName = username;
                    userToUpdate.UserTypeId = model.UserTypeId;
                    userToUpdate.ModuleId = (short)model.DefaultModuleId;

                    this.UserRepository.Update(userToUpdate);
                    await this.UserRepository.SaveAsync();

                    // insert the new roles
                    if (model.UserRoles.Count > 0)
                    {
                        newRolesInserted = await this.rolesService.CreateUserRoles(model.UserRoles, model.UserId);
                    }

                    // if email is changing
                    if (oldEmail.ToLower() != model.Email.ToLower())
                    {
                        var aspnetMembershipToUpdate = this.AspnetMembershipRepository
                            .Get(entity => entity.UserId == model.UserId).FirstOrDefault();

                        var aspnetUserToUpdate = this.AspnetUserRepository
                            .Get(entity => entity.UserId == model.UserId).FirstOrDefault();

                        if (aspnetUserToUpdate != null)
                        {
                            aspnetUserToUpdate.UserName = model.Email;
                            aspnetUserToUpdate.LoweredUserName = model.Email;
                            this.AspnetUserRepository.Update(aspnetUserToUpdate);
                        }

                        if (aspnetMembershipToUpdate != null)
                        {
                            aspnetMembershipToUpdate.Email = model.Email;
                            this.AspnetMembershipRepository.Update(aspnetMembershipToUpdate);
                        }

                        this.tenantDb.SaveChanges();
                    }

                    SimpleCache.Remove(this.authService.GetApiUserCacheKey(model.Email, this.unitOfWork.GetTenantUser().TenantName));

                    result = this.mapper.Map<User>(userToUpdate);
                    result.UserRoles = newRolesInserted.ToList();
                    result.UserStatusId = model.UserStatusId;
                    result.ResultStatus = ApiMsgs.SAVE_SUCCESS;
                    result.HasPassed = true;

                }

            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = ApiMsgs.UPDATE_UNSUCCESSFULL;
                result.HasPassed = false;
            }

            return result;
        }


        /// <summary>
        /// The send new user email.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ApiStatusOutput> SendNewUserEmail(EmailBase model)
        {

            var smptConfiguration = this.emailService.GetSmtpSettings();
            var tenantUser = this.unitOfWork.GetTenantUser();
            var now = DateTime.Now;

            if (smptConfiguration.IsOAuth)
            {
                smptConfiguration = await this.emailService.CheckOAuthConfiguration(smptConfiguration);
            }

            bool isVendor = model.UserTypeCode != null && model.UserTypeCode.Equals("vendor", StringComparison.InvariantCultureIgnoreCase);

            var fromEmail = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FromEmailAddress);
            var subject = isVendor ? "Advantage API User Setup" : "Advantage User Setup";
            var templateToUse = isVendor ? "Templates/VendorNewUserInvite" : "Templates/NewUserInvite";
            model.From = fromEmail;
            model.Subject = subject;
            var tempPassword = this.GenerateTempPassword();
            var destinationEmail = model.To.FirstOrDefault();
            var updatedPassword = this.UpdateUserPassword(destinationEmail, tempPassword);

            if (updatedPassword)
            {
                model.Link += "?email=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(destinationEmail))
                                        + "&tpass=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(tempPassword))
                                        + "&tenant=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(tenantUser.TenantName))
                                        + "&hasAcceptedLatest=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(false.ToString()))
                                        + "&timestamp=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(((DateTimeOffset)now).ToUnixTimeSeconds().ToString()))
                                        + "&reset=false";
                model.Body = await this.viewRenderService.RenderToStringAsync(templateToUse, new UserSetup { Username = destinationEmail, Link = model.Link });
                var result = await this.emailService.SendEmail(model, smptConfiguration);
                return result;
            }
            else
            {
                return new ApiStatusOutput() { HasPassed = false, GeneralInformation = "EmailAddress not registered in Advantage" };
            }


        }

        /// <summary>
        /// The send reset password email.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ApiStatusOutput> SendResetPasswordEmail(EmailBase model)
        {
            var smptConfiguration = this.emailService.GetSmtpSettings();
            var tenantUser = this.unitOfWork.GetTenantUser();
            var now = DateTime.Now;
            var userId = this.AspnetMembershipRepository.Get(u => u.Email == model.To.FirstOrDefault()).FirstOrDefault()?.UserId;

            if (smptConfiguration.IsOAuth)
            {
                smptConfiguration = await this.emailService.CheckOAuthConfiguration(smptConfiguration);
            }

            bool isVendor = model.UserTypeCode != null && model.UserTypeCode.Equals("vendor", StringComparison.InvariantCultureIgnoreCase);
            var fromEmail = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FromEmailAddress);
            var subject = isVendor ? "Advantage API User Setup - Reset Password" : "Advantage User Setup - Reset Password";
            var templateToUse = isVendor ? "Templates/VendorPasswordReset" : "Templates/PasswordReset";
            model.From = fromEmail;
            model.Subject = subject;

            if (userId.HasValue)
            {
                var tempPassword = this.GenerateTempPassword();
                var destinationEmail = model.To.FirstOrDefault();
                var updatedPassword = this.UpdateUserPassword(destinationEmail, tempPassword);
                var unlocked = this.UnlockUserAccount(destinationEmail);
                if (updatedPassword && unlocked)
                {
                    model.Link += "?email=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(destinationEmail))
                                            + "&tpass=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(tempPassword))
                                            + "&tenant=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(tenantUser.TenantName))
                                            + "&hasAcceptedLatest=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(this.HasAcceptedLatestTermsOfUse(await this.GetUserLatestTermsOfUse(userId.Value)).ToString()))
                                            + "&timestamp=" + HttpServerUtility.UrlTokenEncode(Encoding.ASCII.GetBytes(((DateTimeOffset)now).ToUnixTimeSeconds().ToString()))
                                            + "&reset=true";
                    model.Body = await this.viewRenderService.RenderToStringAsync(
                                     templateToUse,
                                     new UserSetup() { Username = destinationEmail, Link = model.Link });
                    var result = await this.emailService.SendEmail(model, smptConfiguration);
                    return result;
                }
            }

            var failResult = new ApiStatusOutput() { HasPassed = false };
            failResult.ResultStatus = ApiMsgs.RECORD_NOT_FOUND_IN_TENANT_USERS_FOR_TENANT;
            return failResult;
        }

        /// <summary>
        /// The check if username exist.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckIfUsernameExist(string username)
        {
            var userInAdvantage = this.UserRepository.Get(user => user.UserName == username).FirstOrDefault();

            return userInAdvantage != null;
        }

        /// <summary>
        /// The check if email exist.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckIfEmailExist(string email)
        {
            var userInAdvantage = this.UserRepository
                .Get(user => user.Email == email)
                .FirstOrDefault();

            var userInTenant = this.AspnetUserRepository
                .Get(user => user.UserName == email)
                .FirstOrDefault();

            var membershipInTenant = this.AspnetMembershipRepository
                .Get(membership => membership.Email.ToLower() == email)
                .FirstOrDefault();

            return userInAdvantage != null || userInTenant != null || membershipInTenant != null;
        }

        /// <summary>
        /// The check if email exist in tenant.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="tenant">
        /// </param>
        /// <param name="loginExist">
        /// The login Exist.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckIfEmailExistForTenant(string email, string tenant, out bool loginExist)
        {
            loginExist = false;
            var modUser = this.unitOfWork.GetTenantUser();
            var tenantId = this.tenantService.GetTenantId(modUser.TenantName);

            var userInTenant = this.AspnetUserRepository
                .Get(user => user.UserName == email)
                .FirstOrDefault();

            var membershipInTenant = this.AspnetMembershipRepository
                .Get(membership => membership.Email.ToLower() == email)
                .FirstOrDefault();

            // if user does nto exist in asp net user table or membership table, then return false because no way it can exist on tenant users table
            if (userInTenant == null && membershipInTenant == null)
            {

                return false;
            }

            loginExist = true;

            // if user in asp net user table exist then get user id from there if not use membership table to get user id, if it still does not exist then assign just Guid.Empty
            var userId = userInTenant != null
                             ? userInTenant.UserId
                             : (membershipInTenant != null ? membershipInTenant.UserId : Guid.Empty);

            var tenantUser = this.tenantUserService.GetTenantUser(tenantId, userId);

            return tenantUser != null;
        }

        /// <summary>
        /// The check if email exist in advantage.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool CheckIfEmailExistInAdvantage(string email)
        {
            var userInAdvantage = this.UserRepository
                .Get(user => user.Email == email)
                .FirstOrDefault();

            return userInAdvantage != null;
        }

        /// <summary>
        /// Used to unlock a user account
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool UnlockUserAccount(string email)
        {
            var aspnetUser = this.AspnetMembershipRepository.Get(u => u.Email == email).FirstOrDefault();

            if (aspnetUser != null)
            {
                aspnetUser.IsLockedOut = false;
                this.AspnetMembershipRepository.Update(aspnetUser);
                this.AspnetMembershipRepository.Save();
                return true;
            }
            return false;
        }
        /// <summary>
        /// The generate temp password.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateTempPassword()
        {
            StringBuilder builder = new StringBuilder();
            RandomGenerator randomGenerator = new RandomGenerator();
            builder.Append(randomGenerator.RandomString(4, true));
            builder.Append(randomGenerator.RandomNumber(1000, 9999));
            builder.Append(randomGenerator.RandomString(2, false));
            builder.Append("*");
            string result = builder.ToString();
            return result;
        }

        /// <summary>
        /// The update user password.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="newPassword">
        /// The new password.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public bool UpdateUserPassword(string email, string newPassword)
        {
            var tenantUser = this.unitOfWork.GetTenantUser();
            var aspnetUser = this.AspnetMembershipRepository.Get(u => u.Email == email).FirstOrDefault();
            var user = this.authService.GetUser(email, tenantUser.TenantName);

            var result = false;

            if (aspnetUser != null && user != null && aspnetUser.UserId != Guid.Empty && user.UserId != Guid.Empty)
            {
                var salt = user.PasswordSalt.IsNullOrEmpty() ? string.Empty : user.PasswordSalt;
                aspnetUser.Password = this.authService.EncodePassword(newPassword, salt);

                this.AspnetMembershipRepository.Update(aspnetUser);
                this.AspnetMembershipRepository.Save();
                result = true;
            }
            return result;


        }

        /// <summary>
        /// The update password set.
        /// </summary>
        /// <param name="passwordSet">
        /// The password set.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ApiStatusOutput> UpdatePasswordSet(UserPassword passwordSet)
        {
            var result = new ApiStatusOutput();
            var aspnetUser = this.AspnetMembershipRepository.Get(u => u.Email == passwordSet.Email).FirstOrDefault();
            var user = this.authService.GetUser(aspnetUser.Email, passwordSet.TenantName);
            var minNonAlphaNumeric = Membership.MinRequiredNonAlphanumericCharacters;
            var minLength = Membership.MinRequiredPasswordLength;
            var passRegex = new Regex(Membership.PasswordStrengthRegularExpression);


            if (!passRegex.IsMatch(passwordSet.Password))
            {
                result.HasPassed = false;
                result.ResultStatus = passwordSet.Password.Length < minLength ? ApiMsgs.PasswordNeedsMinimumLength.Replace("{0}", minLength.ToString()) : ApiMsgs.PasswordNeedsSpecialChar.Replace("{0}", minNonAlphaNumeric.ToString());
            }
            else
            {


                if (aspnetUser.UserId != Guid.Empty)
                {
                    var salt = user.PasswordSalt.IsNullOrEmpty() ? string.Empty : user.PasswordSalt;
                    aspnetUser.Password = this.authService.EncodePassword(passwordSet.Password, salt);
                    aspnetUser.LastPasswordChangedDate = DateTime.Now;
                    aspnetUser.IsApproved = true;
                    aspnetUser.IsLockedOut = false;
                    aspnetUser.FailedPasswordAnswerAttemptCount = 0;
                    aspnetUser.FailedPasswordAttemptCount = 0;
                    aspnetUser.PasswordQuestion = passwordSet.SecurityQuestion;
                    aspnetUser.PasswordAnswer = this.authService.EncodePassword(passwordSet.SecurityAnswer.ToLower().Trim(), salt);

                    this.AspnetMembershipRepository.Update(aspnetUser);

                    try
                    {
                        await this.AspnetMembershipRepository.SaveAsync();
                        user = this.authService.GetUser(aspnetUser.Email, passwordSet.TenantName);

                        //init advantage context manually since unit of work action filter cannot execute on set password
                        this.SetAdvantageContext(user);

                        if (!passwordSet.HasLatestVersion.Equals("true", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // get current version
                            var version = this.systemConfigurationAppSettingService.GetAppSettingValue(ConfigurationAppSettingsKeys.TermsOfUseVersionNumber);
                            var updatedTermsOfUse = await this.UpdateUserTermsOfUse(
                                                        aspnetUser.UserId,
                                                        version);

                            if (!updatedTermsOfUse.HasAcceptedLatestTermsOfUse)
                            {
                                result.HasPassed = false;
                                result.ResultStatus = ApiMsgs.FailedUpdateTermsOfUse;
                            }
                        }

                        //remove from cache
                        SimpleCache.Remove(this.authService.GetApiUserCacheKey(aspnetUser.Email, passwordSet.TenantName));

                        result.HasPassed = true;
                        result.ResultStatus = ApiMsgs.PasswordSetUpdatedSuccessfully;

                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        result.HasPassed = false;
                        result.ResultStatus = ApiMsgs.FailedUpdatingRecord;
                    }

                }
                else
                {
                    result.HasPassed = false;
                    result.ResultStatus = ApiMsgs.UsernameDoesNotExist;
                }
            }

            return result;
        }

        /// <summary>
        /// The get current user type.
        /// </summary>
        /// <returns>
        /// The <see cref="UserType"/>.
        /// </returns>
        public UserType GetCurrentUserType()
        {
            var tenantUser = this.unitOfWork.GetTenantUser();

            var userType = this.UserRepository
                .Get(u => u.Email.ToLower() == tenantUser.Email.ToLower())
                .Select(ut => new UserType
                {
                    Id = ut.UserTypeId,
                    Description = ut.UserType.Description,
                    Code = ut.UserType.Code
                })
                .FirstOrDefault();

            return userType;
        }

        /// <summary>
        /// The get current user information.
        /// </summary>
        /// <returns>
        /// The <see cref="UserInformation"/>.
        /// </returns>
        public UserInformation GetCurrentUserInformation()
        {
            var result = new UserInformation();

            var tenantUser = this.unitOfWork.GetTenantUser();

            var user = this.UserRepository
                .Get(u => u.Email.ToLower() == tenantUser.Email.ToLower())
                .FirstOrDefault();

            if (user != null)
            {
                result.HasMultipleTenants = this.TenantUserRepository.Get(tu => tu.UserId == user.UserId).Count() > 1;
                result.Type = this.GetCurrentUserType();
                result.TermsOfUse = this.GetCurrentUserTermsOfUse();
            }

            return result;
        }
        /// <summary>
        /// The get current user access to a certain resource/Page.
        /// </summary>
        /// <param name="resId">
        /// The resource id/ page Id.
        /// </param>
        /// <param name="userId">
        /// The logged in User Id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<bool> GetUserAcessPermissionsForResourceId(int resId, Guid userId)
        {
            var user = await this.GetUserbyId(userId);
            var accessLevel = this.resourceAccess.Get(a => a.ResourceId == resId).ToList();
            if (accessLevel.Any())
            {
                var result = accessLevel.Any(x => user.UserRoles.Any(i => i.RoleId == x.RoleId) && x.AccessLevel == AccessLevel.FullAccess);
                return result;
            }
            else
                return false;
        }

        /// <summary>
        /// The get current user terms of use.
        /// </summary>
        /// <returns>
        /// The <see cref="UserTermsOfUse"/>.
        /// </returns>
        public UserTermsOfUse GetCurrentUserTermsOfUse()
        {
            var tenantUser = this.unitOfWork.GetTenantUser();

            var user = this.UserRepository
                .Get(u => u.Email.ToLower() == tenantUser.Email.ToLower())
                .FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            var termsOfUse = this.TermsOfUseRepository.Get(t => t.UserId == user.UserId)
                                 .OrderByDescending(t => t.AcceptedDate)
                                 .Select(t => new UserTermsOfUse
                                 {
                                     LastVersionAccepted = t.Version,
                                     LastVersionAcceptedDate = t.AcceptedDate
                                 })
                                 .FirstOrDefault() ?? new UserTermsOfUse();

            var version = this.systemConfigurationAppSettingService.GetAppSettingValue(ConfigurationAppSettingsKeys.TermsOfUseVersionNumber);
            if (!string.IsNullOrEmpty(version))
            {
                termsOfUse.Version = version;
                termsOfUse.HasAcceptedLatestTermsOfUse = termsOfUse.LastVersionAccepted != null && termsOfUse.LastVersionAccepted.Equals(
                                                             version,
                                                             StringComparison.InvariantCultureIgnoreCase);
            }

            return termsOfUse;
        }

        /// <summary>
        /// The has accepted latest terms of use.
        /// </summary>
        /// <param name="userVersion">
        /// The user version.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool HasAcceptedLatestTermsOfUse(string userVersion)
        {
            // get current version
            var version = this.systemConfigurationAppSettingService.GetAppSettingValue(ConfigurationAppSettingsKeys.TermsOfUseVersionNumber);
            if (!string.IsNullOrEmpty(version))
            {
                if (version.Equals(userVersion))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The update user terms of use.
        /// </summary>
        /// <param name="userId">
        /// The user's id.
        /// </param>
        /// /// <param name="version">
        /// The version of advantage.
        /// </param>
        /// <returns>
        /// The <see cref="UserTermsOfUse"/>.
        /// </returns>
        public async Task<UserTermsOfUse> UpdateUserTermsOfUse(Guid userId, string version)
        {
            try
            {
                var user = await this.GetUserbyId(userId);
                var now = DateTime.Now;
                SyUserTermsOfUse termsOfUse = new SyUserTermsOfUse()
                {
                    UserId = userId,
                    Version = version,
                    AcceptedDate = now,
                    ModDate = now,
                    ModUser = user.DisplayName,

                };

                var entity = await this.TermsOfUseRepository.CreateAsync(termsOfUse);
                await this.TermsOfUseRepository.SaveAsync();
                if (entity != null && entity.UserTermsOfUseId > 0)
                {
                    return new UserTermsOfUse()
                    {
                        LastVersionAcceptedDate = now,
                        UserId = userId,
                        Id = entity.UserTermsOfUseId,
                        LastVersionAccepted = version,
                        HasAcceptedLatestTermsOfUse = true,
                        Version = version
                    };
                }

                return new UserTermsOfUse()
                {
                    UserId = userId,
                    Id = 0,
                    LastVersionAccepted = version,
                    HasAcceptedLatestTermsOfUse = false,
                    Version = version
                };
            }
            catch (Exception e)
            {
                e.TrackException();
                return new UserTermsOfUse()
                {
                    UserId = userId,
                    Id = 0,
                    LastVersionAccepted = version,
                    HasAcceptedLatestTermsOfUse = false,
                    Version = version
                };
            }
        }

        /// <summary>
        /// The get admin reps.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetAdminRepsByCampus(Guid campusId)
        {
            var result = await Task.Run(() => SimpleCache.GetOrAddExisting(
                             $"{this.CacheKeyFirstSegment}:GetAdminRepsByCampus:{campusId}",
                             () => this.mapper.Map<IList<IListItem<string, Guid>>>(
                                     this.UserRepository
                                         .Get(
                                             pt => pt.CampusId == campusId && pt.AccountActive == true
                                                   && pt.SyUsersRolesCampGrps.Any(x => x.Role.SysRole.SysRoleId == (int)Constants.SystemRole.AdmissionReps))
                                     .Include(x => x.SyUsersRolesCampGrps).ThenInclude(x => x.Role)
                                     .ThenInclude(x => x.SysRole)).ToList(),
                             CacheExpiration.VERY_LONG));
            return result;
        }

        /// <summary>
        /// The get user latest terms of use.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public async Task<string> GetUserLatestTermsOfUse(Guid userId)
        {
            try
            {
                var entity = await this.TermsOfUseRepository.Get(tou => tou.UserId == userId).OrderByDescending(tou => tou.AcceptedDate).FirstOrDefaultAsync();
                if (entity != null)
                {
                    return entity.Version;
                }

                return string.Empty;
            }
            catch (Exception e)
            {
                e.TrackException();
                return string.Empty;
            }
        }

        /// <summary>
        /// The set advantage context.
        /// </summary>
        /// <param name="user">
        /// The user which will be used to create the context
        /// </param>
        public void SetAdvantageContext(ApiUser user)
        {
            var option = new DbContextOptionsBuilder<AdvantageContext>();
            option.UseSqlServer(user.BuildConnectionString());
            this.unitOfWork.SetContext(new AdvantageContext(option.Options), SimpleCacheValues.KEY + "_" + user.TenantName);
        }
    }
}
