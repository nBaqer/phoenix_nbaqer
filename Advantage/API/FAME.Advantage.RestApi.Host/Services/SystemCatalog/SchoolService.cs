﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the SchoolService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Caching;
    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    /// <summary>
    /// The school logo service to get the default logo of School.
    /// </summary>
    public class SchoolService : ISchoolService
    {
        /// <summary>
        /// The advantage context.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="SchoolService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="mapper">
        /// The mapper
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="unitOfWork">
        /// unit Of Work
        /// </param>
        public SchoolService(
            IAdvantageDbContext advantageContext,
            IMapper mapper,
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService,
            IAdvantageDbContext unitOfWork)
        {
            this.advantageContext = advantageContext;
            this.mapper = mapper;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.unitOfWork = unitOfWork;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The school logo repository.
        /// </summary>
        private IRepository<SySchoolLogo> SySchoolLogoRepository => this.advantageContext.GetRepositoryForEntity<SySchoolLogo>();

        /// <summary>
        /// The school logo repository.
        /// </summary>
        private IRepository<SyCampuses> SyCampusesRepository => this.advantageContext.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The Get School Detail service takes campus id as input and returns all the information related to campus.
        /// </summary>
        /// <remarks>
        /// The Get School Detail  service requires campus Id object of type Guid.
        /// </remarks> 
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The SchoolDetail, this object contails school name, address, phone numbers, fax, email, website and school logo details
        /// </returns>
        public async Task<SchoolDetail> GetDetail(Guid campusId)
        {
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:SchoolDetailsByCampus:{campusId}",
                                   () =>
                                       {
                                           var campusDetail = this.SyCampusesRepository.Get(x => x.CampusId == campusId)
                                               .Include(x => x.Country).Include(x => x.State).FirstOrDefault();

                                           if (campusDetail != null)
                                           {
                                               var schoolDetail = this.mapper.Map<SchoolDetail>(campusDetail);
                                               if (schoolDetail != null)
                                               {
                                                   var schoolLogoName =
                                                       this.systemConfigurationAppSettingService.GetAppSettingValue(
                                                           ConfigurationAppSettingsKeys.StandardLogo);


                                                   if (schoolLogoName != string.Empty)
                                                   {
                                                       var schoolLogo = this.SySchoolLogoRepository.Get(x => x.ImageCode == schoolLogoName);
                                                       if (schoolLogo != null)
                                                       {
                                                           schoolDetail.SchoolLogo = schoolLogo.FirstOrDefault()?.Image;
                                                       }
                                                   }

                                                   return schoolDetail;
                                               }
                                           }

                                           return null;
                                       },
                                   CacheExpiration.VERY_LONG);

                           });
        }

        /// <summary>
        /// The clear school cash.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> ClearSchoolCash(Guid campusId)
        {

            if (!campusId.IsEmpty())
            {
                SimpleCache.Clear();
                return true;
            }

            return false;
        }




    }
}
