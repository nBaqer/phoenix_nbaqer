﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttendanceUnitTypesService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The attendance unit types service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The attendance unit types service.
    /// </summary>
    public class AttendanceUnitTypesService : IAttendanceUnitTypesService
    {
        /// <summary>
        /// The mapper is used to map the Attendance Unit Type Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttendanceUnitTypesService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        public AttendanceUnitTypesService(IAdvantageDbContext advantageDbContext, IMapper mapper, IStatusesService statusService)
        {
            this.advantageDbContext = advantageDbContext;
            this.mapper = mapper;
            this.statusService = statusService;
        }

        /// <summary>
        /// The repository.
        /// </summary>
        private IRepository<ArAttUnitType> Repository => this.advantageDbContext.GetRepositoryForEntity<ArAttUnitType>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="IList{IListItem{TText,TValue}"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetAll()
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:AttendanceUnitType",
                       () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.Repository.Get()).ToList(),
                       CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The get by description.
        /// </summary>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The <see cref="IListItem{TText,TValue}"/>.
        /// </returns>
        public IListItem<string, Guid> GetByDescription(string description)
        {
            var items = this.GetAll().Result;

            var attendanceUnitType = items.FirstOrDefault(x => x.Text.ToLower().Trim() == description.ToLower().Trim());

            return attendanceUnitType;
        }
    }
}
