﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradeScaleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GradeScaleService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The grade scale service.
    /// </summary>
    public class GradeScaleService : IGradeScaleService
    {

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradeScaleService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        public GradeScaleService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
        }

        /// <summary>
        /// The grade scales repository.
        /// </summary>
        private IRepository<ArGradeScales> GradeScalesRepository =>
            this.context.GetRepositoryForEntity<ArGradeScales>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The get grade scales.
        /// </summary>
        /// <param name="gradeSystemId">
        /// The grade system id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<IList<IListItem<string, Guid>>>> GetGradeScales(Guid gradeSystemId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                       $"{this.CacheKeyFirstSegment}:GradeScalesByGradeSystemId:{gradeSystemId}",
                       () =>
                           {
                               var query = this.GradeScalesRepository.Get(x => x.StatusId == this.ActiveStatusId && x.GrdSystemId == gradeSystemId).ToList();
                               ActionResult<IList<IListItem<string, Guid>>> actionResult = new ActionResult<IList<IListItem<string, Guid>>>();
                               if (query.Count > 0)
                               {
                                   var resultSet = this.mapper.Map<IList<IListItem<string, Guid>>>(query);
                                   if (resultSet == null)
                                   {
                                       actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                                       actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                                   }
                                   else
                                   {
                                       actionResult.Result = resultSet;
                                   }
                               }
                               else
                               {
                                   actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                                   actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                               }

                               return actionResult;
                           },
                       CacheExpiration.VERY_LONG));
        }
    }
}
