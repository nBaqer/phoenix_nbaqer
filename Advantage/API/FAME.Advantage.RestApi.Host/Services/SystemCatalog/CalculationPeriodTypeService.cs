﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationPeriodTypeService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the CalculationPeriodTypeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The calculation period type service.
    /// </summary>
    public class CalculationPeriodTypeService : ICalculationPeriodTypeService
    {
        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The default sort fields.
        /// </summary>
        private readonly Func<IQueryable<SyR2t4calculationPeriodTypes>, IOrderedQueryable<SyR2t4calculationPeriodTypes>> defaultSortField;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationPeriodTypeService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The CalculationPeriodTypesMapper class used to transfer the calculationPeriodType entity.
        /// </param>
        public CalculationPeriodTypeService(IAdvantageDbContext advantageDbContext, IMapper mapper)
        {
            this.advantageDbContext = advantageDbContext;
            this.mapper = mapper;
            this.defaultSortField = x => x.OrderBy(_ => _.Description);
        }

        /// <summary>
        /// The calculation period type repository.
        /// </summary>
        private IRepository<SyR2t4calculationPeriodTypes> CalculationPeriodTypeRepository => this.advantageDbContext.GetRepositoryForEntity<SyR2t4calculationPeriodTypes>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations.
        /// </summary>
        /// <remarks>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations. Period types are Payment Period and Period of Enrollment.
        /// R2T4 calculations are performed based on the selected period.
        /// </remarks> 
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetPeriodTypes()
        {
            return await Task.Run(
                       () =>
                       {
                           try
                           {
                               var result = SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:calculationPeriodType",
                                   () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                                       this.CalculationPeriodTypeRepository.Get(null, this.defaultSortField)),
                                   CacheExpiration.LONG);
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               return new List<IListItem<string, Guid>>();
                           }
                       });
        }

        /// <summary>
        /// The is payment period type action method returns boolean value.
        /// </summary>
        /// <remarks>
        /// The Is Payment Period action method returns boolean value, if the results value match with Payment period then it returns the true other wise the return value is false.
        /// </remarks>
        /// <param name="periodTypeId">
        /// The period type id is the Guid.
        /// </param>
        /// <returns>
        ///  Returns boolean value based on the matching value. If the period type is Payment Period then the return value is true else it is Period of Enrollment then it returns false.
        /// </returns>
        public async Task<bool> IsPaymentPeriodType(Guid periodTypeId)
        {
            return await Task.Run(
                       () =>
                       {
                           return this.CalculationPeriodTypeRepository.Get(
                               x => x.CalculationPeriodTypeId == periodTypeId
                                    && x.Description == CalculationPeriodTypes.PAYMENT_PERIOD).Any();
                       });
        }
    }
}
