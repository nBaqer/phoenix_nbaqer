﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SystemConfigurationAppSettingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The user service.
    /// </summary>
    public class SystemConfigurationAppSettingService : ISystemConfigurationAppSettingService
    {
        /// <summary>
        /// The mapper is used to map the Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemConfigurationAppSettingService"/> class. 
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service
        /// </param>
        public SystemConfigurationAppSettingService(IAdvantageDbContext unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The app setting Value repository.
        /// </summary>
        private IRepository<SyConfigAppSetValues> AppSettingValueRepository => this.unitOfWork.GetRepositoryForEntity<SyConfigAppSetValues>();

        /// <summary>
        /// The app setting repository.
        /// </summary>
        private IRepository<SyConfigAppSettings> AppSettingRepository => this.unitOfWork.GetRepositoryForEntity<SyConfigAppSettings>();

        /// <summary>
        /// The get app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAppSettingValue(string keyName)
        {
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:AppSettingValueFor:{keyName}",
                () =>
                    {
                        string value = string.Empty;
                        var appSetting = this.AppSettingRepository.Get(entity => entity.KeyName.ToLower() == keyName)
                            .FirstOrDefault();

                        if (appSetting != null)
                        {
                            var appSettingValue = this.AppSettingValueRepository
                                .Get(entity => entity.SettingId.HasValue && entity.SettingId == appSetting.SettingId)
                                .FirstOrDefault();

                            if (appSettingValue != null)
                            {
                                value = appSettingValue.Value;
                            }
                        }

                        return value;
                    },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The get app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <returns>
        /// The <see cref="IListActionResult"/>.
        /// </returns>
        public async Task<List<AppSettingValue>> GetAppSettingValues(string keyName)
        {
            return await Task.Run(() =>
            {
                return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:AppSettingValuesFor:{keyName}",
                        () =>
                           {
                               List<AppSettingValue> results = new List<AppSettingValue>();
                               string value = string.Empty;
                               var appSetting = this.AppSettingRepository.Get(entity => entity.KeyName.ToLower() == keyName).FirstOrDefault();

                               if (appSetting != null)
                               {
                                   var appSettingValue = this.AppSettingValueRepository.Get(
                                       entity => entity.SettingId.HasValue && entity.SettingId == appSetting.SettingId).ToList();

                                   return this.mapper.Map<List<AppSettingValue>>(appSettingValue);
                               }

                               return results;
                           },
                    CacheExpiration.VERY_LONG);
            });
        }

        /// <summary>
        /// The get app setting value by campus.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetAppSettingValueByCampus(string keyName, Guid campusId)
        {
            return SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:AppSettingValueFor:{keyName}:{campusId}",
                () =>
                {
                    string value = string.Empty;
                    var appSetting = this.AppSettingRepository.Get(entity => entity.KeyName.ToLower() == keyName)
                        .FirstOrDefault();

                    if (appSetting != null)
                    {
                        var appSettingValues = this.AppSettingValueRepository
                            .Get(entity => entity.SettingId.HasValue
                                           && entity.SettingId == appSetting.SettingId);

                        if (appSettingValues != null)
                        {
                            var appSettingValueByCampus =
                                appSettingValues.FirstOrDefault(entity => entity.CampusId == campusId);

                            value = appSettingValueByCampus != null
                                        ? appSettingValueByCampus.Value
                                        : appSettingValues.FirstOrDefault(
                                            entity => string.IsNullOrEmpty(entity.CampusId.ToString()))?.Value;
                        }
                    }

                    return value?.ToLowerInvariant();
                },
                CacheExpiration.VERY_LONG);
        }

        /// <summary>
        /// The set app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task SetAppSettingValue(string keyName, string value)
        {
            var appSetting = this.AppSettingRepository.Get(entity => entity.KeyName.ToLower() == keyName)
                .FirstOrDefault();

            if (appSetting != null)
            {
                var appSettingValue = this.AppSettingValueRepository
                    .Get(entity => entity.SettingId.HasValue && entity.SettingId == appSetting.SettingId)
                    .FirstOrDefault();

                if (appSettingValue != null)
                {
                    appSettingValue.Value = value;
                }

                this.AppSettingValueRepository.Update(appSettingValue);

                await this.AppSettingValueRepository.SaveAsync();
                SimpleCache.Set($"{this.CacheKeyFirstSegment}:AppSettingValueFor:{keyName}", value, CacheExpiration.VERY_LONG);
            }
        }
    }
}
