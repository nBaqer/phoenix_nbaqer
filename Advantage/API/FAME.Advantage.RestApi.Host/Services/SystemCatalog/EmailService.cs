﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the EmailService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Google.Apis.Json;
    using Google.Apis.Util.Store;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Threading.Tasks;

    /// <summary>
    /// The email service.
    /// </summary>
    public class EmailService : IEmailService
    {

        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly ITenantDbContext tenantContext;

        /// <summary>
        /// The System Configuration AppSetting Service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The google oauth settings.
        /// </summary>
        private readonly GoogleOAuthSettings googleOauthSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class. 
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        /// <param name="tenantContext">
        /// The tenant db context.
        /// </param>
        /// <param name="tenantDb">
        /// The tenant Db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="googleOauthSettings">
        /// The google Oauth Settings.
        /// </param>
        public EmailService(IAdvantageDbContext advantageContext, ITenantDbContext tenantContext, ITenantDbContext tenantDb, IMapper mapper, ISystemConfigurationAppSettingService systemConfigurationAppSettingService, IOptions<GoogleOAuthSettings> googleOauthSettings)
        {
            this.advantageContext = advantageContext;
            this.tenantContext = tenantContext;
            this.mapper = mapper;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.googleOauthSettings = googleOauthSettings.Value;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The user repository.
        /// </summary>
        private IRepository<SyUsers> UserRepository => this.advantageContext.GetRepositoryForEntity<SyUsers>();

        /// <summary>
        /// The send email.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="ApiStatusOutput"/>.
        /// </returns>
        public async Task<ApiStatusOutput> SendEmail(IEmailBase email, EmailConfiguration settings)
        {
            MailMessage newEmail = new MailMessage();
            var result = new ApiStatusOutput();
            string extraInformation = string.Empty;
            if (!string.IsNullOrEmpty(email.Body))
            {
                newEmail.From = new MailAddress(email.From);
                newEmail.Subject = email.Subject;
                newEmail.Body = email.Body;
                newEmail.IsBodyHtml = true;


                foreach (var address in email.To)
                {
                    newEmail.To.Add(new MailAddress(address));

                }

                string allEmails = string.Join(",", email.To);
                extraInformation = "Email successfully sent to " + allEmails;

                SmtpClient client = new SmtpClient(settings.Server, settings.Port)
                {
                    EnableSsl = settings.IsSSL,
                    Credentials = new NetworkCredential(settings.Username, settings.Password)
                };
                try
                {
                    client.Send(newEmail);
                    result.HasPassed = true;
                    result.ShortNote = (email.Body.Length > 20) ? email.Body.Substring(0, 20) : string.Empty;
                    result.GeneralInformation = extraInformation;

                }
                catch (Exception e)
                {
                    e.TrackException();
                    result.HasPassed = false;
                    result.GeneralInformation = "Email could not be sent.";

                    result.ResultStatus =
                        "SMTP email settings are not configured. Email has not been sent. Please contact your site Administrator and try again.";
                }
            }
            return result;
        }

        /// <summary>
        /// The get smtp settings.
        /// </summary>
        /// <returns>
        /// The <see cref="EmailConfiguration"/>.
        /// </returns>
        public EmailConfiguration GetSmtpSettings()
        {
            var smtpServer = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.SmtpServer);
            var fameSmtpOAuth = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpOAuth);
            var fameSmtpOAuthClientSecret = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpOAuthClientSecret);
            var fameSmtpOAuthToken = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpOAuthToken);
            var fameSmtpPassword = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpPassword);
            var fameSmtpPort = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpPort);
            var fameSmtpSsl = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpSsl);
            var fameSmtpUsername = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpUsername);
            var fameSmtpServer = this.systemConfigurationAppSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FameSmtpServer);

            int port = 0;
            bool portParsed = int.TryParse(fameSmtpPort, out port);
            bool isOauth = !string.IsNullOrEmpty(fameSmtpOAuth) && fameSmtpOAuth.ToUpper() == "YES";
            bool isSSL = !string.IsNullOrEmpty(fameSmtpSsl) && fameSmtpSsl.ToUpper() == "YES";

            var smptConfiguration = new EmailConfiguration
            {
                Password = fameSmtpPassword,
                Username = fameSmtpUsername,
                Server = fameSmtpServer,
                Port = portParsed ? port : 80,
                IsOAuth = isOauth,
                IsSSL = isSSL,
                OathClientSecret = fameSmtpOAuthClientSecret,
                Token = fameSmtpOAuthToken
            };

            return smptConfiguration;
        }

        /// <summary>
        /// The check o auth configuration.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <returns>
        /// The <see cref="EmailConfiguration"/>.
        /// </returns>
        public async Task<EmailConfiguration> CheckOAuthConfiguration(EmailConfiguration configuration)
        {
            // get file
            var fileToken = this.GetFileToken();

            if (!string.IsNullOrWhiteSpace(fileToken))
            {
                if (fileToken != configuration.Token)
                {
                    // Update the token in Db (this step change the token stored in database in order to update it)
                    configuration.Token = fileToken;
                    await this.systemConfigurationAppSettingService.SetAppSettingValue(
                        EmailServiceConfigurationKeys.FameSmtpOAuthToken,
                        fileToken);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(configuration.Token))
                {
                    throw new Exception("OAuth GMail process is not completed. Please complete it before try to send email");

                }

                // Create file directory if not exists and create the file token
                this.SetFileToken(configuration.Token);

            }

            return configuration;
        }

        /// <summary>
        /// The get file token.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetFileToken()
        {
            var gmailOathPath = this.googleOauthSettings.Path;

            // Get the file token...
            string tokenFile = string.Empty;
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            // var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            var storeFilePath = new Uri(appBinDirectory + gmailOathPath).LocalPath;
            if (Directory.Exists(storeFilePath))
            {
                var filesList = Directory.GetFiles(storeFilePath);
                if (filesList.Length > 0)
                {
                    var file0 = filesList[0];
                    if (file0.Contains("Google.Apis.Auth"))
                    {
                        // Read the file content
                        var filepath = Path.Combine(storeFilePath, file0);
                        tokenFile = File.ReadAllText(filepath);
                    }
                }
            }
            return tokenFile;
        }

        /// <summary>
        /// The setfile token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        public void SetFileToken(string token)
        {
            var gmailOathPath = this.googleOauthSettings.Path;

            // Create the file data-store
            var appBinDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            // var storeFilePath = new Uri(appBinDirectory + @"/GmailOauth/gmailAdvantageService.json").LocalPath;
            var storeFilePath = new Uri(appBinDirectory + gmailOathPath).LocalPath;
            var secc = NewtonsoftJsonSerializer.Instance.Deserialize<Google.Apis.Auth.OAuth2.Responses.TokenResponse>(token);
            var filestore = new FileDataStore(storeFilePath, true);
            var wait = filestore.StoreAsync("user", secc);
            wait.Wait();
        }

        /// <summary>
        /// Sends email to support users. Overrides email To with support user emails in database
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="ApiStatusOutput"/>.
        /// </returns>
        public async Task<ApiStatusOutput> SendEmailToSupportUsers(IEmailBase email, EmailConfiguration settings = null)
        {
            var result = new ApiStatusOutput();

            try
            {
                if (settings == null) // no smtp settings passed
                {
                    var smptConfiguration = this.GetSmtpSettings();
                    if (smptConfiguration.IsOAuth)
                    {
                        smptConfiguration = await this.CheckOAuthConfiguration(smptConfiguration);
                    }

                    settings = smptConfiguration;
                }

                var tenantUsers = this.tenantContext.GetRepositoryForEntity<TenantUsers>().Get();
                var aspnetUsers = this.tenantContext.GetRepositoryForEntity<AspnetUsers>().Get();
                var supportUserEmails = await (from a in tenantUsers
                                               join b in aspnetUsers on a.UserId equals b.UserId
                                               where a.IsSupportUser == true
                                               select b.UserName).ToListAsync();

                if (supportUserEmails.Any())
                {
                    email.To = supportUserEmails;
                    result = await this.SendEmail(email, settings);
                }
                else
                {
                    result.HasPassed = false;
                    result.ShortNote = (email.Body.Length > 20) ? email.Body.Substring(0, 20) : string.Empty;
                    result.GeneralInformation = "There are no support users registered in database.";
                }

                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                result.HasPassed = false;
                result.GeneralInformation = "Email could not be sent.";
                result.ResultStatus = "Error sending email to support. " + e.Message;
                return result;
            }
        }
    }
}
