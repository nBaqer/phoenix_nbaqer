﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RazorViewRenderService.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The razor view render service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Tools
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Abstractions;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.AspNetCore.Mvc.Razor;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    using Microsoft.AspNetCore.Routing;

    /// <summary>
    /// The razor view render service.
    /// </summary>
    public class RazorViewRenderService : IRazorViewRenderService
    {
        /// <summary>
        /// The _razor view engine.
        /// </summary>
        private readonly IRazorViewEngine razorViewEngine;

        /// <summary>
        /// The _temp data provider.
        /// </summary>
        private readonly ITempDataProvider tempDataProvider;

        /// <summary>
        /// The _service provider.
        /// </summary>
        private readonly IServiceProvider serviceProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="RazorViewRenderService"/> class.
        /// </summary>
        /// <param name="razorViewEngine">
        /// The razor view engine.
        /// </param>
        /// <param name="tempDataProvider">
        /// The temp data provider.
        /// </param>
        /// <param name="serviceProvider">
        /// The service provider.
        /// </param>
        public RazorViewRenderService(IRazorViewEngine razorViewEngine, ITempDataProvider tempDataProvider, IServiceProvider serviceProvider)
        {
            this.razorViewEngine = razorViewEngine;
            this.tempDataProvider = tempDataProvider;
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// The render to string async.
        /// </summary>
        /// <param name="viewName">
        /// The view name.
        /// </param>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> RenderToStringAsync(string viewName, object model)
        {
            var httpContext = new DefaultHttpContext { RequestServices = this.serviceProvider };
            var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

            using (var sw = new StringWriter())
            {
                var viewResult = this.razorViewEngine.FindView(actionContext, viewName, false);

                if (viewResult.View == null)
                {
                    throw new ArgumentNullException($"{viewName} does not match any available view");
                }

                var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                {
                    Model = model
                };

                var viewContext = new ViewContext(
                    actionContext,
                    viewResult.View,
                    viewDictionary,
                    new TempDataDictionary(actionContext.HttpContext, this.tempDataProvider),
                    sw,
                    new HtmlHelperOptions());

                await viewResult.View.RenderAsync(viewContext);
                return sw.ToString();
            }
        }

    }
}
