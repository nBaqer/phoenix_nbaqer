﻿using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Services.Tools
{
    public class LoggerService : ILoggerService
    {

        public string GetTempPath()
        {
            string path = System.Environment.CurrentDirectory;
            if (!path.EndsWith("\\")) path += "\\";
            return path;
        }

        /// <summary>
        /// Log a message to a file
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="fileName"></param>
        /// <param name="path"></param>
        public void LogMessageToFile(string msg, string fileName, string path = "")
        {
            var newPath = GetTempPath() + path;
            System.IO.Directory.CreateDirectory(newPath);

            System.IO.StreamWriter sw = System.IO.File.AppendText(
                newPath + fileName);
            try
            {
                sw.WriteLine(msg);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
