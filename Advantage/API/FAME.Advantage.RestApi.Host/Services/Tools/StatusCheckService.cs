﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusCheckService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StatusCheckService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;

    /// <summary>
    /// The status check service.
    /// </summary>
    public class StatusCheckService : IStatusCheckService
    {
        /// <summary>
        /// The tenant aut db context.
        /// </summary>
        private readonly ITenantDbContext tenantAutDbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusCheckService"/> class.
        /// </summary>
        /// <param name="tenantAutDbContext">
        /// The tenant aut db context.
        /// </param>
        public StatusCheckService(ITenantDbContext tenantAutDbContext)
        {
            this.tenantAutDbContext = tenantAutDbContext;
        }

        /// <summary>
        /// The tenant repository.
        /// </summary>
        private IRepository<Tenant> TenantRepository => this.tenantAutDbContext.GetRepositoryForEntity<Tenant>();

        /// <summary>
        /// The get status.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<ListItem<string, bool>>> GetStatus()
        {
            return await Task.Run(
                       () =>
                           {
                               List<ListItem<string, bool>> results = new List<ListItem<string, bool>>();

                               var tenants = this.TenantRepository.Get();

                               if (tenants.Any())
                               {
                                   results.Add(new ListItem<string, bool>()
                                   {
                                       Value = true,
                                       Text = "Tenant Auth is up and running"
                                   });
                               }

                               foreach (var tenant in tenants)
                               {
                                   try
                                   {
                                       SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
                                       connectionStringBuilder.DataSource = tenant.ServerName;
                                       connectionStringBuilder.InitialCatalog = tenant.DatabaseName;
                                       connectionStringBuilder.UserID = tenant.UserName;
                                       connectionStringBuilder.Password = tenant.Password;
                                       connectionStringBuilder.ConnectTimeout = 10;
                                       SqlConnection connection = new SqlConnection(connectionStringBuilder.ConnectionString);
                                       connection.Open();
                                       connection.Close();
                                       results.Add(new ListItem<string, bool>()
                                       {
                                           Value = true,
                                           Text = "The connection to " + tenant.TenantName + " DB was establish"
                                       });
                                   }
                                   catch (Exception e)
                                   {
                                       results.Add(new ListItem<string, bool>()
                                       {
                                           Value = false,
                                           Text = "The connection to " + tenant.TenantName + " DB failed to be established"
                                       });
                                   }
                               }

                               return results;
                           });
        }
    }
}
