﻿namespace FAME.Advantage.RestApi.Host.Services
{
    using Fame.EFCore.Advantage.Interfaces;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Threading.Tasks;

    public class AdvantageDbContext : IAdvantageDbContext, IDisposable
    {
        private IAdvantageContext context;
        private string simpleCacheKey;
        private bool isDisposed = false;

        public ITenantUser CurrenTenantUser { get; private set; }

        /// <summary>
        /// The get repository for entity.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public IRepository<T> GetRepositoryForEntity<T>()
            where T : class
        {
            return this.context.GetRepositoryForEntity<T>();
        }


        public DbCommand LoadStoredProc(string storedProcName)
        {
            return this.context.LoadStoredProc(storedProcName);
        }
        /// <summary>
        /// The save changes.
        /// </summary>
        public void SaveChanges()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// The get simple cache connection string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSimpleCacheConnectionString()
        {
            return this.simpleCacheKey;
        }

        /// <summary>
        /// The set tenat user.
        /// </summary>
        /// <param name="tenantUser">
        /// The tenant user.
        /// </param>
        public void SetTenantUser(ITenantUser tenantUser)
        {
            this.CurrenTenantUser = tenantUser;
        }

        /// <summary>
        /// The get tenant user.
        /// </summary>
        /// <returns>
        /// The <see cref="ITenantUser"/>.
        /// </returns>
        public ITenantUser GetTenantUser()
        {
            return this.CurrenTenantUser;
        }


        /// <summary>
        /// The set context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="simpleCacheKey">
        /// The simple cache key.
        /// </param>
        public void SetContext(IAdvantageContext context, string simpleCacheKey)
        {
            this.context = context;
            this.simpleCacheKey = simpleCacheKey;

        }

        /// <summary>
        /// The dispose.
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing && this.context != null)
                {
                    this.context.Dispose();
                }

                this.isDisposed = true;
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        public void ExecuteSqlCommand(string query, int timeout = 120, params object[] parameters)
        {
            this.context.ExecuteSqlCommand(query, timeout, parameters);
        }

        public Task ExecuteSqlCommandAsync(string query, params object[] parameters)
        {
            return this.context.ExecuteSqlCommandAsync(query, parameters);
        }


        /// <summary>
        /// Perform a raw sql on the database.
        /// </summary>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IRepository"/>.
        /// </returns>
        public List<T> RawSqlQuery<T>(string query, Func<DbDataReader, T> map)
        {
            return this.context.RawSqlQuery(query, map);
        }
    }
}
