﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TimeIntervalService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The time interval service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The time interval service.
    /// </summary>
    public class TimeIntervalService : ITimeIntervalService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeIntervalService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public TimeIntervalService(IAdvantageDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// The time interval repository.
        /// </summary>
        private IRepository<CmTimeInterval> TimeIntervalRepository => this.context.GetRepositoryForEntity<CmTimeInterval>();

        /// <summary>
        /// The get time interval difference.
        /// </summary>
        /// <param name="endTimeIntervalId">
        /// The end time interval id.
        /// </param>
        /// <param name="startTimeIntervalId">
        /// The start time interval id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<decimal> GetTimeIntervalDifference(Guid? endTimeIntervalId, Guid? startTimeIntervalId)
        {
            return await Task.Run(
                       () =>
                           {
                               var startTime = this.TimeIntervalRepository
                                   .Get(x => x.TimeIntervalId == startTimeIntervalId).FirstOrDefault();

                               var endTime = this.TimeIntervalRepository
                                   .Get(x => x.TimeIntervalId == endTimeIntervalId).FirstOrDefault();

                               if (endTime?.TimeIntervalDescrip != null && startTime?.TimeIntervalDescrip != null)
                               {
                                   var endDatetime = Convert.ToDateTime(endTime.TimeIntervalDescrip);
                                   var startDatetime = Convert.ToDateTime(startTime.TimeIntervalDescrip);

                                   return Math.Abs((endDatetime - startDatetime).Hours);
                               }
                               
                                return 0m;
                           });
        }

        /// <summary>
        /// The get all time intervals.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<TimeInterval>> GetAllTimeIntervals()
        {
            return await Task.Run(() =>
                {
                    return this.TimeIntervalRepository.Get().Select(x => new TimeInterval { Id = x.TimeIntervalId, Datetime = x.TimeIntervalDescrip }).ToList();
                });
        }
    }
}
