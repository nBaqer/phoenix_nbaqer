﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentDocumentService.cs" company="FAME Inc.">
//   FAME Inc. 2018 
// </copyright>
// <summary>
//   The Student document service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <inheritdoc />
    /// <summary>
    /// The document history service.
    /// </summary>
    public class StudentDocumentService : IStudentDocumentService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the Document status Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The requirements service.
        /// </summary>
        private readonly IRequirementsService requirementsService;

        /// <summary>
        /// The document status service.
        /// </summary>
        private readonly IDocumentStatusService documentStatusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentDocumentService"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="requirementsService">
        /// requirements Service.
        /// </param>
        /// <param name="documentStatusService">
        /// The document Status Service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public StudentDocumentService(
            IAdvantageDbContext context,
            IRequirementsService requirementsService,
            IDocumentStatusService documentStatusService,
            IMapper mapper)
        {
            this.context = context;
            this.requirementsService = requirementsService;
            this.documentStatusService = documentStatusService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The Config App Set Values repository.
        /// </summary>
        private IRepository<PlStudentDocs> StudentDocumentRepository =>
            this.context.GetRepositoryForEntity<PlStudentDocs>();

        /// <inheritdoc />
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<Document> Create(Document model)
        {
            return await Task.Run(
                       () =>
                       {
                           var document = new PlStudentDocs();
                           document.StudentDocId = Guid.NewGuid();
                           document.StudentId = model.StudentId;
                           document.DocumentId =
                               this.requirementsService.GetByCode(model.DocumentType).Result.AdReqId;
                           document.ModuleId = Convert.ToByte(model.ModuleId);
                           document.ModDate = model.ModifiedDate;
                           document.ModUser = model.ModifiedUser;
                           var status = this.documentStatusService.GetDocumentStatus(model.DocumentStatus, model.CampusId);
                           if (status != null && status.Result != null) 
                           {
                               document.DocStatusId = status.Result.DocStatusId;
                           }

                           var studentDocs = this.Get(document);
                           if (studentDocs.Result == null)
                           {
                               document.ScannedId = 0;
                               document.ReceiveDate = document.RequestDate = DateTime.Now;
                               var tempDetail = Task.Run(
                                   () => this.StudentDocumentRepository.Create(
                                       this.mapper.Map<PlStudentDocs>(document)));
                               this.StudentDocumentRepository.Save();
                               if (tempDetail.Result != null)
                               {
                                   model.DocumentId = tempDetail.Result.StudentDocId;
                                   model.ResultStatusMessage = ApiMsgs.DocumentSaveSuccess;
                               }
                               else
                               {
                                   model.ResultStatusMessage = Global.ERROR_500;
                               }
                           }
                           else
                           {
                               model.DocumentId = studentDocs.Result.StudentDocId;
                               model.ResultStatusMessage = ApiMsgs.DocumentSaveSuccess;
                           }

                           return model;
                       });
        }

        /// <summary>
        /// The get method returns the matching document from database.
        /// </summary>
        /// <param name="model">
        /// The model is type of PlStudentDocs.
        /// </param>
        /// <returns>
        /// The Document.
        /// </returns>
        private async Task<PlStudentDocs> Get(PlStudentDocs model)
        {
            return await Task.Run(
                () =>
                {
                    return this.StudentDocumentRepository.Get(
                        studentDocument => studentDocument.DocumentId == model.DocumentId
                                           && studentDocument.StudentId == model.StudentId
                                           && studentDocument.ModuleId == model.ModuleId
                                           && studentDocument.DocStatusId == model.DocStatusId).FirstOrDefault();
                });
        }
    }
}