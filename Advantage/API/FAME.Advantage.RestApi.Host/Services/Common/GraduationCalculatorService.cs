﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GraduationCalculatorService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the GraduationCalculatorService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The graduation calculator service.
    /// </summary>
    public class GraduationCalculatorService : IGraduationCalculatorService
    {
        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraduationCalculatorService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus Group Service.
        /// </param>
        public GraduationCalculatorService(IAdvantageDbContext advantageDbContext, IStatusesService statusService, ICampusGroupService campusGroupService)
        {
            this.advantageDbContext = advantageDbContext;
            this.statusService = statusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The program schedules repository.
        /// </summary>
        private IRepository<ArProgSchedules> ProgramSchedulesRepository => this.advantageDbContext.GetRepositoryForEntity<ArProgSchedules>();

        /// <summary>
        /// The holidays repository.
        /// </summary>
        private IRepository<SyHolidays> HolidaysRepository => this.advantageDbContext.GetRepositoryForEntity<SyHolidays>();


        /// <summary>
        /// The StuEnrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StuEnrollments => this.advantageDbContext.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The calculate contracted graduation date.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult<DateTime> CalculateContractedGraduationDate(Guid scheduleId, DateTime startDate, Guid? campusId)
        {
            var result = new ActionResult<DateTime>();
            if (scheduleId == Guid.Empty)
            {
                result.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Schedule Id";
                result.ResultStatus = Enums.ResultStatus.Error;
                return result;
            }

            var programSchedule = this.ProgramSchedulesRepository.Get(x => x.ScheduleId == scheduleId && x.Active == true).Include(x => x.ArProgScheduleDetails).Include(x => x.PrgVer).ThenInclude(x => x.UnitType).FirstOrDefault();

            if (programSchedule == null)
            {
                result.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }

            List<Guid> campusGroupIdList = null;

            if (campusId != null)
            {
                campusGroupIdList = this.campusGroupService.GetCampusGroupsForCampus(campusId.Value).Result.ToList();
            }
            else
            {
                campusGroupIdList = new List<Guid>();
                if (programSchedule.PrgVer.CampGrpId != null)
                {
                    campusGroupIdList.Add(programSchedule.PrgVer.CampGrpId.Value);
                }
            }

            if (!programSchedule.ArProgScheduleDetails.Any())
            {
                result.ResultStatusMessage = ApiMsgs.ScheduleDetailsNotFound;
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }

            if (!programSchedule.ArProgScheduleDetails.Any())
            {
                result.ResultStatusMessage = ApiMsgs.ScheduleDetailsNotFound;
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }

            var totalProgramHours = programSchedule.PrgVer.Hours;
            var isTimeClock = programSchedule.PrgVer.UnitType.UnitTypeDescrip == AttendanceUnitTypes.ClockHours
                              || programSchedule.PrgVer.UnitType.UnitTypeDescrip == AttendanceUnitTypes.Minutes;

            if (totalProgramHours == 0)
            {
                result.ResultStatusMessage = ApiMsgs.UnableToCalculateGradDateDueToMissingHours;
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }

            if (!isTimeClock)
            {
                result.ResultStatusMessage = ApiMsgs.UnableToCalculateGradDateProgramIsNotTimeClock;
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }

            var scheduleWeek = programSchedule.ArProgScheduleDetails;

            var contractedGraduationDate = startDate;

            Guid campusGroupAll = this.campusGroupService.GetCampusGroupAllId().Result;

            if (!campusGroupIdList.Contains(campusGroupAll))
            {
                campusGroupIdList.Add(campusGroupAll);
            }

            var activeStatusId = this.statusService.GetActiveStatusId().Result;

            var holidays = this.HolidaysRepository.Get(x => campusGroupIdList.Contains(x.CampGrpId) && x.StatusId == activeStatusId && x.HolidayStartDate >= startDate).Distinct().OrderBy(x => x.HolidayStartDate).ThenBy(x => x.HolidayEndDate).ToList();
            while (totalProgramHours > 0)
            {
                if (holidays.Any(
                    x => contractedGraduationDate >= x.HolidayStartDate
                         && contractedGraduationDate <= x.HolidayEndDate))
                {
                    contractedGraduationDate = contractedGraduationDate.AddDays(1);
                }
                else
                {
                    var dayOfTheWeek = scheduleWeek.FirstOrDefault(x => x.Dw == (int)contractedGraduationDate.DayOfWeek);
                    if (dayOfTheWeek?.Total != null)
                    {
                        totalProgramHours -= dayOfTheWeek.Total.Value;
                    }

                    if (totalProgramHours > 0)
                    {
                        contractedGraduationDate = contractedGraduationDate.AddDays(1);
                    }
                }
            }

            result.Result = contractedGraduationDate;

            return result;
        }
        public ActionResult<DateTime> CalculateContractedGraduationDate(GradDateCalculation gradDateCalculation)
        {
            var result = new ActionResult<DateTime>();

            if (gradDateCalculation == null || gradDateCalculation.CampusId == Guid.Empty || gradDateCalculation.StudentStartDate == DateTime.MinValue || gradDateCalculation.DailyHours == null || !gradDateCalculation.DailyHours.Any(dh => dh.Hours > 0))
            {
                result.ResultStatusMessage = "Missing necessary data for graduation date calculation.";
                result.ResultStatus = Enums.ResultStatus.Warning;
                return result;
            }
            var totalProgramHours = gradDateCalculation.TotalProgramHours;

            var contractedGraduationDate = gradDateCalculation.StudentStartDate;

            var activeStatusId = this.statusService.GetActiveStatusId().Result;

            var holidays = this.HolidaysRepository.Get(x => x.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == gradDateCalculation.CampusId) && x.StatusId == activeStatusId && x.HolidayStartDate >= gradDateCalculation.StudentStartDate).Distinct().OrderBy(x => x.HolidayStartDate).ThenBy(x => x.HolidayEndDate).ToList();
            while (totalProgramHours > 0)
            {
                if (holidays.Any(
                    x => contractedGraduationDate >= x.HolidayStartDate
                         && contractedGraduationDate <= x.HolidayEndDate))
                {
                    contractedGraduationDate = contractedGraduationDate.AddDays(1);
                }
                else
                {
                    var dayOfTheWeek = gradDateCalculation.DailyHours.FirstOrDefault(x => (int)x.DayOfWeek == (int)contractedGraduationDate.DayOfWeek);
                    if (dayOfTheWeek?.Hours != decimal.Zero)
                    {
                        totalProgramHours -= dayOfTheWeek.Hours;
                    }

                    if (totalProgramHours > 0)
                    {
                        contractedGraduationDate = contractedGraduationDate.AddDays(1);
                    }
                }
            }

            result.Result = contractedGraduationDate;

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="startDate">
        ///
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public ActionResult<Boolean> UpdateGraduationDate(List<string> StuEnrollIdList)
        {

            var rv = new ActionResult<Boolean>();

            try
            {

                var data = this.StuEnrollments.Get()
                    .Include(x => x.PrgVer)
                    .Include(x => x.ArStudentSchedules)
                    .ThenInclude(x => x.Schedule)
                    .ThenInclude(x => x.ArProgScheduleDetails)
                    .Where(s => StuEnrollIdList.Contains(s.StuEnrollId.ToString()))
                    .Select(a => new
                    {
                        StuEnrollId = a.StuEnrollId,
                        CampusId = a.CampusId,
                        StudentStartDate = a.StartDate.Value,
                        TotalProgramHours = a.PrgVer.Hours,
                        TotalActualHours = a.ArStudentClockAttendance.Where(c => c.ActualHours != 99.00m && c.ActualHours != 999.00m && c.ActualHours != 9999.00m).Select(t => t.ActualHours).Sum(),
                        LastDayWithAttendRecord = a.ArStudentClockAttendance.Where(c => c.ActualHours != 99.00m && c.ActualHours != 999.00m && c.ActualHours != 9999.00m).Select(t => t.RecordDate).Max(),
                        TransferHours = a.TransferHours,
                        DailyHours = a.ArStudentSchedules.SelectMany(s => s.Schedule.ArProgScheduleDetails.Select(p => new DailyHours 
                        {
                            DayOfWeek = (DayOfWeek)(p.Dw.Value == 7 ? 0 : p.Dw.Value),
                            Hours = p.Total.Value
                        })).ToList(),

                    }).ToList();


                var enrollList = this.StuEnrollments.Get()
                          .Where(s => StuEnrollIdList.Contains(s.StuEnrollId.ToString())).ToList();

                foreach (var gradDateCalculationData in data)
                {
                    var gradDatePrams = new GradDateCalculation();
                    var nextDayforStudent = gradDateCalculationData.LastDayWithAttendRecord;

                    gradDatePrams.CampusId = gradDateCalculationData.CampusId;
                    gradDatePrams.TotalProgramHours = (gradDateCalculationData.TotalProgramHours - ((gradDateCalculationData.TotalActualHours.HasValue ? gradDateCalculationData.TotalActualHours.Value : 0) + (gradDateCalculationData.TransferHours.HasValue ? gradDateCalculationData.TransferHours.Value : 0)));
                    gradDatePrams.DailyHours = gradDateCalculationData.DailyHours;
                    
                    var i = 0;
                    while (true && i <10)
                    { 
                        nextDayforStudent = nextDayforStudent.AddDays(1);
                        if (gradDateCalculationData.DailyHours.Where(d => d.DayOfWeek == nextDayforStudent.DayOfWeek && d.Hours > 0).Any())
                        {
                            break;
                        }
                        i++;
                    }

                    gradDatePrams.StudentStartDate = nextDayforStudent;
                    var gradDate = this.CalculateContractedGraduationDate(gradDatePrams);
                    var enroll = enrollList.Where(s => gradDateCalculationData.StuEnrollId == s.StuEnrollId).FirstOrDefault();
                    enroll.ExpGradDate = (DateTime?)gradDate.Result;
                    StuEnrollments.Update(enroll);

                }

                advantageDbContext.SaveChanges();


                rv.Result = true;
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = $"Successfully returned students grades.";
                return rv;
            }
            catch (Exception e)
            {
                rv.Result = false;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }

        }

        /// <summary>
        /// The get scheduled breaks for holidays returns the scheduled breaks count for the given start date, end date and campusId.
        /// </summary>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<int> GetScheduledBreaksForHolidays(DateTime? startDate, DateTime? endDate, Guid? campusId)
        {
            return await Task.Run(() =>
                           {
                               List<Guid> campusGroupIdList = null;
                               var scheduleBreakCount = 0;
                               if (campusId != null)
                               {
                                   campusGroupIdList = this.campusGroupService.GetCampusGroupsForCampus(campusId.Value).Result.ToList();
                               }

                               Guid campusGroupAll = this.campusGroupService.GetCampusGroupAllId().Result;

                               if (campusGroupIdList != null && !campusGroupIdList.Contains(campusGroupAll))
                               {
                                   campusGroupIdList.Add(campusGroupAll);
                               }

                               var holidays = this.HolidaysRepository.Get(x => campusGroupIdList.Contains(x.CampGrpId) && x.HolidayStartDate >= startDate).Distinct().OrderBy(x => x.HolidayStartDate).ThenBy(x => x.HolidayEndDate).ToList().Count;
                               if (holidays >= 5)
                               {
                                   scheduleBreakCount = holidays;
                               }

                               return scheduleBreakCount;
                           });
        }
    }
}
