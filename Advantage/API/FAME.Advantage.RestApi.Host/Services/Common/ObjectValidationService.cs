﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectValidationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ObjectValidationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Linq;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;

    /// <summary>
    /// The object validation service.
    /// </summary>
    public class ObjectValidationService : IObjectValidationService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectValidationService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        public ObjectValidationService(IAdvantageDbContext unitOfWork, IStatusesService statusService)
        {
            this.unitOfWork = unitOfWork;
            this.statusService = statusService;
        }

        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The validate object exist.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <typeparam name="TKey">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateObjectExist<T, TKey>(TKey key, out string message, string propertyName = null)
            where T : class
        {

            propertyName = string.IsNullOrEmpty(propertyName) ? typeof(T).Name : propertyName;
            var type = typeof(T);
            var statusType = typeof(SyStatuses);
            var statusProperty = type.GetProperties().FirstOrDefault(p => p.PropertyType == statusType);

            int x = 0;
            var result = SimpleCache.GetOrAddExisting(
                    $"{this.CacheKeyFirstSegment}:{propertyName}:{key.ToString()}",
                    () =>
                    {
                        var entity = this.unitOfWork.GetRepositoryForEntity<T>().GetById(key);

                        if (entity == null)
                        {
                            return false;
                        }
                        else
                        {
                            if (statusProperty != null)
                            {
                                entity = this.unitOfWork.GetRepositoryForEntity<T>().Find(key, statusProperty.Name);

                                // get actives status 
                                var activeStatusId = this.statusService.GetActiveStatusId().Result;

                                // will get the SyStatus object
                                var property = statusProperty.GetValue(entity);

                                if (property != null)
                                {
                                    // we know SyStatus object contains an StatusId
                                    var propertyStatusId = property.GetPropertyValue("StatusId");

                                    return (Guid)propertyStatusId == activeStatusId;
                                }
                            }

                            return true;
                        }
                    });

            message = !result ? propertyName + " " + ApiMsgs.NOT_FOUND : string.Empty;
            return result;
        }
    }
}

