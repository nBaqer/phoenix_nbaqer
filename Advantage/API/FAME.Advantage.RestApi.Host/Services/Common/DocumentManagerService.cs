﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentManagerService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The DocumentManagerService Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <inheritdoc />
    /// <summary>
    /// The document manager service.
    /// </summary>
    public class DocumentManagerService : IDocumentManagerService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService configurationAppSettingService;

        /// <summary>
        /// The document History service.
        /// </summary>
        private readonly IDocumentHistoryService documentHistoryService;

        /// <summary>
        /// The document History service.
        /// </summary>
        private readonly IStudentDocumentService studentDocumentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentManagerService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="userService">
        /// user service param
        /// </param>
        /// <param name="configurationAppSettingService">
        /// configurationAppSettingService param
        /// </param>
        /// <param name="documentHistoryService">
        /// The document History Service.
        /// </param>
        /// <param name="studentDocumentService">
        /// The student Document Service.
        /// </param>
        public DocumentManagerService(IAdvantageDbContext context, IUserService userService, ISystemConfigurationAppSettingService configurationAppSettingService, IDocumentHistoryService documentHistoryService, IStudentDocumentService studentDocumentService)
        {
            this.context = context;
            this.userService = userService;
            this.configurationAppSettingService = configurationAppSettingService;
            this.documentHistoryService = documentHistoryService;
            this.studentDocumentService = studentDocumentService;
        }

        /// <inheritdoc />
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The Document model.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<Document> Create(Document model)
        {
            if (model != null)
            {
                var settingValue = this.configurationAppSettingService.GetAppSettingValue(model.DocumentType);
                            
                    if (settingValue != null)
                    {
                        if (model.DocumentContent != null && settingValue != string.Empty)
                        {
                            try
                            {
                                if (!Directory.Exists(settingValue))
                                {
                                    Directory.CreateDirectory(settingValue);
                                }
                               
                                FileStream file = File.Create(
                                    settingValue + model.FileName + model.FileExtension);
                                file.Write(model.DocumentContent, 0, model.DocumentContent.Length);
                                file.Close();
                                model.DocumentId = Guid.NewGuid();
                                var userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);
                                model.ModifiedUser = userDetails.FullName;
                                model.ModifiedDate = DateTime.Now;
                                await this.documentHistoryService.Create(model);
                                await this.studentDocumentService.Create(model);
                            }
                            catch (Exception e)
                            {
                               e.TrackException();
                               model.ResultStatusMessage = Global.ERROR_500;
                               model.ResultStatus = Enums.ResultStatus.Error;
                            }
                        }
                    }
                    else
                    {
                        model.ResultStatusMessage = Global.ErrorMissingConfiguration + model.DocumentType;
                        model.ResultStatus = Enums.ResultStatus.Error;
                    }

                return model;
            }
            else
            {
                Document result = new Document();
                result.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                result.ResultStatus = Enums.ResultStatus.Error;
                return result;
            }
        }
    }
}
