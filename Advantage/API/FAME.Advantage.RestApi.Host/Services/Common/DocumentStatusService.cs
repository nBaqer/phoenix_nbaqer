﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentStatusService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Requirements service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog; 

    /// <inheritdoc />
    /// <summary>
    /// The Document Status Service .
    /// </summary>
    public class DocumentStatusService : IDocumentStatusService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the Document status Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The campus group campus service.
        /// </summary>
        private readonly ICampusGroupCampusService campusGroupCampusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentStatusService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="campusGroupCampusService">
        /// The campus group campus service.
        /// </param>
        public DocumentStatusService(IAdvantageDbContext context, IMapper mapper, ICampusGroupCampusService campusGroupCampusService)
        {
            this.context = context;
            this.mapper = mapper;
            this.campusGroupCampusService = campusGroupCampusService;
        }

        /// <summary>
        /// The adreqs repository.
        /// </summary>
        private IRepository<SyDocStatuses> SyDocStatusRepository =>
            this.context.GetRepositoryForEntity<SyDocStatuses>();

        /// <summary>
        /// The get document status by status.
        /// </summary>
        /// <param name="status">
        ///     The status.
        /// </param>
        /// <param name="campusId">
        ///     The Campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<SyDocStatuses> GetDocumentStatus(int status, Guid campusId)
        {
            return await Task.Run(
                       () =>
                           {
                               var campusGroup = this.campusGroupCampusService.GetCampusGroupByCampusId(campusId)?.Result;
                               return this.mapper.Map<SyDocStatuses>(this.SyDocStatusRepository.Get(ds => ds.SysDocStatusId == status && (ds.CampGrpId == campusGroup.CampGrpId || (ds.CampGrp.IsAllCampusGrp.HasValue && ds.CampGrp.IsAllCampusGrp.Value))).FirstOrDefault());
                           });
        }
    }
}