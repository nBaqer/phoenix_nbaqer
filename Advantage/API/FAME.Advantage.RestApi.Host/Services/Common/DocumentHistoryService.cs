﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentHistoryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The document history service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Common
{
    using System;
    using System.Threading.Tasks;
     
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <inheritdoc />
    /// <summary>
    /// The document history service.
    /// </summary>
    public class DocumentHistoryService : IDocumentHistoryService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The requirements service.
        /// </summary>
        private readonly IRequirementsService requirementsService;

        /// <summary>
        /// The program version service.
        /// </summary>
        private readonly IProgramVersionsService programVersionService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentHistoryService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="requirementsService">
        /// The requirements Service .
        /// </param>
        /// <param name="programVersionService">
        /// The programVersion Service .
        /// </param>
        /// <param name="userService">
        /// The user Service.
        /// </param>
        public DocumentHistoryService(IAdvantageDbContext context, IRequirementsService requirementsService, IProgramVersionsService programVersionService, IUserService userService)
        {
            this.context = context;
            this.requirementsService = requirementsService;
            this.programVersionService = programVersionService;
            this.userService = userService;
        }

        /// <summary>
        /// The Config App Set Values repository.
        /// </summary>
        private IRepository<SyDocumentHistory> SyDocumentHistoryRepository => this.context.GetRepositoryForEntity<SyDocumentHistory>();

        /// <inheritdoc />
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<Guid> Create(Document model)
        {
            return await Task.Run(
                       () =>
                           {
                               SyDocumentHistory document = new SyDocumentHistory();
                               document.FileId = model.FileId;
                               document.StudentId = model.StudentId;
                               document.DisplayName = model.DisplayName;
                               document.DocumentType = model.DocumentType;
                               document.FileExtension = model.FileExtension;
                               document.FileName = model.FileName;
                               document.DocumentId = this.requirementsService.GetByCode(model.DocumentType).Result.AdReqId;
                               document.ModuleId = model.ModuleId;
                               document.ModDate = model.ModifiedDate;
                               document.ModUser = model.ModifiedUser;
                               document.DisplayName = model.DisplayName;
                               document.IsArchived = false;
                               this.SyDocumentHistoryRepository.CreateAsync(document);
                               return document.FileId;
                           });
        }
         
        /// <summary>
        /// The Update action method updates document history for a specific enrollment and student id.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param>
        /// <param name="studentId">
        /// student Id
        /// </param>
        /// <returns>
        /// The <see cref="Boolean"/>.
        /// </returns>
        public async Task<bool> Update(Guid enrollmentId, Guid studentId)
        {
            return await Task.Run(async () =>
                          {
                              try
                              {
                                  var programDescription = await this.programVersionService.GetProgramCodeByEnrollmentId(enrollmentId, studentId);
                                  var studentDocHistory = this.SyDocumentHistoryRepository.Get(
                                       e => e.StudentId == studentId && e.DisplayName.Contains(programDescription.ToString())
                                                                     && (e.Document.Code.Contains(
                                                                             DocumentType.R2T4DocumentType)
                                                                         || e.Document.Code.Contains(
                                                                             DocumentType.TerminatiionDetail)));

                                  var userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);
                                 
                                  if (studentDocHistory != null)
                                  {
                                      foreach (var docHistory in studentDocHistory)
                                      {
                                          docHistory.IsArchived = true;
                                          docHistory.ModUser = userDetails.FullName;
                                          docHistory.ModDate = DateTime.Now;
                                          this.SyDocumentHistoryRepository.Update(docHistory);
                                      }
                                  }

                                  return true;
                              }
                              catch (Exception e)
                              {
                                  e.TrackException();
                                  return false;
                              }
                          });
        }
    }
}