﻿

using System.Data;
using AutoMapper;
using Fame.EFCore.Advantage.Entities;
using Fame.EFCore.Interfaces;
using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
using FAME.Advantage.RestApi.Host.Infrastructure;
using FAME.Advantage.RestApi.Host.Services.Interfaces;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using Fame.EFCore.Extensions;
using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
using FAME.Advantage.RestApi.Host.Services.Interfaces.FinancialAid;
using FAME.Orm.Advantage.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace FAME.Advantage.RestApi.Host.Services.FinacialAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Terms;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;

    /// <summary>
    /// The Title IV Sap check service class for running Title IV core logic.
    /// </summary>
    public class TitleIVSapCheckService : SapCheckService, ITitleIVSapCheckService
    {
        private readonly ITitleIVSAPService titleIVSapService;
        private readonly IProgramVersionsService programVersionsService;
        private readonly ICampusService campusService;
        private readonly IAfaService afaService;
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIVSapCheckService"/> class.
        /// </summary>
        /// <param name="serviceBuilder">
        /// The service builder.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="appSettingService">
        /// The app setting service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="termService">
        /// The term service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        /// <param name="titleIVSapService">
        /// The title IV Sap service.
        /// </param>
        /// <param name="programVersionsService">
        /// The title IV Sap service.
        /// </param>
        public TitleIVSapCheckService(IServiceBuilder serviceBuilder, IMapper mapper, ISystemConfigurationAppSettingService appSettingService
            , IStatusesService statusService, ITermService termService, IEnrollmentService enrollmentService, ITitleIVSAPService titleIVSapService, IProgramVersionsService programVersionsService, ICampusService campusService, IAfaService afaService)
            : base(serviceBuilder, mapper, appSettingService, statusService, termService, enrollmentService)
        {
            this.titleIVSapService = titleIVSapService;
            this.programVersionsService = programVersionsService;
            this.campusService = campusService;
            this.afaService = afaService;
        }
        private string UserEmail => context.GetTenantUser().Email;
        private IRepository<ArFasapchkResults> ArTitleIvsapCheckResultsRepository => this.context.GetRepositoryForEntity<ArFasapchkResults>();
        /// <summary>
        /// The method used to run the core logic for the sap check process.
        /// Does additional validations to check if the increment should be run.
        /// Determines if student is passing Title Iv qualitative and quantitative requirements based on Increment.
        /// </summary>
        public override async Task Run()
        {
            foreach (var enrollment in StudentEnrollments)
            {
                decimal iNormalProgLength;
                decimal iMaxTimeFrame;
                decimal creditsAttempted = 0;

                List<SAPPolicyDetail> sapPolicyDetails = new List<SAPPolicyDetail>();
                if (enrollment.IsTitleIV)
                {
                    enrollment.PolicyDetails = this.GetSAPPolicyDetails(enrollment.TitleIVSapId.Value);

                    if (RunAllIncrements)
                    {
                        sapPolicyDetails = enrollment.PolicyDetails.OrderBy(pd => pd.Increment).ToList();

                        foreach (var pd in sapPolicyDetails)
                        {
                            if (!await this.RunTitleIVLogic(enrollment, pd.Increment))
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        var runningIncrement = 0;
                        if (IncrementToRun.HasValue && IncrementToRun != 0)
                        {
                            runningIncrement = IncrementToRun.Value;

                            if (runningIncrement != 1)
                            {
                                var prev = GetPreviouslyRunResult(enrollment.EnrollmentId, runningIncrement);

                                if (string.IsNullOrEmpty(prev))
                                {
                                    InvalidResults.Add(new SAPResult()
                                    {
                                        StudentEnrollmentId = enrollment.EnrollmentId,
                                        Passed = false,
                                        FailedMessage = "The previous increment for this student has not been run."
                                    });
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            runningIncrement = this.GetLastRunIncrement(enrollment.EnrollmentId) + 1;
                        }

                        await this.RunTitleIVLogic(enrollment, runningIncrement);
                    }

                }

            }
        }
        /// <summary>
        /// The run Title IV Core Logic.
        /// </summary>
        /// <param name="programVersionSap">
        /// The program version SAP object.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="runningIncrement">
        /// The increment to be run.
        /// </param>
        private async Task<bool> RunTitleIVLogic(ProgramVersionSAP enrollment, int runningIncrement)
        {
            DateTime runTime = DateTime.Now;
            DateTime? offsetDate = null;
            List<Term> terms = new List<Term>();

            var sapDetail = enrollment.PolicyDetails.FirstOrDefault(sd => sd.Increment == runningIncrement);
            if (sapDetail != null)
            {
                if (!sapDetail.TriggerValue.HasValue)
                {
                    InvalidResults.Add(new SAPResult()
                    {
                        StudentEnrollmentId = enrollment.EnrollmentId,
                        Passed = false,
                        FailedMessage = "Could Not determine trigger value for policy detail"
                    });
                    return false;
                }
                dynamic temp = new { OverallAttendance = false };
                if (!sapDetail.QualitativeMinimumValue.HasValue)
                {
                    temp.OverallAttendance = true;
                }

                if (sapDetail.TriggerOffsetTypeId == (int)SAPTriggerOffsets.StartOfTerm ||
                    sapDetail.TriggerOffsetTypeId == (int)SAPTriggerOffsets.EndOfTerm)
                {

                    if (!sapDetail.TriggerOffsetSequence.HasValue)
                    {
                        InvalidResults.Add(new SAPResult()
                        {
                            StudentEnrollmentId = enrollment.EnrollmentId,
                            Passed = false,
                            FailedMessage = "Trigger offset must have value for start of term or end of term trigger types."
                        });
                        return false;
                    }

                }
                terms = this.GetTerms(enrollment.EnrollmentId);

                var count = terms.Count;

                bool isValid = true;

                if (sapDetail.TriggerOffsetTypeId == (int)SAPTriggerOffsets.StartOfTerm || sapDetail.TriggerOffsetTypeId == (int)SAPTriggerOffsets.EndOfTerm)
                {
                    if (!enrollment.IsByProgramRegistration)
                    {
                        if (terms.Any(term => !term.StartDate.HasValue || !term.EndDate.HasValue))
                        {
                            InvalidResults.Add(new SAPResult()
                            {
                                StudentEnrollmentId = enrollment.EnrollmentId,
                                Passed = false,
                                FailedMessage = "All terms must have a start date and end date."
                            });
                            return false;
                        }
                    }

                }
                switch (sapDetail.TriggerOffsetTypeId)
                {

                    case (int)SAPTriggerOffsets.StartOfTerm:
                        {
                            if (count >= sapDetail.TriggerOffsetSequence)
                            {
                                var term = terms.ElementAt(sapDetail.TriggerOffsetSequence.Value - 1);

                                if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Days)
                                {
                                    offsetDate = term.StartDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100));

                                }
                                else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Weeks)
                                {
                                    offsetDate = term.StartDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100) * 7);

                                }

                                if (offsetDate.HasValue && offsetDate > runTime)
                                {
                                    InvalidResults.Add(new SAPResult()
                                    {
                                        StudentEnrollmentId = enrollment.EnrollmentId,
                                        Passed = false,
                                        FailedMessage = "It is not yet time to check this increment."
                                    });
                                    isValid = false;
                                }

                            }
                            break;
                        }


                    case (int)SAPTriggerOffsets.EndOfTerm:
                        {
                            if (count >= sapDetail.TriggerOffsetSequence)
                            {
                                var term = terms.ElementAt(sapDetail.TriggerOffsetSequence.Value - 1);

                                if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Days)
                                {
                                    offsetDate = term.EndDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100));

                                }
                                else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Weeks)
                                {
                                    offsetDate = term.EndDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100) * 7);

                                }
                                else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Months)
                                {
                                    offsetDate = term.EndDate?.AddMonths(sapDetail.TriggerValue.Value / 100);

                                }
                                if (offsetDate.HasValue && offsetDate > runTime)
                                {
                                    InvalidResults.Add(new SAPResult()
                                    {
                                        StudentEnrollmentId = enrollment.EnrollmentId,
                                        Passed = false,
                                        FailedMessage = "It is not yet time to check this increment."
                                    });
                                    isValid = false;
                                }

                            }
                            break;
                        }

                    case (int)SAPTriggerOffsets.StartDate:
                        {
                            var startDate = this.GetStudentStartDate(enrollment.EnrollmentId);
                            var addLoAs = false;
                            if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Days)
                            {
                                offsetDate = startDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100));
                                addLoAs = true;
                            }
                            else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Weeks)
                            {
                                offsetDate = startDate?.AddDays(((double)sapDetail.TriggerValue.Value / 100) * 7);
                                addLoAs = true;
                            }
                            else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.Months)
                            {
                                offsetDate = startDate?.AddMonths(sapDetail.TriggerValue.Value / 100);
                                addLoAs = true;
                            }

                            else if (sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.ActualHours
                                     || sapDetail.TriggerUnitTypeId == (int)SAPTriggerUnits.CreditsAttempted)
                            {

                                try
                                {
                                    var triggerMetDateCommand = await context
                                        .LoadStoredProc("USP_TitleIV_GetTriggerMetDate")
                                        .WithSqlParam("@StuEnrollId", enrollment.EnrollmentId, DbType.Guid)
                                        .WithSqlParam("@StuEnrollCampusId", enrollment.CampusId, DbType.Guid)
                                        .WithSqlParam("@TriggerValue", sapDetail.TriggerValue / 100, DbType.Decimal)
                                        .WithSqlParam("@TriggerUnitTypeId", sapDetail.TriggerUnitTypeId, DbType.Int32)
                                        .WithSqlParam("@IncludeExternship", enrollment.TrackExternshipAttendance,
                                            DbType.Boolean)
                                        .WithSqlParam("@IncludeTransferHours", enrollment.IncludeTransferHours,
                                            DbType.Boolean)
                                        .WithSqlParam("@TriggerMetDate", DateTime.Today, DbType.DateTime,
                                            ParameterDirection.Output)
                                        .ExecuteStoredProcAsync();
                                    offsetDate = triggerMetDateCommand.GetOutputParameter<DateTime?>("@TriggerMetDate");

                                    triggerMetDateCommand.Close();

                                    if (!offsetDate.HasValue)
                                    {
                                        await MarkIncrementInactiveIfExists(enrollment.EnrollmentId, sapDetail.Increment);

                                        InvalidResults.Add(new SAPResult()
                                        {
                                            StudentEnrollmentId = enrollment.EnrollmentId,
                                            Passed = false,
                                            FailedMessage = "It is not yet time to check this increment."
                                        });
                                        return false;
                                    }
                                }
                                catch (Exception e)
                                {
                                    if (!offsetDate.HasValue)
                                    {
                                        InvalidResults.Add(new SAPResult()
                                        {
                                            StudentEnrollmentId = enrollment.EnrollmentId,
                                            Passed = false,
                                            FailedMessage = "It is not yet time to check this increment."
                                        });
                                        return false;
                                    }

                                    e.TrackException();
                                }

                            }


                            if (addLoAs)
                            {
                                var studentLoAs = this.GetStudentLOAs(enrollment.EnrollmentId);

                                foreach (var loa in studentLoAs)
                                {
                                    if (loa.StartDate.HasValue && loa.ReturnDate.HasValue)
                                    {
                                        var numOfLoADays = (loa.StartDate.Value - loa.ReturnDate.Value).TotalDays;
                                        offsetDate = offsetDate?.AddDays(numOfLoADays);
                                    }
                                }
                            }

                            if (offsetDate.HasValue && offsetDate > runTime)
                            {

                                InvalidResults.Add(new SAPResult()
                                {
                                    StudentEnrollmentId = enrollment.EnrollmentId,
                                    Passed = false,
                                    FailedMessage = "It is not yet time to check this increment."
                                });
                                isValid = false;
                            }

                            var numOfTermsInRange =
                                this.GetStudentTermsFromStartDate(terms, startDate.Value, offsetDate.Value).Count;
                            if (numOfTermsInRange < 1 &&
                                sapDetail.TriggerUnitTypeId != (int)SAPTriggerUnits.ActualHours &&
                                sapDetail.TriggerUnitTypeId != (int)SAPTriggerUnits.CreditsAttempted
                                && sapDetail.TriggerOffsetTypeId != (int)SAPTriggerOffsets.StartDate)
                            {
                                InvalidResults.Add(new SAPResult()
                                {
                                    StudentEnrollmentId = enrollment.EnrollmentId,
                                    Passed = false,
                                    FailedMessage = "Student has not met the trigger yet."
                                });
                                isValid = false;
                            }

                            break;
                        }

                    default:
                        break;
                }

                if (!isValid)
                {
                    await MarkIncrementInactiveIfExists(enrollment.EnrollmentId, sapDetail.Increment);
                    return false;
                }

                var result = await this.SatisfiesTitleIV(enrollment.EnrollmentId, sapDetail, offsetDate.Value);

                if (result == null)
                {
                    InvalidResults.Add(new SAPResult()
                    {
                        StudentEnrollmentId = enrollment.EnrollmentId,
                        Passed = false,
                        FailedMessage = "Student qualitative/quantitative values could not be calculated."
                    });
                    return false;
                }
                ValidResults.Add(result);
                return true;
            }

            return true;
        }

        /// <summary>
        /// Saves all valid title IV results to the database asynchronously.
        /// </summary>
        public override async Task Save()
        {
            var now = DateTime.Now;
            foreach (var res in ValidResults)
            {
                var update = false;
                var titleIVStatusId = this.GetTitleIVSapStatusId(res);
                if (!titleIVStatusId.HasValue) continue;
                var titleIVResult = titleIVSapService.GetTitleIVResults(res.StudentEnrollmentId)
                    .FirstOrDefault(t4r => t4r.Period == res.SapDetail.Increment);

                if (titleIVResult != null)
                {
                    titleIVResult.IsMakingSap = res.Passed;
                    titleIVResult.TitleIvstatusId = titleIVStatusId.Value;
                    titleIVResult.DatePerformed = now;
                    titleIVResult.CheckPointDate = res.CheckpointDate;
                    titleIVResult.HrsEarned = res.ActualHours;
                    titleIVResult.HrsAttended = res.ScheduledHours;
                    titleIVResult.StuEnrollId = res.StudentEnrollmentId;
                    titleIVResult.PreviewSapCheck = false;
                    titleIVResult.SapdetailId = res.SapDetail.Id;
                    titleIVResult.Period = res.SapDetail.Increment;
                    titleIVResult.Comments = res.PassedMessage;
                    titleIVResult.PercentCompleted = res.Quantitative;
                    titleIVResult.ModDate = now;
                    titleIVResult.ModUser = UserEmail;
                    titleIVResult.StatusId = this.ActiveId;

                    update = true;

                }
                else
                {
                    titleIVResult = new ArFasapchkResults()
                    {
                        IsMakingSap = res.Passed,
                        TitleIvstatusId = titleIVStatusId.Value,
                        DatePerformed = now,
                        CheckPointDate = res.CheckpointDate,
                        HrsEarned = res.ActualHours,
                        HrsAttended = res.ScheduledHours,
                        StuEnrollId = res.StudentEnrollmentId,
                        PreviewSapCheck = false,
                        SapdetailId = res.SapDetail.Id,
                        Period = res.SapDetail.Increment,
                        Comments = res.PassedMessage,
                        PercentCompleted = res.Quantitative,
                        ModDate = now,
                        ModUser = UserEmail,
                        StatusId = this.ActiveId

                    };
                }

                if (GradesFormat != null && GradesFormat.ToLower() == Infrastructure.GradesFormat.Numeric.ToLower())
                {
                    titleIVResult.Average = res.Qualitative;
                }
                else
                {
                    titleIVResult.Gpa = res.Qualitative;
                }

                if (update)
                {
                    ArTitleIvsapCheckResultsRepository.Update(titleIVResult);
                }
                else
                {
                    await ArTitleIvsapCheckResultsRepository.CreateAsync(titleIVResult);
                }
                await ArTitleIvsapCheckResultsRepository.SaveAsync();
            }
        }
        /// <summary>
        /// Updates AFA with latest title iv sap statuses
        /// </summary>
        /// <returns></returns>
        public override async Task Update()
        {
            var afaIntegrationEnabled = false;
            if (CampusId.HasValue)
            {
                afaIntegrationEnabled = this.campusService.IsAFAIntegrationEnabled(CampusId.Value);
            }

            if (afaIntegrationEnabled)
            {
                var attendanceUpdates = await this.afaService.CalculateAttendanceUpdatesForEnrollments(StudentEnrollments.Select(se => se.EnrollmentId));
                await this.afaService.PostAttendanceUpdatesToAfa(attendanceUpdates);
            }
        }
        /// <summary>
        /// Mark increment inactive if exists.
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <param name="increment"></param>
        /// <returns></returns>
        public async Task MarkIncrementInactiveIfExists(Guid stuEnrollmentId, int increment)
        {

            var results = ArTitleIvsapCheckResultsRepository.Get(t4res =>
                t4res.StuEnrollId == stuEnrollmentId && t4res.Period >= increment).ToList();

            if (results.Any())
            {
                foreach (var result in results)
                {
                    result.StatusId = this.InactiveId;
                }
                ArTitleIvsapCheckResultsRepository.Update(results);
                await ArTitleIvsapCheckResultsRepository.SaveAsync();
            }

        }

        /// <summary>
        /// The get title IV Status id based on result.
        /// </summary>
        /// <param name="result">
        /// The SAP result.
        /// </param>
        private int? GetTitleIVSapStatusId(SAPResult result)
        {
            return this.titleIVSapService.GetTitleIVSapStatusId(result);
        }
        /// <summary>
        /// Calculates the quantiative and qualitative values for the student enrollment and checks if the student satisfies Title IV.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The Student enrollment id.
        /// </param>
        /// <param name="sapDetail">
        /// The SAP detail.
        /// </param>
        /// <param name="offsetDate">
        /// The offset date.
        /// </param>
        public async Task<SAPResult> SatisfiesTitleIV(Guid stuEnrollId, SAPPolicyDetail sapDetail, DateTime offsetDate)
        {
            try
            {
                var resultMessage = "";

                var qualAndQuantCommand = (await context
                    .LoadStoredProc("[dbo].[USP_TitleIV_QualitativeAndQuantitative]")
                    .WithSqlParam("@StuEnrollId", stuEnrollId, DbType.Guid)
                    .WithSqlParam("@QuantMinUnitTypeId", sapDetail.QuantitativeMinimumUnitTypeId, DbType.Int32)
                    .WithSqlParam("@OffsetDate", offsetDate, DbType.DateTime)
                    .WithSqlParam("@CumAverage", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@cumWeightedGPA", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@cumSimpleGPA", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@Qualitative", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@Quantitative", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@ActualHours", 0, DbType.Decimal, ParameterDirection.Output)
                    .WithSqlParam("@ScheduledHours", 0, DbType.Decimal, ParameterDirection.Output)

                    .ExecuteStoredProcAsync());

                var res = new SAPResult();
                res.Qualitative = qualAndQuantCommand.GetOutputParameter<decimal>("@Qualitative");
                res.Quantitative = qualAndQuantCommand.GetOutputParameter<decimal>("@Quantitative");
                res.ActualHours = qualAndQuantCommand.GetOutputParameter<decimal>("@ActualHours");
                res.ScheduledHours = qualAndQuantCommand.GetOutputParameter<decimal>("@ScheduledHours");

                qualAndQuantCommand.Close();
                res.StudentEnrollmentId = stuEnrollId;
                res.SapDetail = sapDetail;
                res.Passed = true;
                res.CheckpointDate = offsetDate;

                if (res.Qualitative >= sapDetail.QualitativeMinimumValue)
                {
                    if (GradesFormat != null && GradesFormat.ToLower() == Infrastructure.GradesFormat.Numeric.ToLower())
                    {
                        res.PassedMessage = TitleIVResultMessages.NUMERIC_PREFIX + decimal.Round(res.Qualitative, 2) +
                                            TitleIVResultMessages.PASSED_SUFFIX +
                                            decimal.Round(sapDetail.QualitativeMinimumValue.Value, 2) + ".";
                    }
                    else
                    {
                        res.PassedMessage = TitleIVResultMessages.CGPA_PREFIX + decimal.Round(res.Qualitative, 2) +
                                            TitleIVResultMessages.PASSED_SUFFIX +
                                            decimal.Round(sapDetail.QualitativeMinimumValue.Value, 2) + ".";
                    }
                }
                else
                {
                    res.Passed = false;
                    if (GradesFormat != null && GradesFormat.ToLower() == Infrastructure.GradesFormat.Numeric.ToLower())
                    {
                        res.PassedMessage = TitleIVResultMessages.NUMERIC_PREFIX + decimal.Round(res.Qualitative, 2) +
                                            TitleIVResultMessages.FAILED_SUFFIX +
                                            decimal.Round(sapDetail.QualitativeMinimumValue.Value, 2) + ".";
                    }
                    else
                    {
                        res.PassedMessage = TitleIVResultMessages.CGPA_PREFIX + decimal.Round(res.Qualitative, 2) +
                                            TitleIVResultMessages.FAILED_SUFFIX +
                                            decimal.Round(sapDetail.QualitativeMinimumValue.Value, 2) + ".";
                    }
                }

                if (res.Quantitative >= sapDetail.QuantitativeMinimumValue)
                {
                    if (!string.IsNullOrEmpty(res.PassedMessage))
                    {
                        res.PassedMessage += "\n ";
                    }

                    if (sapDetail.QuantitativeMinimumUnitTypeId == (int)(SAPQuantitativeUnits.CreditsAttempted))
                    {
                        res.PassedMessage += TitleIVResultMessages.PREFIX + decimal.Round(res.Quantitative, 2) +
                                             TitleIVResultMessages.CREDITS_ATTEMPTED_QUANT +
                                             TitleIVResultMessages.PASSED_SUFFIX +
                                             decimal.Round(sapDetail.QuantitativeMinimumValue.Value, 2) + "%.";
                    }
                    else
                    {
                        res.PassedMessage += TitleIVResultMessages.PREFIX + decimal.Round(res.Quantitative, 2) +
                                             TitleIVResultMessages.SCHEDULED_HOURS_QUANT +
                                             TitleIVResultMessages.PASSED_SUFFIX +
                                             decimal.Round(sapDetail.QuantitativeMinimumValue.Value, 2) + "%.";

                    }
                }
                else
                {
                    res.Passed = false;

                    if (!string.IsNullOrEmpty(res.FailedMessage))
                    {
                        res.PassedMessage += "\n ";
                    }

                    if (sapDetail.QuantitativeMinimumUnitTypeId == (int)(SAPQuantitativeUnits.CreditsAttempted))
                    {
                        res.PassedMessage += TitleIVResultMessages.PREFIX + decimal.Round(res.Quantitative, 2) +
                                             TitleIVResultMessages.CREDITS_ATTEMPTED_QUANT +
                                             TitleIVResultMessages.FAILED_SUFFIX +
                                             decimal.Round(sapDetail.QuantitativeMinimumValue.Value, 2) + "%.";
                    }
                    else
                    {
                        res.PassedMessage += TitleIVResultMessages.PREFIX + decimal.Round(res.Quantitative, 2) +
                                             TitleIVResultMessages.SCHEDULED_HOURS_QUANT +
                                             TitleIVResultMessages.FAILED_SUFFIX +
                                             decimal.Round(sapDetail.QuantitativeMinimumValue.Value, 2) + "%.";
                    }
                }

                return res;
            }

            catch (Exception e)
            {
                e.TrackException();
                return null;
            }
        }
        /// <summary>
        /// Validate paramaters passed to execution function and check if title IV is applicable
        /// for enrollments/ program version to verify which path to take.
        /// </summary>
        public override async Task Validate()
        {
            if (!(this.ProgramVersionId == null && (this.StudentEnrollmentIds == null || this.StudentEnrollmentIds.Count == 0)))
            {
                if (await CheckIfTitleIVApplicable())
                {
                    await BuildEnrollmentList();
                }
            }

        }
        /// <summary>
        /// Builds enrollment list to iterate over based on passed in parameters.
        /// </summary>
        private async Task BuildEnrollmentList()
        {
            if (StudentEnrollmentIds != null && StudentEnrollmentIds.Count > 0)
            {
                var enrollments = await this.enrollmentService
                    .Get(StudentEnrollmentIds);


                this.StudentEnrollments =
                    enrollments.Where(_ => _.EnrollmentStartDate <= DateTime.Now && (this.AllowAnyStatus ? true : ValidStudentStatusId.Contains((Constants.SystemStatus)_.SysStatusId)) && _.IsTitleIV)
                    .ToList();
            }
            else if (ProgramVersionId.HasValue)
            {
                var enrollments = await this.enrollmentService
                    .GetByProgramVersion(ProgramVersionId.Value, this.AllowAnyStatus ? null : ValidStudentStatusId);
                this.StudentEnrollments = enrollments
                    .ToList();
            }
        }

        /// <summary>
        /// The get last run increment.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        private int GetLastRunIncrement(Guid stuEnrollId)
        {
            return this.titleIVSapService.GetLastRunIncrement(stuEnrollId);
        }
        /// <summary>
        /// The get the previously run result for the enrollment id.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        /// <param name="increment">
        /// The increment to run.
        /// </param>
        private string GetPreviouslyRunResult(Guid stuEnrollId, int increment)
        {
            return this.titleIVSapService.GetPreviouslyRunResult(stuEnrollId, increment);
        }

        /// <summary>
        /// Check if Title IV applicable.
        /// </summary>
        private async Task<bool> CheckIfTitleIVApplicable()
        {

            if (StudentEnrollmentIds != null && StudentEnrollmentIds.Count > 0)
            {
                var listOfEnrollments = await this.enrollmentService.Get(StudentEnrollmentIds);

                var programVersions = listOfEnrollments.GroupBy(pv => pv.Id)
                    .SelectMany(group => group.ToList()).ToList();
                if (programVersions.All(pv => pv.TitleIVSapId == null))
                {
                    return false;
                }
            }

            else if (ProgramVersionId.HasValue)
            {
                var prgVer = await this.programVersionsService.GetByProgramVersionId(ProgramVersionId.Value);

                if (!prgVer.IsTitleIV)
                    return false;
            }
            return true;
        }
    }
}
