﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SapCheck.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The SapCheck abstract class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Threading.Tasks;
using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
using FAME.Orm.Advantage.Domain.Common;
using Microsoft.EntityFrameworkCore;

namespace FAME.Advantage.RestApi.Host.Services.FinacialAid
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
    using FAME.Advantage.RestApi.DataTransferObjects.Terms;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The sap check base Service class.
    /// </summary>
    public abstract class SapCheckService
    {

        /// <summary>
        /// The System Configuration AppSetting Service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingService;
        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The term service.
        /// </summary>
        private readonly ITermService termService;
        /// <summary>
        /// The sys status codes service.
        /// </summary>
        protected readonly IStatusesService statusService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        protected readonly IEnrollmentService enrollmentService;
        /// <summary>
        /// The db context.
        /// </summary>
        protected readonly IAdvantageDbContext context;

        /// <summary>
        /// The student enrollments to process.
        /// </summary>
        protected IList<ProgramVersionSAP> StudentEnrollments { get; set; } = new List<ProgramVersionSAP>();
        /// <summary>
        /// The list of valid student statuses.
        /// </summary>
        protected readonly List<Constants.SystemStatus> ValidStudentStatusId = new List<Constants.SystemStatus>() { Constants.SystemStatus.CurrentlyAttending, Constants.SystemStatus.AcademicProbation, Constants.SystemStatus.Externship };
        /// <summary>
        /// The program version to pull student enrollments from if none are passed (optional).
        /// </summary>
        protected Guid? ProgramVersionId { get; private set; }
        /// <summary>
        /// The list of student enrollment ids to process (optional).
        /// </summary>
        protected IList<Guid> StudentEnrollmentIds { get; private set; }
        /// <summary>
        /// The campus id.
        /// </summary>
        protected Guid? CampusId { get; private set; }
        /// <summary>
        /// The increment to run (optional).
        /// </summary>
        protected int? IncrementToRun { get; private set; }
        /// <summary>
        /// The flag to run all increments (defaults to false).
        /// </summary>
        protected bool RunAllIncrements { get; private set; }
        /// <summary>
        /// The flag to indicate if any status should be considered when recalculating.
        /// </summary>
        protected bool AllowAnyStatus { get; private set; }
        /// <summary>
        /// The list of invalid results.
        /// </summary>
        protected List<SAPResult> InvalidResults = new List<SAPResult>();
        /// <summary>
        /// The list of valid results.
        /// </summary>
        protected List<SAPResult> ValidResults = new List<SAPResult>();

        /// <summary>
        /// The template method used to validate the sap check process.
        /// </summary>
        public abstract Task Validate();
        /// <summary>
        /// The template method used to run the core logic for the sap check process.
        /// </summary>
        public abstract Task Run();
        /// <summary>
        /// The template method used to save the valid results after running the sap check process.
        /// </summary>
        public abstract Task Save();
        /// <summary>
        /// The template method used to update other processes after saving data.
        /// </summary>
        public abstract Task Update();
        /// <summary>
        /// Initializes a new instance of the <see cref="SapCheckService"/> class.
        /// </summary>
        /// <param name="serviceBuilder">
        /// The service builder.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="appSettingService">
        /// The app setting service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="termService">
        /// The term service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        protected SapCheckService(IServiceBuilder serviceBuilder, IMapper mapper,
            ISystemConfigurationAppSettingService appSettingService, IStatusesService statusService, ITermService termService, IEnrollmentService enrollmentService)
        {
            this.serviceBuilder = serviceBuilder;
            this.context = this.serviceBuilder.GetServiceInstance<IAdvantageDbContext>();
            this.mapper = mapper;
            this.appSettingService = appSettingService;
            this.statusService = statusService;
            this.termService = termService;
            this.enrollmentService = enrollmentService;
        }
        /// <summary>
        /// The student enrollments repository.
        /// </summary>
        protected IRepository<ArStuEnrollments> ArStuEnrollmentsRepository =>
            this.context.GetRepositoryForEntity<ArStuEnrollments>();
        /// <summary>
        /// The program version repository.
        /// </summary>
        protected IRepository<ArPrgVersions> ArPrgVersionsRepository =>
            this.context.GetRepositoryForEntity<ArPrgVersions>();
        /// <summary>
        /// The SAP policy details repository.
        /// </summary>
        protected IRepository<ArSapdetails> ArSapPolicyDetailsRepository =>
            this.context.GetRepositoryForEntity<ArSapdetails>();
        /// <summary>
        /// The AR results repository.
        /// </summary>
        protected IRepository<ArResults> ArResultsRepository => this.context.GetRepositoryForEntity<ArResults>();
        /// <summary>
        /// The transfer grades repository.
        /// </summary>
        protected IRepository<ArTransferGrades> ArTransferGradesRepository =>
            this.context.GetRepositoryForEntity<ArTransferGrades>();
        /// <summary>
        /// The terms repository.
        /// </summary>
        protected IRepository<ArTerm> ArTermsRepository => this.context.GetRepositoryForEntity<ArTerm>();

        /// <summary>
        /// The ar title ivsap check results repository.
        /// </summary>
        protected IRepository<ArFasapchkResults> ArTitleIvsapCheckResultsRepository =>
            this.context.GetRepositoryForEntity<ArFasapchkResults>();

        /// <summary>
        /// The Grade Reps app setting.
        /// </summary>
        protected string GradeReps =>
            this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.GradeCourseRepetitionsMethod);
        /// <summary>
        /// The include hours app setting.
        /// </summary>
        protected bool IncludeHours =>
            Convert.ToBoolean(
                this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.IncludeHoursForFailingGrade));
        /// <summary>
        /// The SAP uses credit ranges app setting.
        /// </summary>
        protected bool SapUsesCreditRanges =>
            Convert.ToBoolean(this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.SAPUsesCreditRanges));
        /// <summary>
        /// Inactive id
        /// </summary>
        protected Guid InactiveId => (Task.Run(() => this.statusService.GetInactiveStatusId())).Result;
        /// <summary>
        /// Active id
        /// </summary>
        protected Guid ActiveId => (Task.Run(() => this.statusService.GetActiveStatusId())).Result;
        /// <summary>
        /// The scheduling method app setting.
        /// </summary>
        protected string SchedulingMethod;
        /// <summary>
        /// The track sap attendance app setting.
        /// </summary>
        protected string TrackSapAttendance;
        /// <summary>
        /// The grades format app setting.
        /// </summary>
        protected string GradesFormat;

        /// <summary>
        /// The entry method for SAP Check execution.
        /// </summary>
        public async Task<List<SAPResult>> Execute(Guid? campusId, Guid? programVersionId, IList<Guid> stuEnrollments, int? incrementToRun = null, bool runAllIncrements = false, bool allowAnyStatus = false)
        {
            this.ProgramVersionId = programVersionId;
            this.StudentEnrollmentIds = stuEnrollments;
            this.CampusId = campusId;
            this.IncrementToRun = incrementToRun;
            this.RunAllIncrements = runAllIncrements;
            this.AllowAnyStatus = allowAnyStatus;
            if (campusId.HasValue)
            {
                SchedulingMethod =
                    this.appSettingService.GetAppSettingValueByCampus(SapCheckConfigurationKeys.SchedulingMethod,
                        campusId.Value);
                TrackSapAttendance = this.appSettingService.GetAppSettingValueByCampus(SapCheckConfigurationKeys.TrackSapAttendance,
                    campusId.Value);
                GradesFormat = this.appSettingService.GetAppSettingValueByCampus(SapCheckConfigurationKeys.GradesFormat,
                    campusId.Value);

                if (string.IsNullOrEmpty(SchedulingMethod))
                {
                    SchedulingMethod = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.SchedulingMethod);
                }
                if (string.IsNullOrEmpty(TrackSapAttendance))
                {
                    TrackSapAttendance = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.TrackSapAttendance);
                }
                if (string.IsNullOrEmpty(GradesFormat))
                {
                    GradesFormat = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.GradesFormat);
                }
            }
            else
            {
                SchedulingMethod = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.SchedulingMethod);
                TrackSapAttendance = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.TrackSapAttendance);
                GradesFormat = this.appSettingService.GetAppSettingValue(SapCheckConfigurationKeys.GradesFormat);
            }
            await Validate();
            await Run();
            await Save();
            await Update();
            return InvalidResults;
        }

        /// <summary>
        /// The get program version sap object.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected ProgramVersionSAP GetProgramVersionSap(Guid enrollmentId)
        {
            var enrollment = ArStuEnrollmentsRepository.Get(se => se.StuEnrollId == enrollmentId).FirstOrDefault();
            ProgramVersionSAP pvSap = this.mapper.Map<ProgramVersionSAP>(enrollment);
            //.Include(pv => pv.PrgVer.Fasap).Include(pv => pv.PrgVer).ThenInclude(pv => pv.Prog).ThenInclude(pv => pv.Ac)
            return pvSap;
        }

        /// <summary>
        /// The get sap policy details.
        /// </summary>
        /// <param name="sapId">
        /// The sap id.
        /// </param>
        protected List<SAPPolicyDetail> GetSAPPolicyDetails(Guid sapId)
        {
            List<SAPPolicyDetail> pd = new List<SAPPolicyDetail>();
            pd =
                ArSapPolicyDetailsRepository.Get(se => se.Sapid == sapId)
                    .OrderBy(se => se.TrigValue)
                    .ToList()
                    .Select((j, i) => new SAPPolicyDetail()
                    {
                        Id = j.SapdetailId,
                        Increment = i + 1,
                        QualitativeMinimumValue = j.QualMinValue,
                        QuantitativeMinimumValue = j.QuantMinValue,
                        QualitativeMinimumTypeId = j.QualMinTypId,
                        QuantitativeMinimumUnitTypeId = j.QuantMinUnitTypId,
                        TriggerValue = j.TrigValue,
                        TriggerUnitTypeId = j.TrigUnitTypId,
                        TriggerOffsetTypeId = j.TrigOffsetTypId,
                        TriggerOffsetSequence = j.TrigOffsetSeq,
                        MinimumCreditsCompleted = j.MinCredsCompltd,
                        MinimumAttendanceValue = j.MinAttendanceValue

                    }).ToList();
            return pd;
        }

        /// <summary>
        /// The is continuing education check.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected bool? IsContinuingEducation(Guid enrollmentId)
        {
            return
                ArStuEnrollmentsRepository.Get(se => se.StuEnrollId == enrollmentId)
                    .Select(se => se.PrgVer.IsContinuingEd).FirstOrDefault();
        }

        /// <summary>
        /// The get terms method.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected List<Term> GetTerms(Guid enrollmentId)
        {
            return this.termService.GetTerms(enrollmentId);
        }

        /// <summary>
        /// The get student terms from start date.
        /// </summary>
        /// <param name="terms">
        /// The enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The enrollment id.
        /// </param>
        /// <param name="endDate">
        /// The enrollment id.
        /// </param>
        protected List<Term> GetStudentTermsFromStartDate(List<Term> terms, DateTime startDate, DateTime endDate)
        {
            return SchedulingMethod != "ModuleStart" ? terms.Where(term => term.EndDate >= startDate && term.EndDate <= endDate).ToList() : terms;
        }

        /// <summary>
        /// The get student start date.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected DateTime? GetStudentStartDate(Guid enrollmentId)
        {
            return this.enrollmentService.GetStudentStartDate(enrollmentId);
        }
        /// <summary>
        /// The get student leave of absence's.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected List<LOA> GetStudentLOAs(Guid enrollmentId)
        {
            return this.enrollmentService.GetStudentLOAs(enrollmentId).ToList();

        }
        /// <summary>
        /// The get offset date for credit policy.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        protected DateTime GetOffsetDateForCreditsPolicy(Guid enrollmentId)
        {
            return ArResultsRepository.Get(ar => ar.StuEnrollId == enrollmentId)
                .OrderByDescending(ar => ar.Test.EndDate)
                .Select(ar => ar.Test.EndDate).FirstOrDefault();

        }

        /// <summary>
        /// The studen with title iv sap.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> RunIVSapCheckForStudents()
        {
            var stuEnrollments = new List<Guid>();
            stuEnrollments = ArTitleIvsapCheckResultsRepository.Get().Select(x => x.StuEnrollId).Distinct().ToList();
            var invalidResults = await Execute(null, null, stuEnrollments);
            if (InvalidResults.Count == 0)
            {
                return true;
            }
            return false;
        }
    }
}

