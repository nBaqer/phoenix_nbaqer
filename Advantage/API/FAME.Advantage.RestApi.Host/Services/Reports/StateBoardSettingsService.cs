﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardSettingsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the State board settings service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Utilities;
    using FAME.Extensions.Helpers;
    using AutoMapper;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The state board setting service class.
    /// </summary>
    public class StateBoardSettingsService : IStateBoardSettingsService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The school service.
        /// </summary>
        private ISchoolService schoolService;

        /// <summary>
        /// The states service.
        /// </summary>
        private IStatesService statesService;

        /// <summary>
        /// The report service.
        /// </summary>
        private IReportService reportService;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StateBoardSettingsService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="schoolService">
        /// The school Service.
        /// </param>
        public StateBoardSettingsService(IAdvantageDbContext advantageDbContext, IMapper mapper, ISchoolService schoolService, IStatesService statesService, IReportService reportService)
        {
            this.advantageDbContext = advantageDbContext;
            this.mapper = mapper;
            this.schoolService = schoolService;
            this.reportService = reportService;
            this.statesService = statesService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The state report relation repository.
        /// </summary>
        private IRepository<SyStateReports> StateReportsRepository => this.advantageDbContext.GetRepositoryForEntity<SyStateReports>();

        /// <summary>
        /// The school state reports setting repository.
        /// </summary>
        private IRepository<SySchoolStateBoardReports> SchoolStateReportsRepository => this.advantageDbContext.GetRepositoryForEntity<SySchoolStateBoardReports>();

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.advantageDbContext.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The state board agency repository.
        /// </summary>
        private IRepository<SyStateBoardAgencies> LicensingAgencyRepository => this.advantageDbContext.GetRepositoryForEntity<SyStateBoardAgencies>();

        /// <summary>
        /// The program repository.
        /// </summary>
        private IRepository<ArPrograms> ProgramRepository => this.advantageDbContext.GetRepositoryForEntity<ArPrograms>();

        /// <summary>
        /// The state board course repository.
        /// </summary>
        private IRepository<SyStateBoardCourses> StateBoardCourseRepository => this.advantageDbContext.GetRepositoryForEntity<SyStateBoardCourses>();

        /// <summary>
        /// The school state board course mapping repository.
        /// </summary>
        private IRepository<SyStateBoardProgramCourseMappings> SchoolStateBoardCourseMappingRepository => this.advantageDbContext.GetRepositoryForEntity<SyStateBoardProgramCourseMappings>();

        /// <summary>
        /// The get state board reports.
        /// </summary>
        /// <param name="stateId">
        /// The state Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        public async System.Threading.Tasks.Task<IEnumerable<IListItem<string, Guid>>> GetStateBoardReports(Guid? stateId)
        {
            return await System.Threading.Tasks.Task.Run(() =>
            {
                var stateBoardReports = this.StateReportsRepository
                    .Get(s => stateId.HasValue && s.StateId == stateId.Value).Select(
                        s => new ListItem<string, Guid>() { Value = s.ReportId, Text = s.Report.ReportDescription }).ToList();

                string cacheKeyId = stateId?.ToString() ?? Guid.Empty.ToString();

                return SimpleCache.GetOrAddExisting(
                    $"{this.CacheKeyFirstSegment}:SyStateBoardReports:{cacheKeyId}",
                    () => stateBoardReports,
                    CacheExpiration.VERY_LONG);
            });
        }

        /// <summary>
        /// The delete state board setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<bool> DeleteStateBoardSetting(StateBoardReportSetting model)
        {
            return await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    var user = this.advantageDbContext.GetTenantUser();

                    if (user.IsSupportUser)
                    {
                        var settingToDelete = this.SchoolStateReportsRepository.Get(
                            s => s.CampusId == model.CampusId && s.ReportId == model.ReportId
                                                              && s.StateId == model.StateId).FirstOrDefault();

                        if (settingToDelete != null && settingToDelete.SchoolStateBoardReportId != Guid.Empty)
                        {
                            this.SchoolStateReportsRepository.Delete(settingToDelete);
                            var mappingsToDelete = this.SchoolStateBoardCourseMappingRepository.Get(
                                    x => x.SchoolStateBoardReportId == settingToDelete.SchoolStateBoardReportId)
                                .ToList();

                            if (mappingsToDelete.Count > 0)
                            {
                                this.SchoolStateBoardCourseMappingRepository.Delete(mappingsToDelete);
                            }
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return false;
                }
            });
        }

        /// <summary>
        /// The save or update state board report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task{TResult}"/>.
        /// </returns>
        public async Task<ActionResult<StateBoardReportSetting>> SaveOrUpdateStateBoardReportSetting(StateBoardReportSetting model)
        {
            return await System.Threading.Tasks.Task.Run(
                       () =>
                       {
                           ActionResult<StateBoardReportSetting> actionResult = new ActionResult<StateBoardReportSetting>();
                           actionResult.ResultStatus = Enums.ResultStatus.Success;
                           actionResult.Result = model;

                           var modDate = DateTime.Now;
                           var user = this.advantageDbContext.GetTenantUser();
                           if (user.IsSupportUser)
                           {

                               var programs = this.ProgramRepository.Get(prog =>
                                       model.ProgramStateBoardCourseMappings.Any(mapp => mapp.ProgramId == prog.ProgId))
                                   .ToList();
                               var courses = this.StateBoardCourseRepository.Get(course =>
                                   model.ProgramStateBoardCourseMappings.Any(mapp =>
                                       mapp.StateBoardCourseId == course.StateBoardCourseId)).ToList();
                               var mappings = model.ProgramStateBoardCourseMappings.Select(_ =>
                                   new SyStateBoardProgramCourseMappings()
                                   {
                                       ProgramId = programs.Where(prog => prog.ProgId == _.ProgramId)
                                           .Select(prog => prog.ProgId).First(),
                                       StateBoardCourseId =
                                           courses.Where(sbc => sbc.StateBoardCourseId == _.StateBoardCourseId)
                                               .Select(sbc => sbc.StateBoardCourseId).First(),
                                       CreatedDate = modDate,
                                       CreatedById = user.Id
                                   }).ToList();
                               try
                               {
                                   // If state board report id is guid.empty then this is a new save
                                   if (model.SchoolStateBoardReportId == Guid.Empty)
                                   {
                                       var schoolStateBoardSetting = this.mapper.Map<SySchoolStateBoardReports>(
                                           model);

                                       schoolStateBoardSetting.ModifiedDate = modDate;
                                       schoolStateBoardSetting.ModifiedUser = user.Email;


                                       schoolStateBoardSetting =
                                           this.SchoolStateReportsRepository.Create(schoolStateBoardSetting);

                                       foreach (var mapping in mappings)
                                       {
                                           mapping.SchoolStateBoardReportId =
                                               schoolStateBoardSetting.SchoolStateBoardReportId;
                                           schoolStateBoardSetting.SyStateBoardProgramCourseMappings.Add(mapping);

                                       }

                                       this.SchoolStateReportsRepository.Save();

                                       actionResult.Result.SchoolStateBoardReportId =
                                           schoolStateBoardSetting.SchoolStateBoardReportId;

                                   }

                                   // we are updating
                                   else
                                   {
                                       var settingToUpdate = this.SchoolStateReportsRepository.Get(
                                               s => s.CampusId == model.CampusId && s.ReportId == model.ReportId
                                                                                 && s.StateId == model.StateId)
                                           .Include(sett => sett.SyStateBoardProgramCourseMappings).FirstOrDefault();

                                       if (settingToUpdate != null &&
                                           settingToUpdate.SchoolStateBoardReportId != Guid.Empty)
                                       {
                                           settingToUpdate.OwnerName = model.OwnerName;
                                           settingToUpdate.OwnerLicenseNumber = model.OwnerLicenseNumber;
                                           settingToUpdate.OfficerName1 = model.OfficerName1;
                                           settingToUpdate.OfficerName2 = model.OfficerName2;
                                           settingToUpdate.OfficerName3 = model.OfficerName3;
                                           settingToUpdate.ModifiedDate = modDate;
                                           settingToUpdate.ModifiedUser = user.Email;

                                           var dupComparer = new InlineComparer<SyStateBoardProgramCourseMappings>(
                                               (i1, i2) => i1.StateBoardCourseId == i2.StateBoardCourseId &&
                                                           i1.ProgramId == i2.ProgramId,
                                               i => i.StateBoardCourseId.GetHashCode() + i.ProgramId.GetHashCode());

                                           var interMappings =
                                               settingToUpdate.SyStateBoardProgramCourseMappings.Intersect(mappings,
                                                   dupComparer);
                                           var comboMappings = settingToUpdate.SyStateBoardProgramCourseMappings
                                               .Union(mappings, dupComparer).ToList();
                                           var deletedMappings =
                                               settingToUpdate.SyStateBoardProgramCourseMappings.Except(interMappings,
                                                   dupComparer);
                                           var addedMappings = comboMappings.Except(
                                               settingToUpdate.SyStateBoardProgramCourseMappings, dupComparer);

                                           foreach (var mapping in deletedMappings)
                                           {
                                               this.SchoolStateBoardCourseMappingRepository.Delete(mapping);
                                           }

                                           foreach (var mapping in addedMappings)
                                           {
                                               mapping.SchoolStateBoardReportId =
                                                   settingToUpdate.SchoolStateBoardReportId;
                                               settingToUpdate.SyStateBoardProgramCourseMappings.Add(mapping);
                                           }

                                           this.SchoolStateReportsRepository.Update(settingToUpdate);
                                           this.SchoolStateReportsRepository.Save();

                                           actionResult.Result.SchoolStateBoardReportId =
                                               settingToUpdate.SchoolStateBoardReportId;
                                       }

                                   }

                                   // always update campus and licensing agency information
                                   var campus = this.CampusRepository.Get(camp => camp.CampusId == model.CampusId)
                                       .FirstOrDefault();

                                   var licensingAgency = this.LicensingAgencyRepository
                                       .Get(la => la.StateBoardAgencyId == model.LicensingAgencyId).FirstOrDefault();

                                   if (campus != null)
                                   {
                                       campus.SchoolName = model.SchoolName;
                                       campus.Address1 = model.SchoolAddress1;
                                       campus.Address2 = model.SchoolAddress2;
                                       campus.City = model.CampusCity;
                                       campus.CountryId = model.CampusCountryId;
                                       campus.StateId = model.CampusStateId;
                                       campus.Zip = model.CampusZipCode;
                                       campus.ModDate = modDate;
                                       campus.ModUser = user.Email;
                                   }

                                   if (licensingAgency != null)
                                   {
                                       licensingAgency.LicensingAddress1 = model.LicensingAddress1;
                                       licensingAgency.LicensingAddress2 = model.LicensingAddress2;
                                       licensingAgency.LicensingCountryId = model.LicensingCountryId;
                                       licensingAgency.LicensingCity = model.LicensingCity;
                                       licensingAgency.LicensingStateId = model.LicensingStateId;
                                   }

                                   this.CampusRepository.Update(campus);
                                   this.CampusRepository.Save();

                                   this.LicensingAgencyRepository.Update(licensingAgency);
                                   this.LicensingAgencyRepository.Save();

                                   return actionResult;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   actionResult.ResultStatus = Enums.ResultStatus.Error;
                                   actionResult.ResultStatusMessage =
                                       "An error occured when trying to save the state board settings.";
                                   return actionResult;
                               }
                           }
                           actionResult.ResultStatusMessage =
                               "User must be a Support User to save the state board settings";
                           return actionResult;
                       });
        }

        /// <summary>
        /// The get state board report setting.
        /// </summary>
        /// <param name="stateId">
        /// The state id.
        /// </param>
        /// <param name="reportId">
        /// The report id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task{TResult}"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<StateBoardReportSetting> GetStateBoardReportSetting(Guid stateId, Guid reportId, Guid campusId)
        {
            return await System.Threading.Tasks.Task.Run(
                       async () =>
                       {
                           var result = this.SchoolStateReportsRepository
                                            .Get(s => s.CampusId == campusId && s.ReportId == reportId && s.StateId == stateId)
                                            .Include(ssrs => ssrs.LicensingAgency)
                                            .ThenInclude(la => la.State)
                                            .Include(ssrs => ssrs.LicensingAgency)
                                            .ThenInclude(la => la.LicensingCountry)

                                            .Select(x => new StateBoardReportSetting
                                            {
                                                SchoolStateBoardReportId = x.SchoolStateBoardReportId,
                                                StateId = x.StateId.Value,
                                                ReportId = x.ReportId.Value,
                                                CampusId = x.CampusId.Value,
                                                OwnerName = x.OwnerName,
                                                OwnerLicenseNumber = x.OwnerLicenseNumber,
                                                OfficerName1 = x.OfficerName1,
                                                OfficerName2 = x.OfficerName2,
                                                OfficerName3 = x.OfficerName3,
                                                LicensingAgencyId = x.LicensingAgencyId,
                                                LicensingAgencyName = x.LicensingAgency.Description,
                                                LicensingAddress1 = x.LicensingAgency.LicensingAddress1,
                                                LicensingAddress2 = x.LicensingAgency.LicensingAddress2,
                                                LicensingCity = x.LicensingAgency.LicensingCity,
                                                LicensingStateId = x.LicensingAgency.LicensingStateId,
                                                LicensingState = x.LicensingAgency.LicensingState.StateDescrip,
                                                LicensingCountryId = x.LicensingAgency.LicensingCountryId,
                                                LicensingCountry = x.LicensingAgency.LicensingCountry.CountryDescrip,
                                                LicensingZipCode = x.LicensingAgency.LicensingZipCode,
                                                ProgramStateBoardCourseMappings = x.SyStateBoardProgramCourseMappings.Where(m => m.SchoolStateBoardReportId == x.SchoolStateBoardReportId).Select(m => new ProgramStateBoardCourseMapping
                                                {
                                                    ProgramId = m.ProgramId,
                                                    StateBoardCourseId = m.StateBoardCourseId
                                                }).ToList()
                                            }).FirstOrDefault() ?? new StateBoardReportSetting();

                           var schoolInformation = await this.schoolService.GetDetail(campusId);

                           if (schoolInformation != null)
                           {
                               result.SchoolName = schoolInformation.SchoolName;
                               result.SchoolAddress1 = schoolInformation.Address1;
                               result.SchoolAddress2 = schoolInformation.Address2;
                               result.CampusCity = schoolInformation.City;
                               result.CampusCountryId = schoolInformation.CountryId;
                               result.CampusStateId = schoolInformation.StateId;
                               result.CampusZipCode = schoolInformation.Zip;
                               result.CampusState = schoolInformation.StateCode;
                           }

                           return result;
                       });
        }

        /// <summary>
        /// The get indiana state board report settings.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<StateBoardReportSetting> GetIndianaStateBoardReportSettings(Guid campusId)
        {
            return await System.Threading.Tasks.Task.Run(
                       async () =>
                           {
                               var stateId = await this.statesService.GetIdByName("Indiana");
                               var reportid = await this.reportService.GetIdByName("StateBoardReport");

                               if (stateId != Guid.Empty && reportid != Guid.Empty && campusId != Guid.Empty)
                               {
                                   var result = await this.GetStateBoardReportSetting(stateId, reportid, campusId);
                                   return result;
                               }
                               return new StateBoardReportSetting();
                           });
        }
    }
}
