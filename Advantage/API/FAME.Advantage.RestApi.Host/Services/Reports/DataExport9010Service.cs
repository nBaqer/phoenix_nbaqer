﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataExport9010Service.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   the DataExport9010Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Extensions;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.AwardType9010;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Report9010;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Internal;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The DataExport9010Service service.
    /// </summary>
    public class DataExport9010Service : IDataExport9010Service
    {

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The campus service.
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// The fund source repository.
        /// </summary>
        private IRepository<SaFundSources> FundSourceRepository => this.context.GetRepositoryForEntity<SaFundSources>();

        /// <summary>
        /// The transaction codes repository.
        /// </summary>
        private IRepository<SaTransCodes> TransactionCodesRepository => this.context.GetRepositoryForEntity<SaTransCodes>();

        /// <summary>
        /// The fund source repository.
        /// </summary>
        private IRepository<SaAwardTypes> SaAwardTypesRepository => this.context.GetRepositoryForEntity<SaAwardTypes>();

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.context.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The 9010 mapping repository.
        /// </summary>
        private IRepository<SyAwardTypes9010Mapping> AwardType9010MappingRepository => this.context.GetRepositoryForEntity<SyAwardTypes9010Mapping>();

        /// <summary>
        /// The syCampGrps repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> SyCampGrpsRepository => this.context.GetRepositoryForEntity<SyCmpGrpCmps>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        /// The 9010 award type repository.
        /// </summary>
        private IRepository<SyAwardTypes9010> AwardTypes9010Repository => this.context.GetRepositoryForEntity<SyAwardTypes9010>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DataExport9010Service"/> class.
        /// </summary>
        /// <param name="context">
        /// The Advantage Database context</param>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        /// <param name="appSettingsService">
        /// The app settings service</param>
        /// <param name="statusService">
        /// The status service</param>
        /// <param name="campusService">
        /// The campus service
        /// </param>
        public DataExport9010Service(IAdvantageDbContext context,
            IReportService reportService,
            ISystemConfigurationAppSettingService appSettingsService, IStatusesService statusService, ICampusService campusService)
        {
            this.context = context;
            this.reportService = reportService;
            this.appSettingsService = appSettingsService;
            this.statusService = statusService;
            this.campusService = campusService;
        }

        /// <summary>
        ///  Given campus id, returns the fund sources for a given campus along with their 9010 mapping, if any.
        /// </summary>
        /// <param name="campusId">
        /// </param>
        /// <returns></returns>
        public async Task<ActionResult<List<Mapping9010>>> Get9010MappingsByCampusId(Guid campusId)
        {
            var rv = new ActionResult<List<Mapping9010>>();

            try
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();
                var fundSources = this.FundSourceRepository.Get();
                var transactionCodes = this.TransactionCodesRepository.Get();
                var saAwardTypes = this.SaAwardTypesRepository.Get();
                var awardTypes9010 = this.AwardTypes9010Repository.Get();
                var awardTypes9010Mapping = this.AwardType9010MappingRepository.Get();

                var mappingTypes9010 = await (from a in awardTypes9010
                                              join b in awardTypes9010Mapping on a.AwardType9010Id equals b.AwardType9010Id
                                              where b.CampusId == campusId && b.StatusId == activeStatusId
                                              select new
                                              {
                                                  a.AwardType9010Id,
                                                  a.Name,
                                                  a.Description,
                                                  a.DisplayOrder,
                                                  b.FundSourceId,
                                                  b.TransCodeId
                                              }).ToListAsync();

                var campusGroupIds = this.SyCampGrpsRepository.Get(a => a.CampusId == campusId);

                //retrieves list of campus fund sources along with 9010 mapping if exists.
                var fundMappings = await (from a in fundSources
                                          join b in saAwardTypes on a.AwardTypeId equals b.AwardTypeId into _ab
                                          from ab in _ab.DefaultIfEmpty()
                                          where campusGroupIds.Any(i => i.CampGrpId == a.CampGrpId)
                                          select new
                                          {
                                              a.FundSourceId,
                                              FundSourceDescription = a.FundSourceDescrip,
                                              a.FundSourceCode,
                                              AwardTypeDescription = ab == null ? "" : ab.Descrip,
                                              IsActive = a.StatusId == activeStatusId,
                                              IsTitleIV = a.TitleIv
                                          }).ToListAsync();

                var fundSourceResult = (from a in fundMappings
                                        join b in mappingTypes9010 on a.FundSourceId equals b.FundSourceId into ab
                                        from _ab in ab.DefaultIfEmpty()
                                        select new Mapping9010
                                        {
                                            EntityId = a.FundSourceId,
                                            EntityDescription = a.FundSourceDescription,
                                            EntityCode = a.FundSourceCode,
                                            AwardTypeDescription = a.AwardTypeDescription,
                                            MappingType9010Id = _ab == null ? Guid.Empty : _ab.AwardType9010Id,
                                            MappingType9010Description = _ab == null ? "" : _ab.Description,
                                            IsActive = a.IsActive,
                                            EntityType = "FundSource",
                                            EntityTypeEnum = EntityTypes9010.FundSource,
                                            IsTitleIV = a.IsTitleIV
                                        }).ToList();

                //retrieves list of campus transactioncodes along with 9010 mapping if exists.
                var transactionCodeMappings = await (from a in transactionCodes
                                                     where campusGroupIds.Any(i => i.CampGrpId == a.CampGrpId)
                                                     select new
                                                     {
                                                         a.TransCodeId,
                                                         a.TransCodeDescrip,
                                                         a.TransCodeCode,
                                                         IsActive = a.StatusId == activeStatusId
                                                     }).ToListAsync();

                var transactionCodeResult = (from a in transactionCodeMappings
                                             join b in mappingTypes9010 on a.TransCodeId equals b.TransCodeId into ab
                                             from _ab in ab.DefaultIfEmpty()
                                             select new Mapping9010
                                             {
                                                 EntityId = a.TransCodeId,
                                                 EntityDescription = a.TransCodeDescrip,
                                                 EntityCode = a.TransCodeCode,
                                                 AwardTypeDescription = "",
                                                 MappingType9010Id = _ab == null ? Guid.Empty : _ab.AwardType9010Id,
                                                 MappingType9010Description = _ab == null ? "" : _ab.Description,
                                                 IsActive = a.IsActive,
                                                 EntityType = "TransactionType",
                                                 EntityTypeEnum = EntityTypes9010.TransactionType
                                             }).ToList();

                var result = fundSourceResult.OrderByDescending(a => a.IsActive).ThenBy(a => a.EntityDescription).Union(transactionCodeResult.OrderByDescending(a => a.IsActive).ThenBy(a => a.EntityDescription)).ToList();
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.Result = result == null || !result.Any() ? new List<Mapping9010>() : result;
                return rv;
            }

            catch (Exception e)
            {
                e.TrackException();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ApiMsgs.UNABLE_TO_RETRIEVE_9010_MAPPINGS;
                return rv;
            }
        }

        /// <summary>
        ///  Given campus id, returns all 9010 award types.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<List<MappingType9010>>> Get9010AwardTypes()
        {
            var rv = new ActionResult<List<MappingType9010>>();

            try
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();

                return await SimpleCache.GetOrAddExisting(
                               $"{this.CacheKeyFirstSegment}:Get9010AwardTypes",
                                async () =>
                               {
                                   var awardTypes = this.AwardTypes9010Repository.Get();
                                   rv.Result = await awardTypes.OrderBy(a => a.DisplayOrder).Where(a => a.StatusId == activeStatusId)
                                   .Select(a => new MappingType9010()
                                   {
                                       Description = a.Description,
                                       DisplayOrder = a.DisplayOrder ?? 0,
                                       Id = a.AwardType9010Id
                                   })
                                   .ToListAsync();

                                   rv.ResultStatus = Enums.ResultStatus.Success;
                                   return rv;
                               },
                               CacheExpiration.VERY_LONG);
            }
            catch (Exception e)
            {
                e.TrackException();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ApiMsgs.UNABLE_TO_RETRIEVE_9010_TYPES;
                return rv;
            }
        }

        /// <summary>
        /// The get title iv notice report.
        /// </summary>
        /// <param name="parameters">
        /// parameters to generate excel.
        /// </param>
        /// <returns>
        /// The <see cref="ReportActionResult"/>.
        /// </returns>
        public async Task<ReportActionResult> Get9010SpreadsheetData(DataExport9010Param parameters)
        {
            var rv = new ReportActionResult();

            try
            {
                DbCommand attendanceCommand = await this.context
                            .LoadStoredProc("USP_FA_9010DataExport")
                            .WithSqlParam("@StuEnrollmentId", parameters.StuEnrollmentId.HasValue ?
                                                              parameters.StuEnrollmentId : Guid.Empty, DbType.Guid)
                            .WithSqlParam("@CampusIds", string.Join(",", parameters.CampusIds), DbType.String)
                            .WithSqlParam("@FiscalYearStart", parameters.FiscalYearStart, DbType.DateTime2)
                            .WithSqlParam("@FiscalYearEnd", parameters.FiscalYearEnd, DbType.DateTime2)
                            .WithSqlParam("@GroupIds", string.Join(",", parameters.GroupIds), DbType.String)
                            .WithSqlParam("@ProgramIds", string.Join(",", parameters.ProgramIds), DbType.String)
                            .WithSqlParam("@ActivitiesConductedAmount", parameters.ActivitiesConductedAmount, DbType.String)
                            .ExecuteStoredProcAsync();

                // Create the DbDataAdapter.
                DbDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = attendanceCommand;

                // Fill the DataTable.
                DataSet tables = new DataSet();
                adapter.Fill(tables);

                attendanceCommand.Close();

                //create a new Excel package
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    //create a WorkSheet
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("90-10 Summary");
                    worksheet.Cells["A1"].LoadFromDataTable(tables.Tables[0], false);

                    //create a WorkSheet 2
                    ExcelWorksheet worksheet2 = excelPackage.Workbook.Worksheets.Add("90-10 Student Ledger");
                    worksheet2.Cells["A1"].LoadFromDataTable(tables.Tables[1], true);

                    //sheet 1
                    worksheet.Cells["A2"].Style.Font.Bold = true;
                    worksheet.Cells["A10"].Style.Font.Bold = true;
                    worksheet.Cells["A16"].Style.Font.Bold = true;
                    worksheet.Cells["A18"].Style.Font.Bold = true;
                    worksheet.Cells["A38"].Style.Font.Bold = true;
                    worksheet.Cells["A40"].Style.Font.Bold = true;
                    worksheet.Cells["A53"].Style.Font.Bold = true;
                    worksheet.Cells["B1"].Style.Font.Bold = true;
                    worksheet.Cells["D1"].Style.Font.Bold = true;
                    worksheet.Cells["B2:B100"].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells["D2:D100"].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells["A1:A100"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.LightGray);
                    worksheet.Cells["B1:B100"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.LightGray);
                    worksheet.Cells["D1:D100"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.LightGray);
                    worksheet.Cells["A10"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells["A38"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells["A53"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells["D57"].Style.Numberformat.Format = "0%";

                    foreach (var cell in worksheet.Cells["B2:B100"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet.Cells["D2:D100"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    worksheet.Calculate();
                    worksheet.Cells.AutoFitColumns();

                    //sheet 2
                    worksheet2.Cells["A1"].Style.Font.Bold = true;
                    worksheet2.Cells["G1"].Style.Font.Bold = true;
                    worksheet2.Cells["N1"].Style.Font.Bold = true;
                    worksheet2.Cells["U1"].Style.Font.Bold = true;
                    worksheet2.Cells["V1"].Style.Font.Bold = true;
                    worksheet2.Cells["W1"].Style.Font.Bold = true;
                    worksheet2.Cells["X1"].Style.Font.Bold = true;
                    worksheet2.Cells["AC1"].Style.Font.Bold = true;
                    worksheet2.Cells["AE1"].Style.Font.Bold = true;
                    worksheet2.Cells["AJ1"].Style.Font.Bold = true;
                    worksheet2.Cells["AL1"].Style.Font.Bold = true;
                    worksheet2.Cells["AO1"].Style.Font.Bold = true;
                    worksheet2.Cells["AR1"].Style.Font.Bold = true;
                    worksheet2.Cells["AU1"].Style.Font.Bold = true;
                    worksheet2.Cells["A2:A10000"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    foreach (var cell in worksheet2.Cells["G2:G10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["N2:N10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["U2:U10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["V2:V10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["X2:X10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AC2:AC10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AE2:AE10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AJ2:AJ10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AK2:AK10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AL2:AL10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AO2:AO10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AQ2:AQ10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AR2:AR10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AS2:AS10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AT2:AT10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    foreach (var cell in worksheet2.Cells["AU2:AU10000"].Where(cell => cell.Value != null))
                        cell.Formula = (string)cell.Value;

                    worksheet2.Cells[2, 4, 10000, 100].Style.Numberformat.Format = "$###,###,##0.00";
                    worksheet2.Cells[1, 1, 1, 100].Style.Font.Name = "Open Sans";
                    worksheet2.Cells[1, 1, 1, 100].Style.Font.Size = 10;

                    worksheet2.Cells["AU2:AU210000"].Style.Numberformat.Format = "#0.00%";
                    worksheet2.Cells["D1:AX1"].Style.WrapText = true;
                    worksheet2.Cells.AutoFitColumns();

                    for (var i = 4; i < 50; i++)
                        worksheet2.Column(i).Width = 13;

                    worksheet2.Row(1).Height = 57;
                    worksheet2.Calculate();
                    worksheet2.View.FreezePanes(2, 1);

                    rv.Data = excelPackage.GetAsByteArray();
                }


                return rv;
            }
            catch (Exception e)
            {
                rv.Data = null;
                rv.ResultStatus = e.Message;
                e.TrackException();
                return rv;
            }
        }

        /// <summary>
        ///  Given new 9010 award mappings, deletes existing and creates new mappings
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult<String>> UpdateCampusAwardMappings9010(List<Mapping9010> entityMappings, Guid campusId)
        {
            var rv = new ActionResult<String>();

            try
            {
                var activeStatusId = await this.statusService.GetActiveStatusId();
                var existingMappings = await this.AwardType9010MappingRepository.Get().Where(a => a.CampusId == campusId).ToListAsync();

                //remove all existing mappings
                foreach (var mapping in existingMappings)
                    this.AwardType9010MappingRepository.Delete(mapping);

                this.context.SaveChanges();

                //we only care about those with mappings
                entityMappings = entityMappings.Where(a => a.MappingType9010Id != null && a.MappingType9010Id != Guid.Empty).ToList();

                var newFundSourceMappings = entityMappings.Where(a => a.EntityTypeEnum == EntityTypes9010.FundSource).Select(a => new SyAwardTypes9010Mapping()
                {
                    AwardTypes9010MappingId = new Guid(),
                    AwardType9010Id = a.MappingType9010Id.Value,
                    CampusId = campusId,
                    FundSourceId = a.EntityId,
                    StatusId = activeStatusId,
                    ModUser = this.TenantUser.Email,
                    ModDate = DateTime.Now,
                    TransCodeId = null
                }).ToList();

                var newTransactionTypeMappings = entityMappings.Where(a => a.EntityTypeEnum == EntityTypes9010.TransactionType).Select(a => new SyAwardTypes9010Mapping()
                {
                    AwardTypes9010MappingId = new Guid(),
                    AwardType9010Id = a.MappingType9010Id.Value,
                    CampusId = campusId,
                    FundSourceId = null,
                    StatusId = activeStatusId,
                    ModUser = this.TenantUser.Email,
                    ModDate = DateTime.Now,
                    TransCodeId = a.EntityId
                }).ToList();

                var newMappings = newFundSourceMappings.Union(newTransactionTypeMappings).ToList();

                //create new mappings
                foreach (var mapping in newMappings)
                    await this.AwardType9010MappingRepository.CreateAsync(mapping);

                this.context.SaveChanges();
                rv.ResultStatus = Enums.ResultStatus.Success;
                return rv;
            }
            catch (Exception e)
            {
                e.TrackException();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ApiMsgs.UNABLE_TO_UPDATE_9010_MAPPINGS;
                return rv;
            }
        }

        /// <summary>
        ///  Given campus id, returns mappings for a given mapping in excel byte array
        /// </summary>
        /// <returns></returns>
        public async Task<ReportActionResult> Get9010MappingsConfigurationByCampus(Guid campusId)
        {
            var rv = new ReportActionResult();

            try
            {
                DbCommand attendanceCommand = await this.context
                            .LoadStoredProc("USP_FA_9010GetCampusMappingsById")
                            .WithSqlParam("@campusId", campusId.ToString(), DbType.String)
                            .ExecuteStoredProcAsync();

                // Create the DbDataAdapter.
                DbDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = attendanceCommand;

                // Fill the DataTable.
                DataSet tables = new DataSet();
                adapter.Fill(tables);

                attendanceCommand.Close();

                //create a new Excel package
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    //create a WorkSheet
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Campus Mappings");
                    worksheet.Cells["A1"].LoadFromDataTable(tables.Tables[0], true);

                    //sheet 1
                    worksheet.Cells["A1:F1"].Style.Font.Bold = true;
                    worksheet.Cells["A1:F100"].Style.Border.BorderAround(ExcelBorderStyle.Medium, System.Drawing.Color.LightGray);

                    worksheet.Calculate();
                    worksheet.Cells.AutoFitColumns();
                    rv.Data = excelPackage.GetAsByteArray();
                }


                return rv;
            }
            catch (Exception e)
            {
                rv.Data = null;
                rv.ResultStatus = e.Message;
                e.TrackException();
                return rv;
            }
        }
    }
}
