﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NaccasSettingService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the State board settings service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Utilities;
    using FAME.Extensions.Helpers;

    using AutoMapper;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Naccas;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The naccas setting service class.
    /// </summary>
    public class NaccasSettingService : INaccasSettingService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The school service.
        /// </summary>
        private ISchoolService schoolService;

        /// <summary>
        /// The states service.
        /// </summary>
        private IStatesService statesService;

        /// <summary>
        /// The report service.
        /// </summary>
        private IReportService reportService;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="star"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public NaccasSettingService(IAdvantageDbContext advantageDbContext, IMapper mapper)
        {
            this.advantageDbContext = advantageDbContext;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();


        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository =>
            this.advantageDbContext.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The approve program repository.
        /// </summary>
        private IRepository<SyApprovedNaccasprogramVersion> ApproveProgramRepository =>
            this.advantageDbContext.GetRepositoryForEntity<SyApprovedNaccasprogramVersion>();

        /// <summary>
        /// The naccas setting repository.
        /// </summary>
        private IRepository<SyNaccasSettings> NaccasSettingRepository =>
            this.advantageDbContext.GetRepositoryForEntity<SyNaccasSettings>();

        /// <summary>
        /// The drop reason mappings repository.
        /// </summary>
        private IRepository<SyNaccasdropReasonsMapping> DropReasonMappingsRepository =>
            this.advantageDbContext.GetRepositoryForEntity<SyNaccasdropReasonsMapping>();

        /// <summary>
        /// The save or update naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="NaccasSettings"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<ActionResult<NaccasSettings>> SaveOrUpdateNaccasReportSetting(NaccasSettings model)
        {
            return await Task.Run(
                       () =>
                           {
                               ActionResult<NaccasSettings> actionResult = new ActionResult<NaccasSettings>();
                               actionResult.ResultStatus = Enums.ResultStatus.Success;
                               actionResult.Result = model;

                               string message = string.Empty;
                               if (!this.IsValid(model, out message))
                               {
                                   actionResult.ResultStatus = Enums.ResultStatus.Error;
                                   actionResult.ResultStatusMessage = message;
                                   return actionResult;
                               }

                               try
                               {

                                   var user = this.advantageDbContext.GetTenantUser();
                                   var now = DateTime.Now;
                                   var setting = this.NaccasSettingRepository.Get(x => x.CampusId == model.CampusId)
                                       .FirstOrDefault();

                                   var campus = this.CampusRepository.Get(x => x.CampusId == model.CampusId)
                                       .FirstOrDefault();

                                   var exist = setting != null;
                                   

                                   //create setting if does not exist
                                   if (!exist)
                                   {
                                       setting = new SyNaccasSettings
                                       {
                                           CampusId = model.CampusId,
                                           CreateDate = now,
                                           CreatedById = user.Id
                                       };

                                       this.NaccasSettingRepository.Create(setting);
                                       this.NaccasSettingRepository.Save();
                                   }
                                   else {
                                       
                                       setting.ModDate = now;
                                       setting.ModUserId = user.Id;
                                   }
                                  

                                   //update campus detail settings for AllowGraduateAndOweMoney and AllowGraduateWithoutFullCompletion
                                   if (campus != null)
                                   {
                                       campus.AllowGraduateAndStillOweMoney = model.AllowGraduateAndOweMoney;
                                       campus.AllowGraduateWithoutFullCompletion =
                                           model.AllowGraduateWithoutFullCompletion;
                                       this.CampusRepository.Update(campus);
                                       this.CampusRepository.Save();
                                   }

                                   if (setting.NaccasSettingId > 0)
                                   {
                                      
                                       //delete current approved version from table
                                       var currentApprovedVersions = this.ApproveProgramRepository
                                           .Get(x => x.NaccasSettingId == setting.NaccasSettingId).ToList();
                                       this.ApproveProgramRepository.Delete(currentApprovedVersions);
                                       this.ApproveProgramRepository.Save();


                                       //insert new approved version from table
                                       var newApproveProgramVersions = new List<SyApprovedNaccasprogramVersion>();
                                       foreach (var aPV in model.ApprovedProgramVersions)
                                       {
                                           var newApprovedPV =
                                               new SyApprovedNaccasprogramVersion
                                               {
                                                   ProgramVersionId = aPV,
                                                   NaccasSettingId =
                                                           setting.NaccasSettingId,
                                                   IsApproved = true,
                                                   CreateDate = now,
                                                   CreatedById = user.Id
                                                   };

                                           newApproveProgramVersions.Add(newApprovedPV);
                                       }

                                       this.ApproveProgramRepository.Create(newApproveProgramVersions);
                                       this.ApproveProgramRepository.Save();



                                       //delete current drop reason mappings
                                       var currentMappings = this.DropReasonMappingsRepository
                                           .Get(x => x.NaccasSettingId == setting.NaccasSettingId).ToList();
                                       this.DropReasonMappingsRepository.Delete(currentMappings);
                                       this.DropReasonMappingsRepository.Save();


                                       //insert new drop reason mappings
                                       var newDropReasonMappings = new List<SyNaccasdropReasonsMapping>();
                                       foreach (var naccasDR in model.DropReasonMappings)
                                       {
                                           foreach (var advDR in naccasDR.AdvantageReasons)
                                           {
                                               var newDropReasonMapping =
                                                   new SyNaccasdropReasonsMapping
                                                       {
                                                           NaccasSettingId =
                                                               setting.NaccasSettingId,
                                                           NaccasdropReasonId =
                                                               naccasDR.NaccasReasonId,
                                                           AdvdropReasonId = advDR
                                                       };

                                               newDropReasonMappings.Add(newDropReasonMapping);
                                           }

                                       }

                                       this.DropReasonMappingsRepository.CreateAsync(newDropReasonMappings);
                                       this.DropReasonMappingsRepository.Save();


                                   }

                                   return actionResult;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   actionResult.ResultStatus = Enums.ResultStatus.Error;
                                   actionResult.ResultStatusMessage =
                                       "An error occured when trying to save the state board settings.";
                                   return actionResult;
                               }
                           });
        }

        /// <summary>
        /// The get naccas report setting.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<NaccasSettings> GetNaccasReportSetting(Guid campusId)
        {
            return await Task.Run(
                       () =>
                           {
                               try
                               {
                                   var model = new NaccasSettings();
                                   model.CampusId = campusId;
                                   model.ApprovedProgramVersions = new List<Guid>();
                                   model.DropReasonMappings = new List<DropReasonMapping>();

                                   var setting = this.NaccasSettingRepository.Get(x => x.CampusId == campusId)
                                       .FirstOrDefault();

                                   var campus = this.CampusRepository.Get(x => x.CampusId == campusId).FirstOrDefault();

                                   if (campus != null)
                                   {
                                       model.AllowGraduateAndOweMoney = campus.AllowGraduateAndStillOweMoney;
                                       model.AllowGraduateWithoutFullCompletion =
                                           campus.AllowGraduateWithoutFullCompletion;

                                   }

                                   if (setting != null)
                                   {
                                       model.CampusId = campusId;

                                       var currentApprovedVersions = this.ApproveProgramRepository
                                           .Get(
                                               x => x.NaccasSettingId == setting.NaccasSettingId
                                                    && x.ProgramVersionId.HasValue).Select(x => x.ProgramVersionId.Value)
                                           .ToList();


                                       model.ApprovedProgramVersions = currentApprovedVersions;

                                       var currentMappings = this.DropReasonMappingsRepository
                                           .Get(
                                               x => x.NaccasSettingId == setting.NaccasSettingId
                                                    && x.NaccasdropReasonId.HasValue).GroupBy(x => x.NaccasdropReasonId)
                                           .Select(
                                               x => new DropReasonMapping
                                                        {
                                                            NaccasReasonId = x.Key.Value,
                                                            AdvantageReasons =
                                                                x.Where(aR => aR.AdvdropReasonId.HasValue)
                                                                    .Select(
                                                                        ar => ar.AdvdropReasonId.Value)
                                                                    .ToList()
                                                        }).ToList();


                                       model.DropReasonMappings = currentMappings;

                                   }

                                   

                                   

                                   return model;
                               }
                               catch (Exception ex)
                               {

                                   ex.TrackException();
                                   return null;
                               }
                           });
        }


        /// <summary>
        /// The validate setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool IsValid(NaccasSettings model, out string message)
        {
            if (model == null)
            {
                message = "Setting is null";
                return false;
            }

            foreach (var m in model.DropReasonMappings)
            {
                if (m.AdvantageReasons.Count == 0)
                {
                    message = "NACCAS report has not mapping";
                    return false;
                }
            }

            message = string.Empty;
            return true;
        }

        /// <summary>
        /// The delete naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> DeleteNaccasReportSetting(NaccasSettings model)
        {
            return await Task.Run(
                       () =>
                           {
                               try
                               {


                                   var setting = this.NaccasSettingRepository.Get(x => x.CampusId == model.CampusId)
                                       .FirstOrDefault();


                                   var campus = this.CampusRepository.Get(x => x.CampusId == model.CampusId).FirstOrDefault();

                                   if (campus != null)
                                   {
                                       campus.AllowGraduateAndStillOweMoney = false;
                                       campus.AllowGraduateWithoutFullCompletion = false;

                                       this.CampusRepository.Update(campus);
                                       this.CampusRepository.Save();
                                   }


                                   if (setting != null)
                                   {
                                       // delete current approved version from table
                                       var currentApprovedVersions = this.ApproveProgramRepository
                                           .Get(x => x.NaccasSettingId == setting.NaccasSettingId).ToList();
                                       this.ApproveProgramRepository.Delete(currentApprovedVersions);
                                       this.ApproveProgramRepository.Save();

                                       // delete current drop reason mappings
                                       var currentMappings = this.DropReasonMappingsRepository
                                           .Get(x => x.NaccasSettingId == setting.NaccasSettingId).ToList();
                                       this.DropReasonMappingsRepository.Delete(currentMappings);
                                       this.DropReasonMappingsRepository.Save();

                                       // delete setting at the end
                                       this.NaccasSettingRepository.Delete(setting);
                                       this.NaccasSettingRepository.Save();
                                   }

                                   return true;
                               }
                               catch (Exception ex)
                               {
                                   ex.TrackException();
                                   return false;
                               }

                           });
        }
    }
}
