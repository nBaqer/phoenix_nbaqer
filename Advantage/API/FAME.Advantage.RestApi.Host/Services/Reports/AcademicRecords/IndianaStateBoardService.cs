﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IndianaStateBoardService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Indiana State Board service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.EntityFrameworkCore;


    /// <summary>
    /// The Indiana State Board Service.
    /// </summary>
    public class IndianaStateBoardService : IIndianaStateBoardService
    {
        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndianaStateBoardService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        public IndianaStateBoardService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService, IReportService reportService, IEnrollmentService enrollmentService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
            this.reportService = reportService;
            this.enrollmentService = enrollmentService;
        }


        /// <summary>
        /// The student Enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> ArStuEnrollments =>
            this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The system status codes repository.
        /// </summary>
        private IRepository<SyStatusCodes> SyStatusCodes =>
            this.context.GetRepositoryForEntity<SyStatusCodes>();

        /// <summary>
        /// The transactions.
        /// </summary>
        private IRepository<SaTransactions> Transactions =>
            this.context.GetRepositoryForEntity<SaTransactions>();

        /// <summary>
        /// The system status repository.
        /// </summary>
        private IRepository<SySysStatus> SySysStatus =>
           this.context.GetRepositoryForEntity<SySysStatus>();

        /// <summary>
        /// The system student attendance summary.
        /// </summary>
        private IRepository<SyStudentAttendanceSummary> SyStudentAttendanceSummary =>
            this.context.GetRepositoryForEntity<SyStudentAttendanceSummary>();

        /// <summary>
        /// The system student status changes.
        /// </summary>
        private IRepository<SyStudentStatusChanges> SyStudentStatusChanges =>
            this.context.GetRepositoryForEntity<SyStudentStatusChanges>();

        /// <summary>
        /// The audit history repository.
        /// </summary>
        private IRepository<SyAuditHistDetail> AuditHistoryRepository =>
            this.context.GetRepositoryForEntity<SyAuditHistDetail>();

        /// <summary>
        /// The at class sect attendance.
        /// </summary>
        private IRepository<AtClsSectAttendance> AtClsSectAttendance =>
            this.context.GetRepositoryForEntity<AtClsSectAttendance>();

        /// <summary>
        /// The at class sect attendance.
        /// </summary>
        private IRepository<AdLeadByLeadGroups> AdLeadByLeadGroups =>
            this.context.GetRepositoryForEntity<AdLeadByLeadGroups>();


        /// <summary>
        /// The State Board Program Course Mappings repository.
        /// </summary>
        private IRepository<SyStateBoardProgramCourseMappings> syStateBoardProgramCourseMappings =>
            this.context.GetRepositoryForEntity<SyStateBoardProgramCourseMappings>();

        /// <summary>
        /// The Stat Board Courses repository.
        /// </summary>
        private IRepository<SyStateBoardCourses> syStateBoardCourses =>
            this.context.GetRepositoryForEntity<SyStateBoardCourses>();

        /// <summary>
        /// The get indiana state board report data.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="month">
        /// The month.
        /// </param>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <param name="studentGroup">
        /// The student group.
        /// </param>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IndianaStateBoardReportResult>> GetIndianaStateBoardReportData(Guid campusId, int month, int year, Guid? studentGroup, Guid? programVersion)
        {
            return await Task.Run(() =>
            {
                List<IndianaStateBoardReportResult> results = new List<IndianaStateBoardReportResult>();
                DateTime startDate = new DateTime(year, month, 1);
                DateTime endDate = startDate.AddMonths(1).AddDays(-1);

                List<Guid> studentInGroup = new List<Guid>();

                if (studentGroup.HasValue)
                {
                    studentInGroup = this.AdLeadByLeadGroups
                        .Get(
                            x => x.LeadGrpId == studentGroup.Value && x.LeadGrp.UseForStudentGroupTracking.HasValue
                                                                   && x.LeadGrp.UseForStudentGroupTracking.Value
                                                                   && x.StuEnrollId.HasValue)
                        .Select(x => x.StuEnrollId.Value)
                        .ToList();
                }


                var stuEnrollmentIdtemp0 = this.SyStudentAttendanceSummary.Get(x => x.StudentAttendedDate >= startDate && x.StudentAttendedDate <= endDate && x.ActualDays > 0)
                .Select(y => y.StuEnrollId).Distinct()
                .ToList();


                var results01 = this.ArStuEnrollments.Get(x => stuEnrollmentIdtemp0.Contains(x.StuEnrollId) && (studentInGroup.Contains(x.StuEnrollId) || studentInGroup.Count == 0))
                    .Include(x => x.PrgVer).ThenInclude(x => x.Prog)
                    .ThenInclude(x => x.SyStateBoardProgramCourseMappings)
                    .ThenInclude(x => x.StateBoardCourse)
                .Select(x => new IndianaStateBoardReportResult
                {
                    StuEnrollId = x.StuEnrollId,
                    BadgeStudentumber = x.BadgeNumber,
                    EnrollmentStart = x.StartDate,
                    LastName = x.Lead.LastName,
                    MiddleName = x.Lead.MiddleName,
                    FirstName = x.Lead.FirstName,
                    EnrollmentId = x.EnrollmentId,
                    ProgramVersionCode = x.PrgVer.Prog.SyStateBoardProgramCourseMappings.FirstOrDefault() != null ? x.PrgVer.Prog.SyStateBoardProgramCourseMappings.FirstOrDefault().StateBoardCourse.Code : String.Empty,
                    ProgramVersionId = x.PrgVerId

                }).ToList();

                var results0 = this.SyStudentStatusChanges.Get(
                    x => (studentInGroup.Contains(x.StuEnrollId) || studentInGroup.Count == 0) && x.DateOfChange.HasValue && x.NewStatusId.HasValue && x.NewStatus.SysStatus.StatusLevelId == 2 && x.CampusId == campusId && x.NewStatus.SysStatus.InSchool.HasValue && (
                        (x.DateOfChange <= endDate && x.DateOfChange >= startDate)
                        || (x.DateOfChange <= startDate && x.NewStatus.SysStatusId != 14)) && stuEnrollmentIdtemp0.Contains(x.StuEnrollId))
                     .Include(x => x.StuEnroll)
                    .ThenInclude(x => x.PrgVer).ThenInclude(x => x.Prog)
                    .ThenInclude(x => x.SyStateBoardProgramCourseMappings)
                    .ThenInclude(x => x.StateBoardCourse)
                .Select(x => new IndianaStateBoardReportResult
                {
                    StuEnrollId = x.StuEnrollId,
                    BadgeStudentumber = x.StuEnroll.BadgeNumber,
                    EnrollmentStart = x.StuEnroll.StartDate,
                    DateOfChange = x.DateOfChange,
                    ToStatus = x.NewStatus.StatusCodeDescrip,
                    FromStatus = x.OrigStatusId.HasValue ? x.OrigStatus.StatusCodeDescrip : string.Empty,
                    IsInSchoolStatus = x.NewStatus.SysStatus.InSchool == 1,
                    LastName = x.StuEnroll.Lead.LastName,
                    MiddleName = x.StuEnroll.Lead.MiddleName,
                    FirstName = x.StuEnroll.Lead.FirstName,
                    EnrollmentId = x.StuEnroll.EnrollmentId,
                    ProgramVersionCode = x.StuEnroll.PrgVer.Prog.SyStateBoardProgramCourseMappings.FirstOrDefault() != null ? x.StuEnroll.PrgVer.Prog.SyStateBoardProgramCourseMappings.FirstOrDefault().StateBoardCourse.Code : String.Empty,
                    ProgramVersionId = x.StuEnroll.PrgVerId,
                    FromSysStatusCode = x.OrigStatusId.HasValue ? x.OrigStatus.SysStatusId : 0,
                    ToSysStatusCode = x.NewStatus.SysStatusId,

                }).ToList();

                results = results0.OrderByDescending(y => y.DateOfChange)
                   .GroupBy(y => y.StuEnrollId)
                   .Select(x => new IndianaStateBoardReportResult
                   {
                       StuEnrollId = x.Key,
                       BadgeStudentumber = x.FirstOrDefault().BadgeStudentumber,
                       ProgramVersionCode = x.FirstOrDefault().ProgramVersionCode,
                       ProgramVersionId = x.FirstOrDefault().ProgramVersionId,
                       LastName = x.FirstOrDefault().LastName,
                       MiddleName = x.FirstOrDefault().MiddleName,
                       FirstName = x.FirstOrDefault().FirstName,
                       EnrollmentId = x.FirstOrDefault().EnrollmentId,
                       EnrollmentStart = x.FirstOrDefault().EnrollmentStart,
                       DateOfChange = x.FirstOrDefault().DateOfChange,
                       ToStatus = x.FirstOrDefault().ToStatus,
                       FromStatus = x.FirstOrDefault(y => y.DateOfChange <= startDate) != null ? x.FirstOrDefault(y => y.DateOfChange <= startDate).ToStatus : string.Empty,
                       IsInSchoolStatus = x.FirstOrDefault(y => y.DateOfChange <= startDate) != null && x.FirstOrDefault(y => y.DateOfChange <= startDate).IsInSchoolStatus,
                       ReportStart = startDate,
                       ReportEnd = endDate,
                       ToSysStatusCode = x.FirstOrDefault().ToSysStatusCode,
                       FromSysStatusCode = x.FirstOrDefault().FromSysStatusCode,

                   })
                   .Where(x => x.IsInSchoolStatus)
                   .ToList();

                results = (from x in results01
                           join b in results on x.StuEnrollId equals b.StuEnrollId into xb
                           from _xb in xb.DefaultIfEmpty()
                           select new IndianaStateBoardReportResult
                           {
                               StuEnrollId = x.StuEnrollId,
                               BadgeStudentumber = x.BadgeStudentumber,
                               EnrollmentStart = x.EnrollmentStart,
                               DateOfChange = _xb != null ? _xb.DateOfChange : null,
                               ToStatus = _xb != null ? _xb.ToStatus : null,
                               FromStatus = _xb != null ? _xb.FromStatus : null,
                               IsInSchoolStatus = _xb != null ? _xb.IsInSchoolStatus : true,
                               ToSysStatusCode = _xb != null ? _xb.ToSysStatusCode : 0,
                               FromSysStatusCode = _xb != null ? _xb.FromSysStatusCode : 0,
                               LastName = x.LastName,
                               MiddleName = x.MiddleName,
                               FirstName = x.FirstName,
                               EnrollmentId = x.EnrollmentId,
                               ReportStart = startDate,
                               ReportEnd = endDate,
                               ProgramVersionCode = x.ProgramVersionCode,
                               ProgramVersionId = x.ProgramVersionId

                           }).ToList();



                List<Guid> stuEnrollmentIds = results.Select(x => x.StuEnrollId).ToList();



                var hourAccruedResults = this.SyStudentAttendanceSummary
                    .Get(x => x.StuEnrollId.HasValue && stuEnrollmentIds.Contains(x.StuEnrollId.Value) && x.StudentAttendedDate.HasValue && x.StudentAttendedDate <= endDate && x.ActualDays > 0)
                    .GroupBy(y => y.StuEnrollId)
                    .Select(x => new
                    {
                        StuEnrollId = x.Key,
                        PresentHours = x.Max(_ => _.ActualRunningPresentDays),
                        MakeUpHours = x.Max(_ => _.ActualRunningMakeupDays),
                        LDA = x.Max(_ => _.StudentAttendedDate.Value)
                    })
                    .Select(z => new HoursAccrued()
                    {
                        StuEnrollId = z.StuEnrollId,
                        Hours = z.PresentHours + z.MakeUpHours,
                        LDA = z.LDA
                    }).ToList();

                var tuitionResults = this.Transactions
                    .Get(
                        tr => stuEnrollmentIds.Contains(tr.StuEnrollId) && !tr.Voided &&
                                                                         tr.TransDate <= endDate)
                    .GroupBy(t => t.StuEnrollId)
                    .Select(t => new Tuition
                    {
                        StuEnrollmentId = t.Key,
                        Amount = t.Sum(a => a.TransAmount)
                    }).ToList();


                var resultsWithHours = results
                    .GroupJoin(hourAccruedResults, r => r.StuEnrollId, ha => ha.StuEnrollId, (r, lda) => new { Result = r, HAResult = lda.DefaultIfEmpty() })
                    .Select(x => new IndianaStateBoardReportResult
                    {
                        StuEnrollId = x.Result.StuEnrollId,
                        BadgeStudentumber = x.Result.BadgeStudentumber,
                        ProgramVersionCode = x.Result.ProgramVersionCode,
                        ProgramVersionId = x.Result.ProgramVersionId,
                        LastName = x.Result.LastName,
                        MiddleName = x.Result.MiddleName,
                        FirstName = x.Result.FirstName,
                        EnrollmentId = x.Result.EnrollmentId,
                        EnrollmentStart = x.Result.EnrollmentStart,
                        DateOfChange = x.Result.DateOfChange,
                        ToStatus = x.Result.ToStatus,
                        FromStatus = x.Result.FromStatus,
                        IsInSchoolStatus = x.Result.IsInSchoolStatus,
                        ReportStart = x.Result.ReportStart,
                        ReportEnd = x.Result.ReportEnd,
                        LDA = x.HAResult.FirstOrDefault()?.LDA.ToString("MM/dd/yyyy"),
                        HoursAccrued = x.HAResult.FirstOrDefault()?.Hours,
                        FromSysStatusCode = x.Result.FromSysStatusCode,
                        ToSysStatusCode = x.Result.ToSysStatusCode
                    }).ToList();

                var finalResults = resultsWithHours
                    .GroupJoin(tuitionResults, r => r.StuEnrollId, t => t.StuEnrollmentId, (r, t) => new { Result = r, TResult = t.DefaultIfEmpty() })
                    .Select(x => new IndianaStateBoardReportResult
                    {
                        StuEnrollId = x.Result.StuEnrollId,
                        BadgeStudentumber = x.Result.BadgeStudentumber,
                        ProgramVersionCode = x.Result.ProgramVersionCode,
                        ProgramVersionId = x.Result.ProgramVersionId,
                        LastName = x.Result.LastName,
                        MiddleName = x.Result.MiddleName,
                        FirstName = x.Result.FirstName,
                        EnrollmentId = x.Result.EnrollmentId,
                        EnrollmentStart = x.Result.EnrollmentStart,
                        DateOfChange = x.Result.DateOfChange,
                        ToStatus = x.Result.ToStatus,
                        FromStatus = x.Result.FromStatus,
                        IsInSchoolStatus = x.Result.IsInSchoolStatus,
                        ReportStart = x.Result.ReportStart,
                        ReportEnd = x.Result.ReportEnd,
                        LDA = x.Result.LDA,
                        LDAFormatted = x.Result.LDA,
                        HoursAccrued = x.Result.HoursAccrued,
                        TuitionOwed = x.TResult.FirstOrDefault()?.OweTuitionDescription,
                        FromSysStatusCode = x.Result.FromSysStatusCode,
                        ToSysStatusCode = x.Result.ToSysStatusCode
                    }).ToList();
                if (programVersion.HasValue && programVersion != Guid.Empty)
                {
                    finalResults = finalResults.Where(x => x.ProgramVersionId == programVersion).ToList();
                }

                //foreach (var result in finalResults)
                //{
                //    DateTime? lastDateOfAttendance = Task.Run(() => this.enrollmentService.GetLDA(result.StuEnrollId)).Result;
                //    if (lastDateOfAttendance.HasValue)
                //    {
                //        var dateFormatted = lastDateOfAttendance.Value.ToString("MM/dd/yyyy");
                //        result.LDA = dateFormatted;
                //        result.LDAFormatted = dateFormatted;
                //    }

                //}

                return finalResults.OrderBy(r => r.LastName).ThenBy(r => r.FirstName).ToList();
            });
        }
    }
}

