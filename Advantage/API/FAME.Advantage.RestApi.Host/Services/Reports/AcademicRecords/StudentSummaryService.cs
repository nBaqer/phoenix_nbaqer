﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusService.cs" company="Fame Inc">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.SystemCatalog
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The student status service.
    /// </summary>
    public class StudentSummaryService : IStudentSummaryService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The student service.
        /// </summary>
        private IStudentService studentService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private IEnrollmentService enrollmentService;

        /// <summary>
        /// The attendances service.
        /// </summary>
        private IAttendanceService attendanceService;

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository =>
            this.advantageContext.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentStatusService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="advantageContext">
        /// The advantage context.
        /// </param>
        public StudentSummaryService(IMapper mapper, IAdvantageDbContext advantageContext, IStudentService studentService, IEnrollmentService enrollmentService, IAttendanceService attendanceService)
        {
            this.mapper = mapper;
            this.advantageContext = advantageContext;
            this.studentService = studentService;
            this.enrollmentService = enrollmentService;
            this.attendanceService = attendanceService;
        }

        /// <summary>
        /// The get students to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<StudentSummary>> GetStudentSummaryByEnrollmentId(string enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<StudentSummary>();

                           try
                           {
                               var enrollment = this.EnrollmentRepository.Get()
                               .Include(x => x.StatusCode)
                               .Include(x => x.Campus)
                               .Include(x => x.PrgVer).ThenInclude(x => x.Prog)
                               .Include(x => x.Lead).ThenInclude(x => x.GenderNavigation)
                               .Where(a => a.StuEnrollId.ToString() == enrollmentId).AsNoTracking().FirstOrDefault();

                               if (enrollment == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"Student with EnrollmentId of {enrollmentId} not found.";
                                   return result;
                               }

                               var enrollmentSummary = await this.enrollmentService.GetEnrollmentProgramSummary(enrollment.StuEnrollId);
                               var attendanceSummary = await this.attendanceService.CalculateAttendance(enrollment.StuEnrollId, true, true, true);
                               var studentContactSummary = await this.studentService.GetStudentContactInfo(enrollment.StuEnrollId);

                               StudentSummary studentSummary = new StudentSummary();
                               studentSummary.FirstName = enrollment.Lead.FirstName;
                               studentSummary.LastName = enrollment.Lead.LastName;
                               studentSummary.StudentNumber = enrollment.Lead.StudentNumber;
                               studentSummary.Role = "student";
                               studentSummary.BirthDate = enrollment.Lead.BirthDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.Gender = enrollment.Lead.GenderNavigation.GenderDescrip;
                               studentSummary.StartDate = enrollment.StartDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.ExpectedGraduationDate = enrollment.ExpGradDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.SchoolName = enrollment.Campus.SchoolName.Trim();
                               studentSummary.ProgramDescription = enrollment.PrgVer.Prog.ProgDescrip;
                               studentSummary.ProgramVersionDescription = enrollment.PrgVer.PrgVerDescrip;
                               studentSummary.Status = enrollment.StatusCode.StatusCodeDescrip;

                               studentSummary.EnrollmentId = enrollment.StuEnrollId.ToString().Trim().ToUpper();
                               studentSummary.CampusId = enrollment.Campus.CampusId.ToString().Trim().ToUpper();
                               studentSummary.ProgramId = enrollment.PrgVer.Prog.ProgId.ToString().Trim().ToUpper();
                               studentSummary.ProgramVersionId = enrollment.PrgVer.PrgVerId.ToString().Trim().ToUpper();
                               studentSummary.StatusCodeId = enrollment.StatusCode.StatusCodeId.ToString().Trim().ToUpper();

                               if (attendanceSummary != null)
                               {
                                   studentSummary.TotalHours = attendanceSummary.ActualHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.AbsentHours = attendanceSummary.AbsentHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.MakeupHours = attendanceSummary.MakeupHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.LastDateAttended = attendanceSummary.LastDateAttended?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                                   studentSummary.AttendancePercentage = attendanceSummary.AttendancePercentage.ToString("0.00") ?? string.Empty;
                               }

                               if (enrollmentSummary != null)
                               {
                                   studentSummary.OverallGPA = enrollmentSummary.OverallGPAString;
                               }

                               if (studentContactSummary != null)
                               {
                                   studentSummary.Phone = studentContactSummary.Phone;
                                   studentSummary.Address = studentContactSummary.Address;
                                   studentSummary.City = studentContactSummary.City;
                                   studentSummary.State = studentContactSummary.State;
                                   studentSummary.Zip = studentContactSummary.Zip;
                                   studentSummary.Email = studentContactSummary.Email;
                                   studentSummary.PhoneOther = studentContactSummary.PhoneOther;
                               }

                               result.Result = studentSummary;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               result.AsString = $"Student summary for student {studentSummary.FirstName} {studentSummary.LastName} - {studentSummary.EnrollmentId} - {DateTime.Now}";
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }
    }
}
