﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InvoiceReportService.cs" company="FAME.INC">
//   2018
// </copyright>
// <summary>
//   Defines the InvoiceReportService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    public class InvoiceReportService : IInvoiceReportService
    {
        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// Initializes a new instance of the <see cref="InvoiceReportService"/> class.
        /// </summary>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        /// <param name="appSettingsService">
        /// The app settings service.
        /// </param>
        public InvoiceReportService(IReportService reportService, ISystemConfigurationAppSettingService appSettingsService)
        {
            this.reportService = reportService;
            this.appSettingsService = appSettingsService;
        }

        public async Task<ReportActionResult> GetInvoiceReport(List<Guid> enrollments,Guid campusId, DateTime refDate)
        {
            ReportRequest reportRequest = new ReportRequest
                                              {
                                                  ReportLocation = Infrastructure.ReportLocations.InvoiceReport,
                                                  ReportDataSourceType = ReportDataSourceType.SqlServer,
                                                  ReportOutput = ReportOutput.Pdf,
                                                  Timeout = TimeSpan.FromMinutes(3)
                                              };


            reportRequest.Parameters = this.SetParameterDictionary(enrollments,campusId,refDate);
            var actionResult = await this.reportService.GenerateReport(reportRequest);
            if (actionResult.Data != null)
            {
                return actionResult;
            }

            return null;
        }

        /// <summary>
        /// The set parameter dictionary.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="refDate">
        /// The ref date.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        private Dictionary<string, object> SetParameterDictionary(List<Guid> enrollments, Guid campusId, DateTime refDate)
        {
            var enrollmentString = string.Join(",", enrollments.Select(e => e.ToString()).ToList());

            return new Dictionary<string, object>
                       {
                           { "StuEnrollIdList", enrollmentString },
                           { "CampusId", campusId },
                           { "RefDate", refDate }
                       };
        }
    }
}
