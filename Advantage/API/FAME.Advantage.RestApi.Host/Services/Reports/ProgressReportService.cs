﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgressReportService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ProgressReportService.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel.Security;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Progress;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The progress report service.
    /// </summary>
    public class ProgressReportService : IProgressReportService
    {


        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;


        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgressReportService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        /// <param name="appSettingsService">
        /// The app settings service.
        /// </param>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        public ProgressReportService(IAdvantageDbContext context, IEnrollmentService enrollmentService, ISystemConfigurationAppSettingService appSettingsService, IReportService reportService)
        {

            this.context = context;
            this.enrollmentService = enrollmentService;
            this.reportService = reportService;
            this.appSettingsService = appSettingsService;
        }

        /// <summary>
        /// The user id.
        /// </summary>
        private Guid UserId => context.GetTenantUser().Id;

        /// <summary>
        /// The get progress report.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="showWorkUnitGrouping"></param>
        /// <param name="showFinanceCalculations"></param>
        /// <param name="ShowWeeklySchedule"></param>
        /// <param name="ShowTermModule"></param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ReportActionResult> GetProgressReport(Guid studentEnrollmentId, bool showWorkUnitGrouping = true, bool showFinanceCalculations = false, bool ShowWeeklySchedule = true, bool ShowTermModule = false)
        {
            var enrollment = await this.enrollmentService.GetEnrollment(studentEnrollmentId, true);
            var param = new ProgressReportParam();
            param.ShowWorkUnitGrouping = showWorkUnitGrouping;
            param.ShowFinanceCalculations = showFinanceCalculations;
            param.ShowWeeklySchedule = ShowWeeklySchedule;
            param.ShowTermModule = ShowTermModule;
            return await this.GetProgressReport(enrollment, param);
        }

        /// <summary>
        /// The get progress report.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ReportActionResult> GetProgressReport(Enrollment enrollment, ProgressReportParam param)
        {
            if (enrollment != null)
            {
                var campusId = enrollment.CampusId;
                var startDate = enrollment.StartDate.HasValue ? enrollment.StartDate.Value.ToShortDateString() : DateTime.Today.ToShortDateString();
                var trackAttendance =
                    this.appSettingsService.GetAppSettingValueByCampus(SapCheckConfigurationKeys.TrackSapAttendance,
                        campusId);
                if (string.IsNullOrEmpty(trackAttendance))
                {
                    trackAttendance =
                        this.appSettingsService.GetAppSettingValue(SapCheckConfigurationKeys.TrackSapAttendance);
                }

                var gradesFormat = this.appSettingsService.GetAppSettingValueByCampus(
                    ConfigurationAppSettingsKeys.GradesFormat,
                    campusId);
                if (string.IsNullOrEmpty(gradesFormat))
                {
                    gradesFormat = this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GradesFormat);
                }

                var gradeBookWeightingLevel = this.appSettingsService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.GradeBookWeightingLevel,
                    campusId);
                if (string.IsNullOrEmpty(gradeBookWeightingLevel))
                {
                    gradeBookWeightingLevel =
                        this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GradeBookWeightingLevel);
                }

                var gradeRoundingString =
                    this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GradeRounding);

                var studentIdentifierValue =
                    this.appSettingsService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.StudentIdentifier, campusId);

                var gpaMethod = this.appSettingsService.GetAppSettingValue(ConfigurationAppSettingsKeys.GPAMethod).ToLower();

                var gradeRounding = gradeRoundingString.Equals(ConfigurationAppSettingsValues.Yes, StringComparison.InvariantCultureIgnoreCase);

                trackAttendance = trackAttendance.Substring(2).ToLower();
                gradesFormat = gradesFormat.ToLower();

                ReportRequest reportRequest = new ReportRequest
                {
                    ReportLocation = Infrastructure.ReportLocations.ProgressReport,
                    ReportDataSourceType = ReportDataSourceType.SqlServer,
                    ReportOutput = ReportOutput.Pdf,
                    Timeout = TimeSpan.FromMinutes(3)
                };

                List<GradeBookComponents> gbc = new List<GradeBookComponents>()
                {
                    GradeBookComponents.Exam,
                    GradeBookComponents.Externships,
                    GradeBookComponents.Final,
                    GradeBookComponents.Homework,
                    GradeBookComponents.LabHours,
                    GradeBookComponents.LabWork,
                    GradeBookComponents.PracticalExams
                };

                var gradeBookComponents = string.Join(",", gbc.Cast<int>().ToList());

                param.StudentEnrollmentId = enrollment.EnrollmentId;
                param.GradeFormat = gradesFormat;
                param.GPAMethod = gpaMethod;
                param.SysComponentTypeId = gradeBookComponents;
                param.GradeRounding = gradeRounding;
                param.StartDate = startDate;
                param.ExpectedGradDate = enrollment.ExpectedGraduationDate.ToShortDateString();
                param.UserId = UserId.ToString();
                param.TrackAttendanceBy = trackAttendance;
                param.SetGradeBookAt = gradeBookWeightingLevel;
                param.CampusId = campusId;
                param.StudentIdentifier = string.IsNullOrEmpty(studentIdentifierValue) ? null : studentIdentifierValue.Trim().ToLower();

                reportRequest.Parameters = SetParameterDictionary(param);
                var actionResult = await this.reportService.GenerateReport(reportRequest);

                if (actionResult.Data != null)
                {
                    return actionResult;
                }
            }

            return null;
        }

        /// <summary>
        /// The set parameter dictionary.
        /// </summary>
        /// <param name="param">
        /// The param.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        private Dictionary<string, object> SetParameterDictionary(ProgressReportParam param)
        {
            return new Dictionary<string, object>
                                                               {
                                                                   { "StuEnrollId", param.StudentEnrollmentId },
                                                                   { "CampGrpId" , param.CampusGroupsId},
                                                                   { "PrgVerId",param.ProgramVersionId},
                                                                   { "StatusCodeId" , param.StatusCodeId},
                                                                   { "TermId",param.TermId},
                                                                   { "StudentGrpId" , param.StudentGroupId},
                                                                   { "GradesFormat", param.GradeFormat},
                                                                   { "GPAMethod", param.GPAMethod },
                                                                   { "ShowWorkUnitGrouping", param.ShowWorkUnitGrouping },
                                                                   { "SysComponentTypeId", param.SysComponentTypeId },
                                                                   { "OrderBy",param.OrderBy},
                                                                   { "ShowFinanceCalculations" ,param.ShowFinanceCalculations},
                                                                   { "ShowWeeklySchedule",param.ShowWeeklySchedule},
                                                                   { "GradeRounding", param.GradeRounding},
                                                                   { "ShowStudentSignatureLine",param.ShowStudentSignatureLine},
                                                                   { "ShowSchoolSignatureLine" ,param.ShowSchoolSignatureLine},
                                                                   { "ShowPageNumber",param.ShowPageNumber},
                                                                   { "ShowHeading" , param.ShowHeading},
                                                                   { "StartDateModifier",param.StartDateModifier},
                                                                   { "StartDate" , param.StartDate},
                                                                   { "ExpectedGradDateModifier",param.ExpectedGradDateModifier},
                                                                   { "ExpectedGradDate", param.ExpectedGradDate},
                                                                   { "UserId",param.UserId},
                                                                   { "SchoolName" , param.SchoolName},
                                                                   { "TrackAttendanceBy",param.TrackAttendanceBy},
                                                                   { "SetGradeBookAt" , param.SetGradeBookAt},
                                                                   { "DisplayHours",param.DisplayHours},
                                                                   { "ShowTermModule" , param.ShowTermModule},
                                                                   { "ShowDateInFooter",param.ShowDateInFooter},
                                                                   { "StudentIdentifier" , param.StudentIdentifier},
                                                                   { "CampusId",param.CampusId},
                                                                   { "ShowAllEnrollments" , param.ShowAllEnrollments}

                                                               };
        }
    }
}
