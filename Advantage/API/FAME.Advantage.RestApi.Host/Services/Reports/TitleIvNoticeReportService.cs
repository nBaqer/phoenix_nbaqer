﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIvNoticeReportService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   the TitleIvNoticeReportService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The title iv notice report service.
    /// </summary>
    public class TitleIvNoticeReportService : ITitleIvNoticeReportService
    {

        /// <summary>
        /// The report service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// The app settings service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingsService;

        /// <summary>
        /// The title iv service.
        /// </summary>
        private readonly ITitleIVSAPService titleIvService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIvNoticeReportService"/> class.
        /// </summary>
        /// <param name="reportService">
        /// The report service.
        /// </param>
        public TitleIvNoticeReportService(IReportService reportService, ISystemConfigurationAppSettingService appSettingsService, ITitleIVSAPService titleIvService)
        {
            this.reportService = reportService;
            this.appSettingsService = appSettingsService;
            this.titleIvService = titleIvService;
        }

        /// <summary>
        /// The get title iv notice report.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ReportActionResult> GetTitleIvNoticeReport(List<Guid> enrollments)
        {
            ReportRequest reportRequest = new ReportRequest
            {
                ReportLocation = Infrastructure.ReportLocations.TitleIvNoticeReport,
                ReportDataSourceType = ReportDataSourceType.SqlServer,
                ReportOutput = ReportOutput.Pdf,
                Timeout = TimeSpan.FromMinutes(3)
            };


            var schoolName = this.appSettingsService.GetAppSettingValue("schoolname");


            reportRequest.Parameters = SetParameterDictionary(enrollments, schoolName);
            var actionResult = await this.reportService.GenerateReport(reportRequest);
            if (actionResult.Data != null)
            {
                await this.titleIvService.UpdatePrintFlag(enrollments);
                return actionResult;
            }

            return actionResult;
        }


        /// <summary>
        /// The set parameter dictionary.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <param name="schoolName">
        /// The school Name.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        private Dictionary<string, object> SetParameterDictionary(List<Guid> enrollments, string schoolName)
        {
            var enrollmentString = string.Join(",", enrollments.Select(e => e.ToString()).ToList());

            return new Dictionary<string, object>
                                                               {
                                                                   { "StudentEnrollmentList", enrollmentString },
                                                                   { "UseEnrollment" , true},
                                                                   { "CampusId",null},
                                                                   { "ProgramId" , null},
                                                                   { "StatusCodeId",null},
                                                                   { "schoolName" , schoolName}

                                                               };
        }
    }
}
