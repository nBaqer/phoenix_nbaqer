﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ReportService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using AutoMapper;
using FAME.Advantage.RestApi.Host.Utilities;
using Microsoft.EntityFrameworkCore;
using Unity.Interception.Utilities;

namespace FAME.Advantage.RestApi.Host.Services.Reports
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.ServiceModel.Security;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;

    using Microsoft.Extensions.Configuration;

    using ReportExecutionClient;

    using ReportServiceClient;

    using Task = System.Threading.Tasks.Task;

    /// <summary>
    /// The report service.
    /// </summary>
    public class ReportService : IReportService
    {
        /// <summary>
        /// The unit of work instance.
        /// </summary>
        private readonly IAdvantageDbContext advantageDbContext;

        /// <summary>
        /// The report connection.
        /// </summary>
        private readonly IReportConnection reportConnection;

        /// <summary>
        /// The report server settings.
        /// </summary>
        private readonly IReportServerSettings reportServerSettings;

        /// <summary>
        /// The configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService configurationAppSettingService;
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The report service url.
        /// </summary>
        private string reportServiceUrl;

        /// <summary>
        /// The report execution url.
        /// </summary>
        private string reportExecutionUrl;

        /// <summary>
        /// The report execution service.
        /// </summary>
        private ReportExecutionServiceSoapClient reportExecutionService;



        /// <summary>
        /// Initializes a new instance of the <see cref="ReportService"/> class.
        /// </summary>
        /// <param name="advantageDbContext">
        /// The advantage db context.
        /// </param>
        /// <param name="reportConnection">
        /// The report Connection.
        /// </param>
        /// <param name="reportServerSettings">
        /// The report Server Settings.
        /// </param>
        /// <param name="configurationAppSettingService">
        /// The configuration App Setting Service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="schoolService">
        /// The school Service.
        /// </param>
        public ReportService(IAdvantageDbContext advantageDbContext, IReportConnection reportConnection, IReportServerSettings reportServerSettings, ISystemConfigurationAppSettingService configurationAppSettingService, IMapper mapper)
        {
            this.advantageDbContext = advantageDbContext;
            this.reportConnection = reportConnection;
            this.reportServerSettings = reportServerSettings;
            this.configurationAppSettingService = configurationAppSettingService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageDbContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The reports repository.
        /// </summary>
        private IRepository<SyReports> ReportsRepository => this.advantageDbContext.GetRepositoryForEntity<SyReports>();

        /// <summary>
        /// The Generate Report Report from SQL Reporting Services.
        /// </summary>
        /// <param name="reportRequest">
        /// The report Request.
        /// </param>
        /// <returns>
        /// A byte array in the requested output. Eg. PDF.
        /// </returns>
        public async Task<ReportActionResult> GenerateReport(IReportRequest reportRequest)
        {
            ReportActionResult actionResult = new ReportActionResult();

            this.reportServiceUrl = this.configurationAppSettingService.GetAppSettingValue(ConfigurationAppSettingsKeys.ReportServicesUrl);
            this.reportExecutionUrl = this.configurationAppSettingService.GetAppSettingValue(ConfigurationAppSettingsKeys.ReportExecutionUrl);

            string report = this.reportServerSettings.ReportServerFolder + reportRequest.ReportLocation;

            var binding = new BasicHttpBinding();
            var endpointReportExecutionAddress = new EndpointAddress(this.reportExecutionUrl);

            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;

            binding.MaxReceivedMessageSize = this.reportServerSettings.MaxReportOutputSize ?? 104857600;

            binding.Security.Mode = this.reportServerSettings.IsConnectionSecuredToReportingServices ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.TransportCredentialOnly;

            if (reportRequest.Timeout.HasValue)
            {
                binding.CloseTimeout = reportRequest.Timeout.Value;
                binding.OpenTimeout = reportRequest.Timeout.Value;
                binding.ReceiveTimeout = reportRequest.Timeout.Value;
                binding.SendTimeout = reportRequest.Timeout.Value;
            }

            // Create the execution service SOAP Client
            this.reportExecutionService = new ReportExecutionServiceSoapClient(binding, endpointReportExecutionAddress);

            if (this.reportExecutionService != null)
            {

                if (this.reportServerSettings.Credentials.IsImpersonationEnabled)
                {
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
                    if (this.reportExecutionService.ClientCredentials != null)
                    {
                        this.reportExecutionService.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                        this.reportExecutionService.ClientCredentials.UserName.UserName = this.reportServerSettings.Credentials.Username;
                        this.reportExecutionService.ClientCredentials.UserName.Password = this.reportServerSettings.Credentials.Password;
                        this.reportExecutionService.ClientCredentials.Windows.ClientCredential.UserName = this.reportServerSettings.Credentials.Username;
                        this.reportExecutionService.ClientCredentials.Windows.ClientCredential.Password = this.reportServerSettings.Credentials.Password;
                    }
                }
                else
                {
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.InheritedFromHost;
                    if (this.reportExecutionService.ClientCredentials != null)
                    {
                        this.reportExecutionService.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                    }
                }

                try
                {
                    var trustedUser = new TrustedUserHeader();

                    var clientCredentials = this.reportExecutionService.ClientCredentials;
                    if (clientCredentials != null)
                    {
                        trustedUser.UserName = clientCredentials.UserName.UserName;
                    }

                    var taskLoadReport = await this.reportExecutionService.LoadReportAsync(trustedUser, report, null);
                    if (taskLoadReport != null)
                    {
                        /*
                         * Get Report required parameters in here
                         * Validate if the parameters in the dictionary matches the required parameters by the report.
                         */

                        if (reportRequest.Parameters == null)
                        {
                            reportRequest.Parameters = new Dictionary<string, object>();
                        }

                        if (reportRequest.ReportDataSourceType == ReportDataSourceType.RestApi)
                        {
                            var token = this.reportConnection.Token;
                            if (token.ToLower().Contains("bearer "))
                            {
                                var tokenString = token.Split(' ');
                                token = tokenString[tokenString.Length - 1];
                            }
                            reportRequest.Parameters.Add("ApiURL", this.reportConnection.ApiUrl);
                            reportRequest.Parameters.Add("Token", token);
                        }
                        else if (reportRequest.ReportDataSourceType == ReportDataSourceType.SqlServer)
                        {
                            reportRequest.Parameters.Add("databasename", this.reportConnection.SqlDatabaseName);
                            reportRequest.Parameters.Add("password", this.reportConnection.SqlPassword);
                            reportRequest.Parameters.Add("servername", this.reportConnection.SqlServer);
                            reportRequest.Parameters.Add("uid", this.reportConnection.SqlUserName);
                        }

                        ReportExecutionClient.ParameterValue[] reportParameters = null;
                        List<string> missingParameterValues = null;

                        missingParameterValues = taskLoadReport.executionInfo.Parameters.Where(x => !reportRequest.Parameters.ContainsKey(x.Name)).Select(x => x.Name).ToList();

                        if (missingParameterValues.Any())
                        {
                            // Return missing parameters exception.
                            // actionResult.ResultStatus = 
                            actionResult.ResultStatus = string.Empty;
                            foreach (var missingParameter in missingParameterValues)
                            {
                                actionResult.ResultStatus += ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " " + missingParameter + "\n";
                            }

                            return actionResult;
                        }

                        reportParameters = taskLoadReport.executionInfo.Parameters.Where(x => reportRequest.Parameters.ContainsKey(x.Name)).Select(x => new ReportExecutionClient.ParameterValue() { Name = x.Name, Value = reportRequest.Parameters[x.Name]?.ToString() }).ToArray();

                        if (reportParameters.Any())
                        {
                            await this.reportExecutionService.SetExecutionParametersAsync(
                                taskLoadReport.ExecutionHeader,
                                trustedUser,
                                reportParameters,
                                "en-us");
                        }

                        string deviceInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                        var response = await this.reportExecutionService.RenderAsync(new RenderRequest(taskLoadReport.ExecutionHeader, trustedUser, this.GetReportOutput(reportRequest.ReportOutput), deviceInfo));
                        actionResult.Data = response.Result;

                        return actionResult;
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    actionResult.ResultStatus = e.Message;
                    if (reportServerSettings.EnableDebugMode)
                    {
                        throw e;
                    }
                }
            }

            return actionResult;
        }


        /// <summary>
        /// The get id by name.
        /// </summary>
        /// <param name="reportName">
        /// The report Name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Guid> GetIdByName(string reportName)
        {
            return await Task.Run(
                       () =>
                           {
                               var state = this.ReportsRepository
                                   .Get(s => s.ReportName.ToLower() == reportName.ToLower()).FirstOrDefault();

                               var result = state != null ? state.ReportId : Guid.Empty;

                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:SyReportByName:{reportName}",
                                   () => result,
                                   CacheExpiration.VERY_LONG);
                           });
        }



        /// <summary>
        /// The get report output.
        /// </summary>
        /// <param name="reportOutput">
        /// The report output.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetReportOutput(ReportOutput reportOutput)
        {
            if (reportOutput == ReportOutput.Pdf)
            {
                return "PDF";
            }

            return string.Empty;
        }
    }
}
