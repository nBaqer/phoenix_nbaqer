﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiServiceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IWapiSettingsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Maintenance.Wapi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceProcess;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi;

    /// <summary>
    /// The IWapiServiceService interface.
    /// </summary>
    public interface IWapiServiceService
    {
        /// <summary>
        /// The get wapi settings.
        /// </summary>
        /// <param name="codeOperation">
        /// The code operation.
        /// </param>
        /// <returns>
        /// The <see cref="WapiSettings"/>.
        /// </returns>
        WapiSettings GetWapiSettings(string codeOperation);

        /// <summary>
        /// Attempts to turn serice on/off.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        bool TurnWapiServiceOff();

        /// <summary>
        /// Attempts to turn serice on.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        bool TurnWapiServiceOn();

        /// <summary>
        /// Returns status of Wapi service.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        ServiceControllerStatus GetWapiServiceStatus();
    }
}
