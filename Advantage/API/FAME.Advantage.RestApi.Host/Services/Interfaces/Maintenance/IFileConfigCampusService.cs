﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigFeatureService.cs" company="Fame Inc">
//   2019
// </copyright>
// <summary>
//   Defines the FileConfigFeatureService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Maintenance
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The file configuration campus service interface.
    /// </summary>
    public interface IFileConfigCampusService
    {
        /// <summary>
        /// Get by campus group id
        /// </summary>
        /// <param name="CampusGroupId"></param>
        /// <returns></returns>
        Task<CampusFileConfig> Get(Guid CampusGroupId);

        /// <summary>
        /// Save the campus file configuration.
        /// </summary>
        /// <returns>
        /// The <see>
        /// <cref>CampusFileConfig</cref>
        /// </see>
        /// </returns>
        Task<ActionResult<CampusFileConfig>> Save(CampusFileConfig campusFileConfig);

        /// <summary>
        /// Delete the campus file configuration
        /// </summary>
        Task<bool> Delete(Guid CampusConfigurationId);
    }
}
