﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileConfigFeatureService.cs" company="Fame Inc">
//   2019
// </copyright>
// <summary>
//   Defines the FileConfigFeatureService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Maintenance
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The file configuration feature service interface.
    /// </summary>
    public interface IFileConfigFeatureService
    {

        /// <summary>
        /// The get all active file feature configurations.
        /// </summary>
        /// <returns>
        /// The <see>
        /// <cref>IList</cref>
        /// </see>
        /// </returns>
        Task<IList<IListItem<string, string>>> Get();


    }
}
