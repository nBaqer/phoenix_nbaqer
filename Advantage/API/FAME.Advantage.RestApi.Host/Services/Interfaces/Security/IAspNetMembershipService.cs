﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAspNetMembershipService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAspNetMembershipService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The AspNetApplicationService interface.
    /// </summary>
    public interface IAspNetMembershipService
    {
        /// <summary>
        /// The get application id.
        /// </summary>
        /// <param name="membership">
        /// The membership.
        /// </param>
        /// <returns>
        /// The <see cref="AspNetMembership"/>.
        /// </returns>
        Task<AspNetMembership> Create(AspNetMembership membership);
    }
}
