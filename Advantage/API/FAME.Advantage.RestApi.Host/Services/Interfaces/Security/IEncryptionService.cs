﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEncryptionService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the IEncryptionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System.Threading.Tasks;

    using FAME.Orm.AdvTenant.Domain.Entities;

    /// <summary>
    /// The Encryption service interface.
    /// </summary>
    public interface IEncryptionService
    {
        /// <summary>
        /// Decrypt the specified cipher.
        /// </summary>
        /// <param name="cipherString"></param>
        /// <returns></returns>
        string Decrypt(string cipherString);

        /// <summary>
        /// Encrypt the specified cipher
        /// </summary>
        /// <param name="cipherString"></param>
        /// <returns></returns>
        string Encrypt(string cipherString);
    }
}