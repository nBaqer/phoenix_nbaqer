﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAspNetApplicationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAspNetApplicationService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System;

    /// <summary>
    /// The AspNetApplicationService interface.
    /// </summary>
    public interface IAspNetApplicationService
    {
        /// <summary>
        /// The get application id.
        /// </summary>
        /// <param name="applicationName">
        /// The application name.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Guid GetApplicationId(string applicationName);
    }
}
