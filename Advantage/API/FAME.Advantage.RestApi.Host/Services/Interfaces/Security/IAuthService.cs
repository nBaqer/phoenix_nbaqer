﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IAuthService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System.Threading.Tasks;

    using FAME.Orm.AdvTenant.Domain.Entities;

    /// <summary>
    /// The AuthService interface.
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// The get token.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<TokenResponse> GetToken(string username, string password, string tenant);
        /// <summary>
        /// the get token for impersonationated User
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        Task<TokenResponse> GetToken(Guid userId, string tenant);

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        ApiUser GetUser(string username, string tenant);
        /// <summary>
        /// Get User given userId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        ApiUser GetUser(System.Guid userId, string tenant);
        /// <summary>
        /// The encode password.
        /// </summary>
        /// <param name="pass">
        /// The pass.
        /// </param>
        /// <param name="salt">
        /// The salt.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string EncodePassword(string pass, string salt);

        /// <summary>
        /// The get api user cache key.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetApiUserCacheKey(string userName, string tenant);
        /// <summary>
        /// the get api User Cache Key
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        string GetApiUserCacheKey(System.Guid userId, string tenant);
    }
}