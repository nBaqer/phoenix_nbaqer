﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITenantUserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITenantUserService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The ITenantUserService interface.
    /// </summary>
    public interface ITenantUserService
    {
        /// <summary>
        /// The get application id.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="TenantUser"/>.
        /// </returns>
        Task<TenantUser> Create(TenantUser user);

        /// <summary>
        /// The get tenant user.
        /// </summary>
        /// <param name="tenantId">
        /// The tenant id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="TenantUser"/>.
        /// </returns>
        TenantUser GetTenantUser(int tenantId, Guid userId);
    }
}
