﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAspNetUserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAspNetUserService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The IAspNetUserService interface.
    /// </summary>
    public interface IAspNetUserService
    {
        /// <summary>
        /// The get application id.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="AspNetUser"/>.
        /// </returns>
        Task<AspNetUser> Create(AspNetUser user);
    }
}
