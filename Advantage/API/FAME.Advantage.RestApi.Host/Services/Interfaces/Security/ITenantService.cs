﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITenantService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITenantService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    /// <summary>
    /// The ITenantService interface.
    /// </summary>
    public interface ITenantService
    {
        /// <summary>
        /// The get tenant id.
        /// </summary>
        /// <param name="tenantName">
        /// The application Name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        int GetTenantId(string tenantName);
    }
}
