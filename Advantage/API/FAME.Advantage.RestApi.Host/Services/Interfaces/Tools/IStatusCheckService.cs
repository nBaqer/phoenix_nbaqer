﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStatusCheckService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStatusCheckService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The StatusCheckService interface.
    /// </summary>
    public interface IStatusCheckService
    {
        /// <summary>
        /// The get status.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<ListItem<string, bool>>> GetStatus();
    }
}
