﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRazorViewRenderService.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The ViewRenderService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Tools
{
    using System.Threading.Tasks;

    /// <summary>
    /// The RazorViewRenderService interface.
    /// </summary>
    public interface IRazorViewRenderService
    {
        /// <summary>
        /// The render to string async.
        /// </summary>
        /// <param name="viewName">
        /// The view name.
        /// </param>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        Task<string> RenderToStringAsync(string viewName, object model);
    }
}
