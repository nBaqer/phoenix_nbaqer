﻿

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Tools
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    /// <summary>
    /// Logger service interface
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        /// Log a message to file
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="fileName"></param>
        /// <param name="path"></param>
        void LogMessageToFile(string msg, string fileName, string path = "");
    }
}
