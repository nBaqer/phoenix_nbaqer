﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IServiceBuilder.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IServiceBuilder interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The Service Builder interface.
    /// Implement this interface in a class that will handle getting the dependency instance of a service where Circular dependency exist. 
    /// Example: The Lead Service needs a dependency to the phone service and vice versa.
    /// </summary>
    public interface IServiceBuilder
    {
        /// <summary>
        /// The get service instance.
        /// Retrieve a service instance dependency. Use in cases where Circular dependency exist.
        /// Example: The Lead Service needs a dependency to the phone service and vice versa.
        /// </summary>
        /// <typeparam name="TServiceInstance">
        /// The type of service you want to get the instance off.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TServiceInstance"/>.
        /// </returns>
        TServiceInstance GetServiceInstance<TServiceInstance>();
    }
}
