﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentAwardsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStudentAwardsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    
    /// <summary>
    /// The StudentAwardsService interface.
    /// </summary>
    public interface IStudentAwardsService
    {
        /// <summary>
        /// The get direct loan student awards action returns the list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </summary>
        /// <remarks>
        /// The get direct loan student awards method returns the list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </remarks>
        /// <param name="enrollmentId">
        /// EnrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="termStartDate">
        /// The term Start Date is the start of the term.
        /// </param>
        /// <param name="termEndDate">
        /// The term End Date is the end date of the term.
        /// </param>
        /// <returns>
        /// Returns list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </returns>
        Task<IEnumerable<StudentAwards>> GetDirectLoanStudentAwards(Guid enrollmentId, DateTime termStartDate, DateTime termEndDate);
        /// <summary>
        /// The sync all student awards.
        /// </summary>
        /// <param name="studentAwards">
        /// The collection of student awards to sync.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>> SyncAllStudentAwards(IEnumerable<DataTransferObjects.StudentAccounts.AFA.StudentAwards> studentAwards);
        /// <summary>
        /// The sync all student award disbursements.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> SyncAllDisbursements(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements);
        /// <summary>
        /// The create or update student awards.
        /// </summary>
        /// <param name="studentAwards">
        /// The collection of student awards to post.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>> CreateUpdateStudentAwards(IEnumerable<DataTransferObjects.StudentAccounts.AFA.StudentAwards> studentAwards);

        /// <summary>
        /// The create or update student disbursements.
        /// </summary>
        /// <param name="disbursements">
        /// The collection of student disbursements to post.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> CreateUpdateStudentDisbursements(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements);

        /// <summary>
        /// The get title IV refunds for AFA.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<AdvStagingRefundV1>> GetTitleIVRefundsForAFA(Guid campusId);

        Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> UpdateDisbursementTransactionReference(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements);

    }
}
