﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAcademicYearService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAcademicYearService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;

    /// <summary>
    /// The AcademicYearService interface.
    /// </summary>
    public interface IAcademicYearService
    {
        /// <summary>
        /// The get academic year by code received from AFA.
        /// </summary>
        /// <remarks>
        /// The get academic year by AFA code returns the acdemic year id for campus group all and active if found
        /// or creates a new record for the code sent using campus group all and active status.
        /// </remarks>
        /// <param name="code">
        /// The academic year code.
        /// </param>
        /// <param name="campusId"></param>
        /// <returns>
        /// Returns the academic year id.
        /// </returns>
        Task<Guid> GetByAFACode(string code, Guid campusId);

        Task<IEnumerable<Guid>> GetYearsByAFACode(string code);

    }
}
