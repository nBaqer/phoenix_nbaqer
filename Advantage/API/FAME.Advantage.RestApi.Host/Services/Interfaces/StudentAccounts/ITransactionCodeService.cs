﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransactionCodeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITransactionCodeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.Common;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;

    /// <summary>
    /// The transaction code interface.
    /// </summary>
    public interface ITransactionCodeService
    {
        /// <summary>
        /// This is to get the drop down list data source for the charging method screen
        /// </summary>
        /// <param name="campusGrpId"></param>
        /// <returns></returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetTransactionCodeForChargingMethod(Guid campusId);

    }
}
