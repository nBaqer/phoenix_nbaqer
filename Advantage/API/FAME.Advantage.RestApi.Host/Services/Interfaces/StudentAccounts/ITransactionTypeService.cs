﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransactionTypeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITransactionTypeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.Common;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;

    /// <summary>
    /// The transaction type interface.
    /// </summary>
    public interface ITransactionTypeService
    {
        /// <summary>
        /// Gets the transaction type id by description.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        Task<int> GetIdByDescription(string description);

    }
}
