﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFundSourceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IFundSourceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.Common;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;

    /// <summary>
    /// The FundSourceService interface.
    /// </summary>
    public interface IFundSourceService
    {
        Task<Guid> GetByAFAAwardType(string awardType);

        /// <summary>
        /// The get by AFA award type.
        /// </summary>
        /// <remarks>
        /// The get academic year by code.
        /// </remarks>
        /// <param name="awardType">
        /// The award type.
        /// </param>
        /// <param name="campusId"></param>
        /// <returns>
        /// Returns the academic year.
        /// </returns>
        Task<Guid> GetByAFAAwardType(string awardType, Guid campusId);

        /// <summary>
        /// Maps the Advantage System fund sources to default award types
        /// </summary>
        /// <param name="fundSource"></param>
        /// <returns></returns>
        Constants.AwardTypes MapSystemFundSourceToAwardType(Constants.SystemFundSources fundSource);

        /// <summary>
        /// Maps the award types received from AFA to the Advantage system fund sources.
        /// </summary>
        /// <param name="awardType"></param>
        /// <returns></returns>
        Constants.SystemFundSources MapAfaAwardTypeToSystemFundSources(string awardType);

        string CleanAFAAwardType(string awardType);

        /// <summary>
        /// Checks if the fund source is a title iv fund source
        /// </summary>
        /// <param name="fundSourceId"></param>
        /// <returns></returns>
        bool IsTitleIV(Guid fundSourceId);

    }
}
