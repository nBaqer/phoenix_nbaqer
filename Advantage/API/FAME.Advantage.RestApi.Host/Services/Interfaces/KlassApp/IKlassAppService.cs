﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IKlassAppStudentSummaryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.KlassApp
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.KlassApp;
    using System.Threading.Tasks;

    /// <summary>
    /// The KlassAppStudentSummaryService interface.
    /// </summary>
    public interface IKlassAppService
    {
        /// <summary>
        /// The get students to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IListActionResult<KlassAppSync>> GetStudentsToSync();
    }
}
