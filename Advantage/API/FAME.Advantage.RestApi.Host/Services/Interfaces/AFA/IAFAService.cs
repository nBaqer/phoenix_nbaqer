// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAfaService.cs" company="FAME Inc.">
//   2018
// </copyright>
// <summary>
//   Defines the IAfaService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AFA
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The AFAService interface.
    /// </summary>
    public interface IAfaService
    {
        /// <summary>
        /// The get AFA session key.
        /// </summary>
        /// <param name="afaSession">
        /// The AFA session.
        /// This is required for communicating with AFA API.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        AfaSession GetAfaSessionKey(AfaSession afaSession);

        /// <summary>
        /// The get all lead demographics.
        /// </summary>
        /// <param name="sessionKey">
        /// The session key.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        Task<IList<AdvStagingDemographicsV1>> GetAllLeadDemographicsAsync(string sessionKey, Guid campusId);

        Task<string> PostAllProgramVersionToAfa(IEnumerable<AdvStagingProgramV1> programVersions);
        Task<IList<AdvStagingPaymentPeriodV1>> GetAllPaymentPeriodsFromAfa(Guid campusId);
        Task<bool> PostPaymentPeriodsToAdvantage(IEnumerable<AdvStagingPaymentPeriodV1> paymentPeriods);
        Task<IList<AdvStagingDisbursementV1>> GetAllTitleIVDisbursementsFromAfa(Guid campusId);
        Task<bool> PostTitleIVDisbursementsToAdvantage(IEnumerable<AdvStagingDisbursementV1> disbursements);
        Task<IList<AdvStagingAwardV1>> GetAllTitleIVAwardsFromAfa(Guid campusId);
        Task<bool> PostTitleIVAwardsToAdvantage(IEnumerable<AdvStagingAwardV1> awards);
        Task<bool> SyncAwardsToAdvantage(IEnumerable<AdvStagingAwardV1> awards);
        Task<bool> SyncDisbursementsToAdvantage(IEnumerable<AdvStagingDisbursementV1> disbursements);
        Task<AFAEnrollmentDetails> GetAfaEnrollmentDetails(Guid enrollmentId);
        Task<IEnumerable<AFAEnrollmentDetails>> GetAfaEnrollmentDetails(IEnumerable<Guid> enrollmentIds);

        Task UpdateEnrollmentsStatus(string cmsId);
        Task<IEnumerable<AdvStagingAttendanceV1>> CalculateAttendanceUpdatesForEnrollments(IEnumerable<Guid> enrollmentIds);
        Task<AdvStagingAttendanceV1> CalculateAttendanceUpdatesForEnrollment(Guid enrollmentId);
        Task<IEnumerable<AdvStagingAttendanceV1>> CalculateAllAttendanceUpdatesForCampus(Guid campusId);
        Task<string> PostAttendanceUpdatesToAfa(IEnumerable<AdvStagingAttendanceV1> attendance);
        IEnumerable<Guid> GetAfaStudentEnrollmentsForCampus(Guid campusId);
        Task<string> PostAllRefundsToAfa(Guid campusId);

        Task<string> PostAllEnrollmentsToAfa(IEnumerable<AdvStagingEnrollmentV2> enrollments);

        Task<bool> UpdateDisbursementTransactionReference(Guid campusId);

        /// <summary>
        /// Attempts to turn wapi service on if AFA Integration is enabled for any campus.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        Task<bool> TurnWapiServiceOnIfIntegrationIsEnabled();
    }
}
