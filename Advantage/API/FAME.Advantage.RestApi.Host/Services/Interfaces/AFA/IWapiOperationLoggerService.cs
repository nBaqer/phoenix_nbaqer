﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWapiOperationLoggerService.cs" company="FameInc">
//   2019
// </copyright>
// <summary>
//   The inteface of WapiOperationLoggerService service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
/// <summary>
/// The WapiOperationLoggerService Interface
/// </summary>
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AFA
{
    /// <summary>
    /// The WapiOperationLoggerService interface
    /// </summary>
    public interface IWapiOperationLoggerService
    {
        /// <summary>
        /// InsertSyWapiOperationLogger
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        Task<bool> InsertSyWapiOperationLogger(DataTransferObjects.AFA.WapiOperationLogger record);
    }
}
