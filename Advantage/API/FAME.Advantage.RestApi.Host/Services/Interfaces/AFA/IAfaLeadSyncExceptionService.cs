﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAfaLeadSyncExceptionService.cs" company="FameInc">
//   2018
// </copyright>
// <summary>
//   The i afa lead sync exception service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AFA
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The AFA LeadSyncExceptionService interface.
    /// </summary>
    public interface IAfaLeadSyncExceptionService
    {
        /// <summary>
        /// The insert AFA lead sync exception.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <returns>
        /// The list of the entries in the table so that the Grid can be bound.
        /// </returns>
        Task<IList<AfaLeadSyncException>> InsertAfaLeadSyncException(DataTransferObjects.AFA.AFALeadSyncException record);

        Task<IList<AfaLeadSyncException>> GetAfaLeadSyncException();
    }
}
