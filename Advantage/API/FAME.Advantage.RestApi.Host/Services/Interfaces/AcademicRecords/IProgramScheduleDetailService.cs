﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgramScheduleDetailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IProgramScheduleDetailService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;

    /// <summary>
    /// The Program Schedule Detail Service interface.
    /// </summary>
    public interface IProgramScheduleDetailService
    {
        /// <summary>
        /// The GetDetailByScheduleId returns the matching program schedule details based on the given scheduleId.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<ProgramScheduleDetails>> GetDetailByScheduleId(List<Guid> scheduleId);

        /// <summary>
        /// The get credit hour scheduled break returns the scheduled breaks within a specific start and end date.
        /// </summary>
        /// <param name="enrollmentId">
        /// The schedule id.
        /// </param>
        /// <param name="endDate">
        /// end Date
        /// </param>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<CreditHourProgramSchedule> GetCreditHourScheduledBreak(Guid enrollmentId, DateTime endDate, DateTime startDate);

        /// <summary>
        /// The get credit Hour completed days by enrollment id action returns completed days in the period.
        /// </summary>
        /// <remarks>
        /// The get credit Hour completed days by EnrollmentId action returns completed days in the period.
        /// It requires enrollmentId. 
        ///  Based on the EnrollmentId and date range, it searches all the enrolled program details and returns completed days in the period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// Returns completed days in the period.
        /// </returns>
        Task<int> GetCreditHourCompletedDaysByEnrollmentId(Guid enrollmentId, DateTime? endDate);

        /// <summary>
        /// The Get EndDate Of CreditHour PaymentPeriod action returns the End date of the payment period when a student withdraws from a non term not self paced program or when DL period is used for calculation for Nonstandard term programs with terms not substantially equal in length..
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour PaymentPeriod by EnrollmentId action returns End date of the payment period when a student withdraws from a non term not self paced program or when DL period is used for calculation for Nonstandard term programs with terms not substantially equal in length..
        /// it requires enrollmentId .
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns End date of the payment period when a student withdraws.
        /// </returns>
        Task<CreditHourWithdrawalEndDateDetails> GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(Guid enrollmentId); 

        /// <summary>
        /// The Get EndDate Of CreditHour Period Of Enrollment action returns the End date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard term program.
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour Period Of Enrollment by EnrollmentId action returns End date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard term program.
        /// it requires enrollmentId .
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns End date of the period of enrollment when a student withdraws.
        /// </returns>
        Task<CreditHourWithdrawalEndDateDetails> GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The GetLoAsForDateRange returns the number of days in LOAs between date range.
        /// </summary>
        /// <param name="studentLoAs">
        /// The student LOAs.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <returns>
        /// The number of days for approved LOAs between given days.
        /// </returns>
        double GetLoAsForDateRange(IEnumerable<LOA> studentLoAs, DateTime endDate, DateTime startDate);
    }
}
