﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITitleIVSAPService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The TitleIVSAPServicecs interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Fame.EFCore.Advantage.Entities;
using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
using System.Linq;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The TitleIVSAPServicecs interface.
    /// </summary>
    public interface ITitleIVSAPService
    {
        /// <summary>
        /// The get title iv results for student.
        /// </summary>
        /// <param name="fasap">
        /// The student id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable<TitleIVSAPResult>"/>.
        /// </returns>
        Task<IEnumerable<TitleIVSAPResult>> GetTitleIVResultsForStudent(FASAPStatus fasap);

        /// <summary>
        /// The get Title IV results for multiple student.
        /// </summary>
        /// <param name="input">
        /// The input for retrieving Title IV results for multiple students.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable<Guid>"/>.
        /// </returns>
        Task<IEnumerable<TitleIVSAPResult>> GetTitleIVResultsForMultipleStudents(TitleIVSAPInput input);

        /// <summary>
        /// The get last run increment.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        int GetLastRunIncrement(Guid stuEnrollId);

        /// <summary>
        /// The get previously run result.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        /// <param name="increment">
        /// The increment to check.
        /// </param>
        string GetPreviouslyRunResult(Guid stuEnrollId, int increment);

        /// <summary>
        /// The get title IV results.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        IQueryable<ArFasapchkResults> GetTitleIVResults(Guid stuEnrollId);

        /// <summary>
        /// The get title IV Status id based on result.
        /// </summary>
        /// <param name="result">
        /// The SAP result.
        /// </param>
        int? GetTitleIVSapStatusId(SAPResult result);


        /// <summary>
        /// The update title iv results for student.
        /// </summary>
        /// <param name="FASAPCheckResultsId ">
        /// The TitleIVSAP Id.
        /// </param>

        Task<Boolean> PlaceStudentOnTitleIVPrbation(string FASAPCheckResultsId);

        /// <summary>
        /// The get title iv sap notices for printing.
        /// </summary>
        /// <param name="campusId ">
        /// The campus Id.
        /// </param>
        /// <param name="asOfDate"></param>
        /// Date to filter notices by
        Task<IEnumerable<TitleIVSAPResult>> GetTitleIVNoticesForPrinting(Guid campusId, DateTime? asOfDate = null);

        /// <summary>
        /// The update print flag.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task UpdatePrintFlag(List<Guid> enrollments);
    }
}
