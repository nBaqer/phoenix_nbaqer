﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IR2T4ResultService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IR2T4ResultService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;
    using FAME.Core.R2T4Calculator.Interfaces;

    /// <summary>
    /// The R2T4 ResultService interface.
    /// </summary>
    public interface IR2T4ResultService
    {
        /// <summary>
        ///  The GetR2T4Results calculates and retuns the R2T4 Results for the given R2T4Input data
        /// </summary>
        /// <remarks>
        /// GetR2T4Results requires the R2t4 input model object which contains the mandatory properties.
        /// </remarks> 
        /// <param name="model">
        /// R2T4Input model
        /// </param>
        /// <returns>
        /// Returns an GetR2T4Results.
        /// </returns>
        Task<IR2T4Input> GetR2T4Results(DataTransferObjects.AcademicRecords.R2T4Input.R2T4Input model);

        /// <summary>
        /// The get R2T4 input service validations.
        /// </summary>
        /// <returns>
        /// The DynamicValidationRuleSet.
        /// </returns>
        DynamicValidationRuleSet GetR2T4ResultValidationRuleSet();

        /// <summary>
        /// The Create action allows to save the R2T4 Results details.
        /// </summary>
        /// <remarks>
        /// The Create action requires R2T4Results object. This object contains the properties of all the R2T4 Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The Create action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Results with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Result> Create(R2T4Result model);

        /// <summary>
        /// The SaveOverrideR2T4Result action allows to save the R2T4 Override Results details.
        /// </summary>
        /// <remarks>
        /// The SaveOverrideR2T4Result action requires R2T4Results object. This object contains the properties of all the R2T4 Override Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The SaveOverrideR2T4Result action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Result> SaveOverrideR2T4Result(R2T4Result model);

        /// <summary>
        /// The Update action allows to update the existing R2T4 Result details.
        /// </summary>
        /// <remarks>
        /// The Update action requires R2T4Result object. This object contains the properties of all the R2T4 Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The Update action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Result> Update(R2T4Result model);

        /// <summary>
        /// The UpdateOverrideResults action allows to update the existing R2T4 Override Result details.
        /// </summary>
        /// <remarks>
        /// The UpdateOverrideResults action requires R2T4Result object. This object contains the properties of all the R2T4 Override Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The UpdateOverrideResults action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Result> UpdateOverrideResult(R2T4Result model);

        /// <summary>
        /// The get action allows to fetch the R2T4 Result detail.
        /// </summary>
        /// <remarks>
        /// The get action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        Task<R2T4Result> Get(Guid terminationId);

        /// <summary>
        /// The GetR2T4ResultId action allows to check the terminationid exists in R2T4 results or not.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultId Results action requires R2T4ResultOverride and TerminationId and based on the Ids provided, it returns the matching R2T4 Result Override record.
        /// </remarks>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// Returns the R2T4 results id based on the matching value. If matching record found then the return empty string.
        /// </returns>
        Task<string> GetR2T4ResultId(R2T4ResultOverride model);

        /// <summary>
        /// The IsResultsExists action allows to check if the R2T4 results are existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultsExists action requires terminationId and based on the Id provided, it returns boolean if the R2T4 result record exists or not.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        Task<bool> IsResultsExists(Guid terminationId);

        /// <summary>
        /// The GetR2T4OverrideResults action allows to fetch the R2T4 Override Result detail.
        /// </summary>
        /// <remarks>
        /// The GetR2T4OverrideResults action requires terminationId and based on the Id provided, it returns the matching R2T4 Override Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Override Result.
        /// </returns>
        Task<R2T4Result> GetR2T4OverrideResults(Guid terminationId);

        /// <summary>
        /// The GetR2T4ResultsByTerminationId action allows to fetch the R2T4 Result detail.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsByTerminationId action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        Task<R2T4Result> GetR2T4ResultsByTerminationId(Guid terminationId);

        /// <summary>
        /// The delete action is to delete the R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the R2T4Results, R2T4OverrideResults by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        Task<string> Delete(Guid terminationId);

        /// <summary>
        /// The IsResultOverridden action allows to check if the R2T4 results is overridden or not for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultOverridden action requires terminationId and based on the Id provided, it returns true if the R2T4 result record is overridden else false.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        Task<bool> IsResultOverridden(Guid terminationId);

        /// <summary>
        /// The GetR2T4ResultsForReport action method returns R2T4 result details for the given termination id.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsForReport action method requires termination Id of type Guid.
        /// </remarks>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object in XML format.
        /// </returns>
        Task<R2T4ResultReport> GetR2T4ResultsForReport(string token, Guid terminationId);

        /// <summary>
        /// The UpdateR2T4ResultStatus action allows to update the existing R2T4 Result completion status.
        /// </summary>
        /// <remarks>
        /// The UpdateR2T4ResultStatus action requires termination Id and the status.
        /// Mandatory parameters required : TerminationId, status
        /// </remarks>
        /// <param name="terminationId">
        /// The UpdateOverrideResults action method requires termination Id of type guid.
        /// </param>
        /// <param name="status">
        /// The UpdateOverrideResults action method requires status type bool.
        /// </param>
        /// <returns>
        /// Returns update R2T4Result status boolean as true or false.
        /// </returns>
        Task<bool> UpdateR2T4ResultStatus(Guid terminationId, bool status);
    }
}