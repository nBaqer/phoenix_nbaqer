﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IApproveTerminationService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the IApproveTerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    /// <summary>
    /// The ApproveTerminationService interface.
    /// </summary>
    public interface IApproveTerminationService
    { 
        /// <summary>
        /// The GetDetails service returns student termination details for the given termination id.
        /// The details also contain the R2T4 calculation along with overridden values
        /// </summary>
        /// <remarks>
        /// The GetDetails action method requires termination Id object of type Guid.
        /// </remarks> 
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="ApproveTermination" /> where ApproveTermination has following information :
        /// EnrollmentName,Status,Dropreason,LastDateAttended,WithdrawalDate,DateOfDetermination,
        /// TotalCharges, TotalTitleIvAid, TotalTitleIvAidDisbursed, PercentageOfTitleIvAidEarned, PostWithdrawalDisbursement, TotalTitleIvAidEarned, AmountToBeReturnedBySchool,
        /// AmountToBeReturnedByStudent, R2T4InputUserName, OverriddenUserName, TicketNumber, TitleIvGrantLessThan50Dollar, R2T4ResultFieldsWithOverriddenValues
        /// </returns>
        Task<ApproveTermination> GetDetails(Guid terminationId);

        /// <summary>
        /// The Update action allows to update the Enrollment status details when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Update action requires Enrollment object which has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </remarks>
        /// <param name="studentEnrollmentDetails">
        /// The studentEnrollmentDetails object of type Enrollment.
        /// </param>
        /// <returns>
        /// Returns an object of Enrollment where Enrollment has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </returns>
        Task<Enrollment> Update(Enrollment studentEnrollmentDetails);

        /// <summary>
        /// The Create action allows to save the student status change details.
        /// </summary>
        /// <remarks>
        /// The Create action requires StudentStatusChanges object which has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </remarks>
        /// <param name="model">
        /// The Post action method accepts a parameter of type StudentStatusChanges.
        /// </param>
        /// <returns>
        /// Returns an object of StudentStatusChanges where StudentStatusChanges has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </returns>
        Task<StudentStatusChanges> Create(StudentStatusChanges model);

        /// <summary>
        /// The generate student summary report.
        /// </summary>
        /// <param name="reportRequest">
        /// The report request.
        /// </param>
        /// <returns>
        /// Returns an object of reportRequest .
        /// </returns> 
        Task<ReportActionResult> GenerateStudentSummaryReport(ReportRequest reportRequest);
    }
}