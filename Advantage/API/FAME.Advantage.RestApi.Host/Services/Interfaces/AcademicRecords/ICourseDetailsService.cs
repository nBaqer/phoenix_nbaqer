﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICourseDetailsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ICourseDetailsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;

    /// <summary>
    /// The CourseDetailsService interface.
    /// </summary>
    public interface ICourseDetailsService
    {
        /// <summary>
        /// The Get Transfer Credits for ReqId method returns ReqId,CourseId,Code,Description,StatusId,ReqTypeId,Credits,FinAidCredits,Hours,Cnt,GrdLvlId,
        /// UnitTypeId,CampGrpId,CourseCatalog,CourseComments,ModDate,ModUser,Su,Pf,CourseCategoryId,TrackTardies,TardiesMakingAbsence,DeptId,MinDate
        /// IsComCourse,IsOnLine,CompletedDate,IsExternship,UseTimeClock,IsAttendanceOnly,AllowCompletedCourseRetake based on the parameter reqID.
        /// </summary>
        /// <param name="reqId">
        /// The req id.
        /// </param>
        /// <returns>
        /// The TransferCredits.
        /// </returns>
        Task<CourseDetails> GetTransferCreditsByReqId(Guid reqId);

        /// <summary>
        /// The GetCourseDetailsByReqIds method returns all the course details by given ReqIds.
        /// </summary>
        /// <param name="reqIds">
        /// The req ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<CourseDetails>> GetCourseDetailsByReqIds(IList<Guid> reqIds);
    }
}
