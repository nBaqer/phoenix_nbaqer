﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgramScheduleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ProgramScheduleService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks; 
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The Program Schedule Service interface.
    /// </summary>
    public interface IProgramScheduleService
    {
        /// <summary>
        /// The GetDetailByProgramVersionId returns the matching program schedule based on the given programVersionId .
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<ProgramSchedule>> GetDetailByProgramVersionId(Guid programVersionId);

        /// <summary>
        /// The GetScheduleHoursAndWeekDayForDateRange returns the total scheduled hours between given start and withdrawal date by program version id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The Schedule Hours as decimal
        /// </returns>
        Task<IEnumerable<IListItem<int, decimal>>> GetScheduleHoursAndWeekDayForDateRange(
            Guid enrollmentId);

        /// <summary>
        /// The GetScheduleWeekDayForEnrollmentId returns the total scheduled hours between given start and withdrawal date by program version id.
        /// </summary>
        /// <param name="prgVerId">
        /// The program version id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment Id.
        /// </param>
        /// <returns>
        /// The Schedule Hours as decimal
        /// </returns>
        Task<IEnumerable<IListItem<int, decimal>>> GetScheduleWeekDayForEnrollmentId(Guid prgVerId, Guid enrollmentId);
    }
}
