﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEnrollmentService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IEnrollmentService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The EnrollmentService interface.
    /// </summary>
    public interface IEnrollmentService
    {
        /// <summary>
        ///  The GetEnrollmentsByStudentId Gets the list of enrollment(s) for the given studentId and enrollmenttype
        ///  and each enrollment listed is displayed as StudentEnrollment name Status Effective date of the current status'. Example: 'Cosmetology - Enrolled 05/01/2017'
        /// </summary>
        /// <remarks>
        /// The GetEnrollmentsByStudentId action requires studentId object which is a Guid and enrollmentdetail.
        /// </remarks> 
        /// <param name="enrollmentDetail">
        /// The enrollmen tType is required field and is a type of enrollment status type.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="Enrollment"/> where StudentEnrollment has the EnrollmentId,ProgramVersionDescription,StatusDescription,EffectiveDate,Status,EnrollmentDate
        /// </returns>
        Task<IEnumerable<Enrollment>> GetEnrollmentsByStudentId(EnrollmentStatus enrollmentDetail);

        /// <summary>
        /// The get enrollment detail action returns enrollment detail of student by enrollment.
        /// </summary>
        /// <remarks>
        /// The get enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId . 
        /// Based on the EnrollmentId, StudentId and enrollmenttype, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        Task<Enrollment> GetEnrollmentDetail(Guid enrollmentId, Guid studentId);

        /// <summary>
        /// gets all the enrollment details for enrollments active in last 60 days
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="programVersionId"></param>
        /// <returns></returns>
        Task<IList<LMSEnrollmentDetails>> GetAllActiveEnrollmentForCampusId(Guid campusId, Guid programVersionId);

        /// <summary>
        /// The get student start date.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id
        /// </param>
        DateTime? GetStudentStartDate(Guid enrollmentId);

        /// <summary>
        /// The get student leave of absences.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id
        /// </param>
        IList<LOA> GetStudentLOAs(Guid enrollmentId);

        /// <summary>
        /// The Get action method returns enrollment details by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The get enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId . 
        /// Based on the EnrollmentId, StudentId and enrollmenttype, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended and StatusCode.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        Task<Enrollment> Get(Guid enrollmentId);

        /// <summary>
        /// The Get action method returns enrollment details for SAP by given list of enrollment id.
        /// </summary>
        /// <param name="enrollmentIds">
        /// The enrollment ids is a required field and is a list of type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        Task<IEnumerable<ProgramVersionSAP>> Get(IEnumerable<Guid> enrollmentIds);

        /// <summary>
        /// The Get action method returns enrollment details for SAP by given program version id.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="systemStatuses">
        /// The system statuses.
        /// </param>
        Task<IEnumerable<ProgramVersionSAP>> GetByProgramVersion(Guid programVersionId, List<Constants.SystemStatus> systemStatuses = null);

        /// <summary>
        /// The Update action allows to update the Enrollment status details when the student is undo terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The update enrollment detail action update enrollment detail of student and in the parameter 
        /// it requires enrollment object with CampusId, EnrollmentId and StudentId . 
        /// Based on the EnrollmentId, it update all the enrolled program details and returns us following information 
        /// LDA, DateDetermined,DropReasonId and StatusCodeId.
        /// </remarks> 
        /// <param name="enrollment">
        /// The enrollment entity is required field.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        Task<Enrollment> Update(Enrollment enrollment);

        /// <summary>
        /// The Get Student Loa Details action allows us to get the list of approve leave of absense count for the specified enrollment and date range.
        /// </summary>
        /// <remarks>
        ///  Get Student Loa Details action allows us to get the count of approve leave of absense for the specified enrollment and date range
        /// it requires enrollmentId , start date and end date. 
        /// Based on the EnrollmentId and date range, it searches all the enrolled program details and returns the count of  approve leave of absense. 
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment object of type Enrollment.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param> 
        /// <returns>
        /// Returns list of approve leave of absense count.
        /// </returns>
        Task<IEnumerable<LOA>> GetStudentLoaDetails(Guid enrollmentId, DateTime startDate, DateTime endDate);
        /// <summary>
        /// Get all the enrollment details for afa sync given the leadId
        /// </summary>
        /// <param name="leadId"></param>
        /// <returns></returns>
        Task<IList<AdvStagingEnrollmentV2>> GetAllLeadEnrollmentsForAfaSync(Guid leadId);
        Task<IList<AdvStagingEnrollmentV2>> GetAllEnrollmentsForAfaSync(Guid campusId);

        /// <summary>
        /// Get enrollments that have an afa student id for a campus with a cms id set.
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        IEnumerable<Guid> GetEnrollmentsByCampusForAFA(Guid campusId);

        /// <summary>
        /// The get active enrollment for klass app.
        /// </summary>
        /// <param name="klassAppStudentId">
        /// The klass app student id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Enrollment> GetActiveEnrollmentForKlassApp(string klassAppStudentId);

        /// <summary>
        /// The get enrollment.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Enrollment> GetEnrollment(Guid studentEnrollmentId, bool checkAllStatuses = false);

        /// <summary>
        /// Get enrollment program section for student summary.
        /// </summary>
        /// <param name="enrollmentId"></param>
        /// <returns></returns>
        Task<EnrollmentProgram> GetEnrollmentProgramSummary(Guid enrollmentId);

        /// <summary>
        /// The get lda.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DateTime?> GetLDA(Guid enrollmentId);

        /// <summary>
        /// The badge number.
        /// </summary>
        /// <param name="campusId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<string> GetNewBadgeNumber(Guid campusId);

        /// <summary>
        /// The have attendance or grade posted.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> HaveAttendanceOrGradePosted(Guid studentEnrollmentId);

        /// <summary>
        /// The get lms student enrollments details for a student.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<StudentEnrollmentDetailsLms>> GetLmsStudentEnrollmentDetails(Guid studentEnrollmentId);

        /// <summary>
        /// The get lms student enrollments details for a set of students.
        /// </summary>
        /// <param name="studentEnrollmentIds">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<List<StudentEnrollmentDetailsLms>>> GetMultipleLmsStudentEnrollmentDetails(List<Guid> studentEnrollmentIds);

        /// <summary>
        /// The get afa student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<AFAEnrollmentDetails>> GetAfaStudentEnrollmentDetails(Guid studentEnrollmentId);
        /// <summary>
        /// The get afa student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentIds">
        /// The student enrollment ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<IEnumerable<AFAEnrollmentDetails>>> GetAfaStudentEnrollmentDetails(IEnumerable<Guid> studentEnrollmentIds);

        /// <summary>
        /// The get enrollment status changes for AFA.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<EnrollmentTracking>> GetEnrollmentStatusChangesForAFA(Guid campusId);

        /// <summary>
        /// Mark enrollment change tracking as processed(AFA Integration).
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The campus id.
        /// </param>
        /// <param name="timeStamp">
        /// The time stamp of original processed record.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        Task<bool> MarkEnrollmentChangeAsProcessed(Guid studentEnrollmentId, DateTime timeStamp);
        /// <summary>
        /// Mark enrollment change tracking as processed(AFA Integration).
        /// </summary>
        /// <param name="enrollments">
        /// The list of enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        Task MarkEnrollmentChangeAsProcessed(IEnumerable<AdvStagingEnrollmentV2> enrollments);

        /// <summary>
        /// Get campus id for enrollment.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        Task<Guid> GetCampusIdForEnrollment(Guid studentEnrollmentId);

        /// <summary>
        /// Get last tracking date for integration enrollment tracking
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        DateTime? GetLastTrackingDate(Guid stuEnrollmentId);

        /// <summary>
        /// Method to increment retry counter
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        void IncrementRetryCount(Guid stuEnrollmentId);
    }
}
