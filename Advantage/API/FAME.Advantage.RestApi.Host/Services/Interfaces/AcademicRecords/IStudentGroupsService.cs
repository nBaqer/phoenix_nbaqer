﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentGroupsService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the IStudentGroupsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The student groups service interface.
    /// </summary>
    public interface IStudentGroupsService
    {
        /// <summary>
        /// Get the student groups by campus Id
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStudentGroupsByCampus(Guid campusId);

    }
}