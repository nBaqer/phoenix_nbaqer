﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITransferGradesService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITransferGradesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TransferGrades;

    /// <summary>
    /// The TransferGradesService interface.
    /// </summary>
    public interface ITransferGradesService
    {
        /// <summary>
        /// The get transfer grades returns TransferId,StuEnrollId,ReqId,GrdSysDetailId,Score,TermId,ModDate,ModUser,IsTransferred,CompletedDate,
        /// IsClinicsSatisfied,IsCourseCompleted,IsGradeOverridden,GradeOverriddenBy,GradeOverriddenDate is based on the enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is required field.
        /// </param>
        /// <returns>
        /// Returns the TransferGrades details.
        /// </returns>
        Task<IEnumerable<TransferGradesDetails>> GetTransferGrades(Guid enrollmentId);
    }
}
