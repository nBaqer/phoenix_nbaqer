﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICampusProgramVersionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ICampusProgramVersionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.CampusProgram;

    /// <summary>
    /// The ICampusProgramVersionService interface.
    /// </summary>
    public interface ICampusProgramVersionService
    {
        /// <summary>
        /// The Get campus program version action returns CampusProgramVersionId,CampusId,ProgramVersionId,IsTitleIV,
        /// IsFAMEApproved,IsSelfPaced,CalculationPeriodTypeId,AllowExcusAbsPerPayPeriod,TermSubEqualInLength,ModUser,ModDate by given campus id and programversion id.
        /// </summary>
        /// <remarks>
        /// The get campus program version action returns campus program version details.  
        /// </remarks> 
        /// <param name="campusId">
        /// The campusId id is required field and is a type of Guid.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id is required field and is a type of Guid.
        /// </param>  
        /// <returns>
        /// Returns campus program version details.
        /// </returns>
        Task<CampusProgramVersions> GetCampusProgramVersion(Guid campusId, Guid programVersionId);

        /// <summary>
        /// The Get campus program version action returns campus program version details for given campus id and programversion id.
        /// </summary>
        /// <remarks>
        /// The get campus program version details action returns campus program version details.  
        /// </remarks> 
        /// <param name="campusId">
        /// The campusId id is required field and is a type of Guid.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id is required field and is a type of Guid.
        /// </param>  
        /// <returns>
        /// Returns campus program version details.
        /// </returns>
        Task<CampusProgramVersions> GetCampusProgramVersionDetails(Guid campusId, Guid programVersionId);

        /// <summary>
        /// The get campus program version .
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<CampusProgramVersions>> GetCampusProgramVersion();
    }
}
