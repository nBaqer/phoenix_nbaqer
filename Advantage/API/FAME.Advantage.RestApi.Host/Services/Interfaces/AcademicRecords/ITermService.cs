﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITermService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITermService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Terms;

    /// <summary>
    /// The ITermService interface.
    /// </summary>
    public interface ITermService
    {
        /// <summary>
        /// The get terms method.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Term> GetTerms(Guid enrollmentId);

        /// <summary>
        /// The insert term.
        /// </summary>
        /// <param name="term">
        /// The term.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<Term>> Insert(Term term);

        /// <summary>
        /// The get term.
        /// </summary>
        /// <param name="programVersionId">
        /// The program Version Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<Term>> Get(Guid programVersionId);

        /// <summary>
        /// The update term.
        /// </summary>
        /// <param name="term">
        /// The term.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<Term>> Update(Term term);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="termId">
        /// The term id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> Delete(Guid termId);

        /// <summary>
        /// The get terms method.
        /// </summary>
        /// <param name="programId">
        /// The program Id.
        /// </param>
        /// <returns>
        /// returns list of term details. 
        /// </returns>
        List<Term> GetTermsByProgram(Guid programId);
    }
}
