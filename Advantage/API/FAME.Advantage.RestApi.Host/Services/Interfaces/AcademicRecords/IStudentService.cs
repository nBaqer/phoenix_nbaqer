﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The StudentService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Orm.Advantage.Domain.Common;

    /// <summary>
    /// The StudentService interface.
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        /// The Search action methods returns the list of student(s) for the provided campus and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given campus of type GUID and Search filter of type string
        /// </remarks>
        /// <param name="studentSearchFilter">
        /// The student search filter object includes the campusId and a Filter string which should of minimum 3 letters of the student First name or Last Name or SSN.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        Task<IEnumerable<IListItem<string, StudentSearchOutput>>> Search(StudentSearch studentSearchFilter);

        /// <summary>
        /// The Search action methods returns the list of student(s) for the provided campus and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given campus of type GUID and Search filter of type string
        /// </remarks>
        /// <param name="studentSearchFilter">
        /// The student search filter object includes the campusId and a Filter string which should of minimum 3 letters of the student First name or Last Name or SSN.
        /// </param>
        /// <param name="statuses">
        /// The list of valid statuses for searching a student.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of"IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        Task<IEnumerable<IListItem<string, StudentSearchOutput>>> Search(StudentSearch studentSearchFilter, IEnumerable<Constants.SystemStatus> statuses);

        /// <summary>
        /// The get hold status method.
        /// </summary>
        /// <param name="studentId">
        /// The student id.
        /// </param>
        Task<bool> GetHoldStatus(System.Guid? studentId);

        /// <summary>
        ///  The GetStudentScheduleDetails Gets the schedule details for the given enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetStudentScheduleDetails action requires enrollment id object which is a Guid.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollmentId.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="StudentScheduleInfo"/>.
        /// </returns>
        Task<IEnumerable<StudentScheduleInfo>> GetStudentScheduleDetails(Guid enrollmentId);

        /// <summary>
        /// The get student contact info.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<StudentContactInfo> GetStudentContactInfo(Guid enrollmentId);

        //
        /// <summary>
        /// Get Student Class Sections
        /// </summary>
        /// <param name="stuEnrollId">
        /// The stuEnrollId.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<List<StudentClassSectionsAndTerm>>> GetStudentClassSections(Guid stuEnrollId);

        //
        /// <summary>
        /// Get Student Student Registered Courses
        /// </summary>
        /// <param name="stuEnrollId">
        /// The stuEnrollId.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<List<StudentRegisteredCourses>>> GetStudentRegisteredCourses(Guid stuEnrollId);
    }
}
