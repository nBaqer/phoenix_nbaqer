﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgramPeriodDetailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IProgramPeriodDetailService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramPeriodDetail;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The program period detail service interface.
    /// </summary>
    public interface IProgramPeriodDetailService
    {
        /// <summary>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// </summary>
        /// <remarks>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength action method returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length based on enrollmentId.
        /// </returns>
        Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardTermProgramDaysByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// GetNonTermProgramDaysByEnrollmentId method gives the total and completed days for an enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// Based on the enrollment id it searches all the enrolled program details and returns the total days for the period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is of type DateTime is required when the total days is 0.
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number of days required to complete failed courses.
        /// </param>
        /// <returns>
        /// Returns total days for the period.
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        Task<ProgramPeriodTotalDays> GetTotalDaysForNonTermNotSelfPacedProgram(Guid enrollmentId, DateTime? endDate, int numberOfDaysRequired);

        /// <summary>
        /// The get non standard program days for not substantially equal method returns the Total Days and completed days in the period by an enrollment is. 
        /// GetNonStandardProgramDaysForNotSubstantiallyEqual method gives the total and completed days for an enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// EnrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is an optional parameter and will considered as end date to calculate Scheduled Breaks and Approved Loa's in case of direct loan payment period.
        /// </param>
        /// <returns>
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardProgramDaysForNotSubstantiallyEqual(Guid enrollmentId, DateTime? endDate);

        /// <summary>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        Task<CreditHourPaymentPeriodDaysDetail> GetCompletedAndTotalDaysForStandardTermPrograms(Guid enrollmentId);

        /// <summary>
        /// The GetNonStandardProgramDaysDetail returns the completed days and total days in a payment period for Non-Standard term programs.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardProgramDaysDetail action method returns the completed days and total days in a payment period for Non-Standard term programs.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// Returns the success or faliure message determine whether the record has been fetched based on enrollmentId.
        /// </returns>
        Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardProgramDaysByEnrollmentId(Guid enrollmentId, DateTime? endDate);

        /// <summary>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="periodEndDate">
        /// The period End Date.
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number Of Days Required.
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        Task<CreditHourPaymentPeriodDaysDetail> GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment(Guid enrollmentId, DateTime periodEndDate, int numberOfDaysRequired);

        /// <summary>
        /// The GetDaysRequiredToCompleteFailedCourse returns the number of days required to complete failed courses for given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the number of days required to complete failed courses for a given enrollment id
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <param name="periodEndDate">
        /// The period End Date is the end date of the period and is of type DateTime.
        /// </param>
        /// <returns>
        /// Returns the details of number of days required to complete failed courses.
        /// </returns>
        Task<FailedCourseDetails> GetDaysRequiredToCompleteFailedCourse(Guid enrollmentId, DateTime periodEndDate);

        /// <summary>
        /// The get withdrawal period term details returns the term details of the withdrawal period.
        /// </summary>
        /// <remarks>
        /// The get withdrawal period term details returns the term details of the withdrawal period based on the provided enrollment Id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <returns>
        /// Returns the term details of the withdrawal period of the enrolled program.
        /// </returns>
        Task<CreditHourPaymentPeriodDetails> GetWithdrawalPeriodTermDetails(Guid enrollmentId);

        /// <summary>
        /// The GetLastDateAttended returns last date attended from attendance table.
        /// </summary> 
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The last date attended.
        /// </returns>
        Task<DateTime?> GetLastDateAttended(Guid enrollmentId);
    }
}