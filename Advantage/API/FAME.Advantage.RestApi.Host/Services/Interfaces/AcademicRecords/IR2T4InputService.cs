﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IR2T4InputService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IR2T4InputService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;

    /// <summary>
    /// The R2T4InputService interface.
    /// </summary>
    public interface IR2T4InputService
    {
        /// <summary>
        /// The get action allows to fetch the R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The get action requires R2T4InputId and TerminationId and based on the Ids provided, it returns the matching R2T4 Input record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Input where R2T4 Input contains all the information related to R2T4 Input.
        /// </returns>
        Task<R2T4Input> Get(Guid terminationId);

        /// <summary>
        /// The create action allows to save the R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The create action requires R2T4Input object. This object contains the properties of all the R2T4 Input data. 
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks> 
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Input> Create(R2T4Input model);

        /// <summary>
        /// The Update action allows to update the existing R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The update action requires R2T4Input object. This object contains the properties of all the R2T4 Input data. 
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks> 
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        Task<R2T4Input> Update(R2T4Input model);

        /// <summary>
        /// The get R2T4 input service validations.
        /// </summary>
        /// <returns>
        /// The DynamicValidationRuleSet.
        /// </returns>
        DynamicValidationRuleSet GetR2T4InputServiceValidations();

        /// <summary>
        /// The delete action is to delete all the R2T4 details like R2T4Input, R2T4OverrideInput, R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related R2T4 details by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        Task<string> Delete(Guid terminationId);

        /// <summary>
        /// The IsR2T4InputExists action allows to check if the R2T4 Input data is existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsR2T4InputExists action requires terminationId and based on the terminationId provided, it returns boolean value if the R2T4 Input record exists or not.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        Task<bool> IsR2T4InputExists(Guid terminationId);

        /// <summary>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// </summary>
        /// <remarks>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// It requires terminationId based on which the completion status of each tabs is determined.
        /// </remarks>
        /// <param name="terminationId">
        /// terminationId is a required field and it is of type guid .
        /// </param>
        /// <returns>
        /// Returns the boolean values of the tabs to determine whether the tab is completed or not based on terminationId.
        /// </returns>
        Task<R2T4CompletionStatus> GetR2T4CompletedStatus(Guid terminationId);
    }
}