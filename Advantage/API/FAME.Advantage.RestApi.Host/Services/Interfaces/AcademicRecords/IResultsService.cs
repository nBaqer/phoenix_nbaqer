﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IResultsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ResultsService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Results;

    /// <summary>
    /// The ResultsService interface.
    /// </summary>
    public interface IResultsService
    {
        /// <summary>
        /// The get result details returns the results list based on the given parameters.
        /// </summary>
        /// <remarks>
        /// The GetResultDetails action method takes the following parameters enrollmentId, startDate, endDate and
        /// returns all the results details which matches this criteria.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<Results>> GetResultDetails(Guid enrollmentId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// The get result details by given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<Results>> GetResultDetailsByEnrollmentId(Guid enrollmentId);
    }
}
