﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassSectionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IClassSectionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The ClassSectionService interface.
    /// </summary>
    public interface IClassSectionService
    {
        /// <summary>
        /// The insert class section for program version.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> InsertClassSectionsForProgramVersion(Guid programVersionId);

        /// <summary>
        /// The delete by program version.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> DeleteByProgramVersion(Guid programVersionId);

        /// <summary>
        /// The get class section detail by date range.
        /// </summary>
        /// <param name="classSectionIds">
        /// The class Section Ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<ClassSectionDetail>> GetDetailByIds(List<Guid> classSectionIds);
        
        /// <summary>
        /// The get detail by term ids.
        /// </summary>
        /// <param name="termIds">
        /// The term ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<ClassSectionDetail>> GetDetailByTermIds(List<Guid> termIds);

        /// <summary>
        /// The get lms periods.
        /// </summary>
        /// <param name="campusGroupdId">
        /// The campus groupd id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<ClassSectionPeriodLMS>> GetLmsPeriods(Guid campusGroupdId);
    }
}
