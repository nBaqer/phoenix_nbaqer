﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentSummaryService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the IUndoTerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    /// <summary>
    /// The ApproveTerminationService interface.
    /// </summary>
    public interface IStudentSummaryService
    {
        /// <summary>
        /// Gets student summmary data by enrollment id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<StudentSummary>> GetStudentSummaryByEnrollmentId(string enrollmentId);
    }
}