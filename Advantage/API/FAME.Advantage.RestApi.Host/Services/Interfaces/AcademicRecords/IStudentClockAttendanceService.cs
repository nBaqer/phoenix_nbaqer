﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentClockAttendanceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStudentClockAttendanceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;

    /// <summary>
    /// The Student Clock Attendance Service interface.
    /// </summary>
    public interface IStudentClockAttendanceService
    {
        /// <summary>
        /// The GetDetailsByEnrollmentId returns matching student clock attendance based on the given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<StudentClockAttendance>> GetDetailsByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The GetDetailsByScheduleId returns matching student clock attendance based on the given schedule id.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<StudentClockAttendance>> GetDetailsByScheduleId(Guid scheduleId);

        /// <summary>
        /// The get last attendance date from attendance table.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DateTime> GetLastAttendanceDate(Guid enrollmentId);
        /// <summary>
        /// Deleting the record in Student clock Attendance table
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        Task<string> Delete(Guid stuEnrollmentId);
    }
}
