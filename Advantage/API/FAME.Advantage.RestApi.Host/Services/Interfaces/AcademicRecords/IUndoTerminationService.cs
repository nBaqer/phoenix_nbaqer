﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUndoTerminationService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the IUndoTerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    /// <summary>
    /// The ApproveTerminationService interface.
    /// </summary>
    public interface IUndoTerminationService
    {
        /// <summary>
        /// The Undotermination action allows to reverse the termination when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Update action requires Enrollment object which has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a string.
        /// </returns>
        Task<string> UndoTermintion(Guid enrollmentId);
    }
}