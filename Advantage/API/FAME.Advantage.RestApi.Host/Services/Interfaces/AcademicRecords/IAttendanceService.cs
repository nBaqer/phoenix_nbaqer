﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAttendanceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAttendanceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ScheduledHours;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The Attendance interface.
    /// </summary>
    public interface IAttendanceService
    {
        /// <summary>
        /// Calculates the attendance for a student enrollment.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <param name="recalculateAttendance"></param>
        /// <param name="getScheduledHoursPerWeek"></param>
        /// <param name="calculateScheduledHoursToLastDay"></param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<EnrollmentAttendance> CalculateAttendance(Guid studentEnrollmentId, bool recalculateAttendance = true, bool getScheduledHoursPerWeek = false, bool calculateScheduledHoursToLastDay = false);

        /// <summary>
        /// Gets the actual attendance numbers for the student based on maximum of each running total.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        Task<Attendance> GetAttendanceSnapshot(Guid studentEnrollmentId);

        /// <summary>
        /// Gets the actual attendance numbers for the student based on maximum of each running total.
        /// </summary>
        /// <param name="attendanceSummary"></param>
        /// <returns></returns>
        Attendance GetAttendanceSnapshot(IEnumerable<Attendance> attendanceSummary);

        /// <summary>
        /// The calculate scheduled hours to last day.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="lastDateOfAttendance">
        /// The last Date Of Attendance.
        /// </param>
        /// <param name="startdate"></param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        decimal CalculateScheduledHoursToLastDay(Guid studentEnrollmentId, DateTime lastDateOfAttendance, DateTime startdate);


        /// <summary>
        /// Posts zero for attendance for specific students on specific days given parameters
        /// </summary>
        /// <param name="param">
        /// The filters and date to post zero's for attendance for a list of students
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        Task<bool> PostZeroForAttendance(PostAttendanceZero param);

        /// <summary>
        /// Posts zero for attendance for specific students on specific day given parameters
        /// </summary>
        /// <param name="param">
        /// The student id list and date to post zero's for attendance for
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        Task<ActionResult<bool>> PostZerosForAttendance(PostZeroParams param);

        /// <summary>
        /// Imports time clock file given parameters
        /// </summary>
        /// <param name="param">
        /// The parameters required for importing a time clock file
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        Task<ActionResult<string>> ImportTimeClockFile(ImportTimeClockFile param);

        /// <summary>
        /// Inserts or updates time clock import log record
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        Task<ActionResult<string>> UpsertTimeClockLog(TimeClockLogParams param);

        /// <summary>
        /// Returns list of imported time clock log records given campus identifier.
        /// </summary>
        /// <param name="campusId">
        /// Campus Identifier</param>
        /// <param name="count">
        /// Number of records to return. Default 25</param>
        /// <returns></returns>
        Task<ActionResult<List<SyTimeClockImportLog>>> GetTimeClockImportLogsByCampus(Guid campusId, int count = 20);

        /// <summary>
        /// Get students who are scheduled to attend on a given date
        /// </summary>
        /// <param name="campusId">
        /// The campus Id
        /// </param>
        /// <param name="adjustmentDate">
        /// The scheduled datetime
        /// </param>
        /// <param name="studentGroupId">
        /// The student group Id
        /// </param>
        /// <param name="programVersionId">
        /// The program version Id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        Task<IEnumerable<ScheduledHourAdjustment>> GetStudentsScheduledForDay(Guid campusId, DateTime adjustmentDate, Guid? studentGroupId, Guid? programVersionId);

        /// <summary>
        /// Given parameters, adjusts selected subset of students scheduled hours
        /// </summary>
        /// <param name="parameters">
        /// parameters
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        Task<ActionResult<string>> AdjustScheduledHours(ScheduledHoursAdjustment parameters);
    }
}