﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgramVersionsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IEnrollmentService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.CampusProgram;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The IProgramVersionService interface.
    /// </summary>
    public interface IProgramVersionsService
    {
        /// <summary>
        /// The Get program action returns program code by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The get program method action returns program code detail of student.  
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>  
        /// <returns>
        /// Returns program code.
        /// </returns>
        Task<string> GetProgramCodeByEnrollmentId(Guid enrollmentId, Guid studentId);

        /// <summary>
        /// The Get program length by enrollment id action returns program length for Title IV  clock hour program by enrollment id..
        /// </summary>
        /// <remarks>
        /// The Get program length by enrollment id action returns program length for Title IV  clock hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program length.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program length.
        /// </returns>
        Task<string> GetProgramLengthByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get payment periods by enrollment id action returns payment periods for a Clock Hour program whose length is shorter
        /// or equal to the defined academic year by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get payment periods by enrollment id action returns payment periods for a Clock Hour program for Title IV  clock hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns payment periods.
        /// </remarks>  
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment periods.
        /// </returns>
        Task<ProgramPeriodDetails> GetPaymentPeriodsByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get enrollment periods length by enrollment id action returns payment periods length for a Clock Hour program whose length is shorter
        /// or equal to the defined academic year by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get enrollment periods length by enrollment id action returns payment periods length for a Clock Hour program for Title IV  clock hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns payment periods length.
        /// </remarks>  
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns list of enrollment periods length.
        /// </returns>
        Task<EnrollmentPeriodDetails> GetEnrollmentPeriodsLengthByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The GetHoursScheduledToComplete action returns Hours Scheduled To Complete for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetHoursScheduledToComplete action returns Hours Scheduled To Complete for Title IV  clock hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns Hours Scheduled To Complete.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns ScheduledAndTotalHours object with scheduled hours and total hours.
        /// </returns>
        Task<ScheduledAndTotalHours> GetHoursScheduledToComplete(Guid enrollmentId);

        /// <summary>
        /// The get credit hour program length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        Task<CreditHourProgramDetails> GetCreditHourProgramLengthByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get withdrawal payment period service method returns the payment period by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The get withdrawal payment period action method returns the payment period by given enrollment Id,
        /// It calculate the scheduled hours and the total hours in the payment period for a clock hour Title 4 eligible program from which the student has withdrawn.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<CurrentPeriodDetails> GetWithdrawalPaymentPeriod(Guid enrollmentId);

        /// <summary>
        /// The get program for SAP by program version Id.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id and is a type of Guid.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ProgramVersionSAP> GetByProgramVersionId(Guid programVersionId);

        /// <summary>
        /// The get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program and in the parameter  
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns payment periods.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns credit hour program payment periods.
        /// </returns>
        Task<CreditHourProgramePeriodDetails> GetCreditHourPaymentPeriodsByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get period of enrollment with excused absence returns period of enrollments details by enrollment id.
        /// </summary>
        /// <remarks>
        /// The get period of enrollment with excused absence returns period of enrollments details by enrollment id.
        /// This action method calculates allowed excused absence per period of enrollment and return a period
        /// of enrollments details for given enrollment id
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// This method returns PeriodOfEnrollmentDetails with list of PeriodOfEnrollments and a ResultStatus .
        /// </returns>
        Task<PeriodOfEnrollmentDetails> GetPeriodOfEnrollmentWithExcusedAbsence(Guid enrollmentId);

        /// <summary>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id,
        /// It calculates the scheduled hours and the total hours in the period of enrollment for the clock hour Title VI eligible program from which the student has withdrawn.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the withdrawal details of period of enrollment of the student based on the enrollment id.
        /// </returns>
        Task<CurrentPeriodDetails> GetWithdrawalPeriodOfEnrollment(Guid enrollmentId);

        /// <summary>
        /// The get credit hour program period length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        Task<CreditHourProgramePeriodDetails> GetCreditHourPeriodsLengthByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get scheduled and total hours for period of enrollments by given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the scheduled hours and total hours per period of enrollment for a given enrollment id
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The Period Of Enrollment Details.
        /// </returns>
        Task<ScheduledAndTotalHours> GetScheduledAndTotalHoursForPeriodOfEnrollment(Guid enrollmentId);

        /// <summary>
        /// The get Title IV program versions by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and TValue is of type Guid.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetTitleIvProgramVersionsByCampus(Guid campusId);

        /// <summary>
        /// The get program versions by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and TValue is of type Guid.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetProgramVersionsByCampus(Guid campusId);

        /// <summary>
        /// Get Program version ID and Description by Program Id
        /// </summary>
        /// <param name="progId"></param>
        /// <returns></returns>
        Task<IList<IListItem<string, Guid>>> GetByProgramId(Guid progId);
        /// <summary>
        /// The get withdrawal period details method calculates the withdrawal period details for payment period or period of enrollment based on the isPaymentPeriod flag.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and of type guid.
        /// </param>
        /// <param name="isPaymentPeriod">
        /// The is payment period is a boolean flag which will be true for payment period and false for period of enrollment.
        /// </param>
        /// <returns>
        /// Returns an object of <see cref="CurrentPeriodDetails"/> where CurrentPeriodDetails has the details of withdrawal period of the student.
        /// </returns>
        Task<CurrentPeriodDetails> GetWithdrawalPeriodDetails(Guid enrollmentId, bool isPaymentPeriod);

        /// <summary>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the payment period and credits earned by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the returns the payment period and credits earned by given enrollment Id,
        /// It calculate the scheduled hours and the total hours in the payment period for a credit hour Title 4 eligible program from which the student has withdrawn.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<CurrentPeriodDetailsWithCreditsEarned> GetCreditHoursWithdrawalPaymentPeriod(Guid enrollmentId);

        /// <summary>
        /// The set program registration type.
        /// </summary>
        /// <param name="programRegistrationType">
        /// The program registration type.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="gradeScaleId">
        /// The grade Scale Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<ProgramVersion>> SetProgramRegistrationType(
            Enums.ProgramRegistrationType programRegistrationType,
            Guid programVersionId,
            Guid? gradeScaleId);

        /// <summary>
        /// The have enrollments.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> HaveEnrollments(Guid programVersionId);

        /// <summary>
        /// The get program version by enrollment id method returns the list of program version id and program id for a specific enrollment Id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ProgramVersionDetails> GetProgramVersionByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// </summary>
        /// <remarks>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// It requires enrollmentId. Based on the EnrollmentId, it searches all the enrolled program details 
        /// and returns the payment period details. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment period details for the specified enrollment id.
        /// </returns>
        Task<CreditHourPaymentPeriodDetails> GetPaymentPeriodForNotSubstantiallyEqualInLength(Guid enrollmentId);

        /// <summary>
        /// The IsProgramVersionSelfPaced action method returns boolean value based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The IsProgramVersionSelfPaced action check the program type of the enrollment and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId ,it search all the enrolled program details and returns if the enrollment is non term and non self based.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns  true or false.
        /// </returns>
        Task<bool> IsProgramVersionSelfPaced(Guid enrollmentId);

        /// <summary>
        /// The IsProgramVersionSubstantiallyEqual method returns boolean value based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The IsProgramVersionSubstantiallyEqual action checks the program type of the enrollment and in the parameter 
        /// it requires EnrollmentId. 
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// Returns  true or false.
        /// </returns>
        Task<bool> IsProgramVersionSubstantiallyEqual(Guid enrollmentId);

        /// <summary>
        /// The get registration type.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Enums.ProgramRegistrationType?> GetRegistrationType(Guid enrollmentId);

        /// <summary>
        /// The GetTermDetailsForPaymentPeriod  method returns the program terms details defined for it.
        /// </summary>
        /// <remarks>
        /// The GetTermDetailsForPaymentPeriod  action returns the program terms details defined for it and in the parameter 
        /// it requires EnrollmentId and  with Drawal Date. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="withdrawalDate">
        /// The with Drawal Date.
        /// </param>
        /// <returns>
        ///  The payment period details is the payment period details of the student and is of type CreditHourPaymentPeriodDetails. 
        /// </returns>
        Task<CreditHourPaymentPeriodDetails> GetTermDetailsForPaymentPeriod(Guid enrollmentId, DateTime withdrawalDate);

        /// <summary>
        /// The get program version detail by enrollment id returns the program versions details based on the enrollment Id
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is of type Guid.
        /// </param>
        /// <returns>
        /// The Program version details.
        /// </returns>
        Task<ProgramVersionDetails> GetProgramVersionDetailByEnrollmentId(Guid enrollmentId);

        /// <summary>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the withdrawal period of enrollment for credit hours by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the period of enrollment on which student has withdrawn from the program for credit hours by given enrollment id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The CurrentPeriodDetailsWithCreditsEarned with data like credit earned, weeks completed, withdrawal period etc.
        /// </returns>
        Task<CurrentPeriodDetailsWithCreditsEarned> GetNonStandardWithdrawalPeriodOfEnrollment(Guid enrollmentId);

        /// <summary>
        /// The GetCampusProgramVersionDetails action method campus program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// Based on the EnrollmentId ,it search all the enrolled program details and returns the campus program version details.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns the campus program version details.
        /// </returns>
        Task<CampusProgramVersions> GetCampusProgramVersionDetails(Guid enrollmentId);

        /// <summary>
        /// The get all program version by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<AdvStagingProgramV1>> GetAllProgramVersionByCampus(Guid campusId);

        /// <summary>
        /// The get start and end date by enrollment retrieves the start date and end date of enrollment.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The EnrollmentDetails.
        /// </returns>
        Task<EnrollmentDetails> GetEnrollmentDetailsById(Guid enrollmentId);

        /// <summary>
        /// The GetCampusProgramVersion action method campus program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// Based on the EnrollmentId, it search all the enrolled program details and returns the campus program version details.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns the campus program version details.
        /// </returns>
        Task<CampusProgramVersions> GetCampusProgramVersion(Guid enrollmentId);

        /// <summary>
        /// The GetStudentAwardsByEnrollmentId action fetches student awards details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the student awards details based on enrollment id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="lastAttendedDate">
        /// The last Attended Date is a nullable date field and is of type datetime.
        /// </param>
        /// <returns>
        /// Returns the student awards details based on enrollment id.
        /// </returns>
        Task<IEnumerable<StudentAwards>> GetStudentAwardsByEnrollmentId(Guid enrollmentId, DateTime? lastAttendedDate);

        /// <summary>
        /// Get Program Version Description by Id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<ProgramVersion>> GetProgramVersionById(string prgVerId);


        /// <summary>
        /// The GetProgramVersionTotalHours returns the total program hours by program version id.
        /// </summary>
        /// <param name="prgVerId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The total Hours as decimal
        /// </returns>
        Task<decimal> GetProgramVersionTotalHours(Guid prgVerId);

        /// <summary>
        /// The get program versions for naccas.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<NaccasProgramVersion>> GetProgramVersionsForNaccas(Guid campusGroupId);

        /// <summary>
        /// The get lms program version definition.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<ProgramVersionLms>> GetLmsProgramVersionDefinition(Guid programVersionId);
    }
}