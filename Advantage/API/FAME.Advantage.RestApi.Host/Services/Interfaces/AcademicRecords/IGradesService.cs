﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGradesService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the IGradesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Orm.Advantage.Domain.Common;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The GradesService interface.
    /// </summary>
    public interface IGradesService
    {
        /// <summary>
        ///  Given PostFinalGrade DTO parameters, posts the final grade for a given student in a given course
        /// </summary>
        /// <param name="parameters">
        /// </param>
        /// <returns></returns>
        Task<ActionResult<PostFinalGrade>> PostFinalGradeByStudentEnrollmentId(PostFinalGrade parameters);

        /// <summary>
        /// The post grade for a component.
        /// </summary>
        /// <param name="gradeInput"></param>
        /// <returns></returns>
        ApiStatusOutput PostStudentGradeForComponent(StudentPostGradesInput gradeInput);

        /// <summary>
        /// Returns all the component grades for a specific student.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The students enrollment id.
        /// </param>
        /// <returns>
        /// The api output result.
        /// </returns>
        Task<ActionResult<List<StudentComponentGrades>>> GetStudentComponentGradesById(Guid stuEnrollId);
    }
}
