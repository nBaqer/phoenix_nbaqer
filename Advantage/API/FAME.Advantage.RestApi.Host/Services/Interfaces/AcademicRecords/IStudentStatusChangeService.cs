﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentStatusChangeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStudentStatusChangeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Microsoft.AspNetCore.Mvc.Rendering;

    /// <summary>
    /// The GraduationService interface.
    /// </summary>
    public interface IStudentStatusChangeService
    {
        /// <summary>
        /// The get graduation statuses.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetGraduationStatuses(Guid campusId);

        /// <summary>
        /// The get statuses to filter.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStatusesToFilter(Guid campusId);


        /// <summary>
        /// The get the have met options.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<SelectListItem>> GetHaveMetOptions();

        /// <summary>
        /// The get students that have met program length.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="filterByStatus">
        /// The filter by status.
        /// </param>
        /// <param name="haveMetOption">
        /// The have Met Option.
        /// </param>
        /// <param name="pageSize">
        /// The page size.
        /// </param>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="hardLoad">
        /// The hard Load.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<StudentReadyForGraduation>> GetStudentsThatHaveMetProgramLength(
            Guid campusId,
            Guid filterByStatus,
             int haveMetOption,
            int pageSize, 
            int page,
            bool hardLoad);

        /// <summary>
        /// The graduate students.
        /// </summary>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <param name="enrollmentsToUpdate">
        /// The enrollment id and the status code id to change.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> ChangeStatus(Guid campusId, List<ListItem<Guid, Guid>> enrollmentsToUpdate);
    }
}
