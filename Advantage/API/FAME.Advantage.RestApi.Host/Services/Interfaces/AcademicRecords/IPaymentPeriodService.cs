﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPaymentPeriodService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IPaymentPeriodService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
using FAME.Advantage.RestApi.DataTransferObjects.AFA;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The PaymentPeriodService interface.
    /// </summary>
    public interface IPaymentPeriodService
    {
        /// <summary>
        /// The update payment periods for AFA staging.
        /// </summary>
        /// <param name="paymentPeriods">
        /// The collection of paymentPeriods to update.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<Guid, List<PaymentPeriod>>>> UpdateAfaPaymentPeriodStaging(IEnumerable<PaymentPeriod> paymentPeriods);

        /// <summary>
        /// The calculate payment period attendance.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<AdvStagingAttendanceV1> CalculatePaymentPeriodAttendance(Guid studentEnrollmentId);

        /// <summary>
        /// The calculate cumulative payment period attendance.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment identifier.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<AdvStagingAttendanceV1> CalculateCumulativePaymentPeriodAttendance(Guid studentEnrollmentId);

        /// <summary>
        /// The calculate payment period attendance for multiple student enrollment ids.
        /// </summary>
        /// <param name="studentEnrollmentIds"></param>
        /// <returns></returns>
        Task<IEnumerable<AdvStagingAttendanceV1>> CalculatePaymentPeriodAttendance(IEnumerable<Guid> studentEnrollmentIds);

        /// <summary>
        /// The calculate cumulative payment period attendance for multiple student enrollment ids.
        /// </summary>
        /// <param name="studentEnrollmentIds"></param>
        /// <returns></returns>
        Task<IEnumerable<AdvStagingAttendanceV1>> CalculateCumulativePaymentPeriodAttendance(IEnumerable<Guid> studentEnrollmentIds);

        /// <summary>
        /// The calculate payment period attendance by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus identifier.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<AdvStagingAttendanceV1>> CalculatePaymentPeriodAttendanceByCampusForAFA(Guid campusId, bool recalculateAttendance = false);

        /// <summary>
        /// Get payment period number by payment period name for a student enrollment
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="paymentPeriodName"></param>
        /// <returns></returns>
        int? GetPaymentPeriodNumberByName(Guid stuEnrollId, string paymentPeriodName);

    }
}
