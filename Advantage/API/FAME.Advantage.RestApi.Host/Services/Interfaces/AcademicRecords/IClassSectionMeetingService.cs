﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassSectionMeetingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IClassSectionMeetingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSectionMeeting;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The Class Section Meeting interface.
    /// </summary>
    public interface IClassSectionMeetingService
    {
        /// <summary>
        /// The GetTimeIntervals service method returns sum of time interval by given class meeting Ids.
        /// </summary>
        /// <param name="clsMeetingIds">
        /// The cls meeting ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<Guid, decimal>>> GetTimeIntervals(List<Guid?> clsMeetingIds);

        /// <summary>
        /// The GetScheduleForDateRangeByClassSections service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The cls section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<DateTime, decimal>>> GetClassLevelScheduleForDateRangeByClassSections(List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime);

        /// <summary>
        /// The GetProgramLevelScheduleForDateRangeByClassSections service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The cls section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<DateTime, decimal>>> GetProgramLevelScheduleForDateRangeByClassSections(Enrollment enrollment, List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime);

        /// <summary>
        /// The get class section meetings by class section id method returns class section meeting details by classSectionId.
        /// </summary>
        /// <param name="classSectionId">
        /// The class section id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsByClassSectionId(Guid classSectionId);

        /// <summary>
        /// The GetClassSectionMeetingsForDateRangeByClassSections service method returns list of class section meeting schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The cls section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsForDateRangeByClassSections(List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime);

        /// <summary>
        /// The get schedule start date by class section ids.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The cls section ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DateTime?> GetScheduleStartDateByClassSectionIds(List<Guid> clsSectionIds);

        /// <summary>
        /// The GetScheduledWeeksForDateRangeByClassSectionIds action method gets scheduled weeks for given date range and class section ids.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="isByDay">
        /// The is by day.
        /// </param>
        /// <param name="clsSectionIds">
        /// The cls section ids.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Tuple<int, IEnumerable<DateTime>>> GetScheduledWeeksForDateRangeByClassSectionIds(Enrollment enrollment, bool isByDay, List<Guid> clsSectionIds, DateTime startDate, DateTime endDate);

        /// <summary>
        /// The GetClassSectionMeetingsAfterPeriodEndDate service method returns list of hours schedule and date by given class meeting Ids and date.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <returns>
        /// returns an object of type ClassSectionMeetingDetail.
        /// </returns>
        Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsAfterPeriodEndDate(
            List<Guid> clsSectionIds,
            DateTime? startDateTime);

        /// <summary>
        /// The GetClassSectionMeetingDetailsList method will gets class section meetings details for the given list of class section id's.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <returns>
        /// It returns the class section meetings details for the given list of class section id's.
        /// </returns>
        Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingDetailsList(List<Guid> clsSectionIds);

        /// <summary>
        /// The GetClassSectionMeetingDetails method will gets class section meetings details for the given list of class section id's.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <returns>
        /// It returns the class section meetings details for the given list of class section id's.
        /// </returns>
        Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingDetails(Guid clsSectionIds);

        /// <summary>
        /// The GetClassSectionsScheduleForDateRange service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<DateTime, Guid>>> GetClassSectionsScheduleForDateRange(
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime);

        /// <summary>
        /// The GetProgramLevelScheduleForDateRange service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<DateTime, Guid>>> GetProgramLevelScheduleForDateRange(
            Enrollment enrollment,
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime);

        /// <summary>
        /// The GetProgramLevelSchedules service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<DateTime, Guid>>> GetProgramLevelSchedules(
            Enrollment enrollment,
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime);
    }
}
