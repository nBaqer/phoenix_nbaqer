﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentTerminationService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The StudentTerminationService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The StudentTerminationService interface.
    /// </summary>
    public interface IStudentTerminationService
    {
        /// <summary>
        /// The Create action allows you to a save the student termination details.
        /// </summary>
        /// <remarks>
        /// The Create action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="model">
        /// The Create action method accepts a parameter of type studentTerminationDetails.
        /// </param>
        /// <returns>
        /// Returns an object of type StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        Task<StudentTermination> Create(StudentTermination model);

        /// <summary>
        /// The Update action allows you to a update the student termination details.
        /// </summary>
        /// <remarks>
        /// The Update action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="studentTerminationDetails">
        /// The studentTerminationDetails object.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        Task<StudentTermination> Update(StudentTermination studentTerminationDetails);

        /// <summary>
        /// The delete action is to delete all the termination details from database by providing the termination id.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related termination details by providing the termination id.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        Task<string> Delete(Guid terminationId);

        /// <summary>
        /// The Get action is to get all the termination details from database by providing the enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get action is to get the all saved termination details for an incompleted termination by providing the enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid .
        /// </param>
        /// <returns>
        /// Returns all saved termination details for an incompleted termination based on the enrollmentId.
        /// </returns>
        Task<StudentTermination> Get(Guid enrollmentId);

        /// <summary>
        /// The Get action is to get all the termination details from database by providing the enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get action is to get the all saved termination details for an incomplete termination by providing the enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns all saved termination details for an incomplete termination based on the enrollmentId.
        /// </returns>
        Task<StudentTermination> GetById(Guid enrollmentId);

        /// <summary>
        /// The Update undo termination action allows you to a update the student termination details.
        /// </summary>
        /// <remarks>
        /// The Update action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="studentTerminationDetails">
        /// The studentTerminationDetails object.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        Task<StudentTermination> UpdateUndoTermination(StudentTermination studentTerminationDetails);

        /// <summary>
        /// The get student termination status code.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student Enrollment Id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<StatusCode>> GetStudentTerminationStatusCode(Guid studentEnrollmentId, Guid campusId);
    }
}
