﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassSectionAttendanceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//    Defines the IClassSectionAttendanceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The Class Section Attendance Service interface.
    /// </summary>
    public interface IClassSectionAttendanceService
    {
        /// <summary>
        /// The get attendance by enrollment and date range.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="starDateTime">
        /// The star date time.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<StudentAttendanceDetails> GetAttendanceByEnrollmentAndDateRange(
            Guid enrollmentId,  
            DateTime? starDateTime,
            DateTime? endDateTime);

        /// <summary>
        /// The get scheduled hours by enrollment and date range.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<decimal> GetScheduledHoursByEnrollmentAndDateRange(Guid enrollmentId, DateTime startDate, DateTime? withdrawalDate);

        /// <summary>
        /// The get last attendance date from attendance table.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DateTime> GetLastAttendanceDate(Guid enrollmentId);
        /// <summary>
        /// Delete from atClsSectAttendance table
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        Task<string> Delete(Guid stuEnrollmentId);
    }
}
