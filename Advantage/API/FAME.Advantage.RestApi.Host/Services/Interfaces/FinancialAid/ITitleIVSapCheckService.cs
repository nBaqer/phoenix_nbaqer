﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITitleIVSapCheckService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ITitleIVSapCheckService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.FinancialAid
{
    /// <summary>
    /// Title IV Service Interface
    /// </summary>
    public interface ITitleIVSapCheckService
    {

        Task<List<SAPResult>> Execute(Guid? campusId, Guid? programVersionId, IList<Guid> stuEnrollments,
            int? incrementToRun, bool runAllIncrements, bool allowAnyStatus);

        /// <summary>
        /// The studen with title iv sap.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Boolean> RunIVSapCheckForStudents();

    }
}
