﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDataExport9010Service.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IDataExport9010Service interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.AwardType9010;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Report9010;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The IDataExport9010Service interface.
    /// </summary>
    public interface IDataExport9010Service
    {
        /// <summary>
        /// Return csv string containing 9010 data.
        /// </summary>
        /// <param name="parameters">
        /// Parameters to generate the data export.
        /// </param>
        /// <returns>
        /// The <see cref="DataExport9010Param"/>.
        /// </returns>
        Task<ReportActionResult> Get9010SpreadsheetData(DataExport9010Param parameters);

        /// <summary>
        /// Returns all available fund sources and mapping, if any 
        /// </summary>
        /// <param name="campusId">
        /// the campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult<List<Mapping9010>>"/>.
        /// </returns>
        Task<ActionResult<List<Mapping9010>>> Get9010MappingsByCampusId(Guid campusId);

        /// <summary>
        ///  Returns all 9010 award types.
        /// </summary>
        /// <returns></returns>
        Task<ActionResult<List<MappingType9010>>> Get9010AwardTypes();


        /// <summary>
        ///  Given new 9010 award mappings, deletes existing and creates new mappings
        /// </summary>
        /// <returns></returns>
        Task<ActionResult<String>> UpdateCampusAwardMappings9010(List<Mapping9010> awardMappings, Guid campusId);

        /// <summary>
        ///  Given campus id, returns mappings for a given mapping in excel byte array
        /// </summary>
        /// <returns></returns>
        Task<ReportActionResult> Get9010MappingsConfigurationByCampus(Guid campusId);
    }
}
