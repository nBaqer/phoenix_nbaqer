﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IIndianaStateBoardService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IIndianaStateBoardService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.AcademicRecords;

    /// <summary>
    /// The IndianaStateBoard interface.
    /// </summary>
    public interface IIndianaStateBoardService
    {
        /// <summary>
        /// The get indiana state board report data.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="month">
        /// The month.
        /// </param>
        /// <param name="year">
        /// The year.
        /// </param>
        /// <param name="studentGroup">
        /// The student group.
        /// </param>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IndianaStateBoardReportResult>> GetIndianaStateBoardReportData(Guid campusId, int month, int year, Guid? studentGroup, Guid? programVersion);

    }
}
