﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgressReportService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ProgressReportService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    /// <summary>
    /// The ProgressReportService interface.
    /// </summary>
    public interface IProgressReportService

    {
        /// <summary>
        /// The get progress report method.
        /// </summary>
        Task<ReportActionResult> GetProgressReport(Guid studentEnrollmentId, bool showWorkUnitGrouping = true, bool showFinanceCalculations = false, bool ShowWeeklySchedule = true, bool ShowTermModule = false);
    }
}
