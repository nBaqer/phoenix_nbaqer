﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IReportService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IReportService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The ReportService interface.
    /// </summary>
    public interface IReportService
    {
        /// <summary>
        /// The Get Report from SQL Reporting Services.
        /// </summary>
        /// <param name="reportRequest">
        /// The report Request.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ReportActionResult> GenerateReport(IReportRequest reportRequest);

        /// <summary>
        /// The get id by name.
        /// </summary>
        /// <param name="reportName">
        /// The report name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Guid> GetIdByName(string reportName);


    }
}
