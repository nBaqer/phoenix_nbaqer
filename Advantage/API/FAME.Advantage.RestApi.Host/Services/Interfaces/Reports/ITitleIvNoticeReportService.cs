﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITitleIvNoticeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ITitleIvNoticeService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    /// <summary>
    /// The TitleIvNoticeService interface.
    /// </summary>
    public interface ITitleIvNoticeReportService
    {
        /// <summary>
        /// The get title iv notice report.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ReportActionResult> GetTitleIvNoticeReport(List<Guid> enrollments);
    }
}
