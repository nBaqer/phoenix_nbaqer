﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInvoiceReportService.cs" company="FAME INC.">
//   2018
// </copyright>
// <summary>
//   Defines the IInvoiceReportService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Reports;

    public interface IInvoiceReportService
    {
        Task<ReportActionResult> GetInvoiceReport(List<Guid> enrollments, Guid campusId, DateTime refDate);
    }
}
