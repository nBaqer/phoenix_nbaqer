﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="INaccasSettingsService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the IStateBoardSettingsService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.Naccas;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;

    /// <summary>
    /// The Naccas setting interface.
    /// </summary>
    public interface INaccasSettingService
    {
        

        /// <summary>
        /// The save or update naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<NaccasSettings>> SaveOrUpdateNaccasReportSetting(NaccasSettings model);

        /// <summary>
        /// The get naccas report setting.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<NaccasSettings> GetNaccasReportSetting(Guid campusId);

        /// <summary>
        /// The validate setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        bool IsValid(NaccasSettings model, out string message);

        /// <summary>
        /// The delete naccas report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> DeleteNaccasReportSetting(NaccasSettings model);

    }
}
