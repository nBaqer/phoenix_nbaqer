﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStateBoardSettingsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStateBoardSettingsService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;

    /// <summary>
    /// The StateBoardSetting interface.
    /// </summary>
    public interface IStateBoardSettingsService
    {
        /// <summary>
        /// The get state board reports.
        /// </summary>
        /// <param name="stateId">
        /// The state Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStateBoardReports(Guid? stateId);

        /// <summary>
        /// The delete state board setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> DeleteStateBoardSetting(StateBoardReportSetting model);

        /// <summary>
        /// The save or update state board report setting.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<StateBoardReportSetting>> SaveOrUpdateStateBoardReportSetting(StateBoardReportSetting model);

        /// <summary>
        /// The get state board report setting.
        /// </summary>
        /// <param name="stateId">
        /// The state id.
        /// </param>
        /// <param name="reportId">
        /// The report id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<StateBoardReportSetting> GetStateBoardReportSetting(Guid stateId, Guid reportId, Guid campusId);

        /// <summary>
        /// The get indiana state board report settings.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<StateBoardReportSetting> GetIndianaStateBoardReportSettings(Guid campusId);
    }
}
