// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentHistoryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IDocumentManagerService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The IDocumentManagerService interface.
    /// </summary>
    public interface IDocumentHistoryService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The document model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Guid> Create(Document model);

        /// <summary>
        /// The Update action method updates document history for a specific enrollment and student id.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param>
        /// <param name="studentId">
        /// student Id
        /// </param> 
        /// <returns>
        /// The <see cref="Boolean"/>.
        /// </returns>
        Task<bool> Update(Guid enrollmentId, Guid studentId);
    }
}

