﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IObjectValidationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IObjectValidationService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    /// <summary>
    /// The ObjectValidationService interface.
    /// </summary>
    public interface IObjectValidationService
    {
        /// <summary>
        /// The validate object exist.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="propertyName">
        /// The property Name.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <typeparam name="TKey">
        /// </typeparam>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ValidateObjectExist<T, TKey>(TKey key, out string message, string propertyName = null) where T : class;


    }
}
