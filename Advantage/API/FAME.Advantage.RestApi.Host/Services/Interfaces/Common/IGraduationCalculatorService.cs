﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGraduationCalculatorService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The GraduationCalculatorService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The GraduationCalculatorService interface.
    /// </summary>
    public interface IGraduationCalculatorService
    {
        /// <summary>
        /// The get contracted graduation date.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        ActionResult<DateTime> CalculateContractedGraduationDate(Guid scheduleId, DateTime startDate, Guid? campusId);
       

        /// <summary>
        /// The calculate contracted graduation date.
        /// </summary>
        /// <param name="gradDateCalculation">
        /// The grad date calculation data.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        ActionResult<Boolean> UpdateGraduationDate(List<string> StuEnrollIdList);


        /// <summary>
        /// The calculate contracted graduation date.
        /// </summary>
        /// <param name="gradDateCalculation">
        /// The grad date calculation data.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        ActionResult<DateTime> CalculateContractedGraduationDate(GradDateCalculation gradDateCalculation);

        /// <summary>
        /// The get scheduled breaks for holidays returns the scheduled breaks count for the given start date, end date and campus id.
        /// </summary>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<int> GetScheduledBreaksForHolidays(DateTime? startDate, DateTime? endDate, Guid? campusId);
    }
}
