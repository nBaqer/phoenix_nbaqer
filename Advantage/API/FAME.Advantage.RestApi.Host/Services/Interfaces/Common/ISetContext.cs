﻿
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Orm.AdvTenant.Domain.Entities;

    /// <summary>
    /// The SetContext interface.
    /// </summary>
    public interface ISetContext
    {
        /// <summary>
        /// The set advantage context.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        void SetAdvantageContext(ApiUser user);
    }
}
