﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentManagerService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IDocumentManagerService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The IDocumentManagerService interface.
    /// </summary>
    public interface IDocumentManagerService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The document model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Document> Create(Document model);
    }
}
