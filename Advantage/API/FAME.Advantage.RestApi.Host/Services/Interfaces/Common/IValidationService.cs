﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IValidationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IValidationService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    /// <summary>
    /// The ValidationService interface.
    /// </summary>
    /// <typeparam name="Model">
    /// </typeparam>
    public interface IValidationService<Model>
    {
        /// <summary>
        /// The validate model ids.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Model"/>.
        /// </returns>
        Model ValidateModelIds(Model model);
    }
}
