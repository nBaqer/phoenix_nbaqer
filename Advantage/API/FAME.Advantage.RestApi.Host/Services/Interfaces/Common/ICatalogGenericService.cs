﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICatalogGenericService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the catalog generic service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The CatalogGenericService interface.
    /// </summary>
    /// <typeparam name="T">
    /// The generic parameter type.
    /// </typeparam>
    public interface ICatalogGenericService<T>
    {
        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        Task<IList<IListItem<string, T>>> GetByCampus(Guid campusId);

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        Task<IList<IListItem<string, T>>> GetByCampusGroup(Guid campusGroupId);

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The Identifier to get by.
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        Task<IList<IListItem<string, T>>> GetById<TEntId>(TEntId entityId);
    }
}
