﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentDocumentService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IDocumentManagerService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The IDocumentManagerService interface.
    /// </summary>
    public interface IStudentDocumentService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The document model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Document> Create(Document model);
    }
}
