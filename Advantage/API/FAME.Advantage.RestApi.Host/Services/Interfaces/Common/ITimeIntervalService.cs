﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ITimeIntervalService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The TimeInterval interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The Time Interval Service interface.
    /// </summary>
    public interface ITimeIntervalService
    {
        /// <summary>
        /// The get time interval detail.
        /// </summary>
        /// <param name="endTimeIntervalId">
        /// The end Time Interval Id.
        /// </param>
        /// <param name="startTimeIntervalId">
        /// The start Time Interval Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<decimal> GetTimeIntervalDifference(Guid? endTimeIntervalId, Guid? startTimeIntervalId);

        /// <summary>
        /// The get all time intervals.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<TimeInterval>> GetAllTimeIntervals();
    }
}
