﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDocumentStatusService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IDocumentStatusService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Common
{
    using System;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The IDocumentStatusService interface.
    /// </summary>
    public interface IDocumentStatusService
    {
        /// <summary>
        /// The get document status.
        /// </summary>
        /// <param name="status">
        /// The status.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<SyDocStatuses> GetDocumentStatus(int status, Guid campusId);
    }
}
