﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStorageService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStorageService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Storage
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.KlassApp;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The IStorageService interface.
    /// </summary>
    public interface IStorageMethod
    {
        /// <summary>
        /// Delete file
        /// </summary>
        Task<ActionResult<string>> DeleteFile(string path);

        /// <summary>
        /// Explore directory
        /// </summary>
        Task<ActionResult<List<FileInformation>>> ExploreDirectory(string directoryPath);

        /// <summary>
        /// Read file contents
        /// </summary>
        Task<ActionResult<string>> ReadFileContentsAsync(string filePath);

        /// <summary>
        /// Explore directory
        /// </summary>
        Task<ActionResult<FileGetResult>> GetFile(string filePath);

        /// <summary>
        /// Move file
        /// </summary>
        Task<ActionResult<string>> MoveFile(string sourceFilePath, string destinationFilePath);

        /// <summary>
        /// Rename file
        /// </summary>
        Task<ActionResult<string>> RenameFile(string filePath, string newFileName);

        /// <summary>
        /// Update file
        /// </summary>
        Task<ActionResult<string>> UpdateFile(string filePath, byte[] fileData);

        /// <summary>
        /// Write file
        /// </summary>
        Task<ActionResult<string>> UploadFile(string filePath, byte[] fileData);

        /// <summary>
        /// Creates directory
        /// </summary>
        Task<ActionResult<string>> CreateDirectory(string directoryPath);

        /// <summary>
        /// Rename directory
        /// </summary>
        Task<ActionResult<string>> RenameDirectory(string sourceDirectoryPath, string renamedDirectoryPath);

        /// <summary>
        /// Delete directory
        /// </summary>
        Task<ActionResult<string>> DeleteDirectory(string directoryPath);

        /// <summary>
        /// Move directory
        /// </summary>
        Task<ActionResult<string>> MoveDirectory(string sourceDirectoryPath, string targetDirectoryPath);
    }
}
