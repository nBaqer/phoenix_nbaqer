﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStorageService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStorageService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Storage
{
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;

    /// <summary>
    /// The IStorageService interface.
    /// </summary>
    public interface IStorageService
    {
        /// <summary>
        /// Upload file
        /// </summary>
        Task<ActionResult<string>> UploadFile(UploadFileParams requestData);

        /// <summary>
        /// Update file
        /// </summary>
        Task<ActionResult<string>> UpdateFile(UpdateFileParams requestData);

        /// <summary>
        /// Delete file
        /// </summary>
        Task<ActionResult<string>> DeleteFile(DeleteFileParams requestData);

        /// <summary>
        /// Get file
        /// </summary>
        Task<ActionResult<FileGetResult>> GetFile(string token, FileConfigurationFeature featureType, string fileName, Guid campusId, Guid? studentId = null);

        /// <summary>
        /// Get file
        /// </summary>
        Task<ActionResult<FileGetResult>> GetFile(GetFileParams requestData);

        /// <summary>
        /// Rename file
        /// </summary>
        Task<ActionResult<string>> RenameFile(RenameFileParams requestData);

        /// <summary>
        /// Move file
        /// </summary>
        Task<ActionResult<string>> MoveFile(MoveFileParams requestData);

        /// <summary>
        /// Explore directory
        /// </summary>
        Task<ActionResult<List<FileInformation>>> ExploreDirectory(ExploreDirectoryParams requestData);

        /// <summary>
        /// Buld storage path given parameters
        /// </summary>
        Task<ActionResult<BuildPathResult>> BuildStoragePath(BuildPathParams requestData);

        /// <summary>
        /// Returns storage settings by campus and feature
        /// </summary>
        Task<StorageSettings> GetStorageSettingByCampus(FileConfigurationFeature featureType, Guid campusId);

        /// <summary>
        /// Returns automated time clock locations for a given tenant.
        /// </summary>
        Task<ActionResult<List<AutomatedTimeClockLocation>>> GetAutomatedTimeClockLocations();

        /// <summary>
        /// Read file contents
        /// </summary>
        Task<ActionResult<string>> ReadFileContents(ReadFileContentsParams requestData);
    }
}
