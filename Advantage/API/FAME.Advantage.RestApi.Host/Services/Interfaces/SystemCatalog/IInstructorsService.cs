﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInstructorsService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the Instructors Service interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The Instructors service interface.
    /// </summary>
    public interface IInstructorsService
    {
        /// <summary>
        /// Gets All Instructors.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<List<Instructors>>> GetAllInstructors( Guid campusGroupId);
    }
}
