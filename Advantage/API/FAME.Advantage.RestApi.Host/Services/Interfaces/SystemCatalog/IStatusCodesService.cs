﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISyStatusesService.cs" company="FAME Inc">
//   Fame Inc, 2018
// </copyright>
// <summary>
//   Defines the ISyStatusesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Orm.Advantage.Domain.Common;

    /// <summary>
    /// The System Statuses Service interface to get the info regarding active/inactive status.
    /// </summary>
    public interface IStatusCodesService
    {
        /// <summary>
        /// The get active status id.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="campusGroup">
        /// The campus group.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Guid> GetStatusCodeIdByName(string name, SystemStatusLevel level, Guid? campusGroup = null);

        /// <summary>
        /// The get all status codes id by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<Guid>> GetAllStatusCodesIdByName(string name, SystemStatusLevel level);

        /// <summary>
        /// The exists.
        /// </summary>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> Exists(Guid statusCodeId, SystemStatusLevel level, Guid? campusGroupId = null);

        /// <summary>
        /// The is system status.
        /// </summary>
        /// <param name="statusCodeId">
        /// The status code id.
        /// </param>
        /// <param name="systemStatus">
        /// The system status.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> IsSystemStatus(Guid statusCodeId, Constants.SystemStatus systemStatus);

        /// <summary>
        /// The get active not enroll lead status by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<string, Guid>>> GetActiveNotEnrollLeadStatusByCampus(Guid campusId);

        /// <summary>
        /// Get status description by status id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<StatusCode>> GetStatusCodeById(string statusCodeId);

        /// <summary>
        /// The get student status codes.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="studentStatuses">
        /// The student statuses.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStudentStatusCodes(
            Guid campusId,
            StudentStatuses studentStatuses);

        /// <summary>
        /// The get status codes.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="systemStatusCodeId">
        /// The system status code id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStatusCodes(
            Guid campusId,
            List<int> systemStatusCodeId);

        /// <summary>
        /// Get status codes for tenant, or filter by campus group id or campus id.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="StatusCode"/>.
        /// </returns>
        Task<ActionResult<List<StatusCode>>> GetStatusCodes(Guid? campusGroupId = null, Guid? campusId = null);
    }
}
