﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDropStatusCodeService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IDropStatusCode type. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
   
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The DropStatusService interface.
    /// </summary>
    public interface IDropStatusCodeService
    {
        /// <summary>
        /// The Search action methods returns the list of status for the provided campus.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the status for a given campus of type GUID
        /// </remarks>
        /// <response code="200">Returns the list of status found</response>
        /// <param name="dropStatus">
        /// The status search filter object includes the campusId.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="IListItem{TText,TValue}"/>  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetDropStatusByCampusId(DropStatus dropStatus);
    }
}
