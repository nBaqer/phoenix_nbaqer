﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRolesService.cs" company="">
//   Fame Inc,2018
// </copyright>
// <summary>
//   Defines the IRolesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The RolesService interface.
    /// </summary>
    public interface IRolesService
    {
        /// <summary>
        /// The get all roles.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllRoles();

        /// <summary>
        /// The get roles by user type.
        /// </summary>
        /// <param name="userTypeId">
        /// The user Type Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetRolesByUserType(int? userTypeId);

        /// <summary>
        /// The create user roles.
        /// </summary>
        /// <param name="roles">
        /// The roles.
        /// </param>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{UserRole}"/>.
        /// </returns>
        Task<IEnumerable<UserRole>> CreateUserRoles(IEnumerable<UserRole> roles, Guid userId);

        /// <summary>
        /// The get roles for user.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{UserRole}"/>.
        /// </returns>
        Task<IEnumerable<UserRole>> GetRolesForUser(Guid userId);

    }
}
