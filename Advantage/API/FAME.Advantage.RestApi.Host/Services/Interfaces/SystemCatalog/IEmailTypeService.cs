﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailTypeService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the email type service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The EmailTypeService interface.
    /// </summary>
    public interface IEmailTypeService : ICatalogGenericService<Guid>
    {
        /// <summary>
        /// The get all active.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        Task<IList<IListItem<string, Guid>>> Get();
    }
}
