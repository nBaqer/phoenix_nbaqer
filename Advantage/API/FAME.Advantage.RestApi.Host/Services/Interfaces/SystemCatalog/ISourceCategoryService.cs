﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISourceCategoryService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the source category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;

    /// <summary>
    /// The SourceCategoryService interface.
    /// </summary>
    public interface ISourceCategoryService 
    {
        /// <summary>
        /// The has source type.
        /// </summary>
        /// <param name="sourceCategoryId">
        /// The source category id.
        /// </param>
        /// <param name="sourceTypeId">
        /// The source type id.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> HasSourceType(Guid sourceCategoryId, Guid sourceTypeId, Guid? campusId);

        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<SourceCategory>> GetByCampus(Guid campusId);
    }
}
