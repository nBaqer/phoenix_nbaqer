﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserTypeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The UserTypeService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The UserTypeService interface.
    /// </summary>
    public interface IUserTypeService
    {
        /// <summary>
        /// The get user type list for current user .
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetUserTypes();
    }
}
