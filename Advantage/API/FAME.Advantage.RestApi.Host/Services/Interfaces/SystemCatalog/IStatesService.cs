﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStatesService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the states service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The StatesService interface.
    /// </summary>
    public interface IStatesService : ICatalogGenericService<Guid>
    {
        /// <summary>
        /// The get state board states.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStateBoardStates();

        /// <summary>
        /// The get all states.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllStates();

        /// <summary>
        /// The get all states short descriptions.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllStatesShortDescription();

        /// <summary>
        /// The get all states for a give country id.
        /// </summary>
        /// <param name="countryId">
        /// The country Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetStatesByCountryId(Guid? countryId);

        /// <summary>
        /// The get id by name.
        /// </summary>
        /// <param name="stateName">
        /// The state name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Guid> GetIdByName(string stateName);
    }
}
