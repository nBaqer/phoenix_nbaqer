﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISystemConfigurationAppSettingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The SystemConfigurationAppSettingService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The ISystemConfigurationAppSettingService interface.
    /// </summary>
    public interface ISystemConfigurationAppSettingService
    {
        /// <summary>
        /// The get app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetAppSettingValue(string keyName);

        /// <summary>
        /// The get app setting values.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<AppSettingValue>> GetAppSettingValues(string keyName);

        /// <summary>
        /// The get app setting value by campus.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetAppSettingValueByCampus(string keyName, Guid campusId);

        /// <summary>
        /// The set app setting value.
        /// </summary>
        /// <param name="keyName">
        /// The key name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task SetAppSettingValue(string keyName, string value);

    }
}
