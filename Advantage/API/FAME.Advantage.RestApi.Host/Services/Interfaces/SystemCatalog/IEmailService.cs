﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IEmailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The email service interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The EmailAddress service interface.
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// The send email .
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="ApiStatusOutput"/>.
        /// </returns>
        Task<ApiStatusOutput> SendEmail(IEmailBase email, EmailConfiguration settings);

        /// <summary>
        /// Sends email to support users. Overrides email To with support user emails in database
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="ApiStatusOutput"/>.
        /// </returns>
        Task<ApiStatusOutput> SendEmailToSupportUsers(IEmailBase email, EmailConfiguration settings = null);

        /// <summary>
        /// The get smtp settings.
        /// </summary>
        /// <returns>
        /// The <see cref="EmailConfiguration"/>.
        /// </returns>
        EmailConfiguration GetSmtpSettings();

        /// <summary>
        /// The check o auth configuration.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        /// <returns>
        /// The <see cref="EmailConfiguration"/>.
        /// </returns>
        Task<EmailConfiguration> CheckOAuthConfiguration(EmailConfiguration configuration);

        /// <summary>
        /// The get file token.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetFileToken();

        /// <summary>
        /// The setfile token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        void SetFileToken(string token);
    }
}
