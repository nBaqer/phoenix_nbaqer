﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IResourceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Resource service interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The ResourceService interface.
    /// </summary>
    public interface IResourceService
    {
        /// <summary>
        /// The get resource list.
        /// </summary>
        /// <param name="resourceTypeId">
        /// The optional resource Type Id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetResources(int? resourceTypeId);
    }
}
