﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPeriodService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The PeriodService Interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The PeriodService interface.
    /// </summary>
    public interface IPeriodService
    {
        /// <summary>
        /// The get periods.
        /// </summary>
        /// <param name="periodId">
        /// The period Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IListItem<Guid, Guid>> GetPeriodDetail(Guid? periodId);

        /// <summary>
        /// The get periods.
        /// </summary>
        /// <param name="periodIds">
        /// The period Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<PeriodDetail>> GetPeriodAndScheduledHoursByIds(List<Guid> periodIds);
    }
}
