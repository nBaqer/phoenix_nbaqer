﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStatusesService.cs" company="FAME Inc">
//   2017
// </copyright>
// <summary>
//   Defines the ISyStatusesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The System Statuses Service interface to get the info regarding active/inactive status.
    /// </summary>
    public interface IStatusesService
    {
        /// <summary>
        /// The get system status that has information of if its Active(A)/Inactive(I).
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// This gives the list status code 'A' for active and 'I" for Inactive as the text and the value will be their respective GUID
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllSystemStatuses();

        /// <summary>
        /// The get active status id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Task<Guid> GetActiveStatusId();


        /// <summary>
        /// The get inactive status id.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Task<Guid> GetInactiveStatusId();

        /// <summary>
        /// The convert status.
        /// </summary>
        /// <param name="statusId">
        /// The status id.
        /// </param>
        /// <returns>
        /// The <see cref="bool?"/>.
        /// </returns>
        Task<bool?> ConvertStatus(Guid statusId);
    }
}
