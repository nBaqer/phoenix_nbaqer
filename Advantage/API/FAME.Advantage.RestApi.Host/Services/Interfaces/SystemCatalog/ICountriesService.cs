﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICountriesService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the countries service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The CountriesService interface.
    /// </summary>
    public interface ICountriesService : ICatalogGenericService<Guid>
    {
        /// <summary>
        /// The get all countries.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllCountries();
    }
}
