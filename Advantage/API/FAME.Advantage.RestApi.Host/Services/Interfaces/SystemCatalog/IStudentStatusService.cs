﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStudentStatusService.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the IStudentStatusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Threading.Tasks;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The IStudentStatusService interface.
    /// </summary>
    public interface IStudentStatusService
    {
        /// <summary>
        /// The get student status by enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param> 
        /// <returns>
        /// The <see cref="SyStudentStatusChanges"/>.
        /// </returns>
        Task<SyStudentStatusChanges> GetStudentStatus(Guid enrollmentId);

        /// <summary>
        /// Delete student status by enrollmentId from syStudentStatusChanges table.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment Id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        Task<bool> Delete(Guid enrollmentId);
    }
}
