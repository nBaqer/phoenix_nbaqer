﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IModuleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Module service interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The ModuleService interface.
    /// </summary>
    public interface IModuleService
    {
        /// <summary>
        /// The get module list.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetModules();
    }
}
