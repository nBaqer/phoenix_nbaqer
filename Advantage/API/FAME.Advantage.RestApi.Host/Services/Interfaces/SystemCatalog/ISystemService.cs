﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISystemService.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the SystemService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    /// <summary>
    /// The SystemService interface.
    /// </summary>
    public interface ISystemService
    {
        /// <summary>
        /// Clears the API cache.
        /// </summary>
        void ClearCache();

        /// <summary>
        /// The clear cache for tenant.
        /// </summary>
        void ClearCacheForTenant();
    }
}
