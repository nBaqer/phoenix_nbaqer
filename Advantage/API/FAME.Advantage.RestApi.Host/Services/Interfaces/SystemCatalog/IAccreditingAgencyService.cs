﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAccreditingAgencyService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAccreditingAgencyService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The accrediting agency service interface.
    /// </summary>
    public interface IAccreditingAgencyService
    {
        /// <summary>
        /// The get accrediting agencies.
        /// </summary>
        /// <returns>
        /// The <see cref=""/>.
        /// </returns>
        Task<ActionResult<List<AccreditingAgency>>> GetAccreditingAgencies();
    }
}
