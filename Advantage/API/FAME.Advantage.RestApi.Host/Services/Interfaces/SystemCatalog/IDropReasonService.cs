﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDropReasonService.cs" company="FAME Inc.">
//   FAME Inc. 2017 
// </copyright>
// <summary>
//   The DropReasonService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The DropReasonService interface.
    /// </summary>
    public interface IDropReasonService
   {
       /// <summary>
       /// The get drop reason list for current user .
       /// </summary>
       /// <param name="campusId">
       /// The campus id .
       /// </param>
       /// <returns>
       /// The <see cref="Task"/>.
       /// </returns>
       Task<IEnumerable<IListItem<string, Guid>>> GetByCampus(Guid campusId);

       /// <summary>
       /// The get naccas drop reasons.
       /// </summary>
       /// <returns>
       /// The <see cref="Task"/>.
       /// </returns>
       Task<IEnumerable<IListItem<string, Guid>>> GetNaccasDropReasons();
   }
}
