﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICalculationPeriodTypeService.cs" company="FAME Inc.">
//   FAME Inc.
// </copyright>
// <summary>
//   Defines the ICalculationPeriodTypeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The CalculationPeriodTypeService interface.
    /// </summary>
    public interface ICalculationPeriodTypeService
    {
        /// <summary>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations.
        /// </summary>
        /// <remarks>
        /// The Get Period Types action method returns the list of PeriodType(s) for R2T4 Calculations. Period types are Payment Period and Period of Enrollment.
        /// R2T4 claculations are performed based on the selected period.
        /// </remarks> 
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the period type and TValue is of type Guid and will have the PeriodId
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetPeriodTypes();

        /// <summary>
        /// The is payment period type action method returns boolen value.
        /// </summary>
        /// <remarks>
        /// The Is Payment Period action method returns boolean value, if the results value match with Payment period then it returns the true other wise the return value is false.
        /// </remarks>
        /// <param name="periodTypeId">
        /// The period type id is the Guid.
        /// </param>
        /// <returns>
        ///  Returns boolean value based on the matching value. If the period type is Payment Period then the return value is true else it is Period of Enrollment then it returns false.
        /// </returns>
        Task<bool> IsPaymentPeriodType(Guid periodTypeId);
    }
}
