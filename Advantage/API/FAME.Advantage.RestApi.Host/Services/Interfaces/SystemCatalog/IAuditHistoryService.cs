﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuditHistoryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAuditHistoryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The AuditHistoryService interface.
    /// </summary>
    public interface IAuditHistoryService
    {
        /// <summary>
        /// The find audit history event date.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity (table name) to search for on the Audit History.
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DateTime?> FindAuditHistoryEventDate<T>(string eventType, string columnName, string value);

        /// <summary>
        /// The get audit history.
        /// </summary>
        /// <param name="eventType">
        /// The event type.
        /// </param>
        /// <param name="columnName">
        /// The column name.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <typeparam name="T">
        /// The entity (table name) to search for on the Audit History.
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<AuditHistory>> GetAuditHistory<T>(string eventType, string columnName, List<string> value);
    }
}
