﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICountriesTypeService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the countries service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The CountriesTypeService interface.
    /// </summary>
    public interface ICountriesTypeService : ICatalogGenericService<Guid>
    {
    }
}
