﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProgramService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IProgramService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Program;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The ProgramService interface.
    /// </summary>
    public interface IProgramService
    {
        /// <summary>
        /// The get clock hour programs by campus id.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetClockHourProgramsByCampus(Guid campusId);

        /// <summary>
        /// Get Program Description by Id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<ProgramDetails>> GetProgramById(string progId);

        /// <summary>
        /// get the list of active Program and Program versions given campusGrpId.
        /// </summary>
        /// <param name="campGrpId"></param>
        /// <returns></returns>
        Task<IList<ProgramAndProgramVersionName>> GetProgramAndProgramVersionForCampGrp(Guid campGrpId);
    }
}
