﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStateBoardAgencyService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStateBoardAgencyService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;

    /// <summary>
    /// The State Board Agency interface.
    /// </summary>
    public interface IStateBoardAgencyService
    {
        /// <summary>
        /// The get all state board agencies.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetAllStateBoardAgencies();

        /// <summary>
        /// The get state board agencies by state id.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetStateBoardAgenciesByStateId(Guid stateId);

        /// <summary>
        /// The get state board agency.
        /// </summary>
        /// <param name="agencyId">
        /// The agency id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<StateBoardAgency> GetStateBoardAgency(int? agencyId);
    }
}
