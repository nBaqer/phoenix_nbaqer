﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISchoolService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the ISchoolService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The SchoolLogoService interface.
    /// </summary>
    public interface ISchoolService
    {
        /// <summary>
        /// The Get School Detail service takes campus id as input and returns all the information related to campus.
        /// </summary>
        /// <remarks>
        /// The Get School Detail  service requires campus Id object of type Guid.
        /// </remarks> 
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The SchoolDetail, this object contails school name, address, phone numbers, fax, email, website and school logo details
        /// </returns>
        Task<SchoolDetail> GetDetail(Guid campusId);

        /// <summary>
        /// The clear school cash.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> ClearSchoolCash(Guid campusId);
    }
}
