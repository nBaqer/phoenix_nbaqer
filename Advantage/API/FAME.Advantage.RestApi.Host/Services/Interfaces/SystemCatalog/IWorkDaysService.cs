﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWorkDaysService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The WorkDaysService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The WorkDaysService interface.
    /// </summary>
    public interface IWorkDaysService
    {
        /// <summary>
        /// The get all work days.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<WorkDay>> GetAllWorkDays();
    }
}
