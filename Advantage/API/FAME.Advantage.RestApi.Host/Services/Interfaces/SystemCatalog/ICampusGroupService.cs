﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICampusGroupService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusGroupService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The CampusGroupService interface.
    /// </summary>
    public interface ICampusGroupService
    {
        /// <summary>
        /// The get campus group all.
        /// </summary>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        Task<Guid> GetCampusGroupAllId();

        /// <summary>
        /// The get all campus groups.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllCampusGroups();

        /// <summary>
        /// The get campus groups for campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<Guid>> GetCampusGroupsForCampus(Guid campusId);

        /// <summary>
        /// The get all the campuses associated with a campus group id
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<Guid>> GetAllTheCampusesByCampusGroup(Guid campusGroupId);
    }
}
