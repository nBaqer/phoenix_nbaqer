﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserRoleStatusService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The UserRoleStatusService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Threading.Tasks;

    /// <summary>
    /// The User role status service interface.
    /// </summary>
    public interface IUserRoleStatusService
    {
        /// <summary>
        /// The IsSupportUser action allows you to validate whether the logged in user is a support user or not.
        /// </summary>
        /// <remarks>
        /// The IsSupportUser method allows you to validate whether the current logged in user is a support user or not.
        /// </remarks>
        /// <returns>
        /// Returns true if the selected user is a support user else returns false.
        /// </returns>
        Task<bool> IsSupportUser();
    }
}
