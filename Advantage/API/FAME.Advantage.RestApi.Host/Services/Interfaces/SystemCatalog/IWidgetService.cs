﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IWidgetService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Widget Service interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Widget;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The widget service interface.
    /// </summary>
    public interface IWidgetService
    {
        /// <summary>
        /// Returns all available widgets along with settings if they exist for a user depending on users system role and current module/> class.
        /// </summary>
        /// <param name="resourceId">
        /// The resouce the user is currently accessing
        /// </param>
        /// <param name="campusId">
        /// The users current campus id
        Task<IEnumerable<Widget>> GetUserResourceWidgets(int resourceId, Guid campusId);
    }
}
