﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICatalogService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the catalog service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The CatalogService interface.
    /// </summary>
      public interface ICatalogService
    {
        /// <summary>
        /// The get catalogs.
        /// </summary>
        /// <returns>
        /// The <see cref="Catalog"/>.
        /// </returns>
        Task<IList<Catalog>> GetCatalog();

    }
}
