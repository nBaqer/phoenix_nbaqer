﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserQuestionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The UserQuestionService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The UserQuestion interface.
    /// </summary>
    public interface IUserQuestionService
    {
        /// <summary>
        /// The get user question list for tenant.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetUserQuestions();
    }
}
