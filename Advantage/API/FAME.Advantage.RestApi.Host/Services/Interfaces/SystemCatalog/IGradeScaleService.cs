﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGradeScaleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IGradeScaleService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The GradeScaleService interface.
    /// </summary>
    public interface IGradeScaleService
    {
        /// <summary>
        /// The get grade scales by grade system.
        /// </summary>
        /// <param name="gradeSystemId">
        /// The grade system id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<IList<IListItem<string, Guid>>>> GetGradeScales(Guid gradeSystemId);
    }
}
