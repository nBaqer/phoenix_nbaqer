﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ISystemStatusService.cs" company="Fame Inc">
//   Fame Inc 2018
// </copyright>
// <summary>
//   Defines the ISysStatusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using FAME.Orm.Advantage.Domain.Common;

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The SysStatusService interface.
    /// </summary>
    public interface ISystemStatusService
    {
        /// <summary>
        /// The get active status id.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="level">
        /// The level.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<int> GetSystemStatusCodeIdByName(string name, SystemStatusLevel level);

        /// <summary>
        /// The get in school system statuses.
        /// </summary>
        Task<IEnumerable<Constants.SystemStatus>> GetInSchoolSystemStatuses();

        /// <summary>
        /// The get active system statuses.
        /// </summary>
        Task<IEnumerable<Constants.SystemStatus>> GetActiveSystemStatuses();

    }
}
