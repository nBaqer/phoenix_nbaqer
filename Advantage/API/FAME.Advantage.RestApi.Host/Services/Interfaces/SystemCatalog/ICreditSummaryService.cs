﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICreditSummaryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ICreditSummaryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The CreditSummaryService interface.
    /// </summary>
    public interface ICreditSummaryService
    {
        /// <summary>
        /// The get credit summary detail.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollmentId.
        /// </param>
        /// <param name="reqIds">
        /// The req Ids.
        /// </param>
        /// <param name="termIds">
        /// The term Ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<CreditSummary>> GetCreditSummaryDetail(Guid enrollmentId, IList<Guid> reqIds, IList<Guid> termIds);

        /// <summary>
        /// The get credit summary by enrollment id returns all the credit summary for given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The list of CreditSummary.
        /// </returns>
        Task<IEnumerable<CreditSummary>> GetCreditSummaryByEnrollmentId(Guid enrollmentId);
    }
}
