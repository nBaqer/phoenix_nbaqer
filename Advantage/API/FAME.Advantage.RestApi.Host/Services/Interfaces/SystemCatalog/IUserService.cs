﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IUserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the User Service interface
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;

    /// <summary>
    /// The User service interface.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// The Search action methods returns the list of users(s) for the provided user type, status, and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given user type of type integer and Search filter of type string
        /// </remarks>
        /// <param name="userSearchFilter">
        /// The user search input object includes the status id, user type id and a Filter string which should of minimum 3 letters of the student FullName, DisplayName or EmailAddress.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the user and displayname and email and user type and TValue is of type Guid and will have the UserId
        /// </returns>
        Task<IEnumerable<IListItem<string, UserSearchOutput>>> Search(UserSearchInput userSearchFilter);

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<User> Create(User user);

        /// <summary>
        /// The get user by id.
        /// </summary>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<User> GetUserbyId(Guid userId);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<User> Update(User model);

        /// <summary>
        /// The send new user email .
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ApiStatusOutput> SendNewUserEmail(EmailBase model);

        /// <summary>
        /// The send reset password email.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ApiStatusOutput> SendResetPasswordEmail(EmailBase model);

        /// <summary>
        /// The check if username exist.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CheckIfUsernameExist(string username);

        /// <summary>
        /// The check if email exist.
        /// </summary>
        /// <param name="email">
        /// email id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool CheckIfEmailExist(string email);

        /// <summary>
        /// Used to unlock a user account.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        bool UnlockUserAccount(string email);

        /// <summary>
        /// The generate temp password.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GenerateTempPassword();

        /// <summary>
        /// The update user password.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="newPassword">
        /// The new Password.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        bool UpdateUserPassword(string email, string newPassword);

        /// <summary>
        /// The update password set.
        /// </summary>
        /// <param name="passwordSet">
        /// The password set.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ApiStatusOutput> UpdatePasswordSet(UserPassword passwordSet);

        /// <summary>
        /// The get current user type.
        /// </summary>
        /// <returns>
        /// The <see cref="UserType"/>.
        /// </returns>
        UserType GetCurrentUserType();

        /// <summary>
        /// The get current user information.
        /// </summary>
        /// <returns>
        /// The <see cref="UserInformation"/>.
        /// </returns>
        UserInformation GetCurrentUserInformation();

        /// <summary>
        /// The get current user terms of use.
        /// </summary>
        /// <returns>
        /// The <see cref="UserTermsOfUse"/>.
        /// </returns>
        UserTermsOfUse GetCurrentUserTermsOfUse();

        /// <summary>
        /// The update user terms of use.
        /// </summary>
        /// <param name="userId">
        /// The user's id.
        /// </param>
        /// /// <param name="version">
        /// The version of advantage.
        /// </param>
        /// <returns>
        /// The <see cref="UserTermsOfUse"/>.
        /// </returns>
        Task<UserTermsOfUse> UpdateUserTermsOfUse(Guid userId, string version);

        /// <summary>
        /// The get admin reps belonging to the given campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<string, Guid>>> GetAdminRepsByCampus(Guid campusId);

        /// <summary>
        /// The get current user access to a certain resource/Page.
        /// </summary>
        /// <param name="resId">
        /// The resource id/ page Id.
        /// </param>
        /// <param name="userId">
        /// The logged in User Id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<bool> GetUserAcessPermissionsForResourceId(int resId, Guid userId);
    }
}
