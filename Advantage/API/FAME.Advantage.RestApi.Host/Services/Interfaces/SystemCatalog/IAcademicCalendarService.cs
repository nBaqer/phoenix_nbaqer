﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAcademicCalendarService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The AcademicCalendarService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The AcademicCalendarService interface.
    /// </summary>
    public interface IAcademicCalendarService
    {
        /// <summary>
        /// The IsClockHour action allows you to validate whether the selected enrollment is of program type clock hour or credit hour.
        /// </summary>
        /// <remarks>
        /// The IsClockHour action requires studentEnrollmentId to check whether it is of program type clock hour or credit hour.
        /// </remarks>
        /// <param name="studentEnrollmentId">
        /// The studentEnrollmentId is of type Guid is the selected enrollment id of the specified student.
        /// </param>
        /// <returns>
        /// Returns true if the given enrollment is of program type clock hour else returns false if it is of program type credit hour. 
        /// </returns>
        Task<bool> IsClockHour(Guid studentEnrollmentId);

        /// <summary>
        /// The GetAllAcademicCalendars action allows you to get all the academic calendar programs.
        /// </summary>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and will show you the academic caledar program type and TValue is of type Guid and will have the academic calendar program.
        /// </returns>
        Task<IEnumerable<IListItem<string, string>>> GetAllAcademicCalendars();
    }
}
