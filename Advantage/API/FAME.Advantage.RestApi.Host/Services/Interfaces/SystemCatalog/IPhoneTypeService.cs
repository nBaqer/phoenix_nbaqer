﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPhoneTypeService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the phone type service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The PhoneTypeService interface.
    /// </summary>
    public interface IPhoneTypeService : ICatalogGenericService<Guid>
    {
    }
}
