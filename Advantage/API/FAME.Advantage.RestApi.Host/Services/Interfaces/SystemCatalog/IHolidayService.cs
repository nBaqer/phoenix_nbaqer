﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IHolidayService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The HolidayService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The HolidayService interface.
    /// </summary>
    public interface IHolidayService
    {
        /// <summary>
        /// The get holiday list.
        /// </summary>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="ReportServiceClient.Task"/>.
        /// </returns>
        Task<IEnumerable<Holiday>> GetHolidayListByCampus(Guid campusId);
    }
}
