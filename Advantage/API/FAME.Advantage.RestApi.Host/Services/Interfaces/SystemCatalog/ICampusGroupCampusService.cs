﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICampusGroupCampusService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusGroupCampusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The CampusGroupCampusService interface.
    /// </summary>
    public interface ICampusGroupCampusService
    {  
        /// <summary>
        /// The get campus group by campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<SyCampGrps> GetCampusGroupByCampusId(Guid campusId);

        /// <summary>
        /// The get campus groups by campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<SyCampGrps>> GetCampusGroupsByCampusId(Guid campusId);
    }
}
