﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPeriodsWorkDaysService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The PeriodsWorkDaysService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    using ReportServiceClient;

    /// <summary>
    /// The PeriodsWorkDaysService interface.
    /// </summary>
    public interface IPeriodsWorkDaysService
    {
        /// <summary>
        /// The get periods work days details returns the list of periodWorkDays based on the given period Ids.
        /// </summary>
        /// <param name="periodIds">
        /// The period ids.
        /// </param>
        /// <returns>
        /// The List of PeriodsWorkDays.
        /// </returns>
        Task<IEnumerable<PeriodsWorkDays>> GetPeriodsWorkDaysDetails(IList<Guid> periodIds);

    }
}
