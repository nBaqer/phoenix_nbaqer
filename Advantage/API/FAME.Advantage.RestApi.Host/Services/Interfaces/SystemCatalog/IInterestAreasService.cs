﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IInterestAreasService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the interest area service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The InterestAreasService interface.
    /// </summary>
    public interface IInterestAreasService : ICatalogGenericService<Guid>
    {
    }
}
