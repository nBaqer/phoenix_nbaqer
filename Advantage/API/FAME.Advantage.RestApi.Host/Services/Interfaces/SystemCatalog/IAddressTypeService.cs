﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAddressTypeService.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the interface for the address type service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;

    /// <summary>
    /// The AddressTypeService interface.
    /// </summary>
    public interface IAddressTypeService : ICatalogGenericService<Guid>
    {
    }
}
