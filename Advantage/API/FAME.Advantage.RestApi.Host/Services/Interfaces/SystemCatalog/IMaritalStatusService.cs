﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMaritalStatusService.cs" company="FAME Inc">
//   2018
// </copyright>
// <summary>
//   The MaritalStatusService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The MaritalStatusService interface.
    /// </summary>
    public interface IMaritalStatusService
    {
        /// <summary>
        /// The get by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see>
        /// <cref>IList</cref>
        /// </see>.
        /// </returns>
        Task<IList<MaritalStatus>> GetByCampus(Guid campusId);

        /// <summary>
        /// The get by campus group.
        /// </summary>
        /// <param name="campusGroupId">
        /// The campus group id.
        /// </param>
        /// <returns>
        /// The <see>
        /// <cref>IList</cref>
        /// </see>.
        /// </returns>
        Task<IList<MaritalStatus>> GetByCampusGroup(Guid campusGroupId);

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="entityId">
        /// The entity id.
        /// </param>
        /// <typeparam name="TEntId">
        /// The Identifier to get by.
        /// </typeparam>
        /// <returns>
        /// The <see>
        /// <cref>IList</cref>
        /// </see>.
        /// </returns>
        Task<IList<MaritalStatus>> GetById<TEntId>(TEntId entityId);
    }
}
