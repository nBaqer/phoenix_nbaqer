﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICampusService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ICampusService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The CampusService interface.
    /// </summary>
    public interface ICampusService
    {
        /// <summary>
        /// The get all campuses.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, Guid}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllCampuses();

        /// <summary>
        /// The get all campuses belonging to the logged in UserId.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<IListItem<string, Guid>>> GetAllCampusesByUserId();
        /// <summary>
        /// The get campus id for CMS id.
        /// </summary>
        /// <param name="cmsId">
        /// The CMS id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Guid GetCampusIdForCmsId(string cmsId);

        /// <summary>
        /// The get cms ids to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<string>> GetCmsIdsToSync();


        /// <summary>
        /// The get Client management software id for campus id.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCmsIdForCampusId(string campusId);

        /// <summary>
        /// Returns if AFA Integration is enabled for given campus.
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        bool IsAFAIntegrationEnabled(Guid campusId);
        
        /// <summary>
        /// Get school name by campus id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<CampusInformation>> GetCampusById(string campusId);

        /// <summary>
        /// The get campus group all.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Guid> GetCampusGroupAllId();

        /// <summary>
        /// Get school name by campus id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<List<CampusGroupsWithCampus>>> GetCampusGroupsWithCampus();
    }
}
