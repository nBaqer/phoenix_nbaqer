﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAttendanceUnitTypesService.cs" company="FAME Inc. 2018">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAttendanceUnitTypesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The AttendanceUnitTypesService interface.
    /// </summary>
    public interface IAttendanceUnitTypesService
    {
        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IList<IListItem<string, Guid>>> GetAll();

        /// <summary>
        /// The get by description.
        /// </summary>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        IListItem<string, Guid> GetByDescription(string description);
    }
}
