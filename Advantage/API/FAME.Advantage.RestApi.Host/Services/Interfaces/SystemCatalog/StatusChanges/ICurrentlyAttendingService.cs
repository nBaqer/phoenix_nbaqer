﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ICurrentlyAttendingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ICurrentlyAttendingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The CurrentlyAttendingService interface.
    /// </summary>
    public interface ICurrentlyAttendingService : IStatusChange
    {
    }
}
