﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStatusChange.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStatusChange type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The StatusChange interface.
    /// </summary>
    public interface IStatusChange
    {
        /// <summary>
        /// The can change.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> CanChange(Guid campusId, Guid enrollmentId);

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="statusCodeId">
        /// The status Code Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ActionResult<bool>> ChangeStatus(Guid campusId, Guid enrollmentId, Guid statusCodeId);

    }
}
