﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IGraduationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The GraduationService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The GraduationService interface.
    /// </summary>
    public interface IGraduationService : IStatusChange
    {
    }
}
