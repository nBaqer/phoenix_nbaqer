﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStateBoardCourseService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IStateBoardCourseService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The StateBoardCourse interface.
    /// </summary>
    public interface IStateBoardCourseService
    {
        /// <summary>
        /// The get state board course by state id.
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{IListItem{string, int}}"/>.
        /// </returns>
        Task<IEnumerable<IListItem<string, int>>> GetByStateId(Guid stateId);
    }
}
