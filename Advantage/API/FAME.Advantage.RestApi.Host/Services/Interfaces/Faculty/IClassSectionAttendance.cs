﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IClassSectionAttendance.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The ClassSectionAttendance interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Faculty
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The ClassSectionAttendance interface.
    /// </summary>
    public interface IClassSectionAttendance
    {
        /// <summary>
        /// The get attendance.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<IEnumerable<AtClsSectAttendance>> GetAttendance();
    }
}
