﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAdvantageDbContext.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAdvantageDbContext interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces
{
    using Fame.EFCore.Advantage.Interfaces;

    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;

    /// <summary>
    /// The AdvantageDbContext interface.
    /// </summary>
    public interface IAdvantageDbContext : IAdvantageContext
    {
        /// <summary>
        /// The set context.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="simpleCacheKey">
        /// The simple cache key.
        /// </param>
        void SetContext(IAdvantageContext context, string simpleCacheKey);

        /// <summary>
        /// The get simple cache connection string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetSimpleCacheConnectionString();

        /// <summary>
        /// The set tenant user.
        /// </summary>
        /// <param name="tenantUser">
        /// The tenant user.
        /// </param>
        void SetTenantUser(ITenantUser tenantUser);

        /// <summary>
        /// The get tenant user.
        /// </summary>
        /// <returns>
        /// The <see cref="ITenantUser"/>.
        /// </returns>
        ITenantUser GetTenantUser();
        
    }
}
