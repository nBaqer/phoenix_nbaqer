﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILeadsAddressService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The LeadsAddressService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The LeadsAddressService interface.
    /// </summary>
    public interface ILeadsAddressService
    {
        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<ListActionResult<Address>> GetAll(Guid leadId);

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Address> Get(Guid leadId, Guid id);

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Address> Create(Address model);

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Address> Update(Address model);

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<string> Delete(Guid leadId, Guid id);

        /// <summary>
        /// The get best.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <param name="address2">
        /// The address 2.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetBest(Guid studentEnrollmentId, out string city, out string state, out string zipCode, out string address2);
    }
}
