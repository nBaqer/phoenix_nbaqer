﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILeadService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the ILeadService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions
{
    using System;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;

    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// The LeadService interface.
    /// </summary>
    public interface ILeadService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        ///  <param name="fromIntegration">
        /// The optional parameter to not validate the email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<Lead> Create(Lead model, bool fromIntegration = false);

        /// <summary>
        /// The update allows you to update an existing lead in advantage. Required fields such as First Name, Last Name are validated ahead of time.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <remarks>
        /// The update requires a <see cref="LeadBase"/> object with at least first name, last name in order to be successfully updated in advantage. If any duplicate by the update is found, then the record will be updated and marked as duplicate.
        /// Changing the Lead last name:
        /// ** If the last name is updated, the api will return you on the response a ResultStatus of "Unable to update Last Name, an official name change was not provided", if the IsOfficialNameChange field is null or empty. Setting the IsOfficialNameChange to Yes will track Prior Last Name.
        /// ** Setting the IsOfficialNameChange to  No will still update the Last Name field but not update the Prior Last name field
        /// Updating the email:
        /// ** To update an email use the Lead/{leadId}/EmailAddress/{id}/Put action
        /// Updating the phone:
        /// ** To update a phone use the Lead/{leadId}/Phone/{id}/Put action
        /// Updating the address:
        /// ** To update an address use the Lead/{leadId}/Address/{id}/Put action
        /// </remarks>
        /// <returns>
        /// The <see cref="LeadBase"/>.
        /// </returns>
        Task<LeadBase> Update(LeadBase model);

        /// <summary>
        /// The update AFA student id.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<LeadBase> UpdateAfaStudentId(LeadBase model);

        /// <summary>
        /// The check if AFA student id exists.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<AfaStudentExist> CheckIfAfaStudentIdExists(LeadBase model);

        /// <summary>
        /// The get lead that's not enrolled
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<DataTransferObjects.Admissions.Lead.V2.Lead> Get(Guid id);

        /// <summary>
        /// The validate duplicates.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task ValidateDuplicates(Lead model);

        /// <summary>
        /// The get lead by student id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<AdLeads> GetByStudentId(Guid id);
    }
}
