﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRequirementsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The IRequirementsService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions
{
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The RequirementsService interface.
    /// </summary>
    public interface IRequirementsService
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="code">
        /// The string code.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<AdReqs> GetByCode(string code);
    }
}
