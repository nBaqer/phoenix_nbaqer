﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaService.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the AfaService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts.AFA;
using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;
using System;
using System.Linq;
using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
using FAME.Advantage.RestApi.Host.Extensions;
using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;

namespace FAME.Advantage.RestApi.Host.Services.AFA
{
    using AutoMapper;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Maintenance.Wapi;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Http;
    using System.ServiceProcess;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The AFA service.
    /// </summary>
    public class AfaService : IAfaService
    {
        /// <summary>
        /// The mapper is used to map the Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;

        /// <summary>
        /// The wapi service.
        /// </summary>
        private readonly IWapiServiceService wapiService;

        /// <summary>
        /// The student awards service.
        /// </summary>
        private readonly IStudentAwardsService studentAwardsService;

        /// <summary>
        /// The payment periods service.
        /// </summary>
        private readonly IPaymentPeriodService paymentPeriodService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;
        private readonly ILeadService leadService;
        private readonly ICatalogService catalogService;
        private readonly IAfaLeadSyncExceptionService afaLeadSyncExceptionService;
        /// <summary>
        /// The wapiOperationLoggerService
        /// </summary>
        private readonly IWapiOperationLoggerService wapiOperationLoggerService;

        /// <summary>
        /// The campus service
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The email service
        /// </summary>
        private readonly IEmailService emailService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingService;

        /// <summary>
        /// The system integration settings
        /// </summary>
        private readonly IIntegrationSettings integrationSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="AfaService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="serviceBuilder">
        /// The service builder.
        /// </param>
        /// <param name="wapiOperationLoggerService">
        /// The wapiOperationLoggerService.
        /// </param>
        public AfaService(IAdvantageDbContext unitOfWork, IMapper mapper, IServiceBuilder serviceBuilder, IWapiServiceService wapiService, IStudentAwardsService studentAwardsService, IPaymentPeriodService paymentPeriodService, IEnrollmentService enrollmentService, ICampusService campusService, IWapiOperationLoggerService wapiOperationLoggerService, ILeadService leadService, ICatalogService catalogService, IAfaLeadSyncExceptionService afaLeadSyncExceptionService, IEmailService emailService, ISystemConfigurationAppSettingService appSettingService, IIntegrationSettings integrationSettings)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.serviceBuilder = serviceBuilder;
            this.wapiService = wapiService;
            this.studentAwardsService = studentAwardsService;
            this.paymentPeriodService = paymentPeriodService;
            this.enrollmentService = enrollmentService;
            this.campusService = campusService;
            this.wapiOperationLoggerService = wapiOperationLoggerService;
            this.leadService = leadService;
            this.catalogService = catalogService;
            this.afaLeadSyncExceptionService = afaLeadSyncExceptionService;
            this.emailService = emailService;
            this.appSettingService = appSettingService;
            this.integrationSettings = integrationSettings;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// Gets or sets the client.
        /// </summary>
        private HttpClient Client { get; set; }

        /// <summary>
        /// The get AFA session key.
        /// </summary>
        /// <param name="afaSession">
        /// The AFA session.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public AfaSession GetAfaSessionKey(AfaSession afaSession)
        {
            var user = this.unitOfWork.GetTenantUser();
            if (user.IsSupportUser)
            {
                if (!string.IsNullOrEmpty(afaSession.ClientKey) && !string.IsNullOrEmpty(afaSession.UserName)
                    && !string.IsNullOrEmpty(afaSession.Password))
                {
                    // make a call to the AFA API and return the session
                    this.Client = new HttpClient();
                    this.Client.DefaultRequestHeaders.Add("ClientKey", afaSession.ClientKey);
                    var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                    wapiSetting = this.GetWapiSettingsForAfa();
                    var uri = wapiSetting.ExternalUrl;
                    uri = uri + "/users/userlogin";
                    //var uri = "http://advqa.fameinc.com/AFAservices/QA/AFAService.WebApi/users/userlogin";
                    HttpResponseMessage response;
                    AfaSession payload = new AfaSession();
                    try
                    {
                        var jsonInString = JsonConvert.SerializeObject(afaSession);

                        var task = this.Client.PostAsync(
                            uri,
                            new StringContent(jsonInString, Encoding.UTF8, "application/json")).ContinueWith(
                            taskwithmsg =>
                                {
                                    payload = JsonConvert.DeserializeObject<AfaSession>(
                                        taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString());
                                    if ((int)taskwithmsg.Result.StatusCode == 200)
                                    {
                                        payload.ClientKey = afaSession.ClientKey;
                                        payload.UserName = afaSession.UserName; // "famedev";
                                        payload.Password = afaSession.Password; // "pass";
                                        response = taskwithmsg.Result;
                                        Debug.WriteLine(
                                            $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                                    }
                                    else
                                    {
                                        response = taskwithmsg.Result;
                                        Debug.WriteLine(
                                            $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                                        var wapiLogger = new WapiOperationLogger();
                                        wapiLogger.Comment = $"Error getting AfaSessionKey - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                                        wapiLogger.CompanyCode = "";
                                        wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                                        wapiLogger.DateExecution = DateTime.Now;
                                        wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                                        wapiLogger.IsError = true;
                                        this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                                    }
                                });
                        task.Wait();
                        return payload;
                    }
                    catch (HttpRequestException e)
                    {
                        e.TrackException();
                        payload.SessionKey = null;
                        return payload;
                    }
                }
                else
                {
                    // throw error need the clientKey or userName or Password
                    return null;
                }
            }
            else
            {
                // throw exception saying only supposrt user can access.
                return null;
            }
        }

        /// <summary>
        /// The get all lead demographics.
        /// </summary>
        /// <param name="sessionKey">
        /// The session key.
        /// </param>
        /// <param name="campusId">
        /// The school campusId.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        public async Task<IList<AdvStagingDemographicsV1>> GetAllLeadDemographicsAsync(string sessionKey, Guid campusId)
        {
            var cmsId = this.campusService.GetCmsIdForCampusId(campusId.ToString());

            var user = this.unitOfWork.GetTenantUser();
            if (user.IsSupportUser)
            {
                if (!string.IsNullOrEmpty(sessionKey))
                {
                    // make a call to afa to get all demographics
                    this.Client = new HttpClient();
                    this.Client.DefaultRequestHeaders.Add("SessionKey", sessionKey);
                    var wapiSetting = this.GetWapiSettingsForAfa();
                    var uri = wapiSetting.ExternalUrl;
                    uri = uri + "/integration/sis/demographic/all/v1/get/" + cmsId;
                    HttpResponseMessage response;
                    IList<AdvStagingDemographicsV1> leads = new List<AdvStagingDemographicsV1>();
                    var task = await this.Client.GetAsync(uri);
                    response = task;
                    if (response.IsSuccessStatusCode)
                    {
                        var leadjson = response.Content.ReadAsStringAsync().Result;
                        leads = JsonConvert.DeserializeObject<List<AdvStagingDemographicsV1>>(leadjson);
                        // adding the code here for sync all leads to have all in one method call
                        var catalogResp = this.catalogService.GetCatalog();
                        var campCatalog = new List<Catalog>();
                        if (catalogResp != null)
                        {
                            campCatalog = catalogResp.Result.ToList().FindAll(x => x.Id.Equals(campusId));
                        }
                        foreach (var lead in leads)
                        {
                            var leadBase = new LeadBase();
                            leadBase.FirstName = lead.FirstName.Trim();
                            leadBase.LastName = lead.LastName.Trim();
                            leadBase.SSN = lead.Ssn.Trim();
                            leadBase.CmsId = lead.LocationCMSID.Trim();
                            leadBase.AfaStudentId = lead.IDStudent;
                            var existResponse = await leadService.CheckIfAfaStudentIdExists(leadBase);
                            if (existResponse != null)
                            {
                                if (!existResponse.AfaStudentIdExists)
                                {
                                    // update adleads with the AFAStudentId & leadID given only if result status is 'AfaStudentIdNotFoundButLeadFound'
                                    if (existResponse.ResultStatusMessage.Equals(ApiMsgs.AfaStudentIdNotFoundButLeadFound))
                                    {
                                        leadBase.LeadId = existResponse.LeadId;
                                        await this.leadService.UpdateAfaStudentId(leadBase);
                                    }
                                    else if (existResponse.ResultStatusMessage.Equals(ApiMsgs.LeadNotFound) || existResponse.ResultStatusMessage.Equals(ApiMsgs.NewLeadCreatedWithDifferentAfaStudentId))
                                    {
                                        try
                                        {


                                            var newlead = new Lead();
                                            newlead.FirstName = lead.FirstName.Trim();
                                            newlead.LastName = lead.LastName.Trim();
                                            if (!lead.MiddleInitial.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.MiddleName = lead.MiddleInitial;
                                            }

                                            newlead.AfaStudentId = lead.IDStudent;
                                            newlead.CampusId = campusId;
                                            newlead.SSN = lead.Ssn.Trim();
                                            //check if the phone and email both doesnt exists then add default phone number 9999
                                            if (lead.Phone.IsNullOrWhiteSpaceOrEmpty() && lead.Email.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                lead.Phone = "9999999999";
                                                newlead.Phone = lead.Phone.Trim();
                                                newlead.IsUsaPhoneNumber = true;
                                                newlead.PhoneTypeId = campCatalog[0].PhoneTypes[0].Value;
                                            }
                                            else if (lead.Phone.IsNullOrWhiteSpaceOrEmpty() && !lead.Email.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                // do not do anything as we have email
                                            }
                                            else
                                            {
                                                newlead.IsUsaPhoneNumber = true;
                                                newlead.PhoneTypeId = campCatalog[0].PhoneTypes[0].Value;
                                                newlead.Phone = lead.Phone.Trim();
                                            }

                                            if (!lead.Address.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.AddressTypeId = campCatalog[0].AddressTypes[0].Value;
                                                newlead.Address1 = lead.Address.Trim();
                                                if (!lead.State.IsNullOrWhiteSpaceOrEmpty())
                                                {
                                                    // check for forign country.. FC means that
                                                    if (lead.State.Trim().Equals("FC"))
                                                    {
                                                        // need to add the code to have other state here and have adress as non USA address
                                                    }
                                                    else
                                                    {
                                                        newlead.StateId = campCatalog[0].States.FirstOrDefault(s => s.Text.ToLower().Equals(lead.State.Trim().ToLower())).Value;
                                                    }
                                                }
                                                newlead.City = lead.City.Trim();
                                                newlead.Zip = lead.ZipCode.Trim();
                                            }

                                            if (!lead.LicenseNumber.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.DriverLicenseNumber = lead.LicenseNumber;
                                            }

                                            if (!lead.LicenseState.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                // check for forign country.. FC means that
                                                if (lead.LicenseState.Trim().Equals("FC"))
                                                {
                                                    // need to add the code to have other state here and have adress as non USA address
                                                }
                                                else
                                                {
                                                    newlead.LicenseState = campCatalog[0].States.FirstOrDefault(s => s.Text.ToLower().Equals(lead.LicenseState.Trim().ToLower())).Value;
                                                }
                                            }

                                            if (!lead.Email.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.Email = lead.Email.Trim();
                                                newlead.EmailTypeId = campCatalog[0].EmailTypes[0].Value;
                                            }

                                            newlead.BirthDate = lead.DOB;
                                            if (!lead.CitizenshipStatus.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.CitizenShipStatus = campCatalog[0].CitizenShip.FirstOrDefault(c => c.AfaValue.Equals(lead.CitizenshipStatus.Trim())).Value;
                                            }

                                            if (!lead.MaritialStatus.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.MaritalStatus = campCatalog[0].MaritalStatus.FirstOrDefault(m => m.AfaValue.Equals(lead.MaritialStatus.Trim())).Value;
                                            }

                                            if (!lead.Gender.IsNullOrWhiteSpaceOrEmpty())
                                            {
                                                newlead.Gender = campCatalog[0].Genders.FirstOrDefault(g => g.AfaValue.Equals(lead.Gender.Trim())).Value;
                                            }
                                            // now create a lead
                                            await this.leadService.Create(newlead, true);
                                            // now insert in exception table if other reason
                                            if (existResponse.ResultStatusMessage.Equals(ApiMsgs.NewLeadCreatedWithDifferentAfaStudentId))
                                            {
                                                var exceptionMsg = new AFALeadSyncException();
                                                exceptionMsg.AfaStudentId = lead.IDStudent;
                                                exceptionMsg.LeadId = existResponse.LeadId;
                                                exceptionMsg.FirstName = lead.FirstName;
                                                exceptionMsg.LastName = lead.LastName;
                                                exceptionMsg.ExceptionReason = existResponse.ResultStatusMessage;
                                                await this.afaLeadSyncExceptionService.InsertAfaLeadSyncException(exceptionMsg);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            Debug.WriteLine(
                                                $"Code: {e.Message} - Reason: {e.Message}");
                                            var wapiLogger = new WapiOperationLogger();
                                            wapiLogger.Comment = $"Error in getting all Lead Demographics - Code-Create Lead: FName: {lead.FirstName} LName:{lead.LastName} ssn: {lead.Ssn} AfaId: {lead.IDStudent} state: {lead.State} - Reason: {e.Message}";
                                            wapiLogger.CompanyCode = "";
                                            wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                                            wapiLogger.DateExecution = DateTime.Now;
                                            wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                                            wapiLogger.IsError = true;
                                            await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                                        }
                                    }
                                    // for the rest of the cases insert into the exception table
                                    else if (existResponse.ResultStatusMessage.Equals(
                                                 ApiMsgs.DuplicateLeadExists)
                                             || existResponse.ResultStatusMessage.Equals(
                                                 ApiMsgs.AfaStudentIdNotFoundButLeadFoundNoSsn)
                                             || existResponse.ResultStatusMessage.Equals(
                                                 ApiMsgs.AfaStudentIdNotFoundButDuplicateLeadWithSsn)
                                             || existResponse.ResultStatusMessage.Equals(
                                                 ApiMsgs.AfaStudentIdNotFoundButLeadFoundDiffSsn))
                                    {
                                        var exceptionMsg = new AFALeadSyncException();
                                        exceptionMsg.AfaStudentId = lead.IDStudent;
                                        exceptionMsg.LeadId = existResponse.LeadId;
                                        exceptionMsg.FirstName = lead.FirstName;
                                        exceptionMsg.LastName = lead.LastName;
                                        exceptionMsg.ExceptionReason = existResponse.ResultStatusMessage;
                                        await this.afaLeadSyncExceptionService.InsertAfaLeadSyncException(exceptionMsg);
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        Debug.WriteLine(
                            $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                        var wapiLogger = new WapiOperationLogger();
                        wapiLogger.Comment = $"Error in getting all Lead Demographics - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                        wapiLogger.CompanyCode = "";
                        wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                        wapiLogger.DateExecution = DateTime.Now;
                        wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                        wapiLogger.IsError = true;
                        await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                    }

                    return leads;
                }
                else
                {
                    // throw error need the Session Key
                    return new List<AdvStagingDemographicsV1>();
                }
            }
            else
            {
                // throw exception saying only supposrt user can access.
                return new List<AdvStagingDemographicsV1>();
            }
        }

        public async Task<string> PostAllProgramVersionToAfa(IEnumerable<AdvStagingProgramV1> programVersions)
        {
            var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
            wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
            var afaSession = new AfaSession();
            afaSession.ClientKey = wapiSetting.ConsumerKey;
            afaSession.Password = wapiSetting.PrivateKey;
            afaSession.UserName = wapiSetting.UserName;

            afaSession = this.GetAfaSessionKey(afaSession);
            var route = wapiSetting.ExternalUrl + "/integration/sis//program/v1/post";
            string result = "";
            this.Client = new HttpClient();
            this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
            HttpResponseMessage response;
            foreach (var pv in programVersions)
            {
                var jsonInString = JsonConvert.SerializeObject(pv);

                var task = this.Client
                    .PostAsync(route, new StringContent(jsonInString, Encoding.UTF8, "application/json")).ContinueWith(
                        taskwithmsg =>
                            {
                                response = taskwithmsg.Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    result = taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString();
                                }
                                else
                                {
                                    Debug.WriteLine(
                                        $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                                    var wapiLogger = new WapiOperationLogger();
                                    wapiLogger.Comment = $"Error in posting all Program Version to Afa - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                                    wapiLogger.CompanyCode = "";
                                    wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                                    wapiLogger.DateExecution = DateTime.Now;
                                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                                    wapiLogger.IsError = true;
                                    this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                                }
                            });
                task.Wait();
            }

            return result;
        }

        public async Task<string> PostAllEnrollmentsToAfa(IEnumerable<AdvStagingEnrollmentV2> enrollments)
        {
            var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
            wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
            var afaSession = new AfaSession();
            afaSession.ClientKey = wapiSetting.ConsumerKey;
            afaSession.Password = wapiSetting.PrivateKey;
            afaSession.UserName = wapiSetting.UserName;

            afaSession = this.GetAfaSessionKey(afaSession);
            var route = wapiSetting.ExternalUrl + "/integration/sis//enrollment/v2/post";
            string result = "";
            this.Client = new HttpClient();
            this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
            foreach (var enrollUpdate in enrollments)
            {
                var jsonInString = JsonConvert.SerializeObject(new List<AdvStagingEnrollmentV2> { enrollUpdate });
                var mess = await this.Client.PostAsync(route,
                    new StringContent(jsonInString, Encoding.UTF8, "application/json"));
                result = await mess.Content.ReadAsStringAsync();
                if (!mess.IsSuccessStatusCode)
                {
                    Debug.WriteLine(
                        $"Code: {mess.StatusCode} - Reason: {mess.ReasonPhrase}");
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment = $"Error in posting all Program Version to Afa - Code: {mess.StatusCode} - Reason: {mess.ReasonPhrase}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return result;
        }

        public FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings GetWapiSettingsForAfa()
        {
            var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
            wapiSetting = this.wapiService.GetWapiSettings("AFA_INTEGRATION");

            return wapiSetting;
        }

        /// <summary>
        /// The get all payment periods.
        /// </summary>
        /// </param>
        /// <param name="campusId">
        /// The school campusId.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        public async Task<IList<AdvStagingPaymentPeriodV1>> GetAllPaymentPeriodsFromAfa(Guid campusId)
        {
            var cmsId = this.campusService.GetCmsIdForCampusId(campusId.ToString());

            var user = this.unitOfWork.GetTenantUser();
            if (user.IsSupportUser)
            {

                var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                var afaSession = new AfaSession();
                afaSession.ClientKey = wapiSetting.ConsumerKey;
                afaSession.Password = wapiSetting.PrivateKey;
                afaSession.UserName = wapiSetting.UserName;

                afaSession = this.GetAfaSessionKey(afaSession);
                this.Client = new HttpClient();
                this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                var uri = wapiSetting.ExternalUrl;
                uri = uri + "/integration/sis/PaymentPeriod/all/v1/get/" + cmsId;
                HttpResponseMessage response;
                List<AdvStagingPaymentPeriodV1> paymentPeriods = new List<AdvStagingPaymentPeriodV1>();
                var task = this.Client.GetAsync(uri).ContinueWith(
                    taskwithmsg =>
                    {
                        response = taskwithmsg.Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var paymentPeriodjson = response.Content.ReadAsStringAsync().Result;
                            paymentPeriods = JsonConvert.DeserializeObject<List<AdvStagingPaymentPeriodV1>>(paymentPeriodjson).OrderBy(a => a.SISEnrollmentID).ToList();
                        }
                        else
                        {
                            Debug.WriteLine(
                                $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                            var wapiLogger = new WapiOperationLogger();
                            wapiLogger.Comment = $"Error in getting all Payment Periods - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                            wapiLogger.CompanyCode = "";
                            wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                            wapiLogger.DateExecution = DateTime.Now;
                            wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                            wapiLogger.IsError = true;
                            this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                        }
                    });

                task.Wait();

                return paymentPeriods;
            }
            else
            {
                // throw exception saying only supposrt user can access.
                return new List<AdvStagingPaymentPeriodV1>();
            }
        }

        public async Task<bool> PostPaymentPeriodsToAdvantage(IEnumerable<AdvStagingPaymentPeriodV1> paymentPeriods)
        {
            var transformedPaymentPeriods = paymentPeriods.Select(pp => new PaymentPeriod()
            {
                AfaStudentId = pp.IDStudent,
                StudentEnrollmentID = pp.SISEnrollmentID,
                AcademicYearSeqNo = pp.AcadYearSeqNo,
                PaymentPeriodName = pp.PaymentPeriodName,
                StartDate = pp.StartDate,
                EndDate = pp.EndDate,
                WeeksOfInstructionalTime = pp.WeeksOfInstructionalTime,
                HoursCreditEnrolled = pp.HoursCreditEnrolled,
                EnrollmentStatusDescription = pp.EnrollmentStatusDescription,
                HoursCreditEarned = pp.HoursCreditEarned,
                EffectiveDate = pp.EffectiveDate,
                TitleIVSapResult = pp.SAP,
                DateCreated = pp.DateCreated,
                UserCreated = pp.UserCreated,
                DateUpdated = pp.DateUpdated,
                UserUpdated = pp.UserUpdated,
                DeleteRecord = pp.Deleted
            });
            var failedResults = await this.paymentPeriodService.UpdateAfaPaymentPeriodStaging(transformedPaymentPeriods);
            // log error in case of failure
            if (failedResults.Any())
            {
                foreach (var failRes in failedResults)
                {
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment = $"Error in updating Payment Periods in advantage staging table for enrollmentId : {failRes.Text}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = "";
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return !failedResults.Any();
        }
        /// <summary>
        /// The get all title iv disbursements.
        /// </summary>
        /// </param>
        /// <param name="campusId">
        /// The school campusId.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        public async Task<IList<AdvStagingDisbursementV1>> GetAllTitleIVDisbursementsFromAfa(Guid campusId)
        {
            //597000
            var cmsId = this.campusService.GetCmsIdForCampusId(campusId.ToString());

            var user = this.unitOfWork.GetTenantUser();
            if (user.IsSupportUser)
            {

                var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                var afaSession = new AfaSession();
                afaSession.ClientKey = wapiSetting.ConsumerKey;
                afaSession.Password = wapiSetting.PrivateKey;
                afaSession.UserName = wapiSetting.UserName;

                afaSession = this.GetAfaSessionKey(afaSession);
                this.Client = new HttpClient();
                this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                var uri = wapiSetting.ExternalUrl;
                uri = uri + "/integration/sis/disbursement/all/v1/get/" + cmsId;
                HttpResponseMessage response;
                List<AdvStagingDisbursementV1> disbursements = new List<AdvStagingDisbursementV1>();
                var task = this.Client.GetAsync(uri).ContinueWith(
                    taskwithmsg =>
                    {
                        response = taskwithmsg.Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var disbursementjson = response.Content.ReadAsStringAsync().Result;
                            disbursements = JsonConvert.DeserializeObject<List<AdvStagingDisbursementV1>>(disbursementjson).OrderBy(a => a.AwardID).ToList();
                        }
                        else
                        {
                            Debug.WriteLine(
                                $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                            var wapiLogger = new WapiOperationLogger();
                            wapiLogger.Comment = $"Error in getting all Disbursements - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                            wapiLogger.CompanyCode = "";
                            wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                            wapiLogger.DateExecution = DateTime.Now;
                            wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                            wapiLogger.IsError = true;
                            this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                        }
                    });

                task.Wait();
                return disbursements;

            }
            else
            {
                // throw exception saying only supposrt user can access.
                return new List<AdvStagingDisbursementV1>();
            }
        }

        /// <summary>
        /// The UpdateDisbursementTransactionReference
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDisbursementTransactionReference(Guid campusId)
        {
            var disbursements = await this.GetAllTitleIVDisbursementsFromAfa(campusId);

            var transformDis = disbursements.Select(
                                                       dis =>
                                                           new
                                                              Disbursement
                                                           {
                                                               CmsId = dis.LocationCMSID,
                                                               AfaStudentId =
                                                                           dis.IDStudent,
                                                               PaymentPeriodName =
                                                                           dis.PaymentPeriodName,
                                                               AnticipatedDisbursementDate =
                                                                           dis.AnticipatedDisbursementDate,
                                                               AnticipatedNetDisbursementAmt =
                                                                           dis
                                                                               .AnticipatedNetDisbursementAmt,
                                                               AnticipatedGrossAmount =
                                                                           dis.AnticipatedGrossAmount,
                                                               FeeAmount = dis.FeeAmount,
                                                               ActualDisbursementDate =
                                                                           dis.ActualDisbursementDate,
                                                               ActualNetDisbursementAmt =
                                                                           dis.ActualNetDisbursementAmt,
                                                               ActualFeeAmount =
                                                                           dis.ActualFeeAmount,
                                                               ActualGrossAmount =
                                                                           dis.ActualGrossAmount,
                                                               DisbursementStatus =
                                                                           dis.DisbursementStatus,
                                                               SequenceNumber =
                                                                           dis.DisbursementSequenceNumber,
                                                               DateCreated = dis.DateCreated,
                                                               UserCreated = dis.UserCreated,
                                                               ModDate = dis.DateUpdated,
                                                               ModUser = dis.UserUpdated,
                                                               FinancialAidId = dis.AwardID,
                                                               Deleted = dis.Deleted
                                                           });

            var result = (await this.studentAwardsService.UpdateDisbursementTransactionReference(transformDis)).Count == 0;

            return result;

        }
        /// <summary>
        /// The PostTitleIVDisbursementsToAdvantage
        /// </summary>
        /// <param name="disbursements"></param>
        /// <returns></returns>
        public async Task<bool> PostTitleIVDisbursementsToAdvantage(IEnumerable<AdvStagingDisbursementV1> disbursements)
        {

            var transformedDisbursements = disbursements.Select(dis => new Disbursement()
            {
                CmsId = dis.LocationCMSID,
                AfaStudentId = dis.IDStudent,
                AnticipatedDisbursementDate = dis.AnticipatedDisbursementDate,
                AnticipatedNetDisbursementAmt = dis.AnticipatedNetDisbursementAmt,
                AnticipatedGrossAmount = dis.AnticipatedGrossAmount,
                FeeAmount = dis.FeeAmount,
                ActualDisbursementDate = dis.ActualDisbursementDate,
                ActualNetDisbursementAmt = dis.ActualNetDisbursementAmt,
                ActualFeeAmount = dis.ActualFeeAmount,
                ActualGrossAmount = dis.ActualGrossAmount,
                PaymentPeriodEndDate = dis.PaymentPeriodEndDate,
                PaymentPeriodStartDate = dis.PaymentPeriodStartDate,
                PaymentPeriodName = dis.PaymentPeriodName,
                DisbursementStatus = dis.DisbursementStatus,
                SequenceNumber = dis.DisbursementSequenceNumber,
                DateCreated = dis.DateCreated,
                UserCreated = dis.UserCreated,
                ModDate = dis.DateUpdated,
                ModUser = dis.UserUpdated,
                FinancialAidId = dis.AwardID,
                Deleted = dis.Deleted
            });
            var failedResults = await this.studentAwardsService.CreateUpdateStudentDisbursements(transformedDisbursements);
            // log error in case of failure
            if (failedResults.Any())
            {
                foreach (var failRes in failedResults)
                {
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment = $"Error in CreateUpdateStudentDisbursements in advantage for enrollmentId : {failRes.Text}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = "";
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return !failedResults.Any();
        }

        /// <summary>
        /// The get all title iv awards.
        /// </summary>
        /// </param>
        /// <param name="campusId">
        /// The school campusId.
        /// </param>
        /// <returns>
        /// The <see >
        /// <cref>"IList"</cref>
        /// </see>
        /// </returns>
        public async Task<IList<AdvStagingAwardV1>> GetAllTitleIVAwardsFromAfa(Guid campusId)
        {
            var cmsId = this.campusService.GetCmsIdForCampusId(campusId.ToString());

            var user = this.unitOfWork.GetTenantUser();
            if (user.IsSupportUser)
            {
                var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                var afaSession = new AfaSession();
                afaSession.ClientKey = wapiSetting.ConsumerKey;
                afaSession.Password = wapiSetting.PrivateKey;
                afaSession.UserName = wapiSetting.UserName;

                afaSession = this.GetAfaSessionKey(afaSession);
                this.Client = new HttpClient();
                this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                var uri = wapiSetting.ExternalUrl;
                uri = uri + "/integration/sis/award/all/v1/get/" + cmsId;
                HttpResponseMessage response;
                List<AdvStagingAwardV1> awards = new List<AdvStagingAwardV1>();
                var task = this.Client.GetAsync(uri).ContinueWith(
                    taskwithmsg =>
                    {
                        response = taskwithmsg.Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var awardjson = response.Content.ReadAsStringAsync().Result;
                            awards = JsonConvert.DeserializeObject<List<AdvStagingAwardV1>>(awardjson).OrderBy(a => a.SISEnrollmentID).ToList();
                        }
                        else
                        {
                            Debug.WriteLine(
                                $"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                            var wapiLogger = new WapiOperationLogger();
                            wapiLogger.Comment = $"Error in getting all TitleIV Awards from Afa - Code: {response.StatusCode} - Reason: {response.ReasonPhrase}";
                            wapiLogger.CompanyCode = "";
                            wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                            wapiLogger.DateExecution = DateTime.Now;
                            wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                            wapiLogger.IsError = true;
                            this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                        }
                    });

                task.Wait();


                return awards;
            }
            else
            {
                // throw exception saying only supposrt user can access.
                return new List<AdvStagingAwardV1>();
            }
        }
        public async Task<bool> PostTitleIVAwardsToAdvantage(IEnumerable<AdvStagingAwardV1> awards)
        {
            var transformedAwards = awards.Select(sa => new StudentAwards()
            {
                AfaStudentId = sa.IDStudent.ToString(),
                StudentEnrollmentId = sa.SISEnrollmentID,
                AwardEndDate = sa.AcadYrEnd,
                AwardStartDate = sa.AcadYrStart,
                AwardStatus = sa.AwardStatus,
                AwardYear = sa.AwardYear,
                TitleIvAwardType = sa.FundCode,
                LoanId = sa.LoanID,
                ModDate = sa.DateUpdated,
                ModUser = sa.UserUpdated,
                FinancialAidId = sa.AwardID,
                Deleted = sa.Deleted
            });
            var failedResults = await this.studentAwardsService.CreateUpdateStudentAwards(transformedAwards);
            // log error in case of failure
            if (failedResults.Any())
            {
                foreach (var failRes in failedResults)
                {
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment =
                        $"Error in CreateUpdateStudentAwards in advantage for enrollmentId : {failRes.Text}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = "";
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return !failedResults.Any();
        }

        public async Task<bool> SyncAwardsToAdvantage(IEnumerable<AdvStagingAwardV1> awards)
        {
            var transformedAwards = awards.Select(sa => new StudentAwards()
            {
                AfaStudentId = sa.IDStudent.ToString(),
                StudentEnrollmentId = sa.SISEnrollmentID,
                AwardEndDate = sa.AcadYrEnd,
                AwardStartDate = sa.AcadYrStart,
                AwardStatus = sa.AwardStatus,
                AwardYear = sa.AwardYear,
                TitleIvAwardType = sa.FundCode,
                LoanId = sa.LoanID,
                ModDate = sa.DateUpdated,
                ModUser = sa.UserUpdated,
                FinancialAidId = sa.AwardID,
                Deleted = sa.Deleted
            });
            var failedResults = await this.studentAwardsService.SyncAllStudentAwards(transformedAwards);
            // log error in case of failure
            if (failedResults.Any())
            {
                foreach (var failRes in failedResults)
                {
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment =
                        $"Error in SyncAllStudentAwards for enrollmentId : {failRes.Text}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = "";
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return !failedResults.Any();
        }

        public async Task<bool> SyncDisbursementsToAdvantage(IEnumerable<AdvStagingDisbursementV1> disbursements)
        {
            var transformedDisbursements = disbursements.Select(sd => new DataTransferObjects.StudentAccounts.AFA.Disbursement()
            {
                AfaStudentId = sd.IDStudent,
                SequenceNumber = sd.DisbursementSequenceNumber,
                ModDate = sd.DateUpdated,
                ModUser = sd.UserUpdated,
                FinancialAidId = sd.AwardID,
                Deleted = sd.Deleted
            });
            var failedResults = await this.studentAwardsService.SyncAllDisbursements(transformedDisbursements);
            // log error in case of failure
            if (failedResults.Any())
            {
                foreach (var failRes in failedResults)
                {
                    var wapiLogger = new WapiOperationLogger();
                    wapiLogger.Comment =
                        $"Error in SyncAllDisbursements for AwardID : {failRes.Text}";
                    wapiLogger.CompanyCode = "";
                    wapiLogger.ServiceCode = "";
                    wapiLogger.DateExecution = DateTime.Now;
                    wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                    wapiLogger.IsError = true;
                    await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                }
            }
            return !failedResults.Any();
        }
        //public FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings GetWapiSettingsForAfa()
        //{
        //    var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
        //    wapiSetting = this.wapiService.GetWapiSettings("AFA_INTEGRATION");

        //    return wapiSetting;
        //}

        //public async Task<string> PostAllProgramVersionToAfa(IEnumerable<AdvStagingProgramV1> programVersions)
        //{
        //    var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
        //    wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
        //    var afaSession = new AfaSession();
        //    afaSession.ClientKey = wapiSetting.ConsumerKey;
        //    afaSession.Password = wapiSetting.PrivateKey;
        //    afaSession.UserName = wapiSetting.UserName;

        //    afaSession = this.GetAfaSessionKey(afaSession);
        //    var route = wapiSetting.ExternalUrl + "/integration/sis//program/v1/post";
        //    string result = "";
        //    this.Client = new HttpClient();
        //    this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
        //    foreach (var pv in programVersions)
        //    {
        //        var jsonInString = JsonConvert.SerializeObject(pv);

        //        var task = this.Client
        //            .PostAsync(route, new StringContent(jsonInString, Encoding.UTF8, "application/json")).ContinueWith(
        //                taskwithmsg =>
        //                    {
        //                        result = //JsonConvert.DeserializeObject<AdvStagingProgramV1>(
        //                            taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString(); //);
        //                    });
        //        task.Wait();
        //    }

        //    return result;
        //}


        public IEnumerable<Guid> GetAfaStudentEnrollmentsForCampus(Guid campusId)
        {
            return this.enrollmentService.GetEnrollmentsByCampusForAFA(campusId);
        }
        public async Task<IEnumerable<AdvStagingAttendanceV1>> CalculateAttendanceUpdatesForEnrollments(IEnumerable<Guid> studentEnrollmentIds)
        {
            return await this.paymentPeriodService.CalculateCumulativePaymentPeriodAttendance(studentEnrollmentIds);
        }
        public async Task<AdvStagingAttendanceV1> CalculateAttendanceUpdatesForEnrollment(Guid studentEnrollmentId)
        {
            return await this.paymentPeriodService.CalculateCumulativePaymentPeriodAttendance(studentEnrollmentId);
        }
        public async Task<IEnumerable<AdvStagingAttendanceV1>> CalculateAllAttendanceUpdatesForCampus(Guid campusId)
        {
            return await this.paymentPeriodService.CalculatePaymentPeriodAttendanceByCampusForAFA(campusId);
        }
        public async Task<AFAEnrollmentDetails> GetAfaEnrollmentDetails(Guid enrollmentId)
        {
            return (await this.enrollmentService.GetAfaStudentEnrollmentDetails(enrollmentId)).Result;
        }
        public async Task<IEnumerable<AFAEnrollmentDetails>> GetAfaEnrollmentDetails(IEnumerable<Guid> enrollmentIds)
        {
            return (await this.enrollmentService.GetAfaStudentEnrollmentDetails(enrollmentIds)).Result;
        }
        public async Task UpdateEnrollmentsStatus(string cmsId)
        {
            var campusId = campusService.GetCampusIdForCmsId(cmsId);

            if (campusService.IsAFAIntegrationEnabled(campusId))
            {

                var advStagingEnrollments = await this.enrollmentService.GetEnrollmentStatusChangesForAFA(campusId);
                if (advStagingEnrollments.Any())
                {
                    var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                    wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                    var afaSession = new AfaSession();
                    afaSession.ClientKey = wapiSetting.ConsumerKey;
                    afaSession.Password = wapiSetting.PrivateKey;
                    afaSession.UserName = wapiSetting.UserName;

                    afaSession = this.GetAfaSessionKey(afaSession);
                    var route = wapiSetting.ExternalUrl + "/integration/sis/enrollment/v2/post";
                    string result = "";
                    this.Client = new HttpClient();
                    this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                    foreach (var enrollment in advStagingEnrollments)
                    {
                       
                        if (enrollment.RetryCount < this.integrationSettings.MaximumRetryCount)
                        {
                            //api call only supports receiving list but i want to keep processing 1 by 1 for now
                            var jsonInString = JsonConvert.SerializeObject(new List<AdvStagingEnrollmentV2> { enrollment.Enrollment });

                            var mess = await this.Client
                            .PostAsync(route, new StringContent(jsonInString, Encoding.UTF8, "application/json"));
                            if (mess.IsSuccessStatusCode)
                            {
                                var processed = await this.enrollmentService.MarkEnrollmentChangeAsProcessed(enrollment.StuEnrollId, enrollment.ModifiedDate);

                                if (!processed)
                                {
                                    this.enrollmentService.IncrementRetryCount(enrollment.StuEnrollId);
                                }
                            }
                            else
                            {
                                this.enrollmentService.IncrementRetryCount(enrollment.StuEnrollId);
                                Debug.WriteLine(
                                    $"Code: {mess.StatusCode} - Reason: {mess.ReasonPhrase}");
                                var wapiLogger = new WapiOperationLogger();
                                wapiLogger.Comment = $"Error in updating enrollment status to AFA - Code: {mess.StatusCode} - Reason: {mess.ReasonPhrase}";
                                wapiLogger.CompanyCode = "";
                                wapiLogger.ServiceCode = wapiSetting.CodeOperation;
                                wapiLogger.DateExecution = DateTime.Now;
                                wapiLogger.NextPlanningDate = DateTime.Now.AddMinutes(1);
                                wapiLogger.IsError = true;
                                await this.wapiOperationLoggerService.InsertSyWapiOperationLogger(wapiLogger);
                            }
                        }

                    }

                }

            }


        }
        public async Task<string> PostAttendanceUpdatesToAfa(IEnumerable<AdvStagingAttendanceV1> attendanceUpdates)
        {

            if (attendanceUpdates.Any())
            {
                var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                var afaSession = new AfaSession();
                afaSession.ClientKey = wapiSetting.ConsumerKey;
                afaSession.Password = wapiSetting.PrivateKey;
                afaSession.UserName = wapiSetting.UserName;

                afaSession = this.GetAfaSessionKey(afaSession);
                var route = wapiSetting.ExternalUrl + "/integration/sis/attendanceupdate/v1/post";
                string result = "";
                this.Client = new HttpClient();
                this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                foreach (var attendanceUpdate in attendanceUpdates)
                {
                    var jsonInString = JsonConvert.SerializeObject(attendanceUpdate);

                    var mess = await this.Client
                        .PostAsync(route, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

                    result = await mess.Content.ReadAsStringAsync();

                }

                return result;
            }
            return String.Empty;

        }

        public async Task<string> PostAllRefundsToAfa(Guid campusId)
        {
            var refunds = (await this.studentAwardsService.GetTitleIVRefundsForAFA(campusId)).ToList();
            if (refunds.Any())
            {
                var wapiSetting = new FAME.Advantage.RestApi.DataTransferObjects.Maintenance.Wapi.WapiSettings();
                wapiSetting = await Task.Run(() => this.GetWapiSettingsForAfa());
                var afaSession = new AfaSession();
                afaSession.ClientKey = wapiSetting.ConsumerKey;
                afaSession.Password = wapiSetting.PrivateKey;
                afaSession.UserName = wapiSetting.UserName;

                afaSession = this.GetAfaSessionKey(afaSession);
                var route = wapiSetting.ExternalUrl + "/integration/sis/refund/v1/post";
                string result = "";
                this.Client = new HttpClient();
                this.Client.DefaultRequestHeaders.Add("SessionKey", afaSession.SessionKey);
                foreach (var pv in refunds)
                {
                    var jsonInString = JsonConvert.SerializeObject(pv);

                    var task = this.Client
                        .PostAsync(route, new StringContent(jsonInString, Encoding.UTF8, "application/json")).ContinueWith(
                            taskwithmsg =>
                            {
                                result = //JsonConvert.DeserializeObject<AdvStagingProgramV1>(
                                    taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString(); //);
                            });
                    task.Wait();
                }

                return result;
            }
            return String.Empty;
        }

        /// <summary>
        /// Attempts to turn wapi service on if AFA Integration is enabled for any campus.
        /// </summary>
        /// <returns>
        /// The <see cref="ServiceControllerStatus"/>.
        /// </returns>
        public async Task<bool> TurnWapiServiceOnIfIntegrationIsEnabled()
        {
            try
            {
                var cmsIdsToSync = await this.campusService.GetCmsIdsToSync();
                var afaIntegrationIsEnabled = cmsIdsToSync != null && cmsIdsToSync.Any();
                var serviceIsOff = (this.wapiService.GetWapiServiceStatus() == ServiceControllerStatus.Stopped);

                if (serviceIsOff && afaIntegrationIsEnabled)
                {
                    this.wapiService.TurnWapiServiceOn();
                }

                return true;
            }
            catch (Exception e)
            {
                try
                {
                    var wapiServiceStatus = Enum.GetName(typeof(ServiceControllerStatus), this.wapiService.GetWapiServiceStatus());
                    var user = this.unitOfWork.GetTenantUser();
                    var fromEmail = this.appSettingService.GetAppSettingValue(EmailServiceConfigurationKeys.FromEmailAddress);
                    await this.emailService.SendEmailToSupportUsers(new EmailBase()
                    {
                        Body = $"Unable to start windows service for AFA Integration. Status: {wapiServiceStatus}, Tenant: {user.TenantName}, Exception: { e.Message}",
                        From = fromEmail,
                        Subject = $"Support: Failed to start wapi service for {user.TenantName}",
                        UserTypeCode = ""
                    });
                }
                catch (Exception x)
                {
                    x.TrackException();
                }

                e.TrackException();
                return false;
            }
        }
    }
}
