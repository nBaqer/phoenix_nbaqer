﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaLeadSyncExceptionService.cs" company=Fame Inc"">
//   2018
// </copyright>
// <summary>
//   Defines the AfaLeadSyncExceptionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AFA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;

    /// <summary>
    /// The AFA lead synchronization exception service.
    /// </summary>
    public class AfaLeadSyncExceptionService : IAfaLeadSyncExceptionService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AfaLeadSyncExceptionService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AfaLeadSyncExceptionService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The AFA lead sync exception repository.
        /// </summary>
        private IRepository<AfaLeadSyncException> AfaLeadSyncExceptionRepository => this.context.GetRepositoryForEntity<AfaLeadSyncException>();

        /// <summary>
        /// The insert AFA lead sync exception.
        /// </summary>
        /// <param name="record">
        /// The record.
        /// </param>
        /// <returns>
        /// The list of the exceptions.
        /// </returns>
        public async Task<IList<AfaLeadSyncException>> InsertAfaLeadSyncException(DataTransferObjects.AFA.AFALeadSyncException record)
        {
            var result = new List<AfaLeadSyncException>();
            if (record != null)
            {
                try
                {
                    record.Id = Guid.NewGuid();
                    await this.AfaLeadSyncExceptionRepository.CreateAsync(this.mapper.Map<AfaLeadSyncException>(record));
                    this.AfaLeadSyncExceptionRepository.Save();
                    result = this.AfaLeadSyncExceptionRepository.Get().ToList();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            return result;
        }

        public async Task<IList<AfaLeadSyncException>> GetAfaLeadSyncException()
        {
            return await Task.Run(() =>
                {
                    var result = this.AfaLeadSyncExceptionRepository.Get().ToList();
                    return result;
                });
            
        }
    }
}
