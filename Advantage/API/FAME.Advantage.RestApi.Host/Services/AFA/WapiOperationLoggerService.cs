﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationLoggerService.cs" company=Fame Inc"">
//   2018
// </copyright>
// <summary>
//   Defines the AfaLeadSyncExceptionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AFA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;
    /// <summary>
    /// The WapiOperationLoggerService
    /// </summary>
    public class WapiOperationLoggerService : IWapiOperationLoggerService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="WapiOperationLoggerService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public WapiOperationLoggerService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        private IRepository<SyWapiOperationLogger> syWapiAllowedServicesRepository => this.context.GetRepositoryForEntity<SyWapiOperationLogger>();
        /// <summary>
        /// InsertSyWapiOperationLogger
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        public async Task<bool> InsertSyWapiOperationLogger(DataTransferObjects.AFA.WapiOperationLogger record)
        {
            var result = false;
            if (record != null)
            {
                try
                {
                    await this.syWapiAllowedServicesRepository.CreateAsync(this.mapper.Map<SyWapiOperationLogger>(record));
                    this.syWapiAllowedServicesRepository.Save();
                    result = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    result = false;
                    throw;
                }
            }

            return result;
        }
    }
}
