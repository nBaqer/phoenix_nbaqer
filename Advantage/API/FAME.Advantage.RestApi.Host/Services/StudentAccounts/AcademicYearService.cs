﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicYearService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AcademicYearService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.Host.Extensions;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using FAME.Extensions.Helpers;

namespace FAME.Advantage.RestApi.Host.Services.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;

    using Microsoft.EntityFrameworkCore;

    using Orm.Advantage.Domain.Common;

    /// <summary>
    /// The academic year service.
    /// </summary>
    public class AcademicYearService : IAcademicYearService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AcademicYearService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus group service
        /// </param>
        public AcademicYearService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService, ICampusGroupService campusGroupService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.statusService = statusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The student awards repository.
        /// </summary>
        private IRepository<SaAcademicYears> AcademicYearRepository => this.unitOfWork.GetRepositoryForEntity<SaAcademicYears>();

        /// <summary>
        /// The student enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.unitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;
        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();
        private Guid CampusGroupAll => Task.Run(() => this.campusGroupService.GetCampusGroupAllId()).Result;

        /// <summary>
        /// Get Academic year id using the given code.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="campusId"></param>
        /// <returns>Academic year id</returns>
        public async Task<Guid> GetByAFACode(string code, Guid campusId)
        {
            var dash = code.IndexOf('-');
            if (dash > 0)
            {
                code = code.Substring(0, dash);
            }

            var descript = int.TryParse(code, out var res) ? code + "/" + (res + 1) : code;

            var academicYear = await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:AcademicYearByCode:{code}:{campusId}",
                () => (this.AcademicYearRepository.Get(ay => ay.AcademicYearCode == code && ay.AcademicYearDescrip == descript && ay.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId))).FirstOrDefault(),
                CacheExpiration.SHORT));

            Guid academicYearId = Guid.Empty;
            if (academicYear != null)
            {
                academicYearId = academicYear.AcademicYearId;
                if (academicYear.StatusId != this.ActiveId)
                {
                    academicYear.StatusId = this.ActiveId;
                    this.AcademicYearRepository.Update(academicYear);
                    this.AcademicYearRepository.Save();
                }

            }

            else
            {
                var temp = (await this.AcademicYearRepository.CreateAsync(new SaAcademicYears()
                {
                    AcademicYearCode = code,
                    StatusId = ActiveId,
                    AcademicYearDescrip = descript,
                    CampGrpId = CampusGroupAll,
                    ModUser = this.unitOfWork.GetTenantUser().Email,
                    ModDate = DateTime.Now
                }));

                this.AcademicYearRepository.Save();
                academicYearId = temp.AcademicYearId;
                SimpleCache.Set($"{this.CacheKeyFirstSegment}:AcademicYearByCode:{code}:{campusId}", academicYearId);

            }

            return academicYearId;
        }

        /// <summary>
        /// Get Academic year ids using the given code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns>Academic year id</returns>
        public async Task<IEnumerable<Guid>> GetYearsByAFACode(string code)
        {
            var dash = code.IndexOf('-');
            if (dash > 0)
            {
                code = code.Substring(0, dash);
            }

            var academicYears = await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:AcademicYearsByCode:{code}",
                () => (this.AcademicYearRepository.Get(ay => ay.AcademicYearCode == code)).Select(ay => ay.AcademicYearId).ToList(),
                CacheExpiration.SHORT));

            return academicYears;
        }
    }
}