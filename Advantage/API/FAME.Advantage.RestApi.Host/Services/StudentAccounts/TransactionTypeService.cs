﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransactionTypeService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TransactionTypeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.Host.Extensions;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using FAME.Extensions.Helpers;

namespace FAME.Advantage.RestApi.Host.Services.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;

    using Microsoft.EntityFrameworkCore;

    using Orm.Advantage.Domain.Common;

    /// <summary>
    /// The transaction type service.
    /// </summary>
    public class TransactionTypeService : ITransactionTypeService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionTypeService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service.
        /// </param>

        public TransactionTypeService(IAdvantageDbContext unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;

        }

        /// <summary>
        /// The transaction type repository.
        /// </summary>
        private IRepository<SaTransTypes> TransactionTypeRepository => this.unitOfWork.GetRepositoryForEntity<SaTransTypes>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();
        public async Task<int> GetIdByDescription(string description)
        {

            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:TransactionTypeByDescription:{description}",
                () => TransactionTypeRepository.Get(tt => tt.Description == description).Select(_ => _.TransTypeId)
                        .FirstOrDefault(),
                CacheExpiration.VERY_LONG));
        }
    }
}
