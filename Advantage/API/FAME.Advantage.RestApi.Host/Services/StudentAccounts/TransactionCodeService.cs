﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransactionCodeService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the TransactionCodeService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Fame.EFCore.Advantage.Entities;
using Fame.EFCore.Interfaces;
using FAME.Advantage.RestApi.DataTransferObjects.Common;
using FAME.Advantage.RestApi.Host.Infrastructure;
using FAME.Advantage.RestApi.Host.Services.Interfaces;
using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using FAME.Extensions.Helpers;

namespace FAME.Advantage.RestApi.Host.Services.StudentAccounts
{
    public class TransactionCodeService : ITransactionCodeService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransactionTypeService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service.
        /// </param>
        public TransactionCodeService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.statusService = statusService;
        }
        /// <summary>
        /// The transaction code repository.
        /// </summary>
        private IRepository<SaTransCodes> TransactionCodeRepository => this.unitOfWork.GetRepositoryForEntity<SaTransCodes>();
        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// Get the active transaction codes for charging methods
        /// </summary>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetTransactionCodeForChargingMethod(Guid campusId)
        {
            return await Task.Run(
                () =>
                {
                    List<int> lstSysCodeId = new List<int>(new int[12] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 17, 20 });
                    var result = SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:ChargingTransactionCode:{campusId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                            this.TransactionCodeRepository.Get(tc => tc.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId) && tc.StatusId == this.ActiveStatusId &&
                                                                     (tc.SysTransCodeId.HasValue ? lstSysCodeId.Contains(tc.SysTransCodeId.Value) : true))).OrderBy(x => x.Text),
                        CacheExpiration.VERY_LONG);
                    return result;
                });
        }
    }
}
