﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwardsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentAwardsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.AFA;
using FAME.Advantage.RestApi.Host.Extensions;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace FAME.Advantage.RestApi.Host.Services.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Tools;
    using Microsoft.EntityFrameworkCore;

    using Orm.Advantage.Domain.Common;

    /// <summary>
    /// The student awards service.
    /// </summary>
    public class StudentAwardsService : IStudentAwardsService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The logger service
        /// </summary>
        private readonly ILoggerService logger;

        private IAcademicYearService academicYearService;

        private IFundSourceService fundSourceService;

        private ITransactionTypeService transactionTypeService;

        /// <summary>
        /// The payment period service.
        /// </summary>
        private IPaymentPeriodService paymentPeriodService;

        /// <summary>
        /// The payment period service.
        /// </summary>
        private ITermService termService;

        /// <summary>
        /// The logging flag
        /// </summary>
        private bool logging = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAwardsService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service.
        /// </param>
        public StudentAwardsService(IAdvantageDbContext unitOfWork, IMapper mapper, IAcademicYearService academicYearService, IFundSourceService fundSourceService, ITransactionTypeService transactionTypeService, IPaymentPeriodService paymentPeriodService, ILoggerService logger,ITermService termService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.academicYearService = academicYearService;
            this.fundSourceService = fundSourceService;
            this.transactionTypeService = transactionTypeService;
            this.paymentPeriodService = paymentPeriodService;
            this.logger = logger;
            this.termService = termService;
            unitOfWork.GetSimpleCacheConnectionString();
        }

        /// <summary>
        /// The student awards repository.
        /// </summary>
        private IRepository<FaStudentAwards> StudentAwardsRepository => this.unitOfWork.GetRepositoryForEntity<FaStudentAwards>();
        /// <summary>
        /// The student disbursements repository.
        /// </summary>
        private IRepository<FaStudentAwardSchedule> StudentDisbursementsRepository => this.unitOfWork.GetRepositoryForEntity<FaStudentAwardSchedule>();
        private IRepository<SaTransactions> TransactionsRepository => this.unitOfWork.GetRepositoryForEntity<SaTransactions>();
        private IRepository<SaPmtDisbRel> PaymentDisbursementsRepository => this.unitOfWork.GetRepositoryForEntity<SaPmtDisbRel>();
        private IRepository<SaRefunds> RefundsRepository => this.unitOfWork.GetRepositoryForEntity<SaRefunds>();

        /// <summary>
        /// The student enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.unitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.unitOfWork.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// Used for initial sync of student awards for AFA integration
        /// </summary>
        /// <param name="studentAwards"></param>
        /// <returns></returns>
        public async Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>> SyncAllStudentAwards(IEnumerable<DataTransferObjects.StudentAccounts.AFA.StudentAwards> studentAwards)
        {
            var distinctFundCodes = studentAwards.Select(sa => sa.TitleIvAwardType).Distinct();
            var saGs = studentAwards
                .GroupBy(pp => pp.StudentEnrollmentId)
                .Select(pp => new { EnrollmentId = pp.Key, StudentAwards = pp.GroupBy(x => x.FinancialAidId, (key, g) => g.OrderByDescending(e => e.ModDate).FirstOrDefault()).ToList() })
                .ToList();
            var failedResults = new List<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>();
            if (saGs.Any())
            {

                foreach (var saG in saGs)
                {
                    if (StudentEnrollmentRepository.Get(se => se.StuEnrollId == saG.EnrollmentId).Any())
                    {

                        var campusId = StudentEnrollmentRepository.Get(se => se.StuEnrollId == saG.EnrollmentId).FirstOrDefault().CampusId;
                        var enrStudentAwards = StudentAwardsRepository
                            .Get(sa => sa.StuEnrollId == saG.EnrollmentId);
                        var processed = new List<string>();
                        if (enrStudentAwards.Any())
                        {
                            foreach (var studentAward in saG.StudentAwards)
                            {
                                try
                                {
                                    var afaAwardType = studentAward
                                        .TitleIvAwardType.Trim();

                                    var academicYears = (await
                                             this.academicYearService.GetYearsByAFACode(studentAward.AwardYear)).ToList();

                                    var sysFundSource =
                                        this.fundSourceService.MapAfaAwardTypeToSystemFundSources(afaAwardType);

                                    var awardType = this.fundSourceService.MapSystemFundSourceToAwardType(sysFundSource);
                                    var fundSourceId =
                                        await this.fundSourceService.GetByAFAAwardType(
                                            afaAwardType, campusId);
                                    if (academicYears.Any() && !fundSourceId.IsEmpty())
                                    {

                                        var existingAwards =
                                   enrStudentAwards.Where(esa => academicYears.Contains(esa.AcademicYearId.Value) && esa.AwardTypeId == fundSourceId).ToList();

                                        if (existingAwards.Any())
                                        {
                                            FaStudentAwards existingAward = null;
                                            if (existingAwards.Count > 1)
                                            {
                                                existingAward = existingAwards.Where(ea => ea.AwardStartDate == studentAward.AwardStartDate).SingleOrDefault();
                                            }
                                            else
                                            {
                                                existingAward = existingAwards.SingleOrDefault();

                                            }
                                            if (existingAward != null)
                                            {
                                                existingAward.LoanId = awardType == Constants.AwardTypes.Loan ? studentAward.LoanId.Trim() : string.Empty;
                                                existingAward.FaId = studentAward.FinancialAidId.Trim();
                                                existingAward.ModDate = DateTime.Now;
                                                existingAward.ModUser = "Integration";
                                                StudentAwardsRepository.Update(existingAward);
                                                processed.Add(studentAward.FinancialAidId);
                                                StudentAwardsRepository.Save();

                                            }
                                            else
                                            {
                                                var failedRes = failedResults.Where(fr => fr.Text == saG.EnrollmentId).FirstOrDefault();
                                                if (failedRes == null)
                                                {
                                                    failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = new List<DataTransferObjects.StudentAccounts.AFA.StudentAwards> { studentAward } });
                                                }
                                                else
                                                {
                                                    failedRes.Value.Add(studentAward);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var failedRes = failedResults.Where(fr => fr.Text == saG.EnrollmentId).FirstOrDefault();
                                            if (failedRes == null)
                                            {
                                                failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = new List<DataTransferObjects.StudentAccounts.AFA.StudentAwards> { studentAward } });
                                            }
                                            else
                                            {
                                                failedRes.Value.Add(studentAward);
                                            }
                                        }

                                    }
                                    else
                                    {
                                        var failedRes = failedResults.Where(fr => fr.Text == saG.EnrollmentId).FirstOrDefault();
                                        if (failedRes == null)
                                        {
                                            failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = new List<DataTransferObjects.StudentAccounts.AFA.StudentAwards> { studentAward } });
                                        }
                                        else
                                        {
                                            failedRes.Value.Add(studentAward);
                                        }
                                    }

                                }
                                catch (Exception e)
                                {
                                    var failedRes = failedResults.Where(fr => fr.Text == saG.EnrollmentId).FirstOrDefault();
                                    if (failedRes == null)
                                    {
                                        failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = new List<DataTransferObjects.StudentAccounts.AFA.StudentAwards> { studentAward } });
                                    }
                                    else
                                    {
                                        failedRes.Value.Add(studentAward);
                                    }

                                }
                            }
                        }

                        StudentAwardsRepository.Save();
                    }

                    else
                    {
                        failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = saG.StudentAwards });
                    }

                }
            }
            if (failedResults.Any() && logging)
            {
                try
                {
                    var campusName = "Default";

                    if (saGs.Any())
                    {
                        var campus = StudentEnrollmentRepository.Get(se => se.StuEnrollId == saGs.First().EnrollmentId).Include(enr => enr.Campus).FirstOrDefault()?.Campus;

                        if (campus != null)
                        {
                            campusName = campus.CampDescrip.Trim();
                        }

                    }

                    var sb = new StringBuilder();
                    sb.AppendLine("Summary of Failed Results for Sync Awards");
                    sb.AppendLine("Run Date: " + DateTime.Now);
                    sb.AppendLine("Distinct Enrollment Ids: ");
                    sb.AppendLine(string.Join(",", failedResults.Select(fr => "\'" + fr.Text + "\'")));
                    var awardCount = 0;
                    foreach (var result in failedResults)
                    {
                        sb.AppendLine("------------------------------------------------------------------------");

                        sb.AppendLine("Enrollment Id: " + result.Text);
                        foreach (var studentAward in result.Value)
                        {
                            sb.AppendLine("FA_Id: " + studentAward.FinancialAidId);
                            sb.AppendLine("Award Type: " + studentAward
                                                .TitleIvAwardType.Trim());
                            sb.AppendLine("Award Year: " + studentAward.AwardYear);
                            sb.AppendLine("Academic Year Start: " + studentAward.AwardStartDate);
                            sb.AppendLine("Academic Year End: " + studentAward.AwardEndDate);

                            awardCount++;
                        }
                        sb.AppendLine("------------------------------------------------------------------------");
                    }
                    sb.AppendLine("Total awards not synced: " + awardCount);
                    logger.LogMessageToFile(sb.ToString(), "SyncAllAwardsLog.txt", $"IntegrationLogs\\{campusName}\\Awards\\");
                }
                catch (Exception e)
                {

                }
            }

            return failedResults;
        }

        /// <summary>
        /// Used for initial sync of disbursements for AFA integration
        /// </summary>
        /// <remarks>
        /// Currently not in use due to disbursement sequence numebers being used as is from Freedom -> Advantage Conversion
        /// </remarks>
        /// <param name="disbursements"></param>
        /// <returns></returns>
        public async Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> SyncAllDisbursements(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements)
        {
            var currStudentAward = Guid.Empty;

            var failedResults = new List<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>();
            var studentAwardIds = disbursements.Select(dis => dis.FinancialAidId).Distinct();
            var studentAwards = StudentAwardsRepository.Get(sa => sa.FaId != null && studentAwardIds.Contains(sa.FaId))
                .Include(sa => sa.StuEnroll).Include(sa => sa.FaStudentAwardSchedule).Include(sa => sa.AwardType).ToList();

            if (studentAwards.Any())
            {
                try
                {
                    foreach (var studentAward in studentAwards)
                    {
                        if (studentAward != null)
                        {
                            currStudentAward = studentAward.StudentAwardId;
                            var studentAwardDisbursements = studentAward.FaStudentAwardSchedule.OrderBy(sas => sas.ExpectedDate).ToList();
                            if (studentAwardDisbursements.Any())
                            {
                                var i = 1;
                                foreach (var disbursement in studentAwardDisbursements)
                                {

                                    disbursement.SequenceNumber = (byte)i;

                                    StudentDisbursementsRepository.Update(disbursement);

                                    StudentDisbursementsRepository.Save();

                                    i++;
                                }

                            }


                        }

                    }


                }
                catch (Exception e)
                {
                    failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = currStudentAward, Value = new List<DataTransferObjects.StudentAccounts.AFA.Disbursement>() });
                }

            }

            else
            {
                failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = Guid.Empty, Value = new List<DataTransferObjects.StudentAccounts.AFA.Disbursement>() });
            }


            return failedResults;
        }



        /// <summary>
        /// Create or update student awards received from AFA
        /// </summary>
        /// <param name="studentAwards"></param>
        /// <returns></returns>
        public async Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>> CreateUpdateStudentAwards(IEnumerable<DataTransferObjects.StudentAccounts.AFA.StudentAwards> studentAwards)
        {
            var saGs = studentAwards
                .GroupBy(pp => pp.StudentEnrollmentId)
                .Select(pp => new { EnrollmentId = pp.Key, StudentAwards = pp.GroupBy(x => x.FinancialAidId, (key, g) => g.OrderByDescending(e => e.ModDate).FirstOrDefault()).ToList() })
                .ToList();
            var failedResults = new List<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>>();
            if (saGs.Any())
            {
                foreach (var saG in saGs)
                {
                    if (StudentEnrollmentRepository.Get(se => se.StuEnrollId == saG.EnrollmentId).Any())
                    {
                        try
                        {
                            var campusId = StudentEnrollmentRepository.Get(se => se.StuEnrollId == saG.EnrollmentId).FirstOrDefault().CampusId;
                            var enrStudentAwards = StudentAwardsRepository
                                .Get(sa => sa.StuEnrollId == saG.EnrollmentId
                                             && saG.StudentAwards.Select(esa => esa.FinancialAidId.Trim()).Contains(sa.FaId));
                            var processed = new List<string>();
                            if (enrStudentAwards.Any())
                            {
                                foreach (var studentAward in saG.StudentAwards)
                                {
                                    var existingAward = enrStudentAwards.FirstOrDefault(esa => esa.FaId == studentAward.FinancialAidId.Trim());
                                    if (existingAward != null)
                                    {
                                        // start adding the functionality to delete the awards here
                                        if (studentAward.Deleted)
                                        {
                                            // get disbursements for the given award
                                            var disbursements = StudentDisbursementsRepository
                                                .Get(d => d.StudentAwardId == existingAward.StudentAwardId)
                                                .Include(d => d.StudentAward).Include(d => d.SaPmtDisbRel).ToList();
                                            if (disbursements != null && disbursements.Any())
                                            {
                                                foreach (var disbursement in disbursements)
                                                {
                                                    var trasnsactionIdDisRel = disbursement.SaPmtDisbRel
                                                        .Where(t => t.TransactionId != null)
                                                        .Select(r => r.TransactionId).ToList();
                                                    var refunds  = RefundsRepository.Get(r => r.AwardScheduleId.Equals(disbursement.AwardScheduleId)).ToList();
                                                    var transactionidRefund = refunds.Where(t => t.TransactionId != null).Select(r => r.TransactionId).ToList();
                                                    var transactionExist = disbursement.TransactionId;
                                                    if (disbursement.SaPmtDisbRel.Any())
                                                    {
                                                        PaymentDisbursementsRepository.Delete(disbursement.SaPmtDisbRel.ToList());
                                                        PaymentDisbursementsRepository.Save();
                                                    }

                                                    if (RefundsRepository.Get(r => r.AwardScheduleId.Equals(disbursement.AwardScheduleId)).Any())
                                                    {
                                                        RefundsRepository.Delete(refunds);
                                                        RefundsRepository.Save();
                                                    }
                                                    StudentDisbursementsRepository.Delete(disbursement);
                                                    StudentDisbursementsRepository.Save();

                                                    var relTrans = TransactionsRepository.Get(t => t.TransactionId==disbursement.TransactionId).ToList();
                                                    if (relTrans != null && relTrans.Any())
                                                    {
                                                        TransactionsRepository.Delete(relTrans);
                                                        TransactionsRepository.Save();
                                                    }
                                                    var refTrans = TransactionsRepository.Get(t => transactionidRefund.Contains(t.TransactionId)).ToList();
                                                    if (refTrans != null && refTrans.Any())
                                                    {
                                                        TransactionsRepository.Delete(refTrans);
                                                        TransactionsRepository.Save();
                                                    }
                                                    var disTrans = TransactionsRepository.Get(t => t.TransactionId == transactionExist).ToList();
                                                    if (disTrans != null && disTrans.Any())
                                                    {
                                                        TransactionsRepository.Delete(disTrans);
                                                        TransactionsRepository.Save();
                                                    }
                                                    
                                                }
                                            }

                                            StudentAwardsRepository.Delete(existingAward);
                                            StudentAwardsRepository.Save();
                                            processed.Add(studentAward.FinancialAidId);
                                            continue;
                                        }
                                        // end adding the functionality to delete the awards here
                                        var academicYearId = await
                                            this.academicYearService.GetByAFACode(studentAward.AwardYear, campusId);

                                        var cleanFundSource = this.fundSourceService.CleanAFAAwardType(studentAward
                                                .TitleIvAwardType);
                                        var sysFundSource =
                                            this.fundSourceService.MapAfaAwardTypeToSystemFundSources(cleanFundSource);
                                        var awardType = this.fundSourceService.MapSystemFundSourceToAwardType(sysFundSource);
                                        var fundSourceId =
                                            await this.fundSourceService.GetByAFAAwardType(
                                                cleanFundSource, campusId);
                                        if (!academicYearId.IsEmpty() && !fundSourceId.IsEmpty())
                                        {
                                            existingAward.AwardTypeId = fundSourceId;
                                            existingAward.AcademicYearId = academicYearId;
                                            existingAward.AwardStartDate = studentAward.AwardStartDate;
                                            existingAward.AwardEndDate = studentAward.AwardEndDate;
                                            existingAward.LoanId = awardType == Constants.AwardTypes.Loan ? studentAward.LoanId.Trim() : string.Empty;
                                            existingAward.FaId = studentAward.FinancialAidId.Trim();
                                            existingAward.AwardStatus = studentAward.AwardStatus;
                                            existingAward.ModDate = DateTime.Now;
                                            existingAward.ModUser = "Integration";
                                            StudentAwardsRepository.Update(existingAward);
                                        }

                                        processed.Add(studentAward.FinancialAidId);
                                    }
                                }
                            }

                            var newStudentAwards = saG.StudentAwards.Where(sa => !processed.Contains(sa.FinancialAidId)).ToList();
                            if (newStudentAwards.Any())
                            {
                                var newStudentAwardEntities = new List<FaStudentAwards>();

                                foreach (var newAward in newStudentAwards)
                                {
                                    // start adding the logic of deleted
                                    if (newAward.Deleted)
                                    {
                                        continue;
                                    }
                                    // end adding the logic of deleted

                                    var academicYearId = await
                                        this.academicYearService.GetByAFACode(newAward.AwardYear, campusId);
                                    var fundSourceId =
                                        await this.fundSourceService.GetByAFAAwardType(
                                            newAward.TitleIvAwardType, campusId);
                                    if (!academicYearId.IsEmpty() && !fundSourceId.IsEmpty())
                                    {
                                        var newStudentAwardEntity = this.mapper.Map<FaStudentAwards>(newAward);
                                        var cleanFundSource = this.fundSourceService.CleanAFAAwardType(newAward
                                                .TitleIvAwardType);
                                        var sysFundSource =
                                            this.fundSourceService.MapAfaAwardTypeToSystemFundSources(cleanFundSource);

                                        var awardType = this.fundSourceService.MapSystemFundSourceToAwardType(sysFundSource);
                                        newStudentAwardEntity.LoanId = awardType == Constants.AwardTypes.Loan ? newAward.LoanId.Trim() : string.Empty;
                                        newStudentAwardEntity.AcademicYearId = academicYearId;
                                        newStudentAwardEntity.AwardTypeId = fundSourceId;
                                        newStudentAwardEntity.ModDate = DateTime.Now;
                                        newStudentAwardEntity.ModUser = "Integration";
                                        newStudentAwardEntities.Add(newStudentAwardEntity);
                                    }
                                }
                                await StudentAwardsRepository.CreateAsync(newStudentAwardEntities);

                            }
                            StudentAwardsRepository.Save();
                        }
                        catch (Exception e)
                        {
                            e.TrackException();
                            failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = saG.StudentAwards });
                        }
                    }
                    else
                    {
                        failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.StudentAwards>>() { Text = saG.EnrollmentId, Value = saG.StudentAwards });
                    }

                }
            }

            return failedResults;
        }

        /// <summary>
        /// Create or update student disbursements as pulled from AFA
        /// </summary>
        /// <param name="disbursements"></param>
        /// <returns></returns>
        public async Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> CreateUpdateStudentDisbursements(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements)
        {
            var currStudentAward = Guid.Empty;
            var disGs = disbursements
                           .GroupBy(dis => dis.FinancialAidId.Trim())
                           .Select(dis => new { FinancialAidId = dis.Key, Disbursements = dis.GroupBy(x => x.SequenceNumber, (key, g) => g.OrderByDescending(e => e.ModDate).FirstOrDefault()).OrderBy(_ => _.SequenceNumber).ToList() })
                           .ToList();
            var failedResults = new List<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>();
            if (disGs.Any())
            {
                foreach (var disG in disGs)
                {
                    if (StudentAwardsRepository.Get(se => se.FaId == disG.FinancialAidId.Trim()).Any())
                    {
                        try
                        {
                            var studentAward = StudentAwardsRepository
                                .Get(sa => sa.FaId == disG.FinancialAidId.Trim()).Include(sa => sa.StuEnroll).Include(sa => sa.FaStudentAwardSchedule).ThenInclude(sas => sas.SaPmtDisbRel).Include(sa => sa.AwardType).FirstOrDefault();
                            if (studentAward != null)
                            {
                                currStudentAward = studentAward.StudentAwardId;
                                var studentAwardDisbursements = studentAward.FaStudentAwardSchedule.ToList();
                                var processed = new List<int>();
                                if (studentAwardDisbursements.Any())
                                {
                                    foreach (var disbursement in disG.Disbursements)
                                    {
                                        var existingDisbursement =
                                            studentAwardDisbursements.FirstOrDefault(edis => edis.SequenceNumber == disbursement.SequenceNumber);
                                        if (existingDisbursement != null)
                                        {
                                            //add logic of delete 
                                            if (disbursement.Deleted)
                                            {
                                                // write code to delete from SaPmtDisbRel,sarefunds and then from  and then fastudentawardSchedule at end from satransactions
                                                var transactionidDisRel = existingDisbursement.SaPmtDisbRel.Where(t => t.TransactionId != null).Select(r => r.TransactionId).ToList();
                                                var refunds = RefundsRepository.Get(r => r.AwardScheduleId.Equals(existingDisbursement.AwardScheduleId)).ToList();
                                                var transactionidRefund = refunds.Where(t => t.TransactionId != null).Select(r => r.TransactionId).ToList();
                                                var transactionExist = existingDisbursement.TransactionId;

                                                if (existingDisbursement.SaPmtDisbRel.Any())
                                                {
                                                    PaymentDisbursementsRepository.Delete(existingDisbursement.SaPmtDisbRel.ToList());
                                                    PaymentDisbursementsRepository.Save();
                                                }

                                                if (RefundsRepository.Get(r => r.AwardScheduleId.Equals(existingDisbursement.AwardScheduleId)).Any())
                                                {
                                                    RefundsRepository.Delete(refunds);
                                                    RefundsRepository.Save();
                                                }

                                                StudentDisbursementsRepository.Delete(existingDisbursement);
                                                StudentDisbursementsRepository.Save();

                                                var relTrans = TransactionsRepository.Get(t => transactionidDisRel.Contains(t.TransactionId)).ToList();
                                                if (relTrans != null && relTrans.Any())
                                                {
                                                    TransactionsRepository.Delete(relTrans);
                                                    TransactionsRepository.Save();
                                                }
                                                var refTrans = TransactionsRepository.Get(t => transactionidRefund.Contains(t.TransactionId)).ToList();
                                                if (refTrans != null && refTrans.Any())
                                                {
                                                    TransactionsRepository.Delete(refTrans);
                                                    TransactionsRepository.Save();
                                                }
                                                var disTrans = TransactionsRepository.Get(t => t.TransactionId == transactionExist).ToList();
                                                if (disTrans != null && disTrans.Any())
                                                {
                                                    TransactionsRepository.Delete(disTrans);
                                                    TransactionsRepository.Save();
                                                }

                                                processed.Add(disbursement.SequenceNumber);
                                                continue;
                                            }

                                            if (disbursement.ActualNetDisbursementAmt == decimal.Zero)
                                            {
                                                existingDisbursement.ExpectedDate =
                                                    disbursement.AnticipatedDisbursementDate;
                                                existingDisbursement.Amount = disbursement.AnticipatedNetDisbursementAmt;
                                                existingDisbursement.GrossAmount = disbursement.AnticipatedGrossAmount;
                                                existingDisbursement.LoanFees = disbursement.FeeAmount;
                                                existingDisbursement.DisbursementStatus = disbursement.DisbursementStatus;
                                                StudentDisbursementsRepository.Update(existingDisbursement);
                                            }

                                            else
                                            {
                                                //received amount comes from SaPmtDisbRel table in advantage
                                                //received date comes from transaction date from saTransactions table
                                                existingDisbursement.ExpectedDate =
                                                    disbursement.AnticipatedDisbursementDate;
                                                existingDisbursement.Amount = disbursement.AnticipatedNetDisbursementAmt;
                                                existingDisbursement.GrossAmount = disbursement.AnticipatedGrossAmount;
                                                existingDisbursement.LoanFees = disbursement.FeeAmount;
                                                existingDisbursement.DisbursementStatus = disbursement.DisbursementStatus;

                                                if ((existingDisbursement.TransactionId == null && !existingDisbursement.SaPmtDisbRel.Any()) && (this.fundSourceService.IsTitleIV(studentAward.AwardTypeId)))
                                                {
                                                    var paymentPeriodNumber = this.paymentPeriodService.GetPaymentPeriodNumberByName(studentAward.StuEnrollId, disbursement.PaymentPeriodName);

                                                    var transactionTypeId =
                                                    await transactionTypeService.GetIdByDescription(TransactionTypes.Payment);
                                                    var termDetails = this.termService.GetTerms(studentAward.StuEnrollId);
                                                    //post a payment to the student's ledger
                                                    var transaction = new SaTransactions()
                                                    {
                                                        TransDate = disbursement.ActualDisbursementDate,
                                                        TransAmount = -(disbursement.ActualNetDisbursementAmt),
                                                        TransTypeId = transactionTypeId,
                                                        FundSourceId = studentAward.AwardTypeId,
                                                        StuEnrollId = studentAward.StuEnrollId,
                                                        CampusId = studentAward.StuEnroll.CampusId,
                                                        AcademicYearId = studentAward.AcademicYearId,
                                                        TransDescrip = studentAward.AwardType.FundSourceDescrip,
                                                        TransReference = paymentPeriodNumber.HasValue ? "Payment Period " + paymentPeriodNumber.Value.ToString() : "",
                                                        ModUser = disbursement.ModUser,
                                                        CreateDate = disbursement.DateCreated,
                                                        ModDate = disbursement.ModDate,
                                                        Voided = false,
                                                        IsAutomatic = false,
                                                        IsPosted = true,
                                                        DisplaySequence = 99999,
                                                        SecondDisplaySequence = 99999,
                                                        TermId = termDetails.FirstOrDefault()?.Id
                                                    };
                                                    transaction = await TransactionsRepository.CreateAsync(transaction);
                                                    TransactionsRepository.Save();

                                                    existingDisbursement.TransactionId =
                                                        transaction.TransactionId;
                                                    existingDisbursement.IsProcessed = true;
                                                    StudentDisbursementsRepository.Update(existingDisbursement);

                                                    var pmtDisbRel = new SaPmtDisbRel()
                                                    {
                                                        TransactionId = transaction.TransactionId,
                                                        Amount = disbursement.ActualNetDisbursementAmt,
                                                        AwardScheduleId = existingDisbursement.AwardScheduleId,
                                                        ModUser = disbursement.ModUser,
                                                        ModDate = disbursement.ModDate,

                                                    };
                                                    await PaymentDisbursementsRepository.CreateAsync(pmtDisbRel);
                                                    PaymentDisbursementsRepository.Save();
                                                }
                                                //else
                                                //{
                                                //    var disbTransaction = TransactionsRepository.GetById(existingDisbursement.TransactionId);
                                                //    if (disbTransaction != null)
                                                //    {
                                                //        var paymentPeriodNumber = this.paymentPeriodService.GetPaymentPeriodNumberByName(studentAward.StuEnrollId, disbursement.PaymentPeriodName);

                                                //        if (paymentPeriodNumber.HasValue && (disbTransaction.TransReference != "Payment Period " + paymentPeriodNumber.Value.ToString()))
                                                //        {
                                                //            disbTransaction.TransReference = "Payment Period " + paymentPeriodNumber.Value.ToString();
                                                //            TransactionsRepository.Update(disbTransaction);
                                                //            TransactionsRepository.Save();
                                                //        }

                                                //    }

                                                //    StudentDisbursementsRepository.Update(existingDisbursement);
                                                //}

                                            }
                                            processed.Add(disbursement.SequenceNumber);
                                        }
                                    }

                                    //recalculate award amount on student award 
                                    studentAward = StudentAwardsRepository.GetById(studentAward.StudentAwardId);
                                    studentAward.GrossAmount = studentAward.FaStudentAwardSchedule
                                        .Select(disb => disb.GrossAmount).DefaultIfEmpty(0).Sum().Value;
                                    studentAward.LoanFees = studentAward.FaStudentAwardSchedule
                                        .Select(disb => disb.LoanFees).DefaultIfEmpty(0).Sum().Value;
                                    studentAward.Disbursements = studentAward.FaStudentAwardSchedule.Count;
                                    StudentAwardsRepository.Save();
                                }

                                var newStudentDisbursements = disG.Disbursements.Where(dis => !processed.Contains(dis.SequenceNumber)).ToList();
                                if (newStudentDisbursements.Any())
                                {
                                    var newStudentDisbursementEntities = new List<FaStudentAwardSchedule>();

                                    foreach (var newDisbursement in newStudentDisbursements)
                                    {
                                        // adding the logic of deleted
                                        if (newDisbursement.Deleted)
                                        {
                                            continue;
                                        }
                                        //netAmount += newDisbursement.AnticipatedNetDisbursementAmt;

                                        var newStudentDisbursementEntity = this.mapper.Map<FaStudentAwardSchedule>(newDisbursement);
                                        newStudentDisbursementEntity.StudentAwardId = studentAward.StudentAwardId;
                                        newStudentDisbursementEntity.DisbursementNumber = (byte)disG.Disbursements.Count;

                                        if (newDisbursement.ActualNetDisbursementAmt != decimal.Zero)
                                        {
                                            //received amount comes from SaPmtDisbRel table in advantage
                                            //received date comes from transaction date from saTransactions table

                                            var paymentPeriodNumber = this.paymentPeriodService.GetPaymentPeriodNumberByName(studentAward.StuEnrollId, newDisbursement.PaymentPeriodName);

                                            var transactionTypeId =
                                            await transactionTypeService.GetIdByDescription(TransactionTypes.Payment);


                                            var termDetails = this.termService.GetTerms(studentAward.StuEnrollId);
                                            if ((this.fundSourceService.IsTitleIV(studentAward.AwardTypeId)))
                                            {
                                                //post a payment to the student's ledger
                                                var transaction = new SaTransactions()
                                                {
                                                    TransDate = newDisbursement.ActualDisbursementDate,
                                                    TransAmount = -(newDisbursement.ActualNetDisbursementAmt),
                                                    TransTypeId = transactionTypeId,
                                                    FundSourceId = studentAward.AwardTypeId,
                                                    StuEnrollId = studentAward.StuEnrollId,
                                                    CampusId = studentAward.StuEnroll.CampusId,
                                                    AcademicYearId = studentAward.AcademicYearId,
                                                    TransDescrip = studentAward.AwardType.FundSourceDescrip,
                                                    TransReference = paymentPeriodNumber.HasValue ? "Payment Period " + paymentPeriodNumber.Value.ToString() : "",
                                                    ModUser = newDisbursement.ModUser,
                                                    CreateDate = newDisbursement.DateCreated,
                                                    ModDate = newDisbursement.ModDate,
                                                    Voided = false,
                                                    IsAutomatic = false,
                                                    IsPosted = true,
                                                    DisplaySequence = 99999,
                                                    SecondDisplaySequence = 99999,
                                                    TermId = termDetails.FirstOrDefault().Id
                                                };
                                                transaction = await TransactionsRepository.CreateAsync(transaction);
                                                TransactionsRepository.Save();

                                                newStudentDisbursementEntity.TransactionId =
                                                        transaction.TransactionId;
                                                newStudentDisbursementEntity.IsProcessed = true;

                                                newStudentDisbursementEntity.SaPmtDisbRel.Add(new SaPmtDisbRel()
                                                {
                                                    TransactionId = transaction.TransactionId,
                                                    Amount = newDisbursement.ActualNetDisbursementAmt,
                                                    ModUser = newDisbursement.ModUser,
                                                    ModDate = newDisbursement.ModDate,
                                                });

                                            }
                                        }
                                        newStudentDisbursementEntities.Add(newStudentDisbursementEntity);

                                    }
                                    await StudentDisbursementsRepository.CreateAsync(newStudentDisbursementEntities);
                                    StudentDisbursementsRepository.Save();

                                }
                                StudentDisbursementsRepository.Save();
                                //recalculate award amount on student award 
                                studentAward = StudentAwardsRepository.GetById(studentAward.StudentAwardId);
                                studentAward.GrossAmount = studentAward.FaStudentAwardSchedule
                                    .Select(disb => disb.GrossAmount).DefaultIfEmpty(0).Sum().Value;
                                studentAward.LoanFees = studentAward.FaStudentAwardSchedule
                                    .Select(disb => disb.LoanFees).DefaultIfEmpty(0).Sum().Value;
                                studentAward.Disbursements = studentAward.FaStudentAwardSchedule.Count;
                                StudentAwardsRepository.Save();
                            }

                        }
                        catch (Exception e)
                        {
                            e.TrackException();
                            failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = currStudentAward, Value = disG.Disbursements });
                        }
                    }
                    else
                    {
                        failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = Guid.Empty, Value = disG.Disbursements });
                    }

                }
            }

            return failedResults;
        }

        public async Task<IList<AdvStagingRefundV1>> GetTitleIVRefundsForAFA(Guid campusId)
        {
            List<AdvStagingRefundV1> refundsForAFA = new List<AdvStagingRefundV1>();
            var cmsId = CampusRepository.Get(c => c.CampusId == campusId).Select(c => c.CmsId).FirstOrDefault();
            if (string.IsNullOrEmpty(cmsId))
            {
                return refundsForAFA;
            }

            var awards = StudentAwardsRepository.Get(a =>
                a.StuEnroll.CampusId == campusId && a.StuEnroll.Lead.AfaStudentId.HasValue)
                .Include(x => x.StuEnroll)
                .ThenInclude(y => y.Lead).Include(x => x.FaStudentAwardSchedule);

            var refunds = RefundsRepository.Get(r => r.FundSourceId.HasValue && r.AwardScheduleId.HasValue && r.RefundAmount.HasValue);
            var transactions = TransactionsRepository.Get(t => awards.Select(a => a.StuEnrollId).Contains(t.StuEnrollId));

            refundsForAFA = (from awa in awards
                             from disb in awa.FaStudentAwardSchedule
                             join refs in refunds on disb.AwardScheduleId equals refs.AwardScheduleId
                             join trans in transactions on refs.TransactionId equals trans.TransactionId
                             select new AdvStagingRefundV1()
                             {
                                 SISEnrollmentID = awa.StuEnrollId,
                                 LocationCMSID = cmsId,
                                 FundSourceId = refs.FundSourceId.Value,
                                 DisbursementSequenceNumber = Convert.ToInt32(disb.SequenceNumber),
                                 RefundAmount = refs.RefundAmount.Value,
                                 RefundDate = trans.TransDate,
                                 AwardID = awa.FaId,
                                 SSN = awa.StuEnroll.Lead.Ssn,
                                 DateCreated = refs.ModDate.Value,
                                 DateUpdated = refs.ModDate,
                                 UserCreated = refs.ModUser,
                                 UserUpdated = refs.ModUser


                             }).ToList();

            refundsForAFA = refundsForAFA.Where(r => this.fundSourceService.IsTitleIV(r.FundSourceId)).ToList();

            return refundsForAFA;
        }

        public async Task<IList<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>> UpdateDisbursementTransactionReference(IEnumerable<DataTransferObjects.StudentAccounts.AFA.Disbursement> disbursements)
        {
            var currStudentAward = Guid.Empty;
            var disGs = disbursements
                           .GroupBy(dis => dis.FinancialAidId.Trim())
                           .Select(dis => new { FinancialAidId = dis.Key, Disbursements = dis.GroupBy(x => x.SequenceNumber, (key, g) => g.OrderByDescending(e => e.ModDate).FirstOrDefault()).OrderBy(_ => _.SequenceNumber).ToList() })
                           .ToList();
            var failedResults = new List<IListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>>();
            if (disGs.Any())
            {
                foreach (var disG in disGs)
                {
                    if (StudentAwardsRepository.Get(se => se.FaId == disG.FinancialAidId.Trim()).Any())
                    {
                        try
                        {
                            var studentAward = StudentAwardsRepository
                                .Get(sa => sa.FaId == disG.FinancialAidId.Trim()).Include(sa => sa.StuEnroll).Include(sa => sa.FaStudentAwardSchedule).ThenInclude(sas => sas.SaPmtDisbRel).Include(sa => sa.AwardType).FirstOrDefault();
                            if (studentAward != null)
                            {
                                currStudentAward = studentAward.StudentAwardId;
                                var studentAwardDisbursements = studentAward.FaStudentAwardSchedule.ToList();
                                var processed = new List<int>();
                                if (studentAwardDisbursements.Any())
                                {

                                    foreach (var disbursement in disG.Disbursements)
                                    {
                                        var existingDisbursement =
                                            studentAwardDisbursements.FirstOrDefault(edis => edis.SequenceNumber == disbursement.SequenceNumber);
                                        if (existingDisbursement != null)
                                        {

                                            var disbTransaction = TransactionsRepository.GetById(existingDisbursement.TransactionId);
                                            if (disbTransaction != null)
                                            {
                                                var paymentPeriodNumber = this.paymentPeriodService.GetPaymentPeriodNumberByName(studentAward.StuEnrollId, disbursement.PaymentPeriodName);

                                                if (paymentPeriodNumber.HasValue && disbTransaction.TransReference.Contains("Payment Period") && (disbTransaction.TransReference != "Payment Period " + paymentPeriodNumber.Value.ToString()))
                                                {
                                                    disbTransaction.TransReference = "Payment Period " + paymentPeriodNumber.Value.ToString();
                                                    TransactionsRepository.Update(disbTransaction);
                                                    TransactionsRepository.Save();
                                                }

                                            }

                                        }

                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            e.TrackException();
                            failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = currStudentAward, Value = disG.Disbursements });
                        }
                    }
                    else
                    {
                        failedResults.Add(new ListItem<Guid, List<DataTransferObjects.StudentAccounts.AFA.Disbursement>>() { Text = Guid.Empty, Value = disG.Disbursements });
                    }

                }
            }

            return failedResults;
        }


        /// <summary>
        /// The get direct loan student awards action returns the list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </summary>
        /// <remarks>
        /// The get direct loan student awards method returns the list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </remarks>
        /// <param name="enrollmentId">
        /// EnrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="termStartDate">
        /// The term Start Date is the start of the term.
        /// </param>
        /// <param name="termEndDate">
        /// The term End Date is the end date of the term.
        /// </param>
        /// <returns>
        /// Returns list of loans awarded to the student for the given enrollmentId during the specified term.
        /// </returns>
        public async Task<IEnumerable<StudentAwards>> GetDirectLoanStudentAwards(Guid enrollmentId, DateTime termStartDate, DateTime termEndDate)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var studentAwards = this.StudentAwardsRepository.Get(s => s.StuEnrollId == enrollmentId).Include(a => a.AwardType).ToList();
                    var faStudentAwardsList = studentAwards.Where(
                        x => x.AwardType != null && x.AwardType.AwardTypeId == (int)Constants.AwardTypes.Loan
                             && ((termStartDate <= x.AwardStartDate && x.AwardStartDate <= termEndDate)
                                 || (termStartDate <= x.AwardEndDate && x.AwardEndDate <= termEndDate)
                                 || (termStartDate >= x.AwardStartDate && x.AwardEndDate >= termEndDate)));

                    if (!faStudentAwardsList.Any())
                    {
                        faStudentAwardsList = studentAwards.Where(
                            x => x.AwardType != null && x.AwardType.AwardTypeId == (int)Constants.AwardTypes.Grant
                                 && ((termStartDate <= x.AwardStartDate && x.AwardStartDate <= termEndDate)
                                 || (termStartDate <= x.AwardEndDate && x.AwardEndDate <= termEndDate)
                                 || (termStartDate >= x.AwardStartDate && x.AwardEndDate >= termEndDate)));

                        if (!faStudentAwardsList.Any())
                        {
                            var awards = new List<StudentAwards> { new StudentAwards { ResultStatus = ApiMsgs.NO_AWARDS } };
                            return awards;
                        }
                        else
                        {
                            var awards = this.mapper.Map<IEnumerable<StudentAwards>>(faStudentAwardsList).ToList();
                            awards[0].ResultStatus = ApiMsgs.AWARDS_FOUND;
                            return awards;
                        }
                    }

                    var studentAwardsList = this.mapper.Map<IEnumerable<StudentAwards>>(faStudentAwardsList).ToList();
                    studentAwardsList[0].ResultStatus = ApiMsgs.DIRECT_LOANS_FOUND;
                    return studentAwardsList;
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new List<StudentAwards> { new StudentAwards { ResultStatus = ApiMsgs.NO_DIRECT_LOAN } };
                }
            });
        }
    }
}