﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FundSourceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the FundSourceService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.Host.Extensions;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using FAME.Extensions.Helpers;

namespace FAME.Advantage.RestApi.Host.Services.StudentAccounts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;

    using Microsoft.EntityFrameworkCore;

    using Orm.Advantage.Domain.Common;

    /// <summary>
    /// The fund source service.
    /// </summary>
    public class FundSourceService : IFundSourceService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FundSourceService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus group service
        /// </param>
        public FundSourceService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService, ICampusGroupService campusGroupService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.statusService = statusService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The fund source repository.
        /// </summary>
        private IRepository<SaFundSources> FundSourceRepository => this.unitOfWork.GetRepositoryForEntity<SaFundSources>();

        /// <summary>
        /// The student enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.unitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The active id.
        /// </summary>
        private Guid ActiveId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;
        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();
        private Guid CampusGroupAll => Task.Run(() => this.campusGroupService.GetCampusGroupAllId()).Result;

        /// <summary>
        /// Get Title IV fund source id using the given AFA award type.
        /// </summary>
        /// <param name="awardType"></param>
        /// <returns>Fund source id</returns>
        public async Task<Guid> GetByAFAAwardType(string awardType)
        {
            var fundSourceId = Guid.Empty;
            //remove any years in award type and remove trailing/leading spaces
            awardType = this.CleanAFAAwardType(awardType);

            //SEOG will also map to other to accomodate special mapping cases...
            var advFundSource = MapAfaAwardTypeToSystemFundSources(awardType);
            var otherFundSource = advFundSource == Constants.SystemFundSources.Other;
            // 'other fund sources' will be matched based on fund source description
            if (otherFundSource)
            {
                //this function will take care of few cases where afa name differs from advantage for fund source
                awardType = MapOtherAwardTypesToAdvantageFundSources(awardType);

                fundSourceId = await Task.Run(() => SimpleCache.GetOrAddExisting(
        $"{this.CacheKeyFirstSegment}:FundSourceByDesc:{awardType}",
        () => (this.FundSourceRepository.Get(fs => fs.FundSourceDescrip == awardType)).Select(ay => ay.FundSourceId).FirstOrDefault(),
        CacheExpiration.SHORT));

            }

            //fund source mapped to one of our title iv cases so we can use system fund source
            else
            {
                fundSourceId = await Task.Run(() => SimpleCache.GetOrAddExisting(
               $"{this.CacheKeyFirstSegment}:FundSourceBySys:{advFundSource}",
               () => (this.FundSourceRepository.Get(fs => fs.AdvFundSourceId.HasValue && fs.AdvFundSourceId == (byte)advFundSource)).Select(ay => ay.FundSourceId).FirstOrDefault(),
               CacheExpiration.SHORT));
            }

            if (fundSourceId.IsEmpty())
            {
                Console.WriteLine("no mapping should fail");
            }

            return fundSourceId;
        }

        /// <summary>
        /// Get Title IV fund source id using the given AFA award type.
        /// </summary>
        /// <param name="awardType"></param>
        /// <param name="campusId"></param>
        /// <returns>Fund source id</returns>
        public async Task<Guid> GetByAFAAwardType(string awardType, Guid campusId)
        {
            var fundSourceId = Guid.Empty;
            //remove any years in award type and remove trailing/leading spaces
            awardType = this.CleanAFAAwardType(awardType);


            //SEOG will also map to other to accomodate special mapping cases...
            var advFundSource = MapAfaAwardTypeToSystemFundSources(awardType);
            var otherFundSource = advFundSource == Constants.SystemFundSources.Other;
            // 'other fund sources' will be matched based on fund source description
            if (otherFundSource)
            {
                //this function will take care of few cases where afa name differs from advantage for fund source
                awardType = MapOtherAwardTypesToAdvantageFundSources(awardType);

                fundSourceId = await Task.Run(() => SimpleCache.GetOrAddExisting(
        $"{this.CacheKeyFirstSegment}:FundSourceByDesc:{awardType}:{campusId}",
        () => (this.FundSourceRepository.Get(fs => fs.FundSourceDescrip == awardType && fs.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId)
                                             && fs.StatusId == this.ActiveId)).Select(ay => ay.FundSourceId).FirstOrDefault(),
        CacheExpiration.SHORT));

                //couldnt match based on fundsource description, fallback to fund source with code 'Other'
                if (fundSourceId.IsEmpty())
                {
                    var other = advFundSource.ToString().ToUpper();
                    fundSourceId = this.FundSourceRepository.Get(fs => fs.FundSourceCode == other && fs.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId)
                                       && fs.StatusId == this.ActiveId).Select(ay => ay.FundSourceId).FirstOrDefault();
                }
            }

            //fund source mapped to one of our title iv cases so we can use system fund source
            else
            {
                fundSourceId = await Task.Run(() => SimpleCache.GetOrAddExisting(
               $"{this.CacheKeyFirstSegment}:FundSourceBySys:{advFundSource}:{campusId}",
               () => (this.FundSourceRepository.Get(fs => fs.AdvFundSourceId.HasValue && fs.AdvFundSourceId == (byte)advFundSource && fs.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId)
                                                    && fs.StatusId == this.ActiveId && fs.TitleIv.HasValue && fs.TitleIv.Value)).Select(ay => ay.FundSourceId).FirstOrDefault(),
               CacheExpiration.SHORT));
            }

            //we couldnt find a matching fund source :(, lets create one
            if (fundSourceId.IsEmpty())
            {
                try
                {

                    var awardTypeId = MapSystemFundSourceToAwardType(advFundSource);
                    var temp = await this.FundSourceRepository.CreateAsync(new SaFundSources()
                    {
                        FundSourceCode = advFundSource.ToString().ToUpper(),
                        StatusId = ActiveId,
                        FundSourceDescrip = advFundSource.ToString().ToUpper() + " (AFA)",
                        CampGrpId = CampusGroupAll,
                        TitleIv = true,
                        AwardTypeId = (int)awardTypeId,
                        AdvFundSourceId = (byte)advFundSource,
                        ModUser = this.unitOfWork.GetTenantUser().Email,
                        ModDate = DateTime.Now
                    });
                    this.FundSourceRepository.Save();
                    fundSourceId = temp.FundSourceId;
                    if (otherFundSource)
                    {
                        SimpleCache.Set($"{this.CacheKeyFirstSegment}:FundSourceByDesc:{awardType}:{campusId}", fundSourceId);

                    }
                    else
                    {
                        SimpleCache.Set($"{this.CacheKeyFirstSegment}:FundSourceBySys:{advFundSource}:{campusId}", fundSourceId);
                    }
                }
                catch(Exception e)
                {
                    fundSourceId = this.FundSourceRepository.Get(fs => fs.FundSourceCode == advFundSource.ToString().ToUpper() && fs.CampGrpId == CampusGroupAll
                                   && fs.StatusId == this.ActiveId).Select(ay => ay.FundSourceId).FirstOrDefault();
                }

            }

            return fundSourceId;
        }

        /// <summary>
        /// Maps the Advantage System fund sources to default award types
        /// </summary>
        /// <param name="fundSource"></param>
        /// <returns></returns>
        public Constants.AwardTypes MapSystemFundSourceToAwardType(Constants.SystemFundSources fundSource)
        {
            Constants.AwardTypes awardType;
            switch (fundSource)
            {
                case Constants.SystemFundSources.Pell:
                    awardType = Constants.AwardTypes.Grant;
                    break;
                case Constants.SystemFundSources.DL_PLUS:
                    awardType = Constants.AwardTypes.Loan;
                    break;
                case Constants.SystemFundSources.DL_SUB:
                    awardType = Constants.AwardTypes.Loan;
                    break;
                case Constants.SystemFundSources.DL_UNSUB:
                    awardType = Constants.AwardTypes.Loan;
                    break;
                case Constants.SystemFundSources.Other:
                    awardType = Constants.AwardTypes.Other;
                    break;
                case Constants.SystemFundSources.SEOG:
                    awardType = Constants.AwardTypes.SchoolScholarship;
                    break;
                default:
                    awardType = Constants.AwardTypes.Other;
                    break;
            }

            return awardType;
        }
        /// <summary>
        /// Maps the award types received from AFA to the Advantage system fund sources.
        /// </summary>
        /// <param name="awardType"></param>
        /// <returns></returns>
        public Constants.SystemFundSources MapAfaAwardTypeToSystemFundSources(string awardType)
        {
            Constants.SystemFundSources systemFundSource;
            switch (awardType.ToUpper())
            {
                case AFA.FundSource.Pell:
                    systemFundSource = Constants.SystemFundSources.Pell;
                    break;
                case AFA.FundSource.DLPLUS:
                    systemFundSource = Constants.SystemFundSources.DL_PLUS;
                    break;
                case AFA.FundSource.DLSUB:
                    systemFundSource = Constants.SystemFundSources.DL_SUB;
                    break;
                case AFA.FundSource.DLUNSUB:
                    systemFundSource = Constants.SystemFundSources.DL_UNSUB;
                    break;
                case AFA.FundSource.OTHER:
                case AFA.FundSource.FSEOG:
                    systemFundSource = Constants.SystemFundSources.Other;
                    break;
                //case AFA.FundSource.FSEOG:
                //    systemFundSource = Constants.SystemFundSources.SEOG;
                //    break;
                default:
                    systemFundSource = Constants.SystemFundSources.Other;
                    break;
            }

            return systemFundSource;
        }

        string MapOtherAwardTypesToAdvantageFundSources(string awardType)
        {
            switch (awardType)
            {
                case AFA.FundSource.Other.SLMSmartOpt:
                    return FundSource.SLMSmartOption;
                case AFA.FundSource.FSEOG:
                    return FundSource.FSEOG;
                case AFA.FundSource.Other.Post911GIBillRights:
                    return FundSource.Post911GIBillBenifits;
                case AFA.FundSource.Other.SEOGMatch:
                    return FundSource.FSEOGMatch;
                case AFA.FundSource.Other.TUBCSpecGrant:
                    return FundSource.TUBCSpecialGrant;
                case AFA.FundSource.Other.OtherAid:
                    return FundSource.OtherAid;
                default: return awardType;

            }
        }
        /// <summary>
        /// check if fund source is a title iv fund source
        /// </summary>
        /// <param name="fundSourceId"></param>
        /// <returns></returns>
        public bool IsTitleIV(Guid fundSourceId)
        {
            var result = FundSourceRepository.Get(fs => fs.FundSourceId == fundSourceId).Select(fs => fs.TitleIv)
                 .FirstOrDefault();
            return result.HasValue && result.Value;
        }
        /// <summary>
        /// Remove numbers from award type ex. 2011-2012 and 2011/2012
        /// </summary>
        /// <param name="awardType"></param>
        /// <returns></returns>
        public string CleanAFAAwardType(string awardType)
        {
            return Regex.Replace(awardType, @"\d{1,}(-|\/|\\)*\d{1,}", "").Trim();
        }
    }
}