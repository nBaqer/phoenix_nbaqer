﻿using System;
using System.Collections.Generic;
using Fame.Orm;
using FAME.Orm.Advantage;

namespace FAME.Advantage.RestApi.Host.Services
{
    using Fame.EFCore.Advantage.Interfaces;

    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;

    public interface IAdvantageUnitOfWork : IUnitOfWork
    {
        ITenantUser CurrenTenantUser { get;  }

        void Start(AdvantageOrm orm);
        void Start(AdvantageOrm orm, ITenantUser tenantUser);

        string GetConString();
    }

    public class AdvantageUnitOfWork : IAdvantageUnitOfWork
    {
        private IUnitOfWork _inner;
        private string _conString;

        public ITenantUser CurrenTenantUser { get; private set; }

        private IAdvantageContext advantageContext;

        public void Start(AdvantageOrm orm)
        {
            _inner = orm.GetUnitOfWork();
            _conString = orm.ConString;

            BeginTransaction();
        }
        public void Start(AdvantageOrm orm, ITenantUser tenantUser )
        {
            _inner = orm.GetUnitOfWork();
            _conString = orm.ConString;
            this.CurrenTenantUser = tenantUser;
            BeginTransaction();
        }

        public string GetConString()
        {
            return _conString;
        }

        public void Dispose()
        {
            _inner?.Dispose();
        }

        public TRepository GetRegisteredRepository<TRepository>() where TRepository : class
        {
            return _inner.GetRegisteredRepository<TRepository>();
        }

        public IRepository<TEntity> GetRepositoryForEntity<TEntity>() where TEntity : class, IEntity, new()
        {
            return _inner.GetRepositoryForEntity<TEntity>();
        }

        public bool WasRolledBack => _inner.WasRolledBack;

        public void ManageTransaction(Action action)
        {
            _inner.ManageTransaction(action);
        }

        public void BeginTransaction()
        {
            _inner.BeginTransaction();
        }

        public void Commit()
        {
            _inner.Commit();
        }

        public void Rollback()
        {
            _inner.Rollback();
        }

        public IEnumerable<TEntity> Execute<TEntity>(ICommand<TEntity> command) where TEntity : class
        {
            return _inner.Execute(command);
        }

        public IEnumerable<TOut> Execute<TEntity, TOut>(ICommand<TEntity, TOut> command) where TEntity : class
        {
            return _inner.Execute(command);
        }

        public T Execute<T>(ICommandScalar<T> command) where T : struct
        {
            return _inner.Execute(command);
        }

        public void Execute(ICommand command)
        {
            _inner.Execute(command);
        }
    }
}
