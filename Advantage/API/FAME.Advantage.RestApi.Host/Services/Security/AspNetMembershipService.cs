﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AspNetMembershipService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AspNetMembershipService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The AspNetMembershipService class.
    /// </summary>
    public class AspNetMembershipService : IAspNetMembershipService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;


        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;


        /// <summary>
        /// Initializes a new instance of the <see cref="AspNetMembershipService"/> class.
        /// </summary>
        /// <param name="tenantDb">
        /// The tenant db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AspNetMembershipService(ITenantDbContext tenantDb, IMapper mapper)
        {
            this.tenantDb = tenantDb;
            this.mapper = mapper;
        }

        /// <summary>
        /// The AspnetMembership repository.
        /// </summary>
        private IRepository<AspnetMembership> AspnetMembershipRepository => this.tenantDb.GetRepositoryForEntity<AspnetMembership>();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="membership">
        /// The membership.
        /// </param>
        /// <returns>
        /// The <see cref="AspNetMembership"/>.
        /// </returns>
        public async Task<AspNetMembership> Create(AspNetMembership membership)
        {
            try
            {

                AspNetMembership result = new AspNetMembership();
                var membershipEntity = this.mapper.Map<AspnetMembership>(membership);
                membershipEntity.CreateDate = DateTime.Now;
                membershipEntity.LastLoginDate = DateTime.Now;
                membershipEntity.LastPasswordChangedDate = DateTime.Now;
                membershipEntity.LastLockoutDate = DateTime.Now;
                membershipEntity.FailedPasswordAttemptWindowStart = DateTime.Now;
                membershipEntity.FailedPasswordAnswerAttemptWindowStart = DateTime.Now;
                membershipEntity.PasswordSalt = membershipEntity.PasswordSalt;
                membershipEntity.IsApproved = true;


                var membershipEntityCreated = await this.AspnetMembershipRepository.CreateAsync(membershipEntity);

               await  this.AspnetMembershipRepository.SaveAsync();

                if (membershipEntityCreated != null)
                {
                    result = this.mapper.Map<AspNetMembership>(membershipEntityCreated);
                    return result;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                e.TrackException();
                return null;
            }
        }
    }
}
