﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAspNetApplicationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AspNetApplicationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Security
{
    using System;
    using System.Linq;

    using AutoMapper;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;

    /// <summary>
    /// The AspNetApplicationService interface.
    /// </summary>
    public class AspNetApplicationService : IAspNetApplicationService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;
        

        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;

        /// <summary>
        /// Initializes a new instance of the <see cref="AspNetApplicationService"/> class.
        /// </summary>
        /// <param name="tenantDb">
        /// The tenant db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AspNetApplicationService(ITenantDbContext tenantDb, IMapper mapper)
        {
            this.tenantDb = tenantDb;
            this.mapper = mapper;
        }

        /// <summary>
        /// The AspnetMembership repository.
        /// </summary>
        private IRepository<AspnetApplications> AspnetApplicationRepository => this.tenantDb.GetRepositoryForEntity<AspnetApplications>();


        /// <summary>
        /// The get application id.
        /// </summary>
        /// <param name="applicationName">
        /// The application name.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>

        public Guid GetApplicationId(string applicationName)
        {
            var application = this.AspnetApplicationRepository
                .Get(_ => _.ApplicationName.ToLower().Trim() == applicationName.ToLower().Trim())
                .FirstOrDefault();

            return application != null ? application.ApplicationId : Guid.Empty;

        }
    }
}
