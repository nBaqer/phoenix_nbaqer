﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the AuthService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Security.Cryptography.Pkcs;
    using System.Text;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Entities;
    using FAME.Orm.AdvTenant.Domain.Entities;

    using Jose;

    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// The auth service.
    /// </summary>
    public class AuthService : IAuthService
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// The auth service settings.
        /// </summary>
        private readonly AuthServiceSettings authServiceSettings;


        /// <summary>
        /// The tenant aut db context.
        /// </summary>
        private readonly ITenantDbContext tenantAutDbContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthService"/> class.
        /// </summary>
        /// <param name="logger">
        /// The logger.
        /// </param>
        /// <param name="authServiceSettings">
        /// The auth service settings.
        /// </param>
        /// <param name="tenantAutDbContext">
        /// The tenant aut db context.
        /// </param>
        public AuthService(ILogger<AuthService> logger, IOptions<AuthServiceSettings> authServiceSettings, ITenantDbContext tenantAutDbContext)
        {
            this.logger = logger;
            this.authServiceSettings = authServiceSettings.Value;
            this.tenantAutDbContext = tenantAutDbContext;
        }

        /// <summary>
        /// The tenant users repository.
        /// </summary>
        private IRepository<TenantUsers> TenantUsersRepository =>
            this.tenantAutDbContext.GetRepositoryForEntity<TenantUsers>();

        /// <summary>
        /// The get token.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<TokenResponse> GetToken(string username, string password, string tenant)
        {
            var auth = await Task.Run(() => this.ValidateLogin(username, password, tenant));
            if (auth.error == null)
            {
                return this.CreateToken(auth.results);
            }

            this.logger.LogWarning($"{ApiMsgs.UNSUCCESSFUL_LOGIN} {auth.error}");
            return new TokenResponse { Message = auth.error };
        }
        /// <summary>
        /// The get token.
        /// </summary>
        /// <param name="userId">
        /// The username.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<TokenResponse> GetToken(Guid userId, string tenant)
        {
            var auth = await Task.Run(() => this.ValidateLogin(userId,  tenant));
            if (auth.error == null)
            {
                return this.CreateToken(auth.results);
            }

            this.logger.LogWarning($"{ApiMsgs.UNSUCCESSFUL_LOGIN} {auth.error}");
            return new TokenResponse { Message = auth.error };
        }
        /// <summary>
        /// The validate token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <returns>
        /// The results and error
        /// </returns>
        public (string results, string error) ValidateToken(string token)
        {
            try
            {
                return (JWT.Decode(token, Encoding.ASCII.GetBytes(this.authServiceSettings.SecretKey), JwsAlgorithm.HS256), null);
            }
            catch
            {
                return (null, ApiMsgs.VALIDATE_TOKEN_FAILURE);
            }
        }

        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        public ApiUser GetUser(string username, string tenant)
        {
            var tenantUser = this.TenantUsersRepository
                .Get(x => x.Tenant.TenantName == tenant && x.User.UserName == username)
                .Include(x => x.User)
                .ThenInclude(x => x.AspnetMembership)
                .Include(x => x.Tenant)
                .ThenInclude(x => x.Environment)
                .FirstOrDefault();

            return new ApiUser(tenantUser);
        }
        /// <summary>
        /// The get user.
        /// </summary>
        /// <param name="userId">
        /// The user Id.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="ApiUser"/>.
        /// </returns>
        public ApiUser GetUser(Guid userId, string tenant)
        {
            var tenantUser = this.TenantUsersRepository
                .Get(x => x.Tenant.TenantName == tenant && x.User.UserId == userId)
                .Include(x => x.User)
                .ThenInclude(x => x.AspnetMembership)
                .Include(x => x.Tenant)
                .ThenInclude(x => x.Environment)
                .FirstOrDefault();

            return new ApiUser(tenantUser);
        }
        /// <summary>
        /// The get api user cache key.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetApiUserCacheKey(string userName, string tenant)
        {
            return $"{userName}:{tenant}:ApiUser";
        }
        /// <summary>
        /// The get api user cache key.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetApiUserCacheKey(Guid userId, string tenant)
        {
            return $"{userId}:{tenant}:ApiUser";
        }
        /// <summary>
        /// The encode password.
        /// </summary>
        /// <param name="pass">
        /// The pass.
        /// </param>
        /// <param name="salt">
        /// The salt.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string EncodePassword(string pass, string salt)
        {
            var passwordBytes = Encoding.Unicode.GetBytes(pass);
            var saltBytes = Convert.FromBase64String(salt);
            var combinedBytes = new byte[saltBytes.Length + passwordBytes.Length];

            Buffer.BlockCopy(saltBytes, 0, combinedBytes, 0, saltBytes.Length);
            Buffer.BlockCopy(passwordBytes, 0, combinedBytes, saltBytes.Length, passwordBytes.Length);

            var sha1 = SHA1.Create();
            var hash = sha1.ComputeHash(combinedBytes);

            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// The get advantage user rights cache key.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetAdvantageUserRightsCacheKey(ApiUser user)
        {
            return $"{user.TenantServerName}:{user.TenantDatabaseName}:{user.UserId}:UserRights";
        }

        /// <summary>
        /// The flatten api user rights.
        /// </summary>
        /// <param name="permissionList">
        /// The permission list.
        /// </param>
        /// <returns>
        /// The list of flatten api user rights.
        /// </returns>
        private static List<string> FlattenApiUserRights(IEnumerable<UserRights> permissionList)
        {
            var flattenedRoles = new List<string>();
            foreach (var rights in permissionList)
            {
                foreach (var accessRight in rights.Modules)
                {
                    var read = $"{accessRight.Name.ToUpper()}-{ApiUserRights.READ}";
                    var create = $"{accessRight.Name.ToUpper()}-{ApiUserRights.CREATE}";
                    var modify = $"{accessRight.Name.ToUpper()}-{ApiUserRights.MODIFY}";
                    var delete = $"{accessRight.Name.ToUpper()}-{ApiUserRights.DELETE}";
                    var admin = $"{accessRight.Name.ToUpper()}-{ApiUserRights.ADMIN}";

                    switch (accessRight.Level.ToLower())
                    {
                        case "read":
                            flattenedRoles.Add(read);
                            break;
                        case "create":
                            flattenedRoles.AddRange(new[] { read, create });
                            break;
                        case "modify":
                            flattenedRoles.AddRange(new[] { read, create, modify });
                            break;
                        case "delete":
                            flattenedRoles.AddRange(new[] { read, create, modify, delete });
                            break;
                        case "admin":
                            flattenedRoles.AddRange(new[] { read, create, modify, delete, admin });
                            break;
                    }
                }
            }
            return flattenedRoles.Distinct().ToList();
        }


        /// <summary>
        /// The get advantage api rights.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The List of UserRights and error if any
        /// </returns>
        private (List<UserRights> results, string error) GetAdvantageApiRights(ApiUser user)
        {
            var userRightsCacheKey = GetAdvantageUserRightsCacheKey(user);
            try
            {
                var userRights = SimpleCache.GetOrAddExisting(
                    userRightsCacheKey,
                    () =>
                        {
                            var option = new DbContextOptionsBuilder<AdvantageContext>();
                            option.UseSqlServer(user.BuildConnectionString());
                            IAdvantageDbContext advantageDbContext = new AdvantageDbContext();
                            advantageDbContext.SetContext(new AdvantageContext(option.Options), SimpleCacheValues.KEY);

                            IRepository<SyUsersRolesCampGrps> userRolesRepository = advantageDbContext.GetRepositoryForEntity<SyUsersRolesCampGrps>();
                            var userRoles = userRolesRepository.Get(x => x.UserId == user.UserId).Select(x => x.Role.SysRole).ToList();
                            return userRoles;
                        },
                    CacheExpiration.SHORT);

                return (userRights.ToList().Select(item => new RolePermission(item).GetUserRights()).ToList(), null);
            }
            catch (Exception e)
            {
                e.TrackException();
                return (null, $"{ApiMsgs.USER_RIGHTS_FAILURE} {e.Message}");
            }
        }

        /// <summary>
        /// The validate login.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// A dictionary of string and objects with the results of the validate login
        /// </returns>
        private (Dictionary<string, object> results, string error) ValidateLogin(string username, string password, string tenant)
        {
            var apiUserCacheKey = GetApiUserCacheKey(username, tenant);
            try
            {
                var user = SimpleCache.GetOrAddExisting(
                    apiUserCacheKey,
                    () => this.GetUser(username, tenant),
                    CacheExpiration.SHORT);
                if (user == null)
                {
                    return (null, $"{ApiMsgs.USER_NOT_FOUND}:{username}");
                }

                var hashedPass = EncodePassword(password, user.PasswordSalt);
                if (hashedPass != user.Password)
                {
                    return (results: null, error: $"{ApiMsgs.PASSWORD_FAILURE} {username}");
                }

                var advRoles = this.GetAdvantageApiRights(user);

                if (advRoles.error != null)
                {
                    return (results: null, error: advRoles.error);
                }

                var token = new AuthToken(
                    username,
                    this.authServiceSettings.Issuer,
                    this.authServiceSettings.Audience,
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddMinutes(this.authServiceSettings.TokenLifetime),
                    FlattenApiUserRights(advRoles.results),
                    user.UserId.ToString(),
                    tenant).ToDictionary();

                return (results: token, error: null);
            }
            catch (Exception e)
            {
                e.TrackException();
                return (results: null, error: e.ToString());
            }
        }

        /// <summary>
        /// The validate login.
        /// </summary>
        /// <param name="userId">
        /// The username.
        /// </param>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <returns>
        /// A dictionary of string and objects with the results of the validate login
        /// </returns>
        private (Dictionary<string, object> results, string error) ValidateLogin(Guid userId, string tenant)
        {
            var apiUserCacheKey = GetApiUserCacheKey(userId, tenant);
            try
            {
                var user = SimpleCache.GetOrAddExisting(
                    apiUserCacheKey,
                    () => this.GetUser(userId, tenant),
                    CacheExpiration.SHORT);
                if (user == null)
                {
                    return (null, $"{ApiMsgs.USER_NOT_FOUND}:{userId}");
                }

                var advRoles = this.GetAdvantageApiRights(user);

                if (advRoles.error != null)
                {
                    return (results: null, error: advRoles.error);
                }

                var token = new AuthToken(
                    user.Email,
                    this.authServiceSettings.Issuer,
                    this.authServiceSettings.Audience,
                    DateTime.UtcNow,
                    DateTime.UtcNow.AddMinutes(this.authServiceSettings.TokenLifetime),
                    FlattenApiUserRights(advRoles.results),
                    user.UserId.ToString(),
                    tenant).ToDictionary();

                return (results: token, error: null);
            }
            catch (Exception e)
            {
                e.TrackException();
                return (results: null, error: e.ToString());
            }
        }

        /// <summary>
        /// The create token.
        /// </summary>
        /// <param name="claims">
        /// The claims.
        /// </param>
        /// <returns>
        /// The <see cref="TokenResponse"/>.
        /// </returns>
        private TokenResponse CreateToken(Dictionary<string, object> claims)
        {
            var response = new TokenResponse();
            try
            {
                response.Token = JWT.Encode(
                    claims,
                    Encoding.ASCII.GetBytes(this.authServiceSettings.SecretKey),
                    JwsAlgorithm.HS256);
                response.Message = ApiMsgs.TOKEN_SUCCESS;
            }
            catch (Exception e)
            {
                e.TrackException();
                this.logger.LogError(e.ToString());
                response.Message = ApiMsgs.CREATE_TOKEN_FAILURE;
            }

            return response;
        }
    }
}