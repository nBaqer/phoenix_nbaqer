﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TenantService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System.Linq;

    using AutoMapper;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    /// <summary>
    /// The TenantService type.
    /// </summary>
    public class TenantService : ITenantService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;


        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;

        

        /// <summary>
        /// Initializes a new instance of the <see cref="TenantService"/> class. 
        /// </summary>
        /// <param name="tenantDb">
        /// The tenant db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public TenantService(ITenantDbContext tenantDb, IMapper mapper)
        {
            this.tenantDb = tenantDb;
            this.mapper = mapper;
        }


        /// <summary>
        /// The AspnetMembership repository.
        /// </summary>
        private IRepository<Tenant> TenantRepository => this.tenantDb.GetRepositoryForEntity<Tenant>();

        /// <summary>
        /// The get tenant id.
        /// </summary>
        /// <param name="tenantName">
        /// The tenant name.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int GetTenantId(string tenantName)
        {
            var tenant = this.TenantRepository
                .Get(_ => _.TenantName.ToLower().Trim() == tenantName.ToLower().Trim())
                .FirstOrDefault();

            return tenant != null ? tenant.TenantId : 0;
        }
    }
}
