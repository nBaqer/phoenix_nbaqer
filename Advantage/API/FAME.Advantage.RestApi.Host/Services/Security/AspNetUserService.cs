﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AspNetUserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IAspNetUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The AspNetUserService type.
    /// </summary>
    public class AspNetUserService : IAspNetUserService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;


        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;



        /// <summary>
        /// Initializes a new instance of the <see cref="AspNetUserService"/> class.
        /// </summary>
        /// <param name="tenantDb">
        /// The tenant db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public AspNetUserService(ITenantDbContext tenantDb, IMapper mapper)
        {
            this.tenantDb = tenantDb;
            this.mapper = mapper;
        }


        /// <summary>
        /// The AspnetMembership repository.
        /// </summary>
        private IRepository<AspnetUsers> AspnetUserRepository => this.tenantDb.GetRepositoryForEntity<AspnetUsers>();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="AspNetUser"/>.
        /// </returns>
        public async Task<AspNetUser> Create(AspNetUser user)
        {
            AspNetUser result = new AspNetUser();
            try
            {
                //user.UserId = Guid.NewGuid();
                user.IsAnonymous = false;
                user.UserName = user.UserName;
                user.UserNameLowerCase = user.UserName.ToLower();
                var userEntity = this.mapper.Map<AspnetUsers>(user);
                userEntity.LastActivityDate = DateTime.Now;
                var userEntityCreated = await this.AspnetUserRepository.CreateAsync(userEntity);
                this.tenantDb.SaveChanges();

                if (userEntityCreated != null)
                {
                    result = this.mapper.Map<AspNetUser>(userEntityCreated);
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = ApiMsgs.SAVE_UNSUCCESSFULL;
            }

            return result;
        }
    }
}
