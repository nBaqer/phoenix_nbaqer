﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantUserService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TenantUserService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Services.Interfaces.Security
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Interfaces;
    using Fame.EFCore.Tenant.Entities;
    using Fame.EFCore.Tenant.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The TenantUserService type.
    /// </summary>
    public class TenantUserService : ITenantUserService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;


        /// <summary>
        /// The tenant db.
        /// </summary>
        private readonly ITenantDbContext tenantDb;


        /// <summary>
        /// The application service.
        /// </summary>
        private readonly IAspNetApplicationService applicationService;

        /// <summary>
        /// The membership service.
        /// </summary>
        private IAspNetMembershipService membershipservice;

        /// <summary>
        /// Initializes a new instance of the <see cref="TenantUserService"/> class. 
        /// </summary>
        /// <param name="tenantDb">
        /// The tenant db.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="applicationService">
        /// </param>
        /// <param name="membershipservice">
        /// The membershipservice.
        /// </param>
        public TenantUserService(ITenantDbContext tenantDb, IMapper mapper, IAspNetApplicationService applicationService, IAspNetMembershipService membershipservice)
        {
            this.tenantDb = tenantDb;
            this.mapper = mapper;
            this.applicationService = applicationService;
            this.membershipservice = membershipservice;
        }


        /// <summary>
        /// The AspnetMembership repository.
        /// </summary>
        private IRepository<TenantUsers> TenantUserRepository => this.tenantDb.GetRepositoryForEntity<TenantUsers>();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<TenantUser> Create(TenantUser model)
        {
            TenantUser result = new TenantUser();
            try
            {
                var tenantUserEntity = this.mapper.Map<TenantUsers>(model);
                var userEntityCreated = await this.TenantUserRepository.CreateAsync(tenantUserEntity);
                await this.TenantUserRepository.SaveAsync();

                if (userEntityCreated != null)
                {
                    result = this.mapper.Map<TenantUser>(userEntityCreated);
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = ApiMsgs.SAVE_UNSUCCESSFULL;
            }

            return result;
        }

        /// <summary>
        /// The get tenat user.
        /// </summary>
        /// <param name="tenantId">
        /// The tenant id.
        /// </param>
        /// <param name="userId">
        /// The user id.
        /// </param>
        /// <returns>
        /// The <see cref="TenantUser"/>.
        /// </returns>
        public TenantUser GetTenantUser(int tenantId, Guid userId)
        {
            var tenantUser = this.TenantUserRepository
                .Get(tu => tu.UserId == userId && tu.TenantId == tenantId)
                .FirstOrDefault();

            if (tenantUser != null)
            {
                TenantUser result = this.mapper.Map<TenantUser>(tenantUser);
                return result;
            }

            return null;
        }
    }
}
