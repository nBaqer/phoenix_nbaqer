﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppStudentSummaryService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the KlassAppStudentSummaryService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.KlassApp
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.KlassApp;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.KlassApp;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Maintenance.Wapi;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The klass app student summary service.
    /// </summary>
    public class KlassAppService : IKlassAppService
    {

        /// <summary>
        /// The mapper is used to map the Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The wapi settings service.
        /// </summary>
        private readonly IWapiServiceService wapiSettingsService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The attendance service.
        /// </summary>
        private readonly IAttendanceService attendanceService;

        /// <summary>
        /// The student service.
        /// </summary>
        private readonly IStudentService studentService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="KlassAppService"/> class. 
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage Context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="wapiSettingsService">
        /// The wapi Settings Service.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="attendanceService">
        /// The attendance Service.
        /// </param>
        /// <param name="studentService">
        /// The student Service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        public KlassAppService(
            IAdvantageDbContext advantageContext,
            IMapper mapper,
            IWapiServiceService wapiSettingsService,
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService,
                                             IAttendanceService attendanceService,
                                             IStudentService studentService,
                                             IEnrollmentService enrollmentService
                                             )
        {
            this.advantageContext = advantageContext;
            this.mapper = mapper;
            this.wapiSettingsService = wapiSettingsService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.attendanceService = attendanceService;
            this.studentService = studentService;
            this.enrollmentService = enrollmentService;
        }

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.advantageContext.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The conversion attendance repository.
        /// </summary>
        private IRepository<AtConversionAttendance> ConversionAttendanceRepository =>
            this.advantageContext.GetRepositoryForEntity<AtConversionAttendance>();

        /// <summary>
        /// The student clock attendance repository.
        /// </summary>
        private IRepository<ArStudentClockAttendance> StudentClockAttendanceRepository =>
            this.advantageContext.GetRepositoryForEntity<ArStudentClockAttendance>();

        /// <summary>
        /// The Program repository.
        /// </summary>
        private IRepository<ArPrograms> ProgramRepository =>
                  this.advantageContext.GetRepositoryForEntity<ArPrograms>();

        /// <summary>
        /// The Program Version repository.
        /// </summary>
        private IRepository<ArPrgVersions> ProgramVersionRepository =>
                         this.advantageContext.GetRepositoryForEntity<ArPrgVersions>();

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository =>
                         this.advantageContext.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The status repository.
        /// </summary>
        private IRepository<SySysStatus> SysStatusRepository =>
                         this.advantageContext.GetRepositoryForEntity<SySysStatus>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// The klass app configuration repository.
        /// </summary>
        private IRepository<SyKlassAppConfigurationSetting> KlassAppConfigurationRepository => this.advantageContext.GetRepositoryForEntity<SyKlassAppConfigurationSetting>();

        /// <summary>
        /// The get students to sync.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IListActionResult<KlassAppSync>> GetStudentsToSync()
        {
            return await Task.Run(
                       async () =>
                           {
                               var results = new ListActionResult<KlassAppSync>();

                               try
                               {

                                   var wapiSettings = this.wapiSettingsService.GetWapiSettings(WapiServiceOperations.KlassAppService);

                                   if (wapiSettings.ResultStatus == Enums.ResultStatus.NotFound)
                                   {
                                       results.ResultStatus = Enums.ResultStatus.Error;
                                       results.ResultStatusMessage = ApiMsgs.WapiServiceSettingNotFound.Replace("WapiServiceSetting", WapiServiceOperations.KlassAppService);
                                       return results;
                                   }

                                   var klassAppEnabledByCampus = await this.systemConfigurationAppSettingService.GetAppSettingValues(ConfigurationAppSettingsKeys.KlassAppCampusToConsider);

                                   if (klassAppEnabledByCampus == null || klassAppEnabledByCampus.All(x => x.Value != ConfigurationAppSettingsValues.Yes))
                                   {
                                       results.ResultStatus = Enums.ResultStatus.Error;
                                       results.ResultStatusMessage = ApiMsgs.KlassAppNotConfiguredForCampuses;
                                       return results;
                                   }

                                   bool isAllCampuses = false;
                                   isAllCampuses = klassAppEnabledByCampus.All(
                                       x => x.Value == ConfigurationAppSettingsValues.Yes && x.CampusId == null);

                                   List<Guid> campuses = new List<Guid>();

                                   if (!isAllCampuses)
                                   {
                                       campuses = klassAppEnabledByCampus
                                           .Where(x => x.Value == ConfigurationAppSettingsValues.Yes && x.CampusId != null)
                                           .Select(x => x.CampusId.Value).ToList();
                                   }

                                   DateTime? previousExecution = wapiSettings.DateLastExecution;

                                   var enrollments = await this.EnrollmentRepository.Get(t => (((isAllCampuses == false && campuses.Any(e => e == t.CampusId)) || isAllCampuses)
                                                                                         && t.StatusCode.SysStatus.InSchool != null && t.StatusCode.SysStatus.InSchool == 1)
                                                                                         && (t.ModDate > previousExecution
                                                                                      || t.ArResults.Any(c => c.ModDate != null && c.ModDate > previousExecution)
                                                                                      || t.AtAttendance.Any(
                                                                                          c => c.AttendanceDate != null && c.AttendanceDate > previousExecution)
                                                                                      || t.AtClsSectAttendance.Any(c => c.ModDate > previousExecution)
                                                                                      || t.ArExternshipAttendance.Any(c => c.ModDate > previousExecution)
                                                                                      || t.ArGrdBkResults.Any(c => c.ModDate > previousExecution)
                                                                                      || t.Lead.ModDate > previousExecution
                                                                                      || t.Lead.AdLeadAddresses.Any(y => y.ModDate > previousExecution)
                                                                                      || t.Lead.AdLeadPhone.Any(y => y.ModDate > previousExecution)))
                                                                                      .Include(x => x.Campus)
                                                                                      .Include(x => x.Lead).ThenInclude(x => x.GenderNavigation)
                                                                                      .AsNoTracking().ToListAsync();

                                   /*
                                    * TODO
                                    * Create the FK on AtConversionAttendance and ArStudentClockAttendance for the StuEnrollId since both tables are missing
                                    * the foreign keys. Update the code above to include the two tables reference on the Entities are updated and remove the code below that
                                    * queries the table separately. This was done due to time constraint getting this service code out.
                                    */

                                   var enrollmentsInConversionAttendance = this.ConversionAttendanceRepository
                                       .Get(x => x.ModDate > previousExecution)
                                       .Select(x => x.StuEnrollId).Distinct().ToList();

                                   var enrollmentsInClockAttendance = this.StudentClockAttendanceRepository
                                       .Get(x => x.ModDate > previousExecution)
                                       .Select(x => x.StuEnrollId).Distinct().ToList();

                                   var enrollmentIdsFromAttendance =
                                       enrollmentsInConversionAttendance.Union(enrollmentsInClockAttendance).Distinct();

                                   var enrollmentsFromAttendance = this.EnrollmentRepository.Get(x => ((isAllCampuses == false && campuses.Any(e => e == x.CampusId)) || isAllCampuses) &&
                                                                                                      enrollmentIdsFromAttendance.Any(e => e == x.StuEnrollId))
                                                                                                      .Include(x => x.Campus)
                                                                                                      .Include(x => x.Lead)
                                                                                                      .ThenInclude(x => x.GenderNavigation).ToList();

                                   var enrollmentsToSync = enrollments.Union(enrollmentsFromAttendance).GroupBy(enrollment => enrollment.StuEnrollId).Select(i => i.First()).ToList();

                                   foreach (var enrollment in enrollmentsToSync)
                                   {

                                       var studentContactSummary = await this.studentService.GetStudentContactInfo(enrollment.StuEnrollId);

                                       /*
                                        * TODO
                                        * Get the Credits Attempted and Credits Earned 
                                        */
                                       KlassAppSync klassAppStudentSummary = new KlassAppSync();
                                       klassAppStudentSummary.FirstName = enrollment.Lead.FirstName;
                                       klassAppStudentSummary.LastName = enrollment.Lead.LastName;
                                       klassAppStudentSummary.StudentNumber = enrollment.Lead.StudentNumber;
                                       klassAppStudentSummary.EnrollmentId = enrollment.StuEnrollId.ToString().Trim().ToUpper();
                                       klassAppStudentSummary.CampusId = enrollment.CampusId.ToString().Trim().ToUpper(); ;
                                       klassAppStudentSummary.SchoolName = enrollment.Campus.SchoolName?.ToString().Trim() ?? string.Empty;
                                       klassAppStudentSummary.TimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                       if (studentContactSummary != null)
                                           klassAppStudentSummary.Email = studentContactSummary.Email;

                                       results.Add(klassAppStudentSummary);
                                   }

                                   results.ResultStatus = Enums.ResultStatus.Success;
                                   results.ResultStatusMessage = string.Empty;

                                   return results;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   results.ResultStatus = Enums.ResultStatus.Error;
                                   results.ResultStatusMessage = e.Message;
                                   return results;
                               }
                           });
        }

        /// <summary>
        /// Returns student summary data given enrollmentId
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<StudentSummary>> GetStudentSummaryByEnrollmentId(string enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<StudentSummary>();

                           try
                           {
                               var enrollment = this.EnrollmentRepository.Get()
                               .Include(x => x.StatusCode)
                               .Include(x => x.Campus)
                               .Include(x => x.PrgVer).ThenInclude(x => x.Prog)
                               .Include(x => x.Lead).ThenInclude(x => x.GenderNavigation)
                               .Where(a => a.StuEnrollId.ToString() == enrollmentId).AsNoTracking().FirstOrDefault();

                               if (enrollment == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"Student with EnrollmentId of {enrollmentId} not found.";
                                   return result;
                               }

                               var enrollmentSummary = await this.enrollmentService.GetEnrollmentProgramSummary(enrollment.StuEnrollId);
                               var attendanceSummary = await this.attendanceService.CalculateAttendance(enrollment.StuEnrollId, true, true, true);
                               var studentContactSummary = await this.studentService.GetStudentContactInfo(enrollment.StuEnrollId);

                               StudentSummary studentSummary = new StudentSummary();
                               studentSummary.FirstName = enrollment.Lead.FirstName;
                               studentSummary.LastName = enrollment.Lead.LastName;
                               studentSummary.StudentNumber = enrollment.Lead.StudentNumber;
                               studentSummary.Role = "student";
                               studentSummary.BirthDate = enrollment.Lead.BirthDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.Gender = enrollment.Lead.GenderNavigation.GenderDescrip;
                               studentSummary.StartDate = enrollment.StartDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.ExpectedGraduationDate = enrollment.ExpGradDate?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                               studentSummary.SchoolName = enrollment.Campus.SchoolName.Trim();
                               studentSummary.ProgramDescription = enrollment.PrgVer.Prog.ProgDescrip;
                               studentSummary.ProgramVersionDescription = enrollment.PrgVer.PrgVerDescrip;
                               studentSummary.Status = enrollment.StatusCode.StatusCodeDescrip;

                               studentSummary.EnrollmentId = enrollment.StuEnrollId.ToString().Trim().ToUpper();
                               studentSummary.CampusId = enrollment.Campus.CampusId.ToString().Trim().ToUpper();
                               studentSummary.ProgramId = enrollment.PrgVer.Prog.ProgId.ToString().Trim().ToUpper();
                               studentSummary.ProgramVersionId = enrollment.PrgVer.PrgVerId.ToString().Trim().ToUpper();
                               studentSummary.StatusCodeId = enrollment.StatusCode.StatusCodeId.ToString().Trim().ToUpper();

                               if (attendanceSummary != null)
                               {
                                   studentSummary.TotalHours = attendanceSummary.ActualHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.AbsentHours = attendanceSummary.AbsentHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.MakeupHours = attendanceSummary.MakeupHours.ToString("N2", CultureInfo.InvariantCulture);
                                   studentSummary.LastDateAttended = attendanceSummary.LastDateAttended?.ToString("yyyy-MM-dd HH:mm:ss") ?? string.Empty;
                                   studentSummary.AttendancePercentage = attendanceSummary.AttendancePercentage.ToString("0.00") ?? string.Empty;
                               }

                               if (enrollmentSummary != null)
                               {
                                   studentSummary.OverallGPA = enrollmentSummary.OverallGPAString;
                               }

                               if (studentContactSummary != null)
                               {
                                   studentSummary.Phone = studentContactSummary.Phone;
                                   studentSummary.Address = studentContactSummary.Address;
                                   studentSummary.City = studentContactSummary.City;
                                   studentSummary.State = studentContactSummary.State;
                                   studentSummary.Zip = studentContactSummary.Zip;
                                   studentSummary.Email = studentContactSummary.Email;
                                   studentSummary.PhoneOther = studentContactSummary.PhoneOther;
                               }

                               result.Result = studentSummary;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               result.AsString = $"Student summary for student {studentSummary.FirstName} {studentSummary.LastName} - {studentSummary.EnrollmentId} - {DateTime.Now}";
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        /// <summary>
        /// The get configuration settings for klass app.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<KlassAppConfigurationSetting>> GetConfigurationSettings()
        {
            return await Task.Run(
                       () =>
                       {
                           return SimpleCache.GetOrAddExisting(
                               $"{this.CacheKeyFirstSegment}:KlassAppConfiguration",
                               () =>
                               {
                                   var result = this.mapper.Map<List<KlassAppConfigurationSetting>>(this.KlassAppConfigurationRepository.Get().Include(x => x.KlassOperationType).Include(x => x.KlassEntity));
                                   return result;
                               },
                               CacheExpiration.VERY_LONG);
                       });
        }
    }
}
