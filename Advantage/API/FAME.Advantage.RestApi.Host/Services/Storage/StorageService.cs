﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StorageService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StorageService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Storage
{
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using static FAME.Advantage.RestApi.DataTransferObjects.Common.Enums;
    using FileConfigurationFeature = DataTransferObjects.Common.Enums.FileConfigurationFeature;
    using FileStorageType = DataTransferObjects.Common.Enums.FileStorageType;

    /// <summary>
    /// The storage service.
    /// </summary>
    public class StorageService : IStorageService
    {

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageContext;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The encryption service
        /// </summary>
        private readonly IEncryptionService encryptionService;

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.advantageContext.GetTenantUser();

        /// <summary>
        /// The document repository.
        /// </summary>
        private IRepository<SyDocumentHistory> SyDocumentHistory => this.advantageContext.GetRepositoryForEntity<SyDocumentHistory>();

        /// <summary>
        /// The SyCampusFileConfiguration repository.
        /// </summary>
        private IRepository<SyCampusFileConfiguration> SyCampusFileConfiguration => this.advantageContext.GetRepositoryForEntity<SyCampusFileConfiguration>();

        /// <summary>
        /// The SyCustomFeatureFileConfiguration repository.
        /// </summary>
        private IRepository<SyCustomFeatureFileConfiguration> SyCustomFeatureFileConfiguration => this.advantageContext.GetRepositoryForEntity<SyCustomFeatureFileConfiguration>();

        /// <summary>
        /// The SyFileConfigurationFeatures repository.
        /// </summary>
        private IRepository<SyFileConfigurationFeatures> SyFileConfigurationFeatures => this.advantageContext.GetRepositoryForEntity<SyFileConfigurationFeatures>();

        /// <summary>
        /// The SyCmpGrpCm repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> SyCmpGrpCmps => this.advantageContext.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// Storage Settings
        /// </summary>
        private StorageSettings StorageSettings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageService"/> class. 
        /// </summary>
        /// <param name="advantageContext">
        /// The advantage Context.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="statusService">
        /// The statusService.
        /// </param>
        /// <param name="encryptionService">
        /// The encryption service
        /// </param>
        public StorageService(
            IAdvantageDbContext advantageContext,
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService,
            IStatusesService statusService,
            IEncryptionService encryptionService
            )
        {
            this.advantageContext = advantageContext;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.statusService = statusService;
            this.encryptionService = encryptionService;
        }

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageContext.GetSimpleCacheConnectionString();

        /// <summary>
        /// Delete file.
        /// </summary>
        public async Task<ActionResult<string>> DeleteFile(DeleteFileParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                var studentId = requestData.StudentId;
                var fileName = requestData.FileName;

                //if document id is provided 
                if (requestData.DocumentId.HasValue)
                {
                    var document = this.SyDocumentHistory.Get().Where(a => a.DocumentId == requestData.DocumentId).Select(a => new { a.FileName, a.FileExtension }).FirstOrDefault();

                    if (document == null)
                    {
                        rv.ResultStatus = ResultStatus.Error;
                        rv.ResultStatusMessage = "Document Id record provided could not be found in the database. Document Id : " + requestData.DocumentId;
                        return rv;
                    }

                    fileName = document.FileName;
                }

                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId, fileName);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);
                return await storageMethod.DeleteFile(filePath);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Explore directory.
        /// </summary>
        public async Task<ActionResult<List<FileInformation>>> ExploreDirectory(ExploreDirectoryParams requestData)
        {
            var rv = new ActionResult<List<FileInformation>>();
            try
            {
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);
                return await storageMethod.ExploreDirectory(filePath);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Get File async.
        /// </summary>
        public async Task<ActionResult<FileGetResult>> GetFile(string token, FileConfigurationFeature featureType, string fileName, Guid campusId, Guid? studentId = null)
        {
            var rv = new ActionResult<FileGetResult>();
            try
            {
                var filePath = await this.BuildFilePath(featureType, campusId, studentId, fileName);
                var storageMethod = await this.StorageMethodFactory(featureType, campusId);
                return await storageMethod.GetFile(filePath);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Get File
        /// </summary>
        public async Task<ActionResult<FileGetResult>> GetFile(GetFileParams requestData)
        {
            var rv = new ActionResult<FileGetResult>();
            try
            {
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId, requestData.FileName);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);
                return await storageMethod.GetFile(filePath);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Rename File
        /// </summary>
        public async Task<ActionResult<string>> RenameFile(RenameFileParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);
                return await storageMethod.RenameFile(filePath, requestData.NewFileName);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Update File
        /// </summary>
        public async Task<ActionResult<string>> UpdateFile(UpdateFileParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId, requestData.ExistingFileName);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);

                using (var memoryStream = new MemoryStream())
                {
                    await requestData.FileToUpload.CopyToAsync(memoryStream);
                    return await storageMethod.UpdateFile(filePath, memoryStream.ToArray());
                }
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Upload File
        /// </summary>
        public async Task<ActionResult<string>> UploadFile(UploadFileParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId, requestData.FileToUpload.FileName);
                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);

                using (var memoryStream = new MemoryStream())
                {
                    await requestData.FileToUpload.CopyToAsync(memoryStream);
                    await storageMethod.CreateDirectory(await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, requestData.StudentId));
                    return await storageMethod.UploadFile(filePath, memoryStream.ToArray());
                }
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Buld storage path given parameters
        /// </summary>
        public async Task<ActionResult<BuildPathResult>> BuildStoragePath(BuildPathParams requestData)
        {
            var rv = new ActionResult<BuildPathResult>();
            try
            {
                rv.Result = new BuildPathResult()
                {
                    Path = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId.Value, requestData.StudentId, requestData.FileName)
                };
                rv.ResultStatus = Enums.ResultStatus.Success;
            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return rv;
        }

        /// <summary>
        /// Move file
        /// </summary>
        public async Task<ActionResult<string>> MoveFile(MoveFileParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Returns IStorageMethod instance
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public async Task<IStorageMethod> StorageMethodFactory(FileConfigurationFeature feature, Guid campusId)
        {
            this.StorageSettings = this.StorageSettings ?? await this.GetStorageSettingByCampus(feature, campusId);
            switch (this.StorageSettings.FileStorageType)
            {
                case FileStorageType.Cloud:
                    return new AzureCloudStorageMethod(this.StorageSettings);
                case FileStorageType.File:
                    return new FileSystemMethod();
                case FileStorageType.Network:
                    return new NetworkShareMethod(this.StorageSettings.UserName, this.StorageSettings.Password, this.StorageSettings.Path);
                default:
                    return new AzureCloudStorageMethod(this.StorageSettings);
            }
        }

        /// <summary>
        /// Get Storage Setting by campus
        /// </summary>
        public async Task<StorageSettings> GetStorageSettingByCampus(FileConfigurationFeature storageFeatureType, Guid campusId)
        {
            var syCmpGrpCmpsRepo = this.SyCmpGrpCmps.Get();
            var campGrpIds = syCmpGrpCmpsRepo.Where(i => i.CampusId == campusId).Select(a => a.CampGrpId);
            var settingsQuery = this.SyCampusFileConfiguration
                                    .Get()
                                    .Include(a => a.SyCustomFeatureFileConfiguration)
                                    .Where(a => campGrpIds.Contains(a.CampusGroupId));

            var settings = await settingsQuery
                                .OrderBy(i => syCmpGrpCmpsRepo.Count(x => x.CampGrpId == i.CampusGroupId))
                                .ToListAsync();

            StorageSettings s = null;

            foreach (var setting in settings)
            {
                //check for all feature flag
                if (setting.AppliesToAllFeatures)
                {
                    s = new StorageSettings
                    {
                        CloudKey = setting.CloudKey,
                        FileStorageType = (FileStorageType)setting.FileStorageType,
                        Password = this.encryptionService.Decrypt(setting.Password), //must decrypt
                        UserName = setting.UserName,
                        Path = setting.Path
                    };
                    return s;
                }

                //check for feature specific settings
                var featureSpecificSetting = setting.SyCustomFeatureFileConfiguration.Where(a => a.FeatureId == (int)storageFeatureType).FirstOrDefault();
                if (featureSpecificSetting != null)
                {
                    s = new StorageSettings
                    {
                        CloudKey = featureSpecificSetting.CloudKey,
                        FileStorageType = (FileStorageType)featureSpecificSetting.FileStorageType,
                        Password = this.encryptionService.Decrypt(featureSpecificSetting.Password), //must decrypt
                        UserName = featureSpecificSetting.UserName,
                        Path = featureSpecificSetting.Path
                    };
                    return s;
                }
            }

            //if code reaches this point no settings found for this campus
            if (s == null)
                throw new Exception("Campus storage settings have not been setup for this campus.");

            return s;
        }

        /// <summary>
        /// Read file contents.
        /// </summary>
        public async Task<ActionResult<string>> ReadFileContents(ReadFileContentsParams requestData)
        {
            var rv = new ActionResult<string>();
            try
            {
                if (requestData.CampusId == Guid.Empty || string.IsNullOrEmpty(requestData.FileName))
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Incorrect parameters passed {requestData.CampusId.ToString()} {requestData.FileName.ToString()} {((int)requestData.FeatureType).ToString()}";
                    return rv;
                }

                var storageMethod = await this.StorageMethodFactory(requestData.FeatureType, requestData.CampusId);
                var filePath = await this.BuildFilePath(requestData.FeatureType, requestData.CampusId, null, requestData.FileName);
                return await storageMethod.ReadFileContentsAsync(filePath);
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Build File Path
        /// </summary>
        public async Task<string> BuildFilePath(FileConfigurationFeature storageFeatureType, Guid campusId, Guid? studentId = null, string fileName = null, string explicitFullPath = null)
        {
            this.StorageSettings = this.StorageSettings ?? await this.GetStorageSettingByCampus(storageFeatureType, campusId);

            switch (storageFeatureType)
            {
                case FileConfigurationFeature.DOC:
                    return Path.Combine(this.StorageSettings.Path, campusId.ToString(), "StudentDocs", studentId.HasValue ? studentId.ToString() : "", fileName ?? "");
                case FileConfigurationFeature.PHOTO:
                    return Path.Combine(this.StorageSettings.Path, campusId.ToString(), "Photos", studentId.HasValue ? studentId.ToString() : "", fileName ?? "");
                case FileConfigurationFeature.LI:
                    return Path.Combine(this.StorageSettings.Path, campusId.ToString(), "LeadImport", fileName ?? "");
                case FileConfigurationFeature.TCA:
                    return Path.Combine(this.StorageSettings.Path, campusId.ToString(), "TimeClockArchive", fileName ?? "");
                case FileConfigurationFeature.R2T4:
                    return Path.Combine(this.StorageSettings.Path, campusId.ToString(), "R2T4", studentId.HasValue ? studentId.ToString() : "", fileName ?? "");
                case FileConfigurationFeature.TCI:
                    return Path.Combine(this.StorageSettings.Path, "", "", fileName ?? "");
                default:
                    return "";
            }
        }

        /// <summary>
        /// Given token authentication of user making request. Uses tenant to get locations
        /// </summary>
        public async Task<ActionResult<List<AutomatedTimeClockLocation>>> GetAutomatedTimeClockLocations()
        {

            var rv = new ActionResult<List<AutomatedTimeClockLocation>>();
            try
            {
                var timeClockAutomation = await this.systemConfigurationAppSettingService.GetAppSettingValues(ConfigurationAppSettingsKeys.AutomatedTimeClockImport);

                if (timeClockAutomation == null)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = "No configuration key found for automated time clock imports.";
                    return rv;
                }

                List<Guid> campusesWithAutomaticTimeClockImports = new List<Guid>();
                bool enabledForAllCampuses = false;

                enabledForAllCampuses = timeClockAutomation.All(x => x.Value == ConfigurationAppSettingsValues.Yes
                                                                && x.CampusId == null);

                if (!enabledForAllCampuses)
                {
                    campusesWithAutomaticTimeClockImports = timeClockAutomation
                        .Where(x => x.Value == ConfigurationAppSettingsValues.Yes && x.CampusId != null)
                        .Select(x => x.CampusId.Value).ToList();
                }
                else
                {
                    campusesWithAutomaticTimeClockImports = await this.advantageContext
                        .GetRepositoryForEntity<SyCampuses>()
                        .Get(x => x.Status.StatusCode == "A")
                        .Select(x => x.CampusId).ToListAsync();
                }

                var data = new List<AutomatedTimeClockLocation>();

                foreach (var campus in campusesWithAutomaticTimeClockImports)
                {
                    var settingsForCampus = await this.GetStorageSettingByCampus(FileConfigurationFeature.TCI, campus);
                    if (settingsForCampus != null)
                        data.Add(new AutomatedTimeClockLocation()
                        {
                            CampusId = campus,
                            Path = settingsForCampus.Path,
                            Username = settingsForCampus.UserName,
                            Password = settingsForCampus.Password
                        });
                }

                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.Result = data;
                rv.ResultStatusMessage = "Successfully returned campuses with automated time clock settings.";
                return rv;

            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                AppInsightExtensions.TrackException(e);
                return rv;
            }
        }
    }
}

