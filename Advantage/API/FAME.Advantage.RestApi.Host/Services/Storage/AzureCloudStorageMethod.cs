﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AzureCloudStorageMethod.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AzureCloudStorageMethod type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Storage
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The CloudStorageMethod implementation
    /// </summary>
    public class AzureCloudStorageMethod : IStorageMethod
    {
        private StorageSettings _settings { get; set; }
        public AzureCloudStorageMethod(StorageSettings settings)
        {
            this._settings = settings;
        }

        public Task<ActionResult<string>> CreateDirectory(string directoryPath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> DeleteDirectory(string directoryPath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> DeleteFile(string path)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<List<FileInformation>>> ExploreDirectory(string directoryPath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<FileGetResult>> GetFile(string filePath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> MoveDirectory(string sourceDirectoryPath, string targetDirectoryPath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> MoveFile(string sourceFilePath, string destinationFilePath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> RenameDirectory(string sourceDirectoryPath, string renamedDirectoryPath)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> RenameFile(string filePath, string newFileName)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> UpdateFile(string filePath, byte[] fileData)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> UploadFile(string filePath, byte[] fileData)
        {
            throw new NotImplementedException();
        }

        public Task<ActionResult<string>> ReadFileContentsAsync(string filePath)
        {
            throw new NotImplementedException();
        }
    }
}
