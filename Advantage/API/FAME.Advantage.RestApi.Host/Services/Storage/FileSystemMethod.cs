﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileSystemMethod.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the FileSystemMethod type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Storage
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// The FileSystemMethod implementation
    /// </summary>
    public class FileSystemMethod : IStorageMethod
    {
        /// <summary>
        /// Delete file
        /// </summary>
        public async Task<ActionResult<string>> DeleteFile(string path)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if file exists 
                if (!File.Exists(path))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.Result = "File not found.";
                    return await Task.FromResult(rv);
                }

                File.Delete(path);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.Result = "File deleted.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Explore directory
        /// </summary>
        public async Task<ActionResult<List<FileInformation>>> ExploreDirectory(string directoryPath)
        {
            var rv = new ActionResult<List<FileInformation>>();
            try
            {
                // Check if directory exists 
                if (!File.Exists(directoryPath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "Directory does not exist.";
                    return await Task.FromResult(rv);
                }

                string[] filePaths = Directory.GetFiles(directoryPath, "*.*");
                rv.Result = filePaths.ToList().Select(a => new FileInformation
                {
                    FilePath = a,
                    FileName = Path.GetFileName(a),
                    Extension = Path.GetExtension(a),
                    ModifiedDate = DateTime.Now
                }).ToList();

                rv.ResultStatus = Enums.ResultStatus.Success;
            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }


            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Explore directory
        /// </summary>
        public async Task<ActionResult<FileGetResult>> GetFile(string filePath)
        {
            var rv = new ActionResult<FileGetResult>();
            try
            {
                // Check if file exists 
                if (!File.Exists(filePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "File not found.";
                    return await Task.FromResult(rv);
                }

                rv.Result = new FileGetResult() { FileContents = File.ReadAllBytes(filePath) };
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "File has been read.";
            }
            catch (IOException ioExp)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ioExp.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Move file
        /// </summary>
        public async Task<ActionResult<string>> MoveFile(string sourceFilePath, string destinationFilePath)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if file exists 
                if (!File.Exists(sourceFilePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "File not found.";
                    return await Task.FromResult(rv);
                }

                File.Move(sourceFilePath, destinationFilePath);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "File has been moved.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Rename file
        /// </summary>
        public async Task<ActionResult<string>> RenameFile(string filePath, string newFileName)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if file exists 
                if (!File.Exists(filePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "File not found.";
                    return await Task.FromResult(rv);
                }

                var newFilePath = Path.Combine(Path.GetDirectoryName(filePath), newFileName + Path.GetExtension(filePath));
                File.Move(filePath, newFilePath);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "File has been renamed.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Update file
        /// </summary>
        public async Task<ActionResult<string>> UpdateFile(string filePath, byte[] fileData)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if file exists 
                if (!File.Exists(filePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "File not found.";
                    return await Task.FromResult(rv);
                }

                File.WriteAllBytes(filePath, fileData);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "File has been updated.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Write file
        /// </summary>
        public async Task<ActionResult<string>> UploadFile(string filePath, byte[] fileData)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if file exists 
                if (File.Exists(filePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = "File already exists with this name.";
                    return await Task.FromResult(rv);
                }

                File.WriteAllBytes(filePath, fileData);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "File has been uploaded successfully.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Creates directory
        /// </summary>
        public async Task<ActionResult<string>> CreateDirectory(string directoryPath)
        {
            var rv = new ActionResult<string>();
            try
            {
                Directory.CreateDirectory(directoryPath);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "Directory has been created.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Rename directory
        /// </summary>
        public async Task<ActionResult<string>> RenameDirectory(string sourceDirectoryPath, string renamedDirectoryPath)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if directory exists 
                if (!Directory.Exists(sourceDirectoryPath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "Source directory does not exist.";
                    return await Task.FromResult(rv);
                }

                Directory.Move(sourceDirectoryPath, renamedDirectoryPath);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "Directory has been renamed.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Delete directory
        /// </summary>
        public async Task<ActionResult<string>> DeleteDirectory(string directoryPath)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if directory exists 
                if (!Directory.Exists(directoryPath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "Directory does not exist.";
                    return await Task.FromResult(rv);
                }

                Directory.Delete(directoryPath, true);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "Directory and contents has been deleted.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Move directory
        /// </summary>
        public async Task<ActionResult<string>> MoveDirectory(string sourceDirectoryPath, string targetDirectoryPath)
        {
            var rv = new ActionResult<string>();
            try
            {
                // Check if directory exists 
                if (!Directory.Exists(sourceDirectoryPath))
                {
                    rv.ResultStatus = Enums.ResultStatus.NotFound;
                    rv.ResultStatusMessage = "Source directory does not exist.";
                    return await Task.FromResult(rv);
                }

                Directory.Move(sourceDirectoryPath, targetDirectoryPath);
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "Directory has been moved.";
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return await Task.FromResult(rv);
        }

        /// <summary>
        /// Move directory
        /// </summary>
        public async Task<ActionResult<string>> ReadFileContentsAsync(string filePath)
        {
            var rv = new ActionResult<string>();
            int attempts = 0;
            int maxAttempts = 6;
            string readContents = "";
            bool completed = false;

            try
            {
                // Check if directory exists 
                if (!File.Exists(filePath))
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = "File could not be found.";
                }

                while (!completed)
                {
                    try
                    {
                        using (StreamReader streamReader = new StreamReader(filePath, Encoding.UTF8))
                        {
                            readContents = await streamReader.ReadToEndAsync();
                            completed = true;
                        }
                    }
                    catch (Exception exc)
                    {
                        Thread.Sleep(5000);
                        attempts++;
                        if (attempts == maxAttempts)
                            throw exc;
                    }
                }

                rv.Result = readContents;
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = "Files contents have been successfully read.";
            }
            catch (Exception e)
            {
                AppInsightExtensions.TrackException(e);
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
            }

            return rv;
        }
    }
}
