﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NetworkShareMethod.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the NetworkShareMethod type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Storage
{
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Storage;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using SimpleImpersonation;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Security.Principal;
    using System.Threading.Tasks;

    /// <summary>
    /// The NetworkShareMethod implementation
    /// </summary>
    public class NetworkShareMethod : FileSystemMethod, IStorageMethod
    {
        private string username { get; set; }
        private string password { get; set; }
        private string domain { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="networkShareRootPath"></param>
        public NetworkShareMethod(string username, string password, string networkShareRootPath)
        {
            var txt = username.Split('\\');
            this.username = txt.Last() ?? "";
            this.domain = txt.Reverse().Skip(1).FirstOrDefault() ?? "";
            this.password = password;
        }

        public new async Task<ActionResult<string>> CreateDirectory(string directoryPath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.CreateDirectory(directoryPath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> DeleteDirectory(string directoryPath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.DeleteDirectory(directoryPath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> DeleteFile(string path)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.DeleteFile(path).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<List<FileInformation>>> ExploreDirectory(string directoryPath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.ExploreDirectory(directoryPath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<List<FileInformation>>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<FileGetResult>> GetFile(string filePath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.GetFile(filePath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<FileGetResult>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> MoveDirectory(string sourceDirectoryPath, string targetDirectoryPath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.MoveDirectory(sourceDirectoryPath, targetDirectoryPath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> MoveFile(string sourceFilePath, string destinationFilePath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.MoveFile(sourceFilePath, destinationFilePath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> ReadFileContentsAsync(string filePath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.ReadFileContentsAsync(filePath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> RenameDirectory(string sourceDirectoryPath, string renamedDirectoryPath)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.RenameDirectory(sourceDirectoryPath, renamedDirectoryPath).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> RenameFile(string filePath, string newFileName)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.RenameFile(filePath, newFileName).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> UpdateFile(string filePath, byte[] fileData)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.UpdateFile(filePath, fileData).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        public new async Task<ActionResult<string>> UploadFile(string filePath, byte[] fileData)
        {
            try
            {
                var credentials = new UserCredentials(domain, username, password);
                return Impersonation.RunAsUser(credentials, LogonType.Network, () =>
                {
                    return base.UploadFile(filePath, fileData).Result;
                });
            }
            catch (Exception e)
            {
                var rv = new ActionResult<string>();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }
    }
}
