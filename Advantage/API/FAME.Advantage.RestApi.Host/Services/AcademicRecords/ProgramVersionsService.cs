﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the program versions service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.CampusProgram;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Results;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.DataTransferObjects.Terms;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.StudentAccounts;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Common;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;

    using Unity.Interception.Utilities;

    /// <summary>
    /// The program versions service.
    /// </summary>
    public class ProgramVersionsService : IProgramVersionsService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The Enrollment Manager service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The Academic Calendar Service.
        /// </summary>
        private readonly IAcademicCalendarService academicCalendarService;

        /// <summary>
        /// The campus program version Service.
        /// </summary>
        private readonly ICampusProgramVersionService campusProgramVersionService;

        /// <summary> 
        /// The class section attendance service.
        /// </summary>
        private readonly IClassSectionAttendanceService classSectionAttendanceService;

        /// <summary>
        /// The class section meeting service.
        /// </summary>
        private readonly IClassSectionMeetingService classSectionMeetingService;

        /// <summary>
        /// The class section service.
        /// </summary>
        private readonly IClassSectionService classSectionService;

        /// <summary>
        /// The credit summary service.
        /// </summary>
        private readonly ICreditSummaryService creditSummaryService;

        /// <summary>
        /// The student clock attendance service.
        /// </summary>
        private readonly IStudentClockAttendanceService studentClockAttendanceService;

        /// <summary>
        /// The program schedule service.
        /// </summary>
        private readonly IProgramScheduleService programScheduleService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The transfer Grades Service.
        /// </summary>
        private readonly ITransferGradesService transferGradesService;

        /// <summary>
        /// The Course Details Service.
        /// </summary>
        private readonly ICourseDetailsService courseDetailsService;

        /// <summary>
        /// The mapper service.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The results service.
        /// </summary>
        private readonly IResultsService resultsService;

        /// <summary>
        /// The student awards service.
        /// </summary>
        private readonly IStudentAwardsService studentAwardsService;

        /// <summary>
        /// The term service.
        /// </summary>
        private readonly ITermService termService;

        /// <summary>
        /// The holiday Service.
        /// </summary>
        private readonly IHolidayService holidayService;

        /// <summary>
        /// The calculation period type service.
        /// </summary>
        private readonly ICalculationPeriodTypeService calculationPeriodTypeService;

        /// <summary>
        /// The User Service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionsService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        /// <param name="academicCalendarService">
        /// The academic calendar service.
        /// </param>
        /// <param name="campusProgramVersionService">
        /// The campus program version service.
        /// </param>
        /// <param name="programScheduleService">
        /// The program schedule service.
        /// </param>
        /// <param name="classSectionAttendanceService">
        /// The class section attendance service.
        /// </param>
        /// <param name="studentClockAttendanceService">
        /// The student clock attendance service.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system configuration app setting service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="transferGradesService">
        /// The transfer grades service.
        /// </param>
        /// <param name="courseDetailsService">
        /// The course details service.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="classSectionService">
        /// The class section service.
        /// </param>
        /// <param name="creditSummaryService">
        /// The credit summary service.
        /// </param>
        /// <param name="resultsService">
        /// The results service.
        /// </param>
        /// <param name="studentAwardsService">
        /// The student Awards Service.
        /// </param>
        /// <param name="termService">
        /// The term service.
        /// </param>
        /// <param name="classSectionMeetingService">
        /// The class section meeting service.
        /// </param>
        /// <param name="holidayService">
        /// The holiday Service.
        /// </param>
        /// <param name="calculationPeriodTypeService">
        /// The calculation period type service.
        /// </param>
        public ProgramVersionsService(
            IAdvantageDbContext context,
            IEnrollmentService enrollmentService,
            IAcademicCalendarService academicCalendarService,
            ICampusProgramVersionService campusProgramVersionService,
            IProgramScheduleService programScheduleService,
            IClassSectionAttendanceService classSectionAttendanceService,
            IStudentClockAttendanceService studentClockAttendanceService,
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService,
            IMapper mapper,
            ITransferGradesService transferGradesService,
            ICourseDetailsService courseDetailsService,
            IStatusesService statusService,
            IClassSectionService classSectionService,
            ICreditSummaryService creditSummaryService,
            IResultsService resultsService,
            IStudentAwardsService studentAwardsService,
            ITermService termService,
            IClassSectionMeetingService classSectionMeetingService,
            IHolidayService holidayService,
            ICalculationPeriodTypeService calculationPeriodTypeService, IUserService userService)
        {
            this.context = context;
            this.enrollmentService = enrollmentService;
            this.academicCalendarService = academicCalendarService;
            this.campusProgramVersionService = campusProgramVersionService;
            this.classSectionAttendanceService = classSectionAttendanceService;
            this.studentClockAttendanceService = studentClockAttendanceService;
            this.programScheduleService = programScheduleService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.mapper = mapper;
            this.transferGradesService = transferGradesService;
            this.courseDetailsService = courseDetailsService;
            this.statusService = statusService;
            this.classSectionService = classSectionService;
            this.creditSummaryService = creditSummaryService;
            this.resultsService = resultsService;
            this.studentAwardsService = studentAwardsService;
            this.termService = termService;
            this.classSectionMeetingService = classSectionMeetingService;
            this.holidayService = holidayService;
            this.calculationPeriodTypeService = calculationPeriodTypeService;
            this.userService = userService;
        }

        /// <summary>
        /// The programVersion  repository.
        /// </summary>
        private IRepository<ArPrgVersions> ProgramVersionRepository =>
            this.context.GetRepositoryForEntity<ArPrgVersions>();

        /// <summary>
        /// The enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentsRepository =>
            this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The campus campus group repository.
        /// </summary>
        private IRepository<SyCmpGrpCmps> CampusCampusGroupRepository =>
            this.context.GetRepositoryForEntity<SyCmpGrpCmps>();

        /// <summary>
        /// The program version definition repository.
        /// </summary>
        private IRepository<ArProgVerDef> ProgramVersionDefinitionRepository =>
            this.context.GetRepositoryForEntity<ArProgVerDef>();

        /// <summary>
        /// The course group repository.
        /// </summary>
        private IRepository<ArReqGrpDef> CourseGroupRepository =>
            this.context.GetRepositoryForEntity<ArReqGrpDef>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        /// The Get program action returns program Description by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The get program method action returns program Description detail of student.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns Program Description.
        /// </returns>
        public async Task<string> GetProgramCodeByEnrollmentId(Guid enrollmentId, Guid studentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           try
                           {
                               string programDescription = string.Empty;
                               var enrollment =
                                   await this.enrollmentService.GetEnrollmentDetail(enrollmentId, studentId);

                               if (enrollment != null)
                               {
                                   var program = this.ProgramVersionRepository.Get(
                                           pv => pv.PrgVerDescrip == enrollment.ProgramVersionDescription)
                                       .FirstOrDefault();
                                   if (program != null)
                                   {
                                       programDescription = program.PrgVerDescrip;
                                   }
                               }

                               return programDescription;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                           }
                       });
        }

        /// <summary>
        /// The Get program length by enrollment id action returns program length for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The Get program length by enrollment id action returns program length for Title IV  clock hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program length.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program length.
        /// </returns>
        public async Task<string> GetProgramLengthByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                    if (isClockHour)
                    {
                        return await this.GetClockHourProgramLength(enrollmentId);
                    }
                    else
                    {
                        return $"{ApiMsgs.NOT_CLOCK_HOUR_PROGRAM}";
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                }
            });
        }

        /// <summary>
        /// The get payment periods by enrollment id action returns payment periods for a Clock Hour program whose length is shorter
        /// or equal to the defined academic year by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get payment periods by enrollment id action returns payment periods for a Clock Hour program for Title IV  clock hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns payment periods.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment periods.
        /// </returns>
        public async Task<ProgramPeriodDetails> GetPaymentPeriodsByEnrollmentId(Guid enrollmentId)
        {
            var programPeriodDetails = new ProgramPeriodDetails();
            return await Task.Run(async () =>
            {
                try
                {
                    var paymentPeriodType = false;
                    var programLength = await this.GetProgramLengthByEnrollmentId(enrollmentId);
                    if (!string.IsNullOrEmpty(programLength) && programLength != $"{ApiMsgs.NOT_CLOCK_HOUR_PROGRAM}")
                    {
                        var campusProgramVersionDetails = await this.GetCampusProgramVersionDetails(enrollmentId);
                        if (campusProgramVersionDetails != null)
                        {
                            if (campusProgramVersionDetails.CalculationPeriodTypeId != null)
                            {
                                paymentPeriodType = await this.calculationPeriodTypeService.IsPaymentPeriodType(campusProgramVersionDetails.CalculationPeriodTypeId.Value);
                            }

                            if (paymentPeriodType)
                            {
                                var enrollment = await this.enrollmentService.Get(enrollmentId);
                                if (enrollment != null)
                                {
                                    var programVersions = this.ProgramVersionRepository
                                        .Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                                    if (programVersions != null && Convert.ToDecimal(programVersions.AcademicYearLength)
                                        != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                    {
                                        programPeriodDetails.NumberOfPaymentPeriods =
                                            programVersions.PayPeriodPerAcYear;

                                        if (Convert.ToDecimal(programLength)
                                            <= Convert.ToDecimal(programVersions.AcademicYearLength))
                                        {
                                            var programLengthPerPeriod = Convert.ToDecimal(
                                                Convert.ToDecimal(programLength)
                                                / programPeriodDetails.NumberOfPaymentPeriods);
                                            if (programLengthPerPeriod > ConstantDecimals.ZeroDecimal)
                                            {
                                                programPeriodDetails.PeriodDetailsList = new List<PeriodDetails>();

                                                for (int programCount = 1;
                                                     programCount <= programVersions.PayPeriodPerAcYear;
                                                     programCount++)
                                                {
                                                    var periodDetails =
                                                        new PeriodDetails
                                                        {
                                                            PeriodLength = programLengthPerPeriod,
                                                            PeriodNumber = programCount
                                                        };
                                                    programPeriodDetails.LastPaymentPeriodLength =
                                                        programLengthPerPeriod;
                                                    programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                                    programPeriodDetails.ResultStatus =
                                                        $"{ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS}";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // if program length is greater than the academic year length
                                            return await this.GetNumberOfPaymentPeriods(enrollmentId);
                                        }
                                    }
                                    else
                                    {
                                        programPeriodDetails.ResultStatus = $"{ApiMsgs.ACADEMIC_PERIOD_ZERO_LENGTH}";
                                    }
                                }
                            }
                            else
                            {
                                programPeriodDetails.ResultStatus = ApiMsgs.PERIOD_FOR_CALCULATION;
                            }
                        }
                        else
                        {
                            programPeriodDetails.ResultStatus = ApiMsgs.NOT_PERIOD_USED_FOR_CALCULATION;
                        }
                    }
                    else
                    {
                        programPeriodDetails.ResultStatus = programLength;
                    }

                    return programPeriodDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new ProgramPeriodDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The get enrollment periods length by enrollment id action returns payment periods length for a Clock Hour program whose length is shorter
        /// or equal to the defined academic year by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get payment periods length by enrollment id action returns payment periods for a Clock Hour program for Title IV  clock hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns payment periods length.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns list of enrollment periods length.
        /// </returns>
        public async Task<EnrollmentPeriodDetails> GetEnrollmentPeriodsLengthByEnrollmentId(Guid enrollmentId)
        {
            var enrollmentPeriodDetails = new EnrollmentPeriodDetails();
            return await Task.Run(async () =>
            {
                try
                {
                    var paymentPeriodType = false;
                    var programLength = await this.GetProgramLengthByEnrollmentId(enrollmentId);
                    if (!string.IsNullOrEmpty(programLength) && programLength != $"{ApiMsgs.NOT_CLOCK_HOUR_PROGRAM}")
                    {
                        var campusProgramVersionDetails = await this.GetCampusProgramVersionDetails(enrollmentId);
                        if (campusProgramVersionDetails != null)
                        {
                            if (campusProgramVersionDetails.CalculationPeriodTypeId != null)
                            {
                                paymentPeriodType = await this.calculationPeriodTypeService.IsPaymentPeriodType(campusProgramVersionDetails.CalculationPeriodTypeId.Value);
                            }

                            if (!paymentPeriodType)
                            {
                                var enrollment = await this.enrollmentService.Get(enrollmentId);
                                if (enrollment != null)
                                {
                                    var programVersions = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                                    if (programVersions != null && Convert.ToDecimal(programVersions.AcademicYearLength) != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                    {
                                        if (Convert.ToDecimal(programLength) <= Convert.ToDecimal(programVersions.AcademicYearLength))
                                        {
                                            var fullAcademicYearNo = Math.Ceiling(Convert.ToDecimal(Convert.ToDecimal(programLength) / programVersions.AcademicYearLength));
                                            enrollmentPeriodDetails.NumberOfPeriodOfEnrollment = Convert.ToInt32(fullAcademicYearNo);

                                            enrollmentPeriodDetails.NumberOfFullAcademicYears = Convert.ToInt32(fullAcademicYearNo);
                                            var programLengthPerPeriod = Convert.ToDecimal(programLength);

                                            if (programLengthPerPeriod > ConstantDecimals.ZeroDecimal)
                                            {
                                                enrollmentPeriodDetails.EnrollmentPeriodsList =
                                                    new List<EnrollmentPeriods>();

                                                for (int programCount = 1;
                                                     programCount <= enrollmentPeriodDetails.NumberOfFullAcademicYears;
                                                     programCount++)
                                                {
                                                    var enrollmentPeriod =
                                                        new EnrollmentPeriods
                                                        {
                                                            EnrollmentLength =
                                                                    programLengthPerPeriod,
                                                            EnrollmentPeriod = programCount
                                                        };

                                                    enrollmentPeriodDetails.EnrollmentPeriodsList.Add(enrollmentPeriod);
                                                    enrollmentPeriodDetails.LastEnrollmentPeriodLength =
                                                        programLengthPerPeriod;
                                                    enrollmentPeriodDetails.ResultStatus =
                                                        $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            return await this.GetPeriodsByEnrollmentId(enrollmentId);
                                        }
                                    }
                                    else
                                    {
                                        enrollmentPeriodDetails.ResultStatus = $"{ApiMsgs.ACADEMIC_PERIOD_ZERO_LENGTH}";
                                    }
                                }
                            }
                            else
                            {
                                enrollmentPeriodDetails.ResultStatus = ApiMsgs.PERIOD_USED_FOR_CALCULATION;
                            }
                        }
                        else
                        {
                            enrollmentPeriodDetails.ResultStatus = ApiMsgs.NOT_PERIOD_USED_FOR_CALCULATION;
                        }
                    }
                    else
                    {
                        enrollmentPeriodDetails.ResultStatus = programLength;
                    }

                    return enrollmentPeriodDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return new EnrollmentPeriodDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The get credit hour program length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        public async Task<CreditHourProgramDetails> GetCreditHourProgramLengthByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                    if (!isClockHour)
                    {
                        return await this.GetCreditHourProgramDetails(enrollmentId);
                    }

                    return new CreditHourProgramDetails
                    {
                        ResultStatus = $"{ApiMsgs.NOT_CREDIT_HOUR_PROGRAM}"
                    };
                }
                catch (Exception)
                {
                    return new CreditHourProgramDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <inheritdoc />
        /// <summary>
        /// The GetHours Scheduled To Complete action returns Hours Scheduled To Complete for Title IV  clock hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetHoursScheduledToComplete action returns Hours Scheduled To Complete for Title IV  clock hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns Hours Scheduled To Complete.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns ScheduledAndTotalHours object with scheduled hours and total hours.
        /// </returns>
        public async Task<ScheduledAndTotalHours> GetHoursScheduledToComplete(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var scheduledAndTotalHours = new ScheduledAndTotalHours();
                           var enrollment = await this.enrollmentService.Get(enrollmentId);
                           if (enrollment == null)
                           {
                               scheduledAndTotalHours.ResultStatus =
                                   $"{ApiMsgs.NOT_FOUND} Enrollment Id: {enrollmentId}.";
                               return scheduledAndTotalHours;
                           }

                           var programVersion = this.ProgramVersionRepository.Get(x => x.PrgVerId == enrollment.PrgVerId)
                               .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                               .ThenInclude(x => x.ArR2t4terminationDetails).FirstOrDefault();

                           if (programVersion?.ArCampusPrgVersions == null)
                           {
                               scheduledAndTotalHours.ResultStatus =
                                   $"{ApiMsgs.NOT_FOUND} Program Version Id {enrollment.PrgVerId}.";
                               return scheduledAndTotalHours;
                           }

                           var enrollmentDetails =
                               programVersion.ArStuEnrollments.FirstOrDefault(x => x.StuEnrollId == enrollmentId);

                           if (enrollmentDetails?.ArR2t4terminationDetails == null)
                           {
                               scheduledAndTotalHours.ResultStatus =
                                   $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                               return scheduledAndTotalHours;
                           }

                           StudentTermination studentTermination = this.mapper.Map<StudentTermination>(
                               programVersion.ArStuEnrollments.FirstOrDefault()?.ArR2t4terminationDetails
                                   .OrderByDescending(x => x.UpdatedDate).FirstOrDefault());

                           if (studentTermination.LastDateAttended == null
                               || studentTermination.LastDateAttended < enrollment.StartDate)
                           {
                               scheduledAndTotalHours.ResultStatus =
                                   ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_FAILED;
                               return scheduledAndTotalHours;
                           }

                           var withdrawalDate = studentTermination.LastDateAttended ?? DateTime.MinValue;
                           withdrawalDate = new DateTime(withdrawalDate.Year, withdrawalDate.Month, withdrawalDate.Day, 23, 59, 00);
                           var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollment.CampusId);
                           var isByDay = trackSapAttendance == TrackSapAttendance.ByDay;
                           return await this.GetScheduledAndTotalHoursForPaymentPeriod(isByDay, enrollment, withdrawalDate, true);
                       });
        }

        /// <summary>
        /// The get withdrawal payment period service method returns the WithdrawalPaymentPeriod based on the given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<CurrentPeriodDetails> GetWithdrawalPaymentPeriod(Guid enrollmentId)
        {
            return await Task.Run(
                       async () => await this.GetWithdrawalPeriodDetails(enrollmentId, true));
        }

        /// <summary>
        /// The get program for SAP by program version Id.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id and is a type of Guid.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ProgramVersionSAP> GetByProgramVersionId(Guid programVersionId)
        {
            return await Task.Run(
                () =>
                {
                    var programVersion = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == programVersionId)
                        .Include(pv => pv.Fasap).ThenInclude(fa => fa.ArStuEnrollments)
                        .Include(pv => pv.UnitType)
                        .Include(pv => pv.Prog).ThenInclude(program => program.Ac)
                        .FirstOrDefault();
                    return this.mapper.Map<ProgramVersionSAP>(programVersion);
                });
        }

        /// <summary>
        /// The get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program by enrollment id.
        /// </summary>
        /// <remarks>
        /// The  get credit hour payment periods by enrollment id action returns payment periods for a credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns payment periods.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns credit hour program payment periods.
        /// </returns>
        public async Task<CreditHourProgramePeriodDetails> GetCreditHourPaymentPeriodsByEnrollmentId(Guid enrollmentId)
        {
            var programPeriodDetails = new CreditHourProgramePeriodDetails();
            decimal weeks;
            return await Task.Run(async () =>
            {
                try
                {
                    var paymentPeriodType = false;
                    var programDetails = await this.GetCreditHourProgramLengthByEnrollmentId(enrollmentId);
                    if (programDetails != null && programDetails.ResultStatus != $"{ApiMsgs.NOT_CREDIT_HOUR_PROGRAM}")
                    {
                        var campusProgramVersionDetail = await this.GetCampusProgramVersionDetails(enrollmentId);
                        if (campusProgramVersionDetail != null)
                        {
                            if (campusProgramVersionDetail.CalculationPeriodTypeId != null)
                            {
                                paymentPeriodType = await this.calculationPeriodTypeService.IsPaymentPeriodType(
                                                        campusProgramVersionDetail.CalculationPeriodTypeId.Value);
                            }

                            if (paymentPeriodType)
                            {
                                var enrollment = await this.enrollmentService.Get(enrollmentId);
                                if (enrollment != null)
                                {
                                    var campusProgramVersionDetails =
                                        await this.campusProgramVersionService.GetCampusProgramVersion(
                                            enrollment.CampusId,
                                            enrollment.PrgVerId);

                                    if (campusProgramVersionDetails != null)
                                    {
                                        if ((campusProgramVersionDetails.TermSubEqualInLen.HasValue
                                             && campusProgramVersionDetails.TermSubEqualInLen == false)
                                            || (campusProgramVersionDetails.IsSelfPaced.HasValue
                                                && campusProgramVersionDetails.IsSelfPaced == false))
                                        {
                                            var programVersions = this.ProgramVersionRepository
                                                .Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                                            if (programVersions != null)
                                            {
                                                if (Convert.ToDecimal(programVersions.AcademicYearLength)
                                                    != Convert.ToDecimal(ConstantDecimals.ZeroDecimal)
                                                    && programDetails.Credits
                                                    <= Convert.ToDecimal(programVersions.AcademicYearLength)
                                                    && programDetails.Weeks
                                                    <= Convert.ToDecimal(programVersions.AcademicYearWeeks))
                                                {
                                                    if (Convert.ToDecimal(programVersions.AcademicYearLength)
                                                        != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                                    {
                                                        programPeriodDetails.NumberOfPaymentPeriods =
                                                            programVersions.PayPeriodPerAcYear;

                                                        var programLengthPerPeriod = Convert.ToDecimal(
                                                            Convert.ToDecimal(programDetails.Credits)
                                                            / programPeriodDetails.NumberOfPaymentPeriods);

                                                        var numberOfWeeksPerPeriod =
                                                            Convert.ToDecimal(programDetails.Weeks) / Convert.ToDecimal(
                                                                programPeriodDetails.NumberOfPaymentPeriods);

                                                        if (programLengthPerPeriod > ConstantDecimals.ZeroDecimal)
                                                        {
                                                            programPeriodDetails.CreditHourProgramePeriodDetailsList =
                                                                new List<CreditHourProgramePeriods>();

                                                            for (int programCount = 1;
                                                                 programCount <= programVersions.PayPeriodPerAcYear;
                                                                 programCount++)
                                                            {
                                                                if (programCount == 1)
                                                                {
                                                                    string[] weeksCount = numberOfWeeksPerPeriod
                                                                        .ToString(CultureInfo.InvariantCulture)
                                                                        .Split('.');

                                                                    weeks = weeksCount.Length > 1
                                                                                ? Math.Ceiling(numberOfWeeksPerPeriod)
                                                                                : numberOfWeeksPerPeriod;
                                                                }
                                                                else
                                                                {
                                                                    weeks = Convert.ToDecimal(
                                                                        numberOfWeeksPerPeriod
                                                                            .ToString(CultureInfo.InvariantCulture)
                                                                            .Split('.')[0]);
                                                                }

                                                                var periodDetails =
                                                                    new CreditHourProgramePeriods
                                                                    {
                                                                        Credits =
                                                                                programLengthPerPeriod,
                                                                        Weeks = Convert
                                                                                .ToInt16(
                                                                                    weeks),
                                                                        Period =
                                                                                programCount
                                                                    };
                                                                programPeriodDetails.CreditHourProgramePeriodDetailsList
                                                                    .Add(periodDetails);
                                                                programPeriodDetails.LastPaymentPeriodCredits =
                                                                    programLengthPerPeriod;
                                                                programPeriodDetails.LastPaymentPeriodWeeks = weeks;
                                                                programPeriodDetails.ResultStatus =
                                                                    $"{ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS}";
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (Convert.ToDecimal(programVersions.AcademicYearLength)
                                                         != Convert.ToDecimal(ConstantDecimals.ZeroDecimal)
                                                         && programDetails.Credits > Convert.ToDecimal(
                                                             programVersions.AcademicYearLength)
                                                         && programDetails.Weeks > Convert.ToDecimal(
                                                             programVersions.AcademicYearWeeks))
                                                {
                                                    var fullAcademicYearNoByWeek = Convert.ToDecimal(
                                                        Convert.ToDecimal(
                                                            programDetails.Weeks / programVersions.AcademicYearWeeks));
                                                    var fullAcademicYearNoByCredit = Convert.ToDecimal(
                                                        Convert.ToDecimal(
                                                            programDetails.Credits
                                                            / programVersions.AcademicYearLength));

                                                    decimal numberOfFullAcademicYears = Math.Floor(
                                                        fullAcademicYearNoByWeek >= fullAcademicYearNoByCredit
                                                            ? Convert.ToDecimal(
                                                                Convert.ToDecimal(
                                                                    programDetails.Credits
                                                                    / programVersions.AcademicYearLength))
                                                            : Convert.ToDecimal(
                                                                Convert.ToDecimal(
                                                                    programDetails.Weeks
                                                                    / programVersions.AcademicYearWeeks)));

                                                    programPeriodDetails.NumberOfFullAcademicYears =
                                                        Convert.ToInt32(numberOfFullAcademicYears);

                                                    var actualWeeks = programVersions.AcademicYearWeeks / 2;
                                                    var weeksInFullAcademicYr =
                                                        programVersions.AcademicYearWeeks
                                                        * programPeriodDetails.NumberOfFullAcademicYears;
                                                    var weeksRemains = programDetails.Weeks - weeksInFullAcademicYr;

                                                    var actualCredits = programVersions.AcademicYearLength / 2;
                                                    var creditsInFullAcademicYr =
                                                        programVersions.AcademicYearLength
                                                        * programPeriodDetails.NumberOfFullAcademicYears;
                                                    var creditsRemains =
                                                        programDetails.Credits - creditsInFullAcademicYr;

                                                    if ((weeksRemains <= actualWeeks && weeksRemains
                                                         != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                                        && (creditsRemains <= actualCredits
                                                            && creditsRemains != Convert.ToDecimal(
                                                                ConstantDecimals.ZeroDecimal)))
                                                    {
                                                        var paymentPeriods = programVersions.PayPeriodPerAcYear;
                                                        programPeriodDetails.NumberOfPaymentPeriods =
                                                            Convert.ToInt32(paymentPeriods * numberOfFullAcademicYears)
                                                            + 1;

                                                        var academicYrCredits =
                                                            programVersions.AcademicYearLength
                                                            * programPeriodDetails.NumberOfFullAcademicYears;
                                                        var lastPaymentPeriodCredits = Convert.ToDecimal(
                                                            programDetails.Credits - academicYrCredits);

                                                        var academicYrWeeks =
                                                            programVersions.AcademicYearWeeks
                                                            * programPeriodDetails.NumberOfFullAcademicYears;
                                                        var lastPaymentPeriodWeeks = Convert.ToInt16(
                                                            programDetails.Weeks - academicYrWeeks);

                                                        var creditsPerPaymentPeriod = Convert.ToDecimal(
                                                            programVersions.AcademicYearLength
                                                            / programVersions.PayPeriodPerAcYear);
                                                        var weeksPerPaymentPeriod = Convert.ToInt16(
                                                            programVersions.AcademicYearWeeks
                                                            / programVersions.PayPeriodPerAcYear);
                                                        programPeriodDetails.CreditHourProgramePeriodDetailsList =
                                                            new List<CreditHourProgramePeriods>();
                                                        if ((lastPaymentPeriodWeeks
                                                             > Convert.ToDecimal(ConstantDecimals.ZeroDecimal)
                                                             && lastPaymentPeriodWeeks <= actualWeeks)
                                                            || (lastPaymentPeriodCredits
                                                                > Convert.ToDecimal(ConstantDecimals.ZeroDecimal)
                                                                && lastPaymentPeriodCredits <= actualCredits))
                                                        {
                                                            for (int programCount = 1;
                                                                 programCount <= programPeriodDetails
                                                                     .NumberOfPaymentPeriods;
                                                                 programCount++)
                                                            {
                                                                if (programCount <= programPeriodDetails
                                                                        .NumberOfPaymentPeriods - 1)
                                                                {
                                                                    var periodDetails =
                                                                        new CreditHourProgramePeriods
                                                                        {
                                                                            Credits =
                                                                                    creditsPerPaymentPeriod,
                                                                            Weeks =
                                                                                    weeksPerPaymentPeriod,
                                                                            Period =
                                                                                    programCount
                                                                        };

                                                                    programPeriodDetails
                                                                        .CreditHourProgramePeriodDetailsList
                                                                        .Add(periodDetails);
                                                                    programPeriodDetails.ResultStatus =
                                                                        $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER}";
                                                                }
                                                                else
                                                                {
                                                                    var periodDetails =
                                                                        new CreditHourProgramePeriods
                                                                        {
                                                                            Credits =
                                                                                    lastPaymentPeriodCredits,
                                                                            Weeks =
                                                                                    lastPaymentPeriodWeeks,
                                                                            Period =
                                                                                    programCount
                                                                        };

                                                                    programPeriodDetails.LastPaymentPeriodCredits =
                                                                        lastPaymentPeriodCredits;
                                                                    programPeriodDetails.LastPaymentPeriodWeeks =
                                                                        lastPaymentPeriodWeeks;
                                                                    programPeriodDetails
                                                                        .CreditHourProgramePeriodDetailsList
                                                                        .Add(periodDetails);
                                                                    programPeriodDetails.ResultStatus =
                                                                        $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER}";
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            for (int programCount = 1;
                                                                 programCount <= programPeriodDetails
                                                                     .NumberOfPaymentPeriods;
                                                                 programCount++)
                                                            {
                                                                if (programCount <= programPeriodDetails
                                                                        .NumberOfPaymentPeriods - 1)
                                                                {
                                                                    var periodDetails =
                                                                        new CreditHourProgramePeriods
                                                                        {
                                                                            Credits =
                                                                                    creditsPerPaymentPeriod,
                                                                            Weeks =
                                                                                    weeksPerPaymentPeriod,
                                                                            Period =
                                                                                    programCount
                                                                        };

                                                                    programPeriodDetails
                                                                        .CreditHourProgramePeriodDetailsList
                                                                        .Add(periodDetails);
                                                                    programPeriodDetails.ResultStatus =
                                                                        $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER}";
                                                                }
                                                                else
                                                                {
                                                                    var periodDetails =
                                                                        new CreditHourProgramePeriods
                                                                        {
                                                                            Credits =
                                                                                    lastPaymentPeriodCredits,
                                                                            Weeks =
                                                                                    lastPaymentPeriodWeeks,
                                                                            Period =
                                                                                    programCount
                                                                        };

                                                                    programPeriodDetails.LastPaymentPeriodCredits =
                                                                        lastPaymentPeriodCredits;
                                                                    programPeriodDetails.LastPaymentPeriodWeeks =
                                                                        lastPaymentPeriodWeeks;
                                                                    programPeriodDetails
                                                                        .CreditHourProgramePeriodDetailsList
                                                                        .Add(periodDetails);
                                                                    programPeriodDetails.ResultStatus =
                                                                        $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER}";
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return await this.GetCreditHourPeriodDetailsByEnrollmentId(
                                                                   enrollmentId);
                                                    }
                                                }
                                                else
                                                {
                                                    programPeriodDetails.ResultStatus = programDetails.ResultStatus;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            programPeriodDetails.ResultStatus =
                                                $"{ApiMsgs.NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH_NON_TERM_NOT_SELFPACED_TERM}";
                                        }
                                    }
                                    else
                                    {
                                        programPeriodDetails.ResultStatus = ApiMsgs.CAMPUS_PROGRAM_VERSION_NOT_FOUND;
                                    }
                                }
                            }
                            else
                            {
                                programPeriodDetails.ResultStatus = ApiMsgs.PERIOD_FOR_CALCULATION;
                            }
                        }
                        else
                        {
                            programPeriodDetails.ResultStatus = ApiMsgs.PERIOD_FOR_CALCULATION;
                        }
                    }
                    else
                    {
                        programPeriodDetails.ResultStatus = programDetails != null
                                                                ? programDetails.ResultStatus
                                                                : $"{ApiMsgs.NOT_CREDIT_HOUR_PROGRAM}";
                    }

                    return programPeriodDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();
                    programPeriodDetails.ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}";
                    return programPeriodDetails;
                }
            });
        }

        /// <inheritdoc />
        /// <summary>
        /// The get period of enrollment with excused absence returns period of enrollments details by enrollment id.
        /// </summary>
        /// <remarks>
        /// The get period of enrollment with excused absence returns period of enrollments details by enrollment id.
        /// This action method calculates allowed excused absence per period of enrollment and return a period
        /// of enrollments details for given enrollment id
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// This method returns PeriodOfEnrollmentDetails with list of PeriodOfEnrollments and a ResultStatus .
        /// </returns>
        public async Task<PeriodOfEnrollmentDetails> GetPeriodOfEnrollmentWithExcusedAbsence(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var periodOfEnrollmentDetails = new PeriodOfEnrollmentDetails
                           {
                               PeriodOfEnrollments = new List<EnrollmentPeriods>()
                           };

                           var enrollmentDetails = await this.enrollmentService.Get(enrollmentId);
                           if (enrollmentDetails == null)
                           {
                               periodOfEnrollmentDetails.ResultStatus =
                                   $"{ApiMsgs.NOT_FOUND}, Enrollment Id : {enrollmentId}";
                               return periodOfEnrollmentDetails;
                           }

                           var programVersionDetails = this.ProgramVersionRepository
                               .Get(x => x.PrgVerId == enrollmentDetails.PrgVerId)
                               .Include(x => x.ArCampusPrgVersions).FirstOrDefault();

                           if (programVersionDetails?.ArCampusPrgVersions == null)
                           {
                               periodOfEnrollmentDetails.ResultStatus =
                                   $"{ApiMsgs.NOT_FOUND}, Program Version Id : {enrollmentDetails.PrgVerId}";
                               return periodOfEnrollmentDetails;
                           }

                           var campusPrgVersions = programVersionDetails.ArCampusPrgVersions.FirstOrDefault(x => x.CampusId == enrollmentDetails.CampusId);
                           var allowedExcusedAbsence = Convert.ToDecimal(campusPrgVersions?.AllowExcusAbsPerPayPrd);
                           allowedExcusedAbsence = allowedExcusedAbsence == 0
                                                       ? allowedExcusedAbsence
                                                       : allowedExcusedAbsence / 100;

                           var periodDetails = await this.GetEnrollmentPeriodsLengthByEnrollmentId(enrollmentId);
                           if (!periodDetails.EnrollmentPeriodsList.Any())
                           {
                               periodOfEnrollmentDetails.ResultStatus = periodDetails.ResultStatus;
                               return periodOfEnrollmentDetails;
                           }

                           periodDetails.EnrollmentPeriodsList.ForEach(
                               enrollmentPeriod =>
                               {
                                   enrollmentPeriod.AllowedExcusedAbsence =
                                           allowedExcusedAbsence * enrollmentPeriod.EnrollmentLength;
                                   periodOfEnrollmentDetails.PeriodOfEnrollments.Add(enrollmentPeriod);
                               });

                           periodOfEnrollmentDetails.ResultStatus = ApiMsgs.EXCUSED_ABS_FOR_PER_OF_ENROLL_CALC_SUCCESSFUL;
                           return periodOfEnrollmentDetails;
                       });
        }

        /// <summary>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the withdrawal details of period of enrollment by given enrollment Id,
        /// It calculates the scheduled hours and the total hours in the period of enrollment for the clock hour Title VI eligible program from which the student has withdrawn.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns the withdrawal details of period of enrollment of the student based on the enrollment id.
        /// </returns>
        public async Task<CurrentPeriodDetails> GetWithdrawalPeriodOfEnrollment(Guid enrollmentId)
        {
            return await Task.Run(
                       async () => await this.GetWithdrawalPeriodDetails(enrollmentId, false));
        }

        /// <summary>
        /// The get credit hour program period length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program period length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program period credits and weeks.
        /// </returns>
        public async Task<CreditHourProgramePeriodDetails> GetCreditHourPeriodsLengthByEnrollmentId(Guid enrollmentId)
        {
            var creditHourProgramPeriodDetails = new CreditHourProgramePeriodDetails();
            return await Task.Run(async () =>
            {
                try
                {
                    var paymentPeriodType = false;
                    var creditHourProgramLength = await this.GetCreditHourProgramLengthByEnrollmentId(enrollmentId);
                    if (creditHourProgramLength.ResultStatus != ApiMsgs.CREDIT_HOUR_PROGRAM_LENGTH)
                    {
                        creditHourProgramPeriodDetails.ResultStatus = creditHourProgramLength.ResultStatus;
                        return creditHourProgramPeriodDetails;
                    }

                    var transferCredit = await this.GetTransferCredits(enrollmentId);
                    var campusProgramVersionDetails = await this.GetCampusProgramVersionDetails(enrollmentId);
                    if (campusProgramVersionDetails != null)
                    {
                        if (campusProgramVersionDetails.CalculationPeriodTypeId != null)
                        {
                            paymentPeriodType = await this.calculationPeriodTypeService.IsPaymentPeriodType(
                                                    campusProgramVersionDetails.CalculationPeriodTypeId.Value);
                        }

                        if (!paymentPeriodType)
                        {
                            var enrollment = await this.enrollmentService.Get(enrollmentId);
                            if (enrollment != null)
                            {
                                var programVersions = this.ProgramVersionRepository
                                    .Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                                if (programVersions != null && Convert.ToDecimal(programVersions.AcademicYearLength)
                                    != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                {
                                    if (Convert.ToDecimal(creditHourProgramLength.Credits)
                                        <= Convert.ToDecimal(programVersions.AcademicYearLength))
                                    {
                                        var fullAcademicYearNo = Math.Ceiling(
                                            Convert.ToDecimal(
                                                creditHourProgramLength.Credits / programVersions.AcademicYearLength));

                                        creditHourProgramPeriodDetails.NumberOfFullAcademicYears =
                                            Convert.ToInt32(fullAcademicYearNo);
                                        var programLengthPerPeriod = creditHourProgramLength.Credits;
                                        var actualWeek = programVersions.Weeks;

                                        if (transferCredit > ConstantDecimals.ZeroDecimal)
                                        {
                                            var programWeek = Convert.ToDecimal(
                                                (programVersions.Weeks / programVersions.Credits)
                                                * creditHourProgramLength.Credits);
                                            var integral = Math.Truncate(programWeek);
                                            var fractional = programWeek - integral;

                                            actualWeek = Convert.ToInt16(
                                                fractional > ConstantDecimals.GreaterThanOneDecimal
                                                    ? Math.Ceiling(programWeek)
                                                    : Math.Floor(programWeek));
                                        }

                                        creditHourProgramPeriodDetails.NumberOfPaymentPeriods =
                                            Convert.ToInt32(fullAcademicYearNo);

                                        if (programLengthPerPeriod > ConstantDecimals.ZeroDecimal)
                                        {
                                            creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList =
                                                new List<CreditHourProgramePeriods>();

                                            for (int programCount = 1;
                                                 programCount <= creditHourProgramPeriodDetails
                                                     .NumberOfFullAcademicYears;
                                                 programCount++)
                                            {
                                                var creditHourProgramPeriods =
                                                    new CreditHourProgramePeriods
                                                    {
                                                        Period = programCount,
                                                        Credits = programLengthPerPeriod,
                                                        Weeks = Convert.ToInt16(
                                                                actualWeek)
                                                    };

                                                creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(
                                                    creditHourProgramPeriods);
                                                creditHourProgramPeriodDetails.LastEnrollmentPeriodLength =
                                                    programLengthPerPeriod;
                                                creditHourProgramPeriodDetails.LastEnrollmentWeek =
                                                    Convert.ToInt16(actualWeek);
                                                creditHourProgramPeriodDetails.ResultStatus =
                                                    $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        return await this.GetCreditPeriodsByEnrollmentId(enrollmentId);
                                    }
                                }
                                else
                                {
                                    creditHourProgramPeriodDetails.ResultStatus =
                                        $"{ApiMsgs.ACADEMIC_PERIOD_ZERO_LENGTH}";
                                }
                            }
                        }
                        else
                        {
                            creditHourProgramPeriodDetails.ResultStatus = ApiMsgs.PERIOD_USED_FOR_CALCULATION;
                        }
                    }
                    else
                    {
                        creditHourProgramPeriodDetails.ResultStatus = ApiMsgs.PERIOD_USED_FOR_CALCULATION;
                    }

                    return creditHourProgramPeriodDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return new CreditHourProgramePeriodDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The get scheduled and total hours for period of enrollments by given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the scheduled hours and total hours per period of enrollment for a given enrollment id
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The Period Of Enrollment Details.
        /// </returns>
        public async Task<ScheduledAndTotalHours> GetScheduledAndTotalHoursForPeriodOfEnrollment(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var scheduledAndTotalHours = new ScheduledAndTotalHours();
                           var enrollment = await this.enrollmentService.Get(enrollmentId);
                           if (enrollment == null)
                           {
                               scheduledAndTotalHours.ResultStatus =
                           $"{ApiMsgs.NOT_FOUND} Enrollment Id: {enrollmentId}.";
                               return scheduledAndTotalHours;
                           }

                           var programVersion = this.ProgramVersionRepository.Get(x => x.PrgVerId == enrollment.PrgVerId)
                                                       .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                                                       .ThenInclude(x => x.ArR2t4terminationDetails).FirstOrDefault();

                           StudentTermination studentTermination;
                           if (programVersion?.ArCampusPrgVersions == null)
                           {
                               scheduledAndTotalHours.ResultStatus = $"{ApiMsgs.NOT_FOUND} Program Version Id {enrollment.PrgVerId}.";
                               return scheduledAndTotalHours;
                           }

                           var enrollmentDetails = programVersion.ArStuEnrollments.FirstOrDefault(x => x.StuEnrollId == enrollmentId);

                           if (enrollmentDetails?.ArR2t4terminationDetails == null)
                           {
                               scheduledAndTotalHours.ResultStatus = $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                               return scheduledAndTotalHours;
                           }
                           else
                           {
                               studentTermination = this.mapper.Map<StudentTermination>(programVersion.ArStuEnrollments.FirstOrDefault()?.ArR2t4terminationDetails
                                   .OrderByDescending(x => x.UpdatedDate)
                                   .FirstOrDefault());

                               if (studentTermination?.LastDateAttended == null || studentTermination.LastDateAttended < enrollment.StartDate)
                               {
                                   scheduledAndTotalHours.ResultStatus = ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_FAILED;
                                   return scheduledAndTotalHours;
                               }
                           }

                           var withdrawalDate = studentTermination.LastDateAttended ?? DateTime.MinValue;
                           withdrawalDate = new DateTime(withdrawalDate.Year, withdrawalDate.Month, withdrawalDate.Day, 23, 59, 00);
                           var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollment.CampusId);
                           var isByDay = trackSapAttendance == TrackSapAttendance.ByDay;
                           return await this.GetScheduledAndTotalHours(isByDay, enrollment, withdrawalDate);
                       });
        }

        /// <summary>
        /// The get Title IV program versions by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and TValue is of type Guid.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetTitleIvProgramVersionsByCampus(Guid campusId)
        {
            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:ProgramVersions:{campusId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                            this.ProgramVersionRepository.Get(pv => pv.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId) && pv.StatusId == this.ActiveStatusId && pv.Fasapid.HasValue)),
                        CacheExpiration.VERY_LONG);
                });
        }

        /// <summary>
        /// The get program versions by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and TValue is of type Guid.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetProgramVersionsByCampus(Guid campusId)
        {
            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:ProgramVersions:{campusId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                            this.ProgramVersionRepository.Get(pv => pv.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId) && pv.StatusId == this.ActiveStatusId)).OrderBy(x => x.Text),
                        CacheExpiration.VERY_LONG);
                });
        }
        /// <summary>
        /// Get Program version ID and Description by Program Id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<IListItem<string, Guid>>> GetByProgramId(Guid progId)
        {
            return await Task.Run(() => SimpleCache.GetOrAddExisting(
                $"{this.CacheKeyFirstSegment}:PvByProgId:{progId}",
                () => this.mapper.Map<IList<IListItem<string, Guid>>>(this.ProgramVersionRepository.Get(p => p.ProgId == progId && p.StatusId == this.ActiveStatusId).OrderBy(a => a.PrgVerDescrip)).ToList(),
                CacheExpiration.VERY_LONG));
        }

        /// <summary>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the payment period and credits earned by given enrollment Id.
        /// </summary>
        /// <remarks>
        /// The GetCreditHoursWithdrawalPaymentPeriod action method returns the returns the payment period and credits earned by given enrollment Id,
        /// It calculate the scheduled hours and the total hours in the payment period for a credit hour Title 4 eligible program from which the student has withdrawn.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<CurrentPeriodDetailsWithCreditsEarned> GetCreditHoursWithdrawalPaymentPeriod(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var withdrawalPaymentPeriod = new CurrentPeriodDetailsWithCreditsEarned();

                           var enrollmentDetail = await this.GetEnrollmentDetailsById(enrollmentId);
                           if (enrollmentDetail.ResultStatus != ApiMsgs.ENROLLMENT_DETAILS_CALC_SUCCESS)
                           {
                               withdrawalPaymentPeriod.ResultStatus = enrollmentDetail.ResultStatus;
                               return withdrawalPaymentPeriod;
                           }

                           if (enrollmentDetail.AcademicCalendarId != null && ((int)enrollmentDetail.AcademicCalendarId != (int)Constants.AcademicCalendars.NonStandardTerm && (int)enrollmentDetail.AcademicCalendarId != (int)Constants.AcademicCalendars.NonTerm))
                           {
                               withdrawalPaymentPeriod.ResultStatus = ApiMsgs.NOT_NONTRM_OR_NON_STANDARD_TERM_PROGRAM;
                               return withdrawalPaymentPeriod;
                           }

                           if (enrollmentDetail.IsSelfPaced)
                           {
                               withdrawalPaymentPeriod.ResultStatus = ApiMsgs.SELF_PACED_PROGRAM;
                               return withdrawalPaymentPeriod;
                           }

                           var creditHoursPeriodDetails = await this.GetCreditHourPaymentPeriodsByEnrollmentId(enrollmentId);
                           if (creditHoursPeriodDetails.CreditHourProgramePeriodDetailsList.Count == 0)
                           {
                               withdrawalPaymentPeriod.ResultStatus = creditHoursPeriodDetails.ResultStatus;
                               return withdrawalPaymentPeriod;
                           }

                           var creditHourProgramPeriodDetails = creditHoursPeriodDetails.CreditHourProgramePeriodDetailsList;
                           var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollmentDetail.Enrollment.CampusId);

                           withdrawalPaymentPeriod = trackSapAttendance == TrackSapAttendance.ByDay ?
                                                         await this.GetCreditHrsPayPeriodForStudentAttendance(
                                                             true,
                                                             enrollmentDetail.Enrollment,
                                                             enrollmentDetail.EndDateTime,
                                                             creditHourProgramPeriodDetails.ToList(),
                                                             enrollmentDetail.AcademicCalendarId ?? 0)
                                                         : await this.GetCreditHrsPayPeriodForClassSectionAttendance(
                                                               true,
                                                               enrollmentDetail.Enrollment,
                                                               enrollmentDetail.EndDateTime,
                                                               creditHourProgramPeriodDetails.ToList(),
                                                               enrollmentDetail.AcademicCalendarId ?? 0);

                           if (withdrawalPaymentPeriod.CreditsEarned == 0)
                           {
                               withdrawalPaymentPeriod.CreditsEarnedOn = null;
                           }

                           if (withdrawalPaymentPeriod.WeeksCompleted == 0)
                           {
                               withdrawalPaymentPeriod.WeeksCompletedOn = null;
                           }

                           return withdrawalPaymentPeriod;
                       });
        }

        /// <summary>
        /// The set program registration type.
        /// Only Support
        /// </summary>
        /// <param name="programRegistrationType">
        /// The program registration type.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="gradeScaleId">
        /// The grade scale id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<ProgramVersion>> SetProgramRegistrationType(
            Enums.ProgramRegistrationType programRegistrationType,
            Guid programVersionId,
            Guid? gradeScaleId)
        {
            ActionResult<ProgramVersion> actionResult = new ActionResult<ProgramVersion>();
            if (programVersionId == Guid.Empty)
            {
                actionResult.ResultStatus = Enums.ResultStatus.Warning;
                actionResult.ResultStatusMessage =
                    ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " program Version Id";
                return actionResult;
            }

            if (programRegistrationType == Enums.ProgramRegistrationType.ByProgram && gradeScaleId == null)
            {
                actionResult.ResultStatus = Enums.ResultStatus.Warning;
                actionResult.ResultStatusMessage =
                    ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " grade scale Id";
                return actionResult;
            }
            // change the logic that if !support user, check for permissions to access the page and the given PV has no enrollments tied to it.
            // If they have then allow them else throw the error below
            if (!this.TenantUser.IsSupportUser)
            {
                // check if the user has permissions to access the programVersionPage
                if (!(this.userService.GetUserAcessPermissionsForResourceId(SystemResources.ProgramVersionPage, this.TenantUser.Id).Result))
                {
                    actionResult.ResultStatus = Enums.ResultStatus.Error;
                    actionResult.ResultStatusMessage = ApiMsgs.SaveAccessPV;
                    return actionResult;
                }
                else
                {
                    // now check if the given PV have no enrollments tied to it.
                    if (this.EnrollmentsRepository.Get(e => e.PrgVerId == programVersionId).Any())
                    {
                        actionResult.ResultStatus = Enums.ResultStatus.Error;
                        actionResult.ResultStatusMessage = ApiMsgs.EnrollmentExistsForPV;
                        return actionResult;
                    }
                }
            }

            return await Task.Run(async () =>
            {
                try
                {
                    var programVersion = this.ProgramVersionRepository
                        .Get(x => x.PrgVerId == programVersionId)
                        .Include(x => x.ArTerm).ThenInclude(x => x.ArClassSections).Include(x => x.ArProgVerDef).ThenInclude(x => x.Req).FirstOrDefault();

                    if (programVersion == null)
                    {
                        actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                        actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                        return actionResult;
                    }

                    programVersion.ThGrdScaleId = gradeScaleId;

                    if ((programVersion.ProgramRegistrationType == 0 || programVersion.ProgramRegistrationType == null) && programRegistrationType == Enums.ProgramRegistrationType.ByProgram)
                    {
                        actionResult = await this.InsertProgramRegistrationType(programVersion);
                    }
                    else if (programVersion.ProgramRegistrationType != null
                             && programVersion.ProgramRegistrationType.Value
                             == (int)Enums.ProgramRegistrationType.ByClass && programRegistrationType
                             == Enums.ProgramRegistrationType.ByProgram)
                    {
                        actionResult = await this.InsertProgramRegistrationType(programVersion);
                    }
                    else if (programVersion.ProgramRegistrationType != null
                             && programVersion.ProgramRegistrationType.Value
                             == (int)Enums.ProgramRegistrationType.ByProgram && programRegistrationType
                             == Enums.ProgramRegistrationType.ByProgram)
                    {

                        if (programVersion.ThGrdScaleId == null)
                        {
                            actionResult.ResultStatus = Enums.ResultStatus.Warning;
                            actionResult.ResultStatusMessage = ApiMsgs.ProgramRegistrationInsertWarning + ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Grade Scale Id";
                            return actionResult;
                        }
                        actionResult = await this.UpdateProgramRegistrationType(programVersion);
                    }
                    else if (programVersion.ProgramRegistrationType != null
                             && programVersion.ProgramRegistrationType.Value
                             == (int)Enums.ProgramRegistrationType.ByProgram && programRegistrationType
                             == Enums.ProgramRegistrationType.ByClass)
                    {
                        programVersion.ProgramRegistrationType = (int)programRegistrationType;
                        actionResult = await this.DeleteProgramRegistrationType(programVersion);
                        actionResult = await this.UpdateProgramRegistrationType(programVersion, true);
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    actionResult.ResultStatus = Enums.ResultStatus.Error;
                    actionResult.ResultStatusMessage = ApiMsgs.UnhandleException + " " + e.Message;
                }

                return actionResult;
            });
        }

        /// <summary>
        /// The have enrollments.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> HaveEnrollments(Guid programVersionId)
        {
            return await Task.Run(() =>
            {
                bool haveEnrollments = this.EnrollmentsRepository.Get(x => x.PrgVerId == programVersionId).Any();

                return haveEnrollments;
            });
        }

        /// <summary>
        /// The get withdrawal period details method calculates the withdrawal period details for payment period or period of enrollment based on the isPaymentPeriod flag.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and of type guid.
        /// </param>
        /// <param name="isPaymentPeriod">
        /// The is payment period is a boolean flag which will be true for payment period and false for period of enrollment.
        /// </param>
        /// <returns>
        /// Returns an object of <see cref="CurrentPeriodDetails"/> where CurrentPeriodDetails has the details of withdrawal period of the student.
        /// </returns>
        public async Task<CurrentPeriodDetails> GetWithdrawalPeriodDetails(Guid enrollmentId, bool isPaymentPeriod)
        {
            var withdrawalPaymentPeriod = new CurrentPeriodDetails();
            string failureMessage = isPaymentPeriod
                                        ? ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_FAILED
                                        : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_FAILED;
            string successMessage = isPaymentPeriod
                                        ? ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL
                                        : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
            string periodAvailabilityMessage = isPaymentPeriod
                                                   ? ApiMsgs.PAYMENT_PERIOD_DETAILS_NOT_FOUND
                                                   : ApiMsgs.PERIOD_OF_ENROLLMENT_DETAILS_NOT_FOUND;

            var enrollment = await this.enrollmentService.Get(enrollmentId);
            if (enrollment == null)
            {
                withdrawalPaymentPeriod.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                return withdrawalPaymentPeriod;
            }

            var programVersion = this.ProgramVersionRepository.Get(x => x.PrgVerId == enrollment.PrgVerId)
                .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                .ThenInclude(x => x.ArR2t4terminationDetails).FirstOrDefault();

            if (programVersion?.ArCampusPrgVersions == null)
            {
                withdrawalPaymentPeriod.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Program Version Id {enrollment.PrgVerId}.";
                return withdrawalPaymentPeriod;
            }

            var enrollmentDetails = programVersion.ArStuEnrollments.FirstOrDefault(x => x.StuEnrollId == enrollmentId);

            if (enrollmentDetails?.ArR2t4terminationDetails == null)
            {
                withdrawalPaymentPeriod.ResultStatus = $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                return withdrawalPaymentPeriod;
            }

            var terminationDetails = enrollmentDetails.ArR2t4terminationDetails.OrderByDescending(x => x.UpdatedDate).FirstOrDefault();

            var studentTermination = this.mapper.Map<StudentTermination>(terminationDetails);

            if (studentTermination?.LastDateAttended == null)
            {
                withdrawalPaymentPeriod.ResultStatus = $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                return withdrawalPaymentPeriod;
            }

            var programPeriodDetails = await this.GetPaymentPeriodsByEnrollmentId(enrollmentId);
            if (!isPaymentPeriod)
            {
                var periodOfEnrollmentDetails = await this.GetEnrollmentPeriodsLengthByEnrollmentId(enrollmentId);
                programPeriodDetails = this.mapper.Map<ProgramPeriodDetails>(periodOfEnrollmentDetails);
            }

            if (programPeriodDetails == null)
            {
                withdrawalPaymentPeriod.ResultStatus = failureMessage;
                return withdrawalPaymentPeriod;
            }

            if (programPeriodDetails.ResultStatus == ApiMsgs.NOT_CLOCK_HOUR_PROGRAM)
            {
                withdrawalPaymentPeriod.ResultStatus = ApiMsgs.NOT_CLOCK_HOUR_PROGRAM;
                return withdrawalPaymentPeriod;
            }

            if (programPeriodDetails.PeriodDetailsList.Count == 0)
            {
                withdrawalPaymentPeriod.ResultStatus = periodAvailabilityMessage;
                return withdrawalPaymentPeriod;
            }

            withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod = enrollment.StartDate;
            var withdrawalDateTime = studentTermination.LastDateAttended ?? DateTime.MinValue;
            withdrawalDateTime = new DateTime(
                withdrawalDateTime.Year,
                withdrawalDateTime.Month,
                withdrawalDateTime.Day,
                23,
                59,
                00);

            var trackSapAttendance =
                this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollment.CampusId);
            var campusPrgVersion = programVersion.ArCampusPrgVersions.FirstOrDefault(x => x.CampusId == enrollment.CampusId);
            var allowExcuseAbsPerPaymentPeriod = Convert.ToDecimal(campusPrgVersion?.AllowExcusAbsPerPayPrd);
            var programSchedules = await this.programScheduleService.GetDetailByProgramVersionId(enrollment.PrgVerId);
            var programScheduleList = programSchedules.ToList();

            withdrawalPaymentPeriod = trackSapAttendance == TrackSapAttendance.ByDay
                                          ? await this.GetPaymentPeriodForStudentClockAttendance(
                                                enrollment,
                                                withdrawalDateTime,
                                                programPeriodDetails,
                                                programScheduleList)
                                          : await this.GetPaymentPeriodForClassSectionAttendance(
                                                enrollment,
                                                allowExcuseAbsPerPaymentPeriod,
                                                withdrawalDateTime,
                                                programPeriodDetails);

            withdrawalPaymentPeriod.ResultStatus = (withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod != null)
                                                       ? successMessage
                                                       : failureMessage;
            return withdrawalPaymentPeriod;
        }

        /// <summary>
        /// The get program version by enrollment id method returns the list of program version id and program id for a specific enrollment Id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ProgramVersionDetails> GetProgramVersionByEnrollmentId(Guid enrollmentId)
        {
            var enrollment = await this.enrollmentService.Get(enrollmentId);
            ProgramVersionDetails programVersionDetails = null;

            if (enrollment != null)
            {
                var programVersion = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                if (programVersion != null)
                {
                    programVersionDetails = this.mapper.Map<ProgramVersionDetails>(programVersion);
                }
            }

            return programVersionDetails;
        }

        /// <summary>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// </summary>
        /// <remarks>
        /// The get payment period for not substantially equal in length action returns the payment period details for 
        /// Non-standard term program with terms not substantially equal in length program.
        /// It requires enrollmentId. Based on the EnrollmentId, it searches all the enrolled program details 
        /// and returns the payment period details. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns payment period details for the specified enrollment id.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDetails> GetPaymentPeriodForNotSubstantiallyEqualInLength(Guid enrollmentId)
        {
            var creditHourPaymentPeriodDetails = new CreditHourPaymentPeriodDetails();
            decimal transferCredits;

            return await Task.Run(
                       async () =>
                       {
                           try
                           {
                               var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                               if (!isClockHour)
                               {
                                   var enrollment = await this.enrollmentService.Get(enrollmentId);
                                   if (enrollment != null)
                                   {
                                       var programVersion = this.ProgramVersionRepository
                                           .Get(x => x.PrgVerId == enrollment.PrgVerId)
                                           .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                                           .ThenInclude(x => x.ArR2t4terminationDetails).Include(x => x.Prog)
                                           .FirstOrDefault();
                                       var withdrawalDate = this.GetWithdrawalDate(programVersion);
                                       if (withdrawalDate != DateTime.MinValue)
                                       {
                                           if (programVersion?.Prog != null && programVersion.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm))
                                           {
                                               var campusProgramVersionDetails =
                                                   await this.campusProgramVersionService.GetCampusProgramVersion(
                                                       enrollment.CampusId,
                                                       programVersion.PrgVerId);

                                               if (campusProgramVersionDetails?.TermSubEqualInLen != null
                                                   && !campusProgramVersionDetails.TermSubEqualInLen.Value)
                                               {
                                                   transferCredits = await this.GetTransferCredits(enrollmentId);

                                                   var paymentPeriodDetailsForTerms = this.GetPaymentPeriodDetailsForTerms(
                                                       enrollmentId,
                                                       withdrawalDate,
                                                       creditHourPaymentPeriodDetails);

                                                   var studentAwardDetails =
                                                       await this.studentAwardsService.GetDirectLoanStudentAwards(
                                                           enrollmentId, paymentPeriodDetailsForTerms.TermStartDate, paymentPeriodDetailsForTerms.TermEndDate);
                                                   var awardDetails = studentAwardDetails.ToList();

                                                   if (awardDetails.Any() && awardDetails.FirstOrDefault()?.ResultStatus == ApiMsgs.DIRECT_LOANS_FOUND)
                                                   {
                                                       creditHourPaymentPeriodDetails =
                                                           await this.GetNonStandardDirectLoanPaymentPeriodDetails(
                                                               enrollmentId,
                                                               programVersion,
                                                               transferCredits,
                                                               withdrawalDate);
                                                   }
                                                   else if (awardDetails.Any() && awardDetails.FirstOrDefault()?.ResultStatus == ApiMsgs.NO_AWARDS)
                                                   {
                                                       creditHourPaymentPeriodDetails =
                                                           new CreditHourPaymentPeriodDetails
                                                           {
                                                               TermOfWithdrawal = ApiMsgs.NO_AWARDS,
                                                               ResultStatus = ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL
                                                           };
                                                   }
                                                   else
                                                   {
                                                       creditHourPaymentPeriodDetails.TermOfWithdrawal = ApiMsgs.NON_STANDARD_PAYMENT_PERIOD;
                                                       creditHourPaymentPeriodDetails = paymentPeriodDetailsForTerms;
                                                       creditHourPaymentPeriodDetails.PaymentPeriodTermType = (int)Constants.PaymentPeriodTermType.NonStandard;
                                                   }
                                               }
                                               else
                                               {
                                                   creditHourPaymentPeriodDetails.ResultStatus =
                                                       ApiMsgs.NONSTANDARD_AND_SUBSTANTIALLY_NOT_EQUAL;
                                               }
                                           }
                                           else
                                           {
                                               creditHourPaymentPeriodDetails.ResultStatus =
                                                   ApiMsgs.NOT_NONSTANDARD_PROGRAM;
                                           }
                                       }
                                   }
                               }
                               else
                               {
                                   creditHourPaymentPeriodDetails.ResultStatus = ApiMsgs.NOT_CREDIT_HOUR_PROGRAM;
                               }

                               return creditHourPaymentPeriodDetails;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();

                               return new CreditHourPaymentPeriodDetails
                               {
                                   ResultStatus =
                                                  ApiMsgs.FAILED_GETTING_RECORD
                               };
                           }
                       });
        }

        /// <summary>
        /// The IsProgramVersionSelfPaced action method returns boolean value based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The IsProgramVersionSelfPaced action check the program type of the enrollment and in the parameter 
        /// it requires EnrollmentId. 
        /// Based on the EnrollmentId ,it search all the enrolled program details and returns if the enrollment is non term and non self based.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns  true or false.
        /// </returns>
        public async Task<bool> IsProgramVersionSelfPaced(Guid enrollmentId)
        {
            var enrollment = await this.enrollmentService.Get(enrollmentId);

            var program = this.ProgramVersionRepository
                .Get(pv => pv.PrgVerId == enrollment.PrgVerId).Include(x => x.Prog)
                .FirstOrDefault();

            if (program?.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonTerm))
            {
                var campusProgramVersionDetails = await this.campusProgramVersionService.GetCampusProgramVersion(enrollment.CampusId, enrollment.PrgVerId);

                return campusProgramVersionDetails.IsSelfPaced.HasValue && campusProgramVersionDetails.IsSelfPaced.Value;
            }

            return true;
        }

        /// <summary>
        /// The get registration type.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public Task<Enums.ProgramRegistrationType?> GetRegistrationType(Guid enrollmentId)
        {
            return Task.Run(
                () =>
                {
                    Enums.ProgramRegistrationType? registrationType = Enums.ProgramRegistrationType.ByClass;
                    var enrollment = this.EnrollmentsRepository.Get(x => x.StuEnrollId == enrollmentId)
                        .Include(x => x.PrgVer).FirstOrDefault();

                    if (enrollment?.PrgVer?.ProgramRegistrationType != null)
                    {
                        registrationType = (Enums.ProgramRegistrationType)enrollment.PrgVer.ProgramRegistrationType;
                    }

                    return registrationType;
                });
        }

        /// <summary>
        /// The IsProgramVersionSubstantiallyEqual method returns boolean value based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The IsProgramVersionSubstantiallyEqual action checks the program type of the enrollment and in the parameter 
        /// it requires EnrollmentId. 
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// Returns  true or false.
        /// </returns>
        public async Task<bool> IsProgramVersionSubstantiallyEqual(Guid enrollmentId)
        {
            var enrollment = await this.GetEnrollmentDetailsById(enrollmentId);
            if (enrollment.AcademicCalendarId == Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm) || enrollment.AcademicCalendarId == Convert.ToInt32(SyAcademicCalendarProgram.NonTerm))
            {
                var campusProgramVersionDetails = await this.campusProgramVersionService.GetCampusProgramVersion(enrollment.Enrollment.CampusId, enrollment.Enrollment.PrgVerId);

                return Convert.ToBoolean(campusProgramVersionDetails?.TermSubEqualInLen) == true;
            }

            return false;
        }

        /// <summary>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the withdrawal period of enrollment for credit hours by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardWithdrawalPeriodOfEnrollment action method gets the period of enrollment on which student has withdrawn from the program for credit hours by given enrollment id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The CurrentPeriodDetailsWithCreditsEarned with data like credit earned, weeks completed, withdrawal period etc.
        /// </returns>
        public async Task<CurrentPeriodDetailsWithCreditsEarned> GetNonStandardWithdrawalPeriodOfEnrollment(Guid enrollmentId)
        {
            return await Task.Run(
                      async () =>
                      {
                          var withdrawalPaymentPeriod = new CurrentPeriodDetailsWithCreditsEarned();

                          var enrollmentDetail = await this.GetEnrollmentDetailsById(enrollmentId);
                          if (enrollmentDetail.ResultStatus != ApiMsgs.ENROLLMENT_DETAILS_CALC_SUCCESS)
                          {
                              withdrawalPaymentPeriod.ResultStatus = enrollmentDetail.ResultStatus;
                              return withdrawalPaymentPeriod;
                          }

                          if (enrollmentDetail.AcademicCalendarId != null && ((int)enrollmentDetail.AcademicCalendarId != (int)Constants.AcademicCalendars.NonStandardTerm && (int)enrollmentDetail.AcademicCalendarId != (int)Constants.AcademicCalendars.NonTerm))
                          {
                              withdrawalPaymentPeriod.ResultStatus = ApiMsgs.NOT_NONTRM_OR_NON_STANDARD_TERM_PROGRAM;
                              return withdrawalPaymentPeriod;
                          }

                          if (enrollmentDetail.IsSelfPaced && enrollmentDetail.AcademicCalendarId == (int)Constants.AcademicCalendars.NonTerm)
                          {
                              withdrawalPaymentPeriod.ResultStatus = ApiMsgs.SELF_PACED_PROGRAM;
                              return withdrawalPaymentPeriod;
                          }

                          var creditHoursPeriodDetails = await this.GetCreditHourPeriodsLengthByEnrollmentId(enrollmentId);
                          if (creditHoursPeriodDetails.CreditHourProgramePeriodDetailsList.Count == 0)
                          {
                              withdrawalPaymentPeriod.ResultStatus = creditHoursPeriodDetails.ResultStatus;
                              return withdrawalPaymentPeriod;
                          }

                          var creditHourProgramPeriodDetails = creditHoursPeriodDetails.CreditHourProgramePeriodDetailsList;
                          var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollmentDetail.Enrollment.CampusId);

                          withdrawalPaymentPeriod = trackSapAttendance == TrackSapAttendance.ByDay ?
                                                         await this.GetCreditHrsPayPeriodForStudentAttendance(
                                                             false,
                                                             enrollmentDetail.Enrollment,
                                                             enrollmentDetail.EndDateTime,
                                                             creditHourProgramPeriodDetails.ToList(),
                                                             enrollmentDetail.AcademicCalendarId ?? 0)
                                                         : await this.GetCreditHrsPayPeriodForClassSectionAttendance(
                                                               false,
                                                               enrollmentDetail.Enrollment,
                                                               enrollmentDetail.EndDateTime,
                                                               creditHourProgramPeriodDetails.ToList(),
                                                               enrollmentDetail.AcademicCalendarId ?? 0);

                          return withdrawalPaymentPeriod;
                      });
        }

        /// <summary>
        /// The get program version detail by enrollment id returns the program versions details based on the enrollment Id
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is of type Guid.
        /// </param>
        /// <returns>
        /// The Program version details.
        /// </returns>
        public async Task<ProgramVersionDetails> GetProgramVersionDetailByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           ProgramVersionDetails programVersionDetails = null;
                           var enrollment = await this.enrollmentService.Get(enrollmentId);
                           if (enrollment != null)
                           {
                               var programVersion = this.ProgramVersionRepository
                                   .Get(pv => pv.PrgVerId == enrollment.PrgVerId).Include(p => p.Prog)
                                   .Include(x => x.ArStuEnrollments).ThenInclude(x => x.ArR2t4terminationDetails)
                                   .FirstOrDefault();
                               var withdrawalDate = this.GetWithdrawalDate(programVersion);

                               if (programVersion?.Prog.Acid != null)
                               {
                                   var academicCalendarId = programVersion.Prog.Acid.Value;
                                   programVersionDetails =
                                       new ProgramVersionDetails
                                       {
                                           AcademicCalendarId = academicCalendarId,
                                           ProgramId = programVersion.ProgId,
                                           ProgramVersionId = programVersion.PrgVerId,
                                           WithdrawalDate = withdrawalDate,
                                           CampusId = enrollment.CampusId
                                       };
                               }
                           }

                           return programVersionDetails;
                       });
        }

        /// <inheritdoc />
        /// <summary>
        /// The GetTermDetailsForPaymentPeriod method returns the program terms details defined for it.
        /// </summary>
        /// <remarks>
        /// The GetTermDetailsForPaymentPeriod action returns the program terms details defined for it and in the parameter 
        /// it requires EnrollmentId and  withdrawal Date. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal Date.
        /// </param>
        /// <returns>
        ///  The payment period details is the payment period details of the student and is of type CreditHourPaymentPeriodDetails. 
        /// </returns>
        public async Task<CreditHourPaymentPeriodDetails> GetTermDetailsForPaymentPeriod(Guid enrollmentId, DateTime withdrawalDate)
        {
            return await Task.Run(
                       () =>
                           {
                               var paymentPeriodDetails = new CreditHourPaymentPeriodDetails();
                               return this.GetPaymentPeriodDetailsForTerms(
                                   enrollmentId,
                                   withdrawalDate,
                                   paymentPeriodDetails);
                           });
        }

        /// <summary>
        /// The GetCampusProgramVersionDetails action method campus program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// Based on the EnrollmentId ,it search all the enrolled program details and returns the campus program version details.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns the campus program version details.
        /// </returns>
        public async Task<CampusProgramVersions> GetCampusProgramVersionDetails(Guid enrollmentId)
        {
            var campusProgramVersionDetails = new CampusProgramVersions();
            var programVersionDetails = await this.GetProgramVersionDetailByEnrollmentId(enrollmentId);
            if (programVersionDetails != null)
            {
                campusProgramVersionDetails = await this.campusProgramVersionService.GetCampusProgramVersion(programVersionDetails.CampusId, programVersionDetails.ProgramVersionId);
            }

            return campusProgramVersionDetails;
        }

        /// <summary>
        /// The get all program version by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<AdvStagingProgramV1>> GetAllProgramVersionByCampus(Guid campusId)
        {
            var campusProgramVersionDetails = this.mapper.Map<IList<CampusProgramVersions>>(await
                this.campusProgramVersionService.GetCampusProgramVersion());
            if (campusProgramVersionDetails.Any())
            {
                var progVersionToReturn = this.mapper
                    .Map<IList<AdvStagingProgramV1>>(
                        this.ProgramVersionRepository.Get(
                            p => p.CampGrp.SyCmpGrpCmps.Any(c => c.CampusId == campusId) && campusProgramVersionDetails
                                     .Select(x => x.ProgramVersionId).Contains(p.PrgVerId))).ToList();
                return progVersionToReturn;
            }
            else
            {
                return new List<AdvStagingProgramV1>();
            }
        }

        /// <summary>
        /// The get start and end date by enrollment retrieves the start date and end date of enrollment.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="EnrollmentDetails"/>.
        /// </returns>
        public async Task<EnrollmentDetails> GetEnrollmentDetailsById(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                           {
                               var enrollmentDetails = new EnrollmentDetails();
                               var enrollment = await this.enrollmentService.Get(enrollmentId);
                               if (enrollment == null)
                               {
                                   enrollmentDetails.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                                   return enrollmentDetails;
                               }

                               var programVersion = this.ProgramVersionRepository.Get(x => x.PrgVerId == enrollment.PrgVerId)
                                   .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                                   .ThenInclude(x => x.ArR2t4terminationDetails)
                                   .Include(x => x.Prog)
                                   .FirstOrDefault();

                               if (programVersion?.ArCampusPrgVersions == null)
                               {
                                   enrollmentDetails.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Program Version Id {enrollment.PrgVerId}.";
                                   return enrollmentDetails;
                               }

                               var campusProgramVersion = programVersion.ArCampusPrgVersions.FirstOrDefault(x => x.CampusId == enrollment.CampusId);

                               var isSelfPaced = campusProgramVersion?.IsSelfPaced;
                               if (isSelfPaced != null)
                               {
                                   enrollmentDetails.IsSelfPaced = (bool)isSelfPaced;
                               }

                               var calculationPeriodTypeId = campusProgramVersion?.CalculationPeriodTypeId;
                               if (calculationPeriodTypeId != null)
                               {
                                   enrollmentDetails.IsPaymentPeriod = await this.calculationPeriodTypeService.IsPaymentPeriodType((Guid)calculationPeriodTypeId);
                               }

                               var academicCalendarId = programVersion.Prog.Acid;
                               if (academicCalendarId != null)
                               {
                                   enrollmentDetails.AcademicCalendarId = academicCalendarId;
                               }

                               var enrollmentDetail = programVersion.ArStuEnrollments.FirstOrDefault(x => x.StuEnrollId == enrollmentId && x.CampusId == enrollment.CampusId);
                               if (enrollmentDetail?.ArR2t4terminationDetails == null)
                               {
                                   enrollmentDetails.ResultStatus = $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                                   return enrollmentDetails;
                               }

                               var terminationDetails = enrollmentDetail.ArR2t4terminationDetails.OrderByDescending(x => x.UpdatedDate).FirstOrDefault();
                               var studentTermination = this.mapper.Map<StudentTermination>(terminationDetails);
                               if (studentTermination?.LastDateAttended == null)
                               {
                                   enrollmentDetails.ResultStatus = $"{ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND} {enrollmentId}.";
                                   return enrollmentDetails;
                               }

                               var withdrawalDateTime = studentTermination.LastDateAttended ?? DateTime.MinValue;
                               withdrawalDateTime = new DateTime(
                                   withdrawalDateTime.Year,
                                   withdrawalDateTime.Month,
                                   withdrawalDateTime.Day,
                                   23,
                                   59,
                                   59);

                               enrollmentDetails.StartDateTime = enrollment.StartDate ?? DateTime.MinValue;
                               enrollmentDetails.EndDateTime = withdrawalDateTime;
                               enrollmentDetails.Enrollment = enrollment;
                               enrollmentDetails.AllowExcusedAbsPerPayPrd = Convert.ToDecimal(campusProgramVersion?.AllowExcusAbsPerPayPrd);

                               enrollmentDetails.ResultStatus = ApiMsgs.ENROLLMENT_DETAILS_CALC_SUCCESS;
                               return enrollmentDetails;
                           });
        }

        /// <summary>
        /// The GetCampusProgramVersion action method campus program version details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// Based on the EnrollmentId, it search all the enrolled program details and returns the campus program version details.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns the campus program version details.
        /// </returns>
        public async Task<CampusProgramVersions> GetCampusProgramVersion(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                           {
                               var campusProgramVersionDetails = new CampusProgramVersions();
                               try
                               {
                                   var programVersionDetails =
                                       await this.GetProgramVersionDetailByEnrollmentId(enrollmentId);
                                   if (programVersionDetails != null)
                                   {
                                       campusProgramVersionDetails =
                                           await this.campusProgramVersionService.GetCampusProgramVersionDetails(
                                               programVersionDetails.CampusId,
                                               programVersionDetails.ProgramVersionId);
                                   }

                                   return campusProgramVersionDetails;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();

                                   campusProgramVersionDetails.ResultStatus = false;
                                   return campusProgramVersionDetails;
                               }
                           });
        }

        /// <summary>
        /// The GetStudentAwardsByEnrollmentId action fetches student awards details based on enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetWithdrawalPeriodOfEnrollment action method returns the student awards details based on enrollment id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="lastAttendedDate">
        /// The last Attended Date is a nullable date field and is of type datetime.
        /// </param>
        /// <returns>
        /// Returns the student awards details based on enrollment id.
        /// </returns>
        public async Task<IEnumerable<StudentAwards>> GetStudentAwardsByEnrollmentId(Guid enrollmentId, DateTime? lastAttendedDate)
        {
            return await Task.Run(
                       async () =>
            {
                try
                {
                    IEnumerable<StudentAwards> studentAwardDetails = new List<StudentAwards>();
                    var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                    if (!isClockHour)
                    {
                        var enrollment = await this.enrollmentService.Get(enrollmentId);
                        if (enrollment != null)
                        {
                            var creditHourPaymentPeriodDetails = new CreditHourPaymentPeriodDetails();

                            var programVersion = this.ProgramVersionRepository
                                .Get(x => x.PrgVerId == enrollment.PrgVerId).Include(x => x.ArCampusPrgVersions)
                                .Include(x => x.ArStuEnrollments).ThenInclude(x => x.ArR2t4terminationDetails)
                                .Include(x => x.Prog).FirstOrDefault();

                            if (programVersion != null)
                            {
                                var withdrawalDate = this.GetWithdrawalDate(programVersion);
                                if (withdrawalDate == DateTime.MinValue && lastAttendedDate != null)
                                {
                                    withdrawalDate = lastAttendedDate.Value;
                                }

                                if (withdrawalDate != DateTime.MinValue)
                                {
                                    if (programVersion.Prog != null && programVersion.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm))
                                    {
                                        var campusProgramVersionDetails = await this.GetCampusProgramVersion(enrollmentId);
                                        if (campusProgramVersionDetails != null
                                            && campusProgramVersionDetails.ResultStatus
                                            && campusProgramVersionDetails.TermSubEqualInLen.HasValue
                                            && !campusProgramVersionDetails.TermSubEqualInLen.Value)
                                        {
                                            var paymentPeriodDetailsForTerms = this.GetPaymentPeriodDetailsForTerms(
                                                enrollmentId,
                                                withdrawalDate,
                                                creditHourPaymentPeriodDetails);

                                            if (paymentPeriodDetailsForTerms != null)
                                            {
                                                studentAwardDetails =
                                                    await this.studentAwardsService.GetDirectLoanStudentAwards(
                                                        enrollmentId,
                                                        paymentPeriodDetailsForTerms.TermStartDate,
                                                        paymentPeriodDetailsForTerms.TermEndDate);

                                                return studentAwardDetails;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return studentAwardDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return new List<StudentAwards> { new StudentAwards { ResultStatus = ApiMsgs.FAILED_GETTING_RECORD } };
                }
            });
        }

        /// <summary>
        /// Get Program Version Description by Id
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<ProgramVersion>> GetProgramVersionById(string prgVerId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var result = new ActionResult<ProgramVersion>();

                           try
                           {
                               var dataItem = await this.ProgramVersionRepository.Get()
                               .Where(a => a.PrgVerId.ToString() == prgVerId)
                               .Select(a => new ProgramVersion()
                               {
                                   ProgramVersionDescription = a.PrgVerDescrip,
                                   ProgramVersionId = a.PrgVerId,
                                   ProgramVersionCode = a.PrgVerCode,
                                   CampusGroupId = a.CampGrpId != null ? a.CampGrpId.Value : Guid.Empty,
                                   StatusId = a.StatusId != null ? a.StatusId.Value : Guid.Empty,
                               }).FirstOrDefaultAsync();

                               if (dataItem == null)
                               {
                                   result.ResultStatus = Enums.ResultStatus.NotFound;
                                   result.ResultStatusMessage = $"Program version with PrgVerId of {prgVerId} not found.";
                                   return result;
                               }

                               result.Result = dataItem;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        #region private methods

        /// <summary>
        /// Get Clock HourProgram Length method returns the length of the clock hour program be enrollment id.
        /// </summary>
        /// <param name="enrollmentId">enrollment id</param>
        /// <returns>returns clock hour program length</returns>
        private async Task<string> GetClockHourProgramLength(Guid enrollmentId)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    string programLength = string.Empty;
                    var enrollment = await this.enrollmentService.Get(enrollmentId);
                    if (enrollment != null)
                    {
                        var program = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                        if (program != null)
                        {
                            programLength = enrollment.TransferHours.HasValue ? Convert.ToString(program.Hours - enrollment.TransferHours) : Convert.ToString(program.Hours, CultureInfo.InvariantCulture);
                        }
                    }

                    return programLength;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                }
            });
        }

        /// <summary>
        ///  The get payment periods by enrollment id action returns payment periods for a Clock Hour program whose length is greater than
        ///  the defined academic year by enrollment id.
        /// The output are Number of payment periods, number of full academic years,last payment period length/s depends on remaining period less than or equal to /greater than half an academic year .
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ProgramPeriodDetails> GetNumberOfPaymentPeriods(Guid enrollmentId)
        {
            var programPeriodDetails = new ProgramPeriodDetails();
            return await Task.Run(async () =>
            {
                var programLength = await this.GetProgramLengthByEnrollmentId(enrollmentId);
                var enrollment = await this.enrollmentService.Get(enrollmentId);
                if (enrollment != null)
                {
                    var programVersions = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                    if (programVersions != null)
                    {
                        programPeriodDetails.NumberOfPaymentPeriods = programVersions.PayPeriodPerAcYear;

                        if (Convert.ToDecimal(programLength) > Convert.ToDecimal(programVersions.AcademicYearLength))
                        {
                            var fullAcademicYearNo = Math.Floor(Convert.ToDecimal(Convert.ToDecimal(programLength) / programVersions.AcademicYearLength));
                            programPeriodDetails.NumberOfFullAcademicYears = Convert.ToInt32(fullAcademicYearNo);
                            var programLengthPerPeriod = Convert.ToDecimal(Convert.ToDecimal(programVersions.AcademicYearLength) / programPeriodDetails.NumberOfPaymentPeriods);

                            programPeriodDetails.NumberOfPaymentPeriods = Convert.ToInt32(programVersions.PayPeriodPerAcYear * programPeriodDetails.NumberOfFullAcademicYears) + 1;

                            var academicProgramYearLength = programVersions.AcademicYearLength * programPeriodDetails.NumberOfFullAcademicYears;

                            programPeriodDetails.LastPaymentPeriodLength = Convert.ToDecimal(Convert.ToDecimal(programLength) - academicProgramYearLength);

                            if (programPeriodDetails.LastPaymentPeriodLength > ConstantDecimals.ZeroDecimal && programPeriodDetails.LastPaymentPeriodLength <= programLengthPerPeriod)
                            {
                                programPeriodDetails.PeriodDetailsList = new List<PeriodDetails>();
                                for (int programCount = 1; programCount <= programPeriodDetails.NumberOfPaymentPeriods; programCount++)
                                {
                                    if (programCount <= programPeriodDetails.NumberOfPaymentPeriods - 1)
                                    {
                                        var periodDetails = new PeriodDetails
                                        {
                                            PeriodLength = programLengthPerPeriod,
                                            PeriodNumber = programCount
                                        };
                                        programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                    }
                                    else
                                    {
                                        var periodDetails = new PeriodDetails
                                        {
                                            PeriodLength = programPeriodDetails.LastPaymentPeriodLength,
                                            PeriodNumber = programCount
                                        };
                                        programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                    }

                                    programPeriodDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER}";
                                }
                            }
                            else if (programPeriodDetails.LastPaymentPeriodLength > ConstantDecimals.ZeroDecimal && programPeriodDetails.LastPaymentPeriodLength > programLengthPerPeriod)
                            {
                                var numberOfPaymentPeriods = Convert.ToInt32(programVersions.PayPeriodPerAcYear * programPeriodDetails.NumberOfFullAcademicYears) + 2;
                                programPeriodDetails.NumberOfPaymentPeriods = numberOfPaymentPeriods;
                                programPeriodDetails.LastPaymentPeriodLength = Convert.ToDecimal(Convert.ToDecimal(programLength) - academicProgramYearLength) / 2;
                                programPeriodDetails.PeriodDetailsList = new List<PeriodDetails>();
                                for (int programCount = 1; programCount <= numberOfPaymentPeriods; programCount++)
                                {
                                    if (programCount <= programPeriodDetails.NumberOfPaymentPeriods - 2)
                                    {
                                        var periodDetails = new PeriodDetails
                                        {
                                            PeriodLength = programLengthPerPeriod,
                                            PeriodNumber = programCount
                                        };
                                        programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                    }
                                    else
                                    {
                                        var periodDetails = new PeriodDetails
                                        {
                                            PeriodLength = programPeriodDetails.LastPaymentPeriodLength,
                                            PeriodNumber = programCount
                                        };
                                        programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                    }

                                    programPeriodDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER_AND_REMAINING_GREATER}";
                                }
                            }
                            else if (programPeriodDetails.LastPaymentPeriodLength == ConstantDecimals.ZeroDecimal)
                            {
                                var numberOfPaymentPeriods = Convert.ToInt32(programVersions.PayPeriodPerAcYear * programPeriodDetails.NumberOfFullAcademicYears);
                                programPeriodDetails.NumberOfPaymentPeriods = numberOfPaymentPeriods;
                                programPeriodDetails.PeriodDetailsList = new List<PeriodDetails>();
                                for (int programCount = 1; programCount <= numberOfPaymentPeriods; programCount++)
                                {
                                    var periodDetails = new PeriodDetails
                                    {
                                        PeriodLength = programLengthPerPeriod,
                                        PeriodNumber = programCount
                                    };
                                    programPeriodDetails.LastPaymentPeriodLength = programLengthPerPeriod;
                                    programPeriodDetails.PeriodDetailsList.Add(periodDetails);
                                    programPeriodDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_LENGTH_GREATER_THAN_ACADEMIC_YEAR}";
                                }
                            }
                        }
                    }
                }

                return programPeriodDetails;
            });
        }

        /// <summary>
        ///  The get payment periods length by enrollment id action returns payment periods for a Clock Hour program whose length is greater than
        ///  the defined academic year by enrollment id.
        /// The output are Number of payment periods,payment periods length ,last payment period length/s depends on remaining period less than or equal to /greater than half an academic year .
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<EnrollmentPeriodDetails> GetPeriodsByEnrollmentId(Guid enrollmentId)
        {
            var enrollmentPeriodDetails = new EnrollmentPeriodDetails();
            return await Task.Run(async () =>
            {
                var programLength = await this.GetProgramLengthByEnrollmentId(enrollmentId);
                var enrollment = await this.enrollmentService.Get(enrollmentId);
                if (enrollment != null)
                {
                    var programVersions = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                    if (programVersions != null)
                    {
                        enrollmentPeriodDetails.NumberOfPeriodOfEnrollment = programVersions.PayPeriodPerAcYear;
                        if (Convert.ToDecimal(programLength) > Convert.ToDecimal(programVersions.AcademicYearLength))
                        {
                            var fullAcademicYearNo = Convert.ToDecimal(Convert.ToDecimal(programLength) / programVersions.AcademicYearLength);
                            string[] years = fullAcademicYearNo.ToString("0.00", CultureInfo.InvariantCulture).Split('.');
                            enrollmentPeriodDetails.NumberOfFullAcademicYears = int.Parse(years[0]);
                            var programLengthPerPeriod = Convert.ToDecimal(programVersions.AcademicYearLength);

                            enrollmentPeriodDetails.NumberOfPeriodOfEnrollment = Convert.ToInt32(enrollmentPeriodDetails.NumberOfFullAcademicYears + 1);

                            var academicProgramYearLength = programVersions.AcademicYearLength * enrollmentPeriodDetails.NumberOfFullAcademicYears;

                            enrollmentPeriodDetails.LastEnrollmentPeriodLength = Convert.ToDecimal(Convert.ToDecimal(programLength) - academicProgramYearLength);

                            if (enrollmentPeriodDetails.LastEnrollmentPeriodLength > ConstantDecimals.ZeroDecimal && enrollmentPeriodDetails.LastEnrollmentPeriodLength <= programLengthPerPeriod)
                            {
                                enrollmentPeriodDetails.EnrollmentPeriodsList = new List<EnrollmentPeriods>();
                                for (int programCount = 1; programCount <= enrollmentPeriodDetails.NumberOfPeriodOfEnrollment; programCount++)
                                {
                                    if (programCount <= enrollmentPeriodDetails.NumberOfPeriodOfEnrollment - 1)
                                    {
                                        var enrollmentPeriod = new EnrollmentPeriods
                                        {
                                            EnrollmentLength = programLengthPerPeriod,
                                            EnrollmentPeriod = programCount
                                        };
                                        enrollmentPeriodDetails.EnrollmentPeriodsList.Add(enrollmentPeriod);
                                    }
                                    else
                                    {
                                        var enrollmentPeriod = new EnrollmentPeriods
                                        {
                                            EnrollmentLength = enrollmentPeriodDetails.LastEnrollmentPeriodLength,
                                            EnrollmentPeriod = programCount
                                        };
                                        enrollmentPeriodDetails.EnrollmentPeriodsList.Add(enrollmentPeriod);
                                    }

                                    enrollmentPeriodDetails.ResultStatus = $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                }
                            }
                            else if (enrollmentPeriodDetails.LastEnrollmentPeriodLength == ConstantDecimals.ZeroDecimal)
                            {
                                var numberOfPaymentPeriods = Convert.ToInt32(enrollmentPeriodDetails.NumberOfFullAcademicYears);

                                enrollmentPeriodDetails.NumberOfPeriodOfEnrollment = numberOfPaymentPeriods;

                                enrollmentPeriodDetails.EnrollmentPeriodsList = new List<EnrollmentPeriods>();

                                for (int programCount = 1; programCount <= numberOfPaymentPeriods; programCount++)
                                {
                                    var enrollmentPeriod = new EnrollmentPeriods
                                    {
                                        EnrollmentLength = programLengthPerPeriod,
                                        EnrollmentPeriod = programCount
                                    };

                                    enrollmentPeriodDetails.LastEnrollmentPeriodLength = programLengthPerPeriod;
                                    enrollmentPeriodDetails.EnrollmentPeriodsList.Add(enrollmentPeriod);

                                    enrollmentPeriodDetails.ResultStatus = $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                }
                            }
                        }
                    }
                }

                return enrollmentPeriodDetails;
            });
        }

        /// <summary>
        /// Get credit hour program details method returns the length of the credit hour program by enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// enrollment id
        /// </param>
        /// <returns>
        /// returns credit hour program length
        /// </returns>
        private async Task<CreditHourProgramDetails> GetCreditHourProgramDetails(Guid enrollmentId)
        {
            var programDetails = new CreditHourProgramDetails();
            decimal transferCredits = ConstantDecimals.ZeroDecimal;
            return await Task.Run(async () =>
            {
                try
                {
                    var enrollment = await this.enrollmentService.Get(enrollmentId);
                    if (enrollment != null)
                    {
                        var program = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId)
                            .Include(x => x.Prog).FirstOrDefault();

                        if (program != null)
                        {
                            if (program.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm)
                                || program.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonTerm))
                            {
                                var transferGradesDetails =
                                    await this.transferGradesService.GetTransferGrades(enrollmentId);
                                transferGradesDetails = transferGradesDetails.ToList();
                                if (transferGradesDetails.Any())
                                {
                                    decimal totalTransferCredits = ConstantDecimals.ZeroDecimal;
                                    foreach (var grades in transferGradesDetails)
                                    {
                                        if (grades.IsCourseCompleted)
                                        {
                                            var reqId = grades.ReqId;
                                            var transferCreditDetails =
                                                await this.courseDetailsService.GetTransferCreditsByReqId(reqId);
                                            var transferCredit = transferCreditDetails.Credits;
                                            totalTransferCredits += Convert.ToInt32(transferCredit);
                                        }
                                    }

                                    transferCredits = totalTransferCredits;
                                }

                                if (program.Prog?.Acid == Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm))
                                {
                                    var campusProgramVersionDetails =
                                        await this.campusProgramVersionService.GetCampusProgramVersion(
                                            enrollment.CampusId,
                                            program.PrgVerId);

                                    if (campusProgramVersionDetails != null)
                                    {
                                        programDetails = this.GetCreditHourProgramLength(
                                                program.Credits,
                                                transferCredits,
                                                program.Weeks);
                                    }
                                    else
                                    {
                                        programDetails.ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}";
                                    }
                                }
                                else
                                {
                                    programDetails = this.GetCreditHourProgramLength(
                                        program.Credits,
                                        transferCredits,
                                        program.Weeks);
                                }
                            }
                        }
                        else
                        {
                            programDetails.ResultStatus = $"{ApiMsgs.NOT_NONTRM_OR_NON_STANDARD_TERM_PROGRAM}";
                        }
                    }
                    else
                    {
                        programDetails.ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}";
                    }

                    return programDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return new CreditHourProgramDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The GetCreditHourProgramLength method calculate the credit hour Non-Term and Non -Standard Term program length
        /// </summary>
        /// <param name="credits">
        /// The credits.
        /// </param>
        /// <param name="transferCredits">
        /// The transfer credits.
        /// </param>
        /// <param name="programWeeks">
        /// The program length.
        /// </param>
        /// <returns>
        /// return credit hour program details as number of weeks and number credits of the program
        /// </returns>
        private CreditHourProgramDetails GetCreditHourProgramLength(decimal credits, decimal transferCredits, decimal programWeeks)
        {
            var programDetails = new CreditHourProgramDetails();
            try
            {
                if (transferCredits > Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                {
                    programDetails.Credits = credits - transferCredits;
                    var programCredits = Convert.ToDecimal(programWeeks / credits);
                    var numberOfWeeksInProgram = programCredits * programDetails.Credits;

                    var numberOfWeeksDecimalPart = numberOfWeeksInProgram.ToString("0.00", CultureInfo.InvariantCulture).Split('.')[1];

                    programDetails.Weeks = Convert.ToInt16(Convert.ToDecimal(numberOfWeeksDecimalPart) >
                                                           ConstantDecimals.GreaterThanOneDecimal
                                                               ? Convert.ToDecimal(numberOfWeeksInProgram.ToString("0.00", CultureInfo.InvariantCulture).Split('.')[0]) + Convert.ToDecimal(1)
                                                               : Convert.ToDecimal(numberOfWeeksInProgram.ToString("0.00", CultureInfo.InvariantCulture).Split('.')[0]));
                }
                else
                {
                    programDetails.Credits = credits;
                    programDetails.Weeks = Convert.ToInt16(programWeeks);
                }

                programDetails.ResultStatus = $"{ApiMsgs.CREDIT_HOUR_PROGRAM_LENGTH}";
                return programDetails;
            }
            catch (Exception e)
            {
                e.TrackException();

                return new CreditHourProgramDetails
                {
                    ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                };
            }
        }

        /// <summary>
        /// The get payment period for student clock attendance.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal Date.
        /// </param>
        /// <param name="programPeriodDetails">
        /// The program period details.
        /// </param>
        /// <param name="programSchedules">
        /// The program schedules.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<CurrentPeriodDetails> GetPaymentPeriodForStudentClockAttendance(
            Enrollment enrollment,
            DateTime withdrawalDate,
            ProgramPeriodDetails programPeriodDetails,
            List<ProgramSchedule> programSchedules)
        {
            var withdrawalPaymentPeriod = new CurrentPeriodDetails { StartDateOfWithdrawalPeriod = enrollment?.StartDate };
            if (enrollment?.StartDate != null
                && enrollment.StartDate != DateTime.MinValue
                && withdrawalDate != DateTime.MinValue
                && programSchedules != null)
            {
                var programScheduleDetails = new List<ProgramScheduleDetails>();
                programSchedules.ToList().ForEach(
                    programSchedule =>
                    {
                        programScheduleDetails = programScheduleDetails.Concat(programSchedule.ProgramScheduleDetails).ToList();
                    });

                var periodNumber = 0;
                var totalAttendance = 0m;

                var periodLength = programPeriodDetails.PeriodDetailsList[periodNumber].PeriodLength;
                var studentClockAttendances = await this.studentClockAttendanceService.GetDetailsByEnrollmentId(enrollment.EnrollmentId);
                var studentAttendanceList = studentClockAttendances.Where(x => x.RecordDate >= enrollment.StartDate && x.RecordDate <= withdrawalDate).ToList();
                var attendanceDates = studentAttendanceList.Select(x => x.RecordDate).OrderBy(x => x).Distinct().ToList();
                withdrawalPaymentPeriod.WithdrawalPeriod = periodNumber + 1;
                withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod = enrollment.StartDate;
                foreach (var attDate in attendanceDates)
                {
                    var studentAttendances = studentAttendanceList.Where(x => x.RecordDate == attDate);
                    foreach (var studentAttendance in studentAttendances)
                    {
                        var programScheduleDetail = programScheduleDetails.FirstOrDefault(
                            x => x.ScheduleId == studentAttendance.ScheduleId
                                 && x.DayOfWeek == (decimal?)studentAttendance.RecordDate.DayOfWeek);
                        if (programScheduleDetail != null)
                        {
                            // ConstantDecimals.AttendanceUpperLimit or null indicates that there is no actual hours entry for that record, so reset it to 0 .
                            if (studentAttendance.ActualHours == ConstantDecimals.AttendanceUpperLimit || studentAttendance.ActualHours == null)
                            {
                                studentAttendance.ActualHours = 0m;
                            }

                            studentAttendance.ScheduleHours = programScheduleDetail.Total ?? 0m;
                        }

                        var attendanceValue = Convert.ToDecimal(studentAttendance.ActualHours);
                        if (periodLength > totalAttendance && periodLength > (attendanceValue + totalAttendance))
                        {
                            totalAttendance += attendanceValue;
                        }
                        else if (periodLength <= attendanceValue + totalAttendance)
                        {
                            if (attDate < withdrawalDate)
                            {
                                var hourDebitedToPreviousPeriod = studentAttendanceList.Where(a => a.RecordDate.Date == attDate.Date && a.RecordDate < attDate).Sum(x => x.ActualHours);
                                withdrawalPaymentPeriod.HourDebitedToPreviousPeriod = (hourDebitedToPreviousPeriod ?? 0m) + (periodLength - totalAttendance);
                                totalAttendance = (attendanceValue + totalAttendance) - periodLength;
                                periodLength = this.GetNextPeriodLength(programPeriodDetails, periodNumber + 1);
                                if (periodLength == -1)
                                {
                                    return withdrawalPaymentPeriod;
                                }

                                periodNumber++;
                                withdrawalPaymentPeriod.WithdrawalPeriod = periodNumber + 1;
                                var dates = studentAttendanceList
                                    .Where(a => a.RecordDate.Date == studentAttendance.RecordDate.Date)
                                    .Select(x => x.RecordDate).ToList();

                                withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod =
                                    totalAttendance > 0
                                        ? studentAttendance.RecordDate
                                        : this.GetPeriodStartDate(dates, studentAttendance.RecordDate);
                            }
                            else
                            {
                                return withdrawalPaymentPeriod;
                            }
                        }
                    }
                }
            }

            return withdrawalPaymentPeriod;
        }

        /// <summary>
        /// The GetPaymentPeriodForClassSectionAttendance returns the Withdrawal Payment Period based on the Class section attendance details.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="allowExcuseAbsPerPaymentPeriod">
        /// The allow excused Abs Per Payment Period.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal Date.
        /// </param>
        /// <param name="programPeriodDetails">
        /// The program period details.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<CurrentPeriodDetails> GetPaymentPeriodForClassSectionAttendance(
            Enrollment enrollment,
            decimal allowExcuseAbsPerPaymentPeriod,
            DateTime withdrawalDate,
            ProgramPeriodDetails programPeriodDetails)
        {
            var withdrawalPaymentPeriod = new CurrentPeriodDetails();

            Guid enrollmentId = enrollment.EnrollmentId;
            if (enrollment.StartDate != null && enrollment.StartDate != DateTime.MinValue
                && withdrawalDate != DateTime.MinValue)
            {
                withdrawalPaymentPeriod.WithdrawalPeriod = 1;
                withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod = enrollment.StartDate;

                var classSectionAttendances =
                    await this.classSectionAttendanceService.GetAttendanceByEnrollmentAndDateRange(
                        enrollmentId,
                        enrollment.StartDate,
                        withdrawalDate);

                if (classSectionAttendances != null)
                {
                    var totalAttendance = 0m;
                    var attendance = classSectionAttendances.ActualAttendanceList;
                    var absence = classSectionAttendances.ExcusedAbsenceList;

                    List<DateTime> meetingDates = attendance.Select(x => x.MeetingDate).ToList().Concat(absence.Select(x => x.MeetingDate).ToList()).ToList();
                    meetingDates = meetingDates.OrderBy(x => x).Distinct().ToList();
                    var periodNumber = 0;
                    var periodLength = programPeriodDetails.PeriodDetailsList[periodNumber].PeriodLength;
                    var actualAbs = 0m;
                    var maxExcusedAbsAllowed = (allowExcuseAbsPerPaymentPeriod / 100) * periodLength;
                    foreach (var date in meetingDates)
                    {
                        var attendanceValue = attendance.Where(a => a.MeetingDate == date).Sum(x => x.AttendanceValue);
                        actualAbs += absence.Where(a => a.MeetingDate == date).Sum(x => Convert.ToDecimal(x.AttendanceValue));

                        // if maxExcusedAbsAllowed > actualAbs, then we only need to consider the actualAbs else maximum maxExcusedAbsAllowed will be consider as excused absent
                        periodLength = this.GetPeriodLengthByAllowedAndActualAbsent(programPeriodDetails, maxExcusedAbsAllowed, actualAbs, periodNumber);

                        if (periodLength > totalAttendance && periodLength > (attendanceValue + totalAttendance))
                        {
                            totalAttendance += attendanceValue;
                        }
                        else if (periodLength <= attendanceValue + totalAttendance)
                        {
                            if (date < withdrawalDate)
                            {
                                var totalHoursAttendedInCurrentPeriod = attendance.Where(a => a.MeetingDate <= date).Sum(x => x.AttendanceValue);
                                var recordsNotCounted = attendance.Where(a => a.MeetingDate > date).Count();
                                withdrawalPaymentPeriod.HourDebitedToPreviousPeriod =
                                    totalHoursAttendedInCurrentPeriod - periodLength;
                                totalAttendance = (attendanceValue + totalAttendance) - periodLength;
                                periodLength = this.GetNextPeriodLength(programPeriodDetails, periodNumber + 1);
                                if (periodLength == -1)
                                {
                                    return withdrawalPaymentPeriod;
                                }

                                //only if the hours debited to previous are greater than 0, meaning that student is over the hours for the first period
                                if (withdrawalPaymentPeriod.HourDebitedToPreviousPeriod > 0 || recordsNotCounted > 0)
                                {
                                    periodNumber++;
                                    withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod = date;
                                    withdrawalPaymentPeriod.WithdrawalPeriod = periodNumber + 1;
                                    var dates = attendance.Where(x => x.MeetingDate.Date == date.Date)
                                        .Select(x => x.MeetingDate).ToList();

                                    withdrawalPaymentPeriod.StartDateOfWithdrawalPeriod = totalAttendance > 0 ? date.AddHours(Convert.ToDouble(totalAttendance)) : this.GetPeriodStartDate(dates, date);
                                    actualAbs = 0m;
                                }

                            }
                            else
                            {
                                return withdrawalPaymentPeriod;
                            }
                        }
                    }
                }
            }

            return withdrawalPaymentPeriod;
        }

        /// <summary>
        /// The get next period start date by given the attendance list, date and previousHours.
        /// </summary>
        /// <param name="attendanceDates">
        /// The attendance.
        /// </param>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <returns>
        /// The DateTime.
        /// </returns>
        private DateTime GetPeriodStartDate(
            List<DateTime> attendanceDates,
            DateTime date)
        {
            var attendanceIndex = attendanceDates.IndexOf(date);
            return (attendanceDates.Count - 1) > attendanceIndex ? attendanceDates[attendanceIndex + 1] : date.Date.AddDays(1);
        }

        /// <summary>
        /// The GetNextPeriodLength returns the next available period length based on programPeriodDetails and periodNumber.
        /// </summary>
        /// <param name="programPeriodDetails">
        /// The program period details.
        /// </param>
        /// <param name="periodNumber">
        /// The period number.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        private decimal GetNextPeriodLength(ProgramPeriodDetails programPeriodDetails, int periodNumber)
        {
            decimal periodLength;
            if (programPeriodDetails?.PeriodDetailsList?.Count >= periodNumber + 1)
            {
                periodLength = programPeriodDetails.PeriodDetailsList[periodNumber].PeriodLength;
            }
            else
            {
                periodLength = -1;
            }

            return periodLength;
        }

        /// <summary>
        /// This GetPeriodLengthByAllowedAndActualAbsent function get period length by max allowed absent and actual absent count.
        /// </summary>
        /// <param name="programPeriodDetails">
        /// The program period details.
        /// </param>
        /// <param name="maxExcusedAbsAllowed">
        /// The max excused abs allowed.
        /// </param>
        /// <param name="actualAbs">
        /// The actual abs.
        /// </param>
        /// <param name="periodNumber">
        /// The period number.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        private decimal GetPeriodLengthByAllowedAndActualAbsent(
            ProgramPeriodDetails programPeriodDetails,
            decimal maxExcusedAbsAllowed,
            decimal actualAbs,
            int periodNumber)
        {
            decimal periodLength;
            if (maxExcusedAbsAllowed >= actualAbs)
            {
                periodLength = programPeriodDetails.PeriodDetailsList[periodNumber].PeriodLength - actualAbs;
            }
            else
            {
                periodLength = programPeriodDetails.PeriodDetailsList[periodNumber].PeriodLength - maxExcusedAbsAllowed;
            }

            return periodLength;
        }

        /// <summary>
        /// The get credit hour program period length by enrollment id action returns program credits and weeks for a credit Hour program.
        /// </summary>
        /// <remarks>
        /// The get credit hour program length by enrollment id action returns program length and weeks for a credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it search all the enrolled program details and program versions and returns program credits and weeks.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns program credits and weeks.
        /// </returns>
        private async Task<CreditHourProgramePeriodDetails> GetCreditPeriodsByEnrollmentId(Guid enrollmentId)
        {
            var creditHourProgramPeriodDetails = new CreditHourProgramePeriodDetails();
            return await Task.Run(async () =>
            {
                var creditHourProgramLength = await this.GetCreditHourProgramLengthByEnrollmentId(enrollmentId);
                var transferCredit = await this.GetTransferCredits(enrollmentId);
                var programLength = creditHourProgramLength.Credits;

                var enrollment = await this.enrollmentService.Get(enrollmentId);
                if (enrollment != null)
                {
                    var programVersions = this.ProgramVersionRepository.Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();
                    if (programVersions != null)
                    {
                        if (Convert.ToDecimal(programLength) > Convert.ToDecimal(programVersions.AcademicYearLength))
                        {
                            var fullAcademicYearNo = Math.Floor(Convert.ToDecimal(Convert.ToDecimal(programLength) / programVersions.AcademicYearLength));

                            creditHourProgramPeriodDetails.NumberOfFullAcademicYears = Convert.ToInt32(fullAcademicYearNo);
                            var programLengthPerPeriod = Convert.ToDecimal(programVersions.AcademicYearLength);
                            var actualWeek = Convert.ToDecimal(programVersions.AcademicYearWeeks);

                            if (transferCredit > ConstantDecimals.ZeroDecimal)
                            {
                                var programWeek = Convert.ToDecimal((programVersions.Weeks / programVersions.Credits) * programLength);
                                var integral = Math.Truncate(programWeek);
                                var fractional = programWeek - integral;

                                programVersions.Weeks = Convert.ToInt16(fractional > ConstantDecimals.GreaterThanOneDecimal ? Math.Ceiling(programWeek) : Math.Floor(programWeek));
                            }

                            var numberOfPeriodOfEnrollment = Convert.ToInt32(creditHourProgramPeriodDetails.NumberOfFullAcademicYears + 1);

                            var academicProgramYearLength = programVersions.AcademicYearLength * creditHourProgramPeriodDetails.NumberOfFullAcademicYears;

                            creditHourProgramPeriodDetails.LastEnrollmentPeriodLength = Convert.ToDecimal(Convert.ToDecimal(programLength) - academicProgramYearLength);
                            creditHourProgramPeriodDetails.LastEnrollmentWeek = Convert.ToDecimal(programVersions.Weeks - (programVersions.AcademicYearWeeks * creditHourProgramPeriodDetails.NumberOfFullAcademicYears));

                            if (creditHourProgramPeriodDetails.LastEnrollmentPeriodLength > ConstantDecimals.ZeroDecimal && creditHourProgramPeriodDetails.LastEnrollmentPeriodLength <= programLengthPerPeriod)
                            {
                                creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList = new List<CreditHourProgramePeriods>();
                                for (var programCount = 1; programCount <= numberOfPeriodOfEnrollment; programCount++)
                                {
                                    if (programCount <= numberOfPeriodOfEnrollment - 1)
                                    {
                                        var creditHourProgramPeriods = new CreditHourProgramePeriods
                                        {
                                            Period = programCount,
                                            Credits = programLengthPerPeriod,
                                            Weeks = Convert.ToInt16(actualWeek)
                                        };
                                        creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                    }
                                    else
                                    {
                                        var creditHourProgramPeriods = new CreditHourProgramePeriods
                                        {
                                            Period = programCount,
                                            Credits = creditHourProgramPeriodDetails.LastEnrollmentPeriodLength,
                                            Weeks = Convert.ToInt16(creditHourProgramPeriodDetails.LastEnrollmentWeek)
                                        };
                                        creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                    }

                                    creditHourProgramPeriodDetails.ResultStatus = $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                }
                            }
                            else if (creditHourProgramPeriodDetails.LastEnrollmentPeriodLength == ConstantDecimals.ZeroDecimal)
                            {
                                var numberOfPaymentPeriods = Convert.ToInt32(creditHourProgramPeriodDetails.NumberOfFullAcademicYears);

                                creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList = new List<CreditHourProgramePeriods>();

                                for (var programCount = 1; programCount <= numberOfPaymentPeriods; programCount++)
                                {
                                    if (programCount <= numberOfPaymentPeriods - 1)
                                    {
                                        var creditHourProgramPeriods =
                                            new CreditHourProgramePeriods
                                            {
                                                Period = programCount,
                                                Credits = programLengthPerPeriod,
                                                Weeks = Convert.ToInt16(actualWeek)
                                            };

                                        creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                    }
                                    else
                                    {
                                        var actualNumberOfPaymentPeriods = numberOfPaymentPeriods - 1;
                                        creditHourProgramPeriodDetails.LastEnrollmentWeek = Convert.ToDecimal(programVersions.Weeks - (programVersions.AcademicYearWeeks * actualNumberOfPaymentPeriods));

                                        var creditHourProgramPeriods =
                                            new CreditHourProgramePeriods
                                            {
                                                Period = programCount,
                                                Credits = programLengthPerPeriod,
                                                Weeks = Convert.ToInt16(creditHourProgramPeriodDetails.LastEnrollmentWeek)
                                            };

                                        creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                    }

                                    creditHourProgramPeriodDetails.LastEnrollmentPeriodLength = programLengthPerPeriod;

                                    creditHourProgramPeriodDetails.ResultStatus = $"{ApiMsgs.ENROLLMENT_LENGTH_SUCCESS}";
                                }
                            }
                        }
                    }
                }

                return creditHourProgramPeriodDetails;
            });
        }

        /// <summary>
        /// The get scheduled and total hours returns the scheduled hours and total hours for an enrollment.
        /// </summary>
        /// <param name="isByDay">
        /// The is by day.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <returns>
        /// The <see cref="ScheduledAndTotalHours"/>.
        /// </returns>
        private async Task<ScheduledAndTotalHours> GetScheduledAndTotalHours(bool isByDay, Enrollment enrollment, DateTime withdrawalDate)
        {
            return await Task.Run(async () =>
            {
                var scheduledAndTotalHours = new ScheduledAndTotalHours();

                var periodOfEnrollment = await this.GetWithdrawalPeriodOfEnrollment(enrollment.EnrollmentId);
                var periodsByEnrollment =
                    await this.GetEnrollmentPeriodsLengthByEnrollmentId(enrollment.EnrollmentId);
                if (periodOfEnrollment.ResultStatus != ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL
                    && periodOfEnrollment.ResultStatus
                    != ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL)
                {
                    scheduledAndTotalHours.ResultStatus = periodOfEnrollment.ResultStatus;
                    return scheduledAndTotalHours;
                }

                if (periodOfEnrollment.StartDateOfWithdrawalPeriod == null || withdrawalDate == DateTime.MinValue)
                {
                    scheduledAndTotalHours.ResultStatus = ApiMsgs.INVALID_START_WITHDRAWAL_DATE;
                    return scheduledAndTotalHours;
                }

                if (!periodsByEnrollment.EnrollmentPeriodsList.Any())
                {
                    scheduledAndTotalHours.ResultStatus = periodsByEnrollment.ResultStatus;
                    return scheduledAndTotalHours;
                }

                scheduledAndTotalHours.TotalHoursInPeriod = periodsByEnrollment.EnrollmentPeriodsList[periodOfEnrollment.WithdrawalPeriod - 1].EnrollmentLength;
                scheduledAndTotalHours.HoursScheduledToComplete = await this.CalculateScheduledHoursToComplete(isByDay, enrollment, withdrawalDate, periodOfEnrollment.StartDateOfWithdrawalPeriod ?? DateTime.MinValue, periodOfEnrollment.HourDebitedToPreviousPeriod);
                if (scheduledAndTotalHours.TotalHoursInPeriod < scheduledAndTotalHours.HoursScheduledToComplete)
                {
                    scheduledAndTotalHours.HoursScheduledToComplete = scheduledAndTotalHours.TotalHoursInPeriod;
                }

                scheduledAndTotalHours.ResultStatus = ApiMsgs.SCHEDULE_TOTAL_PE_HOURS_CALCULATED_SUCCESSFUL;
                return scheduledAndTotalHours;
            });
        }

        /// <summary>
        /// The get credit earned list.
        /// </summary>
        /// <param name="enrollmentDetail">
        /// The enrollment detail.
        /// </param>
        /// <param name="reqIds">
        /// The req ids.
        /// </param>
        /// <param name="termIds">
        /// The term ids.
        /// </param>
        /// <param name="resultDetails">
        /// The result details.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<List<ListItem<DateTime, decimal>>> GetCreditEarnedList(
            Enrollment enrollmentDetail,
            List<Guid> reqIds,
            List<Guid> termIds,
            IEnumerable<Results> resultDetails)
        {
            var creditSummaryList =
                await this.creditSummaryService.GetCreditSummaryDetail(enrollmentDetail.EnrollmentId, reqIds, termIds);

            var creditList = new List<ListItem<DateTime, decimal>>();
            resultDetails.ForEach(
                result =>
                {
                    var creditSummaryDetails = creditSummaryList.Where(
                        cs => new Guid(cs.ClassSectionId) == result.ClassSectionId
                              && cs.StudentEnrollId == result.StudentEnrollmentId).ToList();

                    creditSummaryDetails.ForEach(
                        x =>
                        {
                            if (result.DateCompleted != null && result.DateCompleted != DateTime.MinValue)
                            {
                                if (creditList.Any(c => c.Text == result.DateCompleted))
                                {
                                    var oldValue = creditList.FirstOrDefault(c => c.Text == result.DateCompleted)?.Value ?? 0m;

                                    creditList.ForEach(c =>
                                            {
                                                if (c.Text == result.DateCompleted)
                                                {
                                                    c.Value = oldValue + (x.FaCreditsEarned ?? 0m);
                                                }
                                            });
                                }
                                else
                                {
                                    creditList.Add(
                                        new ListItem<DateTime, decimal>
                                        {
                                            Text =
                                                    result.DateCompleted ?? DateTime.MinValue,
                                            Value = x.FaCreditsEarned ?? 0m
                                        });
                                }
                            }
                        });
                });
            return creditList;
        }

        /// <summary>
        /// The get credit hrs pay period for class section attendance.
        /// </summary>
        /// <param name="isPaymentPeriod">
        /// is Payment Period
        /// </param>
        /// <param name="enrollmentDetail">
        /// The enrollment detail.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        /// <param name="creditHourProgramPeriodDetails">
        /// The credit hour program period details.
        /// </param>
        /// <param name="academicCalendarId">
        /// The Academic Calendar Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<CurrentPeriodDetailsWithCreditsEarned> GetCreditHrsPayPeriodForClassSectionAttendance(
            bool isPaymentPeriod,
            Enrollment enrollmentDetail,
            DateTime endDateTime,
            List<CreditHourProgramePeriods> creditHourProgramPeriodDetails,
            int academicCalendarId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var paymentPeriodNumber = 1;

                           var currentPeriodDetailsWithCreditsEarned = new CurrentPeriodDetailsWithCreditsEarned
                           {
                               CreditsEarned = 0m,
                               WithdrawalPeriod = paymentPeriodNumber
                           };

                           var creditHourProgramPeriod = creditHourProgramPeriodDetails[0];
                           if (creditHourProgramPeriod == null)
                           {
                               currentPeriodDetailsWithCreditsEarned.ResultStatus = ApiMsgs.CREDIT_HOUR_PROGRAM_LENGTH_ZERO;
                               return currentPeriodDetailsWithCreditsEarned;
                           }

                           var attendanceDetails = await this.classSectionAttendanceService.GetAttendanceByEnrollmentAndDateRange(enrollmentDetail.EnrollmentId, enrollmentDetail.StartDate, endDateTime);

                           var scheduleStartDate = await this.GetScheduleStartDateByEnrollmentId(enrollmentDetail.EnrollmentId, true);
                           if (academicCalendarId == (int)Constants.AcademicCalendars.NonStandardTerm)
                           {
                               var termDetails = this.termService.GetTerms(enrollmentDetail.EnrollmentId);
                               scheduleStartDate = termDetails.OrderBy(x => x.StartDate).FirstOrDefault()?.StartDate;
                           }

                           if (scheduleStartDate == null)
                           {
                               currentPeriodDetailsWithCreditsEarned.ResultStatus = ApiMsgs.SCHEDULE_START_DATE_NOT_FOUND;
                               return currentPeriodDetailsWithCreditsEarned;
                           }
                           else
                           {
                               currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = scheduleStartDate;
                           }

                           var results = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentDetail.EnrollmentId);
                           var clsSectIds = results.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();

                           var totalWeeksScheduled = await this.classSectionMeetingService.GetScheduledWeeksForDateRangeByClassSectionIds(
                                                         enrollmentDetail,
                                                         false,
                                                         clsSectIds,
                                                         (DateTime)scheduleStartDate,
                                                         endDateTime);

                           var totalWeeksScheduledDates = totalWeeksScheduled.Item2.ToList();
                           totalWeeksScheduledDates = totalWeeksScheduledDates.Concat(attendanceDetails.ActualAttendanceList.Where(x => x.AttendanceValue > 0).Select(x => x.MeetingDate).ToList()).Distinct().OrderBy(x => x).ToList();

                           var weekCompletionDate = DateTime.MinValue;
                           for (var startDate = scheduleStartDate; startDate <= endDateTime;)
                           {
                               var date = startDate;
                               var endDate = startDate.Value.AddDays(7);
                               var actualAttendanceList = attendanceDetails.ActualAttendanceList.Where(x => x.MeetingDate >= date && x.MeetingDate <= endDate).ToList();
                               if (creditHourProgramPeriodDetails.Count >= paymentPeriodNumber)
                               {
                                   if (actualAttendanceList.Any() || weekCompletionDate != DateTime.MinValue)
                                   {
                                       if (actualAttendanceList.Any())
                                       {
                                           currentPeriodDetailsWithCreditsEarned.WeeksCompleted++;
                                           currentPeriodDetailsWithCreditsEarned.WeeksCompletedOn = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                                       }

                                       if (currentPeriodDetailsWithCreditsEarned.WeeksCompleted == creditHourProgramPeriod.Weeks)
                                       {
                                           weekCompletionDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                                       }

                                       // if weeks defined for the payment period is completed 
                                       if (currentPeriodDetailsWithCreditsEarned.WeeksCompleted >= creditHourProgramPeriod.Weeks)
                                       {
                                           // currentPeriodDetailsWithCreditsEarned.WeeksCompleted
                                           if (currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod != null)
                                           {
                                               var periodStartDate = currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue;
                                               var classSectionIds = attendanceDetails.ActualAttendanceList.Where(x => x.MeetingDate >= periodStartDate && x.MeetingDate <= endDate).Select(x => x.ClassSectionId)
                                                   .Distinct().ToList();

                                               if (classSectionIds.Any())
                                               {
                                                   var resultDetails = await this.resultsService.GetResultDetails(
                                                                           enrollmentDetail.EnrollmentId,
                                                                           periodStartDate,
                                                                           endDate);
                                                   resultDetails = resultDetails.Where(x => x.ClassSectionId != null && x.IsCourseCompleted).OrderBy(x => x.DateCompleted).ToList();
                                                   if (resultDetails.Any())
                                                   {
                                                       var testIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();
                                                       var classSectionDetails = await this.classSectionService.GetDetailByIds(testIds);
                                                       classSectionDetails = classSectionDetails.ToList();

                                                       classSectionDetails = classSectionDetails
                                                           .Where(x => testIds.Contains(x.ClassSectionId)).ToList();
                                                       if (classSectionDetails.Any())
                                                       {
                                                           var reqIds = classSectionDetails.Select(x => x.ReqId).Distinct().ToList();
                                                           var termIds = classSectionDetails.Select(x => x.TermId).Distinct().ToList();
                                                           List<ListItem<DateTime, decimal>> faCreditList = await this.GetCreditEarnedList(enrollmentDetail, reqIds, termIds, resultDetails);

                                                           var totalFaCredit = 0m;
                                                           var isPaymentPeriodChanged = false;

                                                           // calculate the total credit earned.
                                                           foreach (var x in faCreditList)
                                                           {
                                                               currentPeriodDetailsWithCreditsEarned.CreditsEarnedOn = x.Text;
                                                               var total = totalFaCredit + Convert.ToDecimal(x.Value);
                                                               if (total < creditHourProgramPeriod.Credits)
                                                               {
                                                                   totalFaCredit += Convert.ToDecimal(x.Value);
                                                               }
                                                               else if (total >= creditHourProgramPeriod.Credits)
                                                               {
                                                                   currentPeriodDetailsWithCreditsEarned.WeeksCompletedOn = weekCompletionDate;
                                                                   var creditEarnedForNextPp = total - creditHourProgramPeriod.Credits;
                                                                   if (creditHourProgramPeriodDetails.Count > paymentPeriodNumber)
                                                                   {
                                                                       creditHourProgramPeriod = creditHourProgramPeriodDetails[paymentPeriodNumber];
                                                                       var periodEndDate = (weekCompletionDate > x.Text) ? weekCompletionDate : x.Text;
                                                                       var nextScheduleDate = totalWeeksScheduledDates.FirstOrDefault(dt => dt > periodEndDate);

                                                                       if (endDateTime.Date == weekCompletionDate.Date || nextScheduleDate == DateTime.MinValue || endDateTime.Date < nextScheduleDate.Date)
                                                                       {
                                                                           currentPeriodDetailsWithCreditsEarned.WithdrawalPeriod = paymentPeriodNumber;
                                                                           currentPeriodDetailsWithCreditsEarned.CreditsEarned = total;
                                                                           currentPeriodDetailsWithCreditsEarned.EndDateOfWithdrawalPeriod = periodEndDate;
                                                                           currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                                                                           return currentPeriodDetailsWithCreditsEarned;
                                                                       }
                                                                       else
                                                                       {
                                                                           paymentPeriodNumber++;
                                                                           currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = periodEndDate;
                                                                           currentPeriodDetailsWithCreditsEarned.CreditsEarned = creditEarnedForNextPp;
                                                                           currentPeriodDetailsWithCreditsEarned.WithdrawalPeriod = paymentPeriodNumber;
                                                                           currentPeriodDetailsWithCreditsEarned.WeeksCompleted = 0;
                                                                           isPaymentPeriodChanged = true;
                                                                           break;
                                                                       }
                                                                   }

                                                                   currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                                                                   return currentPeriodDetailsWithCreditsEarned;
                                                               }
                                                           }

                                                           // if it is the next payment period then set the start date to StartDateOfWithdrawalPeriod and continue counting.
                                                           if (isPaymentPeriodChanged)
                                                           {
                                                               weekCompletionDate = DateTime.MinValue;
                                                               startDate = this.GetNextPeriodStartDate(totalWeeksScheduledDates, currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue);
                                                               currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = startDate;
                                                               continue;
                                                           }
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }

                               // move to next week 
                               startDate = endDate.Date;
                           }

                           if (currentPeriodDetailsWithCreditsEarned.CreditsEarned == 0)
                           {
                               var earnedCreditDetail = await this.GetTotalCreditEarned(enrollmentDetail, currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue, endDateTime);
                               currentPeriodDetailsWithCreditsEarned.CreditsEarned = earnedCreditDetail.Value;
                               if (earnedCreditDetail.Text != DateTime.MinValue)
                               {
                                   currentPeriodDetailsWithCreditsEarned.CreditsEarnedOn = earnedCreditDetail.Text;
                               }
                           }

                           currentPeriodDetailsWithCreditsEarned.WithdrawalPeriod = paymentPeriodNumber;
                           currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                           return currentPeriodDetailsWithCreditsEarned;
                       });
        }

        /// <summary>
        /// The get credit hrs pay period for student attendance.
        /// </summary>
        /// <param name="isPaymentPeriod">
        /// is Payment Period
        /// </param>
        /// <param name="enrollmentDetail">
        /// The enrollment Detail.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <param name="creditHourProgramPeriodDetails">
        /// The credit hour program period details.
        /// </param>
        /// <param name="academicCalendarId">
        /// The Academic Calendar Id.
        /// </param>
        /// <returns>
        /// The <see cref="CurrentPeriodDetailsWithCreditsEarned"/>.
        /// </returns>
        private async Task<CurrentPeriodDetailsWithCreditsEarned> GetCreditHrsPayPeriodForStudentAttendance(
            bool isPaymentPeriod,
            Enrollment enrollmentDetail,
            DateTime endDateTime,
            IList<CreditHourProgramePeriods> creditHourProgramPeriodDetails,
            int academicCalendarId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var paymentPeriodNumber = 1;

                           var currentPeriodDetailsWithCreditsEarned = new CurrentPeriodDetailsWithCreditsEarned
                           {
                               CreditsEarned = 0m,
                               WithdrawalPeriod = paymentPeriodNumber
                           };

                           var creditHourProgramPeriod = creditHourProgramPeriodDetails[0];
                           if (creditHourProgramPeriod == null)
                           {
                               currentPeriodDetailsWithCreditsEarned.ResultStatus = ApiMsgs.CREDIT_HOUR_PROGRAM_LENGTH_ZERO;
                               return currentPeriodDetailsWithCreditsEarned;
                           }

                           var attendance = await this.studentClockAttendanceService.GetDetailsByEnrollmentId(enrollmentDetail.EnrollmentId);
                           attendance = attendance.Where(att => att.RecordDate >= enrollmentDetail.StartDate && att.RecordDate <= endDateTime).ToList();

                           var scheduleStartDate = await this.GetScheduleStartDateByEnrollmentId(enrollmentDetail.EnrollmentId, false);
                           if (academicCalendarId == (int)Constants.AcademicCalendars.NonStandardTerm)
                           {
                               var termDetails = this.termService.GetTerms(enrollmentDetail.EnrollmentId);
                               scheduleStartDate = termDetails.OrderBy(x => x.StartDate).FirstOrDefault()?.StartDate;
                           }

                           if (scheduleStartDate == null)
                           {
                               currentPeriodDetailsWithCreditsEarned.ResultStatus = ApiMsgs.SCHEDULE_START_DATE_NOT_FOUND;
                               return currentPeriodDetailsWithCreditsEarned;
                           }
                           else
                           {
                               currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = scheduleStartDate;
                           }

                           var results = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentDetail.EnrollmentId);
                           var clsSectIds = results.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();

                           var totalWeeksScheduled = await this.classSectionMeetingService.GetScheduledWeeksForDateRangeByClassSectionIds(
                                                                   enrollmentDetail,
                                                                   true,
                                                                   clsSectIds,
                                                                   (DateTime)scheduleStartDate,
                                                                   endDateTime);

                           var totalWeeksScheduledDates = totalWeeksScheduled.Item2.ToList();
                           totalWeeksScheduledDates = totalWeeksScheduledDates.Concat(attendance.Where(x => x.ActualHours < ConstantDecimals.AttendanceUpperLimit && x.ActualHours > 0).Select(x => x.RecordDate).ToList()).Distinct().OrderBy(x => x).ToList();

                           var weekCompletionDate = DateTime.MinValue;
                           for (var startDate = scheduleStartDate; startDate <= endDateTime;)
                           {
                               var date = startDate;
                               var periodStartDate = currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue;
                               var endDate = startDate.Value.AddDays(6);
                               var totalAttendance = attendance.Count(x => x.RecordDate >= date
                                                                           && x.RecordDate <= endDate
                                                                           && x.ActualHours < ConstantDecimals.AttendanceUpperLimit
                                                                           && x.ActualHours > 0);

                               if (totalAttendance > 0 || weekCompletionDate != DateTime.MinValue)
                               {
                                   if (totalAttendance > 0)
                                   {
                                       currentPeriodDetailsWithCreditsEarned.WeeksCompleted++;
                                       currentPeriodDetailsWithCreditsEarned.WeeksCompletedOn = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                                   }

                                   if (currentPeriodDetailsWithCreditsEarned.WeeksCompleted == creditHourProgramPeriod.Weeks)
                                   {
                                       weekCompletionDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
                                   }

                                   // if weeks defined for the payment period is completed 
                                   if (currentPeriodDetailsWithCreditsEarned.WeeksCompleted >= creditHourProgramPeriod.Weeks)
                                   {
                                       var resultDetails = await this.resultsService.GetResultDetails(
                                                               enrollmentDetail.EnrollmentId,
                                                               periodStartDate,
                                                               endDate);

                                       // filter only for completed courses
                                       resultDetails = resultDetails.Where(x => x.IsCourseCompleted).ToList();

                                       var classSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();

                                       var classSectionDetails = await this.classSectionService.GetDetailByIds(classSectionIds);
                                       classSectionDetails = classSectionDetails.ToList();
                                       if (classSectionDetails.Any())
                                       {
                                           var reqIds = classSectionDetails.Select(x => x.ReqId).Distinct().ToList();
                                           var termIds = classSectionDetails.Select(x => x.TermId).Distinct().ToList();
                                           var faCreditList = await this.GetCreditEarnedList(enrollmentDetail, reqIds, termIds, resultDetails);
                                           faCreditList = faCreditList.Where(x => x.Text <= endDateTime.Date).ToList();
                                           var totalFaCredit = 0m;
                                           var isPaymentPeriodChanged = false;

                                           // calculate the total credit earned.
                                           foreach (var x in faCreditList)
                                           {
                                               var total = totalFaCredit + Convert.ToDecimal(x.Value);
                                               currentPeriodDetailsWithCreditsEarned.CreditsEarnedOn = x.Text;
                                               if (total < creditHourProgramPeriod.Credits)
                                               {
                                                   totalFaCredit += Convert.ToDecimal(x.Value);
                                               }
                                               else if (total >= creditHourProgramPeriod.Credits)
                                               {
                                                   var creditEarnedForNextPp = total - creditHourProgramPeriod.Credits;
                                                   if (creditHourProgramPeriodDetails.Count > paymentPeriodNumber)
                                                   {
                                                       creditHourProgramPeriod = creditHourProgramPeriodDetails[paymentPeriodNumber];
                                                       var periodEndDate = (weekCompletionDate > x.Text) ? weekCompletionDate : x.Text;
                                                       var nextScheduleDate = totalWeeksScheduledDates.FirstOrDefault(dt => dt > periodEndDate);

                                                       if (endDateTime.Date == weekCompletionDate.Date || nextScheduleDate == DateTime.MinValue || endDateTime.Date < nextScheduleDate.Date)
                                                       {
                                                           currentPeriodDetailsWithCreditsEarned.WithdrawalPeriod = paymentPeriodNumber;
                                                           currentPeriodDetailsWithCreditsEarned.CreditsEarned = total;
                                                           currentPeriodDetailsWithCreditsEarned.EndDateOfWithdrawalPeriod = periodEndDate;
                                                           currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                                                           return currentPeriodDetailsWithCreditsEarned;
                                                       }
                                                       else
                                                       {
                                                           paymentPeriodNumber++;
                                                           currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = periodEndDate;
                                                           currentPeriodDetailsWithCreditsEarned.CreditsEarned = creditEarnedForNextPp;
                                                           currentPeriodDetailsWithCreditsEarned.WithdrawalPeriod = paymentPeriodNumber;
                                                           currentPeriodDetailsWithCreditsEarned.WeeksCompleted = 0;
                                                           isPaymentPeriodChanged = true;
                                                           break;
                                                       }
                                                   }

                                                   currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                                                   return currentPeriodDetailsWithCreditsEarned;
                                               }
                                           }

                                           // if it is the next payment period then set the start date to StartDateOfWithdrawalPeriod and continue counting.
                                           if (isPaymentPeriodChanged)
                                           {
                                               weekCompletionDate = DateTime.MinValue;
                                               startDate = this.GetNextPeriodStartDate(totalWeeksScheduledDates, currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue);
                                               currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod = startDate;
                                               continue;
                                           }
                                       }
                                   }
                               }

                               // move to next week
                               startDate = endDate.AddDays(1);
                           }

                           if (currentPeriodDetailsWithCreditsEarned.CreditsEarned == 0)
                           {
                               var earnedCreditsDetail = await this.GetTotalCreditEarned(enrollmentDetail, currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod ?? DateTime.MinValue, endDateTime);
                               currentPeriodDetailsWithCreditsEarned.CreditsEarned = earnedCreditsDetail.Value;
                               if (earnedCreditsDetail.Text != DateTime.MinValue)
                               {
                                   currentPeriodDetailsWithCreditsEarned.CreditsEarnedOn = earnedCreditsDetail.Text;
                               }
                           }

                           currentPeriodDetailsWithCreditsEarned.ResultStatus = isPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS : ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL;
                           return currentPeriodDetailsWithCreditsEarned;
                       });
        }

        /// <summary>
        /// The get next period start date based on available schedule.
        /// </summary>
        /// <param name="totalWeeksScheduled">
        /// The total weeks scheduled.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        private DateTime GetNextPeriodStartDate(IEnumerable<DateTime> totalWeeksScheduled, DateTime endDate)
        {
            var nextScheduleDate = totalWeeksScheduled.FirstOrDefault(dt => dt >= endDate);
            var isScheduledBreak = ((nextScheduleDate - endDate).Days + 1) >= 5;
            if (isScheduledBreak)
            {
                if (nextScheduleDate > endDate)
                {
                    return nextScheduleDate;
                }
            }

            return endDate.AddDays(1).Date;
        }

        /// <summary>
        /// The insert program registration type.
        /// </summary>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ActionResult<ProgramVersion>> InsertProgramRegistrationType(
            ArPrgVersions programVersion)
        {
            ActionResult<ProgramVersion> result = new ActionResult<ProgramVersion>();

            if (programVersion.ThGrdScaleId == null)
            {
                result.ResultStatus = Enums.ResultStatus.Warning;
                result.ResultStatusMessage = ApiMsgs.ProgramRegistrationInsertWarning + ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Grade Scale Id";
                return result;
            }

            var term = new Term
            {
                Code = programVersion.PrgVerCode,
                Description = programVersion.PrgVerDescrip,
                ProgramVersionId = programVersion.PrgVerId,
                CampusGroupId = programVersion.CampGrpId,
                ProgramId = programVersion.ProgId,
                TermTypeId = null
            };

            var termActionResult = await this.termService.Insert(term);
            if (termActionResult.ResultStatus == Enums.ResultStatus.Success)
            {
                programVersion.ProgramRegistrationType = (int)Enums.ProgramRegistrationType.ByProgram;
                this.ProgramVersionRepository.Update(programVersion);
                await this.classSectionService.InsertClassSectionsForProgramVersion(programVersion.PrgVerId);
            }
            else
            {
                result.ResultStatus = termActionResult.ResultStatus;
                result.ResultStatusMessage = termActionResult.ResultStatusMessage;
            }

            return result;
        }

        /// <summary>
        /// The update program registration type.
        /// </summary>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <param name="fromProgramToClass"></param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ActionResult<ProgramVersion>> UpdateProgramRegistrationType(
            ArPrgVersions programVersion, bool fromProgramToClass = false)
        {
            return await Task.Run(async () =>
            {
                ActionResult<ProgramVersion> result = new ActionResult<ProgramVersion>();

                if (fromProgramToClass)
                {
                    programVersion.ProgramRegistrationType = (int)Enums.ProgramRegistrationType.ByClass;
                    programVersion.ThGrdScaleId = null;
                    this.ProgramVersionRepository.Update(programVersion);
                    var deleteResult = await this.classSectionService.DeleteByProgramVersion(programVersion.PrgVerId);

                    if (deleteResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        await this.classSectionService.InsertClassSectionsForProgramVersion(programVersion.PrgVerId);
                    }
                    else
                    {
                        result.ResultStatus = deleteResult.ResultStatus;
                        result.ResultStatusMessage = deleteResult.ResultStatusMessage;
                    }
                }
                else
                {
                    /*
                     * Get Term by Program Version
                     * Update Term Details based on Program Version Details
                     */
                    var termActionResult = await this.termService.Get(programVersion.PrgVerId);
                    if (termActionResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        termActionResult.Result.Code = programVersion.PrgVerCode;
                        termActionResult.Result.Description = programVersion.PrgVerDescrip;
                        termActionResult = await this.termService.Update(termActionResult.Result);
                    }

                    if (termActionResult.ResultStatus != Enums.ResultStatus.Success)
                    {
                        result.ResultStatus = termActionResult.ResultStatus;
                        result.ResultStatusMessage = termActionResult.ResultStatusMessage;
                        return result;
                    }

                    /*
                     * Delete the existing class section and class section by term
                     * Insert the new classSections and class Sections by term
                     */
                    programVersion.ProgramRegistrationType = (int)Enums.ProgramRegistrationType.ByProgram;
                    this.ProgramVersionRepository.Update(programVersion);
                    var deleteResult = await this.classSectionService.DeleteByProgramVersion(programVersion.PrgVerId);

                    if (deleteResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        await this.classSectionService.InsertClassSectionsForProgramVersion(programVersion.PrgVerId);
                    }
                    else
                    {
                        result.ResultStatus = deleteResult.ResultStatus;
                        result.ResultStatusMessage = deleteResult.ResultStatusMessage;
                    }

                }


                return result;
            });
        }

        /// <summary>
        /// The delete program registration type.
        /// </summary>
        /// <param name="programVersion">
        /// The program version.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ActionResult<ProgramVersion>> DeleteProgramRegistrationType(ArPrgVersions programVersion)
        {
            /*
             * Delete the existing class section and class section by term
             * Delete Term
             */
            return await Task.Run(async () =>
            {
                ActionResult<bool> classSectionActionResult = await this.classSectionService.DeleteByProgramVersion(programVersion.PrgVerId);

                ActionResult<ProgramVersion> actionResult = new ActionResult<ProgramVersion>();

                var haveEnrollmentResults = await this.HaveEnrollments(programVersion.PrgVerId);

                if (haveEnrollmentResults)
                {
                    actionResult.ResultStatus = Enums.ResultStatus.Warning;
                    actionResult.ResultStatusMessage = ApiMsgs.FailToDeleteProgramLevelRegistrationDueToEnrollments;
                    return actionResult;
                }

                actionResult.ResultStatus = classSectionActionResult.ResultStatus;
                actionResult.ResultStatusMessage = classSectionActionResult.ResultStatusMessage;

                if (actionResult.ResultStatus == Enums.ResultStatus.Success)
                {
                    var termActionResult = await this.termService.Get(programVersion.PrgVerId);
                    if (termActionResult.ResultStatus == Enums.ResultStatus.Success)
                    {
                        var termDeleteActionResult = await this.termService.Delete(termActionResult.Result.Id);
                        actionResult.ResultStatus = termDeleteActionResult.ResultStatus;
                        actionResult.ResultStatusMessage = termDeleteActionResult.ResultStatusMessage;
                    }
                    else
                    {
                        actionResult.ResultStatus = termActionResult.ResultStatus;
                        actionResult.ResultStatusMessage = termActionResult.ResultStatusMessage;

                    }
                }

                return actionResult;
            });
        }

        /// <summary>
        /// The get withdrawal date returns the withdrawal date of the student.
        /// </summary>
        /// <param name="programVersion">
        /// The program version is the program version details of the specified program.
        /// </param>
        /// <returns>
        /// The withdrawal date of type DateTime.
        /// </returns>
        private DateTime GetWithdrawalDate(ArPrgVersions programVersion)
        {
            try
            {
                var studentTermination = programVersion.ArStuEnrollments.FirstOrDefault()?.ArR2t4terminationDetails
                    .OrderByDescending(x => x.UpdatedDate).FirstOrDefault();
                if (studentTermination != null)
                {
                    var withdrawalDateTime = studentTermination.LastDateAttended ?? DateTime.MinValue;
                    if (withdrawalDateTime != DateTime.MinValue)
                    {
                        withdrawalDateTime = new DateTime(
                            withdrawalDateTime.Year,
                            withdrawalDateTime.Month,
                            withdrawalDateTime.Day,
                            23,
                            59,
                            00);
                    }

                    return withdrawalDateTime;
                }

                return DateTime.MinValue;
            }
            catch (Exception)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// The get payment period details for when the program has terms defined for it.
        /// </summary>
        /// <param name="enrollmentId">
        /// The program version is the program version details of the specified program.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date is the withdrawal date of the student.
        /// </param>
        /// <param name="paymentPeriodDetails">
        /// The payment period details is the payment period details of the student and is of type CreditHourPaymentPeriodDetails.
        /// </param>
        /// <returns>
        /// The <see cref="CreditHourPaymentPeriodDetails"/>.
        /// </returns>
        private CreditHourPaymentPeriodDetails GetPaymentPeriodDetailsForTerms(
            Guid enrollmentId,
            DateTime withdrawalDate,
            CreditHourPaymentPeriodDetails paymentPeriodDetails)
        {
            {
                var terms = this.termService.GetTerms(enrollmentId);
                DateTime previousTermEndDate = DateTime.MinValue;
                DateTime previousTermStartDate = DateTime.MinValue;
                string previousTermDescription = string.Empty;
                foreach (var term in terms)
                {
                    if (term.StartDate.HasValue && term.EndDate.HasValue)
                    {
                        if (previousTermStartDate != DateTime.MinValue && previousTermEndDate != DateTime.MinValue && withdrawalDate.Date >= previousTermEndDate && withdrawalDate.Date <= term.StartDate.Value)
                        {
                            double totalDays = (previousTermEndDate - previousTermStartDate).TotalDays + 1;
                            double days = totalDays % 7;
                            paymentPeriodDetails.WeeksRequiredForTheWithdrawalPaymentPeriod = Convert.ToInt32((totalDays - days) / 7);
                            paymentPeriodDetails.TermStartDate = previousTermStartDate;
                            paymentPeriodDetails.TermEndDate = previousTermEndDate;
                            paymentPeriodDetails.TermName = previousTermDescription;
                            break;
                        }

                        if (term.EndDate.Value >= term.StartDate.Value && (withdrawalDate.Date >= term.StartDate.Value && withdrawalDate.Date <= term.EndDate.Value))
                        {
                            double totalDays = (term.EndDate.Value - term.StartDate.Value).TotalDays + 1;
                            double days = totalDays % 7;
                            paymentPeriodDetails.WeeksRequiredForTheWithdrawalPaymentPeriod = Convert.ToInt32((totalDays - days) / 7);
                            paymentPeriodDetails.TermStartDate = term.StartDate.Value;
                            paymentPeriodDetails.TermEndDate = term.EndDate.Value;
                            paymentPeriodDetails.TermName = term.Description;
                            break;
                        }

                        previousTermEndDate = term.EndDate.Value;
                        previousTermStartDate = term.StartDate.Value;
                        previousTermDescription = term.Description;
                    }
                }
            }

            return paymentPeriodDetails;
        }

        /// <summary>
        /// The get transfer credits returns the transfer credits obtained when the student is transferred.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is of type Guid.
        /// </param>
        /// <returns>
        /// The transfer credits obtained by the student.
        /// </returns>
        private async Task<decimal> GetTransferCredits(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           decimal transferCredits = ConstantDecimals.ZeroDecimal;
                           var transferGrades = await this.transferGradesService.GetTransferGrades(enrollmentId);
                           var transferGradesDetailList = transferGrades.ToList();
                           if (transferGradesDetailList.Any())
                           {
                               decimal totalTransferCredits = ConstantDecimals.ZeroDecimal;
                               foreach (var grades in transferGradesDetailList)
                               {
                                   if (grades.IsCourseCompleted)
                                   {
                                       var reqId = grades.ReqId;
                                       var transferCreditDetails = await this.courseDetailsService.GetTransferCreditsByReqId(reqId);
                                       var transferCredit = transferCreditDetails.Credits;
                                       totalTransferCredits += Convert.ToInt32(transferCredit);
                                   }
                               }

                               transferCredits = totalTransferCredits;
                           }

                           return transferCredits;
                       });
        }

        /// <summary>
        /// The get non standard direct loan payment period details returns the direct loan payment period details for non standard, 
        /// substantially not equal in length program.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <param name="programVersion">
        /// The program version is the program version details of type ArPrgVersions.
        /// </param>
        /// <param name="transferCredits">
        /// The transfer credits is the credits obtained when the student is transferred.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date is the withdrawal date of the student.
        /// </param>
        /// <returns>
        /// The credit hour payment period details of type CreditHourPaymentPeriodDetails.
        /// </returns>
        private async Task<CreditHourPaymentPeriodDetails> GetNonStandardDirectLoanPaymentPeriodDetails(
            Guid enrollmentId,
            ArPrgVersions programVersion,
            decimal transferCredits,
            DateTime withdrawalDate)
        {
            return await Task.Run(
                       async () =>
                       {
                           var paymentPeriodDetails = new CreditHourPaymentPeriodDetails();
                           try
                           {
                               var programDetails = this.GetCreditHourProgramLength(
                                   programVersion.Credits,
                                   transferCredits,
                                   programVersion.Weeks);
                               if (programDetails.ResultStatus == ApiMsgs.CREDIT_HOUR_PROGRAM_LENGTH)
                               {
                                   var creditHourPaymentPeriods =
                                       await this.GetCreditHourPaymentPeriodsByEnrollmentId(enrollmentId);
                                   if (creditHourPaymentPeriods.ResultStatus != ApiMsgs.FAILED_GETTING_RECORD)
                                   {
                                       var withdrawalPayment =
                                           await this.GetCreditHoursWithdrawalPaymentPeriod(enrollmentId);

                                       if (withdrawalPayment.WithdrawalPeriod > 0)
                                       {
                                           var creditHourProgramPeriodDetail =
                                               creditHourPaymentPeriods.CreditHourProgramePeriodDetailsList
                                                   .FirstOrDefault(
                                                       x => x.Period == withdrawalPayment.WithdrawalPeriod);

                                           if (creditHourProgramPeriodDetail != null)
                                           {
                                               paymentPeriodDetails = this.GetPaymentPeriodDetailsForTerms(
                                                                          enrollmentId,
                                                                          withdrawalDate,
                                                                          paymentPeriodDetails);

                                               if (paymentPeriodDetails.WeeksRequiredForTheWithdrawalPaymentPeriod > 0)
                                               {
                                                   if (paymentPeriodDetails.WeeksRequiredForTheWithdrawalPaymentPeriod
                                                       < creditHourProgramPeriodDetail.Weeks)
                                                   {
                                                       paymentPeriodDetails.TermOfWithdrawal =
                                                           ApiMsgs.DIRECT_LOAN_PAYMENT_PERIOD;
                                                       paymentPeriodDetails.PaymentPeriodTermType =
                                                           (int)Constants.PaymentPeriodTermType.DirectLoan;
                                                   }
                                                   else
                                                   {
                                                       paymentPeriodDetails.TermOfWithdrawal =
                                                           ApiMsgs.NON_STANDARD_PAYMENT_PERIOD;
                                                       paymentPeriodDetails.PaymentPeriodTermType =
                                                           (int)Constants.PaymentPeriodTermType.NonStandard;
                                                   }
                                               }

                                               paymentPeriodDetails.CreditsEarnedInWithdrawalPaymentPeriod = withdrawalPayment.CreditsEarned;
                                               paymentPeriodDetails.WeeksCompletedInWithdrawalPaymentPeriod = withdrawalPayment.WeeksCompleted;
                                               paymentPeriodDetails.WeeksRequiredForTheWithdrawalPaymentPeriod = creditHourProgramPeriodDetail.Weeks;
                                               paymentPeriodDetails.CreditsRequiredForWithdrawalPaymentPeriod = creditHourProgramPeriodDetail.Credits;
                                               paymentPeriodDetails.WithdrawalPaymentPeriod = creditHourProgramPeriodDetail.Period;

                                               if (withdrawalPayment.StartDateOfWithdrawalPeriod != null)
                                               {
                                                   paymentPeriodDetails.WithdrawalPaymentPeriodStartDate = withdrawalPayment.StartDateOfWithdrawalPeriod.Value;
                                               }

                                               paymentPeriodDetails.ResultStatus = ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL;
                                           }
                                       }
                                   }
                               }

                               return paymentPeriodDetails;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();

                               return new CreditHourPaymentPeriodDetails { ResultStatus = ApiMsgs.FAILED_GETTING_RECORD };
                           }
                       });
        }

        /// <summary>
        /// The get scheduled and total hours for payment period.
        /// </summary>
        /// <param name="isByDay">
        /// The is by day.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <param name="isPaymentPeriod">
        /// The is Payment Period.
        /// </param>
        /// <returns>
        /// returns the total and scheduled hours
        /// </returns>
        private async Task<ScheduledAndTotalHours> GetScheduledAndTotalHoursForPaymentPeriod(bool isByDay, Enrollment enrollment, DateTime withdrawalDate, bool isPaymentPeriod = false)
        {
            return await Task.Run(
                       async () =>
                       {
                           var scheduledAndTotalHours = new ScheduledAndTotalHours();
                           var paymentPeriodDetails = await this.GetPaymentPeriodsByEnrollmentId(enrollment.EnrollmentId);
                           if (paymentPeriodDetails.PeriodDetailsList.Count == 0)
                           {
                               scheduledAndTotalHours.ResultStatus = paymentPeriodDetails.ResultStatus;
                               return scheduledAndTotalHours;
                           }

                           var paymentPeriod = await this.GetWithdrawalPaymentPeriod(enrollment.EnrollmentId);

                           if (paymentPeriod.ResultStatus
                               != ApiMsgs.WITHDRAWAL_PAYMENT_PERIOD_CALCULATED_SUCCESSFUL
                               && paymentPeriod.ResultStatus
                               != ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL)
                           {
                               scheduledAndTotalHours.ResultStatus = paymentPeriod.ResultStatus;
                               return scheduledAndTotalHours;
                           }

                           if (paymentPeriod.StartDateOfWithdrawalPeriod == null || withdrawalDate == DateTime.MinValue)
                           {
                               scheduledAndTotalHours.ResultStatus = ApiMsgs.INVALID_START_WITHDRAWAL_DATE;
                               return scheduledAndTotalHours;
                           }

                           if (!paymentPeriodDetails.PeriodDetailsList.Any())
                           {
                               scheduledAndTotalHours.ResultStatus = paymentPeriodDetails.ResultStatus;
                               return scheduledAndTotalHours;
                           }

                           scheduledAndTotalHours.TotalHoursInPeriod = paymentPeriodDetails.PeriodDetailsList[paymentPeriod.WithdrawalPeriod - 1].PeriodLength;
                           scheduledAndTotalHours.HoursScheduledToComplete = await this.CalculateScheduledHoursToComplete(isByDay, enrollment, withdrawalDate, paymentPeriod.StartDateOfWithdrawalPeriod ?? DateTime.MinValue, paymentPeriod.HourDebitedToPreviousPeriod);
                           if (scheduledAndTotalHours.TotalHoursInPeriod < scheduledAndTotalHours.HoursScheduledToComplete)
                           {
                               scheduledAndTotalHours.HoursScheduledToComplete = scheduledAndTotalHours.TotalHoursInPeriod;
                           }

                           scheduledAndTotalHours.ResultStatus = isPaymentPeriod ? ApiMsgs.SCHEDULE_TOTAL_PP_HOURS_CALCULATED_SUCCESSFUL : ApiMsgs.SCHEDULE_TOTAL_PE_HOURS_CALCULATED_SUCCESSFUL;
                           return scheduledAndTotalHours;
                       });
        }

        /// <summary>
        /// The GetScheduleStartDateByEnrollmentId method gets schedule start date by given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="isByClass">
        /// The is By Class.
        /// </param>
        /// <returns>
        /// The DateTime.
        /// </returns>
        private async Task<DateTime?> GetScheduleStartDateByEnrollmentId(Guid enrollmentId, bool isByClass)
        {
            return await Task.Run(
                       async () =>
                           {
                               var results = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentId);
                               var classSectionIds = results.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();
                               if (isByClass)
                               {
                                   return await this.classSectionMeetingService.GetScheduleStartDateByClassSectionIds(classSectionIds);
                               }
                               else
                               {
                                   var classSections = await this.classSectionService.GetDetailByIds(classSectionIds);
                                   return classSections.Select(x => x.StartDate).OrderBy(x => x).FirstOrDefault();
                               }
                           });
        }

        /// <summary>
        /// The CalculateScheduledHoursToComplete method calculates scheduled hours to complete for the given enrollment and date range .
        /// </summary>
        /// <param name="isByDay">
        /// The is by day.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="debitedHours">
        /// The debited Hours.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<decimal> CalculateScheduledHoursToComplete(bool isByDay, Enrollment enrollment, DateTime withdrawalDate, DateTime startDate, decimal debitedHours)
        {
            return await Task.Run(
                async () =>
                {
                    var scheduledPerMeetings = new List<IListItem<DateTime, decimal>>();
                    if (isByDay)
                    {
                        var programLevelSchedule = (List<IListItem<DateTime, decimal>>)await this.GetProgramLevelScheduleForDateRange(enrollment, startDate, withdrawalDate);
                        scheduledPerMeetings = programLevelSchedule.ToList();
                    }
                    else
                    {
                        var resultDetails = await this.resultsService.GetResultDetailsByEnrollmentId(enrollment.EnrollmentId);
                        var clsSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();
                        if (clsSectionIds.Any())
                        {
                            var classLevelSchedule = await this.classSectionMeetingService
                                                             .GetClassLevelScheduleForDateRangeByClassSections(
                                                                 clsSectionIds,
                                                                 startDate,
                                                                 withdrawalDate);
                            scheduledPerMeetings = classLevelSchedule.ToList();
                        }
                    }

                    var hoursScheduledToComplete = 0m;
                    var studentLoAs = await this.enrollmentService.GetStudentLoaDetails(
                                          enrollment.EnrollmentId,
                                          startDate,
                                          withdrawalDate);

                    var holidays = await this.holidayService.GetHolidayListByCampus(enrollment.CampusId);
                    holidays = holidays.ToList();
                    var scheduleForStartDate = 0m;
                    scheduledPerMeetings.ForEach(x =>
                    {
                        var scheduleStart = x.Text;
                        var scheduleEnd = x.Text.AddHours(Convert.ToDouble(x.Value));

                        var isDateLoA = studentLoAs.Any(loa => loa.StartDate <= x.Text.Date && loa.EndDate >= x.Text.Date);
                        var holidayList = holidays.Where(
                                hDay => hDay.StartDate.Date <= scheduleStart.Date && hDay.EndDate.Date >= scheduleEnd.Date)
                            .ToList();

                        if (!isDateLoA)
                        {
                            if (holidayList.Any())
                            {
                                holidayList.ForEach(
                                    hd =>
                                    {
                                        if (!hd.AllDay)
                                        {
                                            var totalScheduledHours = 0m;

                                            // HS => Holiday Start date
                                            // SS => Schedule Start date
                                            // HE => Holiday end date
                                            // SE => Schedule end date
                                            if (!(hd.StartDate <= scheduleStart && hd.EndDate >= scheduleEnd)) // case 1 : HS <= SS and HE >= SE
                                            {
                                                if (hd.StartDate <= scheduleStart && hd.EndDate <= scheduleEnd) // case 2 :  HS <= SS and  HE <= SE
                                                {
                                                    totalScheduledHours = (decimal)(hd.EndDate - scheduleStart).TotalHours;
                                                }
                                                else if (hd.StartDate >= scheduleStart && hd.EndDate >= scheduleEnd) // case 3 : SS <= HS and SE <= HE
                                                {
                                                    totalScheduledHours = (decimal)(scheduleEnd - hd.StartDate).TotalHours;
                                                }
                                                else if (hd.StartDate >= scheduleStart && hd.EndDate <= scheduleEnd) // case 4 :  HS >= SS and  HE <= SE
                                                {
                                                    totalScheduledHours = (decimal)(hd.EndDate - hd.StartDate).TotalHours;
                                                }

                                                var actualSchedule = (totalScheduledHours > 0) ? x.Value - totalScheduledHours : x.Value;
                                                hoursScheduledToComplete += actualSchedule;
                                                if (scheduleStart.Date == startDate.Date)
                                                {
                                                    scheduleForStartDate += actualSchedule;
                                                }
                                            }
                                        }
                                    });
                            }
                            else
                            {
                                hoursScheduledToComplete += x.Value;
                                if (scheduleStart.Date == startDate.Date)
                                {
                                    scheduleForStartDate += x.Value;
                                }
                            }
                        }
                    });

                    hoursScheduledToComplete -= scheduleForStartDate > debitedHours ? debitedHours : scheduleForStartDate;
                    return hoursScheduledToComplete;
                });
        }

        /// <summary>
        /// The get program level schedule for date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<IEnumerable<IListItem<DateTime, decimal>>> GetProgramLevelScheduleForDateRange(Enrollment enrollment, DateTime startDate, DateTime withdrawalDate)
        {
            return await Task.Run(
                       async () =>
                           {
                               var schedules = new List<IListItem<DateTime, decimal>>();
                               var studentClockAttendances = await this.studentClockAttendanceService.GetDetailsByEnrollmentId(enrollment.EnrollmentId);
                               studentClockAttendances = studentClockAttendances.Where(x => x.RecordDate >= startDate && x.RecordDate <= withdrawalDate).ToList();
                               studentClockAttendances.ForEach(
                                   x =>
                                       {
                                           schedules.Add(new ListItem<DateTime, decimal> { Text = x.RecordDate, Value = Convert.ToDecimal(x.ScheduleHours) });
                                       });
                               return schedules;
                           });
        }

        /// <summary>
        /// The get credit hour period details by enrollment id action returns credit hour period details whose program length
        /// longer than an academic year with remaining period greater than half an academic year or equal to the defined academic year.
        /// </summary>
        /// <remarks>
        /// The get credit hour period details by enrollment id action returns credit hour period details for a Credit Hour program and in the parameter
        /// it requires EnrollmentId.
        /// Based on the EnrollmentId, it searches all the enrolled program details and program versions and returns credit hour period details.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns list of credit hour program details.
        /// </returns>
        private async Task<CreditHourProgramePeriodDetails> GetCreditHourPeriodDetailsByEnrollmentId(Guid enrollmentId)
        {
            var creditHourProgramPeriodDetails = new CreditHourProgramePeriodDetails();
            return await Task.Run(async () =>
            {
                try
                {
                    var creditHourProgramLength = await this.GetCreditHourProgramLengthByEnrollmentId(enrollmentId);
                    {
                        var enrollment = await this.enrollmentService.Get(enrollmentId);
                        if (creditHourProgramLength != null && enrollment != null)
                        {
                            var programVersions = this.ProgramVersionRepository
                                .Get(pv => pv.PrgVerId == enrollment.PrgVerId).FirstOrDefault();

                            var programLength = creditHourProgramLength.Credits;
                            var programWeek = creditHourProgramLength.Weeks;

                            if (programVersions != null && Convert.ToDecimal(programVersions.AcademicYearLength) != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                            {
                                if (Convert.ToDecimal(programLength) >= Convert.ToDecimal(programVersions.AcademicYearLength))
                                {
                                    if (programVersions.Weeks != Convert.ToDecimal(ConstantDecimals.ZeroDecimal) && programVersions.AcademicYearWeeks != Convert.ToDecimal(ConstantDecimals.ZeroDecimal))
                                    {
                                        var weekDifference = programWeek - programVersions.AcademicYearWeeks;
                                        var actualWeek = programVersions.AcademicYearWeeks / 2;
                                        var creditsDifference = programLength - programVersions.AcademicYearLength;
                                        var actualCredits = programVersions.AcademicYearLength / 2;
                                        if (weekDifference > actualWeek || creditsDifference > actualCredits)
                                        {
                                            var fullAcademicYearNoByWeek = Convert.ToDecimal(Convert.ToDecimal(programWeek / programVersions.AcademicYearWeeks));
                                            var fullAcademicYearNoByCredit = Convert.ToDecimal(Convert.ToDecimal(creditHourProgramLength.Credits / programVersions.AcademicYearLength));

                                            var fullAcademicYearNo = Math.Floor(fullAcademicYearNoByWeek >= fullAcademicYearNoByCredit ? Convert.ToDecimal(Convert.ToDecimal(creditHourProgramLength.Credits / programVersions.AcademicYearLength)) : Convert.ToDecimal(Convert.ToDecimal(programWeek / programVersions.AcademicYearWeeks)));

                                            var paymentPeriods = programVersions.PayPeriodPerAcYear;
                                            creditHourProgramPeriodDetails.NumberOfPaymentPeriods = Convert.ToInt32(paymentPeriods * fullAcademicYearNo) + 2;

                                            creditHourProgramPeriodDetails.NumberOfFullAcademicYears = Convert.ToInt32(fullAcademicYearNo);

                                            creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList =
                                                new List<CreditHourProgramePeriods>();

                                            for (int programCount = 1; programCount <= creditHourProgramPeriodDetails.NumberOfPaymentPeriods; programCount++)
                                            {
                                                if (programCount <= creditHourProgramPeriodDetails.NumberOfPaymentPeriods - 2)
                                                {
                                                    decimal weeksCounter;
                                                    var credits = Convert.ToDecimal(programVersions.AcademicYearLength / paymentPeriods);
                                                    var weeks = Convert.ToDecimal(Convert.ToDecimal(programVersions.AcademicYearWeeks) / paymentPeriods);

                                                    if (programCount % 2 == 0)
                                                    {
                                                        weeksCounter = Convert.ToInt16(weeks.ToString(CultureInfo.InvariantCulture).Split('.')[0]);
                                                    }
                                                    else
                                                    {
                                                        string[] weeksCount = weeks.ToString(CultureInfo.InvariantCulture).Split('.');

                                                        weeksCounter = weeksCount.Length > 1
                                                                           ? Math.Ceiling(Convert.ToDecimal(weeks))
                                                                           : Convert.ToDecimal(weeks);
                                                    }

                                                    var creditHourProgramPeriods =
                                                        new CreditHourProgramePeriods
                                                        {
                                                            Period = programCount,
                                                            Credits = credits,
                                                            Weeks = Convert.ToInt16(weeksCounter)
                                                        };

                                                    creditHourProgramPeriodDetails.LastPaymentPeriodCredits = credits;
                                                    creditHourProgramPeriodDetails.LastPaymentPeriodWeeks = Convert.ToInt16(weeksCounter);
                                                    creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                                    creditHourProgramPeriodDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS}";
                                                }
                                                else
                                                {
                                                    for (int programPeriodCount = programCount; programPeriodCount <= creditHourProgramPeriodDetails.NumberOfPaymentPeriods; programCount++)
                                                    {
                                                        var credits = Convert.ToDecimal((programLength - (programVersions.AcademicYearLength * fullAcademicYearNo)) / paymentPeriods);
                                                        var weeks = Convert.ToDecimal((Convert.ToDecimal(programWeek) - (programVersions.AcademicYearWeeks * fullAcademicYearNo)) / paymentPeriods);

                                                        if (credits > 0 && weeks > ConstantDecimals.ZeroDecimal)
                                                        {
                                                            decimal weeksCounter;

                                                            if (programPeriodCount % 2 == 0)
                                                            {
                                                                weeksCounter = Convert.ToInt16(weeks.ToString(CultureInfo.InvariantCulture).Split('.')[0]);
                                                                creditHourProgramPeriodDetails.LastPaymentPeriodWeeks = weeksCounter;
                                                            }
                                                            else
                                                            {
                                                                string[] weeksCount = weeks.ToString(CultureInfo.InvariantCulture).Split('.');

                                                                weeksCounter =
                                                                    weeksCount.Length > 1
                                                                        ? Math.Ceiling(Convert.ToDecimal(weeks))
                                                                        : Convert.ToDecimal(weeks);
                                                                creditHourProgramPeriodDetails.LastPaymentPeriodWeeks = weeksCounter;
                                                            }

                                                            var creditHourProgramPeriods =
                                                                new CreditHourProgramePeriods
                                                                {
                                                                    Period = programCount,
                                                                    Credits = credits,
                                                                    Weeks = Convert.ToInt16(weeksCounter)
                                                                };

                                                            programPeriodCount++;
                                                            creditHourProgramPeriodDetails.CreditHourProgramePeriodDetailsList.Add(creditHourProgramPeriods);
                                                            creditHourProgramPeriodDetails.LastPaymentPeriodCredits = credits;
                                                            creditHourProgramPeriodDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS}";
                                                        }
                                                        else
                                                        {
                                                            creditHourProgramPeriodDetails.NumberOfPaymentPeriods = Convert.ToInt32(paymentPeriods * fullAcademicYearNo);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        creditHourProgramPeriodDetails.ResultStatus = $"{ApiMsgs.ACADEMIC_PERIOD_ZERO_LENGTH}";
                                    }
                                }
                            }
                        }
                    }

                    return creditHourProgramPeriodDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();

                    return new CreditHourProgramePeriodDetails
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The get total credit earned for enrollment and date range.
        /// </summary>
        /// <param name="enrollmentDetail">
        /// The enrollment detail.
        /// </param>
        /// <param name="periodStartDate">
        /// The period start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ListItem<DateTime, decimal>> GetTotalCreditEarned(
            Enrollment enrollmentDetail,
            DateTime periodStartDate,
            DateTime endDate)
        {
            return await Task.Run(
                       async () =>
                       {
                           var resultDetails = await this.resultsService.GetResultDetails(
                                                   enrollmentDetail.EnrollmentId,
                                                   periodStartDate,
                                                   endDate);

                           // filter only for completed courses
                           resultDetails = resultDetails.Where(x => x.IsCourseCompleted).ToList();

                           var classSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();

                           var classSectionDetails = await this.classSectionService.GetDetailByIds(classSectionIds);
                           classSectionDetails = classSectionDetails.ToList();

                           resultDetails = resultDetails
                               .Where(
                                   x => x.ClassSectionId != null && x.IsCourseCompleted
                                                                 && classSectionIds.Contains(
                                                                     x.ClassSectionId ?? Guid.Empty))
                               .OrderBy(x => x.DateCompleted).ToList();

                           if (resultDetails.Any())
                           {
                               var testIds = resultDetails.Select(x => x.ClassSectionId);
                               classSectionDetails = classSectionDetails
                                   .Where(x => testIds.Contains(x.ClassSectionId)).ToList();
                               if (classSectionDetails.Any())
                               {
                                   var reqIds = classSectionDetails.Select(x => x.ReqId).Distinct().ToList();
                                   var termIds = classSectionDetails.Select(x => x.TermId).Distinct().ToList();
                                   List<ListItem<DateTime, decimal>> faCreditList = await this.GetCreditEarnedList(enrollmentDetail, reqIds, termIds, resultDetails);
                                   var dateOnCreditEarned = faCreditList.Where(x => x.Value > 0).OrderBy(x => x.Text).LastOrDefault()?.Text;
                                   var totalCreditsEarned = faCreditList.ToList().Sum(x => x.Value);
                                   return new ListItem<DateTime, decimal>
                                   {
                                       Value = totalCreditsEarned,
                                       Text = dateOnCreditEarned ?? DateTime.MinValue
                                   };
                               }
                           }

                           return new ListItem<DateTime, decimal> { Value = ConstantDecimals.ZeroDecimal };
                       });
        }

        public async Task<decimal> GetProgramVersionTotalHours(Guid prgVerId)
        {
            var programVersion = await this.ProgramVersionRepository.GetByIdAsync<Guid>(prgVerId);

            return programVersion?.Hours ?? 0;
        }

        public async Task<IEnumerable<NaccasProgramVersion>> GetProgramVersionsForNaccas(Guid campusId)
        {
            var campusGroups = this.CampusCampusGroupRepository.Get(x => x.CampusId == campusId).Select(x => x.CampGrpId).ToList();
            var activeStatusId = await this.statusService.GetActiveStatusId();

            var programVersions = this.ProgramVersionRepository.Get(x => campusGroups.Contains(x.CampGrpId ?? Guid.Empty) && x.StatusId.HasValue && x.StatusId.Value == activeStatusId).Select(x => new NaccasProgramVersion()
            {
                IsApproved = x.SyApprovedNaccasprogramVersion.Any(a => a.IsApproved),
                ProgramVersionId = x.PrgVerId,
                ProgramVersionDescription = x.PrgVerDescrip
            }).OrderBy(x => x.ProgramVersionDescription).ToList();


            return programVersions;
        }

        public async Task<ActionResult<ProgramVersionLms>> GetLmsProgramVersionDefinition(Guid programVersionId)
        {
            var result = new ActionResult<ProgramVersionLms>();
            try
            {
                var courseGroupIds = this.ProgramVersionDefinitionRepository.Get(d => d.PrgVerId == programVersionId)
                    .Select(d => d.ReqId).ToList();

                var groups = this.CourseGroupRepository.Get(g => courseGroupIds.Contains(g.GrpId)).Include(g => g.Req)
                    .ToList();

                var programVersionDefinitions = this.ProgramVersionDefinitionRepository
                    .Get(d => d.PrgVerId == programVersionId)
                    .Include(d => d.PrgVer)
                    .ThenInclude(pv => pv.Prog)
                    .Include(d => d.Req)
                    .GroupBy(d => d.PrgVerId)
                    .Select(d => new ProgramVersionLms()
                    {
                        ProgramId = d.FirstOrDefault().PrgVer.ProgId ?? Guid.Empty,
                        ProgramName = d.FirstOrDefault().PrgVer.Prog.ProgDescrip ?? string.Empty,
                        Id = d.Key,
                        Name = d.FirstOrDefault().PrgVer.PrgVerDescrip,
                        Courses = d.Where(c => c.Req.ReqTypeId == 1).OrderBy(c => c.ReqSeq).Select(c => new ProgramVersionDefinitionCourseLms()
                        {
                            Name = c.Req.Descrip,
                            IsRequired = c.IsRequired,
                            ProgramVersionDefinitionId = c.ProgVerDefId
                        }).ToList(),
                        CourseGroups = d.Where(c => c.Req.ReqTypeId == 2).Select(cg => new CourseGroupLms()
                        {
                            IsRequired = cg.IsRequired,
                            Name = cg.Req.Descrip,
                            ProgramVersionDefinitionId = cg.ProgVerDefId,
                            Courses = groups.Where(g => g.GrpId == cg.ReqId).Select(g => new CourseLms()
                            {
                                Name = g.Req.Descrip,
                                IsRequired = g.IsRequired
                            }).ToList()
                        }).ToList()

                    }).ToList();

                var definition = programVersionDefinitions.FirstOrDefault();



                result.ResultStatus = Enums.ResultStatus.Success;
                result.Result = definition;
                result.ResultStatusMessage = string.Empty;
                result.AsString = "Program Version Definition";
                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = e.Message;
                return result;
            }
        }

        #endregion private methods
    }
}