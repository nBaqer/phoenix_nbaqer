﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4InputService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The R2T4 input service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 input service.
    /// </summary>
    public class R2T4InputService : IR2T4InputService
    {
        /// <summary>
        /// The mapper is used to map the R2T4Input Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4InputService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public R2T4InputService(IMapper mapper, IAdvantageDbContext context)
        {
            this.mapper = mapper;
            this.context = context;
        }

        /// <summary>
        /// The system users repository.
        /// </summary>
        private IRepository<SyUsers> SyUsersRepository => this.context.GetRepositoryForEntity<SyUsers>();

        /// <summary>
        /// The R2T4 input repository.
        /// </summary>
        private IRepository<ArR2t4input> R2T4InputRepository => this.context.GetRepositoryForEntity<ArR2t4input>();

        /// <summary>
        /// The R2T4 override input repository.
        /// </summary>
        private IRepository<ArR2t4overrideInput> R2T4OverrideInputRepository => this.context.GetRepositoryForEntity<ArR2t4overrideInput>();

        /// <summary>
        /// The R2T4 override results repository.
        /// </summary>
        private IRepository<ArR2t4overrideResults> R2T4OverrideResultsRepository => this.context.GetRepositoryForEntity<ArR2t4overrideResults>();

        /// <summary>
        /// The r 2 t 4 termination details repository.
        /// </summary>
        private IRepository<ArR2t4terminationDetails> R2T4TerminationDetailsRepository => this.context.GetRepositoryForEntity<ArR2t4terminationDetails>();

        /// <summary>
        /// The r 2 t 4 termination details repository.
        /// </summary>
        private IRepository<ArCampusPrgVersions> CampusProgramVersionRepository => this.context.GetRepositoryForEntity<ArCampusPrgVersions>();

        /// <summary>
        /// The R2T4 results repository.
        /// </summary>
        private IRepository<ArR2t4results> R2T4ResultsRepository => this.context.GetRepositoryForEntity<ArR2t4results>();




        /// <inheritdoc />
        /// <summary>
        /// The get action allows to fetch the R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The get action requires R2T4InputId and TerminationId and based on the Ids provided, it returns the matching R2T4 Input record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Input where R2T4 Input contains all the information related to R2T4 Input.
        /// </returns>
        public async Task<R2T4Input> Get(Guid terminationId)
        {
            return await Task.Run(() =>
                    {
                        var r2T4InputDetail = this.R2T4InputRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                        var result = this.mapper.Map<R2T4Input>(r2T4InputDetail);
                        if (result != null)
                        {
                            var userId = result.UpdatedBy ?? result.CreatedBy;
                            result.IsTuitionChargedByPaymentPeriod = !result.IsTuitionChargedByPaymentPeriod;

                            var userDetail = this.SyUsersRepository.Get(x => x.UserId == userId).FirstOrDefault();
                            if (userDetail != null)
                            {
                                result.UpdatedByFullName = userDetail.FullName;
                            }

                            var enrollment = this.R2T4TerminationDetailsRepository
                                .Get(_ => _.TerminationId == result.TerminationId).Select(_ => _.StuEnrollment)
                                .FirstOrDefault();

                            if (enrollment != null)
                            {
                                result.IsSelfPaceProgram = this.CampusProgramVersionRepository
                                    .Get(_ => _.PrgVerId == enrollment.PrgVerId && enrollment.CampusId == _.CampusId).Select(_ => _.IsSelfPaced)
                                    .FirstOrDefault();
                            }
                        }

                        return result;
                    });
        }

        /// <inheritdoc />
        /// <summary>
        /// The create service allows to save the R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The create service requires R2T4Input object. This object contains the properties of all the R2T4 Input data. 
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks> 
        /// <param name="model">
        /// The create service method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Input> Create(R2T4Input model)
        {
            var returnR2T4 = new R2T4Input();

            try
            {
                model.CreatedDate = model.UpdatedDate = DateTime.Now;
                var tempDetail = await Task.Run(() => this.R2T4InputRepository.Create(this.mapper.Map<ArR2t4input>(model)));
                this.R2T4InputRepository.Save();
                if (tempDetail != null)
                {
                    returnR2T4.R2T4InputId = tempDetail.R2t4inputId;
                    returnR2T4.TerminationId = tempDetail.TerminationId;
                    returnR2T4.IsTuitionChargedByPaymentPeriod = !tempDetail.IsTuitionChargedByPaymentPeriod;
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <inheritdoc />
        /// <summary>
        /// The update service allows to update the existing R2T4 Input detail.
        /// </summary>
        /// <remarks>
        /// The update service requires R2T4Input object. This object contains the properties of all the R2T4 Input data.
        /// Mandatory parameters required : TerminationId, ProgramUnitTypeId, and CreatedBy.
        /// </remarks> 
        /// <param name="model">
        /// The Post action method accepts a parameter of type R2T4 Input.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Input with R2T4InputId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Input> Update(R2T4Input model)
        {
            var returnR2T4 = new R2T4Input();

            try
            {
                var existingR2T4Input = this.R2T4InputRepository.GetById(model.R2T4InputId);

                existingR2T4Input.TerminationId = model.TerminationId;
                existingR2T4Input.ProgramUnitTypeId = model.ProgramUnitTypeId;
                existingR2T4Input.PellGrantDisbursed = model.PellGrantDisbursed;
                existingR2T4Input.PellGrantCouldDisbursed = model.PellGrantCouldDisbursed;
                existingR2T4Input.Fseogdisbursed = model.FseogDisbursed;
                existingR2T4Input.FseogcouldDisbursed = model.FseogCouldDisbursed;
                existingR2T4Input.TeachGrantDisbursed = model.TeachGrantDisbursed;
                existingR2T4Input.TeachGrantCouldDisbursed = model.TeachGrantCouldDisbursed;
                existingR2T4Input.IraqAfgGrantDisbursed = model.IraqAfgGrantDisbursed;
                existingR2T4Input.IraqAfgGrantCouldDisbursed = model.IraqAfgGrantCouldDisbursed;
                existingR2T4Input.UnsubLoanNetAmountDisbursed = model.UnsubLoanNetAmountDisbursed;
                existingR2T4Input.UnsubLoanNetAmountCouldDisbursed = model.UnsubLoanNetAmountCouldDisbursed;
                existingR2T4Input.SubLoanNetAmountDisbursed = model.SubLoanNetAmountDisbursed;
                existingR2T4Input.SubLoanNetAmountCouldDisbursed = model.SubLoanNetAmountCouldDisbursed;
                existingR2T4Input.PerkinsLoanDisbursed = model.PerkinsLoanDisbursed;
                existingR2T4Input.PerkinsLoanCouldDisbursed = model.PerkinsLoanCouldDisbursed;
                existingR2T4Input.DirectGraduatePlusLoanDisbursed = model.DirectGraduatePlusLoanDisbursed;
                existingR2T4Input.DirectGraduatePlusLoanCouldDisbursed = model.DirectGraduatePlusLoanCouldDisbursed;
                existingR2T4Input.DirectParentPlusLoanDisbursed = model.DirectParentPlusLoanDisbursed;
                existingR2T4Input.DirectParentPlusLoanCouldDisbursed = model.DirectParentPlusLoanCouldDisbursed;
                existingR2T4Input.IsAttendanceNotRequired = model.IsAttendanceNotRequired;
                existingR2T4Input.StartDate = model.StartDate;
                existingR2T4Input.ScheduledEndDate = model.ScheduledEndDate;
                existingR2T4Input.WithdrawalDate = model.WithdrawalDate;
                existingR2T4Input.CompletedTime = model.CompletedTime;
                existingR2T4Input.TotalTime = model.TotalTime;
                existingR2T4Input.TuitionFee = model.TuitionFee;
                existingR2T4Input.RoomFee = model.RoomFee;
                existingR2T4Input.BoardFee = model.BoardFee;
                existingR2T4Input.OtherFee = model.OtherFee;
                existingR2T4Input.IsTuitionChargedByPaymentPeriod = model.IsTuitionChargedByPaymentPeriod;
                existingR2T4Input.CreditBalanceRefunded = model.CreditBalanceRefunded;
                existingR2T4Input.UpdatedById = model.UpdatedBy;
                existingR2T4Input.UpdatedDate = DateTime.Now;
                existingR2T4Input.IsR2t4inputCompleted = model.IsR2T4InputCompleted;

                var result = await Task.Run(() => this.R2T4InputRepository.Update(existingR2T4Input));
                this.R2T4InputRepository.Save();
                if (result != null)
                {
                    returnR2T4.R2T4InputId = result.R2t4inputId;
                    returnR2T4.TerminationId = result.TerminationId;
                    returnR2T4.IsTuitionChargedByPaymentPeriod = !result.IsTuitionChargedByPaymentPeriod;
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                }
            }
            catch
            {
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <summary>
        /// The delete action is to delete all the R2T4 details like R2T4Input, R2T4OverrideInput, R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related R2T4 details by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        public async Task<string> Delete(Guid terminationId)
        {
            return await Task.Run(
                       () =>
                       {
                           try
                           {
                               // getting R2T4 Input records based on termination id.
                               var input = this.R2T4InputRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 Input data exists.
                               if (input != null)
                               {
                                   this.R2T4InputRepository.Delete(input);
                               }

                               // getting R2T4 override Input records based on termination id.
                               var overrideInput = this.R2T4OverrideInputRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 override Input data exists.
                               if (overrideInput != null)
                               {
                                   this.R2T4OverrideInputRepository.Delete(overrideInput);
                               }

                               // getting R2T4 override Results records based on termination id.
                               var overrideResults = this.R2T4OverrideResultsRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 override Results data exists.
                               if (overrideResults != null)
                               {
                                   this.R2T4OverrideResultsRepository.Delete(overrideResults);
                               }

                               // getting R2T4 Results records based on termination id.
                               var results = this.R2T4ResultsRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 Results data exists.
                               if (results != null)
                               {
                                   this.R2T4ResultsRepository.Delete(results);
                               }

                               this.context.SaveChanges();
                               return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                           }
                           catch (Exception)
                           {
                               return $"{ApiMsgs.DELETE_FAILED} {terminationId}";
                           }
                       });
        }

        /// <summary>
        /// The IsR2T4InputExists action allows to check if the R2T4 Input data is existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsR2T4InputExists action requires terminationId and based on the terminationId provided, it returns boolean value if the R2T4 Input record exists or not.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        public async Task<bool> IsR2T4InputExists(Guid terminationId)
        {
            return await Task.Run(() =>
                {
                    bool isExists = true;
                    var r2T4InputDetail = this.R2T4InputRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                    var r2T4OverrideInputDetail = this.R2T4OverrideInputRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                    if (r2T4InputDetail == null && r2T4OverrideInputDetail == null)
                    {
                        isExists = false;
                    }

                    return isExists;
                });
        }

        /// <inheritdoc />
        /// <summary>
        /// The get R2T4 input service validations.
        /// </summary>
        /// <returns>
        /// The DynamicValidationRuleSet.
        /// </returns>
        public DynamicValidationRuleSet GetR2T4InputServiceValidations()
        {
            var set = new DynamicValidationRuleSet("R2T4Input");
            set.Rules.Add("TerminationId", new DynamicValidationRule("TerminationId", true, true));
            set.Rules.Add("ProgramUnitTypeId", new DynamicValidationRule("ProgramUnitTypeId", true, true));
            set.Rules.Add("IsTuitionChargedByPaymentPeriod", new DynamicValidationRule("IsTuitionChargedByPaymentPeriod", true, true));
            set.Rules.Add("IsAttendanceNotRequired", new DynamicValidationRule("IsAttendanceNotRequired", true, true));
            set.Rules.Add("CreatedBy", new DynamicValidationRule("CreatedBy", true, true));
            return set;
        }

        /// <summary>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// </summary>
        /// <remarks>
        /// The GetR2T4CompletedStatus action is to get the R2T4 Tabs completion status from database by terminationId.
        /// It requires terminationId based on which the completion status of each tabs is determined.
        /// </remarks>
        /// <param name="terminationId">
        /// terminationId is a required field and it is of type guid .
        /// </param>
        /// <returns>
        /// Returns the boolean values of the tabs to determine whether the tab is completed or not based on terminationId.
        /// </returns>
        public async Task<R2T4CompletionStatus> GetR2T4CompletedStatus(Guid terminationId)
        {
            return await Task.Run(
                       () =>
                           {
                               var r2T4CompletionStatus = new R2T4CompletionStatus
                               {
                                   IsR2T4InputCompleted =
                                                             this.R2T4InputRepository
                                                                 .Get(x => x.TerminationId == terminationId)
                                                                 .Any(y => y.IsR2t4inputCompleted),
                                   IsR2T4ResultsCompleted =
                                                             this.R2T4ResultsRepository
                                                                 .Get(x => x.TerminationId == terminationId)
                                                                 .Any(y => y.IsR2t4resultsCompleted),
                                   IsR2T4OverrideResultsCompleted =
                                                             this.R2T4OverrideResultsRepository
                                                                 .Get(x => x.TerminationId == terminationId)
                                                                 .Any(y => y.IsR2t4overrideResultsCompleted)
                               };
                               return r2T4CompletionStatus;
                           });
        }
    }
}
