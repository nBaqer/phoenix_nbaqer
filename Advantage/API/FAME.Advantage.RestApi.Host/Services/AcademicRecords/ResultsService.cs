﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResultsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The results service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Results;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    /// <summary>
    /// The results service.
    /// </summary>
    public class ResultsService : IResultsService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ResultsService"/> class.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public ResultsService(IMapper mapper, IAdvantageDbContext context)
        {
            this.mapper = mapper;
            this.context = context;
        }

        /// <summary>
        /// The programVersion  repository.
        /// </summary>
        private IRepository<ArResults> ResultsRepository => this.context.GetRepositoryForEntity<ArResults>();

        /// <summary>
        /// The get result details returns the results list based on the given parameters.
        /// </summary>
        /// <remarks>
        /// The GetResultDetails action method takes the following parameters enrollmentId, testId, startDate, endDate and
        /// returns all the results details which matches this criteria.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Results>> GetResultDetails(Guid enrollmentId, DateTime startDate, DateTime endDate)
        {
            return await Task.Run(
                       () =>
                           {
                               var resultDetails = this.ResultsRepository.Get(
                                        x => x.StuEnrollId == enrollmentId   
                                        && x.DateCompleted != null
                                        && x.DateCompleted >= startDate 
                                        && x.DateCompleted <= endDate);

                               return this.mapper.Map<IEnumerable<Results>>(resultDetails);
                           });
        }

        /// <summary>
        /// The get result details by given enrollment Id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<Results>> GetResultDetailsByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var resultDetails = this.ResultsRepository.Get(
                                   x => x.StuEnrollId == enrollmentId);

                               return this.mapper.Map<IEnumerable<Results>>(resultDetails);
                           });
        }
    }
}
