﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApproveTerminationService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ApproveTerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Reports;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore;
    using Constants = FAME.Orm.Advantage.Domain.Common.Constants;

    /// <inheritdoc />
    /// <summary>
    /// The approve termination service.
    /// </summary>
    public class ApproveTerminationService : IApproveTerminationService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the ApproveTermination Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The status codes service.
        /// </summary>
        private readonly IStatusCodesService statusCodesService;
        /// <summary>
        /// The Student ClockAttendance Service
        /// </summary>
        private readonly IStudentClockAttendanceService studentClkAttendanceService;
        /// <summary>
        /// The ClassSectionAttendance Service
        /// </summary>
        private readonly IClassSectionAttendanceService classSectionAttendanceService;
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IReportService reportService;

        /// <summary>
        /// The lead service.
        /// </summary>
        private readonly ILeadService leadService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService sysConfigAppSettingService;

        /// <summary>
        /// The calculation period type service.
        /// </summary>
        private readonly ICalculationPeriodTypeService calculationPeriodTypeService;

        /// <summary>
        /// The Document Manager service.
        /// </summary>
        private readonly IDocumentManagerService documentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApproveTerminationService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// mapper param
        /// </param>
        /// <param name="userService">
        /// The user Service.
        /// </param>
        /// <param name="reportService">
        /// The report Service.
        /// </param>
        /// <param name="documentService">
        /// The document Service.
        /// </param>
        /// <param name="calculationPeriodTypeService">
        /// The calculation Period Type Service.
        /// </param>
        /// <param name="leadService">
        /// The lead Service.
        /// </param>
        /// <param name="sysConfigAppSettingService">
        /// The sys Config App Setting Service.
        /// </param>
        /// <param name="statusCodesService">
        /// The status Codes Service.
        /// </param>
        /// <param name="studentClkAttendanceService">
        /// The studentClkAttendance Service.
        /// </param>
        /// <param name="classSectionAttendanceService">
        /// The classSectionAttendance Service.
        /// </param>
        public ApproveTerminationService(IAdvantageDbContext context, IMapper mapper, IUserService userService, IReportService reportService, 
            IDocumentManagerService documentService, ICalculationPeriodTypeService calculationPeriodTypeService, ILeadService leadService, 
            ISystemConfigurationAppSettingService sysConfigAppSettingService, IStatusCodesService statusCodesService, IStudentClockAttendanceService studentClkAttendanceService
            , IClassSectionAttendanceService classSectionAttendanceService)
        {
            this.context = context;
            this.mapper = mapper;
            this.userService = userService;
            this.reportService = reportService;
            this.documentService = documentService;
            this.calculationPeriodTypeService = calculationPeriodTypeService;
            this.leadService = leadService;
            this.sysConfigAppSettingService = sysConfigAppSettingService;
            this.statusCodesService = statusCodesService;
            this.studentClkAttendanceService = studentClkAttendanceService;
            this.classSectionAttendanceService = classSectionAttendanceService;
        }

        /// <summary>
        /// The R2T4 termination details repository.
        /// </summary>
        private IRepository<ArR2t4terminationDetails> R2T4TerminationDetailsRepository =>
            this.context.GetRepositoryForEntity<ArR2t4terminationDetails>();

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The sy student status changes repository.
        /// </summary>
        private IRepository<SyStudentStatusChanges> SyStudentStatusChangesRepository => this.context.GetRepositoryForEntity<SyStudentStatusChanges>();

        /// <inheritdoc />
        /// <summary>
        /// The GetDetails service returns student termination details for the given termination id.
        /// The details also contain the R2T4 calculation along with overridden values
        /// </summary>
        /// <remarks>
        /// The GetDetails action method requires termination Id object of type Guid.
        /// </remarks> 
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="ApproveTermination" /> where ApproveTermination has following information :
        /// EnrollmentName,Status,Dropreason,LastDateAttended,WithdrawalDate,DateOfDetermination,
        /// TotalCharges, TotalTitleIvAid, TotalTitleIvAidDisbursed, PercentageOfTitleIvAidEarned, PostWithdrawalDisbursement, TotalTitleIvAidEarned, AmountToBeReturnedBySchool,
        /// AmountToBeReturnedByStudent, R2T4InputUserName, OverriddenUserName, TicketNumber, TitleIvGrantLessThan50Dollar, R2T4ResultFieldsWithOverriddenValues
        /// </returns>
        public async Task<ApproveTermination> GetDetails(Guid terminationId)
        {
            return await Task.Run(
                       () =>
                       {
                           var r2T4TerminationDetails = this.R2T4TerminationDetailsRepository
                             .Get(x => x.TerminationId == terminationId)
                             .Include(x => x.StuEnrollment)
                                 .ThenInclude(x => x.Lead)
                                 .ThenInclude(x => x.Campus)
                                 .ThenInclude(x => x.Country)
                             .Include(x => x.StuEnrollment)
                                 .ThenInclude(x => x.PrgVer).ThenInclude(pv => pv.UnitType)
                             .Include(x => x.DropReason)
                             .Include(x => x.StatusCode)
                             .Include(x => x.ArR2t4input)
                                 .ThenInclude(x => x.UpdatedBy)
                             .Include(x => x.ArR2t4results)
                             .Include(x => x.ArR2t4overrideResults)
                                 .ThenInclude(x => x.UpdatedBy).FirstOrDefault();

                           if (r2T4TerminationDetails?.StuEnrollment != null && r2T4TerminationDetails.StuEnrollment.Lead == null)
                           {
                               r2T4TerminationDetails.StuEnrollment.Lead = this.leadService.GetByStudentId(r2T4TerminationDetails.StuEnrollment.StudentId)?.Result;
                           }

                           var approveTerminationDetail =
                               this.mapper.Map<ApproveTermination>(r2T4TerminationDetails);
                           if (approveTerminationDetail == null)
                           {
                               return null;
                           }

                           var approveTermination = this.mapper.Map<ApproveTermination>(r2T4TerminationDetails);

                           var calculationPeriodType = this.calculationPeriodTypeService.GetPeriodTypes()?.Result?.Where(periodType => periodType.Text == CalculationPeriodTypes.PERIOD_OF_ENROLLMENT).FirstOrDefault();
                           if (r2T4TerminationDetails?.CalculationPeriodTypeId == calculationPeriodType?.Value)
                           {
                               approveTermination.AdditionalInformationDetail.IsTuitionByPaymentPeriod = true;
                           }

                           var studentIdentifier = this.sysConfigAppSettingService.GetAppSettingValue(StudentConfigurationKeys.StudentIdentifier);

                           if (studentIdentifier != null && approveTermination != null)
                           {
                               approveTermination.StudentIdentifier = studentIdentifier;
                               switch (studentIdentifier)
                               {
                                   case StudentIdentifierKeys.StudentId:
                                       approveTermination.StudentIdentifierId =
                                           r2T4TerminationDetails?.StuEnrollment?.Lead?.StudentNumber;
                                       break;
                                   case StudentIdentifierKeys.EnrollmentId:
                                       approveTermination.StudentIdentifierId =
                                           r2T4TerminationDetails?.StuEnrollment?.EnrollmentId;
                                       break;
                                   default:
                                       approveTermination.StudentIdentifierId = approveTermination.StudentSsn;
                                       break;
                               }
                           }

                           if (approveTermination?.AdditionalInformationDetail != null)
                           {
                               if (r2T4TerminationDetails?.CalculationPeriodTypeId != null)
                                   approveTermination.AdditionalInformationDetail.CalculationPeriodType = this.calculationPeriodTypeService.GetPeriodTypes()?.Result?.Where(periodType => periodType.Value == r2T4TerminationDetails.CalculationPeriodTypeId).FirstOrDefault()?.Text;

                               if ((approveTermination.AdditionalInformationDetail.IsTuitionByPaymentPeriod) && approveTermination?.AdditionalInformationDetail?.CalculationPeriodType ==
                                   CalculationPeriodTypes.PAYMENT_PERIOD &&
                                   (string.IsNullOrEmpty(approveTermination?.R2T4CalculationSummaryDetail
                                        ?.PostWithdrawalDisbursement) || approveTermination
                                        ?.R2T4CalculationSummaryDetail?
                                        .PostWithdrawalDisbursement == "$0.00"))
                               {
                                   approveTermination.AdditionalInformationDetail.IsAdditionalInfoStep3Required = true;
                               }

                               if (approveTermination?.AdditionalInformationDetail?.TitleIvGrantLessThan50Dollar
                                       ?.Count > 0
                                   || approveTermination?.AdditionalInformationDetail
                                       ?.R2T4ResultFieldsWithOverriddenValues?.Count > 0
                                   || approveTermination?.AdditionalInformationDetail
                                           ?.IsNotRequiredToTakeAattendance == true
                                  || (approveTermination.AdditionalInformationDetail.IsAdditionalInfoStep3Required)
                                  || approveTermination?.R2T4CalculationSummaryDetail?.PercentageOfTitleIvAidEarned == "100.0%")
                               {
                                   approveTermination.IsAdditionalInfoRequired = true;
                               }
                           }
                           else if (approveTermination != null)
                           {
                               approveTermination.IsAdditionalInfoRequired = false;
                           }
                           return approveTermination;
                       });
        }

        /// <summary>
        /// The Update action allows to update the Enrollment status details when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Update action requires Enrollment object which has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </remarks>
        /// <param name="studentEnrollmentDetails">
        /// The studentEnrollmentDetails object of type Enrollment.
        /// </param>
        /// <returns>
        /// Returns an object of Enrollment where Enrollment has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </returns>
        public async Task<Enrollment> Update(Enrollment studentEnrollmentDetails)
        {
            return await Task.Run(
                       async () =>
                       {
                           try
                           {
                               var updateEnrollmentDetails = this.EnrollmentRepository
                                   .Get(se => se.StuEnrollId == studentEnrollmentDetails.EnrollmentId)
                                   .FirstOrDefault();

                               if (updateEnrollmentDetails == null)
                               {
                                   return new Enrollment
                                   {
                                       ResultStatus =
                                                      $"{ApiMsgs.UPDATE_IDS_NO_MATCH} {studentEnrollmentDetails.EnrollmentId}"
                                   };
                               }

                               updateEnrollmentDetails.StatusCodeId = studentEnrollmentDetails.StatusCodeId;
                               updateEnrollmentDetails.DropReasonId = studentEnrollmentDetails.DropReasonId;
                               updateEnrollmentDetails.Lda = studentEnrollmentDetails.LastDateAttended;
                               updateEnrollmentDetails.DateDetermined = studentEnrollmentDetails.DateDetermined;
                               updateEnrollmentDetails.ModDate = DateTime.Now;
                               var userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);
                               updateEnrollmentDetails.ModUser = userDetails.FullName;
                               var studentEnrollmentsDetails =
                                   this.EnrollmentRepository.Update(updateEnrollmentDetails);
                               await this.EnrollmentRepository.SaveAsync();
                               var results = this.mapper.Map<Enrollment>(studentEnrollmentsDetails);
                               results.ResultStatus = ApiMsgs.APPROVE_STUDENT_TERMINATION_SUCCESS;
                               return results;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();

                               return new Enrollment
                               {
                                   ResultStatus =
                                                  $"{ApiMsgs.APPROVE_STUDENT_TERMINATION_UNSUCCESS} {studentEnrollmentDetails.EnrollmentId}"
                               };
                           }
                       });
        }

        /// <summary>
        /// The Create action allows to save the student status change details.
        /// </summary>
        /// <remarks>
        /// The Create action requires StudentStatusChanges object which has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </remarks>
        /// <param name="model">
        /// The Post action method accepts a parameter of type StudentStatusChanges.
        /// </param>
        /// <returns>
        /// Returns an object of StudentStatusChanges where StudentStatusChanges has StudentStatusChangeId, StudentEnrollmentId, OriginalStatusId, NewStatusId, CampusId, 
        /// ModifiedDate, ModifiedUser, IsReversal, DropReasonId, DateOfChange, LastdateAttended, CaseNumber, RequestedBy, HaveBackup, HaveClientConfirmation and ResultStatus.
        /// </returns>
        public async Task<StudentStatusChanges> Create(StudentStatusChanges model)
        {
            var studentStatusChanges = new StudentStatusChanges();

            try
            {
                // start deleting the attendance and grades for the given enrollment if newstatus is No start
                if (await this.statusCodesService.IsSystemStatus(Guid.Parse(model.NewStatusId.ToString()), Constants.SystemStatus.NoStart))
                {
                    // deleting from student clock attendance
                    var deleteStudentClkAttendance =  await this.studentClkAttendanceService.Delete(model.StudentEnrollmentId);
                    // deleting from atClsSectAttendance
                    var deleteClassAttendance = await this.classSectionAttendanceService.Delete(model.StudentEnrollmentId);
                }
                var studentStatusChangesEntity = this.mapper.Map<SyStudentStatusChanges>(model);
                studentStatusChangesEntity.DateOfChange = model.DateOfChange;
                studentStatusChangesEntity.ModDate = DateTime.Now;
                var userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);
                studentStatusChangesEntity.ModUser = userDetails.FullName;
                studentStatusChangesEntity.OrigStatusId = model.OriginalStatusId;
                studentStatusChangesEntity.NewStatusId = model.NewStatusId;
                studentStatusChangesEntity.DropReasonId = model.DropReasonId;
                studentStatusChangesEntity.IsReversal = model.IsReversal;
                studentStatusChangesEntity.CampusId = model.CampusId;
                studentStatusChangesEntity.StuEnrollId = model.StudentEnrollmentId;
                var studentStatusChangesDetails = await this.SyStudentStatusChangesRepository.CreateAsync(studentStatusChangesEntity);
                await this.SyStudentStatusChangesRepository.SaveAsync();
                if (studentStatusChangesDetails != null)
                {
                    studentStatusChanges = this.mapper.Map<StudentStatusChanges>(studentStatusChangesDetails);
                    studentStatusChanges.ResultStatus = ApiMsgs.SAVE_STUDENT_STATUS_CHANGES_SUCCESS;
                }
                else
                {
                    studentStatusChanges.ResultStatus = ApiMsgs.SAVE_STUDENT_STATUS_CHANGES_UNSUCCESS;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                studentStatusChanges.ResultStatus = ApiMsgs.SAVE_STUDENT_STATUS_CHANGES_UNSUCCESS;
            }

            return studentStatusChanges;
        }

        /// <inheritdoc />
        /// <summary>
        /// The generate student summary report.
        /// </summary>
        /// <param name="reportRequest">
        /// The report request.
        /// </param>
        /// <returns>
        /// Returns an object of reportRequest .
        /// </returns>
        public async Task<ReportActionResult> GenerateStudentSummaryReport(ReportRequest reportRequest)
        {
            var actionResult = await this.reportService.GenerateReport(reportRequest);
            if (actionResult.Data == null)
            {
                return actionResult;
            }

            var isPreviewReport = Convert.ToBoolean(reportRequest.Parameters["isPreviewReport"].ToString());
            if (isPreviewReport)
            {
                return actionResult;
            }

            Guid terminationId =
                new Guid(reportRequest.Parameters[R2T4ReportParameters.StudentTerminationId].ToString());

            var student = this.R2T4TerminationDetailsRepository
                .Get(x => x.TerminationId == terminationId)
                .Include(x => x.StuEnrollment)
                    .ThenInclude(x => x.Lead)
                .Include(x => x.StuEnrollment)
                    .ThenInclude(x => x.PrgVer)
                .FirstOrDefault();

            Document document = new Document
            {
                DocumentType = reportRequest.Parameters["reportCategory"].ToString(),
                DocumentSubtype = reportRequest.Parameters["reportName"].ToString()
            };
            if (student != null)
            {
                document.StudentId = student.StuEnrollment.StudentId;
                document.CampusId = student.StuEnrollment.CampusId;
                document.FileId = Guid.NewGuid();
                document.FileName = document.FileId.ToString();
                document.DocumentContent = actionResult.Data;
                document.FileExtension = reportRequest.Parameters["reportExtension"].ToString();
                document.ModuleId = 1;
                document.DocumentStatusId = new Guid();
                document.ReceiveDate = DateTime.Now;
                document.RequestDate = DateTime.Now;
                document.DocumentStatus = Convert.ToInt32(SysDocumentStatuses.Approved);
                document.DocumentContent = actionResult.Data;

                document.DisplayName = student.StuEnrollment.PrgVer != null ? $"{student.StuEnrollment.Lead.LastName} {student.StuEnrollment.Lead.FirstName} {student.StuEnrollment?.PrgVer?.PrgVerDescrip} {document.DocumentSubtype} {DateTime.Now.Date.ToShortDateString()}" : $"{student.StuEnrollment.Lead.LastName} {student.StuEnrollment.Lead.FirstName} {document.DocumentSubtype} {DateTime.Now.Date.ToShortDateString()}";

                await this.documentService.Create(document);
            }

            return actionResult;
        }
    }
}