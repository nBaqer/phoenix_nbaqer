﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleDetailService.cs" company="FAME Inc.">
//     FAME Inc. 2018
// </copyright>
// <summary>
// The program schedule detail service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Orm.Advantage.Domain.Common;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore.Internal;

    using Unity.Interception.Utilities;

    /// <inheritdoc/>
    /// <summary>
    /// The program schedule detail service.
    /// </summary>
    public class ProgramScheduleDetailService : IProgramScheduleDetailService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The Enrollment Manager service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The class section meeting service.
        /// </summary>
        private readonly IClassSectionMeetingService classSectionMeetingService;

        /// <summary>
        /// The student termination service.
        /// </summary>
        private readonly IStudentTerminationService studentTerminationService;

        /// <summary>
        /// The program version service.
        /// </summary>
        private readonly IProgramVersionsService programVersionsService;

        /// <summary>
        /// The class section service.
        /// </summary>
        private readonly IClassSectionService classSectionService;

        /// <summary>
        /// The results service.
        /// </summary>
        private readonly IResultsService resultsService;

        /// <summary>
        /// The holiday Service.
        /// </summary>
        private readonly IHolidayService holidayService;

        /// <summary>
        /// The Course Details Service.
        /// </summary>
        private readonly ICourseDetailsService courseDetailsService;

        /// <summary>
        /// The credit summary service.
        /// </summary>
        private readonly ICreditSummaryService creditSummaryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramScheduleDetailService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">The mapper.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system configuration app setting service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        /// <param name="programVersionsService">
        /// The program Versions Service.
        /// </param>
        /// <param name="classSectionService">
        /// The class Section Service.
        /// </param>
        /// <param name="classSectionMeetingService">
        /// The class Section Meeting Service.
        /// </param>
        /// <param name="studentTerminationService">
        /// The student Termination Service.
        /// </param>
        /// <param name="resultsService">
        /// The results Service.
        /// </param>
        /// <param name="holidayService">
        /// The holiday Service.
        /// </param>
        /// <param name="courseDetailsService">
        /// The course Details Service.
        /// </param>
        /// <param name="creditSummaryService">
        /// The credit Summary Service.
        /// </param>
        public ProgramScheduleDetailService(
            IAdvantageDbContext context,
            IMapper mapper,
            ISystemConfigurationAppSettingService systemConfigurationAppSettingService,
            IEnrollmentService enrollmentService,
            IProgramVersionsService programVersionsService,
            IClassSectionService classSectionService,
            IClassSectionMeetingService classSectionMeetingService,
            IStudentTerminationService studentTerminationService,
            IResultsService resultsService,
            IHolidayService holidayService,
            ICourseDetailsService courseDetailsService,
            ICreditSummaryService creditSummaryService)
        {
            this.context = context;
            this.mapper = mapper;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.enrollmentService = enrollmentService;
            this.programVersionsService = programVersionsService;
            this.classSectionService = classSectionService;
            this.classSectionMeetingService = classSectionMeetingService;
            this.studentTerminationService = studentTerminationService;
            this.resultsService = resultsService;
            this.holidayService = holidayService;
            this.courseDetailsService = courseDetailsService;
            this.creditSummaryService = creditSummaryService;
        }

        /// <summary>
        /// The program schedule details repository.
        /// </summary>
        private IRepository<ArProgScheduleDetails> ProgramScheduleDetailsRepository => this.context.GetRepositoryForEntity<ArProgScheduleDetails>();

        /// <summary>
        /// The GetDetailByScheduleId returns the matching program schedule details by given scheduleId.
        /// </summary>
        /// <param name="scheduleIds">The schedule Ids.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task<IEnumerable<ProgramScheduleDetails>> GetDetailByScheduleId(List<Guid> scheduleIds)
        {
            return await Task.Run(() =>
            {
                var programScheduleDetails =
                    this.ProgramScheduleDetailsRepository.Get(x => scheduleIds.Contains((Guid)x.ScheduleId)).OrderBy(x => x.ScheduleId);

                return this.mapper.Map<IEnumerable<ProgramScheduleDetails>>(programScheduleDetails);
            });
        }

        /// <summary>
        /// The get credit hour scheduled break returns the scheduled breaks within a specific start
        /// and end date.
        /// </summary>
        /// <param name="enrollmentId">The enrollment Id.</param>
        /// <param name="endDate">end Date</param>
        /// <param name="startDate">The start Date.</param>
        /// <returns>The <see cref="Task"/>.</returns>
        public async Task<CreditHourProgramSchedule> GetCreditHourScheduledBreak(Guid enrollmentId, DateTime endDate, DateTime startDate)
        {
            return await Task.Run(
                       async () =>
                       {
                           var programScheduleDetails = new CreditHourProgramSchedule { ResultStatus = string.Empty };
                           if (startDate == DateTime.MinValue || endDate == DateTime.MinValue)
                           {
                               programScheduleDetails.ResultStatus = ApiMsgs.INVALID_START_OR_END_DATE;
                               return programScheduleDetails;
                           }

                           var enrollment = await this.enrollmentService.Get(enrollmentId);
                           if (enrollment == null)
                           {
                               programScheduleDetails.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                               return programScheduleDetails;
                           }

                           var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollment.CampusId);

                           // Get the scheduled breaks for start date and end date
                           var scheduleBreaksDetail = trackSapAttendance == TrackSapAttendance.ByDay
                                                     ? await this.GetScheduledBreaksForStudentClockAttendance(
                                                           startDate,
                                                           endDate,
                                                           enrollment)
                                                     : await this.GetScheduledBreaksForClassSectionAttendance(
                                                           startDate,
                                                           endDate,
                                                           enrollment);

                           programScheduleDetails.TotalScheduledBreakDays = scheduleBreaksDetail.Text;
                           programScheduleDetails.BreakDateList = scheduleBreaksDetail.Value;

                           programScheduleDetails.ResultStatus = programScheduleDetails.ResultStatus == string.Empty ?
                                                                     ApiMsgs.SCHEDULE_BREAKS_CALCULATED_SUCCESSFUL :
                                                                     programScheduleDetails.ResultStatus;

                           return programScheduleDetails;
                       });
        }

        /// <summary>
        /// The get credit Hour completed days by enrollment id action returns completed days in the period.
        /// </summary>
        /// <remarks>
        /// The get credit Hour completed days by EnrollmentId action returns completed days in the
        /// period. It requires enrollmentId. Based on the EnrollmentId and date range, it searches
        /// all the enrolled program details and returns completed days in the period.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// The Completed Days
        /// </returns>
        public async Task<int> GetCreditHourCompletedDaysByEnrollmentId(Guid enrollmentId, DateTime? endDate)
        {
            var actualCompletedDays = 0;
            double scheduleLoaCount;

            return await Task.Run(async () =>
            {
                try
                {
                    var currentPeriodDetails = new CurrentPeriodDetails();
                    var enrollment = await this.enrollmentService.Get(enrollmentId);
                    if (enrollment != null)
                    {
                        var programType = await this.programVersionsService.IsProgramVersionSelfPaced(enrollmentId);
                        if (!programType)
                        {
                            var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
                            if (endDate == DateTime.MinValue || endDate == null)
                            {
                                var endDateOfCreditHourPaymentPeriod = await this.GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(enrollmentId);
                                endDate = endDateOfCreditHourPaymentPeriod.EndDateOfWithdrawalPeriod;
                            }

                            var dateOfWithdrawal = terminationDetails?.LastDateAttended;
                            if (endDate < dateOfWithdrawal)
                            {
                                dateOfWithdrawal = endDate;
                            }

                            var currentPeriodDetailsWithCreditsEarned = await this.programVersionsService.GetCreditHoursWithdrawalPaymentPeriod(enrollmentId);
                            currentPeriodDetails.StartDateOfWithdrawalPeriod = currentPeriodDetailsWithCreditsEarned?.StartDateOfWithdrawalPeriod;

                            if (dateOfWithdrawal != null && currentPeriodDetails.StartDateOfWithdrawalPeriod != null)
                            {
                                var scheduledBreaks = await this.GetCreditHourScheduledBreak(enrollmentId, dateOfWithdrawal.Value, currentPeriodDetails.StartDateOfWithdrawalPeriod ?? DateTime.MinValue);

                                var starDateToWithdrawalDateDifference =
                                    (dateOfWithdrawal.Value - currentPeriodDetails.StartDateOfWithdrawalPeriod.Value).Days + 1;

                                var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId);

                                scheduleLoaCount = this.GetLoAsForDateRange(studentLoAs, dateOfWithdrawal.Value, currentPeriodDetails.StartDateOfWithdrawalPeriod.Value);

                                actualCompletedDays = Convert.ToInt32(starDateToWithdrawalDateDifference - scheduleLoaCount - Convert.ToInt32(scheduledBreaks.TotalScheduledBreakDays));
                            }
                        }

                        return actualCompletedDays;
                    }
                    else
                    {
                        return actualCompletedDays;
                    }
                }
                catch (Exception)
                {
                    return actualCompletedDays;
                }
            });
        }

        /// <summary>
        /// The Get EndDate Of CreditHour PaymentPeriod action returns the End date of the payment
        /// period when a student withdraws from a non term not self paced program or when DL period
        /// is used for calculation for Nonstandard term programs with terms not substantially equal
        /// in length.
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour PaymentPeriod by EnrollmentId action returns End date of
        /// the payment period when a student withdraws from a non term not self paced program or
        /// when DL period is used for calculation for Nonstandard term programs with terms not
        /// substantially equal in length. it requires enrollmentId .
        /// </remarks>
        /// <param name="enrollmentId">The enrollment id is required field and is a type of Guid.</param>
        /// <returns>Returns End date of the payment period when a student withdraws.</returns>
        public async Task<CreditHourWithdrawalEndDateDetails> GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(
            Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var endDateOfWithdrawalPaymentPeriod = new CreditHourWithdrawalEndDateDetails();
                           try
                           {
                               var enrollments = await this.enrollmentService.Get(enrollmentId);
                               if (enrollments == null)
                               {
                                   endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                       $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                                   return endDateOfWithdrawalPaymentPeriod;
                               }
                               var enrollmentAcademicCalendarId = (await this.programVersionsService.GetEnrollmentDetailsById(enrollmentId)).AcademicCalendarId;


                               var substantiallyEqual =
                                   await this.programVersionsService.IsProgramVersionSubstantiallyEqual(
                                       enrollmentId);

                               var selfPaced =
                                   await this.programVersionsService.IsProgramVersionSelfPaced(enrollmentId);

                               var termType = true;
                               if (enrollmentAcademicCalendarId != null)
                               {

                                   if (((int)enrollmentAcademicCalendarId == (int)Constants.AcademicCalendars.NonStandardTerm && !substantiallyEqual)
                                    || ((int)enrollmentAcademicCalendarId == (int)Constants.AcademicCalendars.NonTerm && !selfPaced))
                                   {
                                       termType = false;
                                   }

                               }
                              
                               if (termType)
                               {
                                   endDateOfWithdrawalPaymentPeriod.ResultStatus = ApiMsgs
                                       .NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH_NON_TERM_NOT_SELFPACED_TERM;
                                   return endDateOfWithdrawalPaymentPeriod;
                               }

                               var trackSapAttendance =
                                   this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                                       ConfigurationAppSettingsKeys.TrackSapAttendance,
                                       enrollments.CampusId);
                               var isByDay = trackSapAttendance == TrackSapAttendance.ByDay;

                             
                               var creditHoursPeriodDetails =
                                   await this.programVersionsService.GetCreditHourPaymentPeriodsByEnrollmentId(
                                       enrollmentId);
                               var creditHourProgramPeriodDetails =
                                   creditHoursPeriodDetails.CreditHourProgramePeriodDetailsList;

                               var creditHrWithdrawalPeriodDetails =
                                   await this.programVersionsService.GetCreditHoursWithdrawalPaymentPeriod(
                                       enrollmentId);

                               var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
                               DateTime withdrawalDate = terminationDetails.LastDateAttended ?? DateTime.MinValue;

                               var startDateOfWithdrawalPeriod =
                                   creditHrWithdrawalPeriodDetails.StartDateOfWithdrawalPeriod ?? DateTime.MinValue;
                               var creditsEarnedInWithdrawalPeriod = creditHrWithdrawalPeriodDetails.CreditsEarned;
                               var withdrawalPeriod = creditHrWithdrawalPeriodDetails.WithdrawalPeriod - 1;
                               var weeksCompletedInWithdrawalPeriod =
                                   creditHrWithdrawalPeriodDetails.WeeksCompleted;
                               var weeksCompletedOn =
                                   creditHrWithdrawalPeriodDetails.WeeksCompletedOn ?? DateTime.MinValue;
                               var creditsEarnedOn =
                                   creditHrWithdrawalPeriodDetails.CreditsEarnedOn ?? DateTime.MinValue;

                               var lastPeriodCredits = creditHourProgramPeriodDetails.ElementAt(withdrawalPeriod)
                                   ?.Credits;
                               var lastPeriodWeeks = creditHourProgramPeriodDetails.ElementAt(withdrawalPeriod)
                                   ?.Weeks;
                               int? weeksPendingToComplete = lastPeriodWeeks - weeksCompletedInWithdrawalPeriod;
                               Tuple<bool, DateTime> endDateOfCredits = null;
                               if (creditHrWithdrawalPeriodDetails.ResultStatus
                                   == ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS)
                               {
                                   if (creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod
                                       == DateTime.MinValue
                                       || creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod == null)
                                   {
                                       var endDateOfWeeks = await this.GetWeeksEndDate(
                                                                enrollmentId,
                                                                weeksPendingToComplete,
                                                                startDateOfWithdrawalPeriod,
                                                                weeksCompletedOn);
                                       if (endDateOfWeeks != DateTime.MinValue)
                                       {
                                           endDateOfCredits = await this.GetCreditsEndDate(
                                                                  enrollmentId,
                                                                  creditHourProgramPeriodDetails.ToList(),
                                                                  startDateOfWithdrawalPeriod,
                                                                  creditsEarnedInWithdrawalPeriod,
                                                                  withdrawalDate,
                                                                  creditsEarnedOn,
                                                                  withdrawalPeriod,
                                                                  isByDay);
                                       }

                                       if (endDateOfCredits != null
                                           && (endDateOfCredits.Item2 != DateTime.MinValue
                                               && endDateOfWeeks != DateTime.MinValue))
                                       {
                                           if (endDateOfWeeks.Date > endDateOfCredits.Item2.Date)
                                           {
                                               endDateOfWithdrawalPaymentPeriod.EndDateOfWithdrawalPeriod =
                                                   endDateOfWeeks;
                                               endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                                   selfPaced
                                                       ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                                       : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                               return endDateOfWithdrawalPaymentPeriod;
                                           }

                                           endDateOfWithdrawalPaymentPeriod.EndDateOfWithdrawalPeriod =
                                               endDateOfCredits.Item2;
                                           endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                               selfPaced
                                                   ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                                   : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                           return endDateOfWithdrawalPaymentPeriod;
                                       }

                                       if (endDateOfCredits != null
                                           && (creditsEarnedOn == DateTime.MinValue
                                               && endDateOfCredits.Item2 == DateTime.MinValue
                                               && !endDateOfCredits.Item1))
                                       {
                                           if (endDateOfWeeks != DateTime.MinValue)
                                           {
                                               endDateOfWithdrawalPaymentPeriod.EndDateOfWithdrawalPeriod =
                                                   endDateOfWeeks;
                                               endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                                   selfPaced
                                                       ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                                       : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                               return endDateOfWithdrawalPaymentPeriod;
                                           }
                                       }

                                       if (endDateOfWeeks == DateTime.MinValue)
                                       {
                                           endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                               ApiMsgs.FAILED_CALCULATING_WEEKS_ENDDATE.Replace("[weeks]", Convert.ToString(weeksPendingToComplete, CultureInfo.InvariantCulture));
                                           return endDateOfWithdrawalPaymentPeriod;
                                       }

                                       if (endDateOfCredits != null && endDateOfCredits.Item2 == DateTime.MinValue)
                                       {
                                           endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                               ApiMsgs.FAILED_CALCULATING_CREDITS_ENDDATE.Replace("[credits]", Convert.ToString(lastPeriodCredits, CultureInfo.InvariantCulture));
                                           return endDateOfWithdrawalPaymentPeriod;
                                       }
                                   }
                                   else
                                   {
                                       endDateOfWithdrawalPaymentPeriod.EndDateOfWithdrawalPeriod =
                                           creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod;

                                       endDateOfWithdrawalPaymentPeriod.ResultStatus =
                                           selfPaced
                                               ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                               : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                       return endDateOfWithdrawalPaymentPeriod;
                                   }
                               }

                               endDateOfWithdrawalPaymentPeriod.ResultStatus = ApiMsgs.FAILED_CALCULATING_ENDDATE;
                               return endDateOfWithdrawalPaymentPeriod;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               endDateOfWithdrawalPaymentPeriod.ResultStatus = ApiMsgs.FAILED_CALCULATING_ENDDATE;
                               return endDateOfWithdrawalPaymentPeriod;
                           }
                       });
        }

        /// <summary>
        /// The Get EndDate Of CreditHour Period Of Enrollment action returns the End date of the
        /// Period Of Enrollment when a student withdraws from a Title IV Non-term or Non-Standard
        /// term program.
        /// </summary>
        /// <remarks>
        /// The Get EndDate Of CreditHour Period Of Enrollment by EnrollmentId action returns End
        /// date of the Period Of Enrollment when a student withdraws from a Title IV Non-term or
        /// Non-Standard term program. it requires enrollmentId .
        /// </remarks>
        /// <param name="enrollmentId">The enrollment id is required field and is a type of Guid.</param>
        /// <returns>Returns End date of period of enrollment when a student withdraws.</returns>
        public async Task<CreditHourWithdrawalEndDateDetails> GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(
            Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var endDateOfWithdrawalPeriodOfEnrollment = new CreditHourWithdrawalEndDateDetails();

                           try
                           {
                               var enrollments = await this.enrollmentService.Get(enrollmentId);
                               if (enrollments == null)
                               {
                                   endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                       $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                                   return endDateOfWithdrawalPeriodOfEnrollment;
                               }

                               var enrollmentAcademicCalendarId = (await this.programVersionsService.GetEnrollmentDetailsById(enrollmentId)).AcademicCalendarId;
                            
                              
                               var substantiallyEqual =
                                   await this.programVersionsService.IsProgramVersionSubstantiallyEqual(
                                       enrollmentId);

                               var selfPaced =
                                   await this.programVersionsService.IsProgramVersionSelfPaced(enrollmentId);

                               var termType = true;
                               if (enrollmentAcademicCalendarId != null)
                               {

                                   if (((int)enrollmentAcademicCalendarId == (int)Constants.AcademicCalendars.NonStandardTerm && !substantiallyEqual)
                                    || ((int)enrollmentAcademicCalendarId == (int)Constants.AcademicCalendars.NonTerm && !selfPaced))
                                    {
                                       termType = false;
                                    }
                                    
                               }

                               if (termType)
                               {
                                   endDateOfWithdrawalPeriodOfEnrollment.ResultStatus = ApiMsgs
                                       .NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH_NON_TERM_NOT_SELFPACED_TERM;
                                   return endDateOfWithdrawalPeriodOfEnrollment;
                               }

                               var trackSapAttendance =
                                   this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                                       ConfigurationAppSettingsKeys.TrackSapAttendance,
                                       enrollments.CampusId);
                               var isByDay = trackSapAttendance == TrackSapAttendance.ByDay;


                               var creditHoursPeriodOfEnrollmentDetails =
                                   await this.programVersionsService.GetCreditHourPeriodsLengthByEnrollmentId(
                                       enrollmentId);
                               var creditHourProgramPeriodDetails = creditHoursPeriodOfEnrollmentDetails
                                   .CreditHourProgramePeriodDetailsList;

                               var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
                               DateTime withdrawalDate = terminationDetails.LastDateAttended ?? DateTime.MinValue;

                               var creditHrWithdrawalPeriodDetails =
                                   await this.programVersionsService.GetNonStandardWithdrawalPeriodOfEnrollment(
                                       enrollmentId);
                               var startDateOfWithdrawalPeriod =
                                   creditHrWithdrawalPeriodDetails.StartDateOfWithdrawalPeriod ?? DateTime.MinValue;
                               var creditsEarnedInWithdrawalPeriod = creditHrWithdrawalPeriodDetails.CreditsEarned;
                               var weeksCompletedInWithdrawalPeriod =
                                   creditHrWithdrawalPeriodDetails.WeeksCompleted;
                               var weeksCompletedOn =
                                   creditHrWithdrawalPeriodDetails.WeeksCompletedOn ?? DateTime.MinValue;
                               var creditsEarnedOn =
                                   creditHrWithdrawalPeriodDetails.CreditsEarnedOn ?? DateTime.MinValue;
                               var withdrawalPeriod = creditHrWithdrawalPeriodDetails.WithdrawalPeriod - 1;
                               var lastPeriodCredits = creditHourProgramPeriodDetails.ElementAt(withdrawalPeriod)
                                   ?.Credits;
                               var lastPeriodWeeks = creditHourProgramPeriodDetails.ElementAt(withdrawalPeriod)
                                   ?.Weeks;
                               int? weeksPendingToComplete = lastPeriodWeeks - weeksCompletedInWithdrawalPeriod;
                               Tuple<bool, DateTime> endDateOfPeriodOfEnrollmentCredits = null;
                               if (creditHrWithdrawalPeriodDetails.ResultStatus
                                   == ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL)
                               {
                                   if (creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod
                                       == DateTime.MinValue
                                       || creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod == null)
                                   {
                                       var endDateOfPeriodOfEnrollmentWeeks = await this.GetWeeksEndDate(
                                                                                  enrollmentId,
                                                                                  weeksPendingToComplete,
                                                                                  startDateOfWithdrawalPeriod,
                                                                                  weeksCompletedOn);
                                       if (endDateOfPeriodOfEnrollmentWeeks != DateTime.MinValue)
                                       {
                                           endDateOfPeriodOfEnrollmentCredits = await this.GetCreditsEndDate(
                                                                                    enrollmentId,
                                                                                    creditHourProgramPeriodDetails.ToList(),
                                                                                    startDateOfWithdrawalPeriod,
                                                                                    creditsEarnedInWithdrawalPeriod,
                                                                                    withdrawalDate,
                                                                                    creditsEarnedOn,
                                                                                    withdrawalPeriod,
                                                                                    isByDay);
                                       }

                                       if (endDateOfPeriodOfEnrollmentCredits != null
                                           && (endDateOfPeriodOfEnrollmentCredits.Item2 != DateTime.MinValue
                                               && endDateOfPeriodOfEnrollmentWeeks != DateTime.MinValue))
                                       {
                                           if (endDateOfPeriodOfEnrollmentWeeks.Date
                                               > endDateOfPeriodOfEnrollmentCredits.Item2.Date)
                                           {
                                               endDateOfWithdrawalPeriodOfEnrollment.EndDateOfWithdrawalPeriod =
                                                   endDateOfPeriodOfEnrollmentWeeks;
                                               endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                                   !selfPaced
                                                       ? ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD
                                                       : ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD;
                                               return endDateOfWithdrawalPeriodOfEnrollment;
                                           }

                                           endDateOfWithdrawalPeriodOfEnrollment.EndDateOfWithdrawalPeriod =
                                               endDateOfPeriodOfEnrollmentCredits.Item2;
                                           endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                               selfPaced
                                                   ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                                   : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                           return endDateOfWithdrawalPeriodOfEnrollment;
                                       }

                                       if (endDateOfPeriodOfEnrollmentCredits != null
                                           && (creditsEarnedOn == DateTime.MinValue
                                               && endDateOfPeriodOfEnrollmentCredits.Item2 == DateTime.MinValue
                                               && !endDateOfPeriodOfEnrollmentCredits.Item1))
                                       {
                                           if (endDateOfPeriodOfEnrollmentWeeks != DateTime.MinValue)
                                           {
                                               endDateOfWithdrawalPeriodOfEnrollment.EndDateOfWithdrawalPeriod =
                                                   endDateOfPeriodOfEnrollmentWeeks;
                                               endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                                   !selfPaced
                                                       ? ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD
                                                       : ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD;
                                               return endDateOfWithdrawalPeriodOfEnrollment;
                                           }
                                       }

                                       if (endDateOfPeriodOfEnrollmentWeeks == DateTime.MinValue)
                                       {
                                           endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                               ApiMsgs.FAILED_CALCULATING_WEEKS_ENDDATE.Replace("[weeks]", Convert.ToString(weeksPendingToComplete, CultureInfo.InvariantCulture));
                                           return endDateOfWithdrawalPeriodOfEnrollment;
                                       }

                                       if (endDateOfPeriodOfEnrollmentCredits != null
                                           && endDateOfPeriodOfEnrollmentCredits.Item2 == DateTime.MinValue)
                                       {
                                           endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                               ApiMsgs.FAILED_CALCULATING_CREDITS_ENDDATE.Replace("[credits]", Convert.ToString(lastPeriodCredits, CultureInfo.InvariantCulture));
                                           return endDateOfWithdrawalPeriodOfEnrollment;
                                       }
                                   }
                                   else
                                   {
                                       endDateOfWithdrawalPeriodOfEnrollment.EndDateOfWithdrawalPeriod =
                                           creditHrWithdrawalPeriodDetails.EndDateOfWithdrawalPeriod;

                                       endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                           selfPaced
                                               ? ApiMsgs.END_DATE_OF_NOT_SUBSTANTIALLYEQUAL_PAYMENTPERIOD
                                               : ApiMsgs.END_DATE_OF_NOT_SELFPACED_PAYMENTPERIOD;
                                       return endDateOfWithdrawalPeriodOfEnrollment;
                                   }
                               }

                               endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                   ApiMsgs.FAILED_CALCULATING_ENDDATE;
                               return endDateOfWithdrawalPeriodOfEnrollment;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               endDateOfWithdrawalPeriodOfEnrollment.ResultStatus =
                                   ApiMsgs.FAILED_CALCULATING_ENDDATE;
                               return endDateOfWithdrawalPeriodOfEnrollment;
                           }
                       });
        }

        /// <summary>
        /// The GetLoAsForDateRange returns the number of days in LOAs between date range.
        /// </summary>
        /// <param name="studentLoAs">The student LOAs.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="startDate">The start date.</param>
        /// <returns>The number of days for approved LOAs between given days.</returns>
        public double GetLoAsForDateRange(IEnumerable<LOA> studentLoAs, DateTime endDate, DateTime startDate)
        {
            double numberOfLoAs = 0.0;
            double previousTermNumberOfLoAs = 0.0;
            DateTime? loaEndDate = null;
            DateTime? loaStartDate = null;

            var enrollmentId = studentLoAs.Select(x => x.StudentEnrollmentId).FirstOrDefault();
            var scheduleBreaks = this.GetCreditHourScheduledBreak(enrollmentId, endDate, startDate).Result;
            var scheduleBreakWithLoa = new BreakDateRange();
            var overlappingDates = 0;

            var approvedLoAs = studentLoAs.Where(x => endDate >= x.StartDate && startDate <= x.EndDate).ToList();
            foreach( var x in approvedLoAs)
               {
                    if (x.EndDate != null && x.StartDate != null)
                    {
                        if (loaEndDate != null && loaStartDate != null && (x.StartDate - loaEndDate).Value.Days == 1)
                        {
                            for (var date = loaStartDate; date <= loaEndDate;)
                            {
                                if (date >= startDate && date <= endDate)
                                {
                                    scheduleBreakWithLoa = scheduleBreaks.BreakDateList.FirstOrDefault(
                                        _ => date.HasValue && _.BreakStartDate <= date && _.BreakEndDate >= date);

                                        previousTermNumberOfLoAs += 1;
                                    if (scheduleBreakWithLoa != null)
                                    {
                                        overlappingDates++;
                                       scheduleBreakWithLoa = null;
                                    }
                                }

                                date = date.Value.AddDays(1);
                            }
                        }

                        var daysDifference = (x.EndDate - x.StartDate).Value.TotalDays + 1;
                        if (daysDifference + previousTermNumberOfLoAs >= 5)
                        {
                            for (var date = x.StartDate; date <= x.EndDate;)
                            {
                                if (date >= startDate && date <= endDate)
                                {
                                    scheduleBreakWithLoa = scheduleBreaks.BreakDateList.FirstOrDefault(
                                        _ => date.HasValue && _.BreakStartDate <= date && _.BreakEndDate >= date);

                                        numberOfLoAs += 1;
                                    if (scheduleBreakWithLoa != null)
                                    {
                                        overlappingDates++;
                                        scheduleBreakWithLoa = null;
                                    }
                                }

                                date = date.Value.AddDays(1);
                            }
                        }

                        loaEndDate = x.EndDate.Value;
                        loaStartDate = x.StartDate.Value;
                    }
                    
                }
            double totalLoAs = numberOfLoAs + previousTermNumberOfLoAs - overlappingDates;
            return totalLoAs >= 5 || overlappingDates > 0 ? totalLoAs : 0.0;
        }

        #region private method

        /// <summary>
        /// The GetWeeksEndDate method will calculate the end date of the weeks earned in the
        /// withdrawal payment period.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="weeksPendingToComplete">
        /// The weeks Pending To Complete.
        /// </param>
        /// <param name="startDateOfWithdrawalPeriod">
        /// The start Date Of Withdrawal Period.
        /// </param>
        /// <param name="weeksCompletedOn">
        /// The weeks Completed On.
        /// </param>
        /// <returns>
        /// Returns end date of the weeks.
        /// </returns>
        private async Task<DateTime> GetWeeksEndDate(Guid enrollmentId, int? weeksPendingToComplete, DateTime startDateOfWithdrawalPeriod, DateTime weeksCompletedOn)
        {
            return await Task.Run(
                       async () =>
                       {
                           var enrollmentDetail = await this.programVersionsService.GetEnrollmentDetailsById(enrollmentId);

                           if (weeksPendingToComplete <= 0)
                           {
                               return weeksCompletedOn;
                           }

                           if (weeksCompletedOn != DateTime.MinValue || startDateOfWithdrawalPeriod != DateTime.MinValue)
                           {
                               var trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TrackSapAttendance, enrollmentDetail.Enrollment.CampusId);
                               DateTime date;

                               if (weeksCompletedOn == DateTime.MinValue)
                               {
                                   date = startDateOfWithdrawalPeriod;
                               }
                               else
                               {
                                   date = weeksCompletedOn.AddDays(1);
                               }

                               DateTime classEndDate = new DateTime();
                               DateTime meetingEndDate = new DateTime();

                               var results = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentId);
                               var clsSectIds = results.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();

                               var classSectionDetails = await this.classSectionService.GetDetailByIds(clsSectIds);

                               if (trackSapAttendance == TrackSapAttendance.ByDay)
                               {
                                   foreach (var classSection in classSectionDetails)
                                   {
                                       if (classSection != null)
                                       {
                                           var classDate = classSection.EndDate;
                                           if (classDate != DateTime.MinValue
                                               && classEndDate.Date < classDate.Date)
                                           {
                                               classEndDate = classDate;
                                           }
                                       }
                                   }
                               }
                               else
                               {
                                   var classSectionMeetingsDetails = await this.classSectionMeetingService.GetClassSectionMeetingDetailsList(clsSectIds);
                                   if (classSectionMeetingsDetails.Any())
                                   {
                                       foreach (var meeting in classSectionMeetingsDetails)
                                       {
                                           if (meeting != null)
                                           {
                                               var dates = meeting.EndDate;
                                               if (dates != DateTime.MinValue && meetingEndDate.Date < dates.Date)
                                               {
                                                   meetingEndDate = dates;
                                               }
                                           }
                                       }
                                   }
                               }

                               var laterEndDate = meetingEndDate < classEndDate ? classEndDate : meetingEndDate;
                               for (var weekCount = 0; weekCount <= weeksPendingToComplete;)
                               {
                                   if (weekCount != weeksPendingToComplete)
                                   {
                                       var startDate = date;
                                       var endDate = startDate.AddDays(6).Date;

                                       var scheduleWeeks = await this.classSectionMeetingService.GetScheduledWeeksForDateRangeByClassSectionIds(
                                                                                       enrollmentDetail.Enrollment,
                                                                                       trackSapAttendance == TrackSapAttendance.ByDay,
                                                                                       clsSectIds,
                                                                                       startDate,
                                                                                       endDate);

                                       var totalWeeksScheduledDates = scheduleWeeks.Item2.ToList();

                                       if (endDate <= laterEndDate)
                                       {
                                           if (totalWeeksScheduledDates.Count != 0)
                                           {
                                               if (weekCount != weeksPendingToComplete)
                                               {
                                                   weekCount++;
                                               }
                                           }
                                       }
                                       else
                                       {
                                           return DateTime.MinValue;
                                       }

                                       if (weekCount == weeksPendingToComplete)
                                       {
                                           var endDateOfWeeks = endDate;
                                           return endDateOfWeeks;
                                       }

                                       date = endDate.AddDays(1);
                                   }
                               }
                           }

                           return DateTime.MinValue;
                       });
        }

        /// <summary>
        /// The GetCreditsEndDate method will return end date of the credits earned in the withdrawal
        /// payment period.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="creditHourProgramPeriodDetails">
        /// The credit Hour Program Period Details.
        /// </param>
        /// <param name="startDateOfWithdrawalPeriod">
        /// The start Date Of Withdrawal Period.
        /// </param>
        /// <param name="creditsEarnedInWithdrawalPeriod">
        /// The credits Earned In Withdrawal Period.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal Date.
        /// </param>
        /// <param name="creditsEarnedOn">
        /// The credits Earned On.
        /// </param>
        /// <param name="withdrawalPeriod">
        /// The withdrawal Period.
        /// </param>
        /// <param name="isByDay">
        /// The is By Day.
        /// </param>
        /// <returns>
        /// Returns end date of the credits.
        /// </returns>
        private async Task<Tuple<bool, DateTime>> GetCreditsEndDate(Guid enrollmentId, List<CreditHourProgramePeriods> creditHourProgramPeriodDetails, DateTime startDateOfWithdrawalPeriod, decimal creditsEarnedInWithdrawalPeriod, DateTime withdrawalDate, DateTime creditsEarnedOn, int withdrawalPeriod, bool isByDay = false)
        {
            return await Task.Run(async () =>
            {
                decimal totalCreditScheduled = 0;
                decimal totalFailedCredits;
                var lastPaymentPeriodCredits = Convert.ToDecimal(creditHourProgramPeriodDetails.ElementAt(withdrawalPeriod)?.Credits);
                var resultDetails = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentId);
                resultDetails = resultDetails.ToList();
                if (resultDetails.Any())
                {
                    var classSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).Distinct().ToList();
                    var classSectionDetailsByIds = await this.classSectionService.GetDetailByIds(classSectionIds);
                    classSectionDetailsByIds = classSectionDetailsByIds.ToList();
                    var creditSummaryList = await this.creditSummaryService.GetCreditSummaryByEnrollmentId(enrollmentId);
                    creditSummaryList = creditSummaryList.ToList();
                    var clsSecIdOfCreditNeeded = new Guid();

                    // filter courses having start date greater than or equal to withdrawal period start date
                    var classSectionDetails = classSectionDetailsByIds.OrderBy(x => x.StartDate).ToList();
                    if (classSectionDetails.Any())
                    {
                        var failedCourseReqIds = creditSummaryList.Where(x => x.FinalGrade == "Fail").Select(x => x.ReqId).Distinct().ToList();
                        var classSectionMeetingsDetails = await this.classSectionMeetingService.GetClassSectionMeetingDetailsList(classSectionIds);
                        classSectionMeetingsDetails = classSectionMeetingsDetails.OrderBy(x => x.StartDate).ToList();
                        if (isByDay)
                        {
                            var classSectionDetailsForFailedCourses = classSectionDetailsByIds
                                .Where(x => x.StartDate >= startDateOfWithdrawalPeriod && x.StartDate <= withdrawalDate)
                                .OrderBy(x => x.StartDate).ToList();

                            var failedCourseCredits = await this.GetCreditRemainsList(failedCourseReqIds, classSectionDetailsForFailedCourses);
                            totalFailedCredits = failedCourseCredits.Sum(x => x.Value);
                        }
                        else
                        {
                            var classSectionMeetingDetailsForFailedCourses =
                                classSectionMeetingsDetails.Where(x => x.StartDate.Date >= startDateOfWithdrawalPeriod.Date && x.StartDate.Date <= withdrawalDate.Date)
                                 .OrderBy(x => x.StartDate).ToList();

                            var meetingClsSecIds = classSectionMeetingDetailsForFailedCourses.Select(x => x.ClassSectionId).Distinct().ToList();
                            var classSectionMeetingsDetailForFailedCourses = classSectionDetails.Where(x => meetingClsSecIds.Contains(x.ClassSectionId)).ToList();
                            var failedCourseCredits = await this.GetCreditRemainsList(failedCourseReqIds, classSectionMeetingsDetailForFailedCourses);
                            totalFailedCredits = failedCourseCredits.Sum(x => x.Value);
                        }

                        var totalCreditsEarnedInWithdrawalPeriod = creditsEarnedInWithdrawalPeriod + totalFailedCredits;
                        var creditsNeeded = lastPaymentPeriodCredits - totalCreditsEarnedInWithdrawalPeriod;
                        if (creditsNeeded <= 0)
                        {
                            return new Tuple<bool, DateTime>(false, creditsEarnedOn);
                        }

                        // checking whether the student has enough classes scheduled to earn the credits
                        if (isByDay)
                        {
                            var creditReqIds = creditSummaryList.Where(x => string.IsNullOrEmpty(x.FinalGrade)).Select(x => x.ReqId).Distinct().ToList();
                            classSectionDetails = classSectionDetailsByIds
                                .Where(x => x.StartDate >= startDateOfWithdrawalPeriod && x.StartDate <= withdrawalDate && x.EndDate > withdrawalDate)
                                .OrderBy(x => x.StartDate).ToList();

                            classSectionDetails = classSectionDetails.Where(x => creditReqIds.Contains(x.ReqId)).ToList();

                            var reqIds = classSectionDetails.Select(x => x.ReqId).Distinct().ToList();
                            var creditList = await this.GetCreditRemainsList(reqIds, classSectionDetails);
                            var totalCredits = creditList.Sum(x => x.Value);
                            if (totalCredits < creditsNeeded)
                            {
                                classSectionDetails = classSectionDetailsByIds.Where(x => x.StartDate > withdrawalDate)
                                    .OrderBy(x => x.StartDate).ToList();
                                reqIds = classSectionDetails.Select(x => x.ReqId).Distinct().ToList();
                                creditList = await this.GetCreditRemainsList(reqIds, classSectionDetails);
                            }

                            // calculate the total credit remains.
                            foreach (var credit in creditList)
                            {
                                decimal creditsScheduled = credit.Value;
                                totalCreditScheduled += Convert.ToInt32(creditsScheduled);
                                if (totalCreditScheduled >= creditsNeeded)
                                {
                                    clsSecIdOfCreditNeeded = credit.Text;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var creditClsSecIds = creditSummaryList.Where(x => string.IsNullOrEmpty(x.FinalGrade)).Select(x => x.ClassSectionId.ToLower()).Distinct().ToList();
                            var meetingClsSecIds = classSectionMeetingsDetails.Where(x => x.StartDate >= startDateOfWithdrawalPeriod && x.StartDate <= withdrawalDate && x.EndDate > withdrawalDate
                                                                                                && creditClsSecIds.Contains(x.ClassSectionId.ToString().ToLower()))
                                                                                                .OrderBy(x => x.StartDate)
                                                                                                .Select(x => x.ClassSectionId)
                                                                                                .Distinct().ToList();
                            var meetingClsSecIdList = meetingClsSecIds;
                            var classSectionDetailList = classSectionDetails.Where(x => meetingClsSecIdList.ToList().Contains(x.ClassSectionId)).ToList();
                            var meetingReqIds = classSectionDetails.Where(x => meetingClsSecIdList.ToList().Contains(x.ClassSectionId)).Select(x => x.ReqId).Distinct().ToList();
                            var creditList = await this.GetCreditRemainsList(meetingReqIds, classSectionDetailList);

                            var totalCredits = creditList.Sum(x => x.Value);
                            if (totalCredits < creditsNeeded)
                            {
                                totalCreditScheduled = totalCredits;
                                var classSectionIdList = classSectionMeetingsDetails.Where(x => x.StartDate > withdrawalDate).OrderBy(x => x.StartDate).Select(x => x.ClassSectionId).Distinct().ToList();
                                classSectionDetailList = new List<ClassSectionDetail>();
                                classSectionIdList.ForEach(
                                    classSectionId =>
                                    {
                                        var classSectionDetail = classSectionDetails.FirstOrDefault(y => y.ClassSectionId == classSectionId);
                                        if (classSectionDetail != null)
                                        {
                                            classSectionDetailList.Add(classSectionDetail);
                                        }
                                    });
                                var meetingReqId = classSectionDetails.Where(x => classSectionIdList.Contains(x.ClassSectionId)).Select(x => x.ReqId).Distinct().ToList();
                                creditList = await this.GetCreditRemainsList(meetingReqId, classSectionDetailList);
                            }

                            // calculate the total credit remains.
                            foreach (var credit in creditList)
                            {
                                totalCreditScheduled += Convert.ToInt32(credit.Value);
                                if (totalCreditScheduled >= creditsNeeded)
                                {
                                    clsSecIdOfCreditNeeded = credit.Text;
                                    break;
                                }
                            }
                        }

                        if (totalCreditScheduled < creditsNeeded)
                        {
                            return new Tuple<bool, DateTime>(true, DateTime.MinValue);
                        }

                        var clsSectMeetingsDetailsList = await this.classSectionMeetingService.GetClassSectionMeetingDetails(clsSecIdOfCreditNeeded);

                        var endDate = clsSectMeetingsDetailsList.Any() ? clsSectMeetingsDetailsList.OrderBy(x => x.StartDate).LastOrDefault()?.EndDate ?? DateTime.MinValue :
                                    classSectionDetails.OrderBy(x => x.StartDate).LastOrDefault()?.EndDate ?? DateTime.MinValue;
                        return new Tuple<bool, DateTime>(false, endDate);
                    }
                }

                return new Tuple<bool, DateTime>(false, DateTime.MinValue);
            });
        }

        /// <summary>
        /// The get credit earned list.
        /// </summary>
        /// <param name="reqIds">The req ids.</param>
        /// <param name="classSectionDetails">The class section details</param>
        /// <returns>The <see cref="Task"/>.</returns>
        private async Task<List<ListItem<Guid, decimal>>> GetCreditRemainsList(
           List<Guid> reqIds,
           IEnumerable<ClassSectionDetail> classSectionDetails)
        {
            var creditSummaryList = await this.courseDetailsService.GetCourseDetailsByReqIds(reqIds);

            var creditList = new List<ListItem<Guid, decimal>>();
            classSectionDetails.ForEach(
                result =>
                {
                    var creditSummaryDetails = creditSummaryList.Where(
                        cs => cs.ReqId == result.ReqId).ToList();

                    creditSummaryDetails.ForEach(
                        x =>
                        {
                            creditList.Add(
                                         new ListItem<Guid, decimal>
                                         {
                                             Text = result.ClassSectionId,
                                             Value = x.FinAidCredits ?? 0m
                                         });
                        });
                });
            return creditList;
        }

        /// <summary>
        /// The get scheduled breaks for student clock attendance returns the scheduled breaks count
        /// for the given start date and end date.
        /// </summary>
        /// <remarks>
        /// The GetScheduledBreaksForStudentClockAttendance action method returns the scheduled
        /// breaks count for the given start date and scheduleId. It calculate the scheduled breaks
        /// for the specific date range.
        /// </remarks>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ListItem<int, List<BreakDateRange>>> GetScheduledBreaksForStudentClockAttendance(DateTime startDate, DateTime endDate, Enrollment enrollment)
        {
            return await Task.Run(
                       async () =>
                           {
                               var breakDateRangeList = new List<BreakDateRange>();
                               var scheduleBreaks = 0;
                               var resultDetails = await this.resultsService.GetResultDetailsByEnrollmentId(enrollment.EnrollmentId);
                               var clsSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();
                               var holidays = await this.holidayService.GetHolidayListByCampus(enrollment.CampusId);
                               holidays = holidays.ToList();
                               if (clsSectionIds.Any())
                               {
                                   var scheduleList = await this.classSectionMeetingService.GetProgramLevelSchedules(
                                                          enrollment,
                                                          clsSectionIds,
                                                          startDate,
                                                          endDate);
                                   var meetingSchedules = scheduleList.ToList();

                                   if (meetingSchedules.Any())
                                   {
                                       meetingSchedules = meetingSchedules.OrderBy(p => p.Text).ToList();
                                       var originalMeetingSchedules = meetingSchedules;
                                       var scheduledPerWeekByMeetings = this.GetSchedulesPerWeek(meetingSchedules);
                                       var maxSchedulePerWeek = scheduledPerWeekByMeetings.Max(x => x.Value.Value);
                                       foreach (var scheduledPerWeekByMeeting in scheduledPerWeekByMeetings)
                                       {
                                           scheduledPerWeekByMeeting.Value.Value = maxSchedulePerWeek;
                                       }

                                       foreach (var holiday in holidays)
                                       {
                                           meetingSchedules = holiday.AllDay ? meetingSchedules.Where(x => !(x.Text.Date >= holiday.StartDate.Date && x.Text.Date <= holiday.EndDate.Date)).ToList()
                                                   : meetingSchedules.Where(x => !(x.Text >= holiday.StartDate && x.Text <= holiday.EndDate)).ToList();
                                       }

                                       if (meetingSchedules.Any())
                                       {
                                           // get the schedule break between start date and schedule start date
                                           var meetingSchedule = meetingSchedules.OrderBy(x => x.Text).FirstOrDefault();

                                           if (meetingSchedule != null && meetingSchedule.Text != DateTime.MinValue
                                                                       && startDate.Date < meetingSchedule.Text.Date)
                                           {
                                               var daysRequiredForScheduleBreak = this.GetDaysRequiredForScheduleBreakInSchedule(meetingSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);
                                               var daysDiff = Convert.ToInt32((meetingSchedule.Text.Date - startDate.Date).Days);
                                               if (daysDiff >= daysRequiredForScheduleBreak)
                                               {
                                                   scheduleBreaks += daysDiff;
                                                   breakDateRangeList.Add(new BreakDateRange { BreakEndDate = meetingSchedule.Text.Date.AddDays(-1), BreakStartDate = startDate.Date });
                                               }
                                           }

                                           meetingSchedules = meetingSchedules.OrderBy(x => x.Text).ToList();
                                           IListItem<DateTime, Guid> previousSchedule = new ListItem<DateTime, Guid> { Text = DateTime.MinValue, Value = Guid.Empty };

                                           foreach (var currentSchedule in meetingSchedules)
                                           {
                                               if (previousSchedule.Text == DateTime.MinValue)
                                               {
                                                   previousSchedule = currentSchedule;
                                               }
                                               else
                                               {
                                                   var previousScheduleDate = previousSchedule.Text.Date;
                                                   var currentScheduleDate = currentSchedule.Text.Date;
                                                   var daysRequiredForScheduleBreak = this.GetDaysRequiredForScheduleBreakInSchedule(previousSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);
                                                   var daysDifference = (currentScheduleDate - previousScheduleDate).Days - 1;
                                                   var holidaysBetweenSchedule = holidays.Where(x => x.StartDate.Date > previousScheduleDate && x.StartDate.Date < currentScheduleDate);
                                                   holidaysBetweenSchedule = holidaysBetweenSchedule.Where(x => (x.EndDate.Date - x.StartDate.Date).Days + 1 >= 5).ToList();
                                                   var breakDateRange = new BreakDateRange
                                                   {
                                                       BreakEndDate = currentSchedule.Text.Date.AddDays(-1),
                                                       BreakStartDate = previousSchedule.Text.Date.AddDays(1)
                                                   };
                                                   var holidayStartDate = DateTime.MinValue;
                                                   var holidayEndDate = DateTime.MinValue;
                                                   if (holidaysBetweenSchedule.Any() && daysDifference <= 7)
                                                   {
                                                       var holidayDaysBreak = 0;
                                                       holidaysBetweenSchedule.ForEach(
                                                           x =>
                                                           {
                                                               holidayDaysBreak += (x.EndDate.Date - x.StartDate.Date).Days + 1;
                                                               if (holidayEndDate == DateTime.MinValue
                                                                   || holidayEndDate < x.EndDate.Date)
                                                               {
                                                                   holidayEndDate = x.EndDate.Date;
                                                               }

                                                               if (holidayStartDate == DateTime.MinValue
                                                                   || holidayStartDate < x.StartDate.Date)
                                                               {
                                                                   holidayEndDate = x.StartDate.Date;
                                                               }
                                                           });

                                                       // check if the difference is more than holiday days in the date range.
                                                       if (daysRequiredForScheduleBreak == 7 && daysDifference <= 7)
                                                       {
                                                           daysDifference = holidayDaysBreak;
                                                           breakDateRange = new BreakDateRange
                                                           {
                                                               BreakEndDate = holidayEndDate,
                                                               BreakStartDate = holidayStartDate
                                                           };
                                                       }
                                                       else if (daysRequiredForScheduleBreak == 6 && daysDifference <= 6)
                                                       {
                                                           daysDifference = holidayDaysBreak;
                                                           breakDateRange = new BreakDateRange
                                                           {
                                                               BreakEndDate = holidayEndDate,
                                                               BreakStartDate = holidayStartDate
                                                           };
                                                       }
                                                       else
                                                       {
                                                           if (holidayDaysBreak > daysDifference)
                                                           {
                                                               breakDateRange = new BreakDateRange
                                                               {
                                                                   BreakEndDate = holidayEndDate,
                                                                   BreakStartDate = holidayStartDate
                                                               };
                                                               daysDifference = holidayDaysBreak;
                                                           }
                                                       }

                                                       daysRequiredForScheduleBreak = 5;
                                                   }

                                                   if (daysRequiredForScheduleBreak <= daysDifference)
                                                   {
                                                       scheduleBreaks += daysDifference;
                                                       breakDateRangeList.Add(breakDateRange);
                                                   }

                                                   previousSchedule = currentSchedule;
                                               }
                                           }

                                           meetingSchedule = meetingSchedules.OrderBy(x => x.Text).LastOrDefault();
                                           if (meetingSchedule != null && meetingSchedule.Text != DateTime.MinValue
                                                                       && meetingSchedule.Text < endDate)
                                           {
                                               var daysRequiredForScheduleBreak = this.GetDaysRequiredForScheduleBreakInSchedule(meetingSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);

                                               // get the schedule break between schedule end date and end date
                                               var daysDiff = Convert.ToInt32((endDate.Date - meetingSchedule.Text.Date).Days);
                                               if (daysDiff >= daysRequiredForScheduleBreak)
                                               {
                                                   scheduleBreaks += daysDiff;
                                                   breakDateRangeList.Add(new BreakDateRange { BreakEndDate = endDate.Date, BreakStartDate = meetingSchedule.Text.Date.AddDays(1) });
                                               }
                                           }
                                       }
                                   }
                               }

                               return new ListItem<int, List<BreakDateRange>> { Text = scheduleBreaks, Value = breakDateRangeList };
                           });
        }

        /// <summary>
        /// The get scheduled breaks for student clock attendance returns the scheduled breaks count
        /// for the given start date and end date.
        /// </summary>
        /// <remarks>
        /// The GetScheduledBreaksForStudentClockAttendance action method returns the scheduled
        /// breaks count for the given start date and enrollmentId. It calculate the scheduled breaks
        /// for the specific date range.
        /// </remarks>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ListItem<int, List<BreakDateRange>>> GetScheduledBreaksForClassSectionAttendance(DateTime startDate, DateTime endDate, Enrollment enrollment)
        {
            return await Task.Run(
                       async () =>
                       {
                           var breakDateRangeList = new List<BreakDateRange>();
                           var scheduleBreaks = 0;
                           var resultDetails = await this.resultsService.GetResultDetailsByEnrollmentId(enrollment.EnrollmentId);
                           var clsSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();
                           var holidays = await this.holidayService.GetHolidayListByCampus(enrollment.CampusId);
                           var holidayList = holidays.OrderBy(x => x.StartDate).ToList();
                           if (clsSectionIds.Any())
                           {
                               var classSectionsSchedule = await this.classSectionMeetingService.GetClassSectionsScheduleForDateRange(clsSectionIds, startDate, endDate);
                               var meetingSchedules = classSectionsSchedule.ToList();
                               if (meetingSchedules.Any())
                               {
                                   meetingSchedules = meetingSchedules.OrderBy(p => p.Text).ToList();
                                   var originalMeetingSchedules = meetingSchedules;
                                   var scheduledPerWeekByMeetings = this.GetSchedulesPerWeek(meetingSchedules);

                                   foreach (var holiday in holidayList)
                                   {
                                       meetingSchedules = holiday.AllDay ?
                                                              meetingSchedules.Where(x => !(x.Text.Date >= holiday.StartDate.Date && x.Text.Date <= holiday.EndDate.Date)).ToList() :
                                                              meetingSchedules.Where(x => !(x.Text >= holiday.StartDate && x.Text <= holiday.EndDate)).ToList();
                                   }

                                   if (meetingSchedules.Any())
                                   {
                                       // get the schedule break between start date and schedule
                                       // start date
                                       var meetingSchedule = meetingSchedules.OrderBy(x => x.Text).FirstOrDefault();
                                       if (meetingSchedule != null
                                           && meetingSchedule.Text != DateTime.MinValue
                                           && startDate.Date < meetingSchedule.Text.Date)
                                       {
                                           var daysRequiredForScheduleBreak = this.GetDaysRequiredForScheduleBreakInSchedule(meetingSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);

                                           var daysDiff = Convert.ToInt32((meetingSchedule.Text.Date - startDate.Date).Days);
                                           if (daysDiff >= daysRequiredForScheduleBreak)
                                           {
                                               scheduleBreaks = scheduleBreaks + daysDiff;
                                               breakDateRangeList.Add(new BreakDateRange { BreakEndDate = meetingSchedule.Text.Date.AddDays(-1), BreakStartDate = startDate.Date });
                                           }
                                       }

                                       meetingSchedules = meetingSchedules.OrderBy(x => x.Text).ToList();
                                       IListItem<DateTime, Guid> previousSchedule = new ListItem<DateTime, Guid> { Text = DateTime.MinValue, Value = Guid.Empty };
                                       foreach (var currentSchedule in meetingSchedules)
                                       {
                                           if (previousSchedule.Text == DateTime.MinValue)
                                           {
                                               previousSchedule = currentSchedule;
                                           }
                                           else
                                           {
                                               var previousScheduleDate = previousSchedule.Text.Date;
                                               var currentScheduleDate = currentSchedule.Text.Date;
                                               var isPeriodChanged = this.IsPeriodChanged(previousSchedule, currentSchedule, meetingSchedules, scheduledPerWeekByMeetings);
                                               var daysRequiredForScheduleBreak = isPeriodChanged ? 5 : this.GetDaysRequiredForScheduleBreakInSchedule(previousSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);

                                               var daysDifference = (currentScheduleDate - previousScheduleDate).Days - 1;
                                               var holidaysBetweenSchedule = holidayList.Where(x => x.StartDate.Date > previousScheduleDate && x.StartDate.Date < currentScheduleDate);
                                               holidaysBetweenSchedule = holidaysBetweenSchedule.Where(x => (x.EndDate.Date - x.StartDate.Date).Days + 1 >= 5).ToList();
                                               var breakDateRange = new BreakDateRange
                                               {
                                                   BreakEndDate = currentSchedule.Text.Date.AddDays(-1),
                                                   BreakStartDate = previousSchedule.Text.Date.AddDays(1)
                                               };
                                               var holidayStartDate = DateTime.MinValue;
                                               var holidayEndDate = DateTime.MinValue;
                                               if (holidaysBetweenSchedule.Any() && daysDifference <= 7)
                                               {
                                                   var holidayDaysBreak = 0;
                                                   holidaysBetweenSchedule.ForEach(
                                                       x =>
                                                           {
                                                               holidayDaysBreak +=
                                                                   (x.EndDate.Date - x.StartDate.Date).Days + 1;
                                                           });

                                                   // check if the difference is more than holiday days in the date range.
                                                   if (daysRequiredForScheduleBreak == 7 && daysDifference <= 7)
                                                   {
                                                       daysDifference = holidayDaysBreak;
                                                       breakDateRange = new BreakDateRange
                                                       {
                                                           BreakEndDate = holidayEndDate,
                                                           BreakStartDate = holidayStartDate
                                                       };
                                                   }
                                                   else if (daysRequiredForScheduleBreak == 6 && daysDifference <= 6)
                                                   {
                                                       daysDifference = holidayDaysBreak;
                                                       breakDateRange = new BreakDateRange
                                                       {
                                                           BreakEndDate = holidayEndDate,
                                                           BreakStartDate = holidayStartDate
                                                       };
                                                   }
                                                   else
                                                   {
                                                       daysDifference = holidayDaysBreak > daysDifference ? holidayDaysBreak : daysDifference;
                                                   }

                                                   daysRequiredForScheduleBreak = 5;
                                               }

                                               if (daysRequiredForScheduleBreak <= daysDifference)
                                               {
                                                   scheduleBreaks += daysDifference;
                                                   breakDateRangeList.Add(breakDateRange);
                                               }

                                               previousSchedule = currentSchedule;
                                           }
                                       }

                                       meetingSchedule = meetingSchedules.OrderBy(x => x.Text).LastOrDefault();
                                       if (meetingSchedule != null
                                           && meetingSchedule.Text != DateTime.MinValue
                                           && meetingSchedule.Text < endDate)
                                       {
                                           var daysRequiredForScheduleBreak = this.GetDaysRequiredForScheduleBreakInSchedule(meetingSchedule.Text, originalMeetingSchedules, scheduledPerWeekByMeetings);

                                           // get the schedule break between schedule end date and
                                           // withdrawal date
                                           var daysDiff = Convert.ToInt32((endDate.Date - meetingSchedule.Text.Date).Days);
                                           if (daysDiff >= daysRequiredForScheduleBreak)
                                           {
                                               scheduleBreaks += daysDiff;
                                               breakDateRangeList.Add(new BreakDateRange { BreakEndDate = endDate.Date, BreakStartDate = meetingSchedule.Text.Date.AddDays(1) });
                                           }
                                       }
                                   }
                               }
                           }

                           return new ListItem<int, List<BreakDateRange>> { Text = scheduleBreaks, Value = breakDateRangeList };
                       });
        }

        /// <summary>
        /// The is period changed.
        /// </summary>
        /// <param name="previousSchedule">
        /// The previous schedule.
        /// </param>
        /// <param name="currentSchedule">
        /// The current schedule.
        /// </param>
        /// <param name="meetingSchedules">
        /// The meeting schedules.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool IsPeriodChanged(IListItem<DateTime, Guid> previousSchedule, IListItem<DateTime, Guid> currentSchedule, List<IListItem<DateTime, Guid>> meetingSchedules, Dictionary<DateTime, IListItem<DateTime, int>> scheduledPerWeekByMeetings)
        {
            if (previousSchedule.Value != currentSchedule.Value)
            {
                var previousValue = scheduledPerWeekByMeetings[previousSchedule.Text].Value;
                var currentValue = scheduledPerWeekByMeetings[currentSchedule.Text].Value;
                if (previousValue != currentValue)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The get days required for schedule break in schedule.
        /// </summary>
        /// <param name="currentScheduleDate">
        /// The current schedule date.
        /// </param>
        /// <param name="originalMeetingsSchedule">
        /// The Original Meetings Schedule
        /// </param>
        /// <param name="scheduledPerWeekByMeetings">
        /// The scheduled per week by meetings.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int GetDaysRequiredForScheduleBreakInSchedule(DateTime currentScheduleDate, List<IListItem<DateTime, Guid>> originalMeetingsSchedule, Dictionary<DateTime, IListItem<DateTime, int>> scheduledPerWeekByMeetings)
        {
            var schedulePerWeek = scheduledPerWeekByMeetings[currentScheduleDate];
            switch (schedulePerWeek.Value)
            {
                case 1:
                    return 7; // returns 7 in case only one schedule per week
                case 2:
                    var weekStartDate = schedulePerWeek.Text;
                    var weekEndDate = weekStartDate.AddDays(6);
                    var schedulesForWeek = originalMeetingsSchedule.Where(x => x.Text >= weekStartDate && x.Text <= weekEndDate).ToList();
                    var firstSchedulesForWeek = schedulesForWeek.FirstOrDefault()?.Text ?? DateTime.MinValue;
                    var secondSchedulesForWeek = schedulesForWeek.LastOrDefault()?.Text ?? DateTime.MinValue;
                    var dayOfWeek = Convert.ToInt32(firstSchedulesForWeek.DayOfWeek);
                    var secondDayOfWeek = Convert.ToInt32(secondSchedulesForWeek.DayOfWeek);
                    var nextDayOfWeek = dayOfWeek == 6 ? 0 : (dayOfWeek == 0 ? 6 : dayOfWeek + 1);
                    return (secondDayOfWeek == nextDayOfWeek) ? 6 : 5;

                default:
                    return 5; // returns 5 in case more than two schedule per week and is on none consecutive days
            }
        }

        /// <summary>
        /// The get schedules per week.
        /// </summary>
        /// <param name="meetingSchedules">
        /// The meeting schedules.
        /// </param>
        /// <returns>
        /// The schedules per week.
        /// </returns>
        private Dictionary<DateTime, IListItem<DateTime, int>> GetSchedulesPerWeek(List<IListItem<DateTime, Guid>> meetingSchedules)
        {
            var schedulePerWeek = new Dictionary<DateTime, IListItem<DateTime, int>>();
            var periodIds = meetingSchedules.Select(x => x.Value).Distinct().ToList();
            foreach (var periodId in periodIds)
            {
                var meetingSchedule = meetingSchedules.Where(m => m.Value == periodId).OrderBy(x => x.Text).ToList();
                var scheduleStartDate = meetingSchedule.FirstOrDefault()?.Text ?? DateTime.MinValue;
                var scheduleEndDate = meetingSchedule.LastOrDefault()?.Text ?? DateTime.MinValue;
                for (var startDate = scheduleStartDate; startDate <= scheduleEndDate;)
                {
                    var weekStart = startDate;
                    var weekEnd = startDate.AddDays(6);
                    var meetings = meetingSchedule.Where(x => x.Text >= weekStart && x.Text <= weekEnd).ToList();
                    meetings.ForEach(
                        x =>
                            {
                                if (!schedulePerWeek.ContainsKey(x.Text))
                                {
                                        schedulePerWeek.Add(x.Text, new ListItem<DateTime, int> { Text = weekStart, Value = meetings.Count });
                                }
                            });

                    startDate = weekEnd.AddDays(1); 
                }
            }

            return schedulePerWeek;
        }

        #endregion private method
    }
}