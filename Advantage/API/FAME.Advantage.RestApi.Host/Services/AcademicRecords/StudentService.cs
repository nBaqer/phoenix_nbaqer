﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <inheritDoc/>
// <summary>
//   Defines the StudentService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;
    using FAME.Orm.Advantage.Domain.Common;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Query.Internal;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The student service.
    /// </summary>
    /// <inheritdoc cref="IStudentService"/>
    public class StudentService : IStudentService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentService"/> class.
        /// </summary>
        /// <param name="context">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        public StudentService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
        }

        /// <summary>
        /// The student repository.
        /// </summary>
        private IRepository<AdLeads> StudentRepository =>
            this.context.GetRepositoryForEntity<AdLeads>();

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository =>
            this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The student schedules repository.
        /// </summary>
        private IRepository<ArStudentSchedules> StudentSchedulesRepository =>
            this.context.GetRepositoryForEntity<ArStudentSchedules>();

        /// <summary>
        /// The termination repository.
        /// </summary>
        private IRepository<ArR2t4terminationDetails> TerminationRepository =>
            this.context.GetRepositoryForEntity<ArR2t4terminationDetails>();


        /// <summary>
        /// The Student Groups repository.
        /// </summary>
        private IRepository<AdStuGrpStudents> StuGrpStudentsRepository =>
            this.context.GetRepositoryForEntity<AdStuGrpStudents>();

        /// <summary>
        /// The Student Groups holds repository.
        /// </summary>
        private IRepository<AdStudentGroups> StudentGroupsRepository =>
            this.context.GetRepositoryForEntity<AdStudentGroups>();

        /// <summary>
        /// The student adressees repository.
        /// </summary>
        private IRepository<AdLeadAddresses> StudentAdressesRepository =>
            this.context.GetRepositoryForEntity<AdLeadAddresses>();

        private IRepository<AdLeadPhone> StudentPhoneRepository =>
            this.context.GetRepositoryForEntity<AdLeadPhone>();


        private IRepository<AdLeadEmail> StudentEmailRepository =>
            this.context.GetRepositoryForEntity<AdLeadEmail>();

        /// <summary>
        /// The Class Sections repository.
        /// </summary>
        private IRepository<ArClassSections> ClassSectionsRepository =>
         this.context.GetRepositoryForEntity<ArClassSections>();

        /// <summary>
        /// The Reqs repository.
        /// </summary>
        private IRepository<ArReqs> ReqsRepository =>
         this.context.GetRepositoryForEntity<ArReqs>();

        /// <summary>
        /// The Term repository.
        /// </summary>
        private IRepository<ArTerm> TermRepository =>
         this.context.GetRepositoryForEntity<ArTerm>();

        /// <summary>
        /// The ClsSectMeetings repository.
        /// </summary>
        private IRepository<ArClsSectMeetings> ClsSectMeetingsRepository =>
         this.context.GetRepositoryForEntity<ArClsSectMeetings>();

        /// <summary>
        /// The ArResults repository.
        /// </summary>
        private IRepository<ArResults> ArResultsRepository =>
         this.context.GetRepositoryForEntity<ArResults>();

        /// <summary>
        /// The ArGradeSystemDetails repository.
        /// </summary>
        private IRepository<ArGradeSystemDetails> ArGradeSystemDetailsRepository =>
         this.context.GetRepositoryForEntity<ArGradeSystemDetails>();

        /// <summary>
        /// The Search action methods returns the list of student(s) for the provided campus and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given campus of type GUID and Search filter of type string
        /// </remarks>
        /// <param name="studentSearchFilter">
        /// The student search filter object includes the campusId and a Filter string which should of minimum 3 letters of the student First name or Last Name or SSN.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of"IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        public async Task<IEnumerable<IListItem<string, StudentSearchOutput>>> Search(StudentSearch studentSearchFilter)
        {
            return await Task.Run(
                       () =>
                           {
                               var studentSearch = this.StudentRepository.Get(s => s.LeadStatusNavigation.SysStatusId == (int)Constants.LeadStatus.Enrolled
                               && s.CampusId == studentSearchFilter.CampusId && s.StudentId != Guid.Empty
                               && (studentSearchFilter.Filter == null || studentSearchFilter.Filter == string.Empty || (s.Ssn.Contains(studentSearchFilter.Filter.Replace("-", "")) || s.FirstName.Contains(studentSearchFilter.Filter)
                                   || s.LastName.Contains(studentSearchFilter.Filter) || s.MiddleName.Contains(studentSearchFilter.Filter) || ((s.LastName ?? "") + (" " + s.FirstName + " ") + (s.MiddleName ?? "")).Contains(studentSearchFilter.Filter)))).ToList();
                               return this.mapper.Map<IEnumerable<IListItem<string, StudentSearchOutput>>>(studentSearch.OrderBy(name => name.LastName));
                           });
        }

        /// <summary>
        /// The Search action methods returns the list of student(s) for the provided campus and a search filter.
        /// </summary>
        /// <remarks>
        /// Search action method allows to search the student(s) for a given campus of type GUID and Search filter of type string
        /// </remarks>
        /// <param name="studentSearchFilter">
        /// The student search filter object includes the campusId and a Filter string which should of minimum 3 letters of the student First name or Last Name or SSN.
        /// </param>
        /// <param name="statuses">
        /// The list of valid statuses for searching a student.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of"IListItem{TText,TValue}"  where TText is of type string and will show you the full name of the student and SSN and TValue is of type Guid and will have the StudentId
        /// </returns>
        public async Task<IEnumerable<IListItem<string, StudentSearchOutput>>> Search(StudentSearch studentSearchFilter, IEnumerable<Constants.SystemStatus> statuses)
        {
            return await Task.Run(
                       () =>
                       {
                           return this.StudentRepository.Get(s => (studentSearchFilter.ShowAll ? true : s.ArStuEnrollments.Any(se => statuses.Contains((Constants.SystemStatus)se.StatusCode.SysStatusId)))
                          && s.CampusId == studentSearchFilter.CampusId && s.StudentId != Guid.Empty
                          && (!studentSearchFilter.ProgramVersionId.HasValue || s.PrgVerId == studentSearchFilter.ProgramVersionId)
                          && (studentSearchFilter.Filter == null || studentSearchFilter.Filter == string.Empty || (s.Ssn.Contains(studentSearchFilter.Filter.Replace("-", "")) || s.FirstName.Contains(studentSearchFilter.Filter)
                              || s.LastName.Contains(studentSearchFilter.Filter) || s.MiddleName.Contains(studentSearchFilter.Filter) || ((s.LastName ?? "") + (" " + s.FirstName + " ") + (s.MiddleName ?? "")).Contains(studentSearchFilter.Filter))))
                               .OrderByDescending(_ => _.LastName)
                               .Select(_ => new ListItem<string, StudentSearchOutput>()
                               {
                                   Text = _.StudentId.ToString(),
                                   Value = new StudentSearchOutput()
                                   {
                                       StudentId = _.StudentId,
                                       Ssn = !string.IsNullOrEmpty(_.Ssn) ? _.Ssn.Replace(_.Ssn.Substring(0, 5), SsnMask.Ssn) : "",
                                       DisplayName = ((_.LastName ?? "") + ", " + _.FirstName + " " + (_.MiddleName ?? "")).TrimEnd(),
                                       Enrollments = _.ArStuEnrollments.Where(se => studentSearchFilter.ShowAll ? true : statuses.Contains((Constants.SystemStatus)se.StatusCode.SysStatusId)).Select(se => se.StuEnrollId).ToList(),

                                   }

                               }).ToList();
                       });
        }

        /// <summary>
        /// The Student Search Filter validations.
        /// </summary>
        /// <returns>
        /// The <see cref="DynamicValidationRuleSet"/>.
        /// </returns>
        public DynamicValidationRuleSet GetStudentSearchValidations()
        {
            var set = new DynamicValidationRuleSet("StudentSearch");
            set.Rules.Add("CampusId", new DynamicValidationRule("CampusId", true, true));
            set.Rules.Add("Filter", new DynamicValidationRule("Filter", true, true, 3, 152));
            return set;
        }

        /// <summary>
        ///  The GetEnrollmentsByStudentId Gets the list of enrollment(s) for the given studentId
        ///  and each enrollment listed is displayed as StudentEnrollment name Status Effective date of the current status'. Example: 'Cosmetology - Enrolled 05/01/2017'
        /// </summary>
        /// <remarks>
        /// The GetEnrollmentsByStudentId action requires studentId object which is a Guid.
        /// </remarks> 
        /// <param name="terminationId">
        /// The studentId.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="StudentInfoForReports"/>.
        /// </returns>
        public StudentInfoForReports GetEnrollmentsByTerminationId(Guid terminationId)
        {
            StudentInfoForReports student = new StudentInfoForReports();
            var terminationDetails = this.TerminationRepository
                .Get(termination => termination.TerminationId == terminationId)
                .Include(se => se.CalculationPeriodType)
                .FirstOrDefault();

            var enrollment =
                this.EnrollmentRepository.Get(enroll => enroll.StuEnrollId == terminationDetails.StuEnrollmentId).FirstOrDefault();
            var studentInfo = this.StudentRepository.Get(e => e.StudentId == enrollment.StudentId).FirstOrDefault();
            if (enrollment != null)
            {
                AcademicCalendarService academic = new AcademicCalendarService(this.context, this.mapper);
                var isClockHour = academic.IsClockHour(enrollment.StuEnrollId);
                student.IsClockHour = isClockHour.Result;
            }

            if (studentInfo != null && enrollment != null && terminationDetails != null)
            {
                student.StudentId = studentInfo.StudentId;
                student.Ssn = Utility.GetMaskedSsn(studentInfo.Ssn);
                student.DisplayName = studentInfo.LastName + ", " + studentInfo.FirstName + " "
                                          + studentInfo.MiddleName;
                student.Dod = Utility.FormatValueByType(terminationDetails.DateWithdrawalDetermined, FieldType.Date);
                student.CaluclationPeriodType = terminationDetails.CalculationPeriodType.Code;
            }

            return student;
        }

        /// <summary>
        /// The get hold status.
        /// </summary>
        /// <param name="studentId">
        /// The student id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Boolean> GetHoldStatus(System.Guid? studentId)
        {

            var hasHold = false;
            return await Task.Run(
                       () =>
                       {
                           var stuGroup = StuGrpStudentsRepository.Get(s => s.StudentId == studentId && s.IsDeleted.HasValue && !s.IsDeleted.Value).FirstOrDefault();

                           if (stuGroup != null)
                           {
                               var temp = StudentGroupsRepository.Get(x => x.StuGrpId == stuGroup.StuGrpId).FirstOrDefault();
                               if (temp != null)
                                   hasHold = true;

                           }

                           return hasHold;

                       });

        }


        /// <summary>
        ///  The GetStudentScheduleDetails Gets the schedule details for the given enrollment id.
        /// </summary>
        /// <remarks>
        /// The GetStudentScheduleDetails action requires enrollment id object which is a Guid.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollmentId.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="StudentScheduleInfo"/>.
        /// </returns>
        public async Task<IEnumerable<StudentScheduleInfo>> GetStudentScheduleDetails(Guid enrollmentId)
        {
            return await Task.Run(
                () =>
                {
                    var studentScheduleInfo = this.StudentSchedulesRepository.Get(e => e.StuEnrollId == enrollmentId);

                    return this.mapper.Map<IEnumerable<StudentScheduleInfo>>(studentScheduleInfo);
                });
        }

        /// <summary>
        /// The get student contact info.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<StudentContactInfo> GetStudentContactInfo(Guid enrollmentId)
        {
            return await Task.Run(() =>
                {
                    var contactInfo = new StudentContactInfo();

                    var studentId = this.EnrollmentRepository
                        .Get(e => e.StuEnrollId == enrollmentId)
                        .Select(e => e.StudentId)
                        .FirstOrDefault();


                    if (studentId != Guid.Empty)
                    {
                        var leadId = this.StudentRepository
                            .Get(s => s.StudentId == studentId)
                            .Select(s => s.LeadId)
                            .FirstOrDefault();

                        if (leadId != Guid.Empty)
                        {
                            var activeStatusCodeId = this.statusService.GetActiveStatusId();
                            var address = this.StudentAdressesRepository
                                .Get(a => a.LeadId == leadId && a.IsShowOnLeadPage == true && a.IsMailingAddress == true && a.StatusId.Equals(activeStatusCodeId.Result))
                                .FirstOrDefault();
                            if (address != null)
                            {
                                contactInfo.Address = address.Address1?.Trim();
                                contactInfo.Address2 = address.Address2?.Trim();
                                contactInfo.City = address.City?.Trim();
                                contactInfo.State = address.State?.Trim();
                                contactInfo.Zip = address.ZipCode?.Trim();
                            }

                            var phone = this.StudentPhoneRepository
                                .Get(a => a.LeadId == leadId && a.IsBest)
                                .FirstOrDefault();

                            if (phone != null)
                            {
                                contactInfo.Phone = phone.Phone?.Trim();
                            }

                            var phoneOther = this.StudentPhoneRepository
                                .Get(a => a.LeadId == leadId && !a.IsBest && a.Phone != (contactInfo.Phone ?? string.Empty)).OrderBy(x => x.Position)
                                .FirstOrDefault();

                            if (phoneOther != null)
                            {
                                contactInfo.PhoneOther = phoneOther.Phone?.Trim();
                            }

                            var email = this.StudentEmailRepository
                                .Get(a => a.LeadId == leadId && a.IsPreferred)
                                .FirstOrDefault();

                            if (email != null)
                            {
                                contactInfo.Email = email.Email?.Trim();
                            }
                        }
                    }

                    return contactInfo;
                });

        }


        /// <summary>
        /// The get Student Class Sections.
        /// </summary>
        /// <param name="stuEnrollId">
        ///  The stuEnrollId.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<List<StudentClassSectionsAndTerm>>> GetStudentClassSections(Guid stuEnrollId)
        {
            return await Task.Run(
                       async () =>
                       {

                           var result = new ActionResult<List<StudentClassSectionsAndTerm>>();
                           try
                           {
                               var Term = this.TermRepository.Get();
                               var ClsSectMeetings = this.ClsSectMeetingsRepository.Get();
                               var ResultsClsSectionIdList = this.ArResultsRepository.Get(x => x.StuEnrollId == stuEnrollId).Select(y => y.TestId).ToList();

                               var SectionsAndTerm = this.ClassSectionsRepository.Get(y => ResultsClsSectionIdList.Contains(y.ClsSectionId)).Include(x => x.Req)
                               .Select(a => new StudentClassSectionsAndTerm()
                               {
                                   Name = a.Req.Descrip,
                                   Id = a.ClsSectionId,
                                   Code = (a.Req.Code != null) ? a.Req.Code.Trim() : a.Req.Code,
                                   StartDate = a.StartDate,
                                   EndDate = a.EndDate,
                                   InstructorId = a.InstructorId != null ? a.InstructorId : Guid.Empty,
                                   PeriodId = ClsSectMeetings.Where(x => a.ClsSectionId == x.ClsSectionId).Select(x => x.PeriodId).DefaultIfEmpty().ToList(),
                                   CourseId = a.Req.ReqId,
                                   TermName = Term.Where(x => x.TermId == a.TermId).Select(x => x.TermDescrip).FirstOrDefault(),
                                   TermId = a.TermId,
                                   TermStartDate = Term.Where(x => x.TermId == a.TermId).Select(x => x.StartDate).FirstOrDefault(),
                                   TermEndDate = Term.Where(x => x.TermId == a.TermId).Select(x => x.EndDate).FirstOrDefault(),

                               }).ToList();

                               result.Result = SectionsAndTerm;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        /// <summary>
        /// The get Student Class Sections.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The stuEnrollId.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<List<StudentRegisteredCourses>>> GetStudentRegisteredCourses(Guid stuEnrollId)
        {
            return await Task.Run(
                       async () =>
                       {

                           var result = new ActionResult<List<StudentRegisteredCourses>>();
                           var grade = new List<StudentRegisteredCourses>();
                           var campusId = this.EnrollmentRepository.Get(se => se.StuEnrollId == stuEnrollId).Select(x => x.CampusId).FirstOrDefault();

                           try
                           {

                               string gradesFormat = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                                  ConfigurationAppSettingsKeys.GradesFormat,
                                   campusId);

                               // for letter schools 
                               if (gradesFormat.ToLower() == "letter")
                               {
                                   var gradeSystemDetails = this.ArGradeSystemDetailsRepository.Get();
                                   grade = this.ArResultsRepository.Get(x => x.StuEnrollId == stuEnrollId)
                                  .Select(a => new StudentRegisteredCourses()
                                  {
                                      TestId = a.TestId,
                                      Grade = a.GrdSysDetailId.HasValue ? gradeSystemDetails.Where(g => g.GrdSysDetailId == a.GrdSysDetailId).Select(s => s.Grade).FirstOrDefault().ToString() : "null"

                                  }).ToList();
                               }
                               else
                               { // for score school 
                                   grade = this.ArResultsRepository.Get(x => x.StuEnrollId == stuEnrollId)
                                  .Select(a => new StudentRegisteredCourses()
                                  {
                                      TestId = a.TestId,
                                      Grade = ((decimal?)a.Score).ToString()

                                  }).ToList();
                               }

                               var arResultsRecId = this.ArResultsRepository.Get(x => x.StuEnrollId == stuEnrollId).Select(y => y.TestId).ToList();
                               var RegisteredCourses = this.ClassSectionsRepository.Get(y => arResultsRecId.Contains(y.ClsSectionId)).Include(x => x.Req)
                               .Select(a => new StudentRegisteredCourses()
                               {
                                   Name = a.Req.Descrip,
                                   Id = a.Req.ReqId,
                                   Code = (a.Req.Code != null) ? a.Req.Code.Trim() : a.Req.Code,
                                   Grade = grade.Where(r => r.TestId == a.ClsSectionId).Select(s => s.Grade).FirstOrDefault(),
                                   TestId = a.ClsSectionId
                               }).ToList();

                               result.Result = RegisteredCourses;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.ResultStatusMessage = string.Empty;
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }
    }
}