﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionAttendanceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The class section attendance service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.Validation.Constants;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using Microsoft.ApplicationInsights;
    using Unity.Interception.Utilities;

    /// <inheritdoc />
    /// <summary>
    /// The class section attendance service.
    /// </summary>
    public class ClassSectionAttendanceService : IClassSectionAttendanceService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The class section meeting service.
        /// </summary>
        private readonly IClassSectionMeetingService classSectionMeetingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionAttendanceService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="classSectionMeetingService">
        /// The class Section Meeting Service.
        /// </param>
        public ClassSectionAttendanceService(IAdvantageDbContext context, IClassSectionMeetingService classSectionMeetingService)
        {
            this.context = context;
            this.classSectionMeetingService = classSectionMeetingService;
        }

        /// <summary>
        /// The class section attendance repository.
        /// </summary>
        private IRepository<AtClsSectAttendance> ClassSectionAttendanceRepository => this.context.GetRepositoryForEntity<AtClsSectAttendance>();

        /// <summary>
        /// The get attendance by enrollment and date range service method returns the attendance of student based on given the enrollment id and date range.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="starDateTime">
        /// The star date time.
        /// </param>
        /// <param name="endDateTime">
        /// The end date time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<StudentAttendanceDetails> GetAttendanceByEnrollmentAndDateRange(Guid enrollmentId, DateTime? starDateTime, DateTime? endDateTime)
        {
            return await Task.Run(
                       async () =>
                       {
                           var studentAttendanceDetails = new StudentAttendanceDetails();
                           var actualAttendanceByDate = new List<AttendanceWithDate>();
                           var excusedAttendanceByDate = new List<AttendanceWithDate>();

                           try
                           {
                               var classSectionAttendance = this.ClassSectionAttendanceRepository.Get(x => x.StuEnrollId == enrollmentId
                                                                                                              && x.MeetDate >= starDateTime
                                                                                                              && x.MeetDate <= endDateTime);

                               var actualAttendance = classSectionAttendance.Where(x => x.Actual != ConstantDecimals.AttendanceUpperLimit).OrderBy(x => x.MeetDate);
                               var excusedAbsence = classSectionAttendance.Where(x => x.Excused && x.Actual != 1).OrderBy(x => x.MeetDate);

                               var clsMeetingIds = actualAttendance.Where(x => x.ClsSectMeetingId != null).Select(x => x.ClsSectMeetingId).Distinct().ToList();
                               clsMeetingIds = clsMeetingIds.Concat(excusedAbsence.Where(x => x.ClsSectMeetingId != null).Select(x => x.ClsSectMeetingId)).Distinct().ToList();

                               var lstTimeIntervalsByMeetingIds = await this.classSectionMeetingService.GetTimeIntervals(clsMeetingIds);
                               lstTimeIntervalsByMeetingIds = lstTimeIntervalsByMeetingIds.ToList();

                               if (actualAttendance.Any())
                               {
                                   var actualWithSchedules = actualAttendance.Where(
                                           x => x.ClsSectMeetingId != null)
                                       .ToList();
                                   if (!actualWithSchedules.Any())
                                   {
                                       actualWithSchedules.ForEach(
                                           attendance =>
                                               {
                                                   var timeIntervalsByMeeting =
                                                       lstTimeIntervalsByMeetingIds.FirstOrDefault(
                                                           x => x.Text == attendance.ClsSectMeetingId);
                                                   if (timeIntervalsByMeeting != null)
                                                   {
                                                       actualAttendanceByDate.Add(new AttendanceWithDate
                                                                                      {
                                                                                          AttendanceValue = timeIntervalsByMeeting.Value,
                                                                                          MeetingDate = attendance.MeetDate,
                                                                                          MeetingId = timeIntervalsByMeeting.Text
                                                                                      });
                                                   } 
                                               }); 
                                   }
                                   else
                                   {
                                       actualWithSchedules.ForEach(x =>
                                       {
                                           if (x.ClsSectMeetingId != null)
                                           {
                                               actualAttendanceByDate.Add(new AttendanceWithDate
                                               { 
                                                   MeetingDate = x.MeetDate,
                                                   MeetingId = x.ClsSectMeetingId ?? Guid.Empty,
                                                   ClassSectionId = x.ClsSectionId,
                                                   AttendanceValue = x.Actual / 60
                                               });
                                           }
                                       });
                                   }
                               }

                               if (excusedAbsence.Any())
                               {
                                   var excusedAbsenceWithSchedule = excusedAbsence.Where(
                                           x => Convert.ToDecimal(x.Scheduled) > 0 && x.ClsSectMeetingId != null)
                                       .ToList();  

                                   if (excusedAbsenceWithSchedule.Any())
                                   {
                                       excusedAbsenceWithSchedule.ForEach(x =>
                                       {
                                           if (x.ClsSectMeetingId != null)
                                           {
                                               var timeIntervalsByMeeting = lstTimeIntervalsByMeetingIds.FirstOrDefault(
                                                   timeInterval => timeInterval.Text == x.ClsSectMeetingId);

                                               if (timeIntervalsByMeeting != null)
                                               {
                                                   excusedAttendanceByDate.Add(new AttendanceWithDate
                                                                                   {
                                                                                       MeetingId = x.ClsSectMeetingId ?? Guid.Empty,
                                                                                       MeetingDate = x.MeetDate,
                                                                                       AttendanceValue = timeIntervalsByMeeting.Value - (x.Actual / 60)
                                                                                   });
                                               }
                                           }
                                       });
                                   }
                                   else
                                   {
                                       lstTimeIntervalsByMeetingIds.ForEach(
                                           timeIntervalsByMeeting =>
                                           {
                                               var absenceDetails = excusedAbsence.FirstOrDefault(x => x.ClsSectMeetingId == timeIntervalsByMeeting.Text);
                                               excusedAttendanceByDate.Add(new AttendanceWithDate
                                               {
                                                   MeetingId = timeIntervalsByMeeting.Text,
                                                   MeetingDate = absenceDetails?.MeetDate ?? DateTime.MinValue,
                                                   AttendanceValue = timeIntervalsByMeeting.Value - absenceDetails?.Actual ?? 0m
                                               });
                                           });
                                   }
                               }

                               studentAttendanceDetails.ActualAttendanceList = actualAttendanceByDate;
                               studentAttendanceDetails.ExcusedAbsenceList = excusedAttendanceByDate;
                               studentAttendanceDetails.ResultStatus = ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS;
                           }
                           catch(Exception e)
                           {
                               e.TrackException();
                               studentAttendanceDetails.ActualAttendanceList = null;
                               studentAttendanceDetails.ExcusedAbsenceList = null;
                               studentAttendanceDetails.ResultStatus = $"{ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS}";
                           }

                           return studentAttendanceDetails;
                       });
        }

        /// <inheritdoc />
        /// <summary>
        /// The get scheduled hours by enrollment and date range.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="withdrawalDate">
        /// The withdrawal date.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        /// <exception cref="T:System.NotImplementedException">
        /// </exception>
        public async Task<decimal> GetScheduledHoursByEnrollmentAndDateRange(
            Guid enrollmentId,
            DateTime startDate,
            DateTime? withdrawalDate)
        {
            return await Task.Run(
                       async () =>
                           {
                               var scheduleHours = 0m; 
                               var classSectionAttendance = this.ClassSectionAttendanceRepository.Get(x => x.StuEnrollId == enrollmentId
                                                                                                           && x.MeetDate >= startDate
                                                                                                           && x.MeetDate <= withdrawalDate
                                                                                                           && Convert.ToDecimal(x.Scheduled) != 0m);

                               var classSectionMeetingIds = classSectionAttendance.Select(x => x.ClsSectMeetingId).Distinct().ToList();
                               var lstTimeIntervalsByMeetingId = await this.classSectionMeetingService.GetTimeIntervals(classSectionMeetingIds);
                               classSectionAttendance.ForEach(
                                   attendance =>
                                       {
                                           var timeInterval = lstTimeIntervalsByMeetingId.FirstOrDefault(
                                               x => x.Text == attendance.ClsSectMeetingId);
                                           
                                           if (timeInterval != null)
                                           {
                                               scheduleHours += timeInterval.Value;
                                           }
                                       });
                                
                               return scheduleHours;
                           });
        }

        /// <summary>
        /// The GetLastDateAttended returns last date attended from attendance table.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The last date attended.
        /// </returns>
        public async Task<DateTime> GetLastAttendanceDate(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           { 
                               var classSectionAttendance = this.ClassSectionAttendanceRepository.Get(x => 
                                   x.StuEnrollId == enrollmentId 
                                   && Convert.ToDecimal(x.Scheduled) != ConstantDecimals.ZeroDecimal
                                   && Convert.ToDecimal(x.Scheduled) != ConstantDecimals.AttendanceUpperLimit).OrderByDescending(x => x.MeetDate);

                               return classSectionAttendance.FirstOrDefault()?.MeetDate ?? DateTime.MinValue; 
                           });
        }
        /// <summary>
        /// Delete the records from the table
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        public async Task<string> Delete(Guid stuEnrollmentId)
        {
            return await Task.Run(
                () =>
                {
                    try
                    {
                        var studentClassAttendance =
                            this.ClassSectionAttendanceRepository.Get(x => x.StuEnrollId == stuEnrollmentId);
                        if (studentClassAttendance != null)
                        {
                            this.ClassSectionAttendanceRepository.Delete(studentClassAttendance);
                            this.context.SaveChanges();
                            return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                        }
                        else
                        {
                            return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                        }
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return $"{ApiMsgs.DELETE_FAILED} {stuEnrollmentId}";
                    }
                });
        }
    }
}