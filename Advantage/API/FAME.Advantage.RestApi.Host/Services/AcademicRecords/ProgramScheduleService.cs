﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;
     
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    using Interfaces;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The program schedule service.
    /// </summary>
    public class ProgramScheduleService : IProgramScheduleService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The student service.
        /// </summary>
        private readonly IStudentService studentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramScheduleService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="studentService">
        /// the student Service
        /// </param>
        public ProgramScheduleService(IAdvantageDbContext context, IMapper mapper, IStudentService studentService)
        {
            this.context = context;
            this.mapper = mapper;
            this.studentService = studentService;
        }

        /// <summary>
        /// The program schedule repository.
        /// </summary>
        private IRepository<ArProgSchedules> ProgramScheduleRepository => this.context.GetRepositoryForEntity<ArProgSchedules>();

        /// <summary>
        /// The GetDetailByProgramVersionId returns the matching program schedule based on the given programVersionId .
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<IEnumerable<ProgramSchedule>> GetDetailByProgramVersionId(Guid programVersionId)
        {
            return await Task.Run(
                       () =>
                           {
                               var programSchedules = this.ProgramScheduleRepository
                                   .Get(x => x.PrgVerId == programVersionId).Include(x => x.ArProgScheduleDetails);

                               var programScheduleList = this.mapper.Map<IEnumerable<ProgramSchedule>>(programSchedules).ToList(); 
                               if (programScheduleList.Any())
                               { 
                                   programScheduleList.ForEach(
                                       programSchedule =>
                                           {
                                               var programScheduleDetails = programSchedules.FirstOrDefault(
                                                   x => x.ScheduleId == programSchedule.ScheduleId);
                                               if (programScheduleDetails != null)
                                               {
                                                   programSchedule.ProgramScheduleDetails = this.mapper.Map<IEnumerable<ProgramScheduleDetails>>(programScheduleDetails.ArProgScheduleDetails).ToList();
                                               }
                                           }); 
                               }
                               
                               return programScheduleList;
                           });
        } 

        /// <summary>
        /// The GetScheduleHoursAndWeekDayForDateRange returns the list of scheduled hour and day  between given start and withdrawal date by program version id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The Schedule hours and day for week 
        /// </returns>
        public async Task<IEnumerable<IListItem<int, decimal>>> GetScheduleHoursAndWeekDayForDateRange(
            Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var daysWithScheduledHoursDictionary = new List<IListItem<int, decimal>>();
                           var programSchedules = await this.GetDetailByEnrollmentId(enrollmentId);
                           programSchedules = programSchedules.ToList();

                           if (programSchedules.Any() && programSchedules.FirstOrDefault() != null)
                           {
                               var programSchedulesDetails = programSchedules.FirstOrDefault()?.ProgramScheduleDetails;

                               programSchedulesDetails?.ForEach(
                                   x =>
                                   {
                                       if (x.DayOfWeek != null)
                                       {
                                           daysWithScheduledHoursDictionary.Add(new ListItem<int, decimal>()
                                           {
                                               Text = Convert.ToInt32(x.DayOfWeek),
                                               Value = x.Total ?? 0m
                                           });
                                       }
                                   });
                           }

                           return daysWithScheduledHoursDictionary;
                       });
        }

        /// <summary>
        /// The GetScheduleWeekDayForEnrollmentId returns the list of scheduled hour and day  between given start and withdrawal date by program version id.
        /// </summary>
        /// <param name="prgVerId">
        /// The program version id.
        /// </param>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The Schedule hours and day for week 
        /// </returns>
        public async Task<IEnumerable<IListItem<int, decimal>>> GetScheduleWeekDayForEnrollmentId(Guid prgVerId, Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var daysWithScheduledHoursDictionary = new List<IListItem<int, decimal>>();
                           var programSchedules = await this.GetDetailByProgramVersionId(prgVerId);
                           programSchedules = programSchedules.ToList();

                           var studentSchedules = await this.studentService.GetStudentScheduleDetails(enrollmentId);

                           var scheduleIds = studentSchedules.Select(x => x.ScheduleId).FirstOrDefault();

                           programSchedules = programSchedules.Where(p => p.ScheduleId == scheduleIds).ToList();

                           if (programSchedules.Any() && programSchedules.FirstOrDefault() != null)
                           {
                               var programSchedulesDetails = programSchedules.FirstOrDefault()?.ProgramScheduleDetails;

                               programSchedulesDetails?.ForEach(
                                   x =>
                                   {
                                       if (x.DayOfWeek != null)
                                       {
                                           daysWithScheduledHoursDictionary.Add(new ListItem<int, decimal>()
                                           {
                                               Text = Convert.ToInt32(x.DayOfWeek),
                                               Value = x.Total ?? 0m
                                           });
                                       }
                                   });
                           }

                           return daysWithScheduledHoursDictionary;
                       });
        }

        /// <summary>
        /// The GetDetailByEnrollmentId returns the matching program schedule based on the given enrollmentId .
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment Id.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        private async Task<IEnumerable<ProgramSchedule>> GetDetailByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var studentSchedules = await this.studentService.GetStudentScheduleDetails(enrollmentId);
                           studentSchedules = studentSchedules.ToList();
                           if (studentSchedules.Any())
                           {
                               var programSchedules = this.ProgramScheduleRepository
                                   .Get(x => x.ScheduleId == studentSchedules.FirstOrDefault().ScheduleId).Include(x => x.ArProgScheduleDetails);

                               var programScheduleList = this.mapper.Map<IEnumerable<ProgramSchedule>>(programSchedules).ToList();
                               if (programScheduleList.Any())
                               {
                                   programScheduleList.ForEach(
                                       programSchedule =>
                                       {
                                           var programScheduleDetails = programSchedules.FirstOrDefault(
                                               x => x.ScheduleId == programSchedule.ScheduleId);
                                           if (programScheduleDetails != null)
                                           {
                                               programSchedule.ProgramScheduleDetails = this.mapper.Map<IEnumerable<ProgramScheduleDetails>>(programScheduleDetails.ArProgScheduleDetails).ToList();
                                           }
                                       });
                               }

                               return programScheduleList;
                           }

                           return null;
                       });
        }
    }
}
