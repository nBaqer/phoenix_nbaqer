﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClockAttendanceService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student clock attendance service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    /// <inheritdoc />
    /// <summary>
    /// The student clock attendance service.
    /// </summary>
    public class StudentClockAttendanceService : IStudentClockAttendanceService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentClockAttendanceService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public StudentClockAttendanceService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The student clock attendance repository.
        /// </summary>
        private IRepository<ArStudentClockAttendance> StudentClockAttendanceRepository => this.context.GetRepositoryForEntity<ArStudentClockAttendance>();

        /// <summary>
        /// The GetDetailsByEnrollmentId returns matching student clock attendance based on the given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<StudentClockAttendance>> GetDetailsByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var studentClockAttendance = this.StudentClockAttendanceRepository.Get(x => x.StuEnrollId == enrollmentId); 

                               return this.mapper.Map<IEnumerable<StudentClockAttendance>>(studentClockAttendance);
                           });
        }

        /// <summary>
        /// The GetDetailsByScheduleId returns matching student clock attendance based on the given schedule id.
        /// </summary>
        /// <param name="scheduleId">
        /// The schedule Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<StudentClockAttendance>> GetDetailsByScheduleId(Guid scheduleId)
        {
            return await Task.Run(
                       () =>
                           {
                               var studentClockAttendance = this.StudentClockAttendanceRepository.Get(x => x.ScheduleId == scheduleId);

                               return this.mapper.Map<IEnumerable<StudentClockAttendance>>(studentClockAttendance);
                           });
        }

        /// <summary>
        /// The GetLastDateAttended returns last date attended from attendance table.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The last date attended.
        /// </returns>
        public async Task<DateTime> GetLastAttendanceDate(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var studentClockAttendance = this.StudentClockAttendanceRepository.Get(x =>
                                   x.StuEnrollId == enrollmentId
                                   && Convert.ToDecimal(x.SchedHours) != ConstantDecimals.ZeroDecimal
                                   && Convert.ToDecimal(x.SchedHours) != ConstantDecimals.AttendanceUpperLimit).OrderByDescending(x => x.RecordDate);

                               return studentClockAttendance.FirstOrDefault()?.RecordDate ?? DateTime.MinValue;
                           });
        }
        /// <summary>
        /// Delete the records from the table
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        public async Task<string> Delete(Guid stuEnrollmentId)
        {
            return await Task.Run(
                () =>
                {
                    try
                    {
                        var studentClkAttendance =
                            this.StudentClockAttendanceRepository.Get(x => x.StuEnrollId == stuEnrollmentId);
                        if (studentClkAttendance != null)
                        {
                            this.StudentClockAttendanceRepository.Delete(studentClkAttendance);
                            this.context.SaveChanges();
                            return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                        }
                        else
                        {
                            return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                        }
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return $"{ApiMsgs.DELETE_FAILED} {stuEnrollmentId}";
                    }
                });
        }
    }
}
