﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CourseDetailsService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CourseDetailsService Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    /// <summary>
    /// The course details service.
    /// </summary>
    public class CourseDetailsService : ICourseDetailsService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CourseDetailsService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public CourseDetailsService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The course details repository.
        /// </summary>
        private IRepository<ArReqs> CourseDetailsRepository => this.context.GetRepositoryForEntity<ArReqs>();

        /// <summary>
        /// The GetTransferCreditsByReqId method returns ReqId,CourseId,Code,Descrip,StatusId,ReqTypeId,Credits,FinAidCredits,Hours,Cnt,GrdLvlId,
        /// UnitTypeId,CampGrpId,CourseCatalog,CourseComments,ModDate,ModUser,Su,Pf,CourseCategoryId,TrackTardies,TardiesMakingAbsence,DeptId,MinDate
        /// IsComCourse,IsOnLine,CompletedDate,IsExternship,UseTimeClock,IsAttendanceOnly,AllowCompletedCourseRetake based on the parameter reqID.
        /// </summary>
        /// <param name="reqId">
        /// The req id is Guid.
        /// </param>
        /// <returns>
        /// The TransferCredits.
        /// </returns>
        public async Task<CourseDetails> GetTransferCreditsByReqId(Guid reqId)
        {
            return await Task.Run(() =>
                {
                    var transferCreditsDetails = this.CourseDetailsRepository.Get(s => s.ReqId == reqId).FirstOrDefault();
                    return this.mapper.Map<CourseDetails>(transferCreditsDetails);
                });
        }

        /// <summary>
        /// The GetCourseDetailsByReqIds method returns all the course details by given ReqIds.
        /// </summary>
        /// <param name="reqIds">
        /// The req ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<CourseDetails>> GetCourseDetailsByReqIds(IList<Guid> reqIds)
        {
            return await Task.Run(() =>
                {
                    var courseDetails = this.CourseDetailsRepository.Get(s => reqIds.Contains(s.ReqId));
                    return this.mapper.Map<IEnumerable<CourseDetails>>(courseDetails);
                });
        } 
    }
}
