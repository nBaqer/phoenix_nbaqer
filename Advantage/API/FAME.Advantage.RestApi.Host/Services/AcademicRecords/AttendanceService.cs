﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttendanceService.cs" company="">
//   
// </copyright>
// <summary>
//   The attendance service
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Extensions;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ScheduledHours;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Security;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Storage;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using Microsoft.AspNetCore.Http;
    using Microsoft.EntityFrameworkCore;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The attendance service
    /// </summary>
    public class AttendanceService : IAttendanceService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService appSettingService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The holiday service.
        /// </summary>
        private readonly IHolidayService holidayService;

        /// <summary>
        /// The status service.
        /// </summary>
        private IStatusesService statusService;

        /// <summary>
        /// The status service.
        /// </summary>
        private IUserService userService;

        /// <summary>
        /// The tenant service.
        /// </summary>
        private readonly ITenantService tenantService;

        /// <summary>
        /// The graduationCalculatorService service.
        /// </summary>
        private IGraduationCalculatorService graduationCalculatorService;

        /// <summary>
        /// The storage service.
        /// </summary>
        private readonly IStorageService storageService;

        /// <summary>
        /// The student attendance summary repository.
        /// </summary>
        private IRepository<SyStudentAttendanceSummary> StudentAttendanceSummaryRepository => this.context.GetRepositoryForEntity<SyStudentAttendanceSummary>();

        /// <summary>
        /// The student enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The student schedule repository.
        /// </summary>
        private IRepository<ArStudentSchedules> StudentScheduleRepository => this.context.GetRepositoryForEntity<ArStudentSchedules>();

        /// <summary>
        /// The student clock attendance repository
        /// </summary>
        private IRepository<ArStudentClockAttendance> StudentClockAttendanceRepository => this.context.GetRepositoryForEntity<ArStudentClockAttendance>();

        /// <summary>
        /// The request accessor
        /// </summary>
        private IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        /// Initializes a new instance of the <see cref="AttendanceService"/> class. 
        /// Instantiates a new instance of the <see cref="AttendanceService"/>
        /// </summary>
        /// <param name="context">
        /// The advantage db context
        /// </param>
        /// <param name="appSettingService">
        /// The app setting service
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="holidayService">
        /// The status Service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        /// <param name="tenantService">
        /// The enrollment Service.
        /// </param>
        /// <param name="httpContextAccessor">
        /// The httpContextAccessor.
        /// </param>
        /// <param name="storageService">
        /// The file storage service.
        /// </param>
        /// <param name="graduationCalculatorService">
        /// The graduation calculator service.
        /// </param>
        public AttendanceService(IAdvantageDbContext context, ISystemConfigurationAppSettingService appSettingService, IStatusesService statusService, IHolidayService holidayService, IEnrollmentService enrollmentService, ITenantService tenantService, IHttpContextAccessor httpContextAccessor, IStorageService storageService, IGraduationCalculatorService graduationCalculatorService)
        {
            this.context = context;
            this.appSettingService = appSettingService;
            this.statusService = statusService;
            this.holidayService = holidayService;
            this.enrollmentService = enrollmentService;
            this.tenantService = tenantService;
            this._httpContextAccessor = httpContextAccessor;
            this.storageService = storageService;
            this.graduationCalculatorService = graduationCalculatorService;
        }




        /// <summary>
        /// Calculate attendance for a student enrollment based on track attendance method
        /// and unit type description of program. If recalculate flag is passed as false, then only the latest attendance that is currently
        /// stored in the database is returned.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id
        /// </param>
        /// <param name="recalculateAttendance">
        /// The recalculate attendance 
        /// </param>
        /// <param name="getScheduledHoursPerWeek">
        /// The get schedule hours per week
        /// </param>
        /// <param name="calculateScheduledHoursToLastDay">
        /// The calculate schedule hours to last day
        /// </param>
        /// <returns><see cref="Attendance"/>
        /// </returns>
        public async Task<EnrollmentAttendance> CalculateAttendance(Guid studentEnrollmentId, bool recalculateAttendance = true, bool getScheduledHoursPerWeek = false, bool calculateScheduledHoursToLastDay = false)
        {
            ArStuEnrollments enrollment = this.StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId)
                .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                .Include(se => se.AtClsSectAttendance).Include(se => se.ArStudentSchedules).ThenInclude(ss => ss.Schedule).ThenInclude(sch => sch.ArProgScheduleDetails).FirstOrDefault();

            if (enrollment != null)
            {
                string trackSapAttendance = this.appSettingService.GetAppSettingValueByCampus(
                     FAME.Advantage.RestApi.Host.Infrastructure.ConfigurationAppSettingsKeys.TrackSapAttendance,
                     enrollment.CampusId).Trim().ToLowerInvariant();

                string unitType = enrollment.PrgVer.UnitType.UnitTypeDescrip.Trim();

                if (!string.IsNullOrEmpty(trackSapAttendance) && !string.IsNullOrEmpty(unitType))
                {
                    string sProc = "";
                    bool makeupIncluded = false;
                    bool inHours = false;
                    if (trackSapAttendance == TrackSapAttendance.ByClass)
                    {
                        switch (unitType)
                        {
                            case AttendanceUnitTypes.PresentAbsent:
                            case AttendanceUnitTypes.None:
                                sProc = "[dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]";
                                makeupIncluded = false;
                                inHours = false;
                                break;
                            case AttendanceUnitTypes.Minutes:
                                sProc = "[dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]";
                                makeupIncluded = false;
                                inHours = false;
                                break;
                            case AttendanceUnitTypes.ClockHours:
                                sProc = "[dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]";
                                makeupIncluded = false;
                                inHours = true;
                                break;

                        }
                    }
                    else if (trackSapAttendance == TrackSapAttendance.ByDay)
                    {
                        switch (unitType)
                        {
                            case AttendanceUnitTypes.Minutes:
                                sProc = "[dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]";
                                makeupIncluded = true;
                                inHours = true;
                                break;
                            case AttendanceUnitTypes.PresentAbsent:
                            case AttendanceUnitTypes.ClockHours:
                                sProc = "[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]";
                                makeupIncluded = false;
                                inHours = true;
                                break;

                        }
                    }

                    if (!string.IsNullOrEmpty(sProc) && recalculateAttendance)
                    {

                        System.Data.Common.DbCommand attendanceCommand = await this.context
                            .LoadStoredProc(sProc)
                            .WithSqlParam("@StuEnrollIdList", studentEnrollmentId.ToString(), DbType.String)
                            .ExecuteStoredProcAsync();
                        attendanceCommand.Close();
                    }

                    IQueryable<Attendance> attendanceSummary = this.StudentAttendanceSummaryRepository.Get(sas => sas.StuEnrollId == studentEnrollmentId).OrderBy(sas => sas.StudentAttendedDate.Value).Select(sas => new Attendance()
                    {
                        ClassSectionId = sas.ClsSectionId,
                        Scheduled = sas.ActualRunningScheduledDays.GetValueOrDefault(0),
                        Actual = sas.ActualRunningPresentDays.GetValueOrDefault(0),
                        Tardy = sas.ActualRunningTardyDays.GetValueOrDefault(0),
                        Absent = sas.ActualRunningAbsentDays.GetValueOrDefault(0),
                        Makeup = sas.ActualRunningMakeupDays.GetValueOrDefault(0),
                        MakeupIncluded = makeupIncluded,
                        InHours = inHours,
                        AttendedDate = sas.StudentAttendedDate.Value
                    });

                    if (trackSapAttendance == TrackSapAttendance.ByClass)
                    {

                        if (unitType == AttendanceUnitTypes.PresentAbsent || unitType == AttendanceUnitTypes.None)
                        {
                            IEnumerable<Guid?> classSectionIds = enrollment.AtClsSectAttendance.Select(csa => csa.ClsSectMeetingId).Distinct();

                            var clsSectionIntervalMinutes = (from csm in context.GetRepositoryForEntity<ArClsSectMeetings>().Get(csm => classSectionIds.Contains(csm.ClsSectionId))
                                                             join per in context.GetRepositoryForEntity<SyPeriods>().Get() on csm.PeriodId equals per.PeriodId
                                                             join startTimes in context.GetRepositoryForEntity<CmTimeInterval>().Get() on per.StartTimeId equals startTimes.TimeIntervalId
                                                             join endTimes in context.GetRepositoryForEntity<CmTimeInterval>().Get() on per.EndTimeId equals endTimes.TimeIntervalId
                                                             select new { csm.ClsSectionId, Minutes = (startTimes.TimeIntervalDescrip.GetValueOrDefault() - endTimes.TimeIntervalDescrip.GetValueOrDefault()).Minutes });


                            attendanceSummary = from asumm in attendanceSummary
                                                join csim in clsSectionIntervalMinutes on asumm.ClassSectionId equals csim.ClsSectionId
                                                select new Attendance()
                                                {
                                                    Scheduled = asumm.Scheduled * csim.Minutes,
                                                    Actual = asumm.Actual * csim.Minutes,
                                                    Tardy = asumm.Tardy * csim.Minutes,
                                                    Absent = asumm.Absent * csim.Minutes,
                                                    Makeup = asumm.Makeup * csim.Minutes,
                                                    MakeupIncluded = asumm.MakeupIncluded,
                                                    InHours = asumm.InHours,
                                                    AttendedDate = asumm.AttendedDate
                                                };
                        }
                    }
                    decimal scheduledHoursPerWeek = 0;

                    if (getScheduledHoursPerWeek)
                    {
                        ArStudentSchedules studentSchedule = enrollment.ArStudentSchedules.FirstOrDefault(sch => sch.Active.HasValue && sch.Active.Value);
                        if (studentSchedule != null && studentSchedule.Schedule != null && studentSchedule.Schedule.ArProgScheduleDetails.Any())
                        {
                            scheduledHoursPerWeek = studentSchedule.Schedule.ArProgScheduleDetails.Select(sd => sd.Total.GetValueOrDefault(0)).DefaultIfEmpty(0).Sum();
                        }
                    }
                    decimal scheduledHoursLastDay = 0;

                    DateTime? lastDateOfAttendance = await this.enrollmentService.GetLDA(studentEnrollmentId);

                    if (calculateScheduledHoursToLastDay && lastDateOfAttendance.HasValue && enrollment.StartDate.HasValue)
                    {
                        scheduledHoursLastDay = this.CalculateScheduledHoursToLastDay(enrollment.StuEnrollId, lastDateOfAttendance.Value, enrollment.StartDate.Value );
                    }
                    return new EnrollmentAttendance()
                    {
                        StudentEnrollmentId = studentEnrollmentId,
                        AttendanceSummary = attendanceSummary.ToList(),
                        MakeupIncluded = makeupIncluded,
                        InHours = inHours,
                        LastDateAttended = lastDateOfAttendance,
                        ScheduledHoursPerWeek = scheduledHoursPerWeek,
                        ScheduledHoursLastDate = scheduledHoursLastDay,
                        TransferHours = enrollment.TransferHours ?? 0
                    };
                }
            }

            return null;
        }


        /// <summary>
        /// Get students who are scheduled to attend on a given date
        /// </summary>
        /// <param name="campusId">
        /// The campus Id
        /// </param>
        /// <param name="scheduledDay">
        /// The scheduled datetime
        /// </param>
        /// <param name="studentGroupId">
        /// The student group Id
        /// </param>
        /// <param name="programVersionId">
        /// The program version Id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<IEnumerable<ScheduledHourAdjustment>> GetStudentsScheduledForDay(Guid campusId, DateTime scheduledDay, Guid? studentGroupId, Guid? programVersionId)
        {
            var adjustmentRecords = GetStudentsAdjustmentsQuery(campusId, scheduledDay, studentGroupId, programVersionId).ToList();
            this.PopulateScheduledHoursIfNotExists(adjustmentRecords, scheduledDay);
            return adjustmentRecords;
        }

        /// <summary>
        /// Given parameters, adjusts selected subset of students scheduled hours
        /// </summary>
        /// <param name="parameters">
        /// parameters
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<ActionResult<string>> AdjustScheduledHours(ScheduledHoursAdjustment parameters)
        {
            var rv = new ActionResult<string>();

            try
            {
                var query = GetStudentsAdjustmentsQuery(parameters.campusId, parameters.adjustmentDate, parameters.studentGroupId, parameters.programVersionId);
                var recordsToAdjust = query.ToList();
                this.PopulateScheduledHoursIfNotExists(recordsToAdjust, parameters.adjustmentDate);

                var ids = query.Select(a => a.StuEnrollId).ToList();
                var existingAttendanceRecords = await this.StudentEnrollmentRepository.Get()
                    .Include(a => a.ArStudentClockAttendance)
                    .SelectMany(a => a.ArStudentClockAttendance)
                    .Where(i => ids.Contains(i.StuEnrollId) && i.RecordDate == parameters.adjustmentDate)
                    .ToListAsync();

                foreach (var adjustment in recordsToAdjust)
                {
                    var existingAttendanceRecord = existingAttendanceRecords.Where(a => a.RecordDate == parameters.adjustmentDate && a.StuEnrollId == adjustment.StuEnrollId).FirstOrDefault();
                    var databaseScheduledHours = adjustment.ScheduledHours;
                    var adjustmentAmount = parameters.AmountToAdjust;

                    switch (parameters.AdjustmentMethod)
                    {
                        case AdjustmentMethod.RemoveScheduledHours:
                            {
                                adjustmentAmount = -databaseScheduledHours;
                                databaseScheduledHours = 0;
                                break;
                            }
                        case AdjustmentMethod.AdjustScheduledHoursByAmount:
                            {
                                if ((databaseScheduledHours + adjustmentAmount) < 0)
                                {
                                    adjustmentAmount = -databaseScheduledHours;
                                    databaseScheduledHours = 0;
                                }
                                else
                                {
                                    databaseScheduledHours = databaseScheduledHours + adjustmentAmount;
                                }

                                break;
                            }
                        case AdjustmentMethod.SetScheduledHoursToActualHours:
                            {
                                if (existingAttendanceRecord != null &&
                                    existingAttendanceRecord.ActualHours.HasValue &&
                                    existingAttendanceRecord.ActualHours != 99 &&
                                    existingAttendanceRecord.ActualHours != 999 &&
                                    existingAttendanceRecord.ActualHours != 9999)
                                {
                                    adjustmentAmount = existingAttendanceRecord.ActualHours.Value - databaseScheduledHours;
                                    databaseScheduledHours = existingAttendanceRecord.ActualHours.Value;
                                }

                                break;
                            }
                        case AdjustmentMethod.RevertScheduledHoursPriorToAnyAdjustment:
                            {
                                adjustmentAmount = -adjustment.TotalPriorAdjustments;
                                databaseScheduledHours = databaseScheduledHours - adjustment.TotalPriorAdjustments;
                                break;
                            }
                    }

                    if (existingAttendanceRecord != null) // update record
                    {
                        existingAttendanceRecord.SchedHours = databaseScheduledHours;
                        existingAttendanceRecord.ModDate = DateTime.Now;
                        existingAttendanceRecord.ModUser = this.context.GetTenantUser().Email;
                    }
                    else // create record
                    {
                        await this.ArStudentClockAttendance.CreateAsync(new ArStudentClockAttendance()
                        {
                            StuEnrollId = adjustment.StuEnrollId,
                            ScheduleId = adjustment.ScheduleId,
                            RecordDate = parameters.adjustmentDate,
                            SchedHours = databaseScheduledHours,
                            ActualHours = 9999m,
                            ModDate = DateTime.Now,
                            ModUser = this.context.GetTenantUser().Email,
                            IsTardy = false,
                            PostByException = "no",
                            Comments = null,
                            TardyProcessed = false,
                            Converted = false
                        });
                    }

                    //if no adjustment do not add any adjustment record;
                    if (adjustmentAmount == 0)
                    {
                        continue;
                    }

                    //create adjustment records
                    await this.ArScheduledHoursAdjustments.CreateAsync(new ArScheduledHoursAdjustments()
                    {
                        ScheduledHoursAdjustmentId = Guid.NewGuid(),
                        AdjustmentAmount = adjustmentAmount,
                        StuEnrollId = adjustment.StuEnrollId,
                        ModDate = DateTime.Now,
                        ModUser = this.context.GetTenantUser().Email,
                        RecordDate = parameters.adjustmentDate,
                    });

                    this.context.SaveChanges();
                }

                rv.ResultStatus = Enums.ResultStatus.Success;
                return rv;
            }
            catch (Exception e)
            {
                e.TrackException();
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = ApiMsgs.UNABLE_TO_ADJUST_SCHED_HOURS;
                return rv;
            }
        }

        /// <summary>
        /// The calculate scheduled hours to last day.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="lastDateOfAttendance">
        /// The last Date Of Attendance.
        /// </param>
        /// <returns>
        /// The <see cref="decimal"/>.
        /// </returns>
        public decimal CalculateScheduledHoursToLastDay(Guid studentEnrollmentId, DateTime lastDateOfAttendance, DateTime startdate)
        {
            decimal result = 0;
            ArStudentSchedules schedule = this.StudentScheduleRepository.Get(ss => ss.StuEnrollId == studentEnrollmentId && ss.Active.HasValue && ss.Active.Value)
                .Include(ss => ss.StuEnroll)
               .Include(ss => ss.Schedule).ThenInclude(sch => sch.ArProgScheduleDetails)
               .Include(ss => ss.Schedule).ThenInclude(sch => sch.PrgVer).ThenInclude(pv => pv.UnitType).FirstOrDefault();

            if (schedule == null || schedule.Schedule == null || !schedule.Schedule.ArProgScheduleDetails.Any())
            {
                return result;
            }

            decimal totalProgramHours = schedule.Schedule.PrgVer.Hours;
            bool isTimeClock = schedule.Schedule.PrgVer.UnitType.UnitTypeDescrip == AttendanceUnitTypes.ClockHours
                              || schedule.Schedule.PrgVer.UnitType.UnitTypeDescrip == AttendanceUnitTypes.Minutes;

            if (totalProgramHours == 0 || !isTimeClock)
            {
                return result;
            }

            result = this.StudentClockAttendanceRepository
                .Get(ss => ss.StuEnrollId == studentEnrollmentId && ss.SchedHours.HasValue && ss.RecordDate <= lastDateOfAttendance && ss.RecordDate >= startdate)
                .Sum(x => x.SchedHours.Value);

            return result;
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the actual attendance numbers for the student based on maximum of each running total.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns>
        /// The Attendance snap shop
        /// </returns>
        public async Task<Attendance> GetAttendanceSnapshot(Guid studentEnrollmentId)
        {
            List<Attendance> attendanceSummary = (await this.CalculateAttendance(studentEnrollmentId))?.AttendanceSummary.ToList();
            if (attendanceSummary != null)
            {
                Attendance attendance = new Attendance()
                {
                    Scheduled = attendanceSummary.Max(atsum => atsum.Scheduled),
                    Actual = attendanceSummary.Max(atsum => atsum.Actual),
                    Tardy = attendanceSummary.Max(atsum => atsum.Tardy),
                    Makeup = attendanceSummary.Max(atsum => atsum.Makeup)

                };
                return attendance;
            }

            return null;
        }

        /// <summary>
        /// Gets the actual attendance numbers for the student based on maximum of each running total.
        /// </summary>
        /// <param name="attendanceSummary">
        /// The attendance summary
        /// </param>
        /// <returns>
        /// The <see cref="Attendance"/>.
        /// </returns>
        public Attendance GetAttendanceSnapshot(IEnumerable<Attendance> attendanceSummary)
        {
            List<Attendance> attSummary = attendanceSummary.ToList();
            Attendance attendance = new Attendance()
            {
                Scheduled = attSummary.Max(atsum => atsum.Scheduled),
                Actual = attSummary.Max(atsum => atsum.Actual),
                Tardy = attSummary.Max(atsum => atsum.Tardy),
                Makeup = attSummary.Max(atsum => atsum.Makeup)
            };
            return attendance;
        }

        /// <summary>
        /// The widget user resource settings repository.
        /// </summary>
        private IRepository<ArStudentClockAttendance> ArStudentClockAttendance => this.context.GetRepositoryForEntity<ArStudentClockAttendance>();
        private IRepository<ArScheduledHoursAdjustments> ArScheduledHoursAdjustments => this.context.GetRepositoryForEntity<ArScheduledHoursAdjustments>();
        private IRepository<ArPrgVersions> ArPrgVersions => this.context.GetRepositoryForEntity<ArPrgVersions>();
        private IRepository<ArPrograms> ArPrograms => this.context.GetRepositoryForEntity<ArPrograms>();
        private IRepository<ArStuEnrollments> ArStuEnrollments => this.context.GetRepositoryForEntity<ArStuEnrollments>();
        private IRepository<ArAttUnitType> ArAttUnitType => this.context.GetRepositoryForEntity<ArAttUnitType>();
        private IRepository<AdLeadByLeadGroups> AdLeadByLeadGroups => this.context.GetRepositoryForEntity<AdLeadByLeadGroups>();
        private IRepository<AdLeads> AdLeads => this.context.GetRepositoryForEntity<AdLeads>();
        private IRepository<AtConversionAttendance> AtConversionAttendance => this.context.GetRepositoryForEntity<AtConversionAttendance>();

        /// <summary>
        /// Posts zero for attendance for specific students on specific day given parameters
        /// </summary>
        /// <param name="param">
        /// The student id list and date to post zero's for attendance for
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<ActionResult<bool>> PostZerosForAttendance(PostZeroParams param)
        {
            var rv = new ActionResult<bool>();

            try
            {
                var postZeroSproc = await context
                                      .LoadStoredProc("USP_FA_PostZerosForStudents")
                                      .WithSqlParam("@StuEnrollIdList", string.Join(",", param.StuEnrollIdList), DbType.String)
                                      .WithSqlParam("@Date", param.Date, DbType.DateTime)
                                      .WithSqlParam("@ModUser", this.TenantUser.Email, DbType.String)
                                      .ExecuteStoredProcAsync();

                postZeroSproc.Close();
                rv.Result = true;
                rv.ResultStatus = Enums.ResultStatus.Success;
                return rv;
            }
            catch (Exception e)
            {
                e.TrackException();
                rv.Result = false;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }


        /// <summary>
        /// Posts zero for attendance for specific students on specific days given parameters
        /// </summary>
        /// <param name="param">
        /// The filters and date to post zero's for attendance for a list of students
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public async Task<bool> PostZeroForAttendance(PostAttendanceZero param)
        {
            Guid activeStatusId = await this.statusService.GetActiveStatusId();
            User userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);
            DateTime dateToSetToZero = param.Dates.FirstOrDefault();
            string campusId = param.CampusId;
            string listOfStudents = "";

            DataSet clockHourAttendanceRecords = new DataSet();

            //IQueryable<ArStudentClockAttendance> SCA = this.ArStudentClockAttendance.Get();
            //IQueryable<ArPrgVersions> PV = this.ArPrgVersions.Get();
            //IQueryable<ArPrograms> P = this.ArPrograms.Get();
            //IQueryable<ArStuEnrollments> SE = this.ArStuEnrollments.Get();
            //IQueryable<AdLeads> S = this.AdLeads.Get();
            //IQueryable<ArAttUnitType> AUT = this.ArAttUnitType.Get();
            //IQueryable<AdLeadByLeadGroups> LLG = this.AdLeadByLeadGroups.Get();

            //IQueryable<object> query = from _sca in SCA
            //                           join _pv in PV
            //                           join _p in P
            //                           join _se in SE
            //                           join _s in S
            //                           join _aut in AUT
            //join llg in LLG // optional

            List<Item> attendanceRecordsOnGrid = new List<Item>();
            List<ClockHourAttendanceInfo> attendanceItems = new List<ClockHourAttendanceInfo>();
            bool isHoliday = await this.IsHoliday(dateToSetToZero, campusId);

            foreach (Item recordOnGrid in attendanceRecordsOnGrid)
            {
                ClockHourAttendanceInfo i = new ClockHourAttendanceInfo();
                i.IsInDB = false;
                i.StuEnrollId = recordOnGrid.StuEnrollId;
                i.ScheduleId = recordOnGrid.ScheduleId;
                i.Source = recordOnGrid.Source;
                //if record date is not null
                i.RecordDate = i.RecordDate;
                if (recordOnGrid.ActualHours.ToString().Length > 0)
                {
                    if (recordOnGrid.ActualHours.ToString() == "999.00")
                    {
                        i.ActualHours = 0;
                    }
                    else if (recordOnGrid.ActualHours.ToString() == "9999.00")
                    {
                        i.ActualHours = 9999;
                    }
                    else
                    {
                        i.ActualHours = recordOnGrid.ActualHours;
                    }
                }
                else
                {
                    i.ActualHours = 999;
                }

                i.IsTardy = recordOnGrid.Tardy;
                i.SchedHours = Decimal.Parse(recordOnGrid.SchedHours);
                i.UnitTypeDescription = recordOnGrid.UnitTypeDescrip == null ? "" : recordOnGrid.UnitTypeDescrip;
                i.PostByException = recordOnGrid.PostByException == null ? "no" : recordOnGrid.PostByException;
                attendanceItems.Add(i);
            }


            foreach (ClockHourAttendanceInfo attendanceRecord in attendanceItems)
            {
                if (attendanceRecord.UnitTypeDescription.IsNullOrEmpty())
                {
                    if (attendanceRecord.UnitTypeDescription.ToLower().Substring(0, 7) == "present")
                    {
                        //update record

                        if (attendanceRecord.ActualHours != 9999 && attendanceRecord.ActualHours != 999)
                        {
                            await DoSomethingAsync(attendanceRecord, isHoliday, userDetails.DisplayName);
                            //update student from future start to currently attending 
                        }
                    }
                }
                else
                {
                    //update

                    if (attendanceRecord.ActualHours != 9999 && attendanceRecord.ActualHours != 999)
                    {
                        //update student from future start to currently attending 
                        await DoSomethingAsync(attendanceRecord, isHoliday, userDetails.DisplayName);
                    }
                }
            }

            this.context.SaveChanges();
            return true;
        }

        /// <summary>
        /// Method to import time clock file
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<ActionResult<string>> ImportTimeClockFile(ImportTimeClockFile param)
        {
            var rv = new ActionResult<string>();
            try
            {
                var currentUser = this.TenantUser;
                var tenantId = this.tenantService.GetTenantId(currentUser.TenantName);

                var wapiSetting = await this.context.GetRepositoryForEntity<SyWapiSettings>()
                    .Get(a => a.CodeOperation == WapiSettingCodes.TC_FILEWATCH_SERVICE).AsNoTracking()
                    .FirstOrDefaultAsync();

                var wapiAdvSetting = await this.context.GetRepositoryForEntity<SyWapiSettings>()
                   .Get(a => a.CodeOperation == WapiSettingCodes.ADVANTAGE_API).AsNoTracking()
                   .FirstOrDefaultAsync();

                var timeClockImportParams = new TimeClockImportParams()
                {
                    Username = currentUser.Email,
                    TenantId = tenantId.ToString(),
                    TenantUserId = currentUser.Id.ToString(),
                    AdvantageApiToken = (this._httpContextAccessor.HttpContext.Request.Headers[ServiceDefaults.AUTHORIZATION]).ToString().Replace("Bearer ", ""),
                    FileName = Path.GetFileName(param.FilePath),
                    CampusId = param.CampusId.ToString(),
                    ApiUrl = wapiAdvSetting.ExternalUrl,
                };

                //make request to the aspx web method in ServiceMethods.aspx
                var uri = wapiSetting.ExternalUrl + "/ImportTimeClockFile?cmpid=" + param.CampusId.ToString();
                var httpClient = new HttpClient() { Timeout = new TimeSpan(1, 0, 0) };
                var jsonInString = JsonConvert.SerializeObject(timeClockImportParams);
                var task = await httpClient.PostAsync(uri, new StringContent(jsonInString, Encoding.UTF8, "application/json"));

                if (!task.IsSuccessStatusCode)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = task.ReasonPhrase;
                    return rv;
                }
                else
                {
                    dynamic obj = JsonConvert.DeserializeObject<dynamic>(task.Content.ReadAsStringAsync().Result);
                    rv = JsonConvert.DeserializeObject<ActionResult<string>>(JsonConvert.SerializeObject(obj.d));
                    return rv;
                }
            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                AppInsightExtensions.TrackException(e);
                return rv;
            }
        }

        /// <summary>
        /// Inserts or updates time clock import log record
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public async Task<ActionResult<string>> UpsertTimeClockLog(TimeClockLogParams param)
        {
            var rv = new ActionResult<string>();
            try
            {
                var tcLogRepo = this.context.GetRepositoryForEntity<SyTimeClockImportLog>();
                var syUsers = this.context.GetRepositoryForEntity<SyUsers>();
                var userFullName = await syUsers.Get(x => x.UserId == this.TenantUser.Id).Select(x => x.FullName).FirstOrDefaultAsync();
                var autoImportModUser = param.IsAutoImport.HasValue && param.IsAutoImport.Value ? "Auto Import" : null;

                //update
                if (param.TimeClockImportLogId.HasValue)
                {
                    var existingLogItem = await this.context.GetRepositoryForEntity<SyTimeClockImportLog>()
                                                    .Get(x => x.TimeClockImportLogId == param.TimeClockImportLogId.Value)
                                                    .FirstOrDefaultAsync();
                    existingLogItem.Message = param.Message;
                    existingLogItem.Status = param.Status;
                    existingLogItem.FileName = param.FileName;
                    existingLogItem.ModDate = DateTime.Now;
                    existingLogItem.ModUser = autoImportModUser ?? userFullName ?? this.TenantUser.Email;
                    existingLogItem.CampusId = param.CampusId;
                    tcLogRepo.Update(existingLogItem);
                    this.context.SaveChanges();

                    rv.Result = existingLogItem.TimeClockImportLogId.ToString();
                    rv.ResultStatus = Enums.ResultStatus.Success;
                    rv.ResultStatusMessage = $"Successfully updated log record";
                }
                else // insert
                {
                    var newItem = await tcLogRepo.CreateAsync(new SyTimeClockImportLog()
                    {
                        Message = param.Message,
                        Status = param.Status,
                        FileName = param.FileName,
                        ModDate = DateTime.Now,
                        ModUser = autoImportModUser ?? userFullName ?? this.TenantUser.Email,
                        CampusId = param.CampusId
                    });

                    tcLogRepo.Create(newItem);
                    this.context.SaveChanges();

                    rv.Result = newItem.TimeClockImportLogId.ToString();
                    rv.ResultStatus = Enums.ResultStatus.Success;
                    rv.ResultStatusMessage = $"Successfully created log record with id: {newItem.TimeClockImportLogId.ToString()}";
                }

                return rv;
            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                AppInsightExtensions.TrackException(e);
                return rv;
            }
        }

        /// <summary>
        /// Returns list of imported time clock log records given campus identifier.
        /// </summary>
        /// <param name="campusId">
        /// Campus Identifier</param>
        /// <param name="count">
        /// Number of records to return. Default 20</param>
        /// <returns></returns>
        public async Task<ActionResult<List<SyTimeClockImportLog>>> GetTimeClockImportLogsByCampus(Guid campusId, int count = 20)
        {
            var rv = new ActionResult<List<SyTimeClockImportLog>>();
            try
            {
                var logRecords = await this.context.GetRepositoryForEntity<SyTimeClockImportLog>()
                                     .Get(x => x.CampusId == campusId).OrderByDescending(x => x.ModDate).Take(count).AsNoTracking().ToListAsync();

                rv.Result = logRecords;
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = $"Successfully returned records.";
                return rv;
            }
            catch (Exception e)
            {
                rv.Result = null;
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                AppInsightExtensions.TrackException(e);
                return rv;
            }
        }

        private async Task<ArStudentClockAttendance> DoSomethingAsync(ClockHourAttendanceInfo info, bool isHoliday, string userName)
        {
            ArStudentClockAttendance a = new ArStudentClockAttendance();
            info.Source = await this.GetStudentSourceAsync(info.StuEnrollId);

            if (info.Source.ToString().ToLower() == "attendance")
            {
                if (isHoliday)
                {
                    info.SchedHours = 0;
                }

                if (info.SchedHours == 0)
                {
                    a.SchedHours = 0;
                }
                else
                {
                    a.SchedHours = info.SchedHours;
                }

                if (info.UnitTypeDescription.ToString().Trim() != "")
                {
                    if (info.ActualHours == 0 && info.UnitTypeDescription.ToLower().Substring(0, 7) != "present")
                    {
                        a.ActualHours = null;
                    }
                    else if (info.ActualHours == 0 && info.UnitTypeDescription.ToLower().Substring(0, 7) == "present")
                    {
                        a.ActualHours = 0;
                    }
                    else if (info.ActualHours == 999)
                    {
                        a.ActualHours = null;
                    }
                    else if (info.ActualHours == 9999)
                    {
                        a.ActualHours = 9999;
                    }
                    else
                    {
                        a.ActualHours = info.ActualHours;
                    }
                }
                if (info.PostByException == "yes")
                {
                    a.PostByException = "yes";
                }
                else
                {
                    a.PostByException = "no";
                }
                a.ModDate = DateTime.Now;
                a.ModUser = userName;

                if (info.IsTardy == 1)
                {
                    a.IsTardy = true;
                }
                else
                {
                    a.IsTardy = false;
                }

                if (a.TardyProcessed == false)
                {
                    a.TardyProcessed = false;
                }
                else
                {
                    a.TardyProcessed = true;
                }


            }
            else
            {
                //Conversion attendance TODO implementation
                if (isHoliday)
                {
                    info.SchedHours = 0;
                }

                if (info.SchedHours == 0)
                {

                }
            }
            return a;
        }

        private async Task<Boolean> UpsertRecord(ArStudentClockAttendance item)
        {
            IQueryable<ArStudentClockAttendance> attendanceDb = this.ArStudentClockAttendance.Get();
            ArStudentClockAttendance existingItem = await attendanceDb.Where(a => a.StuEnrollId == item.StuEnrollId && a.RecordDate == item.RecordDate).FirstOrDefaultAsync();

            if (existingItem != null)
            {
                existingItem.ActualHours = item.SchedHours;
                existingItem.ActualHours = item.ActualHours;
                existingItem.PostByException = item.PostByException;
                existingItem.ModDate = item.ModDate;
                existingItem.ModUser = item.ModUser;
                existingItem.IsTardy = item.IsTardy;
                existingItem.TardyProcessed = item.TardyProcessed;
                this.ArStudentClockAttendance.Update(existingItem);
                return true;
            }
            else
            {
                return true;
            }
        }

        private async Task<string> GetStudentSourceAsync(string studentEnrollmentId)
        {
            IQueryable<ArStudentClockAttendance> arConverstionAttendance = this.ArStudentClockAttendance.Get();
            bool recordExists = await arConverstionAttendance.AnyAsync(a => a.StuEnrollId.ToString().ToLower() == studentEnrollmentId.ToLower());
            return recordExists ? "conversion" : "attendance";
        }

        private async Task<bool> IsHoliday(DateTime date, string campusId)
        {
            IEnumerable<Holiday> allHolidays = await this.holidayService.GetHolidayListByCampus(Guid.Parse(campusId));
            foreach (Holiday holiday in allHolidays)
            {
                if (date >= holiday.StartDate && date <= holiday.EndDate)
                    return true;
            }

            return false;
        }

        private IQueryable<ScheduledHourAdjustment> GetStudentsAdjustmentsQuery(Guid campusId, DateTime scheduledDay, Guid? studentGroupId, Guid? programVersionId)
        {
            Expression<Func<ArStuEnrollments, bool>> filter =
                se => se.CampusId == campusId && (se.StatusCode.SysStatus.InSchool.HasValue && se.StatusCode.SysStatus.InSchool.Value == 1)
                            && se.StartDate <= scheduledDay;

            if (studentGroupId.HasValue)
            {
                filter = filter.And(se => se.Lead.AdLeadByLeadGroups.Any(lg => lg.LeadGrpId == studentGroupId && lg.StuEnrollId == se.StuEnrollId));
            }

            if (programVersionId.HasValue)
            {
                filter = filter.And(se => se.PrgVerId == programVersionId);
            }

            return this.StudentEnrollmentRepository.Get(filter)
                .OrderBy(x => x.Lead.LastName)
                .ThenBy(x => x.Lead.FirstName)
                        .Select(x => new ScheduledHourAdjustment
                        {
                            LeadId = x.LeadId.GetValueOrDefault(),
                            StudentName = x.Lead.LastName + ", " + x.Lead.FirstName,
                            ProgramVersion = x.PrgVer.PrgVerDescrip,
                            StudentGroups = string.Join(",", x.AdLeadByLeadGroups.Select(a => a.LeadGrp.Descrip)),
                            ScheduledHours = x.ArStudentClockAttendance.Where(y => y.RecordDate == scheduledDay).Select(y => y.SchedHours).FirstOrDefault(),
                            ActualHours = x.ArStudentClockAttendance.Where(y => y.RecordDate == scheduledDay && y.ActualHours != 99 && y.ActualHours != 999 && y.ActualHours != 9999)
                                                                    .Select(y => y.ActualHours).FirstOrDefault() ?? 9999m,
                            TotalPriorAdjustments = x.ArScheduledHoursAdjustments.Where(y => y.RecordDate == scheduledDay).Sum(z => z.AdjustmentAmount) ?? 0,
                            StuEnrollId = x.StuEnrollId,
                            ScheduleId = x.ArStudentSchedules.FirstOrDefault().ScheduleId,
                        });
        }

        private void PopulateScheduledHoursIfNotExists(List<ScheduledHourAdjustment> records, DateTime day)
        {
            //populate scheduled hours only if record does not exist in database
            if (records.Any())
            {
                var scheduledHoursAccordingToSchedule = this.context
                      .RawSqlQuery($"SELECT * FROM dbo.StudentScheduledHours('{string.Join(",", records.Select(a => a.StuEnrollId))}', '{day.ToShortDateString()}')"
                      , x => new StudentScheduledHours { StuEnrollId = (Guid)x[0], CalculatedScheduledHours = (decimal)x[1], ScheduleId = (Guid)x[2] });

                foreach (var enroll in records)
                {
                    if (!enroll.ScheduledHours.HasValue)
                    {
                        enroll.ScheduledHours = scheduledHoursAccordingToSchedule.Where(a => a.StuEnrollId == enroll.StuEnrollId)
                            .Select(x => x.CalculatedScheduledHours).FirstOrDefault();
                    }
                }
            }
        }
    }

    public class Item
    {
        public bool RowStateModified { get; set; }
        public string StuEnrollId { get; set; }
        public string ScheduleId { get; set; }
        public string Source { get; set; }
        public string RecordDate { get; set; }
        public decimal ActualHours { get; set; }
        public int Tardy { get; set; }
        public string SchedHours { get; set; }
        public string UnitTypeDescrip { get; set; }
        public string PostByException { get; set; }
    }
    public class ClockHourAttendanceInfo
    {
        public bool IsInDB { get; set; }
        public string StuEnrollId { get; set; }
        public string ScheduleId { get; set; }
        public DateTime RecordDate { get; set; }
        public decimal SchedHours { get; set; }
        public decimal ActualHours { get; set; }
        public string ModUser { get; set; }
        public DateTime ModDate { get; set; }
        public string UnitTypeDescription { get; set; }
        public int IsTardy { get; set; }
        public decimal TotalHoursPresent { get; set; }
        public decimal TotalHoursSched { get; set; }
        public decimal TotalHoursAbsent { get; set; }
        public decimal TotalMakeUpHours { get; set; }
        public decimal TotalSchedHoursByWeek { get; set; }
        public DateTime LDA { get; set; }
        public string Source { get; set; }
        public string PostByException { get; set; }
        public decimal AttendancePercentage { get; set; }
        public decimal ActualDaysAbsent { get; set; }
        public decimal DaysStudentwasTardy { get; set; }
        public decimal NumberOfTardiesMaking1Absense { get; set; }
        public decimal ActualTardyHours { get; set; }
        public decimal NumberOfTardy { get; set; }
        public decimal AdjustedActual { get; set; }
        public decimal AdjustedAbsent { get; set; }
        public decimal AdjustedTardy { get; set; }
        public decimal ActualDaysPresent { get; set; }
        public int TardyProcessed { get; set; }
        public decimal TransferHours { get; set; }
    }
}
