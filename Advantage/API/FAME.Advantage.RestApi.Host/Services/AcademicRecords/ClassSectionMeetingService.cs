﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionMeetingService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ClassSectionMeetingService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSectionMeeting;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Unity.Interception.Utilities;

    /// <inheritdoc />
    /// <summary>
    /// The class section meeting service.
    /// </summary>
    public class ClassSectionMeetingService : IClassSectionMeetingService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The time interval service.
        /// </summary>
        private readonly ITimeIntervalService timeIntervalService;

        /// <summary>
        /// The period service.
        /// </summary>
        private readonly IPeriodService periodService;

        /// <summary>
        /// The holiday service.
        /// </summary>
        private readonly IHolidayService holidayService;

        /// <summary>
        /// The program schedule service.
        /// </summary>
        private readonly IProgramScheduleService programScheduleService;

        /// <summary>
        /// The class section service.
        /// </summary>
        private readonly IClassSectionService classSectionService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionMeetingService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="timeIntervalService">
        /// The time Interval Service.
        /// </param>
        /// <param name="periodService">
        /// The period Service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="programScheduleService">
        /// The program Schedule Service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        /// <param name="holidayService">
        /// The holiday Service.
        /// </param>
        /// <param name="classSectionService">
        /// The class Section Service.
        /// </param>
        public ClassSectionMeetingService(IAdvantageDbContext context, ITimeIntervalService timeIntervalService, IPeriodService periodService, IMapper mapper, IProgramScheduleService programScheduleService, IEnrollmentService enrollmentService, IHolidayService holidayService, IClassSectionService classSectionService)
        {
            this.context = context;
            this.timeIntervalService = timeIntervalService;
            this.periodService = periodService;
            this.mapper = mapper;
            this.programScheduleService = programScheduleService;
            this.enrollmentService = enrollmentService;
            this.holidayService = holidayService;
            this.classSectionService = classSectionService;
        }

        /// <summary>
        /// The class section attendance repository.
        /// </summary>
        private IRepository<ArClsSectMeetings> ClassSectionMeetingRepository => this.context.GetRepositoryForEntity<ArClsSectMeetings>();

        /// <summary>
        /// The GetTimeIntervals service method returns sum of time interval by given class meeting Ids.
        /// </summary>
        /// <param name="clsMeetingIds">
        /// The class meeting ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<Guid, decimal>>> GetTimeIntervals(List<Guid?> clsMeetingIds)
        {
            return await Task.Run(
                      async () =>
                           {
                               var meetingIdAndDuration = new List<ListItem<Guid, decimal>>();
                               clsMeetingIds = clsMeetingIds.Where(x => x != null && x != Guid.Empty).ToList();
                               var classMeetings = this.ClassSectionMeetingRepository.Get(
                                       x => clsMeetingIds.Contains(x.ClsSectMeetingId));
                               foreach (var meeting in classMeetings)
                               {
                                   var startAndEndTimeId = await this.periodService.GetPeriodDetail(meeting.PeriodId);
                                   if (startAndEndTimeId != null)
                                   {
                                       var timeInterval = await this.timeIntervalService.GetTimeIntervalDifference(
                                                              startAndEndTimeId.Text,
                                                              startAndEndTimeId.Value);

                                       meetingIdAndDuration.Add(new ListItem<Guid, decimal>
                                       {
                                           Text = meeting.ClsSectMeetingId,
                                           Value = Convert.ToDecimal(timeInterval)
                                       });
                                   }
                               }

                               return meetingIdAndDuration;
                           });
        }

        /// <summary>
        /// The GetClassLevelScheduleForDateRangeByClassSections service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<DateTime, decimal>>> GetClassLevelScheduleForDateRangeByClassSections(
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime)
        {
            return await Task.Run(
                       async () =>
                           {
                               var meetingIdAndDuration = new List<ListItem<DateTime, decimal>>();
                               var classMeetingDetails = this.ClassSectionMeetingRepository.Get(x =>
                                        clsSectionIds.Contains(x.ClsSectionId)
                                        && x.StartDate <= endDateTime
                                        && x.EndDate >= startDateTime.Date);
                                
                               var periodIds = classMeetingDetails.Select(x => x.PeriodId ?? Guid.Empty).Distinct().ToList();
                               var periodAndScheduledHours = await this.periodService.GetPeriodAndScheduledHoursByIds(periodIds);
                               periodAndScheduledHours.ForEach(
                                   x =>
                                       {
                                           classMeetingDetails.Where(cm => cm.PeriodId == x.PeriodId)
                                               .ForEach(
                                                   cm =>
                                                       {
                                                           for (var date = cm.StartDate; date <= cm.EndDate;)
                                                           {
                                                               var dayOfWeek = date.Value.DayOfWeek.ToString();
                                                               var scheduleForDayOfWeek = x.ScheduleByWorkday.FirstOrDefault(s => dayOfWeek.ToLower().Contains(s.Text.ToLower()));

                                                               if (scheduleForDayOfWeek != null && date >= startDateTime.Date && date <= endDateTime.Date)
                                                               {
                                                                   var periodTimeSpan = TimeSpan.Parse(x.StartTimeInterval.ToString("HH:mm:ss"));
                                                                   var datetime = date.Value.AddHours(periodTimeSpan.TotalHours);
                                                                   var isAlreadyExist = meetingIdAndDuration.Any(meet => meet.Text == datetime);
                                                                   if (!isAlreadyExist)
                                                                   {
                                                                       meetingIdAndDuration.Add(
                                                                           new ListItem<DateTime, decimal>
                                                                           {
                                                                               Text = datetime,
                                                                               Value = scheduleForDayOfWeek.Value
                                                                           });
                                                                   }
                                                               }

                                                               date = date.Value.AddDays(1);
                                                           }
                                                       });
                                       });

                               return meetingIdAndDuration;
                           });
        }

        /// <summary>
        /// The GetProgramLevelScheduleForDateRangeByClassSections service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<DateTime, decimal>>> GetProgramLevelScheduleForDateRangeByClassSections(Enrollment enrollment, List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime)
        {
            return await Task.Run(
                       async () =>
                       {
                           var meetingIdAndDuration = new List<ListItem<DateTime, decimal>>();
                           var classSectionsDetails = await this.classSectionService.GetDetailByIds(clsSectionIds);

                           var programScheduleDetail = await this.programScheduleService.GetScheduleHoursAndWeekDayForDateRange(enrollment.EnrollmentId);

                           programScheduleDetail = programScheduleDetail.ToList();
                           classSectionsDetails.ForEach(
                                   cm =>
                                       {
                                           for (var date = cm.StartDate; date <= cm.EndDate;)
                                           {
                                               var currentDate = date;
                                               var dayOfWeek = Convert.ToInt32(date.DayOfWeek);
                                               var scheduledHours = programScheduleDetail.FirstOrDefault(x => x.Text == dayOfWeek);
                                               if (scheduledHours != null && date >= startDateTime && date <= endDateTime)
                                               {
                                                   var isAlreadyExist = meetingIdAndDuration.Any(x => x.Text == currentDate);
                                                   if (!isAlreadyExist)
                                                   {
                                                       meetingIdAndDuration.Add(new ListItem<DateTime, decimal>
                                                       {
                                                           Text = date,
                                                           Value = scheduledHours.Value
                                                       });
                                                   }
                                               }

                                               date = date.AddDays(1);
                                           }
                                       });

                           return meetingIdAndDuration;
                       });
        }

        /// <summary>
        /// The get class section meetings by class section id method returns class section meeting details by classSectionId.
        /// </summary>
        /// <param name="classSectionId">
        /// The class section id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsByClassSectionId(Guid classSectionId)
        {
            return await Task.Run(() =>
                           {
                               var classSecMeetingDetails = this.ClassSectionMeetingRepository.Get(x => x.ClsSectionId == classSectionId).ToList();

                               return this.mapper.Map<IList<ClassSectionMeetingDetail>>(classSecMeetingDetails).ToList();
                           });
        }

        /// <summary>
        /// The GetClassLevelScheduleForDateRangeByClassSections service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsForDateRangeByClassSections(
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime)
        {
            return await Task.Run(
                       () =>
                       {
                           var classMeetingDetails = this.ClassSectionMeetingRepository.Get(x =>
                                    clsSectionIds.Contains(x.ClsSectionId)
                                    && x.StartDate <= endDateTime && x.EndDate >= startDateTime);
                           return this.mapper.Map<IList<ClassSectionMeetingDetail>>(classMeetingDetails)
                               .OrderBy(x => x.StartDate).ToList();
                       });
        }

        /// <summary>
        /// The get schedule start date by class section ids.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<DateTime?> GetScheduleStartDateByClassSectionIds(List<Guid> clsSectionIds)
        {
            return await Task.Run(
                       () =>
                           {
                               var meetingStartDate = this.ClassSectionMeetingRepository
                                   .Get(x => clsSectionIds.Contains(x.ClsSectionId))
                                   .Select(x => x.StartDate).Distinct()
                                   .OrderBy(x => x).FirstOrDefault();

                               return meetingStartDate;
                           });
        }

        /// <summary>
        /// The GetScheduledWeeksForDateRangeByClassSectionIds action method gets scheduled weeks for given date range and class section ids.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="isByDay">
        /// The is by day.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Tuple<int, IEnumerable<DateTime>>> GetScheduledWeeksForDateRangeByClassSectionIds(Enrollment enrollment, bool isByDay, List<Guid> clsSectionIds, DateTime startDate, DateTime endDate)
        {
            int scheduledWeeksCount = 0;
            IList<DateTime> scheduledWeeks = new List<DateTime>();
            return await Task.Run(
                       async () =>
                           {
                               var meetingsDatesAndHoursScheduled =
                                   isByDay
                                       ? await this.GetProgramLevelScheduleForDateRangeByClassSections(
                                           enrollment,
                                           clsSectionIds,
                                           startDate,
                                           endDate)
                                       : await this.GetClassLevelScheduleForDateRangeByClassSections(
                                           clsSectionIds,
                                           startDate,
                                           endDate);

                               meetingsDatesAndHoursScheduled = meetingsDatesAndHoursScheduled.OrderBy(x => x.Text).ToList();
                               var studentLoAs = await this.enrollmentService.GetStudentLoaDetails(
                                                     enrollment.EnrollmentId,
                                                     startDate,
                                                     endDate);
                               var holidays = await this.holidayService.GetHolidayListByCampus(enrollment.CampusId);

                               var finalMeetingsDatesAndHoursScheduled = new List<IListItem<DateTime, decimal>>();
                               meetingsDatesAndHoursScheduled.ForEach(
                                   x =>
                                       {
                                           if (!studentLoAs.Any(loa => loa.StartDate <= x.Text.Date && loa.EndDate >= x.Text.Date) &&
                                               !holidays.Any(holiday => holiday.StartDate <= x.Text && holiday.EndDate >= x.Text))
                                           {
                                               finalMeetingsDatesAndHoursScheduled.Add(x);
                                           }
                                       });

                               for (DateTime startDateTime = startDate; startDateTime <= endDate;)
                               {
                                   var weekStartDate = startDateTime;
                                   var weekEndDate = startDateTime.AddDays(6);

                                   var schedulesForWeek = finalMeetingsDatesAndHoursScheduled.Where(x => x.Text >= weekStartDate && x.Text <= weekEndDate).ToList();

                                   if (schedulesForWeek.Any())
                                   {
                                       scheduledWeeksCount++;
                                       schedulesForWeek.ForEach(x =>
                                           {
                                               if (!scheduledWeeks.Contains(x.Text) && x.Value > 0)
                                               {
                                                   scheduledWeeks.Add(x.Text);
                                               }
                                           });
                                   }

                                   startDateTime = weekEndDate;
                               }

                               return new Tuple<int, IEnumerable<DateTime>>(scheduledWeeksCount, scheduledWeeks);
                           });
        }

        /// <summary>
        /// The GetClassSectionMeetingsAfterPeriodEndDate service method returns list of hours schedule and date by given class meeting Ids and date.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <returns>
        /// Returns an object of type ClassSectionMeetingDetail.
        /// </returns>
        public async Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingsAfterPeriodEndDate(
            List<Guid> clsSectionIds,
            DateTime? startDateTime)
        {
            return await Task.Run(
                       () =>
                           {
                               var classMeetingDetails = this.ClassSectionMeetingRepository.Get(x =>
                                   clsSectionIds.Contains(x.ClsSectionId)
                                   && x.StartDate > startDateTime);
                               return this.mapper.Map<IList<ClassSectionMeetingDetail>>(classMeetingDetails).ToList();
                           });
        }

        /// <summary>
        /// The GetClassSectionMeetingDetailsList method will gets class section meetings details for the given list of class section id's .
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <returns>
        /// It returns the class section meetings details for the given list of class section id's.
        /// </returns>
        public async Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingDetailsList(List<Guid> clsSectionIds)
        {
            return await Task.Run(() =>
                {
                    var classSecMeetingDetails = this.ClassSectionMeetingRepository.Get(x => clsSectionIds.Contains(x.ClsSectionId)).ToList();
                    return this.mapper.Map<IList<ClassSectionMeetingDetail>>(classSecMeetingDetails).ToList();
                });
        }

        /// <summary>
        /// The GetClassSectionMeetingDetailsList method will gets class section meetings details for the given list of class section id's .
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <returns>
        /// It returns the class section meetings details for the given list of class section id's.
        /// </returns>
        public async Task<IList<ClassSectionMeetingDetail>> GetClassSectionMeetingDetails(Guid clsSectionIds)
        {
            return await Task.Run(() =>
                {
                    var classSecMeetingDetails = this.ClassSectionMeetingRepository.Get(x => clsSectionIds == x.ClsSectionId).ToList();
                    return this.mapper.Map<IList<ClassSectionMeetingDetail>>(classSecMeetingDetails).ToList();
                });
        }

        /// <summary>
        /// The GetClassSectionsScheduleForDateRange service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<DateTime, Guid>>> GetClassSectionsScheduleForDateRange(
            List<Guid> clsSectionIds,
            DateTime startDateTime,
            DateTime endDateTime)
        {
            return await Task.Run(
                       async () =>
                       {
                           var meetingIdAndDuration = new List<ListItem<DateTime, Guid>>();
                           var classMeetingDetails = this.ClassSectionMeetingRepository.Get(x =>
                                    clsSectionIds.Contains(x.ClsSectionId)
                                    && x.StartDate <= endDateTime
                                    && x.EndDate >= startDateTime);

                           var periodIds = classMeetingDetails.Select(x => x.PeriodId ?? Guid.Empty).Distinct().ToList();
                           var periodAndScheduledHours = await this.periodService.GetPeriodAndScheduledHoursByIds(periodIds);
                           periodAndScheduledHours.ForEach(
                               x =>
                               {
                                   classMeetingDetails.Where(cm => cm.PeriodId == x.PeriodId)
                                           .ForEach(
                                               cm =>
                                               {
                                                   for (var date = cm.StartDate; date <= cm.EndDate;)
                                                   {
                                                       var dayOfWeek = date.Value.DayOfWeek.ToString();
                                                       var scheduleForDayOfWeek = x.ScheduleByWorkday.FirstOrDefault(s => dayOfWeek.ToLower().Contains(s.Text.ToLower()));

                                                       if (scheduleForDayOfWeek != null && date >= startDateTime.Date && date <= endDateTime.Date)
                                                       {
                                                           var periodTimeSpan = TimeSpan.Parse(x.StartTimeInterval.ToString("HH:mm:ss"));
                                                           var datetime = date.Value.AddHours(periodTimeSpan.TotalHours);
                                                           meetingIdAndDuration.Add(new ListItem<DateTime, Guid>
                                                           {
                                                               Text = datetime,
                                                               Value = x.PeriodId
                                                           });
                                                       }

                                                       date = date.Value.AddDays(1);
                                                   }
                                               });
                               });

                           return meetingIdAndDuration;
                       });
        }

        /// <summary>
        /// The GetProgramLevelScheduleForDateRange service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<DateTime, Guid>>> GetProgramLevelScheduleForDateRange(Enrollment enrollment, List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime)
        {
            return await Task.Run(
                       async () =>
                       {
                           var meetingIdAndDuration = new List<ListItem<DateTime, Guid>>();
                           var classSectionsDetails = await this.classSectionService.GetDetailByIds(clsSectionIds);

                           var programScheduleDetail = await this.programScheduleService.GetScheduleHoursAndWeekDayForDateRange(
                                   enrollment.EnrollmentId); 

                           programScheduleDetail = programScheduleDetail.ToList();
                           classSectionsDetails.ForEach(
                                   cm =>
                                   {
                                       for (var date = cm.StartDate; date <= cm.EndDate;)
                                       {
                                           var dayOfWeek = Convert.ToInt32(date.DayOfWeek);
                                           var scheduledHours = programScheduleDetail.FirstOrDefault(x => x.Text == dayOfWeek);
                                           if (scheduledHours != null && date >= startDateTime && date <= endDateTime)
                                           {
                                               meetingIdAndDuration.Add(new ListItem<DateTime, Guid>
                                               {
                                                   Text = date,
                                                   Value = cm.TermId
                                               });
                                           }

                                           date = date.AddDays(1);
                                       }
                                   });

                           return meetingIdAndDuration;
                       });
        }

        /// <summary>
        /// The GetProgramLevelSchedules service method returns list of hours schedule and date by given class meeting Ids and date range.
        /// </summary>
        /// <param name="enrollment">
        /// The enrollment.
        /// </param>
        /// <param name="clsSectionIds">
        /// The class section ids.
        /// </param>
        /// <param name="startDateTime">
        /// The start Date Time.
        /// </param>
        /// <param name="endDateTime">
        /// The end Date Time.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<DateTime, Guid>>> GetProgramLevelSchedules(Enrollment enrollment, List<Guid> clsSectionIds, DateTime startDateTime, DateTime endDateTime)
        {
            return await Task.Run(
                       async () =>
                       {
                           var meetingIdAndDuration = new List<ListItem<DateTime, Guid>>();
                           var classSectionsDetails = await this.classSectionService.GetDetailByIds(clsSectionIds);

                           var programScheduleDetail = await this.programScheduleService.GetScheduleWeekDayForEnrollmentId(
                                   enrollment.PrgVerId, enrollment.EnrollmentId);

                           programScheduleDetail = programScheduleDetail.ToList();
                           classSectionsDetails.ForEach(
                                   cm =>
                                   {
                                       for (var date = cm.StartDate; date <= cm.EndDate;)
                                       {
                                           var dayOfWeek = Convert.ToInt32(date.DayOfWeek);
                                           var scheduledHours = programScheduleDetail.FirstOrDefault(x => x.Text == dayOfWeek && x.Value > 0);
                                           if (scheduledHours != null && date >= startDateTime && date <= endDateTime)
                                           {
                                               meetingIdAndDuration.Add(new ListItem<DateTime, Guid>
                                               {
                                                   Text = date,
                                                   Value = cm.TermId
                                               });
                                           }

                                           date = date.AddDays(1);
                                       }
                                   });

                           return meetingIdAndDuration;
                       });
        }
    }
}
