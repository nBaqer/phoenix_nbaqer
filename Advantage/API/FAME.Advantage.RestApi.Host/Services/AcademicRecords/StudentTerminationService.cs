﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentTerminationService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the TerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;

    /// <summary>
    /// The student termination service.
    /// </summary>
    public class StudentTerminationService : IStudentTerminationService
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The status codes service.
        /// </summary>
        private readonly IStatusCodesService statusCodesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentTerminationService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        /// <param name="statusCodesService">
        /// The status Codes Service.
        /// </param>
        public StudentTerminationService(IAdvantageDbContext unitOfWork, IMapper mapper, IEnrollmentService enrollmentService, IStatusCodesService statusCodesService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            unitOfWork.GetSimpleCacheConnectionString();
            this.enrollmentService = enrollmentService;
            this.statusCodesService = statusCodesService;
        }

        /// <summary>
        /// The termination details repository.
        /// </summary>
        private IRepository<ArR2t4terminationDetails> R2T4TerminationDetailRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4terminationDetails>();

        /// <summary>
        /// The r 2 t 4 addition information repository.
        /// </summary>
        private IRepository<ArR2t4additionalInformation> R2T4AdditionInformationRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4additionalInformation>();

        /// <summary>
        /// The r 2 t 4 calculation results repository.
        /// </summary>
        private IRepository<ArR2t4calculationResults> R2T4CalculationResultsRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4calculationResults>();

        /// <summary>
        /// The r 2 t 4 input repository.
        /// </summary>
        private IRepository<ArR2t4input> R2T4InputRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4input>();

        /// <summary>
        /// The r 2 t 4 override input repository.
        /// </summary>
        private IRepository<ArR2t4overrideInput> R2T4OverrideInputRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4overrideInput>();

        /// <summary>
        /// The r 2 t 4 override results repository.
        /// </summary>
        private IRepository<ArR2t4overrideResults> R2T4OverrideResultsRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4overrideResults>();

        /// <summary>
        /// The r 2 t 4 results repository.
        /// </summary>
        private IRepository<ArR2t4results> R2T4ResultsRepository => this.unitOfWork.GetRepositoryForEntity<ArR2t4results>();

        /// <summary>
        /// The Create action allows you to a save the student termination details.
        /// </summary>
        /// <remarks>
        /// The Create action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="model">
        /// The Create action method accepts a parameter of type studentTerminationDetails.
        /// </param>
        /// <returns>
        /// Returns an object of type StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType, CreatedDate, CreatedBy, UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        public async Task<StudentTermination> Create(StudentTermination model)
        {
            var terminationResults = new StudentTermination();

            try
            {
                model.CreatedDate = model.UpdatedDate = DateTime.Now;
                var terminationEntity = this.mapper.Map<ArR2t4terminationDetails>(model);
                var terminationDetails = await this.R2T4TerminationDetailRepository.CreateAsync(terminationEntity);
                await this.R2T4TerminationDetailRepository.SaveAsync();
                if (terminationDetails != null)
                {
                    terminationResults = this.mapper.Map<StudentTermination>(terminationDetails);
                    terminationResults.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                }
                else
                {
                    terminationResults.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                terminationResults.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return terminationResults;
        }

        /// <summary>
        /// The Update action allows you to a update the student termination details.
        /// </summary>
        /// <remarks>
        /// The Update action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="studentTerminationDetails">
        /// The studentTerminationDetails object.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </returns>
        public async Task<StudentTermination> Update(StudentTermination studentTerminationDetails)
        {
            //var terminationEntity = this.mapper.Map<ArR2t4terminationDetails>(studentTerminationDetails);
            return await Task.Run(async () =>
            {
                try
                {
                    var updateTerminationDetails = await Task.Run(
                                                       () => this.R2T4TerminationDetailRepository.GetById(
                                                           studentTerminationDetails.TerminationId));
                    if (updateTerminationDetails == null)
                    {
                        return new StudentTermination
                        {
                            ResultStatus =
                                           $"{ApiMsgs.UPDATE_IDS_NO_MATCH} {studentTerminationDetails.TerminationId}"
                        };
                    }
                    else
                    {
                        updateTerminationDetails.TerminationId = studentTerminationDetails.TerminationId;
                        updateTerminationDetails.StuEnrollmentId = studentTerminationDetails.StudentEnrollmentId;
                        updateTerminationDetails.StatusCodeId = studentTerminationDetails.StatusCodeId;
                        updateTerminationDetails.DropReasonId = studentTerminationDetails.DropReasonId;
                        updateTerminationDetails.DateWithdrawalDetermined =
                            studentTerminationDetails.DateWithdrawalDetermined;
                        updateTerminationDetails.LastDateAttended = studentTerminationDetails.LastDateAttended;
                        updateTerminationDetails.IsPerformingR2t4calculator =
                            studentTerminationDetails.IsPerformingR2T4Calculator ?? false;
                        updateTerminationDetails.CalculationPeriodTypeId =
                            studentTerminationDetails.CalculationPeriodType;
                        updateTerminationDetails.UpdatedById = studentTerminationDetails.UpdatedBy;
                        updateTerminationDetails.UpdatedDate = DateTime.Now;
                        updateTerminationDetails.IsR2t4approveTabEnabled =
                            studentTerminationDetails.IsR2T4ApproveTabEnabled;

                        var terminationDetails = this.R2T4TerminationDetailRepository.Update(updateTerminationDetails);
                        await this.R2T4TerminationDetailRepository.SaveAsync();
                        var results = this.mapper.Map<StudentTermination>(terminationDetails);
                        results.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                        return results;
                    }
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new StudentTermination
                    {
                        ResultStatus =
                                       $"{ApiMsgs.UPDATE_UNSUCCESSFULL} {studentTerminationDetails.TerminationId}"
                    };
                }
            });

        }

        /// <summary>
        /// The delete action is to delete all the termination details from database by providing the termination id.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the all related termination details by providing the termination id.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        public async Task<string> Delete(Guid terminationId)
        {
            return await Task.Run(
                       () =>
                           {
                               try
                               {
                                   var r2T4TerminationDetail =
                                       this.R2T4TerminationDetailRepository.GetById(terminationId);
                                   if (r2T4TerminationDetail == null)
                                   {
                                       return $"{ApiMsgs.DELETE_RECORD_NOT_FOUND} {terminationId}";
                                   }

                                   int error = 0; // all good
                                   string message = "";

                                   // get additional information
                                   var additionInformation =
                                       this.R2T4AdditionInformationRepository.Get(
                                           e => e.TerminationId == terminationId);

                                   // if additional information exist then delete it
                                   if (additionInformation != null)
                                   {
                                       this.R2T4AdditionInformationRepository.Delete(additionInformation);
                                   }

                                   // get calculation results
                                   var calculationResults =
                                       this.R2T4CalculationResultsRepository.Get(e => e.TerminationId == terminationId);

                                   // if calculation results exist then delete it
                                   if (calculationResults != null)
                                   {
                                       this.R2T4CalculationResultsRepository.Delete(calculationResults);
                                   }

                                   // get input
                                   var input =
                                       this.R2T4InputRepository.Get(e => e.TerminationId == terminationId);

                                   // if input exist then delete it
                                   if (input != null)
                                   {
                                       this.R2T4InputRepository.Delete(input);
                                   }

                                   // get override input
                                   var overrideInput =
                                       this.R2T4OverrideInputRepository.Get(e => e.TerminationId == terminationId);

                                   // if override input exist then delete it
                                   if (overrideInput != null)
                                   {
                                       this.R2T4OverrideInputRepository.Delete(overrideInput);
                                   }

                                   // get override results
                                   var overrideResults =
                                       this.R2T4OverrideResultsRepository.Get(e => e.TerminationId == terminationId);

                                   // if override results exist then delete it
                                   if (overrideResults != null)
                                   {
                                       this.R2T4OverrideResultsRepository.Delete(overrideResults);
                                   }

                                   // get results
                                   var results =
                                       this.R2T4ResultsRepository.Get(e => e.TerminationId == terminationId);

                                   // if results exist then delete it
                                   if (results != null)
                                   {
                                       this.R2T4ResultsRepository.Delete(results);
                                   }

                                   // we know already it exists, not need for checking r2T4TerminationDetail
                                   this.R2T4TerminationDetailRepository.Delete(r2T4TerminationDetail);


                                   this.unitOfWork.SaveChanges();
                                   return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   return $"{ApiMsgs.DELETE_FAILED} {terminationId}";
                               }
                           });


        }

        /// <summary>
        /// The get student termination validations.
        /// </summary>
        /// <returns>
        /// The <see cref="DynamicValidationRuleSet"/>.
        /// </returns>
        public DynamicValidationRuleSet GetStudentTerminationValidations()
        {
            var set = new DynamicValidationRuleSet("StudentTermination");
            set.Rules.Add("StudentEnrollmentId", new DynamicValidationRule("StudentEnrollmentId", true, true));
            set.Rules.Add("StatusCodeId", new DynamicValidationRule("StatusCodeId", true, true));
            set.Rules.Add("DropReasonId", new DynamicValidationRule("DropReasonId", true, true));
            set.Rules.Add("DateWithdrawalDetermined", new DynamicValidationRule("DateWithdrawalDetermined", true, true));
            set.Rules.Add("LastDateAttended", new DynamicValidationRule("LastDateAttended", true, true));
            set.Rules.Add("IsPerformingR2T4Calculator", new DynamicValidationRule("IsPerformingR2T4Calculator", true, true));
            set.Rules.Add("CalculationPeriodType", new DynamicValidationRule("CalculationPeriodType", true, false));
            set.Rules.Add("UpdatedBy", new DynamicValidationRule("UpdatedBy", true, false));
            set.Rules.Add("UpdatedDate", new DynamicValidationRule("UpdatedDate", true, false));
            set.Rules.Add("CreatedDate", new DynamicValidationRule("CreatedDate", true, true));
            set.Rules.Add("CreatedBy", new DynamicValidationRule("CreatedBy", true, true));
            set.Rules.Add("MethodType", new DynamicValidationRule("MethodType", true, true));
            return set;
        }

        /// <summary>
        /// The Get action is to get all the termination details from database by providing the enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get action is to get the all saved termination details for an incomplete termination by providing the enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns all saved termination details for an incomplete termination based on the enrollmentId.
        /// </returns>
        public async Task<StudentTermination> Get(Guid enrollmentId)
        {
            return await Task.Run(() =>
                {
                    try
                    {
                        var getStudentTerminationQuery =
                            this.R2T4TerminationDetailRepository.Get(e => e.StuEnrollmentId == enrollmentId && e.IsTerminationReversed != true);

                        var studentTerminationEntity = getStudentTerminationQuery.FirstOrDefault();
                        var studentTerminationDetails = this.mapper.Map<StudentTermination>(
                            studentTerminationEntity ?? new ArR2t4terminationDetails());
                        return studentTerminationDetails;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        return new StudentTermination
                        {
                            ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                        };
                    }
                });
        }

        /// <summary>
        /// The Get action is to get all the termination details from database by providing the enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get action is to get the all saved termination details for an incomplete termination by providing the enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns all saved termination details for an incomplete termination based on the enrollmentId.
        /// </returns>
        public async Task<StudentTermination> GetById(Guid enrollmentId)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var getStudentTerminationQuery =
                        this.R2T4TerminationDetailRepository.Get(e => e.StuEnrollmentId == enrollmentId && e.IsTerminationReversed == false);

                    var studentTerminationEntity = getStudentTerminationQuery.FirstOrDefault();
                    var studentTerminationDetails = this.mapper.Map<StudentTermination>(studentTerminationEntity);
                    return studentTerminationDetails;
                }
                catch (Exception e)
                {
                    e.TrackException();
                    return new StudentTermination
                    {
                        ResultStatus = $"{ApiMsgs.FAILED_GETTING_RECORD}"
                    };
                }
            });
        }

        /// <summary>
        /// The Update action allows you to a update the student termination details during the undo operation.
        /// </summary>
        /// <remarks>
        /// The Update action requires StudentTerminationDetails object which has TerminationId, StuEnrollmentId, StatusCodeId, DropReasonId, DateWithdrawalDetermined, 
        /// LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled.
        /// </remarks>
        /// <param name="studentTerminationDetails">
        /// The studentTerminationDetails object.
        /// </param>
        /// <returns>
        /// Returns an object of StudentTermination where StudentTermination has the TerminationId, StudentEnrollmentId, StatusCodeId, DropReasonId, 
        /// DateWithdrawalDetermined, LastDateAttended, IsPerformingR2T4Calculator, CalculationPeriodType,  UpdatedBy, UpdatedDate, IsR2T4ApproveTabEnabled and IsTerminationReversed.
        /// </returns>
        public async Task<StudentTermination> UpdateUndoTermination(StudentTermination studentTerminationDetails)
        {
            return await Task.Run(
                       () =>
                           {
                               try
                               {
                                   var updateTerminationDetails = this.R2T4TerminationDetailRepository.Get(s => s.TerminationId == studentTerminationDetails.TerminationId && s.IsTerminationReversed == false).FirstOrDefault();

                                   if (updateTerminationDetails == null)
                                   {
                                       return new StudentTermination
                                       {
                                           ResultStatus = $"{ApiMsgs.UPDATE_IDS_NO_MATCH} {studentTerminationDetails.TerminationId}"
                                       };
                                   }
                                   else
                                   {
                                       updateTerminationDetails.UpdatedById = studentTerminationDetails.UpdatedBy;
                                       updateTerminationDetails.UpdatedDate = DateTime.Now;
                                       updateTerminationDetails.IsTerminationReversed = true;

                                       var terminationDetails =
                                           this.R2T4TerminationDetailRepository.Update(updateTerminationDetails);
                                       this.R2T4TerminationDetailRepository.Save();
                                       var results = this.mapper.Map<StudentTermination>(terminationDetails);
                                       results.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                                       return results;
                                   }
                               }
                               catch (Exception)
                               {
                                   return new StudentTermination
                                   {
                                       ResultStatus =
                                                      $"{ApiMsgs.UPDATE_UNSUCCESSFULL} {studentTerminationDetails.TerminationId}"
                                   };
                               }
                           });
        }

        /// <summary>
        /// The get student termination status code.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<StatusCode>> GetStudentTerminationStatusCode(Guid studentEnrollmentId, Guid campusId)
        {
            return await Task.Run(
                      async () =>
                           {
                               List<StatusCode> results = new List<StatusCode>();

                               bool haveAttendanceOrGrades = await this.enrollmentService.HaveAttendanceOrGradePosted(studentEnrollmentId);


                               var currentlyAttendingStatuses = (await this.statusCodesService.GetStudentStatusCodes(
                                                                     campusId,
                                                                     StudentStatuses.Dropped)).ToList();


                               foreach (var statusCode in currentlyAttendingStatuses)
                               {
                                   results.Add(new StatusCode()
                                   {
                                       StatusCodeId = statusCode.Value.ToString(),
                                       StatusCodeDescription = statusCode.Text,
                                       SystemStatusCodeId = (int)StudentStatuses.Dropped
                                   });
                               }

                               if (!haveAttendanceOrGrades)
                               {
                                   var noStartStatuses = (await this.statusCodesService.GetStudentStatusCodes(
                                                              campusId,
                                                              StudentStatuses.NoStart)).ToList();

                                   foreach (var statusCode in noStartStatuses)
                                   {
                                       results.Add(new StatusCode()
                                                       {
                                                           StatusCodeId = statusCode.Value.ToString(),
                                                           StatusCodeDescription = statusCode.Text,
                                                           SystemStatusCodeId = (int)StudentStatuses.NoStart
                                       });
                                   }
                               }


                               return results.OrderBy(x => x.StatusCodeDescription).ToList();
                           });
        }
    }
}