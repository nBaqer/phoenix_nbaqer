﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GradesService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the GradesService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Interfaces;
    using Interfaces.AcademicRecords;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// The grades service.
    /// </summary>
    public class GradesService : IGradesService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the Grades Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradesService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        public GradesService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService, IEnrollmentService enrollmentService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.enrollmentService = enrollmentService;
        }

        /// <summary>
        /// The grade book results repository.
        /// </summary>
        private IRepository<ArResults> ArResults => this.context.GetRepositoryForEntity<ArResults>();

        /// <summary>
        /// The grade book results repository.
        /// </summary>
        private IRepository<ArClassSections> ArClassSections => this.context.GetRepositoryForEntity<ArClassSections>();

        /// <summary>
        /// The student enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> ArStuEnrollments => this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The program versions repository.
        /// </summary>
        private IRepository<ArPrgVersions> ArPrgVersions => this.context.GetRepositoryForEntity<ArPrgVersions>();


        /// <summary>
        /// The ar terms repository.
        /// </summary>
        private IRepository<ArTerm> ArTerms => this.context.GetRepositoryForEntity<ArTerm>();

        /// <summary>
        /// The ar grade system details repository.
        /// </summary>
        private IRepository<ArGradeSystemDetails> ArGradeSystemDetails => this.context.GetRepositoryForEntity<ArGradeSystemDetails>();

        /// <summary>
        /// The grade book results repository.
        /// </summary>
        private IRepository<ArGrdBkResults> GradeBookResults => this.context.GetRepositoryForEntity<ArGrdBkResults>();

        /// <summary>
        /// The grade book weight details repository.
        /// </summary>
        private IRepository<ArGrdBkWgtDetails> GrdbkWeightDetails => this.context.GetRepositoryForEntity<ArGrdBkWgtDetails>();


        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        ///  Given PostFinalGrade DTO parameters, posts the final grade for a given student in a given course
        /// </summary>
        /// <param name="parameters">
        /// </param>
        /// <returns></returns>
        public async Task<ActionResult<PostFinalGrade>> PostFinalGradeByStudentEnrollmentId(PostFinalGrade parameters)
        {
            var rv = new ActionResult<PostFinalGrade>();

            try
            {
                //tables
                var arClassSections = this.ArClassSections.Get();
                var arResults = this.ArResults.Get();
                var arStuEnrollments = this.ArStuEnrollments.Get();
                var arTerms = this.ArTerms.Get();
                var arPrgVersions = this.ArPrgVersions.Get();
                var arGradeSystemDetails = this.ArGradeSystemDetails.Get();
                Guid? grdSysDetailId = Guid.Empty;
                var isNumeric = true;

                //validation
                var classSection = await arClassSections.Where(a => a.ClsSectionId == parameters.ClassSectionId).Select(a => new { a.ClsSectionId, a.CampusId }).FirstOrDefaultAsync();

                if (classSection == null)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Class section with id : {parameters.ClassSectionId} was not found.";
                    return rv;
                }

                if (!classSection.CampusId.HasValue)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Campus could not be found based on class section id.";
                    return rv;
                }

                var gradesFormat = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus("GradesFormat", classSection.CampusId.Value);
                isNumeric = gradesFormat.ToLower() == GradesFormat.Numeric.ToLower();

                if (String.IsNullOrEmpty(gradesFormat))
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Grades format campus setting not found.";
                    return rv;
                }

                // enrollment exists
                var studentExists = await arStuEnrollments.AnyAsync(a => a.StuEnrollId == parameters.StudentEnrollmentId);

                if (!studentExists)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Student with StudentEnrollmentId of {parameters.StudentEnrollmentId} does not exist.";
                    return rv;
                }

                //student is registered for course
                var studentIsRegisteredInCourse = await (from a in arResults
                                                         where a.TestId == parameters.ClassSectionId && a.StuEnrollId == parameters.StudentEnrollmentId
                                                         select a).AnyAsync();

                if (!studentIsRegisteredInCourse)
                {
                    rv.ResultStatus = Enums.ResultStatus.Error;
                    rv.ResultStatusMessage = $"Student is not registered in class section with id : {parameters.ClassSectionId}.";
                    return rv;
                }

                //grade format validations 
                if (isNumeric) //numeric validation
                {
                    if (!parameters.Score.HasValue || !(parameters.Score.HasValue && parameters.Score >= 0 && parameters.Score <= 100))
                    {
                        rv.ResultStatus = Enums.ResultStatus.Error;
                        rv.ResultStatusMessage = $"For a numeric grade format school, score must be between 0 and 100. Value sent was {parameters.Score}.";
                        return rv;
                    }
                }
                else //letter validation
                {
                    if (string.IsNullOrEmpty(parameters.LetterGrade))
                    {
                        rv.ResultStatus = Enums.ResultStatus.Error;
                        rv.ResultStatusMessage = $"LetterGrade parameter is required for letter grade format schools. Value sent was {parameters.LetterGrade}.";
                        return rv;
                    }

                    grdSysDetailId = await (from a in arStuEnrollments
                                            join b in arPrgVersions on a.PrgVerId equals b.PrgVerId
                                            join c in arGradeSystemDetails on b.GrdSystemId equals c.GrdSystemId
                                            where c.Grade != null && c.Grade.Trim().ToLower() == parameters.LetterGrade.Trim().ToLower() &&
                                            a.StuEnrollId == parameters.StudentEnrollmentId
                                            select (Guid?)c.GrdSysDetailId).FirstOrDefaultAsync();

                    if (!grdSysDetailId.HasValue || grdSysDetailId.Value == Guid.Empty)
                    {
                        rv.ResultStatus = Enums.ResultStatus.Error;
                        rv.ResultStatusMessage = $"LetterGrade passed is not part of grade system for student's program or could not be found. Value sent was {parameters.LetterGrade}.";
                        return rv;
                    }
                }

                //end validation

                //settings
                var campusId = classSection.CampusId.Value;
                var existingArResultRecord = await arResults.Where(a => a.StuEnrollId == parameters.StudentEnrollmentId && a.TestId == parameters.ClassSectionId).FirstOrDefaultAsync();

                if (existingArResultRecord != null)
                {
                    existingArResultRecord.Score = parameters.Score;
                    existingArResultRecord.IsCourseCompleted = true;
                    existingArResultRecord.IsClinicsSatisfied = true;
                    existingArResultRecord.GrdSysDetailId = null;
                    existingArResultRecord.ModUser = this.TenantUser.Email;
                    existingArResultRecord.ModDate = DateTime.Now;
                    existingArResultRecord.IsGradeOverridden = true;
                    existingArResultRecord.GradeOverriddenBy = this.TenantUser.Email;
                    existingArResultRecord.GradeOverriddenDate = DateTime.Now;

                    if (!isNumeric)
                    {
                        existingArResultRecord.Score = null;
                        existingArResultRecord.IsInComplete = null;
                        existingArResultRecord.GrdSysDetailId = grdSysDetailId;
                    }

                    this.ArResults.Update(existingArResultRecord);
                }
                else
                {
                    var newRecord = new ArResults();
                    newRecord.Score = parameters.Score;
                    newRecord.IsCourseCompleted = true;
                    newRecord.IsClinicsSatisfied = true;
                    newRecord.GrdSysDetailId = null;
                    newRecord.ModUser = this.TenantUser.Email;
                    newRecord.ModDate = DateTime.Now;
                    newRecord.IsGradeOverridden = true;
                    newRecord.GradeOverriddenBy = this.TenantUser.Email;
                    newRecord.GradeOverriddenDate = DateTime.Now;

                    newRecord.Cnt = 0;
                    newRecord.Hours = 0;
                    newRecord.DroppedInAddDrop = null;
                    newRecord.IsTransfered = null;
                    newRecord.DateDetermined = DateTime.Now;
                    newRecord.DateCompleted = null;

                    if (!isNumeric)
                    {
                        newRecord.Score = null;
                        newRecord.IsInComplete = null;
                        newRecord.GrdSysDetailId = grdSysDetailId;
                    }

                    await this.ArResults.CreateAsync(newRecord);
                }

                this.context.SaveChanges();
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = $"Successfully posted final grade for student.";
                return rv;
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }

        /// <summary>
        /// Post a students grade for a specific component.
        /// </summary>
        /// <param name="gradeInput">
        /// The dto for posting a component grade requiring a student enrollment id, a class section id, a score and a component id.
        /// </param>
        /// <returns>
        /// The api status output with status message.
        /// </returns>
        public ApiStatusOutput PostStudentGradeForComponent(StudentPostGradesInput gradeInput)
        {
            var output = new ApiStatusOutput();
            gradeInput.ResultNumber = gradeInput.ResultNumber.HasValue && gradeInput.ResultNumber.Value == 0 ? 1 : gradeInput.ResultNumber; //default result number to 1 if not passed
            gradeInput.ResultNumber = gradeInput.ResultNumber.HasValue ? gradeInput.ResultNumber : 1;
            output.HasPassed = true;

            if (gradeInput.StuEnrollId == Guid.Empty || gradeInput.ComponentId == Guid.Empty || gradeInput.ClassSectionId == Guid.Empty)
            {
                output.HasPassed = false;
                output.ResultStatus = "Missing required parameter for grade input.";
                return output;
            }

            if (gradeInput.Score > 100)
            {
                output.HasPassed = false;
                output.ResultStatus = "Score cannot be greater than 100.";
                return output;
            }

            var grdbkResult = GradeBookResults.Get(gbr => gbr.StuEnrollId == gradeInput.StuEnrollId
            && gbr.ClsSectionId == gradeInput.ClassSectionId
            && gbr.InstrGrdBkWgtDetail.GrdComponentTypeId == gradeInput.ComponentId
            && (gbr.ResNum == gradeInput.ResultNumber || (gbr.ResNum == 0 && gradeInput.ResultNumber == 1)))
            .OrderBy(x => x.ResNum).FirstOrDefault();

            var enrollment = this.ArStuEnrollments.Get(enr => enr.StuEnrollId == gradeInput.StuEnrollId).FirstOrDefault();

            if (gradeInput.PostDate.HasValue && enrollment.StartDate.HasValue && gradeInput.PostDate < enrollment.StartDate)
            {
                output.HasPassed = false;
                output.ResultStatus = $"Post date parameter passed must be later or equal to students startdate of {enrollment.StartDate.Value.ToShortDateString() } .";
                return output;
            }

            if (!gradeInput.PostDate.HasValue)
            {
                gradeInput.PostDate = enrollment.StartDate ?? DateTime.Now; //default post date to start date else date time now if not passed
            }

            if (enrollment != null)
            {
                Guid campusId = enrollment.CampusId;

                var gradesFormat = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.GradesFormat,
                campusId);

                var gradeBookWeightingLevel = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.GradeBookWeightingLevel,
               campusId);


                if (gradesFormat.Equals(Infrastructure.GradesFormat.Letter, StringComparison.InvariantCultureIgnoreCase))
                {
                    var classSection = this.ArClassSections.Get(cs => cs.ClsSectionId == gradeInput.ClassSectionId).Include(cs => cs.GrdScale).ThenInclude(gs => gs.ArGradeScaleDetails)
                        .FirstOrDefault();

                    if (classSection != null && classSection.GrdScale != null && classSection.GrdScale.ArGradeScaleDetails.Any())
                    {
                        var minScoreAllowed = classSection.GrdScale.ArGradeScaleDetails.Min(gsd => gsd.MinVal);
                        var maxScoreAllowed = classSection.GrdScale.ArGradeScaleDetails.Max(gsd => gsd.MaxVal);


                        if (gradeInput.Score < minScoreAllowed || gradeInput.Score > maxScoreAllowed)
                        {
                            //return validation error -- invalid score for existing grade scale
                            output.HasPassed = false;
                            output.ResultStatus = "Invalid score for existing grade scale for given class section.";
                            return output;
                        }
                    }

                    else
                    {
                        //return validation error -- no gradescale found
                        output.HasPassed = false;
                        output.ResultStatus = "No grade scale found for given class section.";
                        return output;
                    }


                }
                if (grdbkResult != null)
                {
                    grdbkResult.Score = gradeInput.Score;
                    grdbkResult.ModDate = DateTime.Now;
                    grdbkResult.ModUser = this.TenantUser.Email;
                    grdbkResult.PostDate = gradeInput.PostDate;
                    grdbkResult.ResNum = grdbkResult.ResNum == 0 ? 1 : grdbkResult.ResNum;
                    grdbkResult.Comments = "";
                    GradeBookResults.Update(grdbkResult);
                    GradeBookResults.Save();

                    output.ResultStatus = "Score has been successfully updated for the specified component.";
                }
                //create new grade entry
                else
                {
                    Expression<Func<ArGrdBkWgtDetails, bool>> grdbkWeightDetailQuery =
                        gbwd => gbwd.GrdComponentTypeId == gradeInput.ComponentId && gbwd.InstrGrdBkWgt.StatusId == this.ActiveStatusId;


                    if (gradeBookWeightingLevel.Equals(Infrastructure.GradeBookWeightLevel.CourseLevel, StringComparison.InvariantCultureIgnoreCase))
                    {

                        var reqId = this.ArClassSections.Get(cs => cs.ClsSectionId == gradeInput.ClassSectionId).Select(cs => cs.ReqId).FirstOrDefault();

                        grdbkWeightDetailQuery = grdbkWeightDetailQuery.And(gbwd => gbwd.InstrGrdBkWgt.ReqId == reqId);

                    }
                    else
                    {
                        grdbkWeightDetailQuery = grdbkWeightDetailQuery.And(gbwd => gbwd.InstrGrdBkWgt.Instructor.ArClassSections.Any(cs => cs.ClsSectionId == gradeInput.ClassSectionId));
                    }
                    var instrGrdbkWeightDetail = this.GrdbkWeightDetails
                        .Get(grdbkWeightDetailQuery).FirstOrDefault();

                    if (instrGrdbkWeightDetail != null)
                    {
                        var newGrdbkResult = new ArGrdBkResults
                        {
                            StuEnrollId = gradeInput.StuEnrollId,
                            ClsSectionId = gradeInput.ClassSectionId,
                            Score = gradeInput.Score,
                            InstrGrdBkWgtDetailId = instrGrdbkWeightDetail.InstrGrdBkWgtDetailId,
                            IsCompGraded = true,
                            ResNum = gradeInput.ResultNumber.Value,
                            ModDate = DateTime.Now,
                            ModUser = this.TenantUser.Email,
                            Comments = "",
                            PostDate = gradeInput.PostDate
                        };
                        GradeBookResults.Create(newGrdbkResult);
                        GradeBookResults.Save();

                        output.ResultStatus = "Score has been successfully created for the specified component.";
                    }
                    else
                    {
                        output.HasPassed = false;
                        output.ResultStatus = "Could not find the instructor grade weight details for the specified component.";
                    }


                }

            }
            else
            {
                output.HasPassed = false;
                output.ResultStatus = "No enrollment found for given student enrollment id.";
            }

            return output;
        }

        /// <summary>
        /// Returns all the component grades for a specific student.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The students enrollment id.
        /// </param>
        /// <returns>
        /// The api output result.
        /// </returns>
        public async Task<ActionResult<List<StudentComponentGrades>>> GetStudentComponentGradesById(Guid stuEnrollId)
        {
            var rv = new ActionResult<List<StudentComponentGrades>>();

            try
            {
                //tables
                var arGradeBkResults = this.GradeBookResults.Get();
                var arGrdBkWgtDetails = this.GrdbkWeightDetails.Get();
                var dataGroup = (from a in arGradeBkResults.Include(x => x.InstrGrdBkWgtDetail)
                                 where a.StuEnrollId == stuEnrollId
                                 group a by new { a.StuEnrollId, a.InstrGrdBkWgtDetailId, a.ClsSectionId } into a_
                                 select a_);

                var data = await dataGroup
                     .Select(x => new StudentComponentGrades()
                     {
                         ClsSectionId = x.Key.ClsSectionId,
                         Description = string.IsNullOrEmpty(x.FirstOrDefault().InstrGrdBkWgtDetail.Descrip) ? "" : x.FirstOrDefault().InstrGrdBkWgtDetail.Descrip.Trim(),
                         ComponentId = x.FirstOrDefault().InstrGrdBkWgtDetail.GrdComponentTypeId,
                         Grades = x.Select(i => new ComponentAttempt { ResultNumber = i.ResNum, Score = i.Score, PostDate = i.PostDate }).ToList()
                     }).ToListAsync();

                rv.Result = data;
                rv.ResultStatus = Enums.ResultStatus.Success;
                rv.ResultStatusMessage = $"Successfully returned students grades.";
                return rv;
            }
            catch (Exception e)
            {
                rv.ResultStatus = Enums.ResultStatus.Error;
                rv.ResultStatusMessage = e.Message;
                return rv;
            }
        }
    }
}