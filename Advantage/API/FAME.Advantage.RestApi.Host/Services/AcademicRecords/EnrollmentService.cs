﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the EnrollmentService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Extensions;

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using AutoMapper;
    using DataTransferObjects.AcademicRecords.Enrollment;
    using DataTransferObjects.SystemCatalog;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Extensions;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Enrollment.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentSummary;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Enrollment;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using Interfaces;
    using Interfaces.AcademicRecords;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore;
    using Orm.Advantage.Domain.Common;
    using Remotion.Linq.Clauses;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    /// <summary>
    /// The enrollment service.
    /// </summary>
    public class EnrollmentService : IEnrollmentService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly IUserService userService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The system status service.
        /// </summary>
        private readonly ISystemStatusService systemStatusService;

        /// <summary>
        /// The phone service.
        /// </summary>
        private readonly ILeadsPhoneService phoneService;

        /// <summary>
        /// The email service.
        /// </summary>
        private readonly ILeadsEmailService emailService;

        /// <summary>
        /// The address service.
        /// </summary>
        private readonly ILeadsAddressService addressService;


        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="userService">
        /// The user Service.
        /// </param>
        /// <param name="systemStatusService">
        /// The system Status Service.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="systemConfigurationAppSettingService">
        /// The system Configuration App Setting Service.
        /// </param>
        /// <param name="phoneService">
        /// The phone Service.
        /// </param>
        /// <param name="emailService">
        /// The email Service.
        /// </param>
        /// <param name="addressService">
        /// The address Service.
        /// </param>
        public EnrollmentService(IAdvantageDbContext context, IMapper mapper, IUserService userService, ISystemStatusService systemStatusService, IStatusesService statusService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService, ILeadsPhoneService phoneService, ILeadsEmailService emailService, ILeadsAddressService addressService)
        {
            this.context = context;
            this.mapper = mapper;
            this.userService = userService;
            this.systemStatusService = systemStatusService;
            this.statusService = statusService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
            this.addressService = addressService;
            this.phoneService = phoneService;
            this.emailService = emailService;
        }

        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.context.GetRepositoryForEntity<SyCampuses>();

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.context.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The extern ship attendance.
        /// </summary>
        private IRepository<ArExternshipAttendance> ExternShipAttendance => this.context.GetRepositoryForEntity<ArExternshipAttendance>();

        /// <summary>
        /// The class section attendance.
        /// </summary>
        private IRepository<AtClsSectAttendance> ClassSectionAttendance => this.context.GetRepositoryForEntity<AtClsSectAttendance>();

        /// <summary>
        /// The student clock attendance.
        /// </summary>
        private IRepository<ArStudentClockAttendance> StudentClockAttendance => this.context.GetRepositoryForEntity<ArStudentClockAttendance>();

        /// <summary>
        /// The conversion attendance.
        /// </summary>
        private IRepository<AtConversionAttendance> ConversionAttendance => this.context.GetRepositoryForEntity<AtConversionAttendance>();

        /// <summary>
        /// The credit summary.
        /// </summary>
        private IRepository<SyCreditSummary> CreditSummary => this.context.GetRepositoryForEntity<SyCreditSummary>();

        /// <summary>
        /// The program version definition repository
        /// </summary>
        private IRepository<ArProgVerDef> ProgramVersionDefinition => this.context.GetRepositoryForEntity<ArProgVerDef>();

        /// <summary>
        /// The grade components repository
        /// </summary>
        private IRepository<ArGrdComponentTypes> GradeComponentTypes => this.context.GetRepositoryForEntity<ArGrdComponentTypes>();

        /// <summary>
        /// The ar class section repository
        /// </summary>
        private IRepository<ArClassSections> ClassSection => this.context.GetRepositoryForEntity<ArClassSections>();

        /// <summary>
        /// The grade book details repository.
        /// </summary>
        private IRepository<ArGrdBkWgtDetails> GradeBookWeightDetails => this.context.GetRepositoryForEntity<ArGrdBkWgtDetails>();

        /// <summary>
        /// The grade book results repository.
        /// </summary>
        private IRepository<ArGrdBkResults> GradeBookResults => this.context.GetRepositoryForEntity<ArGrdBkResults>();

        /// <summary>
        /// The integration enrollment tracking repository.
        /// </summary>
        private IRepository<IntegrationEnrollmentTracking> EnrollmentTracking => this.context.GetRepositoryForEntity<IntegrationEnrollmentTracking>();
        /// <summary>
        /// The Student LOA repository.
        /// </summary>
        private IRepository<ArStudentLoas> StudentLoas => this.context.GetRepositoryForEntity<ArStudentLoas>();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The GetEnrollmentsByStudentId Gets the list of enrollment(s) for the given studentId
        ///  and each enrollment listed is displayed as StudentEnrollment name Status Effective date of the current status'. Example: 'Cosmetology - Enrolled 05/01/2017'
        /// </summary>
        /// <remarks>
        /// The GetEnrollmentsByStudentId action requires studentId object which is a Guid.
        /// </remarks>
        /// <param name="enrollmentDetail">
        /// The enrollment Detail.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of <see cref="Enrollment"/> where StudentEnrollment has the EnrollmentId,ProgramVersionDescription,StatusDescription,EffectiveDate,Status,EnrollmentDate
        /// </returns>
        public async Task<IEnumerable<Enrollment>> GetEnrollmentsByStudentId(EnrollmentStatus enrollmentDetail)
        {
            return await Task.Run(() =>
            {
                IList<ArStuEnrollments> studentEnrollmentDetails;
                IEnumerable<Enrollment> enrollments = null;
                if (enrollmentDetail.IsActiveEnrollments)
                {
                    studentEnrollmentDetails = this.EnrollmentRepository
                        .Get(se => se.StudentId == enrollmentDetail.StudentId)
                        .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                        .Include(en => en.PrgVer.ArCampusPrgVersions)
                        .Include(se => se.Lead).Include(se => se.StatusCode)
                        .Include(se => se.SyStudentStatusChanges).ToList();

                    enrollments = this.mapper.Map<IEnumerable<Enrollment>>(studentEnrollmentDetails).OrderBy(_ => _.Status).ThenBy(_ => _.ProgramVersionDescription).ToList();

                    List<int> activeStatusIds =
                        new List<int>
                        {
                        (int)Constants.SystemStatus.FutureStart,
                        (int)Constants.SystemStatus.CurrentlyAttending,
                        (int)Constants.SystemStatus.LeaveOfAbsence,
                        (int)Constants.SystemStatus.Suspension,
                        (int)Constants.SystemStatus.AcademicProbation,
                        (int)Constants.SystemStatus.Externship,
                        (int)Constants.SystemStatus.DisciplinaryProbation,
                        (int)Constants.SystemStatus.WarningProbation,
                        (int)Constants.SystemStatus.NoStart
                        };

                    foreach (Enrollment enrollment in enrollments)
                    {
                        enrollment.Status = activeStatusIds.Contains(enrollment.SystemStatusId) ? "Active" : "Inactive";
                    }
                }
                else if (enrollmentDetail.IsDroppedEnrollments)
                {
                    studentEnrollmentDetails = this.EnrollmentRepository
                        .Get(se => se.StudentId == enrollmentDetail.StudentId && (se.StatusCode.SysStatusId == (int)Constants.SystemStatus.Dropped || se.StatusCode.SysStatusId == (int)Constants.SystemStatus.NoStart))
                        .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                        .Include(en => en.PrgVer.ArCampusPrgVersions)
                        .Include(se => se.Lead).Include(se => se.StatusCode)
                        .Include(se => se.SyStudentStatusChanges).ToList();
                    enrollments = this.mapper.Map<IEnumerable<Enrollment>>(studentEnrollmentDetails).OrderBy(_ => _.Status).ThenBy(_ => _.ProgramVersionDescription).ToList();

                    foreach (Enrollment enrollment in enrollments)
                    {
                        enrollment.Status = "Active";
                    }
                }

                return enrollments?.ToList();
            });
        }

        /// <summary>
        /// The get enrollment detail action returns enrollment detail of student by enrollment.
        /// </summary>
        /// <remarks>
        /// The get enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId. 
        /// Based on the EnrollmentId and StudentId, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="studentId">
        /// The student id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        public async Task<Enrollment> GetEnrollmentDetail(Guid enrollmentId, Guid studentId)
        {
            return await Task.Run(() =>
            {
                ArStuEnrollments enrollmentDetail = this.EnrollmentRepository.Get(s => s.StudentId == studentId && s.StuEnrollId == enrollmentId)
                    .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                    .Include(en => en.PrgVer.ArCampusPrgVersions)
                    .Include(se => se.Lead)
                    .Include(se => se.StatusCode)
                    .Include(se => se.SyStudentStatusChanges)
                    .FirstOrDefault();
                return this.mapper.Map<Enrollment>(enrollmentDetail);
            });
        }
        /// <summary>
        /// Get all the enrollment details for afa sync given the leadId
        /// </summary>
        /// <param name="leadId"></param>
        /// <returns></returns>
        public async Task<IList<AdvStagingEnrollmentV2>> GetAllLeadEnrollmentsForAfaSync(Guid leadId)
        {
            List<AdvStagingEnrollmentV2> enrollments = new List<AdvStagingEnrollmentV2>();
            List<Guid> enrollmentsSearched = this.EnrollmentRepository.Get(e => e.LeadId == leadId)
                .Select(e => e.StuEnrollId).ToList();
            if (enrollmentsSearched.Any())
            {
                var results = await this.GetAfaStudentEnrollmentDetails(enrollmentsSearched);

                enrollments = results.Result
                    .Select(x =>
                        new AdvStagingEnrollmentV2()
                        {
                            SISEnrollmentID = x.StudentEnrollmentId,
                            StartDate = x.StartDate.GetValueOrDefault(),
                            ProgramName = x.ProgramVersionName,
                            ProgramVersionID = x.ProgramVersionId,
                            EnrollmentStatus = x.EnrollmentStatus,
                            SSN = x.SSN,
                            StatusEffectiveDate = x.StatusEffectiveDate,
                            IDStudent = x.AfaStudentId.GetValueOrDefault(),
                            LocationCMSID = x.CMSId,
                            ReturnFromLOADate = x.ReturnFromLOADate,
                            LeaveOfAbsenceDate = x.LeaveOfAbsenceDate,
                            LastDateAttendance = x.LastDateAttendance,
                            GradDate = x.GradDate.GetValueOrDefault(),
                            AdmissionsCriteria = x.AdmissionsCriteria,
                            DroppedDate = x.DroppedDate,
                            DateCreated = x.ModDate,
                            DateUpdated = x.ModDate,
                            UserCreated = x.ModUser,
                            UserUpdated = x.ModUser
                        }).ToList();

                return enrollments;
            }

            return enrollments;
        }

        /// <summary>
        /// The get all enrollments for AFA sync.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IList<AdvStagingEnrollmentV2>> GetAllEnrollmentsForAfaSync(Guid campusId)
        {
            List<AdvStagingEnrollmentV2> enrollments = new List<AdvStagingEnrollmentV2>();
            string cmsId = this.CampusRepository.Get(c => c.CampusId == campusId).Select(c => c.CmsId)
                .FirstOrDefault();
            if (string.IsNullOrEmpty(cmsId))
            {
                return enrollments;
            }
            else
            {
                List<Guid> enrollmentsSearched = this.EnrollmentRepository
                    .Get(
                        a => a.CampusId == campusId
                             && a.Lead.AfaStudentId.HasValue).Select(a => a.StuEnrollId)
                    .ToList();

                if (enrollmentsSearched.Any())
                {
                    var results = await this.GetAfaStudentEnrollmentDetails(enrollmentsSearched);

                    enrollments = results.Result
                        .Select(x =>
                            new AdvStagingEnrollmentV2()
                            {
                                SISEnrollmentID = x.StudentEnrollmentId,
                                StartDate = x.StartDate.GetValueOrDefault(),
                                ProgramName = x.ProgramVersionName,
                                ProgramVersionID = x.ProgramVersionId,
                                EnrollmentStatus = x.EnrollmentStatus,
                                SSN = x.SSN,
                                StatusEffectiveDate = x.StatusEffectiveDate,
                                IDStudent = x.AfaStudentId.GetValueOrDefault(),
                                LocationCMSID = x.CMSId,
                                ReturnFromLOADate = x.ReturnFromLOADate,
                                LeaveOfAbsenceDate = x.LeaveOfAbsenceDate,
                                LastDateAttendance = x.LastDateAttendance,
                                GradDate = x.GradDate.GetValueOrDefault(),
                                AdmissionsCriteria = x.AdmissionsCriteria,
                                DroppedDate = x.DroppedDate,
                                DateCreated = x.ModDate,
                                DateUpdated = x.ModDate,
                                UserCreated = x.ModUser,
                                UserUpdated = x.ModUser
                            }).ToList();

                    return enrollments;
                }

                return enrollments;
            }
        }

        /// <summary>
        /// The get enrollments by campus for afa.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The List of enrollments.
        /// </returns>
        public IEnumerable<Guid> GetEnrollmentsByCampusForAFA(Guid campusId)
        {
            List<Guid> enrollments = new List<Guid>();
            string cmsId = this.CampusRepository.Get(c => c.CampusId == campusId).Select(c => c.CmsId).FirstOrDefault();
            if (string.IsNullOrEmpty(cmsId))
            {
                return enrollments;
            }

            enrollments = this.EnrollmentRepository.Get(a =>
                    a.CampusId == campusId && a.Lead.AfaStudentId.HasValue)
                .Select(e => e.StuEnrollId).ToList();
            return enrollments;
        }

        /// <summary>
        /// The get student start date.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id
        /// </param>
        /// <returns>
        /// The <see cref="DateTime"/>.
        /// </returns>
        public DateTime? GetStudentStartDate(Guid enrollmentId)
        {
            return this.EnrollmentRepository.Get(se => se.StuEnrollId == enrollmentId).Select(se => se.StartDate)
                .FirstOrDefault();
        }

        /// <summary>
        /// The get student leave of absences.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id
        /// </param>
        /// <returns>
        /// The list of LOA.
        /// </returns>
        public IList<LOA> GetStudentLOAs(Guid enrollmentId)
        {
            return this.EnrollmentRepository.Get(se => se.StuEnrollId == enrollmentId)
                .Select(_ =>
                    _.ArStudentLoas.GroupBy(loa => new { loa.LoareturnDate, loa.StartDate })
                        .Select(g => new LOA()
                        {
                            Count = g.Count(),
                            ReturnDate = g.FirstOrDefault().LoareturnDate,
                            StartDate = g.FirstOrDefault().StartDate,
                            EndDate = g.FirstOrDefault().EndDate,
                            StudentEnrollmentId = g.FirstOrDefault().StuEnrollId
                        }).ToList()).FirstOrDefault();
        }

        /// <summary>
        /// The Get action method returns enrollment details by given enrollment id.
        /// </summary>
        /// <remarks>
        /// The get enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId. 
        /// Based on the EnrollmentId and StudentId, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>       
        /// <returns>
        /// Returns Enrollment entity where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        public async Task<Enrollment> Get(Guid enrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               ArStuEnrollments enrollmentDetail = this.EnrollmentRepository
                                   .Get(se => se.StuEnrollId == enrollmentId)
                                   .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                                   .Include(en => en.PrgVer.ArCampusPrgVersions)
                                   .FirstOrDefault();
                               return this.mapper.Map<Enrollment>(enrollmentDetail);
                           });
        }

        /// <summary>
        /// The Get action method returns enrollment details by given list of enrollment id.
        /// </summary>
        /// <param name="enrollmentIds">
        /// The enrollment ids is a required field and is a list of type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of StudentEnrollment where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        public async Task<IEnumerable<ProgramVersionSAP>> Get(IEnumerable<Guid> enrollmentIds)
        {
            return await Task.Run(() =>
            {
                List<ArStuEnrollments> stuEnrollments = this.EnrollmentRepository
                    .Get(l => enrollmentIds.Contains(l.StuEnrollId))
                    .Include(en => en.StatusCode)
                    .Include(en => en.PrgVer).ThenInclude(en => en.Fasap)
                    .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                    .Include(en => en.PrgVer.ArCampusPrgVersions)
                    .Include(en => en.PrgVer.Prog).ThenInclude(prog => prog.Ac)
                    .ToList();

                List<ProgramVersionSAP> enrollments = new List<ProgramVersionSAP>();

                foreach (ArStuEnrollments enrollment in stuEnrollments)
                {
                    enrollments.Add(this.mapper.Map<ProgramVersionSAP>(enrollment));
                }

                return enrollments;
            });
        }

        /// <summary>
        /// The Get action method returns enrollment details for SAP by given program version id.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <param name="systemStatuses">
        /// The system statuses
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<ProgramVersionSAP>> GetByProgramVersion(Guid programVersionId, List<Constants.SystemStatus> systemStatuses = null)
        {
            return await Task.Run(() =>
            {
                Expression<Func<ArStuEnrollments, bool>> filter = se => se.PrgVerId == programVersionId;

                filter = filter.And(se =>
                       se.StartDate <= DateTime.Now &&
                       se.PrgVer.Fasapid.HasValue);

                if (systemStatuses != null && systemStatuses.Count > 0)
                {
                    filter = filter.And(se => systemStatuses.Contains((Constants.SystemStatus)se.StatusCode.SysStatusId));
                }

                return this.mapper.Map<IEnumerable<ProgramVersionSAP>>(this.EnrollmentRepository
                      .Get(filter).Include(en => en.StatusCode).Include(en => en.PrgVer).ThenInclude(pv => pv.Fasap)
                      .Include(en => en.PrgVer.Prog).ThenInclude(en => en.Ac).Include(en => en.PrgVer.UnitType));
            });
        }

        /// <summary>
        /// The Update action allows to update the Enrollment status details when the student is undo terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The update enrollment detail action returns enrollment detail of student and in the parameter 
        /// it requires object with CampusId, EnrollmentId and StudentId. 
        /// Based on the EnrollmentId and StudentId, it search all the enrolled program details and returns us following information 
        /// EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </remarks> 
        /// <param name="enrollment">
        /// The enrollment object of type Enrollment.
        /// </param>       
        /// <returns>
        /// Returns enrollment entity where result contains information like EnrollmentId, StudentId, EnrollmentDate, StartDate, DateDetermined, LastDateAttended, UnitTypeDescription and StatusCode.
        /// </returns>
        public async Task<Enrollment> Update(Enrollment enrollment)
        {
            return await Task.Run(async () =>
                           {
                               try
                               {
                                   ArStuEnrollments enrollmentEntity = this.EnrollmentRepository
                                       .Get(se => se.StuEnrollId == enrollment.EnrollmentId).FirstOrDefault();

                                   User userDetails = await this.userService.GetUserbyId(this.context.GetTenantUser().Id);

                                   if (enrollmentEntity != null)
                                   {
                                       enrollmentEntity.Lda = enrollment.LastDateAttended;
                                       enrollmentEntity.DateDetermined = enrollment.DateDetermined;
                                       enrollmentEntity.DropReasonId = enrollment.DropReasonId;
                                       enrollmentEntity.StatusCodeId = enrollment.StatusCodeId;
                                       enrollmentEntity.ModDate = DateTime.Now;
                                       enrollmentEntity.ModUser = userDetails.FullName;

                                       this.EnrollmentRepository.Update(enrollmentEntity);
                                       this.EnrollmentRepository.Save();
                                   }

                                   Enrollment enrollmentDetails = this.mapper.Map<Enrollment>(enrollmentEntity);
                                   enrollmentDetails.ResultStatus = $"{ApiMsgs.UPDATE_SUCCESS}";
                                   return enrollmentDetails;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   return new Enrollment
                                   {
                                       ResultStatus = $"{ApiMsgs.FailedUpdatingRecord} {enrollment.EnrollmentId}"
                                   };
                               }
                           });
        }

        /// <summary>
        /// The Get Student Loa Details action allows us to get the list of approve leave of absence count for the specified enrollment and date range.
        /// </summary>
        /// <remarks>
        ///  Get Student Loa Details action allows us to get the count of approve leave of absence for the specified enrollment and date range
        /// it requires enrollmentId , start date and end date. 
        /// Based on the EnrollmentId and date range, it searches all the enrolled program details and returns the count of  approve leave of absence. 
        /// </remarks> 
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param> 
        /// <returns>
        /// Returns the list of approve leave of absence count.
        /// </returns>
        public async Task<IEnumerable<LOA>> GetStudentLoaDetails(Guid enrollmentId, DateTime startDate, DateTime endDate)
        {
            return await Task.Run(() =>
                {
                    return this.EnrollmentRepository.Get(se => se.StuEnrollId == enrollmentId).Select(_ =>
                   _.ArStudentLoas.GroupBy(loa => new { loa.EndDate, loa.StartDate })
                               .Select(g => new LOA()
                               {
                                   Count = g.Count(),
                                   EndDate = g.FirstOrDefault().EndDate,
                                   StartDate = g.FirstOrDefault().StartDate,
                                   StudentEnrollmentId = g.FirstOrDefault().StuEnrollId

                               }).Where(x => x.StartDate >= startDate && x.EndDate <= endDate).ToList()).FirstOrDefault();
                });
        }

        /// <summary>
        /// The get active enrollment using a Klass App Student Id
        /// </summary>
        /// <param name="klassAppStudentId">
        /// The Klass App Student Id
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Enrollment> GetActiveEnrollmentForKlassApp(string klassAppStudentId)
        {
            List<Constants.SystemStatus> activeStatuses = (await this.systemStatusService.GetActiveSystemStatuses()).ToList();

            return await Task.Run(() =>
                {
                    return this.mapper.Map<Enrollment>(this.EnrollmentRepository.Get(se => se.Lead.StudentNumber == klassAppStudentId && activeStatuses.Contains((Constants.SystemStatus)se.StatusCode.SysStatusId))
                        .OrderByDescending(se => se.StartDate).FirstOrDefault());
                });
        }

        /// <summary>
        /// The get active enrollment using a student enrollment Id
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student Enrollment Id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Enrollment> GetEnrollment(Guid studentEnrollmentId, bool checkAllStatuses = false)
        {
            List<Constants.SystemStatus> activeStatuses = (await this.systemStatusService.GetActiveSystemStatuses()).ToList();

            return await Task.Run(() =>
                {
                    return this.mapper.Map<Enrollment>(this.EnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId && (!checkAllStatuses ? activeStatuses.Contains((Constants.SystemStatus)se.StatusCode.SysStatusId) : true))
                        .OrderByDescending(se => se.StartDate).FirstOrDefault());
                });
        }


        /// <summary>
        /// The get enrollment program summary.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<EnrollmentProgram> GetEnrollmentProgramSummary(Guid enrollmentId)
        {

            EnrollmentProgram result = await this.EnrollmentRepository.Get(se => se.StuEnrollId == enrollmentId)
                                           .Include(se => se.StatusCode)
                                           .Include(se => se.PrgVer).ThenInclude(se => se.Prog)
                                           .Include(se => se.AdLeadByLeadGroups).ThenInclude(lg => lg.LeadGrp)
                                           .Select(se => new EnrollmentProgram()
                                           {
                                               StuEnrollId = se.StuEnrollId,
                                               StatusCodeId = se.StatusCodeId,
                                               SysStatusId = se.StatusCode.SysStatusId,
                                               Status = se.StatusCode.StatusCodeDescrip,
                                               StartDate = se.StartDate,
                                               BadgeId = se.BadgeNumber,
                                               DropDate = se.DateDetermined,
                                               DropReason = se.DropReason.Descrip,
                                               EnrollmentDate = se.EnrollDate,
                                               GraduationDate = se.ContractedGradDate,
                                               RevisedGraduationDate = se.ExpGradDate,
                                               ReEnrollmentDate = se.ReEnrollmentDate,
                                               CampusId = se.CampusId,
                                               StudentGroups = se.AdLeadByLeadGroups.Where(llg => llg.LeadGrp.StatusId == ActiveStatusId).Select(llg => llg.LeadGrp.Descrip).ToList(),
                                               DoCourseWeightOverallGPA = se.PrgVer.DoCourseWeightOverallGpa,
                                               ACId = se.PrgVer.Prog.Acid ?? 0
                                           }).FirstOrDefaultAsync();


            if (result.SysStatusId == 10)
            {
                StudentLoas loaStudents = await this.StudentLoas.Get(se => se.StuEnrollId == enrollmentId)
                                          .Select(se => new StudentLoas()
                                          {
                                              StuEnrollId = se.StuEnrollId,
                                              LoaStartDate = se.StartDate,
                                              LoaEndDate = se.EndDate
                                          }).OrderByDescending(s => s.LoaStartDate).FirstOrDefaultAsync();

                result.LoaStartDate = loaStudents.LoaStartDate;
                result.LoaEndDate = loaStudents.LoaEndDate;
            }


            double overallGPA = 0;
            if (result != null)
            {
                // calculate weights 
                List<SyCreditSummary> creditResults = this.CreditSummary
                    .Get(cs => cs.StuEnrollId == enrollmentId)
                    .ToList();

                string allowCourseWeighting = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                    ConfigurationAppSettingsKeys.AllowCourseWeighting,
                    result.CampusId);

                string gradesFormat = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                   ConfigurationAppSettingsKeys.GradesFormat,
                    result.CampusId);

                string GPAMethod = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                   ConfigurationAppSettingsKeys.GPAMethod,
                    result.CampusId);



                if (gradesFormat.ToLower() == "numeric" && result.ACId == (int)AcademicCalendar.ClockHour)
                {
                    DbCommand triggerMetDateCommand = await context
                                        .LoadStoredProc("USP_GPACalculator")
                                        .WithSqlParam("@EnrollmentId", enrollmentId, DbType.Guid)
                                        .WithSqlParam("@StudentGPA", 0, DbType.Decimal, ParameterDirection.Output)
                                        .ExecuteStoredProcAsync();

                    var cumGPA = triggerMetDateCommand.GetOutputParameter<double?>("@StudentGPA");
                    overallGPA = cumGPA.HasValue ? cumGPA.Value : 0;
                    triggerMetDateCommand.Close();

                }
                else if (gradesFormat.ToLower() == "letter")
                {

                    DbCommand triggerMetDateCommandWeighting = await context
                                                           .LoadStoredProc("Usp_PR_Sub2_Enrollment_Summary")
                                                           .WithSqlParam("@StuEnrollId", enrollmentId, DbType.Guid)
                                                           .ExecuteStoredProcAsync();

                    // Create the DbDataAdapter.
                    DbDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = triggerMetDateCommandWeighting;

                    // Fill the DataTable.
                    DataSet tables = new DataSet();
                    adapter.Fill(tables);
                    var weightedAverage_CumGPA = Convert.ToDouble(tables.Tables[0].Rows[0]["WeightedAverage_CumGPA"]);
                    var simpleAverage_CumGPA = Convert.ToDouble(tables.Tables[0].Rows[0]["SimpleAverage_CumGPA"]);

                    triggerMetDateCommandWeighting.Close();
                    if (GPAMethod.ToLower() == "weightedavg")
                    {
                        overallGPA = weightedAverage_CumGPA;
                    }
                    else
                    {
                        overallGPA = simpleAverage_CumGPA;
                    }

                }
                else
                {
                    int creditsSummaryCount = creditResults.Count;
                    if (creditResults.Any())
                    {
                        decimal? sum = creditResults.DefaultIfEmpty().Sum(r => r.FinalScore ?? 0);
                        overallGPA = Convert.ToDouble(sum ?? 0) / creditsSummaryCount;
                    }
                }

                result.OverallGPA = Math.Round(overallGPA, 2);
            }
            return result ?? new EnrollmentProgram();
        }

        /// <inheritdoc />
        /// <summary>
        /// The get lda.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        /// <exception cref="T:System.NotImplementedException">
        /// </exception>
        public async Task<DateTime?> GetLDA(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                           {
                               {
                                   Enrollment enrollmentDetails = await this.GetEnrollment(enrollmentId, true);
                                   List<decimal?> invalidValues = new List<decimal?> { 0, 99, 999, 9999 };
                                   List<DateTime?> dates = new List<DateTime?>();
                                   if (enrollmentDetails != null)
                                   {
                                       string trackSapAttendance = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                                           ConfigurationAppSettingsKeys.TrackSapAttendance,
                                           enrollmentDetails.CampusId);

                                       DateTime? lda = this.EnrollmentRepository.Get(e => e.StuEnrollId == enrollmentId).Select(e => e.Lda).FirstOrDefault();
                                       dates.Add(lda);

                                       DateTime? maxAttendedDate = this.ExternShipAttendance.Get(a => a.StuEnrollId == enrollmentId).Select(a => a.AttendedDate).AsEnumerable()
                                           .MaxOrNull();

                                       dates.Add(maxAttendedDate);

                                       if (trackSapAttendance == TrackSapAttendance.ByDay)
                                       {
                                           DateTime? maxRecordDate = this.StudentClockAttendance
                                               .Get(a => a.StuEnrollId == enrollmentId && !invalidValues.Contains(a.ActualHours)).Select(a => a.RecordDate).AsEnumerable()
                                               .MaxOrNull();

                                           dates.Add(maxRecordDate);
                                       }
                                       else
                                       {
                                           DateTime? maxMeetDate = this.ClassSectionAttendance
                                               .Get(a => a.StuEnrollId == enrollmentId && !invalidValues.Contains(a.Actual)).Select(a => a.MeetDate).AsEnumerable()
                                               .MaxOrNull();

                                           dates.Add(maxMeetDate);
                                       }

                                       DateTime? maxMeetDateConversion = this.ConversionAttendance
                                           .Get(a => a.StuEnrollId == enrollmentId && !invalidValues.Contains(a.Actual)).Select(a => a.MeetDate).AsEnumerable()
                                               .MaxOrNull();

                                       dates.Add(maxMeetDateConversion);
                                   }

                                   IEnumerable<DateTime?> allDates = dates.Where(r => r.HasValue);
                                   DateTime? finalResult = allDates.Any() ? allDates.Max() : null;
                                   return finalResult;
                               }
                           });
        }

        /// <summary>
        /// The get new badge number.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> GetNewBadgeNumber(Guid campusId)
        {
            return await Task.Run(
                       () =>
                           {
                               long badgeNumberMaxInt = this.EnrollmentRepository.Get(b => b.BadgeNumber != null && b.BadgeNumber != string.Empty)
                                   .Where(x => x.CampusId == campusId).Max(x => Convert.ToInt64(x.BadgeNumber));
                               badgeNumberMaxInt += 1;
                               return badgeNumberMaxInt.ToString();
                           });
        }

        /// <summary>
        /// The have attendance or grade posted.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<bool> HaveAttendanceOrGradePosted(Guid studentEnrollmentId)
        {
            return await Task.Run(
                       () =>
                           {
                               var haveAttendanceOrGrades = this.EnrollmentRepository.Get(x => x.StuEnrollId == studentEnrollmentId).Any(
                                   x => x.AtAttendance.Any(y => y.EnrollId == studentEnrollmentId && y.Actual != 9999 && y.Actual != null && y.Actual != 999 && y.Actual != 0)
                                        || x.AtClsSectAttendance.Any(y => y.StuEnrollId == studentEnrollmentId && y.Actual != 9999 && y.Actual != 999 && y.Actual != 0)
                                        || x.ArStudentClockAttendance.Any(y => y.StuEnrollId == studentEnrollmentId && y.ActualHours != 9999 && y.ActualHours != null && y.ActualHours != 999 && y.ActualHours != 0)
                                        || x.ArExternshipAttendance.Any(y => y.StuEnrollId == studentEnrollmentId && y.HoursAttended != 9999)
                                        || x.AtAttendance.Any(y => y.EnrollId == studentEnrollmentId && y.Actual != 9999 && y.Actual != null && y.Actual != 999 && y.Actual != 0)
                                        || x.ArGrdBkResults.Any(y => y.StuEnrollId == studentEnrollmentId && y.Score != null && y.PostDate != null)
                                        || x.ArTransferGrades.Any(y => y.StuEnrollId == studentEnrollmentId && y.Score != null)
                                        || x.ArResults.Any(y => y.StuEnrollId == studentEnrollmentId && y.Score != null));


                               return haveAttendanceOrGrades;
                           });
        }


        /// <summary>
        /// The get afa student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<ActionResult<AFAEnrollmentDetails>> GetAfaStudentEnrollmentDetails(Guid studentEnrollmentId)
        {
            var result = new ActionResult<AFAEnrollmentDetails>();
            try
            {
                var studentEnrollmentDetails = this.EnrollmentRepository
         .Get(e => e.StuEnrollId == studentEnrollmentId).Include(enr => enr.SyStudentStatusChanges).ThenInclude(ssc => ssc.OrigStatus)
         .Include(enr => enr.SyStudentStatusChanges).ThenInclude(ssc => ssc.NewStatus)
         .Include(enr => enr.PrgVer).Include(enr => enr.Campus)
         .Include(enr => enr.Lead).ThenInclude(ld => ld.Admincriteria).Include(enr => enr.StatusCode).ThenInclude(sc => sc.SysStatus).Include(enr => enr.ArStudentLoas)
         .FirstOrDefault();

                if (studentEnrollmentDetails != null)
                {
                    AFAEnrollmentDetails afaEnrollmentDetails = new AFAEnrollmentDetails();
                    afaEnrollmentDetails.StudentEnrollmentId = studentEnrollmentDetails.StuEnrollId;
                    afaEnrollmentDetails.StartDate = studentEnrollmentDetails.StartDate;
                    afaEnrollmentDetails.ProgramVersionName = studentEnrollmentDetails.PrgVer.PrgVerDescrip;
                    afaEnrollmentDetails.ProgramVersionId = studentEnrollmentDetails.PrgVerId;
                    afaEnrollmentDetails.AfaStudentId = studentEnrollmentDetails.Lead.AfaStudentId;
                    afaEnrollmentDetails.CMSId = studentEnrollmentDetails.Campus.CmsId;
                    afaEnrollmentDetails.SSN = studentEnrollmentDetails.Lead.Ssn;
                    afaEnrollmentDetails.GradDate = studentEnrollmentDetails.ExpGradDate;
                    afaEnrollmentDetails.ModDate = studentEnrollmentDetails.ModDate;
                    afaEnrollmentDetails.ModUser = studentEnrollmentDetails.ModUser;
                    afaEnrollmentDetails.LastDateAttendance = await this.GetLDA(studentEnrollmentDetails.StuEnrollId);
                    afaEnrollmentDetails.AdmissionsCriteria = studentEnrollmentDetails.Lead.Admincriteria.Code;
                    afaEnrollmentDetails.EnrollmentStatus = studentEnrollmentDetails.StatusCode.SysStatusId.ToString();
                    afaEnrollmentDetails.AdvStudentNumber = studentEnrollmentDetails.Lead.StudentNumber;
                    afaEnrollmentDetails.InSchoolTransferHrs = studentEnrollmentDetails.TotalTransferHoursFromThisSchool - studentEnrollmentDetails.TotalTransferHoursFromThisSchool;
                    afaEnrollmentDetails.PriorSchoolHrs = studentEnrollmentDetails.TransferHours;

                    ArStuEnrollments enrollment = this.EnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentDetails.StuEnrollId)
                        .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                        .Include(se => se.AtClsSectAttendance).Include(se => se.ArStudentSchedules).ThenInclude(ss => ss.Schedule).ThenInclude(sch => sch.ArProgScheduleDetails).FirstOrDefault();

                    ArStudentSchedules studentSchedule = enrollment.ArStudentSchedules.FirstOrDefault(sch => sch.Active.HasValue && sch.Active.Value);
                    if (enrollment != null && studentSchedule != null && studentSchedule.Schedule != null && studentSchedule.Schedule.ArProgScheduleDetails.Any())
                    {
                        afaEnrollmentDetails.HoursPerWeek = studentSchedule.Schedule.ArProgScheduleDetails.Select(sd => sd.Total.GetValueOrDefault(0)).DefaultIfEmpty(0).Sum();
                    }

                    var latestStatusChange = studentEnrollmentDetails.SyStudentStatusChanges.Where(sch => sch.NewStatusId == studentEnrollmentDetails.StatusCodeId)
                            .OrderByDescending(doc => doc.DateOfChange).FirstOrDefault();

                    if (latestStatusChange != null)
                    {
                        var statusEffectiveDate = latestStatusChange.DateOfChange;
                        afaEnrollmentDetails.StatusEffectiveDate = statusEffectiveDate;
                        var enrollmentSysStatusId = studentEnrollmentDetails.StatusCode.SysStatusId;
                        var inSchool = latestStatusChange.NewStatus.SysStatus.InSchool.GetValueOrDefault() == 1;

                        var returnedFromLOA = false;
                        var returnedFromSuspension = false;
                        if (latestStatusChange.OrigStatus != null)
                        {
                            returnedFromLOA = inSchool && latestStatusChange.OrigStatus.SysStatusId == (int)StudentStatuses.LeaveOfAbsence;
                            returnedFromSuspension = inSchool && latestStatusChange.OrigStatus.SysStatusId == (int)StudentStatuses.Suspension;
                        }

                        switch (enrollmentSysStatusId)
                        {
                            case (int)StudentStatuses.Dropped:
                                afaEnrollmentDetails.DroppedDate = studentEnrollmentDetails.DateDetermined;
                                break;
                            case (int)StudentStatuses.LeaveOfAbsence:
                                if (statusEffectiveDate.HasValue)
                                {
                                    var leaveOfAbsence = studentEnrollmentDetails.ArStudentLoas
                                        .Where(loa => loa.StartDate == statusEffectiveDate.Value).OrderByDescending(loa => loa.LoareturnDate).FirstOrDefault();

                                    if (leaveOfAbsence != null)
                                    {
                                        afaEnrollmentDetails.LeaveOfAbsenceDate = leaveOfAbsence.StartDate;
                                    }

                                }

                                break;


                        }

                        if (returnedFromLOA)
                        {

                            var latestChangeToLoa = studentEnrollmentDetails.SyStudentStatusChanges.Where(sch => sch.NewStatus.SysStatusId == (int)StudentStatuses.LeaveOfAbsence)
                            .Select(SSC => SSC.DateOfChange).OrderByDescending(doc => doc).FirstOrDefault();

                            if (latestChangeToLoa.HasValue)
                            {
                                var leaveOfAbsence = studentEnrollmentDetails.ArStudentLoas
                                      .Where(loa => loa.StartDate == latestChangeToLoa.Value).OrderByDescending(loa => loa.LoareturnDate).FirstOrDefault();

                                if (leaveOfAbsence != null)
                                {
                                    afaEnrollmentDetails.ReturnFromLOADate = leaveOfAbsence.LoareturnDate;
                                }
                            }

                        }

                        else if (returnedFromSuspension)
                        {

                            var latestChangeToSuspension = studentEnrollmentDetails.SyStudentStatusChanges.Where(sch => sch.NewStatus.SysStatusId == (int)StudentStatuses.Suspension)
                            .Select(SSC => SSC.DateOfChange).OrderByDescending(doc => doc).FirstOrDefault();

                            if (latestChangeToSuspension.HasValue)
                            {
                                var suspension = studentEnrollmentDetails.ArStdSuspensions
                                       .Where(loa => loa.StartDate == latestChangeToSuspension.Value).OrderByDescending(loa => loa.EndDate).FirstOrDefault();

                                if (suspension != null)
                                {
                                    afaEnrollmentDetails.ReturnFromLOADate = suspension.EndDate.AddDays(1);
                                }
                            }
                        }
                    }
                    result.ResultStatus = Enums.ResultStatus.Success;
                    result.ResultStatusMessage = "Successfully retrieved afa enrollment details";
                    result.Result = afaEnrollmentDetails;
                }

                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = e.Message;
                return result;
            }
        }


        /// <summary>
        /// The get afa student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentIds">
        /// The list of student enrollment ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<ActionResult<IEnumerable<AFAEnrollmentDetails>>> GetAfaStudentEnrollmentDetails(IEnumerable<Guid> studentEnrollmentIds)
        {
            var result = new ActionResult<IEnumerable<AFAEnrollmentDetails>>();
            var enrollmentDetailsResult = new List<AFAEnrollmentDetails>();
            try
            {
                var studentEnrollmentDetails = this.EnrollmentRepository
         .Get(e => studentEnrollmentIds.Contains(e.StuEnrollId)).Include(enr => enr.SyStudentStatusChanges).ThenInclude(ssc => ssc.OrigStatus)
         .Include(enr => enr.SyStudentStatusChanges).ThenInclude(ssc => ssc.NewStatus)
         .Include(enr => enr.PrgVer).Include(enr => enr.Campus)
         .Include(enr => enr.Lead).ThenInclude(ld => ld.Admincriteria).Include(enr => enr.StatusCode).ThenInclude(sc => sc.SysStatus).Include(enr => enr.ArStudentLoas)
         .ToList();

                if (studentEnrollmentDetails.Any())
                {

                    foreach (var studentEnrollmentDetail in studentEnrollmentDetails)
                    {
                        AFAEnrollmentDetails afaEnrollmentDetails = new AFAEnrollmentDetails();
                        afaEnrollmentDetails.StudentEnrollmentId = studentEnrollmentDetail.StuEnrollId;
                        afaEnrollmentDetails.StartDate = studentEnrollmentDetail.StartDate;
                        afaEnrollmentDetails.ProgramVersionName = studentEnrollmentDetail.PrgVer.PrgVerDescrip;
                        afaEnrollmentDetails.ProgramVersionId = studentEnrollmentDetail.PrgVerId;
                        afaEnrollmentDetails.AfaStudentId = studentEnrollmentDetail.Lead.AfaStudentId;
                        afaEnrollmentDetails.CMSId = studentEnrollmentDetail.Campus.CmsId;
                        afaEnrollmentDetails.SSN = studentEnrollmentDetail.Lead.Ssn;
                        afaEnrollmentDetails.GradDate = studentEnrollmentDetail.ExpGradDate;
                        afaEnrollmentDetails.ModDate = studentEnrollmentDetail.ModDate;
                        afaEnrollmentDetails.ModUser = studentEnrollmentDetail.ModUser;
                        afaEnrollmentDetails.LastDateAttendance = await this.GetLDA(studentEnrollmentDetail.StuEnrollId);
                        afaEnrollmentDetails.AdmissionsCriteria = studentEnrollmentDetail.Lead.Admincriteria.Code;
                        afaEnrollmentDetails.EnrollmentStatus = studentEnrollmentDetail.StatusCode.SysStatusId.ToString();
                        afaEnrollmentDetails.AdvStudentNumber = studentEnrollmentDetail.Lead.StudentNumber;
                        afaEnrollmentDetails.InSchoolTransferHrs = studentEnrollmentDetail.TotalTransferHoursFromThisSchool;
                        afaEnrollmentDetails.PriorSchoolHrs = studentEnrollmentDetail.TransferHours - studentEnrollmentDetail.TotalTransferHoursFromThisSchool;

                        ArStuEnrollments enrollment = this.EnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentDetail.StuEnrollId)
                            .Include(se => se.PrgVer).ThenInclude(pv => pv.UnitType)
                            .Include(se => se.AtClsSectAttendance).Include(se => se.ArStudentSchedules).ThenInclude(ss => ss.Schedule).ThenInclude(sch => sch.ArProgScheduleDetails).FirstOrDefault();

                        ArStudentSchedules studentSchedule = enrollment.ArStudentSchedules.FirstOrDefault(sch => sch.Active.HasValue && sch.Active.Value);
                        if (enrollment != null && studentSchedule != null && studentSchedule.Schedule != null && studentSchedule.Schedule.ArProgScheduleDetails.Any())
                        {
                            afaEnrollmentDetails.HoursPerWeek = studentSchedule.Schedule.ArProgScheduleDetails.Select(sd => sd.Total.GetValueOrDefault(0)).DefaultIfEmpty(0).Sum();
                        }

                        var latestStatusChange = studentEnrollmentDetail.SyStudentStatusChanges.Where(sch => sch.NewStatusId == studentEnrollmentDetail.StatusCodeId)
                            .OrderByDescending(doc => doc.DateOfChange).FirstOrDefault();

                        if (latestStatusChange != null)
                        {
                            var statusEffectiveDate = latestStatusChange.DateOfChange;
                            afaEnrollmentDetails.StatusEffectiveDate = statusEffectiveDate;
                            var enrollmentSysStatusId = studentEnrollmentDetail.StatusCode.SysStatusId;
                            var inSchool = latestStatusChange.NewStatus.SysStatus.InSchool.GetValueOrDefault() == 1;
                            // for future start status
                            var returnedFromLOA = false;
                            var returnedFromSuspension = false;
                            if (latestStatusChange.OrigStatus != null)
                            {
                                returnedFromLOA = inSchool && latestStatusChange.OrigStatus.SysStatusId == (int)StudentStatuses.LeaveOfAbsence;
                                returnedFromSuspension = inSchool && latestStatusChange.OrigStatus.SysStatusId == (int)StudentStatuses.Suspension;
                            }

                            switch (enrollmentSysStatusId)
                            {
                                case (int)StudentStatuses.Dropped:
                                    afaEnrollmentDetails.DroppedDate = studentEnrollmentDetail.DateDetermined;
                                    break;
                                case (int)StudentStatuses.LeaveOfAbsence:
                                    if (statusEffectiveDate.HasValue)
                                    {
                                        var leaveOfAbsence = studentEnrollmentDetail.ArStudentLoas
                                            .Where(loa => loa.StartDate == statusEffectiveDate.Value).OrderByDescending(loa => loa.LoareturnDate).FirstOrDefault();

                                        if (leaveOfAbsence != null)
                                        {
                                            afaEnrollmentDetails.LeaveOfAbsenceDate = leaveOfAbsence.StartDate;
                                        }

                                    }

                                    break;


                            }

                            if (returnedFromLOA)
                            {
                                var latestChangeToLoa = studentEnrollmentDetail.SyStudentStatusChanges.Where(sch => sch.NewStatus != null && (sch.NewStatus.SysStatusId == (int)StudentStatuses.LeaveOfAbsence))
                                .Select(SSC => SSC.DateOfChange).OrderByDescending(doc => doc).FirstOrDefault();

                                if (latestChangeToLoa.HasValue)
                                {
                                    var leaveOfAbsence = studentEnrollmentDetail.ArStudentLoas
                                          .Where(loa => loa.StartDate == latestChangeToLoa.Value).OrderByDescending(loa => loa.LoareturnDate).FirstOrDefault();

                                    if (leaveOfAbsence != null)
                                    {
                                        afaEnrollmentDetails.ReturnFromLOADate = leaveOfAbsence.LoareturnDate;
                                    }
                                }

                            }

                            else if (returnedFromSuspension)
                            {

                                var latestChangeToSuspension = studentEnrollmentDetail.SyStudentStatusChanges.Where(sch => sch.NewStatus != null && (sch.NewStatus.SysStatusId == (int)StudentStatuses.Suspension))
                                .Select(SSC => SSC.DateOfChange).OrderByDescending(doc => doc).FirstOrDefault();

                                if (latestChangeToSuspension.HasValue)
                                {
                                    var suspension = studentEnrollmentDetail.ArStdSuspensions
                                           .Where(loa => loa.StartDate == latestChangeToSuspension.Value).OrderByDescending(loa => loa.EndDate).FirstOrDefault();

                                    if (suspension != null)
                                    {
                                        afaEnrollmentDetails.ReturnFromLOADate = suspension.EndDate.AddDays(1);
                                    }
                                }
                            }
                        }


                        enrollmentDetailsResult.Add(afaEnrollmentDetails);
                    }

                    result.ResultStatus = Enums.ResultStatus.Success;
                    result.ResultStatusMessage = "Successfully retrieved afa enrollment details";
                    result.Result = enrollmentDetailsResult;
                }

                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = e.Message;
                return result;
            }
        }

        /// <summary>
        /// The get enrollment status changes to send to AFA.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<IEnumerable<EnrollmentTracking>> GetEnrollmentStatusChangesForAFA(Guid campusId)
        {
            var result = new List<EnrollmentTracking>();
            try
            {
                var studentEnrollmentPendingChanges = this.EnrollmentTracking
         .Get(e => e.StuEnroll.CampusId == campusId && !e.Processed).Include(enr => enr.StuEnroll)
         .ToList();

                if (studentEnrollmentPendingChanges.Any())
                {
                    var results = await this.GetAfaStudentEnrollmentDetails(studentEnrollmentPendingChanges.Select(et => et.StuEnrollId));

                    foreach (var pendingChange in studentEnrollmentPendingChanges)
                    {

                        result.Add(new EnrollmentTracking()
                        {
                            StuEnrollId = pendingChange.StuEnrollId,
                            EnrollmentStatus = pendingChange.EnrollmentStatus,
                            ModifiedDate = pendingChange.ModDate,
                            RetryCount = pendingChange.RetryCount,
                            Enrollment = results.Result.Where(enrDets => enrDets.StudentEnrollmentId == pendingChange.StuEnrollId).Select(x => new AdvStagingEnrollmentV2()
                            {
                                SISEnrollmentID = x.StudentEnrollmentId,
                                StartDate = x.StartDate.GetValueOrDefault(),
                                ProgramName = x.ProgramVersionName,
                                ProgramVersionID = x.ProgramVersionId,
                                EnrollmentStatus = x.EnrollmentStatus,
                                SSN = x.SSN,
                                StatusEffectiveDate = x.StatusEffectiveDate,
                                IDStudent = x.AfaStudentId.GetValueOrDefault(),
                                LocationCMSID = x.CMSId,
                                ReturnFromLOADate = x.ReturnFromLOADate,
                                LeaveOfAbsenceDate = x.LeaveOfAbsenceDate,
                                LastDateAttendance = x.LastDateAttendance,
                                GradDate = x.GradDate.GetValueOrDefault(),
                                AdmissionsCriteria = x.AdmissionsCriteria,
                                DroppedDate = x.DroppedDate,
                                DateCreated = x.ModDate,
                                DateUpdated = x.ModDate,
                                UserCreated = x.ModUser,
                                UserUpdated = x.ModUser


                            }).FirstOrDefault()
                        });
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                e.TrackException();
                return result;
            }
        }

        /// <summary>
        /// Get last tracking date for integration enrollment tracking
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        /// <returns></returns>
        public DateTime? GetLastTrackingDate(Guid stuEnrollmentId)
        {
            try
            {
                var trackingDate = this.EnrollmentTracking
       .Get(e => e.StuEnrollId == stuEnrollmentId).Select(pc => pc.ModDate)
       ?.FirstOrDefault();
                return trackingDate;
            }
            catch (Exception e)
            {
                e.TrackException();
                return null;
            }
        }

        /// <summary>
        /// Method to increment retry counter
        /// </summary>
        /// <param name="stuEnrollmentId"></param>
        public async void IncrementRetryCount(Guid stuEnrollmentId)
        {
            var enrollmentTracking = this.EnrollmentTracking.FirstOrDefault(et => et.StuEnrollId == stuEnrollmentId);

            if (enrollmentTracking != null)
            {
                enrollmentTracking.RetryCount++;
                this.EnrollmentTracking.Update(enrollmentTracking);
                await this.EnrollmentTracking.SaveAsync();
            }
        }
        /// <summary>
        /// Mark enrollment change tracking as processed(AFA Integration).
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The campus id.
        /// </param>
        /// <param name="timeStamp">
        /// The time stamp of original processed record.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<bool> MarkEnrollmentChangeAsProcessed(Guid studentEnrollmentId, DateTime timeStamp)
        {
            try
            {
                var pendingChangeToProcess = this.EnrollmentTracking
         .Get(e => e.StuEnrollId == studentEnrollmentId && e.ModDate == timeStamp && !e.Processed)
         .FirstOrDefault();

                if (pendingChangeToProcess != null)
                {
                    pendingChangeToProcess.Processed = true;
                    this.EnrollmentTracking.Update(pendingChangeToProcess);
                    await this.EnrollmentTracking.SaveAsync();
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                e.TrackException();
                return false;
            }
        }


        /// <summary>
        /// Mark enrollment change tracking as processed(AFA Integration).
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task MarkEnrollmentChangeAsProcessed(IEnumerable<AdvStagingEnrollmentV2> enrollments)
        {
            try
            {
                var pendingChangesToProcess = this.EnrollmentTracking
         .Get(e => enrollments.Select(_ => _.SISEnrollmentID).Contains(e.StuEnrollId) && !e.Processed)
         .ToList();

                if (pendingChangesToProcess.Any())
                {
                    foreach (var pendingChangeToProcess in pendingChangesToProcess)
                    {
                        var enrollment = enrollments.Where(enr => enr.SISEnrollmentID == pendingChangeToProcess.StuEnrollId).FirstOrDefault();

                        if (enrollment != null && enrollment.DateUpdated == pendingChangeToProcess.ModDate)
                        {
                            pendingChangeToProcess.Processed = true;
                            this.EnrollmentTracking.Update(pendingChangeToProcess);
                            await this.EnrollmentTracking.SaveAsync();
                        }
                    }

                }

            }
            catch (Exception e)
            {
                e.TrackException();
            }
        }


        /// <summary>
        /// gets all the enrollment details for enrollments active in last 60 days
        /// </summary>
        /// <param name="campusId"></param>
        /// <param name="programVersionId"></param>
        /// <returns></returns>
        public async Task<IList<LMSEnrollmentDetails>> GetAllActiveEnrollmentForCampusId(Guid campusId, Guid programVersionId)
        {
            List<LMSEnrollmentDetails> enrollments = new List<LMSEnrollmentDetails>();
            return await Task.Run(
                () =>
                {
                    List<ArStuEnrollments> enrollmentsSearched;
                    if (programVersionId != Guid.Empty)
                    {
                        enrollmentsSearched = (List<ArStuEnrollments>)this.EnrollmentRepository
                            .Get(
                                a => a.CampusId == campusId && a.PrgVerId == programVersionId)
                            .Include(pv => pv.PrgVer).Include(p => p.PrgVer.Prog).Include(se => se.StatusCode).Include(y => y.Lead).OrderBy(e => e.Lead.LastName).ThenBy(e => e.Lead.FirstName).ToList();
                    }
                    else
                    {
                        enrollmentsSearched = (List<ArStuEnrollments>)this.EnrollmentRepository
                            .Get(
                                a => a.CampusId == campusId)
                            .Include(pv => pv.PrgVer).Include(p => p.PrgVer.Prog).Include(se => se.StatusCode).Include(y => y.Lead).OrderBy(e => e.Lead.LastName).ThenBy(e => e.Lead.FirstName).ToList();
                    }

                    if (enrollmentsSearched.Any())
                    {
                        // check if its active enrollment and within 60 days then add to the list 
                        try
                        {
                            List<int> activeStatusIds =
                                new List<int>
                                {
                                    (int)Constants.SystemStatus.FutureStart,
                                    (int)Constants.SystemStatus.CurrentlyAttending,
                                    (int)Constants.SystemStatus.LeaveOfAbsence,
                                    (int)Constants.SystemStatus.Suspension,
                                    (int)Constants.SystemStatus.AcademicProbation,
                                    (int)Constants.SystemStatus.Externship,
                                    (int)Constants.SystemStatus.DisciplinaryProbation,
                                    (int)Constants.SystemStatus.WarningProbation,
                                    (int)Constants.SystemStatus.NoStart
                                };
                            foreach (ArStuEnrollments enrollment in enrollmentsSearched)
                            {
                                // mod date < 60 or LDS < 60
                                if (((DateTime.Now - enrollment.ModDate).TotalDays <= 60) || ((DateTime.Now - (enrollment.Lda ?? DateTime.MinValue)).TotalDays <= 60))
                                {
                                    if (activeStatusIds.Contains(enrollment.StatusCode.SysStatusId))
                                    {
                                        enrollments.Add(
                                            new LMSEnrollmentDetails()
                                            {
                                                StudentEnrollmentId = enrollment.StuEnrollId,
                                                FirstName = enrollment.Lead.FirstName,
                                                LastName = enrollment.Lead.LastName,
                                                ProgramName = enrollment.PrgVer.Prog.ProgDescrip,
                                                ProgramId = enrollment.PrgVer.ProgId ?? Guid.Empty,
                                                ProgramVersionName = enrollment.PrgVer.PrgVerDescrip,
                                                ProgramVersionId = enrollment.PrgVerId,
                                                Status = enrollment.StatusCode.StatusCodeDescrip,
                                                StatusCodeId = enrollment.StatusCode.StatusCodeId.ToString().Trim().ToUpper()
                                            });
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            e.TrackException();
                            Console.WriteLine(e);
                            throw;
                        }
                        return enrollments;
                    }

                    return enrollments;
                });
        }

        /// <summary>
        /// The get lms student enrollment details.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<ActionResult<StudentEnrollmentDetailsLms>> GetLmsStudentEnrollmentDetails(Guid studentEnrollmentId)
        {
            var result = new ActionResult<StudentEnrollmentDetailsLms>();
            var rv = await this.GetMultipleLmsStudentEnrollmentDetails(new List<Guid>() { studentEnrollmentId });
            result.Result = rv.Result != null && rv.Result.Any() ? rv.Result.FirstOrDefault() : null;
            result.ResultStatus = rv.ResultStatus;
            result.ResultStatusMessage = rv.ResultStatusMessage;
            return result;
        }

        /// <summary>
        /// The get lms student enrollment details for a set of students.
        /// </summary>
        /// <param name="studentEnrollmentIds">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<ActionResult<List<StudentEnrollmentDetailsLms>>> GetMultipleLmsStudentEnrollmentDetails(List<Guid> studentEnrollmentIds)
        {
            return await Task.Run(
                       () =>
                       {

                           var result = new ActionResult<List<StudentEnrollmentDetailsLms>>();
                           try
                           {
                               var studentEnrollmentDetails = this.EnrollmentRepository
                                   .Get(e => studentEnrollmentIds.Any(i => i == e.StuEnrollId))
                                   .Include(x => x.StatusCode)
                                   .Select(
                                       e => new StudentEnrollmentDetailsLms()
                                       {
                                           FirstName = e.Lead.FirstName,
                                           LastName = e.Lead.LastName,
                                           MiddleName = e.Lead.MiddleName,
                                           ProgramName = e.PrgVer.Prog.ProgDescrip,
                                           ProgramId = e.PrgVer.ProgId ?? Guid.Empty,
                                           ProgramVersionId = e.PrgVerId,
                                           ProgramVersionName = e.PrgVer.PrgVerDescrip,
                                           StudentEnrollmentId = e.StuEnrollId,
                                           Status = e.StatusCode.StatusCodeDescrip,
                                           StatusCodeId = e.StatusCode.StatusCodeId.ToString().Trim().ToUpper(),
                                           Gender = e.Lead.GenderNavigation == null ? null : e.Lead.GenderNavigation.GenderDescrip,
                                           StudentId = e.Lead.StudentNumber,
                                           StartDate = e.StartDate != null ? e.StartDate.Value.ToShortDateString() : null
                                       }).ToList();

                               if (studentEnrollmentDetails != null && studentEnrollmentDetails.Any())
                               {
                                   foreach (var studentEnrollmentDetail in studentEnrollmentDetails)
                                   {
                                       studentEnrollmentDetail.BestEmail =
                                           this.emailService.GetBest(studentEnrollmentDetail.StudentEnrollmentId);
                                       studentEnrollmentDetail.BestPhone =
                                           this.phoneService.GetBest(studentEnrollmentDetail.StudentEnrollmentId);

                                       studentEnrollmentDetail.BestAddress = this.addressService.GetBest(
                                           studentEnrollmentDetail.StudentEnrollmentId,
                                           out string city,
                                           out string state,
                                           out string zip,
                                           out string address2);
                                       studentEnrollmentDetail.City = city;
                                       studentEnrollmentDetail.State = state;
                                       studentEnrollmentDetail.Zip = zip;
                                       studentEnrollmentDetail.Address2 = address2;
                                   }
                               }

                               result.ResultStatus = Enums.ResultStatus.Success;
                               result.Result = studentEnrollmentDetails;
                               result.ResultStatusMessage = string.Empty;
                               result.AsString = "Student Enrollment Details";
                               return result;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               result.ResultStatus = Enums.ResultStatus.Error;
                               result.ResultStatusMessage = e.Message;
                               return result;
                           }
                       });
        }

        /// <summary>
        /// Get campus id for enrollment.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        public async Task<Guid> GetCampusIdForEnrollment(Guid studentEnrollmentId)
        {
            var campusId = (await this.EnrollmentRepository.GetByIdAsync(studentEnrollmentId)).CampusId;
            return campusId;
        }
    }
}