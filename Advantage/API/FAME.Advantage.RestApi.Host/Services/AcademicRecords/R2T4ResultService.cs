﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4ResultService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the EnrollmentService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;
    using FAME.Advantage.RestApi.Host.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Mappings.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Validations.DynamicValidation;
    using FAME.Core.R2T4Calculator;
    using FAME.Core.R2T4Calculator.Common;
    using FAME.Core.R2T4Calculator.Entities;
    using FAME.Core.R2T4Calculator.Interfaces;
    using FAME.Extensions;

    /// <summary>
    /// The R2T4Result service.
    /// </summary>
    public class R2T4ResultService : IR2T4ResultService
    {
        /// <summary>
        /// The mapper is used to map the R2T4Results Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The report mapper.
        /// </summary>
        private readonly IMapper reportMapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The system configuration app setting service.
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4ResultService"/> class. 
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        public R2T4ResultService(IMapper mapper, IAdvantageDbContext context, IStatusesService statusService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService)
        {
            this.mapper = mapper;
            this.context = context;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
        }

        /// <summary>
        /// The R2T4 results repository.
        /// </summary>
        private IRepository<ArR2t4results> R2T4ResultsRepository => this.context.GetRepositoryForEntity<ArR2t4results>();

        /// <summary>
        /// The R2T4 termination repository.
        /// </summary>
        private IRepository<ArR2t4terminationDetails> TerminationRepository => this.context.GetRepositoryForEntity<ArR2t4terminationDetails>();

        /// <summary>
        /// The Override R2T4 results repository.
        /// </summary>
        private IRepository<ArR2t4overrideResults> R2T4OverrideResultsRepository => this.context.GetRepositoryForEntity<ArR2t4overrideResults>();

        /// <summary>
        /// The system users repository.
        /// </summary>
        private IRepository<SyUsers> SyUsersRepository => this.context.GetRepositoryForEntity<SyUsers>();

        /// <summary>
        ///  The Post action method calculates and returns the R2T4 Results for the given R2T4Input data
        /// </summary>
        /// <remarks>
        /// Post requires the R2T4 input model object which contains the mandatory properties.
        /// At least one loan or grant disbursed or could have been disbursed,
        /// Program type, Hours/Days Scheduled and Total hours/days and Withdrawal date.
        /// </remarks> 
        /// <param name="model">
        /// R2T4Input model of type DTO
        /// </param>
        /// <returns>
        /// Returns an R2T4Input object which contains R2T4 results.
        /// </returns>
        public async Task<IR2T4Input> GetR2T4Results(DataTransferObjects.AcademicRecords.R2T4Input.R2T4Input model)
        {
            return await Task.Run(
                       () =>
                           {
                               R2T4Facade facade = new R2T4Facade();
                               var inputInfo = this.MapR2T4Input(model);
                               facade.CalculateR2T4(inputInfo);
                               return inputInfo;
                           });
        }

        /// <summary>
        ///  The MapR2T4Input is used as mapper between DTO R2T4Input and R2T4calculator.R2T4Input
        /// </summary>
        /// <remarks>
        /// The MapR2T4Input action requires R2T4Input object.
        /// </remarks> 
        /// <param name="model">
        /// The R2T4Input model object.
        /// </param>
        /// <returns>
        /// Returns an <see cref="IR2T4Input"/> object which is used to R2T4 calculations.
        /// </returns>
        public IR2T4Input MapR2T4Input(DataTransferObjects.AcademicRecords.R2T4Input.R2T4Input model)
        {
            var inputInfo = new Core.R2T4Calculator.InputOutput.R2T4Input();
            var student = new Student();

            inputInfo.Grants = new List<TitleIVGrants>();
            TitleIVGrants pellGrant = new TitleIVGrants
            {
                GrantName = Constants.PellGrant,
                AmountDisbursed = model.PellGrantDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.PellGrantDisbursed),
                AmountExpected = model.PellGrantCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.PellGrantCouldDisbursed)
            };
            inputInfo.Grants.Add(pellGrant);

            TitleIVGrants fseogGrant = new TitleIVGrants
            {
                GrantName = Constants.Fseog,
                AmountDisbursed = model.FseogDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.FseogDisbursed),
                AmountExpected = model.FseogCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.FseogCouldDisbursed)
            };
            inputInfo.Grants.Add(fseogGrant);
            TitleIVGrants teachGrant = new TitleIVGrants
            {
                GrantName = Constants.TeachGrant,
                AmountDisbursed = model.TeachGrantDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.TeachGrantDisbursed),
                AmountExpected = model.TeachGrantCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.TeachGrantCouldDisbursed)
            };
            inputInfo.Grants.Add(teachGrant);
            TitleIVGrants iraqAfgGrant = new TitleIVGrants
            {
                GrantName = Constants.IraqAfghanistanServiceGrant,
                AmountDisbursed = model.IraqAfgGrantDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.IraqAfgGrantDisbursed),
                AmountExpected = model.IraqAfgGrantCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.IraqAfgGrantCouldDisbursed)
            };
            inputInfo.Grants.Add(iraqAfgGrant);

            inputInfo.Loans = new List<TitleIVLoans>();
            var unSubFfel = new TitleIVLoans
            {
                LoanName = Constants.UnsubsidizedFfelOrDirectStaffordLoan,
                AmountDisbursed = model.UnsubLoanNetAmountDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.UnsubLoanNetAmountDisbursed),
                AmountExpected = model.UnsubLoanNetAmountCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.UnsubLoanNetAmountCouldDisbursed)
            };
            inputInfo.Loans.Add(unSubFfel);

            TitleIVLoans subFfel = new TitleIVLoans
            {
                LoanName = Constants.SubsidizedFfelOrDirectStaffordLoan,
                AmountDisbursed = model.SubLoanNetAmountDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.SubLoanNetAmountDisbursed),
                AmountExpected = model.SubLoanNetAmountCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.SubLoanNetAmountCouldDisbursed)
            };
            inputInfo.Loans.Add(subFfel);

            TitleIVLoans perkins = new TitleIVLoans
            {
                LoanName = Constants.PerkinsLoan,
                AmountDisbursed = model.PerkinsLoanDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.PerkinsLoanDisbursed),
                AmountExpected = model.PerkinsLoanCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.PerkinsLoanCouldDisbursed)
            };
            inputInfo.Loans.Add(perkins);

            TitleIVLoans ffelStudent = new TitleIVLoans
            {
                LoanName = Constants.FfelOrDirectPlusStudent,
                AmountDisbursed = model.DirectGraduatePlusLoanDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.DirectGraduatePlusLoanDisbursed),
                AmountExpected = model.DirectGraduatePlusLoanCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.DirectGraduatePlusLoanCouldDisbursed)
            };
            inputInfo.Loans.Add(ffelStudent);

            TitleIVLoans ffelParent = new TitleIVLoans();
            ffelParent.LoanName = Constants.FfelOrDirectPlusParent;
            ffelParent.AmountDisbursed = model.DirectParentPlusLoanDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.DirectParentPlusLoanDisbursed);
            ffelParent.AmountExpected = model.DirectParentPlusLoanCouldDisbursed == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.DirectParentPlusLoanCouldDisbursed);
            inputInfo.Loans.Add(ffelParent);

            student.InstitutionalCharges = new List<InstitutionalCharges>();
            InstitutionalCharges tutionCharge = new InstitutionalCharges(ConstantInstitutionalCharges.Tution, model.TuitionFee == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.TuitionFee));
            student.InstitutionalCharges.Add(tutionCharge);

            InstitutionalCharges roomCharge = new InstitutionalCharges(ConstantInstitutionalCharges.Room, model.RoomFee == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.RoomFee));
            student.InstitutionalCharges.Add(roomCharge);

            InstitutionalCharges boardCharge = new InstitutionalCharges(ConstantInstitutionalCharges.Board, model.BoardFee == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.BoardFee));
            student.InstitutionalCharges.Add(boardCharge);

            InstitutionalCharges otherCharge = new InstitutionalCharges(ConstantInstitutionalCharges.Other, model.OtherFee == null ? ConstantDecimals.ZeroDecimal : Convert.ToDecimal(model.OtherFee));
            student.InstitutionalCharges.Add(otherCharge);

            inputInfo.ProgramType = model.ProgramUnitTypeId == 0 ? ProgramType.CreditHour : ProgramType.ClockHour;
            inputInfo.PaymentType = model.PaymentType == 1 ? PaymentTypes.PaymentPeriod : PaymentTypes.PeriodOfEnrollment;
            student.IsTutionCharged = model.IsTuitionChargedByPaymentPeriod;
            student.CreditBalanceRefunded = Convert.ToDecimal(model.CreditBalanceRefunded);

            if (inputInfo.ProgramType == ProgramType.CreditHour)
            {
                if (!model.IsAttendanceNotRequired)
                {
                    student.IsAttendanceRequired = true;
                    student.ScheduledDays = Convert.ToInt32(model.CompletedTime);
                    student.TotalDays = Convert.ToInt32(model.TotalTime);
                }
                else
                {
                    student.IsAttendanceRequired = false;
                    student.ScheduledDays = 0.00m;
                    student.TotalDays = 0.00m;
                }
            }
            else if (inputInfo.ProgramType == ProgramType.ClockHour)
            {
                student.ScheduledHours = Convert.ToInt32(model.CompletedTime);
                student.TotalHours = Convert.ToInt32(model.TotalTime);
            }

            inputInfo.Student = student;
            return inputInfo;
        }

        /// <summary>
        /// The get R2T4 input service validations.
        /// </summary>
        /// <returns>
        /// The DynamicValidationRuleSet.
        /// </returns>
        public DynamicValidationRuleSet GetR2T4ResultValidationRuleSet()
        {
            var set = new DynamicValidationRuleSet("R2T4Input");
            set.Rules.Add("TerminationId", new DynamicValidationRule("TerminationId", true, true));
            set.Rules.Add("R2T4InputId", new DynamicValidationRule("R2T4InputId", true, true));
            return set;
        }

        /// <summary>
        /// The Create action allows to save the R2T4 Results details.
        /// </summary>
        /// <remarks>
        /// The Create action requires R2T4Results object. This object contains the properties of all the R2T4 Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The Create action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Results with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Result> Create(R2T4Result model)
        {
            var returnR2T4 = new R2T4Result();
            try
            {
                model.CreatedDate = model.UpdatedDate = DateTime.Now;
                model.CreatedById = model.UpdatedById = this.context.GetTenantUser().Id;
                model.R2T4ResultsId = Guid.Empty;

                var tempDetail = await Task.Run(() => this.R2T4ResultsRepository.Create(this.mapper.Map<ArR2t4results>(model)));
                this.R2T4ResultsRepository.Save();
                if (tempDetail != null)
                {
                    returnR2T4.R2T4ResultsId = tempDetail.R2t4resultsId;
                    returnR2T4.TerminationId = tempDetail.TerminationId;
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <summary>
        /// The SaveOverrideR2T4Result action allows to save the R2T4 Override Results details.
        /// </summary>
        /// <remarks>
        /// The SaveOverrideR2T4Result action requires R2T4Results object. This object contains the properties of all the R2T4 Override Results data. 
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The SaveOverrideR2T4Result action method accepts a parameter of type R2T4 Results.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Result> SaveOverrideR2T4Result(R2T4Result model)
        {
            var returnR2T4 = new R2T4Result();
            try
            {
                if (this.context.GetTenantUser().IsSupportUser)
                {
                    model.CreatedDate = model.UpdatedDate = DateTime.Now;
                    model.CreatedById = model.UpdatedById = this.context.GetTenantUser().Id;
                    model.R2T4ResultsId = Guid.Empty;

                    var tempDetail = await Task.Run(
                                         () => this.R2T4OverrideResultsRepository.Create(
                                             this.mapper.Map<ArR2t4overrideResults>(model)));
                    this.R2T4ResultsRepository.Save();
                    if (tempDetail != null)
                    {
                        returnR2T4.R2T4ResultsId = tempDetail.R2t4overrideResultsId;
                        returnR2T4.TerminationId = tempDetail.TerminationId;
                        returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                    }
                    else
                    {
                        returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                    }
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.OVERRIDE_R2T4RESULTS_NOT_SUPPORT_USER;
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <summary>
        /// The Update action allows to update the existing R2T4 Result details.
        /// </summary>
        /// <remarks>
        /// The Update action requires R2T4Result object. This object contains the properties of all the R2T4 Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The Update action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Result> Update(R2T4Result model)
        {
            var returnR2T4 = new R2T4Result();

            try
            {
                model.UpdatedById = this.context.GetTenantUser().Id;
                var existingR2T4Result = this.R2T4ResultsRepository.GetById(model.R2T4ResultsId);

                existingR2T4Result.TerminationId = model.TerminationId;
                existingR2T4Result.SubTotalAmountDisbursedA = model.SubTotalAmountDisbursedA;
                existingR2T4Result.SubTotalAmountCouldDisbursedC = model.SubTotalAmountCouldDisbursedC;
                existingR2T4Result.SubTotalNetAmountDisbursedB = model.SubTotalNetAmountDisbursedB;
                existingR2T4Result.SubTotalNetAmountDisbursedD = model.SubTotalNetAmountDisbursedD;
                existingR2T4Result.BoxEresult = model.BoxEResult;
                existingR2T4Result.BoxFresult = model.BoxFResult;
                existingR2T4Result.BoxGresult = model.BoxGResult;
                existingR2T4Result.PercentageOfActualAttendence = model.PercentageOfActualAttendance;
                existingR2T4Result.BoxHresult = model.BoxHResult;
                existingR2T4Result.BoxIresult = model.BoxIResult;
                existingR2T4Result.BoxJresult = model.BoxJResult;
                existingR2T4Result.BoxKresult = model.BoxKResult;
                existingR2T4Result.BoxLresult = model.BoxLResult;
                existingR2T4Result.BoxMresult = model.BoxMResult;
                existingR2T4Result.BoxNresult = model.BoxNResult;
                existingR2T4Result.BoxOresult = model.BoxOResult;
                existingR2T4Result.UnsubDirectLoanSchoolReturn = model.UnsubDirectLoanSchoolReturn;
                existingR2T4Result.SubDirectLoanSchoolReturn = model.SubDirectLoanSchoolReturn;
                existingR2T4Result.PerkinsLoanSchoolReturn = model.PerkinsLoanSchoolReturn;
                existingR2T4Result.DirectGraduatePlusLoanSchoolReturn = model.DirectGraduatePlusLoanSchoolReturn;
                existingR2T4Result.DirectParentPlusLoanSchoolReturn = model.DirectParentPlusLoanSchoolReturn;
                existingR2T4Result.BoxPresult = model.BoxPResult;
                existingR2T4Result.PellGrantSchoolReturn = model.PellGrantSchoolReturn;
                existingR2T4Result.FseogschoolReturn = model.FseogSchoolReturn;
                existingR2T4Result.TeachGrantSchoolReturn = model.TeachGrantSchoolReturn;
                existingR2T4Result.IraqAfgGrantSchoolReturn = model.IraqAfgGrantSchoolReturn;
                existingR2T4Result.BoxQresult = model.BoxQResult;
                existingR2T4Result.BoxRresult = model.BoxRResult;
                existingR2T4Result.BoxSresult = model.BoxSResult;
                existingR2T4Result.BoxTresult = model.BoxTResult;
                existingR2T4Result.BoxUresult = model.BoxUResult;
                existingR2T4Result.PellGrantAmountToReturn = model.PellGrantAmountToReturn;
                existingR2T4Result.FseogamountToReturn = model.FseogAmountToReturn;
                existingR2T4Result.TeachGrantAmountToReturn = model.TeachGrantAmountToReturn;
                existingR2T4Result.IraqAfgGrantAmountToReturn = model.IraqAfgGrantAmountToReturn;
                existingR2T4Result.OverriddenData = model.OverriddenData;
                existingR2T4Result.PostWithdrawalData = model.PostWithdrawalData;
                existingR2T4Result.UpdatedById = model.UpdatedById;
                existingR2T4Result.UpdatedDate = DateTime.Now;
                existingR2T4Result.IsR2t4resultsCompleted = model.IsR2T4ResultsCompleted;
                existingR2T4Result.PellGrantDisbursed = model.PellGrantDisbursed;
                existingR2T4Result.PellGrantCouldDisbursed = model.PellGrantCouldDisbursed;
                existingR2T4Result.Fseogdisbursed = model.FseogDisbursed;
                existingR2T4Result.FseogcouldDisbursed = model.FseogCouldDisbursed;
                existingR2T4Result.TeachGrantDisbursed = model.TeachGrantDisbursed;
                existingR2T4Result.TeachGrantCouldDisbursed = model.TeachGrantCouldDisbursed;
                existingR2T4Result.IraqAfgGrantDisbursed = model.IraqAfgGrantDisbursed;
                existingR2T4Result.IraqAfgGrantCouldDisbursed = model.IraqAfgGrantCouldDisbursed;
                existingR2T4Result.UnsubLoanNetAmountDisbursed = model.UnsubLoanNetAmountDisbursed;
                existingR2T4Result.UnsubLoanNetAmountCouldDisbursed = model.UnsubLoanNetAmountCouldDisbursed;
                existingR2T4Result.SubLoanNetAmountDisbursed = model.SubLoanNetAmountDisbursed;
                existingR2T4Result.SubLoanNetAmountCouldDisbursed = model.SubLoanNetAmountCouldDisbursed;
                existingR2T4Result.PerkinsLoanDisbursed = model.PerkinsLoanDisbursed;
                existingR2T4Result.PerkinsLoanCouldDisbursed = model.PerkinsLoanCouldDisbursed;
                existingR2T4Result.DirectGraduatePlusLoanDisbursed = model.DirectGraduatePlusLoanDisbursed;
                existingR2T4Result.DirectGraduatePlusLoanCouldDisbursed = model.DirectGraduatePlusLoanCouldDisbursed;
                existingR2T4Result.DirectParentPlusLoanDisbursed = model.DirectParentPlusLoanDisbursed;
                existingR2T4Result.DirectParentPlusLoanCouldDisbursed = model.DirectParentPlusLoanCouldDisbursed;
                existingR2T4Result.IsAttendanceNotRequired = model.IsAttendanceNotRequired;
                existingR2T4Result.StartDate = model.StartDate;
                existingR2T4Result.ScheduledEndDate = model.ScheduledEndDate;
                existingR2T4Result.WithdrawalDate = model.WithdrawalDate;
                existingR2T4Result.CompletedTime =model.CompletedTime;
                existingR2T4Result.TotalTime = model.TotalTime;
                existingR2T4Result.TuitionFee = model.TuitionFee;
                existingR2T4Result.RoomFee = model.RoomFee;
                existingR2T4Result.BoardFee = model.BoardFee;
                existingR2T4Result.OtherFee = model.OtherFee;

                var result = await Task.Run(() => this.R2T4ResultsRepository.Update(existingR2T4Result));
                this.R2T4ResultsRepository.Save();
                if (result != null)
                {
                    returnR2T4.R2T4ResultsId = result.R2t4resultsId;
                    returnR2T4.TerminationId = result.TerminationId;
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                }
            }
            catch(Exception e)
            {
                e.TrackException();
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <summary>
        /// The UpdateOverrideResults action allows to update the existing R2T4 Override Result details.
        /// </summary>
        /// <remarks>
        /// The UpdateOverrideResults action requires R2T4Result object. This object contains the properties of all the R2T4 Override Result data.
        /// Mandatory parameters required : TerminationId.
        /// </remarks>
        /// <param name="model">
        /// The UpdateOverrideResults action method accepts a parameter of type R2T4 Result.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4Result with R2T4ResultsId, TerminationId and ResultStatus properties
        /// </returns>
        public async Task<R2T4Result> UpdateOverrideResult(R2T4Result model)
        {
            var returnR2T4 = new R2T4Result();
            try
            {
                if (this.context.GetTenantUser().IsSupportUser)
                {
                    model.UpdatedById = this.context.GetTenantUser().Id;
                    var existingR2T4Result = this.R2T4OverrideResultsRepository.GetById(model.R2T4ResultsId);
                    existingR2T4Result.TerminationId = model.TerminationId;
                    existingR2T4Result.SubTotalAmountDisbursedA = model.SubTotalAmountDisbursedA;
                    existingR2T4Result.SubTotalAmountCouldDisbursedC = model.SubTotalAmountCouldDisbursedC;
                    existingR2T4Result.SubTotalNetAmountDisbursedB = model.SubTotalNetAmountDisbursedB;
                    existingR2T4Result.SubTotalNetAmountDisbursedD = model.SubTotalNetAmountDisbursedD;
                    existingR2T4Result.BoxEresult = model.BoxEResult;
                    existingR2T4Result.BoxFresult = model.BoxFResult;
                    existingR2T4Result.BoxGresult = model.BoxGResult;
                    existingR2T4Result.PercentageOfActualAttendence = model.PercentageOfActualAttendance;
                    existingR2T4Result.BoxHresult = model.BoxHResult;
                    existingR2T4Result.BoxIresult = model.BoxIResult;
                    existingR2T4Result.BoxJresult = model.BoxJResult;
                    existingR2T4Result.BoxKresult = model.BoxKResult;
                    existingR2T4Result.BoxLresult = model.BoxLResult;
                    existingR2T4Result.BoxMresult = model.BoxMResult;
                    existingR2T4Result.BoxNresult = model.BoxNResult;
                    existingR2T4Result.BoxOresult = model.BoxOResult;
                    existingR2T4Result.UnsubDirectLoanSchoolReturn = model.UnsubDirectLoanSchoolReturn;
                    existingR2T4Result.SubDirectLoanSchoolReturn = model.SubDirectLoanSchoolReturn;
                    existingR2T4Result.PerkinsLoanSchoolReturn = model.PerkinsLoanSchoolReturn;
                    existingR2T4Result.DirectGraduatePlusLoanSchoolReturn = model.DirectGraduatePlusLoanSchoolReturn;
                    existingR2T4Result.DirectParentPlusLoanSchoolReturn = model.DirectParentPlusLoanSchoolReturn;
                    existingR2T4Result.BoxPresult = model.BoxPResult;
                    existingR2T4Result.PellGrantSchoolReturn = model.PellGrantSchoolReturn;
                    existingR2T4Result.FseogschoolReturn = model.FseogSchoolReturn;
                    existingR2T4Result.TeachGrantSchoolReturn = model.TeachGrantSchoolReturn;
                    existingR2T4Result.IraqAfgGrantSchoolReturn = model.IraqAfgGrantSchoolReturn;
                    existingR2T4Result.BoxQresult = model.BoxQResult;
                    existingR2T4Result.BoxRresult = model.BoxRResult;
                    existingR2T4Result.BoxSresult = model.BoxSResult;
                    existingR2T4Result.BoxTresult = model.BoxTResult;
                    existingR2T4Result.BoxUresult = model.BoxUResult;
                    existingR2T4Result.PellGrantAmountToReturn = model.PellGrantAmountToReturn;
                    existingR2T4Result.FseogamountToReturn = model.FseogAmountToReturn;
                    existingR2T4Result.TeachGrantAmountToReturn = model.TeachGrantAmountToReturn;
                    existingR2T4Result.IraqAfgGrantAmountToReturn = model.IraqAfgGrantAmountToReturn;
                    existingR2T4Result.TicketNumber = model.TicketNumber;
                    existingR2T4Result.UpdatedById = model.UpdatedById;
                    existingR2T4Result.UpdatedDate = DateTime.Now;

                    existingR2T4Result.PellGrantDisbursed = model.PellGrantDisbursed;
                    existingR2T4Result.PellGrantCouldDisbursed = model.PellGrantCouldDisbursed;
                    existingR2T4Result.Fseogdisbursed = model.FseogDisbursed;
                    existingR2T4Result.FseogcouldDisbursed = model.FseogCouldDisbursed;
                    existingR2T4Result.TeachGrantDisbursed = model.TeachGrantDisbursed;
                    existingR2T4Result.TeachGrantCouldDisbursed = model.TeachGrantCouldDisbursed;
                    existingR2T4Result.IraqAfgGrantDisbursed = model.IraqAfgGrantDisbursed;
                    existingR2T4Result.IraqAfgGrantCouldDisbursed = model.IraqAfgGrantCouldDisbursed;
                    existingR2T4Result.UnsubLoanNetAmountDisbursed = model.UnsubLoanNetAmountDisbursed;
                    existingR2T4Result.UnsubLoanNetAmountCouldDisbursed = model.UnsubLoanNetAmountCouldDisbursed;
                    existingR2T4Result.SubLoanNetAmountDisbursed = model.SubLoanNetAmountDisbursed;
                    existingR2T4Result.SubLoanNetAmountCouldDisbursed = model.SubLoanNetAmountCouldDisbursed;
                    existingR2T4Result.PerkinsLoanDisbursed = model.PerkinsLoanDisbursed;
                    existingR2T4Result.PerkinsLoanCouldDisbursed = model.PerkinsLoanCouldDisbursed;
                    existingR2T4Result.DirectGraduatePlusLoanDisbursed = model.DirectGraduatePlusLoanDisbursed;
                    existingR2T4Result.DirectGraduatePlusLoanCouldDisbursed = model.DirectGraduatePlusLoanCouldDisbursed;
                    existingR2T4Result.DirectParentPlusLoanDisbursed = model.DirectParentPlusLoanDisbursed;
                    existingR2T4Result.DirectParentPlusLoanCouldDisbursed = model.DirectParentPlusLoanCouldDisbursed;
                    existingR2T4Result.IsAttendanceNotRequired = model.IsAttendanceNotRequired;
                    existingR2T4Result.StartDate = model.StartDate;
                    existingR2T4Result.ScheduledEndDate = model.ScheduledEndDate;
                    existingR2T4Result.WithdrawalDate = model.WithdrawalDate;
                    existingR2T4Result.CompletedTime = model.CompletedTime;
                    existingR2T4Result.TotalTime = model.TotalTime;
                    existingR2T4Result.TuitionFee = model.TuitionFee;
                    existingR2T4Result.RoomFee = model.RoomFee;
                    existingR2T4Result.BoardFee = model.BoardFee;
                    existingR2T4Result.OtherFee = model.OtherFee;
                    existingR2T4Result.CreditBalanceRefunded = model.CreditBalanceRefunded;
                    existingR2T4Result.PostWithdrawalData = model.PostWithdrawalData;
                    existingR2T4Result.OverriddenData = model.OverriddenData;
                    existingR2T4Result.IsR2t4overrideResultsCompleted = model.IsR2T4OverrideResultsCompleted;

                    var result = await Task.Run(() => this.R2T4OverrideResultsRepository.Update(existingR2T4Result));
                    this.R2T4OverrideResultsRepository.Save();
                    if (result != null)
                    {
                        await Task.Run(() => this.UpdateR2T4ResultStatus(model.TerminationId, model.IsR2T4ResultsCompleted));
                        returnR2T4.R2T4ResultsId = result.R2t4overrideResultsId;
                        returnR2T4.TerminationId = result.TerminationId;
                        returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS;
                    }
                    else
                    {
                        returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
                    }
                }
                else
                {
                    returnR2T4.ResultStatus = ApiMsgs.OVERRIDE_R2T4RESULTS_NOT_SUPPORT_USER;
                }
            }
            catch(Exception e)
            {
                e.TrackException();
                returnR2T4.ResultStatus = ApiMsgs.SAVE_STUDENT_TERMINATION_UNSUCCESS;
            }

            return returnR2T4;
        }

        /// <summary>
        /// The UpdateR2T4ResultStatus action allows to update the existing R2T4 Result completion status.
        /// </summary>
        /// <remarks>
        /// The UpdateR2T4ResultStatus action requires termination Id and the status.
        /// Mandatory parameters required : TerminationId, status
        /// </remarks>
        /// <param name="terminationId">
        /// The UpdateOverrideResults action method requires termination Id of type guid.
        /// </param>
        /// <param name="status">
        /// The UpdateOverrideResults action method requires status type bool.
        /// </param>
        /// <returns>
        /// Returns update R2T4Result status boolean as true or false.
        /// </returns>
        public async Task<bool> UpdateR2T4ResultStatus(Guid terminationId, bool status)
        {
            bool result = false;
            var existingR2T4Result = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).FirstOrDefault();
            if (existingR2T4Result != null)
            {
                existingR2T4Result.IsR2t4resultsCompleted = status;
                this.R2T4ResultsRepository.Update(existingR2T4Result);
                await Task.Run(() => this.R2T4ResultsRepository.SaveAsync());
                result = true;
            }

            return result;
        }

        /// <summary>
        /// The get action allows to fetch the R2T4 Result detail.
        /// </summary>
        /// <remarks>
        /// The get action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        public async Task<R2T4Result> Get(Guid terminationId)
        {
            return await Task.Run(() =>
                {
                    var r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                    var r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();

                    DateTime r2T4UpdatedDate = DateTime.MinValue;
                    DateTime r2T4OverrideUpdatedDate = DateTime.MinValue;

                    if (r2T4ResultDetail?.UpdatedDate != null)
                    {
                        r2T4UpdatedDate = r2T4ResultDetail.UpdatedDate.Value;
                    }

                    if (r2T4OverrideResultDetail?.UpdatedDate != null)
                    {
                        r2T4OverrideUpdatedDate = r2T4OverrideResultDetail.UpdatedDate.Value;
                    }

                    if (r2T4UpdatedDate > r2T4OverrideUpdatedDate)
                    {
                        r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                        var results = this.mapper.Map<R2T4Result>(r2T4ResultDetail);
                        results.IsInputIncluded = false;
                        return results;
                    }
                    else
                    {
                        r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository
                            .Get(x => x.TerminationId == terminationId).LastOrDefault();
                        if (r2T4OverrideResultDetail != null)
                        {
                            var overrrideResults = this.mapper.Map<R2T4Result>(r2T4OverrideResultDetail);
                            overrrideResults.IsInputIncluded = true;
                            return overrrideResults;
                        }
                    }

                    return null;
                });
        }

        /// <summary>
        /// The GetR2T4ResultsForReport action method returns R2T4 result details for the given termination id.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsForReport action method requires termination Id of type Guid.
        /// </remarks>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object in XML format.
        /// </returns>
        public async Task<R2T4ResultReport> GetR2T4ResultsForReport(string token, Guid terminationId)
        {
            return await Task.Run(() =>
            {
                var r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                var r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                DateTime r2T4UpdatedDate = DateTime.MinValue;
                DateTime r2T4OverrideUpdatedDate = DateTime.MinValue;
                StudentService studentService = new StudentService(this.context, this.mapper, this.statusService, this.systemConfigurationAppSettingService);

                if (r2T4ResultDetail?.UpdatedDate != null)
                {
                    r2T4UpdatedDate = r2T4ResultDetail.UpdatedDate.Value;
                }

                if (r2T4OverrideResultDetail?.UpdatedDate != null)
                {
                    r2T4OverrideUpdatedDate = r2T4OverrideResultDetail.UpdatedDate.Value;
                }

                if (r2T4UpdatedDate > r2T4OverrideUpdatedDate)
                {
                    r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId)
                        .LastOrDefault();
                    var results = this.mapper.Map<R2T4ResultReport>(r2T4ResultDetail);
                    this.BindR2T4InputToResults(ref results, terminationId);
                    results.IsInputIncluded = false;
                    this.MapDuplicatedResultFields(results);
                    this.MapPostWithdrawalResultFields(results);
                    results.StudentInfo = studentService.GetEnrollmentsByTerminationId(terminationId);
                    results.StudentInfo.FormCompleted = Utility.FormatValueByType(r2T4UpdatedDate, FieldType.Date);
                    return results;
                }
                else
                {
                    r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository
                        .Get(x => x.TerminationId == terminationId).LastOrDefault();
                    if (r2T4OverrideResultDetail != null)
                    {
                        var overrrideResults = this.mapper.Map<R2T4ResultReport>(r2T4OverrideResultDetail);
                        overrrideResults.IsInputIncluded = true;
                        this.MapDuplicatedResultFields(overrrideResults);
                        this.MapPostWithdrawalResultFields(overrrideResults);
                        overrrideResults.StudentInfo = studentService.GetEnrollmentsByTerminationId(terminationId);
                        overrrideResults.StudentInfo.FormCompleted = Utility.FormatValueByType(r2T4OverrideUpdatedDate, FieldType.Date);
                        return overrrideResults;
                    }
                }

                return null;
            });
        }

        /// <summary>
        /// The MapDuplicatedResultFields action method returns R2T4 result details for the given termination id.
        /// This method includes the duplicated R2T4 properties to the object for reporting purpose.
        /// </summary>
        /// <remarks>
        /// The MapDuplicatedResultFields action method requires object of R2T4Result.
        /// </remarks>
        /// <param name="result">
        /// this action accepts R2t4Result object
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object.
        /// </returns>
        public R2T4Result MapDuplicatedResultFields(R2T4Result result)
        {
            var overriddenData = result.OverriddenData.Split(':');
            foreach (var valuePair in overriddenData)
            {
                if (!valuePair.IsNullOrEmpty())
                {
                    var detail = valuePair.Split('=');
                    var propertyName = detail[0];
                    propertyName = propertyName[0].ToString().ToUpper()
                                   + propertyName.Substring(1, propertyName.Length - 2)
                                   + propertyName[propertyName.Length - 1].ToString().ToLower();
                    if (!detail[1].IsNullOrEmpty())
                    {
                        result[propertyName] = detail[1];
                    }
                    else
                    {
                        result[propertyName] = string.Empty;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// The MapDuplicatedResultFields action method returns R2T4 result details for the given termination id.
        /// This method includes the duplicated R2T4 properties to the object for reporting purpose.
        /// </summary>
        /// <remarks>
        /// The MapDuplicatedResultFields action method requires object of R2T4Result.
        /// </remarks>
        /// <param name="result">
        /// this action accepts R2t4Result object
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object.
        /// </returns>
        public R2T4ResultReport MapDuplicatedResultFields(R2T4ResultReport result)
        {
            var overriddenData = result.OverriddenData.Split(':');
            foreach (var valuePair in overriddenData)
            {
                if (!valuePair.IsNullOrEmpty())
                {
                    var detail = valuePair.Split('=');
                    var propertyName = detail[0];
                    propertyName = propertyName[0].ToString().ToUpper()
                                   + propertyName.Substring(1, propertyName.Length - 2)
                                   + propertyName[propertyName.Length - 1].ToString().ToLower();
                    if (!detail[1].IsNullOrEmpty())
                    {
                        result[propertyName] = Utility.FormatValueByType(detail[1], string.Empty);
                    }
                    else
                    {
                        result[propertyName] = string.Empty;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// The MapPostWithdrawalResultFields action method returns R2T4 result details for the given termination id.
        /// This method includes the Post-Withdrawal data to the R2T4 Results object for reporting purpose.
        /// </summary>
        /// <remarks>
        /// The MapPostWithdrawalResultFields action method requires object of R2T4Result.
        /// </remarks>
        /// <param name="result">
        /// this action accepts R2t4Result object
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object.
        /// </returns>
        public R2T4Result MapPostWithdrawalResultFields(R2T4Result result)
        {
            var postWithdrawalData = result.PostWithdrawalData.Split(':');
            foreach (var valuePair in postWithdrawalData)
            {
                if (!valuePair.IsNullOrEmpty())
                {
                    var detail = valuePair.Split('=');
                    var propertyName = detail[0];
                    propertyName = propertyName[0].ToString().ToUpper()
                                   + propertyName.Substring(1, propertyName.Length - 1);
                                   
                    if (!detail[1].IsNullOrEmpty())
                    {
                        result[propertyName] = detail[1];
                    }
                    else
                    {
                        result[propertyName] = string.Empty;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// The MapPostWithdrawalResultFields action method returns R2T4 result details for the given termination id.
        /// This method includes the Post-Withdrawal data to the R2T4 Results object for reporting purpose.
        /// </summary>
        /// <remarks>
        /// The MapPostWithdrawalResultFields action method requires object of R2T4Result.
        /// </remarks>
        /// <param name="result">
        /// this action accepts R2t4Result object
        /// </param>
        /// <returns>
        /// Returns an <see cref="R2T4Result"/> object.
        /// </returns>
        public R2T4ResultReport MapPostWithdrawalResultFields(R2T4ResultReport result)
        {
            var postWithdrawalData = result.PostWithdrawalData.Split(':');
            foreach (var valuePair in postWithdrawalData)
            {
                if (!valuePair.IsNullOrEmpty())
                {
                    var detail = valuePair.Split('=');
                    var propertyName = detail[0];
                    propertyName = propertyName[0].ToString().ToUpper()
                                   + propertyName.Substring(1, propertyName.Length - 1);

                    if (!detail[1].IsNullOrEmpty())
                    {
                        result[propertyName] = detail[1];
                    }
                    else
                    {
                        result[propertyName] = string.Empty;
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// The GetR2T4ResultId action allows to check the terminationid exists in R2T4 results or not.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultId Results action requires R2T4ResultOverride and TerminationId and based on the Id provided, it returns the matching R2T4 Result Override record.
        /// </remarks>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// Returns the results id based on the matching value. If matching record found then the return empty string.
        /// </returns>
        public async Task<string> GetR2T4ResultId(R2T4ResultOverride model)
        {
            return await Task.Run(() =>
            {
                if (model != null && model.TabId == 0)
                {
                    return this.R2T4ResultsRepository.Get(x => x.TerminationId == model.TerminationId)
                        .Select(x => x.R2t4resultsId).FirstOrDefault().ToString();
                }

                return this.R2T4OverrideResultsRepository.Get(x => x.TerminationId == model.TerminationId)
                    .Select(x => x.R2t4overrideResultsId).FirstOrDefault().ToString();
            });
        }

        /// <summary>
        /// The IsResultsExists action allows to check if the R2T4 results are existing for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultsExists action requires terminationId and based on the Id provided, it returns boolean if the R2T4 result record exists or not.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        public async Task<bool> IsResultsExists(Guid terminationId)
        {
            return await Task.Run(() =>
            {
                bool isExists = true;
                var r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                var r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                if (r2T4ResultDetail == null && r2T4OverrideResultDetail == null)
                {
                    isExists = false;
                }

                return isExists;
            });
        }

        /// <summary>
        /// The BindR2T4InputToResults action allows to fetch the R2T4 Input detail for the given termination and binds it to the R2T4 Result object.
        /// </summary>
        /// <remarks>
        /// The BindR2T4InputToResults accepts the parameter of type R2T4 Result and termination Id.
        /// </remarks>
        /// <param name="r2T4ResultDetail">
        /// object of the R2T4Result.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Result along with R2T4 Input contains all the information related to R2T4 Input.
        /// </returns>
        public R2T4Result BindR2T4InputToResults(ref R2T4Result r2T4ResultDetail, Guid terminationId)
        {
            IRepository<ArR2t4input> inputRepository = this.context.GetRepositoryForEntity<ArR2t4input>();
            var model = inputRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
            if (model != null)
            {
                r2T4ResultDetail.PellGrantDisbursed = model.PellGrantDisbursed;
                r2T4ResultDetail.PellGrantCouldDisbursed = model.PellGrantCouldDisbursed;
                r2T4ResultDetail.FseogDisbursed = model.Fseogdisbursed;
                r2T4ResultDetail.FseogCouldDisbursed = model.FseogcouldDisbursed;
                r2T4ResultDetail.TeachGrantDisbursed = model.TeachGrantDisbursed;
                r2T4ResultDetail.TeachGrantCouldDisbursed = model.TeachGrantCouldDisbursed;
                r2T4ResultDetail.IraqAfgGrantDisbursed = model.IraqAfgGrantDisbursed;
                r2T4ResultDetail.IraqAfgGrantCouldDisbursed = model.IraqAfgGrantCouldDisbursed;
                r2T4ResultDetail.UnsubLoanNetAmountDisbursed = model.UnsubLoanNetAmountDisbursed;
                r2T4ResultDetail.UnsubLoanNetAmountCouldDisbursed = model.UnsubLoanNetAmountCouldDisbursed;
                r2T4ResultDetail.SubLoanNetAmountDisbursed = model.SubLoanNetAmountDisbursed;
                r2T4ResultDetail.SubLoanNetAmountCouldDisbursed = model.SubLoanNetAmountCouldDisbursed;
                r2T4ResultDetail.PerkinsLoanDisbursed = model.PerkinsLoanDisbursed;
                r2T4ResultDetail.PerkinsLoanCouldDisbursed = model.PerkinsLoanCouldDisbursed;
                r2T4ResultDetail.DirectGraduatePlusLoanDisbursed = model.DirectGraduatePlusLoanDisbursed;
                r2T4ResultDetail.DirectGraduatePlusLoanCouldDisbursed = model.DirectGraduatePlusLoanCouldDisbursed;
                r2T4ResultDetail.DirectParentPlusLoanDisbursed = model.DirectParentPlusLoanDisbursed;
                r2T4ResultDetail.DirectParentPlusLoanCouldDisbursed = model.DirectParentPlusLoanCouldDisbursed;
                r2T4ResultDetail.IsAttendanceNotRequired = model.IsAttendanceNotRequired;
                r2T4ResultDetail.StartDate = model.StartDate;
                r2T4ResultDetail.ScheduledEndDate = model.ScheduledEndDate;
                r2T4ResultDetail.WithdrawalDate = model.WithdrawalDate;
                r2T4ResultDetail.CompletedTime = model.CompletedTime;
                r2T4ResultDetail.TotalTime = model.TotalTime;
                r2T4ResultDetail.TuitionFee = model.TuitionFee;
                r2T4ResultDetail.RoomFee = model.RoomFee;
                r2T4ResultDetail.BoardFee = model.BoardFee;
                r2T4ResultDetail.OtherFee = model.OtherFee;
                r2T4ResultDetail.CreditBalanceRefunded = model.CreditBalanceRefunded;
                r2T4ResultDetail.ProgramUnitTypeId = model.ProgramUnitTypeId;
            }

            return r2T4ResultDetail;
        }

        /// <summary>
        /// The BindR2T4InputToResults action allows to fetch the R2T4 Input detail for the given termination and binds it to the R2T4 Result object.
        /// </summary>
        /// <remarks>
        /// The BindR2T4InputToResults accepts the parameter of type R2T4 Result and termination Id.
        /// </remarks>
        /// <param name="r2T4ResultDetail">
        /// object of the R2T4Result.
        /// </param>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Result along with R2T4 Input contains all the information related to R2T4 Input.
        /// </returns>
        public R2T4ResultReport BindR2T4InputToResults(ref R2T4ResultReport r2T4ResultDetail, Guid terminationId)
        {
            IRepository<ArR2t4input> inputRepository = this.context.GetRepositoryForEntity<ArR2t4input>();
            var model = inputRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
            if (model != null)
            {
                r2T4ResultDetail.PellGrantDisbursed = Utility.FormatValueByType(model.PellGrantDisbursed?.ToString(),string.Empty);
                r2T4ResultDetail.PellGrantCouldDisbursed = Utility.FormatValueByType(model.PellGrantCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.FseogDisbursed = Utility.FormatValueByType(model.Fseogdisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.FseogCouldDisbursed = Utility.FormatValueByType(model.FseogcouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.TeachGrantDisbursed = Utility.FormatValueByType(model.TeachGrantDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.TeachGrantCouldDisbursed = Utility.FormatValueByType(model.TeachGrantCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.IraqAfgGrantDisbursed = Utility.FormatValueByType(model.IraqAfgGrantDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.IraqAfgGrantCouldDisbursed = Utility.FormatValueByType(model.IraqAfgGrantCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.UnsubLoanNetAmountDisbursed = Utility.FormatValueByType(model.UnsubLoanNetAmountDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.UnsubLoanNetAmountCouldDisbursed = Utility.FormatValueByType(model.UnsubLoanNetAmountCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.SubLoanNetAmountDisbursed = Utility.FormatValueByType(model.SubLoanNetAmountDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.SubLoanNetAmountCouldDisbursed = Utility.FormatValueByType(model.SubLoanNetAmountCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.PerkinsLoanDisbursed = Utility.FormatValueByType(model.PerkinsLoanDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.PerkinsLoanCouldDisbursed = Utility.FormatValueByType(model.PerkinsLoanCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.DirectGraduatePlusLoanDisbursed = Utility.FormatValueByType(model.DirectGraduatePlusLoanDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.DirectGraduatePlusLoanCouldDisbursed = Utility.FormatValueByType(model.DirectGraduatePlusLoanCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.DirectParentPlusLoanDisbursed = Utility.FormatValueByType(model.DirectParentPlusLoanDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.DirectParentPlusLoanCouldDisbursed = Utility.FormatValueByType(model.DirectParentPlusLoanCouldDisbursed?.ToString(), string.Empty);
                r2T4ResultDetail.IsAttendanceNotRequired = model.IsAttendanceNotRequired;
                r2T4ResultDetail.StartDate = Utility.FormatValueByType(model.StartDate, FieldType.Date);
                r2T4ResultDetail.ScheduledEndDate = Utility.FormatValueByType(model.ScheduledEndDate, FieldType.Date);
                r2T4ResultDetail.WithdrawalDate = Utility.FormatValueByType(model.WithdrawalDate, FieldType.Date);
                r2T4ResultDetail.CompletedTime = Utility.FormatValueByType(model.CompletedTime?.ToString(), FieldType.Number);
                r2T4ResultDetail.TotalTime = Utility.FormatValueByType(model.TotalTime?.ToString(), FieldType.Number);
                r2T4ResultDetail.TuitionFee = Utility.FormatValueByType(model.TuitionFee?.ToString(), string.Empty);
                r2T4ResultDetail.RoomFee = Utility.FormatValueByType(model.RoomFee?.ToString(), string.Empty);
                r2T4ResultDetail.BoardFee = Utility.FormatValueByType(model.BoardFee?.ToString(), string.Empty);
                r2T4ResultDetail.OtherFee = Utility.FormatValueByType(model.OtherFee?.ToString(), string.Empty);
                r2T4ResultDetail.CreditBalanceRefunded = Utility.FormatValueByType(model.CreditBalanceRefunded?.ToString(), string.Empty);
                r2T4ResultDetail.ProgramUnitTypeId = model.ProgramUnitTypeId;
            }

            return r2T4ResultDetail;
        }

        /// <inheritdoc />
        /// <summary>
        /// The GetR2T4OverrideResults action allows to fetch the R2T4 Override Result detail.
        /// </summary>
        /// <remarks>
        /// The GetR2T4OverrideResults action requires terminationId and based on the Id provided, it returns the matching R2T4 Override Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Override Result.
        /// </returns>
        public async Task<R2T4Result> GetR2T4OverrideResults(Guid terminationId)
        {
            return await Task.Run(() =>
            { 
                var r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                var overrrideResults = this.mapper.Map<R2T4Result>(r2T4OverrideResultDetail); 
                if (r2T4OverrideResultDetail != null)
                {
                    var userId = r2T4OverrideResultDetail.UpdatedById ?? r2T4OverrideResultDetail.CreatedById;
                    var userDetail = this.SyUsersRepository.Get(x => x.UserId == userId).FirstOrDefault();
                    if (userDetail != null)
                    {
                        overrrideResults.UpdatedByFullName = userDetail.FullName;
                    }
                } 

                return overrrideResults;
            });
        }

        /// <inheritdoc />
        /// <summary>
        /// The GetR2T4ResultsByTerminationId action allows to fetch the R2T4 Result detail.
        /// </summary>
        /// <remarks>
        /// The GetR2T4ResultsByTerminationId action requires terminationId and based on the Id provided, it returns the matching R2T4 Result record.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns an object of R2T4 Results where R2T4 Result contains all the information related to R2T4 Result.
        /// </returns>
        public async Task<R2T4Result> GetR2T4ResultsByTerminationId(Guid terminationId)
        {
            return await Task.Run(() =>
            {
                var r2T4ResultDetail = this.R2T4ResultsRepository.Get(x => x.TerminationId == terminationId).LastOrDefault();
                return this.mapper.Map<R2T4Result>(r2T4ResultDetail);  
            });
        }

        /// <summary>
        /// The delete action is to delete the R2T4Results, R2T4OverrideResults from database by providing the terminationId.
        /// </summary>
        /// <remarks>
        /// The delete action is to delete the the R2T4Results, R2T4OverrideResults by providing the terminationId.
        /// If in case there is any exception/error encounters while deleting termination detail,
        /// the process will automatically roll back all the transactions and will return a message with the details.
        /// </remarks>
        /// <param name="terminationId">
        /// Termination Id is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns successful or failure message as result
        /// </returns>
        public async Task<string> Delete(Guid terminationId)
        {
            return await Task.Run(
                       () =>
                       {
                           try
                           {
                            // getting R2T4 override Results records based on termination id.
                               var overrideResults = this.R2T4OverrideResultsRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 override Results data exists.
                               if (overrideResults != null)
                               {
                                   this.R2T4OverrideResultsRepository.Delete(overrideResults);
                               }

                               // getting R2T4 Results records based on termination id.
                               var results = this.R2T4ResultsRepository.Get(e => e.TerminationId == terminationId);

                               // delete the records if R2T4 Results data exists.
                               if (results != null)
                               {
                                   this.R2T4ResultsRepository.Delete(results);
                               }

                               this.context.SaveChanges();
                               return $"{ApiMsgs.DELETE_SUCCESSFUL}";
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               return $"{ApiMsgs.DELETE_FAILED} {terminationId}";
                           }
                       });
        }

        /// <inheritdoc />
        /// <summary>
        /// The IsResultOverridden action allows to check if the R2T4 results is overridden or not for the given termination Id.
        /// </summary>
        /// <remarks>
        /// The IsResultOverridden action requires terminationId and based on the Id provided, it returns true if the R2T4 result record is overridden else false.
        /// </remarks>
        /// <param name="terminationId">
        /// The termination Id.
        /// </param>
        /// <returns>
        /// Returns a boolean.
        /// </returns>
        public async Task<bool> IsResultOverridden(Guid terminationId)
        {
            return await Task.Run(() =>
                { 
                    var r2T4OverrideResultDetail = this.R2T4OverrideResultsRepository.FirstOrDefault(x => x.TerminationId == terminationId);
                    return r2T4OverrideResultDetail != null;
                });
        }
    }
}
