﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusProgramVersionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the CampusProgramVersion Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.CampusProgram;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    /// <summary>
    /// The Campus Program Version service.
    /// </summary>
    public class CampusProgramVersionService : ICampusProgramVersionService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the campusProgramVersion Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CampusProgramVersionService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public CampusProgramVersionService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The Campus Program Version repository.
        /// </summary>
        private IRepository<ArCampusPrgVersions> CampusProgramVersionRepository => this.context.GetRepositoryForEntity<ArCampusPrgVersions>();

        /// <summary>
        /// The Get campus program version action returns campus program version details for given campus id and programversion id.
        /// </summary>
        /// <remarks>
        /// The get campus program version details action returns campus program version details.  
        /// </remarks> 
        /// <param name="campusId">
        /// The campusId id is required field and is a type of Guid.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id is required field and is a type of Guid.
        /// </param>  
        /// <returns>
        /// Returns campus program version details.
        /// </returns>
        public async Task<CampusProgramVersions> GetCampusProgramVersionDetails(Guid campusId, Guid programVersionId)
        {
            return await Task.Run(() =>
            {
                var campusProgramVersionDetails = this.CampusProgramVersionRepository.Get(s => s.CampusId == campusId && s.PrgVerId == programVersionId).FirstOrDefault();
                if (campusProgramVersionDetails == null)
                {
                    var campusProgramVersions = new CampusProgramVersions { ResultStatus = false };
                    return campusProgramVersions;
                }
                else
                {
                    var campusProgramVersions = this.mapper.Map<CampusProgramVersions>(campusProgramVersionDetails);
                    campusProgramVersions.ResultStatus = true;
                    return campusProgramVersions;
                }
            });
        }
        
        /// <summary>
        /// The Get campus program version action returns CampusProgramVersionId,CampusId,ProgramVersionId,IsTitleIV,
        /// IsFAMEApproved,IsSelfPaced,CalculationPeriodTypeId,AllowExcusAbsPerPayPeriod,TermSubEqualInLength,ModUser,ModDate by given campus id and programversion id.
        /// </summary>
        /// <remarks>
        /// The get campus program version action returns campus program version details.  
        /// </remarks> 
        /// <param name="campusId">
        /// The campusId id is required field and is a type of Guid.
        /// </param>
        /// <param name="programVersionId">
        /// The program version id is required field and is a type of Guid.
        /// </param>  
        /// <returns>
        /// Returns campus program version details.
        /// </returns>
        public async Task<CampusProgramVersions> GetCampusProgramVersion(Guid campusId, Guid programVersionId)
        {
            return await Task.Run(() =>
                {
                    var campusProgramVersionDetails = this.CampusProgramVersionRepository.Get(s => s.CampusId == campusId && s.PrgVerId == programVersionId).FirstOrDefault();

                    return this.mapper.Map<CampusProgramVersions>(campusProgramVersionDetails);
                });
        }

        /// <summary>
        /// The get campus program version.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<CampusProgramVersions>> GetCampusProgramVersion()
        {
            return await Task.Run(() =>
                {
                    var campusProgramVersionDetails = this.CampusProgramVersionRepository.Get(s => s.IsFameapproved).ToList();

                    return this.mapper.Map<IEnumerable<CampusProgramVersions>>(campusProgramVersionDetails);
                });
        }
    }
}
