﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransferGradesService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TransferGradesService Service type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TransferGrades;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;

    /// <summary>
    /// The transfer credits service.
    /// </summary>
    public class TransferGradesService : ITransferGradesService
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransferGradesService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public TransferGradesService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The transfer grades repository.
        /// </summary>
        private IRepository<ArTransferGrades> TransferGradesRepository => this.context.GetRepositoryForEntity<ArTransferGrades>();

        /// <summary>
        /// The get transfer grades returns list of TransferId,StuEnrollId,ReqId,GrdSysDetailId,Score,TermId,ModDate,ModUser,IsTransferred,CompletedDate,
        /// IsClinicsSatisfied,IsCourseCompleted,IsGradeOverridden,GradeOverriddenBy,GradeOverriddenDate.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The TransferGrades.
        /// </returns>
        public async Task<IEnumerable<TransferGradesDetails>> GetTransferGrades(Guid enrollmentId)
        {
            return await Task.Run(() =>
                {
                    var transferGradesDetails = this.TransferGradesRepository.Get(s => s.StuEnrollId == enrollmentId);
                    return this.mapper.Map<IEnumerable<TransferGradesDetails>>(transferGradesDetails);
                });
        }
    }
}
