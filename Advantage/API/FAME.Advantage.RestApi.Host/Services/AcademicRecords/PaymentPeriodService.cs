﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriodService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PaymentPeriodService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections;
using System.Linq.Expressions;
using FAME.Advantage.RestApi.DataTransferObjects.AFA;
using FAME.Orm.Advantage.Domain.Common;
using Microsoft.EntityFrameworkCore.Internal;

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Attendance;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The class section service.
    /// </summary>
    public class PaymentPeriodService : IPaymentPeriodService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// The attendance service
        /// </summary>
        private IAttendanceService attendanceService;
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPeriodService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="attendanceService">
        /// The attendance Service.
        /// </param>
        public PaymentPeriodService(IAdvantageDbContext context, IMapper mapper, IAttendanceService attendanceService)
        {
            this.context = context;
            this.mapper = mapper;
            this.attendanceService = attendanceService;
        }
        /// <summary>
        /// The campus repository.
        /// </summary>
        private IRepository<SyCampuses> CampusRepository => this.context.GetRepositoryForEntity<SyCampuses>();
        /// <summary>
        /// The student enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StudentEnrollmentRepository => this.context.GetRepositoryForEntity<ArStuEnrollments>();
        /// <summary>
        /// The AFA payment period staging repository.
        /// </summary>
        private IRepository<AfaPaymentPeriodStaging> AfaPaymentPeriodStagingRepository => this.context.GetRepositoryForEntity<AfaPaymentPeriodStaging>();
        /// <summary>
        /// The Title IV Sap results repository.
        /// </summary>
        private IRepository<ArFasapchkResults> TitleIvSapResultsRepository => this.context.GetRepositoryForEntity<ArFasapchkResults>();
        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        /// Update the afa staging table for payment periods
        /// </summary>
        /// <param name="paymentPeriods"></param>
        /// <returns></returns>
        public async Task<IList<IListItem<Guid, List<PaymentPeriod>>>> UpdateAfaPaymentPeriodStaging(IEnumerable<PaymentPeriod> paymentPeriods)
        {
            var ppGs = paymentPeriods
                .GroupBy(pp => pp.StudentEnrollmentID)
                .Select(pp => new { EnrollmentId = pp.Key, PaymentPeriods = pp.ToList() })
                .ToList();
            var failedResults = new List<IListItem<Guid, List<PaymentPeriod>>>();
            if (ppGs.Any())
            {
                foreach (var ppG in ppGs)
                {
                    if (StudentEnrollmentRepository.Get(se => se.StuEnrollId == ppG.EnrollmentId).Any())
                    {
                        try
                        {
                            var enrPaymentPeriods = AfaPaymentPeriodStagingRepository
                                .Get(apps => apps.StudentEnrollmentId == ppG.EnrollmentId
                                 && ppG.PaymentPeriods.Select(pp => pp.PaymentPeriodName).Contains(apps.Description));

                            if (enrPaymentPeriods.Any())
                            {
                                AfaPaymentPeriodStagingRepository.Delete(enrPaymentPeriods);
                            }
                            var latestPaymentPeriods = ppG.PaymentPeriods.Where(pp => !pp.DeleteRecord).GroupBy(pp => pp.PaymentPeriodName).Select(g => g.OrderByDescending(pp => pp.DateCreated)
                                .ThenByDescending(pp => pp.DateUpdated).First()).ToList();
                            var newPaymentPeriods = this.mapper.Map<List<AfaPaymentPeriodStaging>>(latestPaymentPeriods).OrderBy(_ => _.StartDate);
                            await AfaPaymentPeriodStagingRepository.CreateAsync(newPaymentPeriods);
                            AfaPaymentPeriodStagingRepository.Save();
                        }
                        catch (Exception e)
                        {
                            failedResults.Add(new ListItem<Guid, List<PaymentPeriod>>() { Text = ppG.EnrollmentId, Value = ppG.PaymentPeriods });
                        }
                    }
                    else
                    {
                        failedResults.Add(new ListItem<Guid, List<PaymentPeriod>>() { Text = ppG.EnrollmentId, Value = ppG.PaymentPeriods });
                    }

                }
            }

            return failedResults;
        }
        /// <summary>
        /// Method for calculating the payment period attendance for a given student enrollment id.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        public async Task<AdvStagingAttendanceV1> CalculatePaymentPeriodAttendance(Guid studentEnrollmentId)
        {
            var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                .Include(y => y.Lead).Include(se => se.StatusCode)
                .FirstOrDefault();

            if (enrollment != null)
            {
                //get corresponding enrollment payment period records from AFA staging table, making sure to order by the startdate
                var enrPaymentPeriods =
                    AfaPaymentPeriodStagingRepository.Get(apps => apps.StudentEnrollmentId == studentEnrollmentId)
                        .OrderBy(apps => apps.StartDate).ToList();

                //if we have records to work with
                if (enrPaymentPeriods.Any())
                {
                    //calculate the attendance for this enrollment
                    var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId));

                    if (enrollmentAttendance != null)
                    {
                        decimal paymentPeriodRT = 0;
                        PaymentPeriod periodToUpdate;
                        //if we only have one payment period record, this is the record to update
                        if (enrPaymentPeriods.Count() == 1)
                        {
                            periodToUpdate = this.mapper.Map<PaymentPeriod>(enrPaymentPeriods.FirstOrDefault());
                            periodToUpdate.Period = 1;
                            paymentPeriodRT = periodToUpdate.HoursCreditEnrolled;
                        }
                        //otherwise we need to find the period to update by matching the period with the students actual hours
                        else
                        {
                            decimal total = 0;
                            var actualHours = enrollmentAttendance.ActualHours;

                            //enrollment payment periods with running totals
                            var enrPaymentPeriodsWithRT = enrPaymentPeriods.Select(epp =>
                                new { PaymentPeriod = epp, Total = (total = total + epp.CreditOrHours) }).ToList();

                            var temp = enrPaymentPeriodsWithRT.Aggregate((epp, next) =>
                                actualHours >= next.Total ? next : epp);

                            periodToUpdate = this.mapper.Map<PaymentPeriod>(temp.PaymentPeriod);
                            periodToUpdate.Period = enrPaymentPeriodsWithRT.IndexOf(temp) + 1;
                            paymentPeriodRT = temp.Total;

                        }
                        //Student hasnt reached this payment period so skip
                        if (enrollmentAttendance.ActualHours < paymentPeriodRT)
                        {
                            return null;
                        }
                        //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                        var titleIvResults = TitleIvSapResultsRepository
                            .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                            .Include(x => x.TitleIvstatus)
                            .OrderBy(res => res.Period).ToList();
                        //default to passed title iv
                        var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                        //if we have title iv sap results to work with
                        if (titleIvResults.Any())
                        {
                            //lets try to find the result by matching on the period
                            var titleIvResult = titleIvResults
                                .FirstOrDefault(t4r => t4r.Period == periodToUpdate.Period)?.TitleIvstatus.Code;
                            //if we could not find a title iv sap result by the period, we need to take the last run title iv result
                            if (string.IsNullOrEmpty(titleIvResult))
                            {
                                titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;
                            }

                            //if we have the title iv result, lets see if its passing or failing
                            if (!string.IsNullOrEmpty(titleIvResult))
                            {
                                switch (titleIvResult)
                                {
                                    case TitleIVSapStatus.Passed:
                                    case TitleIVSapStatus.Warning:
                                    case TitleIVSapStatus.Probation:
                                        titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                        break;
                                    case TitleIVSapStatus.Ineligible:
                                        titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                        break;
                                    default:
                                        titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                        break;
                                }
                            }
                        }

                        //get the corresponding payment period status based on Advantage system status
                        var status = AFA.PaymentPeriodStatus.Enrolled;
                        var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                        switch (enrollmentStatus)
                        {
                            case (int)Constants.SystemStatus.Graduated:
                                status = AFA.PaymentPeriodStatus.Graduate;
                                break;
                            case (int)Constants.SystemStatus.NoStart:
                                status = AFA.PaymentPeriodStatus.NoShow;
                                break;
                            case (int)Constants.SystemStatus.LeaveOfAbsence:
                                status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                                break;
                            case (int)Constants.SystemStatus.Dropped:
                            case (int)Constants.SystemStatus.TransferOut:
                                status = AFA.PaymentPeriodStatus.Drop;
                                break;
                            case (int)Constants.SystemStatus.FutureStart:
                            case (int)Constants.SystemStatus.CurrentlyAttending:
                            case (int)Constants.SystemStatus.Suspension:
                            case (int)Constants.SystemStatus.Externship:
                                status = AFA.PaymentPeriodStatus.Enrolled;
                                break;
                            default:
                                status = AFA.PaymentPeriodStatus.Enrolled;
                                break;
                        }

                        //get the effective date of when period was first met
                        periodToUpdate.EffectiveDate = enrollmentAttendance.AttendanceSummary
                            .FirstOrDefault(ats => ats.ActualHours >= paymentPeriodRT)
                            ?.AttendedDate;

                        //set status and title IV Sap result
                        periodToUpdate.EnrollmentStatusDescription = status;
                        periodToUpdate.TitleIVSapResult = titleIvSapMesage;
                        var time = DateTime.Now;
                        var userName = context.GetTenantUser().Email;
                        return new AdvStagingAttendanceV1()
                        {
                            SISEnrollmentID = periodToUpdate.StudentEnrollmentID,
                            IDStudent = periodToUpdate.AfaStudentId,
                            DateCreated = time,
                            DateUpdated = time,
                            SSN = enrollment.Lead.Ssn,
                            EffectiveDate = periodToUpdate.EffectiveDate.GetValueOrDefault(),
                            EndDate = periodToUpdate.EndDate,
                            HoursCreditEarned = periodToUpdate.HoursCreditEnrolled,
                            LocationCMSID = enrollment.Campus.CmsId,
                            PaymentPeriodName = periodToUpdate.PaymentPeriodName,
                            SAP = periodToUpdate.TitleIVSapResult,
                            StartDate = periodToUpdate.StartDate,
                            UserCreated = userName,
                            UserUpdated = userName
                        };
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Method for calculating the payment period attendance for a given student enrollment id.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AdvStagingAttendanceV1>> CalculateAllPaymentPeriodAttendance(Guid studentEnrollmentId)
        {
            var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                .Include(y => y.Lead).Include(se => se.StatusCode)
                .FirstOrDefault();
            var attendanceList = new List<AdvStagingAttendanceV1>();

            if (enrollment != null)
            {
                //get corresponding enrollment payment period records from AFA staging table, making sure to order by the startdate
                var enrPaymentPeriods =
                    AfaPaymentPeriodStagingRepository.Get(apps => apps.StudentEnrollmentId == studentEnrollmentId)
                        .OrderBy(apps => apps.StartDate).ToList();

                //if we have records to work with
                if (enrPaymentPeriods.Any())
                {
                    //calculate the attendance for this enrollment
                    var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId));

                    if (enrollmentAttendance != null)
                    {

                        //otherwise we need to find the period to update by matching the period with the students actual hours

                        decimal total = 0;
                        var actualHours = enrollmentAttendance.ActualHours;

                        //enrollment payment periods with running totals
                        var enrPaymentPeriodsWithRT = enrPaymentPeriods.Select(epp =>
                            new { PaymentPeriod = epp, Total = (total = total + epp.CreditOrHours) }).ToList();

                        var temp = enrPaymentPeriodsWithRT.Aggregate((epp, next) =>
                            actualHours >= next.Total ? next : epp);

                        var maxPeriod = enrPaymentPeriodsWithRT.IndexOf(temp) + 1;
                        //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                        var titleIvResults = TitleIvSapResultsRepository
                            .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                            .Include(x => x.TitleIvstatus)
                            .OrderBy(res => res.Period).ToList();

                        decimal extraHours = 0;
                        var i = 0;
                        var shouldContinue = true;
                        decimal prevPaymentPeriodRT = enrPaymentPeriodsWithRT[0].Total;
                        do
                        {
                            PaymentPeriod periodToUpdate;
                            decimal paymentPeriodRT = 0;
                            decimal hoursEarned = 0;
                            var currPeriod = enrPaymentPeriodsWithRT[i];
                            periodToUpdate = this.mapper.Map<PaymentPeriod>(currPeriod.PaymentPeriod);
                            periodToUpdate.Period = i + 1;
                            paymentPeriodRT = currPeriod.Total;
                            var attendanceDayMet = enrollmentAttendance.AttendanceSummary.Where(ats => (ats.ActualHours + extraHours) >= paymentPeriodRT).FirstOrDefault();

                            if (attendanceDayMet == null)
                            {
                                attendanceDayMet = enrollmentAttendance.AttendanceSummary.LastOrDefault();
                                shouldContinue = false;
                            }

                            hoursEarned = attendanceDayMet.ActualHours - paymentPeriodRT >= 0 ? paymentPeriodRT : attendanceDayMet.ActualHours - prevPaymentPeriodRT < 0 ? attendanceDayMet.ActualHours : attendanceDayMet.ActualHours - prevPaymentPeriodRT;

                            //default to passed title iv
                            var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                            //if we have title iv sap results to work with
                            if (titleIvResults.Any())
                            {
                                //lets try to find the result by matching on the period
                                var titleIvResult = titleIvResults
                                    .FirstOrDefault(t4r => t4r.Period == periodToUpdate.Period)?.TitleIvstatus.Code;
                                //if we could not find a title iv sap result by the period, we need to take the last run title iv result
                                if (string.IsNullOrEmpty(titleIvResult))
                                {
                                    titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;
                                }

                                //if we have the title iv result, lets see if its passing or failing
                                if (!string.IsNullOrEmpty(titleIvResult))
                                {
                                    switch (titleIvResult)
                                    {
                                        case TitleIVSapStatus.Passed:
                                        case TitleIVSapStatus.Warning:
                                        case TitleIVSapStatus.Probation:
                                            titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                            break;
                                        case TitleIVSapStatus.Ineligible:
                                            titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                            break;
                                        default:
                                            titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                            break;
                                    }
                                }
                            }

                            //get the corresponding payment period status based on Advantage system status
                            var status = AFA.PaymentPeriodStatus.Enrolled;
                            var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                            switch (enrollmentStatus)
                            {
                                case (int)Constants.SystemStatus.Graduated:
                                    status = AFA.PaymentPeriodStatus.Graduate;
                                    break;
                                case (int)Constants.SystemStatus.NoStart:
                                    status = AFA.PaymentPeriodStatus.NoShow;
                                    break;
                                case (int)Constants.SystemStatus.LeaveOfAbsence:
                                    status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                                    break;
                                case (int)Constants.SystemStatus.Dropped:
                                case (int)Constants.SystemStatus.TransferOut:
                                    status = AFA.PaymentPeriodStatus.Drop;
                                    break;
                                case (int)Constants.SystemStatus.FutureStart:
                                case (int)Constants.SystemStatus.CurrentlyAttending:
                                case (int)Constants.SystemStatus.Suspension:
                                case (int)Constants.SystemStatus.Externship:
                                    status = AFA.PaymentPeriodStatus.Enrolled;
                                    break;
                                default:
                                    status = AFA.PaymentPeriodStatus.Enrolled;
                                    break;
                            }

                            //get the effective date of when period was first met
                            periodToUpdate.EffectiveDate = attendanceDayMet.AttendedDate;

                            //set status and title IV Sap result
                            periodToUpdate.EnrollmentStatusDescription = status;
                            periodToUpdate.TitleIVSapResult = titleIvSapMesage;
                            var time = DateTime.Now;
                            var userName = context.GetTenantUser().Email;

                            attendanceList.Add(new AdvStagingAttendanceV1()
                            {
                                SISEnrollmentID = periodToUpdate.StudentEnrollmentID,
                                IDStudent = periodToUpdate.AfaStudentId,
                                DateCreated = time,
                                DateUpdated = time,
                                SSN = enrollment.Lead.Ssn,
                                EffectiveDate = periodToUpdate.EffectiveDate.GetValueOrDefault(),
                                EndDate = periodToUpdate.EndDate,
                                HoursCreditEarned = hoursEarned,
                                LocationCMSID = enrollment.Campus.CmsId,
                                PaymentPeriodName = periodToUpdate.PaymentPeriodName,
                                SAP = periodToUpdate.TitleIVSapResult,
                                StartDate = periodToUpdate.StartDate,
                                UserCreated = userName,
                                UserUpdated = userName
                            });

                            prevPaymentPeriodRT = paymentPeriodRT;
                            i++;

                        }
                        while (shouldContinue && i < enrPaymentPeriodsWithRT.Count);



                    }
                }
                return attendanceList;
            }

            return null;
        }



        private async Task<AdvStagingAttendanceV1> CalculateCumulativeCore(ArStuEnrollments enrollment)
        {
            var studentEnrollmentId = enrollment.StuEnrollId;
            var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId));

            if (enrollmentAttendance != null)
            {

                var actualHours = enrollmentAttendance.ActualHours;

                //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                var titleIvResults = TitleIvSapResultsRepository
                    .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                    .Include(x => x.TitleIvstatus)
                    .OrderBy(res => res.Period).ToList();


                //default to passed title iv
                var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                //if we have title iv sap results to work with
                if (titleIvResults.Any())
                {
                    //we need to take the last run title iv result
                    var titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;


                    //if we have the title iv result, lets see if its passing or failing
                    if (!string.IsNullOrEmpty(titleIvResult))
                    {
                        switch (titleIvResult)
                        {
                            case TitleIVSapStatus.Passed:
                            case TitleIVSapStatus.Warning:
                            case TitleIVSapStatus.Probation:
                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                break;
                            case TitleIVSapStatus.Ineligible:
                                titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                break;
                            default:
                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                break;
                        }
                    }
                }

                //get the corresponding payment period status based on Advantage system status
                var status = AFA.PaymentPeriodStatus.Enrolled;
                var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                switch (enrollmentStatus)
                {
                    case (int)Constants.SystemStatus.Graduated:
                        status = AFA.PaymentPeriodStatus.Graduate;
                        break;
                    case (int)Constants.SystemStatus.NoStart:
                        status = AFA.PaymentPeriodStatus.NoShow;
                        break;
                    case (int)Constants.SystemStatus.LeaveOfAbsence:
                        status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                        break;
                    case (int)Constants.SystemStatus.Dropped:
                    case (int)Constants.SystemStatus.TransferOut:
                        status = AFA.PaymentPeriodStatus.Drop;
                        break;
                    case (int)Constants.SystemStatus.FutureStart:
                    case (int)Constants.SystemStatus.CurrentlyAttending:
                    case (int)Constants.SystemStatus.Suspension:
                    case (int)Constants.SystemStatus.Externship:
                        status = AFA.PaymentPeriodStatus.Enrolled;
                        break;
                    default:
                        status = AFA.PaymentPeriodStatus.Enrolled;
                        break;
                }

                //get the effective date of last attended date
                var effectiveDate = enrollmentAttendance.AttendanceSummary.OrderByDescending(ats => ats.AttendedDate).Select(ats => ats.AttendedDate).FirstOrDefault();
                var time = DateTime.Now;
                var userName = context.GetTenantUser().Email;

                //no valid effective date means this is not a valid attendance update
                if (effectiveDate == DateTime.MinValue)
                {
                    return null;
                }

                return new AdvStagingAttendanceV1()
                {
                    SISEnrollmentID = enrollment.StuEnrollId,
                    IDStudent = enrollment.Lead.AfaStudentId ?? 0,
                    DateCreated = time,
                    DateUpdated = time,
                    SSN = enrollment.Lead.Ssn,
                    EffectiveDate = enrollmentAttendance.LastDateAttended ?? effectiveDate,
                    LocationCMSID = enrollment.Campus.CmsId,
                    HoursCreditEarned = actualHours,
                    SAP = titleIvSapMesage,
                    UserCreated = userName,
                    UserUpdated = userName
                };

            }
            return null;
        }
        /// <summary>
        /// Method for calculating the cumulative payment period attendance for a given student enrollment id.
        /// </summary>
        /// <param name="studentEnrollmentId"></param>
        /// <returns></returns>
        public async Task<AdvStagingAttendanceV1> CalculateCumulativePaymentPeriodAttendance(Guid studentEnrollmentId)
        {
            var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                .Include(y => y.Lead).Include(se => se.StatusCode)
                .FirstOrDefault();
            AdvStagingAttendanceV1 attendance = null;

            if (enrollment != null)
            {

                attendance = await this.CalculateCumulativeCore(enrollment);

            }

            return attendance;
        }

        public async Task<IEnumerable<AdvStagingAttendanceV1>> CalculatePaymentPeriodAttendance(IEnumerable<Guid> studentEnrollmentIds)
        {
            var attendanceList = new List<AdvStagingAttendanceV1>();
            try
            {

                foreach (var studentEnrollmentId in studentEnrollmentIds)
                {

                    var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                   .Include(y => y.Lead).Include(se => se.StatusCode)
                   .FirstOrDefault();

                    if (enrollment != null)
                    {
                        //get corresponding enrollment payment period records from AFA staging table, making sure to order by the startdate
                        var enrPaymentPeriods =
                            AfaPaymentPeriodStagingRepository.Get(apps => apps.StudentEnrollmentId == studentEnrollmentId)
                                .OrderBy(apps => apps.StartDate).ToList();

                        //if we have records to work with
                        if (enrPaymentPeriods.Any())
                        {
                            //calculate the attendance for this enrollment
                            var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId));

                            if (enrollmentAttendance != null)
                            {
                                decimal paymentPeriodRT = 0;
                                PaymentPeriod periodToUpdate;
                                //if we only have one payment period record, this is the record to update
                                if (enrPaymentPeriods.Count() == 1)
                                {
                                    periodToUpdate = this.mapper.Map<PaymentPeriod>(enrPaymentPeriods.FirstOrDefault());
                                    periodToUpdate.Period = 1;
                                    paymentPeriodRT = periodToUpdate.HoursCreditEnrolled;
                                }
                                //otherwise we need to find the period to update by matching the period with the students actual hours
                                else
                                {
                                    decimal total = 0;
                                    var actualHours = enrollmentAttendance.ActualHours;

                                    //enrollment payment periods with running totals
                                    var enrPaymentPeriodsWithRT = enrPaymentPeriods.Select(epp =>
                                        new { PaymentPeriod = epp, Total = (total = total + epp.CreditOrHours) }).ToList();

                                    var temp = enrPaymentPeriodsWithRT.Aggregate((epp, next) =>
                                        actualHours >= next.Total ? next : epp);

                                    periodToUpdate = this.mapper.Map<PaymentPeriod>(temp.PaymentPeriod);
                                    periodToUpdate.Period = enrPaymentPeriodsWithRT.IndexOf(temp) + 1;
                                    paymentPeriodRT = temp.Total;
                                }
                                //Student hasnt reached this payment period so skip
                                if (enrollmentAttendance.ActualHours < paymentPeriodRT)
                                {
                                    continue;
                                }
                                //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                                var titleIvResults = TitleIvSapResultsRepository
                                    .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                                    .Include(x => x.TitleIvstatus)
                                    .OrderBy(res => res.Period).ToList();
                                //default to passed title iv
                                var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                                //if we have title iv sap results to work with
                                if (titleIvResults.Any())
                                {
                                    //lets try to find the result by matching on the period
                                    var titleIvResult = titleIvResults
                                        .FirstOrDefault(t4r => t4r.Period == periodToUpdate.Period)?.TitleIvstatus.Code;
                                    //if we could not find a title iv sap result by the period, we need to take the last run title iv result
                                    if (string.IsNullOrEmpty(titleIvResult))
                                    {
                                        titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;
                                    }

                                    //if we have the title iv result, lets see if its passing or failing
                                    if (!string.IsNullOrEmpty(titleIvResult))
                                    {
                                        switch (titleIvResult)
                                        {
                                            case TitleIVSapStatus.Passed:
                                            case TitleIVSapStatus.Warning:
                                            case TitleIVSapStatus.Probation:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                            case TitleIVSapStatus.Ineligible:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                                break;
                                            default:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                        }
                                    }
                                }

                                //get the corresponding payment period status based on Advantage system status
                                var status = AFA.PaymentPeriodStatus.Enrolled;
                                var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                                switch (enrollmentStatus)
                                {
                                    case (int)Constants.SystemStatus.Graduated:
                                        status = AFA.PaymentPeriodStatus.Graduate;
                                        break;
                                    case (int)Constants.SystemStatus.NoStart:
                                        status = AFA.PaymentPeriodStatus.NoShow;
                                        break;
                                    case (int)Constants.SystemStatus.LeaveOfAbsence:
                                        status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                                        break;
                                    case (int)Constants.SystemStatus.Dropped:
                                    case (int)Constants.SystemStatus.TransferOut:
                                        status = AFA.PaymentPeriodStatus.Drop;
                                        break;
                                    case (int)Constants.SystemStatus.FutureStart:
                                    case (int)Constants.SystemStatus.CurrentlyAttending:
                                    case (int)Constants.SystemStatus.Suspension:
                                    case (int)Constants.SystemStatus.Externship:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                    default:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                }

                                //get the effective date of when period was first met
                                periodToUpdate.EffectiveDate = enrollmentAttendance.AttendanceSummary
                                    .FirstOrDefault(ats => ats.ActualHours >= paymentPeriodRT)
                                    ?.AttendedDate;

                                //set status and title IV Sap result
                                periodToUpdate.EnrollmentStatusDescription = status;
                                periodToUpdate.TitleIVSapResult = titleIvSapMesage;
                                var time = DateTime.Now;
                                var userName = context.GetTenantUser().Email;


                                attendanceList.Add(new AdvStagingAttendanceV1()
                                {
                                    SISEnrollmentID = periodToUpdate.StudentEnrollmentID,
                                    IDStudent = periodToUpdate.AfaStudentId,
                                    DateCreated = time,
                                    DateUpdated = time,
                                    SSN = enrollment.Lead.Ssn,
                                    EffectiveDate = periodToUpdate.EffectiveDate.GetValueOrDefault(),
                                    EndDate = periodToUpdate.EndDate,
                                    HoursCreditEarned = periodToUpdate.HoursCreditEnrolled,
                                    LocationCMSID = enrollment.Campus.CmsId,
                                    PaymentPeriodName = periodToUpdate.PaymentPeriodName,
                                    SAP = periodToUpdate.TitleIVSapResult,
                                    StartDate = periodToUpdate.StartDate,
                                    UserCreated = userName,
                                    UserUpdated = userName
                                });
                            }
                        }
                    }
                }
            }

            catch (Exception e)
            {
                e.TrackException();
                Console.WriteLine(e);
            }

            return attendanceList;

        }

        /// <summary>
        /// Method for calculating the payment period attendance for a given student enrollment id.
        /// </summary>
        /// <param name="studentEnrollmentIds"></param>
        /// <returns></returns>
        public async Task<IDictionary<Guid, List<AdvStagingAttendanceV1>>> CalculateAllPaymentPeriodAttendance(IEnumerable<Guid> studentEnrollmentIds)
        {
            var studentsPPAttendance = new Dictionary<Guid, List<AdvStagingAttendanceV1>>();

            foreach (var studentEnrollmentId in studentEnrollmentIds)
            {
                var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                .Include(y => y.Lead).Include(se => se.StatusCode)
                .FirstOrDefault();

                var attendanceList = new List<AdvStagingAttendanceV1>();

                if (enrollment != null)
                {
                    //get corresponding enrollment payment period records from AFA staging table, making sure to order by the startdate
                    var enrPaymentPeriods =
                        AfaPaymentPeriodStagingRepository.Get(apps => apps.StudentEnrollmentId == studentEnrollmentId)
                            .OrderBy(apps => apps.StartDate).ToList();

                    //if we have records to work with
                    if (enrPaymentPeriods.Any())
                    {
                        //calculate the attendance for this enrollment
                        var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId));

                        if (enrollmentAttendance != null)
                        {

                            //otherwise we need to find the period to update by matching the period with the students actual hours


                            decimal total = 0;
                            var actualHours = enrollmentAttendance.ActualHours;

                            //enrollment payment periods with running totals
                            var enrPaymentPeriodsWithRT = enrPaymentPeriods.Select(epp =>
                                new { PaymentPeriod = epp, Total = (total = total + epp.CreditOrHours) }).ToList();

                            var temp = enrPaymentPeriodsWithRT.Aggregate((epp, next) =>
                                actualHours >= next.Total ? next : epp);

                            var maxPeriod = enrPaymentPeriodsWithRT.IndexOf(temp) + 1;
                            //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                            var titleIvResults = TitleIvSapResultsRepository
                                .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                                .Include(x => x.TitleIvstatus)
                                .OrderBy(res => res.Period).ToList();

                            decimal extraHours = 0;
                            var i = 0;
                            var shouldContinue = true;
                            decimal prevPaymentPeriodRT = enrPaymentPeriodsWithRT[0].Total;
                            do
                            {
                                PaymentPeriod periodToUpdate;
                                decimal paymentPeriodRT = 0;
                                decimal hoursEarned = 0;
                                var currPeriod = enrPaymentPeriodsWithRT[i];
                                periodToUpdate = this.mapper.Map<PaymentPeriod>(currPeriod.PaymentPeriod);
                                periodToUpdate.Period = i + 1;
                                paymentPeriodRT = currPeriod.Total;
                                var attendanceDayMet = enrollmentAttendance.AttendanceSummary.Where(ats => (ats.ActualHours + extraHours) >= paymentPeriodRT).FirstOrDefault();

                                if (attendanceDayMet == null)
                                {
                                    attendanceDayMet = enrollmentAttendance.AttendanceSummary.LastOrDefault();
                                    shouldContinue = false;
                                }

                                hoursEarned = attendanceDayMet.ActualHours - paymentPeriodRT >= 0 ? paymentPeriodRT : attendanceDayMet.ActualHours - prevPaymentPeriodRT < 0 ? attendanceDayMet.ActualHours : attendanceDayMet.ActualHours - prevPaymentPeriodRT;

                                //default to passed title iv
                                var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                                //if we have title iv sap results to work with
                                if (titleIvResults.Any())
                                {
                                    //lets try to find the result by matching on the period
                                    var titleIvResult = titleIvResults
                                        .FirstOrDefault(t4r => t4r.Period == periodToUpdate.Period)?.TitleIvstatus.Code;
                                    //if we could not find a title iv sap result by the period, we need to take the last run title iv result
                                    if (string.IsNullOrEmpty(titleIvResult))
                                    {
                                        titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;
                                    }

                                    //if we have the title iv result, lets see if its passing or failing
                                    if (!string.IsNullOrEmpty(titleIvResult))
                                    {
                                        switch (titleIvResult)
                                        {
                                            case TitleIVSapStatus.Passed:
                                            case TitleIVSapStatus.Warning:
                                            case TitleIVSapStatus.Probation:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                            case TitleIVSapStatus.Ineligible:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                                break;
                                            default:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                        }
                                    }
                                }

                                //get the corresponding payment period status based on Advantage system status
                                var status = AFA.PaymentPeriodStatus.Enrolled;
                                var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                                switch (enrollmentStatus)
                                {
                                    case (int)Constants.SystemStatus.Graduated:
                                        status = AFA.PaymentPeriodStatus.Graduate;
                                        break;
                                    case (int)Constants.SystemStatus.NoStart:
                                        status = AFA.PaymentPeriodStatus.NoShow;
                                        break;
                                    case (int)Constants.SystemStatus.LeaveOfAbsence:
                                        status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                                        break;
                                    case (int)Constants.SystemStatus.Dropped:
                                    case (int)Constants.SystemStatus.TransferOut:
                                        status = AFA.PaymentPeriodStatus.Drop;
                                        break;
                                    case (int)Constants.SystemStatus.FutureStart:
                                    case (int)Constants.SystemStatus.CurrentlyAttending:
                                    case (int)Constants.SystemStatus.Suspension:
                                    case (int)Constants.SystemStatus.Externship:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                    default:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                }

                                //get the effective date of when period was first met
                                periodToUpdate.EffectiveDate = attendanceDayMet.AttendedDate;

                                //set status and title IV Sap result
                                periodToUpdate.EnrollmentStatusDescription = status;
                                periodToUpdate.TitleIVSapResult = titleIvSapMesage;
                                var time = DateTime.Now;
                                var userName = context.GetTenantUser().Email;

                                attendanceList.Add(new AdvStagingAttendanceV1()
                                {
                                    SISEnrollmentID = periodToUpdate.StudentEnrollmentID,
                                    IDStudent = periodToUpdate.AfaStudentId,
                                    DateCreated = time,
                                    DateUpdated = time,
                                    SSN = enrollment.Lead.Ssn,
                                    EffectiveDate = periodToUpdate.EffectiveDate.GetValueOrDefault(),
                                    EndDate = periodToUpdate.EndDate,
                                    HoursCreditEarned = hoursEarned,
                                    LocationCMSID = enrollment.Campus.CmsId,
                                    PaymentPeriodName = periodToUpdate.PaymentPeriodName,
                                    SAP = periodToUpdate.TitleIVSapResult,
                                    StartDate = periodToUpdate.StartDate,
                                    UserCreated = userName,
                                    UserUpdated = userName
                                });

                                prevPaymentPeriodRT = paymentPeriodRT;
                                i++;

                            }
                            while (shouldContinue && i < enrPaymentPeriodsWithRT.Count);


                            studentsPPAttendance.Add(enrollment.StuEnrollId, attendanceList);
                        }
                    }
                }

            }
            return studentsPPAttendance;

        }

        public async Task<IEnumerable<AdvStagingAttendanceV1>> CalculateCumulativePaymentPeriodAttendance(IEnumerable<Guid> studentEnrollmentIds)
        {
            var attendanceList = new List<AdvStagingAttendanceV1>();
            try
            {

                foreach (var studentEnrollmentId in studentEnrollmentIds)
                {

                    var enrollment = StudentEnrollmentRepository.Get(se => se.StuEnrollId == studentEnrollmentId).Include(x => x.Campus)
                   .Include(y => y.Lead).Include(se => se.StatusCode)
                   .FirstOrDefault();

                    if (enrollment != null)
                    {
                        var attendanceUpdate = await this.CalculateCumulativeCore(enrollment);

                        if (attendanceUpdate != null)
                        {
                            attendanceList.Add(attendanceUpdate);
                        }
                    }
                }
            }

            catch (Exception e)
            {
                e.TrackException();
                Console.WriteLine(e);
            }

            return attendanceList;

        }

        public async Task<IList<AdvStagingAttendanceV1>> CalculatePaymentPeriodAttendanceByCampusForAFA(Guid campusId, bool recalculateAttendance = false)
        {
            var paymentPeriods = new List<AdvStagingAttendanceV1>();
            var cmsId = CampusRepository.Get(c => c.CampusId == campusId).Select(c => c.CmsId).FirstOrDefault();
            if (string.IsNullOrEmpty(cmsId))
            {
                return paymentPeriods;
            }

            var enrollments = StudentEnrollmentRepository.Get(a =>
                    a.CampusId == campusId && a.Lead.AfaStudentId.HasValue)
                .Include(y => y.Lead)
                .Include(x => x.StatusCode).ToList();

            if (enrollments.Any())
            {
                foreach (var enrollment in enrollments)
                {
                    try
                    {
                        var studentEnrollmentId = enrollment.StuEnrollId;
                        //get corresponding enrollment payment period records from AFA staging table, making sure to order by the startdate
                        var enrPaymentPeriods =
                            AfaPaymentPeriodStagingRepository.Get(apps => apps.StudentEnrollmentId == studentEnrollmentId)
                                .OrderBy(apps => apps.StartDate).ToList();

                        //if we have records to work with
                        if (enrPaymentPeriods.Any())
                        {
                            //calculate the attendance for this enrollment
                            var enrollmentAttendance = (await this.attendanceService.CalculateAttendance(studentEnrollmentId, recalculateAttendance));

                            if (enrollmentAttendance != null)
                            {
                                decimal paymentPeriodRT = 0;
                                PaymentPeriod periodToUpdate;
                                //if we only have one payment period record, this is the record to update
                                if (enrPaymentPeriods.Count() == 1)
                                {
                                    periodToUpdate = this.mapper.Map<PaymentPeriod>(enrPaymentPeriods.FirstOrDefault());
                                    periodToUpdate.Period = 1;
                                    paymentPeriodRT = periodToUpdate.HoursCreditEnrolled;

                                }
                                //otherwise we need to find the period to update by matching the period with the students actual hours
                                else
                                {
                                    decimal total = 0;
                                    var actualHours = enrollmentAttendance.ActualHours;

                                    //enrollment payment periods with running totals
                                    var enrPaymentPeriodsWithRT = enrPaymentPeriods.Select(epp =>
                                        new { PaymentPeriod = epp, Total = (total = total + epp.CreditOrHours) }).ToList();

                                    var temp = enrPaymentPeriodsWithRT.Aggregate((epp, next) =>
                                        actualHours >= next.Total ? next : epp);

                                    periodToUpdate = this.mapper.Map<PaymentPeriod>(temp.PaymentPeriod);
                                    periodToUpdate.Period = enrPaymentPeriodsWithRT.IndexOf(temp) + 1;
                                    paymentPeriodRT = temp.Total;

                                }

                                //Student hasnt reached this payment period so skip
                                if (enrollmentAttendance.ActualHours < paymentPeriodRT)
                                {
                                    continue;
                                }
                                //now we need to look for the title iv sap results for the specific student enrollment, making sure to order by the period
                                var titleIvResults = TitleIvSapResultsRepository
                                    .Get(t4r => t4r.StuEnrollId == studentEnrollmentId)
                                    .Include(x => x.TitleIvstatus)
                                    .OrderBy(res => res.Period).ToList();
                                //default to passed title iv
                                var titleIvSapMesage = AFA.TitleIVSapStatus.Passed;

                                //if we have title iv sap results to work with
                                if (titleIvResults.Any())
                                {
                                    //lets try to find the result by matching on the period
                                    var titleIvResult = titleIvResults
                                        .FirstOrDefault(t4r => t4r.Period == periodToUpdate.Period)?.TitleIvstatus.Code;
                                    //if we could not find a title iv sap result by the period, we need to take the last run title iv result
                                    if (string.IsNullOrEmpty(titleIvResult))
                                    {
                                        titleIvResult = titleIvResults.LastOrDefault()?.TitleIvstatus.Code;
                                    }

                                    //if we have the title iv result, lets see if its passing or failing
                                    if (!string.IsNullOrEmpty(titleIvResult))
                                    {
                                        switch (titleIvResult)
                                        {
                                            case TitleIVSapStatus.Passed:
                                            case TitleIVSapStatus.Warning:
                                            case TitleIVSapStatus.Probation:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                            case TitleIVSapStatus.Ineligible:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Failed;
                                                break;
                                            default:
                                                titleIvSapMesage = AFA.TitleIVSapStatus.Passed;
                                                break;
                                        }
                                    }
                                }

                                //get the corresponding payment period status based on Advantage system status
                                var status = AFA.PaymentPeriodStatus.Enrolled;
                                var enrollmentStatus = enrollment.StatusCode.SysStatusId;
                                switch (enrollmentStatus)
                                {
                                    case (int)Constants.SystemStatus.Graduated:
                                        status = AFA.PaymentPeriodStatus.Graduate;
                                        break;
                                    case (int)Constants.SystemStatus.NoStart:
                                        status = AFA.PaymentPeriodStatus.NoShow;
                                        break;
                                    case (int)Constants.SystemStatus.LeaveOfAbsence:
                                        status = AFA.PaymentPeriodStatus.LeaveOfAbsence;
                                        break;
                                    case (int)Constants.SystemStatus.Dropped:
                                    case (int)Constants.SystemStatus.TransferOut:
                                        status = AFA.PaymentPeriodStatus.Drop;
                                        break;
                                    case (int)Constants.SystemStatus.FutureStart:
                                    case (int)Constants.SystemStatus.CurrentlyAttending:
                                    case (int)Constants.SystemStatus.Suspension:
                                    case (int)Constants.SystemStatus.Externship:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                    default:
                                        status = AFA.PaymentPeriodStatus.Enrolled;
                                        break;
                                }

                                //get the effective date of when period was first met
                                periodToUpdate.EffectiveDate = enrollmentAttendance.AttendanceSummary
                                    .FirstOrDefault(ats => ats.ActualHours >= paymentPeriodRT)
                                    ?.AttendedDate;

                                //set status and title IV Sap result
                                periodToUpdate.EnrollmentStatusDescription = status;
                                periodToUpdate.TitleIVSapResult = titleIvSapMesage;
                                var time = DateTime.Now;
                                var userName = context.GetTenantUser().Email;
                                paymentPeriods.Add(new AdvStagingAttendanceV1()
                                {
                                    SISEnrollmentID = periodToUpdate.StudentEnrollmentID,
                                    IDStudent = periodToUpdate.AfaStudentId,
                                    DateCreated = time,
                                    DateUpdated = time,
                                    SSN = enrollment.Lead.Ssn,
                                    EffectiveDate = periodToUpdate.EffectiveDate.GetValueOrDefault(),
                                    EndDate = periodToUpdate.EndDate,
                                    HoursCreditEarned = periodToUpdate.HoursCreditEnrolled,
                                    LocationCMSID = cmsId,
                                    PaymentPeriodName = periodToUpdate.PaymentPeriodName,
                                    SAP = periodToUpdate.TitleIVSapResult,
                                    StartDate = periodToUpdate.StartDate,
                                    UserCreated = userName,
                                    UserUpdated = userName
                                });
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }

                }
            }
            return paymentPeriods;
        }

        /// <summary>
        /// Get payment period number by payment period name for a student enrollment
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="paymentPeriodName"></param>
        /// <returns></returns>
        public int? GetPaymentPeriodNumberByName(Guid stuEnrollId, string paymentPeriodName)
        {

            int? paymentPeriodNumber = null;
            var paymentPeriods = this.AfaPaymentPeriodStagingRepository.Get(pp => pp.StudentEnrollmentId == stuEnrollId).OrderBy(pp => pp.StartDate).ToList()
           .Select((j, i) => new { PaymentPeriodName = j.Description, Increment = i + 1 as int? });
            if (paymentPeriods.Any())
            {
                paymentPeriodNumber = paymentPeriods.Where(pp => pp.PaymentPeriodName == paymentPeriodName).Select(pp => pp.Increment).FirstOrDefault();
            }

            return paymentPeriodNumber;
        }


    }
}
