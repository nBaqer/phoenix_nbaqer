﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusChange.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusChange type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices.WindowsRuntime;
    using System.Threading.Tasks;

    using AutoMapper;

    using Castle.Components.DictionaryAdapter;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog.StatusChanges;
    using FAME.Extensions.Helpers;
    using FAME.Orm.Advantage.Domain.Common;

    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The graduation service.
    /// </summary>
    public class StudentStatusChangeService : IStudentStatusChangeService
    {
        /// <summary>
        /// The advantage unit of work.
        /// </summary>
        private readonly IAdvantageDbContext advantageUnitOfWork;

        /// <summary>
        /// The status codes service.
        /// </summary>
        private readonly IStatusCodesService statusCodesService;

        /// <summary>
        /// The statuses service.
        /// </summary>
        private readonly IStatusesService statusesService;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The attendance service.
        /// </summary>
        private readonly IAttendanceService attendanceService;

        /// <summary>
        /// The currently attending service.
        /// </summary>
        private readonly ICurrentlyAttendingService currentlyAttendingService;

        /// <summary>
        /// The graduation service.
        /// </summary>
        private readonly IGraduationService graduationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentStatusChangeService"/> class. 
        /// </summary>
        /// <param name="advantageUnitOfWork">
        /// The advantage unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusCodesService">
        /// The status codes service.
        /// </param>
        /// <param name="statusesService">
        /// The statuses service.
        /// </param>
        /// <param name="attendanceService">
        /// The attendance Service.
        /// </param>
        /// <param name="currentlyAttendingService">
        /// The currently Attending Service.
        /// </param>
        /// <param name="graduationService">
        /// The graduation Service.
        /// </param>
        public StudentStatusChangeService(
            IAdvantageDbContext advantageUnitOfWork,
            IMapper mapper,
            IStatusCodesService statusCodesService,
            IStatusesService statusesService,
            IAttendanceService attendanceService,
            ICurrentlyAttendingService currentlyAttendingService,
            IGraduationService graduationService)
        {
            this.advantageUnitOfWork = advantageUnitOfWork;
            this.statusCodesService = statusCodesService;
            this.statusesService = statusesService;
            this.mapper = mapper;
            this.attendanceService = attendanceService;
            this.graduationService = graduationService;
            this.currentlyAttendingService = currentlyAttendingService;
        }

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The credit summary repository.
        /// </summary>
        private IRepository<SyCreditSummary> CreditSummaryRepository => this.advantageUnitOfWork.GetRepositoryForEntity<SyCreditSummary>();

        /// <summary>
        /// The program version repository.
        /// </summary>
        private IRepository<ArPrgVersions> ProgramVersionRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArPrgVersions>();

        /// <summary>
        /// The grade book Wgt Details.
        /// </summary>
        private IRepository<ArGrdBkWgtDetails> GrdBkWgtDetailsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdBkWgtDetails>();


        /// <summary>
        /// The grade book Component Types.
        /// </summary>
        private IRepository<ArGrdComponentTypes> GrdComponentTypesRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdComponentTypes>();


        /// <summary>
        /// The grade book Component Types.
        /// </summary>
        private IRepository<ArGrdBkResults> GrdBkResultsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArGrdBkResults>();

        /// <summary>
        /// The student enrollments.
        /// </summary>
        private IRepository<ArStuEnrollments> StuEnrollmentsRepository => this.advantageUnitOfWork.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.advantageUnitOfWork.GetSimpleCacheConnectionString();


        /// <summary>
        /// The get graduation statuses.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetGraduationStatuses(Guid campusId)
        {
            return await Task.Run(
                       async () =>
                           {
                               var gradStatuses = (await this.statusCodesService.GetStudentStatusCodes(
                                                  campusId,
                                                  StudentStatuses.Graduated)).ToList();

                               var currentlyAttendingStatuses = (await this.statusCodesService.GetStudentStatusCodes(
                                                                     campusId,
                                                                     StudentStatuses.CurrentlyAttending)).ToList();

                               var statuses = gradStatuses.Union(currentlyAttendingStatuses).OrderBy(x => x.Text).ToList();
                               statuses.Insert(0, new ListItem<string, Guid>() { Text = "Select", Value = Guid.Empty });

                               return statuses;
                           });
        }

        /// <summary>
        /// The get statuses to filter.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStatusesToFilter(Guid campusId)
        {
            return await Task.Run(async () =>
                {
                    var statuses = (await this.statusCodesService.GetStudentStatusCodes(
                                          campusId,
                                          StudentStatuses.Graduated)).ToList();

                    statuses.Insert(0, new ListItem<string, Guid>() { Text = "In School Statuses", Value = Guid.Empty });

                    return statuses;
                });
        }

        /// <summary>
        /// The get have met options.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<IEnumerable<SelectListItem>> GetHaveMetOptions()
        {
            return await  Task.Run(
                        async () =>
                            {
                                bool atLeastOneProgramTrackHours =
                                    this.ProgramVersionRepository.Get(_ => _.Prog.Acid == (int)AcademicCalendar.ClockHour).FirstOrDefault()
                                    != null;
                               var list = new List<SelectListItem>();
                               list.Add(
                                   new SelectListItem()
                                       {
                                           Selected = atLeastOneProgramTrackHours,
                                           Text = "Actual Hours",
                                           Value = ((int)Constants.HaveMetOptions.ActualHours).ToString()
                                       });
                               list.Add(
                                   new SelectListItem()
                                   {
                                           Text = "Scheduled Hours",
                                           Value = ((int)Constants.HaveMetOptions.ScheduledHours).ToString()
                                       });
                               list.Add(
                                   new SelectListItem()
                                   {
                                           Text = "Actual/Scheduled Hours",
                                           Value = ((int)Constants.HaveMetOptions.ActualScheduledHours).ToString()
                                       });
                               list.Add(
                                   new SelectListItem()
                                   {
                                       Selected = !atLeastOneProgramTrackHours,
                                           Text = "Credits Earned",
                                           Value = ((int)Constants.HaveMetOptions.CreditsEarned).ToString()
                                       });

                               return list;
                           });
        }

        /// <summary>
        /// The get students that have met program length.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="filterByStatus">
        /// The filter by status.
        /// </param>
        /// <param name="haveMetOption">
        /// The have Met Option.
        /// </param>
        /// <param name="pageSize">
        /// The page size.
        /// </param>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <param name="hardLoad">
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<StudentReadyForGraduation>> GetStudentsThatHaveMetProgramLength(Guid campusId, Guid filterByStatus,  int haveMetOption, int pageSize, int page, bool hardLoad)
        {

            
            return await Task.Run(
                       async () =>
                           {
                               List<StudentReadyForGraduation> results =
                                   new List<StudentReadyForGraduation>();

                               List<Guid> statusCodesId = new List<Guid>();

                               if (filterByStatus == Guid.Empty)
                               {
                                   List<int> inSchoolStatuses = new List<int>();
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.AcademicProbation);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.CurrentlyAttending);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.DisciplinaryProbation);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.Externship);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.LeaveOfAbsence);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.Suspension);
                                   inSchoolStatuses.Add((int)InSchoolStudentStatuses.WarningProbation);


                                   statusCodesId = (await this.statusCodesService.GetStatusCodes(
                                                       campusId,
                                                       inSchoolStatuses)).Select(x => x.Value).ToList();

                               }
                               else
                               {
                                   statusCodesId.Add(filterByStatus);
                               }

                               if (pageSize == 1)
                               {
                                   pageSize = 100;
                               }

                             
                               var enrollments = (from enfrollments in this.EnrollmentRepository.Get(x => statusCodesId.Any(statusCode => statusCode == x.StatusCodeId) && x.CampusId == campusId
                                        && x.PrgVer.Prog.Acid == (int)AcademicCalendar.ClockHour)
                                                       select new StudentReadyForGraduation
                                                       {
                                                           Name = enfrollments.Lead.LastName + "," + enfrollments.Lead.FirstName + " (" + enfrollments.PrgVer.PrgVerDescrip + ")",
                                                           Status = enfrollments.StatusCode.StatusCodeDescrip,
                                                           MissingGrades = enfrollments.ArGrdBkResults.Count(x => x.Score == null && x.InstrGrdBkWgtDetail.GrdComponentType.SysComponentTypeId != 500 ) > 0 ? "Y" : "N",
                                                           LedgerBalance = enfrollments.SaTransactions.Where(y => y.Voided == false).Sum(m => m.TransAmount) > 0 ? "Y" : "N",
                                                           StudentEnrollmentId = enfrollments.StuEnrollId,
                                                           TotalRequiredForGraduation = enfrollments.PrgVer.Prog.Acid == (int)AcademicCalendar.ClockHour ? enfrollments.PrgVer.Hours : enfrollments.PrgVer.Credits,
                                                           IsClockHour = enfrollments.PrgVer.Prog.Acid == (int)AcademicCalendar.ClockHour,
                                                           CampusId = enfrollments.CampusId,
                                                           StudentId = enfrollments.StudentId,
                                                           ActualCreditsOrHours = enfrollments.TransferHours ?? 0
                                                       }
                                                       ).OrderBy(x => x.Name);


                               if (hardLoad)
                               {
                                   foreach (var enrollment in enrollments)
                                   {
                                       SimpleCache.Remove($"{this.CacheKeyFirstSegment}:metOption:{haveMetOption}:enrollmentMetProgramLength:{enrollment.StudentEnrollmentId}");

                                   }
                               }


                              if (filterByStatus == Guid.Empty)
                               {
                                   foreach (var enrollment in enrollments)
                                   {
                                       var cacheEnrollment = await SimpleCache.GetOrAddExisting(
                                           $"{this.CacheKeyFirstSegment}:metOption:{haveMetOption}:enrollmentMetProgramLength:{enrollment.StudentEnrollmentId}",
                                           async () =>
                                               {
                                                   if (enrollment.IsClockHour)
                                                   {
                                                       var attendance =
                                                           await this.attendanceService.CalculateAttendance(
                                                               enrollment.StudentEnrollmentId);
                                                       enrollment.ActualCreditsOrHours += attendance.ActualHours;

                                                       if (((haveMetOption == (int)Constants.HaveMetOptions.ActualHours || haveMetOption == 0)
                                                            && enrollment.ActualCreditsOrHours
                                                            >= enrollment.TotalRequiredForGraduation)
                                                           || (haveMetOption
                                                               == (int)Constants.HaveMetOptions.ScheduledHours
                                                               && attendance.ScheduledHours
                                                               >= enrollment.TotalRequiredForGraduation)
                                                           || (haveMetOption == (int)Constants.HaveMetOptions
                                                                   .ActualScheduledHours
                                                               && enrollment.ActualCreditsOrHours
                                                               >= enrollment.TotalRequiredForGraduation
                                                               && attendance.ScheduledHours
                                                               >= enrollment.TotalRequiredForGraduation))
                                                       {
                                                           return enrollment;
                                                       }
                                                   }
                                                   else
                                                   {
                                                       /*Until the implementation for Graduating a Credit hour school, the query to find enrollments will exclude all credit based enrollments and this piece of code will not be executed.*/
                                                       /*Find if the student enrollment have completed all the credits*/
                                                       var credits = this.CreditSummaryRepository
                                                           .Get(x => x.StuEnrollId == enrollment.StudentEnrollmentId)
                                                           .Sum(x => x.CreditsEarned);
                                                       enrollment.ActualCreditsOrHours = credits ?? 0;
                                                       if (haveMetOption == (int)Constants.HaveMetOptions.CreditsEarned
                                                           && enrollment.ActualCreditsOrHours >= enrollment.TotalRequiredForGraduation)
                                                       {
                                                           return enrollment;
                                                       }
                                                   }

                                                   return null;
                                               },
                                        CacheExpiration.SHORT);

                                       if (cacheEnrollment != null)
                                       {
                                           results.Add(cacheEnrollment);
                                       }
                                   }
                               }
                               else
                               {
                                   results = enrollments.ToList();
                               }


                               /*//For testing purpose bring first enrollment as ready to grad
                               results.Add(enrollments.FirstOrDefault());*/

                               return results;
                           });
        }

        /// <summary>
        /// The change status.
        /// </summary>
        /// <param name="campusId">
        /// The campus Id.
        /// </param>
        /// <param name="enrollmentsToUpdate">
        /// The enrollments To Update.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> ChangeStatus(Guid campusId, List<ListItem<Guid, Guid>> enrollmentsToUpdate)
        {
            return await Task.Run(
                       async () =>
                           {
                               var gradStatuses =
                                   (await this.statusCodesService.GetStudentStatusCodes(
                                        campusId,
                                        StudentStatuses.Graduated)).ToList();

                               var currentlyAttendingStatuses =
                                   (await this.statusCodesService.GetStudentStatusCodes(
                                        campusId,
                                        StudentStatuses.CurrentlyAttending)).ToList();

                               var changeToGrad = enrollmentsToUpdate
                                   .Where(x => gradStatuses.Any(y => y.Value == x.Text)).ToList();
                               var changeToCurrentlyAttending = enrollmentsToUpdate
                                   .Where(x => currentlyAttendingStatuses.Any(y => y.Value == x.Text)).ToList();

                               ActionResult<bool> actionResult = new ActionResult<bool>();
                               actionResult = await this.ChangeStatusTo(campusId, changeToGrad, this.graduationService);

                               if (actionResult.Result)
                               {
                                   actionResult = await this.ChangeStatusTo(
                                                      campusId,
                                                      changeToCurrentlyAttending,
                                                      this.currentlyAttendingService);
                               }

                               return actionResult;
                           });
        }

        /// <summary>
        /// The change status to.
        /// </summary>
        /// <param name="campusId">
        /// The campus id.
        /// </param>
        /// <param name="changeTo">
        /// The change To.
        /// </param>
        /// <param name="statusChange">
        /// The status change.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<ActionResult<bool>> ChangeStatusTo(
            Guid campusId,
            List<ListItem<Guid, Guid>> changeTo,
            IStatusChange statusChange)
        {
            return await Task.Run(async () =>
            {
                ActionResult<bool> actionResult = new ActionResult<bool>();
                actionResult.ResultStatusMessage = string.Empty;
                actionResult.ResultStatus = Enums.ResultStatus.Success;
                foreach (var change in changeTo)
                {
                    var changeResult = await statusChange.CanChange(campusId, change.Value);
                    if (changeResult.Result)
                    {
                        changeResult = await statusChange.ChangeStatus(campusId, change.Value, change.Text);
                        if (!changeResult.Result)
                        {
                            actionResult.ResultStatus = Enums.ResultStatus.Warning;
                            actionResult.ResultStatusMessage += changeResult.ResultStatusMessage + "Some errors have occured while changing the status.";
                        }
                    }
                    else
                    {
                        actionResult.ResultStatus = Enums.ResultStatus.Warning;
                        actionResult.ResultStatusMessage += changeResult.ResultStatusMessage + "The status cannot be changed.";
                    }
                }

                actionResult.Result = actionResult.ResultStatus == Enums.ResultStatus.Success;
                return actionResult;
            });
        }

    }
}
