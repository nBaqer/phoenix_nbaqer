﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UndoTerminationService.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   Defines the UndoTerminationService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Threading.Tasks;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <inheritdoc />
    /// <summary>
    /// The undo termination service.
    /// </summary>
    public class UndoTerminationService : IUndoTerminationService
    {
        /// <summary>
        /// The Enrollment Manager service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The student status service.
        /// </summary>
        private readonly IStudentStatusService studentStatusService;

        /// <summary>
        /// The document history service.
        /// </summary>
        private readonly IDocumentHistoryService documentHistoryService;

        /// <summary>
        /// The student termination service.
        /// </summary>
        private readonly IStudentTerminationService studentTerminationService;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

       /// <summary>
        /// Initializes a new instance of the <see cref="UndoTerminationService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment Service.
        /// </param>
        /// <param name="studentStatusService">
        /// The student status Service.       
        /// </param>
        /// <param name="documentHistoryService">
        /// The student history Service.
        /// </param>
        /// <param name="studentTerminationService">
        /// The student termination Service.
        /// </param>
        public UndoTerminationService(IAdvantageDbContext context, IEnrollmentService enrollmentService, IStudentStatusService studentStatusService,IDocumentHistoryService documentHistoryService, IStudentTerminationService studentTerminationService)
        {
            this.context = context;
            this.enrollmentService = enrollmentService;
            this.studentStatusService = studentStatusService;
            this.documentHistoryService = documentHistoryService;
            this.studentTerminationService = studentTerminationService;
        }

        /// <summary>
        /// The Undotermination action allows to reverse the termination when the student is terminated from the specified enrollment.
        /// </summary>
        /// <remarks>
        /// The Update action requires Enrollment object which has EnrollmentId, StudentId, EnrollmentDate, EffectiveDate, StartDate, DateDetermined, LastDateAttended, 
        /// UnitTypeDescription, ProgramVersionDescription, Status, StatusCode, StatusCodeDescription, CampusId, SSN, SystemStatusId, StatusCode, ResultStatus, 
        /// DropReasonId and StatusCodeId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollmentId object of type Enrollment.
        /// </param>
        /// <returns>
        ///  Returns successful or failure message as result.
        /// </returns>
        public async Task<string> UndoTermintion(Guid enrollmentId)
        {
            return await Task.Run(async () =>
            {
                try
                {
                    // Getting enrollment deatils based on termination id. 
                    var enrollmentDetails = await this.enrollmentService.Get(enrollmentId);

                    if (enrollmentDetails != null)
                    {
                        // Getting student's original status from syStudentStatusChanges.          
                        var studentStatusOriginal = await this.studentStatusService.GetStudentStatus(enrollmentId);

                        enrollmentDetails.DropReasonId = null;
                        enrollmentDetails.LastDateAttended = null;
                        enrollmentDetails.DateDetermined = null;
                        enrollmentDetails.StatusCodeId = studentStatusOriginal.OrigStatusId;

                        // Update ArStuEnrollments based on enrollmnet id with the original status.
                        var result = await this.enrollmentService.Update(enrollmentDetails);
                        if (result.ResultStatus == ApiMsgs.UPDATE_SUCCESS)
                        {
                            var studentId = enrollmentDetails.StudentId;

                            // Deleting student details based on enrollment id from syStudentStatusChanges.                   
                            var studentStatus = await this.studentStatusService.Delete(enrollmentId);
                           
                            if (studentStatus)
                            {
                                // Updating the IsArchived column based on student is and enrollment id
                                var docStatus = await this.documentHistoryService.Update(enrollmentId, studentId);

                                if (docStatus)
                                {
                                    var terminationDetails = await this.studentTerminationService.GetById(enrollmentId);
                                    if (terminationDetails != null)
                                    {
                                        var terminationStatus = await this.studentTerminationService.UpdateUndoTermination(terminationDetails);

                                        if (terminationStatus.ResultStatus == ApiMsgs.SAVE_STUDENT_TERMINATION_SUCCESS)
                                         {
                                            return $"{ApiMsgs.UNDO_TERMINATION_SUCCESS}";
                                         }
                                       else
                                         {
                                            return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                                         }
                                    }
                                }
                                else
                                {
                                    return $"{ApiMsgs.SAVE_UNSUCCESSFULL} {enrollmentId}";
                                }
                            }
                            else
                            {
                                return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                            }
                        }
                        else
                        {
                            return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                        }
                    }
                    else
                    {
                        return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                    }
                    return $"{ApiMsgs.UNDO_TERMINATION_SUCCESS}";
                }
                catch (Exception)
                {
                    return $"{ApiMsgs.NOT_FOUND} {enrollmentId}";
                }
            });
        }
    }
}