﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ClassSectionService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Security.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The class section service.
    /// </summary>
    public class ClassSectionService : IClassSectionService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The audit history service.
        /// </summary>
        private readonly IAuditHistoryService auditHistoryService;

        /// <summary>
        /// The campus group service.
        /// </summary>
        private readonly ICampusGroupService campusGroupService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassSectionService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status service.
        /// </param>
        /// <param name="auditHistoryService">
        /// The audit History Service.
        /// </param>
        /// <param name="campusGroupService">
        /// The campus Group Service.
        /// </param>
        public ClassSectionService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService, IAuditHistoryService auditHistoryService, ICampusGroupService campusGroupService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
            this.auditHistoryService = auditHistoryService;
            this.campusGroupService = campusGroupService;
        }

        /// <summary>
        /// The class section repository.
        /// </summary>
        private IRepository<ArClassSections> ClassSectionRepository => this.context.GetRepositoryForEntity<ArClassSections>();

        /// <summary>
        /// The class section terms repository.
        /// </summary>
        private IRepository<ArClassSectionTerms> ClassSectionTermsRepository => this.context.GetRepositoryForEntity<ArClassSectionTerms>();

        /// <summary>
        /// The terms repository.
        /// </summary>
        private IRepository<ArTerm> TermsRepository => this.context.GetRepositoryForEntity<ArTerm>();


        private IRepository<SyPeriods> PeriodsRepository => this.context.GetRepositoryForEntity<SyPeriods>();

        private IRepository<SyPeriodsWorkDays> PeriodDaysRepository => this.context.GetRepositoryForEntity<SyPeriodsWorkDays>();

        private IRepository<PlWorkDays> DaysRepository => this.context.GetRepositoryForEntity<PlWorkDays>();

        private IRepository<ArClsSectMeetings> ClassSectionMeetingsRepository => this.context.GetRepositoryForEntity<ArClsSectMeetings>();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The tenant user.
        /// </summary>
        private ITenantUser TenantUser => this.context.GetTenantUser();

        /// <summary>
        /// The insert class sections for program version.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> InsertClassSectionsForProgramVersion(Guid programVersionId)
        {
            return await Task.Run(async () =>
                {
                    ActionResult<bool> actionResult = new ActionResult<bool>();
                    actionResult.ResultStatus = Enums.ResultStatus.Success;

                    var term = this.TermsRepository
                        .Get(x => x.StatusId == this.ActiveStatusId && x.ProgramVersionId == programVersionId)
                        .Include(x => x.ProgramVersion).ThenInclude(x => x.ArProgVerDef).Include(x => x.CampGrp).ThenInclude(x => x.Campus).FirstOrDefault();

                    if (term != null && term.ProgramVersion != null)
                    {
                        if (term.ProgramVersion.ThGrdScaleId == null)
                        {
                            actionResult.ResultStatus = Enums.ResultStatus.Warning;
                            actionResult.ResultStatusMessage = ApiMsgs.ProgramRegistrationInsertWarning + ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Grade Scale Id";
                            return actionResult;
                        }

                        IList<ArProgVerDef> programDefinitions = term.ProgramVersion.ArProgVerDef.ToList();

                        if (programDefinitions.Any())
                        {
                            List<ArClassSections> classSectionsList = new List<ArClassSections>();
                            List<ArClassSectionTerms> classSectionTermses = new List<ArClassSectionTerms>();
                            /*
                             * Finding out when the courses were so the class section can have the same start date as when the course was assigned to the program version.
                             */
                            var auditHistoryEvents =
                                await this.auditHistoryService.GetAuditHistory<ArProgVerDef>(
                                    "I",
                                    "PrgVerId",
                                    programDefinitions.Select(x => x.PrgVerId.ToString()).ToList());

                            IList<Guid> campuses = new List<Guid>();
                            if (term.CampGrp.CampusId != null)
                            {
                                campuses.Add(term.CampGrp.CampusId.Value);
                            }
                            else
                            {
                                if (term.CampGrpId != null)
                                {
                                    campuses = await this.campusGroupService.GetAllTheCampusesByCampusGroup(term.CampGrpId.Value);
                                }
                            }
                            foreach (var programVersionDefinition in programDefinitions)
                            {
                                DateTime? classSectionStartDate = auditHistoryEvents
                                    .FirstOrDefault(x => x.Value == programVersionDefinition.PrgVerId.ToString())
                                    ?.EventDate;

                                if (classSectionStartDate == null)
                                {
                                    classSectionStartDate = new DateTime(programVersionDefinition.ModDate.Year, 1, 1);
                                }


                                foreach (var campus in campuses)
                                {
                                    Guid classSectionId = Guid.NewGuid();

                                    ArClassSections classSection = new ArClassSections
                                    {
                                        ClsSectionId = classSectionId,
                                        TermId = term.TermId,
                                        ReqId = programVersionDefinition.ReqId,
                                        ClsSection = programVersionDefinition.Req.Code,
                                        StartDate = classSectionStartDate.Value,
                                        EndDate = classSectionStartDate.Value.AddYears(99), /* Tweak to meet requirement of having a start date and end date for automatic program level registration */
                                        GrdScaleId = term.ProgramVersion.ThGrdScaleId.Value,
                                        MaxStud = int.MaxValue,
                                        ModUser = this.TenantUser.Email,
                                        ModDate = DateTime.Now,
                                        ProgramVersionDefinitionId = programVersionDefinition.ProgVerDefId,
                                        CampusId = campus
                                    };

                                    ArClassSectionTerms classSectionTerm =
                                        new ArClassSectionTerms
                                        {
                                            ClsSectionId = classSectionId,
                                            TermId = term.TermId,
                                            ClsSectTermId = Guid.NewGuid(),
                                            ModDate = DateTime.Now,
                                            ModUser = this.TenantUser.Email
                                        };

                                    classSectionsList.Add(classSection);
                                    classSectionTermses.Add(classSectionTerm);
                                }

                            }

                            this.ClassSectionRepository.Create(classSectionsList);
                            this.ClassSectionTermsRepository.Create(classSectionTermses);
                        }
                    }

                    return actionResult;
                });
        }

        /// <summary>
        /// The delete by program version.
        /// </summary>
        /// <param name="programVersionId">
        /// The program version id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> DeleteByProgramVersion(Guid programVersionId)
        {
            return await Task.Run(
                       () =>
                           {
                               ActionResult<bool> actionResult = new ActionResult<bool>();
                               actionResult.ResultStatus = Enums.ResultStatus.Success;
                               IList<ArClassSections> classSections = this.ClassSectionRepository
                                   .Get(x => x.ProgramVersionDefinition.PrgVerId == programVersionId)
                                   .Include(x => x.ProgramVersionDefinition).ThenInclude(x => x.PrgVer).Include(x => x.ArResults).ToList();

                               foreach (var classSection in classSections)
                               {
                                   if (classSection.ArResults.Count == 0)
                                   {
                                       this.ClassSectionTermsRepository.Delete(
                                           x => x.ClsSectionId == classSection.ClsSectionId);
                                       this.ClassSectionRepository.Delete(classSection);
                                   }
                                   else
                                   {
                                       actionResult.ResultStatus = Enums.ResultStatus.Error;
                                       actionResult.Result = false;
                                       actionResult.ResultStatusMessage =
                                           ApiMsgs.FailedToDeleteClassSectionResultsExist;
                                       return actionResult;
                                   }
                               }


                               this.ClassSectionRepository.Save();
                               actionResult.Result = true;

                               return actionResult;
                           });
        }

        /// <summary>
        /// The GetDetailByIds returns list of ClassSection Detail by given ClassSection Ids.
        /// </summary>
        /// <param name="classSectionIds">
        /// The class Section Ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<ClassSectionDetail>> GetDetailByIds(List<Guid> classSectionIds)
        {
            return await Task.Run(
                       () =>
                           {
                               var classSectionDetails = this.ClassSectionRepository.Get(x => classSectionIds.Contains(x.ClsSectionId));

                               return this.mapper.Map<IEnumerable<ClassSectionDetail>>(classSectionDetails);
                           });
        }

        /// <summary>
        /// The get detail by term ids.
        /// </summary>
        /// <param name="termIds">
        /// The term ids.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<IEnumerable<ClassSectionDetail>> GetDetailByTermIds(List<Guid> termIds)
        {
            return await Task.Run(
                       () =>
                           {
                               var classSectionDetails = this.ClassSectionRepository.Get(x => termIds.Contains(x.TermId));

                               return this.mapper.Map<IEnumerable<ClassSectionDetail>>(classSectionDetails);
                           });
        }

        /// <summary>
        /// The get lms periods.
        /// </summary>
        /// <param name="campusGroupdId">
        /// The campus groupd id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<IEnumerable<ClassSectionPeriodLMS>> GetLmsPeriods(Guid campusGroupdId)
        {
            return await Task.Run(
                      async () =>
                           {
                               var activeStatusId = await this.statusService.GetActiveStatusId();

                               var results = this.PeriodDaysRepository.Get(pd => pd.Period.CampGrpId == campusGroupdId && pd.Period.StatusId == activeStatusId)
                                   .Include(x => x.Period)
                                   .ThenInclude(x => x.StartTime)
                                   .Include(x => x.Period)
                                   .ThenInclude(x => x.EndTime)
                                   .Include(x => x.WorkDay)
                                   .GroupBy(g => g.PeriodId)
                                   .Select(
                                       g => new ClassSectionPeriodLMS()
                                       {
                                           Id = g.Key,
                                           Name = g.FirstOrDefault().Period.PeriodDescrip,
                                           Code = g.FirstOrDefault().Period.PeriodCode,
                                           StatTime =
                                                        g.FirstOrDefault() != null && g.FirstOrDefault().Period.StartTime
                                                            .TimeIntervalDescrip.HasValue
                                                            ? g.FirstOrDefault().Period
                                                                .StartTime.TimeIntervalDescrip
                                                                .Value.ToLongTimeString()
                                                            : "",
                                           EndTime =
                                               g.FirstOrDefault().Period.EndTime
                                                   .TimeIntervalDescrip.HasValue
                                                   ? g.FirstOrDefault().Period.EndTime
                                                       .TimeIntervalDescrip.Value
                                                       .ToLongTimeString()
                                                   : "",
                                           DaysOfWeek = g.Select(d => d.WorkDay.WorkDaysDescrip)
                                                            .ToList()
                                       }).ToList();

                               return results;
                           });
        }
    }
}
