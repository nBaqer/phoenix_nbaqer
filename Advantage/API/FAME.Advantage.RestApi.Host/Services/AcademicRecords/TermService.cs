﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TermService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Term Service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Terms;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;

    /// <summary>
    /// The term service.
    /// </summary>
    public class TermService : ITermService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the StudentEnrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TermService"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        public TermService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
        }

        /// <summary>
        /// The terms repository.
        /// </summary>
        private IRepository<ArTerm> TermsRepository => this.context.GetRepositoryForEntity<ArTerm>();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;

        /// <summary>
        /// The get terms method.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<Term> GetTerms(Guid enrollmentId)
        {
            return this.TermsRepository.Get(term =>
                    term.ArClassSections.Any(cs => cs.ArResults.Any(res => res.StuEnrollId == enrollmentId)) ||
                    term.ArTransferGrades.Any(tg => tg.StuEnrollId == enrollmentId))
                .Select(_ => new Term() { Id = _.TermId, StartDate = _.StartDate, EndDate = _.EndDate, Code = _.TermCode, Description = _.TermDescrip })
                .GroupBy(p => p.Id)
                .Select(g => g.FirstOrDefault()).OrderBy(_ => _.StartDate)
                .ToList();
        }

        /// <summary>
        /// The get term by program version id.
        /// </summary>
        /// <param name="programVersionId">
        /// The program Version Id.
        /// </param>
        /// <returns>
        /// The <see cref="Term"/>.
        /// </returns>
        public async Task<ActionResult<Term>> Get(Guid programVersionId)
        {
            return await Task.Run(
                       () =>
                           {
                               var term = this.TermsRepository.Get(
                                   x => x.StatusId == this.ActiveStatusId && x.ProgramVersionId == programVersionId).FirstOrDefault();
                               ActionResult<Term> actionResult = new ActionResult<Term>();
                               if (term == null)
                               {
                                   actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                                   actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                                   return actionResult;
                               }

                               actionResult.Result = this.mapper.Map<Term>(term);
                               return actionResult;
                           });
        }

        /// <summary>
        /// The insert term.
        /// </summary>
        /// <param name="term">
        /// The term.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult{Term}"/>.
        /// </returns>
        public async Task<ActionResult<Term>> Insert(Term term)
        {
            ActionResult<Term> result = new ActionResult<Term>();
            if (term == null)
            {
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = ApiMsgs.MALFORMED_MODEL_ERROR;
                return result;
            }

            result = this.ValidateModel(term, true);
            if (result.ResultStatus != Enums.ResultStatus.Success)
            {
                return result;
            }

            Guid activeStatusId = this.ActiveStatusId;
            ArTerm newTerm = new ArTerm
            {
                StatusId = activeStatusId,
                ProgramVersionId = term.ProgramVersionId,
                CampGrpId = term.CampusGroupId,
                StartDate = term.StartDate,
                EndDate = term.EndDate,
                TermCode = term.Code,
                TermDescrip = term.Description,
                ShiftId = term.ShiftId,
                ModUser = this.context.GetTenantUser().Email,
                ModDate = DateTime.Now,
                IsModule = term.IsModule,
                TermTypeId = term.TermTypeId,
                ProgId = term.ProgramId,
                MidPtDate = term.MidPointDate,
                MaxGradDate = term.MaximumGraduationDate,
                TermId = Guid.NewGuid()
            };

            return await Task.Run(
                       async () =>
                           {
                               await this.TermsRepository.CreateAsync(newTerm);
                               result.Result = term;
                               result.Result.Id = newTerm.TermId;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               return result;
                           });
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="term">
        /// The term.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<Term>> Update(Term term)
        {
            ActionResult<Term> result = new ActionResult<Term>();
            if (term == null)
            {
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = ApiMsgs.MALFORMED_MODEL_ERROR;
                return result;
            }

            result = this.ValidateModel(term, false);
            if (result.ResultStatus != Enums.ResultStatus.Success)
            {
                return result;
            }

            var entity = this.TermsRepository.Get(x => x.StatusId == this.ActiveStatusId && x.TermId == term.Id).FirstOrDefault();

            ActionResult<Term> actionResult = new ActionResult<Term>();
            if (entity == null)
            {
                actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                return actionResult;
            }

            entity.StatusId = this.ActiveStatusId;
            entity.ProgramVersionId = term.ProgramVersionId;
            entity.CampGrpId = term.CampusGroupId;
            entity.StartDate = term.StartDate;
            entity.EndDate = term.EndDate;
            entity.TermCode = term.Code;
            entity.TermDescrip = term.Description;
            entity.ShiftId = term.ShiftId;
            entity.ModUser = this.context.GetTenantUser().Email;
            entity.ModDate = DateTime.Now;
            entity.IsModule = term.IsModule;
            entity.TermTypeId = term.TermTypeId;
            entity.ProgId = term.ProgramId;
            entity.MidPtDate = term.MidPointDate;
            entity.MaxGradDate = term.MaximumGraduationDate;

            return await Task.Run(() =>
                           {
                               this.TermsRepository.Update(entity);
                               result.Result = term;
                               result.ResultStatus = Enums.ResultStatus.Success;
                               return result;
                           });
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="termId">
        /// The term id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ActionResult<bool>> Delete(Guid termId)
        {
            return await Task.Run(
                       () =>
                           {
                               ActionResult<bool> actionResult = new ActionResult<bool>();

                               var entity = this.TermsRepository.Get(x => x.StatusId == this.ActiveStatusId && x.TermId == termId).FirstOrDefault();

                               if (entity == null)
                               {
                                   actionResult.ResultStatus = Enums.ResultStatus.NotFound;
                                   actionResult.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                                   return actionResult;
                               }

                               this.TermsRepository.Delete(entity);

                               actionResult.Result = true;

                               return actionResult;
                           });
        }

        /// <summary>
        /// The get term by program action returns list of term details by given program id.
        /// </summary>
        /// <param name="programId">
        /// The program Id.
        /// </param>
        /// <returns>
        /// returns list of term details. 
        /// </returns>
        public List<Term> GetTermsByProgram(Guid programId)
        {
            return this.TermsRepository.Get(term => term.ProgId == programId && term.StatusId == this.ActiveStatusId).Select(
                    _ => new Term() { Id = _.TermId, StartDate = _.StartDate, EndDate = _.EndDate, Code = _.TermCode }).GroupBy(p => p.Id)
                .Select(g => g.FirstOrDefault()).OrderBy(_ => _.StartDate)
                .ToList();
        }
        
        /// <summary>
        /// The validate model.
        /// </summary>
        /// <param name="term">
        /// The term.
        /// </param>
        /// <param name="isInsert">
        /// The is Insert. If true validation for insert mode will take place, if false validation for update will take place.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private ActionResult<Term> ValidateModel(Term term, bool isInsert)
        {
            ActionResult<Term> result = new ActionResult<Term>();
            result.ResultStatus = Enums.ResultStatus.Success;

            if (term == null)
            {
                result.ResultStatus = Enums.ResultStatus.Error;
                result.ResultStatusMessage = ApiMsgs.MALFORMED_MODEL_ERROR;
                return result;
            }

            if (!isInsert)
            {
                if (term.Id == Guid.Empty)
                {
                    result.ResultStatus = Enums.ResultStatus.Warning;
                    result.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Id.";
                }
            }

            if (string.IsNullOrEmpty(term.Code))
            {
                result.ResultStatus = Enums.ResultStatus.Warning;
                result.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Code.";
            }

            if (string.IsNullOrEmpty(term.Description))
            {
                result.ResultStatus = Enums.ResultStatus.Warning;
                result.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Description.";
            }

            if (term.StatusId == Guid.Empty)
            {
                result.ResultStatus = Enums.ResultStatus.Warning;
                result.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Status Id.";
            }

            return result;
        }
    }
}
