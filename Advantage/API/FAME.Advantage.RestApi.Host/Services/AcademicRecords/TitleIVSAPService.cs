﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The title iv service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Fame.EFCore.Interfaces;
using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;
using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP;
    using FAME.Advantage.RestApi.DataTransferObjects.AFA;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AFA;
    using FAME.Extensions.Helpers;
    using Microsoft.EntityFrameworkCore.Extensions.Internal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// The title iv service.
    /// </summary>
    public class TitleIVSAPService : ITitleIVSAPService
    {
        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The advantage db context.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The campus service.
        /// </summary>
        private readonly ICampusService campusService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The afa service.
        /// </summary>
        private readonly IAfaService afaService;

        /// <summary>
        /// The system configuration app setting service
        /// </summary>
        private readonly ISystemConfigurationAppSettingService systemConfigurationAppSettingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIVSAPService"/> class.
        /// </summary>
        /// <param name="context">
        /// The db context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The mapper.
        /// </param>
        /// <param name="campusService"></param>
        /// <param name="enrollmentService"></param>
        /// <param name="afaService"></param>
        /// <param name="systemConfigurationAppSettingService"></param>
        public TitleIVSAPService(IAdvantageDbContext context, IMapper mapper, IStatusesService statusService, ICampusService campusService, IEnrollmentService enrollmentService, IAfaService afaService, ISystemConfigurationAppSettingService systemConfigurationAppSettingService)
        {
            this.context = context;
            this.mapper = mapper;
            this.statusService = statusService;
            this.campusService = campusService;
            this.enrollmentService = enrollmentService;
            this.afaService = afaService;
            this.systemConfigurationAppSettingService = systemConfigurationAppSettingService;
        }
        /// <summary>
        /// The title IV status repository.
        /// </summary>
        private IRepository<SyTitleIvsapStatus> SyTitleIvsapStatusRepository =>
            this.context.GetRepositoryForEntity<SyTitleIvsapStatus>();
        /// <summary>
        /// The Title IV results repository.
        /// </summary>
        private IRepository<ArFasapchkResults> ArTitleIvsapCheckResultsRepository =>
            this.context.GetRepositoryForEntity<ArFasapchkResults>();
        /// <summary>
        /// The Student Enrollments repository.
        /// </summary>
        private IRepository<ArStuEnrollments> StuEnrollmentsRepository =>
            this.context.GetRepositoryForEntity<ArStuEnrollments>();
        /// <summary>
        /// The programVersion  repository.
        /// </summary>
        private IRepository<ArPrgVersions> ProgramVersionRepository =>
           this.context.GetRepositoryForEntity<ArPrgVersions>();
        /// <summary>
        /// The SAP policy details repository.
        /// </summary>
        protected IRepository<ArSap> ArSapRepository =>
            this.context.GetRepositoryForEntity<ArSap>();
        /// <summary>
        /// The Title Iv sap Custom Verbiage repository.
        /// </summary>
        protected IRepository<SyTitleIvsapCustomVerbiage> syTitleIVSapCustomVerbiageRepository =>
            this.context.GetRepositoryForEntity<SyTitleIvsapCustomVerbiage>();
        /// <summary>
        ///         /// <summary>
        /// The Sap details repository.
        /// </summary>
        protected IRepository<ArSapdetails> ArSapdetailsRepository =>
            this.context.GetRepositoryForEntity<ArSapdetails>();
        /// <summary>
        /// The cache key first segment .
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();
        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;


        /// <summary>
        /// The get Title IV results for student.
        /// </summary>
        /// <param name="studentId">
        /// The student id.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable<TitleIVSAPResult>"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<IEnumerable<TitleIVSAPResult>> GetTitleIVResultsForStudent(FASAPStatus fasap)
        {
            return await Task.Run(() =>
            {
                TitleIVSAPResult canBeUpdated = new TitleIVSAPResult();
                IEnumerable<TitleIVSAPResult> results = new List<TitleIVSAPResult>();
                IQueryable<TitleIVSAPResult> query = ArTitleIvsapCheckResultsRepository
                    .Get(r => r.StuEnrollId == fasap.EnrollmentId)
                    .Select(r => new
                    {
                        TriggeredAt = Math.Round(r.Sapdetail.TrigValue / 100.0, 2) + " " + r.Sapdetail.TrigUnitTyp.TrigUnitTypDescrip + " after the " + r.Sapdetail.TrigOffsetTyp.TrigOffTypDescrip + (r.Sapdetail.TrigOffsetSeq.HasValue ? r.Sapdetail.TrigOffsetSeq.Value.ToString() : string.Empty),
                        Reason = r.Comments,
                        Increment = r.Period,
                        IsMakingSAP = r.IsMakingSap,
                        MinQuantitative = r.Sapdetail.QuantMinValue,
                        MinQualitative = r.Sapdetail.QualMinValue,
                        SAPStatusCode = r.TitleIvstatus.Code,
                        SAPStatus = r.TitleIvstatus.Description,
                        FASAPCheckResultsId = r.StdRecKey,
                        StudentEnrollmentId = r.StuEnrollId,
                        DatePerformed = r.DatePerformed,
                        TitleIVSAPId = r.TitleIvstatusId,
                        canMoveToProbation = false,
                        SAPCheckDate = r.ModDate,
                        CheckPointDate = r.CheckPointDate,
                    })
                    .Select(
                        r => new TitleIVSAPResult()
                        {
                            TriggeredAt = r.TriggeredAt,
                            Reason = r.SAPStatusCode == TitleIVSapStatus.Probation
                            ? r.Reason.Replace(TitleIV.PlaceHolders.Minimum_Quantity, r.MinQuantitative.ToString())
                            .Replace(TitleIV.PlaceHolders.Minimum_GPA, r.MinQualitative.ToString()) : r.Reason,
                            Increment = r.Increment,
                            IsMakingSAP = r.IsMakingSAP,
                            SAPStatus = r.SAPStatus,
                            FASAPCheckResultsId = r.FASAPCheckResultsId,
                            StudentEnrollmentId = r.StudentEnrollmentId,
                            DatePerformed = r.DatePerformed,
                            TitleIVSAPId = r.TitleIVSAPId,
                            canMoveToProbation = r.canMoveToProbation,
                            SAPCheckDate = r.SAPCheckDate,
                            CheckPointDate = r.CheckPointDate,

                        });


                canBeUpdated = EligibleForTitleIVProbation(fasap.EnrollmentId.ToString());
                if (canBeUpdated != null)
                {
                    List<TitleIVSAPResult> inMemoryQuery = query.OrderByDescending(d => d.DatePerformed).ToList();
                    inMemoryQuery.Where(x => x.FASAPCheckResultsId == canBeUpdated.FASAPCheckResultsId).First().canMoveToProbation = true;
                    query = inMemoryQuery.AsQueryable();
                }

                return query.ToList();
            });
        }

        /// <summary>
        /// The get Title IV results for multiple student.
        /// </summary>
        /// <param name="input">
        /// The enrollment ids.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable<TitleIVSAPResult>"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public async Task<IEnumerable<TitleIVSAPResult>> GetTitleIVResultsForMultipleStudents(TitleIVSAPInput input)
        {

            return await Task.Run(() =>
            {
                Expression<Func<ArStuEnrollments, bool>> filter = f => true;
                if (input == null || !input.ProgramVersionId.HasValue && !(input.Enrollments != null && input.Enrollments.Count > 0))
                {
                    return null;
                }
                else
                {
                    if (input.CampusId.HasValue)
                    {
                        filter = filter.And(f => f.CampusId == input.CampusId);
                    }
                    if (input.ProgramVersionId.HasValue)
                    {
                        filter = filter.And(f => f.PrgVerId == input.ProgramVersionId);
                    }
                    else if (input.Enrollments != null && input.Enrollments.Count > 0)
                    {
                        filter = filter.And(f => input.Enrollments.Contains(f.StuEnrollId));
                    }

                    if (!string.IsNullOrEmpty(input.Filter))
                    {
                        input.Filter = input.Filter.ToLower().Trim();
                        filter = filter.And(
                            f => (f.Lead.FirstName.ToLower().Contains(input.Filter)
                                  || f.Lead.LastName.ToLower().Contains(input.Filter)
                                  || f.Lead.MiddleName.ToLower().Contains(input.Filter)));

                    }

                    List<ArStuEnrollments> query = StuEnrollmentsRepository.Get(filter).Include(x => x.Lead)
                        .Include(x => x.ArFasapchkResults).ThenInclude(y => y.Sapdetail)
                        .Include(x => x.ArFasapchkResults).ThenInclude(y => y.TitleIvstatus)
                        .Include(x => x.ArFasapchkResults).ThenInclude(y => y.Status).ToList();

                    IEnumerable<TitleIVSAPResult> result = query.Select(
                        se => se.ArFasapchkResults.OrderBy(res => res.Period).Select(
                            g => new TitleIVSAPResult
                            {
                                Reason = !string.IsNullOrEmpty(g.Comments) ? g.TitleIvstatus.Code == TitleIVSapStatus.Probation
                            ? g.Comments.Replace(TitleIV.PlaceHolders.Minimum_Quantity, g.Sapdetail.QuantMinValue.ToString())
                            .Replace(TitleIV.PlaceHolders.Minimum_GPA, g.Sapdetail.QualMinValue.ToString()) : g.Comments : "",
                                Increment = g.Period,
                                IsMakingSAP = g.IsMakingSap,
                                DatePerformed = g.DatePerformed,
                                SAPCheckDate = g.ModDate,
                                SAPStatus = g.TitleIvstatus.Description,
                                LeadId = se.LeadId,
                                StudentEnrollmentId = se.StuEnrollId,
                                CheckPointDate = g.CheckPointDate,
                                Active = g.Status.StatusCode.ToUpper() == "A",
                                StudentName =
                                             se.Lead.FirstName + " " + se.Lead.MiddleName + " "
                                             + se.Lead.LastName + " ("
                                             + (!string.IsNullOrEmpty(se.Lead.Ssn)
                                                    ? se.Lead.Ssn.Replace(
                                                        se.Lead.Ssn.Substring(0, 5),
                                                        SsnMask.Ssn)
                                                    : "") + ")"
                            })).SelectMany(x => x);

                    List<TitleIVSAPResult> asList = result.ToList();

                    return asList;
                }
            });
        }

        /// <summary>
        /// The get Title IV results as IQueryable.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        public IQueryable<ArFasapchkResults> GetTitleIVResults(Guid stuEnrollId)
        {
            return ArTitleIvsapCheckResultsRepository.Get(res =>
                res.StuEnrollId == stuEnrollId && !res.PreviewSapCheck);
        }
        /// <summary>
        /// The get the last run increment.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        public int GetLastRunIncrement(Guid stuEnrollId)
        {
            ArFasapchkResults t4Res = GetTitleIVResults(stuEnrollId).OrderByDescending(res => res.Period).FirstOrDefault();
            return t4Res?.Period ?? 0;
        }
        /// <summary>
        /// The get the previously run result for the enrollment id.
        /// </summary>
        /// <param name="stuEnrollId">
        /// The student enrollment id.
        /// </param>
        /// <param name="increment">
        /// The increment to run.
        /// </param>
        public string GetPreviouslyRunResult(Guid stuEnrollId, int increment)
        {
            ArFasapchkResults prevRunToIncrement = GetTitleIVResults(stuEnrollId)
                .OrderByDescending(res => res.Period).ThenByDescending(res => res.DatePerformed).Include(res => res.TitleIvstatus)
                .FirstOrDefault(res => (res.Period == (increment - 1)));

            return prevRunToIncrement?.TitleIvstatus.Code;
        }
        /// <summary>
        /// The get Title IV Status id based on result.
        /// </summary>
        /// <param name="result">
        /// The SAP result.
        /// </param>
        public int? GetTitleIVSapStatusId(SAPResult result)
        {
            if (result.Passed)
            {
                return SyTitleIvsapStatusRepository
                    .Get(t4 => t4.Code == TitleIVSapStatus.Passed && t4.StatusId == ActiveStatusId).FirstOrDefault()
                    ?.Id;
            }

            else
            {

                string latestStatusCode = result.SapDetail.Increment == 1 ? TitleIVSapStatus.Passed : GetPreviouslyRunResult(result.StudentEnrollmentId, result.SapDetail.Increment);

                if (latestStatusCode != null)
                {
                    switch (latestStatusCode)
                    {
                        case TitleIVSapStatus.Passed:
                            return SyTitleIvsapStatusRepository
                                .Get(t4 => t4.Code == TitleIVSapStatus.Warning && t4.StatusId == ActiveStatusId).FirstOrDefault()
                                ?.Id;
                        case TitleIVSapStatus.Warning:
                        case TitleIVSapStatus.Ineligible:
                            return SyTitleIvsapStatusRepository
                                .Get(t4 => t4.Code == TitleIVSapStatus.Ineligible && t4.StatusId == ActiveStatusId).FirstOrDefault()
                                ?.Id;
                        case TitleIVSapStatus.Probation:
                            return SyTitleIvsapStatusRepository
                                .Get(t4 => t4.Code == TitleIVSapStatus.Probation && t4.StatusId == ActiveStatusId).FirstOrDefault()
                                ?.Id;
                        default:
                            break;
                    }
                }

            }

            return null;
        }

        /// <summary>
        /// The Place Student On TitleIV Prbation.
        /// </summary>
        /// <param name="FASAPCheckResultsId">
        /// The FASAPCheckResultsId.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>

        public async Task<Boolean> PlaceStudentOnTitleIVPrbation(string FASAPCheckResultsId)
        {
            bool success = false;
            ArFasapchkResults toUpdate = ArTitleIvsapCheckResultsRepository.Get(r => r.StdRecKey.ToString() == FASAPCheckResultsId).FirstOrDefault();
            TitleIVSAPResult canBeUpdated = EligibleForTitleIVProbation(toUpdate.StuEnrollId.ToString());

            if (canBeUpdated != null)
            {
                await Task.Run(async () =>
                {
                    int statusID = (int)TitleIVStatuses.Probation;
                    success = await UpdateTitleIVResultsForStudent(FASAPCheckResultsId, statusID);

                    if (success)
                    {
                        var campusId = await this.enrollmentService.GetCampusIdForEnrollment(toUpdate.StuEnrollId);
                        var afaIntegrationEnabled = this.campusService.IsAFAIntegrationEnabled(campusId);

                        if (afaIntegrationEnabled)
                        {
                            var attendanceUpdate = await this.afaService.CalculateAttendanceUpdatesForEnrollment(toUpdate.StuEnrollId);
                            if (attendanceUpdate != null)
                            {
                                await this.afaService.PostAttendanceUpdatesToAfa(new List<AdvStagingAttendanceV1> { attendanceUpdate });
                            }
                        }
                    }
                });
            }
            return success;
        }
        /// <summary>
        /// The Place Student On TitleIV Prbation.
        /// </summary>
        /// <param name="FASAPCheckResultsId">
        /// The FASAPCheckResultsId.
        /// </param>
        /// <param name="statusID">
        /// The statusID for they type of TitleIVStatuses.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        async Task<Boolean> CreateNewTitleIVResultsForStudent(string FASAPCheckResultsId, int statusID)
        {
            bool success = false;

            await Task.Run(() =>
            {

                ArFasapchkResults oldRecord = ArTitleIvsapCheckResultsRepository
                      .Get(x => x.StdRecKey.ToString() == FASAPCheckResultsId).FirstOrDefault();

                if (oldRecord != null)
                {

                    ArFasapchkResults newRecord = new ArFasapchkResults()
                    {
                        StdRecKey = Guid.NewGuid(),
                        StuEnrollId = oldRecord.StuEnrollId,
                        Period = oldRecord.Period,
                        IsMakingSap = oldRecord.IsMakingSap,
                        HrsAttended = oldRecord.HrsAttended,
                        HrsEarned = oldRecord.HrsEarned,
                        CredsAttempted = oldRecord.CredsAttempted,
                        CredsEarned = oldRecord.CredsEarned,
                        Gpa = oldRecord.Gpa,
                        DatePerformed = oldRecord.DatePerformed,
                        Comments = oldRecord.Comments,
                        ModUser = context.GetTenantUser().Email,
                        ModDate = DateTime.Now,
                        SapdetailId = oldRecord.SapdetailId,
                        Average = oldRecord.Average,
                        Attendance = oldRecord.Attendance,
                        CumFinAidCredits = oldRecord.CumFinAidCredits,
                        PercentCompleted = oldRecord.PercentCompleted,
                        CheckPointDate = oldRecord.CheckPointDate,
                        TermStartDate = oldRecord.TermStartDate,
                        PreviewSapCheck = oldRecord.PreviewSapCheck,
                        TitleIvstatusId = statusID,
                    };
                    SimpleCache.Remove($"{this.CacheKeyFirstSegment}:TitleIVSAPResults:{oldRecord.StuEnrollId}");
                    context.GetRepositoryForEntity<ArFasapchkResults>().Create(newRecord);
                    try
                    {
                        context.SaveChanges();
                        success = true;
                    }
                    catch (Exception e)
                    {
                        e.TrackException();
                        success = false;
                    }

                }

            });
            return success;
        }

        /// <summary>
        /// The update title iv results for student.
        /// </summary>
        /// <param name="FASAPCheckResultsId">
        /// The fasap check results id.
        /// </param>
        /// <param name="statusID">
        /// The status id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        async Task<Boolean> UpdateTitleIVResultsForStudent(string FASAPCheckResultsId, int statusID)
        {
            bool success = false;

            await Task.Run(() =>
                {
                    ArFasapchkResults titleIVtoUpdate = ArTitleIvsapCheckResultsRepository
                        .Get(x => x.StdRecKey.ToString() == FASAPCheckResultsId).Include(t4 => t4.Sapdetail).ThenInclude(sd => sd.Sap).ThenInclude(sap => sap.SyTitleIvsapCustomVerbiage).FirstOrDefault();
                    SyTitleIvsapStatus defaultComments = SyTitleIvsapStatusRepository.Get(x => x.Id == statusID).FirstOrDefault();

                    if (titleIVtoUpdate != null)
                    {
                        string customVerbiage = titleIVtoUpdate.Sapdetail.Sap.SyTitleIvsapCustomVerbiage.Where(cv => cv.TitleIvsapStatusId == statusID).Select(cv => cv.Message).FirstOrDefault();

                        titleIVtoUpdate.IsMakingSap = titleIVtoUpdate.IsMakingSap;
                        titleIVtoUpdate.TitleIvstatusId = statusID;
                        titleIVtoUpdate.DatePerformed = titleIVtoUpdate.DatePerformed;
                        titleIVtoUpdate.CheckPointDate = titleIVtoUpdate.CheckPointDate;
                        titleIVtoUpdate.StuEnrollId = titleIVtoUpdate.StuEnrollId;
                        titleIVtoUpdate.PreviewSapCheck = false;
                        titleIVtoUpdate.SapdetailId = titleIVtoUpdate.SapdetailId;
                        titleIVtoUpdate.Period = titleIVtoUpdate.Period;
                        titleIVtoUpdate.Comments = customVerbiage ?? defaultComments.DefaultMessage;
                        titleIVtoUpdate.PercentCompleted = titleIVtoUpdate.PercentCompleted;
                        titleIVtoUpdate.ModDate = DateTime.Now;
                        titleIVtoUpdate.ModUser = context.GetTenantUser().Email;


                        SimpleCache.Remove($"{this.CacheKeyFirstSegment}:TitleIVSAPResults:{titleIVtoUpdate.StuEnrollId}");
                        context.GetRepositoryForEntity<ArFasapchkResults>().Update(titleIVtoUpdate);
                        try
                        {
                            context.SaveChanges();
                            success = true;
                        }
                        catch (Exception e)
                        {
                            e.TrackException();
                            success = false;
                        }
                    }
                });

            return success;
        }
        /// <summary>
        /// Check if student is Eligible to be put in Probation.
        /// </summary>
        /// <param name="StuEnrollId">
        /// The StuEnrollId.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        TitleIVSAPResult EligibleForTitleIVProbation(string StuEnrollId)
        {
            TitleIVSAPResult canBeUpdated = new TitleIVSAPResult();
            IQueryable<TitleIVSAPResult> query = ArTitleIvsapCheckResultsRepository
                   .Get(r => r.StuEnrollId.ToString() == StuEnrollId).Select(
                       r => new TitleIVSAPResult()
                       {
                           FASAPCheckResultsId = r.StdRecKey,
                           StudentEnrollmentId = r.StuEnrollId,
                           DatePerformed = r.DatePerformed,
                           TitleIVSAPId = r.TitleIvstatusId,
                           Increment = r.Period,
                           SAPCheckDate = r.ModDate

                       });

            TitleIVSAPResult hasIneligible = query.OrderByDescending(d => d.Increment).OrderByDescending(d => d.DatePerformed).FirstOrDefault();
            if (hasIneligible != null)
            {
                if (hasIneligible.TitleIVSAPId == (int)TitleIVStatuses.Ineligible)
                {

                    canBeUpdated = (from q in query
                                    join a in StuEnrollmentsRepository.Get() on q.StudentEnrollmentId equals
                                        a.StuEnrollId
                                    join d in ProgramVersionRepository.Get() on a.PrgVerId equals d.PrgVerId
                                    join s in ArSapRepository.Get() on d.Fasapid equals s.Sapid
                                    where s.PayOnProbation == true
                                          && q.FASAPCheckResultsId == hasIneligible.FASAPCheckResultsId
                                    select q).FirstOrDefault();

                    return canBeUpdated;
                }
            }

            return null;

        }
        /// <summary>
        /// The get title iv sap notices for printing.
        /// </summary>
        /// <param name="campusId ">
        /// The campus Id.
        /// </param>
        /// <param name="asOfDate"></param>
        /// Date to filter by, takes precedence over any campus setting.
        public async Task<IEnumerable<TitleIVSAPResult>> GetTitleIVNoticesForPrinting(Guid campusId, DateTime? asOfDate = null)
        {
            string titleIVWidgetFilterSetting = this.systemConfigurationAppSettingService.GetAppSettingValueByCampus(ConfigurationAppSettingsKeys.TitleIVWidgetFilter, campusId);
            bool filterSettingExists = titleIVWidgetFilterSetting != null && titleIVWidgetFilterSetting != String.Empty;

            if (!asOfDate.HasValue && filterSettingExists)
                asOfDate = DateTime.Parse(titleIVWidgetFilterSetting);

            List<ArStuEnrollments> query = await StuEnrollmentsRepository
                .Get(se => se.CampusId == campusId &&
                           se.ArFasapchkResults.Any(f => f.StatusId == ActiveStatusId && !f.Printed && (!asOfDate.HasValue || f.DatePerformed >= asOfDate))
                ).Include(x => x.Lead)
                .Include(x => x.ArFasapchkResults).ThenInclude(y => y.Sapdetail)
                .Include(x => x.ArFasapchkResults).ThenInclude(y => y.TitleIvstatus)
                .Include(x => x.ArFasapchkResults).ThenInclude(y => y.Status).ToListAsync();
            string studentIdentifier = systemConfigurationAppSettingService.GetAppSettingValueByCampus(
                   ConfigurationAppSettingsKeys.StudentIdentifier,
                   campusId).Trim().ToLowerInvariant();

            IOrderedEnumerable<TitleIVSAPResult> result = query.Select(
                     se => se.ArFasapchkResults.Where(f => f.StatusId == ActiveStatusId)
                     .Select(y => new
                     {
                         Printed = y.Printed,
                         FASAPCheckResultsId = y.StdRecKey,
                         Increment = y.Period,
                         IsMakingSAP = y.IsMakingSap,
                         DatePerformed = y.DatePerformed,
                         SAPCheckDate = y.ModDate,
                         SAPStatus = y.TitleIvstatus.Description,
                         LeadId = se.LeadId,
                         StudentEnrollmentId = se.StuEnrollId,
                         CheckPointDate = y.CheckPointDate,
                         StudentName =
                                          se.Lead.FirstName + " " + se.Lead.MiddleName + " "
                                          + se.Lead.LastName,
                         SSN = !string.IsNullOrEmpty(se.Lead.Ssn)
                                                 ? " ("
                                          + se.Lead.Ssn.Replace(
                                                     se.Lead.Ssn.Substring(0, 5),
                                                     SsnMask.Ssn) + ")"
                                                 : "",
                         StudentId = !string.IsNullOrEmpty(se.Lead.StudentNumber)
                                                 ? " ("
                                          + se.Lead.StudentNumber + ")"
                                                 : "",
                         EnrollmentId = !string.IsNullOrEmpty(se.EnrollmentId)
                                                 ? " ("
                                          + se.EnrollmentId + ")"
                                                 : ""

                     })

                     .OrderByDescending(res => res.Increment).FirstOrDefault())
                     .Where(t4r => !t4r.Printed).Select(
                         g => new TitleIVSAPResult
                         {
                             FASAPCheckResultsId = g.FASAPCheckResultsId,
                             Increment = g.Increment,
                             IsMakingSAP = g.IsMakingSAP,
                             DatePerformed = g.DatePerformed,
                             SAPCheckDate = g.SAPCheckDate,
                             SAPStatus = g.SAPStatus,
                             LeadId = g.LeadId,
                             StudentEnrollmentId = g.StudentEnrollmentId,
                             CheckPointDate = g.CheckPointDate,
                             StudentName = studentIdentifier.Equals("ssn", StringComparison.InvariantCultureIgnoreCase) ? g.StudentName + g.SSN : studentIdentifier.Equals("studentid", StringComparison.InvariantCultureIgnoreCase) ? g.StudentName + g.StudentId : g.StudentName + g.EnrollmentId
                         }).OrderBy(r => r.StudentName);

            return result;

        }

        /// <summary>
        /// The update print flag.
        /// </summary>
        /// <param name="enrollments">
        /// The enrollments.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task UpdatePrintFlag(List<Guid> enrollments)
        {
            List<ArFasapchkResults> resultsToUpdate = this.ArTitleIvsapCheckResultsRepository
                .Get(r => enrollments.Contains(r.StuEnrollId))
                .OrderByDescending(r => r.DatePerformed)
                .ThenByDescending(r => r.Period)
                .GroupBy(r => r.StuEnrollId)
                .Select(r => r.FirstOrDefault())
                .ToList();

            foreach (ArFasapchkResults r in resultsToUpdate)
            {
                r.Printed = true;
                this.context.GetRepositoryForEntity<ArFasapchkResults>().Update(r);
            }

            this.context.SaveChanges();

        }
    }
}
