﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentGroupsService.cs" company="FAME Inc.">
//   FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentGroupsService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{

    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    /// <summary>
    /// The student groups service
    /// </summary>
    public class StudentGroupsService : IStudentGroupsService
    {

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The holiday Service.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentGroupsService"/> class.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="studentGroupsService">
        /// The student groups service.

        public StudentGroupsService(
            IAdvantageDbContext context,
            IStatusesService statusService,
            IMapper mapper
           )
        {
            this.context = context;
            this.statusService = statusService;
            this.mapper = mapper;
        }

        /// <summary>
        /// The student groups repository.
        /// </summary>
        private IRepository<AdLeadGroups> StudentGroupsRepository =>
            this.context.GetRepositoryForEntity<AdLeadGroups>();


        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.context.GetSimpleCacheConnectionString();

        /// <summary>
        /// The active status id.
        /// </summary>
        private Guid ActiveStatusId => Task.Run(() => this.statusService.GetActiveStatusId()).Result;


        /// <summary>
        /// The get student groups by campus.
        /// </summary>
        /// <param name="campusId">
        /// The campus id and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns an IEnumerable of "IListItem{TText,TValue}"  where TText is of type string and TValue is of type Guid.
        /// </returns>
        public async Task<IEnumerable<IListItem<string, Guid>>> GetStudentGroupsByCampus(Guid campusId)
        {
            return await Task.Run(
                () =>
                {
                    return SimpleCache.GetOrAddExisting(
                        $"{this.CacheKeyFirstSegment}:StudentGroups:{campusId}",
                        () => this.mapper.Map<IEnumerable<IListItem<string, Guid>>>(
                            this.StudentGroupsRepository.Get(pv => pv.CampGrp.SyCmpGrpCmps.Any(cmpGrp => cmpGrp.CampusId == campusId) && pv.StatusId == this.ActiveStatusId)).OrderBy(x => x.Text),
                        CacheExpiration.VERY_LONG);
                });
        }
    }
}
