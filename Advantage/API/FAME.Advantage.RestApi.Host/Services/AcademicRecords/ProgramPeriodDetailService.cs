﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramPeriodDetailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the program period detail service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using Castle.Core.Internal;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Course;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramPeriodDetail;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using Microsoft.ApplicationInsights;
    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    /// <summary>
    /// The program period detail service.
    /// </summary>
    public class ProgramPeriodDetailService : IProgramPeriodDetailService
    {  
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The program version service.
        /// </summary>
        private readonly IProgramVersionsService programVersionsService;

        /// <summary>
        /// The program schedule detail service.
        /// </summary>
        private readonly IProgramScheduleDetailService programScheduleDetailService;

        /// <summary>
        /// The class section meeting service.
        /// </summary>
        private readonly IClassSectionMeetingService classSectionMeetingService;

        /// <summary>
        /// The student termination service.
        /// </summary>
        private readonly IStudentTerminationService studentTerminationService;

        /// <summary>
        /// The enrollment service.
        /// </summary>
        private readonly IEnrollmentService enrollmentService;

        /// <summary>
        /// The Academic Calendar Service.
        /// </summary>
        private readonly IAcademicCalendarService academicCalendarService;

        /// <summary>
        /// The results service.
        /// </summary>
        private readonly IResultsService resultsService; 

        /// <summary>
        /// The term service.
        /// </summary>
        private readonly ITermService termService; 

        /// <summary>
        /// The credit summary service.
        /// </summary>
        private readonly ICreditSummaryService creditSummaryService;

        /// <summary>
        /// The class section service.
        /// </summary>
        private readonly IClassSectionService classSectionService;

        /// <summary>
        /// The calculation period type service.
        /// </summary>
        private readonly ICalculationPeriodTypeService calculationPeriodTypeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramPeriodDetailService"/> class.
        /// </summary>
        /// <param name="programVersionsService">
        /// The program versions service.
        /// </param>
        /// <param name="programScheduleDetailService">
        /// The program schedule detail service.
        /// </param>
        /// <param name="studentTerminationService">
        /// The student termination service.
        /// </param>
        /// <param name="enrollmentService">
        /// The enrollment service.
        /// </param>
        /// <param name="academicCalendarService">
        /// The academic calendar service.
        /// </param>
        /// <param name="termService">
        /// The term service.
        /// </param>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="classSectionMeetingService">
        /// The class section meeting service.
        /// </param>
        /// <param name="resultsService">
        /// The results service.
        /// </param>
        /// <param name="creditSummaryService">
        /// The credit Summary Service.
        /// </param>
        /// <param name="classSectionService">
        /// The class Section Service.
        /// </param>
        /// <param name="calculationPeriodTypeService">
        /// The calculation Period Type Service.
        /// </param>
        public ProgramPeriodDetailService(
            IProgramVersionsService programVersionsService,
            IProgramScheduleDetailService programScheduleDetailService,
            IStudentTerminationService studentTerminationService,
            IEnrollmentService enrollmentService,
            IAcademicCalendarService academicCalendarService,
            ITermService termService, 
            IAdvantageDbContext context,
            IClassSectionMeetingService classSectionMeetingService,
            IResultsService resultsService,
            ICreditSummaryService creditSummaryService,
            IClassSectionService classSectionService,
            ICalculationPeriodTypeService calculationPeriodTypeService)
        {
            this.programVersionsService = programVersionsService;
            this.programScheduleDetailService = programScheduleDetailService;
            this.studentTerminationService = studentTerminationService;
            this.enrollmentService = enrollmentService;
            this.academicCalendarService = academicCalendarService;
            this.termService = termService;
            this.context = context;
            this.classSectionMeetingService = classSectionMeetingService;
            this.resultsService = resultsService;
            this.creditSummaryService = creditSummaryService;
            this.classSectionService = classSectionService;
            this.calculationPeriodTypeService = calculationPeriodTypeService;
        } 

        /// <summary>
        /// The programVersion  repository.
        /// </summary>
        private IRepository<ArPrgVersions> ProgramVersionRepository =>
            this.context.GetRepositoryForEntity<ArPrgVersions>(); 

        /// <summary>
        /// The Grade System Details repository.
        /// </summary>
        private IRepository<ArGradeSystemDetails> GradeSystemRepository =>
            this.context.GetRepositoryForEntity<ArGradeSystemDetails>();

        /// <summary>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// GetNonTermProgramDaysByEnrollmentId method gives the total and completed days for an enrollmentId.
        /// </summary>
        /// <remarks>
        /// The get total days for non term not self paced program method returns the Total Days in the period 
        /// when a student withdraws from a non term not self paced program.
        /// Based on the enrollment id it searches all the enrolled program details and returns the total days for the period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is of type DateTime is required when the total days is 0.
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number of days required to complete failed courses.
        /// </param>
        /// <returns>
        /// Returns total days for the period.
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        public async Task<ProgramPeriodTotalDays> GetTotalDaysForNonTermNotSelfPacedProgram(Guid enrollmentId, DateTime? endDate, int numberOfDaysRequired)
        {
            return await Task.Run(
                       async () =>
                       {
                           var programPeriodTotalDays = new ProgramPeriodTotalDays();
                           bool paymentPeriodType = false;

                           try
                           {
                               var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                               if (!isClockHour)
                               {
                                   var programVersion =
                                       await this.programVersionsService.GetProgramVersionDetailByEnrollmentId(
                                           enrollmentId);

                                   if (programVersion != null && programVersion.AcademicCalendarId
                                       == Convert.ToInt32(SyAcademicCalendarProgram.NonTerm))
                                   {
                                       var campusProgramVersionDetails =
                                           await this.programVersionsService.GetCampusProgramVersionDetails(
                                               enrollmentId);
                                       if (campusProgramVersionDetails != null)
                                       {
                                           if (campusProgramVersionDetails.CalculationPeriodTypeId != null)
                                           {
                                               paymentPeriodType =
                                                   await this.calculationPeriodTypeService.IsPaymentPeriodType(
                                                       campusProgramVersionDetails.CalculationPeriodTypeId.Value);
                                           }

                                           if (campusProgramVersionDetails.IsSelfPaced == null)
                                           {
                                               programPeriodTotalDays.ResultStatus = ApiMsgs.NON_SELFPACED_PROGRAM;
                                               return programPeriodTotalDays;
                                           }

                                           if (paymentPeriodType)
                                           {
                                               if (!campusProgramVersionDetails.IsSelfPaced.Value)
                                               {
                                                   var creditHoursWithdrawalPaymentPeriod =
                                                       await this.programVersionsService
                                                           .GetCreditHoursWithdrawalPaymentPeriod(enrollmentId);
                                                   if (creditHoursWithdrawalPaymentPeriod != null)
                                                   {
                                                       var creditHourPaymentPeriodDetails =
                                                           new CreditHourWithdrawalEndDateDetails();
                                                       if (endDate != null)
                                                       {
                                                           creditHourPaymentPeriodDetails.EndDateOfWithdrawalPeriod =
                                                               endDate;
                                                       }
                                                       else
                                                       {
                                                           creditHourPaymentPeriodDetails =
                                                               await this.programScheduleDetailService
                                                                   .GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(
                                                                       enrollmentId);
                                                       }

                                                       if (creditHourPaymentPeriodDetails != null)
                                                       {
                                                           if (creditHoursWithdrawalPaymentPeriod
                                                                   .StartDateOfWithdrawalPeriod.HasValue
                                                               && creditHourPaymentPeriodDetails
                                                                   .EndDateOfWithdrawalPeriod.HasValue)
                                                           {
                                                               var totalWithdrawalDays = (creditHourPaymentPeriodDetails.EndDateOfWithdrawalPeriod.Value.Date - creditHoursWithdrawalPaymentPeriod.StartDateOfWithdrawalPeriod.Value.Date).Days + 1;

                                                               if (totalWithdrawalDays > 0)
                                                               {
                                                                   var studentLoAs =
                                                                       this.enrollmentService.GetStudentLOAs(
                                                                           enrollmentId);

                                                                   var scheduledLoaCount =
                                                                       this.programScheduleDetailService
                                                                           .GetLoAsForDateRange(
                                                                               studentLoAs,
                                                                               creditHourPaymentPeriodDetails.EndDateOfWithdrawalPeriod.Value,
                                                                               creditHoursWithdrawalPaymentPeriod.StartDateOfWithdrawalPeriod.Value);

                                                                   var failedCourseDetail =
                                                                       numberOfDaysRequired == 0 ? 
                                                                       await this.GetDaysRequiredToCompleteFailedCourse(
                                                                           enrollmentId,
                                                                           creditHourPaymentPeriodDetails.EndDateOfWithdrawalPeriod ?? DateTime.MinValue)
                                                                       : new FailedCourseDetails { DaysRequiredToCompleteFailedCourse = numberOfDaysRequired };
                                                                   var scheduledBreaksDetails =
                                                                       await this.programScheduleDetailService
                                                                           .GetCreditHourScheduledBreak(
                                                                               enrollmentId,
                                                                               creditHourPaymentPeriodDetails.EndDateOfWithdrawalPeriod.Value, 
                                                                               creditHoursWithdrawalPaymentPeriod.StartDateOfWithdrawalPeriod.Value);
                                                                   int scheduledBreaks = scheduledBreaksDetails
                                                                       .TotalScheduledBreakDays;

                                                                   programPeriodTotalDays.TotalDays =
                                                                       Convert.ToInt32(totalWithdrawalDays)
                                                                       + Convert.ToInt32(
                                                                           failedCourseDetail
                                                                               .DaysRequiredToCompleteFailedCourse)
                                                                       - Convert.ToInt32(scheduledLoaCount)
                                                                       - scheduledBreaks;
                                                                   programPeriodTotalDays.ResultStatus =
                                                                       ApiMsgs.TOTAL_DAYS_FOR_NONTERM_NOTSELFPACED_PGM;
                                                                   return programPeriodTotalDays;
                                                               }
                                                           }
                                                           else
                                                           {
                                                               programPeriodTotalDays.ResultStatus =
                                                                   creditHourPaymentPeriodDetails.ResultStatus;
                                                               return programPeriodTotalDays;
                                                           }
                                                       }
                                                   }
                                               }
                                               else
                                               {
                                                   programPeriodTotalDays.ResultStatus = ApiMsgs.SELFPACED_PROGRAM;
                                               }
                                           }
                                           else
                                           {
                                               programPeriodTotalDays.ResultStatus = ApiMsgs.PERIOD_FOR_CALCULATION;
                                           }
                                       }
                                   }
                                   else
                                   {
                                       programPeriodTotalDays.ResultStatus = ApiMsgs.NOT_NONTERM_PROGRAM;
                                   }
                               }
                               else
                               {
                                   programPeriodTotalDays.ResultStatus = ApiMsgs.NOT_CREDIT_HOUR_PROGRAM;
                               }

                               return programPeriodTotalDays;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               programPeriodTotalDays.ResultStatus = ApiMsgs.FAILED_GETTING_RECORD;
                               return programPeriodTotalDays;
                           }
                       });
        }

        /// <summary>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// </summary>
        /// <remarks>
        /// The GetDetailsForNonStandardTermSubstantiallyEqualLength action method returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <returns>
        /// Returns the completed days and total days in a payment period for Non-Standard term programs with terms of substantially equal in length based on enrollmentId.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardTermProgramDaysByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail();
                           var isProgramVersionSubstantiallyEqual = await this.programVersionsService.IsProgramVersionSubstantiallyEqual(enrollmentId);

                           if (isProgramVersionSubstantiallyEqual)
                           {
                               creditHourPaymentPeriodDaysDetail =
                                   await this.GetNonStandardProgramDaysCalculation(enrollmentId);
                           }
                           else
                           {
                               creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.PROGRAM_TYPE_NOT_FOUND;
                           }

                           return creditHourPaymentPeriodDaysDetail;
                       });
        }

        /// <summary>
        /// The get non standard program days for not substantially equal method returns the Total Days and completed days in the period by an enrollment is. 
        /// GetNonStandardProgramDaysForNotSubstantiallyEqual method gives the total and completed days for an enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// EnrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date is an optional parameter and will considered as end date to calculate Scheduled Breaks and Approved LoA's in case of direct loan payment period.
        /// </param>
        /// <returns>
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardProgramDaysForNotSubstantiallyEqual(Guid enrollmentId, DateTime? endDate)
        {
            return await Task.Run(
                async () =>
                {
                    var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail();
                    var programVersionDetails = await this.programVersionsService.GetProgramVersionDetailByEnrollmentId(enrollmentId);
                    if (programVersionDetails == null)
                    {
                        creditHourPaymentPeriodDaysDetail.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                        return creditHourPaymentPeriodDaysDetail;
                    }

                    if (programVersionDetails.AcademicCalendarId != Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm))
                    {
                        creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_NONSTANDARD_PROGRAM;
                        return creditHourPaymentPeriodDaysDetail;
                    }

                    var isProgramVersionSubstantiallyEqual = await this.programVersionsService.IsProgramVersionSubstantiallyEqual(enrollmentId);

                    if (isProgramVersionSubstantiallyEqual)
                    {
                        creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.PROGRAM_NOT_SUBSTANTIALLY_EQUAL;
                        return creditHourPaymentPeriodDaysDetail;
                    }

                    var paymentPeriodDetails = await this.programVersionsService.GetPaymentPeriodForNotSubstantiallyEqualInLength(enrollmentId);
                    if (paymentPeriodDetails == null)
                    {
                        creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.FAILED_GETTING_RECORD;
                        return creditHourPaymentPeriodDaysDetail;
                    }

                    if (paymentPeriodDetails.TermOfWithdrawal == ApiMsgs.DIRECT_LOAN_PAYMENT_PERIOD)
                    {
                        var creditHourWithdrawalEndDate = endDate != null
                                                              ? new CreditHourWithdrawalEndDateDetails
                                                                    {
                                                                        EndDateOfWithdrawalPeriod
                                                                            = endDate
                                                                    }
                                                              : await this.programScheduleDetailService
                                                                    .GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(
                                                                        enrollmentId);

                        if (creditHourWithdrawalEndDate.EndDateOfWithdrawalPeriod == null)
                        {
                            creditHourPaymentPeriodDaysDetail.ResultStatus = creditHourWithdrawalEndDate.ResultStatus;
                            return creditHourPaymentPeriodDaysDetail;
                        }

                        if (creditHourWithdrawalEndDate.ResultStatus == ApiMsgs.FAILED_CALCULATING_ENDDATE)
                        {
                            creditHourPaymentPeriodDaysDetail.ResultStatus = creditHourWithdrawalEndDate.ResultStatus;
                            return creditHourPaymentPeriodDaysDetail;
                        }

                        creditHourPaymentPeriodDaysDetail = await this.CalculateNonStandardProgramDaysForNotSubstantiallyEqual(enrollmentId, paymentPeriodDetails.WithdrawalPaymentPeriodStartDate, creditHourWithdrawalEndDate.EndDateOfWithdrawalPeriod.Value);
                    }
                    else
                    {
                        creditHourPaymentPeriodDaysDetail = await this.CalculateNonStandardProgramDaysForNotSubstantiallyEqual(enrollmentId, paymentPeriodDetails.TermStartDate, paymentPeriodDetails.TermEndDate);
                    }

                    return creditHourPaymentPeriodDaysDetail;
                });
        }

        /// <summary>
        /// The GetNonStandardProgramDaysDetail returns the completed days and total days in a payment period for Non-Standard term programs.
        /// </summary>
        /// <remarks>
        /// The GetNonStandardProgramDaysDetail action method returns the completed days and total days in a payment period for Non-Standard term programs.
        /// It calculate the total and completed days for a specific enrollmentId.
        /// </remarks>
        /// <param name="enrollmentId">
        /// enrollmentId is a required field and it is of type guid.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        /// <returns>
        /// Returns the success or failure message determine whether the record has been fetched based on enrollmentId.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardProgramDaysByEnrollmentId(Guid enrollmentId, DateTime? endDate)
        {
            return await Task.Run(
                       async () =>
                       {
                           bool paymentPeriodType = false;
                           var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail();
                           var programVersionDetails = await this.programVersionsService.GetProgramVersionDetailByEnrollmentId(enrollmentId);
                           var paymentPeriodEndDateDetails = new CreditHourWithdrawalEndDateDetails();

                           if (programVersionDetails != null)
                           {
                               if (programVersionDetails.AcademicCalendarId
                                   != Convert.ToInt32(SyAcademicCalendarProgram.NonStandardTerm))
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_NONSTANDARD_PROGRAM;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               var campusProgramVersionDetails =
                                   await this.programVersionsService.GetCampusProgramVersionDetails(enrollmentId);
                               if (campusProgramVersionDetails != null)
                               {
                                   if (campusProgramVersionDetails.CalculationPeriodTypeId != null)
                                   {
                                       paymentPeriodType =
                                           await this.calculationPeriodTypeService.IsPaymentPeriodType(
                                               campusProgramVersionDetails.CalculationPeriodTypeId.Value);
                                   }

                                   if (!paymentPeriodType)
                                   {
                                       var currentPeriodDetailsWithCreditsEarned =
                                           await this.programVersionsService
                                               .GetNonStandardWithdrawalPeriodOfEnrollment(enrollmentId);

                                       if (currentPeriodDetailsWithCreditsEarned != null 
                                           && currentPeriodDetailsWithCreditsEarned.ResultStatus != ApiMsgs.PAYMENT_PERIOD_CALCULATION_SUCCESS 
                                           && currentPeriodDetailsWithCreditsEarned.ResultStatus != ApiMsgs.WITHDRAWAL_PERIOD_OF_ENROLLMENT_CALCULATED_SUCCESSFUL)
                                       {
                                           creditHourPaymentPeriodDaysDetail.ResultStatus = currentPeriodDetailsWithCreditsEarned.ResultStatus;
                                           return creditHourPaymentPeriodDaysDetail;
                                       }

                                       if (currentPeriodDetailsWithCreditsEarned != null && currentPeriodDetailsWithCreditsEarned.ResultStatus != ApiMsgs.NON_STANDARD_TERM_SUBSTANTIALLY_EQUAL_LENGTH_NON_TERM_NOT_SELFPACED_TERM)
                                       {
                                           var currentPeriodDetails = new CurrentPeriodDetails();
                                           var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
                                           var dateOfWithdrawal = terminationDetails.LastDateAttended;

                                           currentPeriodDetails.StartDateOfWithdrawalPeriod =
                                               currentPeriodDetailsWithCreditsEarned.StartDateOfWithdrawalPeriod;

                                           if (endDate != null)
                                           {
                                               paymentPeriodEndDateDetails.EndDateOfWithdrawalPeriod = endDate.Value;
                                           }
                                           else
                                           {
                                               paymentPeriodEndDateDetails =
                                                   await this.programScheduleDetailService
                                                       .GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(
                                                           enrollmentId);
                                           }

                                           var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId);

                                           double approveLoaCount;
                                           if (dateOfWithdrawal != null
                                               && currentPeriodDetails.StartDateOfWithdrawalPeriod != null)
                                           {
                                               var starDateToWithdrawalDateDifference =
                                                   (dateOfWithdrawal.Value - currentPeriodDetails
                                                        .StartDateOfWithdrawalPeriod.Value).TotalDays + 1;

                                               approveLoaCount =
                                                   this.programScheduleDetailService.GetLoAsForDateRange(
                                                       studentLoAs,
                                                       dateOfWithdrawal.Value,
                                                       currentPeriodDetails.StartDateOfWithdrawalPeriod.Value);

                                               var scheduledBreakForCompletedDays =
                                                   await this.programScheduleDetailService
                                                       .GetCreditHourScheduledBreak(enrollmentId, dateOfWithdrawal.Value, currentPeriodDetails.StartDateOfWithdrawalPeriod.Value);
                                               creditHourPaymentPeriodDaysDetail.CompletedDays = Convert.ToInt32(starDateToWithdrawalDateDifference - approveLoaCount - scheduledBreakForCompletedDays.TotalScheduledBreakDays);
                                           }

                                           if (paymentPeriodEndDateDetails.EndDateOfWithdrawalPeriod != null
                                               && currentPeriodDetails.StartDateOfWithdrawalPeriod != null)
                                           {
                                               approveLoaCount =
                                                   this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, paymentPeriodEndDateDetails.EndDateOfWithdrawalPeriod.Value, currentPeriodDetails.StartDateOfWithdrawalPeriod.Value);

                                               var starDateToEndDateDifference =
                                                   (paymentPeriodEndDateDetails.EndDateOfWithdrawalPeriod.Value - currentPeriodDetails.StartDateOfWithdrawalPeriod.Value).TotalDays + 1;

                                               var scheduledBreakForTotalDays =
                                                   await this.programScheduleDetailService
                                                       .GetCreditHourScheduledBreak(enrollmentId, paymentPeriodEndDateDetails.EndDateOfWithdrawalPeriod.Value, currentPeriodDetails.StartDateOfWithdrawalPeriod.Value);
                                               creditHourPaymentPeriodDaysDetail.TotalDays = Convert.ToInt32(starDateToEndDateDifference - approveLoaCount - scheduledBreakForTotalDays.TotalScheduledBreakDays);
                                               creditHourPaymentPeriodDaysDetail.ResultStatus =
                                                   ApiMsgs.COMPLETED_DAYS_CALCULATED_SUCCESSFUL;
                                           }
                                           else
                                           {
                                               creditHourPaymentPeriodDaysDetail.ResultStatus = paymentPeriodEndDateDetails.ResultStatus;
                                           }
                                       }
                                       else
                                       {
                                           creditHourPaymentPeriodDaysDetail.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                                       }
                                   }
                                   else
                                   {
                                       creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.PERIOD_USED_FOR_CALCULATION;
                                   }
                               }
                               else
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_PERIOD_USED_FOR_CALCULATION;
                               }
                           }
                           else
                           {
                               creditHourPaymentPeriodDaysDetail.ResultStatus = $"{ApiMsgs.NOT_FOUND} for Enrollment Id: {enrollmentId}.";
                               return creditHourPaymentPeriodDaysDetail;
                           }

                           return creditHourPaymentPeriodDaysDetail;
                       });
        }

        /// <summary>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get credit hour completed and total days for standard term programs action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDaysDetail> GetCompletedAndTotalDaysForStandardTermPrograms(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                       {
                           try
                           {
                               var paymentPeriodDays = new CreditHourPaymentPeriodDaysDetail();
                               double completedTermDays = 0;
                               double totalTermDays = 0;
                               double numberOfLoaDaysTillWithdrawalDate = 0;
                               double numberOfLoaDaysForTerm = 0;
                               int numberOfScheduledBreaksTillWithdrawalDate = 0;
                               int numberOfScheduledBreaksForTerm = 0;
                               var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                               if (!isClockHour)
                               {
                                   var programVersion = await
                                       this.programVersionsService.GetProgramVersionDetailByEnrollmentId(enrollmentId);

                                   var withdrawalDate = programVersion.WithdrawalDate;
                                   if (withdrawalDate != DateTime.MinValue)
                                   {
                                       if (programVersion.AcademicCalendarId.HasValue && (programVersion.AcademicCalendarId.Value
                                                                                               == Convert.ToInt32(SyAcademicCalendarProgram.Quarter)
                                                                                               || programVersion.AcademicCalendarId.Value
                                                                                               == Convert.ToInt32(SyAcademicCalendarProgram.Semester)
                                                                                               || programVersion.AcademicCalendarId.Value
                                                                                               == Convert.ToInt32(SyAcademicCalendarProgram.Trimester)))
                                       {
                                           if (programVersion.ProgramId != null)
                                           {
                                               var terms = this.termService.GetTerms(enrollmentId);
                                               var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId);

                                               foreach (var term in terms)
                                               {
                                                   if (term.StartDate.HasValue && term.EndDate.HasValue
                                                       && term.EndDate.Value >= term.StartDate.Value
                                                       && withdrawalDate >= term.StartDate.Value
                                                       && withdrawalDate <= term.EndDate.Value)
                                                   {
                                                       completedTermDays = Math.Round((withdrawalDate.Value - term.StartDate.Value).TotalDays, 2);
                                                       if (completedTermDays > 0)
                                                       {
                                                           numberOfLoaDaysTillWithdrawalDate = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, withdrawalDate.Value, term.StartDate.Value);
                                                           var scheduledBreaks =
                                                               await this.programScheduleDetailService
                                                                   .GetCreditHourScheduledBreak(
                                                                       enrollmentId,
                                                                       (DateTime)withdrawalDate,
                                                                       term.StartDate ?? DateTime.MinValue);
                                                           numberOfScheduledBreaksTillWithdrawalDate =
                                                               scheduledBreaks.TotalScheduledBreakDays;
                                                       }

                                                       totalTermDays = (term.EndDate.Value - term.StartDate.Value).TotalDays + 1;
                                                       if (totalTermDays > 0)
                                                       {
                                                           numberOfLoaDaysForTerm = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, term.EndDate.Value, term.StartDate.Value);
                                                           var scheduledBreaks =
                                                               await this.programScheduleDetailService
                                                                   .GetCreditHourScheduledBreak(enrollmentId, term.EndDate ?? DateTime.MinValue, term.StartDate ?? DateTime.MinValue);
                                                           numberOfScheduledBreaksForTerm = scheduledBreaks.TotalScheduledBreakDays;
                                                       }

                                                       break;
                                                   }
                                               }

                                               paymentPeriodDays.CompletedDays =
                                                   completedTermDays - numberOfLoaDaysTillWithdrawalDate - numberOfScheduledBreaksTillWithdrawalDate;
                                               paymentPeriodDays.TotalDays =
                                                   totalTermDays - numberOfLoaDaysForTerm - numberOfScheduledBreaksForTerm;
                                               paymentPeriodDays.ResultStatus = ApiMsgs.COMPLETED_TOTAL_DAYS_SUCCESSFULL;
                                           }
                                       }
                                       else
                                       {
                                           paymentPeriodDays.ResultStatus = ApiMsgs.NOT_STANDARD_PROGRAM;
                                       }
                                   }
                               }
                               else
                               {
                                   paymentPeriodDays.ResultStatus = ApiMsgs.NOT_CREDIT_HOUR_PROGRAM;
                               }

                               return paymentPeriodDays;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               return new CreditHourPaymentPeriodDaysDetail { ResultStatus = ApiMsgs.FAILED_GETTING_RECORD };
                           }
                       });
        }

        /// <inheritdoc />
        /// <summary>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// </summary>
        /// <remarks>
        /// The get non term completed and total days for period of enrollment action returns the completed and total days for the specified enrollment id.
        /// It requires enrollmentId which is of type Guid.
        /// Based on the Enrollment Id, it searches all the enrolled program details and returns the completed and total days for the selected payment period. 
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is required field and is a type of Guid.
        /// </param>
        /// <param name="periodEndDate">
        /// The period End Date
        /// </param>
        /// <param name="numberOfDaysRequired">
        /// The number of days required required to complete failed courses
        /// </param>
        /// <returns>
        /// Returns completed and total days for the selected payment period.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDaysDetail> GetNonTermCompletedAndTotalDaysForPeriodOfEnrollment(Guid enrollmentId, DateTime periodEndDate, int numberOfDaysRequired)
        {
            return await Task.Run(
                       async () =>
                       {
                           try
                           { 
                               var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail(); 
                               var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId); 
                               if (isClockHour)
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_CREDIT_HOUR_PROGRAM;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               var enrollmentDetail = await this.programVersionsService.GetEnrollmentDetailsById(enrollmentId); 
                               if (enrollmentDetail.IsSelfPaced)
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.SELF_PACED_PROGRAM;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               if (enrollmentDetail.IsPaymentPeriod)
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.PERIOD_USED_NOT_POE;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               var withdrawalDate = enrollmentDetail.EndDateTime;
                               if (withdrawalDate == DateTime.MinValue)
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.WITHDRAWAL_DATE_NOT_FOUND;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               if (enrollmentDetail.AcademicCalendarId == null)
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.ACADEMIC_CALENDAR_NOT_FOUND_FOR_ENROLLMENT;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               if (enrollmentDetail.AcademicCalendarId != Convert.ToInt32(SyAcademicCalendarProgram.NonTerm))
                               {
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_NON_TERM_PROGRAM;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               var withdrawalPeriodDetail = await this.programVersionsService.GetNonStandardWithdrawalPeriodOfEnrollment(enrollmentId);
                               var startDate = withdrawalPeriodDetail?.StartDateOfWithdrawalPeriod ?? DateTime.MinValue; 
                               if (startDate == DateTime.MinValue)
                               { 
                                   creditHourPaymentPeriodDaysDetail.ResultStatus = enrollmentDetail.IsPaymentPeriod ? ApiMsgs.PAYMENT_PERIOD_START_END_DATE_INCORRECT : ApiMsgs.PERIOD_OF_ENROLLMENT_START_END_DATE_INCORRECT;
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               var withdrawalPeriodStatus = string.Empty;
                               if (periodEndDate == DateTime.MinValue)
                               {
                                   var withdrawalPeriodEndDateDetail = await this.programScheduleDetailService.GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(enrollmentId);
                                   periodEndDate = withdrawalPeriodEndDateDetail?.EndDateOfWithdrawalPeriod ?? DateTime.MinValue;
                                   withdrawalPeriodStatus = withdrawalPeriodEndDateDetail.ResultStatus;
                               }

                               if (periodEndDate == DateTime.MinValue)
                               {
                                   if(withdrawalPeriodStatus.Contains("Student's schedule is not set to"))
                                   {
                                       creditHourPaymentPeriodDaysDetail.ResultStatus = withdrawalPeriodStatus;
                                   }
                                   else
                                   {
                                      creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.REQUIRED_ENDDATE_MESSAGE;
                                   }
                                   return creditHourPaymentPeriodDaysDetail;
                               }

                               // take the earlier date between withdrawalDate and periodEndDate
                               withdrawalDate = periodEndDate < withdrawalDate ? periodEndDate : withdrawalDate;

                               var completedDays = (withdrawalDate.Date - startDate.Date).Days + 1;  
                               var totalDays = (periodEndDate != DateTime.MinValue && startDate <= periodEndDate) ? (periodEndDate.Date - startDate.Date).Days + 1 : 0.0;  
                               var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId); 

                               var numberOfLoAsTillWithdrawalDate = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, withdrawalDate, startDate);
                               var numberOfLoAsTillEndDate = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, periodEndDate, startDate);
                               var scheduleBreaksTillWithdrawalDate = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, withdrawalDate, startDate);
                               var scheduleBreaksTillEndDate = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, periodEndDate, startDate); 
                               var failedCoursesDetail = numberOfDaysRequired == 0 ? await this.GetDaysRequiredToCompleteFailedCourse(enrollmentId, periodEndDate) : new FailedCourseDetails { DaysRequiredToCompleteFailedCourse  = numberOfDaysRequired };

                               creditHourPaymentPeriodDaysDetail.CompletedDays = completedDays - numberOfLoAsTillWithdrawalDate - scheduleBreaksTillWithdrawalDate.TotalScheduledBreakDays;
                               creditHourPaymentPeriodDaysDetail.TotalDays = totalDays + failedCoursesDetail.DaysRequiredToCompleteFailedCourse - numberOfLoAsTillEndDate - scheduleBreaksTillEndDate.TotalScheduledBreakDays;

                               creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.COMPLETED_TOTAL_DAYS_SUCCESSFULL; 
                               return creditHourPaymentPeriodDaysDetail;
                           }
                           catch (Exception e)
                           {
                               e.TrackException();
                               return new CreditHourPaymentPeriodDaysDetail { ResultStatus = ApiMsgs.FAILED_GETTING_RECORD };
                           }
                       });
        }

        /// <summary>
        /// The GetDaysRequiredToCompleteFailedCourse returns the number of days required to complete failed courses for given enrollment id.
        /// </summary>
        /// <remarks>
        /// This action method returns the number of days required to complete failed courses for a given enrollment id
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <param name="periodEndDate">
        /// The period End Date is the end date of the period and is of type DateTime.
        /// </param>
        /// <returns>
        /// Returns the details of number of days required to complete failed courses.
        /// </returns>
        public async Task<FailedCourseDetails> GetDaysRequiredToCompleteFailedCourse(Guid enrollmentId, DateTime periodEndDate)
        { 
            return await Task.Run(
                       async () =>
                           {
                               var failedCourseDetail = new FailedCourseDetails(); 
                               var courses = await this.GetAllCourseByEnrollment(enrollmentId);
                               courses = courses.ToList();
                               if (!courses.Any())
                               {
                                   failedCourseDetail.ResultStatus = ApiMsgs.COURSES_NOT_FOUND;
                                   return failedCourseDetail;
                               }

                               var failedGrades = this.GetAllFailGrades(); 
                               var creditSummaryList = await this.GetAllCreditSummaryDetailByEnrollmentId(enrollmentId); 
                               var failedCourse = creditSummaryList.ToList().Where(x => failedGrades.Contains(x.FinalGrade)).ToList();
                               if (!failedCourse.Any())
                               {
                                   failedCourseDetail.ResultStatus = ApiMsgs.NO_FAILED_COURSE_FOUND;
                                   return failedCourseDetail;
                               }  

                               var failedCourseIds = failedCourse.Select(x => x.ReqId).ToList();
                               var creditsRequiredMakeupFailedCourse = courses.Where(x => failedCourseIds.Contains(x.ReqId)).Sum(x => x.FinAidCredits); 

                               var enrollmentDetail = await this.programVersionsService.GetEnrollmentDetailsById(enrollmentId);

                               var withdrawalPeriod = enrollmentDetail.IsPaymentPeriod ?
                                                         await this.programVersionsService.GetCreditHoursWithdrawalPaymentPeriod(enrollmentId)
                                                         : await this.programVersionsService.GetNonStandardWithdrawalPeriodOfEnrollment(enrollmentId);

                               if (periodEndDate == DateTime.MinValue)
                               {
                                   periodEndDate = await this.GetPeriodEndDateByEnrollmentId(enrollmentId);
                               }

                               if (periodEndDate == DateTime.MinValue)
                               {
                                   failedCourseDetail.ResultStatus = ApiMsgs.REQUIRED_ENDDATE_MESSAGE;
                                   return failedCourseDetail;
                               }

                               var resultDetails = await this.resultsService.GetResultDetailsByEnrollmentId(enrollmentId);
                               resultDetails = resultDetails.ToList();
                               var classSectionIds = resultDetails.Select(x => x.ClassSectionId ?? Guid.Empty).ToList();

                               var classSections = resultDetails.Where(x => x.DateCompleted >= withdrawalPeriod.StartDateOfWithdrawalPeriod
                                                                            && x.DateCompleted <= periodEndDate).Select(x => x.ClassSectionId.ToString().ToLower()).ToList();
                               var failedCourseNames = string.Join(" ,", failedCourse.Where(x => classSections.Contains(x.ClassSectionId.ToLower())).Select(x => x.ReqDescription));

                               if (classSections.Count == 0)
                               {
                                   failedCourseDetail.ResultStatus = failedCourseNames.IsNullOrEmpty() ? ApiMsgs.FAILED_COURSE_NOT_FOUND_MESSAGE : string.Format(ApiMsgs.FAILED_COURSE_DETAIL_MESSAGE, failedCourseNames);
                                   return failedCourseDetail;
                               }

                               if (withdrawalPeriod.WithdrawalPeriod > 0)
                               {
                                   var periodDetails = enrollmentDetail.IsPaymentPeriod ?
                                                           await this.programVersionsService.GetCreditHourPaymentPeriodsByEnrollmentId(enrollmentId)
                                                           : await this.programVersionsService.GetCreditHourPeriodsLengthByEnrollmentId(enrollmentId);
                                   if (periodDetails.CreditHourProgramePeriodDetailsList.Count > 0)
                                   {
                                       var creditRequiredToCompletePeriod = periodDetails
                                           .CreditHourProgramePeriodDetailsList[withdrawalPeriod.WithdrawalPeriod - 1]
                                           .Credits;
                                       if (creditRequiredToCompletePeriod < creditsRequiredMakeupFailedCourse)
                                       {
                                           creditsRequiredMakeupFailedCourse = creditRequiredToCompletePeriod;
                                       }
                                   }
                               } 
                               
                               failedCourseDetail.FailedCourseList = failedCourse.Select(x => x.ReqDescription).OrderBy(x => x).ToList();

                               if (classSectionIds.Any())
                               {
                                   List<ClassSectionDetail> clsSectionsDetails; 
                                   var classSectionMeetingDetailsList = await this.classSectionMeetingService.GetClassSectionMeetingsAfterPeriodEndDate(classSectionIds, periodEndDate);
                                  
                                   if (classSectionMeetingDetailsList.Count > 0)
                                   {
                                       var clsSectionsDetailsList = await this.classSectionService.GetDetailByIds(classSectionMeetingDetailsList.Select(x => x.ClassSectionId).ToList());
                                       clsSectionsDetails = clsSectionsDetailsList.ToList();

                                       clsSectionsDetails.ForEach(
                                           clsSect =>
                                           {
                                               var meetings = classSectionMeetingDetailsList.Where(x => x.ClassSectionId == clsSect.ClassSectionId).ToList();
                                               if (meetings.Any())
                                               {
                                                   clsSect.StartDate = meetings.OrderBy(e => e.StartDate).FirstOrDefault()?.StartDate ?? DateTime.MinValue;
                                                   clsSect.EndDate = meetings.LastOrDefault()?.EndDate ?? DateTime.MinValue;
                                               }
                                           });
                                   }
                                   else
                                   {
                                       var clsSectionsDetailsList = await this.classSectionService.GetDetailByIds(classSectionIds);
                                       clsSectionsDetails = clsSectionsDetailsList.Where(x => x.StartDate > periodEndDate).ToList();
                                   }  

                                   var startDate = clsSectionsDetails.OrderBy(e => e.StartDate).FirstOrDefault()?.StartDate ?? DateTime.MinValue;
                                   clsSectionsDetails = clsSectionsDetails.OrderBy(e => e.EndDate).ToList();
                                   var endDate = DateTime.MinValue;

                                   var totalCredits = 0m;
                                   foreach (var classSection in clsSectionsDetails)
                                   {
                                       var course = courses.FirstOrDefault(c => c.ReqId == classSection.ReqId);
                                       totalCredits += course?.FinAidCredits ?? 0m;
                                       if (endDate < classSection.EndDate)
                                       {
                                           endDate = classSection.EndDate;
                                       }

                                       if (totalCredits >= creditsRequiredMakeupFailedCourse)
                                       {  
                                           break;
                                       }
                                   }

                                   if (totalCredits < creditsRequiredMakeupFailedCourse)
                                   {
                                       failedCourseDetail.ResultStatus = string.Format(ApiMsgs.FAILED_COURSE_DETAIL_MESSAGE, failedCourseNames);
                                       return failedCourseDetail;
                                   }

                                   if (endDate == DateTime.MinValue)
                                   {
                                       endDate = clsSectionsDetails.LastOrDefault()?.EndDate ?? DateTime.MinValue;
                                   }

                                   failedCourseDetail.DaysRequiredToCompleteFailedCourse = (endDate.Date - startDate.Date).Days + 1;
                                   var scheduleBreak = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, endDate, startDate);
                                   failedCourseDetail.DaysRequiredToCompleteFailedCourse = (scheduleBreak.TotalScheduledBreakDays > 0) ?
                                                                                               failedCourseDetail.DaysRequiredToCompleteFailedCourse - scheduleBreak.TotalScheduledBreakDays :
                                                                                               failedCourseDetail.DaysRequiredToCompleteFailedCourse;
                               }

                               failedCourseDetail.ResultStatus = ApiMsgs.CALCULATING_FAILED_COURSE_SUCCESSFUL;
                               return failedCourseDetail;
                          });
        }

        /// <summary>
        /// The get withdrawal period term details returns the term details of the withdrawal period.
        /// </summary>
        /// <remarks>
        /// The get withdrawal period term details returns the term details of the withdrawal period based on the provided enrollment Id.
        /// </remarks>
        /// <param name="enrollmentId">
        /// The enrollment id is the required field and is of type Guid.
        /// </param>
        /// <returns>
        /// Returns the term details of the withdrawal period of the enrolled program.
        /// </returns>
        public async Task<CreditHourPaymentPeriodDetails> GetWithdrawalPeriodTermDetails(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                           {
                               try
                               {
                                   var creditHourPaymentPeriodDetails = new CreditHourPaymentPeriodDetails();
                                   var isClockHour = await this.academicCalendarService.IsClockHour(enrollmentId);
                                   if (!isClockHour)
                                   {
                                       var programVersion =
                                           await this.programVersionsService.GetProgramVersionDetailByEnrollmentId(
                                               enrollmentId);

                                       if (programVersion?.ProgramId != null
                                           && programVersion.WithdrawalDate != DateTime.MinValue
                                           && programVersion.WithdrawalDate.HasValue)
                                       {
                                           creditHourPaymentPeriodDetails =
                                               await this.programVersionsService.GetTermDetailsForPaymentPeriod(
                                                   enrollmentId,
                                                   programVersion.WithdrawalDate.Value);
                                           if (creditHourPaymentPeriodDetails != null)
                                           {
                                               creditHourPaymentPeriodDetails.ResultStatus = ApiMsgs.TERM_DETAILS_FOUND;
                                           }
                                       }
                                       else
                                       {
                                           creditHourPaymentPeriodDetails.ResultStatus = ApiMsgs.PROGRAM_VERSION_NOT_FOUND;
                                       }
                                   }
                                   else
                                   {
                                       creditHourPaymentPeriodDetails.ResultStatus = ApiMsgs.NOT_CREDIT_HOUR_PROGRAM;
                                   }

                                   return creditHourPaymentPeriodDetails;
                               }
                               catch (Exception e)
                               {
                                   e.TrackException();
                                   var creditHourPaymentPeriodDetails =
                                       new CreditHourPaymentPeriodDetails
                                           {
                                               ResultStatus =
                                                   ApiMsgs.FAILED_GETTING_RECORD
                                           };
                                   return creditHourPaymentPeriodDetails;
                               }
                           });
        }

        /// <summary>
        /// The GetLastDateAttended returns last date attended from attendance table.
        /// </summary> 
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The last date attended.
        /// </returns>
        public async Task<DateTime?> GetLastDateAttended(Guid enrollmentId)
        {
            return await Task.Run(
                       async () =>
                           {
                               return await this.enrollmentService.GetLDA(enrollmentId);
                           });
        }

        #region private methods

        /// <summary>
        /// The get period end date by enrollment id returns the period end date for the given enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<DateTime> GetPeriodEndDateByEnrollmentId(Guid enrollmentId)
        {
            var campusProgramVersionDetails = await this.programVersionsService.GetCampusProgramVersionDetails(enrollmentId); 

            var isPaymentPeriod = await this.calculationPeriodTypeService.IsPaymentPeriodType(campusProgramVersionDetails?.CalculationPeriodTypeId ?? Guid.Empty);
            var periodEndDateDetail = isPaymentPeriod
                                          ? await this.programScheduleDetailService
                                                .GetEndDateOfCreditHourPaymentPeriodByEnrollmentId(enrollmentId)
                                          : await this.programScheduleDetailService
                                                .GetEndDateOfCreditHourPeriodOfEnrollmentByEnrollmentId(enrollmentId);

            return periodEndDateDetail.EndDateOfWithdrawalPeriod ?? DateTime.MinValue;
        }

        /// <summary>
        /// The get all course by enrollment.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<IEnumerable<ArReqs>> GetAllCourseByEnrollment(Guid enrollmentId)
        {
            var enrollment = await this.enrollmentService.Get(enrollmentId);

            var programVersion = this.ProgramVersionRepository.Get(x => x.PrgVerId == enrollment.PrgVerId)
                .Include(x => x.ArCampusPrgVersions).Include(x => x.ArStuEnrollments)
                .ThenInclude(x => x.ArR2t4terminationDetails).Include(x => x.ArProgVerDef).ThenInclude(x => x.Req)
                .FirstOrDefault();

            return programVersion?.ArProgVerDef.Select(x => x.Req); 
        }

        /// <summary>
        /// The get all fail grades.
        /// </summary>
        /// <returns>
        /// The list of fail grades
        /// </returns>
        private List<string> GetAllFailGrades()
        {
            return this.GradeSystemRepository.Get(g => g.IsPass == false).Select(x => x.Grade.Trim()).OrderBy(x => x).ToList();
        }

        /// <summary>
        /// The get all credit summary detail by enrollment id.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollmentId.
        /// </param>
        /// <returns>
        /// The List of CreditSummary.
        /// </returns>
        private async Task<List<CreditSummary>> GetAllCreditSummaryDetailByEnrollmentId(Guid enrollmentId)
        {
            return await Task.Run(
                       async () => await this.creditSummaryService.GetCreditSummaryByEnrollmentId(enrollmentId)) as List<CreditSummary>;
        }  

        /// <summary>
        /// The calculate non standard program days for not substantially equal method returns the Total Days and completed days in the period by an enrollment is. 
        /// CalculateNonStandardProgramDaysForNotSubstantiallyEqual method gives the total and completed days for an enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <param name="startDate">
        /// The start date of the payment period.
        /// </param>
        /// <param name="endDate">
        /// The endDate of the payment period.
        /// </param>
        /// <returns>
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        private async Task<CreditHourPaymentPeriodDaysDetail> CalculateNonStandardProgramDaysForNotSubstantiallyEqual(Guid enrollmentId, DateTime startDate, DateTime endDate)
        {
            var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail();
            var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
            var dateOfWithdrawal = terminationDetails?.LastDateAttended;

            if (dateOfWithdrawal != null)
            {
                var startDateToWithdrawalDateDifference = (dateOfWithdrawal.Value - startDate).TotalDays + 1;

                var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId);
                var scheduledLoaCount = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, dateOfWithdrawal.Value, startDate);

                var scheduledBreakForCompletedDays = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, dateOfWithdrawal.Value, startDate);
                creditHourPaymentPeriodDaysDetail.CompletedDays = Convert.ToInt32(startDateToWithdrawalDateDifference - scheduledLoaCount - scheduledBreakForCompletedDays.TotalScheduledBreakDays);
                
                var starDateToEndDateDifference = (endDate - startDate).TotalDays + 1;
                var studentLoATotalDays = this.enrollmentService.GetStudentLOAs(enrollmentId);
                var scheduleLoaCount = this.programScheduleDetailService.GetLoAsForDateRange(studentLoATotalDays, endDate, startDate);

                var scheduledBreakForTotalDays = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, endDate, startDate);
                creditHourPaymentPeriodDaysDetail.TotalDays = Convert.ToInt32(starDateToEndDateDifference - scheduleLoaCount - scheduledBreakForTotalDays.TotalScheduledBreakDays);
            }

            creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.COMPLETED_DAYS_CALCULATED_SUCCESSFUL;
            return creditHourPaymentPeriodDaysDetail;
        }

        /// <summary>
        /// GetNonTermProgramDaysCalculation by enrollment id calculates the total and completed days for an enrollmentId.
        /// </summary>
        /// <param name="enrollmentId">
        /// The enrollment id.
        /// </param>
        /// <returns>
        /// Returns total and completed days details for the specified enrollment id.
        /// </returns>
        private async Task<CreditHourPaymentPeriodDaysDetail> GetNonStandardProgramDaysCalculation(Guid enrollmentId)
        {
            var creditHourPaymentPeriodDaysDetail = new CreditHourPaymentPeriodDaysDetail();
            var terminationDetails = await this.studentTerminationService.Get(enrollmentId);
            var dateOfWithdrawal = terminationDetails?.LastDateAttended;

            if (dateOfWithdrawal != null)
            {
                var termDates = await this.programVersionsService.GetTermDetailsForPaymentPeriod(enrollmentId, dateOfWithdrawal.Value);

                if (termDates != null && termDates.TermStartDate != DateTime.MinValue && termDates.TermEndDate != DateTime.MinValue)
                {
                    var starDateToWithdrawalDateDifference = (dateOfWithdrawal.Value - termDates.TermStartDate).TotalDays + 1;

                    var studentLoAs = this.enrollmentService.GetStudentLOAs(enrollmentId);
                    var scheduledLoaCount = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, dateOfWithdrawal.Value, termDates.TermStartDate);

                    var scheduledBreakForCompletedDays = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, dateOfWithdrawal.Value, termDates.TermStartDate);
                   
                    creditHourPaymentPeriodDaysDetail.CompletedDays = Convert.ToInt32(starDateToWithdrawalDateDifference - scheduledLoaCount - scheduledBreakForCompletedDays.TotalScheduledBreakDays);

                    var starDateToEndDateDifference = (termDates.TermEndDate - termDates.TermStartDate).TotalDays + 1;
                    var studentLoATotalDays = this.programScheduleDetailService.GetLoAsForDateRange(studentLoAs, termDates.TermEndDate, termDates.TermStartDate);

                    var scheduledBreakForTotalDays = await this.programScheduleDetailService.GetCreditHourScheduledBreak(enrollmentId, termDates.TermEndDate, termDates.TermStartDate);
                    
                    creditHourPaymentPeriodDaysDetail.TotalDays = Convert.ToInt32(starDateToEndDateDifference - studentLoATotalDays - scheduledBreakForTotalDays.TotalScheduledBreakDays);
                    creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.COMPLETED_DAYS_CALCULATED_SUCCESSFUL;
                }
                else
                {
                    creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_FOUND;
                }
            }
            else
            {
                creditHourPaymentPeriodDaysDetail.ResultStatus = ApiMsgs.NOT_FOUND;
            }

            return creditHourPaymentPeriodDaysDetail;
        }

        #endregion
    }
}