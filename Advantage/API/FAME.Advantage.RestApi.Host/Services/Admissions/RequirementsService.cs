﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RequirementsService.cs" company="FAME Inc.">
//   FAME Inc. 2018 
// </copyright>
// <summary>
//   The Requirements service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Admissions
{
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;

    /// <inheritdoc />
    /// <summary>
    /// The Requirements Service.
    /// </summary>
    public class RequirementsService : IRequirementsService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext context;

        /// <summary>
        /// The mapper is used to map the requirement Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequirementsService"/> class. 
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public RequirementsService(IAdvantageDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <summary>
        /// The adreqs repository.
        /// </summary>
        private IRepository<AdReqs> AdReqsRepository =>
            this.context.GetRepositoryForEntity<AdReqs>();

        /// <inheritdoc />
        /// <summary>
        /// The get by code.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <returns>
        /// The <see cref="T:System.Threading.Tasks.Task" />.
        /// </returns>
        public async Task<AdReqs> GetByCode(string code)
        {
            return await Task.Run(
                       () =>
                           {
                               var requirement = this.AdReqsRepository.Get(x => x.Code == code).FirstOrDefault();
                               return this.mapper.Map<AdReqs>(requirement);
                           });
        }
    }
}
