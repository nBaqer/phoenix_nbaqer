﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsEmailService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadsEmailService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;

    /// <summary>
    /// The lead email service.
    /// </summary>
    public class LeadsEmailService : ILeadsEmailService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext uow;

        /// <summary>
        /// The system status service.
        /// </summary>
        private readonly IStatusesService systemStatusService;

        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The object validation service.
        /// </summary>
        private readonly IObjectValidationService objectValidationService;

        /// <summary>
        /// The lead service.
        /// </summary>
        private ILeadService leadService;

        /// <summary>
        /// The statuses service.
        /// </summary>
        private IStatusesService statusesService;


        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsEmailService"/> class.
        /// </summary>
        /// <param name="uow">
        /// The uow.
        /// </param>
        /// <param name="systemStatusService">
        /// The system status service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="serviceBuilder">
        /// The service Builder.
        /// </param>
        /// <param name="objectValidationService">
        /// The object Validation Service.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        public LeadsEmailService(IAdvantageDbContext uow, IStatusesService systemStatusService, IMapper mapper, IServiceBuilder serviceBuilder, IObjectValidationService objectValidationService, IStatusesService statusService)
        {
            this.uow = uow;
            this.systemStatusService = systemStatusService;
            this.mapper = mapper;
            this.serviceBuilder = serviceBuilder;
            this.objectValidationService = objectValidationService;
            this.statusesService = statusService;
        }

        /// <summary>
        /// The lead email repository.
        /// </summary>
        private IRepository<AdLeadEmail> EmailRepository => this.uow.GetRepositoryForEntity<AdLeadEmail>();

        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.uow.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ListActionResult<EmailAddress>> GetAll(Guid leadId)
        {
            ListActionResult<EmailAddress> list = new ListActionResult<EmailAddress>();
            if (leadId == Guid.Empty)
            {
                list.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.";
                list.ResultStatus = Enums.ResultStatus.Error;
                return list;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();
            list = this.mapper.Map<ListActionResult<EmailAddress>>(
                this.EmailRepository.Get(
                    x => x.LeadId == leadId && x.StatusId == activeStatus));

            if (list != null && list.Any())
            {
                list.ResultStatus = Enums.ResultStatus.Success;
            }
            else if (list != null)
            {
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }
            else
            {
                list = new ListActionResult<EmailAddress>();
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }

            return list;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<EmailAddress> Get(Guid leadId, Guid id)
        {
            if (id == Guid.Empty)
            {
                var model = new EmailAddress
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            if (leadId == Guid.Empty)
            {
                var model = new EmailAddress
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();

            var email = this.EmailRepository.Get(
                x => x.LeadEmailId == id && x.LeadId == leadId && x.StatusId == activeStatus);

            EmailAddress result = null;

            if (email != null)
            {
                result = this.mapper.Map<EmailAddress>(email);
            }

            if (result == null)
            {
                result = new EmailAddress
                {
                    ResultStatusMessage = ApiMsgs.NOT_FOUND,
                    ResultStatus = Enums.ResultStatus.Warning
                };
            }

            return result;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<EmailAddress> Create(EmailAddress model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(async () =>
            {
                var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                var user = this.uow.GetTenantUser();

                var validatedModel = this.ValidateReferences(model);

                // if model is not null, then a foreign key to another entity was not found
                if (validatedModel != null)
                {
                    return validatedModel;
                }

                var lead = this.leadService.Get(model.LeadId).Result;
                EmailAddress email = new EmailAddress();

                if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                {
                    var entity = new AdLeadEmail();
                    if (lead.LeadId != null && model.EmailTypeId.HasValue)
                    {
                        var isPreferred = false;
                        if (model.IsPreferred.HasValue)
                        {
                            if (model.IsPreferred.Value)
                            {
                                if (this.HasEmailPreferred(model.LeadId, activeStatus, out var result))
                                {
                                    email.ResultStatusMessage = ApiMsgs.LeadEmailOnlyOnePreferredEmail;
                                    email.ResultStatus = Enums.ResultStatus.Error;
                                    return email;
                                }
                                else
                                {
                                    isPreferred = true;
                                }
                            }
                        }

                        if (!isPreferred)
                        {
                            if (model.IsShowOnLeadPage.HasValue)
                            {
                                if (model.IsShowOnLeadPage.Value)
                                {
                                    if (this.HasEmailsShownOnLeadPage(model.LeadId, activeStatus, out var results) && results.Count == 1)
                                    {
                                        email.ResultStatusMessage = ApiMsgs.LeadEmailNoRoomOnLeadPage;
                                        email.ResultStatus = Enums.ResultStatus.Error;
                                        return email;
                                    }
                                }
                            }
                        }

                        entity.LeadEmailId = Guid.NewGuid();
                        entity.LeadId = lead.LeadId.Value;
                        entity.StatusId = activeStatus;
                        entity.IsPortalUserName = model.IsPortalUserName ?? false;
                        entity.IsShowOnLeadPage = (model.IsPreferred.HasValue && model.IsPreferred.Value) || (model.IsShowOnLeadPage ?? false);
                        entity.IsPreferred = model.IsPreferred ?? false;
                        entity.ModDate = DateTime.Now;
                        entity.ModUser = user.Email;
                        entity.Email = model.Email;
                        entity.EmailTypeId = model.EmailTypeId.Value;

                        await this.EmailRepository.CreateAsync(entity);

                        email = this.mapper.Map<EmailAddress>(entity);
                    }
                    else
                    {
                        email.ResultStatus = Enums.ResultStatus.NotFound;
                        model.ResultStatusMessage = "The lead could not be found with the specific id.";
                    }
                }
                else if (lead != null)
                {
                    email = new EmailAddress { ResultStatus = lead.ResultStatus, ResultStatusMessage = lead.ResultStatusMessage };
                }

                return email;
            });
        }


        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<EmailAddress> Update(EmailAddress model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                       {
                           var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                           var user = this.uow.GetTenantUser();
                           var validatedModel = this.ValidateReferences(model);

                           // if model is not null, then a foreign key to another entity was not found
                           if (validatedModel != null)
                           {
                               return validatedModel;
                           }


                           var lead = this.leadService.Get(model.LeadId).Result;

                           if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success && model.Id.HasValue)
                           {
                               var email = this.EmailRepository.FirstOrDefault(
                                   x => x.LeadId == model.LeadId && x.LeadEmailId == model.Id
                                                                 && x.StatusId == activeStatus);

                               if (email != null && model.EmailTypeId.HasValue)
                               {
                                   email.ModDate = DateTime.Now;
                                   email.ModUser = user.Email;
                                   email.EmailTypeId = model.EmailTypeId.Value;
                                   email.Email = model.Email;

                                   if (model.StatusId != null)
                                   {
                                       email.StatusId = model.StatusId.Value;
                                   }

                                   if (model.IsPortalUserName != null)
                                   {
                                       email.IsPortalUserName = model.IsPortalUserName.Value;
                                   }

                                   var isPreferred = false;
                                   if (model.IsPreferred.HasValue)
                                   {

                                       if (model.IsPreferred.Value)
                                       {
                                           if (this.HasEmailPreferred(model.LeadId, activeStatus, out var result))
                                           {
                                               if (!result.Equals(model.Id.Value))
                                               {
                                                   model.ResultStatusMessage = ApiMsgs.LeadEmailOnlyOnePreferredEmail;
                                                   model.ResultStatus = Enums.ResultStatus.Error;
                                                   return model;
                                               }
                                               else
                                               {
                                                   isPreferred = true;
                                               }
                                           }
                                       }

                                   }

                                   if (!isPreferred)
                                   {
                                       if (model.IsShowOnLeadPage.HasValue)
                                       {
                                           if (model.IsShowOnLeadPage.Value)
                                           {
                                               if (this.HasEmailsShownOnLeadPage(model.LeadId, activeStatus, out var results) && results.Count == 1)
                                               {
                                                   if (!results.Contains(model.Id.Value))
                                                   {
                                                       model.ResultStatusMessage = ApiMsgs.LeadEmailNoRoomOnLeadPage;
                                                       model.ResultStatus = Enums.ResultStatus.Error;
                                                       return model;
                                                   }

                                               }
                                           }

                                       }
                                   }


                                   email.IsShowOnLeadPage = (model.IsPreferred.HasValue && model.IsPreferred.Value) || (model.IsShowOnLeadPage ?? false);
                                   email.IsPreferred = model.IsPreferred ?? false;
                                   this.EmailRepository.Update(email);

                                   return this.mapper.Map<EmailAddress>(email);
                               }
                               else
                               {
                                   model.ResultStatus = Enums.ResultStatus.NotFound;
                                   model.ResultStatusMessage = "The email could not be found for the specified lead.";
                               }


                               return model;
                           }
                           else
                           {
                               model.ResultStatus = lead?.ResultStatus ?? Enums.ResultStatus.NotFound;
                               model.ResultStatusMessage = lead?.ResultStatusMessage ?? "The lead could not be found for the specified id.";

                               return model;
                           }
                       });
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> Delete(Guid leadId, Guid id)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                       {
                           var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                           var inactiveStatus = this.systemStatusService.GetInactiveStatusId().Result;
                           var user = this.uow.GetTenantUser();
                           var lead = this.leadService.Get(leadId).Result;
                           if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                           {
                               var email = this.EmailRepository.FirstOrDefault(
                                   x => x.LeadId == leadId && x.LeadEmailId == id
                                                                 && x.StatusId == activeStatus);

                               if (email != null)
                               {
                                   email.StatusId = inactiveStatus;
                                   email.ModDate = DateTime.Now;
                                   email.ModUser = user.Email;
                                   this.EmailRepository.Update(email);
                               }
                               else
                               {
                                   return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                               }
                           }
                           else
                           {
                               return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                           }

                           return ApiMsgs.DELETE_SUCCESSFUL;
                       });
        }


        /// <summary>
        /// The validate references.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private EmailAddress ValidateReferences(EmailAddress model)
        {
            EmailAddress result = new EmailAddress();
            string resultStatusMessage = string.Empty;

            // validate email type id
            if (model.EmailTypeId.HasValue && model.EmailTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyEmailType, Guid>(model.EmailTypeId.Value, out resultStatusMessage, "EmailTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            return null;
        }



        /// <summary>
        /// Does this lead have emails shown on lead info page.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasEmailsShownOnLeadPage(Guid leadId, Guid activeStatusId, out List<Guid> results)
        {

            results = this.EmailRepository.Get(x => x.LeadId == leadId && x.IsShowOnLeadPage && !x.IsPreferred && x.StatusId == activeStatusId).Select(em => em.LeadEmailId).ToList();
            return results.Count > 0;
        }

        /// <summary>
        /// The is the has email the preferred.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="result">
        /// The resulting preferred email.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasEmailPreferred(Guid leadId, Guid activeStatusId, out Guid result)
        {
            result = this.EmailRepository.Get(x => x.LeadId == leadId && x.IsPreferred && x.StatusId == activeStatusId).Select(em => em.LeadEmailId).FirstOrDefault();
            return result != Guid.Empty;
        }

        /// <summary>
        /// The get best.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetBest(Guid studentEnrollmentId)
        {
            string best = string.Empty;

            var activeStatusId = this.statusesService.GetActiveStatusId().Result;

            var leadId = this.EnrollmentRepository.Get(x => x.StuEnrollId == studentEnrollmentId).Select(e => e.LeadId)
                .FirstOrDefault();

            if (leadId.HasValue)
            {
                best = this.EmailRepository.Get(e => e.LeadId == leadId && e.IsShowOnLeadPage && e.StatusId == activeStatusId).Select(e => e.Email).FirstOrDefault();
                
                // if haven't got value for best email try with is preferred
                if (best.IsNullOrEmpty())
                {
                    best = this.EmailRepository.Get(e => e.LeadId == leadId && e.IsPreferred && e.StatusId == activeStatusId).Select(e => e.Email).FirstOrDefault();
                }
            }

            return best;
        }

    }
}

