﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadsAddressService.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadsAddressService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;

    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// The lead address service.
    /// </summary>
    public class LeadsAddressService : ILeadsAddressService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext uow;

        /// <summary>
        /// The system status service.
        /// </summary>
        private readonly IStatusesService systemStatusService;

        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The object validation service.
        /// </summary>
        private readonly IObjectValidationService objectValidationService;

        /// <summary>
        /// The lead service.
        /// </summary>
        private ILeadService leadService;

        /// <summary>
        /// The statuses service.
        /// </summary>
        private IStatusesService statusesService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsAddressService"/> class.
        /// </summary>
        /// <param name="uow">
        /// The uow.
        /// </param>
        /// <param name="systemStatusService">
        /// The system status service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="serviceBuilder">
        /// The service Builder.
        /// </param>
        /// <param name="objectValidationService">
        /// The object Validation Service.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        public LeadsAddressService(IAdvantageDbContext uow, IStatusesService systemStatusService, IMapper mapper, IServiceBuilder serviceBuilder, IObjectValidationService objectValidationService, IStatusesService statusService)
        {
            this.uow = uow;
            this.systemStatusService = systemStatusService;
            this.mapper = mapper;
            this.serviceBuilder = serviceBuilder;
            this.objectValidationService = objectValidationService;
            this.statusesService = statusService;
        }

        /// <summary>
        /// The lead address repository.
        /// </summary>
        private IRepository<AdLeadAddresses> AddressRepository => this.uow.GetRepositoryForEntity<AdLeadAddresses>();


        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.uow.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ListActionResult<Address>> GetAll(Guid leadId)
        {

            ListActionResult<Address> list = new ListActionResult<Address>();
            if (leadId == Guid.Empty)
            {
                list.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.";
                list.ResultStatus = Enums.ResultStatus.Error;
                return list;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();
            list = this.mapper.Map<ListActionResult<Address>>(
               this.AddressRepository.Get(
                   x => x.LeadId == leadId && x.StatusId == activeStatus));

            if (list != null && list.Any())
            {
                list.ResultStatus = Enums.ResultStatus.Success;
            }
            else if (list != null)
            {
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }
            else
            {
                list = new ListActionResult<Address>();
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }

            return list;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Address> Get(Guid leadId, Guid id)
        {
            if (id == Guid.Empty)
            {
                var model = new Address
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            if (leadId == Guid.Empty)
            {
                var model = new Address
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();

            var address = this.AddressRepository.Get(
                x => x.AdLeadAddressId == id && x.LeadId == leadId && x.StatusId == activeStatus);

            Address result = null;

            if (address != null)
            {
                result = this.mapper.Map<Address>(address);
            }

            if (result == null)
            {
                result = new Address
                {
                    ResultStatusMessage = ApiMsgs.NOT_FOUND,
                    ResultStatus = Enums.ResultStatus.Warning
                };
            }

            return result;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Address> Create(Address model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(async () =>
            {
                var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                var user = this.uow.GetTenantUser();
                Address address = new Address();

                if (model.LeadId != null && model.AddressTypeId != null)
                {
                    var validatedModel = this.ValidateReferences(model);

                    // if model is not null, then a foreign key to another entity was not found
                    if (validatedModel != null)
                    {
                        return validatedModel;
                    }

                    var lead = this.leadService.Get(model.LeadId.Value).Result;
                    if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                    {
                        var entity = new AdLeadAddresses();

                        if (lead.LeadId != null)
                        {

                            if (model.IsMailingAddress.HasValue)
                            {
                                if (this.HasMailingAddress(model.LeadId.Value, activeStatus, out var result) && model.IsMailingAddress.Value)
                                {
                                    address.ResultStatusMessage = ApiMsgs.LeadAddressOnlyOneMailing;
                                    address.ResultStatus = Enums.ResultStatus.Error;
                                    return address;
                                }
                            }


                            if (model.IsShowOnLeadPage.HasValue)
                            {
                                if (this.HasAddressShownOnLeadPage(model.LeadId.Value, activeStatus, out var results) && model.IsShowOnLeadPage.Value)
                                {
                                    address.ResultStatusMessage = ApiMsgs.LeadAddressOnlyOneShownOnLeadPage;
                                    address.ResultStatus = Enums.ResultStatus.Error;
                                    return address;
                                }
                            }

                            entity.AdLeadAddressId = Guid.NewGuid();
                            entity.LeadId = lead.LeadId.Value;
                            entity.StatusId = activeStatus;
                            entity.Address1 = model.Address1;
                            entity.Address2 = model.Address2;
                            entity.AddressApto = model.ApartmentNumber ?? string.Empty;
                            entity.City = model.City;
                            entity.StateId = model.StateId;
                            entity.CountryId = model.CountryId;
                            entity.ZipCode = model.Zip;
                            entity.CountyId = model.CountyId;
                            entity.ForeignCountryStr = model.ForeignCountryStr;
                            entity.ForeignCountyStr = model.ForeignCountyStr;
                            entity.IsInternational = model.IsInternational ?? false;
                            entity.IsMailingAddress = model.IsMailingAddress ?? false;
                            entity.IsShowOnLeadPage = model.IsShowOnLeadPage ?? false;
                            entity.ModDate = DateTime.Now;
                            entity.ModUser = user.Email;
                            entity.AddressTypeId = model.AddressTypeId.Value;

                            await this.AddressRepository.CreateAsync(entity);

                            address = this.mapper.Map<Address>(entity);
                        }
                        else
                        {
                            address.ResultStatus = Enums.ResultStatus.NotFound;
                        }
                    }
                    else if (lead != null)
                    {
                        address = new Address { ResultStatus = lead.ResultStatus, ResultStatusMessage = lead.ResultStatusMessage };
                    }
                }

                return address;
            });
        }



        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Address> Update(Address model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                       {
                           var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                           var user = this.uow.GetTenantUser();
                           if (model.LeadId != null && model.AddressTypeId != null && model.Id.HasValue)
                           {
                               var validatedModel = this.ValidateReferences(model);

                               // if model is not null, then a foreign key to another entity was not found
                               if (validatedModel != null)
                               {
                                   return validatedModel;
                               }

                               var lead = this.leadService.Get(model.LeadId.Value).Result;

                               if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                               {
                                   var address = this.AddressRepository.FirstOrDefault(
                                       x => x.LeadId == model.LeadId && x.AdLeadAddressId == model.Id
                                                                     && x.StatusId == activeStatus);
                                   if (address != null)
                                   {
                                       if (model.IsMailingAddress.HasValue)
                                       {
                                           if (this.HasMailingAddress(model.LeadId.Value, activeStatus, out var result))
                                           {
                                               if (model.IsMailingAddress.Value && !result.Equals(model.Id.Value))
                                               {
                                                   model.ResultStatusMessage = ApiMsgs.LeadAddressOnlyOneMailing;
                                                   model.ResultStatus = Enums.ResultStatus.Error;
                                                   return model;
                                               }
                                           }
                                       }

                                       if (model.IsShowOnLeadPage.HasValue)
                                       {
                                           if (this.HasAddressShownOnLeadPage(model.LeadId.Value, activeStatus, out var results))
                                           {
                                               if (model.IsShowOnLeadPage.Value && !results.Contains(model.Id.Value))
                                               {
                                                   model.ResultStatusMessage = ApiMsgs.LeadAddressOnlyOneShownOnLeadPage;
                                                   model.ResultStatus = Enums.ResultStatus.Error;
                                                   return model;
                                               }
                                           }
                                       }

                                       address.ModDate = DateTime.Now;
                                       address.ModUser = user.Email;
                                       address.AddressTypeId = model.AddressTypeId.Value;

                                       if (model.Address1 != null)
                                       {
                                           address.Address1 = model.Address1;
                                       }

                                       if (model.Address2 != null)
                                       {
                                           address.Address2 = model.Address2;
                                       }

                                       if (!string.IsNullOrEmpty(model.ApartmentNumber))
                                       {
                                           address.AddressApto = model.ApartmentNumber;
                                       }
                                       else
                                       {
                                           if (address.AddressApto == null)
                                           {
                                               address.AddressApto = string.Empty;
                                           }
                                       }

                                       if (model.City != null)
                                       {
                                           address.City = model.City;
                                       }

                                       if (model.Zip != null)
                                       {
                                           address.ZipCode = model.Zip;
                                       }

                                       if (model.StateId.HasValue)
                                       {
                                           address.StateId = model.StateId;
                                       }

                                       if (model.CountryId.HasValue)
                                       {
                                           address.CountryId = model.CountryId;
                                       }

                                       if (model.CountyId.HasValue)
                                       {
                                           address.CountyId = model.CountyId;
                                       }

                                       if (model.StatusId != null)
                                       {
                                           address.StatusId = model.StatusId.Value;
                                       }

                                       if (model.IsInternational != null)
                                       {
                                           address.IsInternational = model.IsInternational.Value;
                                       }

                                       address.IsShowOnLeadPage = model.IsShowOnLeadPage ?? false;
                                       address.IsMailingAddress = model.IsMailingAddress ?? false;

                                       this.AddressRepository.Update(address);

                                       return this.mapper.Map<Address>(address);
                                   }
                                   else
                                   {
                                       model.ResultStatus = Enums.ResultStatus.NotFound;
                                       model.ResultStatusMessage = "The address could not be found for the specified lead.";
                                   }

                                   return model;
                               }
                               else
                               {
                                   model.ResultStatus = lead?.ResultStatus ?? Enums.ResultStatus.NotFound;
                                   model.ResultStatusMessage = lead?.ResultStatusMessage ?? "The lead could not be found for the specified id.";

                                   return model;
                               }
                           }

                           return model;
                       });
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> Delete(Guid leadId, Guid id)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                       {
                           var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                           var inactiveStatus = this.systemStatusService.GetInactiveStatusId().Result;
                           var user = this.uow.GetTenantUser();
                           var lead = this.leadService.Get(leadId).Result;
                           if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                           {
                               var address = this.AddressRepository.FirstOrDefault(
                                   x => x.LeadId == leadId && x.AdLeadAddressId == id
                                                                 && x.StatusId == activeStatus);

                               if (address != null)
                               {
                                   address.StatusId = inactiveStatus;
                                   address.ModDate = DateTime.Now;
                                   address.ModUser = user.Email;
                                   this.AddressRepository.Update(address);
                               }
                               else
                               {
                                   return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                               }
                           }
                           else
                           {
                               return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                           }

                           return ApiMsgs.DELETE_SUCCESSFUL;
                       });
        }

        /// <summary>
        /// The get best.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <param name="city">
        /// The city.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public string GetBest(Guid studentEnrollmentId, out string city, out string state, out string zipCode, out string address2)
        {
            string best = string.Empty;
            city = string.Empty;
            state = string.Empty;
            zipCode = string.Empty;
            address2 = string.Empty;


            var activeStatusId = this.statusesService.GetActiveStatusId().Result;

            var leadId = this.EnrollmentRepository.Get(x => x.StuEnrollId == studentEnrollmentId).Select(e => e.LeadId)
                .FirstOrDefault();

            if (leadId.HasValue)
            {
                var address = this.AddressRepository.Get(e => e.LeadId == leadId && e.IsShowOnLeadPage && e.StatusId == activeStatusId).Include(a => a.StateNavigation).Select(e => e).FirstOrDefault();

                // if haven't got value for mail address with is show on lead page field try with is mailing address
                if (address == null)
                {
                    address = this.AddressRepository.Get(e => e.LeadId == leadId && e.IsMailingAddress && e.StatusId == activeStatusId).Include(a => a.StateNavigation).Select(e => e).FirstOrDefault();
                }

                // if address is not null then set values
                if (address != null)
                {
                    best = address.Address1;
                    address2 = address.Address2;
                    city = address.City;
                    state = address.State.IsNullOrEmpty() ? (address.StateNavigation.StateCode ?? string.Empty) : address.State;
                    zipCode = address.ZipCode;
                }
            }

            return best;
        }

        /// <summary>
        /// The validate references.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private Address ValidateReferences(Address model)
        {
            Address result = new Address();
            string resultStatusMessage = string.Empty;

            // validate address type id
            if (model.AddressTypeId != null && model.AddressTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<PlAddressTypes, Guid>(model.AddressTypeId.Value, out resultStatusMessage, "AddressTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate state id
            if (model.StateId != null && model.StateId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyStates, Guid>(model.StateId.Value, out resultStatusMessage, "StateId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate country id
            if (model.CountryId != null && model.CountryId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdCountries, Guid>(model.CountryId.Value, out resultStatusMessage, "CountryId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate county id
            if (model.CountyId != null && model.CountyId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdCounties, Guid>(model.CountyId.Value, out resultStatusMessage, "CountyId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            return null;
        }

        /// <summary>
        /// The is the address shown on lead page.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasAddressShownOnLeadPage(Guid leadId, Guid activeStatusId, out List<Guid> results)
        {

            results = this.AddressRepository.Get(x => x.LeadId == leadId && x.IsShowOnLeadPage && x.StatusId == activeStatusId).Select(addr => addr.AdLeadAddressId).ToList();

            return results.Count > 0;
        }

        /// <summary>
        /// The has mailing address.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="result">
        /// The resulting mailing address.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasMailingAddress(Guid leadId, Guid activeStatusId, out Guid result)
        {
            result = this.AddressRepository.Get(x => x.LeadId == leadId && x.IsMailingAddress && x.StatusId == activeStatusId).Select(addr => addr.AdLeadAddressId).FirstOrDefault();

            return result != Guid.Empty;
        }
    }
}

