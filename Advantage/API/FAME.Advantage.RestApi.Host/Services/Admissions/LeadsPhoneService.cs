﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPhoneService.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the LeadPhoneService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Services.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;

    /// <summary>
    /// The lead phone service.
    /// </summary>
    public class LeadsPhoneService : ILeadsPhoneService
    {
        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext uow;

        /// <summary>
        /// The system status service.
        /// </summary>
        private readonly IStatusesService systemStatusService;

        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;

        /// <summary>
        /// The object validation service.
        /// </summary>
        private readonly IObjectValidationService objectValidationService;

        /// <summary>
        /// The mapper.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The lead service.
        /// </summary>
        private ILeadService leadService;

        /// <summary>
        /// The statuses service.
        /// </summary>
        private IStatusesService statusesService;


        /// <summary>
        /// Initializes a new instance of the <see cref="LeadsPhoneService"/> class.
        /// </summary>
        /// <param name="uow">
        /// The uow.
        /// </param>
        /// <param name="systemStatusService">
        /// The system status service.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="serviceBuilder">
        /// The service Builder.
        /// </param>
        /// <param name="objectValidationService">
        /// The object Validation Service.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        public LeadsPhoneService(IAdvantageDbContext uow, IStatusesService systemStatusService, IMapper mapper, IServiceBuilder serviceBuilder, IObjectValidationService objectValidationService, IStatusesService statusService)
        {
            this.uow = uow;
            this.systemStatusService = systemStatusService;
            this.mapper = mapper;
            this.serviceBuilder = serviceBuilder;
            this.objectValidationService = objectValidationService;
            this.statusesService = statusService;
        }

        /// <summary>
        /// The lead phone repository.
        /// </summary>
        private IRepository<AdLeadPhone> PhoneRepository => this.uow.GetRepositoryForEntity<AdLeadPhone>();


        /// <summary>
        /// The enrollment repository.
        /// </summary>
        private IRepository<ArStuEnrollments> EnrollmentRepository => this.uow.GetRepositoryForEntity<ArStuEnrollments>();

        /// <summary>
        /// The get all.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<ListActionResult<PhoneNumber>> GetAll(Guid leadId)
        {
            ListActionResult<PhoneNumber> list = new ListActionResult<PhoneNumber>();
            if (leadId == Guid.Empty)
            {
                list.ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.";
                list.ResultStatus = Enums.ResultStatus.Error;
                return list;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();
            list = this.mapper.Map<ListActionResult<PhoneNumber>>(
                           this.PhoneRepository.Get(
                               x => x.LeadId == leadId && x.StatusId == activeStatus).OrderBy(x => x.Position));

            if (list != null && list.Any())
            {
                list.ResultStatus = Enums.ResultStatus.Success;
            }
            else if (list != null)
            {
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }
            else
            {
                list = new ListActionResult<PhoneNumber>();
                list.ResultStatus = Enums.ResultStatus.NotFound;
            }

            return list;
        }

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<PhoneNumber> Get(Guid leadId, Guid id)
        {
            if (id == Guid.Empty)
            {
                var model = new PhoneNumber
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            if (leadId == Guid.Empty)
            {
                var model = new PhoneNumber
                {
                    ResultStatusMessage = ApiMsgs.MISSING_REQUIRED_PARAMS_ERROR + " Lead Id.",
                    ResultStatus = Enums.ResultStatus.Error
                };
                return model;
            }

            var activeStatus = await this.systemStatusService.GetActiveStatusId();

            var phone = this.PhoneRepository.Get(
                x => x.LeadPhoneId == id && x.LeadId == leadId && x.StatusId == activeStatus);

            PhoneNumber result = null;

            if (phone != null)
            {
                result = this.mapper.Map<PhoneNumber>(phone);
            }

            if (result == null)
            {
                result = new PhoneNumber
                {
                    ResultStatusMessage = ApiMsgs.NOT_FOUND,
                    ResultStatus = Enums.ResultStatus.Warning
                };
            }

            return result;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<PhoneNumber> Create(PhoneNumber model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(async () =>
            {
                var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                var user = this.uow.GetTenantUser();

                var validatedModel = this.ValidateReferences(model);

                // if model is not null, then a foreign key to another entity was not found
                if (validatedModel != null)
                {
                    return validatedModel;
                }

                var lead = this.leadService.Get(model.LeadId).Result;
                PhoneNumber phone = new PhoneNumber();
                if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                {
                    var entity = new AdLeadPhone();
                    if (lead.LeadId != null && model.PhoneTypeId.HasValue)
                    {
                        entity.LeadPhoneId = Guid.NewGuid();
                        entity.LeadId = lead.LeadId.Value;
                        entity.StatusId = activeStatus;
                        entity.Extension = model.Extension ?? string.Empty;
                        entity.IsForeignPhone = model.IsForeignPhone ?? false;
                        entity.ModDate = DateTime.Now;
                        entity.ModUser = user.Email;
                        entity.Phone = model.Phone;
                        entity.PhoneTypeId = model.PhoneTypeId.Value;

                        var isBest = false;
                        if (model.IsBest.HasValue)
                        {
                            if (model.IsBest.Value)
                            {
                                if (this.HasThePhoneTheBest(model.LeadId, activeStatus, out var result))
                                {
                                    phone.ResultStatusMessage = ApiMsgs.LeadPhoneOnlyOneBestPhone;
                                    phone.ResultStatus = Enums.ResultStatus.Error;
                                    return phone;
                                }
                                else
                                {
                                    isBest = true;
                                }
                            }


                        }

                        if (!isBest)
                        {
                            if (model.IsShowOnLeadPage.HasValue)
                            {
                                if (model.IsShowOnLeadPage.Value)
                                {
                                    if (this.HasPhonesShownOnLeadPage(model.LeadId, activeStatus, out var results) && results.Count == 2)
                                    {
                                        phone.ResultStatusMessage = ApiMsgs.LeadPhoneNoRoomOnLeadPage;
                                        phone.ResultStatus = Enums.ResultStatus.Error;
                                        return phone;
                                    }
                                }

                            }
                        }

                        entity.IsShowOnLeadPage = (model.IsBest.HasValue && model.IsBest.Value) || (model.IsShowOnLeadPage ?? false);
                        entity.IsBest = model.IsBest ?? false;
                        entity.Position = model.IsBest.HasValue && model.IsBest.Value ? 1 : this.GetPhonePosition(model.LeadId, activeStatus);

                        await this.PhoneRepository.CreateAsync(entity);

                        phone = this.mapper.Map<PhoneNumber>(entity);
                    }
                    else
                    {
                        phone.ResultStatus = Enums.ResultStatus.NotFound;
                        phone.ResultStatusMessage = "The lead could not be found with the specific id.";

                    }
                }
                else if (lead != null)
                {
                    phone = new PhoneNumber { ResultStatus = lead.ResultStatus, ResultStatusMessage = lead.ResultStatusMessage };
                }

                return phone;
            });
        }


        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<PhoneNumber> Update(PhoneNumber model)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                           {
                               var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                               var user = this.uow.GetTenantUser();

                               var validatedModel = this.ValidateReferences(model);

                               // if model is not null, then a foreign key to another entity was not found
                               if (validatedModel != null)
                               {
                                   return validatedModel;
                               }

                               var lead = this.leadService.Get(model.LeadId).Result;

                               if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success && model.Id.HasValue)
                               {
                                   var phone = this.PhoneRepository.FirstOrDefault(
                                       x => x.LeadId == model.LeadId && x.LeadPhoneId == model.Id
                                                                     && x.StatusId == activeStatus);

                                   if (phone != null && model.PhoneTypeId.HasValue)
                                   {
                                       if (!string.IsNullOrEmpty(model.Extension))
                                       {
                                           phone.Extension = model.Extension;
                                       }
                                       else
                                       {
                                           if (phone.Extension == null)
                                           {
                                               phone.Extension = string.Empty;
                                           }
                                       }

                                       phone.ModDate = DateTime.Now;
                                       phone.ModUser = user.Email;
                                       phone.PhoneTypeId = model.PhoneTypeId.Value;
                                       phone.Phone = model.Phone;

                                       if (model.StatusId != null)
                                       {
                                           phone.StatusId = model.StatusId.Value;
                                       }

                                       if (model.IsForeignPhone != null)
                                       {
                                           phone.IsForeignPhone = model.IsForeignPhone.Value;
                                       }


                                       var isBest = false;

                                       if (model.IsBest.HasValue)
                                       {
                                           if (model.IsBest.Value)
                                           {
                                               if (this.HasThePhoneTheBest(model.LeadId, activeStatus, out var result))
                                               {

                                                   if (!result.Equals(model.Id.Value))
                                                   {
                                                       model.ResultStatusMessage = ApiMsgs.LeadPhoneOnlyOneBestPhone;
                                                       model.ResultStatus = Enums.ResultStatus.Error;
                                                       return model;
                                                   }
                                                   else
                                                   {
                                                       isBest = true;
                                                   }
                                               }
                                           }

                                       }

                                       if (!isBest)
                                       {
                                           if (model.IsShowOnLeadPage.HasValue)
                                           {
                                               if (model.IsShowOnLeadPage.Value)
                                               {
                                                   if (this.HasPhonesShownOnLeadPage(model.LeadId, activeStatus, out var results) && results.Count == 2)
                                                   {
                                                       if (!results.Contains(model.Id.Value))
                                                       {
                                                           model.ResultStatusMessage = ApiMsgs.LeadPhoneNoRoomOnLeadPage;
                                                           model.ResultStatus = Enums.ResultStatus.Error;
                                                           return model;
                                                       }
                                                   }
                                               }
                                           }
                                       }

                                       phone.IsShowOnLeadPage = (model.IsBest.HasValue && model.IsBest.Value) || (model.IsShowOnLeadPage ?? false);
                                       phone.IsBest = model.IsBest ?? false;
                                       phone.Position = model.IsBest.HasValue && model.IsBest.Value ? 1 : phone.Position;
                                       this.PhoneRepository.Update(phone);

                                       return this.mapper.Map<PhoneNumber>(phone);
                                   }
                                   else
                                   {
                                       model.ResultStatus = Enums.ResultStatus.NotFound;
                                       model.ResultStatusMessage = "The phone could not be found for the specified lead.";
                                   }


                                   return model;
                               }
                               else
                               {
                                   model.ResultStatus = lead?.ResultStatus ?? Enums.ResultStatus.NotFound;
                                   model.ResultStatusMessage = lead?.ResultStatusMessage ?? "The lead could not be found for the specified id.";

                                   return model;
                               }
                           });
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> Delete(Guid leadId, Guid id)
        {
            this.leadService = this.serviceBuilder.GetServiceInstance<ILeadService>();
            return await Task.Run(
                       () =>
                           {
                               var activeStatus = this.systemStatusService.GetActiveStatusId().Result;
                               var inactiveStatus = this.systemStatusService.GetInactiveStatusId().Result;
                               var user = this.uow.GetTenantUser();
                               var lead = this.leadService.Get(leadId).Result;
                               if (lead != null && lead.ResultStatus == Enums.ResultStatus.Success)
                               {
                                   var phone = this.PhoneRepository.FirstOrDefault(
                                       x => x.LeadId == leadId && x.LeadPhoneId == id
                                                                     && x.StatusId == activeStatus);

                                   if (phone != null)
                                   {
                                       phone.StatusId = inactiveStatus;
                                       phone.ModDate = DateTime.Now;
                                       phone.ModUser = user.Email;
                                       this.PhoneRepository.Update(phone);
                                   }
                                   else
                                   {
                                       return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                                   }
                               }
                               else
                               {
                                   return ApiMsgs.DELETE_RECORD_NOT_FOUND;
                               }

                               return ApiMsgs.DELETE_SUCCESSFUL;
                           });
        }

        /// <summary>
        /// The validate references.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private PhoneNumber ValidateReferences(PhoneNumber model)
        {
            PhoneNumber result = new PhoneNumber();
            string resultStatusMessage = string.Empty;

            // validate phone type id
            if (model.PhoneTypeId.HasValue && model.PhoneTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyPhoneType, Guid>(model.PhoneTypeId.Value, out resultStatusMessage, "PhoneTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            return null;
        }

        /// <summary>
        /// The is the phone the best.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="result">
        /// The result.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasThePhoneTheBest(Guid leadId, Guid activeStatusId, out Guid result)
        {
            result = this.PhoneRepository.Get(x => x.LeadId == leadId && x.IsBest && x.StatusId == activeStatusId).Select(phone => phone.LeadPhoneId).FirstOrDefault();
            return result != Guid.Empty;
        }


        /// <summary>
        /// Does this lead have emails shown on lead info page.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool HasPhonesShownOnLeadPage(Guid leadId, Guid activeStatusId, out List<Guid> results)
        {

            results = this.PhoneRepository.Get(x => x.LeadId == leadId && !x.IsBest && x.IsShowOnLeadPage && x.StatusId == activeStatusId).Select(phone => phone.LeadPhoneId).ToList();
            return results.Count > 0;
        }

        /// <summary>
        /// The get phone position.
        /// </summary>
        /// <param name="leadId">
        /// The lead id.
        /// </param>
        /// <param name="activeStatusId">
        /// The active status id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int GetPhonePosition(Guid leadId, Guid activeStatusId)
        {
            var lastPhone = this.PhoneRepository.Get(x => x.LeadId == leadId && x.StatusId == activeStatusId)
                .OrderByDescending(x => x.Position).FirstOrDefault();
            if (lastPhone != null)
            {
                return lastPhone.Position + 1;
            }

            return 0;
        }


        /// <summary>
        /// The get best.
        /// </summary>
        /// <param name="studentEnrollmentId">
        /// The student enrollment id.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetBest(Guid studentEnrollmentId)
        {
            string best = string.Empty;


            var activeStatusId = this.statusesService.GetActiveStatusId().Result;

            var leadId = this.EnrollmentRepository.Get(x => x.StuEnrollId == studentEnrollmentId).Select(e => e.LeadId)
                .FirstOrDefault();

            if (leadId.HasValue)
            {
                best = this.PhoneRepository.Get(e => e.LeadId == leadId && e.IsShowOnLeadPage && e.StatusId == activeStatusId).Select(e => e.Phone).FirstOrDefault();

                // if haven't got value for best phone yet, try with is best
                if (best.IsNullOrEmpty())
                {
                    best = this.PhoneRepository.Get(e => e.LeadId == leadId && e.IsBest && e.StatusId == activeStatusId).Select(e => e.Phone).FirstOrDefault();
                }
            }

            return best;
        }
    }
}

