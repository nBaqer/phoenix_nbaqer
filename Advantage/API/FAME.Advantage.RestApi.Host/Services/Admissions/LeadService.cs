﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadService.cs" company="FAME Inc.">
//   FAME Inc. YYYY
// </copyright>
// <summary>
//   Defines the LeadService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace FAME.Advantage.RestApi.Host.Services.Admissions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    using AutoMapper;

    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Interfaces;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Services.Interfaces;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Admissions;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.Common;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.SystemCatalog;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;

    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;

    using Constants = FAME.Orm.Advantage.Domain.Common.Constants;

    /// <summary>
    /// The lead service.
    /// </summary>
    public class LeadService : ILeadService, IValidationService<Lead>
    {
        /// <summary>
        /// The mapper is used to map the Enrollment Entities with Business objects.
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// The unit of work.
        /// </summary>
        private readonly IAdvantageDbContext unitOfWork;

        /// <summary>
        /// The status service.
        /// </summary>
        private readonly IStatusesService statusService;

        /// <summary>
        /// The status codes service.
        /// </summary>
        private readonly IStatusCodesService statusCodesService;

        /// <summary>
        /// The source category service.
        /// </summary>
        private readonly ISourceCategoryService sourceCategoryService;

        /// <summary>
        /// The object validation service.
        /// </summary>
        private readonly IObjectValidationService objectValidationService;

        /// <summary>
        /// The service builder.
        /// </summary>
        private readonly IServiceBuilder serviceBuilder;

        /// <summary>
        /// The lead phone service.
        /// </summary>
        private ILeadsPhoneService leadPhoneService;

        /// <summary>
        /// The lead email service.
        /// </summary>
        private ILeadsEmailService leadEmailService;

        /// <summary>
        /// The lead address service.
        /// </summary>
        private ILeadsAddressService leadAddressService;

        /// <summary>
        /// The campus service.
        /// </summary>
        private ICampusService campusService;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeadService"/> class.
        /// </summary>
        /// <param name="unitOfWork">
        /// The unit of work.
        /// </param>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        /// <param name="statusService">
        /// The status Service.
        /// </param>
        /// <param name="statusCodesService">
        /// The status Codes Service.
        /// </param>
        /// <param name="sourceCategoryService">
        /// The source Category Service.
        /// </param>
        /// <param name="objectValidationService">
        /// The object Validation Service.
        /// </param>
        /// <param name="serviceBuilder">
        /// The service Builder.
        /// </param>
        /// <param name="campusService">
        /// The campus Service.
        /// </param>
        public LeadService(IAdvantageDbContext unitOfWork, IMapper mapper, IStatusesService statusService, IStatusCodesService statusCodesService, ISourceCategoryService sourceCategoryService, IObjectValidationService objectValidationService, IServiceBuilder serviceBuilder, ICampusService campusService)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.statusService = statusService;
            this.statusCodesService = statusCodesService;
            this.sourceCategoryService = sourceCategoryService;
            this.objectValidationService = objectValidationService;
            this.serviceBuilder = serviceBuilder;
            this.campusService = campusService;
        }

        /// <summary>
        /// The lead repository.
        /// </summary>
        private IRepository<AdLeads> LeadRepository => this.unitOfWork.GetRepositoryForEntity<AdLeads>();

        /// <summary>
        /// The lead duplicates repository.
        /// </summary>
        private IRepository<AdLeadDuplicates> LeadDuplicatesRepository => this.unitOfWork.GetRepositoryForEntity<AdLeadDuplicates>();

        /// <summary>
        /// The cache key first segment.
        /// </summary>
        private string CacheKeyFirstSegment => this.unitOfWork.GetSimpleCacheConnectionString();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// ///  <param name="fromIntegration">
        /// The optional parameter to not validate the email.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<Lead> Create(Lead model, bool fromIntegration = false)
        {
            this.leadPhoneService = this.serviceBuilder.GetServiceInstance<ILeadsPhoneService>();
            this.leadEmailService = this.serviceBuilder.GetServiceInstance<ILeadsEmailService>();
            this.leadAddressService = this.serviceBuilder.GetServiceInstance<ILeadsAddressService>();

            var user = this.unitOfWork.GetTenantUser();
            var result = new Lead();
            try
            {
                var leadStatusId = Guid.Empty;
                // skip validation if its coming from Afaintegration
                if (!fromIntegration)
                {
                    // before anything else, validate ids
                    var validatedModel = this.ValidateModelIds(model);

                    // if model is not null, then it does have some sort of issue and a message
                    if (validatedModel != null)
                    {
                        return validatedModel;
                    }
                }
                

                if (model.LeadStatusId.HasValue && model.LeadStatusId != Guid.Empty)
                {

                    var exists = await this.statusCodesService.Exists(
                        model.LeadStatusId.Value,
                        SystemStatusLevel.Lead,
                        model.CampusId);
                    if (exists)
                    {
                        leadStatusId = model.LeadStatusId.Value;
                    }
                    else
                    {
                        result.ResultStatusMessage = ApiMsgs.LeadStatusNotFoundForCampus;
                        result.ResultStatus = Enums.ResultStatus.NotFound;
                        return result;
                    }

                    // check if lead status id is an enrolled status
                    if (await this.statusCodesService.IsSystemStatus(leadStatusId, Constants.SystemStatus.Enrolled))
                    {
                        result.ResultStatusMessage = ApiMsgs.LeadStatusCannotBeEnrolled;
                        result.ResultStatus = Enums.ResultStatus.Error;
                        return result;
                    }
                }

                // supplied lead status id was empty so set to imported
                if (leadStatusId == Guid.Empty)
                {
                    leadStatusId = await this.statusCodesService.GetStatusCodeIdByName(LeadStatus.Imported, SystemStatusLevel.Lead);
                }

                if (leadStatusId == Guid.Empty)
                {
                    result.ResultStatusMessage = ApiMsgs.ActiveImportStatusNotFound;
                    result.ResultStatus = Enums.ResultStatus.Error;
                    return result;
                }
                var activeStatusCodeId = await this.statusService.GetActiveStatusId();

                if (activeStatusCodeId == Guid.Empty)
                {
                    result.ResultStatusMessage = ApiMsgs.ActiveStatusNotFound;
                    result.ResultStatus = Enums.ResultStatus.NotFound;
                    return result;
                }

                if (leadStatusId != Guid.Empty && activeStatusCodeId != Guid.Empty)
                {

                    AdLeads leadEntity = new AdLeads();
                    leadEntity.LeadId = Guid.NewGuid();
                    leadEntity.CampusId = model.CampusId;
                    leadEntity.LeadStatus = leadStatusId;
                    leadEntity.AdmissionsRep = model.AdmissionsRepId;
                    leadEntity.SourceCategoryId = model.SourceCategoryId;
                    leadEntity.SourceTypeId = model.SourceTypeId;
                    leadEntity.BirthDate = model.BirthDate;
                    leadEntity.CreatedDate = DateTime.Now;
                    leadEntity.DateApplied = model.DateApplied;
                    leadEntity.FirstName = model.FirstName.ToPascalCase();
                    leadEntity.LastName = model.LastName.ToPascalCase();
                    leadEntity.MiddleName = model.MiddleName.ToPascalCase();
                    leadEntity.AreaId = model.InterestAreaId;
                    leadEntity.Ssn = model.SSN;
                    leadEntity.StudentStatusId = activeStatusCodeId;
                    leadEntity.StudentId = Guid.Empty;
                    leadEntity.ModUser = user.Email.TrimEventIfNull();
                    leadEntity.AfaStudentId = model.AfaStudentId;
                    leadEntity.Citizen = model.CitizenShipStatus;
                    leadEntity.DrivLicNumber = model.DriverLicenseNumber;
                    leadEntity.DrivLicStateId = model.LicenseState;
                    leadEntity.MaritalStatus = model.MaritalStatus;
                    leadEntity.Gender = model.Gender;
                    if (user.UserType == UserTypes.Vendor)
                    {
                        leadEntity.VendorId = user.Id;
                    }

                    var entityCreated = await this.LeadRepository.CreateAsync(leadEntity);

                    await this.LeadRepository.SaveAsync();

                    /*
                     * TODO put the lead output model here inside the cache so it can be reused on multiple services like Phone, EmailAddress or Address service.
                     */

                    var leadEmail = new EmailAddress();
                    var leadPhone = new PhoneNumber();
                    var leadAddress = new Address();

                    if (entityCreated != null)
                    {
                        if (!model.Email.IsNullOrEmpty() && model.EmailTypeId.HasValue && model.EmailTypeId != Guid.Empty)
                        {
                            // add to email leads
                            leadEmail =
                               await this.leadEmailService.Create(new EmailAddress()
                               {
                                   Email = model.Email.TrimEventIfNull(),
                                   LeadId = entityCreated.LeadId,
                                   EmailTypeId = model.EmailTypeId.Value,
                                   IsPreferred = true,
                                   IsPortalUserName = false,
                                   ModifiedDate = DateTime.Now,
                                   StatusId = activeStatusCodeId,
                                   IsShowOnLeadPage = true,
                                   ModifiedUser = user.Email.TrimEventIfNull()
                               });
                        }

                        if (!model.Phone.IsNullOrEmpty() && model.PhoneTypeId.HasValue && model.PhoneTypeId != Guid.Empty)
                        {
                            leadPhone =
                            await this.leadPhoneService.Create(new PhoneNumber()
                            {
                                LeadId = entityCreated.LeadId,
                                Phone = model.Phone,
                                PhoneTypeId = model.PhoneTypeId.Value,
                                Position = 1,
                                IsBest = true,
                                IsForeignPhone = !model.IsUsaPhoneNumber,
                                StatusId = activeStatusCodeId,
                                IsShowOnLeadPage = true
                            });
                        }

                        if (!model.Address1.IsNullOrEmpty() && model.AddressTypeId.HasValue && model.AddressTypeId != Guid.Empty)
                        {
                            // add to address leads
                            leadAddress =
                            await this.leadAddressService.Create(new Address()
                            {
                                Address1 = model.Address1.TrimEventIfNull(),
                                LeadId = entityCreated.LeadId,
                                AddressTypeId = model.AddressTypeId.Value,
                                Address2 = model.Address2.TrimEventIfNull(),
                                City = model.City.TrimEventIfNull(),
                                StateId = model.StateId,
                                Zip = model.Zip.TrimEventIfNull(),
                                CountryId = model.CountryId,
                                CountyId = model.CountyId,
                                IsMailingAddress = true,
                                IsInternational = !model.IsUsaPhoneNumber,
                                ModifiedDate = DateTime.Now,
                                StatusId = activeStatusCodeId,
                                IsShowOnLeadPage = true,
                                ModifiedUser = user.Email
                            });
                        }
                    }


                    try
                    {
                        this.unitOfWork.SaveChanges();
                        if (entityCreated != null)
                        {
                            var entity = this.LeadRepository.Get(lead => lead.LeadId == entityCreated.LeadId)
                                .Include(lead => lead.AdLeadEmail).ThenInclude(em => em.Status)
                                .Include(lead => lead.AdLeadAddresses).ThenInclude(em => em.Status)
                                .Include(lead => lead.AdLeadPhone).ThenInclude(em => em.Status).FirstOrDefault();
                            if (entity != null)
                            {
                                result = this.mapper.Map<Lead>(entity);

                                result.IsUsaPhoneNumber = model.IsUsaPhoneNumber;
                                result.AddressTypeId = model.AddressTypeId;

                                result.AddressId = leadAddress?.Id;
                                result.PhoneId = leadPhone?.Id;
                                result.EmailId = leadEmail?.Id;
                            }
                        }

                        await this.ValidateDuplicates(result);
                        result.ResultStatusMessage = ApiMsgs.SAVE_SUCCESS;
                        result.ResultStatus = Enums.ResultStatus.Success;
                    }
                    catch (Exception ex)
                    {
                        if (entityCreated != null)
                        {
                            if (leadAddress.Id != null)
                            {
                                await this.leadAddressService.Delete(entityCreated.LeadId, leadAddress.Id.Value);
                            }

                            if (leadPhone.Id != null)
                            {
                                await this.leadPhoneService.Delete(entityCreated.LeadId, leadPhone.Id.Value);
                            }

                            if (leadEmail.Id != null)
                            {
                                await this.leadEmailService.Delete(entityCreated.LeadId, leadEmail.Id.Value);
                            }
                            this.unitOfWork.SaveChanges();

                            this.LeadRepository.Delete(this.LeadRepository.GetById(entityCreated.LeadId));

                            this.unitOfWork.SaveChanges();
                        }

                        result.LeadId = Guid.Empty;
                        result.LeadStatusId = Guid.Empty;
                        result.CreateDate = null;
                        result.ResultStatusMessage = ApiMsgs.SAVE_UNSUCCESSFULL;
                        result.ResultStatus = Enums.ResultStatus.Error;
                    }
                }
            }
            catch (Exception e)
            {
                e.TrackException();
                result.ResultStatusMessage = ApiMsgs.SAVE_UNSUCCESSFULL;
                result.ResultStatus = Enums.ResultStatus.Error;
            }

            if (result?.LeadId != null)
            {
                SimpleCache.Remove($"{this.CacheKeyFirstSegment}:GetLeadById:{result.LeadId}");
            }

            return result;
        }

        /// <summary>
        /// The get lead that's not enrolled
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<DataTransferObjects.Admissions.Lead.V2.Lead> Get(Guid id)
        {
            if (id == Guid.Empty)
            {
                var lead = new DataTransferObjects.Admissions.Lead.V2.Lead
                {
                    ResultStatusMessage = ApiMsgs.MALFORMED_MODEL_ERROR,
                    ResultStatus = Enums.ResultStatus.Error
                };
                return lead;
            }
            else
            {
                return await Task.Run(() =>
                   {
                       return SimpleCache.GetOrAddExisting(
                           $"{this.CacheKeyFirstSegment}:GetLeadById:{id}",
                          async () =>
                               {
                                   AdLeads lead = this.LeadRepository.Get(x => x.LeadId == id).Include(x => x.AdLeadEmail)
                            .ThenInclude(x => x.Status).Include(x => x.AdLeadPhone).ThenInclude(x => x.Status)
                            .Include(x => x.AdLeadAddresses).ThenInclude(x => x.Status)
                            .Include(x => x.Campus).ThenInclude(x => x.SyCampGrps).FirstOrDefault();

                                   if (lead != null)
                                   {
                                       var enrolledLeadStatuses = await this.statusCodesService.GetAllStatusCodesIdByName(
                                                                      LeadStatus.Enrolled,
                                                                      SystemStatusLevel.Lead);

                                       if (enrolledLeadStatuses.Contains(lead.LeadStatus))
                                       {
                                           var model = new DataTransferObjects.Admissions.Lead.V2.Lead()
                                           {
                                               ResultStatusMessage =
                                                                   ApiMsgs
                                                                       .LeadIsEnrolled,
                                               ResultStatus =
                                                                   Enums.ResultStatus
                                                                       .Error
                                           };
                                           return model;
                                       }
                                   }

                                   if (lead == null)
                                   {
                                       var model = new DataTransferObjects.Admissions.Lead.V2.Lead()
                                       {
                                           ResultStatusMessage = ApiMsgs.NOT_FOUND,
                                           ResultStatus = Enums.ResultStatus.NotFound
                                       };
                                       return model;
                                   }

                                   var result = this.mapper.Map<DataTransferObjects.Admissions.Lead.V2.Lead>(lead);
                                   return result;
                               },
                           CacheExpiration.VERY_SHORT);
                   });
            }
        }

        /// <summary>
        /// The update allows you to update an existing lead in advantage. Required fields such as First Name, Last Name are validated ahead of time.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <remarks>
        /// The update requires a <see cref="LeadBase"/> object with at least first name, last name, phone, phone type and/or email and email type ir order to be successfully updated in advantage. If any duplicate by the update is found, then the record will be updated and marked as duplicate.
        /// Changing the Lead last name:
        /// ** If the last name is updated, the api will return you on the response a ResultStatus of "Unable to update Last Name, an official name change was not provided", if the IsOfficialNameChange field is null or empty. Setting the IsOfficialNameChange to Yes will track Prior Last Name.
        /// ** Setting the IsOfficialNameChange to  No will still update the Last Name field but not update the Prior Last name field
        /// Updating the email:
        /// ** To update an email use the Lead/{leadId}/EmailAddress/{id}/Put action
        /// Updating the phone:
        /// ** To update a phone use the Lead/{leadId}/Phone/{id}/Put action
        /// Updating the address:
        /// ** To update an address use the Lead/{leadId}/Address/{id}/Put action
        /// </remarks>
        /// <returns>
        /// The <see cref="LeadBase"/>.
        /// </returns>
        public async Task<LeadBase> Update(LeadBase model)
        {
            // before anything else, validate ids
            var validatedModel = this.ValidateBaseModelIds(model);

            // if model is not null, then it does have some sort of issue and a message
            if (validatedModel != null)
            {
                return validatedModel;
            }
            this.leadPhoneService = this.serviceBuilder.GetServiceInstance<ILeadsPhoneService>();

            var user = this.unitOfWork.GetTenantUser();
            var leadStatusEnrolled = await this.statusCodesService.GetStatusCodeIdByName(LeadStatus.Enrolled, SystemStatusLevel.Lead);

            return await Task.Run(
                       async () =>
                       {
                           var model1 = model;
                           var lead = this.LeadRepository.Get(x => x.LeadId == model1.LeadId && x.StudentId == Guid.Empty)
                               .Include(x => x.AdLeadAddresses)
                               .ThenInclude(x => x.Status)
                               .Include(x => x.AdLeadEmail)
                               .ThenInclude(x => x.Status)
                               .Include(x => x.AdLeadPhone)
                               .ThenInclude(x => x.Status)
                               .FirstOrDefault();

                           if (lead == null)
                           {
                               model.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                               model.ResultStatus = Enums.ResultStatus.Error;
                               return model;
                           }
                           else
                           {
                               SimpleCache.Remove($"{this.CacheKeyFirstSegment}:GetLeadById:{model.LeadId}");
                               if (lead.LeadStatus == leadStatusEnrolled)
                               {
                                   model.ResultStatusMessage = ApiMsgs.UNABLE_TO_UPDATE_LEAD_ENROLLED;
                                   model.ResultStatus = Enums.ResultStatus.Error;
                                   return model;
                               }

                               if ((!string.Equals(lead.LastName.TrimEventIfNull(), model.LastName.TrimEventIfNull(), StringComparison.CurrentCultureIgnoreCase)) && model.IsOfficialNameChange == null)
                               {
                                   model.ResultStatusMessage = ApiMsgs.SAVE_UNSUCCESSFUL_OFFICIAL_NAME_NOT_PROVIDED;
                                   model.ResultStatus = Enums.ResultStatus.Error;
                                   return model;
                               }

                               var leadStatusId = Guid.Empty;

                               if (model.LeadStatusId != null)
                               {
                                   var exists = await this.statusCodesService.Exists(
                                                    model.LeadStatusId.Value,
                                                    SystemStatusLevel.Lead,
                                                    model.CampusId);
                                   if (exists)
                                   {
                                       leadStatusId = model.LeadStatusId.Value;
                                   }
                                   else
                                   {
                                       model.ResultStatusMessage = ApiMsgs.LeadStatusNotFoundForCampus;
                                       model.ResultStatus = Enums.ResultStatus.NotFound;
                                       return model;
                                   }

                                   // check if lead status id is an enrolled status
                                   if (await this.statusCodesService.IsSystemStatus(leadStatusId, Constants.SystemStatus.Enrolled))
                                   {
                                       model.ResultStatusMessage = ApiMsgs.LeadStatusCannotBeEnrolled;
                                       model.ResultStatus = Enums.ResultStatus.Error;
                                       return model;
                                   }
                               }

                               if (leadStatusId == Guid.Empty)
                               {
                                   leadStatusId = lead.LeadStatus;
                               }
                               if (model.SourceTypeId.HasValue)
                               {
                                   if (!model.SourceCategoryId.HasValue)
                                   {
                                       model.ResultStatusMessage = ApiMsgs.SourceCategoryNotSupplied;
                                       return model;
                                   }

                                   if (!await this.sourceCategoryService.HasSourceType(model.SourceCategoryId.Value, model.SourceTypeId.Value, model.CampusId))
                                   {
                                       model.ResultStatusMessage = ApiMsgs.SourceTypeDoesNotBelongToSourceCategory;
                                       return model;
                                   }

                                   lead.SourceTypeId = model.SourceTypeId;

                               }
                               if (model.IsOfficialNameChange != null)
                               {
                                   if (!string.Equals(lead.LastName.TrimEventIfNull(), model.LastName.TrimEventIfNull(), StringComparison.CurrentCultureIgnoreCase) && model.IsOfficialNameChange.Value)
                                   {
                                       /* Update the Last Name with tracking */
                                       var leadTrackLastNameHistoryRep = this.unitOfWork.GetRepositoryForEntity<AdLastNameHistory>();
                                       AdLastNameHistory lastNameHistory = new AdLastNameHistory();
                                       lastNameHistory.LastName = lead.LastName.ToPascalCase();
                                       lastNameHistory.LeadId = lead.LeadId;
                                       lastNameHistory.ModDate = DateTime.Now;
                                       lastNameHistory.ModUser = user.Email;

                                       await leadTrackLastNameHistoryRep.CreateAsync(lastNameHistory);
                                       lead.LastName = model.LastName.TrimEventIfNull().ToPascalCase();
                                   }
                                   else
                                   {
                                       lead.LastName = model.LastName.TrimEventIfNull().ToPascalCase();
                                   }
                               }


                               lead.FirstName = model.FirstName.TrimEventIfNull().ToPascalCase();

                               if (model.MiddleName != null)
                               {
                                   lead.MiddleName = model.MiddleName.TrimEventIfNull().ToPascalCase();
                               }
                               if (model.SSN != null)
                               {
                                   lead.Ssn = model.SSN.TrimEventIfNull();
                               }

                               if (model.DateApplied.HasValue)
                               {
                                   lead.DateApplied = model.DateApplied;
                               }

                               if (model.InterestAreaId.HasValue)
                               {
                                   lead.AreaId = model.InterestAreaId;
                               }

                               if (model.BirthDate.HasValue)
                               {
                                   lead.BirthDate = model.BirthDate;
                               }
                               if (model.AdmissionsRepId.HasValue)
                               {
                                   lead.AdmissionsRep = model.AdmissionsRepId;
                               }

                               lead.LeadStatus = leadStatusId;

                               if (model.SourceCategoryId.HasValue)
                               {
                                   lead.SourceCategoryId = model.SourceCategoryId;
                               }

                               lead.ModDate = DateTime.Now;
                               lead.ModUser = user.Email;
                               if (model.CampusId.HasValue)
                               {
                                   lead.CampusId = model.CampusId;
                               }
                               lead.AfaStudentId = model.AfaStudentId;
                               this.LeadRepository.Update(lead);
                           }

                           this.ValidateDuplicatesBase(model, out Expression<Func<AdLeads, bool>> filter, true);

                           this.unitOfWork.SaveChanges();

                           lead = this.LeadRepository.Get(x => x.LeadId == model1.LeadId && x.StudentId == Guid.Empty && x.LeadStatus != leadStatusEnrolled)
                               .Include(x => x.AdLeadAddresses)
                               .ThenInclude(x => x.Status)
                               .Include(x => x.AdLeadEmail)
                               .ThenInclude(x => x.Status)
                               .Include(x => x.AdLeadPhone)
                               .ThenInclude(x => x.Status)
                               .FirstOrDefault();

                           return this.mapper.Map<LeadBase>(lead);
                       });
        }

        /// <summary>
        /// The update AFA student id.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<LeadBase> UpdateAfaStudentId(LeadBase model)
        {
            return await Task.Run(
                       async () =>
                           {
                               var model1 = model;
                               var lead = this.LeadRepository.Get(x => x.LeadId == model1.LeadId).FirstOrDefault();

                               if (lead == null)
                               {
                                   model.ResultStatusMessage = ApiMsgs.NOT_FOUND;
                                   model.ResultStatus = Enums.ResultStatus.Error;
                                   return model;
                               }
                               else
                               {
                                   lead.ModDate = DateTime.Now;
                                   lead.AfaStudentId = model.AfaStudentId;
                                   var user = this.unitOfWork.GetTenantUser();
                                   lead.ModUser = user.Email.TrimEventIfNull();
                                   this.LeadRepository.Update(lead);
                                   this.unitOfWork.SaveChanges();
                                   lead = this.LeadRepository.Get(x => x.LeadId == model1.LeadId).FirstOrDefault();

                                   return this.mapper.Map<LeadBase>(lead);
                               }
                           });
        }

        /// <summary>
        /// The validate duplicates base.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <param name="filter">
        /// The filter.
        /// </param>
        /// <param name="saveToDb">
        /// The save to db.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateDuplicatesBase(LeadBase model, out Expression<Func<AdLeads, bool>> filter, bool saveToDb = false)
        {
            filter = null;
            var result = false;
            if (!model.SSN.IsNullOrEmpty())
            {
                filter = lead =>
                    string.Equals(model.SSN != null ? model.SSN.TrimEventIfNull() : string.Empty, lead.Ssn ?? lead.Ssn.TrimEventIfNull());
                result = true;
            }

            if (saveToDb && result)
            {
                Expression<Func<AdLeads, bool>> query = lead => lead.LeadId != model.LeadId;
                query = query.And(filter);

                var possibleDuplicates = this.LeadRepository
                    .Get(query)
                    .ToList();

                var duplicateLeads = new List<AdLeadDuplicates>();
                foreach (var dupe in possibleDuplicates)
                {
                    if (model.LeadId != null)
                    {
                        var duplicate =
                            new AdLeadDuplicates() { NewLeadGuid = model.LeadId.Value, PosibleDuplicateGuid = dupe.LeadId };
                        duplicateLeads.Add(duplicate);
                    }
                }

                this.LeadDuplicatesRepository.Create(duplicateLeads);
                this.LeadDuplicatesRepository.Save();
            }

            return result;
        }

        /// <summary>
        /// The validate duplicates.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task ValidateDuplicates(Lead model)
        {
            Expression<Func<AdLeads, bool>> filter = null;
            Expression<Func<AdLeads, bool>> query = lead => lead.LeadId != model.LeadId;

            LeadBase lb = this.mapper.Map<Lead>(model);

            // if model has ssn then check for matching ssn, first name, and last name
            if (!this.ValidateDuplicatesBase(lb, out filter, false))
            {
                // if it does not have SSN then check for match first name, last name, and ( email or phone )
                filter = lead => model.FirstName.ToLower().TrimEventIfNull() == lead.FirstName.ToLower().TrimEventIfNull()
                                && model.LastName.ToLower().TrimEventIfNull() == lead.LastName.ToLower().TrimEventIfNull() &&
                    ((!model.Phone.IsNullOrEmpty() && lead.AdLeadPhone.Any(
                          lphone => !lphone.Phone.IsNullOrEmpty() && lphone.Phone.TrimEventIfNull() == model.Phone.TrimEventIfNull()))
                     || (!model.Email.IsNullOrEmpty() && lead.AdLeadEmail.Any(
                             lemail => !lemail.Email.IsNullOrEmpty() && lemail.Email.TrimEventIfNull() == model.Email.TrimEventIfNull())));
            }

            query = query.And(filter);

            var possibleDuplicates = this.LeadRepository
                .Get(query)
                .ToList();

            var duplicateLeads = new List<AdLeadDuplicates>();
            foreach (var dupe in possibleDuplicates)
            {
                if (model.LeadId != null)
                {
                    var duplicate =
                        new AdLeadDuplicates() { NewLeadGuid = model.LeadId.Value, PosibleDuplicateGuid = dupe.LeadId };
                    duplicateLeads.Add(duplicate);
                }
            }

            this.LeadDuplicatesRepository.Create(duplicateLeads);
            await this.LeadDuplicatesRepository.SaveAsync();
        }

        /// <summary>
        /// The validate base model ids.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Lead"/>.
        /// </returns>
        public Lead ValidateBaseModelIds(LeadBase model)
        {

            var result = new Lead();

            // validate lead Id
            if (model.LeadId.HasValue && model.LeadId != Guid.Empty)
            {
                var lead = this.LeadRepository.Get(l => l.LeadId == model.LeadId)
                    .FirstOrDefault();

                if (lead == null)
                {
                    result.ResultStatusMessage = "Lead Id does not exist";
                    result.ResultStatus = Enums.ResultStatus.NotFound;
                    return result;
                }
            }

            // if anything happenes, message will be always be not found
            result.ResultStatus = Enums.ResultStatus.NotFound;

            string resultStatusMessage = string.Empty;

            // validate admission rep type 
            if (model.AdmissionsRepId.HasValue && model.AdmissionsRepId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyUsers, Guid>(model.AdmissionsRepId.Value, out resultStatusMessage, "AdmissionsRepId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate lead status type 
            if (model.LeadStatusId.HasValue && model.LeadStatusId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyStatusCodes, Guid>(model.LeadStatusId.Value, out resultStatusMessage, "LeadStatusId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate lead status type 
            if (model.StudentStatusId.HasValue && model.StudentStatusId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyStatuses, Guid>(model.StudentStatusId.Value, out resultStatusMessage, "StudentStatusId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate enrolled state
            if (model.EnrollStateId.HasValue && model.EnrollStateId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyStates, Guid>(model.EnrollStateId.Value, out resultStatusMessage, "EnrollStateId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate vendor  
            if (model.VendorId.HasValue && model.VendorId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyUsers, Guid>(model.VendorId.Value, out resultStatusMessage, "VendorId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate source category
            if (model.SourceCategoryId.HasValue && model.SourceCategoryId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdSourceCatagory, Guid>(model.SourceCategoryId.Value, out resultStatusMessage, "SourceCategoryId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate source type
            if (model.SourceTypeId.HasValue && model.SourceTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdSourceType, Guid>(model.SourceTypeId.Value, out resultStatusMessage, "SourceTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // return no errors
            return null;
        }

        /// <summary>
        /// The validate model ids.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Lead"/>.
        /// </returns>
        public Lead ValidateModelIds(Lead model)
        {
            LeadBase lb = this.mapper.Map<Lead>(model);
            var result = this.ValidateBaseModelIds(lb);

            if (result != null)
            {
                return result;
            }

            result = new Lead();
            string resultStatusMessage = string.Empty;

            // validate phone type 
            if (model.PhoneTypeId.HasValue && model.PhoneTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyPhoneType, Guid>(model.PhoneTypeId.Value, out resultStatusMessage, "PhoneTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate email type 
            if (model.EmailTypeId.HasValue && model.EmailTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyEmailType, Guid>(model.EmailTypeId.Value, out resultStatusMessage, "EmailTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate address type 
            if (model.AddressTypeId.HasValue && model.AddressTypeId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<PlAddressTypes, Guid>(model.AddressTypeId.Value, out resultStatusMessage, "AddressTypeId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate state type 
            if (model.StateId.HasValue && model.StateId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<SyStates, Guid>(model.StateId.Value, out resultStatusMessage, "StateId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate county type 
            if (model.CountyId.HasValue && model.CountyId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdCounties, Guid>(model.CountyId.Value, out resultStatusMessage, "CountyId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }

            // validate country type 
            if (model.CountryId.HasValue && model.CountryId != Guid.Empty && !this.objectValidationService.ValidateObjectExist<AdCountries, Guid>(model.CountryId.Value, out resultStatusMessage, "CountryId"))
            {
                result.ResultStatusMessage = resultStatusMessage;
                return result;
            }


            // return no errors
            return null;
        }

        /// <summary>
        /// The get lead by student id.
        /// This function will hold the lead that was found on the cache for a <see cref="CacheExpiration.VERY_SHORT"/> period of time.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<AdLeads> GetByStudentId(Guid id)
        {
            return await Task.Run(
                       () =>
                           {
                               return SimpleCache.GetOrAddExisting(
                                   $"{this.CacheKeyFirstSegment}:GetByStudentId:{id}",
                                   () =>
                                       {
                                           return this.LeadRepository.Get(x => x.StudentId == id)
                                               .Include(x => x.AdLeadEmail).ThenInclude(x => x.Status)
                                               .Include(x => x.AdLeadPhone).ThenInclude(x => x.Status)
                                               .Include(x => x.AdLeadAddresses).ThenInclude(x => x.Status)
                                               .FirstOrDefault();
                                       },
                                   CacheExpiration.VERY_SHORT);
                           });
        }

        /// <summary>
        /// The check if AFA student id exists.
        /// This is to check if lead exists or not using the names and SSN for the given CMS id campus.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<AfaStudentExist> CheckIfAfaStudentIdExists(LeadBase model)
        {
            return await Task.Run(() =>
            {
                AfaStudentExist result = new AfaStudentExist();
                Guid campusId = this.campusService.GetCampusIdForCmsId(model.CmsId);
                
                // first check if afa student id exists if yes return else move forward
                var afaLead = this.LeadRepository.Get(l => l.AfaStudentId == model.AfaStudentId).FirstOrDefault();
                if (afaLead != null)
                {
                    // got the afastudentId.. stop and return successful
                    result.AfaStudentId = afaLead.AfaStudentId;
                    result.LeadId = afaLead.LeadId;
                    result.AfaStudentIdExists = true;
                    result.ResultStatusMessage = ApiMsgs.AfaStudentIdExists;
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.FirstName)) // this check was for the race condition
                    {
                        var lead = this.LeadRepository.Get(
                            l => //l.FirstName.TrimEventIfNull().ToLower() == model.FirstName.TrimEventIfNull().ToLower()
                                // && l.LastName.TrimEventIfNull().ToLower() == model.LastName.TrimEventIfNull().ToLower() &&
                                l.Ssn.TrimEventIfNull() == model.SSN.TrimEventIfNull()
                                 && l.CampusId == campusId);
                        if (lead.Any())
                        {
                            if (lead.Count() == 1)
                            {
                                // found exact match and no duplicates
                                // return AfaStudentIdNotFoundButLeadFound
                                result.ResultStatus = Enums.ResultStatus.Success;
                                var firstOrDefault = lead.FirstOrDefault();
                                if (firstOrDefault != null)
                                {
                                    result.LeadId = firstOrDefault.LeadId;
                                }

                                var stuId = lead.FirstOrDefault(l => l.AfaStudentId != null);
                                if (stuId != null)
                                {
                                    result.AfaStudentId = stuId.AfaStudentId;
                                    result.AfaStudentIdExists = true;
                                    result.ResultStatusMessage = ApiMsgs.AfaStudentIdExists;
                                }
                                else
                                {
                                    result.AfaStudentId = model.AfaStudentId;
                                    result.AfaStudentIdExists = false;
                                    result.ResultStatusMessage = ApiMsgs.AfaStudentIdNotFoundButLeadFound;
                                }
                            }
                            else
                            {
                                // return that duplicate lead exists return the most recent lead id
                                result.ResultStatus = Enums.ResultStatus.Success;
                                result.ResultStatusMessage = ApiMsgs.DuplicateLeadExists;
                                result.AfaStudentIdExists = false;
                                result.AfaStudentId = model.AfaStudentId;
                                lead = lead.OrderBy(l => l.ModDate);
                                result.LeadId = lead.LastOrDefault().LeadId;
                            }
                        }
                        else
                        {
                            // check for the leads with no ssn and only first name and last name
                            // still if the lead not found then send lead not found else send lead found with no ssn
                            var noSsn = this.LeadRepository.Get(
                                l => l.FirstName.TrimEventIfNull().ToLower()
                                     == model.FirstName.TrimEventIfNull().ToLower()
                                     && l.LastName.TrimEventIfNull().ToLower()
                                     == model.LastName.TrimEventIfNull().ToLower() && l.CampusId == campusId); // && (l.Ssn.TrimEventIfNull() == null || l.Ssn.TrimEventIfNull() == string.Empty));
                            result.ResultStatus = Enums.ResultStatus.Success;
                            if (noSsn != null)
                            {
                                if (noSsn.Any())
                                {
                                    if (noSsn.Count() == 1)
                                    {
                                        var ssnFirst = noSsn.FirstOrDefault();

                                        // check if the ssn  is not null if not null then no ssn else duplicate ssn
                                        if (!string.IsNullOrEmpty(ssnFirst.Ssn))
                                        {
                                            if (ssnFirst.Ssn != model.SSN)
                                            {
                                                // check if the ssnFirst Student has got afaId.. then create the lead as it may be a new student with same name... also insert in exception because it may be same student with just a ssn misstyped.
                                                if (noSsn.Any(l => l.AfaStudentId != null))
                                                {
                                                    // create the lead and have a new exception message 
                                                    result.LeadId = ssnFirst.LeadId;
                                                    result.AfaStudentId = model.AfaStudentId;
                                                    result.AfaStudentIdExists = false;
                                                    result.ResultStatusMessage = ApiMsgs.NewLeadCreatedWithDifferentAfaStudentId;
                                                }
                                                else
                                                {
                                                    result.LeadId = ssnFirst.LeadId;
                                                    result.AfaStudentId = model.AfaStudentId;
                                                    result.AfaStudentIdExists = false;
                                                    result.ResultStatusMessage = ApiMsgs.AfaStudentIdNotFoundButLeadFoundDiffSsn;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            result.LeadId = ssnFirst.LeadId;
                                            result.AfaStudentId = model.AfaStudentId;
                                            result.AfaStudentIdExists = false;
                                            result.ResultStatusMessage = ApiMsgs.AfaStudentIdNotFoundButLeadFoundNoSsn;
                                        }
                                    }
                                    else
                                    {
                                        // duplicate with no ssn
                                        result.AfaStudentId = model.AfaStudentId;
                                        result.AfaStudentIdExists = false;
                                        result.ResultStatusMessage = ApiMsgs.AfaStudentIdNotFoundButDuplicateLeadWithSsn;
                                    }
                                }
                                else
                                {
                                    // return that lead does not exists
                                    result.ResultStatus = Enums.ResultStatus.Success;
                                    result.ResultStatusMessage = ApiMsgs.LeadNotFound;
                                    result.AfaStudentIdExists = false;
                                    result.AfaStudentId = model.AfaStudentId;
                                }
                            }
                            else
                            {
                                // return that lead does not exists
                                result.ResultStatus = Enums.ResultStatus.Success;
                                result.ResultStatusMessage = ApiMsgs.LeadNotFound;
                                result.AfaStudentIdExists = false;
                                result.AfaStudentId = model.AfaStudentId;
                            }
                        }
                    }
                    
                }


                return result;
            });
        }

        /// <summary>
        /// The get page no.
        /// </summary>
        /// <param name="page">
        /// The page.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private static int GetPageNo(int page)
        {
            return page < 0 ? 0 : page;
        }
    }
}