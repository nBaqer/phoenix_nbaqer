﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using StructureMap.AspNetCore;

namespace FAME.Advantage.RestApi.Host
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseSetting("detailedErrors", "true")
                .UseIISIntegration()
                .UseStartup<Startup>()
                .CaptureStartupErrors(true)
                .UseApplicationInsights()
                .UseStructureMap()
                .Build();
            host.Run();
        }
    }
}
