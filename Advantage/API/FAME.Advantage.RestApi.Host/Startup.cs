﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Startup.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The startup.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host
{
    using AutoMapper;
    using AutoMapper.Configuration;
    using Fame.EFCore.Tenant;
    using Fame.EFCore.Tenant.Identity;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.Host.Infrastructure.Filters;
    using FAME.Advantage.RestApi.Host.Infrastructure.Initializer;
    using FAME.Advantage.RestApi.Host.Infrastructure.Middleware;
    using FAME.Advantage.RestApi.Host.Infrastructure.Settings;
    using FAME.Advantage.RestApi.Host.Infrastructure.Swagger;
    using FAME.Advantage.RestApi.Host.Mappings;
    using FAME.Extensions;
    using FAME.Extensions.Helpers;
    using FluentValidation.AspNetCore;
    using Microsoft.ApplicationInsights.AspNetCore;
    using Microsoft.ApplicationInsights.Extensibility;
    using Microsoft.ApplicationInsights.SnapshotCollector;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.PlatformAbstractions;
    using Microsoft.IdentityModel.Tokens;
    using StructureMap;
    using Swashbuckle.AspNetCore.Swagger;
    using System;
    using System.IO;
    using System.Text;

    /// <summary>
    /// The startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="env">
        /// The env.
        /// </param>
        public Startup(IHostingEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// The configure services.
        /// </summary>
        /// <param name="services">
        /// The services.
        /// </param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<TenantAuthDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<AuthServiceSettings>(this.Configuration.GetSection("AuthService"));
            services.Configure<TenantDbSettings>(this.Configuration.GetSection("TenantDb"));
            services.Configure<SimpleCacheSettings>(this.Configuration.GetSection("CacheSettings"));
            services.Configure<GoogleOAuthSettings>(this.Configuration.GetSection("GmailOauth"));

            services.AddAuthentication(
                options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    }).AddJwtBearer(options => options.TokenValidationParameters = this.ConfigureJwtBearerOptions().TokenValidationParameters);
            this.ConfigureAutoMapperService(services);

            services.AddMvc(AddFilters).AddXmlSerializerFormatters().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Configure SnapshotCollector from application settings
            services.Configure<SnapshotCollectorConfiguration>(Configuration.GetSection(nameof(SnapshotCollectorConfiguration)));

            // Add SnapshotCollector telemetry processor.
            services.AddSingleton<ITelemetryProcessorFactory>(sp => new SnapshotCollectorTelemetryProcessorFactory(sp));

            services.AddSingleton<ITelemetryInitializer, AppInsightsTelemetryInitializer>(); // app insights

            services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc(
                        SwashbuckleInfo.DOCUMENT_VERSION_V1,
                        new Info
                        {
                            Title = SwashbuckleInfo.DOCUMENT_TITLE,
                            Version = SwashbuckleInfo.DOCUMENT_VERSION_V1,
                            Description = SwashbuckleInfo.DOCUMENT_DESCRIPTION,
                            TermsOfService = SwashbuckleInfo.TERMS_OF_SERVICE,
                            Contact = new Contact
                            {
                                Email = SwashbuckleInfo.CONTACT_EMAIL,
                                Name = SwashbuckleInfo.CONTACT_NAME,
                                Url = SwashbuckleInfo.CONTACT_URL
                            }
                        });
                    c.IgnoreObsoleteActions();
                    c.OperationFilter<AddAuthorizationHeaderParameterOperationFilter>();
                    c.SchemaFilter<SwaggerSchemaFilter>();
                    // Set the comments path for the Swagger JSON and UI.
                    string basePath = PlatformServices.Default.Application.ApplicationBasePath;
                    string apiXmlPath = Path.Combine(basePath, @"FAME.Advantage.RestApi.Host.xml");
                    string dtoXmlPath = Path.Combine(basePath, @"FAME.Advantage.RestApi.DataTransferObjects.xml");

                    c.IncludeXmlComments(apiXmlPath);
                    c.IncludeXmlComments(dtoXmlPath);
                });
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        /// <param name="env">
        /// The env.
        /// </param>
        /// <param name="loggerFactory">
        /// The logger factory.
        /// </param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseAuthentication();
            env.EnvironmentName = EnvironmentName.Production;

            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/error");
            //}

            app.UseDeveloperExceptionPage();

            app.UseMiddleware(typeof(CustomExceptionHandlerMiddleware));
            app.UseMiddleware(typeof(RefreshTokenMiddleware));
            app.UseMvc();
            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            });

            app.UseSwaggerUI(c =>
            {
                string basePath = Environment.GetEnvironmentVariable("ASPNETCORE_APPL_PATH") ?? string.Empty;

                c.SwaggerEndpoint($"{basePath}/swagger/v1/swagger.json", "Advantage API V1");
                c.ShowRequestHeaders();
                c.ShowJsonEditor();
                c.DocExpansion("list");
                c.EnabledValidator(null);
            });

            this.ConfigureSimpleCache();

            //App Insights configuration
            TelemetryConfiguration tConfig = app.ApplicationServices.GetRequiredService<TelemetryConfiguration>();
            if (tConfig.InstrumentationKey == null || tConfig.InstrumentationKey.IsNullOrEmpty())
            {
                tConfig.DisableTelemetry = true;
            }
        }

        /// <summary>
        /// The configure container.
        /// </summary>
        /// <param name="registry">
        /// The registry.
        /// </param>
        public void ConfigureContainer(Registry registry)
        {
            registry.Scan(_ =>
            {
                _.AssemblyContainingType(typeof(Startup));
                _.WithDefaultConventions();
                _.LookForRegistries();
                _.AddAllTypesOf<IMappingDefinition>();
            });

            registry.For<IConfigurationRoot>().Use(this.Configuration);
        }

        /// <summary>
        /// The is api.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsApi(HttpContext context)
        {
            return context.Request.Path.Value.ToLower().StartsWith("/api");
        }

        /// <summary>
        /// The get signing key.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="SymmetricSecurityKey"/>.
        /// </returns>
        private static SymmetricSecurityKey GetSigningKey(string key)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key));
        }

        /// <summary>
        /// The add filters.
        /// </summary>
        /// <param name="options">
        /// The options.
        /// </param>
        private static void AddFilters(MvcOptions options)
        {
            options.Filters.Add(typeof(UnitOfWorkActionFilter));
        }

        /// <summary>
        /// The configure jwt bearer options.
        /// </summary>
        /// <returns>
        /// The <see cref="JwtBearerOptions"/>.
        /// </returns>
        private JwtBearerOptions ConfigureJwtBearerOptions()
        {
            AuthServiceSettings authConfigSection = this.Configuration.GetSection("AuthService").Get<AuthServiceSettings>();

            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSigningKey(authConfigSection.SecretKey),

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = authConfigSection.Issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = authConfigSection.Audience,

                // Validate the token expiry
                ValidateLifetime = true,
                RequireExpirationTime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero,

                ValidateActor = false,
            };

            return new JwtBearerOptions
            {

                TokenValidationParameters = tokenValidationParameters
            };
        }

        /// <summary>
        /// The configure simple cache.
        /// </summary>
        private void ConfigureSimpleCache()
        {
            SimpleCache.Enabled = this.Configuration.GetSection("CacheSettings")
                                        .Get<SimpleCacheSettings>().Enable;
        }

        /// <summary>
        /// The configure auto mapper service.
        /// </summary>
        /// <param name="services">
        /// The services.
        /// </param>
        private void ConfigureAutoMapperService(IServiceCollection services)
        {
            Container container = new Container(registry =>
            {
                registry.Scan(
                    scan =>
                    {
                        scan.AssemblyContainingType(typeof(Startup));
                        scan.AddAllTypesOf<IMappingDefinition>();
                    });
            });

            System.Collections.Generic.IEnumerable<IMappingDefinition> mappingDefinitions = container.GetAllInstances<IMappingDefinition>();
            MapperConfigurationExpression configurationExpression = new MapperConfigurationExpression();
            mappingDefinitions.Each(mappingDefinition => mappingDefinition.MapEntity(configurationExpression));

            MapperConfiguration configuration = new MapperConfiguration(configurationExpression);

            IMapper mapper = configuration.CreateMapper();
            services.AddScoped(typeof(IListItem<,>), typeof(ListItem<,>));
            services.AddSingleton(mapper);
        }


        /// <summary>
        /// Class defining processor factory for snapshot collector for application insights
        /// </summary>
        /// </param>
        private class SnapshotCollectorTelemetryProcessorFactory : ITelemetryProcessorFactory
        {
            private readonly IServiceProvider _serviceProvider;

            public SnapshotCollectorTelemetryProcessorFactory(IServiceProvider serviceProvider) =>
                _serviceProvider = serviceProvider;

            public ITelemetryProcessor Create(ITelemetryProcessor next)
            {
                var snapshotConfigurationOptions = _serviceProvider.GetService<IOptions<SnapshotCollectorConfiguration>>();
                return new SnapshotCollectorTelemetryProcessor(next, configuration: snapshotConfigurationOptions.Value);
            }
        }
    }
}
