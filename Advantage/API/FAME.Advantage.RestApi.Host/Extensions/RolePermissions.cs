﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolePermissions.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the RolePermissions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Extensions
{
    /// <summary>
    /// The role permissions.
    /// </summary>
    public static class RolePermissions
    {
        /// <summary>
        /// The admissions read.
        /// </summary>
        public const string AdmissionsRead = "AD-Read";

        /// <summary>
        /// The admissions modify.
        /// </summary>
        public const string AdmissionsModify = "AD-Modify";

        /// <summary>
        /// The admissions create.
        /// </summary>
        public const string AdmissionsCreate = "AD-Create";

        /// <summary>
        /// The admissions delete.
        /// </summary>
        public const string AdmissionsDelete = "AD-Delete";

        /// <summary>
        /// The system read.
        /// </summary>
        public const string SystemRead = "SY-Read";
    }
}
