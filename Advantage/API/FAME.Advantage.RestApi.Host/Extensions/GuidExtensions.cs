﻿using System;

namespace FAME.Advantage.RestApi.Host.Extensions
{
    public static class GuidExtensions
    {
        public static bool IsEmpty(this Guid input)
        {
            return input == Guid.Empty;
        }
    }
}
