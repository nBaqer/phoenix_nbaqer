﻿using System;

namespace FAME.Advantage.RestApi.Host.Extensions
{
    public static class TimestampExtensions
    {
        private static readonly DateTime BASE_DATE = new DateTime(1970, 1, 1);

        public static DateTime ToDateTime(this int timeT)
        {
            return BASE_DATE.AddSeconds(timeT);
        }

        public static DateTime ToDateTime(this long timeT)
        {
            return BASE_DATE.AddSeconds(timeT);
        }

        public static long ToTimestamp(this DateTime dateTime)
        {
            return Convert.ToInt64((dateTime - BASE_DATE).TotalSeconds);
        }

        public static long FromDateTime(this DateTime dateTime)
        {
            return Convert.ToInt64((dateTime - BASE_DATE).TotalSeconds);
        }
    }
}
