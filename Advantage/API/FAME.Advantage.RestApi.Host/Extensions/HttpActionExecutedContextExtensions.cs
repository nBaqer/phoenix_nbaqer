﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace FAME.Advantage.RestApi.Host.Extensions
{
    public static class HttpActionExecutedContextExtensions
    {
        public static bool HasException(this ActionExecutedContext context)
        {
            return !context.HasNoException();
        }

        public static bool HasNoException(this ActionExecutedContext context)
        {
            return context.Exception == null;
        }
    }
}
