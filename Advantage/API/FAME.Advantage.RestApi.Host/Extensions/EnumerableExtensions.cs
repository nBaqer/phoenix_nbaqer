﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Extensions
{
    public static class EnumerableExtensions
    {

        public static Nullable<DateTime> MaxOrNull(this IEnumerable<DateTime> source)
        {
            if (source.Any())
                return source.Max();
            else
                return null;
        }
    }
}
