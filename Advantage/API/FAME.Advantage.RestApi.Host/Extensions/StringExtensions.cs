﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The string extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Extensions
{
    using System.Text;
    using System.Text.RegularExpressions;

    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The string extensions.
    /// </summary>
    public static class StringExtensions
    {

        /// <summary>
        /// The to proper case.
        /// Capitalize the first character and add a space before
        ///  each capitalized letter (except the first character).
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ToProperCase(this string value)
        {
            string pattern = @"(?<=\w)(?=[A-Z])";
            var result = Regex.Replace(value, pattern, " ", RegexOptions.None);
            return result.Substring(0, 1).ToUpper() + result.Substring(1);
        }

        /// <summary>
        /// The is null or white space or empty.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool IsNullOrWhiteSpaceOrEmpty(this string input)
        {
            return string.IsNullOrWhiteSpace(input);
        }

        /// <summary>
        /// The remove all formatting.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string RemoveAllFormatting(this string input)
        {
            return Regex.Replace(input, AdvRegex.REPLACE_NON_ALPHA, string.Empty);
        }

        /// <summary>
        /// The wild card starts with.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string WildCardStartsWith(this string input)
        {
            return $"{input}%";
        }

        /// <summary>
        /// The wild card ends with.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string WildCardEndsWith(this string input)
        {
            return $"%{input}";
        }

        /// <summary>
        /// The wild card contains.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string WildCardContains(this string input)
        {
            return $"%{input}%";
        }

        /// <summary>
        /// The to pascal case.
        /// This function will return a string text in pascal case. If there are spaces at the start or end they will be trimmed in the process.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ToPascalCase(this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (text.Trim().Length > 0)
                {
                    if (text.Trim().Contains(" "))
                    {
                        string[] content = text.Trim().Split(' ');
                        StringBuilder toPascalCase = new StringBuilder();
                        foreach (var value in content)
                        {
                            toPascalCase.Append(value.ToPascalCase() + " ");
                        }

                        return toPascalCase.ToString().Trim();
                    }
                    else
                    {
                        return text.Trim().ToLower().Substring(0, 1).ToUpper() + text.Trim().ToLower().Substring(1, text.Trim().Length - 1);
                    }
                }
            }

            return text;
        }
    }
}
