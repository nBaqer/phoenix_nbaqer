﻿

namespace FAME.Advantage.RestApi.Host.Common
{
    using System;
    using System.Globalization;
    using System.Text;

    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Extensions;

    /// <summary>
    /// The class for various useful functions.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// The random string.
        /// </summary>
        /// <param name="size">
        /// The size.
        /// </param>
        /// <param name="useLowerCase">
        /// The use lower case.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string RandomString(int size, bool useLowerCase)
        {
            Random random = new Random();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < size - 1; i++)
            {
                var ch = Convert.ToChar(Convert.ToInt32((26 * random.NextDouble()) + 65));
                builder.Append(ch);
            }

            if (useLowerCase)
            {
                return builder.ToString().ToLower();
            }

            return builder.ToString();
        }

        /// <summary>
        /// The random number.
        /// </summary>
        /// <param name="min">
        /// The min.
        /// </param>
        /// <param name="max">
        /// The max.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        /// <summary>
        /// The GetMaskedSsn method takes the ssn as parameter and returns SSN value in the masked format.
        /// </summary>
        /// <param name="ssn">
        /// The Student Ssn number.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetMaskedSsn(string ssn)
        {
            return !string.IsNullOrEmpty(ssn) ? $"XXX-XX-{ssn.Substring(5)}" : string.Empty;
        }

        /// <summary>
        /// The parse datetime to string takes a date and format the date into MM/dd/yyyy format string.
        /// </summary>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ParseDatetimeToString(DateTime? date)
        {
            return date != null ? DateTime.Parse(date.ToString()).ToString("MM/dd/yyyy") : string.Empty;
        }

        /// <summary>
        /// The parse currency formatted number takes a decimal value and format it into comma seperated currency with $ symbol.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ParseCurrencyFormattedNumber(decimal value)
        {
            return $"{Math.Round(value, 2).ToString("C", new CultureInfo("EN-us"))}";
        }

        /// <summary>
        /// The parse currency formatted number takes a decimal value and format it into comma seperated currency with $ symbol.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string FormatNumber(decimal value)
        {
            return Math.Round(value, 2).ToString("N", new CultureInfo("EN-us"));
        }

        /// <summary>
        /// The parse percentage formatted number takes a decimal value and format it into comma seperated currency with % symbol.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string ParsePercentageFormattedNumber(decimal value)
        {
            return $"{Math.Round(value, 1)}%";
        }

        /// <summary>
        /// The format data by type formats the value based on the data type.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <typeparam name="T">
        /// Generic type
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// returns a formatted string value
        /// </returns>
        public static string FormatValueByType<T>(T value, string type)
        {
            string formattedValue;
            switch (type)
            {
                case FieldType.Percentage:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.ParsePercentageFormattedNumber(Convert.ToDecimal(value)) : string.Empty;
                    break;

                case FieldType.Date:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.ParseDatetimeToString(Convert.ToDateTime(value)) : string.Empty;
                    break;

                case FieldType.Number:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Convert.ToString(Convert.ToDecimal(value), CultureInfo.InvariantCulture) : string.Empty;
                    break;

                default:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.FormatNumber(Convert.ToDecimal(value)) : string.Empty;
                    break;
            }

            return formattedValue;
        }

        /// <summary>
        /// The get type by property name takes the name of property and returns the type of the property in the object.
        /// </summary>
        /// <param name="propName">
        /// The prop name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetTypeByPropertyName(string propName)
        {
            var fieldsNameWithTypes = GetFieldsNameWithType();
            if (fieldsNameWithTypes.GetType().GetProperty(propName) != null)
            {
                return Convert.ToString(fieldsNameWithTypes.GetPropertyValue(propName));
            }

            return string.Empty;
        }

        /// <summary>
        /// The get fields name with type returns an object with fieldNames and their types.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public static object GetFieldsNameWithType()
        {
            return new
                       {
                           BoxHresult = FieldType.Percentage,
                           BoxMresult = FieldType.Percentage,
                           StartDate = FieldType.Date,
                           ScheduledEndDate = FieldType.Date,
                           WithdrawalDate = FieldType.Date,
                           PercentageOfActualAttendence = FieldType.Number,
                           TotalTime = FieldType.Number,
                           CompletedTime = FieldType.Number
                       };
        }
    }
}
