﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSearchModelValidator.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The Student Entity.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Validations.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Services.AcademicRecords;
    using FluentValidation;

    /// <summary>
    /// The student search model validator.
    /// this class provides the required validation rules for the request
    /// </summary>
    public class StudentSearchModelValidator : AbstractValidator<StudentSearch>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentSearchModelValidator"/> class.
        /// </summary>
        /// <param name="studentService">
        /// The student service.
        /// </param>
        public StudentSearchModelValidator(StudentService studentService)
        {
            var dvset = studentService.GetStudentSearchValidations();
            this.When(
                x => dvset.Rules.ContainsKey("CampusId") && dvset.Rules["CampusId"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["CampusId"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.CampusId)
                                    .NotEmpty()
                                    .WithMessage($"{rule.PropertyName} is Required");
                        });
                });
            this.When(
                x => dvset.Rules.ContainsKey("Filter") && dvset.Rules["Filter"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["Filter"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.Filter)
                                .NotEmpty()
                                .WithMessage($"{rule.PropertyName} is Required");
                                });
                        this.When(
                            x => rule.MinLength.HasValue && rule.MaxLength.HasValue,
                            () =>
                                {
                                    this.RuleFor(x => x.Filter)
                                    .Length(rule.MinLength.Value, rule.MaxLength.Value)
                                    .WithMessage($"{rule.PropertyName} must be between {rule.MinLength.Value} and {rule.MaxLength.Value} characters");
                        });
                });
        }
    }
}