﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentTerminationModelValidator.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentTerminationModelValidator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Validations.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.Host.Services.AcademicRecords;
    using FAME.Orm.Advantage.Domain.Common;

    using FluentValidation;

    /// <summary>
    /// The student termination model validator.
    /// </summary>
    public class StudentTerminationModelValidator : AbstractValidator<StudentTermination>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StudentTerminationModelValidator"/> class.
        /// </summary>
        /// <param name="studentTerminationService">
        /// The student service.
        /// </param>
        public StudentTerminationModelValidator(StudentTerminationService studentTerminationService)
        {
            var dvset = studentTerminationService.GetStudentTerminationValidations();

            this.When(
                x => dvset.Rules.ContainsKey("MethodType") && dvset.Rules["MethodType"].IsEnabled,
                () =>
                    {
                        this.When(
                            x => x.MethodType.Equals((int)Constants.MethodType.Save),
                            () =>
                                {
                                    this.When(
                                        x => dvset.Rules.ContainsKey("StudentEnrollmentId")
                                             && dvset.Rules["StudentEnrollmentId"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["StudentEnrollmentId"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.StudentEnrollmentId).NotEmpty()
                                                                .WithMessage($"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                });
                        this.When(
                            x => x.MethodType.Equals((int)Constants.MethodType.Others),
                            () =>
                                {
                                    this.When(
                                        x => dvset.Rules.ContainsKey("StudentEnrollmentId")
                                             && dvset.Rules["StudentEnrollmentId"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["StudentEnrollmentId"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.StudentEnrollmentId).NotEmpty()
                                                                .WithMessage($"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("StatusCodeId")
                                             && dvset.Rules["StatusCodeId"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["StatusCodeId"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.StatusCodeId).NotEmpty().WithMessage(
                                                                $"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("DropReasonId")
                                             && dvset.Rules["DropReasonId"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["DropReasonId"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.DropReasonId).NotEmpty().WithMessage(
                                                                $"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("DateWithdrawalDetermined")
                                             && dvset.Rules["DateWithdrawalDetermined"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["DateWithdrawalDetermined"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.DateWithdrawalDetermined).NotEmpty().WithMessage(
                                                                $"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("IsPerformingR2T4Calculator")
                                             && dvset.Rules["IsPerformingR2T4Calculator"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["IsPerformingR2T4Calculator"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.IsPerformingR2T4Calculator).NotEmpty()
                                                                .WithMessage($"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("CalculationPeriodType")
                                             && dvset.Rules["CalculationPeriodType"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["CalculationPeriodType"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.CalculationPeriodType).NotEmpty()
                                                                .WithMessage($"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                    this.When(
                                        x => dvset.Rules.ContainsKey("CreatedBy") && dvset.Rules["CreatedBy"].IsEnabled,
                                        () =>
                                            {
                                                var subRule = dvset.Rules["CreatedBy"];
                                                this.When(
                                                    x => subRule.IsRequired,
                                                    () =>
                                                        {
                                                            this.RuleFor(x => x.CreatedBy).NotEmpty().WithMessage(
                                                                $"{subRule.PropertyName} is Required");
                                                        });
                                            });
                                });
                    });
        }
    }
}