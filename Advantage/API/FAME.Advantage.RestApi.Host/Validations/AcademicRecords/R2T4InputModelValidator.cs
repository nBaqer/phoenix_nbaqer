﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4InputModelValidator.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4InputModelValidator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Validations.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input; 
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FluentValidation;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 input model validator. 
    /// </summary>
    public class R2T4InputModelValidator : AbstractValidator<R2T4Input>
    {
        /// <summary>
        /// Initializes a new instance of the R2T4InputModelValidator class.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        public R2T4InputModelValidator(IR2T4InputService service)
        {
            var dvset = service.GetR2T4InputServiceValidations();
            this.When(
                x => dvset.Rules.ContainsKey("TerminationId") && dvset.Rules["TerminationId"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["TerminationId"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.TerminationId).NotEmpty()
                                        .WithMessage($"{rule.PropertyName} is Required");
                                });
                    });

            this.When(
                x => dvset.Rules.ContainsKey("ProgramUnitTypeId") && dvset.Rules["ProgramUnitTypeId"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["ProgramUnitTypeId"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.ProgramUnitTypeId).NotEmpty()
                                        .WithMessage($"{rule.PropertyName} is Required");
                                });
                    });
             
            this.When(
                x => dvset.Rules.ContainsKey("CreatedBy") && dvset.Rules["CreatedBy"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["CreatedBy"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.CreatedBy).NotEmpty()
                                        .WithMessage($"{rule.PropertyName} is Required");
                                });
                    });
        }
    }
}
