﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4ResultInputValidator.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4InputModelValidator type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Validations.AcademicRecords
{
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using FAME.Advantage.RestApi.Host.Services.Interfaces.AcademicRecords;
    using FluentValidation;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 Result input model validator. 
    /// </summary>
    public class R2T4ResultInputValidator : AbstractValidator<R2T4Input>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4ResultInputValidator"/> class. 
        /// Initializes a new instance of the R2T4InputModelValidator class.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        public R2T4ResultInputValidator(IR2T4ResultService service)
        {
            var dvset = service.GetR2T4ResultValidationRuleSet();
            this.When(
                x => dvset.Rules.ContainsKey("TerminationId") && dvset.Rules["TerminationId"].IsEnabled,
                () =>
                {
                    var rule = dvset.Rules["TerminationId"];
                    this.When(
                        x => rule.IsRequired,
                        () =>
                        {
                            this.RuleFor(x => x.TerminationId).NotEmpty()
                                    .WithMessage($"{rule.PropertyName} is Required");
                        });
                });

            this.When(
                x => dvset.Rules.ContainsKey("R2T4InputId") && dvset.Rules["R2T4InputId"].IsEnabled,
                () =>
                {
                    var rule = dvset.Rules["R2T4InputId"];
                    this.When(
                        x => rule.IsRequired,
                        () =>
                        {
                            this.RuleFor(x => x.R2T4InputId).NotEmpty()
                                .WithMessage($"{rule.PropertyName} is Required");
                        });
                });
            }
    }
}
