﻿namespace FAME.Advantage.RestApi.Host.Validations.DynamicValidation
{
    public class DynamicValidationRule
    {
        public string PropertyName { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsRequired { get; set; }
        public int? MinLength { get; set; }
        public int? MaxLength { get; set; }
        public string RegexMatch { get; set; }

        public DynamicValidationRule()
        {
            
        }
        public DynamicValidationRule(string propertyName, bool isEnabled,bool isRequired)
        {
            PropertyName = propertyName;
            IsEnabled = isEnabled;
            IsRequired = isRequired;
        }
        public DynamicValidationRule(string propertyName, bool isEnabled, bool isRequired,string regexMatch)
        {
            PropertyName = propertyName;
            IsEnabled = isEnabled;
            IsRequired = isRequired;
            RegexMatch = regexMatch;
        }
        public DynamicValidationRule(string propertyName, bool isEnabled, bool isRequired,int minLength,int maxLength)
        {
            PropertyName = propertyName;
            IsEnabled = isEnabled;
            IsRequired = isRequired;
            MinLength = minLength;
            MaxLength = maxLength;
        }
        public DynamicValidationRule(string propertyName, bool isEnabled, bool isRequired, int minLength, int maxLength,string regexMatch)
        {
            PropertyName = propertyName;
            IsEnabled = isEnabled;
            IsRequired = isRequired;
            MinLength = minLength;
            MaxLength = maxLength;
            RegexMatch = regexMatch;
        }
    }
}
