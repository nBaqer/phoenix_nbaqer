﻿using System.Collections.Generic;

namespace FAME.Advantage.RestApi.Host.Validations.DynamicValidation
{
    public class DynamicValidationRuleSet
    {
       public string EntityName { get; set; }
       public Dictionary<string,DynamicValidationRule> Rules { get; set; }

        public DynamicValidationRuleSet(string entityName)
        {
            EntityName = entityName;
            Rules = new Dictionary<string, DynamicValidationRule>();
        }
        public DynamicValidationRuleSet(string entityName, Dictionary<string, DynamicValidationRule> rules)
        {
            EntityName = entityName;
            Rules = rules;
        }
    }
}
