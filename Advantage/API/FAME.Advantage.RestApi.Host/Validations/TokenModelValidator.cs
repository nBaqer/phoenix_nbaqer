﻿using FAME.Orm.AdvTenant.Domain.Entities;
using FluentValidation;

namespace FAME.Advantage.RestApi.Host.Validations
{
    public class TokenModelValidator : AbstractValidator<TokenRequest>
    {
        public TokenModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().WithMessage("Username is required");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Password is required");
        }
    }
}
