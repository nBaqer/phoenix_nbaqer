﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatusModelValidator.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The Status Entity. 
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Validations.SystemCatalog
{
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Advantage.RestApi.Host.Services.SystemCatalog;
    using FluentValidation;

    /// <summary>
    /// Initializes a new instance of the <see cref="DropStatusModelValidator"/> class.
    /// </summary>
    public class DropStatusModelValidator : AbstractValidator<DropStatus>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DropStatusModelValidator"/> class.
        /// </summary>
        /// <param name="statusService">
        /// The status service
        /// </param>
        public DropStatusModelValidator(DropStatusCodeService statusService)
        {
            var dvset = statusService.GetDropStatusValidations();
            this.When(
                x => dvset.Rules.ContainsKey("CampusId") && dvset.Rules["CampusId"].IsEnabled,
                () =>
                    {
                        var rule = dvset.Rules["CampusId"];
                        this.When(
                            x => rule.IsRequired,
                            () =>
                                {
                                    this.RuleFor(x => x.CampusId)
                                        .NotEmpty()
                                        .WithMessage($"{rule.PropertyName} is Required");
                                });
                    });
        }
    }
}
