﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardReportsSettingMapper.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardReportsSettingMapper type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;


    /// <summary>
    /// The address type mapping that will give the value as the GUId and text as address code
    /// </summary>
    public class StateBoardReportsSettingMapper : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper
            .CreateMap<StateBoardReportSetting, SySchoolStateBoardReports>()
            .ForMember(destination => destination.ModifiedDate, option => option.Ignore())
            .ForMember(destination => destination.ModifiedUser, option => option.Ignore())
            .ForMember(destination => destination.SchoolStateBoardReportId, option => option.Ignore())
            .ForMember(destination => destination.Report, option => option.Ignore())
            .ForMember(destination => destination.State, option => option.Ignore())
            .ForMember(destination => destination.SyStateBoardProgramCourseMappings, option => option.Ignore())
            .ForMember(destination => destination.Campus, option => option.Ignore())
            .ForMember(destination => destination.LicensingAgency, option => option.Ignore());

    }
}