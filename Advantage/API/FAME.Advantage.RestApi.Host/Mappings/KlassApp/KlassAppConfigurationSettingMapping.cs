﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KlassAppConfigurationSettingMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The klass app configuration setting mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.KlassApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.KlassApp;

    /// <summary>
    /// The klass app configuration setting mapping.
    /// </summary>
    public class KlassAppConfigurationSettingMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyKlassAppConfigurationSetting, KlassAppConfigurationSetting>()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.Id))
                .ForMember(
                    destination => destination.AdvantageId,
                    option => option.MapFrom(source => source.AdvantageId))
                .ForMember(
                    destination => destination.CreationDate,
                    option => option.MapFrom(source => source.CreationDate))
                .ForMember(destination => destination.IsActive, option => option.MapFrom(source => source.IsActive))
                .ForMember(
                    destination => destination.IsCustomField,
                    option => option.MapFrom(source => source.IsCustomField))
                .ForMember(destination => destination.ItemLabel, option => option.MapFrom(source => source.ItemLabel))
                .ForMember(destination => destination.ItemStatus, option => option.MapFrom(source => source.ItemStatus))
                .ForMember(destination => destination.ItemValue, option => option.MapFrom(source => source.ItemValue))
                .ForMember(
                    destination => destination.ItemValueType,
                    option => option.MapFrom(source => source.ItemValueType))
                .ForMember(destination => destination.KlassAppId, option => option.MapFrom(source => source.KlassAppId))
                .ForMember(
                    destination => destination.KlassEntityId,
                    option => option.MapFrom(source => source.KlassEntityId))
                .ForMember(
                    destination => destination.LastOperationLog,
                    option => option.MapFrom(source => source.LastOperationLog))
                .ForMember(
                    destination => destination.KlassOperationTypeId,
                    option => option.MapFrom(source => source.KlassOperationTypeId))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.KlassOperationTypeCode, option => option.MapFrom(source => source.KlassOperationType.Code));
        }
    }
}
