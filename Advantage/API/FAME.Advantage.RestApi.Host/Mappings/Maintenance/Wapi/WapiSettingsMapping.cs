﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiSettingsMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the WapiSettingsMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Maintenance.Wapi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper.Configuration;

    /// <summary>
    /// The wapi settings mapping.
    /// </summary>
    public class WapiSettingsMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<Fame.EFCore.Advantage.Entities.SyWapiSettings, DataTransferObjects.Maintenance.Wapi.WapiSettings>()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.Id)).ForMember(
                    destination => destination.ConsumerKey,
                    option => option.MapFrom(source => source.ConsumerKey))
                .ForMember(destination => destination.PrivateKey, option => option.MapFrom(source => source.PrivateKey))
                .ForMember(destination => destination.UserName, option => option.MapFrom(source => source.UserName))
                .ForMember(destination => destination.ExternalUrl, option => option.MapFrom(source => source.ExternalUrl))
                .ForMember(
                    destination => destination.CodeOperation,
                    option => option.MapFrom(source => source.CodeOperation))
                .ForMember(destination => destination.DateLastExecution, option => option.MapFrom(source => source.DateLastExecution));
        }
    }
}
