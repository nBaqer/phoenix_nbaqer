﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomFileConfigurationMapping.cs" company="FAME INC">
//  FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the CustomFileConfigurationMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.Maintenance;
    using FAME.Advantage.RestApi.DataTransferObjects.Reports.StateBoard;


    /// <summary>
    /// The custom file configuration mapping per feature
    /// </summary>
    public class CustomFileConfigurationMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper
            .CreateMap<SyCustomFeatureFileConfiguration, CustomFileFeatureConfig>()
            .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
            .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
            .ForMember(destination => destination.Id, option => option.MapFrom(source => source.CustomFeatureConfigurationId))
            .ForMember(destination => destination.FileStorageType, option => option.MapFrom(source => source.FileStorageType))
            .ForMember(destination => destination.CampusFileConfigurationId, option => option.MapFrom(source => source.CampusConfigurationId))
            .ForMember(destination => destination.CloudKey, option => option.MapFrom(source => source.CloudKey))
            .ForMember(destination => destination.Password, option => option.MapFrom(source => source.Password))
            .ForMember(destination => destination.UserName, option => option.MapFrom(source => source.UserName))
            .ForMember(destination => destination.Path, option => option.MapFrom(source => source.Path));



    }
}