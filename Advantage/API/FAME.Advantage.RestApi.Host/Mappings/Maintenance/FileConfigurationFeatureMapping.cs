﻿
namespace FAME.Advantage.RestApi.Host.Mappings.Maintenance
{

    // --------------------------------------------------------------------------------------------------------------------
    // <copyright file="FileConfigurationFeatureMapping.cs" company="Fame Inc">
    //   2019
    // </copyright>
    // <summary>
    //   Defines the FileConfigurationFeatureMapping type.
    // </summary>
    // --------------------------------------------------------------------------------------------------------------------


    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The file configuration feature mapping.
    /// </summary>
    public class FileConfigurationFeatureMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyFileConfigurationFeatures, IListItem<string, string>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.FeatureId))
            .ForMember(
                destination => destination.Text,
                option => option.MapFrom(source => source.FeatureDescription))
           ;
    }
}

