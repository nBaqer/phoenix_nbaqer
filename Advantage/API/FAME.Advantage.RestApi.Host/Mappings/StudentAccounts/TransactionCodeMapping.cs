﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransactionCodeMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TransactionCodeMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.StudentAccounts
{
    using System;
    using Fame.EFCore.Advantage.Entities;
    using AutoMapper.Configuration;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    /// <summary>
    /// The TransactionCode mapping that will give the value as the transactioncodeId and text as transactionCode Description
    /// </summary>
    public class TransactionCodeMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SaTransCodes, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.TransCodeId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.TransCodeDescrip));
    }
}
