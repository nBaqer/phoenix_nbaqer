﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwardsMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentAwardsMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.StudentAccounts
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.StudentAccounts;

    /// <summary>
    /// The student awards mapping maps FaStudentAwards and StudentAwards.
    /// </summary>
    public class StudentAwardsMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps FaStudentAwards and StudentAwards.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<FaStudentAwards, StudentAwards>()
                .ForMember(destination => destination.StudentAwardId, option => option.MapFrom(source => source.StudentAwardId))
                .ForMember(destination => destination.AwardId, option => option.MapFrom(source => source.AwardId))
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.AwardTypeId, option => option.MapFrom(source => source.AwardTypeId))
                .ForMember(destination => destination.AcademicYearId, option => option.MapFrom(source => source.AcademicYearId))
                .ForMember(destination => destination.LenderId, option => option.MapFrom(source => source.LenderId))
                .ForMember(destination => destination.ServicerId, option => option.MapFrom(source => source.ServicerId))
                .ForMember(destination => destination.GuarantorId, option => option.MapFrom(source => source.GuarantorId))
                .ForMember(destination => destination.GrossAmount, option => option.MapFrom(source => source.GrossAmount))
                .ForMember(destination => destination.LoanFees, option => option.MapFrom(source => source.LoanFees))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.AwardStartDate, option => option.MapFrom(source => source.AwardStartDate))
                .ForMember(destination => destination.AwardEndDate, option => option.MapFrom(source => source.AwardEndDate))
                .ForMember(destination => destination.Disbursements, option => option.MapFrom(source => source.Disbursements))
                .ForMember(destination => destination.LoanId, option => option.MapFrom(source => source.LoanId))
                .ForMember(destination => destination.EmasfundCode, option => option.MapFrom(source => source.EmasfundCode))
                .ForMember(destination => destination.FinancialAidId, option => option.MapFrom(source => source.FaId))
                .ForMember(destination => destination.AwardCode, option => option.MapFrom(source => source.AwardCode))
                .ForMember(destination => destination.AwardSubCode, option => option.MapFrom(source => source.AwardSubCode))
                .ForMember(destination => destination.AwardStatus, option => option.MapFrom(source => source.AwardStatus))
                .ForMember(destination => destination.ResultStatus, option => option.Ignore());

            mapper.CreateMap<DataTransferObjects.StudentAccounts.AFA.StudentAwards, FaStudentAwards>()
                .ForMember(destination => destination.StudentAwardId, option => option.Ignore())
                .ForMember(destination => destination.AwardId, option => option.Ignore())
                .ForMember(destination => destination.StuEnrollId, option => option.MapFrom(source => source.StudentEnrollmentId))
                .ForMember(destination => destination.AwardTypeId, option => option.Ignore())
                .ForMember(destination => destination.AcademicYearId, option => option.Ignore())
                .ForMember(destination => destination.LenderId, option => option.Ignore())
                .ForMember(destination => destination.ServicerId, option => option.Ignore())
                .ForMember(destination => destination.GuarantorId, option => option.Ignore())
                .ForMember(destination => destination.GrossAmount, option => option.UseValue<decimal>(0))
                .ForMember(destination => destination.LoanFees, option => option.UseValue<decimal>(0))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.AwardStartDate, option => option.MapFrom(source => source.AwardStartDate))
                .ForMember(destination => destination.AwardEndDate, option => option.MapFrom(source => source.AwardEndDate))
                .ForMember(destination => destination.Disbursements, option => option.Ignore())
                .ForMember(destination => destination.LoanId, option => option.Ignore())
                .ForMember(destination => destination.EmasfundCode, option => option.Ignore())
                .ForMember(destination => destination.FaId, option => option.MapFrom(source => source.FinancialAidId.Trim()))
                .ForMember(destination => destination.AwardCode, option => option.Ignore())
                .ForMember(destination => destination.AwardSubCode, option => option.Ignore())
                .ForMember(destination => destination.AwardStatus, option => option.MapFrom(source => source.AwardStatus));

            mapper.CreateMap<DataTransferObjects.StudentAccounts.AFA.Disbursement, FaStudentAwardSchedule>()
                .ForMember(destination => destination.AwardScheduleId, option => option.Ignore())
                .ForMember(destination => destination.StudentAwardId, option => option.Ignore())
                .ForMember(destination => destination.ExpectedDate,
                    option => option.MapFrom(source => source.AnticipatedDisbursementDate))
                .ForMember(destination => destination.Amount,
                    option => option.MapFrom(source => source.AnticipatedNetDisbursementAmt))
                .ForMember(destination => destination.GrossAmount,
                    option => option.MapFrom(source => source.AnticipatedGrossAmount))
                .ForMember(destination => destination.LoanFees,
                    option => option.MapFrom(source => source.FeeAmount))
                .ForMember(destination => destination.Reference, option => option.Ignore())
                .ForMember(destination => destination.TransactionId, option => option.Ignore())
                .ForMember(destination => destination.TermNumber, option => option.Ignore())
                .ForMember(destination => destination.Recid, option => option.Ignore())
                .ForMember(destination => destination.DisbursementNumber, option => option.Ignore())
                .ForMember(destination => destination.IsProcessed, option => option.UseValue<bool>(false))
                .ForMember(destination => destination.SequenceNumber, option => option.MapFrom(source => source.SequenceNumber))
                .ForMember(destination => destination.DisbursementStatus, option => option.MapFrom(source => source.DisbursementStatus))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate));

        }
    }
}
