﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AspnetMembershipMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AspnetMembershipMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Mappings.Security
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Tenant.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The aspnet membership mapping.
    /// </summary>
    public class AspnetMembershipMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AspnetMembership, AspNetMembership>()
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId))
                .ForMember(destination => destination.ApplicationId, option => option.MapFrom(source => source.ApplicationId))
                .ForMember(destination => destination.Password, option => option.MapFrom(source => source.Password))
                .ForMember(destination => destination.PasswordFormat, option => option.MapFrom(source => source.PasswordFormat))
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.PasswordQuestion, option => option.MapFrom(source => source.PasswordQuestion))
                .ForMember(destination => destination.CreateDate, option => option.MapFrom(source => source.CreateDate))
                .ForMember(destination => destination.LastLoginDate, option => option.MapFrom(source => source.LastLoginDate))
                .ForMember(destination => destination.LastLockoutDate, option => option.MapFrom(source => source.LastLockoutDate))
                .ReverseMap();
        }
    }
}
