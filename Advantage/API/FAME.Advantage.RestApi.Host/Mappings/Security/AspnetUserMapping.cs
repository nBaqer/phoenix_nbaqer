﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AspnetUserMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AspnetUserMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Mappings.Security
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Tenant.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The aspnet user mapping.
    /// </summary>
    public class AspnetUserMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AspnetUsers, AspNetUser>()
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId))
                .ForMember(destination => destination.ApplicationId, option => option.MapFrom(source => source.ApplicationId))
                .ForMember(destination => destination.IsAnonymous, option => option.MapFrom(source => source.IsAnonymous))
                .ForMember(destination => destination.LastActivityDate, option => option.MapFrom(source => source.LastActivityDate))
                .ForMember(destination => destination.UserName, option => option.MapFrom(source => source.UserName))
                .ForMember(destination => destination.UserNameLowerCase, option => option.MapFrom(source => source.LoweredUserName))
                .ReverseMap();
        }
    }
}
