﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TenantUserMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the tenant user mapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Mappings.Security
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Tenant.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Security;

    /// <summary>
    /// The tenant user mapping.
    /// </summary>
    public class TenantUserMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<TenantUsers, TenantUser>()
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId))
                .ForMember(destination => destination.TenantId, option => option.MapFrom(source => source.TenantId))
                .ForMember(destination => destination.IsDefaultTenant, option => option.MapFrom(source => source.IsDefaultTenant))
                .ForMember(destination => destination.IsSupportUser, option => option.MapFrom(source => source.IsSupportUser))
                .ReverseMap();
        }
    }
}
