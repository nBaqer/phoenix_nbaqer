﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RolesMapping.cs" company="FAME">
//   Fame Inc 2018
// </copyright>
// <summary>
//   The roles mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The roles mapping.
    /// </summary>
    public class RolesMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyRoles, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.RoleId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Role));
    }
}
