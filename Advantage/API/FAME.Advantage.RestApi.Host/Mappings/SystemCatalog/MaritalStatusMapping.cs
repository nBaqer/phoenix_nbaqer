﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MaritalStatusMapping.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   The marital status mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The marital status mapping.
    /// </summary>
    public class MaritalStatusMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<AdMaritalStatus, MaritalStatus>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.MaritalStatId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.MaritalStatDescrip))
            .ForMember(destination => destination.AfaValue, option => option.MapFrom(source => source.AfaMapping.AfaCode));
    }
}
