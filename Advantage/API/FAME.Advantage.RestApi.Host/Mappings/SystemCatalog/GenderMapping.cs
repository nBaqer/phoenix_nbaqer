﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GenderMapping.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the GenderMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The gender mapping.
    /// </summary>
    public class GenderMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<AdGenders, Gender>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.GenderId))
            .ForMember(
                destination => destination.Text,
                option => option.MapFrom(source => source.GenderDescrip))
            .ForMember(
                destination => destination.AfaValue,
                option => option.MapFrom(x => x.AfaMapping.AfaCode));
    }
}
