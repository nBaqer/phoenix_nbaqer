﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditSummaryMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The credit summary mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The credit summary mapping.
    /// </summary>
    public class CreditSummaryMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyCreditSummary, CreditSummary>()
                .ForMember(destination => destination.StudentEnrollId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.TermId, option => option.MapFrom(source => source.TermId))
                .ForMember(destination => destination.TermDescription, option => option.MapFrom(source => source.TermDescrip))
                .ForMember(destination => destination.ReqId, option => option.MapFrom(source => source.ReqId))
                .ForMember(destination => destination.ReqDescription, option => option.MapFrom(source => source.ReqDescrip))
                .ForMember(destination => destination.ClassSectionId, option => option.MapFrom(source => source.ClsSectionId))
                .ForMember(destination => destination.CreditsEarned, option => option.MapFrom(source => source.CreditsEarned))
                .ForMember(destination => destination.CreditsAttempted, option => option.MapFrom(source => source.CreditsAttempted))
                .ForMember(destination => destination.CurrentScore, option => option.MapFrom(source => source.CurrentScore))
                .ForMember(destination => destination.CurrentGrade, option => option.MapFrom(source => source.CurrentGrade))
                .ForMember(destination => destination.FinalScore, option => option.MapFrom(source => source.FinalScore))
                .ForMember(destination => destination.FinalGrade, option => option.MapFrom(source => source.FinalGrade))
                .ForMember(destination => destination.Completed, option => option.MapFrom(source => source.Completed))
                .ForMember(destination => destination.FinalGpa, option => option.MapFrom(source => source.FinalGpa))
                .ForMember(destination => destination.ProductWeightedAverageCreditsGpa, option => option.MapFrom(source => source.ProductWeightedAverageCreditsGpa))
                .ForMember(destination => destination.CountWeightedAverageCredits, option => option.MapFrom(source => source.CountWeightedAverageCredits))
                .ForMember(destination => destination.ProductSimpleAverageCreditsGpa, option => option.MapFrom(source => source.ProductSimpleAverageCreditsGpa))
                .ForMember(destination => destination.CountSimpleAverageCredits, option => option.MapFrom(source => source.CountSimpleAverageCredits))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.TermGpaSimple, option => option.MapFrom(source => source.TermGpaSimple))
                .ForMember(destination => destination.TermGpaWeighted, option => option.MapFrom(source => source.TermGpaWeighted))
                .ForMember(destination => destination.CourseCredits, option => option.MapFrom(source => source.Coursecredits))
                .ForMember(destination => destination.CumulativeGpa, option => option.MapFrom(source => source.CumulativeGpa))
                .ForMember(destination => destination.CumulativeGpaSimple, option => option.MapFrom(source => source.CumulativeGpaSimple))
                .ForMember(destination => destination.FaCreditsEarned, option => option.MapFrom(source => source.FacreditsEarned))
                .ForMember(destination => destination.Average, option => option.MapFrom(source => source.Average))
                .ForMember(destination => destination.CumAverage, option => option.MapFrom(source => source.CumAverage))
                .ForMember(destination => destination.TermStartDate, option => option.MapFrom(source => source.TermStartDate));
        }
    }
}
