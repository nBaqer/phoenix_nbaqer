﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardCourseMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardCourseMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The state board course mapping that will give the value as the state board course id and text as state board course description
    /// </summary>
    public class StateBoardCourseMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyStateBoardCourses, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.StateBoardCourseId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Code + "-" + source.Description));
    }
}