﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SourceCategoryMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SourceCategoryMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper.Configuration;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The source category mapping that will give the value as the GUId and text as source category code
    /// </summary>
    public class SourceCategoryMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper
            .CreateMap<AdSourceCatagory, DataTransferObjects.Admissions.Lead.SourceCategory>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.SourceCatagoryId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.SourceCatagoryDescrip))
            .ForMember(destination => destination.SourceType, option => option.MapFrom(source => this.GetSourceType(source)));

        /// <summary>
        /// The get source type.
        /// </summary>
        /// <param name="sourceCategory">
        /// The source category.
        /// </param>
        /// <returns>
        /// The list of active source types that belongs to the given source category
        /// </returns>
        private IList<IListItem<string, Guid>> GetSourceType(AdSourceCatagory sourceCategory)
        {
            var sourceTypes = new List<IListItem<string, Guid>>();

            var sourceTypeList = sourceCategory.AdSourceType.Where(x => x.Status.StatusCode == SystemStatusCodes.Active && x.SourceCatagoryId == sourceCategory.SourceCatagoryId).ToList();
            foreach (var sourceType in sourceTypeList)
            {
                sourceTypes.Add(new ListItem<string, Guid>()
                                    {
                                        Value = sourceType.SourceTypeId,
                                       Text = sourceType.SourceTypeDescrip
                                    });
            }

            return sourceTypes;
        }
    }
}