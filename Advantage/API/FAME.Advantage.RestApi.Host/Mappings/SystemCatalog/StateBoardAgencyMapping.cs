﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StateBoardAgencyMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StateBoardAgencyMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The StateBoardAgency type mapping that will give the value as the state board agency id and text as state board agency description
    /// </summary>
    public class StateBoardAgencyMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyStateBoardAgencies, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.StateBoardAgencyId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Description));
    }
}