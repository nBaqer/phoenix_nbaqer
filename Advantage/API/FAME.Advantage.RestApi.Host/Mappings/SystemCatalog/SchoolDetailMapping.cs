﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SchoolDetailMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SchoolDetailMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The School Detail Mapping that will give the value as the GUId and text as state code
    /// </summary>
    public class SchoolDetailMapping : IMappingDefinition
    {
        /// <summary>
        /// The map SyCampuses entity to SchoolDetail DTO.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyCampuses, SchoolDetail>()
                .ForMember(destination => destination.Address1, option => option.MapFrom(source => source.Address1))
                .ForMember(destination => destination.Address2, option => option.MapFrom(source => source.Address2))
                .ForMember(destination => destination.CountryId, option => option.MapFrom(source => source.CountryId))
                .ForMember(destination => destination.StateId, option => option.MapFrom(source => source.StateId))
                .ForMember(
                    destination => destination.CountryCode,
                    option => option.MapFrom(source => this.GetCountryCode(source.Country)))
                .ForMember(
                    destination => destination.CountryDescription,
                    option => option.MapFrom(source => this.GetCountryDescription(source.Country)))
                .ForMember(destination => destination.City, option => option.MapFrom(source => source.City))
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(
                    destination => destination.Fax,
                    option => option.MapFrom(source => this.GetFormattedPhoneNumber(source.Fax)))
                .ForMember(
                    destination => destination.PhoneNumber1,
                    option => option.MapFrom(source => this.GetFormattedPhoneNumber(source.Phone1)))
                .ForMember(
                    destination => destination.PhoneNumber2,
                    option => option.MapFrom(source => this.GetFormattedPhoneNumber(source.Phone2)))
                .ForMember(
                    destination => destination.PhoneNumber3,
                    option => option.MapFrom(source => this.GetFormattedPhoneNumber(source.Phone3)))
                .ForMember(
                    destination => destination.StateCode,
                    option => option.MapFrom(source => this.GetStateCode(source.State)))
                .ForMember(
                    destination => destination.StateDescription,
                    option => option.MapFrom(source => this.GetStateDescription(source.State))).ForMember(
                    destination => destination.Website,
                    option => option.MapFrom(source => source.Website)).ForMember(
                    destination => destination.Zip,
                    option => option.MapFrom(source => source.Zip));
        }

        /// <summary>
        /// The get state code takes SyStates as input and returns state code.
        /// </summary>
        /// <param name="sourceState">
        /// The source state.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetStateCode(SyStates sourceState)
        {
            return sourceState?.StateCode;
        }

        /// <summary>
        /// The get state description takes SyStates as input and returns state description.
        /// </summary>
        /// <param name="sourceState">
        /// The source state.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetStateDescription(SyStates sourceState)
        {
            return sourceState?.StateDescrip;
        }

        /// <summary>
        /// The get formatted phone number.
        /// </summary>
        /// <param name="phoneNumber">
        /// The phone Number.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetFormattedPhoneNumber(string phoneNumber)
        {
            return (!string.IsNullOrEmpty(phoneNumber))
                       ? $"{double.Parse(phoneNumber):(###) ###-####}"
                       : string.Empty;
        }

        /// <summary>
        /// The get country description takes AdCountries as input and returns country description.
        /// </summary>
        /// <param name="sourceCountry">
        /// The source country.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetCountryDescription(AdCountries sourceCountry)
        {
            return sourceCountry?.CountryDescrip;
        }

        /// <summary>
        /// The get country code takes AdCountries as input and returns country code.
        /// </summary>
        /// <param name="sourceCountry">
        /// The source country.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetCountryCode(AdCountries sourceCountry)
        {
            return sourceCountry?.CountryCode;
        }
    }
}