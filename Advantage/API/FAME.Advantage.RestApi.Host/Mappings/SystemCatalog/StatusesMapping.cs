﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatusesMapping.cs" company="FAME INC">
//   2017
// </copyright>
// <summary>
//   Defines the StatusesMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The system statuses mapping that will give the value as the GUId and text as active/Inactive
    /// </summary>
    public class StatusesMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyStatuses, IListItem<string, Guid>>()
        .ForMember(destination => destination.Value, option => option.MapFrom(source => source.StatusId))
        .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Status));
    }
}