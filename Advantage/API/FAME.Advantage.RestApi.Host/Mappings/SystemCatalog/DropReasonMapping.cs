﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropReasonMapping.cs" company="FAME.inc">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The drop reason mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The drop reason mapping .
    /// </summary>
    public class DropReasonMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of Drop Reasons .
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for DropReasonId and Description .
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<ArDropReasons, IListItem<string, Guid>>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.DropReasonId))
                .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Descrip));
    }
}
