﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StatesMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StatesMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The states mapping that will give the value as the GUId and text as state code
    /// </summary>
    public class StatesMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyStates, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.StateId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.StateCode));
    }
}