﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserRoleMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the user role mapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The user role mapping.
    /// </summary>
    public class UserRoleMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyUsersRolesCampGrps, UserRole>()
                .ForMember(destination => destination.CampusGroupId, option => option.MapFrom(source => source.CampGrpId))
                .ForMember(destination => destination.RoleId, option => option.MapFrom(source => source.RoleId))
                .ForMember(destination => destination.SysRoleId, option => option.MapFrom(source => source.Role.SysRoleId))
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId))
                .ForMember(destination => destination.CampusGroupUserRoleId, option => option.MapFrom(source => source.UserRoleCampGrpId))
                .ReverseMap();
        }
    }
}
