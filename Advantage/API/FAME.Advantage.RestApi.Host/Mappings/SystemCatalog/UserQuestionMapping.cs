﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserQuestionMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user question mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using Fame.EFCore.Tenant.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Orm.Advantage.Domain.Entities;

    /// <summary>
    /// The user question mapping .
    /// </summary>
    public class UserQuestionMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of User Questions.
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for UserQuestionId and UserQuestionDescription.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<TenantQuestions, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.Id))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Description));
    }
}
