﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramVersionMapping type.
// </summary>ClassSectionMeetingService.cs  
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The program version type mapping that will give the value as the program version id and text as program version description
    /// </summary>
    public class ProgramVersionMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<ArPrgVersions, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.PrgVerId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.PrgVerDescrip));
    }
}