﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HolidayMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The holiday mapping provides mapping between entity SyHolidays and DTO Holiday.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper;
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The holiday mapping provides mapping between entity SyHolidays and DTO Holiday.
    /// </summary>
    public class HolidayMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity provides mapping between entity SyHolidays and DTO Holiday.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyHolidays, Holiday>()
            .ForMember(destination => destination.Id, option => option.MapFrom(source => source.HolidayId))
            .ForMember(destination => destination.Description, option => option.MapFrom(source => source.HolidayDescrip))
            .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.HolidayStartDate))
            .ForMember(destination => destination.EndDate, option => option.MapFrom(source => source.HolidayEndDate))
            .ForMember(destination => destination.StartTimeId, option => option.MapFrom(source => source.StartTimeId))
            .ForMember(destination => destination.EndTimeId, option => option.MapFrom(source => source.EndTimeId))
            .ForMember(destination => destination.AllDay, option => option.MapFrom(source => source.AllDay))
            .ForMember(destination => destination.Code, option => option.MapFrom(source => source.HolidayCode))
            .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
            .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
            .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId))
            .ForMember(destination => destination.StartTime, option => option.Ignore())
            .ForMember(destination => destination.EndTime, option => option.Ignore());
    }
}
