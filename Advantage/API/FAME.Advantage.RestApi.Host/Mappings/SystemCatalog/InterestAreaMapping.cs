﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InterestAreaMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the InterestAreaMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The interest area mapping that will give the value as the GUId and text as interest area code
    /// </summary>
    public class InterestAreaMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<ArPrgGrp, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.PrgGrpId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.PrgGrpDescrip));
    }
}