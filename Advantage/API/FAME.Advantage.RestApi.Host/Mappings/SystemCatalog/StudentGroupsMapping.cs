﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentGroupsMapping.cs" company="FAME INC">
//  FAME Inc. 2019
// </copyright>
// <summary>
//   Defines the StudentGroupsMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The student groups type mapping that will give the value as the student group id and text as student group description
    /// </summary>
    public class StudentGroupsMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<AdLeadGroups, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.LeadGrpId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Descrip));
    }
}