﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AcademicCalendarMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The academic calendar mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The academic calendar mapping .
    /// </summary>
    public class AcademicCalendarMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of academic calendar programs.
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for AcademicCalendarId and Description.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyAcademicCalendars, IListItem<string, string>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.Acid))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Acdescrip));
    }
}
