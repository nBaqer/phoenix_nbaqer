﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModuleMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The module mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The module mapping .
    /// </summary>
    public class ModuleMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of modules.
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for ModuleId and Module name.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyModules, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.ModuleId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.ModuleName));
    }
}
