﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddressTypeMapping.cs" company="FAME INC">
// FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AddressTypeMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The address type mapping that will give the value as the GUId and text as address code
    /// </summary>
    public class AddressTypeMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<PlAddressTypes, IListItem<string, Guid>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.AddressTypeId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.AddressDescrip));
    }
}