﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserMapping.cs" company="FAME Inc. 2018">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The user mapping.
    /// </summary>
    public class UserMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyUsers, User>()
                .ForMember(destination => destination.UserId, option => option.MapFrom(source => source.UserId))
                .ForMember(destination => destination.FullName, option => option.MapFrom(source => source.FullName))
                .ForMember(destination => destination.DisplayName, option => option.MapFrom(source => source.UserName))
                .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                .ForMember(destination => destination.UserTypeId, option => option.MapFrom(source => source.UserTypeId))
                .ForMember(destination => destination.UserTypeCode, option => option.MapFrom(source => source.UserType.Code))
                .ForMember(destination => destination.Password, option => option.Ignore())
                .ForMember(destination => destination.ConfirmPassword, option => option.Ignore())
                .ForMember(destination => destination.SendEmail, option => option.Ignore())
                .ForMember(destination => destination.DefaultModuleId, option => option.MapFrom(source => source.ModuleId))
                .ForMember(destination => destination.AccountActive, option => option.MapFrom(source => source.AccountActive))
                //.ForMember(destination => destination.UserRoles, option => option.MapFrom(source => source.SyUsersRolesCampGrps))
                .ReverseMap()
                .ForMember(dest => dest.UserType, option => option.Ignore());
            mapper.CreateMap<SyUsers, IListItem<string, Guid>>()
                .ForMember(destination => destination.Text, option => option.MapFrom((source => source.FullName)))
                .ForMember(destination => destination.Value, option => option.MapFrom((source => source.UserId)));
        }
    }
}
