﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResourceMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The resource mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The resource mapping .
    /// </summary>
    public class ResourceMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of resources.
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for ResourceId and Resource name.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyResources, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.ResourceId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Resource));
    }
}
