﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CalculationPeriodTypeMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The calculation period type mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The calculation period type mapping.
    /// </summary>
    public class CalculationPeriodTypeMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list  calculation period type.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyR2t4calculationPeriodTypes, IListItem<string, Guid>>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.CalculationPeriodTypeId))
                .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Description));
        }
    }
}
