﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhoneTypeMapping.cs" company="FAME INC">
//  FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PhoneTypeMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;
    using FAME.Orm.Advantage.Domain.Entities.SystemCatalog;

    /// <summary>
    /// The phone type mapping that will give the value as the GUId and text as phone type code
    /// </summary>
    public class PhoneTypeMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyPhoneType, IListItem<string, Guid>>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.PhoneTypeId))
                .ForMember(destination => destination.Text, option => option.MapFrom(source => source.PhoneTypeDescrip));
            
        }
    }
}