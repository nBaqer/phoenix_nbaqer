﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusGroupMapping.cs" company="">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The campus group mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The campus group mapping.
    /// </summary>
    public class CampusGroupMapping : IMappingDefinition
    {
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            
                mapper.CreateMap<SyCampGrps, CampusGroup>()
                    .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.CampusId))
                    .ForMember(destination => destination.CampusGroupCode, option => option.MapFrom(source => source.CampGrpCode))
                    .ForMember(destination => destination.CampusGroupDescription, option => option.MapFrom(source => source.CampGrpDescrip))
                    .ForMember(destination => destination.CampusGroupId, option => option.MapFrom(source => source.CampGrpId))
                    .ForMember(destination => destination.IsAllCampusGroup, option => option.MapFrom(source => source.IsAllCampusGrp))
                    .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId));

            mapper.CreateMap<SyCampGrps, IListItem<string, Guid>>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.CampGrpId))
                .ForMember(destination => destination.Text, option => option.MapFrom(source => source.CampGrpDescrip));
        }
    }
}
