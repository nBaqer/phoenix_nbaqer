﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CitizenShipMapping.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   The citizen ship mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    /// <summary>
    /// The citizen ship mapping.
    /// </summary>
    public class CitizenShipMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
            .CreateMap<AdCitizenships, CitizenShip>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.CitizenshipId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.CitizenshipDescrip))
            .ForMember(destination => destination.AfaValue, option => option.MapFrom(x => x.AfaMapping.AfaCode));
        }
    }
}
