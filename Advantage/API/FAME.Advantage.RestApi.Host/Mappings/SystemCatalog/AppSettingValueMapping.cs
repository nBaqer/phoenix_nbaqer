﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AppSettingValueMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AppSettingValueMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The app setting value mapping.
    /// </summary>
    public class AppSettingValueMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity for <code>SyConfigAppSetValues</code>
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) =>
            mapper.CreateMap<SyConfigAppSetValues, AppSettingValue>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.Value))
                .ForMember(destination => destination.Active, option => option.MapFrom(source => source.Active))
                .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.CampusId))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.SettingId, option => option.MapFrom(source => source.SettingId))
                .ForMember(destination => destination.ValueId, option => option.MapFrom(source => source.ValueId));
    }
}