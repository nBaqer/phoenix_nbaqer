﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DropStatusMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The student search mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using System;
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The drop status mapping.
    /// </summary>
    public class DropStatusMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper. 
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyStatusCodes, IListItem<string, Guid>>()
                .ForMember(destination => destination.Value, option => option.MapFrom(source => source.StatusCodeId))
                .ForMember(destination => destination.Text, option => option.MapFrom(source => source.StatusCodeDescrip));
        }
    }
}
