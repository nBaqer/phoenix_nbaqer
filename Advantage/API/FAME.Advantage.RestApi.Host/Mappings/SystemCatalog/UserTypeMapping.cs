﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserTypeMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user type mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;

    /// <summary>
    /// The user type mapping .
    /// </summary>
    public class UserTypeMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of User Types.
        /// </summary>
        /// <param name="mapper">
        /// The mapper will get values for UserTypeId and Code.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<SyUserType, IListItem<string, int>>()
            .ForMember(destination => destination.Value, option => option.MapFrom(source => source.UserTypeId))
            .ForMember(destination => destination.Text, option => option.MapFrom(source => source.Code));
    }
}
