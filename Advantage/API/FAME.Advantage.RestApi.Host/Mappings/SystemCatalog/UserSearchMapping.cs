﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserSearchMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The user search mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.SystemCatalog
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.SystemCatalog.Users;

    /// <summary>
    /// The user search mapping.
    /// </summary>
    public class UserSearchMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of Users
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyUsers, IListItem<string, UserSearchOutput>>()
                .ForMember(
                    x => x.Text,
                    opt => opt.MapFrom(source => source.UserId))
                .ForMember(
                x => x.Value,
                opt => opt.ResolveUsing(
                    model => new UserSearchOutput()
                    {
                        FullName = model.FullName,
                        DisplayName = model.UserName,
                        Email = model.Email,
                        UserType = model.UserType.Code,
                        Status = model.AccountActive.HasValue ? model.AccountActive.Value ? "Active" : "Inactive" : string.Empty,
                    }));
        }
    }
}