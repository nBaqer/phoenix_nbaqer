﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadBaseMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead base mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Extensions;

    /// <summary>
    /// The lead base mapping.
    /// </summary>
    public class LeadBaseMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            var map = mapper.CreateMap<Lead, LeadBase>()
                        .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                        .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.MiddleName, option => option.MapFrom(source => source.MiddleName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.InterestAreaId, option => option.MapFrom(source => source.InterestAreaId))
                        .ForMember(destination => destination.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryId))
                        .ForMember(destination => destination.BirthDate, option => option.MapFrom(source => source.BirthDate))
                        .ForMember(destination => destination.AdmissionsRepId, option => option.MapFrom(source => source.AdmissionsRepId))
                        .ForMember(destination => destination.DateApplied, option => option.MapFrom(source => source.DateApplied))
                        .ForMember(destination => destination.CreateDate, option => option.MapFrom(source => source.CreateDate))
                        .ForMember(destination => destination.SSN, option => option.MapFrom(source => source.SSN))
                        .ForMember(destination => destination.LeadStatusId, option => option.MapFrom(source => source.LeadStatusId))
                        .ForMember(destination => destination.StudentStatusId, option => option.MapFrom(source => source.StudentStatusId))
                        .ForMember(destination => destination.VendorId, option => option.MapFrom(source => source.VendorId))
                        .ForMember(destination => destination.SourceTypeId, option => option.MapFrom(source => source.SourceTypeId))
                        .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                        .ReverseMap();



            var map2 = mapper.CreateMap<AdLeads, LeadBase>()
                        .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                        .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.MiddleName, option => option.MapFrom(source => source.MiddleName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.InterestAreaId, option => option.MapFrom(source => source.AreaId))
                        .ForMember(destination => destination.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryId))
                        .ForMember(destination => destination.BirthDate, option => option.MapFrom(source => source.BirthDate))
                        .ForMember(destination => destination.AdmissionsRepId, option => option.MapFrom(source => source.AdmissionsRep))
                        .ForMember(destination => destination.DateApplied, option => option.MapFrom(source => source.DateApplied))
                        .ForMember(destination => destination.CreateDate, option => option.MapFrom(source => source.CreatedDate))
                        .ForMember(destination => destination.SSN, option => option.MapFrom(source => source.Ssn))
                        .ForMember(destination => destination.LeadStatusId, option => option.MapFrom(source => source.LeadStatus))
                        .ForMember(destination => destination.StudentStatusId, option => option.MapFrom(source => source.StudentStatusId))
                        .ForMember(destination => destination.VendorId, option => option.MapFrom(source => source.VendorId))
                        .ForMember(destination => destination.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryId))
                        .ForMember(destination => destination.SourceTypeId, option => option.MapFrom(source => source.SourceTypeId))
                        .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                        .ReverseMap();
        }
    }
}
