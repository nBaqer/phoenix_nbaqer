﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadAddressMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead address mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;

    /// <summary>
    /// The lead address mapping.
    /// </summary>
    public class LeadAddressMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AdLeadAddresses, Address>()
                 .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                 .ForMember(destination => destination.Address1, option => option.MapFrom(source => source.Address1))
                 .ForMember(destination => destination.Address2, option => option.MapFrom(source => source.Address2))
                 .ForMember(destination => destination.ApartmentNumber, option => option.MapFrom(source => source.AddressApto))
                .ForMember(destination => destination.IsMailingAddress, option => option.MapFrom(source => source.IsMailingAddress))
                .ForMember(destination => destination.IsInternational, option => option.MapFrom(source => source.IsInternational))
                .ForMember(destination => destination.City, option => option.MapFrom(source => source.City))
                .ForMember(destination => destination.State, option => option.MapFrom(source => source.State))
                .ForMember(destination => destination.Country, option => option.MapFrom(source => source.Country.CountryDescrip))
                .ForMember(destination => destination.County, option => option.MapFrom(source => source.County.CountyDescrip))
                .ForMember(destination => destination.Zip, option => option.MapFrom(source => source.ZipCode))
                .ForMember(destination => destination.ForeignCountryStr, option => option.MapFrom(source => source.ForeignCountryStr))
                .ForMember(destination => destination.ForeignCountyStr, option => option.MapFrom(source => source.ForeignCountyStr))

                 .ForMember(
                     destination => destination.IsShowOnLeadPage,
                     option => option.MapFrom(source => source.IsShowOnLeadPage))
                 .ForMember(destination => destination.Id, option => option.MapFrom(source => source.AdLeadAddressId))
                 .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                 .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                 .ForMember(destination => destination.AddressTypeId, option => option.MapFrom(source => source.AddressTypeId))
                 .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId));
        }
    }
}
