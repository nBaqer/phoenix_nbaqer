﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadPhoneMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead phone mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;

    /// <summary>
    /// The lead phone mapping.
    /// </summary>
    public class LeadPhoneMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AdLeadPhone, PhoneNumber>()
                 .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                 .ForMember(destination => destination.Phone, option => option.MapFrom(source => source.Phone))
                 .ForMember(destination => destination.Extension, option => option.MapFrom(source => source.Extension))
                 .ForMember(destination => destination.IsBest, option => option.MapFrom(source => source.IsBest))
                 .ForMember(destination => destination.IsForeignPhone, option => option.MapFrom(source => source.IsForeignPhone))
                 .ForMember(
                     destination => destination.IsShowOnLeadPage,
                     option => option.MapFrom(source => source.IsShowOnLeadPage))
                 .ForMember(destination => destination.Id, option => option.MapFrom(source => source.LeadPhoneId))
                 .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                 .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                 .ForMember(destination => destination.PhoneTypeId, option => option.MapFrom(source => source.PhoneTypeId))
                 .ForMember(destination => destination.Position, option => option.MapFrom(source => source.Position))
                 .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId));
        }
    }
}
