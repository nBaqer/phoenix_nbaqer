﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Extensions;

    /// <summary>
    /// The lead mapping.
    /// </summary>
    public class LeadMapping : IMappingDefinition
    {


        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            var map = mapper.CreateMap<AdLeads, Lead>()
                        .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                        .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.MiddleName, option => option.MapFrom(source => source.MiddleName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.InterestAreaId, option => option.MapFrom(source => source.AreaId))
                        .ForMember(destination => destination.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryId))

                        //// Phone Fields
                        .ForMember(destination => destination.Phone, option => option.MapFrom(source => source.AdLeadPhone.FirstOrDefault(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase) && x.IsBest).Phone.TrimEventIfNull()))
                        .ForMember(destination => destination.Phone, opt => opt.NullSubstitute(string.Empty))
                        .ForMember(destination => destination.PhoneTypeId, option => option.MapFrom(source => source.AdLeadPhone.FirstOrDefault(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase) && x.IsBest).PhoneTypeId))
                        .ForMember(destination => destination.PhoneTypeId, opt => opt.NullSubstitute(Guid.Empty))
                        .ForMember(destination => destination.IsUsaPhoneNumber, option => option.MapFrom(source => !source.AdLeadPhone.FirstOrDefault(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase) && x.IsBest).IsForeignPhone))

                        //// EmailAddress Fields
                        .ForMember(destination => destination.Email, option => option.MapFrom(source => source.AdLeadEmail.FirstOrDefault(x => x.IsPreferred && x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase)).Email.TrimEventIfNull()))
                        .ForMember(destination => destination.Email, opt => opt.NullSubstitute(string.Empty))
                        .ForMember(destination => destination.EmailTypeId, option => option.MapFrom(source => source.AdLeadEmail.FirstOrDefault(x => x.IsPreferred && x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase)).EmailTypeId))
                        .ForMember(destination => destination.EmailTypeId, opt => opt.NullSubstitute(Guid.Empty))

                        .ForMember(destination => destination.Address1, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).Address1.TrimEventIfNull() : string.Empty))
                        .ForMember(destination => destination.Address2, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).Address2.TrimEventIfNull() : string.Empty))
                        .ForMember(destination => destination.City, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).City.TrimEventIfNull() : string.Empty))
                        .ForMember(destination => destination.StateId, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).StateId : Guid.Empty))
                        .ForMember(destination => destination.Zip, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).ZipCode.TrimEventIfNull() : string.Empty))
                        .ForMember(destination => destination.CountyId, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).CountyId : Guid.Empty))
                        .ForMember(destination => destination.CountryId, option => option.MapFrom(source => this.GetBestAddress(source) != null ? this.GetBestAddress(source).CountryId : Guid.Empty))

                        .ForMember(destination => destination.BestTime, option => option.MapFrom(source => source.BestTime.TrimEventIfNull()))
                        .ForMember(destination => destination.BirthDate, option => option.MapFrom(source => source.BirthDate))
                        .ForMember(destination => destination.AdmissionsRepId, option => option.MapFrom(source => source.AdmissionsRep))
                        .ForMember(destination => destination.DateApplied, option => option.MapFrom(source => source.DateApplied))
                        .ForMember(destination => destination.CreateDate, option => option.MapFrom(source => source.CreatedDate))
                        .ForMember(destination => destination.SSN, option => option.MapFrom(source => source.Ssn))
                        .ForMember(destination => destination.LeadStatusId, option => option.MapFrom(source => source.LeadStatus))
                        .ForMember(destination => destination.StudentStatusId, option => option.MapFrom(source => source.StudentStatusId))
                        .ForMember(destination => destination.VendorId, option => option.MapFrom(source => source.VendorId))
                        .ForMember(destination => destination.SourceTypeId, option => option.MapFrom(source => source.SourceTypeId))
                        .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                        .ReverseMap();
        }


        /// <summary>
        /// Get the best (IsPreferred) email, bring the first or default.
        /// Store the found object on variable to reuse on the next one as shown on the GetBestEmail.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="AdLeadEmail"/>.
        /// </returns>
        private AdLeadEmail GetBestEmail(AdLeads lead)
        {

            if (lead.AdLeadEmail.Any())
            {
                var email = lead.AdLeadEmail.FirstOrDefault(x => x.Status.StatusCode == SystemStatusCodes.Active && x.IsPreferred);
                return email;
            }

            return null;
        }

        /// <summary>
        /// The get best phone.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="AdLeadPhone"/>.
        /// </returns>
        private AdLeadPhone GetBestPhone(AdLeads lead)
        {

            if (lead.AdLeadPhone.Any(x => x.IsForeignPhone == false))
            {
                var phone = lead.AdLeadPhone.FirstOrDefault(x => x.IsForeignPhone == false && x.Status.StatusCode == SystemStatusCodes.Active && x.IsBest);
                return phone;
            }

            return null;
        }


        /// <summary>
        /// Get the list of active nonInternational addresses, 
        /// Store the found object on variable to reuse on the next one as shown on the GEtBestPhone.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<Address> GetAddresses(AdLeads lead)
        {

            var addresses = new List<Address>();
            var addressList = lead.AdLeadAddresses.Where(x => (x.IsInternational == null || x.IsInternational == false) && x.Status.StatusCode == SystemStatusCodes.Active).ToList();

            if (addressList.Count() > 1)
            {
                foreach (var address in addressList)
                {
                    addresses.Add(new Address()
                    {
                        LeadId = address.LeadId,
                        AddressTypeId = address.AddressTypeId,
                        Address1 = address.Address1,
                        Address2 = address.Address2,
                        City = address.City,
                        StateId = address.StateId,
                        Zip = address.ZipCode,
                        CountryId = address.CountryId,
                        StatusId = address.StatusId,
                        IsMailingAddress = address.IsMailingAddress,
                        IsShowOnLeadPage = address.IsShowOnLeadPage,
                        IsInternational = address.IsInternational,
                        CountyId = address.CountyId
                    });
                }
            }

            return addresses;
        }

        /// <summary>
        /// Get the best (IsShowOnLeadPage) address first or the first or default one that is active. 
        /// Store the found object on variable to reuse on the next one as shown on the GetBestEmail.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="AdLeadAddresses"/>.
        /// </returns>
        private AdLeadAddresses GetBestAddress(AdLeads lead)
        {

            var addressList = lead.AdLeadAddresses.Where(x =>  x.Status.StatusCode == SystemStatusCodes.Active).Where(x => x.IsMailingAddress && x.IsShowOnLeadPage);
            var address = addressList.FirstOrDefault(x => x.IsShowOnLeadPage);
            return address;
        }
    }
}
