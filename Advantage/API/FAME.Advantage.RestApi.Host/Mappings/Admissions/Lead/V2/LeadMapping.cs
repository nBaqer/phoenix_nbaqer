﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the LeadMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead.V2
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.Host.Extensions;
    using FAME.Extensions;

    /// <summary>
    /// The lead mapping.
    /// </summary>
    public class LeadMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            var map = mapper.CreateMap<AdLeads, DataTransferObjects.Admissions.Lead.V2.Lead>()
                        .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                        .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.MiddleName, option => option.MapFrom(source => source.MiddleName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName.TrimEventIfNull().ToPascalCase()))
                        .ForMember(destination => destination.InterestAreaId, option => option.MapFrom(source => source.AreaId))
                        .ForMember(destination => destination.SourceCategoryId, option => option.MapFrom(source => source.SourceCategoryId))
                        .ForMember(destination => destination.BestTime, option => option.MapFrom(source => source.BestTime.TrimEventIfNull()))

                        .ForMember(destination => destination.Addresses, option => option.MapFrom(source => source.AdLeadAddresses.Where(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.IsShowOnLeadPage)))
                        .ForMember(destination => destination.Emails, option => option.MapFrom(source => source.AdLeadEmail.Where(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.IsPreferred).ThenBy(x => x.IsShowOnLeadPage)))
                        .ForMember(destination => destination.Phones, option => option.MapFrom(source => source.AdLeadPhone.Where(x => x.Status.StatusCode.Equals(SystemStatusCodes.Active, StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.IsBest).ThenBy(x => x.IsShowOnLeadPage)))

                        .ForMember(destination => destination.BirthDate, option => option.MapFrom(source => source.BirthDate))
                        .ForMember(destination => destination.AdmissionsRepId, option => option.MapFrom(source => source.AdmissionsRep))
                        .ForMember(destination => destination.DateApplied, option => option.MapFrom(source => source.DateApplied))
                        .ForMember(destination => destination.CreateDate, option => option.MapFrom(source => source.CreatedDate))
                        .ForMember(destination => destination.SSN, option => option.MapFrom(source => source.Ssn))
                        .ForMember(destination => destination.LeadStatusId, option => option.MapFrom(source => source.LeadStatus))
                        .ForMember(destination => destination.StudentStatusId, option => option.MapFrom(source => source.StudentStatusId))
                        .ForMember(destination => destination.VendorId, option => option.MapFrom(source => source.VendorId))
                        .ForMember(destination => destination.SourceTypeId, option => option.MapFrom(source => source.SourceTypeId))
                        .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                        .ReverseMap();
        }

        /// <summary>
        /// Get the list of active nonInternational addresses, 
        /// Store the found object on variable to reuse on the next one as shown on the GEtBestPhone.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<Address> GetAddresses(AdLeads lead)
        {
            var addresses = new List<Address>();
            var addressList = lead.AdLeadAddresses.Where(x => (x.IsInternational == null || x.IsInternational == false) && x.Status.StatusCode == SystemStatusCodes.Active).ToList();

            if (addressList.Count > 0)
            {
                foreach (var address in addressList)
                {
                    addresses.Add(new Address()
                    {
                        Id = address.AdLeadAddressId,
                        LeadId = address.LeadId,
                        AddressTypeId = address.AddressTypeId,
                        Address1 = address.Address1,
                        Address2 = address.Address2,
                        City = address.City,
                        StateId = address.StateId,
                        Zip = address.ZipCode,
                        CountryId = address.CountryId,
                        StatusId = address.StatusId,
                        IsMailingAddress = address.IsMailingAddress,
                        IsShowOnLeadPage = address.IsShowOnLeadPage,
                        IsInternational = address.IsInternational,
                        CountyId = address.CountyId
                    });
                }
            }

            return addresses;
        }

        /// <summary>
        /// Get the list of active nonInternational addresses, 
        /// Store the found object on variable to reuse on the next one as shown on the GEtBestPhone.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<EmailAddress> GetEmails(AdLeads lead)
        {

            var emails = new List<EmailAddress>();
            var emailList = lead.AdLeadEmail.Where(x => x.Status.StatusCode == SystemStatusCodes.Active).ToList();

            if (emailList.Count > 0)
            {
                foreach (var email in emailList)
                {
                    emails.Add(new EmailAddress()
                    {
                        LeadId = email.LeadId,
                        Email = email.Email,
                        IsPreferred = email.IsPreferred,
                        IsPortalUserName = email.IsPortalUserName,
                        IsShowOnLeadPage = email.IsShowOnLeadPage,
                        Id = email.LeadEmailId,
                        ModifiedDate = email.ModDate,
                        ModifiedUser = email.ModUser,
                        EmailTypeId = email.EmailTypeId,
                        StatusId = email.StatusId
                    });
                }
            }

            return emails;
        }

        /// <summary>
        /// Get the list of active nonInternational addresses, 
        /// Store the found object on variable to reuse on the next one as shown on the GEtBestPhone.
        /// </summary>
        /// <param name="lead">
        /// The lead.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<PhoneNumber> GetPhones(AdLeads lead)
        {
            var phones = new List<PhoneNumber>();
            var phoneList = lead.AdLeadPhone.Where(x => x.Status.StatusCode == SystemStatusCodes.Active).ToList();

            if (phoneList.Count > 0)
            {
                foreach (var phone in phoneList)
                {
                    phones.Add(new PhoneNumber()
                    {
                        LeadId = phone.LeadId,
                        Phone = phone.Phone,
                        Extension = phone.Extension,
                        IsBest = phone.IsBest,
                        IsForeignPhone = phone.IsForeignPhone,
                        IsShowOnLeadPage = phone.IsShowOnLeadPage,
                        Id = phone.LeadPhoneId,
                        ModifiedDate = phone.ModDate,
                        ModifiedUser = phone.ModUser,
                        PhoneTypeId = phone.PhoneTypeId,
                        Position = phone.Position,
                        StatusId = phone.StatusId,
                    });
                }
            }

            return phones;
        }
    }
}
