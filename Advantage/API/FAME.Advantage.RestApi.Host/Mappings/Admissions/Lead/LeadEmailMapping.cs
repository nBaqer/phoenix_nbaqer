﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LeadEmailMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The lead email mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Admissions.Lead
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Admissions.Lead;

    /// <summary>
    /// The lead email mapping.
    /// </summary>
    public class LeadEmailMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AdLeadEmail, EmailAddress>()
                 .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                 .ForMember(destination => destination.Email, option => option.MapFrom(source => source.Email))
                 .ForMember(destination => destination.IsPreferred, option => option.MapFrom(source => source.IsPreferred))
                 .ForMember(destination => destination.IsPortalUserName, option => option.MapFrom(source => source.IsPortalUserName))
                 .ForMember(
                     destination => destination.IsShowOnLeadPage,
                     option => option.MapFrom(source => source.IsShowOnLeadPage))
                 .ForMember(destination => destination.Id, option => option.MapFrom(source => source.LeadEmailId))
                 .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                 .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                 .ForMember(destination => destination.EmailTypeId, option => option.MapFrom(source => source.EmailTypeId))
                 .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId));
        }
    }
}
