﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMappingDefinition.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IMappingDefinition type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings
{
    using AutoMapper.Configuration;

    /// <summary>
    /// The Mapping Definition interface.
    /// Implement this interface on your class that defines the manual mapping between the entities.
    /// </summary>
    public interface IMappingDefinition
    {
        /// <summary>
        /// The map entities.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        void MapEntity(MapperConfigurationExpression mapper);
    }
}
