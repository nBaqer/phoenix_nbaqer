﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WapiOperationLoggerMapping.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the WapiOperationLoggerMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Mappings.Afa
{
    using AutoMapper.Configuration;
    /// <summary>
    /// The WapiOperationLoggerMapping class 
    /// </summary>
    public class WapiOperationLoggerMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<DataTransferObjects.AFA.WapiOperationLogger, Fame.EFCore.Advantage.Entities.SyWapiOperationLogger>()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.Id))
                .ForMember(destination => destination.ServiceCode, option => option.MapFrom(source => source.ServiceCode))
                .ForMember(destination => destination.CompanyCode,option => option.MapFrom(source => source.CompanyCode))
                .ForMember(destination => destination.DateExecution, option => option.MapFrom(source => source.DateExecution))
                .ForMember(destination => destination.NextPlanningDate, option => option.MapFrom(source => source.NextPlanningDate))
                .ForMember(destination => destination.IsError, option => option.MapFrom(source => source.IsError))
                .ForMember(destination => destination.Comment, option => option.MapFrom(source=>source.Comment));
        }
    }
}
