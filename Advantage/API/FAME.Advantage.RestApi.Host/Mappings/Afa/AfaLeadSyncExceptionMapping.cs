﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaLeadSyncExceptionMapping.cs" company="Fame Inc">
//   2018
// </copyright>
// <summary>
//   Defines the AfaLeadSyncExceptionMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.Afa
{
    using AutoMapper.Configuration;

    /// <summary>
    /// The AFA lead sync exception mapping.
    /// </summary>
    public class AfaLeadSyncExceptionMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<DataTransferObjects.AFA.AFALeadSyncException,Fame.EFCore.Advantage.Entities.AfaLeadSyncException > ()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.Id))
                .ForMember(destination => destination.LeadId, option => option.MapFrom(source => source.LeadId))
                .ForMember(
                    destination => destination.AfaStudentId,
                    option => option.MapFrom(source => source.AfaStudentId))
                .ForMember(destination => destination.FirstName, option => option.MapFrom(source => source.FirstName))
                .ForMember(destination => destination.LastName, option => option.MapFrom(source => source.LastName))
                .ForMember(destination => destination.ExceptionReason,option => option.MapFrom(source => source.ExceptionReason))
                .ForMember(destination => destination.Lead, option => option.Ignore());
        }
    }
}
