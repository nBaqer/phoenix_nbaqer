﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FAME.Advantage.RestApi.Host.Mappings.Afa
{
    using AutoMapper.Configuration;

    public class AfaProgramVersionMapping : IMappingDefinition
    {
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<Fame.EFCore.Advantage.Entities.ArPrgVersions, DataTransferObjects.AFA.AdvStagingProgramV1>()
                .ForMember(
                    destination => destination.ProgramVersionID,
                    option => option.MapFrom(source => source.PrgVerId))
                .ForMember(
                    destination => destination.ProgramName,
                    option => option.MapFrom(source => source.PrgVerDescrip))
                .ForMember(destination => destination.DateCreated, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.UserCreated, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.DateUpdated, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.UserUpdated, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.DateUpdated, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.LocationCMSID, option => option.Ignore());
        }
    }
}
