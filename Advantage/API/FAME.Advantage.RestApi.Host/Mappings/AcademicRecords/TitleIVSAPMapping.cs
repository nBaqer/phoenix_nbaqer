﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVSAPMapping.cs" company="Fame Inc.">
//   Fame Inc. 2018
// </copyright>
// <summary>
//   The title ivsap mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.TitleIVSAP;

    /// <summary>
    /// The title ivsap mapping.
    /// </summary>
    public class TitleIVSAPMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArFasapchkResults, TitleIVSAPResult>()
                .ForMember(destination => destination.IsMakingSAP, option => option.MapFrom(source => source.IsMakingSap))
                .ForMember(destination => destination.TriggeredAt, option => option.MapFrom(source => source.Sapdetail.TrigValue))
                .ForMember(destination => destination.Reason, option => option.MapFrom(source => source.Sapdetail.TrigValue))
                .ForMember(destination => destination.Increment, option => option.MapFrom(source => source.Sapdetail.TrigOffsetSeq));
        }
    }
}
