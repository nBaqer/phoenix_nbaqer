﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSchedulesMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student schedules mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The student schedules mapping.
    /// </summary>
    public class StudentSchedulesMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to arStudentSchedules
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArStudentSchedules, StudentScheduleInfo>()
                .ForMember(destination => destination.StudentScheduleId,
                    option => option.MapFrom(source => source.StuScheduleId))
                .ForMember(destination => destination.ScheduleId, option => option.MapFrom(source => source.ScheduleId))
                .ForMember(destination => destination.EnrollmentId,
                    option => option.MapFrom(source => source.StuEnrollId));
        }
    }
}
