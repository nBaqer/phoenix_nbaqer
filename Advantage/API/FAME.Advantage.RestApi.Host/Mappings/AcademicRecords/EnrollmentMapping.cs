﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnrollmentMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The enrollment mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using System.Collections.Generic;
    using System.Linq;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Orm.Advantage.Domain.Common;

    /// <summary>
    /// The enrollment mapping.
    /// </summary>
    public class EnrollmentMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of Enrollments
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            var activeStatusIds =
                new List<int>
                    {
                        (int)Constants.SystemStatus.FutureStart,
                        (int)Constants.SystemStatus.CurrentlyAttending,
                        (int)Constants.SystemStatus.LeaveOfAbsence,
                        (int)Constants.SystemStatus.Suspension,
                        (int)Constants.SystemStatus.AcademicProbation,
                        (int)Constants.SystemStatus.Externship,
                        (int)Constants.SystemStatus.DisciplinaryProbation,
                        (int)Constants.SystemStatus.WarningProbation
                    };

            mapper.CreateMap<ArStuEnrollments, DataTransferObjects.AcademicRecords.Enrollment.Enrollment>()
                .ForMember(destination => destination.EnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.ProgramVersionDescription, option => option.MapFrom(source => source.PrgVer.PrgVerDescrip))
                .ForMember(destination => destination.StatusCodeDescription, option => option.MapFrom(source => source.StatusCode.StatusCodeDescrip))
                .ForMember(destination => destination.EffectiveDate, option => option.MapFrom(source => source.SyStudentStatusChanges.Max(sc => sc.DateOfChange) != null ? source.SyStudentStatusChanges.Max(sc => sc.DateOfChange) : source.SyStudentStatusChanges.Max(sc => sc.ModDate)))
                .ForMember(destination => destination.Status, option => option.MapFrom(source => activeStatusIds.Contains(source.StatusCode.SysStatusId) ? "Active" : "Inactive"))
                .ForMember(destination => destination.EnrollmentDate, option => option.MapFrom(source => source.EnrollDate))
                .ForMember(destination => destination.DateDetermined, option => option.MapFrom(source => source.DateDetermined))
                .ForMember(destination => destination.LastDateAttended, option => option.MapFrom(source => source.Lda))
                .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.StatusCode, option => option.MapFrom(source => source.StatusCode.StatusCode))
                .ForMember(destination => destination.UnitTypeDescription, option => option.MapFrom(source => source.PrgVer.UnitType.UnitTypeDescrip))
                .ForMember(destination => destination.StudentId, option => option.MapFrom(source => source.StudentId))
                .ForMember(destination => destination.SSN, option => option.MapFrom(source => source.Lead.Ssn))
                .ForMember(destination => destination.SystemStatusId, option => option.MapFrom(source => source.StatusCode.SysStatusId))
                .ForMember(destination => destination.DropReasonId, option => option.MapFrom(source => source.DropReasonId))
                .ForMember(destination => destination.StatusCodeId, option => option.MapFrom(source => source.StatusCodeId))
                .ForMember(destination => destination.PrgVerId, option => option.MapFrom(source => source.PrgVerId))
                .ForMember(destination => destination.TransferHours, option => option.MapFrom(source => source.TransferHours))
                .ForMember(destination => destination.TransferDate, option => option.MapFrom(source => source.TransferDate))
                .ForMember(destination => destination.ExpectedGraduationDate, option => option.MapFrom(source => source.ExpGradDate))
                .ForMember(destination => destination.CalculationPeriodTypeId, option => option.MapFrom(source => source.PrgVer.ArCampusPrgVersions.FirstOrDefault(x => x.CampusId == source.CampusId).CalculationPeriodTypeId))
                .ForMember(destination => destination.ResultStatus, option => option.Ignore());
        }
    }
}