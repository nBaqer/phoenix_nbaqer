﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionMeetingMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The class section meeting mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSectionMeeting;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The class section mapping between entity ArClsSectionMeetings and ClassSectionMeetingDetail.
    /// </summary>
    public class ClassSectionMeetingMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArClsSectMeetings, ClassSectionMeetingDetail>()
                .ForMember(
                    destination => destination.ClassSectionId,
                    option => option.MapFrom(source => source.ClsSectionId))
                .ForMember(
                    destination => destination.StartDate,
                    option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.EndDate,
                    option => option.MapFrom(source => source.EndDate))
                .ForMember(
                    destination => destination.PeriodId,
                    option => option.MapFrom(source => source.PeriodId))
                .ForMember(
                    destination => destination.ClassSectionMeetingId,
                    option => option.MapFrom(source => source.ClsSectMeetingId));
        }
    }
}
