﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TerminationDocumentHistoryMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TerminationDocumentHistoryMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;

    /// <summary>
    /// The student docs mapping.
    /// </summary>
    public class TerminationDocumentHistoryMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity class creates mapping between SyDocumentHistory and TerminationDocumentHistory classes.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyDocumentHistory, TerminationDocumentHistory>()
                .ForMember(
                    destination => destination.DisplayName,
                    option => option.MapFrom(source => source.DisplayName))
                .ForMember(destination => destination.DocumentId, option => option.MapFrom(source => source.DocumentId))
                .ForMember(
                    destination => destination.DocumentType,
                    option => option.MapFrom(source => source.DocumentType))
                .ForMember(
                    destination => destination.FileExtension,
                    option => option.MapFrom(source => source.FileExtension))
                .ForMember(destination => destination.FileId, option => option.MapFrom(source => source.FileId))
                .ForMember(destination => destination.FileName, option => option.MapFrom(source => source.FileName))
                .ForMember(destination => destination.IsArchived, option => option.MapFrom(source => source.IsArchived))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModuleId, option => option.MapFrom(source => source.ModuleId))
                .ForMember(destination => destination.StudentId, option => option.MapFrom(source => source.StudentId))
                .ForMember(
                    destination => destination.FileNameOnly,
                    option => option.MapFrom(source => source.FileNameOnly))
            .ForMember(destination => destination.Lead, option => option.Ignore())
            .ForMember(destination => destination.LeadId, option => option.Ignore()).ForMember(
                destination => destination.Document,
                option => option.Ignore());
        }
    }
}
