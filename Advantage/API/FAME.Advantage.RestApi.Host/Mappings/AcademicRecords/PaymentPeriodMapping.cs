﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriodMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The payment period mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.PaymentPeriod;

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The payment period mapping.
    /// </summary>
    public class PaymentPeriodMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<PaymentPeriod, AfaPaymentPeriodStaging>()
                .ForMember(
                    destination => destination.AfaStudentId,
                    option => option.MapFrom(source => source.AfaStudentId))
                .ForMember(
                    destination => destination.StudentEnrollmentId,
                    option => option.MapFrom(source => source.StudentEnrollmentID))
                .ForMember(
                    destination => destination.AcademicYearSequenceNum,
                    option => option.MapFrom(source => source.AcademicYearSeqNo))
                .ForMember(
                    destination => destination.Description,
                    option => option.MapFrom(source => source.PaymentPeriodName))
                .ForMember(
                    destination => destination.StartDate,
                    option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.EndDate,
                    option => option.MapFrom(source => source.EndDate))
                .ForMember(
                    destination => destination.WeeksInstructionalTime,
                    option => option.MapFrom(source => source.WeeksOfInstructionalTime))
                .ForMember(
                    destination => destination.CreditOrHours,
                    option => option.MapFrom(source => source.HoursCreditEnrolled))
                .ForMember(
                    destination => destination.Status,
                    option => option.MapFrom(source => source.EnrollmentStatusDescription))
                .ForMember(
                    destination => destination.CreditOrHoursEarned,
                    option => option.MapFrom(source => source.HoursCreditEarned))
                .ForMember(
                    destination => destination.CreditOrHoursEarnedEffectiveDate,
                    option => option.Condition(source => (source.EffectiveDate.HasValue)))
                .ForMember(
                    destination => destination.TitleIvsapresult,
                    option => option.MapFrom(source => source.TitleIVSapResult))
                .ForMember(
                    destination => destination.DateCreated,
                    option => option.MapFrom(source => source.DateCreated))
                .ForMember(
                    destination => destination.UserCreated,
                    option => option.MapFrom(source => source.UserCreated))
                .ForMember(
                    destination => destination.DeleteRecord,
                    option => option.MapFrom(source => source.DeleteRecord));


            mapper.CreateMap<AfaPaymentPeriodStaging, PaymentPeriod>()
                .ForMember(
                    destination => destination.AfaStudentId,
                    option => option.MapFrom(source => source.AfaStudentId))
                .ForMember(
                    destination => destination.StudentEnrollmentID,
                    option => option.MapFrom(source => source.StudentEnrollmentId))
                .ForMember(
                    destination => destination.AcademicYearSeqNo,
                    option => option.MapFrom(source => source.AcademicYearSequenceNum))
                .ForMember(
                    destination => destination.PaymentPeriodName,
                    option => option.MapFrom(source => source.Description))
                .ForMember(
                    destination => destination.StartDate,
                    option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.EndDate,
                    option => option.MapFrom(source => source.EndDate))
                .ForMember(
                    destination => destination.WeeksOfInstructionalTime,
                    option => option.MapFrom(source => source.WeeksInstructionalTime))
                .ForMember(
                    destination => destination.HoursCreditEnrolled,
                    option => option.MapFrom(source => source.CreditOrHours))
                .ForMember(
                    destination => destination.EnrollmentStatusDescription,
                    option => option.MapFrom(source => source.Status))
                .ForMember(
                    destination => destination.HoursCreditEarned,
                    option => option.MapFrom(source => source.CreditOrHoursEarned))
                .ForMember(
                    destination => destination.EffectiveDate,
                    option => option.MapFrom(source => source.CreditOrHoursEarnedEffectiveDate))
                .ForMember(
                    destination => destination.TitleIVSapResult,
                    option => option.MapFrom(source => source.TitleIvsapresult))
                .ForMember(
                    destination => destination.DateCreated,
                    option => option.MapFrom(source => source.DateCreated))
                .ForMember(
                    destination => destination.UserCreated,
                    option => option.MapFrom(source => source.UserCreated))
                .ForMember(
                    destination => destination.DeleteRecord,
                    option => option.MapFrom(source => source.DeleteRecord));

        }
    }
}
