﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodDetailsMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The period details mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The period details mapping maps EnrollmentPeriodDetails and ProgramPeriodDetails.
    /// </summary>
    public class PeriodDetailsMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the EnrollmentPeriodDetails and ProgramPeriodDetails.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<EnrollmentPeriodDetails, ProgramPeriodDetails>()
                .ForMember(destination => destination.PeriodDetailsList, option => option.MapFrom(source => source.EnrollmentPeriodsList))
                .ForMember(
                    destination => destination.NumberOfPaymentPeriods,
                    option => option.MapFrom(source => source.NumberOfPeriodOfEnrollment))
                .ForMember(
                    destination => destination.NumberOfFullAcademicYears,
                    option => option.MapFrom(source => source.NumberOfFullAcademicYears)).ForMember(
                    destination => destination.LastPaymentPeriodLength,
                    option => option.MapFrom(source => source.LastEnrollmentPeriodLength)).ForMember(
                    destination => destination.ResultStatus,
                    option => option.MapFrom(source => source.ResultStatus));
        }
    }
}
