﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ClassSectionMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The class section mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using System;

    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ClassSection;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The class section mapping.
    /// </summary>
    public class ClassSectionMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArClassSections, ClassSectionDetail>()
                .ForMember(
                    destination => destination.ClassSectionId,
                    option => option.MapFrom(source => source.ClsSectionId))
                .ForMember(
                    destination => destination.StartDate,
                    option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.EndDate,
                    option => option.MapFrom(source => source.EndDate))
                .ForMember(
                    destination => destination.ReqId,
                    option => option.MapFrom(source => source.ReqId))
                .ForMember(
                    destination => destination.TermId,
                    option => option.MapFrom(source => source.TermId));
        }
    }
}
