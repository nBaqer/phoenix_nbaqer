﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentStatusMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentStatusMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;

    /// <summary>
    /// The student status mapping.
    /// </summary>
    public class StudentStatusMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to SyStudentStatusChanges
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<SyStudentStatusChanges, StudentStatusChanges>()
                .ForMember(destination => destination.StudentStatusChangeId, option => option.MapFrom(source => source.StudentStatusChangeId))
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.OriginalStatusId, option => option.MapFrom(source => source.OrigStatusId))
                .ForMember(destination => destination.NewStatusId, option => option.MapFrom(source => source.NewStatusId))
                .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.CampusId))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.IsReversal, option => option.MapFrom(source => source.IsReversal))
                .ForMember(destination => destination.DropReasonId, option => option.MapFrom(source => source.DropReasonId))
                .ForMember(destination => destination.DateOfChange, option => option.MapFrom(source => source.DateOfChange))
                .ForMember(destination => destination.LastdateAttended, option => option.MapFrom(source => source.Lda))
                .ForMember(destination => destination.CaseNumber, option => option.MapFrom(source => source.CaseNumber))
                .ForMember(destination => destination.RequestedBy, option => option.MapFrom(source => source.RequestedBy))
                .ForMember(destination => destination.HaveBackup, option => option.MapFrom(source => source.HaveBackup))
                .ForMember(destination => destination.HaveClientConfirmation, option => option.MapFrom(source => source.HaveClientConfirmation))
                .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollId)).ReverseMap();
        }
    }
}