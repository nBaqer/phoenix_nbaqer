﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TransferGradesMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Transfer Grades Mapping mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
   using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The transfer grades mapping.
    /// </summary>
    public class TransferGradesMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<ArTransferGrades, DataTransferObjects.AcademicRecords.TransferGrades.TransferGradesDetails>()
                .ForMember(destination => destination.TransferId, option => option.MapFrom(source => source.TransferId))
                .ForMember(destination => destination.StuEnrollId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.ReqId, option => option.MapFrom(source => source.ReqId))
                .ForMember(destination => destination.GrdSysDetailId, option => option.MapFrom(source => source.GrdSysDetailId))
                .ForMember(destination => destination.Score, option => option.MapFrom(source => source.Score))
                .ForMember(destination => destination.TermId, option => option.MapFrom(source => source.TermId))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.IsTransferred, option => option.MapFrom(source => source.IsTransferred))
                .ForMember(destination => destination.CompletedDate, option => option.MapFrom(source => source.CompletedDate))
                .ForMember(destination => destination.IsClinicsSatisfied, option => option.MapFrom(source => source.IsClinicsSatisfied))
                .ForMember(destination => destination.IsCourseCompleted, option => option.MapFrom(source => source.IsCourseCompleted))
                .ForMember(destination => destination.IsGradeOverridden, option => option.MapFrom(source => source.IsGradeOverridden))
                .ForMember(destination => destination.GradeOverriddenBy, option => option.MapFrom(source => source.GradeOverriddenBy))
                .ForMember(destination => destination.GradeOverriddenDate, option => option.MapFrom(source => source.GradeOverriddenDate));
            }
    }
}
