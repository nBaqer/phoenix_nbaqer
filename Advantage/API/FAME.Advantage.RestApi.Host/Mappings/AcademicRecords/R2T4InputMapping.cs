﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4InputMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4InputMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Input;
    using Fame.EFCore.Advantage.Entities;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 input mapping. This will provide a mapping between DTO and Entitiy of R2T4 Input model
    /// </summary>
    public class R2T4InputMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity class creates mapping between R2T4InputEntity and R2T4Input classes.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArR2t4input, R2T4Input>()

                .ForMember(
                    destination => destination.R2T4InputId,
                    option => option.MapFrom(source => source.R2t4inputId))
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.ProgramUnitTypeId,
                    option => option.MapFrom(source => source.ProgramUnitTypeId))
                .ForMember(
                    destination => destination.PellGrantDisbursed,
                    option => option.MapFrom(source => source.PellGrantDisbursed))
                .ForMember(
                    destination => destination.PellGrantCouldDisbursed,
                    option => option.MapFrom(source => source.PellGrantCouldDisbursed))
                .ForMember(
                    destination => destination.FseogDisbursed,
                    option => option.MapFrom(source => source.Fseogdisbursed))
                .ForMember(
                    destination => destination.FseogCouldDisbursed,
                    option => option.MapFrom(source => source.FseogcouldDisbursed))
                .ForMember(
                    destination => destination.TeachGrantDisbursed,
                    option => option.MapFrom(source => source.TeachGrantDisbursed))
                .ForMember(
                    destination => destination.TeachGrantCouldDisbursed,
                    option => option.MapFrom(source => source.TeachGrantCouldDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantCouldDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantCouldDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanCouldDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.IsAttendanceNotRequired,
                    option => option.MapFrom(source => source.IsAttendanceNotRequired))
                .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.ScheduledEndDate,
                    option => option.MapFrom(source => source.ScheduledEndDate))
                .ForMember(
                    destination => destination.WithdrawalDate,
                    option => option.MapFrom(source => source.WithdrawalDate))
                .ForMember(
                    destination => destination.CompletedTime,
                    option => option.MapFrom(source => source.CompletedTime))
                .ForMember(destination => destination.TotalTime, option => option.MapFrom(source => source.TotalTime))
                .ForMember(destination => destination.TuitionFee, option => option.MapFrom(source => source.TuitionFee))
                .ForMember(destination => destination.RoomFee, option => option.MapFrom(source => source.RoomFee))
                .ForMember(destination => destination.BoardFee, option => option.MapFrom(source => source.BoardFee))
                .ForMember(destination => destination.OtherFee, option => option.MapFrom(source => source.OtherFee))
                .ForMember(
                    destination => destination.IsTuitionChargedByPaymentPeriod,
                    option => option.MapFrom(source => source.IsTuitionChargedByPaymentPeriod))
                .ForMember(
                    destination => destination.CreditBalanceRefunded,
                    option => option.MapFrom(source => source.CreditBalanceRefunded))
                .ForMember(destination => destination.CreatedBy, option => option.MapFrom(source => source.CreatedById))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(destination => destination.UpdatedBy, option => option.MapFrom(source => source.UpdatedById))
                .ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate)).ForMember(
                    destination => destination.ResultStatus,
                    option => option.Ignore())

                //Reverse mapping
                .ReverseMap()

                //Ignore navigational properties
                .ForMember(destination => destination.Termination, option => option.Ignore())
                .ForMember(destination => destination.CreatedBy, option => option.Ignore())
                .ForMember(destination => destination.UpdatedBy, option => option.Ignore())
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.ProgramUnitTypeId,
                    option => option.MapFrom(source => source.ProgramUnitTypeId))
                .ForMember(
                    destination => destination.PellGrantDisbursed,
                    option => option.MapFrom(source => source.PellGrantDisbursed))
                .ForMember(
                    destination => destination.PellGrantCouldDisbursed,
                    option => option.MapFrom(source => source.PellGrantCouldDisbursed))
                .ForMember(
                    destination => destination.Fseogdisbursed,
                    option => option.MapFrom(source => source.FseogDisbursed))
                .ForMember(
                    destination => destination.FseogcouldDisbursed,
                    option => option.MapFrom(source => source.FseogCouldDisbursed))
                .ForMember(
                    destination => destination.TeachGrantDisbursed,
                    option => option.MapFrom(source => source.TeachGrantDisbursed))
                .ForMember(
                    destination => destination.TeachGrantCouldDisbursed,
                    option => option.MapFrom(source => source.TeachGrantCouldDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantCouldDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantCouldDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanCouldDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.IsAttendanceNotRequired,
                    option => option.MapFrom(source => source.IsAttendanceNotRequired))
                .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.ScheduledEndDate,
                    option => option.MapFrom(source => source.ScheduledEndDate))
                .ForMember(
                    destination => destination.WithdrawalDate,
                    option => option.MapFrom(source => source.WithdrawalDate))
                .ForMember(
                    destination => destination.CompletedTime,
                    option => option.MapFrom(source => source.CompletedTime))
                .ForMember(destination => destination.TotalTime, option => option.MapFrom(source => source.TotalTime))
                .ForMember(destination => destination.TuitionFee, option => option.MapFrom(source => source.TuitionFee))
                .ForMember(destination => destination.RoomFee, option => option.MapFrom(source => source.RoomFee))
                .ForMember(destination => destination.BoardFee, option => option.MapFrom(source => source.BoardFee))
                .ForMember(destination => destination.OtherFee, option => option.MapFrom(source => source.OtherFee))
                .ForMember(
                    destination => destination.IsTuitionChargedByPaymentPeriod,
                    option => option.MapFrom(source => source.IsTuitionChargedByPaymentPeriod))
                .ForMember(
                    destination => destination.CreditBalanceRefunded,
                    option => option.MapFrom(source => source.CreditBalanceRefunded))
                .ForMember(destination => destination.CreatedById, option => option.MapFrom(source => source.CreatedBy))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(destination => destination.UpdatedById, option => option.MapFrom(source => source.UpdatedBy))
                .ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate))
            .ForMember(
            destination => destination.IsR2t4inputCompleted,
            option => option.MapFrom(source => source.IsR2T4InputCompleted));
        }
    }
}
