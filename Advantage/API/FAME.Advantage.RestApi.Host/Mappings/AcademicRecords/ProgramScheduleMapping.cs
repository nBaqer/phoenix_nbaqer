﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration; 
    using Fame.EFCore.Advantage.Entities; 
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;

    /// <summary>
    /// The program schedule mapping between Entity ArProgSchedules and DTO ProgramSchedule
    /// </summary>
    public class ProgramScheduleMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArProgSchedules, ProgramSchedule>()
                .ForMember(destination => destination.Active, option => option.MapFrom(source => source.Active))
                .ForMember(destination => destination.Code, option => option.MapFrom(source => source.Code))
                .ForMember(destination => destination.Description, option => option.MapFrom(source => source.Descrip))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ProgramVersion, option => option.MapFrom(source => source.PrgVer))
                .ForMember(destination => destination.ProgramVersionId, option => option.MapFrom(source => source.PrgVerId))
                .ForMember(destination => destination.ScheduleId, option => option.MapFrom(source => source.ScheduleId))
                .ForMember(destination => destination.UseFlexTime, option => option.MapFrom(source => source.UseFlexTime));
        }
    }
}
