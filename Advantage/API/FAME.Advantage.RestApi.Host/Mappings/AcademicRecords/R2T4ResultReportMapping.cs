﻿// </copyright>
// <summary>
//   Defines the R2T4OverrideResultReportMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;
    using FAME.Advantage.RestApi.Host.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 override result mapping. This will provide a mapping between DTO and Entitiy of R2T4 result model
    /// </summary>
    public class R2T4ResultReportMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity class creates mapping between R2T4ResultEntity and R2T4Result classes.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArR2t4results, R2T4ResultReport>()
                .ForMember(
                    destination => destination.R2T4ResultsId,
                    option => option.MapFrom(source => source.R2t4resultsId))
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.SubTotalAmountDisbursedA,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalAmountDisbursedA, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalAmountCouldDisbursedC,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalAmountCouldDisbursedC, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedB,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalNetAmountDisbursedB, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedD,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalNetAmountDisbursedD, string.Empty)))
                .ForMember(
                    destination => destination.BoxEResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxEresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxFResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxFresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxGResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxGresult, string.Empty)))
                .ForMember(
                    destination => destination.PercentageOfActualAttendance,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PercentageOfActualAttendence, FieldType.Number)))
                .ForMember(
                    destination => destination.BoxHResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxHresult, FieldType.Number)))
                .ForMember(
                    destination => destination.BoxIResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxIresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxJResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxJresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxKResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxKresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxLResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxLresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxMResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxMresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxNResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxNresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxOResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxOresult, string.Empty)))
                .ForMember(
                    destination => destination.UnsubDirectLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.UnsubDirectLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.SubDirectLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubDirectLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.PerkinsLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PerkinsLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.DirectGraduatePlusLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.DirectParentPlusLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.DirectParentPlusLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.BoxPResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxPresult, string.Empty)))
                .ForMember(
                    destination => destination.PellGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PellGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.FseogSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.FseogschoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.TeachGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.TeachGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.IraqAfgGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.IraqAfgGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.BoxQResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxQresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxRResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxRresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxSResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxSresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxTResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxTresult, string.Empty)))
                .ForMember(
                    destination => destination.BoxUResult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxUresult, string.Empty)))
                .ForMember(
                    destination => destination.PellGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PellGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.FseogAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.FseogamountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.TeachGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.TeachGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.IraqAfgGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.IraqAfgGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.PostWithdrawalData,
                    option => option.MapFrom(source => source.PostWithdrawalData))
                .ForMember(
                    destination => destination.OverriddenData,
                    option => option.MapFrom(source => source.OverriddenData))
                .ForMember(
                    destination => destination.CreatedById,
                    option => option.MapFrom(source => source.CreatedById))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(
                    destination => destination.UpdatedById,
                    option => option.MapFrom(source => source.UpdatedById)).ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate)).ForMember(
                    destination => destination.ResultStatus,
                    option => option.Ignore())

                // Reverse mapping
                .ReverseMap()

                // Ignore navigational properties
                .ForMember(destination => destination.Termination, option => option.Ignore())
                .ForMember(destination => destination.CreatedBy, option => option.Ignore())
                .ForMember(destination => destination.UpdatedBy, option => option.Ignore())
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.SubTotalAmountDisbursedA,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalAmountDisbursedA, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalAmountCouldDisbursedC,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalAmountCouldDisbursedC, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedB,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalNetAmountDisbursedB, string.Empty)))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedD,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubTotalNetAmountDisbursedD, string.Empty)))
                .ForMember(
                    destination => destination.BoxEresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxEResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxFresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxFResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxGresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxGResult, string.Empty)))
                .ForMember(
                    destination => destination.PercentageOfActualAttendence,
                    option => option.MapFrom(source => source.PercentageOfActualAttendance))
                .ForMember(
                    destination => destination.BoxHresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxHResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxIresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxIResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxJresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxJResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxKresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxKResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxLresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxLResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxMresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxMResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxNresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxNResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxOresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxOResult, string.Empty)))
                .ForMember(
                    destination => destination.UnsubDirectLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.UnsubDirectLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.SubDirectLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.SubDirectLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.PerkinsLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PerkinsLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.DirectGraduatePlusLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.DirectParentPlusLoanSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.DirectParentPlusLoanSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.BoxPresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxPResult, string.Empty)))
                .ForMember(
                    destination => destination.PellGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PellGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.FseogschoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.FseogSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.TeachGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.TeachGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.IraqAfgGrantSchoolReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.IraqAfgGrantSchoolReturn, string.Empty)))
                .ForMember(
                    destination => destination.BoxQresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxQResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxRresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxRResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxSresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxSResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxTresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxTResult, string.Empty)))
                .ForMember(
                    destination => destination.BoxUresult,
                    option => option.MapFrom(source => Utility.FormatValueByType(source.BoxUResult, string.Empty)))
                .ForMember(
                    destination => destination.PellGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.PellGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.FseogamountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.FseogAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.TeachGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.TeachGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.IraqAfgGrantAmountToReturn,
                    option => option.MapFrom(
                        source => Utility.FormatValueByType(source.IraqAfgGrantAmountToReturn, string.Empty)))
                .ForMember(
                    destination => destination.CreatedById,
                    option => option.MapFrom(source => source.CreatedById))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(
                    destination => destination.UpdatedById,
                    option => option.MapFrom(source => source.UpdatedById)).ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate))
            .ForMember(
                destination => destination.IsR2t4resultsCompleted,
                option => option.MapFrom(source => source.IsR2T4ResultsCompleted));
        }
    }
}
