﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ResultMapping.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   The result mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities; 
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Results;

    /// <inheritdoc />
    /// <summary>
    /// The result mapping provides a custom mapping between ArResults and Results.
    /// </summary>
    public class ResultMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArResults, Results>()
                .ForMember(destination => destination.ResultId, option => option.MapFrom(source => source.ResultId))
                .ForMember(destination => destination.ClassSectionId, option => option.MapFrom(source => source.TestId))
                .ForMember(destination => destination.Score, option => option.MapFrom(source => source.Score))
                .ForMember(destination => destination.GradeSystemDetailId, option => option.MapFrom(source => source.GrdSysDetailId))
                .ForMember(destination => destination.Cnt, option => option.MapFrom(source => source.Cnt))
                .ForMember(destination => destination.Hours, option => option.MapFrom(source => source.Hours))
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.IsInComplete, option => option.MapFrom(source => source.IsInComplete))
                .ForMember(destination => destination.DroppedInAddDrop, option => option.MapFrom(source => source.DroppedInAddDrop))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.IsTransferred, option => option.MapFrom(source => source.IsTransfered))
                .ForMember(destination => destination.IsClinicsSatisfied, option => option.MapFrom(source => source.IsClinicsSatisfied))
                .ForMember(destination => destination.IsCourseCompleted, option => option.MapFrom(source => source.IsCourseCompleted)) 
                .ForMember(destination => destination.DateDetermined, option => option.MapFrom(source => source.DateDetermined))
                .ForMember(destination => destination.IsGradeOverridden, option => option.MapFrom(source => source.IsGradeOverridden))
                .ForMember(destination => destination.GradeOverriddenBy, option => option.MapFrom(source => source.GradeOverriddenBy))
                .ForMember(destination => destination.GradeOverriddenDate, option => option.MapFrom(source => source.GradeOverriddenDate))
                .ForMember(destination => destination.DateCompleted, option => option.MapFrom(source => source.DateCompleted));
        }
    }
}