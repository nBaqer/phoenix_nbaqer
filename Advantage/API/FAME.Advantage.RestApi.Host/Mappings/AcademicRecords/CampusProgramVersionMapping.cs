﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CampusProgramVersionMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The campus program version mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    /// <summary>
    /// The campus program version mapping.
    /// </summary>
    public class CampusProgramVersionMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of campus program versions
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArCampusPrgVersions, DataTransferObjects.AcademicRecords.CampusProgram.CampusProgramVersions>()
                .ForMember(destination => destination.CampusProgramVersionId, option => option.MapFrom(source => source.CampusPrgVerId))
                .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.CampusId))
                .ForMember(destination => destination.ProgramVersionId, option => option.MapFrom(source => source.PrgVerId))
                .ForMember(destination => destination.IsTitleIv, option => option.MapFrom(source => source.IsTitleIv))
                .ForMember(destination => destination.IsFameApproved, option => option.MapFrom(source => source.IsFameapproved))
                .ForMember(destination => destination.IsSelfPaced, option => option.MapFrom(source => source.IsSelfPaced))
                .ForMember(destination => destination.CalculationPeriodTypeId, option => option.MapFrom(source => source.CalculationPeriodTypeId))
                .ForMember(destination => destination.AllowExcusAbsPerPayPrd, option => option.MapFrom(source => source.AllowExcusAbsPerPayPrd))
                .ForMember(destination => destination.TermSubEqualInLen, option => option.MapFrom(source => source.TermSubEqualInLen))
                .ForMember(destination => destination.ModUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ResultStatus, option => option.Ignore());
        }
    }
}
