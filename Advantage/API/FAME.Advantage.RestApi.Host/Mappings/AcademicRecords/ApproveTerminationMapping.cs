﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ApproveTerminationMapping.cs" company="FAME Inc.">
//  Fame Inc. 2018
// </copyright>
// <summary>
//   The approve termination mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ApproveTermination;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.Host.Common;
    using FAME.Advantage.RestApi.Host.Infrastructure;
    using FAME.Core.R2T4Calculator.Common;
    using FAME.Extensions;

    /// <inheritdoc />
    /// <summary>
    /// The approve termination mapping provides a custom mapping between ArR2t4terminationDetails and ApproveTermination.
    /// </summary>
    public class ApproveTerminationMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArR2t4terminationDetails, ApproveTermination>().ForMember(
                destination => destination.DateOfDetermination,
                option => option.MapFrom(source => Utility.ParseDatetimeToString(source.DateWithdrawalDetermined))).ForMember(
                destination => destination.LastDateAttended,
                option => option.MapFrom(source => this.GetLastDateOfAttendance(source))).ForMember(
                destination => destination.WithdrawalDate,
                option => option.MapFrom(source => this.GetWithdrawalDate(source))).ForMember(
                destination => destination.StudentName,
                option => option.MapFrom(source => this.GetStudentName(source))).ForMember(
                destination => destination.StudentSsn,
                option => option.MapFrom(source => this.GetStudentSsn(source))).ForMember(
                destination => destination.EnrollmentName,
                option => option.MapFrom(source => this.GetEnrollmentName(source))).ForMember(
                destination => destination.DropReason,
                option => option.MapFrom(source => this.GetDropReason(source))).ForMember(
                destination => destination.Status,
                option => option.MapFrom(source => this.GetStatusCode(source))).ForMember(
                destination => destination.R2T4CalculationSummaryDetail,
                option => option.MapFrom(source => this.GetR2T4CalculationSummaryDetail(source))).ForMember(
                destination => destination.AdditionalInformationDetail,
                option => option.MapFrom(source => this.GetAdditionalInformation(source)))
                .ForMember(
                    destination => destination.UnitTypeDescription,
                    option => option.MapFrom(source => source.StuEnrollment.PrgVer.UnitType.UnitTypeDescrip))
                .ForMember(
                    destination => destination.IsR2T4,
                    option => option.MapFrom(source => source.IsPerformingR2t4calculator))
                .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.StuEnrollment.CampusId));
        }

        /// <summary>
        /// The get status code takes the terminationDetails as parameter and returns status name.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination Details.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetStatusCode(ArR2t4terminationDetails terminationDetails)
        {
            return Convert.ToString(terminationDetails?.StatusCode?.StatusCodeDescrip);
        }

        /// <summary>
        /// The get drop reason takes the terminationDetails as parameter and returns drop reason name.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetDropReason(ArR2t4terminationDetails terminationDetails)
        {
            return Convert.ToString(terminationDetails?.DropReason?.Descrip);
        }

        /// <summary>
        /// The get enrollment name takes the terminationDetails as parameter and returns enrollment name.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetEnrollmentName(ArR2t4terminationDetails terminationDetails)
        {
            return Convert.ToString(terminationDetails?.StuEnrollment?.PrgVer?.PrgVerDescrip);
        }

        /// <summary>
        /// The get withdrawal date takes the terminationDetails as parameter and returns a date in MM/dd/yyyy format.
        /// </summary>
        /// <param name="terminationDetails">
        /// The source.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetWithdrawalDate(ArR2t4terminationDetails terminationDetails)
        {
            if (terminationDetails.ArR2t4overrideResults?.FirstOrDefault()?.UpdatedDate
                > terminationDetails.ArR2t4results?.FirstOrDefault()?.UpdatedDate)
            {
                return Utility.ParseDatetimeToString(
                    terminationDetails.ArR2t4overrideResults != null
                        ? terminationDetails.ArR2t4overrideResults?.FirstOrDefault()?.WithdrawalDate
                        : terminationDetails.ArR2t4input?.FirstOrDefault()?.WithdrawalDate);
            }

            return Utility.ParseDatetimeToString(terminationDetails.ArR2t4input?.FirstOrDefault()?.WithdrawalDate);
        }

        /// <summary>
        /// The get last date of attendance takes the terminationDetails as parameter and returns a date in MM/dd/yyyy format.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetLastDateOfAttendance(ArR2t4terminationDetails terminationDetails)
        {
            if (terminationDetails.ArR2t4overrideResults?.FirstOrDefault()?.UpdatedDate
                > terminationDetails.ArR2t4results?.FirstOrDefault()?.UpdatedDate)
            {
                return Utility.ParseDatetimeToString(
                    terminationDetails.ArR2t4overrideResults != null
                        ? terminationDetails.ArR2t4overrideResults?.FirstOrDefault()?.WithdrawalDate
                        : terminationDetails.LastDateAttended);
            }
            else
            {
                return Utility.ParseDatetimeToString(
                    terminationDetails.ArR2t4input.Count != 0 ? terminationDetails.ArR2t4input?.FirstOrDefault()?.WithdrawalDate
                        : terminationDetails.LastDateAttended);
            }
        }

        /// <summary>
        /// The get R2T4 calculation summary details takes the terminationDetails as parameter and returns the R2T4 Calculation summary for Approve termination.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="R2T4CalculationSummary"/>. This object contains all the calculation for R2T4 and segregated values for the loans and grants
        /// that need to be returned by school and student. 
        /// </returns>
        private R2T4CalculationSummary GetR2T4CalculationSummaryDetail(ArR2t4terminationDetails terminationDetails)
        {
            var calculationSummary = new R2T4CalculationSummary();
            if (terminationDetails?.ArR2t4results != null && terminationDetails.ArR2t4overrideResults != null)
            {
                var overrideResult = terminationDetails.ArR2t4overrideResults.FirstOrDefault();
                var results = terminationDetails.ArR2t4results?.FirstOrDefault();
                var inputDetail = terminationDetails.ArR2t4input?.FirstOrDefault();
                var isOverridden = (results == null && overrideResult != null)
                                   || overrideResult?.UpdatedDate > results?.UpdatedDate;

                calculationSummary.AmountToBeReturnedBySchool = new List<ListItem<string, string>>();
                calculationSummary.AmountToBeReturnedByStudent = new List<ListItem<string, string>>();

                decimal totalCharges;

                if (isOverridden)
                {
                    totalCharges = Convert.ToDecimal(overrideResult.TuitionFee ?? 0)
                                   + Convert.ToDecimal(overrideResult.BoardFee ?? 0) + Convert.ToDecimal(
                                       overrideResult.OtherFee ?? 0) + Convert.ToDecimal(overrideResult.RoomFee ?? 0);
                }
                else
                {
                    totalCharges = Convert.ToDecimal(inputDetail?.TuitionFee ?? 0)
                                   + Convert.ToDecimal(inputDetail?.BoardFee ?? 0)
                                   + Convert.ToDecimal(inputDetail?.OtherFee ?? 0)
                                   + Convert.ToDecimal(inputDetail?.RoomFee ?? 0);
                }

                var postWithdrawalDisbursement = Convert.ToDecimal(isOverridden ? overrideResult.BoxJresult : results?.BoxJresult);
                calculationSummary.TotalCharges = Utility.ParseCurrencyFormattedNumber(totalCharges);
                calculationSummary.TotalTitleIvAid = Utility.ParseCurrencyFormattedNumber(Convert.ToDecimal(isOverridden ? overrideResult.BoxGresult : results?.BoxGresult));
                calculationSummary.TotalTitleIvAidDisbursed = Utility.ParseCurrencyFormattedNumber(Convert.ToDecimal(isOverridden ? overrideResult.BoxEresult : results?.BoxEresult));
                calculationSummary.PercentageOfTitleIvAidEarned = Utility.ParsePercentageFormattedNumber(Convert.ToDecimal(isOverridden ? overrideResult.BoxHresult : results?.BoxHresult));
                calculationSummary.PostWithdrawalDisbursement = Utility.ParseCurrencyFormattedNumber(postWithdrawalDisbursement);
                calculationSummary.TotalTitleIvAidToReturn = Utility.ParseCurrencyFormattedNumber(Convert.ToDecimal(isOverridden ? overrideResult.BoxKresult : results?.BoxKresult));
                calculationSummary.TotalAmountToBeReturnedBySchool = Utility.ParseCurrencyFormattedNumber(Convert.ToDecimal(isOverridden ? overrideResult.BoxOresult : results?.BoxOresult));

                var boxRValue = Convert.ToDecimal(isOverridden ? overrideResult.BoxRresult : results?.BoxRresult);
                var boxQValue = Convert.ToDecimal(isOverridden ? overrideResult.BoxQresult : results?.BoxQresult);
                var boxUValue = Convert.ToDecimal(isOverridden ? overrideResult.BoxUresult : results?.BoxUresult);

                decimal totalReturnByStudent = 0;
                if (boxRValue < boxQValue)
                {
                    totalReturnByStudent = boxRValue;
                }
                else if (boxRValue > boxQValue)
                {
                    totalReturnByStudent = boxQValue;
                }

                totalReturnByStudent += boxUValue > 0 ? boxUValue : 0;
                calculationSummary.TotalAmountToBeReturnedByStudent = Utility.ParseCurrencyFormattedNumber(totalReturnByStudent > 0 ? totalReturnByStudent : 0);

                if (postWithdrawalDisbursement <= 0)
                {
                    // Loans disbursed
                    var loanAmountDisbursed = new List<ListItem<string, decimal>>();
                    var fieldListToLoanDisbursed = this.GetFieldListToLoanDisbursed();
                    foreach (var prop in fieldListToLoanDisbursed.GetType().GetProperties())
                    {
                        var amountDisbursed = Convert.ToDecimal(isOverridden
                                                                    ? overrideResult.GetPropertyValue(prop.Name)
                                                                    : inputDetail.GetPropertyValue(prop.Name));
                        if (amountDisbursed > 0)
                        {
                            var field = fieldListToLoanDisbursed.GetPropertyValue(prop.Name);
                            loanAmountDisbursed.Add(new ListItem<string, decimal> { Text = Convert.ToString(field), Value = amountDisbursed });
                        }
                    }

                    var loansAndGrantsToCheck = this.GetFieldListToLoanReturned();
                    if (loansAndGrantsToCheck != null)
                    {
                        var loansAndGrantsProperties = loansAndGrantsToCheck.GetType().GetProperties();
                        foreach (var prop in loansAndGrantsProperties)
                        {
                            decimal resultVal = 0;
                            if (isOverridden)
                            {
                                if (overrideResult.GetType().GetProperty(prop.Name) != null)
                                {
                                    resultVal = Convert.ToDecimal(overrideResult.GetPropertyValue(prop.Name));
                                }
                                else
                                {
                                    resultVal = Convert.ToDecimal(inputDetail.GetPropertyValue(prop.Name));
                                }
                            }
                            else if (results != null)
                            {
                                if (results.GetType().GetProperty(prop.Name) != null)
                                {
                                    resultVal = Convert.ToDecimal(results.GetPropertyValue(prop.Name));
                                }
                                else
                                {
                                    resultVal = Convert.ToDecimal(inputDetail.GetPropertyValue(prop.Name));
                                }
                            }

                            var fieldName = Convert.ToString(loansAndGrantsToCheck.GetPropertyValue(prop.Name));
                            var loanDisbursed = loanAmountDisbursed.FirstOrDefault(x => x.Text == fieldName);
                            var amountToBeReturnedByStudent = loanDisbursed?.Value - resultVal ?? 0;
                            if (totalReturnByStudent > 0 && totalReturnByStudent > amountToBeReturnedByStudent)
                            {
                                totalReturnByStudent = totalReturnByStudent - amountToBeReturnedByStudent;
                            }
                            else
                            {
                                amountToBeReturnedByStudent = totalReturnByStudent;
                                totalReturnByStudent = 0;
                            }

                            // Loans to be returned by student
                            if (amountToBeReturnedByStudent > 0)
                            {
                                calculationSummary.AmountToBeReturnedByStudent.Add(new ListItem<string, string>
                                {
                                    Value = Utility.ParseCurrencyFormattedNumber(amountToBeReturnedByStudent),
                                    Text = Convert.ToString(fieldName)
                                });
                            }

                            // Loans and Grants to be returned by school
                            if (resultVal > 0)
                            {
                                calculationSummary.AmountToBeReturnedBySchool.Add(new ListItem<string, string>
                                {
                                    Value = this.FormatValueByType(resultVal, this.GetTypeByPropertyName(prop.Name)),
                                    Text = Convert.ToString(fieldName)
                                });
                            }
                        }
                    }

                    // Grants to be returned by student
                    var fieldsToCheckForGrantToReturn = this.GetFieldListToCheckForGrantToReturn();
                    if (fieldsToCheckForGrantToReturn != null)
                    {
                        foreach (var prop in fieldsToCheckForGrantToReturn.GetType().GetProperties())
                        {
                            var grantAmountToReturn = Convert.ToDecimal(
                                isOverridden
                                    ? (overrideResult.GetType().GetProperty(prop.Name) != null) ? overrideResult.GetPropertyValue(prop.Name) : 0.0
                                    : (results?.GetType().GetProperty(prop.Name) != null) ? results.GetPropertyValue(prop.Name) : 0.0);
                            if (grantAmountToReturn > 0)
                            {
                                var fieldName = fieldsToCheckForGrantToReturn.GetPropertyValue(prop.Name);
                                calculationSummary.AmountToBeReturnedByStudent.Add(new ListItem<string, string>
                                {
                                    Value = Utility.ParseCurrencyFormattedNumber(grantAmountToReturn),
                                    Text = Convert.ToString(fieldName)
                                });
                            }
                        }
                    }
                }
            }

            return calculationSummary;
        }

        /// <summary>
        /// The get additional information takes the terminationDetails as parameter and returns a additional information about the R2T4 calculation and overridden field and user.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details
        /// </param>
        /// <returns>
        /// The <see cref="AdditionalInformation"/>. This contains the details about R2T4 calculation and what are all information has been modified when it was overridden any any user.
        /// </returns>
        private AdditionalInformation GetAdditionalInformation(ArR2t4terminationDetails terminationDetails)
        {
            var additionalInformation = new AdditionalInformation();
            additionalInformation.R2T4ResultFieldsWithOverriddenValues = new List<OverriddenListItem>();
            additionalInformation.TitleIvGrantLessThan50Dollar = new List<ListItem<string, string>>();
            if (terminationDetails?.ArR2t4input != null)
            {
                additionalInformation.R2T4InputUserName = Convert.ToString(terminationDetails.ArR2t4input.FirstOrDefault()?.UpdatedBy.FullName);
                additionalInformation.IsClockHour = Convert.ToBoolean(Convert.ToInt32(terminationDetails.ArR2t4input?.FirstOrDefault()?.ProgramUnitTypeId) == Convert.ToInt32(ProgramType.ClockHour));
            }

            additionalInformation.IsNotRequiredToTakeAattendance =
                Convert.ToBoolean(terminationDetails?.ArR2t4input?.FirstOrDefault()?.IsAttendanceNotRequired);

            if (terminationDetails?.ArR2t4results != null && terminationDetails.ArR2t4overrideResults != null)
            {
                additionalInformation.CalculationPeriodTypeId = terminationDetails.CalculationPeriodTypeId;
                var overrideResult = terminationDetails.ArR2t4overrideResults.FirstOrDefault();
                var results = terminationDetails.ArR2t4results.FirstOrDefault();
                var isOverridden = overrideResult?.UpdatedDate > results?.UpdatedDate;
                var boxHValue = isOverridden ? Convert.ToDecimal(overrideResult.BoxHresult) : Convert.ToDecimal(results?.BoxHresult);

                var completedTime = isOverridden
                                        ? Convert.ToDecimal(overrideResult.CompletedTime)
                                        : Convert.ToDecimal(results?.CompletedTime);

                var totalTime = isOverridden
                                    ? Convert.ToDecimal(overrideResult.TotalTime)
                                    : Convert.ToDecimal(results?.TotalTime);

                // first check if the total and completed time is not equal and then compare BoxH value.
                additionalInformation.IsAttendance100Percent = (totalTime > completedTime) && boxHValue == 100;

                if (overrideResult != null && results != null)
                {
                    var fielsToCheckForOverridden = this.GetFieldListToCheckForOverridden();
                    if (isOverridden && fielsToCheckForOverridden != null)
                    {
                        additionalInformation.OverriddenUserName = overrideResult.UpdatedBy.FullName;
                        additionalInformation.TicketNumber = overrideResult.TicketNumber.ToString();

                        var properties = fielsToCheckForOverridden.GetType().GetProperties();
                        var inputDetail = terminationDetails.ArR2t4input?.FirstOrDefault();
                        foreach (var prop in properties)
                        {
                            var resultVal = (results.GetType().GetProperty(prop.Name) != null) ? results.GetPropertyValue(prop.Name) : inputDetail?.GetPropertyValue(prop.Name);
                            var overriddenVal = overrideResult.GetPropertyValue(prop.Name);
                            if (Convert.ToString(overriddenVal) != Convert.ToString(resultVal))
                            {
                                var fieldName = fielsToCheckForOverridden.GetPropertyValue(prop.Name);
                                additionalInformation.R2T4ResultFieldsWithOverriddenValues.Add(
                                    new OverriddenListItem
                                    {
                                        ResultValue = ((string)fieldName != ConstantR2T4Key.PercentageOfActualAttendence) ? this.FormatValueByType(resultVal, this.GetTypeByPropertyName(prop.Name)) : resultVal?.ToString(),
                                        OverriddenResultValue = ((string)fieldName != ConstantR2T4Key.PercentageOfActualAttendence) ? this.FormatValueByType(overriddenVal, this.GetTypeByPropertyName(prop.Name)) : overriddenVal?.ToString(),
                                        FieldName = Convert.ToString(fieldName)
                                    });
                            }
                        }

                        if (!string.IsNullOrEmpty(overrideResult.OverriddenData))
                        {
                            var duplicateField = this.GetDuplicateFieldListToCheckOverridden();
                            if (duplicateField != null)
                            {
                                this.GetOverriddenDuplicateFields(duplicateField, overrideResult, results, additionalInformation);
                            }
                        }

                        if (!string.IsNullOrEmpty(overrideResult.PostWithdrawalData))
                        {
                            var pwdFieldList = this.GetPWDFieldsToCheckOverridden();
                            if (pwdFieldList != null)
                            {
                                this.GetPostWithdrawalOverriddenFields(pwdFieldList, overrideResult, results, additionalInformation);
                            }
                        }
                    }
                }

                this.GetNonRefundableGrants(isOverridden, overrideResult, results, additionalInformation);
            }

            //Check Tuition charged by payment period' check box is selected or not on 'R2T4 Input' tab
            if (terminationDetails?.ArR2t4input?.FirstOrDefault()?.IsTuitionChargedByPaymentPeriod != null)
            {
                additionalInformation.IsTuitionByPaymentPeriod = Convert.ToBoolean(
                    terminationDetails?.ArR2t4input?.FirstOrDefault()?.IsTuitionChargedByPaymentPeriod);

                decimal creditBalance = Convert.ToDecimal(
                    terminationDetails?.ArR2t4input?.FirstOrDefault()?.CreditBalanceRefunded);
                additionalInformation.CreditBalanceRefunded = Utility.ParseCurrencyFormattedNumber(creditBalance);
            }
            else
            {
                additionalInformation.IsTuitionByPaymentPeriod = true;
            }

            additionalInformation.R2T4ResultFieldsWithOverriddenValues = additionalInformation
                .R2T4ResultFieldsWithOverriddenValues.OrderBy(x => x.FieldName).ToList();

            if (additionalInformation.IsAttendance100Percent
                || additionalInformation.IsNotRequiredToTakeAattendance
                || additionalInformation.R2T4ResultFieldsWithOverriddenValues.Count > 0
                || additionalInformation.TitleIvGrantLessThan50Dollar.Count > 0
                || !additionalInformation.IsTuitionByPaymentPeriod)
            {
                additionalInformation.IsAdditionalInfoRequired = true;
            }
            else
            {
                additionalInformation.IsAdditionalInfoRequired = false;
            }

            return additionalInformation;
        }


        /// <summary>
        /// The GetpostWithdrawalOverriddenFields function finds all the overridden field under 
        /// Step:11 Post Withdrawal section and add to AdditionalInformation.
        /// </summary>
        /// <param name="pwdFieldList">
        /// The pwd field list.
        /// </param>
        /// <param name="overriddenResult">
        /// The overridden Result.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="additionalInformation">
        /// The additional information.
        /// </param>
        private void GetPostWithdrawalOverriddenFields(
            object pwdFieldList,
            ArR2t4overrideResults overriddenResult,
            ArR2t4results results,
            AdditionalInformation additionalInformation)
        {
            if (additionalInformation == null || pwdFieldList == null || overriddenResult == null || results == null)
            {
                return;
            }

            var postWithdrawalFieldList = overriddenResult.PostWithdrawalData?.Split(':') ?? new string[] { };
            var pwdResultData = results.PostWithdrawalData?.Split(':') ?? new string[] { };

            var pwdFieldListProperties = pwdFieldList.GetType().GetProperties();
            foreach (var property in pwdFieldListProperties)
            {
                var fieldAndvalue =
                    postWithdrawalFieldList.FirstOrDefault(x => x.ToLower().Contains(property.Name.ToLower()));
                if (!string.IsNullOrEmpty(fieldAndvalue))
                {
                    var nameAndVal = fieldAndvalue.Split('=');
                    if (nameAndVal.Length == 2)
                    {
                        var val = nameAndVal[1] != string.Empty ? nameAndVal[1] : "0";
                        var resultPwd = pwdResultData.FirstOrDefault(x => x.Contains(nameAndVal[0]));
                        if (resultPwd != null)
                        {
                            var result = resultPwd.Split('=');
                            var overriddenVal = result[1] != string.Empty ? result[1] : "0";
                            if (val != overriddenVal)
                            {
                                var fieldName = pwdFieldList.GetPropertyValue(property.Name).ToString();
                                additionalInformation.R2T4ResultFieldsWithOverriddenValues.Add(
                                    new OverriddenListItem
                                    {
                                        FieldName = fieldName,
                                        ResultValue = property.Name.Contains("Chk") ? result[1] : this.FormatValueByType(result[1], property.Name.Contains("Dt") ? FieldType.Date : string.Empty),
                                        OverriddenResultValue = property.Name.Contains("Chk") ? nameAndVal[1] : this.FormatValueByType(nameAndVal[1], property.Name.Contains("Dt") ? FieldType.Date : string.Empty)
                                    });
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The GetNonRefundableGrants function finds all the Grants with value between $1- $50 under 
        /// Step:10 Return of Grant Funds by the Student section and add to AdditionalInformation under non refundable grants.
        /// </summary>
        /// <param name="isOverridden">
        /// The is overridden.
        /// </param>
        /// <param name="overrideResult">
        /// The override result.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="additionalInformation">
        /// The additional information.
        /// </param>
        private void GetNonRefundableGrants(
            bool isOverridden,
            ArR2t4overrideResults overrideResult,
            ArR2t4results results,
            AdditionalInformation additionalInformation)
        {
            if (additionalInformation == null && results == null)
            {
                return;
            }

            var nonRefundableGrantsFieldList = this.GetFieldListToCheckForGrantToReturn();
            if (nonRefundableGrantsFieldList != null)
            {
                var nonRefundableGrantsProperties = nonRefundableGrantsFieldList.GetType().GetProperties();
                foreach (var prop in nonRefundableGrantsProperties)
                {
                    var grantAmount = Convert.ToDecimal(
                        isOverridden ? overrideResult.GetPropertyValue(prop.Name) : results.GetPropertyValue(prop.Name));
                    if (grantAmount > 0 && grantAmount <= 50)
                    {
                        var fieldName = nonRefundableGrantsFieldList.GetPropertyValue(prop.Name);
                        additionalInformation.TitleIvGrantLessThan50Dollar.Add(
                            new ListItem<string, string>()
                            {
                                Text = Convert.ToString(fieldName),
                                Value = this.FormatValueByType(
                                        grantAmount,
                                        this.GetTypeByPropertyName(prop.Name))
                            });
                    }
                }
            }
        }

        /// <summary>
        /// The get overridden duplicate fields.
        /// </summary>
        /// <param name="duplicateField">
        /// The duplicate field.
        /// </param>
        /// <param name="overriddenResult">
        /// The overridden Result.
        /// </param>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="additionalInformation">
        /// The additional information.
        /// </param>
        private void GetOverriddenDuplicateFields(
            object duplicateField,
            ArR2t4overrideResults overriddenResult,
            ArR2t4results results,
            AdditionalInformation additionalInformation)
        {
            if (additionalInformation == null || duplicateField == null || overriddenResult == null || results == null)
            {
                return;
            }

            var resultData = results.OverriddenData?.Split(':') ?? new string[] { };

            var overriddenResultData = overriddenResult.OverriddenData?.Split(':') ?? new string[] { };
            var duplicateFieldProperties = duplicateField.GetType().GetProperties();
            foreach (var overriddenField in overriddenResultData)
            {
                if (overriddenField != string.Empty)
                {
                    var fieldAndValue = overriddenField.Split('=');
                    var fieldName = fieldAndValue[0];
                    var result = resultData.FirstOrDefault(x => x.Contains(fieldName));
                    var resultNameAndValue = result?.Split('=');
                    if (!string.IsNullOrEmpty(resultNameAndValue?[0]))
                    {
                        var resultVal = resultNameAndValue[1] != string.Empty ? resultNameAndValue[1] : "0.0";
                        var fieldVal = fieldAndValue[1] != string.Empty ? fieldAndValue[1] : "0.0";
                        var originalFieldName = fieldName;

                        if (fieldName.Length > 0)
                        {
                            var stepIndex = fieldName.Substring(3, 1);
                            fieldName = fieldName.Replace("Step1F", "BoxG");
                            fieldName = fieldName.Substring(fieldName.Length - 2).ToUpper();
                            if (fieldName.Length == 2)
                            {
                                originalFieldName = this.GetActualFieldName(fieldName.Substring(1));
                            }

                            if (fieldName.IndexOf("X", StringComparison.Ordinal) == 0)
                            {
                                fieldName = fieldName.Substring(1);
                            }

                            if (int.TryParse(stepIndex, out var isNumber))
                            {
                                fieldName = "Box" + ((fieldName.IndexOf(stepIndex, StringComparison.Ordinal) >= 0) ? fieldName : isNumber + fieldName);
                            }
                            else
                            {
                                fieldName = "Box" + fieldName;
                            }
                        }

                        if (Convert.ToDecimal(fieldVal).ToString(CultureInfo.InvariantCulture)
                            != Convert.ToDecimal(resultVal).ToString(CultureInfo.InvariantCulture))
                        {
                            var fieldText = duplicateFieldProperties.FirstOrDefault(
                                x => string.Equals(x.Name, fieldName, StringComparison.CurrentCultureIgnoreCase));
                            if (fieldText != null)
                            {
                                var fieldCompleteText = duplicateField.GetPropertyValue(fieldText.Name);
                                additionalInformation.R2T4ResultFieldsWithOverriddenValues.Add(
                                    new OverriddenListItem
                                    {
                                        ResultValue =
                                                this.FormatValueByType(
                                                    resultNameAndValue[1],
                                                    this.GetTypeByPropertyName(originalFieldName)),
                                        OverriddenResultValue =
                                                this.FormatValueByType(
                                                    fieldAndValue[1],
                                                    this.GetTypeByPropertyName(originalFieldName)),
                                        FieldName = Convert.ToString(fieldCompleteText)
                                    });
                            }
                        }
                    }
                }
            }

            this.RemoveUnneccessaryCompare(results, additionalInformation);
        }

        /// <summary>
        /// The remove unneccessary comparison for overridden items.
        /// </summary>
        /// <param name="results">
        /// The results.
        /// </param>
        /// <param name="additionalInformation">
        /// The additional information.
        /// </param>
        private void RemoveUnneccessaryCompare(ArR2t4results results, AdditionalInformation additionalInformation)
        {
            var boxJVal = (results.GetType().GetProperty("BoxJresult") != null) ? results.GetPropertyValue("BoxJresult") : null;
            var boxKVal = (results.GetType().GetProperty("BoxKresult") != null) ? results.GetPropertyValue("BoxKresult") : null;

            var itemJi = additionalInformation.R2T4ResultFieldsWithOverriddenValues.FirstOrDefault(x => x.FieldName == ConstantR2T4Key.Box4Ji);
            var itemJe = additionalInformation.R2T4ResultFieldsWithOverriddenValues.FirstOrDefault(x => x.FieldName == ConstantR2T4Key.Box4Je);

            var itemKi = additionalInformation.R2T4ResultFieldsWithOverriddenValues.FirstOrDefault(x => x.FieldName == ConstantR2T4Key.Box4Ki);
            var itemKe = additionalInformation.R2T4ResultFieldsWithOverriddenValues.FirstOrDefault(x => x.FieldName == ConstantR2T4Key.Box4Ke);

            if (boxJVal == null)
            {
                if (itemJi != null && itemJi.OverriddenResultValue == "$0.00")
                {
                    additionalInformation.R2T4ResultFieldsWithOverriddenValues.Remove(itemJi);
                }

                if (itemJe != null && itemJe.OverriddenResultValue == "$0.00")
                {
                    additionalInformation.R2T4ResultFieldsWithOverriddenValues.Remove(itemJe);
                }
            }

            if (boxKVal == null)
            {
                if (itemKi != null && itemKi.OverriddenResultValue == "$0.00")
                {
                    additionalInformation.R2T4ResultFieldsWithOverriddenValues.Remove(itemKi);
                }

                if (itemKe != null && itemKe.OverriddenResultValue == "$0.00")
                {
                    additionalInformation.R2T4ResultFieldsWithOverriddenValues.Remove(itemKe);
                }
            }
        }

        /// <summary>
        /// The get actual field name by box name.
        /// </summary>
        /// <param name="box">
        /// The box.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetActualFieldName(string box)
        {
            string originalFieldName;
            switch (box)
            {
                case "A":
                    originalFieldName = $"SubTotalAmountDisbursed{box}";
                    break;
                case "B":
                    originalFieldName = $"SubTotalNetAmountDisbursed{box}";
                    break;

                case "C":
                    originalFieldName = $"SubTotalAmountCouldDisbursed{box}";
                    break;

                case "D":
                    originalFieldName = $"SubTotalNetAmountDisbursed{box}";
                    break;
                default:
                    originalFieldName = $"Box{box}result";
                    break;
            }

            return originalFieldName;
        }

        #region Utility Methods

        /// <summary>
        /// The get student ssn takes the terminationDetails as parameter and returns SSN value in the required format.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetStudentSsn(ArR2t4terminationDetails terminationDetails)
        {
            return (terminationDetails?.StuEnrollment?.Lead != null && terminationDetails.StuEnrollment.Lead?.Ssn != string.Empty)
                       ? $"XXX-XX-{terminationDetails.StuEnrollment.Lead.Ssn.Substring(5)}" : string.Empty;
        }

        /// <summary>
        /// The get student name takes the terminationDetails as parameter and returns student name in last name, first name middle name format.
        /// </summary>
        /// <param name="terminationDetails">
        /// The termination details.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetStudentName(ArR2t4terminationDetails terminationDetails)
        {
            return terminationDetails.StuEnrollment?.Lead != null ?
            $"{terminationDetails.StuEnrollment.Lead.LastName}, {terminationDetails.StuEnrollment.Lead.FirstName} {terminationDetails.StuEnrollment.Lead.MiddleName}"
            : string.Empty;
        }

        /// <summary>
        /// The format data by type formats the value based on the data type.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <typeparam name="T">
        /// Generic type
        /// </typeparam>
        /// <returns>
        /// The <see cref="string"/>.
        /// returns a formatted string value
        /// </returns>
        private string FormatValueByType<T>(T value, string type)
        {
            string formattedValue;
            switch (type)
            {
                case FieldType.Percentage:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.ParsePercentageFormattedNumber(Convert.ToDecimal(value)) : string.Empty;
                    break;

                case FieldType.Date:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.ParseDatetimeToString(Convert.ToDateTime(value)) : string.Empty;
                    break;

                case FieldType.Number:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Convert.ToString(Math.Round(Convert.ToDecimal(value), 2), CultureInfo.InvariantCulture) : string.Empty;
                    break;

                default:
                    formattedValue = value != null && !string.IsNullOrEmpty(value.ToString()) ? Utility.ParseCurrencyFormattedNumber(Convert.ToDecimal(value)) : string.Empty;
                    break;
            }

            return formattedValue;
        }

        /// <summary>
        /// The get type by property name takes the name of property and returns the type of the property in the object.
        /// </summary>
        /// <param name="propName">
        /// The prop name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetTypeByPropertyName(string propName)
        {
            var fieldsNameWithTypes = this.GetFieldsNameWithType();
            if (fieldsNameWithTypes.GetType().GetProperty(propName) != null)
            {
                return Convert.ToString(fieldsNameWithTypes.GetPropertyValue(propName));
            }

            return string.Empty;
        }

        /// <summary>
        /// The get fields name with type returns an object with fieldNames and their types.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetFieldsNameWithType()
        {
            return new
            {
                BoxHresult = FieldType.Percentage,
                BoxMresult = FieldType.Percentage,
                StartDate = FieldType.Date,
                ScheduledEndDate = FieldType.Date,
                WithdrawalDate = FieldType.Date,
                PercentageOfActualAttendence = FieldType.Number,
                TotalTime = FieldType.Number,
                CompletedTime = FieldType.Number
            };
        }
        #endregion Utility Methods

        /// <summary>
        /// The get field list to check for overridden method returns an object of all the fields to be checked for overridden.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetFieldListToCheckForOverridden()
        {
            return new
            {
                ConstantR2T4Key.BoardFee,
                ConstantR2T4Key.BoxEresult,
                ConstantR2T4Key.BoxFresult,
                ConstantR2T4Key.BoxGresult,
                ConstantR2T4Key.BoxHresult,
                ConstantR2T4Key.BoxIresult,
                ConstantR2T4Key.BoxJresult,
                ConstantR2T4Key.BoxKresult,
                ConstantR2T4Key.BoxLresult,
                ConstantR2T4Key.BoxMresult,
                ConstantR2T4Key.BoxNresult,
                ConstantR2T4Key.BoxOresult,
                ConstantR2T4Key.BoxPresult,
                ConstantR2T4Key.BoxQresult,
                ConstantR2T4Key.BoxRresult,
                ConstantR2T4Key.BoxSresult,
                ConstantR2T4Key.BoxTresult,
                ConstantR2T4Key.BoxUresult,
                ConstantR2T4Key.CompletedTime,
                ConstantR2T4Key.DirectGraduatePlusLoanCouldDisbursed,
                ConstantR2T4Key.DirectGraduatePlusLoanDisbursed,
                ConstantR2T4Key.DirectParentPlusLoanCouldDisbursed,
                ConstantR2T4Key.DirectParentPlusLoanDisbursed,
                ConstantR2T4Key.FseogamountToReturn,
                ConstantR2T4Key.FseogcouldDisbursed,
                ConstantR2T4Key.Fseogdisbursed,
                ConstantR2T4Key.IraqAfgGrantAmountToReturn,
                ConstantR2T4Key.IraqAfgGrantCouldDisbursed,
                ConstantR2T4Key.IraqAfgGrantDisbursed,
                ConstantR2T4Key.OtherFee,
                ConstantR2T4Key.PellGrantAmountToReturn,
                ConstantR2T4Key.PellGrantCouldDisbursed,
                ConstantR2T4Key.PellGrantDisbursed,
                ConstantR2T4Key.PercentageOfActualAttendence,
                ConstantR2T4Key.PerkinsLoanCouldDisbursed,
                ConstantR2T4Key.PerkinsLoanDisbursed,
                ConstantR2T4Key.RoomFee,
                ConstantR2T4Key.ScheduledEndDate,
                ConstantR2T4Key.StartDate,
                ConstantR2T4Key.SubDirectLoanSchoolReturn,
                ConstantR2T4Key.TeachGrantSchoolReturn,
                ConstantR2T4Key.UnsubDirectLoanSchoolReturn,
                ConstantR2T4Key.PerkinsLoanSchoolReturn,
                ConstantR2T4Key.IraqAfgGrantSchoolReturn,
                ConstantR2T4Key.PellGrantSchoolReturn,
                ConstantR2T4Key.FseogschoolReturn,
                ConstantR2T4Key.DirectParentPlusLoanSchoolReturn,
                ConstantR2T4Key.DirectGraduatePlusLoanSchoolReturn,
                ConstantR2T4Key.SubLoanNetAmountDisbursed,
                ConstantR2T4Key.SubLoanNetAmountCouldDisbursed,
                ConstantR2T4Key.SubTotalAmountDisbursedA,
                ConstantR2T4Key.SubTotalAmountCouldDisbursedC,
                ConstantR2T4Key.SubTotalNetAmountDisbursedB,
                ConstantR2T4Key.SubTotalNetAmountDisbursedD,
                ConstantR2T4Key.TeachGrantAmountToReturn,
                ConstantR2T4Key.TeachGrantCouldDisbursed,
                ConstantR2T4Key.TeachGrantDisbursed,
                ConstantR2T4Key.TotalTime,
                ConstantR2T4Key.TuitionFee,
                ConstantR2T4Key.UnsubLoanNetAmountCouldDisbursed,
                ConstantR2T4Key.UnsubLoanNetAmountDisbursed,
                ConstantR2T4Key.WithdrawalDate
            };
        }

        /// <summary>
        /// The get duplicate field list to check overridden.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetDuplicateFieldListToCheckOverridden()
        {
            return new
            {
                BoxEA = ConstantR2T4Key.BoxEa,
                BoxEB = ConstantR2T4Key.BoxEb,
                BoxFA = ConstantR2T4Key.BoxFa,
                BoxFC = ConstantR2T4Key.BoxFc,
                BoxGA = ConstantR2T4Key.BoxGa,
                BoxGB = ConstantR2T4Key.BoxGb,
                BoxGC = ConstantR2T4Key.BoxGc,
                BoxGD = ConstantR2T4Key.BoxGd,
                ConstantR2T4Key.BoxG,
                ConstantR2T4Key.BoxH,
                Box4JI = ConstantR2T4Key.Box4Ji,
                Box4JE = ConstantR2T4Key.Box4Je,
                Box4KE = ConstantR2T4Key.Box4Ke,
                Box4KI = ConstantR2T4Key.Box4Ki,
                ConstantR2T4Key.Box5H,
                ConstantR2T4Key.Box5L,
                ConstantR2T4Key.Box5M,
                ConstantR2T4Key.Box7K,
                ConstantR2T4Key.Box7O,
                ConstantR2T4Key.Box8B,
                ConstantR2T4Key.Box8P,
                ConstantR2T4Key.Box9F,
                ConstantR2T4Key.Box9Q,
                ConstantR2T4Key.Box9R,
                ConstantR2T4Key.Box9S,
                ConstantR2T4Key.Box9T
            };
        }

        /// <summary>
        /// The get pwd fields to check overridden.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetPWDFieldsToCheckOverridden()
        {
            return new
            {
                ConstantR2T4Key.TxtPwd,
                ConstantR2T4Key.TxtPwdBox2,
                ConstantR2T4Key.TxtPwdOffered,
                ConstantR2T4Key.TxtPwdBox2Offered,
                ConstantR2T4Key.TxtPwdBox3,
                ConstantR2T4Key.PwdPell3,
                ConstantR2T4Key.PwdPell6,
                ConstantR2T4Key.PwdFseog3,
                ConstantR2T4Key.PwdFseog6,
                ConstantR2T4Key.PwdTeach3,
                ConstantR2T4Key.PwdTeach6,
                ConstantR2T4Key.PwdIasg3,
                ConstantR2T4Key.PwdIasg6,
                ConstantR2T4Key.PwdPerkins1,
                ConstantR2T4Key.PwdPerkins2,
                ConstantR2T4Key.PwdPerkins3,
                ConstantR2T4Key.PwdPerkins4,
                ConstantR2T4Key.PwdPerkins5,
                ConstantR2T4Key.PwdPerkins6,
                ConstantR2T4Key.PwdSub1,
                ConstantR2T4Key.PwdSub2,
                ConstantR2T4Key.PwdSub3,
                ConstantR2T4Key.PwdSub4,
                ConstantR2T4Key.PwdSub5,
                ConstantR2T4Key.PwdSub6,
                ConstantR2T4Key.PwdUnSub1,
                ConstantR2T4Key.PwdUnSub2,
                ConstantR2T4Key.PwdUnSub3,
                ConstantR2T4Key.PwdUnSub4,
                ConstantR2T4Key.PwdUnSub5,
                ConstantR2T4Key.PwdUnSub6,
                ConstantR2T4Key.PwdGrad1,
                ConstantR2T4Key.PwdGrad2,
                ConstantR2T4Key.PwdGrad3,
                ConstantR2T4Key.PwdGrad4,
                ConstantR2T4Key.PwdGrad5,
                ConstantR2T4Key.PwdGrad6,
                ConstantR2T4Key.PwdParent1,
                ConstantR2T4Key.PwdParent2,
                ConstantR2T4Key.PwdParent3,
                ConstantR2T4Key.PwdParent4,
                ConstantR2T4Key.PwdParent5,
                ConstantR2T4Key.PwdParent6,
                ConstantR2T4Key.PwdTotal1,
                ConstantR2T4Key.PwdTotal2,
                ConstantR2T4Key.PwdTotal3,
                ConstantR2T4Key.PwdTotal4,
                ConstantR2T4Key.PwdTotal5,
                ConstantR2T4Key.PwdTotal6,
                ConstantR2T4Key.DtPostWithdrwal,
                ConstantR2T4Key.DtDeadline,
                ConstantR2T4Key.DtResponseReceived,
                ConstantR2T4Key.DtGrantTransferred,
                ConstantR2T4Key.DtLoanTransferred,
                ConstantR2T4Key.ChkResponseReceived,
                ConstantR2T4Key.ChkResponseNotReceived,
                ConstantR2T4Key.ChkNotAccept
            };
        }

        /// <summary>
        /// The get field list to check for loan to return method returns an object of grants to be returned by student.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetFieldListToCheckForGrantToReturn()
        {
            return new
            {
                ConstantGrantToReturnByStudent.PellGrantAmountToReturn,
                ConstantGrantToReturnByStudent.FseogamountToReturn,
                ConstantGrantToReturnByStudent.TeachGrantAmountToReturn,
                ConstantGrantToReturnByStudent.IraqAfgGrantAmountToReturn,
            };
        }

        /// <summary>
        /// The get field list to loan disbursed returns an object of loans amount disbursed.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetFieldListToLoanDisbursed()
        {
            return new
            {
                ConstantLoanDisbursed.UnsubLoanNetAmountDisbursed,
                ConstantLoanDisbursed.SubLoanNetAmountDisbursed,
                ConstantLoanDisbursed.PerkinsLoanDisbursed,
                ConstantLoanDisbursed.DirectGraduatePlusLoanDisbursed,
                ConstantLoanDisbursed.DirectParentPlusLoanDisbursed
            };
        }

        /// <summary>
        /// The get field list to loan returned returns an object of loans to be returned by school.
        /// </summary>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        private object GetFieldListToLoanReturned()
        {
            return new
            {
                ConstantLoanToReturnBySchool.UnsubDirectLoanSchoolReturn,
                ConstantLoanToReturnBySchool.SubDirectLoanSchoolReturn,
                ConstantLoanToReturnBySchool.PerkinsLoanSchoolReturn,
                ConstantLoanToReturnBySchool.DirectGraduatePlusLoanSchoolReturn,
                ConstantLoanToReturnBySchool.DirectParentPlusLoanSchoolReturn,
                ConstantLoanToReturnBySchool.PellGrantSchoolReturn,
                ConstantLoanToReturnBySchool.FseogschoolReturn,
                ConstantLoanToReturnBySchool.TeachGrantSchoolReturn,
                ConstantLoanToReturnBySchool.IraqAfgGrantSchoolReturn
            };
        }
    }
}