﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PeriodTypesMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The period types mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The period types mapping maps EnrollmentPeriods and PeriodDetails.
    /// </summary>
    public class PeriodTypesMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the EnrollmentPeriods and PeriodDetails.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<EnrollmentPeriods, PeriodDetails>()
                .ForMember(
                    destination => destination.PeriodLength,
                    option => option.MapFrom(source => source.EnrollmentLength)).ForMember(
                    destination => destination.PeriodNumber,
                    option => option.MapFrom(source => source.EnrollmentPeriod));
        }
    }
}
