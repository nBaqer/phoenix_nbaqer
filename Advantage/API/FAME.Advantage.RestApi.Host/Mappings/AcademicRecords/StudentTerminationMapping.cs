﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentTerminationMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the StudentTerminationMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.StudentTermination;

    /// <summary>
    /// The student termination mapping.
    /// </summary>
    public class StudentTerminationMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to Student Termination.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArR2t4terminationDetails, StudentTermination>()
                .ForMember(destination => destination.TerminationId, option => option.MapFrom(source => source.TerminationId))
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollmentId))
                .ForMember(destination => destination.StatusCodeId, option => option.MapFrom(source => source.StatusCodeId))
                .ForMember(destination => destination.DropReasonId, option => option.MapFrom(source => source.DropReasonId))
                .ForMember(destination => destination.DateWithdrawalDetermined, option => option.MapFrom(source => source.DateWithdrawalDetermined))
                .ForMember(destination => destination.LastDateAttended, option => option.MapFrom(source => source.LastDateAttended))
                .ForMember(destination => destination.IsPerformingR2T4Calculator, option => option.MapFrom(source => source.IsPerformingR2t4calculator))
                .ForMember(destination => destination.CalculationPeriodType, option => option.MapFrom(source => source.CalculationPeriodTypeId))
                .ForMember(destination => destination.CreatedDate, option => option.MapFrom(source => source.CreatedDate))
                .ForMember(destination => destination.CreatedBy, option => option.MapFrom(source => source.CreatedById))
                .ForMember(destination => destination.UpdatedBy, option => option.MapFrom(source => source.UpdatedById))
                .ForMember(destination => destination.UpdatedDate, option => option.MapFrom(source => source.UpdatedDate))
                .ForMember(destination => destination.IsR2T4ApproveTabEnabled, option => option.MapFrom(source => source.IsR2t4approveTabEnabled))
                .ForMember(destination => destination.ResultStatus, option => option.Ignore())
                .ForMember(destination => destination.MethodType, option => option.Ignore())
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollmentId)).ReverseMap()
                .ForMember(destination => destination.CalculationPeriodTypeId, option => option.MapFrom(source => source.CalculationPeriodType))
                .ForMember(destination => destination.IsTerminationReversed, option => option.MapFrom(source => source.IsTerminationReversed))
                .ForAllOtherMembers(option => option.Ignore());
        }
    }
}
