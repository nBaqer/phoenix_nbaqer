﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4OverrideResultMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the R2T4OverrideResultMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.R2T4Result;

    /// <inheritdoc />
    /// <summary>
    /// The R2T4 override result mapping. This will provide a mapping between DTO and Entitiy of R2T4 result model
    /// </summary>
    public class R2T4OverrideResultMapping : IMappingDefinition
    {
        /// <inheritdoc />
        /// <summary>
        /// The map entity class creates mapping between R2T4OverrideResultsEntity and R2T4Result classes.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper) =>
            mapper.CreateMap<ArR2t4overrideResults, R2T4Result>()
                .ForMember(
                    destination => destination.R2T4ResultsId,
                    option => option.MapFrom(source => source.R2t4overrideResultsId))
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.SubTotalAmountDisbursedA,
                    option => option.MapFrom(source => source.SubTotalAmountDisbursedA))
                .ForMember(
                    destination => destination.SubTotalAmountCouldDisbursedC,
                    option => option.MapFrom(source => source.SubTotalAmountCouldDisbursedC))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedB,
                    option => option.MapFrom(source => source.SubTotalNetAmountDisbursedB))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedD,
                    option => option.MapFrom(source => source.SubTotalNetAmountDisbursedD))
                .ForMember(destination => destination.BoxEResult, option => option.MapFrom(source => source.BoxEresult))
                .ForMember(destination => destination.BoxFResult, option => option.MapFrom(source => source.BoxFresult))
                .ForMember(destination => destination.BoxGResult, option => option.MapFrom(source => source.BoxGresult))
                .ForMember(
                    destination => destination.PercentageOfActualAttendance,
                    option => option.MapFrom(source => source.PercentageOfActualAttendence))
                .ForMember(destination => destination.BoxHResult, option => option.MapFrom(source => source.BoxHresult))
                .ForMember(destination => destination.BoxIResult, option => option.MapFrom(source => source.BoxIresult))
                .ForMember(destination => destination.BoxJResult, option => option.MapFrom(source => source.BoxJresult))
                .ForMember(destination => destination.BoxKResult, option => option.MapFrom(source => source.BoxKresult))
                .ForMember(destination => destination.BoxLResult, option => option.MapFrom(source => source.BoxLresult))
                .ForMember(destination => destination.BoxMResult, option => option.MapFrom(source => source.BoxMresult))
                .ForMember(destination => destination.BoxNResult, option => option.MapFrom(source => source.BoxNresult))
                .ForMember(destination => destination.BoxOResult, option => option.MapFrom(source => source.BoxOresult))
                .ForMember(
                    destination => destination.UnsubDirectLoanSchoolReturn,
                    option => option.MapFrom(source => source.UnsubDirectLoanSchoolReturn))
                .ForMember(
                    destination => destination.SubDirectLoanSchoolReturn,
                    option => option.MapFrom(source => source.SubDirectLoanSchoolReturn))
                .ForMember(
                    destination => destination.PerkinsLoanSchoolReturn,
                    option => option.MapFrom(source => source.PerkinsLoanSchoolReturn))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanSchoolReturn,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanSchoolReturn))
                .ForMember(
                    destination => destination.DirectParentPlusLoanSchoolReturn,
                    option => option.MapFrom(source => source.DirectParentPlusLoanSchoolReturn))
                .ForMember(destination => destination.BoxPResult, option => option.MapFrom(source => source.BoxPresult))
                .ForMember(
                    destination => destination.PellGrantSchoolReturn,
                    option => option.MapFrom(source => source.PellGrantSchoolReturn))
                .ForMember(
                    destination => destination.FseogSchoolReturn,
                    option => option.MapFrom(source => source.FseogschoolReturn))
                .ForMember(
                    destination => destination.TeachGrantSchoolReturn,
                    option => option.MapFrom(source => source.TeachGrantSchoolReturn))
                .ForMember(
                    destination => destination.IraqAfgGrantSchoolReturn,
                    option => option.MapFrom(source => source.IraqAfgGrantSchoolReturn))
                .ForMember(destination => destination.BoxQResult, option => option.MapFrom(source => source.BoxQresult))
                .ForMember(destination => destination.BoxRResult, option => option.MapFrom(source => source.BoxRresult))
                .ForMember(destination => destination.BoxSResult, option => option.MapFrom(source => source.BoxSresult))
                .ForMember(destination => destination.BoxTResult, option => option.MapFrom(source => source.BoxTresult))
                .ForMember(destination => destination.BoxUResult, option => option.MapFrom(source => source.BoxUresult))
                .ForMember(
                    destination => destination.PellGrantAmountToReturn,
                    option => option.MapFrom(source => source.PellGrantAmountToReturn))
                .ForMember(
                    destination => destination.FseogAmountToReturn,
                    option => option.MapFrom(source => source.FseogamountToReturn))
                .ForMember(
                    destination => destination.TeachGrantAmountToReturn,
                    option => option.MapFrom(source => source.TeachGrantAmountToReturn))
                .ForMember(
                    destination => destination.IraqAfgGrantAmountToReturn,
                    option => option.MapFrom(source => source.IraqAfgGrantAmountToReturn))
                .ForMember(
                    destination => destination.PellGrantDisbursed,
                    option => option.MapFrom(source => source.PellGrantDisbursed))
                .ForMember(
                    destination => destination.PellGrantCouldDisbursed,
                    option => option.MapFrom(source => source.PellGrantCouldDisbursed))
                .ForMember(
                    destination => destination.FseogDisbursed,
                    option => option.MapFrom(source => source.Fseogdisbursed))
                .ForMember(
                    destination => destination.FseogCouldDisbursed,
                    option => option.MapFrom(source => source.FseogcouldDisbursed))
                .ForMember(
                    destination => destination.TeachGrantDisbursed,
                    option => option.MapFrom(source => source.TeachGrantDisbursed))
                .ForMember(
                    destination => destination.TeachGrantCouldDisbursed,
                    option => option.MapFrom(source => source.TeachGrantCouldDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantDisbursed))
                .ForMember(
                    destination => destination.IraqAfgGrantCouldDisbursed,
                    option => option.MapFrom(source => source.IraqAfgGrantCouldDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.UnsubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.UnsubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountDisbursed))
                .ForMember(
                    destination => destination.SubLoanNetAmountCouldDisbursed,
                    option => option.MapFrom(source => source.SubLoanNetAmountCouldDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanDisbursed))
                .ForMember(
                    destination => destination.PerkinsLoanCouldDisbursed,
                    option => option.MapFrom(source => source.PerkinsLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanDisbursed))
                .ForMember(
                    destination => destination.DirectParentPlusLoanCouldDisbursed,
                    option => option.MapFrom(source => source.DirectParentPlusLoanCouldDisbursed))
                .ForMember(
                    destination => destination.IsAttendanceNotRequired,
                    option => option.MapFrom(source => source.IsAttendanceNotRequired))
                .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.StartDate))
                .ForMember(
                    destination => destination.ScheduledEndDate,
                    option => option.MapFrom(source => source.ScheduledEndDate))
                .ForMember(
                    destination => destination.WithdrawalDate,
                    option => option.MapFrom(source => source.WithdrawalDate))
                .ForMember(
                    destination => destination.CompletedTime,
                    option => option.MapFrom(source => source.CompletedTime))
                .ForMember(destination => destination.TotalTime, option => option.MapFrom(source => source.TotalTime))
                .ForMember(destination => destination.TuitionFee, option => option.MapFrom(source => source.TuitionFee))
                .ForMember(destination => destination.RoomFee, option => option.MapFrom(source => source.RoomFee))
                .ForMember(destination => destination.BoardFee, option => option.MapFrom(source => source.BoardFee))
                .ForMember(destination => destination.OtherFee, option => option.MapFrom(source => source.OtherFee))
                .ForMember(
                    destination => destination.CreditBalanceRefunded,
                    option => option.MapFrom(source => source.CreditBalanceRefunded))
                .ForMember(
                    destination => destination.PostWithdrawalData,
                    option => option.MapFrom(source => source.PostWithdrawalData))
                .ForMember(
                    destination => destination.OverriddenData,
                    option => option.MapFrom(source => source.OverriddenData))
                .ForMember(
                    destination => destination.TicketNumber,
                    option => option.MapFrom(source => source.TicketNumber))
                .ForMember(
                    destination => destination.CreatedById,
                    option => option.MapFrom(source => source.CreatedById))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(
                    destination => destination.UpdatedById,
                    option => option.MapFrom(source => source.UpdatedById)).ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate)).ForMember(
                    destination => destination.ResultStatus,
                    option => option.Ignore())

                // Reverse mapping
                .ReverseMap()

                // Ignore navigational properties
                .ForMember(destination => destination.Termination, option => option.Ignore())
                .ForMember(destination => destination.CreatedBy, option => option.Ignore())
                .ForMember(destination => destination.UpdatedBy, option => option.Ignore())
                .ForMember(
                    destination => destination.TerminationId,
                    option => option.MapFrom(source => source.TerminationId))
                .ForMember(
                    destination => destination.SubTotalAmountDisbursedA,
                    option => option.MapFrom(source => source.SubTotalAmountDisbursedA))
                .ForMember(
                    destination => destination.SubTotalAmountCouldDisbursedC,
                    option => option.MapFrom(source => source.SubTotalAmountCouldDisbursedC))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedB,
                    option => option.MapFrom(source => source.SubTotalNetAmountDisbursedB))
                .ForMember(
                    destination => destination.SubTotalNetAmountDisbursedD,
                    option => option.MapFrom(source => source.SubTotalNetAmountDisbursedD))
                .ForMember(destination => destination.BoxEresult, option => option.MapFrom(source => source.BoxEResult))
                .ForMember(destination => destination.BoxFresult, option => option.MapFrom(source => source.BoxFResult))
                .ForMember(destination => destination.BoxGresult, option => option.MapFrom(source => source.BoxGResult))
                .ForMember(
                    destination => destination.PercentageOfActualAttendence,
                    option => option.MapFrom(source => source.PercentageOfActualAttendance))
                .ForMember(destination => destination.BoxHresult, option => option.MapFrom(source => source.BoxHResult))
                .ForMember(destination => destination.BoxIresult, option => option.MapFrom(source => source.BoxIResult))
                .ForMember(destination => destination.BoxJresult, option => option.MapFrom(source => source.BoxJResult))
                .ForMember(destination => destination.BoxKresult, option => option.MapFrom(source => source.BoxKResult))
                .ForMember(destination => destination.BoxLresult, option => option.MapFrom(source => source.BoxLResult))
                .ForMember(destination => destination.BoxMresult, option => option.MapFrom(source => source.BoxMResult))
                .ForMember(destination => destination.BoxNresult, option => option.MapFrom(source => source.BoxNResult))
                .ForMember(destination => destination.BoxOresult, option => option.MapFrom(source => source.BoxOResult))
                .ForMember(
                    destination => destination.UnsubDirectLoanSchoolReturn,
                    option => option.MapFrom(source => source.UnsubDirectLoanSchoolReturn))
                .ForMember(
                    destination => destination.SubDirectLoanSchoolReturn,
                    option => option.MapFrom(source => source.SubDirectLoanSchoolReturn))
                .ForMember(
                    destination => destination.PerkinsLoanSchoolReturn,
                    option => option.MapFrom(source => source.PerkinsLoanSchoolReturn))
                .ForMember(
                    destination => destination.DirectGraduatePlusLoanSchoolReturn,
                    option => option.MapFrom(source => source.DirectGraduatePlusLoanSchoolReturn))
                .ForMember(
                    destination => destination.DirectParentPlusLoanSchoolReturn,
                    option => option.MapFrom(source => source.DirectParentPlusLoanSchoolReturn))
                .ForMember(destination => destination.BoxPresult, option => option.MapFrom(source => source.BoxPResult))
                .ForMember(
                    destination => destination.PellGrantSchoolReturn,
                    option => option.MapFrom(source => source.PellGrantSchoolReturn))
                .ForMember(
                    destination => destination.FseogschoolReturn,
                    option => option.MapFrom(source => source.FseogSchoolReturn))
                .ForMember(
                    destination => destination.TeachGrantSchoolReturn,
                    option => option.MapFrom(source => source.TeachGrantSchoolReturn))
                .ForMember(
                    destination => destination.IraqAfgGrantSchoolReturn,
                    option => option.MapFrom(source => source.IraqAfgGrantSchoolReturn))
                .ForMember(destination => destination.BoxQresult, option => option.MapFrom(source => source.BoxQResult))
                .ForMember(destination => destination.BoxRresult, option => option.MapFrom(source => source.BoxRResult))
                .ForMember(destination => destination.BoxSresult, option => option.MapFrom(source => source.BoxSResult))
                .ForMember(destination => destination.BoxTresult, option => option.MapFrom(source => source.BoxTResult))
                .ForMember(destination => destination.BoxUresult, option => option.MapFrom(source => source.BoxUResult))
                .ForMember(
                    destination => destination.PellGrantAmountToReturn,
                    option => option.MapFrom(source => source.PellGrantAmountToReturn))
                .ForMember(
                    destination => destination.FseogamountToReturn,
                    option => option.MapFrom(source => source.FseogAmountToReturn))
                .ForMember(
                    destination => destination.TeachGrantAmountToReturn,
                    option => option.MapFrom(source => source.TeachGrantAmountToReturn))
                .ForMember(
                    destination => destination.IraqAfgGrantAmountToReturn,
                    option => option.MapFrom(source => source.IraqAfgGrantAmountToReturn))
                .ForMember(
                    destination => destination.TicketNumber,
                    option => option.MapFrom(source => source.TicketNumber))
                .ForMember(
                    destination => destination.CreatedById,
                    option => option.MapFrom(source => source.CreatedById))
                .ForMember(
                    destination => destination.CreatedDate,
                    option => option.MapFrom(source => source.CreatedDate))
                .ForMember(
                    destination => destination.UpdatedById,
                    option => option.MapFrom(source => source.UpdatedById)).ForMember(
                    destination => destination.UpdatedDate,
                    option => option.MapFrom(source => source.UpdatedDate))

        .ForMember(
            destination => destination.IsR2t4overrideResultsCompleted,
            option => option.MapFrom(source => source.IsR2T4OverrideResultsCompleted));
    }
}
