﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramScheduleDetailMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program schedule detail mapping between ProgramScheduleDetails DTO and ArProgScheduleDetails Entituy
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration; 
    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;

    /// <summary>
    /// The program schedule detail mapping between ProgramScheduleDetails DTO and ArProgScheduleDetails Entituy
    /// </summary>
    public class ProgramScheduleDetailMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArProgScheduleDetails, ProgramScheduleDetails>()
                .ForMember(destination => destination.AllowEarlyin, option => option.MapFrom(source => source.AllowEarlyin))
                .ForMember(destination => destination.AllowExtraHours, option => option.MapFrom(source => source.AllowExtrahours))
                .ForMember(destination => destination.AllowLateout, option => option.MapFrom(source => source.AllowLateout))
                .ForMember(destination => destination.CheckTardyin, option => option.MapFrom(source => source.CheckTardyin))
                .ForMember(destination => destination.DayOfWeek, option => option.MapFrom(source => source.Dw))
                .ForMember(destination => destination.Lunchin, option => option.MapFrom(source => source.Lunchin))
                .ForMember(destination => destination.Lunchout, option => option.MapFrom(source => source.Lunchout))
                .ForMember(destination => destination.Timein, option => option.MapFrom(source => source.Timein))
                .ForMember(destination => destination.Timeout, option => option.MapFrom(source => source.Timeout))
                .ForMember(destination => destination.MaxBeforeTardy, option => option.MapFrom(source => source.MaxBeforetardy))
                .ForMember(destination => destination.MaximumNumberOfLunch, option => option.MapFrom(source => source.Maxnolunch))
                .ForMember(destination => destination.TardyIntime, option => option.MapFrom(source => source.TardyIntime))
                .ForMember(destination => destination.Schedule, option => option.Ignore())
                .ForMember(destination => destination.ScheduleId, option => option.MapFrom(source => source.ScheduleId))
                .ForMember(destination => destination.ScheduleDetailId, option => option.MapFrom(source => source.ScheduleDetailId));
        }
    }
}
