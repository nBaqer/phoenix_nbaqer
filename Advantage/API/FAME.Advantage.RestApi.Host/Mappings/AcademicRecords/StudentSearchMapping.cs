﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentSearchMapping.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   The student search mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.Students;
    using FAME.Advantage.RestApi.Host.Infrastructure;

    /// <summary>
    /// The student search mapping.
    /// </summary>
    public class StudentSearchMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the output from database query to list of Students
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<AdLeads, IListItem<string, StudentSearchOutput>>().ForMember(
                x => x.Value,
                opt => opt.ResolveUsing(
                    model => new StudentSearchOutput()
                                 {
                                     StudentId = model.StudentId,
                                     Ssn = !string.IsNullOrEmpty(model.Ssn) ? model.Ssn.Replace(model.Ssn.Substring(0, 5), SsnMask.Ssn) : "",
                                     DisplayName = ((model.LastName ?? "" ) + ", " + model.FirstName + " " + (model.MiddleName ?? "" )).TrimEnd()
                    }));
        }
    }
}