﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionDetailsMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The program version mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramVersion;

    /// <summary>
    /// The program version details mapping between Entity ArPrgVersions and DTO ProgramVersionDetails
    /// </summary>
    public class ProgramVersionDetailsMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArPrgVersions, ProgramVersionDetails>()
                .ForMember(destination => destination.ProgramVersionId, option => option.MapFrom(source => source.PrgVerId))
                    .ForMember(destination => destination.ProgramId, option => option.MapFrom(source => source.ProgId));
        }
    }
}
