﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentClockAttendanceMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The student clock attendance mapping between Entity ArStudentClockAttendance and DTO StudentClockAttendance
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.AcademicRecords
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.AcademicRecords.ProgramSchedule;

    /// <summary>
    /// The student clock attendance mapping between Entity ArStudentClockAttendance and DTO StudentClockAttendance
    /// </summary>
    public class StudentClockAttendanceMapping : IMappingDefinition
    {
        /// <summary>
        /// The map entity.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArStudentClockAttendance, StudentClockAttendance>()
                .ForMember(destination => destination.ActualHours, option => option.MapFrom(source => source.ActualHours))
                .ForMember(destination => destination.Comments, option => option.MapFrom(source => source.Comments))
                .ForMember(destination => destination.Converted, option => option.MapFrom(source => source.Converted))
                .ForMember(destination => destination.IsTardy, option => option.MapFrom(source => source.IsTardy))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.PostByException, option => option.MapFrom(source => source.PostByException))
                .ForMember(destination => destination.RecordDate, option => option.MapFrom(source => source.RecordDate))
                .ForMember(destination => destination.ScheduleHours, option => option.MapFrom(source => source.SchedHours))
                .ForMember(destination => destination.ScheduleId, option => option.MapFrom(source => source.ScheduleId))
                .ForMember(destination => destination.StudentEnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.TardyProcessed, option => option.MapFrom(source => source.TardyProcessed))
                .ForMember(destination => destination.SchedHoursOnTermination, option => option.MapFrom(source => source.SchedHoursOnTermination));
        }
    }
}
