﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionSAPMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The Program Version SAP mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;

namespace FAME.Advantage.RestApi.Host.Mappings.FinacialAid
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.Common;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;

    /// <summary>
    /// The program version sap mapping class that contains many of the core details needed to run the sap procedure.
    /// </summary>
    public class ProgramVersionSAPMapping : IMappingDefinition
    {
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper
                .CreateMap<ArStuEnrollments, ProgramVersionSAP>()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.PrgVerId))
                .ForMember(destination => destination.EnrollmentId, option => option.MapFrom(source => source.StuEnrollId))
                .ForMember(destination => destination.CampusId, option => option.MapFrom(source => source.CampusId))
                    .ForMember(destination => destination.SapId, option => option.MapFrom(source => source.PrgVer.Sapid))
                    .ForMember(destination => destination.SysStatusId, option => option.MapFrom(source => source.StatusCode.SysStatusId))
                .ForMember(destination => destination.EnrollmentStartDate,
                    option => option.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.TitleIVSapId,
                    option => option.MapFrom(source => source.PrgVer.Fasapid))
                .ForMember(destination => destination.Credits, option => option.MapFrom(source => source.PrgVer.Credits))
                .ForMember(destination => destination.ProgramVersionDescription,
                    option => option.MapFrom(source => source.PrgVer.PrgVerDescrip))
                .ForMember(destination => destination.TriggerUnitTypeId,
                    option => option.MapFrom(source => source.PrgVer.Fasap.TrigUnitTypId))
                .ForMember(destination => destination.TriggerOffsetTypeId,
                    option => option.MapFrom(source => source.PrgVer.Fasap.TrigOffsetTypId))
                .ForMember(destination => destination.MinimumTermGPA,
                    option => option.MapFrom(source => source.PrgVer.Fasap.MinTermGpa))
                .ForMember(destination => destination.TermGPAOver,
                    option => option.MapFrom(source => source.PrgVer.Fasap.TermGpaover))
                .ForMember(destination => destination.TrackExternshipAttendance,
                    option => option.MapFrom(source => source.PrgVer.Fasap.TrackExternAttendance))
                .ForMember(destination => destination.IncludeTransferHours,
                    option => option.MapFrom(source => source.PrgVer.Fasap.IncludeTransferHours))
                .ForMember(destination => destination.TransferHours,
                    option => option.MapFrom(source => source.TransferHours))
                    .ForMember(destination => destination.ProgramRegistrationType,
                    option => option.MapFrom(source => source.PrgVer.ProgramRegistrationType.HasValue ? (Enums.ProgramRegistrationType)source.PrgVer.ProgramRegistrationType : Enums.ProgramRegistrationType.ByClass))
            .ForMember(destination => destination.AttendanceType, option => option.MapFrom(source => source.PrgVer.UnitType.UnitTypeDescrip))
            .ForMember(destination => destination.ClockHourProgram, option => option.MapFrom(source => source.PrgVer.Prog.Ac.Acdescrip.Contains("Clock") ? source.PrgVer.Prog.Acid : (int?)null));

            mapper.CreateMap<ArPrgVersions, ProgramVersionSAP>()
            .ForMember(destination => destination.Id, option => option.MapFrom(source => source.PrgVerId))
                .ForMember(destination => destination.EnrollmentId, option => option.MapFrom(source => source.ArStuEnrollments.FirstOrDefault(pv => pv.PrgVerId == source.PrgVerId)))
            .ForMember(destination => destination.SapId, option => option.MapFrom(source => source.Sapid))
            .ForMember(destination => destination.TitleIVSapId,
                option => option.MapFrom(source => source.Fasapid))
            .ForMember(destination => destination.Credits, option => option.MapFrom(source => source.Credits))
            .ForMember(destination => destination.ProgramVersionDescription,
                option => option.MapFrom(source => source.PrgVerDescrip))
            .ForMember(destination => destination.TriggerUnitTypeId,
                option => option.MapFrom(source => source.Fasap.TrigUnitTypId))
            .ForMember(destination => destination.TriggerOffsetTypeId,
                option => option.MapFrom(source => source.Fasap.TrigOffsetTypId))
            .ForMember(destination => destination.MinimumTermGPA,
                option => option.MapFrom(source => source.Fasap.MinTermGpa))
            .ForMember(destination => destination.TermGPAOver,
                option => option.MapFrom(source => source.Fasap.TermGpaover))
            .ForMember(destination => destination.TrackExternshipAttendance,
                option => option.MapFrom(source => source.Fasap.TrackExternAttendance))
            .ForMember(destination => destination.IncludeTransferHours,
                option => option.MapFrom(source => source.Fasap.IncludeTransferHours))
            .ForMember(destination => destination.ProgramRegistrationType,
                option => option.MapFrom(source => source.ProgramRegistrationType.HasValue ? (Enums.ProgramRegistrationType)source.ProgramRegistrationType : Enums.ProgramRegistrationType.ByClass))
        .ForMember(destination => destination.AttendanceType, option => option.MapFrom(source => source.UnitType.UnitTypeDescrip))
        .ForMember(destination => destination.ClockHourProgram, option => option.MapFrom(source => source.Prog.Ac.Acdescrip.Contains("Clock") ? source.Prog.Acid : (int?)null));
        }

    }

}


