﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SAPPolicyDetailMapping.cs" company="FAME.inc">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The SAP policy detail mapping.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.FinacialAid
{
    using AutoMapper.Configuration;
    using Fame.EFCore.Advantage.Entities;
    using FAME.Advantage.RestApi.DataTransferObjects.FinancialAid;

    /// <summary>
    /// The sap policy detail mapping class maps the fields for the sap details.
    /// </summary>
    public class SAPPolicyDetailMapping : IMappingDefinition
    {
        public void MapEntity(MapperConfigurationExpression mapper) => mapper.CreateMap<ArSapdetails, SAPPolicyDetail>()
            .ForMember(destination => destination.Id, option => option.MapFrom(source => source.SapdetailId))
            .ForMember(destination => destination.QualitativeMinimumValue, option => option.MapFrom(source => source.QualMinValue))
            .ForMember(destination => destination.QuantitativeMinimumValue, option => option.MapFrom(source => source.QuantMinValue))
            .ForMember(destination => destination.QualitativeMinimumTypeId, option => option.MapFrom(source => source.QualMinTypId))
            .ForMember(destination => destination.QuantitativeMinimumUnitTypeId, option => option.MapFrom(source => source.QuantMinUnitTypId))
            .ForMember(destination => destination.TriggerValue, option => option.MapFrom(source => source.TrigValue))
            .ForMember(destination => destination.TriggerUnitTypeId, option => option.MapFrom(source => source.TrigUnitTypId))
            .ForMember(destination => destination.TriggerOffsetTypeId, option => option.MapFrom(source => source.TrigOffsetTypId))
            .ForMember(destination => destination.TriggerOffsetSequence, option => option.MapFrom(source => source.TrigOffsetSeq))
            .ForMember(destination => destination.MinimumCreditsCompleted, option => option.MapFrom(source => source.MinCredsCompltd))
            .ForMember(destination => destination.MinimumAttendanceValue, option => option.MapFrom(source => source.MinAttendanceValue));
    }

}


