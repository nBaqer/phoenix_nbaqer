﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TermsMapping.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the TermsMapping type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.Advantage.RestApi.Host.Mappings.FinacialAid
{
    using AutoMapper.Configuration;

    using Fame.EFCore.Advantage.Entities;

    using FAME.Advantage.RestApi.DataTransferObjects.Terms;

    /// <summary>
    /// The terms mapping maps ArTerm and Term.
    /// </summary>
    public class TermsMapping : IMappingDefinition
    {
        /// <summary>
        /// The MapEntity maps the ArTerm and Term.
        /// </summary>
        /// <param name="mapper">
        /// The mapper.
        /// </param>
        public void MapEntity(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<ArTerm, Term>()
                .ForMember(destination => destination.Id, option => option.MapFrom(source => source.TermId))
                .ForMember(destination => destination.Code, option => option.MapFrom(source => source.TermCode))
                .ForMember(destination => destination.StatusId, option => option.MapFrom(source => source.StatusId))
                .ForMember(destination => destination.Description, option => option.MapFrom(source => source.TermDescrip))
                .ForMember(destination => destination.CampusGroupId, option => option.MapFrom(source => source.CampGrpId))
                .ForMember(destination => destination.StartDate, option => option.MapFrom(source => source.StartDate))
                .ForMember(destination => destination.EndDate, option => option.MapFrom(source => source.EndDate))
                .ForMember(destination => destination.ShiftId, option => option.MapFrom(source => source.ShiftId))
                .ForMember(destination => destination.ModifiedUser, option => option.MapFrom(source => source.ModUser))
                .ForMember(destination => destination.ModifiedDate, option => option.MapFrom(source => source.ModDate))
                .ForMember(destination => destination.IsModule, option => option.MapFrom(source => source.IsModule))
                .ForMember(destination => destination.TermTypeId, option => option.MapFrom(source => source.TermTypeId))
                .ForMember(destination => destination.ProgramId, option => option.MapFrom(source => source.ProgId))
                .ForMember(destination => destination.MidPointDate, option => option.MapFrom(source => source.MidPtDate))
                .ForMember(destination => destination.MaximumGraduationDate, option => option.MapFrom(source => source.MaxGradDate));
        }
    }
}
