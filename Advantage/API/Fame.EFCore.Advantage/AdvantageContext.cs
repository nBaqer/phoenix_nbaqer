﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdvantageContext.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The Advantage context.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
    using Fame.EFCore.Advantage.Interfaces;
    using Fame.EFCore.Infrastructure;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using System;

    /*<summary>
     This is the scaffolded Advantage database context that contains all the db sets and model builder definitions for Advantage.
    </summary>*/
    public partial class AdvantageContext : DatabaseContext, IAdvantageContext
    {
        public AdvantageContext()
        {
        }

        public AdvantageContext(DbContextOptions options)
            : base(options)
        {
        }

        public AdvantageContext(DbContextOptions<AdvantageContext> options)
            : base(options)
        {
        }

        void IAdvantageContext.SaveChanges()
        {
            base.SaveChanges();
        }

        /*<summary>The db set for AdAdminCriteria</summary>*/
        public virtual DbSet<AdAdminCriteria> AdAdminCriteria { get; set; }
        /*<summary>The db set for AdAdvInterval</summary>*/
        public virtual DbSet<AdAdvInterval> AdAdvInterval { get; set; }
        /*<summary>The db set for AdAgencySponsors</summary>*/
        public virtual DbSet<AdAgencySponsors> AdAgencySponsors { get; set; }
        /*<summary>The db set for AdCitizenships</summary>*/
        public virtual DbSet<AdCitizenships> AdCitizenships { get; set; }
        /*<summary>The db set for AdColleges</summary>*/
        public virtual DbSet<AdColleges> AdColleges { get; set; }
        /*<summary>The db set for AdCounties</summary>*/
        public virtual DbSet<AdCounties> AdCounties { get; set; }
        /*<summary>The db set for AdCountries</summary>*/
        public virtual DbSet<AdCountries> AdCountries { get; set; }
        /*<summary>The db set for AdDegCertSeeking</summary>*/
        public virtual DbSet<AdDegCertSeeking> AdDegCertSeeking { get; set; }
        /*<summary>The db set for AdDependencyTypes</summary>*/
        public virtual DbSet<AdDependencyTypes> AdDependencyTypes { get; set; }
        /*<summary>The db set for AdEdLvls</summary>*/
        public virtual DbSet<AdEdLvls> AdEdLvls { get; set; }
        /*<summary>The db set for AdEntrTestOverRide</summary>*/
        public virtual DbSet<AdEntrTestOverRide> AdEntrTestOverRide { get; set; }
        /*<summary>The db set for AdEthCodes</summary>*/
        public virtual DbSet<AdEthCodes> AdEthCodes { get; set; }
        /*<summary>The db set for AdExpertiseLevel</summary>*/
        public virtual DbSet<AdExpertiseLevel> AdExpertiseLevel { get; set; }
        /*<summary>The db set for AdExpQuickLeadSections</summary>*/
        public virtual DbSet<AdExpQuickLeadSections> AdExpQuickLeadSections { get; set; }
        /*<summary>The db set for AdExtraCurr</summary>*/
        public virtual DbSet<AdExtraCurr> AdExtraCurr { get; set; }
        /*<summary>The db set for AdExtraCurrGrp</summary>*/
        public virtual DbSet<AdExtraCurrGrp> AdExtraCurrGrp { get; set; }
        /*<summary>The db set for AdExtraCurricular</summary>*/
        public virtual DbSet<AdExtraCurricular> AdExtraCurricular { get; set; }
        /*<summary>The db set for AdFullPartTime</summary>*/
        public virtual DbSet<AdFullPartTime> AdFullPartTime { get; set; }
        /*<summary>The db set for AdGenders</summary>*/
        public virtual DbSet<AdGenders> AdGenders { get; set; }
        /*<summary>The db set for AdGeographicTypes</summary>*/
        public virtual DbSet<AdGeographicTypes> AdGeographicTypes { get; set; }
        /*<summary>The db set for AdHighSchools</summary>*/
        public virtual DbSet<AdHighSchools> AdHighSchools { get; set; }
        /*<summary>The db set for AdImportLeadsAdvFldMappings</summary>*/
        public virtual DbSet<AdImportLeadsAdvFldMappings> AdImportLeadsAdvFldMappings { get; set; }
        /*<summary>The db set for AdImportLeadsFields</summary>*/
        public virtual DbSet<AdImportLeadsFields> AdImportLeadsFields { get; set; }
        /*<summary>The db set for AdImportLeadsLookupValsMap</summary>*/
        public virtual DbSet<AdImportLeadsLookupValsMap> AdImportLeadsLookupValsMap { get; set; }
        /*<summary>The db set for AdImportLeadsMappings</summary>*/
        public virtual DbSet<AdImportLeadsMappings> AdImportLeadsMappings { get; set; }
        /*<summary>The db set for AdLastNameHistory</summary>*/
        public virtual DbSet<AdLastNameHistory> AdLastNameHistory { get; set; }
        /*<summary>The db set for AdLeadAddresses</summary>*/
        public virtual DbSet<AdLeadAddresses> AdLeadAddresses { get; set; }
        /*<summary>The db set for AdLeadByLeadGroups</summary>*/
        public virtual DbSet<AdLeadByLeadGroups> AdLeadByLeadGroups { get; set; }
        /*<summary>The db set for AdLeadDocsReceived</summary>*/
        public virtual DbSet<AdLeadDocsReceived> AdLeadDocsReceived { get; set; }
        /*<summary>The db set for AdLeadDuplicates</summary>*/
        public virtual DbSet<AdLeadDuplicates> AdLeadDuplicates { get; set; }
        /*<summary>The db set for AdLeadEducation</summary>*/
        public virtual DbSet<AdLeadEducation> AdLeadEducation { get; set; }
        /*<summary>The db set for AdLeadEmail</summary>*/
        public virtual DbSet<AdLeadEmail> AdLeadEmail { get; set; }
        /*<summary>The db set for AdLeadEmployment</summary>*/
        public virtual DbSet<AdLeadEmployment> AdLeadEmployment { get; set; }
        /*<summary>The db set for AdLeadEntranceTest</summary>*/
        public virtual DbSet<AdLeadEntranceTest> AdLeadEntranceTest { get; set; }
        /*<summary>The db set for AdLeadExtraCurriculars</summary>*/
        public virtual DbSet<AdLeadExtraCurriculars> AdLeadExtraCurriculars { get; set; }
        /*<summary>The db set for AdLeadFields</summary>*/
        public virtual DbSet<AdLeadFields> AdLeadFields { get; set; }
        /*<summary>The db set for AdLeadGroups</summary>*/
        public virtual DbSet<AdLeadGroups> AdLeadGroups { get; set; }
        /*<summary>The db set for AdLeadGrpReqGroups</summary>*/
        public virtual DbSet<AdLeadGrpReqGroups> AdLeadGrpReqGroups { get; set; }
        /*<summary>The db set for AdLeadImports</summary>*/
        public virtual DbSet<AdLeadImports> AdLeadImports { get; set; }
        /*<summary>The db set for AdLeadNotes</summary>*/
        public virtual DbSet<AdLeadNotes> AdLeadNotes { get; set; }
        /*<summary>The db set for AdLeadNotes1</summary>*/
        public virtual DbSet<AdLeadNotes1> AdLeadNotes1 { get; set; }
        /*<summary>The db set for AdLeadOtherContacts</summary>*/
        public virtual DbSet<AdLeadOtherContacts> AdLeadOtherContacts { get; set; }
        /*<summary>The db set for AdLeadOtherContactsAddreses</summary>*/
        public virtual DbSet<AdLeadOtherContactsAddreses> AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The db set for AdLeadOtherContactsEmail</summary>*/
        public virtual DbSet<AdLeadOtherContactsEmail> AdLeadOtherContactsEmail { get; set; }
        /*<summary>The db set for AdLeadOtherContactsPhone</summary>*/
        public virtual DbSet<AdLeadOtherContactsPhone> AdLeadOtherContactsPhone { get; set; }
        /*<summary>The db set for AdLeadPayments</summary>*/
        public virtual DbSet<AdLeadPayments> AdLeadPayments { get; set; }
        /*<summary>The db set for AdLeadPhone</summary>*/
        public virtual DbSet<AdLeadPhone> AdLeadPhone { get; set; }
        /*<summary>The db set for AdLeadReqsReceived</summary>*/
        public virtual DbSet<AdLeadReqsReceived> AdLeadReqsReceived { get; set; }
        /*<summary>The db set for AdLeads</summary>*/
        public virtual DbSet<AdLeads> AdLeads { get; set; }
        /*<summary>The db set for AdLeadSkills</summary>*/
        public virtual DbSet<AdLeadSkills> AdLeadSkills { get; set; }
        /*<summary>The db set for AdLeadTranReceived</summary>*/
        public virtual DbSet<AdLeadTranReceived> AdLeadTranReceived { get; set; }
        /*<summary>The db set for AdLeadTransactions</summary>*/
        public virtual DbSet<AdLeadTransactions> AdLeadTransactions { get; set; }
        /*<summary>The db set for AdLevel</summary>*/
        public virtual DbSet<AdLevel> AdLevel { get; set; }
        /*<summary>The db set for AdMaritalStatus</summary>*/
        public virtual DbSet<AdMaritalStatus> AdMaritalStatus { get; set; }
        /*<summary>The db set for AdNationalities</summary>*/
        public virtual DbSet<AdNationalities> AdNationalities { get; set; }
        /*<summary>The db set for AdPrgVerTestDetails</summary>*/
        public virtual DbSet<AdPrgVerTestDetails> AdPrgVerTestDetails { get; set; }
        /*<summary>The db set for AdQuickLead</summary>*/
        public virtual DbSet<AdQuickLead> AdQuickLead { get; set; }
        /*<summary>The db set for AdQuickLeadMap</summary>*/
        public virtual DbSet<AdQuickLeadMap> AdQuickLeadMap { get; set; }
        /*<summary>The db set for AdQuickLeadSections</summary>*/
        public virtual DbSet<AdQuickLeadSections> AdQuickLeadSections { get; set; }
        /*<summary>The db set for AdReqGroups</summary>*/
        public virtual DbSet<AdReqGroups> AdReqGroups { get; set; }
        /*<summary>The db set for AdReqGrpDef</summary>*/
        public virtual DbSet<AdReqGrpDef> AdReqGrpDef { get; set; }
        /*<summary>The db set for AdReqLeadGroups</summary>*/
        public virtual DbSet<AdReqLeadGroups> AdReqLeadGroups { get; set; }
        /*<summary>The db set for AdReqs</summary>*/
        public virtual DbSet<AdReqs> AdReqs { get; set; }
        /*<summary>The db set for AdReqsEffectiveDates</summary>*/
        public virtual DbSet<AdReqsEffectiveDates> AdReqsEffectiveDates { get; set; }
        /*<summary>The db set for AdReqTypes</summary>*/
        public virtual DbSet<AdReqTypes> AdReqTypes { get; set; }
        /*<summary>The db set for AdSkills</summary>*/
        public virtual DbSet<AdSkills> AdSkills { get; set; }
        /*<summary>The db set for AdSourceAdvertisement</summary>*/
        public virtual DbSet<AdSourceAdvertisement> AdSourceAdvertisement { get; set; }
        /*<summary>The db set for AdSourceCatagory</summary>*/
        public virtual DbSet<AdSourceCatagory> AdSourceCatagory { get; set; }
        /*<summary>The db set for AdSourceType</summary>*/
        public virtual DbSet<AdSourceType> AdSourceType { get; set; }
        /*<summary>The db set for AdStudentGroups</summary>*/
        public virtual DbSet<AdStudentGroups> AdStudentGroups { get; set; }
        /*<summary>The db set for AdStuGrpStudents</summary>*/
        public virtual DbSet<AdStuGrpStudents> AdStuGrpStudents { get; set; }
        /*<summary>The db set for AdStuGrpTypes</summary>*/
        public virtual DbSet<AdStuGrpTypes> AdStuGrpTypes { get; set; }
        /*<summary>The db set for AdTitles</summary>*/
        public virtual DbSet<AdTitles> AdTitles { get; set; }
        /*<summary>The db set for AdVehicles</summary>*/
        public virtual DbSet<AdVehicles> AdVehicles { get; set; }
        /*<summary>The db set for AdVendorCampaign</summary>*/
        public virtual DbSet<AdVendorCampaign> AdVendorCampaign { get; set; }
        /*<summary>The db set for AdVendorPayFor</summary>*/
        public virtual DbSet<AdVendorPayFor> AdVendorPayFor { get; set; }
        /*<summary>The db set for AdVendors</summary>*/
        public virtual DbSet<AdVendors> AdVendors { get; set; }
        /*<summary>The db set for AdvProgTyps</summary>*/
        public virtual DbSet<AdvProgTyps> AdvProgTyps { get; set; }
        /*<summary>The db set for AfaCatalogMapping</summary>*/
        public virtual DbSet<AfaCatalogMapping> AfaCatalogMapping { get; set; }
        /*<summary>The db set for AfaLeadSyncException</summary>*/
        public virtual DbSet<AfaLeadSyncException> AfaLeadSyncException { get; set; }
        /*<summary>The db set for AfaPaymentPeriodStaging</summary>*/
        public virtual DbSet<AfaPaymentPeriodStaging> AfaPaymentPeriodStaging { get; set; }
        /*<summary>The db set for AllNotes</summary>*/
        public virtual DbSet<AllNotes> AllNotes { get; set; }
        /*<summary>The db set for ArAttendTypes</summary>*/
        public virtual DbSet<ArAttendTypes> ArAttendTypes { get; set; }
        /*<summary>The db set for ArAttUnitType</summary>*/
        public virtual DbSet<ArAttUnitType> ArAttUnitType { get; set; }
        /*<summary>The db set for ArBkCategories</summary>*/
        public virtual DbSet<ArBkCategories> ArBkCategories { get; set; }
        /*<summary>The db set for ArBooks</summary>*/
        public virtual DbSet<ArBooks> ArBooks { get; set; }
        /*<summary>The db set for ArBoolean</summary>*/
        public virtual DbSet<ArBoolean> ArBoolean { get; set; }
        /*<summary>The db set for ArBridgeGradeComponentTypesCourses</summary>*/
        public virtual DbSet<ArBridgeGradeComponentTypesCourses> ArBridgeGradeComponentTypesCourses { get; set; }
        /*<summary>The db set for ArBuildings</summary>*/
        public virtual DbSet<ArBuildings> ArBuildings { get; set; }
        /*<summary>The db set for ArCampusPrgVersions</summary>*/
        public virtual DbSet<ArCampusPrgVersions> ArCampusPrgVersions { get; set; }
        /*<summary>The db set for ArCertifications</summary>*/
        public virtual DbSet<ArCertifications> ArCertifications { get; set; }
        /*<summary>The db set for ArchiveArGrdBkResults</summary>*/
        public virtual DbSet<ArchiveArGrdBkResults> ArchiveArGrdBkResults { get; set; }
        /*<summary>The db set for ArchiveArResults</summary>*/
        public virtual DbSet<ArchiveArResults> ArchiveArResults { get; set; }
        /*<summary>The db set for ArClassSections</summary>*/
        public virtual DbSet<ArClassSections> ArClassSections { get; set; }
        /*<summary>The db set for ArClassSectionTerms</summary>*/
        public virtual DbSet<ArClassSectionTerms> ArClassSectionTerms { get; set; }
        /*<summary>The db set for ArClsSectionTimeClockPolicy</summary>*/
        public virtual DbSet<ArClsSectionTimeClockPolicy> ArClsSectionTimeClockPolicy { get; set; }
        /*<summary>The db set for ArClsSectMeetings</summary>*/
        public virtual DbSet<ArClsSectMeetings> ArClsSectMeetings { get; set; }
        /*<summary>The db set for ArClsSectStudents</summary>*/
        public virtual DbSet<ArClsSectStudents> ArClsSectStudents { get; set; }
        /*<summary>The db set for ArCollegeDivisions</summary>*/
        public virtual DbSet<ArCollegeDivisions> ArCollegeDivisions { get; set; }
        /*<summary>The db set for ArConsequenceTyps</summary>*/
        public virtual DbSet<ArConsequenceTyps> ArConsequenceTyps { get; set; }
        /*<summary>The db set for ArCourseBks</summary>*/
        public virtual DbSet<ArCourseBks> ArCourseBks { get; set; }
        /*<summary>The db set for ArCourseCategories</summary>*/
        public virtual DbSet<ArCourseCategories> ArCourseCategories { get; set; }
        /*<summary>The db set for ArCourseEquivalent</summary>*/
        public virtual DbSet<ArCourseEquivalent> ArCourseEquivalent { get; set; }
        /*<summary>The db set for ArCourseReqs</summary>*/
        public virtual DbSet<ArCourseReqs> ArCourseReqs { get; set; }
        /*<summary>The db set for ArCreditsPerService</summary>*/
        public virtual DbSet<ArCreditsPerService> ArCreditsPerService { get; set; }
        /*<summary>The db set for ArDegrees</summary>*/
        public virtual DbSet<ArDegrees> ArDegrees { get; set; }
        /*<summary>The db set for ArDepartments</summary>*/
        public virtual DbSet<ArDepartments> ArDepartments { get; set; }
        /*<summary>The db set for ArDropReasons</summary>*/
        public virtual DbSet<ArDropReasons> ArDropReasons { get; set; }
        /*<summary>The db set for ArExternshipAttendance</summary>*/
        public virtual DbSet<ArExternshipAttendance> ArExternshipAttendance { get; set; }
        /*<summary>The db set for ArFasapchkResults</summary>*/
        public virtual DbSet<ArFasapchkResults> ArFasapchkResults { get; set; }
        /*<summary>The db set for ArFerpacategory</summary>*/
        public virtual DbSet<ArFerpacategory> ArFerpacategory { get; set; }
        /*<summary>The db set for ArFerpaentity</summary>*/
        public virtual DbSet<ArFerpaentity> ArFerpaentity { get; set; }
        /*<summary>The db set for ArFerpapage</summary>*/
        public virtual DbSet<ArFerpapage> ArFerpapage { get; set; }
        /*<summary>The db set for ArFerpapolicy</summary>*/
        public virtual DbSet<ArFerpapolicy> ArFerpapolicy { get; set; }
        /*<summary>The db set for ArGradeScaleDetails</summary>*/
        public virtual DbSet<ArGradeScaleDetails> ArGradeScaleDetails { get; set; }
        /*<summary>The db set for ArGradeScales</summary>*/
        public virtual DbSet<ArGradeScales> ArGradeScales { get; set; }
        /*<summary>The db set for ArGradeSystemDetails</summary>*/
        public virtual DbSet<ArGradeSystemDetails> ArGradeSystemDetails { get; set; }
        /*<summary>The db set for ArGradeSystems</summary>*/
        public virtual DbSet<ArGradeSystems> ArGradeSystems { get; set; }
        /*<summary>The db set for ArGrdBkConversionResults</summary>*/
        public virtual DbSet<ArGrdBkConversionResults> ArGrdBkConversionResults { get; set; }
        /*<summary>The db set for ArGrdBkEvalTyps</summary>*/
        public virtual DbSet<ArGrdBkEvalTyps> ArGrdBkEvalTyps { get; set; }
        /*<summary>The db set for ArGrdBkResults</summary>*/
        public virtual DbSet<ArGrdBkResults> ArGrdBkResults { get; set; }
        /*<summary>The db set for ArGrdBkWeights</summary>*/
        public virtual DbSet<ArGrdBkWeights> ArGrdBkWeights { get; set; }
        /*<summary>The db set for ArGrdBkWgtDetails</summary>*/
        public virtual DbSet<ArGrdBkWgtDetails> ArGrdBkWgtDetails { get; set; }
        /*<summary>The db set for ArGrdComponentTypes</summary>*/
        public virtual DbSet<ArGrdComponentTypes> ArGrdComponentTypes { get; set; }
        /*<summary>The db set for ArGrdTyps</summary>*/
        public virtual DbSet<ArGrdTyps> ArGrdTyps { get; set; }
        /*<summary>The db set for ArHousing</summary>*/
        public virtual DbSet<ArHousing> ArHousing { get; set; }
        /*<summary>The db set for ArInstructionType</summary>*/
        public virtual DbSet<ArInstructionType> ArInstructionType { get; set; }
        /*<summary>The db set for ArInstructorsSupervisors</summary>*/
        public virtual DbSet<ArInstructorsSupervisors> ArInstructorsSupervisors { get; set; }
        /*<summary>The db set for ArLoareasons</summary>*/
        public virtual DbSet<ArLoareasons> ArLoareasons { get; set; }
        /*<summary>The db set for ArMentorGradeComponentTypesCourses</summary>*/
        public virtual DbSet<ArMentorGradeComponentTypesCourses> ArMentorGradeComponentTypesCourses { get; set; }
        /*<summary>The db set for ArOverridenConflicts</summary>*/
        public virtual DbSet<ArOverridenConflicts> ArOverridenConflicts { get; set; }
        /*<summary>The db set for ArOverridenPreReqs</summary>*/
        public virtual DbSet<ArOverridenPreReqs> ArOverridenPreReqs { get; set; }
        /*<summary>The db set for ArPeriod</summary>*/
        public virtual DbSet<ArPeriod> ArPeriod { get; set; }
        /*<summary>The db set for ArPostScoreDictationSpeedTest</summary>*/
        public virtual DbSet<ArPostScoreDictationSpeedTest> ArPostScoreDictationSpeedTest { get; set; }
        /*<summary>The db set for ArPrgChargePeriodSeq</summary>*/
        public virtual DbSet<ArPrgChargePeriodSeq> ArPrgChargePeriodSeq { get; set; }
        /*<summary>The db set for ArPrgGrp</summary>*/
        public virtual DbSet<ArPrgGrp> ArPrgGrp { get; set; }
        /*<summary>The db set for ArPrgMinorCerts</summary>*/
        public virtual DbSet<ArPrgMinorCerts> ArPrgMinorCerts { get; set; }
        /*<summary>The db set for ArPrgVerInstructionType</summary>*/
        public virtual DbSet<ArPrgVerInstructionType> ArPrgVerInstructionType { get; set; }
        /*<summary>The db set for ArPrgVersionFees</summary>*/
        public virtual DbSet<ArPrgVersionFees> ArPrgVersionFees { get; set; }
        /*<summary>The db set for ArPrgVersions</summary>*/
        public virtual DbSet<ArPrgVersions> ArPrgVersions { get; set; }
        /*<summary>The db set for ArProbWarningTypes</summary>*/
        public virtual DbSet<ArProbWarningTypes> ArProbWarningTypes { get; set; }
        /*<summary>The db set for ArProgCredential</summary>*/
        public virtual DbSet<ArProgCredential> ArProgCredential { get; set; }
        /*<summary>The db set for ArPrograms</summary>*/
        public virtual DbSet<ArPrograms> ArPrograms { get; set; }
        /*<summary>The db set for ArProgramVersionType</summary>*/
        public virtual DbSet<ArProgramVersionType> ArProgramVersionType { get; set; }
        /*<summary>The db set for ArProgScheduleDetails</summary>*/
        public virtual DbSet<ArProgScheduleDetails> ArProgScheduleDetails { get; set; }
        /*<summary>The db set for ArProgSchedules</summary>*/
        public virtual DbSet<ArProgSchedules> ArProgSchedules { get; set; }
        /*<summary>The db set for ArProgTypes</summary>*/
        public virtual DbSet<ArProgTypes> ArProgTypes { get; set; }
        /*<summary>The db set for ArProgVerDef</summary>*/
        public virtual DbSet<ArProgVerDef> ArProgVerDef { get; set; }
        /*<summary>The db set for ArPtGrdScales</summary>*/
        public virtual DbSet<ArPtGrdScales> ArPtGrdScales { get; set; }
        /*<summary>The db set for ArQualMinTyps</summary>*/
        public virtual DbSet<ArQualMinTyps> ArQualMinTyps { get; set; }
        /*<summary>The db set for ArQuantMinUnitTyps</summary>*/
        public virtual DbSet<ArQuantMinUnitTyps> ArQuantMinUnitTyps { get; set; }
        /*<summary>The db set for ArR2t4additionalInformation</summary>*/
        public virtual DbSet<ArR2t4additionalInformation> ArR2t4additionalInformation { get; set; }
        /*<summary>The db set for ArR2t4calculationResults</summary>*/
        public virtual DbSet<ArR2t4calculationResults> ArR2t4calculationResults { get; set; }
        /*<summary>The db set for ArR2t4input</summary>*/
        public virtual DbSet<ArR2t4input> ArR2t4input { get; set; }
        /*<summary>The db set for ArR2t4overrideInput</summary>*/
        public virtual DbSet<ArR2t4overrideInput> ArR2t4overrideInput { get; set; }
        /*<summary>The db set for ArR2t4overrideResults</summary>*/
        public virtual DbSet<ArR2t4overrideResults> ArR2t4overrideResults { get; set; }
        /*<summary>The db set for ArR2t4results</summary>*/
        public virtual DbSet<ArR2t4results> ArR2t4results { get; set; }
        /*<summary>The db set for ArR2t4terminationDetails</summary>*/
        public virtual DbSet<ArR2t4terminationDetails> ArR2t4terminationDetails { get; set; }
        /*<summary>The db set for ArReqGrpDef</summary>*/
        public virtual DbSet<ArReqGrpDef> ArReqGrpDef { get; set; }
        /*<summary>The db set for ArReqs</summary>*/
        public virtual DbSet<ArReqs> ArReqs { get; set; }
        /*<summary>The db set for ArReqTypes</summary>*/
        public virtual DbSet<ArReqTypes> ArReqTypes { get; set; }
        /*<summary>The db set for ArResults</summary>*/
        public virtual DbSet<ArResults> ArResults { get; set; }
        /*<summary>The db set for ArRooms</summary>*/
        public virtual DbSet<ArRooms> ArRooms { get; set; }
        /*<summary>The db set for ArSap</summary>*/
        public virtual DbSet<ArSap> ArSap { get; set; }
        /*<summary>The db set for ArSapchkResults</summary>*/
        public virtual DbSet<ArSapchkResults> ArSapchkResults { get; set; }
        /*<summary>The db set for ArSapdetails</summary>*/
        public virtual DbSet<ArSapdetails> ArSapdetails { get; set; }
        /*<summary>The db set for ArSapquantByInstruction</summary>*/
        public virtual DbSet<ArSapquantByInstruction> ArSapquantByInstruction { get; set; }
        /*<summary>The db set for ArSapquantResults</summary>*/
        public virtual DbSet<ArSapquantResults> ArSapquantResults { get; set; }
        /*<summary>The db set for ArSapShortHandSkillRequirement</summary>*/
        public virtual DbSet<ArSapShortHandSkillRequirement> ArSapShortHandSkillRequirement { get; set; }
        /*<summary>The db set for ArSdateSetup</summary>*/
        public virtual DbSet<ArSdateSetup> ArSdateSetup { get; set; }
        /*<summary>The db set for ArShifts</summary>*/
        public virtual DbSet<ArShifts> ArShifts { get; set; }
        /*<summary>The db set for ArStdSuspensions</summary>*/
        public virtual DbSet<ArStdSuspensions> ArStdSuspensions { get; set; }
        /*<summary>The db set for ArStudentClockAttendance</summary>*/
        public virtual DbSet<ArStudentClockAttendance> ArStudentClockAttendance { get; set; }
        /*<summary>The db set for ArStudentLoas</summary>*/
        public virtual DbSet<ArStudentLoas> ArStudentLoas { get; set; }
        /*<summary>The db set for ArStudentOld</summary>*/
        public virtual DbSet<ArStudentOld> ArStudentOld { get; set; }
        /*<summary>The db set for ArStudentSchedules</summary>*/
        public virtual DbSet<ArStudentSchedules> ArStudentSchedules { get; set; }
        /*<summary>The db set for ArStudentTimeClockPunches</summary>*/
        public virtual DbSet<ArStudentTimeClockPunches> ArStudentTimeClockPunches { get; set; }
        /*<summary>The db set for ArStuEnrollments</summary>*/
        public virtual DbSet<ArStuEnrollments> ArStuEnrollments { get; set; }
        /*<summary>The db set for ArStuPayPeriods</summary>*/
        public virtual DbSet<ArStuPayPeriods> ArStuPayPeriods { get; set; }
        /*<summary>The db set for ArStuProbWarnings</summary>*/
        public virtual DbSet<ArStuProbWarnings> ArStuProbWarnings { get; set; }
        /*<summary>The db set for ArStuReschReason</summary>*/
        public virtual DbSet<ArStuReschReason> ArStuReschReason { get; set; }
        /*<summary>The db set for ArTerm</summary>*/
        public virtual DbSet<ArTerm> ArTerm { get; set; }
        /*<summary>The db set for ArTermEnrollSummary</summary>*/
        public virtual DbSet<ArTermEnrollSummary> ArTermEnrollSummary { get; set; }
        /*<summary>The db set for ArTestingModels</summary>*/
        public virtual DbSet<ArTestingModels> ArTestingModels { get; set; }
        /*<summary>The db set for ArTimeClockSpecialCode</summary>*/
        public virtual DbSet<ArTimeClockSpecialCode> ArTimeClockSpecialCode { get; set; }
        /*<summary>The db set for ArTrackTransfer</summary>*/
        public virtual DbSet<ArTrackTransfer> ArTrackTransfer { get; set; }
        /*<summary>The db set for ArTransferGrades</summary>*/
        public virtual DbSet<ArTransferGrades> ArTransferGrades { get; set; }
        /*<summary>The db set for ArTriggerOffsetTyps</summary>*/
        public virtual DbSet<ArTriggerOffsetTyps> ArTriggerOffsetTyps { get; set; }
        /*<summary>The db set for ArTrigOffsetTyps</summary>*/
        public virtual DbSet<ArTrigOffsetTyps> ArTrigOffsetTyps { get; set; }
        /*<summary>The db set for ArTrigUnitTyps</summary>*/
        public virtual DbSet<ArTrigUnitTyps> ArTrigUnitTyps { get; set; }
        /*<summary>The db set for ArUnschedClosures</summary>*/
        public virtual DbSet<ArUnschedClosures> ArUnschedClosures { get; set; }
        /*<summary>The db set for AtAttendance</summary>*/
        public virtual DbSet<AtAttendance> AtAttendance { get; set; }
        /*<summary>The db set for AtClsSectAttendance</summary>*/
        public virtual DbSet<AtClsSectAttendance> AtClsSectAttendance { get; set; }
        /*<summary>The db set for AtConversionAttendance</summary>*/
        public virtual DbSet<AtConversionAttendance> AtConversionAttendance { get; set; }
        /*<summary>The db set for AtOnLineAttendance</summary>*/
        public virtual DbSet<AtOnLineAttendance> AtOnLineAttendance { get; set; }
        /*<summary>The db set for AtOnLineStudents</summary>*/
        public virtual DbSet<AtOnLineStudents> AtOnLineStudents { get; set; }
        /*<summary>The db set for CmDocuments</summary>*/
        public virtual DbSet<CmDocuments> CmDocuments { get; set; }
        /*<summary>The db set for CmTimeInterval</summary>*/
        public virtual DbSet<CmTimeInterval> CmTimeInterval { get; set; }
        /*<summary>The db set for DbaDbversion</summary>*/
        public virtual DbSet<DbaDbversion> DbaDbversion { get; set; }
        /*<summary>The db set for DeploymentMetadata</summary>*/
        public virtual DbSet<DeploymentMetadata> DeploymentMetadata { get; set; }
        /*<summary>The db set for DocumentFile</summary>*/
        public virtual DbSet<DocumentFile> DocumentFile { get; set; }
        /*<summary>The db set for FaAwardTimeIntervals</summary>*/
        public virtual DbSet<FaAwardTimeIntervals> FaAwardTimeIntervals { get; set; }
        /*<summary>The db set for FaLenders</summary>*/
        public virtual DbSet<FaLenders> FaLenders { get; set; }
        /*<summary>The db set for FaStudentAwards</summary>*/
        public virtual DbSet<FaStudentAwards> FaStudentAwards { get; set; }
        /*<summary>The db set for FaStudentAwardSchedule</summary>*/
        public virtual DbSet<FaStudentAwardSchedule> FaStudentAwardSchedule { get; set; }
        /*<summary>The db set for FaStudentPaymentPlans</summary>*/
        public virtual DbSet<FaStudentPaymentPlans> FaStudentPaymentPlans { get; set; }
        /*<summary>The db set for FaStuPaymentPlanSchedule</summary>*/
        public virtual DbSet<FaStuPaymentPlanSchedule> FaStuPaymentPlanSchedule { get; set; }
        /*<summary>The db set for HrEmpCerts</summary>*/
        public virtual DbSet<HrEmpCerts> HrEmpCerts { get; set; }
        /*<summary>The db set for HrEmpContactInfo</summary>*/
        public virtual DbSet<HrEmpContactInfo> HrEmpContactInfo { get; set; }
        /*<summary>The db set for HrEmpDegrees</summary>*/
        public virtual DbSet<HrEmpDegrees> HrEmpDegrees { get; set; }
        /*<summary>The db set for HrEmpHrinfo</summary>*/
        public virtual DbSet<HrEmpHrinfo> HrEmpHrinfo { get; set; }
        /*<summary>The db set for HrEmployeeEmergencyContacts</summary>*/
        public virtual DbSet<HrEmployeeEmergencyContacts> HrEmployeeEmergencyContacts { get; set; }
        /*<summary>The db set for HrEmployees</summary>*/
        public virtual DbSet<HrEmployees> HrEmployees { get; set; }
        /*<summary>The db set for LeadImage</summary>*/
        public virtual DbSet<LeadImage> LeadImage { get; set; }
        /*<summary>The db set for MsgEntitiesFieldGroups</summary>*/
        public virtual DbSet<MsgEntitiesFieldGroups> MsgEntitiesFieldGroups { get; set; }
        /*<summary>The db set for MsgGroups</summary>*/
        public virtual DbSet<MsgGroups> MsgGroups { get; set; }
        /*<summary>The db set for MsgMessages</summary>*/
        public virtual DbSet<MsgMessages> MsgMessages { get; set; }
        /*<summary>The db set for MsgRules</summary>*/
        public virtual DbSet<MsgRules> MsgRules { get; set; }
        /*<summary>The db set for MsgTemplates</summary>*/
        public virtual DbSet<MsgTemplates> MsgTemplates { get; set; }
        /*<summary>The db set for ParamDetail</summary>*/
        public virtual DbSet<ParamDetail> ParamDetail { get; set; }
        /*<summary>The db set for ParamDetailProp</summary>*/
        public virtual DbSet<ParamDetailProp> ParamDetailProp { get; set; }
        /*<summary>The db set for ParamItem</summary>*/
        public virtual DbSet<ParamItem> ParamItem { get; set; }
        /*<summary>The db set for ParamItemProp</summary>*/
        public virtual DbSet<ParamItemProp> ParamItemProp { get; set; }
        /*<summary>The db set for ParamSection</summary>*/
        public virtual DbSet<ParamSection> ParamSection { get; set; }
        /*<summary>The db set for ParamSet</summary>*/
        public virtual DbSet<ParamSet> ParamSet { get; set; }
        /*<summary>The db set for PlAddressTypes</summary>*/
        public virtual DbSet<PlAddressTypes> PlAddressTypes { get; set; }
        /*<summary>The db set for PlCorpHqs</summary>*/
        public virtual DbSet<PlCorpHqs> PlCorpHqs { get; set; }
        /*<summary>The db set for PlEmployerContact</summary>*/
        public virtual DbSet<PlEmployerContact> PlEmployerContact { get; set; }
        /*<summary>The db set for PlEmployerJobCats</summary>*/
        public virtual DbSet<PlEmployerJobCats> PlEmployerJobCats { get; set; }
        /*<summary>The db set for PlEmployerJobs</summary>*/
        public virtual DbSet<PlEmployerJobs> PlEmployerJobs { get; set; }
        /*<summary>The db set for PlEmployers</summary>*/
        public virtual DbSet<PlEmployers> PlEmployers { get; set; }
        /*<summary>The db set for PlExitInterview</summary>*/
        public virtual DbSet<PlExitInterview> PlExitInterview { get; set; }
        /*<summary>The db set for PlFee</summary>*/
        public virtual DbSet<PlFee> PlFee { get; set; }
        /*<summary>The db set for PlFldStudy</summary>*/
        public virtual DbSet<PlFldStudy> PlFldStudy { get; set; }
        /*<summary>The db set for PlHowPlaced</summary>*/
        public virtual DbSet<PlHowPlaced> PlHowPlaced { get; set; }
        /*<summary>The db set for PlIndustries</summary>*/
        public virtual DbSet<PlIndustries> PlIndustries { get; set; }
        /*<summary>The db set for PlInterview</summary>*/
        public virtual DbSet<PlInterview> PlInterview { get; set; }
        /*<summary>The db set for PlJobBenefit</summary>*/
        public virtual DbSet<PlJobBenefit> PlJobBenefit { get; set; }
        /*<summary>The db set for PlJobCats</summary>*/
        public virtual DbSet<PlJobCats> PlJobCats { get; set; }
        /*<summary>The db set for PlJobSchedule</summary>*/
        public virtual DbSet<PlJobSchedule> PlJobSchedule { get; set; }
        /*<summary>The db set for PlJobStatus</summary>*/
        public virtual DbSet<PlJobStatus> PlJobStatus { get; set; }
        /*<summary>The db set for PlJobType</summary>*/
        public virtual DbSet<PlJobType> PlJobType { get; set; }
        /*<summary>The db set for PlJobWorkDays</summary>*/
        public virtual DbSet<PlJobWorkDays> PlJobWorkDays { get; set; }
        /*<summary>The db set for PlLocations</summary>*/
        public virtual DbSet<PlLocations> PlLocations { get; set; }
        /*<summary>The db set for PlSalaryType</summary>*/
        public virtual DbSet<PlSalaryType> PlSalaryType { get; set; }
        /*<summary>The db set for PlSkillGroups</summary>*/
        public virtual DbSet<PlSkillGroups> PlSkillGroups { get; set; }
        /*<summary>The db set for PlSkills</summary>*/
        public virtual DbSet<PlSkills> PlSkills { get; set; }
        /*<summary>The db set for PlStudentDocs</summary>*/
        public virtual DbSet<PlStudentDocs> PlStudentDocs { get; set; }
        /*<summary>The db set for PlStudentsPlaced</summary>*/
        public virtual DbSet<PlStudentsPlaced> PlStudentsPlaced { get; set; }
        /*<summary>The db set for PlTransportation</summary>*/
        public virtual DbSet<PlTransportation> PlTransportation { get; set; }
        /*<summary>The db set for PlWorkDays</summary>*/
        public virtual DbSet<PlWorkDays> PlWorkDays { get; set; }
        /*<summary>The db set for PriorWorkAddress</summary>*/
        public virtual DbSet<PriorWorkAddress> PriorWorkAddress { get; set; }
        /*<summary>The db set for PriorWorkContact</summary>*/
        public virtual DbSet<PriorWorkContact> PriorWorkContact { get; set; }
        /*<summary>The db set for PrivateSchoolsList</summary>*/
        public virtual DbSet<PrivateSchoolsList> PrivateSchoolsList { get; set; }
        /*<summary>The db set for PublicSchoolsList</summary>*/
        public virtual DbSet<PublicSchoolsList> PublicSchoolsList { get; set; }
        /*<summary>The db set for References</summary>*/
        public virtual DbSet<References> References { get; set; }
        /*<summary>The db set for RptAdmissionsRep</summary>*/
        public virtual DbSet<RptAdmissionsRep> RptAdmissionsRep { get; set; }
        /*<summary>The db set for RptInstructor</summary>*/
        public virtual DbSet<RptInstructor> RptInstructor { get; set; }
        /*<summary>The db set for RptLeadStatus</summary>*/
        public virtual DbSet<RptLeadStatus> RptLeadStatus { get; set; }
        /*<summary>The db set for RptStuEnrollmentStatus</summary>*/
        public virtual DbSet<RptStuEnrollmentStatus> RptStuEnrollmentStatus { get; set; }
        /*<summary>The db set for SaAcademicYears</summary>*/
        public virtual DbSet<SaAcademicYears> SaAcademicYears { get; set; }
        /*<summary>The db set for SaAdmissionDeposits</summary>*/
        public virtual DbSet<SaAdmissionDeposits> SaAdmissionDeposits { get; set; }
        /*<summary>The db set for SaAppliedPayments</summary>*/
        public virtual DbSet<SaAppliedPayments> SaAppliedPayments { get; set; }
        /*<summary>The db set for SaAwardTypes</summary>*/
        public virtual DbSet<SaAwardTypes> SaAwardTypes { get; set; }
        /*<summary>The db set for SaBankAccounts</summary>*/
        public virtual DbSet<SaBankAccounts> SaBankAccounts { get; set; }
        /*<summary>The db set for SaBankCodes</summary>*/
        public virtual DbSet<SaBankCodes> SaBankCodes { get; set; }
        /*<summary>The db set for SaBatchPayments</summary>*/
        public virtual DbSet<SaBatchPayments> SaBatchPayments { get; set; }
        /*<summary>The db set for SaBillingMethods</summary>*/
        public virtual DbSet<SaBillingMethods> SaBillingMethods { get; set; }
        /*<summary>The db set for SaBillTypes</summary>*/
        public virtual DbSet<SaBillTypes> SaBillTypes { get; set; }
        /*<summary>The db set for SaCourseFees</summary>*/
        public virtual DbSet<SaCourseFees> SaCourseFees { get; set; }
        /*<summary>The db set for SaDeferredRevenues</summary>*/
        public virtual DbSet<SaDeferredRevenues> SaDeferredRevenues { get; set; }
        /*<summary>The db set for SaFeeLevels</summary>*/
        public virtual DbSet<SaFeeLevels> SaFeeLevels { get; set; }
        /*<summary>The db set for SaFundSources</summary>*/
        public virtual DbSet<SaFundSources> SaFundSources { get; set; }
        /*<summary>The db set for SaGlaccounts</summary>*/
        public virtual DbSet<SaGlaccounts> SaGlaccounts { get; set; }
        /*<summary>The db set for SaGldistributions</summary>*/
        public virtual DbSet<SaGldistributions> SaGldistributions { get; set; }
        /*<summary>The db set for SaIncrements</summary>*/
        public virtual DbSet<SaIncrements> SaIncrements { get; set; }
        /*<summary>The db set for SaLateFees</summary>*/
        public virtual DbSet<SaLateFees> SaLateFees { get; set; }
        /*<summary>The db set for SaPaymentDescriptions</summary>*/
        public virtual DbSet<SaPaymentDescriptions> SaPaymentDescriptions { get; set; }
        /*<summary>The db set for SaPayments</summary>*/
        public virtual DbSet<SaPayments> SaPayments { get; set; }
        /*<summary>The db set for SaPaymentTypes</summary>*/
        public virtual DbSet<SaPaymentTypes> SaPaymentTypes { get; set; }
        /*<summary>The db set for SaPeriodicFees</summary>*/
        public virtual DbSet<SaPeriodicFees> SaPeriodicFees { get; set; }
        /*<summary>The db set for SaPmtDisbRel</summary>*/
        public virtual DbSet<SaPmtDisbRel> SaPmtDisbRel { get; set; }
        /*<summary>The db set for SaPmtPeriodBatchHeaders</summary>*/
        public virtual DbSet<SaPmtPeriodBatchHeaders> SaPmtPeriodBatchHeaders { get; set; }
        /*<summary>The db set for SaPmtPeriodBatchItems</summary>*/
        public virtual DbSet<SaPmtPeriodBatchItems> SaPmtPeriodBatchItems { get; set; }
        /*<summary>The db set for SaPmtPeriods</summary>*/
        public virtual DbSet<SaPmtPeriods> SaPmtPeriods { get; set; }
        /*<summary>The db set for SaPrgVerDefaultChargePeriods</summary>*/
        public virtual DbSet<SaPrgVerDefaultChargePeriods> SaPrgVerDefaultChargePeriods { get; set; }
        /*<summary>The db set for SaProgramVersionFees</summary>*/
        public virtual DbSet<SaProgramVersionFees> SaProgramVersionFees { get; set; }
        /*<summary>The db set for SaRateScheduleDetails</summary>*/
        public virtual DbSet<SaRateScheduleDetails> SaRateScheduleDetails { get; set; }
        /*<summary>The db set for SaRateSchedules</summary>*/
        public virtual DbSet<SaRateSchedules> SaRateSchedules { get; set; }
        /*<summary>The db set for SaRefunds</summary>*/
        public virtual DbSet<SaRefunds> SaRefunds { get; set; }
        /*<summary>The db set for SaReversedTransactions</summary>*/
        public virtual DbSet<SaReversedTransactions> SaReversedTransactions { get; set; }
        /*<summary>The db set for SaSysTransCodes</summary>*/
        public virtual DbSet<SaSysTransCodes> SaSysTransCodes { get; set; }
        /*<summary>The db set for SaTransactions</summary>*/
        public virtual DbSet<SaTransactions> SaTransactions { get; set; }
        /*<summary>The db set for SaTransCodeGlaccounts</summary>*/
        public virtual DbSet<SaTransCodeGlaccounts> SaTransCodeGlaccounts { get; set; }
        /*<summary>The db set for SaTransCodes</summary>*/
        public virtual DbSet<SaTransCodes> SaTransCodes { get; set; }
        /*<summary>The db set for SaTransTypes</summary>*/
        public virtual DbSet<SaTransTypes> SaTransTypes { get; set; }
        /*<summary>The db set for SaTuitionCategories</summary>*/
        public virtual DbSet<SaTuitionCategories> SaTuitionCategories { get; set; }
        /*<summary>The db set for SaTuitionEarnings</summary>*/
        public virtual DbSet<SaTuitionEarnings> SaTuitionEarnings { get; set; }
        /*<summary>The db set for SaTuitionEarningsPercentageRanges</summary>*/
        public virtual DbSet<SaTuitionEarningsPercentageRanges> SaTuitionEarningsPercentageRanges { get; set; }
        /*<summary>The db set for SyAcademicCalendars</summary>*/
        public virtual DbSet<SyAcademicCalendars> SyAcademicCalendars { get; set; }
        /*<summary>The db set for SyAddressStatuses</summary>*/
        public virtual DbSet<SyAddressStatuses> SyAddressStatuses { get; set; }
        /*<summary>The db set for SyAdvantageResourceRelations</summary>*/
        public virtual DbSet<SyAdvantageResourceRelations> SyAdvantageResourceRelations { get; set; }
        /*<summary>The db set for SyAdvBuild</summary>*/
        public virtual DbSet<SyAdvBuild> SyAdvBuild { get; set; }
        /*<summary>The db set for SyAdvFundSources</summary>*/
        public virtual DbSet<SyAdvFundSources> SyAdvFundSources { get; set; }
        /*<summary>The db set for SyAnnouncements</summary>*/
        public virtual DbSet<SyAnnouncements> SyAnnouncements { get; set; }
        /*<summary>The db set for SyAuditHist</summary>*/
        public virtual DbSet<SyAuditHist> SyAuditHist { get; set; }
        /*<summary>The db set for SyAuditHistDetail</summary>*/
        public virtual DbSet<SyAuditHistDetail> SyAuditHistDetail { get; set; }
        /*<summary>The db set for SyAwardTypes9010</summary>*/
        public virtual DbSet<SyAwardTypes9010> SyAwardTypes9010 { get; set; }
        /*<summary>The db set for SyAwardTypes9010Mapping</summary>*/
        public virtual DbSet<SyAwardTypes9010Mapping> SyAwardTypes9010Mapping { get; set; }
        /*<summary>The db set for SyBridgeResourceIdRptFldId</summary>*/
        public virtual DbSet<SyBridgeResourceIdRptFldId> SyBridgeResourceIdRptFldId { get; set; }
        /*<summary>The db set for SyBusObjects</summary>*/
        public virtual DbSet<SyBusObjects> SyBusObjects { get; set; }
        /*<summary>The db set for SyCampGrps</summary>*/
        public virtual DbSet<SyCampGrps> SyCampGrps { get; set; }
        /*<summary>The db set for SyCampuses</summary>*/
        public virtual DbSet<SyCampuses> SyCampuses { get; set; }
        /*<summary>The db set for SyCertifications</summary>*/
        public virtual DbSet<SyCertifications> SyCertifications { get; set; }
        /*<summary>The db set for SyClsSectStatuses</summary>*/
        public virtual DbSet<SyClsSectStatuses> SyClsSectStatuses { get; set; }
        /*<summary>The db set for SyCmpGrpCmps</summary>*/
        public virtual DbSet<SyCmpGrpCmps> SyCmpGrpCmps { get; set; }
        /*<summary>The db set for SyCompOps</summary>*/
        public virtual DbSet<SyCompOps> SyCompOps { get; set; }
        /*<summary>The db set for SyConfigAppSetLookup</summary>*/
        public virtual DbSet<SyConfigAppSetLookup> SyConfigAppSetLookup { get; set; }
        /*<summary>The db set for SyConfigAppSettings</summary>*/
        public virtual DbSet<SyConfigAppSettings> SyConfigAppSettings { get; set; }
        /*<summary>The db set for SyConfigAppSetValues</summary>*/
        public virtual DbSet<SyConfigAppSetValues> SyConfigAppSetValues { get; set; }
        /*<summary>The db set for SyContactTypes</summary>*/
        public virtual DbSet<SyContactTypes> SyContactTypes { get; set; }
        /*<summary>The db set for SyDdls</summary>*/
        public virtual DbSet<SyDdls> SyDdls { get; set; }
        /*<summary>The db set for SyDepartments</summary>*/
        public virtual DbSet<SyDepartments> SyDepartments { get; set; }
        /*<summary>The db set for SyDocStatuses</summary>*/
        public virtual DbSet<SyDocStatuses> SyDocStatuses { get; set; }
        /*<summary>The db set for SyDocumentHistory</summary>*/
        public virtual DbSet<SyDocumentHistory> SyDocumentHistory { get; set; }
        /*<summary>The db set for SyEdexpNotPostPellAcgSmartTeachDisbursementTable</summary>*/
        public virtual DbSet<SyEdexpNotPostPellAcgSmartTeachDisbursementTable> SyEdexpNotPostPellAcgSmartTeachDisbursementTable { get; set; }
        /*<summary>The db set for SyEdexpNotPostPellAcgSmartTeachStudentTable</summary>*/
        public virtual DbSet<SyEdexpNotPostPellAcgSmartTeachStudentTable> SyEdexpNotPostPellAcgSmartTeachStudentTable { get; set; }
        /*<summary>The db set for SyEdexpressExceptionReportDirectLoanDisbursementTable</summary>*/
        public virtual DbSet<SyEdexpressExceptionReportDirectLoanDisbursementTable> SyEdexpressExceptionReportDirectLoanDisbursementTable { get; set; }
        /*<summary>The db set for SyEdexpressExceptionReportDirectLoanStudentTable</summary>*/
        public virtual DbSet<SyEdexpressExceptionReportDirectLoanStudentTable> SyEdexpressExceptionReportDirectLoanStudentTable { get; set; }
        /*<summary>The db set for SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable</summary>*/
        public virtual DbSet<SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable> SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable { get; set; }
        /*<summary>The db set for SyEdexpressExceptionReportPellAcgSmartTeachStudentTable</summary>*/
        public virtual DbSet<SyEdexpressExceptionReportPellAcgSmartTeachStudentTable> SyEdexpressExceptionReportPellAcgSmartTeachStudentTable { get; set; }
        /*<summary>The db set for SyEmailType</summary>*/
        public virtual DbSet<SyEmailType> SyEmailType { get; set; }
        /*<summary>The db set for SyEntities</summary>*/
        public virtual DbSet<SyEntities> SyEntities { get; set; }
        /*<summary>The db set for SyEvents</summary>*/
        public virtual DbSet<SyEvents> SyEvents { get; set; }
        /*<summary>The db set for SyFameEsparchivedFiles</summary>*/
        public virtual DbSet<SyFameEsparchivedFiles> SyFameEsparchivedFiles { get; set; }
        /*<summary>The db set for SyFameEspawardCutOffDate</summary>*/
        public virtual DbSet<SyFameEspawardCutOffDate> SyFameEspawardCutOffDate { get; set; }
        /*<summary>The db set for SyFameEspexceptionReport</summary>*/
        public virtual DbSet<SyFameEspexceptionReport> SyFameEspexceptionReport { get; set; }
        /*<summary>The db set for SyFamilyIncome</summary>*/
        public virtual DbSet<SyFamilyIncome> SyFamilyIncome { get; set; }
        /*<summary>The db set for SyFieldCalculation</summary>*/
        public virtual DbSet<SyFieldCalculation> SyFieldCalculation { get; set; }
        /*<summary>The db set for SyFields</summary>*/
        public virtual DbSet<SyFields> SyFields { get; set; }
        /*<summary>The db set for SyFieldTypes</summary>*/
        public virtual DbSet<SyFieldTypes> SyFieldTypes { get; set; }
        /*<summary>The db set for SyFldCaptions</summary>*/
        public virtual DbSet<SyFldCaptions> SyFldCaptions { get; set; }
        /*<summary>The db set for SyFldCategories</summary>*/
        public virtual DbSet<SyFldCategories> SyFldCategories { get; set; }
        /*<summary>The db set for SyGenerateStudentFormatId</summary>*/
        public virtual DbSet<SyGenerateStudentFormatId> SyGenerateStudentFormatId { get; set; }
        /*<summary>The db set for SyGenerateStudentId</summary>*/
        public virtual DbSet<SyGenerateStudentId> SyGenerateStudentId { get; set; }
        /*<summary>The db set for SyGenerateStudentSeq</summary>*/
        public virtual DbSet<SyGenerateStudentSeq> SyGenerateStudentSeq { get; set; }
        /*<summary>The db set for SyGradeRounding</summary>*/
        public virtual DbSet<SyGradeRounding> SyGradeRounding { get; set; }
        /*<summary>The db set for SyGrdPolicies</summary>*/
        public virtual DbSet<SyGrdPolicies> SyGrdPolicies { get; set; }
        /*<summary>The db set for SyGrdPolicyParams</summary>*/
        public virtual DbSet<SyGrdPolicyParams> SyGrdPolicyParams { get; set; }
        /*<summary>The db set for SyHolidays</summary>*/
        public virtual DbSet<SyHolidays> SyHolidays { get; set; }
        /*<summary>The db set for SyHomePageNotes</summary>*/
        public virtual DbSet<SyHomePageNotes> SyHomePageNotes { get; set; }
        /*<summary>The db set for SyHrdepartments</summary>*/
        public virtual DbSet<SyHrdepartments> SyHrdepartments { get; set; }
        /*<summary>The db set for SyInputMasks</summary>*/
        public virtual DbSet<SyInputMasks> SyInputMasks { get; set; }
        /*<summary>The db set for SyInstFldsDdreq</summary>*/
        public virtual DbSet<SyInstFldsDdreq> SyInstFldsDdreq { get; set; }
        /*<summary>The db set for SyInstFldsLog</summary>*/
        public virtual DbSet<SyInstFldsLog> SyInstFldsLog { get; set; }
        /*<summary>The db set for SyInstFldsMask</summary>*/
        public virtual DbSet<SyInstFldsMask> SyInstFldsMask { get; set; }
        /*<summary>The db set for SyInstitutionAddresses</summary>*/
        public virtual DbSet<SyInstitutionAddresses> SyInstitutionAddresses { get; set; }
        /*<summary>The db set for SyInstitutionContacts</summary>*/
        public virtual DbSet<SyInstitutionContacts> SyInstitutionContacts { get; set; }
        /*<summary>The db set for SyInstitutionImportTypes</summary>*/
        public virtual DbSet<SyInstitutionImportTypes> SyInstitutionImportTypes { get; set; }
        /*<summary>The db set for SyInstitutionPhone</summary>*/
        public virtual DbSet<SyInstitutionPhone> SyInstitutionPhone { get; set; }
        /*<summary>The db set for SyInstitutions</summary>*/
        public virtual DbSet<SyInstitutions> SyInstitutions { get; set; }
        /*<summary>The db set for SyInstitutionTypes</summary>*/
        public virtual DbSet<SyInstitutionTypes> SyInstitutionTypes { get; set; }
        /*<summary>The db set for SyInstResFldsReq</summary>*/
        public virtual DbSet<SyInstResFldsReq> SyInstResFldsReq { get; set; }
        /*<summary>The db set for SyIpedsResourcesReportTitle</summary>*/
        public virtual DbSet<SyIpedsResourcesReportTitle> SyIpedsResourcesReportTitle { get; set; }
        /*<summary>The db set for SyKlassAppConfigurationSetting</summary>*/
        public virtual DbSet<SyKlassAppConfigurationSetting> SyKlassAppConfigurationSetting { get; set; }
        /*<summary>The db set for SyKlassEntity</summary>*/
        public virtual DbSet<SyKlassEntity> SyKlassEntity { get; set; }
        /*<summary>The db set for SyKlassOperationType</summary>*/
        public virtual DbSet<SyKlassOperationType> SyKlassOperationType { get; set; }
        /*<summary>The db set for SyLangs</summary>*/
        public virtual DbSet<SyLangs> SyLangs { get; set; }
        /*<summary>The db set for SyLeadStatusChangePermissions</summary>*/
        public virtual DbSet<SyLeadStatusChangePermissions> SyLeadStatusChangePermissions { get; set; }
        /*<summary>The db set for SyLeadStatusChanges</summary>*/
        public virtual DbSet<SyLeadStatusChanges> SyLeadStatusChanges { get; set; }
        /*<summary>The db set for SyLeadStatusesChanges</summary>*/
        public virtual DbSet<SyLeadStatusesChanges> SyLeadStatusesChanges { get; set; }
        /*<summary>The db set for SyLoggerDualEnrollment</summary>*/
        public virtual DbSet<SyLoggerDualEnrollment> SyLoggerDualEnrollment { get; set; }
        /*<summary>The db set for SyMenuItems</summary>*/
        public virtual DbSet<SyMenuItems> SyMenuItems { get; set; }
        /*<summary>The db set for SyMenuItemType</summary>*/
        public virtual DbSet<SyMenuItemType> SyMenuItemType { get; set; }
        /*<summary>The db set for SyMessageGroups</summary>*/
        public virtual DbSet<SyMessageGroups> SyMessageGroups { get; set; }
        /*<summary>The db set for SyMessageGroupSchemas</summary>*/
        public virtual DbSet<SyMessageGroupSchemas> SyMessageGroupSchemas { get; set; }
        /*<summary>The db set for SyMessages</summary>*/
        public virtual DbSet<SyMessages> SyMessages { get; set; }
        /*<summary>The db set for SyMessageSchemas</summary>*/
        public virtual DbSet<SyMessageSchemas> SyMessageSchemas { get; set; }
        /*<summary>The db set for SyMessageTemplates</summary>*/
        public virtual DbSet<SyMessageTemplates> SyMessageTemplates { get; set; }
        /*<summary>The db set for SyModCaptions</summary>*/
        public virtual DbSet<SyModCaptions> SyModCaptions { get; set; }
        /*<summary>The db set for SyModuleDef</summary>*/
        public virtual DbSet<SyModuleDef> SyModuleDef { get; set; }
        /*<summary>The db set for SyModuleEntities</summary>*/
        public virtual DbSet<SyModuleEntities> SyModuleEntities { get; set; }
        /*<summary>The db set for SyModules</summary>*/
        public virtual DbSet<SyModules> SyModules { get; set; }
        /*<summary>The db set for SyMrus</summary>*/
        public virtual DbSet<SyMrus> SyMrus { get; set; }
        /*<summary>The db set for SyMrutypes</summary>*/
        public virtual DbSet<SyMrutypes> SyMrutypes { get; set; }
        /*<summary>The db set for SyNavigationNodes</summary>*/
        public virtual DbSet<SyNavigationNodes> SyNavigationNodes { get; set; }
        /*<summary>The db set for SyNotesPageFields</summary>*/
        public virtual DbSet<SyNotesPageFields> SyNotesPageFields { get; set; }
        /*<summary>The db set for SyPeriods</summary>*/
        public virtual DbSet<SyPeriods> SyPeriods { get; set; }
        /*<summary>The db set for SyPeriodsWorkDays</summary>*/
        public virtual DbSet<SyPeriodsWorkDays> SyPeriodsWorkDays { get; set; }
        /*<summary>The db set for SyPhoneStatuses</summary>*/
        public virtual DbSet<SyPhoneStatuses> SyPhoneStatuses { get; set; }
        /*<summary>The db set for SyPhoneType</summary>*/
        public virtual DbSet<SyPhoneType> SyPhoneType { get; set; }
        /*<summary>The db set for SyPositions</summary>*/
        public virtual DbSet<SyPositions> SyPositions { get; set; }
        /*<summary>The db set for SyPrefixes</summary>*/
        public virtual DbSet<SyPrefixes> SyPrefixes { get; set; }
        /*<summary>The db set for SyProgramUnitTypes</summary>*/
        public virtual DbSet<SyProgramUnitTypes> SyProgramUnitTypes { get; set; }
        /*<summary>The db set for SyQuickLinks</summary>*/
        public virtual DbSet<SyQuickLinks> SyQuickLinks { get; set; }
        /*<summary>The db set for SyR2t4calculationPeriodTypes</summary>*/
        public virtual DbSet<SyR2t4calculationPeriodTypes> SyR2t4calculationPeriodTypes { get; set; }
        /*<summary>The db set for SyRdfsiteSums</summary>*/
        public virtual DbSet<SyRdfsiteSums> SyRdfsiteSums { get; set; }
        /*<summary>The db set for SyReasonNotEnrolled</summary>*/
        public virtual DbSet<SyReasonNotEnrolled> SyReasonNotEnrolled { get; set; }
        /*<summary>The db set for SyRelations</summary>*/
        public virtual DbSet<SyRelations> SyRelations { get; set; }
        /*<summary>The db set for SyRelationshipStatus</summary>*/
        public virtual DbSet<SyRelationshipStatus> SyRelationshipStatus { get; set; }
        /*<summary>The db set for SyReportDescrip</summary>*/
        public virtual DbSet<SyReportDescrip> SyReportDescrip { get; set; }
        /*<summary>The db set for SyReports</summary>*/
        public virtual DbSet<SyReports> SyReports { get; set; }
        /*<summary>The db set for SyReportTabs</summary>*/
        public virtual DbSet<SyReportTabs> SyReportTabs { get; set; }
        /*<summary>The db set for SyReportUserPrefs</summary>*/
        public virtual DbSet<SyReportUserPrefs> SyReportUserPrefs { get; set; }
        /*<summary>The db set for SyReportUserSettings</summary>*/
        public virtual DbSet<SyReportUserSettings> SyReportUserSettings { get; set; }
        /*<summary>The db set for SyReqTypes</summary>*/
        public virtual DbSet<SyReqTypes> SyReqTypes { get; set; }
        /*<summary>The db set for SyResCaptions</summary>*/
        public virtual DbSet<SyResCaptions> SyResCaptions { get; set; }
        /*<summary>The db set for SyResources</summary>*/
        public virtual DbSet<SyResources> SyResources { get; set; }
        /*<summary>The db set for SyResourceSdf</summary>*/
        public virtual DbSet<SyResourceSdf> SyResourceSdf { get; set; }
        /*<summary>The db set for SyResourceTypes</summary>*/
        public virtual DbSet<SyResourceTypes> SyResourceTypes { get; set; }
        /*<summary>The db set for SyResTblFlds</summary>*/
        public virtual DbSet<SyResTblFlds> SyResTblFlds { get; set; }
        /*<summary>The db set for SyRlsResLvls</summary>*/
        public virtual DbSet<SyRlsResLvls> SyRlsResLvls { get; set; }
        /*<summary>The db set for SyRoles</summary>*/
        public virtual DbSet<SyRoles> SyRoles { get; set; }
        /*<summary>The db set for SyRolesModules</summary>*/
        public virtual DbSet<SyRolesModules> SyRolesModules { get; set; }
        /*<summary>The db set for SyRptAdHocFields</summary>*/
        public virtual DbSet<SyRptAdHocFields> SyRptAdHocFields { get; set; }
        /*<summary>The db set for SyRptAdhocRelations</summary>*/
        public virtual DbSet<SyRptAdhocRelations> SyRptAdhocRelations { get; set; }
        /*<summary>The db set for SyRptAgencies</summary>*/
        public virtual DbSet<SyRptAgencies> SyRptAgencies { get; set; }
        /*<summary>The db set for SyRptAgencyFields</summary>*/
        public virtual DbSet<SyRptAgencyFields> SyRptAgencyFields { get; set; }
        /*<summary>The db set for SyRptAgencyFldValues</summary>*/
        public virtual DbSet<SyRptAgencyFldValues> SyRptAgencyFldValues { get; set; }
        /*<summary>The db set for SyRptAgencySchoolMapping</summary>*/
        public virtual DbSet<SyRptAgencySchoolMapping> SyRptAgencySchoolMapping { get; set; }
        /*<summary>The db set for SyRptFields</summary>*/
        public virtual DbSet<SyRptFields> SyRptFields { get; set; }
        /*<summary>The db set for SyRptFilterListPrefs</summary>*/
        public virtual DbSet<SyRptFilterListPrefs> SyRptFilterListPrefs { get; set; }
        /*<summary>The db set for SyRptFilterOtherPrefs</summary>*/
        public virtual DbSet<SyRptFilterOtherPrefs> SyRptFilterOtherPrefs { get; set; }
        /*<summary>The db set for SyRptParams</summary>*/
        public virtual DbSet<SyRptParams> SyRptParams { get; set; }
        /*<summary>The db set for SyRptPrefsIpedsSfapr</summary>*/
        public virtual DbSet<SyRptPrefsIpedsSfapr> SyRptPrefsIpedsSfapr { get; set; }
        /*<summary>The db set for SyRptProps</summary>*/
        public virtual DbSet<SyRptProps> SyRptProps { get; set; }
        /*<summary>The db set for SyRptSortPrefs</summary>*/
        public virtual DbSet<SyRptSortPrefs> SyRptSortPrefs { get; set; }
        /*<summary>The db set for SyRptSql</summary>*/
        public virtual DbSet<SyRptSql> SyRptSql { get; set; }
        /*<summary>The db set for SyRptSqlcount</summary>*/
        public virtual DbSet<SyRptSqlcount> SyRptSqlcount { get; set; }
        /*<summary>The db set for SyRptUserPrefs</summary>*/
        public virtual DbSet<SyRptUserPrefs> SyRptUserPrefs { get; set; }
        /*<summary>The db set for SySchedulingMethods</summary>*/
        public virtual DbSet<SySchedulingMethods> SySchedulingMethods { get; set; }
        /*<summary>The db set for SySchoolLogo</summary>*/
        public virtual DbSet<SySchoolLogo> SySchoolLogo { get; set; }
        /*<summary>The db set for SySchoolStateBoardReports</summary>*/
        public virtual DbSet<SySchoolStateBoardReports> SySchoolStateBoardReports { get; set; }
        /*<summary>The db set for SySdf</summary>*/
        public virtual DbSet<SySdf> SySdf { get; set; }
        /*<summary>The db set for SySdfDtypes</summary>*/
        public virtual DbSet<SySdfDtypes> SySdfDtypes { get; set; }
        /*<summary>The db set for SySdfmoduleValue</summary>*/
        public virtual DbSet<SySdfmoduleValue> SySdfmoduleValue { get; set; }
        /*<summary>The db set for SySdfModVis</summary>*/
        public virtual DbSet<SySdfModVis> SySdfModVis { get; set; }
        /*<summary>The db set for SySdfRange</summary>*/
        public virtual DbSet<SySdfRange> SySdfRange { get; set; }
        /*<summary>The db set for SySdfValList</summary>*/
        public virtual DbSet<SySdfValList> SySdfValList { get; set; }
        /*<summary>The db set for SySdfValTypes</summary>*/
        public virtual DbSet<SySdfValTypes> SySdfValTypes { get; set; }
        /*<summary>The db set for SySdfvisiblity</summary>*/
        public virtual DbSet<SySdfvisiblity> SySdfvisiblity { get; set; }
        /*<summary>The db set for SyStateBoardAgencies</summary>*/
        public virtual DbSet<SyStateBoardAgencies> SyStateBoardAgencies { get; set; }
        /*<summary>The db set for SyStateBoardCourses</summary>*/
        public virtual DbSet<SyStateBoardCourses> SyStateBoardCourses { get; set; }
        /*<summary>The db set for SyStateBoardProgramCourseMappings</summary>*/
        public virtual DbSet<SyStateBoardProgramCourseMappings> SyStateBoardProgramCourseMappings { get; set; }
        /*<summary>The db set for SyStateReports</summary>*/
        public virtual DbSet<SyStateReports> SyStateReports { get; set; }
        /*<summary>The db set for SyStates</summary>*/
        public virtual DbSet<SyStates> SyStates { get; set; }
        /*<summary>The db set for SyStatusChangeDeleteReasons</summary>*/
        public virtual DbSet<SyStatusChangeDeleteReasons> SyStatusChangeDeleteReasons { get; set; }
        /*<summary>The db set for SyStatusChangesDeleted</summary>*/
        public virtual DbSet<SyStatusChangesDeleted> SyStatusChangesDeleted { get; set; }
        /*<summary>The db set for SyStatusCodes</summary>*/
        public virtual DbSet<SyStatusCodes> SyStatusCodes { get; set; }
        /*<summary>The db set for SyStatuses</summary>*/
        public virtual DbSet<SyStatuses> SyStatuses { get; set; }
        /*<summary>The db set for SyStatusLevels</summary>*/
        public virtual DbSet<SyStatusLevels> SyStatusLevels { get; set; }
        /*<summary>The db set for SyStudentAttendanceSummary</summary>*/
        public virtual DbSet<SyStudentAttendanceSummary> SyStudentAttendanceSummary { get; set; }
        /*<summary>The db set for SyStudentFormat</summary>*/
        public virtual DbSet<SyStudentFormat> SyStudentFormat { get; set; }
        /*<summary>The db set for SyStudentStatusChanges</summary>*/
        public virtual DbSet<SyStudentStatusChanges> SyStudentStatusChanges { get; set; }
        /*<summary>The db set for SyStuRestrictions</summary>*/
        public virtual DbSet<SyStuRestrictions> SyStuRestrictions { get; set; }
        /*<summary>The db set for SyStuRestrictionTypes</summary>*/
        public virtual DbSet<SyStuRestrictionTypes> SyStuRestrictionTypes { get; set; }
        /*<summary>The db set for SySubMods</summary>*/
        public virtual DbSet<SySubMods> SySubMods { get; set; }
        /*<summary>The db set for SySuffixes</summary>*/
        public virtual DbSet<SySuffixes> SySuffixes { get; set; }
        /*<summary>The db set for SySummLists</summary>*/
        public virtual DbSet<SySummLists> SySummLists { get; set; }
        /*<summary>The db set for SySysDocStatuses</summary>*/
        public virtual DbSet<SySysDocStatuses> SySysDocStatuses { get; set; }
        /*<summary>The db set for SySysRoles</summary>*/
        public virtual DbSet<SySysRoles> SySysRoles { get; set; }
        /*<summary>The db set for SySysStatus</summary>*/
        public virtual DbSet<SySysStatus> SySysStatus { get; set; }
        /*<summary>The db set for SySystemStatusWorkflowRules</summary>*/
        public virtual DbSet<SySystemStatusWorkflowRules> SySystemStatusWorkflowRules { get; set; }
        /*<summary>The db set for SyTables</summary>*/
        public virtual DbSet<SyTables> SyTables { get; set; }
        /*<summary>The db set for SyTblCaptions</summary>*/
        public virtual DbSet<SyTblCaptions> SyTblCaptions { get; set; }
        /*<summary>The db set for SyTblDependencies</summary>*/
        public virtual DbSet<SyTblDependencies> SyTblDependencies { get; set; }
        /*<summary>The db set for SyTblFlds</summary>*/
        public virtual DbSet<SyTblFlds> SyTblFlds { get; set; }
        /*<summary>The db set for SyTermTypes</summary>*/
        public virtual DbSet<SyTermTypes> SyTermTypes { get; set; }
        /*<summary>The db set for SyTitleIvsapCustomVerbiage</summary>*/
        public virtual DbSet<SyTitleIvsapCustomVerbiage> SyTitleIvsapCustomVerbiage { get; set; }
        /*<summary>The db set for SyTitleIvsapStatus</summary>*/
        public virtual DbSet<SyTitleIvsapStatus> SyTitleIvsapStatus { get; set; }
        /*<summary>The db set for SyUniversalSearchModules</summary>*/
        public virtual DbSet<SyUniversalSearchModules> SyUniversalSearchModules { get; set; }
        /*<summary>The db set for SyUserImpersonationLog</summary>*/
        public virtual DbSet<SyUserImpersonationLog> SyUserImpersonationLog { get; set; }
        /*<summary>The db set for SyUserQuestions</summary>*/
        public virtual DbSet<SyUserQuestions> SyUserQuestions { get; set; }
        /*<summary>The db set for SyUserResources</summary>*/
        public virtual DbSet<SyUserResources> SyUserResources { get; set; }
        /*<summary>The db set for SyUserResPermissions</summary>*/
        public virtual DbSet<SyUserResPermissions> SyUserResPermissions { get; set; }
        /*<summary>The db set for SyUsers</summary>*/
        public virtual DbSet<SyUsers> SyUsers { get; set; }
        /*<summary>The db set for SyUserSpecQuestions</summary>*/
        public virtual DbSet<SyUserSpecQuestions> SyUserSpecQuestions { get; set; }
        /*<summary>The db set for SyUsersRolesCampGrps</summary>*/
        public virtual DbSet<SyUsersRolesCampGrps> SyUsersRolesCampGrps { get; set; }
        /*<summary>The db set for SyUserTermsOfUse</summary>*/
        public virtual DbSet<SyUserTermsOfUse> SyUserTermsOfUse { get; set; }
        /*<summary>The db set for SyUserType</summary>*/
        public virtual DbSet<SyUserType> SyUserType { get; set; }
        /*<summary>The db set for SyVersionHistory</summary>*/
        public virtual DbSet<SyVersionHistory> SyVersionHistory { get; set; }
        /*<summary>The db set for SyWapiAllowedServices</summary>*/
        public virtual DbSet<SyWapiAllowedServices> SyWapiAllowedServices { get; set; }
        /*<summary>The db set for SyWapiBridgeExternalCompanyAllowedServices</summary>*/
        public virtual DbSet<SyWapiBridgeExternalCompanyAllowedServices> SyWapiBridgeExternalCompanyAllowedServices { get; set; }
        /*<summary>The db set for SyWapiExternalCompanies</summary>*/
        public virtual DbSet<SyWapiExternalCompanies> SyWapiExternalCompanies { get; set; }
        /*<summary>The db set for SyWapiExternalOperationMode</summary>*/
        public virtual DbSet<SyWapiExternalOperationMode> SyWapiExternalOperationMode { get; set; }
        /*<summary>The db set for SyWapiOperationLogger</summary>*/
        public virtual DbSet<SyWapiOperationLogger> SyWapiOperationLogger { get; set; }
        /*<summary>The db set for SyWapiSettings</summary>*/
        public virtual DbSet<SyWapiSettings> SyWapiSettings { get; set; }
        /*<summary>The db set for SyWidgetResourceRoles</summary>*/
        public virtual DbSet<SyWidgetResourceRoles> SyWidgetResourceRoles { get; set; }
        /*<summary>The db set for SyWidgets</summary>*/
        public virtual DbSet<SyWidgets> SyWidgets { get; set; }
        /*<summary>The db set for SyWidgetUserResourceSettings</summary>*/
        public virtual DbSet<SyWidgetUserResourceSettings> SyWidgetUserResourceSettings { get; set; }
        /*<summary>The db set for TblAwardYearMapping</summary>*/
        public virtual DbSet<TblAwardYearMapping> TblAwardYearMapping { get; set; }
        /*<summary>The db set for TblPrefReport</summary>*/
        public virtual DbSet<TblPrefReport> TblPrefReport { get; set; }
        /*<summary>The db set for TempStatusChangeHistoryTbl</summary>*/
        public virtual DbSet<TempStatusChangeHistoryTbl> TempStatusChangeHistoryTbl { get; set; }
        /*<summary>The db set for TmCategories</summary>*/
        public virtual DbSet<TmCategories> TmCategories { get; set; }
        /*<summary>The db set for TmPermissions</summary>*/
        public virtual DbSet<TmPermissions> TmPermissions { get; set; }
        /*<summary>The db set for TmResultActions</summary>*/
        public virtual DbSet<TmResultActions> TmResultActions { get; set; }
        /*<summary>The db set for TmResults</summary>*/
        public virtual DbSet<TmResults> TmResults { get; set; }
        /*<summary>The db set for TmResultTasks</summary>*/
        public virtual DbSet<TmResultTasks> TmResultTasks { get; set; }
        /*<summary>The db set for TmTaskResults</summary>*/
        public virtual DbSet<TmTaskResults> TmTaskResults { get; set; }
        /*<summary>The db set for TmTasks</summary>*/
        public virtual DbSet<TmTasks> TmTasks { get; set; }
        /*<summary>The db set for TmUsersTaskDefaultView</summary>*/
        public virtual DbSet<TmUsersTaskDefaultView> TmUsersTaskDefaultView { get; set; }
        /*<summary>The db set for TmUserTasks</summary>*/
        public virtual DbSet<TmUserTasks> TmUserTasks { get; set; }
        /*<summary>The db set for TmWorkflow</summary>*/
        public virtual DbSet<TmWorkflow> TmWorkflow { get; set; }

        /*<summary>The db set for SyCreditSummary</summary>*/
        public virtual DbSet<SyCreditSummary> SyCreditSummary { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdAdminCriteria>(entity =>
            {
                entity.HasKey(e => e.Admincriteriaid);

                entity.ToTable("adAdminCriteria");

                entity.Property(e => e.Admincriteriaid)
                    .HasColumnName("admincriteriaid")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdAdvInterval>(entity =>
            {
                entity.HasKey(e => e.AdvIntervalId);

                entity.ToTable("adAdvInterval");

                entity.HasIndex(e => new { e.AdvIntervalCode, e.AdvIntervalDescrip })
                    .HasName("UIX_adAdvInterval_AdvIntervalCode_AdvIntervalDescrip")
                    .IsUnique();

                entity.Property(e => e.AdvIntervalId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdvIntervalCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AdvIntervalDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdAdvInterval)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adAdvInterval_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdAdvInterval)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adAdvInterval_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdAgencySponsors>(entity =>
            {
                entity.HasKey(e => e.AgencySpId);

                entity.ToTable("adAgencySponsors");

                entity.HasIndex(e => new { e.AgencySpCode, e.AgencySpDescrip })
                    .HasName("UIX_adAgencySponsors_AgencySpCode_AgencySpDescrip")
                    .IsUnique();

                entity.Property(e => e.AgencySpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AgencySpCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.AgencySpDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdAgencySponsors)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adAgencySponsors_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdAgencySponsors)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adAgencySponsors_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdCitizenships>(entity =>
            {
                entity.HasKey(e => e.CitizenshipId);

                entity.ToTable("adCitizenships");

                entity.HasIndex(e => new { e.CitizenshipCode, e.CitizenshipDescrip })
                    .HasName("UIX_adCitizenships_CitizenshipCode_CitizenshipDescrip")
                    .IsUnique();

                entity.Property(e => e.CitizenshipId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CitizenshipCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CitizenshipDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AfaMapping)
                    .WithMany(p => p.AdCitizenships)
                    .HasForeignKey(d => d.AfaMappingId)
                    .HasConstraintName("FK_adCitizenships_AfaCatalogMapping_AfaMappingId_Id");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdCitizenships)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCitizenships_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdCitizenships)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCitizenships_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdColleges>(entity =>
            {
                entity.HasKey(e => e.CollegeId);

                entity.ToTable("adColleges");

                entity.HasIndex(e => new { e.CollegeCode, e.CollegeName })
                    .HasName("UIX_adColleges_CollegeCode_CollegeName")
                    .IsUnique();

                entity.Property(e => e.CollegeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CollegeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CollegeName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdColleges)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adColleges_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.AdColleges)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_adColleges_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.AdColleges)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_adColleges_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdColleges)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adColleges_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdCounties>(entity =>
            {
                entity.HasKey(e => e.CountyId);

                entity.ToTable("adCounties");

                entity.HasIndex(e => new { e.CountyCode, e.CountyDescrip })
                    .HasName("UIX_adCounties_CountyCode_CountyDescrip")
                    .IsUnique();

                entity.Property(e => e.CountyId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CountyCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CountyDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdCounties)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCounties_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdCounties)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCounties_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdCountries>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.ToTable("adCountries");

                entity.HasIndex(e => new { e.CountryCode, e.CountryDescrip })
                    .HasName("UIX_adCountries_CountryCode_CountryDescrip")
                    .IsUnique();

                entity.Property(e => e.CountryId).ValueGeneratedNever();

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CountryDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsDefault).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdCountries)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCountries_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdCountries)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adCountries_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdDegCertSeeking>(entity =>
            {
                entity.HasKey(e => e.DegCertSeekingId);

                entity.ToTable("adDegCertSeeking");

                entity.Property(e => e.DegCertSeekingId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdDependencyTypes>(entity =>
            {
                entity.HasKey(e => e.DependencyTypeId);

                entity.ToTable("adDependencyTypes");

                entity.Property(e => e.DependencyTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdEdLvls>(entity =>
            {
                entity.HasKey(e => e.EdLvlId);

                entity.ToTable("adEdLvls");

                entity.HasIndex(e => new { e.EdLvlId, e.EdLvlDescrip })
                    .HasName("UIX_adEdLvls_EdLvlId_EdLvlDescrip")
                    .IsUnique();

                entity.Property(e => e.EdLvlId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EdLvlCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.EdLvlDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdEdLvls)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adEdLvls_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdEdLvls)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adEdLvls_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdEntrTestOverRide>(entity =>
            {
                entity.HasKey(e => e.EntrTestOverRideId);

                entity.ToTable("adEntrTestOverRide");

                entity.HasIndex(e => e.LeadId);

                entity.HasIndex(e => new { e.EntrTestId, e.StudentId, e.OverRide });

                entity.Property(e => e.EntrTestOverRideId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverRide)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EntrTest)
                    .WithMany(p => p.AdEntrTestOverRide)
                    .HasForeignKey(d => d.EntrTestId)
                    .HasConstraintName("FK_adEntrTestOverRide_adReqs_EntrTestId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdEntrTestOverRide)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_adEntrTestOverRide_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdEthCodes>(entity =>
            {
                entity.HasKey(e => e.EthCodeId);

                entity.ToTable("adEthCodes");

                entity.HasIndex(e => new { e.EthCodeId, e.EthCodeDescrip })
                    .HasName("UIX_adEthCodes_EthCodeId_EthCodeDescrip")
                    .IsUnique();

                entity.Property(e => e.EthCodeId).ValueGeneratedNever();

                entity.Property(e => e.EthCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.EthCodeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdEthCodes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adEthCodes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdEthCodes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adEthCodes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdExpertiseLevel>(entity =>
            {
                entity.HasKey(e => e.ExpertiseId);

                entity.ToTable("adExpertiseLevel");

                entity.HasIndex(e => new { e.ExpertiseId, e.ExpertiseDescrip })
                    .HasName("UIX_adExpertiseLevel_ExpertiseId_ExpertiseDescrip")
                    .IsUnique();

                entity.Property(e => e.ExpertiseId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ExpertiseCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpertiseDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdExpertiseLevel)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adExpertiseLevel_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdExpertiseLevel)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adExpertiseLevel_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdExpQuickLeadSections>(entity =>
            {
                entity.HasKey(e => e.ExpId);

                entity.ToTable("adExpQuickLeadSections");
            });

            modelBuilder.Entity<AdExtraCurr>(entity =>
            {
                entity.HasKey(e => e.ExtraCurrId);

                entity.ToTable("adExtraCurr");

                entity.HasIndex(e => new { e.ExtraCurrCode, e.ExtraCurrDescrip, e.ExtraCurricularGrpId })
                    .HasName("UIX_adExtraCurr_ExtraCurrCode_ExtraCurrDescrip_ExtraCurricularGrpID")
                    .IsUnique();

                entity.Property(e => e.ExtraCurrId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ExtraCurrCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraCurrDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraCurricularGrpId).HasColumnName("ExtraCurricularGrpID");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdExtraCurr)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurr_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.ExtraCurricularGrp)
                    .WithMany(p => p.AdExtraCurr)
                    .HasForeignKey(d => d.ExtraCurricularGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurr_adExtraCurrGrp_ExtraCurricularGrpID_ExtraCurrGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdExtraCurr)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurr_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdExtraCurrGrp>(entity =>
            {
                entity.HasKey(e => e.ExtraCurrGrpId);

                entity.ToTable("adExtraCurrGrp");

                entity.HasIndex(e => new { e.ExtraCurrGrpCode, e.ExtraCurrGrpDescrip })
                    .HasName("UIX_adExtraCurrGrp_ExtraCurrGrpCode_ExtraCurrGrpDescrip")
                    .IsUnique();

                entity.Property(e => e.ExtraCurrGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ExtraCurrGrpCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExtraCurrGrpDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdExtraCurrGrp)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adExtraCurrGrp_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdExtraCurrGrp)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adExtraCurrGrp_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdExtraCurricular>(entity =>
            {
                entity.HasKey(e => e.IdExtraCurricular);

                entity.ToTable("adExtraCurricular");

                entity.Property(e => e.ExtraCurrComment)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ExtraCurrDescription)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ExtraCurrGrp)
                    .WithMany(p => p.AdExtraCurricular)
                    .HasForeignKey(d => d.ExtraCurrGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurricular_adExtraCurrGrp_ExtraCurrGrpId_ExtraCurrGrpId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdExtraCurricular)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurricular_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.AdExtraCurricular)
                    .HasForeignKey(d => d.LevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adExtraCurricular_adLevel_LevelId_LevelId");
            });

            modelBuilder.Entity<AdFullPartTime>(entity =>
            {
                entity.HasKey(e => e.FullTimeId);

                entity.ToTable("adFullPartTime");

                entity.Property(e => e.FullTimeId).ValueGeneratedNever();

                entity.Property(e => e.FullPartTimeCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullPartTimeDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdFullPartTime)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adFullPartTime_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdFullPartTime)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adFullPartTime_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdGenders>(entity =>
            {
                entity.HasKey(e => e.GenderId);

                entity.ToTable("adGenders");

                entity.Property(e => e.GenderId).ValueGeneratedNever();

                entity.Property(e => e.GenderCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.GenderDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AfaMapping)
                    .WithMany(p => p.AdGenders)
                    .HasForeignKey(d => d.AfaMappingId)
                    .HasConstraintName("FK_adGenders_AfaCatalogMapping_AfaMappingId_Id");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdGenders)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adGenders_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdGenders)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adGenders_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdGeographicTypes>(entity =>
            {
                entity.HasKey(e => e.GeographicTypeId);

                entity.ToTable("adGeographicTypes");

                entity.Property(e => e.GeographicTypeId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdHighSchools>(entity =>
            {
                entity.HasKey(e => e.Hsid);

                entity.ToTable("adHighSchools");

                entity.HasIndex(e => new { e.Hscode, e.Hsname, e.CampGrpId })
                    .HasName("UIX_adHighSchools_HSCode_HSName_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.Hsid)
                    .HasColumnName("HSId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Hscode)
                    .IsRequired()
                    .HasColumnName("HSCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Hsname)
                    .IsRequired()
                    .HasColumnName("HSName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdHighSchools)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adHighSchools_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.AdHighSchools)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_adHighSchools_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.AdHighSchools)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_adHighSchools_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdHighSchools)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adHighSchools_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdImportLeadsAdvFldMappings>(entity =>
            {
                entity.HasKey(e => e.AdvFieldMapping);

                entity.ToTable("adImportLeadsAdvFldMappings");

                entity.Property(e => e.AdvFieldMapping).HasDefaultValueSql("(newid())");

                entity.Property(e => e.IlfieldId).HasColumnName("ILFieldId");

                entity.HasOne(d => d.Ilfield)
                    .WithMany(p => p.AdImportLeadsAdvFldMappings)
                    .HasForeignKey(d => d.IlfieldId)
                    .HasConstraintName("FK_adImportLeadsAdvFldMappings_adImportLeadsFields_ILFieldId_ILFieldId");

                entity.HasOne(d => d.Mapping)
                    .WithMany(p => p.AdImportLeadsAdvFldMappings)
                    .HasForeignKey(d => d.MappingId)
                    .HasConstraintName("FK_adImportLeadsAdvFldMappings_adImportLeadsMappings_MappingId_MappingId");
            });

            modelBuilder.Entity<AdImportLeadsFields>(entity =>
            {
                entity.HasKey(e => e.IlfieldId);

                entity.ToTable("adImportLeadsFields");

                entity.Property(e => e.IlfieldId).HasColumnName("ILFieldId");

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ddlname)
                    .HasColumnName("DDLName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FldName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdImportLeadsLookupValsMap>(entity =>
            {
                entity.HasKey(e => e.LookupValuesMapId);

                entity.ToTable("adImportLeadsLookupValsMap");

                entity.Property(e => e.LookupValuesMapId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FileValue)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IlfieldId).HasColumnName("ILFieldId");

                entity.HasOne(d => d.Ilfield)
                    .WithMany(p => p.AdImportLeadsLookupValsMap)
                    .HasForeignKey(d => d.IlfieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adImportLeadsLookupValsMap_adImportLeadsFields_ILFieldId_ILFieldId");

                entity.HasOne(d => d.Mapping)
                    .WithMany(p => p.AdImportLeadsLookupValsMap)
                    .HasForeignKey(d => d.MappingId)
                    .HasConstraintName("FK_adImportLeadsLookupValsMap_adImportLeadsMappings_MappingId_MappingId");
            });

            modelBuilder.Entity<AdImportLeadsMappings>(entity =>
            {
                entity.HasKey(e => e.MappingId);

                entity.ToTable("adImportLeadsMappings");

                entity.Property(e => e.MappingId).ValueGeneratedNever();

                entity.Property(e => e.MapName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdImportLeadsMappings)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adImportLeadsMappings_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdImportLeadsMappings)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adImportLeadsMappings_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLastNameHistory>(entity =>
            {
                entity.ToTable("adLastNameHistory");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLastNameHistory)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLastNameHistory_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeadAddresses>(entity =>
            {
                entity.HasKey(e => e.AdLeadAddressId);

                entity.ToTable("adLeadAddresses");

                entity.HasIndex(e => new { e.LeadId, e.StatusId, e.IsShowOnLeadPage });

                entity.Property(e => e.AdLeadAddressId)
                    .HasColumnName("adLeadAddressId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Address2)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AddressApto)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.City)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignCountryStr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignCountyStr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsInternational).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadAddresses_plAddressTypes_AddressTypeId_AddressTypeId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_adLeadAddresses_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_adLeadAddresses_adCounties_CountyId_CountyId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadAddresses_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.StateNavigation)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_adLeadAddresses_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadAddresses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadAddresses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadByLeadGroups>(entity =>
            {
                entity.HasKey(e => e.LeadGrpLeadId);

                entity.ToTable("adLeadByLeadGroups");

                entity.HasIndex(e => new { e.StuEnrollId, e.LeadGrpId });

                entity.Property(e => e.LeadGrpLeadId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LeadGrp)
                    .WithMany(p => p.AdLeadByLeadGroups)
                    .HasForeignKey(d => d.LeadGrpId)
                    .HasConstraintName("FK_adLeadByLeadGroups_adLeadGroups_LeadGrpId_LeadGrpId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadByLeadGroups)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_adLeadByLeadGroups_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.AdLeadByLeadGroups)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_adLeadByLeadGroups_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<AdLeadDocsReceived>(entity =>
            {
                entity.HasKey(e => e.LeadDocId);

                entity.ToTable("adLeadDocsReceived");

                entity.HasIndex(e => new { e.LeadId, e.DocumentId })
                    .HasName("UIX_adLeadDocsReceived_LeadId_DocumentId")
                    .IsUnique();

                entity.Property(e => e.LeadDocId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverrideReason)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.HasOne(d => d.DocStatus)
                    .WithMany(p => p.AdLeadDocsReceived)
                    .HasForeignKey(d => d.DocStatusId)
                    .HasConstraintName("FK_adLeadDocsReceived_syDocStatuses_DocStatusId_DocStatusId");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.AdLeadDocsReceived)
                    .HasForeignKey(d => d.DocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadDocsReceived_adReqs_DocumentId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadDocsReceived)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadDocsReceived_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeadDuplicates>(entity =>
            {
                entity.HasKey(e => e.IdDuplicates);

                entity.ToTable("adLeadDuplicates");

                entity.HasOne(d => d.NewLeadGu)
                    .WithMany(p => p.AdLeadDuplicatesNewLeadGu)
                    .HasForeignKey(d => d.NewLeadGuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadDuplicates_adLeads_NewLeadGuid_LeadId");

                entity.HasOne(d => d.PosibleDuplicateGu)
                    .WithMany(p => p.AdLeadDuplicatesPosibleDuplicateGu)
                    .HasForeignKey(d => d.PosibleDuplicateGuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadDuplicates_adLeads_PosibleDuplicateGuid_LeadId");
            });

            modelBuilder.Entity<AdLeadEducation>(entity =>
            {
                entity.HasKey(e => e.LeadEducationId);

                entity.ToTable("adLeadEducation");

                entity.Property(e => e.LeadEducationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Certificate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EducationInstType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FinalGrade)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpa)
                    .HasColumnName("GPA")
                    .HasColumnType("decimal(3, 2)");

                entity.Property(e => e.GraduatedDate).HasColumnType("datetime");

                entity.Property(e => e.Major)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.HasOne(d => d.CertificateNavigation)
                    .WithMany(p => p.AdLeadEducation)
                    .HasForeignKey(d => d.CertificateId)
                    .HasConstraintName("FK_adLeadEducation_arDegrees_CertificateId_DegreeId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadEducation)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadEducation_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadEducation)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadEducation_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadEmail>(entity =>
            {
                entity.HasKey(e => e.LeadEmailId);

                entity.Property(e => e.LeadEmailId)
                    .HasColumnName("LeadEMailId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("EMail")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailTypeId).HasColumnName("EMailTypeId");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.HasOne(d => d.EmailType)
                    .WithMany(p => p.AdLeadEmail)
                    .HasForeignKey(d => d.EmailTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadEmail)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeadEmail_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadEmail)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeadEmail_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadEmployment>(entity =>
            {
                entity.HasKey(e => e.StEmploymentId);

                entity.ToTable("adLeadEmployment");

                entity.HasIndex(e => new { e.LeadId, e.JobTitleId, e.EmployerName, e.EmployerJobTitle })
                    .HasName("UIX_adLeadEmployment_LeadId_JobTitleId_EmployerName_EmployerJobTitle")
                    .IsUnique();

                entity.Property(e => e.StEmploymentId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerJobTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.JobResponsibilities)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.JobStatus)
                    .WithMany(p => p.AdLeadEmployment)
                    .HasForeignKey(d => d.JobStatusId)
                    .HasConstraintName("FK_adLeadEmployment_plJobStatus_JobStatusId_JobStatusId");

                entity.HasOne(d => d.JobTitle)
                    .WithMany(p => p.AdLeadEmployment)
                    .HasForeignKey(d => d.JobTitleId)
                    .HasConstraintName("FK_adLeadEmployment_adTitles_JobTitleId_TitleId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadEmployment)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadEmployment_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeadEntranceTest>(entity =>
            {
                entity.HasKey(e => e.LeadEntrTestId);

                entity.ToTable("adLeadEntranceTest");

                entity.HasIndex(e => new { e.LeadId, e.EntrTestId });

                entity.Property(e => e.LeadEntrTestId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ActualScore).HasColumnType("decimal(19, 3)");

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CompletedDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverrideReason)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.TestTaken)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EntrTest)
                    .WithMany(p => p.AdLeadEntranceTest)
                    .HasForeignKey(d => d.EntrTestId)
                    .HasConstraintName("FK_adLeadEntranceTest_adReqs_EntrTestId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadEntranceTest)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_adLeadEntranceTest_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeadExtraCurriculars>(entity =>
            {
                entity.HasKey(e => e.LeadExtraCurricularId);

                entity.ToTable("adLeadExtraCurriculars");

                entity.Property(e => e.LeadExtraCurricularId)
                    .HasColumnName("LeadExtraCurricularID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ExtraCurricularId).HasColumnName("ExtraCurricularID");

                entity.Property(e => e.LeadId).HasColumnName("LeadID");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadExtraCurriculars)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_adLeadExtraCurriculars_adLeads_LeadID_LeadId");
            });

            modelBuilder.Entity<AdLeadFields>(entity =>
            {
                entity.HasKey(e => e.LeadFieldId);

                entity.ToTable("adLeadFields");

                entity.Property(e => e.LeadFieldId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.LeadId).HasColumnName("LeadID");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdLeadGroups>(entity =>
            {
                entity.HasKey(e => e.LeadGrpId);

                entity.ToTable("adLeadGroups");

                entity.Property(e => e.LeadGrpId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UseForScheduling).HasDefaultValueSql("((0))");

                entity.Property(e => e.UseForStudentGroupTracking).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdLeadGroups)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadGroups_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadGroups)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadGroups_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadGrpReqGroups>(entity =>
            {
                entity.HasKey(e => e.LeadGrpReqGrpId);

                entity.ToTable("adLeadGrpReqGroups");

                entity.HasIndex(e => new { e.ReqGrpId, e.LeadGrpId })
                    .HasName("UIX_adLeadGrpReqGroups_ReqGrpId_LeadGrpId")
                    .IsUnique();

                entity.Property(e => e.LeadGrpReqGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.HasOne(d => d.LeadGrp)
                    .WithMany(p => p.AdLeadGrpReqGroups)
                    .HasForeignKey(d => d.LeadGrpId)
                    .HasConstraintName("FK_adLeadGrpReqGroups_adLeadGroups_LeadGrpId_LeadGrpId");

                entity.HasOne(d => d.ReqGrp)
                    .WithMany(p => p.AdLeadGrpReqGroups)
                    .HasForeignKey(d => d.ReqGrpId)
                    .HasConstraintName("FK_adLeadGrpReqGroups_adReqGroups_ReqGrpId_ReqGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadGrpReqGroups)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadGrpReqGroups_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadImports>(entity =>
            {
                entity.HasKey(e => e.LeadImportId);

                entity.ToTable("adLeadImports");

                entity.Property(e => e.AbilityToBenefit)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AccruedKit)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AccruedOthr)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AccruedReg)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AccruedTuit)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCity)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLine1)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AddressState)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Age)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo1)
                    .HasColumnName("AllClinicInfo_1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo10)
                    .HasColumnName("AllClinicInfo_10")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo11)
                    .HasColumnName("AllClinicInfo_11")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo12)
                    .HasColumnName("AllClinicInfo_12")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo13)
                    .HasColumnName("AllClinicInfo_13")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo14)
                    .HasColumnName("AllClinicInfo_14")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo15)
                    .HasColumnName("AllClinicInfo_15")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo16)
                    .HasColumnName("AllClinicInfo_16")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo17)
                    .HasColumnName("AllClinicInfo_17")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo18)
                    .HasColumnName("AllClinicInfo_18")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo19)
                    .HasColumnName("AllClinicInfo_19")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo2)
                    .HasColumnName("AllClinicInfo_2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo20)
                    .HasColumnName("AllClinicInfo_20")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo21)
                    .HasColumnName("AllClinicInfo_21")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo22)
                    .HasColumnName("AllClinicInfo_22")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo23)
                    .HasColumnName("AllClinicInfo_23")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo24)
                    .HasColumnName("AllClinicInfo_24")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo25)
                    .HasColumnName("AllClinicInfo_25")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo26)
                    .HasColumnName("AllClinicInfo_26")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo27)
                    .HasColumnName("AllClinicInfo_27")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo28)
                    .HasColumnName("AllClinicInfo_28")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo29)
                    .HasColumnName("AllClinicInfo_29")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo3)
                    .HasColumnName("AllClinicInfo_3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo30)
                    .HasColumnName("AllClinicInfo_30")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo31)
                    .HasColumnName("AllClinicInfo_31")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo32)
                    .HasColumnName("AllClinicInfo_32")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo33)
                    .HasColumnName("AllClinicInfo_33")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo34)
                    .HasColumnName("AllClinicInfo_34")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo35)
                    .HasColumnName("AllClinicInfo_35")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo36)
                    .HasColumnName("AllClinicInfo_36")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo37)
                    .HasColumnName("AllClinicInfo_37")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo38)
                    .HasColumnName("AllClinicInfo_38")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo39)
                    .HasColumnName("AllClinicInfo_39")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo4)
                    .HasColumnName("AllClinicInfo_4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo40)
                    .HasColumnName("AllClinicInfo_40")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo5)
                    .HasColumnName("AllClinicInfo_5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo6)
                    .HasColumnName("AllClinicInfo_6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo7)
                    .HasColumnName("AllClinicInfo_7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo8)
                    .HasColumnName("AllClinicInfo_8")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AllClinicInfo9)
                    .HasColumnName("AllClinicInfo_9")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atbscore)
                    .HasColumnName("ATBscore")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttendMonthKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsFri)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsMon)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsSat)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsSun)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsThr)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsTue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttndSchdHrsWed)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BeginLeaveDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BeginProbationDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CitizenType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClassIdNum)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractedGradDt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CourseNumEnrolledIn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreditHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentEnrollStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DefaultLtrRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DegreeSeekingType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Dependency)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DropReasonCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EducationLevel)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndLeaveDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EnrolledDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EntranceinterviewDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FinAidRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullPartTimeRop)
                    .HasColumnName("FullPartTimeROP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullTimePartTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GradRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HeadHousehold)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HighSchGeddate)
                    .HasColumnName("HighSchGEDDate")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.HighSchoolRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HousingType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InHighSchool)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IncomeLevel)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LabCnt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastDateAttended)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastMonthAbsentHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastMonthMakeupHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastMonthTotalHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MilesFromSchool)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MonthEndPellPaidHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NextStudRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NotesKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumOfDeps)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumPracticalTaken)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumWrittenTaken)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OhioClinicHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OhioDemoHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OhioPracticalHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OhioTheoryHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ostudent)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Over50Miles)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumAreaCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumAreaCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumBody1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumBody2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumPrefix1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumPrefix2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneType1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneType2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrevEnrollEndDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrevStudRecKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCourseKitCost)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCourseOthrCost)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCourseRegCost)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCourseRoomCost)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCourseTuitCost)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivCurBalOwed)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivFirstTransDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivLastTransDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivOvtChargedAmt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivStatusLogKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivTclockSchdRecKey)
                    .HasColumnName("PrivTClockSchdRecKey")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivTotalHrsAbsent)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivTotalHrsAttended)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivTransferedInHrs)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrivZipPlus4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProspectKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Race)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReEnrolledDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecNum)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResultCnt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResultTot)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RevisedGradDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SapinclPrevEnr)
                    .HasColumnName("SAPInclPrevEnr")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs1)
                    .HasColumnName("SchdDailyHrs_1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs2)
                    .HasColumnName("SchdDailyHrs_2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs3)
                    .HasColumnName("SchdDailyHrs_3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs4)
                    .HasColumnName("SchdDailyHrs_4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs5)
                    .HasColumnName("SchdDailyHrs_5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs6)
                    .HasColumnName("SchdDailyHrs_6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdDailyHrs7)
                    .HasColumnName("SchdDailyHrs_7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchdHrsLastJune30th)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduledHrsLda)
                    .HasColumnName("ScheduledHrsLDA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sex)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateRegDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateRegId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StuGroupKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StuTransKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubjectResKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TclockBadgeNum)
                    .HasColumnName("TClockBadgeNum")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermResKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotHrsLastJune30th)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotPracticalScores)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotWrittenScores)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalHrsMakeup)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TranHrsUpFront)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransHrsLnYr1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrbanType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkUnitResKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.XferSubjResKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.YearlyIncome)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdLeadNotes>(entity =>
            {
                entity.ToTable("adLead_Notes");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadNotes)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLead_Notes_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Notes)
                    .WithMany(p => p.AdLeadNotes)
                    .HasForeignKey(d => d.NotesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLead_Notes_AllNotes_NotesId_NotesId");
            });

            modelBuilder.Entity<AdLeadNotes1>(entity =>
            {
                entity.HasKey(e => e.LeadNoteId);

                entity.ToTable("adLeadNotes");

                entity.Property(e => e.LeadNoteId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.LeadNoteDescrip)
                    .IsRequired()
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('UK')");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadNotes1)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadNotes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadOtherContacts>(entity =>
            {
                entity.HasKey(e => e.OtherContactId);

                entity.ToTable("adLeadOtherContacts");

                entity.HasIndex(e => e.OtherContactId)
                    .HasName("UIX_adLeadOtherContacts_OtherContactId")
                    .IsUnique();

                entity.Property(e => e.OtherContactId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comments)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ContactType)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.ContactTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContacts_syContactTypes_ContactTypeId_ContactTypeId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContacts_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_adLeadOtherContacts_syPrefixes_PrefixId_PrefixId");

                entity.HasOne(d => d.Relationship)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.RelationshipId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContacts_syRelations_RelationshipId_RelationId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContacts_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Sufix)
                    .WithMany(p => p.AdLeadOtherContacts)
                    .HasForeignKey(d => d.SufixId)
                    .HasConstraintName("FK_adLeadOtherContacts_sySuffixes_SufixId_SuffixId");
            });

            modelBuilder.Entity<AdLeadOtherContactsAddreses>(entity =>
            {
                entity.HasKey(e => e.OtherContactsAddresesId);

                entity.ToTable("adLeadOtherContactsAddreses");

                entity.Property(e => e.OtherContactsAddresesId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.County)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsInternational).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_plAddressTypes_AddressTypeId_AddressTypeId");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.CountyNavigation)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_adCounties_CountyId_CountyId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.OtherContact)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.OtherContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_adLeadOtherContacts_OtherContactId_OtherContactId");

                entity.HasOne(d => d.StateNavigation)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadOtherContactsAddreses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsAddreses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadOtherContactsEmail>(entity =>
            {
                entity.HasKey(e => e.OtherContactsEmailId);

                entity.ToTable("adLeadOtherContactsEmail");

                entity.Property(e => e.OtherContactsEmailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("EMail")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EmailTypeId).HasColumnName("EMailTypeId");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EmailType)
                    .WithMany(p => p.AdLeadOtherContactsEmail)
                    .HasForeignKey(d => d.EmailTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsEmail_syEmailType_EmailTypeId_EmailTypeId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadOtherContactsEmail)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsEmail_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.OtherContact)
                    .WithMany(p => p.AdLeadOtherContactsEmail)
                    .HasForeignKey(d => d.OtherContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsEmail_adLeadOtherContacts_OtherContactId_OtherContactId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadOtherContactsEmail)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adLeadOtherContactsEmail_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadOtherContactsPhone>(entity =>
            {
                entity.HasKey(e => e.OtherContactsPhoneId);

                entity.ToTable("adLeadOtherContactsPhone");

                entity.Property(e => e.OtherContactsPhoneId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadOtherContactsPhone)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsPhone_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.OtherContact)
                    .WithMany(p => p.AdLeadOtherContactsPhone)
                    .HasForeignKey(d => d.OtherContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsPhone_adLeadOtherContacts_OtherContactId_OtherContactId");

                entity.HasOne(d => d.PhoneType)
                    .WithMany(p => p.AdLeadOtherContactsPhone)
                    .HasForeignKey(d => d.PhoneTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadOtherContactsPhone_syPhoneType_PhoneTypeId_PhoneTypeId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadOtherContactsPhone)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adLeadOtherContactsPhone_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadPayments>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.ToTable("adLeadPayments");

                entity.Property(e => e.TransactionId).ValueGeneratedNever();

                entity.Property(e => e.CheckNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentReference).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Transaction)
                    .WithOne(p => p.AdLeadPayments)
                    .HasForeignKey<AdLeadPayments>(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadPayments_adLeadTransactions_TransactionId_TransactionId");
            });

            modelBuilder.Entity<AdLeadPhone>(entity =>
            {
                entity.HasKey(e => e.LeadPhoneId);

                entity.ToTable("adLeadPhone");

                entity.HasIndex(e => new { e.LeadId, e.Position });

                entity.Property(e => e.LeadPhoneId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Extension)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadPhone)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadPhone_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.PhoneType)
                    .WithMany(p => p.AdLeadPhone)
                    .HasForeignKey(d => d.PhoneTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadPhone_syPhoneType_PhoneTypeId_PhoneTypeId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLeadPhone)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadPhone_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdLeadReqsReceived>(entity =>
            {
                entity.HasKey(e => e.LeadReqId);

                entity.Property(e => e.LeadReqId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverrideReason)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ReceivedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.AdLeadReqsReceived)
                    .HasForeignKey(d => d.DocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeadReqsReceived_adReqs_DocumentId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadReqsReceived)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeadReqsReceived_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeads>(entity =>
            {
                entity.HasKey(e => e.LeadId);

                entity.ToTable("adLeads");

                entity.HasIndex(e => e.FirstName);

                entity.HasIndex(e => e.LastName);

                entity.HasIndex(e => e.Ssn);

                entity.HasIndex(e => e.StudentId)
                    .HasName("UIX_adLeads_StudentId_ExcludeStudentIdWithZeros")
                    .IsUnique()
                    .HasFilter("([StudentId]<>'00000000-0000-0000-0000-000000000000')");

                entity.HasIndex(e => e.StudentNumber);

                entity.HasIndex(e => new { e.LeadId, e.ModDate, e.CampusId })
                    .HasName("IX_adLeads_CampusId_LeadId_ModDate");

                entity.Property(e => e.LeadId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressApt)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Admincriteriaid).HasColumnName("admincriteriaid");

                entity.Property(e => e.AdvertisementNote)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Age)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AlienNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AreaId).HasColumnName("AreaID");

                entity.Property(e => e.AssignedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.BestTime)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.CampusOfInterest)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Children)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(240)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateApplied).HasColumnType("datetime");

                entity.Property(e => e.DefaultPhone).HasDefaultValueSql("((1))");

                entity.Property(e => e.DrivLicNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DrivLicStateId).HasColumnName("DrivLicStateID");

                entity.Property(e => e.EntranceInterviewDate).HasColumnType("datetime");

                entity.Property(e => e.ExpectedStart).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignPhone2).HasDefaultValueSql("((0))");

                entity.Property(e => e.HighSchoolGradDate).HasColumnType("datetime");

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InquiryTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NickName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreferredContactId).HasDefaultValueSql("((1))");

                entity.Property(e => e.ProgramId).HasColumnName("ProgramID");

                entity.Property(e => e.ProgramOfInterest)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProspectId)
                    .HasColumnName("ProspectID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecruitmentOffice)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShiftId).HasColumnName("ShiftID");

                entity.Property(e => e.SourceCategoryId).HasColumnName("SourceCategoryID");

                entity.Property(e => e.SourceDate).HasColumnType("datetime");

                entity.Property(e => e.SourceTypeId).HasColumnName("SourceTypeID");

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.StudentStatusId).HasDefaultValueSql("('1AF592A6-8790-48EC-9916-5412C25EF49F')");

                entity.Property(e => e.WorkEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AddressStatusNavigation)
                    .WithMany(p => p.AdLeadsAddressStatusNavigation)
                    .HasForeignKey(d => d.AddressStatus)
                    .HasConstraintName("FK_adLeads_syStatuses_AddressStatus_StatusId");

                entity.HasOne(d => d.AddressTypeNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.AddressType)
                    .HasConstraintName("FK_adLeads_plAddressTypes_AddressType_AddressTypeId");

                entity.HasOne(d => d.Admincriteria)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Admincriteriaid)
                    .HasConstraintName("FK_adLeads_adAdminCriteria_admincriteriaid_admincriteriaid");

                entity.HasOne(d => d.AdmissionsRepNavigation)
                    .WithMany(p => p.AdLeadsAdmissionsRepNavigation)
                    .HasForeignKey(d => d.AdmissionsRep)
                    .HasConstraintName("FK_adLeads_syUsers_AdmissionsRep_UserId");

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_adLeads_arPrgGrp_AreaID_PrgGrpId");

                entity.HasOne(d => d.AttendType)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.AttendTypeId)
                    .HasConstraintName("FK_adLeads_arAttendTypes_AttendTypeId_AttendTypeId");

                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.CampaignId)
                    .HasConstraintName("FK_adLeads_adVendorCampaign_CampaignId_CampaignId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_adLeads_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.CitizenNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Citizen)
                    .HasConstraintName("FK_adLeads_adCitizenships_Citizen_CitizenshipId");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Country)
                    .HasConstraintName("FK_adLeads_adCountries_Country_CountryId");

                entity.HasOne(d => d.CountyNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.County)
                    .HasConstraintName("FK_adLeads_adCounties_County_CountyId");

                entity.HasOne(d => d.DegCertSeeking)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.DegCertSeekingId)
                    .HasConstraintName("FK_adLeads_adDegCertSeeking_DegCertSeekingId_DegCertSeekingId");

                entity.HasOne(d => d.DependencyType)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.DependencyTypeId)
                    .HasConstraintName("FK_adLeads_adDependencyTypes_DependencyTypeId_DependencyTypeId");

                entity.HasOne(d => d.DrivLicState)
                    .WithMany(p => p.AdLeadsDrivLicState)
                    .HasForeignKey(d => d.DrivLicStateId)
                    .HasConstraintName("FK_adLeads_syStates_DrivLicStateID_StateId");

                entity.HasOne(d => d.EnrollState)
                    .WithMany(p => p.AdLeadsEnrollState)
                    .HasForeignKey(d => d.EnrollStateId)
                    .HasConstraintName("FK_adLeads_syStates_EnrollStateId_StateId");

                entity.HasOne(d => d.FamilyIncomeNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.FamilyIncome)
                    .HasConstraintName("FK_adLeads_syFamilyIncome_FamilyIncome_FamilyIncomeID");

                entity.HasOne(d => d.GenderNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Gender)
                    .HasConstraintName("FK_adLeads_adGenders_Gender_GenderId");

                entity.HasOne(d => d.GeographicType)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.GeographicTypeId)
                    .HasConstraintName("FK_adLeads_adGeographicTypes_GeographicTypeId_GeographicTypeId");

                entity.HasOne(d => d.HighSchool)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.HighSchoolId)
                    .HasConstraintName("FK_adLeads_SyInstitutions_HighSchoolId_HSId");

                entity.HasOne(d => d.Housing)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.HousingId)
                    .HasConstraintName("FK_adLeads_arHousing_HousingId_HousingId");

                entity.HasOne(d => d.LeadStatusNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.LeadStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeads_syStatusCodes_LeadStatus_StatusCodeId");

                entity.HasOne(d => d.Leadgrp)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.LeadgrpId)
                    .HasConstraintName("FK_adLeads_adLeadGroups_LeadGrpId_LeadGrpId");

                entity.HasOne(d => d.MaritalStatusNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.MaritalStatus)
                    .HasConstraintName("FK_adLeads_adMaritalStatus_MaritalStatus_MaritalStatId");

                entity.HasOne(d => d.NationalityNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Nationality)
                    .HasConstraintName("FK_adLeads_adNationalities_Nationality_NationalityId");

                entity.HasOne(d => d.PhoneStatusNavigation)
                    .WithMany(p => p.AdLeadsPhoneStatusNavigation)
                    .HasForeignKey(d => d.PhoneStatus)
                    .HasConstraintName("FK_adLeads_syStatuses_PhoneStatus_StatusId");

                entity.HasOne(d => d.PrefixNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Prefix)
                    .HasConstraintName("FK_adLeads_syPrefixes_Prefix_PrefixId");

                entity.HasOne(d => d.PreviousEducationNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.PreviousEducation)
                    .HasConstraintName("FK_adLeads_adEdLvls_PreviousEducation_EdLvlId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_adLeads_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.Program)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.ProgramId)
                    .HasConstraintName("FK_adLeads_arPrograms_ProgramID_ProgId");

                entity.HasOne(d => d.ProgramSchedule)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.ProgramScheduleId)
                    .HasConstraintName("FK_adLeads_arProgSchedules_ProgramScheduleId_ScheduleId");

                entity.HasOne(d => d.RaceNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Race)
                    .HasConstraintName("FK_adLeads_adEthCodes_Race_EthCodeId");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("FK_adLeads_arShifts_ShiftID_ShiftId");

                entity.HasOne(d => d.SourceAdvertisementNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.SourceAdvertisement)
                    .HasConstraintName("FK_adLeads_adSourceAdvertisement_SourceAdvertisement_SourceAdvId");

                entity.HasOne(d => d.SourceCategory)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.SourceCategoryId)
                    .HasConstraintName("FK_adLeads_adSourceCatagory_SourceCategoryID_SourceCatagoryId");

                entity.HasOne(d => d.SourceType)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.SourceTypeId)
                    .HasConstraintName("FK_adLeads_adSourceType_SourceTypeID_SourceTypeId");

                entity.HasOne(d => d.SponsorNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Sponsor)
                    .HasConstraintName("FK_adLeads_adAgencySponsors_Sponsor_AgencySpId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.AdLeadsState)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_adLeads_syStates_StateId_StateId");

                entity.HasOne(d => d.StudentStatus)
                    .WithMany(p => p.AdLeadsStudentStatus)
                    .HasForeignKey(d => d.StudentStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdLeads_syStatuses_StudentStatusId_StatusId");

                entity.HasOne(d => d.SuffixNavigation)
                    .WithMany(p => p.AdLeads)
                    .HasForeignKey(d => d.Suffix)
                    .HasConstraintName("FK_adLeads_sySuffixes_Suffix_SuffixId");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.AdLeadsVendor)
                    .HasForeignKey(d => d.VendorId)
                    .HasConstraintName("FK_adLeads_syUsers_VendorId_UserId");
            });

            modelBuilder.Entity<AdLeadSkills>(entity =>
            {
                entity.HasKey(e => e.LeadSkillId);

                entity.ToTable("adLeadSkills");

                entity.Property(e => e.LeadSkillId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadSkills)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_adLeadSkills_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.AdLeadSkills)
                    .HasForeignKey(d => d.SkillId)
                    .HasConstraintName("FK_adLeadSkills_plSkills_SkillId_SkillId");
            });

            modelBuilder.Entity<AdLeadTranReceived>(entity =>
            {
                entity.ToTable("adLeadTranReceived");

                entity.Property(e => e.AdLeadTranReceivedId)
                    .HasColumnName("adLeadTranReceivedId")
                    .ValueGeneratedNever();

                entity.Property(e => e.ApprovalDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverrideReason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReceivedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.AdLeadTranReceived)
                    .HasForeignKey(d => d.DocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadTranReceived_adReqs_DocumentId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadTranReceived)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadTranReceived_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdLeadTransactions>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.ToTable("adLeadTransactions");

                entity.Property(e => e.TransactionId).ValueGeneratedNever();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.IsEnrolled).HasColumnName("isEnrolled");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReversalReason)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.TransAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.TransDate).HasColumnType("datetime");

                entity.Property(e => e.TransDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransReference)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdLeadTransactions)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadTransactions_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.LeadRequirement)
                    .WithMany(p => p.AdLeadTransactions)
                    .HasForeignKey(d => d.LeadRequirementId)
                    .HasConstraintName("FK_adLeadTransactions_adReqs_LeadRequirementId_adReqId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.AdLeadTransactions)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadTransactions_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TransType)
                    .WithMany(p => p.AdLeadTransactions)
                    .HasForeignKey(d => d.TransTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLeadTransactions_saTransTypes_TransTypeId_TransTypeId");
            });

            modelBuilder.Entity<AdLevel>(entity =>
            {
                entity.HasKey(e => e.LevelId);

                entity.ToTable("adLevel");

                entity.Property(e => e.LevelId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("('F23DE1E2-D90A-4720-B4C7-0F6FB09C9965')");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdLevel)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adLevel_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdMaritalStatus>(entity =>
            {
                entity.HasKey(e => e.MaritalStatId);

                entity.ToTable("adMaritalStatus");

                entity.Property(e => e.MaritalStatId).ValueGeneratedNever();

                entity.Property(e => e.MaritalStatCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.MaritalStatDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AfaMapping)
                    .WithMany(p => p.AdMaritalStatus)
                    .HasForeignKey(d => d.AfaMappingId)
                    .HasConstraintName("FK_adMaritalStatus_AfaCatalogMapping_afaMappingId_Id");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdMaritalStatus)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adMaritalStatus_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdMaritalStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adMaritalStatus_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdNationalities>(entity =>
            {
                entity.HasKey(e => e.NationalityId);

                entity.ToTable("adNationalities");

                entity.HasIndex(e => new { e.NationalityCode, e.NationalityDescrip, e.CampGrpId })
                    .HasName("UIX_adNationalities_NationalityCode_NationalityDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.NationalityId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalityCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.NationalityDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdNationalities)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adNationalities_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdNationalities)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adNationalities_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdPrgVerTestDetails>(entity =>
            {
                entity.HasKey(e => e.PrgVerTestDetailId);

                entity.ToTable("adPrgVerTestDetails");

                entity.Property(e => e.PrgVerTestDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdReqId).HasColumnName("adReqId");

                entity.Property(e => e.MinScore).HasColumnType("decimal(19, 3)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AdReq)
                    .WithMany(p => p.AdPrgVerTestDetails)
                    .HasForeignKey(d => d.AdReqId)
                    .HasConstraintName("FK_adPrgVerTestDetails_adReqs_adReqId_adReqId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.AdPrgVerTestDetails)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_adPrgVerTestDetails_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.ReqGrp)
                    .WithMany(p => p.AdPrgVerTestDetails)
                    .HasForeignKey(d => d.ReqGrpId)
                    .HasConstraintName("FK_adPrgVerTestDetails_adReqGroups_ReqGrpId_ReqGrpId");
            });

            modelBuilder.Entity<AdQuickLead>(entity =>
            {
                entity.HasKey(e => e.QkLeadId);

                entity.ToTable("adQuickLead");

                entity.Property(e => e.QkLeadId)
                    .HasColumnName("QkLeadID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.LeadValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentId)
                    .HasColumnName("StudentID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdQuickLeadMap>(entity =>
            {
                entity.HasKey(e => e.MapId);

                entity.ToTable("adQuickLeadMap");

                entity.Property(e => e.MapId).HasColumnName("mapId");

                entity.Property(e => e.CtrlIdName)
                    .HasColumnName("ctrlIdName")
                    .HasMaxLength(100);

                entity.Property(e => e.FldName)
                    .HasColumnName("fldName")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCtrlId)
                    .HasColumnName("parentCtrlId")
                    .HasMaxLength(50);

                entity.Property(e => e.PropName)
                    .HasColumnName("propName")
                    .HasMaxLength(100);

                entity.Property(e => e.Sequence).HasColumnName("sequence");
            });

            modelBuilder.Entity<AdQuickLeadSections>(entity =>
            {
                entity.HasKey(e => e.SectionId);

                entity.ToTable("adQuickLeadSections");

                entity.Property(e => e.SectionId).ValueGeneratedNever();

                entity.Property(e => e.SectionName)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdReqGroups>(entity =>
            {
                entity.HasKey(e => e.ReqGrpId);

                entity.ToTable("adReqGroups");

                entity.Property(e => e.ReqGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsMandatoryReqGrp).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdReqGroups)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adReqGroups_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdReqGroups)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adReqGroups_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdReqGrpDef>(entity =>
            {
                entity.HasKey(e => e.ReqGrpDefId);

                entity.ToTable("adReqGrpDef");

                entity.Property(e => e.ReqGrpDefId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdReqId).HasColumnName("adReqId");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AdReq)
                    .WithMany(p => p.AdReqGrpDef)
                    .HasForeignKey(d => d.AdReqId)
                    .HasConstraintName("FK_adReqGrpDef_adReqs_adReqId_adReqId");

                entity.HasOne(d => d.LeadGrp)
                    .WithMany(p => p.AdReqGrpDef)
                    .HasForeignKey(d => d.LeadGrpId)
                    .HasConstraintName("FK_adReqGrpDef_adLeadGroups_LeadGrpId_LeadGrpId");

                entity.HasOne(d => d.ReqGrp)
                    .WithMany(p => p.AdReqGrpDef)
                    .HasForeignKey(d => d.ReqGrpId)
                    .HasConstraintName("FK_adReqGrpDef_adReqGroups_ReqGrpId_ReqGrpId");
            });

            modelBuilder.Entity<AdReqLeadGroups>(entity =>
            {
                entity.HasKey(e => e.ReqLeadGrpId);

                entity.ToTable("adReqLeadGroups");

                entity.Property(e => e.ReqLeadGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdReqEffectiveDateId).HasColumnName("adReqEffectiveDateId");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LeadGrp)
                    .WithMany(p => p.AdReqLeadGroups)
                    .HasForeignKey(d => d.LeadGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adReqLeadGroups_adLeadGroups_LeadGrpId_LeadGrpId");
            });

            modelBuilder.Entity<AdReqs>(entity =>
            {
                entity.HasKey(e => e.AdReqId);

                entity.ToTable("adReqs");

                entity.HasIndex(e => new { e.Code, e.Descrip })
                    .HasName("UIX_adReqs_Code_Descrip")
                    .IsUnique();

                entity.Property(e => e.AdReqId)
                    .HasColumnName("adReqId")
                    .ValueGeneratedNever();

                entity.Property(e => e.AdReqTypeId).HasColumnName("adReqTypeId");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.HasOne(d => d.AdReqType)
                    .WithMany(p => p.AdReqs)
                    .HasForeignKey(d => d.AdReqTypeId)
                    .HasConstraintName("FK_adReqs_adReqTypes_adReqTypeId_adReqTypeId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdReqs)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adReqs_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.EdLvl)
                    .WithMany(p => p.AdReqs)
                    .HasForeignKey(d => d.EdLvlId)
                    .HasConstraintName("FK_adReqs_adEdLvls_EdLvlId_EdLvlId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdReqs)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_adReqs_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdReqsEffectiveDates>(entity =>
            {
                entity.HasKey(e => e.AdReqEffectiveDateId);

                entity.ToTable("adReqsEffectiveDates");

                entity.Property(e => e.AdReqEffectiveDateId)
                    .HasColumnName("adReqEffectiveDateId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdReqId).HasColumnName("adReqId");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.MinScore).HasColumnType("decimal(19, 3)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.AdReq)
                    .WithMany(p => p.AdReqsEffectiveDates)
                    .HasForeignKey(d => d.AdReqId)
                    .HasConstraintName("FK_adReqsEffectiveDates_adReqs_adReqId_adReqId");
            });

            modelBuilder.Entity<AdReqTypes>(entity =>
            {
                entity.HasKey(e => e.AdReqTypeId);

                entity.ToTable("adReqTypes");

                entity.Property(e => e.AdReqTypeId)
                    .HasColumnName("adReqTypeId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdSkills>(entity =>
            {
                entity.HasKey(e => e.SkillId);

                entity.ToTable("adSkills");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdSkills)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSkills_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.AdSkills)
                    .HasForeignKey(d => d.LevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSkills_adLevel_LevelId_LevelId");

                entity.HasOne(d => d.SkillGrp)
                    .WithMany(p => p.AdSkills)
                    .HasForeignKey(d => d.SkillGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSkills_plSkillGroups_SkillGrpId_SkillGrpId");
            });

            modelBuilder.Entity<AdSourceAdvertisement>(entity =>
            {
                entity.HasKey(e => e.SourceAdvId);

                entity.ToTable("adSourceAdvertisement");

                entity.Property(e => e.SourceAdvId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Cost)
                    .HasColumnName("cost")
                    .HasColumnType("decimal(9, 2)");

                entity.Property(e => e.Enddate)
                    .HasColumnName("enddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceAdvCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceAdvDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Startdate)
                    .HasColumnName("startdate")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.AdvInterval)
                    .WithMany(p => p.AdSourceAdvertisement)
                    .HasForeignKey(d => d.AdvIntervalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceAdvertisement_adAdvInterval_AdvIntervalId_AdvIntervalId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdSourceAdvertisement)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adSourceAdvertisement_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.SourceType)
                    .WithMany(p => p.AdSourceAdvertisement)
                    .HasForeignKey(d => d.SourceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceAdvertisement_adSourceType_SourceTypeId_SourceTypeId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdSourceAdvertisement)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceAdvertisement_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdSourceCatagory>(entity =>
            {
                entity.HasKey(e => e.SourceCatagoryId);

                entity.ToTable("adSourceCatagory");

                entity.HasIndex(e => new { e.SourceCatagoryCode, e.SourceCatagoryDescrip })
                    .HasName("UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip")
                    .IsUnique();

                entity.Property(e => e.SourceCatagoryId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceCatagoryCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceCatagoryDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdSourceCatagory)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adSourceCatagory_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdSourceCatagory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceCatagory_syStatuses_StatusID_StatusId");
            });

            modelBuilder.Entity<AdSourceType>(entity =>
            {
                entity.HasKey(e => e.SourceTypeId);

                entity.ToTable("adSourceType");

                entity.HasIndex(e => new { e.SourceTypeCode, e.SourceTypeDescrip })
                    .HasName("UIX_adSourceType_SourceTypeCode_SourceTypeDescrip")
                    .IsUnique();

                entity.Property(e => e.SourceTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceTypeCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdSourceType)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_adSourceType_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.SourceCatagory)
                    .WithMany(p => p.AdSourceType)
                    .HasForeignKey(d => d.SourceCatagoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceType_adSourceCatagory_SourceCatagoryId_SourceCatagoryId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdSourceType)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adSourceType_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdStudentGroups>(entity =>
            {
                entity.HasKey(e => e.StuGrpId);

                entity.ToTable("adStudentGroups");

                entity.Property(e => e.StuGrpId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdStuGrpStudents>(entity =>
            {
                entity.HasKey(e => e.StuGrpStuId);

                entity.ToTable("adStuGrpStudents");

                entity.Property(e => e.StuGrpStuId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.DateAdded).HasColumnType("datetime");

                entity.Property(e => e.DateRemoved).HasColumnType("datetime");

                entity.Property(e => e.IsDeleted).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdStuGrpTypes>(entity =>
            {
                entity.HasKey(e => e.GrpTypeId);

                entity.ToTable("adStuGrpTypes");

                entity.Property(e => e.GrpTypeId)
                    .HasColumnName("GrpTypeID")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdTitles>(entity =>
            {
                entity.HasKey(e => e.TitleId);

                entity.ToTable("adTitles");

                entity.HasIndex(e => new { e.TitleCode, e.TitleDescrip, e.CampGrpId })
                    .HasName("UIX_adTitles_TitleCode_TitleDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.TitleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TitleCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TitleDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.AdTitles)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adTitles_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.JobCat)
                    .WithMany(p => p.AdTitles)
                    .HasForeignKey(d => d.JobCatId)
                    .HasConstraintName("FK_adTitles_plJobCats_JobCatId_JobCatId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.AdTitles)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adTitles_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<AdVehicles>(entity =>
            {
                entity.HasKey(e => e.VehicleId);

                entity.HasIndex(e => new { e.LeadId, e.Position })
                    .HasName("UIX_AdVehicles_LeadId_Position")
                    .IsUnique();

                entity.Property(e => e.Color)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Make)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Permit)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Plate)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AdVehicles)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AdVehicles_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AdVendorCampaign>(entity =>
            {
                entity.HasKey(e => e.CampaignId);

                entity.ToTable("adVendorCampaign");

                entity.HasIndex(e => e.CampaignId)
                    .HasName("UIX_adVendorCampaign_CampaignId")
                    .IsUnique();

                entity.HasIndex(e => new { e.CampaignCode, e.VendorId })
                    .HasName("UIX_adVendorCampaign_CampaignCode_VendorId")
                    .IsUnique();

                entity.Property(e => e.AccountId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CampaignCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCampaignBegin).HasColumnType("datetime");

                entity.Property(e => e.DateCampaignEnd).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.PayFor)
                    .WithMany(p => p.AdVendorCampaign)
                    .HasForeignKey(d => d.PayForId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adVendorCampaign_adVendorPayFor_PayForId_IdPayFor");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.AdVendorCampaign)
                    .HasForeignKey(d => d.VendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_adVendorCampaign_adVendors_VendorId_VendorId");
            });

            modelBuilder.Entity<AdVendorPayFor>(entity =>
            {
                entity.HasKey(e => e.IdPayFor);

                entity.ToTable("adVendorPayFor");

                entity.HasIndex(e => e.IdPayFor)
                    .HasName("UIX_adVendorPayFor_IdPayFor")
                    .IsUnique();

                entity.HasIndex(e => e.PayForCode)
                    .HasName("UIX_adVendorPayFor_PayForCode")
                    .IsUnique();

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PayForCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdVendors>(entity =>
            {
                entity.HasKey(e => e.VendorId);

                entity.ToTable("adVendors");

                entity.HasIndex(e => e.VendorCode)
                    .HasName("UIX_adVendors_VendorCode")
                    .IsUnique();

                entity.Property(e => e.DateOperationBegin).HasColumnType("datetime");

                entity.Property(e => e.DateOperationEnd).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.VendorCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VendorName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdvProgTyps>(entity =>
            {
                entity.HasKey(e => e.AdvProgTypId);

                entity.ToTable("advProgTyps");

                entity.Property(e => e.AdvProgTypId).HasColumnName("advProgTypId");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AfaCatalogMapping>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.AfaCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Table)
                    .WithMany(p => p.AfaCatalogMapping)
                    .HasForeignKey(d => d.TableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AfaCatalogMapping_syTables_TableId_TblId");
            });

            modelBuilder.Entity<AfaLeadSyncException>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ExceptionReason)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.AfaLeadSyncException)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_AfaLeadSyncException_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<AfaPaymentPeriodStaging>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreditOrHoursEarnedEffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TitleIvsapresult)
                    .HasColumnName("TitleIVSAPResult")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.UserCreated)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.StudentEnrollment)
                    .WithMany(p => p.AfaPaymentPeriodStaging)
                    .HasForeignKey(d => d.StudentEnrollmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AfaPaymentPeriodStaging_arStuEnrollments_StudentEnrollmentId_StuEnrollId");
            });

            modelBuilder.Entity<AllNotes>(entity =>
            {
                entity.HasKey(e => e.NotesId);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NoteText)
                    .IsRequired()
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.NoteType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.HasOne(d => d.ModuleCodeNavigation)
                    .WithMany(p => p.AllNotes)
                    .HasPrincipalKey(p => p.ModuleCode)
                    .HasForeignKey(d => d.ModuleCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AllNotes_syModules_ModuleCode_ModuleID");

                entity.HasOne(d => d.PageField)
                    .WithMany(p => p.AllNotes)
                    .HasForeignKey(d => d.PageFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId");
            });

            modelBuilder.Entity<ArAttendTypes>(entity =>
            {
                entity.HasKey(e => e.AttendTypeId);

                entity.ToTable("arAttendTypes");

                entity.Property(e => e.AttendTypeId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GerptAgencyFldValId).HasColumnName("GERptAgencyFldValId");

                entity.Property(e => e.Gesequence).HasColumnName("GESequence");

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArAttUnitType>(entity =>
            {
                entity.HasKey(e => e.UnitTypeId);

                entity.ToTable("arAttUnitType");

                entity.Property(e => e.UnitTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.UnitTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArBkCategories>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("arBkCategories");

                entity.Property(e => e.CategoryId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CategoryCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArBkCategories)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBkCategories_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArBkCategories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBkCategories_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArBooks>(entity =>
            {
                entity.HasKey(e => e.BkId);

                entity.ToTable("arBooks");

                entity.Property(e => e.BkId).ValueGeneratedNever();

                entity.Property(e => e.BkAuthor)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BkTitle)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isbn)
                    .IsRequired()
                    .HasColumnName("ISBN")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArBooks)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBooks_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.ArBooks)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBooks_arBkCategories_CategoryId_CategoryId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArBooks)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBooks_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArBoolean>(entity =>
            {
                entity.HasKey(e => e.BoolValue);

                entity.ToTable("arBoolean");

                entity.Property(e => e.BoolValue).HasColumnName("boolValue");

                entity.Property(e => e.BoolDescrip)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArBridgeGradeComponentTypesCourses>(entity =>
            {
                entity.HasKey(e => e.BridgeGradeComponentTypesCoursesId);

                entity.ToTable("arBridge_GradeComponentTypes_Courses");

                entity.Property(e => e.BridgeGradeComponentTypesCoursesId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.GrdComponentTypeIdReqId).HasColumnName("GrdComponentTypeId_ReqId");

                entity.HasOne(d => d.GrdComponentType)
                    .WithMany(p => p.ArBridgeGradeComponentTypesCourses)
                    .HasForeignKey(d => d.GrdComponentTypeId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_arBridge_GradeComponentTypes_Courses_arGrdComponentTypes_GrdComponentTypeId_GrdComponentTypeId");
            });

            modelBuilder.Entity<ArBuildings>(entity =>
            {
                entity.HasKey(e => e.BldgId);

                entity.ToTable("arBuildings");

                entity.HasIndex(e => new { e.BldgCode, e.BldgDescrip, e.CampGrpId })
                    .HasName("UIX_arBuildings_BldgCode_BldgDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.BldgId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BldgClose)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.BldgCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.BldgComments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.BldgDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BldgEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BldgName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BldgOpen)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.BldgTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Zip)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArBuildings)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBuildings_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.ArBuildings)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBuildings_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.ArBuildings)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_arBuildings_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArBuildings)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arBuildings_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArCampusPrgVersions>(entity =>
            {
                entity.HasKey(e => e.CampusPrgVerId);

                entity.ToTable("arCampusPrgVersions");

                entity.Property(e => e.CampusPrgVerId).ValueGeneratedNever();

                entity.Property(e => e.AllowExcusAbsPerPayPrd).HasColumnType("decimal(18, 1)");

                entity.Property(e => e.IsFameapproved).HasColumnName("IsFAMEApproved");

                entity.Property(e => e.IsTitleIv).HasColumnName("IsTitleIV");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArCampusPrgVersions)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCampusPrgVersions_arPrgVersions_PrgVerId_PrgVerId");
            });

            modelBuilder.Entity<ArCertifications>(entity =>
            {
                entity.HasKey(e => e.CertId);

                entity.ToTable("arCertifications");

                entity.Property(e => e.CertId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CertCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CertDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArCertifications)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCertifications_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArCertifications)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCertifications_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArchiveArGrdBkResults>(entity =>
            {
                entity.HasKey(e => e.IdArchive);

                entity.ToTable("_archive_arGrdBkResults");

                entity.Property(e => e.ArchiveDate)
                    .HasColumnName("Archive_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ArchiveUser)
                    .HasColumnName("Archive_User")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostDate).HasColumnType("datetime");

                entity.Property(e => e.Score).HasColumnType("decimal(6, 2)");
            });

            modelBuilder.Entity<ArchiveArResults>(entity =>
            {
                entity.HasKey(e => e.IdArchive);

                entity.ToTable("_archive_arResults");

                entity.Property(e => e.ArchiveDate)
                    .HasColumnName("Archive_Date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ArchiveUser)
                    .HasColumnName("Archive_User")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateDetermined).HasColumnType("datetime");

                entity.Property(e => e.GradeOverriddenDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArClassSections>(entity =>
            {
                entity.HasKey(e => e.ClsSectionId);

                entity.ToTable("arClassSections");

                entity.HasIndex(e => new { e.ClsSectionId, e.ReqId });

                entity.HasIndex(e => new { e.ClsSectionId, e.TermId, e.ReqId, e.GrdScaleId });

                entity.HasIndex(e => new { e.ReqId, e.TermId, e.ClsSectionId, e.InstrGrdBkWgtId, e.GrdScaleId });

                entity.Property(e => e.ClsSectionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ClassRoom)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClsSection)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CohortStartDate).HasColumnType("datetime");

                entity.Property(e => e.CourseId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Session)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.StudentStartDate).HasColumnType("datetime");

                entity.Property(e => e.TermGuid)
                    .HasColumnName("TermGUID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_arClassSections_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.GrdScale)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.GrdScaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClassSections_arGradeScales_GrdScaleId_GrdScaleId");

                entity.HasOne(d => d.InstrGrdBkWgt)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.InstrGrdBkWgtId)
                    .HasConstraintName("FK_arClassSections_arGrdBkWeights_InstrGrdBkWgtId_InstrGrdBkWgtId");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK_arClassSections_syUsers_InstructorId_UserId");

                entity.HasOne(d => d.LeadGrp)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.LeadGrpId)
                    .HasConstraintName("FK_arClassSections_adLeadGroups_LeadGrpId_LeadGrpId");

                entity.HasOne(d => d.ProgramVersionDefinition)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.ProgramVersionDefinitionId)
                    .HasConstraintName("FK_arClassSections_arProgVerDef_ProgramVersionDefinitionId_ProgVerDefId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClassSections_arReqs_ReqId_ReqId");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("FK_arClassSections_arShifts_shiftid_ShiftId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.ArClassSections)
                    .HasForeignKey(d => d.TermId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClassSections_arTerm_TermId_TermId");
            });

            modelBuilder.Entity<ArClassSectionTerms>(entity =>
            {
                entity.HasKey(e => e.ClsSectTermId);

                entity.ToTable("arClassSectionTerms");

                entity.Property(e => e.ClsSectTermId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArClassSectionTerms)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClassSectionTerms_arClassSections_ClsSectionId_ClsSectionId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.ArClassSectionTerms)
                    .HasForeignKey(d => d.TermId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClassSectionTerms_arTerm_TermId_TermId");
            });

            modelBuilder.Entity<ArClsSectionTimeClockPolicy>(entity =>
            {
                entity.HasKey(e => e.ClsSectionPolicyId);

                entity.ToTable("arClsSectionTimeClockPolicy");

                entity.Property(e => e.ClsSectionPolicyId).ValueGeneratedNever();

                entity.Property(e => e.AssignTardyInTime).HasColumnType("datetime");

                entity.Property(e => e.MaxInBeforeTardy).HasColumnType("datetime");

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArClsSectionTimeClockPolicy)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClsSectionTimeClockPolicy_arClassSections_ClsSectionId_ClsSectionId");
            });

            modelBuilder.Entity<ArClsSectMeetings>(entity =>
            {
                entity.HasKey(e => e.ClsSectMeetingId);

                entity.ToTable("arClsSectMeetings");

                entity.HasIndex(e => new { e.ClsSectionId, e.StartDate, e.EndDate, e.PeriodId });

                entity.Property(e => e.ClsSectMeetingId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.InstructionTypeId).HasColumnName("InstructionTypeID");

                entity.Property(e => e.IsMeetingRescheduled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArClsSectMeetings)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClsSectMeetings_arClassSections_ClsSectionId_ClsSectionId");

                entity.HasOne(d => d.EndInterval)
                    .WithMany(p => p.ArClsSectMeetingsEndInterval)
                    .HasForeignKey(d => d.EndIntervalId)
                    .HasConstraintName("FK_arClsSectMeetings_cmTimeInterval_EndIntervalId_TimeIntervalId");

                entity.HasOne(d => d.InstructionType)
                    .WithMany(p => p.ArClsSectMeetings)
                    .HasForeignKey(d => d.InstructionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClsSectMeetings_arInstructionType_InstructionTypeID_InstructionTypeId");

                entity.HasOne(d => d.Room)
                    .WithMany(p => p.ArClsSectMeetings)
                    .HasForeignKey(d => d.RoomId)
                    .HasConstraintName("FK_arClsSectMeetings_arRooms_RoomId_RoomId");

                entity.HasOne(d => d.TimeInterval)
                    .WithMany(p => p.ArClsSectMeetingsTimeInterval)
                    .HasForeignKey(d => d.TimeIntervalId)
                    .HasConstraintName("FK_arClsSectMeetings_cmTimeInterval_TimeIntervalId_TimeIntervalId");

                entity.HasOne(d => d.WorkDays)
                    .WithMany(p => p.ArClsSectMeetings)
                    .HasForeignKey(d => d.WorkDaysId)
                    .HasConstraintName("FK_arClsSectMeetings_plWorkDays_WorkDaysId_WorkDaysId");
            });

            modelBuilder.Entity<ArClsSectStudents>(entity =>
            {
                entity.HasKey(e => e.ClsSectStudentId);

                entity.ToTable("arClsSectStudents");

                entity.Property(e => e.ClsSectStudentId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Grade)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArClsSectStudents)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arClsSectStudents_arClassSections_ClsSectionId_ClsSectionId");
            });

            modelBuilder.Entity<ArCollegeDivisions>(entity =>
            {
                entity.HasKey(e => e.CollegeDivId);

                entity.ToTable("arCollegeDivisions");

                entity.HasIndex(e => new { e.CollegeDivCode, e.CollegeDivDescrip })
                    .HasName("UIX_arCollegeDivisions_CollegeDivCode_CollegeDivDescrip")
                    .IsUnique();

                entity.Property(e => e.CollegeDivId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CollegeDivCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CollegeDivDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArCollegeDivisions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCollegeDivisions_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArConsequenceTyps>(entity =>
            {
                entity.HasKey(e => e.ConsequenceTypId);

                entity.ToTable("arConsequenceTyps");

                entity.Property(e => e.ConseqTypDesc)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArCourseBks>(entity =>
            {
                entity.HasKey(e => e.CourseBkId);

                entity.ToTable("arCourseBks");

                entity.Property(e => e.CourseBkId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Bk)
                    .WithMany(p => p.ArCourseBks)
                    .HasForeignKey(d => d.BkId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCourseBks_arBooks_BkId_BkId");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.ArCourseBks)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCourseBks_arReqs_CourseId_ReqId");
            });

            modelBuilder.Entity<ArCourseCategories>(entity =>
            {
                entity.HasKey(e => e.CourseCategoryId);

                entity.ToTable("arCourseCategories");

                entity.Property(e => e.CourseCategoryId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArCourseCategories)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCourseCategories_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArCourseCategories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCourseCategories_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArCourseEquivalent>(entity =>
            {
                entity.HasKey(e => e.ReqEquivalentId);

                entity.ToTable("arCourseEquivalent");

                entity.Property(e => e.ReqEquivalentId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EquivReq)
                    .WithMany(p => p.ArCourseEquivalentEquivReq)
                    .HasForeignKey(d => d.EquivReqId)
                    .HasConstraintName("FK_arCourseEquivalent_arReqs_EquivReqId_ReqId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArCourseEquivalentReq)
                    .HasForeignKey(d => d.ReqId)
                    .HasConstraintName("FK_arCourseEquivalent_arReqs_ReqId_ReqId");
            });

            modelBuilder.Entity<ArCourseReqs>(entity =>
            {
                entity.HasKey(e => e.CourseReqId);

                entity.ToTable("arCourseReqs");

                entity.Property(e => e.CourseReqId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PreCoReq)
                    .WithMany(p => p.ArCourseReqsPreCoReq)
                    .HasForeignKey(d => d.PreCoReqId)
                    .HasConstraintName("FK_arCourseReqs_arReqs_PreCoReqId_ReqId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArCourseReqs)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_arCourseReqs_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArCourseReqsReq)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arCourseReqs_arReqs_ReqId_ReqId");
            });

            modelBuilder.Entity<ArCreditsPerService>(entity =>
            {
                entity.HasKey(e => e.CreditPerServiceId);

                entity.ToTable("arCreditsPerService");

                entity.Property(e => e.CreditPerServiceId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfCredits).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.StartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ArDegrees>(entity =>
            {
                entity.HasKey(e => e.DegreeId);

                entity.ToTable("arDegrees");

                entity.HasIndex(e => new { e.DegreeCode, e.DegreeDescrip, e.CampGrpId })
                    .HasName("UIX_arDegrees_DegreeCode_DegreeDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.DegreeId).ValueGeneratedNever();

                entity.Property(e => e.DegreeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DegreeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArDegrees)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDegrees_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArDegrees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDegrees_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArDepartments>(entity =>
            {
                entity.HasKey(e => e.DeptId);

                entity.ToTable("arDepartments");

                entity.HasIndex(e => new { e.DeptCode, e.DeptDescrip })
                    .HasName("UIX_arDepartments_DeptCode_DeptDescrip")
                    .IsUnique();

                entity.Property(e => e.DeptId).ValueGeneratedNever();

                entity.Property(e => e.DeptCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DeptDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CollegeDiv)
                    .WithMany(p => p.ArDepartments)
                    .HasForeignKey(d => d.CollegeDivId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDepartments_arCollegeDivisions_CollegeDivId_CollegeDivId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArDepartments)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDepartments_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArDropReasons>(entity =>
            {
                entity.HasKey(e => e.DropReasonId);

                entity.ToTable("arDropReasons");

                entity.HasIndex(e => new { e.Code, e.Descrip, e.StatusId })
                    .HasName("UIX_arDropReasons_Code_Descrip_StatusId")
                    .IsUnique();

                entity.Property(e => e.DropReasonId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArDropReasons)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDropReasons_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArDropReasons)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arDropReasons_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArExternshipAttendance>(entity =>
            {
                entity.HasKey(e => e.ExternshipAttendanceId);

                entity.ToTable("arExternshipAttendance");

                entity.HasIndex(e => new { e.HoursAttended, e.StuEnrollId })
                    .HasName("IX_arExternshipAttendance_StuEnrollId_HoursAttended");

                entity.Property(e => e.ExternshipAttendanceId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AttendedDate).HasColumnType("datetime");

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HoursAttended).HasColumnType("decimal(6, 2)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdComponentType)
                    .WithMany(p => p.ArExternshipAttendance)
                    .HasForeignKey(d => d.GrdComponentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arExternshipAttendance_arGrdComponentTypes_GrdComponentTypeId_GrdComponentTypeId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArExternshipAttendance)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arExternshipAttendance_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArFasapchkResults>(entity =>
            {
                entity.HasKey(e => e.StdRecKey)
                    .HasAnnotation("SqlServer:Clustered", false);

                entity.ToTable("arFASAPChkResults");

                entity.Property(e => e.StdRecKey).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attendance).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.CheckPointDate).HasColumnType("datetime");

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.CumFinAidCredits).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.DatePerformed).HasColumnType("datetime");

                entity.Property(e => e.Gpa).HasColumnName("GPA");

                entity.Property(e => e.IsMakingSap).HasColumnName("IsMakingSAP");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PercentCompleted).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.SapdetailId)
                    .HasColumnName("SAPDetailId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.StatusId).HasDefaultValueSql("('F23DE1E2-D90A-4720-B4C7-0F6FB09C9965')");

                entity.Property(e => e.TermStartDate).HasColumnType("datetime");

                entity.Property(e => e.TitleIvstatusId)
                    .HasColumnName("TitleIVStatusId")
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Sapdetail)
                    .WithMany(p => p.ArFasapchkResults)
                    .HasForeignKey(d => d.SapdetailId)
                    .HasConstraintName("FK_arFASAPChkResults_arSAPDetails_SAPDetailId_SAPDetailId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArFasapchkResults)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFASAPChkResults_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArFasapchkResults)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFASAPChkResults_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.TitleIvstatus)
                    .WithMany(p => p.ArFasapchkResults)
                    .HasForeignKey(d => d.TitleIvstatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFASAPChkResults_syTitleIVSapStatus_TitleIVStatusId_Id");
            });

            modelBuilder.Entity<ArFerpacategory>(entity =>
            {
                entity.HasKey(e => e.FerpacategoryId);

                entity.ToTable("arFERPACategory");

                entity.Property(e => e.FerpacategoryId)
                    .HasColumnName("FERPACategoryId")
                    .ValueGeneratedNever();

                entity.Property(e => e.FerpacategoryCode)
                    .IsRequired()
                    .HasColumnName("FERPACategoryCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.FerpacategoryDescrip)
                    .IsRequired()
                    .HasColumnName("FERPACategoryDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArFerpacategory)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arFERPACategory_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArFerpacategory)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPACategory_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArFerpaentity>(entity =>
            {
                entity.HasKey(e => e.FerpaentityId);

                entity.ToTable("arFERPAEntity");

                entity.Property(e => e.FerpaentityId)
                    .HasColumnName("FERPAEntityId")
                    .ValueGeneratedNever();

                entity.Property(e => e.FerpaentityCode)
                    .IsRequired()
                    .HasColumnName("FERPAEntityCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.FerpaentityDescrip)
                    .IsRequired()
                    .HasColumnName("FERPAEntityDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArFerpaentity)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arFERPAEntity_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArFerpaentity)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPAEntity_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArFerpapage>(entity =>
            {
                entity.HasKey(e => e.FerpapageId);

                entity.ToTable("arFERPAPage");

                entity.Property(e => e.FerpapageId)
                    .HasColumnName("FERPAPageId")
                    .ValueGeneratedNever();

                entity.Property(e => e.FerpacategoryId).HasColumnName("FERPACategoryId");

                entity.HasOne(d => d.Ferpacategory)
                    .WithMany(p => p.ArFerpapage)
                    .HasForeignKey(d => d.FerpacategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPAPage_arFERPACategory_FERPACategoryId_FERPACategoryId");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.ArFerpapage)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPAPage_syResources_ResourceId_ResourceID");
            });

            modelBuilder.Entity<ArFerpapolicy>(entity =>
            {
                entity.HasKey(e => e.FerpapolicyId);

                entity.ToTable("arFERPAPolicy");

                entity.Property(e => e.FerpapolicyId)
                    .HasColumnName("FERPAPolicyId")
                    .ValueGeneratedNever();

                entity.Property(e => e.FerpacategoryId).HasColumnName("FERPACategoryId");

                entity.Property(e => e.FerpaentityId).HasColumnName("FERPAEntityId");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Ferpacategory)
                    .WithMany(p => p.ArFerpapolicy)
                    .HasForeignKey(d => d.FerpacategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPAPolicy_arFERPACategory_FERPACategoryId_FERPACategoryId");

                entity.HasOne(d => d.Ferpaentity)
                    .WithMany(p => p.ArFerpapolicy)
                    .HasForeignKey(d => d.FerpaentityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arFERPAPolicy_arFERPAEntity_FERPAEntityId_FERPAEntityId");
            });

            modelBuilder.Entity<ArGradeScaleDetails>(entity =>
            {
                entity.HasKey(e => e.GrdScaleDetailId);

                entity.ToTable("arGradeScaleDetails");

                entity.Property(e => e.GrdScaleDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdScale)
                    .WithMany(p => p.ArGradeScaleDetails)
                    .HasForeignKey(d => d.GrdScaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeScaleDetails_arGradeScales_GrdScaleId_GrdScaleId");

                entity.HasOne(d => d.GrdSysDetail)
                    .WithMany(p => p.ArGradeScaleDetails)
                    .HasForeignKey(d => d.GrdSysDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeScaleDetails_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId");
            });

            modelBuilder.Entity<ArGradeScales>(entity =>
            {
                entity.HasKey(e => e.GrdScaleId);

                entity.ToTable("arGradeScales");

                entity.HasIndex(e => new { e.Descrip, e.CampGrpId })
                    .HasName("UIX_arGradeScales_Descrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.GrdScaleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArGradeScales)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeScales_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.GrdSystem)
                    .WithMany(p => p.ArGradeScales)
                    .HasForeignKey(d => d.GrdSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeScales_arGradeSystems_GrdSystemId_GrdSystemId");

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.ArGradeScales)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK_arGradeScales_syUsers_InstructorId_UserId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArGradeScales)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeScales_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArGradeSystemDetails>(entity =>
            {
                entity.HasKey(e => e.GrdSysDetailId);

                entity.ToTable("arGradeSystemDetails");

                entity.Property(e => e.GrdSysDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Gpa).HasColumnName("GPA");

                entity.Property(e => e.Grade)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GradeDescription)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsInGpa).HasColumnName("IsInGPA");

                entity.Property(e => e.IsInSap).HasColumnName("IsInSAP");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Quality)
                    .HasColumnName("quality")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdSystem)
                    .WithMany(p => p.ArGradeSystemDetails)
                    .HasForeignKey(d => d.GrdSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeSystemDetails_arGradeSystems_GrdSystemId_GrdSystemId");
            });

            modelBuilder.Entity<ArGradeSystems>(entity =>
            {
                entity.HasKey(e => e.GrdSystemId);

                entity.ToTable("arGradeSystems");

                entity.HasIndex(e => new { e.Descrip, e.CampGrpId })
                    .HasName("UIX_arGradeSystems_Descrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.GrdSystemId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArGradeSystems)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeSystems_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArGradeSystems)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGradeSystems_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArGrdBkConversionResults>(entity =>
            {
                entity.HasKey(e => e.ConversionResultId);

                entity.ToTable("arGrdBkConversionResults");

                entity.HasIndex(e => new { e.MinResult, e.Score, e.StuEnrollId, e.ReqId, e.TermId, e.GrdComponentTypeId, e.ConversionResultId })
                    .HasName("IX_arGrdBkConversionResults_StuEnrollId_ReqId_TermId_GrdComponentTypeId_ConversionResultId_MinResult_Score");

                entity.Property(e => e.ConversionResultId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comments)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsCourseCredited)
                    .HasColumnName("isCourseCredited")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostDate).HasColumnType("smalldatetime");

                entity.Property(e => e.PostUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ConversionResult)
                    .WithOne(p => p.InverseConversionResult)
                    .HasForeignKey<ArGrdBkConversionResults>(d => d.ConversionResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkConversionResults_arGrdBkConversionResults_ConversionResultId_ConversionResultId");
            });

            modelBuilder.Entity<ArGrdBkEvalTyps>(entity =>
            {
                entity.HasKey(e => e.GrdBkEvalTypId);

                entity.ToTable("arGrdBkEvalTyps");

                entity.Property(e => e.GrdBkEvalTypId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.GrdBkEvalTypCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.GrdBkEvalTypDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArGrdBkEvalTyps)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkEvalTyps_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArGrdBkEvalTyps)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkEvalTyps_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArGrdBkResults>(entity =>
            {
                entity.HasKey(e => e.GrdBkResultId);

                entity.ToTable("arGrdBkResults");

                entity.HasIndex(e => new { e.ClsSectionId, e.InstrGrdBkWgtDetailId });

                entity.HasIndex(e => new { e.StuEnrollId, e.ClsSectionId });

                entity.HasIndex(e => new { e.ClsSectionId, e.InstrGrdBkWgtDetailId, e.Score })
                    .HasName("IX_arGrdBkResults_Score_ClsSectionId_InstrGrdBkWgtDetailId");

                entity.HasIndex(e => new { e.Score, e.ClsSectionId, e.GrdBkResultId, e.InstrGrdBkWgtDetailId, e.StuEnrollId })
                    .HasName("IX_arGrdBkResults_ClsSectionId_GrdBkResultId_InstrGrdBkWgtDetailId_StuEnrollId_Score");

                entity.HasIndex(e => new { e.StuEnrollId, e.InstrGrdBkWgtDetailId, e.ClsSectionId, e.ResNum, e.PostDate })
                    .HasName("UIX_arGrdBkResults_StuEnrollId_InstrGrdBkWgtDetailId_ClsSectionId_ResNum_PostDate")
                    .IsUnique();

                entity.Property(e => e.GrdBkResultId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comments)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateCompleted).HasColumnType("date");

                entity.Property(e => e.IsCompGraded).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsCourseCredited)
                    .HasColumnName("isCourseCredited")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Score).HasColumnType("decimal(6, 2)");

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArGrdBkResults)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkResults_arClassSections_ClsSectionId_ClsSectionId");

                entity.HasOne(d => d.InstrGrdBkWgtDetail)
                    .WithMany(p => p.ArGrdBkResults)
                    .HasForeignKey(d => d.InstrGrdBkWgtDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkResults_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtDetailId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArGrdBkResults)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkResults_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArGrdBkWeights>(entity =>
            {
                entity.HasKey(e => e.InstrGrdBkWgtId);

                entity.ToTable("arGrdBkWeights");

                entity.Property(e => e.InstrGrdBkWgtId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Instructor)
                    .WithMany(p => p.ArGrdBkWeights)
                    .HasForeignKey(d => d.InstructorId)
                    .HasConstraintName("FK_arGrdBkWeights_syUsers_InstructorId_UserId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArGrdBkWeights)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkWeights_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArGrdBkWgtDetails>(entity =>
            {
                entity.HasKey(e => e.InstrGrdBkWgtDetailId);

                entity.ToTable("arGrdBkWgtDetails");

                entity.HasIndex(e => e.GrdComponentTypeId);

                entity.HasIndex(e => new { e.Number, e.InstrGrdBkWgtId, e.InstrGrdBkWgtDetailId, e.GrdComponentTypeId })
                    .HasName("IX_arGrdBkWgtDetails_InstrGrdBkWgtId_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number");

                entity.HasIndex(e => new { e.InstrGrdBkWgtDetailId, e.Number, e.GrdComponentTypeId, e.Required, e.MustPass })
                    .HasName("IX_arGrdBkWgtDetails_GrdComponentTypeId_Required_MustPass_InstrGrdBkWgtDetailId_Number");

                entity.Property(e => e.InstrGrdBkWgtDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MustPass).HasDefaultValueSql("((0))");

                entity.Property(e => e.Required).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.GrdComponentType)
                    .WithMany(p => p.ArGrdBkWgtDetails)
                    .HasForeignKey(d => d.GrdComponentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkWgtDetails_arGrdComponentTypes_GrdComponentTypeId_GrdComponentTypeId");

                entity.HasOne(d => d.GrdPolicy)
                    .WithMany(p => p.ArGrdBkWgtDetails)
                    .HasForeignKey(d => d.GrdPolicyId)
                    .HasConstraintName("FK_arGrdBkWgtDetails_syGrdPolicies_GrdPolicyId_GrdPolicyId");

                entity.HasOne(d => d.InstrGrdBkWgt)
                    .WithMany(p => p.ArGrdBkWgtDetails)
                    .HasForeignKey(d => d.InstrGrdBkWgtId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdBkWgtDetails_arGrdBkWeights_InstrGrdBkWgtId_InstrGrdBkWgtId");
            });

            modelBuilder.Entity<ArGrdComponentTypes>(entity =>
            {
                entity.HasKey(e => e.GrdComponentTypeId);

                entity.ToTable("arGrdComponentTypes");

                entity.HasIndex(e => new { e.GrdComponentTypeId, e.SysComponentTypeId });

                entity.HasIndex(e => new { e.SysComponentTypeId, e.GrdComponentTypeId, e.Descrip });

                entity.Property(e => e.GrdComponentTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArGrdComponentTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdComponentTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArGrdComponentTypes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arGrdComponentTypes_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.SysComponentType)
                    .WithMany(p => p.ArGrdComponentTypes)
                    .HasForeignKey(d => d.SysComponentTypeId)
                    .HasConstraintName("FK_arGrdComponentTypes_syResources_SysComponentTypeId_ResourceID");
            });

            modelBuilder.Entity<ArGrdTyps>(entity =>
            {
                entity.HasKey(e => e.GrdTypId);

                entity.ToTable("arGrdTyps");

                entity.Property(e => e.GrdTypDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArHousing>(entity =>
            {
                entity.HasKey(e => e.HousingId);

                entity.ToTable("arHousing");

                entity.Property(e => e.HousingId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArInstructionType>(entity =>
            {
                entity.HasKey(e => e.InstructionTypeId);

                entity.ToTable("arInstructionType");

                entity.HasIndex(e => e.InstructionTypeCode)
                    .HasName("UIX_arInstructionType_InstructionTypeCode")
                    .IsUnique();

                entity.Property(e => e.InstructionTypeId).ValueGeneratedNever();

                entity.Property(e => e.InstructionTypeCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InstructionTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArInstructionType)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arInstructionType_syCampGrps_CampGrpId_CampGrpId");
            });

            modelBuilder.Entity<ArInstructorsSupervisors>(entity =>
            {
                entity.HasKey(e => e.InstructorId);

                entity.ToTable("arInstructorsSupervisors");

                entity.Property(e => e.InstructorId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser).HasMaxLength(50);

                entity.HasOne(d => d.Instructor)
                    .WithOne(p => p.ArInstructorsSupervisorsInstructor)
                    .HasForeignKey<ArInstructorsSupervisors>(d => d.InstructorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arInstructorsSupervisors_syUsers_InstructorId_UserId");

                entity.HasOne(d => d.Supervisor)
                    .WithMany(p => p.ArInstructorsSupervisorsSupervisor)
                    .HasForeignKey(d => d.SupervisorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arInstructorsSupervisors_syUsers_SupervisorId_UserId");
            });

            modelBuilder.Entity<ArLoareasons>(entity =>
            {
                entity.HasKey(e => e.LoareasonId);

                entity.ToTable("arLOAReasons");

                entity.HasIndex(e => new { e.Code, e.Descrip, e.StatusId })
                    .HasName("UIX_arLOAReasons_Code_Descrip_StatusId")
                    .IsUnique();

                entity.Property(e => e.LoareasonId)
                    .HasColumnName("LOAReasonId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArLoareasons)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arLOAReasons_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArLoareasons)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_arLOAReasons_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArMentorGradeComponentTypesCourses>(entity =>
            {
                entity.HasKey(e => e.MentorProctoredId);

                entity.ToTable("arMentor_GradeComponentTypes_Courses");

                entity.Property(e => e.MentorProctoredId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.MentorOperator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MentorRequirement)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArOverridenConflicts>(entity =>
            {
                entity.HasKey(e => e.OverridenConflictId);

                entity.ToTable("arOverridenConflicts");

                entity.Property(e => e.OverridenConflictId).ValueGeneratedNever();

                entity.Property(e => e.LeftClsSectionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RightClsSectionId).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.LeftClsSection)
                    .WithMany(p => p.ArOverridenConflictsLeftClsSection)
                    .HasForeignKey(d => d.LeftClsSectionId)
                    .HasConstraintName("FK_arOverridenConflicts_arClassSections_LeftClsSectionId_ClsSectionId");

                entity.HasOne(d => d.RightClsSection)
                    .WithMany(p => p.ArOverridenConflictsRightClsSection)
                    .HasForeignKey(d => d.RightClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arOverridenConflicts_arClassSections_RightClsSectionId_ClsSectionId");
            });

            modelBuilder.Entity<ArOverridenPreReqs>(entity =>
            {
                entity.HasKey(e => e.OverridenPreReqId);

                entity.ToTable("arOverridenPreReqs");

                entity.HasIndex(e => new { e.StuEnrollId, e.ReqId })
                    .HasName("UIX_arOverridenPreReqs_StuEnrollId_ReqId")
                    .IsUnique();

                entity.Property(e => e.OverridenPreReqId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArOverridenPreReqs)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arOverridenPreReqs_arReqs_ReqId_ReqId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArOverridenPreReqs)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arOverridenPreReqs_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArPeriod>(entity =>
            {
                entity.HasKey(e => e.PrdId);

                entity.ToTable("arPeriod");

                entity.Property(e => e.PrdId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrdCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PrdDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArPeriod)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPeriod_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TimeInterval)
                    .WithMany(p => p.ArPeriod)
                    .HasForeignKey(d => d.TimeIntervalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPeriod_cmTimeInterval_TimeIntervalId_TimeIntervalId");
            });

            modelBuilder.Entity<ArPostScoreDictationSpeedTest>(entity =>
            {
                entity.ToTable("arPostScoreDictationSpeedTest");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Accuracy)
                    .HasColumnName("accuracy")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Datepassed)
                    .HasColumnName("datepassed")
                    .HasColumnType("datetime");

                entity.Property(e => e.Grdcomponenttypeid).HasColumnName("grdcomponenttypeid");

                entity.Property(e => e.Istestmentorproctored)
                    .HasColumnName("istestmentorproctored")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Speed)
                    .HasColumnName("speed")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Stuenrollid).HasColumnName("stuenrollid");

                entity.Property(e => e.Termid).HasColumnName("termid");
            });

            modelBuilder.Entity<ArPrgChargePeriodSeq>(entity =>
            {
                entity.HasKey(e => e.PrgChrPeriodSeqId);

                entity.ToTable("arPrgChargePeriodSeq");

                entity.Property(e => e.PrgChrPeriodSeqId).ValueGeneratedNever();

                entity.Property(e => e.Endvalue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.FeeLevelId).HasColumnName("FeeLevelID");

                entity.Property(e => e.StartValue).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.FeeLevel)
                    .WithMany(p => p.ArPrgChargePeriodSeq)
                    .HasForeignKey(d => d.FeeLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgChargePeriodSeq_saFeeLevels_FeeLevelID_FeeLevelId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArPrgChargePeriodSeq)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_arPrgChargePeriodSeq_arPrgVersions_PrgVerId_PrgVerId");
            });

            modelBuilder.Entity<ArPrgGrp>(entity =>
            {
                entity.HasKey(e => e.PrgGrpId);

                entity.ToTable("arPrgGrp");

                entity.HasIndex(e => new { e.PrgGrpCode, e.PrgGrpDescrip })
                    .HasName("UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip")
                    .IsUnique();

                entity.Property(e => e.PrgGrpId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrgGrpCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PrgGrpDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArPrgGrp)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArPrgGrp)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_arPrgGrp_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArPrgMinorCerts>(entity =>
            {
                entity.HasKey(e => e.PrgMinorCertId);

                entity.ToTable("arPrgMinorCerts");

                entity.Property(e => e.PrgMinorCertId).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Child)
                    .WithMany(p => p.ArPrgMinorCertsChild)
                    .HasForeignKey(d => d.ChildId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgMinorCerts_arPrgVersions_ChildId_PrgVerId");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.ArPrgMinorCertsParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgMinorCerts_arPrgVersions_ParentId_PrgVerId");
            });

            modelBuilder.Entity<ArPrgVerInstructionType>(entity =>
            {
                entity.HasKey(e => e.PrgVerInstructionTypeId);

                entity.ToTable("arPrgVerInstructionType");

                entity.Property(e => e.PrgVerInstructionTypeId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Hours).HasColumnType("decimal(6, 0)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.InstructionType)
                    .WithMany(p => p.ArPrgVerInstructionType)
                    .HasForeignKey(d => d.InstructionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVerInstructionType_arInstructionType_InstructionTypeId_InstructionTypeId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArPrgVerInstructionType)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_arPrgVerInstructionType_arPrgVersions_PrgVerId_PrgVerId");
            });

            modelBuilder.Entity<ArPrgVersionFees>(entity =>
            {
                entity.HasKey(e => e.PrgVersionFeeId);

                entity.ToTable("arPrgVersionFees");

                entity.Property(e => e.PrgVersionFeeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrgVersionFeeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PrgVersionFeeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arPrgVersionFees_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_arPrgVersionFees_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.RateSchedule)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.RateScheduleId)
                    .HasConstraintName("FK_arPrgVersionFees_saRateSchedules_RateScheduleId_RateScheduleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersionFees_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.TransCodeId)
                    .HasConstraintName("FK_arPrgVersionFees_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.ArPrgVersionFees)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_arPrgVersionFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");
            });

            modelBuilder.Entity<ArPrgVersions>(entity =>
            {
                entity.HasKey(e => e.PrgVerId);

                entity.ToTable("arPrgVersions");

                entity.Property(e => e.PrgVerId).ValueGeneratedNever();

                entity.Property(e => e.AcademicYearLength).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.Credits).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.DoCourseWeightOverallGpa).HasColumnName("DoCourseWeightOverallGPA");

                entity.Property(e => e.Fasapid).HasColumnName("FASAPId");

                entity.Property(e => e.Hours).HasColumnType("decimal(9, 2)");

                entity.Property(e => e.IsContinuingEd).HasDefaultValueSql("((0))");

                entity.Property(e => e.LthalfTime).HasColumnName("LTHalfTime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrgVerCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PrgVerDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProgramRegistrationType).HasDefaultValueSql("((0))");

                entity.Property(e => e.Sapid).HasColumnName("SAPId");

                entity.Property(e => e.SchedMethodId).HasDefaultValueSql("((4))");

                entity.Property(e => e.TotalCost)
                    .HasColumnType("decimal(9, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UnitTypeId).HasDefaultValueSql("('{00000000-0000-0000-0000-000000000000}')");

                entity.Property(e => e.WeightedGpa).HasColumnName("Weighted GPA");

                entity.HasOne(d => d.BillingMethod)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.BillingMethodId)
                    .HasConstraintName("FK_arPrgVersions_saBillingMethods_BillingMethodId_BillingMethodId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arPrgVersions_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Degree)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.DegreeId)
                    .HasConstraintName("FK_arPrgVersions_arDegrees_DegreeId_DegreeId");

                entity.HasOne(d => d.Dept)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.DeptId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_arDepartments_DeptId_DeptId");

                entity.HasOne(d => d.Fasap)
                    .WithMany(p => p.ArPrgVersionsFasap)
                    .HasForeignKey(d => d.Fasapid)
                    .HasConstraintName("FK_arPrgVersions_arSap_FASAPId_SAPId");

                entity.HasOne(d => d.GrdSystem)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.GrdSystemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_arGradeSystems_GrdSystemId_GrdSystemId");

                entity.HasOne(d => d.PrgGrp)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.PrgGrpId)
                    .HasConstraintName("FK_arPrgVersions_arPrgGrp_PrgGrpId_PrgGrpId");

                entity.HasOne(d => d.Prog)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.ProgId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_arPrgVersions_arPrograms_ProgId_ProgId");

                entity.HasOne(d => d.ProgTyp)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.ProgTypId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_arProgTypes_ProgTypId_ProgTypId");

                entity.HasOne(d => d.Sap)
                    .WithMany(p => p.ArPrgVersionsSap)
                    .HasForeignKey(d => d.Sapid)
                    .HasConstraintName("FK_arPrgVersions_arSAP_SAPId_SAPId");

                entity.HasOne(d => d.SchedMethod)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.SchedMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_sySchedulingMethods_SchedMethodId_SchedMethodId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_arPrgVersions_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.ThGrdScale)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.ThGrdScaleId)
                    .HasConstraintName("FK_arPrgVersions_arGradeScales_ThGrdScaleId_GrdScaleId");

                entity.HasOne(d => d.TuitionEarning)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.TuitionEarningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_saTuitionEarnings_TuitionEarningId_TuitionEarningId");

                entity.HasOne(d => d.UnitType)
                    .WithMany(p => p.ArPrgVersions)
                    .HasForeignKey(d => d.UnitTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrgVersions_arAttUnitType_UnitTypeId_UnitTypeId");
            });

            modelBuilder.Entity<ArProbWarningTypes>(entity =>
            {
                entity.HasKey(e => e.ProbWarningTypeId);

                entity.ToTable("arProbWarningTypes");

                entity.Property(e => e.ProbWarningTypeId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArProgCredential>(entity =>
            {
                entity.HasKey(e => e.CredentialId);

                entity.ToTable("arProgCredential");

                entity.Property(e => e.CredentialId).ValueGeneratedNever();

                entity.Property(e => e.CredentialCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CredentialDescription)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GerptAgencyFldValId).HasColumnName("GERptAgencyFldValId");

                entity.Property(e => e.Gesequence).HasColumnName("GESequence");

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArProgCredential)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arProgCredential_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArProgCredential)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arProgCredential_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArPrograms>(entity =>
            {
                entity.HasKey(e => e.ProgId);

                entity.ToTable("arPrograms");

                entity.HasIndex(e => new { e.ProgCode, e.ProgDescrip })
                    .HasName("UIX_arPrograms_ProgCode_ProgDescrip")
                    .IsUnique();

                entity.Property(e => e.ProgId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Acid).HasColumnName("ACId");

                entity.Property(e => e.Cipcode)
                    .HasColumnName("CIPCode")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CredentialLevel)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.IsGeprogram).HasColumnName("IsGEProgram");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProgCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ProgDescrip)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Shiftid).HasColumnName("shiftid");

                entity.HasOne(d => d.Ac)
                    .WithMany(p => p.ArPrograms)
                    .HasForeignKey(d => d.Acid)
                    .HasConstraintName("FK_arPrograms_syAcademicCalendars_ACId_ACId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArPrograms)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arPrograms_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.CredentialLvl)
                    .WithMany(p => p.ArPrograms)
                    .HasForeignKey(d => d.CredentialLvlId)
                    .HasConstraintName("FK_arPrograms_arProgCredential_CredentialLvlId_CredentialId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArPrograms)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arPrograms_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArProgramVersionType>(entity =>
            {
                entity.HasKey(e => e.ProgramVersionTypeId);

                entity.ToTable("arProgramVersionType");

                entity.Property(e => e.ProgramVersionTypeId).ValueGeneratedNever();

                entity.Property(e => e.CodeProgramVersionType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DescriptionProgramVersionType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArProgScheduleDetails>(entity =>
            {
                entity.HasKey(e => e.ScheduleDetailId);

                entity.ToTable("arProgScheduleDetails");

                entity.HasIndex(e => e.ScheduleId);

                entity.Property(e => e.ScheduleDetailId).ValueGeneratedNever();

                entity.Property(e => e.AllowEarlyin).HasColumnName("allow_earlyin");

                entity.Property(e => e.AllowExtrahours).HasColumnName("allow_extrahours");

                entity.Property(e => e.AllowLateout).HasColumnName("allow_lateout");

                entity.Property(e => e.CheckTardyin).HasColumnName("check_tardyin");

                entity.Property(e => e.Dw).HasColumnName("dw");

                entity.Property(e => e.Lunchin)
                    .HasColumnName("lunchin")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lunchout)
                    .HasColumnName("lunchout")
                    .HasColumnType("datetime");

                entity.Property(e => e.MaxBeforetardy)
                    .HasColumnName("max_beforetardy")
                    .HasColumnType("datetime");

                entity.Property(e => e.Maxnolunch).HasColumnName("maxnolunch");

                entity.Property(e => e.TardyIntime)
                    .HasColumnName("tardy_intime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Timein)
                    .HasColumnName("timein")
                    .HasColumnType("datetime");

                entity.Property(e => e.Timeout)
                    .HasColumnName("timeout")
                    .HasColumnType("datetime");

                entity.Property(e => e.Total).HasColumnName("total");

                entity.HasOne(d => d.Schedule)
                    .WithMany(p => p.ArProgScheduleDetails)
                    .HasForeignKey(d => d.ScheduleId)
                    .HasConstraintName("FK_arProgScheduleDetails_arProgSchedules_ScheduleId_ScheduleId");
            });

            modelBuilder.Entity<ArProgSchedules>(entity =>
            {
                entity.HasKey(e => e.ScheduleId);

                entity.ToTable("arProgSchedules");

                entity.HasIndex(e => e.PrgVerId);

                entity.Property(e => e.ScheduleId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArProgSchedules)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_arProgSchedules_arPrgVersions_PrgVerId_PrgVerId");
            });

            modelBuilder.Entity<ArProgTypes>(entity =>
            {
                entity.HasKey(e => e.ProgTypId);

                entity.ToTable("arProgTypes");

                entity.HasIndex(e => new { e.Code, e.Description, e.CampGrpId })
                    .HasName("UIX_arProgTypes_Code_Description_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.ProgTypId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedssequence).HasColumnName("IPEDSSequence");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArProgTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arProgTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArProgTypes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arProgTypes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArProgVerDef>(entity =>
            {
                entity.HasKey(e => e.ProgVerDefId);

                entity.ToTable("arProgVerDef");

                entity.Property(e => e.ProgVerDefId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Credits).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Hours).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrkForCompletion)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.GrdSysDetail)
                    .WithMany(p => p.ArProgVerDef)
                    .HasForeignKey(d => d.GrdSysDetailId)
                    .HasConstraintName("FK_arProgVerDef_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArProgVerDef)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arProgVerDef_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArProgVerDef)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arProgVerDef_arReqs_ReqId_ReqId");
            });

            modelBuilder.Entity<ArPtGrdScales>(entity =>
            {
                entity.HasKey(e => e.PtGrdScaleId);

                entity.ToTable("arPtGrdScales");

                entity.Property(e => e.PtGrdScaleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Gpa)
                    .HasColumnName("GPA")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IncInGpa).HasColumnName("IncInGPA");

                entity.Property(e => e.IncInSap).HasColumnName("IncInSAP");

                entity.Property(e => e.Letter)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PtGrdScaleDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PtGrdScalePerc).HasColumnType("decimal(4, 0)");
            });

            modelBuilder.Entity<ArQualMinTyps>(entity =>
            {
                entity.HasKey(e => e.QualMinTypId);

                entity.ToTable("arQualMinTyps");

                entity.Property(e => e.QualMinTypDesc)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArQuantMinUnitTyps>(entity =>
            {
                entity.HasKey(e => e.QuantMinUnitTypId);

                entity.ToTable("arQuantMinUnitTyps");

                entity.Property(e => e.QuantMinTypDesc)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArR2t4additionalInformation>(entity =>
            {
                entity.HasKey(e => e.AdditionalInfoId);

                entity.ToTable("arR2T4AdditionalInformation");

                entity.Property(e => e.AdditionalInfoId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AdditionalInformtion)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4additionalInformationCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4AdditionalInformation_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4additionalInformation)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4AdditionalInformation_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4additionalInformationUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4AdditionalInformation_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4calculationResults>(entity =>
            {
                entity.HasKey(e => e.R2t4calculationResultsId);

                entity.ToTable("arR2T4CalculationResults");

                entity.Property(e => e.R2t4calculationResultsId)
                    .HasColumnName("R2T4CalculationResultsId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DirectGraduatePlusLoanReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.FseogreturnedBySchool)
                    .HasColumnName("FSEOGReturnedBySchool")
                    .HasColumnType("money");

                entity.Property(e => e.FseogreturnedByStudent)
                    .HasColumnName("FSEOGReturnedByStudent")
                    .HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.PellGrantReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.PercentageOfTitleIvaidEarned)
                    .HasColumnName("PercentageOfTitleIVAidEarned")
                    .HasColumnType("money");

                entity.Property(e => e.PerkinsLoanReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.SubDirectLoanReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.SubDirectLoanReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.TeachGrantReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.TeachGrantReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.TotalAmountToBeReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.TotalAmountToBeReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.TotalCharges).HasColumnType("money");

                entity.Property(e => e.TotalTitleIvaid)
                    .HasColumnName("TotalTitleIVAid")
                    .HasColumnType("money");

                entity.Property(e => e.TotalTitleIvaidDisbursed)
                    .HasColumnName("TotalTitleIVAidDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.TotalTitleIvaidToReturn)
                    .HasColumnName("TotalTitleIVAidToReturn")
                    .HasColumnType("money");

                entity.Property(e => e.UnsubDirectLoanReturnedBySchool).HasColumnType("money");

                entity.Property(e => e.UnsubDirectLoanReturnedByStudent).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4calculationResultsCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .HasConstraintName("FK_arR2T4CalculationResults_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4calculationResults)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4CalculationResults_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4calculationResultsUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4CalculationResults_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4input>(entity =>
            {
                entity.HasKey(e => e.R2t4inputId);

                entity.ToTable("arR2T4Input");

                entity.Property(e => e.R2t4inputId)
                    .HasColumnName("R2T4InputId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.BoardFee).HasColumnType("money");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreditBalanceRefunded).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.FseogcouldDisbursed)
                    .HasColumnName("FSEOGCouldDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.Fseogdisbursed)
                    .HasColumnName("FSEOGDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.IsR2t4inputCompleted).HasColumnName("IsR2T4InputCompleted");

                entity.Property(e => e.IsTuitionChargedByPaymentPeriod).HasDefaultValueSql("((0))");

                entity.Property(e => e.OtherFee).HasColumnType("money");

                entity.Property(e => e.PellGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.RoomFee).HasColumnType("money");

                entity.Property(e => e.ScheduledEndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.TuitionFee).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WithdrawalDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4inputCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4Input_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4input)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4Input_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4inputUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4Input_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4overrideInput>(entity =>
            {
                entity.HasKey(e => e.R2t4overrideInputId);

                entity.ToTable("arR2T4OverrideInput");

                entity.Property(e => e.R2t4overrideInputId)
                    .HasColumnName("R2T4OverrideInputId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.BoardFee).HasColumnType("money");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreditBalanceRefunded).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.FseogcouldDisbursed)
                    .HasColumnName("FSEOGCouldDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.Fseogdisbursed)
                    .HasColumnName("FSEOGDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.OtherFee).HasColumnType("money");

                entity.Property(e => e.PellGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.RoomFee).HasColumnType("money");

                entity.Property(e => e.ScheduledEndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.TuitionFee).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WithdrawalDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4overrideInputCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4OverrideInput_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4overrideInput)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4OverrideInput_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4overrideInputUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4OverrideInputs_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4overrideResults>(entity =>
            {
                entity.HasKey(e => e.R2t4overrideResultsId);

                entity.ToTable("arR2T4OverrideResults");

                entity.Property(e => e.R2t4overrideResultsId)
                    .HasColumnName("R2T4OverrideResultsId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.BoardFee).HasColumnType("money");

                entity.Property(e => e.BoxEresult)
                    .HasColumnName("BoxEResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxFresult)
                    .HasColumnName("BoxFResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxGresult)
                    .HasColumnName("BoxGResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxHresult)
                    .HasColumnName("BoxHResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxIresult)
                    .HasColumnName("BoxIResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxJresult)
                    .HasColumnName("BoxJResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxKresult)
                    .HasColumnName("BoxKResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxLresult)
                    .HasColumnName("BoxLResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxMresult)
                    .HasColumnName("BoxMResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxNresult)
                    .HasColumnName("BoxNResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxOresult)
                    .HasColumnName("BoxOResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxPresult)
                    .HasColumnName("BoxPResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxQresult)
                    .HasColumnName("BoxQResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxRresult)
                    .HasColumnName("BoxRResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxSresult)
                    .HasColumnName("BoxSResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxTresult)
                    .HasColumnName("BoxTResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxUresult)
                    .HasColumnName("BoxUResult")
                    .HasColumnType("money");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreditBalanceRefunded).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.FseogamountToReturn)
                    .HasColumnName("FSEOGAmountToReturn")
                    .HasColumnType("money");

                entity.Property(e => e.FseogcouldDisbursed)
                    .HasColumnName("FSEOGCouldDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.Fseogdisbursed)
                    .HasColumnName("FSEOGDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.FseogschoolReturn)
                    .HasColumnName("FSEOGSchoolReturn")
                    .HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.IsR2t4overrideResultsCompleted).HasColumnName("IsR2T4OverrideResultsCompleted");

                entity.Property(e => e.OtherFee).HasColumnType("money");

                entity.Property(e => e.OverriddenData).IsUnicode(false);

                entity.Property(e => e.PellGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.PellGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.PercentageOfActualAttendence).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.PerkinsLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.PostWithdrawalData).IsUnicode(false);

                entity.Property(e => e.RoomFee).HasColumnType("money");

                entity.Property(e => e.ScheduledEndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SubDirectLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.SubTotalAmountCouldDisbursedC).HasColumnType("money");

                entity.Property(e => e.SubTotalAmountDisbursedA).HasColumnType("money");

                entity.Property(e => e.SubTotalNetAmountDisbursedB).HasColumnType("money");

                entity.Property(e => e.SubTotalNetAmountDisbursedD).HasColumnType("money");

                entity.Property(e => e.TeachGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.TeachGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.TuitionFee).HasColumnType("money");

                entity.Property(e => e.UnsubDirectLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WithdrawalDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4overrideResultsCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4OverrideResults_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4overrideResults)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4OverrideResults_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4overrideResultsUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4OverrideResults_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4results>(entity =>
            {
                entity.HasKey(e => e.R2t4resultsId);

                entity.ToTable("arR2T4Results");

                entity.Property(e => e.R2t4resultsId)
                    .HasColumnName("R2T4ResultsId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.BoardFee).HasColumnType("money");

                entity.Property(e => e.BoxEresult)
                    .HasColumnName("BoxEResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxFresult)
                    .HasColumnName("BoxFResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxGresult)
                    .HasColumnName("BoxGResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxHresult)
                    .HasColumnName("BoxHResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxIresult)
                    .HasColumnName("BoxIResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxJresult)
                    .HasColumnName("BoxJResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxKresult)
                    .HasColumnName("BoxKResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxLresult)
                    .HasColumnName("BoxLResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxMresult)
                    .HasColumnName("BoxMResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxNresult)
                    .HasColumnName("BoxNResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxOresult)
                    .HasColumnName("BoxOResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxPresult)
                    .HasColumnName("BoxPResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxQresult)
                    .HasColumnName("BoxQResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxRresult)
                    .HasColumnName("BoxRResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxSresult)
                    .HasColumnName("BoxSResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxTresult)
                    .HasColumnName("BoxTResult")
                    .HasColumnType("money");

                entity.Property(e => e.BoxUresult)
                    .HasColumnName("BoxUResult")
                    .HasColumnType("money");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DirectGraduatePlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectGraduatePlusLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.DirectParentPlusLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.FseogamountToReturn)
                    .HasColumnName("FSEOGAmountToReturn")
                    .HasColumnType("money");

                entity.Property(e => e.FseogcouldDisbursed)
                    .HasColumnName("FSEOGCouldDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.Fseogdisbursed)
                    .HasColumnName("FSEOGDisbursed")
                    .HasColumnType("money");

                entity.Property(e => e.FseogschoolReturn)
                    .HasColumnName("FSEOGSchoolReturn")
                    .HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.IraqAfgGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.IsR2t4resultsCompleted).HasColumnName("IsR2T4ResultsCompleted");

                entity.Property(e => e.OtherFee).HasColumnType("money");

                entity.Property(e => e.OverriddenData).IsUnicode(false);

                entity.Property(e => e.PellGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.PellGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.PellGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.PercentageOfActualAttendence).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.PerkinsLoanCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanDisbursed).HasColumnType("money");

                entity.Property(e => e.PerkinsLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.PostWithdrawalData).IsUnicode(false);

                entity.Property(e => e.RoomFee).HasColumnType("money");

                entity.Property(e => e.ScheduledEndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.SubDirectLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.SubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.SubTotalAmountCouldDisbursedC).HasColumnType("money");

                entity.Property(e => e.SubTotalAmountDisbursedA).HasColumnType("money");

                entity.Property(e => e.SubTotalNetAmountDisbursedB).HasColumnType("money");

                entity.Property(e => e.SubTotalNetAmountDisbursedD).HasColumnType("money");

                entity.Property(e => e.TeachGrantAmountToReturn).HasColumnType("money");

                entity.Property(e => e.TeachGrantCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantDisbursed).HasColumnType("money");

                entity.Property(e => e.TeachGrantSchoolReturn).HasColumnType("money");

                entity.Property(e => e.TuitionFee).HasColumnType("money");

                entity.Property(e => e.UnsubDirectLoanSchoolReturn).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountCouldDisbursed).HasColumnType("money");

                entity.Property(e => e.UnsubLoanNetAmountDisbursed).HasColumnType("money");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.WithdrawalDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4resultsCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4Results_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Termination)
                    .WithMany(p => p.ArR2t4results)
                    .HasForeignKey(d => d.TerminationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4Results_arR2T4TerminationDetails_TerminationId_TerminationId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4resultsUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4Results_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArR2t4terminationDetails>(entity =>
            {
                entity.HasKey(e => e.TerminationId);

                entity.ToTable("arR2T4TerminationDetails");

                entity.Property(e => e.TerminationId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateWithdrawalDetermined).HasColumnType("datetime");

                entity.Property(e => e.IsPerformingR2t4calculator).HasColumnName("IsPerformingR2T4Calculator");

                entity.Property(e => e.IsR2t4approveTabEnabled).HasColumnName("IsR2T4ApproveTabEnabled");

                entity.Property(e => e.IsTerminationReversed).HasDefaultValueSql("((1))");

                entity.Property(e => e.LastDateAttended).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CalculationPeriodType)
                    .WithMany(p => p.ArR2t4terminationDetails)
                    .HasForeignKey(d => d.CalculationPeriodTypeId)
                    .HasConstraintName("FK_arR2T4TerminationDetails_syR2T4CalculationPeriodTypes_CalculationPeriodType_CalculationPeriodTypeId");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.ArR2t4terminationDetailsCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4TerminationDetails_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.DropReason)
                    .WithMany(p => p.ArR2t4terminationDetails)
                    .HasForeignKey(d => d.DropReasonId)
                    .HasConstraintName("FK_arR2T4TerminationDetails_arDropReasons_DropReasonId_DropReasonId");

                entity.HasOne(d => d.StatusCode)
                    .WithMany(p => p.ArR2t4terminationDetails)
                    .HasForeignKey(d => d.StatusCodeId)
                    .HasConstraintName("FK_arR2T4TerminationDetails_syStatusCodes_StatusCodeId_StatusCodeId");

                entity.HasOne(d => d.StuEnrollment)
                    .WithMany(p => p.ArR2t4terminationDetails)
                    .HasForeignKey(d => d.StuEnrollmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arR2T4TerminationDetails_arStuEnrollments_StuEnrollmentId_StuEnrollId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.ArR2t4terminationDetailsUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_arR2T4TerminationDetails_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<ArReqGrpDef>(entity =>
            {
                entity.HasKey(e => e.ReqGrpId);

                entity.ToTable("arReqGrpDef");

                entity.Property(e => e.ReqGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Hours).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Grp)
                    .WithMany(p => p.ArReqGrpDefGrp)
                    .HasForeignKey(d => d.GrpId)
                    .HasConstraintName("FK_arReqGrpDef_arReqs_GrpId_ReqId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArReqGrpDefReq)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arReqGrpDef_arReqs_ReqId_ReqId");
            });

            modelBuilder.Entity<ArReqs>(entity =>
            {
                entity.HasKey(e => e.ReqId);

                entity.ToTable("arReqs");

                entity.HasIndex(e => new { e.ReqId, e.Descrip, e.Code, e.Credits, e.FinAidCredits });

                entity.Property(e => e.ReqId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CompletedDate).HasColumnType("datetime");

                entity.Property(e => e.CourseCatalog)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CourseComments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId).HasColumnName("CourseID");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FinAidCredits).HasDefaultValueSql("((0.00))");

                entity.Property(e => e.IsComCourse).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsExternship).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsOnLine).HasDefaultValueSql("((0))");

                entity.Property(e => e.MinDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Pf)
                    .HasColumnName("PF")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Su)
                    .HasColumnName("SU")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arReqs_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.CourseCategory)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.CourseCategoryId)
                    .HasConstraintName("FK_arReqs_arCourseCategories_CourseCategoryId_CourseCategoryId");

                entity.HasOne(d => d.Dept)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.DeptId)
                    .HasConstraintName("FK_arReqs_arDepartments_DeptId_DeptId");

                entity.HasOne(d => d.GrdLvl)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.GrdLvlId)
                    .HasConstraintName("FK_arReqs_adEdLvls_GrdLvlId_EdLvlId");

                entity.HasOne(d => d.ReqType)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.ReqTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arReqs_arReqTypes_ReqTypeId_ReqTypeId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arReqs_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.UnitType)
                    .WithMany(p => p.ArReqs)
                    .HasForeignKey(d => d.UnitTypeId)
                    .HasConstraintName("FK_arReqs_arAttUnitType_UnitTypeId_UnitTypeId");
            });

            modelBuilder.Entity<ArReqTypes>(entity =>
            {
                entity.HasKey(e => e.ReqTypeId);

                entity.ToTable("arReqTypes");

                entity.Property(e => e.ReqTypeId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArReqTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arReqTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArReqTypes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arReqTypes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArResults>(entity =>
            {
                entity.HasKey(e => e.ResultId);

                entity.ToTable("arResults");

                entity.HasIndex(e => new { e.StuEnrollId, e.TestId })
                    .HasName("UIX_arResults_StuEnrollId_TestId")
                    .IsUnique();

                entity.HasIndex(e => new { e.StuEnrollId, e.TestId, e.Score });

                entity.HasIndex(e => new { e.TestId, e.GrdSysDetailId, e.StuEnrollId, e.Score });

                entity.HasIndex(e => new { e.Score, e.GrdSysDetailId, e.ResultId, e.StuEnrollId, e.TestId })
                    .HasName("IX_arResults_GrdSysDetailId_ResultId_StuEnrollId_TestId_Score");

                entity.Property(e => e.ResultId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DateCompleted).HasColumnType("datetime");

                entity.Property(e => e.DateDetermined).HasColumnType("datetime");

                entity.Property(e => e.DroppedInAddDrop).HasDefaultValueSql("((0))");

                entity.Property(e => e.GradeOverriddenBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GradeOverriddenDate).HasColumnType("datetime");

                entity.Property(e => e.IsClinicsSatisfied).HasColumnName("isClinicsSatisfied");

                entity.Property(e => e.IsTransfered).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdSysDetail)
                    .WithMany(p => p.ArResults)
                    .HasForeignKey(d => d.GrdSysDetailId)
                    .HasConstraintName("FK_arResults_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArResults)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arResults_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.Test)
                    .WithMany(p => p.ArResults)
                    .HasForeignKey(d => d.TestId)
                    .HasConstraintName("FK_arResults_arClassSections_TestId_ClsSectionId");
            });

            modelBuilder.Entity<ArRooms>(entity =>
            {
                entity.HasKey(e => e.RoomId);

                entity.ToTable("arRooms");

                entity.Property(e => e.RoomId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Bldg)
                    .WithMany(p => p.ArRooms)
                    .HasForeignKey(d => d.BldgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arRooms_arBuildings_BldgId_BldgId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArRooms)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_arRooms_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArSap>(entity =>
            {
                entity.HasKey(e => e.Sapid);

                entity.ToTable("arSAP");

                entity.Property(e => e.Sapid)
                    .HasColumnName("SAPId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.MinTermAv).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MinTermGpa)
                    .HasColumnName("MinTermGPA")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sapcode)
                    .IsRequired()
                    .HasColumnName("SAPCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SapcriteriaId).HasColumnName("SAPCriteriaId");

                entity.Property(e => e.Sapdescrip)
                    .IsRequired()
                    .HasColumnName("SAPDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermAvOver).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TermGpaover)
                    .HasColumnName("TermGPAOver")
                    .HasColumnType("decimal(6, 2)");

                entity.Property(e => e.TrackExternAttendance).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArSap)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arSAP_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArSap)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAP_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TrigOffsetTyp)
                    .WithMany(p => p.ArSap)
                    .HasForeignKey(d => d.TrigOffsetTypId)
                    .HasConstraintName("FK_arSAP_arTrigOffsetTyps_TrigOffsetTypId_TrigOffsetTypId");

                entity.HasOne(d => d.TrigUnitTyp)
                    .WithMany(p => p.ArSap)
                    .HasForeignKey(d => d.TrigUnitTypId)
                    .HasConstraintName("FK_arSAP_arTrigUnitTyps_TrigUnitTypId_TrigUnitTypId");
            });

            modelBuilder.Entity<ArSapchkResults>(entity =>
            {
                entity.HasKey(e => e.StdRecKey)
                    .HasAnnotation("SqlServer:Clustered", false);

                entity.ToTable("arSAPChkResults");

                entity.Property(e => e.StdRecKey).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attendance).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.CheckPointDate).HasColumnType("datetime");

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.CumFinAidCredits).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.DatePerformed).HasColumnType("datetime");

                entity.Property(e => e.Gpa).HasColumnName("GPA");

                entity.Property(e => e.IsMakingSap).HasColumnName("IsMakingSAP");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PercentCompleted).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.SapdetailId)
                    .HasColumnName("SAPDetailId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.TermStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Sapdetail)
                    .WithMany(p => p.ArSapchkResults)
                    .HasForeignKey(d => d.SapdetailId)
                    .HasConstraintName("FK_arSAPChkResults_arSAPDetails_SAPDetailId_SAPDetailId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArSapchkResults)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPChkResults_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArSapdetails>(entity =>
            {
                entity.HasKey(e => e.SapdetailId);

                entity.ToTable("arSAPDetails");

                entity.Property(e => e.SapdetailId)
                    .HasColumnName("SAPDetailId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Accuracy).HasColumnName("accuracy");

                entity.Property(e => e.MinAttendanceValue).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.QualMinValue).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.Sapid).HasColumnName("SAPId");

                entity.HasOne(d => d.Sap)
                    .WithMany(p => p.ArSapdetails)
                    .HasForeignKey(d => d.Sapid)
                    .HasConstraintName("FK_arSAPDetails_arSAP_SAPId_SAPId");

                entity.HasOne(d => d.TrigOffsetTyp)
                    .WithMany(p => p.ArSapdetails)
                    .HasForeignKey(d => d.TrigOffsetTypId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPDetails_arTrigOffsetTyps_TrigOffsetTypId_TrigOffsetTypId");

                entity.HasOne(d => d.TrigUnitTyp)
                    .WithMany(p => p.ArSapdetails)
                    .HasForeignKey(d => d.TrigUnitTypId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPDetails_arTrigUnitTyps_TrigUnitTypId_TrigUnitTypId");
            });

            modelBuilder.Entity<ArSapquantByInstruction>(entity =>
            {
                entity.HasKey(e => e.SapquantInsTypeId);

                entity.ToTable("arSAPQuantByInstruction");

                entity.Property(e => e.SapquantInsTypeId)
                    .HasColumnName("SAPQuantInsTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Moddate)
                    .HasColumnName("moddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SapdetailId).HasColumnName("SAPDetailId");

                entity.HasOne(d => d.InstructionType)
                    .WithMany(p => p.ArSapquantByInstruction)
                    .HasForeignKey(d => d.InstructionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantByInstruction_arInstructionType_InstructionTypeId_InstructionTypeId");

                entity.HasOne(d => d.Sapdetail)
                    .WithMany(p => p.ArSapquantByInstruction)
                    .HasForeignKey(d => d.SapdetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantByInstruction_arSAPDetails_SAPDetailId_SAPDetailId");
            });

            modelBuilder.Entity<ArSapquantResults>(entity =>
            {
                entity.HasKey(e => e.ArSapquantResultId);

                entity.ToTable("arSAPQuantResults");

                entity.Property(e => e.ArSapquantResultId)
                    .HasColumnName("arSAPQuantResultID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.PercentCompleted).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.SapdetailId).HasColumnName("SAPDetailId");

                entity.HasOne(d => d.ArSapquantResult)
                    .WithOne(p => p.InverseArSapquantResult)
                    .HasForeignKey<ArSapquantResults>(d => d.ArSapquantResultId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantResults_arSAPQuantResults_arSAPQuantResultID_arSAPQuantResultID");

                entity.HasOne(d => d.InstructionType)
                    .WithMany(p => p.ArSapquantResults)
                    .HasForeignKey(d => d.InstructionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantResults_arInstructionType_InstructionTypeId_InstructionTypeId");

                entity.HasOne(d => d.Sapdetail)
                    .WithMany(p => p.ArSapquantResults)
                    .HasForeignKey(d => d.SapdetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantResults_arSAPDetails_SAPDetailId_SAPDetailId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArSapquantResults)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSAPQuantResults_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArSapShortHandSkillRequirement>(entity =>
            {
                entity.HasKey(e => e.ShortHandSkillRequirementId);

                entity.ToTable("arSAP_ShortHandSkillRequirement");

                entity.Property(e => e.ShortHandSkillRequirementId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Operator)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SapdetailId).HasColumnName("SAPDetailId");
            });

            modelBuilder.Entity<ArSdateSetup>(entity =>
            {
                entity.HasKey(e => e.SdateSetupId);

                entity.ToTable("arSDateSetup");

                entity.Property(e => e.SdateSetupId)
                    .HasColumnName("SDateSetupId")
                    .ValueGeneratedNever();

                entity.Property(e => e.ExpGradDate).HasColumnType("datetime");

                entity.Property(e => e.MaxGradDate).HasColumnType("datetime");

                entity.Property(e => e.MidPtDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdate)
                    .HasColumnName("SDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SdateSetupCode)
                    .IsRequired()
                    .HasColumnName("SDateSetupCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SdateSetupDescrip)
                    .IsRequired()
                    .HasColumnName("SDateSetupDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArSdateSetup)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arSDateSetup_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Prog)
                    .WithMany(p => p.ArSdateSetup)
                    .HasForeignKey(d => d.ProgId)
                    .HasConstraintName("FK_arSDateSetup_arPrograms_ProgId_ProgId");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.ArSdateSetup)
                    .HasForeignKey(d => d.ShiftId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSDateSetup_arShifts_ShiftId_ShiftId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArSdateSetup)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arSDateSetup_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArShifts>(entity =>
            {
                entity.HasKey(e => e.ShiftId);

                entity.ToTable("arShifts");

                entity.HasIndex(e => new { e.ShiftCode, e.ShiftDescrip, e.CampGrpId })
                    .HasName("UIX_arShifts_ShiftCode_ShiftDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.ShiftId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShiftCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ShiftDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArShifts)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arShifts_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArShifts)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arShifts_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<ArStdSuspensions>(entity =>
            {
                entity.HasKey(e => e.StuSuspensionId);

                entity.ToTable("arStdSuspensions");

                entity.Property(e => e.StuSuspensionId).ValueGeneratedNever();

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStdSuspensions)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStdSuspensions_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArStudentClockAttendance>(entity =>
            {
                entity.HasKey(e => new { e.StuEnrollId, e.ScheduleId, e.RecordDate });

                entity.ToTable("arStudentClockAttendance");

                entity.HasIndex(e => new { e.StuEnrollId, e.IsTardy, e.RecordDate });

                entity.HasIndex(e => new { e.StuEnrollId, e.RecordDate, e.SchedHours, e.ActualHours });

                entity.HasIndex(e => new { e.StuEnrollId, e.RecordDate, e.ActualHours, e.SchedHours, e.IsTardy });

                entity.Property(e => e.RecordDate).HasColumnType("smalldatetime");

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .HasMaxLength(240)
                    .IsUnicode(false);

                entity.Property(e => e.IsTardy)
                    .HasColumnName("isTardy")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostByException)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no')");
            });

            modelBuilder.Entity<ArStudentLoas>(entity =>
            {
                entity.HasKey(e => e.StudentLoaid);

                entity.ToTable("arStudentLOAs");

                entity.Property(e => e.StudentLoaid)
                    .HasColumnName("StudentLOAId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.LoareasonId).HasColumnName("LOAReasonId");

                entity.Property(e => e.LoarequestDate)
                    .HasColumnName("LOARequestDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.LoareturnDate)
                    .HasColumnName("LOAReturnDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Loareason)
                    .WithMany(p => p.ArStudentLoas)
                    .HasForeignKey(d => d.LoareasonId)
                    .HasConstraintName("FK_arStudentLOAs_arLOAReasons_LOAReasonId_LOAReasonId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStudentLoas)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStudentLOAs_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArStudentOld>(entity =>
            {
                entity.HasKey(e => e.StudentId)
                    .HasAnnotation("SqlServer:Clustered", false);

                entity.ToTable("arStudentOld");

                entity.Property(e => e.StudentId).ValueGeneratedNever();

                entity.Property(e => e.Admincriteriaid).HasColumnName("admincriteriaid");

                entity.Property(e => e.AlienNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ApportionedCreditBalanceAy1).HasColumnName("ApportionedCreditBalanceAY1");

                entity.Property(e => e.ApportionedCreditBalanceAy2).HasColumnName("ApportionedCreditBalanceAY2");

                entity.Property(e => e.AssignedDate).HasColumnType("datetime");

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.DrivLicNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExpectedStart).HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Objective)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.SourceDate).HasColumnType("datetime");

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArStudentSchedules>(entity =>
            {
                entity.HasKey(e => e.StuScheduleId);

                entity.ToTable("arStudentSchedules");

                entity.HasIndex(e => e.ScheduleId);

                entity.HasIndex(e => e.StuEnrollId);

                entity.HasIndex(e => new { e.StuScheduleId, e.ScheduleId })
                    .HasName("UIX_arStudentSchedules_StuScheduleId_ScheduleId")
                    .IsUnique();

                entity.Property(e => e.StuScheduleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Source)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('attendance')");

                entity.Property(e => e.StartDate).HasColumnType("smalldatetime");

                entity.HasOne(d => d.Schedule)
                    .WithMany(p => p.ArStudentSchedules)
                    .HasForeignKey(d => d.ScheduleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStudentSchedules_arProgSchedules_ScheduleId_ScheduleId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStudentSchedules)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStudentSchedules_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArStudentTimeClockPunches>(entity =>
            {
                entity.HasKey(e => new { e.BadgeId, e.PunchTime, e.PunchType });

                entity.ToTable("arStudentTimeClockPunches");

                entity.Property(e => e.BadgeId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PunchTime).HasColumnType("smalldatetime");

                entity.Property(e => e.ClockId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClsSectMeetingId).HasColumnName("ClsSectMeetingID");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SpecialCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStudentTimeClockPunches)
                    .HasForeignKey(d => d.StuEnrollId)
                    .HasConstraintName("FK_arStudentTimeClockPunches_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArStuEnrollments>(entity =>
            {
                entity.HasKey(e => e.StuEnrollId);

                entity.ToTable("arStuEnrollments");

                entity.HasIndex(e => new { e.ModDate, e.StudentId, e.CampusId })
                    .HasName("IX_arStuEnrollments_CampusId_ModDate_StudentId");

                entity.HasIndex(e => new { e.CampusId, e.PrgVerId, e.StudentId, e.StuEnrollId })
                    .HasName("IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId_StudentId");

                entity.HasIndex(e => new { e.ShiftId, e.StatusCodeId, e.StudentId, e.PrgVerId });

                entity.HasIndex(e => new { e.StartDate, e.PrgVerId, e.ShiftId, e.StatusCodeId, e.StudentId })
                    .HasName("IX_arStuEnrollments_PrgVerId_ShiftId_StatusCodeId_StudentId_StartDate");

                entity.HasIndex(e => new { e.StartDate, e.StudentId, e.StatusCodeId, e.ShiftId, e.StuEnrollId, e.PrgVerId })
                    .HasName("IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId_StuEnrollId_PrgVerId_StartDate");

                entity.HasIndex(e => new { e.ExpGradDate, e.StatusCodeId, e.StartDate, e.CampusId, e.EdLvlId, e.Degcertseekingid, e.StudentId, e.PrgVerId, e.StuEnrollId })
                    .HasName("IX_arStuEnrollments_StatusCodeId_StartDate_CampusId_EdLvlId_degcertseekingid_StudentId_PrgVerId_StuEnrollId_ExpGradDate");

                entity.Property(e => e.StuEnrollId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Attendtypeid).HasColumnName("attendtypeid");

                entity.Property(e => e.BadgeNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CohortStartDate).HasColumnType("datetime");

                entity.Property(e => e.ContractedGradDate).HasColumnType("datetime");

                entity.Property(e => e.DateDetermined).HasColumnType("datetime");

                entity.Property(e => e.Degcertseekingid).HasColumnName("degcertseekingid");

                entity.Property(e => e.DistanceEdStatus)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.EnrollDate).HasColumnType("datetime");

                entity.Property(e => e.EnrollmentId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EntranceInterviewDate).HasColumnType("datetime");

                entity.Property(e => e.ExpGradDate).HasColumnType("datetime");

                entity.Property(e => e.ExpStartDate).HasColumnType("datetime");

                entity.Property(e => e.FaadvisorId).HasColumnName("FAAdvisorId");

                entity.Property(e => e.Graduatedorreceiveddate)
                    .HasColumnName("graduatedorreceiveddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Lda)
                    .HasColumnName("LDA")
                    .HasColumnType("datetime");

                entity.Property(e => e.MidPtDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReEnrollmentDate).HasColumnType("datetime");

                entity.Property(e => e.Sapid).HasColumnName("SAPId");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.TransferDate).HasColumnType("datetime");

                entity.HasOne(d => d.AcademicAdvisorNavigation)
                    .WithMany(p => p.ArStuEnrollmentsAcademicAdvisorNavigation)
                    .HasForeignKey(d => d.AcademicAdvisor)
                    .HasConstraintName("FK_arStuEnrollments_syUsers_AcademicAdvisor_UserId");

                entity.HasOne(d => d.AdmissionsRepNavigation)
                    .WithMany(p => p.ArStuEnrollmentsAdmissionsRepNavigation)
                    .HasForeignKey(d => d.AdmissionsRep)
                    .HasConstraintName("FK_arStuEnrollments_syUsers_AdmissionsRep_UserId");

                entity.HasOne(d => d.Attendtype)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.Attendtypeid)
                    .HasConstraintName("FK_arStuEnrollments_arAttendTypes_attendtypeid_AttendTypeId");

                entity.HasOne(d => d.BillingMethod)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.BillingMethodId)
                    .HasConstraintName("FK_arStuEnrollments_saBillingMethods_BillingMethodId_BillingMethodId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuEnrollments_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.Degcertseeking)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.Degcertseekingid)
                    .HasConstraintName("FK_arStuEnrollments_adDegCertSeeking_degcertseekingid_DegCertSeekingId");

                entity.HasOne(d => d.DropReason)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.DropReasonId)
                    .HasConstraintName("FK_arStuEnrollments_arDropReasons_DropReasonId_DropReasonId");

                entity.HasOne(d => d.EdLvl)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.EdLvlId)
                    .HasConstraintName("FK_arStuEnrollments_adEdLvls_EdLvlId_EdLvlId");

                entity.HasOne(d => d.Faadvisor)
                    .WithMany(p => p.ArStuEnrollmentsFaadvisor)
                    .HasForeignKey(d => d.FaadvisorId)
                    .HasConstraintName("FK_arStuEnrollments_syUsers_FAAdvisorId_UserId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuEnrollments_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuEnrollments_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.PrgVersionType)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.PrgVersionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuEnrollments_arProgramVersionType_PrgVersionTypeId_ProgramVersionTypeId");

                entity.HasOne(d => d.Sap)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.Sapid)
                    .HasConstraintName("FK_arStuEnrollments_arSAP_SAPId_SAPId");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("FK_arStuEnrollments_arShifts_ShiftId_ShiftId");

                entity.HasOne(d => d.StatusCode)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.StatusCodeId)
                    .HasConstraintName("FK_arStuEnrollments_syStatusCodes_StatusCodeId_StatusCodeId");

                entity.HasOne(d => d.StuEnroll)
                    .WithOne(p => p.InverseStuEnroll)
                    .HasForeignKey<ArStuEnrollments>(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuEnrollments_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.ArStuEnrollments)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_arStuEnrollments_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");
            });

            modelBuilder.Entity<ArStuPayPeriods>(entity =>
            {
                entity.HasKey(e => new { e.StudentEnrollmentId, e.PaymentPeriod, e.PeriodStartDate, e.PeriodEndDate, e.ProgLength });

                entity.ToTable("arStuPayPeriods");

                entity.Property(e => e.StudentEnrollmentId).HasColumnName("StudentEnrollmentID");

                entity.Property(e => e.PeriodStartDate).HasColumnType("datetime");

                entity.Property(e => e.PeriodEndDate).HasColumnType("datetime");

                entity.Property(e => e.AcademicCalendarType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PaymentPeriodRange)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.StuEnrollPayPeriodId)
                    .HasColumnName("StuEnrollPayPeriodID")
                    .HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<ArStuProbWarnings>(entity =>
            {
                entity.HasKey(e => e.StuProbWarningId);

                entity.ToTable("arStuProbWarnings");

                entity.Property(e => e.StuProbWarningId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProbWarningType)
                    .WithMany(p => p.ArStuProbWarnings)
                    .HasForeignKey(d => d.ProbWarningTypeId)
                    .HasConstraintName("FK_arStuProbWarnings_arProbWarningTypes_ProbWarningTypeId_ProbWarningTypeId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStuProbWarnings)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuProbWarnings_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArStuReschReason>(entity =>
            {
                entity.HasKey(e => e.ReschReasonTypeId);

                entity.ToTable("arStuReschReason");

                entity.Property(e => e.ReschReasonTypeId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.ArStuReschReason)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuReschReason_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArStuReschReason)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arStuReschReason_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<ArTerm>(entity =>
            {
                entity.HasKey(e => e.TermId);

                entity.ToTable("arTerm");

                entity.HasIndex(e => new { e.TermCode, e.TermDescrip, e.CampGrpId });

                entity.HasIndex(e => new { e.TermId, e.StartDate, e.TermDescrip, e.EndDate });

                entity.Property(e => e.TermId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsModule).HasDefaultValueSql("((0))");

                entity.Property(e => e.MaxGradDate).HasColumnType("datetime");

                entity.Property(e => e.MidPtDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.TermCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TermDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_arTerm_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Prog)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.ProgId)
                    .HasConstraintName("FK_arTerm_arPrograms_ProgId_ProgId");

                entity.HasOne(d => d.ProgramVersion)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.ProgramVersionId)
                    .HasConstraintName("FK_arTerm_arPrgVersions_ProgramVersionId_PrgVerId");

                entity.HasOne(d => d.Shift)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.ShiftId)
                    .HasConstraintName("FK_arTerm_arShifts_ShiftId_ShiftId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTerm_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TermType)
                    .WithMany(p => p.ArTerm)
                    .HasForeignKey(d => d.TermTypeId)
                    .HasConstraintName("FK_arTerm_syTermTypes_TermTypeId_TermTypeId");
            });

            modelBuilder.Entity<ArTermEnrollSummary>(entity =>
            {
                entity.HasKey(e => e.TesummId);

                entity.ToTable("arTermEnrollSummary");

                entity.HasIndex(e => new { e.TermId, e.StuEnrollId })
                    .HasName("UIX_arTermEnrollSummary_TermId_StuEnrollId")
                    .IsUnique();

                entity.Property(e => e.TesummId)
                    .HasColumnName("TESummId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DescripXtranscript)
                    .HasColumnName("DescripXTranscript")
                    .HasMaxLength(250);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser).HasMaxLength(50);

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArTermEnrollSummary)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTermEnrollSummary_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.ArTermEnrollSummary)
                    .HasForeignKey(d => d.TermId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTermEnrollSummary_arTerm_TermId_TermId");
            });

            modelBuilder.Entity<ArTestingModels>(entity =>
            {
                entity.HasKey(e => e.TestingModelId);

                entity.ToTable("arTestingModels");

                entity.Property(e => e.TestingModelDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArTimeClockSpecialCode>(entity =>
            {
                entity.HasKey(e => e.Tcsid);

                entity.ToTable("arTimeClockSpecialCode");

                entity.Property(e => e.Tcsid)
                    .HasColumnName("TCSId")
                    .ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TcspecialCode)
                    .IsRequired()
                    .HasColumnName("TCSpecialCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TcspunchType).HasColumnName("TCSPunchType");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.ArTimeClockSpecialCode)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTimeClockSpecialCode_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.InstructionType)
                    .WithMany(p => p.ArTimeClockSpecialCode)
                    .HasForeignKey(d => d.InstructionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTimeClockSpecialCode_arInstructionType_InstructionTypeId_InstructionTypeId");
            });

            modelBuilder.Entity<ArTrackTransfer>(entity =>
            {
                entity.HasKey(e => e.TrackTransferId);

                entity.ToTable("arTrackTransfer");

                entity.Property(e => e.TrackTransferId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArTransferGrades>(entity =>
            {
                entity.HasKey(e => e.TransferId);

                entity.ToTable("arTransferGrades");

                entity.HasIndex(e => new { e.StuEnrollId, e.TermId });

                entity.HasIndex(e => new { e.StuEnrollId, e.TransferId, e.TermId, e.ReqId });

                entity.Property(e => e.TransferId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CompletedDate).HasColumnType("datetime");

                entity.Property(e => e.GradeOverriddenBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GradeOverriddenDate).HasColumnType("datetime");

                entity.Property(e => e.IsClinicsSatisfied)
                    .HasColumnName("isClinicsSatisfied")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTransferred).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdSysDetail)
                    .WithMany(p => p.ArTransferGrades)
                    .HasForeignKey(d => d.GrdSysDetailId)
                    .HasConstraintName("FK_arTransferGrades_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId");

                entity.HasOne(d => d.Req)
                    .WithMany(p => p.ArTransferGrades)
                    .HasForeignKey(d => d.ReqId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTransferGrades_arReqs_ReqId_ReqId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.ArTransferGrades)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTransferGrades_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.ArTransferGrades)
                    .HasForeignKey(d => d.TermId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_arTransferGrades_arTerm_TermId_TermId");
            });

            modelBuilder.Entity<ArTriggerOffsetTyps>(entity =>
            {
                entity.HasKey(e => e.TrigOffsetTypeId);

                entity.ToTable("arTriggerOffsetTyps");

                entity.Property(e => e.TrigOffsetTypeId).ValueGeneratedNever();

                entity.Property(e => e.TrigOffTypDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArTrigOffsetTyps>(entity =>
            {
                entity.HasKey(e => e.TrigOffsetTypId);

                entity.ToTable("arTrigOffsetTyps");

                entity.Property(e => e.TrigOffTypDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArTrigUnitTyps>(entity =>
            {
                entity.HasKey(e => e.TrigUnitTypId);

                entity.ToTable("arTrigUnitTyps");

                entity.Property(e => e.TrigUnitTypDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ArUnschedClosures>(entity =>
            {
                entity.HasKey(e => e.UnschedClosureId);

                entity.ToTable("arUnschedClosures");

                entity.Property(e => e.UnschedClosureId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ClassReSchduledFrom).HasColumnType("datetime");

                entity.Property(e => e.ClassReSchduledTo).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.ArUnschedClosures)
                    .HasForeignKey(d => d.ClsSectionId)
                    .HasConstraintName("FK_arUnschedClosures_arClassSections_ClsSectionId_ClsSectionId");
            });

            modelBuilder.Entity<AtAttendance>(entity =>
            {
                entity.HasKey(e => e.AttendanceId);

                entity.ToTable("atAttendance");

                entity.Property(e => e.AttendanceId).ValueGeneratedNever();

                entity.Property(e => e.AttendanceDate).HasColumnType("datetime");

                entity.Property(e => e.AttendanceType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Enroll)
                    .WithMany(p => p.AtAttendance)
                    .HasForeignKey(d => d.EnrollId)
                    .HasConstraintName("FK_atAttendance_arStuEnrollments_EnrollId_StuEnrollId");
            });

            modelBuilder.Entity<AtClsSectAttendance>(entity =>
            {
                entity.HasKey(e => e.ClsSectAttId);

                entity.ToTable("atClsSectAttendance");

                entity.HasIndex(e => new { e.Actual, e.MeetDate, e.StuEnrollId })
                    .HasName("IX_atClsSectAttendance_StuEnrollId_Actual_MeetDate");

                entity.HasIndex(e => new { e.ClsSectionId, e.MeetDate, e.StuEnrollId, e.ClsSectMeetingId })
                    .HasName("UIX_atClsSectAttendance_ClsSectionId_MeetDate_StuEnrollId_ClsSectMeetingId")
                    .IsUnique();

                entity.HasIndex(e => new { e.StuEnrollId, e.Actual, e.Excused, e.MeetDate });

                entity.HasIndex(e => new { e.Tardy, e.Actual, e.StuEnrollId, e.MeetDate });

                entity.HasIndex(e => new { e.ClsSectAttId, e.StuEnrollId, e.Actual, e.ClsSectionId, e.MeetDate })
                    .HasName("IX_atClsSectAttendance_StuEnrollId_Actual_ClsSectionId_MeetDate_ClsSectAttId");

                entity.HasIndex(e => new { e.StuEnrollId, e.MeetDate, e.Actual, e.Excused, e.Tardy });

                entity.HasIndex(e => new { e.StuEnrollId, e.MeetDate, e.ClsSectAttId, e.ClsSectionId, e.Actual, e.Tardy, e.Excused });

                entity.Property(e => e.ClsSectAttId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Actual).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Comments)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeetDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.Scheduled).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.ClsSectMeeting)
                    .WithMany(p => p.AtClsSectAttendance)
                    .HasForeignKey(d => d.ClsSectMeetingId)
                    .HasConstraintName("FK_atClsSectAttendance_arClsSectMeetings_ClsSectMeetingId_ClsSectMeetingId");

                entity.HasOne(d => d.ClsSection)
                    .WithMany(p => p.AtClsSectAttendance)
                    .HasForeignKey(d => d.ClsSectionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_atClsSectAttendance_arClassSections_ClsSectionId_ClsSectionId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.AtClsSectAttendance)
                    .HasForeignKey(d => d.StuEnrollId)
                    .HasConstraintName("FK_atClsSectAttendance_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<AtConversionAttendance>(entity =>
            {
                entity.HasKey(e => e.ConversionAttendanceId);

                entity.ToTable("atConversionAttendance");

                entity.HasIndex(e => new { e.MeetDate, e.StuEnrollId })
                    .HasName("UIX_atConversionAttendance_MeetDate_StuEnrollId")
                    .IsUnique();

                entity.Property(e => e.ConversionAttendanceId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AttendanceId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Comments)
                    .HasColumnName("comments")
                    .HasMaxLength(240)
                    .IsUnicode(false);

                entity.Property(e => e.MeetDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.PostByException)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no')");
            });

            modelBuilder.Entity<AtOnLineAttendance>(entity =>
            {
                entity.HasKey(e => e.OnlineAttendanceId);

                entity.ToTable("atOnLineAttendance");

                entity.HasIndex(e => new { e.OnLineTermId, e.CourseShortName, e.OnLineStudentId, e.AttendedDate, e.Minutes })
                    .HasName("UIX_atOnLineAttendance_OnLineTermId_CourseShortName_OnLineStudentId_AttendedDate_Minutes")
                    .IsUnique();

                entity.Property(e => e.OnlineAttendanceId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.AttendedDate).HasColumnType("datetime");

                entity.Property(e => e.CourseShortName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AtOnLineStudents>(entity =>
            {
                entity.HasKey(e => e.StuEnrollId);

                entity.ToTable("atOnLineStudents");

                entity.Property(e => e.StuEnrollId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CmDocuments>(entity =>
            {
                entity.HasKey(e => e.DocumentId);

                entity.ToTable("cmDocuments");

                entity.HasIndex(e => new { e.DocumentCode, e.DocumentDescrip, e.CampGrpId })
                    .HasName("UIX_cmDocuments_DocumentCode_DocumentDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.DocumentId).ValueGeneratedNever();

                entity.Property(e => e.DocumentCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.CmDocuments)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_cmDocuments_syCampGrps_CampGrpId_CampGrpId");
            });

            modelBuilder.Entity<CmTimeInterval>(entity =>
            {
                entity.HasKey(e => e.TimeIntervalId);

                entity.ToTable("cmTimeInterval");

                entity.HasIndex(e => e.TimeIntervalDescrip)
                    .HasName("UIX_cmTimeInterval_TimeIntervalDescrip")
                    .IsUnique();

                entity.Property(e => e.TimeIntervalId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeIntervalDescrip).HasColumnType("datetime");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.CmTimeInterval)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_cmTimeInterval_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.CmTimeInterval)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_cmTimeInterval_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<DbaDbversion>(entity =>
            {
                entity.HasKey(e => e.DbversionId);

                entity.ToTable("DBA_DBVersion");

                entity.Property(e => e.DbversionId)
                    .HasColumnName("DBVersionId")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Dbversion)
                    .IsRequired()
                    .HasColumnName("DBVersion")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SubVersion)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpgradeDate)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<DeploymentMetadata>(entity =>
            {
                entity.ToTable("DeploymentMetadata", "RedGateLocal");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.As)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("(suser_sname())");

                entity.Property(e => e.BlockId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.By)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("(original_login())");

                entity.Property(e => e.CompletedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.InsertedSerial)
                    .IsRequired()
                    .HasMaxLength(8)
                    .HasDefaultValueSql("(@@dbts+(1))");

                entity.Property(e => e.MetadataVersion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedSerial)
                    .IsRequired()
                    .IsRowVersion();

                entity.Property(e => e.With)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasDefaultValueSql("(app_name())");
            });

            modelBuilder.Entity<DocumentFile>(entity =>
            {
                entity.ToTable("DocumentFile", "doc");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Content).IsRequired();

                entity.Property(e => e.DateCreated)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.StudentDoc)
                    .WithMany(p => p.DocumentFile)
                    .HasForeignKey(d => d.StudentDocId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentFile_plStudentDocs_StudentDocId_StudentDocId");
            });

            modelBuilder.Entity<FaAwardTimeIntervals>(entity =>
            {
                entity.HasKey(e => e.AwdTimeIntervalId);

                entity.ToTable("faAwardTimeIntervals");

                entity.Property(e => e.AwdTimeIntervalId).ValueGeneratedNever();

                entity.Property(e => e.AwdTimeIntervalDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FaLenders>(entity =>
            {
                entity.HasKey(e => e.LenderId);

                entity.ToTable("faLenders");

                entity.HasIndex(e => new { e.Code, e.LenderDescrip, e.PrimaryContact })
                    .HasName("UIX_faLenders_Code_LenderDescrip_PrimaryContact")
                    .IsUnique();

                entity.Property(e => e.LenderId).ValueGeneratedNever();

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comments)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CustService)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LenderDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherContact)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherPayState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayAddress1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayAddress2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayCity)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayZip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostClaim)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreClaim)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryContact)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.FaLenders)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_faLenders_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.FaLendersCountry)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_faLenders_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.PayCountry)
                    .WithMany(p => p.FaLendersPayCountry)
                    .HasForeignKey(d => d.PayCountryId)
                    .HasConstraintName("FK_faLenders_adCountries_PayCountryId_CountryId");

                entity.HasOne(d => d.PayState)
                    .WithMany(p => p.FaLendersPayState)
                    .HasForeignKey(d => d.PayStateId)
                    .HasConstraintName("FK_faLenders_syStates_PayStateId_StateId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.FaLendersState)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_faLenders_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.FaLenders)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_faLenders_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<FaStudentAwards>(entity =>
            {
                entity.HasKey(e => e.StudentAwardId);

                entity.ToTable("faStudentAwards");

                entity.HasIndex(e => e.AwardTypeId);

                entity.HasIndex(e => e.StuEnrollId);

                entity.HasIndex(e => e.StudentAwardId);

                entity.Property(e => e.StudentAwardId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AwardCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AwardEndDate).HasColumnType("datetime");

                entity.Property(e => e.AwardStartDate).HasColumnType("datetime");

                entity.Property(e => e.AwardStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AwardSubCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmasfundCode)
                    .HasColumnName("EMASFundCode")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FaId)
                    .HasColumnName("FA_Id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrossAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.LoanFees).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.LoanId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AcademicYear)
                    .WithMany(p => p.FaStudentAwards)
                    .HasForeignKey(d => d.AcademicYearId)
                    .HasConstraintName("FK_faStudentAwards_saAcademicYears_AcademicYearId_AcademicYearId");

                entity.HasOne(d => d.AwardType)
                    .WithMany(p => p.FaStudentAwards)
                    .HasForeignKey(d => d.AwardTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_faStudentAwards_saFundSources_AwardTypeId_FundSourceId");

                entity.HasOne(d => d.Guarantor)
                    .WithMany(p => p.FaStudentAwardsGuarantor)
                    .HasForeignKey(d => d.GuarantorId)
                    .HasConstraintName("FK_faStudentAwards_faLenders_GuarantorId_LenderId");

                entity.HasOne(d => d.Lender)
                    .WithMany(p => p.FaStudentAwardsLender)
                    .HasForeignKey(d => d.LenderId)
                    .HasConstraintName("FK_faStudentAwards_faLenders_LenderId_LenderId");

                entity.HasOne(d => d.Servicer)
                    .WithMany(p => p.FaStudentAwardsServicer)
                    .HasForeignKey(d => d.ServicerId)
                    .HasConstraintName("FK_faStudentAwards_faLenders_ServicerId_LenderId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.FaStudentAwards)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_faStudentAwards_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<FaStudentAwardSchedule>(entity =>
            {
                entity.HasKey(e => e.AwardScheduleId);

                entity.ToTable("faStudentAwardSchedule");

                entity.HasIndex(e => e.AwardScheduleId);

                entity.HasIndex(e => e.StudentAwardId);

                entity.Property(e => e.AwardScheduleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.DisbursementStatus)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExpectedDate).HasColumnType("datetime");

                entity.Property(e => e.GrossAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.IsProcessed)
                    .HasColumnName("isProcessed")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LoanFees).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('sa')");

                entity.Property(e => e.Recid)
                    .HasColumnName("recid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SequenceNumber).HasDefaultValueSql("((1))");

                entity.Property(e => e.TermNumber)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.HasOne(d => d.StudentAward)
                    .WithMany(p => p.FaStudentAwardSchedule)
                    .HasForeignKey(d => d.StudentAwardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_faStudentAwardSchedule_faStudentAwards_StudentAwardId_StudentAwardId");
            });

            modelBuilder.Entity<FaStudentPaymentPlans>(entity =>
            {
                entity.HasKey(e => e.PaymentPlanId);

                entity.ToTable("faStudentPaymentPlans");

                entity.Property(e => e.PaymentPlanId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayPlanDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayPlanEndDate).HasColumnType("datetime");

                entity.Property(e => e.PayPlanStartDate).HasColumnType("datetime");

                entity.Property(e => e.TotalAmountDue).HasColumnType("decimal(19, 4)");

                entity.HasOne(d => d.AcademicYear)
                    .WithMany(p => p.FaStudentPaymentPlans)
                    .HasForeignKey(d => d.AcademicYearId)
                    .HasConstraintName("FK_faStudentPaymentPlans_saAcademicYears_AcademicYearId_AcademicYearId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.FaStudentPaymentPlans)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_faStudentPaymentPlans_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<FaStuPaymentPlanSchedule>(entity =>
            {
                entity.HasKey(e => e.PayPlanScheduleId);

                entity.ToTable("faStuPaymentPlanSchedule");

                entity.Property(e => e.PayPlanScheduleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.ExpectedDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reference)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PaymentPlan)
                    .WithMany(p => p.FaStuPaymentPlanSchedule)
                    .HasForeignKey(d => d.PaymentPlanId)
                    .HasConstraintName("FK_faStuPaymentPlanSchedule_faStudentPaymentPlans_PaymentPlanId_PaymentPlanId");
            });

            modelBuilder.Entity<HrEmpCerts>(entity =>
            {
                entity.HasKey(e => e.EmpCertId);

                entity.ToTable("hrEmpCerts");

                entity.Property(e => e.EmpCertId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Cert)
                    .WithMany(p => p.HrEmpCerts)
                    .HasForeignKey(d => d.CertId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpCerts_syCertifications_CertId_CertificationId");

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HrEmpCerts)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpCerts_hrEmployees_EmpId_EmpId");
            });

            modelBuilder.Entity<HrEmpContactInfo>(entity =>
            {
                entity.HasKey(e => e.EmpContactInfoId);

                entity.ToTable("hrEmpContactInfo");

                entity.Property(e => e.EmpContactInfoId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Beeper)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HrEmpContactInfo)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpContactInfo_hrEmployees_EmpId_EmpId");
            });

            modelBuilder.Entity<HrEmpDegrees>(entity =>
            {
                entity.HasKey(e => e.EmpDegreeId);

                entity.ToTable("hrEmpDegrees");

                entity.Property(e => e.EmpDegreeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Degree)
                    .WithMany(p => p.HrEmpDegrees)
                    .HasForeignKey(d => d.DegreeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpDegrees_arDegrees_DegreeId_DegreeId");

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HrEmpDegrees)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpDegrees_hrEmployees_EmpId_EmpId");
            });

            modelBuilder.Entity<HrEmpHrinfo>(entity =>
            {
                entity.HasKey(e => e.EmpHrinfoId);

                entity.ToTable("hrEmpHRInfo");

                entity.HasIndex(e => e.EmpHrinfoId)
                    .HasName("UIX_hrEmpHRInfo_EmpHRInfoId")
                    .IsUnique();

                entity.Property(e => e.EmpHrinfoId)
                    .HasColumnName("EmpHRInfoId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.HireDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.HrEmpHrinfo)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_hrEmpHRInfo_syHRDepartments_DepartmentId_HRDepartmentId");

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HrEmpHrinfo)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmpHRInfo_hrEmployees_EmpId_EmpId");

                entity.HasOne(d => d.Position)
                    .WithMany(p => p.HrEmpHrinfo)
                    .HasForeignKey(d => d.PositionId)
                    .HasConstraintName("FK_hrEmpHRInfo_syPositions_PositionId_PositionId");
            });

            modelBuilder.Entity<HrEmployeeEmergencyContacts>(entity =>
            {
                entity.HasKey(e => e.EmployeeEmergencyContactId);

                entity.ToTable("hrEmployeeEmergencyContacts");

                entity.Property(e => e.EmployeeEmergencyContactId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Beeper)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeEmergencyContactName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Emp)
                    .WithMany(p => p.HrEmployeeEmergencyContacts)
                    .HasForeignKey(d => d.EmpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmployeeEmergencyContacts_hrEmployees_EmpId_EmpId");

                entity.HasOne(d => d.Relation)
                    .WithMany(p => p.HrEmployeeEmergencyContacts)
                    .HasForeignKey(d => d.RelationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmployeeEmergencyContacts_syRelations_RelationId_RelationId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.HrEmployeeEmergencyContacts)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmployeeEmergencyContacts_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<HrEmployees>(entity =>
            {
                entity.HasKey(e => e.EmpId);

                entity.ToTable("hrEmployees");

                entity.Property(e => e.EmpId).ValueGeneratedNever();

                entity.Property(e => e.Address1)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate).HasColumnType("smalldatetime");

                entity.Property(e => e.City)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mi)
                    .HasColumnName("MI")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_hrEmployees_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_hrEmployees_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.GenderId)
                    .HasConstraintName("FK_hrEmployees_adGenders_GenderId_GenderId");

                entity.HasOne(d => d.MaritalStat)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.MaritalStatId)
                    .HasConstraintName("FK_hrEmployees_adMaritalStatus_MaritalStatId_MaritalStatId");

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_hrEmployees_syPrefixes_PrefixId_PrefixId");

                entity.HasOne(d => d.Race)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.RaceId)
                    .HasConstraintName("FK_hrEmployees_adEthCodes_RaceId_EthCodeId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_hrEmployees_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hrEmployees_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Suffix)
                    .WithMany(p => p.HrEmployees)
                    .HasForeignKey(d => d.SuffixId)
                    .HasConstraintName("FK_hrEmployees_sySuffixes_SuffixId_SuffixId");
            });

            modelBuilder.Entity<LeadImage>(entity =>
            {
                entity.HasKey(e => e.ImageId);

                entity.ToTable("LeadImage", "doc");

                entity.Property(e => e.Image).IsRequired();

                entity.Property(e => e.ImgFile)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MediaType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.LeadImage)
                    .HasForeignKey(d => d.LeadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LeadImage_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<MsgEntitiesFieldGroups>(entity =>
            {
                entity.HasKey(e => e.EntityFieldGroupId);

                entity.ToTable("msgEntitiesFieldGroups");

                entity.Property(e => e.EntityFieldGroupId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FieldGroupName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MsgGroups>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.ToTable("msgGroups");

                entity.HasIndex(e => new { e.Code, e.Descrip, e.CampGroupId })
                    .HasName("UIX_msgGroups_Code_Descrip_CampGroupId")
                    .IsUnique();

                entity.Property(e => e.GroupId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.HasOne(d => d.CampGroup)
                    .WithMany(p => p.MsgGroups)
                    .HasForeignKey(d => d.CampGroupId)
                    .HasConstraintName("FK_msgGroups_syCampGrps_CampGroupId_CampGrpId");
            });

            modelBuilder.Entity<MsgMessages>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.ToTable("msgMessages");

                entity.Property(e => e.MessageId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DateDelivered).HasColumnType("datetime");

                entity.Property(e => e.DeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.DeliveryType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastDeliveryAttempt).HasColumnType("datetime");

                entity.Property(e => e.LastDeliveryMsg)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.MsgContent).HasColumnType("text");

                entity.Property(e => e.ReType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientType)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.MsgMessages)
                    .HasForeignKey(d => d.TemplateId)
                    .HasConstraintName("FK_msgMessages_msgTemplates_TemplateId_TemplateId");
            });

            modelBuilder.Entity<MsgRules>(entity =>
            {
                entity.HasKey(e => e.RuleId);

                entity.ToTable("msgRules");

                entity.Property(e => e.RuleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DeliveryType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastRun).HasColumnType("datetime");

                entity.Property(e => e.LastRunError)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ReType)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RecipientType)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RuleSql).HasColumnType("text");

                entity.HasOne(d => d.CampGroup)
                    .WithMany(p => p.MsgRules)
                    .HasForeignKey(d => d.CampGroupId)
                    .HasConstraintName("FK_msgRules_syCampGrps_CampGroupId_CampGrpId");

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.MsgRules)
                    .HasForeignKey(d => d.TemplateId)
                    .HasConstraintName("FK_msgRules_msgTemplates_TemplateId_TemplateId");
            });

            modelBuilder.Entity<MsgTemplates>(entity =>
            {
                entity.HasKey(e => e.TemplateId);

                entity.ToTable("msgTemplates");

                entity.HasIndex(e => new { e.CampGroupId, e.Code, e.Descrip })
                    .HasName("UIX_msgTemplates_CampGroupId_Code_Descrip")
                    .IsUnique();

                entity.Property(e => e.TemplateId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Data).HasColumnType("text");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.HasOne(d => d.CampGroup)
                    .WithMany(p => p.MsgTemplates)
                    .HasForeignKey(d => d.CampGroupId)
                    .HasConstraintName("FK_msgTemplates_syCampGrps_CampGroupId_CampGrpId");
            });

            modelBuilder.Entity<ParamDetail>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.Property(e => e.CaptionOverride)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<ParamDetailProp>(entity =>
            {
                entity.HasKey(e => e.Detailpropertyid);

                entity.Property(e => e.Detailpropertyid).HasColumnName("detailpropertyid");

                entity.Property(e => e.Childcontrol)
                    .HasColumnName("childcontrol")
                    .HasMaxLength(100);

                entity.Property(e => e.Detailid).HasColumnName("detailid");

                entity.Property(e => e.Propname)
                    .IsRequired()
                    .HasColumnName("propname")
                    .HasMaxLength(100);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value");

                entity.Property(e => e.Valuetype)
                    .IsRequired()
                    .HasColumnName("valuetype")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<ParamItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ControllerClass)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsItemOverriden).HasDefaultValueSql("((0))");

                entity.Property(e => e.ReturnValueName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Valueprop).HasColumnName("valueprop");
            });

            modelBuilder.Entity<ParamItemProp>(entity =>
            {
                entity.HasKey(e => e.Itempropertyid);

                entity.Property(e => e.Itempropertyid).HasColumnName("itempropertyid");

                entity.Property(e => e.Childcontrol)
                    .HasColumnName("childcontrol")
                    .HasMaxLength(100);

                entity.Property(e => e.Itemid).HasColumnName("itemid");

                entity.Property(e => e.Propname)
                    .IsRequired()
                    .HasColumnName("propname")
                    .HasMaxLength(100);

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value");

                entity.Property(e => e.Valuetype)
                    .IsRequired()
                    .HasColumnName("valuetype")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<ParamSection>(entity =>
            {
                entity.HasKey(e => e.SectionId);

                entity.Property(e => e.SectionCaption).HasMaxLength(100);

                entity.Property(e => e.SectionDescription).HasMaxLength(500);

                entity.Property(e => e.SectionName).HasMaxLength(50);
            });

            modelBuilder.Entity<ParamSet>(entity =>
            {
                entity.HasKey(e => e.SetId);

                entity.Property(e => e.SetDescription)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.SetDisplayName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SetName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SetType)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PlAddressTypes>(entity =>
            {
                entity.HasKey(e => e.AddressTypeId);

                entity.ToTable("plAddressTypes");

                entity.HasIndex(e => new { e.AddressCode, e.AddressDescrip, e.CampGrpId })
                    .HasName("UIX_plAddressTypes_AddressCode_AddressDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.AddressTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AddressCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlAddressTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plAddressTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlAddressTypes)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plAddressTypes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlCorpHqs>(entity =>
            {
                entity.HasKey(e => e.CorpHqid);

                entity.ToTable("plCorpHQs");

                entity.Property(e => e.CorpHqid)
                    .HasColumnName("CorpHQId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CorpHqcode)
                    .IsRequired()
                    .HasColumnName("CorpHQCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CorpHqname)
                    .IsRequired()
                    .HasColumnName("CorpHQName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.State)
                    .WithMany(p => p.PlCorpHqs)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plCorpHQs_syStates_StateId_StateId");
            });

            modelBuilder.Entity<PlEmployerContact>(entity =>
            {
                entity.HasKey(e => e.EmployerContactId);

                entity.ToTable("plEmployerContact");

                entity.Property(e => e.EmployerContactId).ValueGeneratedNever();

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressTypeId).HasColumnName("AddressTypeID");

                entity.Property(e => e.Beeper)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CellBestTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CellPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeBestTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeExt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PinNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TitleId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WorkBestTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkExt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.AddressTypeId)
                    .HasConstraintName("FK_plEmployerContact_plAddressTypes_AddressTypeID_AddressTypeId");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.Country)
                    .HasConstraintName("FK_plEmployerContact_adCountries_Country_CountryId");

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.EmployerId)
                    .HasConstraintName("FK_plEmployerContact_plEmployers_EmployerId_EmployerId");

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_plEmployerContact_syPrefixes_PrefixId_PrefixId");

                entity.HasOne(d => d.StateNavigation)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.State)
                    .HasConstraintName("FK_plEmployerContact_syStates_State_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plEmployerContact_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Suffix)
                    .WithMany(p => p.PlEmployerContact)
                    .HasForeignKey(d => d.SuffixId)
                    .HasConstraintName("FK_plEmployerContact_sySuffixes_SuffixId_SuffixId");
            });

            modelBuilder.Entity<PlEmployerJobCats>(entity =>
            {
                entity.HasKey(e => e.EmployerJobId);

                entity.ToTable("plEmployerJobCats");

                entity.Property(e => e.EmployerJobId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.PlEmployerJobCats)
                    .HasForeignKey(d => d.EmployerId)
                    .HasConstraintName("FK_plEmployerJobCats_plEmployers_EmployerId_EmployerId");

                entity.HasOne(d => d.JobCat)
                    .WithMany(p => p.PlEmployerJobCats)
                    .HasForeignKey(d => d.JobCatId)
                    .HasConstraintName("FK_plEmployerJobCats_plJobType_JobCatId_JobGroupId");
            });

            modelBuilder.Entity<PlEmployerJobs>(entity =>
            {
                entity.HasKey(e => e.EmployerJobId);

                entity.ToTable("plEmployerJobs");

                entity.HasIndex(e => new { e.Code, e.EmployerJobTitle, e.JobTitleId, e.EmployerId })
                    .HasName("UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId")
                    .IsUnique();

                entity.Property(e => e.EmployerJobId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EmployerJobTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursFrom)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursTo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobDescription)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.JobPostedDate).HasColumnType("datetime");

                entity.Property(e => e.JobRequirements)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.OpenedFrom).HasColumnType("datetime");

                entity.Property(e => e.OpenedTo).HasColumnType("datetime");

                entity.Property(e => e.SalaryFrom)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryTo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryTypeId).HasColumnName("SalaryTypeID");

                entity.Property(e => e.Start).HasColumnType("datetime");

                entity.Property(e => e.WorkDays)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_plEmployerJobs_adCounties_AreaId_CountyId");

                entity.HasOne(d => d.Benefits)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.BenefitsId)
                    .HasConstraintName("FK_plEmployerJobs_plJobBenefit_BenefitsId_JobBenefitId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plEmployerJobs_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_plEmployerJobs_plEmployerContact_ContactId_EmployerContactId");

                entity.HasOne(d => d.Employer)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.EmployerId)
                    .HasConstraintName("FK_plEmployerJobs_plEmployers_EmployerId_EmployerId");

                entity.HasOne(d => d.Expertise)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.ExpertiseId)
                    .HasConstraintName("FK_plEmployerJobs_adExpertiseLevel_ExpertiseId_ExpertiseId");

                entity.HasOne(d => d.Fee)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.FeeId)
                    .HasConstraintName("FK_plEmployerJobs_plFee_FeeId_FeeId");

                entity.HasOne(d => d.JobGroup)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.JobGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plEmployerJobs_plJobCats_JobGroupId_JobCatId");

                entity.HasOne(d => d.JobTitle)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.JobTitleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plEmployerJobs_adTitles_JobTitleId_TitleId");

                entity.HasOne(d => d.SalaryType)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.SalaryTypeId)
                    .HasConstraintName("FK_plEmployerJobs_plSalaryType_SalaryTypeID_SalaryTypeId");

                entity.HasOne(d => d.Schedule)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.ScheduleId)
                    .HasConstraintName("FK_plEmployerJobs_plJobSchedule_ScheduleId_JobScheduleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plEmployerJobs_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.PlEmployerJobs)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_plEmployerJobs_plJobType_TypeId_JobGroupId");
            });

            modelBuilder.Entity<PlEmployers>(entity =>
            {
                entity.HasKey(e => e.EmployerId);

                entity.ToTable("plEmployers");

                entity.HasIndex(e => e.CampGrpId);

                entity.HasIndex(e => e.CountyId);

                entity.HasIndex(e => e.IndustryId);

                entity.HasIndex(e => e.LocationId);

                entity.HasIndex(e => e.StateId);

                entity.HasIndex(e => new { e.Code, e.EmployerDescrip })
                    .HasName("UIX_plEmployers_Code_EmployerDescrip")
                    .IsUnique();

                entity.Property(e => e.EmployerId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployerDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignFax).HasDefaultValueSql("((0))");

                entity.Property(e => e.ForeignPhone).HasDefaultValueSql("((0))");

                entity.Property(e => e.ForeignZip).HasDefaultValueSql("((0))");

                entity.Property(e => e.GroupName).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plEmployers_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_plEmployers_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_plEmployers_adCounties_CountyId_CountyId");

                entity.HasOne(d => d.Fee)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.FeeId)
                    .HasConstraintName("FK_plEmployers_plFee_FeeId_FeeId");

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("FK_plEmployers_plIndustries_IndustryId_IndustryId");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_plEmployers_plLocations_LocationId_LocationId");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_plEmployers_plEmployers_ParentId_EmployerId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_plEmployers_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlEmployers)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plEmployers_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlExitInterview>(entity =>
            {
                entity.HasKey(e => e.ExtInterviewId);

                entity.ToTable("plExitInterview");

                entity.Property(e => e.ExtInterviewId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AvailableDate).HasColumnType("datetime");

                entity.Property(e => e.AvailableDays)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AvailableHours)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Eligible)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HighSalary)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LowSalary)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WaiverSigned)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WantAssistance)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_plExitInterview_adCounties_AreaId_CountyId");

                entity.HasOne(d => d.Enrollment)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.EnrollmentId)
                    .HasConstraintName("FK_plExitInterview_arStuEnrollments_EnrollmentId_StuEnrollId");

                entity.HasOne(d => d.ExpertiseLevel)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.ExpertiseLevelId)
                    .HasConstraintName("FK_plExitInterview_adExpertiseLevel_ExpertiseLevelId_ExpertiseId");

                entity.HasOne(d => d.FullTime)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.FullTimeId)
                    .HasConstraintName("FK_plExitInterview_adFullPartTime_FullTimeId_FullTimeId");

                entity.HasOne(d => d.ScStatus)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.ScStatusId)
                    .HasConstraintName("FK_plExitInterview_syStatuses_ScStatusId_StatusId");

                entity.HasOne(d => d.Transportation)
                    .WithMany(p => p.PlExitInterview)
                    .HasForeignKey(d => d.TransportationId)
                    .HasConstraintName("FK_plExitInterview_plTransportation_TransportationId_TransportationId");
            });

            modelBuilder.Entity<PlFee>(entity =>
            {
                entity.HasKey(e => e.FeeId);

                entity.ToTable("plFee");

                entity.Property(e => e.FeeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FeeCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FeeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlFee)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plFee_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlFee)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plFee_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlFldStudy>(entity =>
            {
                entity.HasKey(e => e.FldStudyId);

                entity.ToTable("plFldStudy");

                entity.Property(e => e.FldStudyId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FldStudyCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FldStudyDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlFldStudy)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plFldStudy_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlFldStudy)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plFldStudy_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlHowPlaced>(entity =>
            {
                entity.HasKey(e => e.HowPlacedId);

                entity.ToTable("plHowPlaced");

                entity.Property(e => e.HowPlacedId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.HowPlacedCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.HowPlacedDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlHowPlaced)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plHowPlaced_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlHowPlaced)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plHowPlaced_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlIndustries>(entity =>
            {
                entity.HasKey(e => e.IndustryId);

                entity.ToTable("plIndustries");

                entity.HasIndex(e => new { e.IndustryCode, e.IndustryDescrip, e.CampGrpId })
                    .HasName("UIX_plIndustries_IndustryCode_IndustryDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.IndustryId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.IndustryCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.IndustryDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlIndustries)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plIndustries_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlIndustries)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plIndustries_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlInterview>(entity =>
            {
                entity.HasKey(e => e.InterviewId);

                entity.ToTable("plInterview");

                entity.Property(e => e.InterviewId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.InterViewCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.InterviewDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlInterview)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plInterview_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlInterview)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plInterview_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobBenefit>(entity =>
            {
                entity.HasKey(e => e.JobBenefitId);

                entity.ToTable("plJobBenefit");

                entity.Property(e => e.JobBenefitId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.JobBenefitCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobBenefitDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CamGrp)
                    .WithMany(p => p.PlJobBenefit)
                    .HasForeignKey(d => d.CamGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobBenefit_syCampGrps_CamGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlJobBenefit)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobBenefit_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobCats>(entity =>
            {
                entity.HasKey(e => e.JobCatId);

                entity.ToTable("plJobCats");

                entity.HasIndex(e => new { e.JobCatCode, e.JobCatDescrip, e.CampGrpId })
                    .HasName("UIX_plJobCats_JobCatCode_JobCatDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.JobCatId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.JobCatCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.JobCatDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlJobCats)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobCats_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlJobCats)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobCats_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobSchedule>(entity =>
            {
                entity.HasKey(e => e.JobScheduleId);

                entity.ToTable("plJobSchedule");

                entity.Property(e => e.JobScheduleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.JobScheduleCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobScheduleDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CamGrp)
                    .WithMany(p => p.PlJobSchedule)
                    .HasForeignKey(d => d.CamGrpId)
                    .HasConstraintName("FK_plJobSchedule_syCampGrps_CamGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlJobSchedule)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobSchedule_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobStatus>(entity =>
            {
                entity.HasKey(e => e.JobStatusId);

                entity.ToTable("plJobStatus");

                entity.HasIndex(e => new { e.JobStatusDescrip, e.JobCode })
                    .HasName("UIX_plJobStatus_JobStatusDescrip_JobCode")
                    .IsUnique();

                entity.Property(e => e.JobStatusId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.JobCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.JobStatusDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlJobStatus)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plJobStatus_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlJobStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobStatus_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobType>(entity =>
            {
                entity.HasKey(e => e.JobGroupId);

                entity.ToTable("plJobType");

                entity.Property(e => e.JobGroupId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.JobGroupCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JobGroupDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CamGrp)
                    .WithMany(p => p.PlJobType)
                    .HasForeignKey(d => d.CamGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobType_syCampGrps_CamGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlJobType)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plJobType_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlJobWorkDays>(entity =>
            {
                entity.HasKey(e => e.JobWorkDaysId);

                entity.ToTable("plJobWorkDays");

                entity.Property(e => e.JobWorkDaysId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.EmployerJob)
                    .WithMany(p => p.PlJobWorkDays)
                    .HasForeignKey(d => d.EmployerJobId)
                    .HasConstraintName("FK_plJobWorkDays_plEmployerJobs_EmployerJobId_EmployerJobId");
            });

            modelBuilder.Entity<PlLocations>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.ToTable("plLocations");

                entity.HasIndex(e => new { e.LocationCode, e.LocationDescrip, e.CampGrpId })
                    .HasName("UIX_plLocations_LocationCode_LocationDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.LocationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.LocationCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.LocationDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlLocations)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plLocations_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlLocations)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plLocations_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlSalaryType>(entity =>
            {
                entity.HasKey(e => e.SalaryTypeId);

                entity.ToTable("plSalaryType");

                entity.Property(e => e.SalaryTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryTypeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlSalaryType)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plSalaryType_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlSalaryType)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plSalaryType_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlSkillGroups>(entity =>
            {
                entity.HasKey(e => e.SkillGrpId);

                entity.ToTable("plSkillGroups");

                entity.HasIndex(e => new { e.SkillGrpCode, e.SkillGrpName, e.CampGrpId })
                    .HasName("UIX_plSkillGroups_SkillGrpCode_SkillGrpName_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.SkillGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SkillGrpCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SkillGrpName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlSkillGroups)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plSkillGroups_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlSkillGroups)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plSkillGroups_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlSkills>(entity =>
            {
                entity.HasKey(e => e.SkillId);

                entity.ToTable("plSkills");

                entity.HasIndex(e => new { e.SkillCode, e.SkillDescrip, e.CampGrpId })
                    .HasName("UIX_plSkills_SkillCode_SkillDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.SkillId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SkillCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SkillDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlSkills)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plSkills_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.SkillGrp)
                    .WithMany(p => p.PlSkills)
                    .HasForeignKey(d => d.SkillGrpId)
                    .HasConstraintName("FK_plSkills_plSkillGroups_SkillGrpId_SkillGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlSkills)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plSkills_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlStudentDocs>(entity =>
            {
                entity.HasKey(e => e.StudentDocId);

                entity.ToTable("plStudentDocs");

                entity.HasIndex(e => new { e.DocumentId, e.StudentId, e.DocStatusId })
                    .HasName("IX_plStudentDocs_StudentId_DocStatusId_DocumentId");

                entity.Property(e => e.StudentDocId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ImgContenttype)
                    .HasColumnName("img_contenttype")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImgData)
                    .HasColumnName("img_data")
                    .HasColumnType("image");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ReceiveDate).HasColumnType("datetime");

                entity.Property(e => e.RequestDate).HasColumnType("datetime");

                entity.HasOne(d => d.DocStatus)
                    .WithMany(p => p.PlStudentDocs)
                    .HasForeignKey(d => d.DocStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plStudentDocs_syDocStatuses_DocStatusId_DocStatusId");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.PlStudentDocs)
                    .HasForeignKey(d => d.DocumentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_plStudentDocs_adReqs_DocumentId_adReqId");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.PlStudentDocs)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_plStudentDocs_syModules_ModuleID_ModuleID");
            });

            modelBuilder.Entity<PlStudentsPlaced>(entity =>
            {
                entity.HasKey(e => e.PlacementId);

                entity.Property(e => e.PlacementId).ValueGeneratedNever();

                entity.Property(e => e.GraduatedDate).HasColumnType("datetime");

                entity.Property(e => e.JobDescrip)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PlacedDate).HasColumnType("datetime");

                entity.Property(e => e.Reason)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Supervisor)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.TerminationDate).HasColumnType("datetime");

                entity.Property(e => e.TerminationReason)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Benefits)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.BenefitsId)
                    .HasConstraintName("FK_PlStudentsPlaced_plJobBenefit_BenefitsId_JobBenefitId");

                entity.HasOne(d => d.EmployerJob)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.EmployerJobId)
                    .HasConstraintName("FK_PlStudentsPlaced_plEmployerJobs_EmployerJobId_EmployerJobId");

                entity.HasOne(d => d.FeeNavigation)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.Fee)
                    .HasConstraintName("FK_PlStudentsPlaced_plFee_Fee_FeeId");

                entity.HasOne(d => d.FldStudy)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.FldStudyId)
                    .HasConstraintName("FK_PlStudentsPlaced_plFldStudy_FldStudyId_FldStudyId");

                entity.HasOne(d => d.HowPlaced)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.HowPlacedId)
                    .HasConstraintName("FK_PlStudentsPlaced_plHowPlaced_HowPlacedId_HowPlacedId");

                entity.HasOne(d => d.Interview)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.InterviewId)
                    .HasConstraintName("FK_PlStudentsPlaced_plInterview_InterviewId_InterviewId");

                entity.HasOne(d => d.JobStatus)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.JobStatusId)
                    .HasConstraintName("FK_PlStudentsPlaced_plJobStatus_JobStatusId_JobStatusId");

                entity.HasOne(d => d.PlacementRepNavigation)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.PlacementRep)
                    .HasConstraintName("FK_PlStudentsPlaced_syUsers_PlacementRep_UserId");

                entity.HasOne(d => d.SalaryType)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.SalaryTypeId)
                    .HasConstraintName("FK_PlStudentsPlaced_plSalaryType_SalaryTypeId_SalaryTypeId");

                entity.HasOne(d => d.Schedule)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.ScheduleId)
                    .HasConstraintName("FK_PlStudentsPlaced_plJobSchedule_ScheduleId_JobScheduleId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.StuEnrollId)
                    .HasConstraintName("FK_PlStudentsPlaced_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.WorkDays)
                    .WithMany(p => p.PlStudentsPlaced)
                    .HasForeignKey(d => d.WorkDaysId)
                    .HasConstraintName("FK_PlStudentsPlaced_plJobWorkDays_WorkDaysId_JobWorkDaysId");
            });

            modelBuilder.Entity<PlTransportation>(entity =>
            {
                entity.HasKey(e => e.TransportationId);

                entity.ToTable("plTransportation");

                entity.HasIndex(e => new { e.TransportationCode, e.TransportationDescrip, e.CampGrpId })
                    .HasName("UIX_plTransportation_TransportationCode_TransportationDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.TransportationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TransportationDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.PlTransportation)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_plTransportation_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.PlTransportation)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_plTransportation_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<PlWorkDays>(entity =>
            {
                entity.HasKey(e => e.WorkDaysId);

                entity.ToTable("plWorkDays");

                entity.Property(e => e.WorkDaysId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.WorkDaysDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PriorWorkAddress>(entity =>
            {
                entity.Property(e => e.Address1)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Address2)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Apartment)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StateInternational)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.PriorWorkAddress)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_PriorWorkAddress_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.StEmployment)
                    .WithMany(p => p.PriorWorkAddress)
                    .HasForeignKey(d => d.StEmploymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PriorWorkAddress_adLeadEmployment_StEmploymentId_StEmploymentId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.PriorWorkAddress)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_PriorWorkAddress_syStates_StateId_StateId");
            });

            modelBuilder.Entity<PriorWorkContact>(entity =>
            {
                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.MiddleName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.PriorWorkContact)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_PriorWorkContact_syPrefixes_PrefixId_PrefixId");

                entity.HasOne(d => d.StEmployment)
                    .WithMany(p => p.PriorWorkContact)
                    .HasForeignKey(d => d.StEmploymentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PriorWorkContact_adLeadEmployment_StEmploymentId_StEmploymentId");
            });

            modelBuilder.Entity<PrivateSchoolsList>(entity =>
            {
                entity.HasKey(e => e.InstId);

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AlternativeSchoolNetwork)
                    .HasColumnName("\"Alternative School Network\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AmericanMontessoriSociety)
                    .HasColumnName("\"American Montessori Society\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AnsiFipsCountyNumber)
                    .HasColumnName("\"Ansi Fips County Number\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AssociationMilitaryCollegesAndSchools)
                    .HasColumnName("\"Association Military Colleges And Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AssociationMontessoriInternational)
                    .HasColumnName("\"Association Montessori International\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AssociationWaldorfSchoolsOfNorthAmerica)
                    .HasColumnName("\"Association Waldorf Schools of North America\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Coed)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaysInSchoolYear)
                    .HasColumnName("\"Days In School Year\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EuropeanCouncilForInternationalSchools)
                    .HasColumnName("\"European Council For International Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullTimeEquivalentTeachers)
                    .HasColumnName("\"Full Time Equivalent Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HasReligiousOrientation)
                    .HasColumnName("\"Has Religious Orientation\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HighGrade)
                    .HasColumnName("High Grade")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HomeSchooling)
                    .HasColumnName("\"Home Schooling\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursInDay)
                    .HasColumnName("\"Hours In Day\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LessThan14TimeTeachers)
                    .HasColumnName("\"Less Than 1 4 Time Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Library)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocatedInHome)
                    .HasColumnName("\"Located In Home\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LowGrade)
                    .HasColumnName("\"Low Grade\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress)
                    .HasColumnName("\"Mailing Address\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCity)
                    .HasColumnName("\"Mailing City\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingState)
                    .HasColumnName("\"Mailing State\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingZip)
                    .HasColumnName("\"Mailing Zip\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingZip4)
                    .HasColumnName("\"Mailing Zip4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MinutesInDay)
                    .HasColumnName("\"Minutes In Day\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoreThan12TimeTeachers)
                    .HasColumnName("\"More Than 1 2 Time Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoreThan14TimeTeachers)
                    .HasColumnName("\"More Than 1 4 Time Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MoreThan34TimeTeachers)
                    .HasColumnName("\"More Than 3 4 Time Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalAssociationForTheEduOfYoungChildren)
                    .HasColumnName("\"National Association For The Edu  of Young Children\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalAssociationOfIndependentSchools)
                    .HasColumnName("\"National Association of Independent Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalAssociationOfLaboratorySchools)
                    .HasColumnName("\"National Association of Laboratory Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalAssociationOfPrivateSpecialEduCenters)
                    .HasColumnName("\"National Association of Private Special Edu  Centers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCoalitionOfAlternativeCommunitySchools)
                    .HasColumnName("\"National Coalition of Alternative Community Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalCoalitionOfGirlsSchools)
                    .HasColumnName("\"National Coalition of Girls Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalIndependentPrivateSchoolsAssociation)
                    .HasColumnName("\"National Independent Private Schools Association\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NcesId)
                    .HasColumnName("Nces Id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered1)
                    .HasColumnName("\"Offered 1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered10)
                    .HasColumnName("\"Offered 10\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered11)
                    .HasColumnName("\"Offered 11\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered12)
                    .HasColumnName("\"Offered 12\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered2)
                    .HasColumnName("\"Offered 2\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered3)
                    .HasColumnName("\"Offered 3\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered4)
                    .HasColumnName("\"Offered 4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered5)
                    .HasColumnName("\"Offered 5\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered6)
                    .HasColumnName("\"Offered 6\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered7)
                    .HasColumnName("\"Offered 7\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered8)
                    .HasColumnName("\"Offered 8\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered9)
                    .HasColumnName("\"Offered 9\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedK)
                    .HasColumnName("\"Offered K\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedPrek)
                    .HasColumnName("\"Offered Prek\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedT1)
                    .HasColumnName("\"Offered T1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedTk)
                    .HasColumnName("\"Offered Tk\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedUg)
                    .HasColumnName("\"Offered Ug\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherAssociationsForExceptionalChildren)
                    .HasColumnName("\"Other Associations For Exceptional Children\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherMontessoriAssociations)
                    .HasColumnName("\"Other Montessori associations\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherSchoolAssociations)
                    .HasColumnName("\"Other school associations\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherSpecialEmphasisAssociations)
                    .HasColumnName("\"Other Special Emphasis Associations\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PercentTo4yearCollege)
                    .HasColumnName("\"Percent To 4year College\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrekKT1DayLength)
                    .HasColumnName("\"Prek K T1 Day Length\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrekKT1DaysPerWeek)
                    .HasColumnName("\"Prek K T1 Days Per Week\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReligiousAffiliation)
                    .HasColumnName("\"Religious Affiliation\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolCounty)
                    .HasColumnName("\"School County\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolLevel)
                    .HasColumnName("School Level")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolName)
                    .HasColumnName("School Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolType)
                    .HasColumnName("\"School Type\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateFips)
                    .HasColumnName("\"State Fips\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateOrRegionalIndependentSchoolAssociation)
                    .HasColumnName("\"State or regional independent school association\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentTeacherRatio)
                    .HasColumnName("\"Student Teacher Ratio\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students1)
                    .HasColumnName("\"Students 1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students10)
                    .HasColumnName("\"Students 10\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students11)
                    .HasColumnName("\"Students 11\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students12)
                    .HasColumnName("\"Students 12\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students2)
                    .HasColumnName("\"Students 2\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students3)
                    .HasColumnName("\"Students 3\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students4)
                    .HasColumnName("\"Students 4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students5)
                    .HasColumnName("\"Students 5\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students6)
                    .HasColumnName("\"Students 6\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students7)
                    .HasColumnName("\"Students 7\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students8)
                    .HasColumnName("\"Students 8\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students9)
                    .HasColumnName("\"Students 9\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsK)
                    .HasColumnName("\"Students K\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsPrek)
                    .HasColumnName("\"Students Prek\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsT1)
                    .HasColumnName("\"Students T1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsTk)
                    .HasColumnName("\"Students Tk\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsUg)
                    .HasColumnName("\"Students Ug\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TheAssociationOfBoardingSchools)
                    .HasColumnName("\"The Association of Boarding Schools\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Total2race)
                    .HasColumnName("\"Total 2race\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmericanIndianAlaskan)
                    .HasColumnName("\"Total American Indian Alaskan\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAsian)
                    .HasColumnName("\"Total Asian\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalBlack)
                    .HasColumnName("\"Total Black\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalFemale)
                    .HasColumnName("\"Total Female\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalHispanic)
                    .HasColumnName("\"Total Hispanic\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalMale)
                    .HasColumnName("\"Total Male\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPacific)
                    .HasColumnName("\"Total Pacific\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalStudents)
                    .HasColumnName("\"Total Students\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalTeachers)
                    .HasColumnName("\"Total Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalWhite)
                    .HasColumnName("\"Total White\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip4)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PublicSchoolsList>(entity =>
            {
                entity.HasKey(e => e.InstId);

                entity.Property(e => e.Address)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AnsiFipsCountyNumber)
                    .HasColumnName("\"Ansi Fips County Number\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BureauOfIndian)
                    .HasColumnName("\"Bureau Of Indian\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CharterSchool)
                    .HasColumnName("\"Charter School\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountFreeLunch)
                    .HasColumnName("\"Count Free Lunch\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountReducedLunch)
                    .HasColumnName("\"Count Reduced Lunch\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictCode112)
                    .HasColumnName("\"District Code 112\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DistrictCode113)
                    .HasColumnName("\"District Code 113\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FreeLunchEligible)
                    .HasColumnName("\"Free Lunch Eligible\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FreeReducedLunchTotal)
                    .HasColumnName("\"Free & Reduced Lunch Total\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullTimeEquivalentTeachers)
                    .HasColumnName("\"Full Time Equivalent Teachers\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HighGrade)
                    .HasColumnName("High Grade")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Latitude)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalEducationAgencyId)
                    .HasColumnName("\"Local Education Agency Id\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LocalSchoolId)
                    .HasColumnName("\"Local School Id\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LowGrade)
                    .HasColumnName("\"Low Grade\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MagnetSchool)
                    .HasColumnName("\"Magnet School\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress)
                    .HasColumnName("\"Mailing Address\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCity)
                    .HasColumnName("\"Mailing City\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingState)
                    .HasColumnName("\"Mailing State\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingZip)
                    .HasColumnName("\"Mailing Zip\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailingZip4)
                    .HasColumnName("\"Mailing Zip4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NcesId)
                    .HasColumnName("Nces Id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered1)
                    .HasColumnName("\"Offered 1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered10)
                    .HasColumnName("\"Offered 10\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered11)
                    .HasColumnName("\"Offered 11\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered12)
                    .HasColumnName("\"Offered 12\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered2)
                    .HasColumnName("\"Offered 2\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered3)
                    .HasColumnName("\"Offered 3\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered4)
                    .HasColumnName("\"Offered 4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered5)
                    .HasColumnName("\"Offered 5\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered6)
                    .HasColumnName("\"Offered 6\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered7)
                    .HasColumnName("\"Offered 7\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered8)
                    .HasColumnName("\"Offered 8\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Offered9)
                    .HasColumnName("\"Offered 9\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedK)
                    .HasColumnName("\"Offered K\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedPrek)
                    .HasColumnName("\"Offered Prek\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfferedUg)
                    .HasColumnName("\"Offered Ug\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperationalStatus)
                    .HasColumnName("\"Operational Status\"")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReducePriceLunchEligible)
                    .HasColumnName("\"Reduce Price Lunch Eligible\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolCounty)
                    .HasColumnName("School County")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolDistrict)
                    .HasColumnName("School District")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolLevel)
                    .HasColumnName("School Level")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolName)
                    .HasColumnName("School Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolType)
                    .HasColumnName("\"School Type\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SharedTimeSchool)
                    .HasColumnName("\"Shared Time School\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateDistrictId)
                    .HasColumnName("\"State District Id\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateFips)
                    .HasColumnName("\"State Fips\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StateSchoolId)
                    .HasColumnName("\"State School Id\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentTeacherRatio)
                    .HasColumnName("\"Student Teacher Ratio\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students1)
                    .HasColumnName("\"Students 1\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students10)
                    .HasColumnName("\"Students 10\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students11)
                    .HasColumnName("\"Students 11\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students12)
                    .HasColumnName("\"Students 12\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students2)
                    .HasColumnName("\"Students 2\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students3)
                    .HasColumnName("\"Students 3\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students4)
                    .HasColumnName("\"Students 4\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students5)
                    .HasColumnName("\"Students 5\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students6)
                    .HasColumnName("\"Students 6\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students7)
                    .HasColumnName("\"Students 7\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students8)
                    .HasColumnName("\"Students 8\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Students9)
                    .HasColumnName("\"Students 9\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsK)
                    .HasColumnName("\"Students K\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsPrek)
                    .HasColumnName("\"Students Prek\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentsUg)
                    .HasColumnName("\"Students Ug\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title1Eligible)
                    .HasColumnName("\"Title 1 Eligible\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title1SchoolWide)
                    .HasColumnName("\"Title 1 School Wide\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title1Status)
                    .HasColumnName("\"Title 1 Status\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Total2race)
                    .HasColumnName("\"Total 2race\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmericanIndianAlaskan)
                    .HasColumnName("\"Total American Indian Alaskan\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAsian)
                    .HasColumnName("\"Total Asian\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalBlack)
                    .HasColumnName("\"Total Black\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalFemale)
                    .HasColumnName("\"Total Female\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalFreeReducedLunch)
                    .HasColumnName("\"Total Free & Reduced Lunch\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalHispanic)
                    .HasColumnName("\"Total Hispanic\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalMale)
                    .HasColumnName("\"Total Male\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalPacific)
                    .HasColumnName("\"Total Pacific\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalStudents)
                    .HasColumnName("\"Total Students\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TotalWhite)
                    .HasColumnName("\"Total White\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UnionIdentificationNumber)
                    .HasColumnName("\"Union Identification Number\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrbanLocaleCode)
                    .HasColumnName("\"Urban Locale Code\"")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip4)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<References>(entity =>
            {
                entity.HasKey(e => e.ReferenceId);

                entity.Property(e => e.ReferenceId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ForEmergency)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProspectId)
                    .HasColumnName("ProspectID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Relationship)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StudentContactId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Zip)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RptAdmissionsRep>(entity =>
            {
                entity.ToTable("rptAdmissionsRep");

                entity.Property(e => e.RptAdmissionsRepId)
                    .HasColumnName("rptAdmissionsRepId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdmissionsRepDescrip)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AdmissionsRepId)
                    .HasColumnName("AdmissionsRepID")
                    .HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.AdmissionsRep)
                    .WithMany(p => p.RptAdmissionsRep)
                    .HasForeignKey(d => d.AdmissionsRepId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_rptAdmissionsRep_syUsers_AdmissionsRepID_UserId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.RptAdmissionsRep)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_rptAdmissionsRep_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.RptAdmissionsRep)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_rptAdmissionsRep_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<RptInstructor>(entity =>
            {
                entity.HasKey(e => e.InstructorId);

                entity.ToTable("rptInstructor");

                entity.Property(e => e.InstructorId)
                    .HasColumnName("InstructorID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.InstructorDescrip)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RptLeadStatus>(entity =>
            {
                entity.HasKey(e => e.StatusCodeId);

                entity.ToTable("rptLeadStatus");

                entity.Property(e => e.StatusCodeId).ValueGeneratedNever();

                entity.Property(e => e.StatusCodeDescrip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.RptLeadStatus)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_rptLeadStatus_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.RptLeadStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_rptLeadStatus_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<RptStuEnrollmentStatus>(entity =>
            {
                entity.HasKey(e => e.StatusCodeId);

                entity.ToTable("rptStuEnrollmentStatus");

                entity.Property(e => e.StatusCodeId).ValueGeneratedNever();

                entity.Property(e => e.StatusCodeDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaAcademicYears>(entity =>
            {
                entity.HasKey(e => e.AcademicYearId);

                entity.ToTable("saAcademicYears");

                entity.HasIndex(e => new { e.AcademicYearCode, e.AcademicYearDescrip, e.CampGrpId })
                    .HasName("UIX_saAcademicYears_AcademicYearCode_AcademicYearDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.AcademicYearId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AcademicYearCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.AcademicYearDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaAcademicYears)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saAcademicYears_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaAcademicYears)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saAcademicYears_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaAdmissionDeposits>(entity =>
            {
                entity.HasKey(e => e.AdmDepositId);

                entity.ToTable("saAdmissionDeposits");

                entity.Property(e => e.AdmDepositId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AdmDepositDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CheckNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DepositDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.SaAdmissionDeposits)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saAdmissionDeposits_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<SaAppliedPayments>(entity =>
            {
                entity.HasKey(e => e.AppliedPmtId);

                entity.ToTable("saAppliedPayments");

                entity.HasIndex(e => e.TransactionId);

                entity.Property(e => e.AppliedPmtId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaAwardTypes>(entity =>
            {
                entity.HasKey(e => e.AwardTypeId);

                entity.ToTable("saAwardTypes");

                entity.Property(e => e.AwardTypeId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaBankAccounts>(entity =>
            {
                entity.HasKey(e => e.BankAcctId);

                entity.ToTable("saBankAccounts");

                entity.HasIndex(e => new { e.BankRoutingNumber, e.BankAcctNumber })
                    .HasName("UIX_saBankAccounts_BankRoutingNumber_BankAcctNumber")
                    .IsUnique();

                entity.HasIndex(e => new { e.BankAcctDescrip, e.StatusId, e.CampGrpId })
                    .HasName("UIX_saBankAccounts_BankAcctDescrip_StatusId_CampGrpId")
                    .IsUnique();

                entity.HasIndex(e => new { e.BankAcctNumber, e.StatusId, e.CampGrpId })
                    .HasName("UIX_saBankAccounts_BankAcctNumber_StatusId_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.BankAcctId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.BankAcctDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankAcctNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankRoutingNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.SaBankAccounts)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBankAccounts_saBankCodes_BankId_BankId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaBankAccounts)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saBankAccounts_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaBankAccounts)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBankAccounts_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaBankCodes>(entity =>
            {
                entity.HasKey(e => e.BankId);

                entity.ToTable("saBankCodes");

                entity.HasIndex(e => new { e.Code, e.BankDescrip, e.CampGrpId })
                    .HasName("UIX_saBankCodes_Code_BankDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.BankId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BankDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignFax).HasDefaultValueSql("((0))");

                entity.Property(e => e.ForeignPhone).HasDefaultValueSql("((0))");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaBankCodes)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saBankCodes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SaBankCodes)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_saBankCodes_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaBankCodes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBankCodes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaBatchPayments>(entity =>
            {
                entity.HasKey(e => e.BatchPaymentId);

                entity.ToTable("saBatchPayments");

                entity.HasIndex(e => e.BatchPaymentDate);

                entity.HasIndex(e => e.BatchPaymentNumber)
                    .HasName("UIX_saBatchPayments_BatchPaymentNumber")
                    .IsUnique();

                entity.Property(e => e.BatchPaymentId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.BatchPaymentAmount)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.BatchPaymentDate).HasColumnType("datetime");

                entity.Property(e => e.BatchPaymentDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BatchPaymentNumber)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.FundSource)
                    .WithMany(p => p.SaBatchPayments)
                    .HasForeignKey(d => d.FundSourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBatchPayments_saFundSources_FundSourceId_FundSourceId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SaBatchPayments)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBatchPayments_syUsers_UserId_UserId");
            });

            modelBuilder.Entity<SaBillingMethods>(entity =>
            {
                entity.HasKey(e => e.BillingMethodId);

                entity.ToTable("saBillingMethods");

                entity.HasIndex(e => new { e.BillingMethodCode, e.BillingMethodDescrip, e.CampGrpId })
                    .HasName("UIX_saBillingMethods_BillingMethodCode_BillingMethodDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.BillingMethodId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.BillingMethodCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.BillingMethodDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaBillingMethods)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saBillingMethods_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaBillingMethods)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saBillingMethods_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaBillTypes>(entity =>
            {
                entity.HasKey(e => e.BillTypeId);

                entity.ToTable("saBillTypes");

                entity.Property(e => e.BillTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.BillTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaCourseFees>(entity =>
            {
                entity.HasKey(e => e.CourseFeeId);

                entity.ToTable("saCourseFees");

                entity.Property(e => e.CourseFeeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.SaCourseFees)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saCourseFees_arReqs_CourseId_ReqId");

                entity.HasOne(d => d.RateSchedule)
                    .WithMany(p => p.SaCourseFees)
                    .HasForeignKey(d => d.RateScheduleId)
                    .HasConstraintName("FK_saCourseFees_saRateSchedules_RateScheduleId_RateScheduleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaCourseFees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saCourseFees_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaCourseFees)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saCourseFees_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.SaCourseFees)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_saCourseFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");
            });

            modelBuilder.Entity<SaDeferredRevenues>(entity =>
            {
                entity.HasKey(e => e.DefRevenueId);

                entity.ToTable("saDeferredRevenues");

                entity.HasIndex(e => e.TransactionId);

                entity.Property(e => e.DefRevenueId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.DefRevenueDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.SaDeferredRevenues)
                    .HasForeignKey(d => d.TransactionId)
                    .HasConstraintName("FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId");
            });

            modelBuilder.Entity<SaFeeLevels>(entity =>
            {
                entity.HasKey(e => e.FeeLevelId);

                entity.ToTable("saFeeLevels");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaFundSources>(entity =>
            {
                entity.HasKey(e => e.FundSourceId);

                entity.ToTable("saFundSources");

                entity.HasIndex(e => new { e.FundSourceCode, e.FundSourceDescrip, e.CampGrpId })
                    .HasName("UIX_saFundSources_FundSourceCode_FundSourceDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.FundSourceId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AwardYear)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CutoffDate).HasColumnType("datetime");

                entity.Property(e => e.FundSourceCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.FundSourceDescrip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TitleIv)
                    .HasColumnName("TitleIV")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.AdvFundSource)
                    .WithMany(p => p.SaFundSources)
                    .HasForeignKey(d => d.AdvFundSourceId)
                    .HasConstraintName("FK_saFundSources_syAdvFundSources_AdvFundSourceId_AdvFundSourceId");

                entity.HasOne(d => d.AwardType)
                    .WithMany(p => p.SaFundSources)
                    .HasForeignKey(d => d.AwardTypeId)
                    .HasConstraintName("FK_saFundSources_saAwardTypes_AwardTypeId_AwardTypeId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaFundSources)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saFundSources_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaFundSources)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saFundSources_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaGlaccounts>(entity =>
            {
                entity.HasKey(e => e.GlaccountId);

                entity.ToTable("saGLAccounts");

                entity.Property(e => e.GlaccountId)
                    .HasColumnName("GLAccountId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.GlaccountCode)
                    .IsRequired()
                    .HasColumnName("GLAccountCode")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GlaccountDescription)
                    .IsRequired()
                    .HasColumnName("GLAccountDescription")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaGlaccounts)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saGLAccounts_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaGlaccounts)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saGLAccounts_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaGldistributions>(entity =>
            {
                entity.HasKey(e => e.GldistributionId);

                entity.ToTable("saGLDistributions");

                entity.Property(e => e.GldistributionId)
                    .HasColumnName("GLDistributionId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.GlaccountId).HasColumnName("GLAccountId");

                entity.Property(e => e.Glamount)
                    .HasColumnName("GLAmount")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Gldate)
                    .HasColumnName("GLDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Gldescrip)
                    .IsRequired()
                    .HasColumnName("GLDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Glaccount)
                    .WithMany(p => p.SaGldistributions)
                    .HasForeignKey(d => d.GlaccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saGLDistributions_saGLAccounts_GLAccountId_GLAccountId");

                entity.HasOne(d => d.Transaction)
                    .WithMany(p => p.SaGldistributions)
                    .HasForeignKey(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saGLDistributions_saTransactions_TransactionId_TransactionId");
            });

            modelBuilder.Entity<SaIncrements>(entity =>
            {
                entity.HasKey(e => e.IncrementId);

                entity.ToTable("saIncrements");

                entity.HasIndex(e => e.BillingMethodId)
                    .HasName("UIX_saIncrements_BillingMethodId")
                    .IsUnique();

                entity.Property(e => e.IncrementId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EffectiveDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExcAbscenesPercent).HasColumnType("decimal(6, 4)");

                entity.Property(e => e.IncrementName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser).HasMaxLength(50);

                entity.HasOne(d => d.BillingMethod)
                    .WithOne(p => p.SaIncrements)
                    .HasForeignKey<SaIncrements>(d => d.BillingMethodId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saIncrements_saBillingMethods_BillingMethodId_BillingMethodId");
            });

            modelBuilder.Entity<SaLateFees>(entity =>
            {
                entity.HasKey(e => e.LateFeesId);

                entity.ToTable("saLateFees");

                entity.Property(e => e.LateFeesId).ValueGeneratedNever();

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.FlatAmount)
                    .HasColumnType("numeric(9, 2)")
                    .HasDefaultValueSql("((0.00))");

                entity.Property(e => e.LateFeesCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.LateFeesDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnType("numeric(6, 2)");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaLateFees)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saLateFees_saTransCodes_TransCodeId_TransCodeId");
            });

            modelBuilder.Entity<SaPaymentDescriptions>(entity =>
            {
                entity.HasKey(e => e.PmtDescriptionId);

                entity.ToTable("saPaymentDescriptions");

                entity.Property(e => e.PmtDescriptionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmtCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PmtDescription)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaPaymentDescriptions)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saPaymentDescriptions_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaPaymentDescriptions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPaymentDescriptions_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaPayments>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.ToTable("saPayments");

                entity.HasIndex(e => e.TransactionId);

                entity.Property(e => e.TransactionId).ValueGeneratedNever();

                entity.Property(e => e.CheckNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.BankAcct)
                    .WithMany(p => p.SaPayments)
                    .HasForeignKey(d => d.BankAcctId)
                    .HasConstraintName("FK_saPayments_saBankAccounts_BankAcctId_BankAcctId");

                entity.HasOne(d => d.PaymentType)
                    .WithMany(p => p.SaPayments)
                    .HasForeignKey(d => d.PaymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPayments_saPaymentTypes_PaymentTypeId_PaymentTypeId");

                entity.HasOne(d => d.Transaction)
                    .WithOne(p => p.SaPayments)
                    .HasForeignKey<SaPayments>(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPayments_saTransactions_TransactionId_TransactionId");
            });

            modelBuilder.Entity<SaPaymentTypes>(entity =>
            {
                entity.HasKey(e => e.PaymentTypeId);

                entity.ToTable("saPaymentTypes");

                entity.Property(e => e.PaymentTypeId).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaPeriodicFees>(entity =>
            {
                entity.HasKey(e => e.PeriodicFeeId);

                entity.ToTable("saPeriodicFees");

                entity.Property(e => e.PeriodicFeeId).ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermStartDate).HasColumnType("datetime");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.PrgVerId)
                    .HasConstraintName("FK_saPeriodicFees_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.ProgTyp)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.ProgTypId)
                    .HasConstraintName("FK_saPeriodicFees_arProgTypes_ProgTypId_ProgTypId");

                entity.HasOne(d => d.RateSchedule)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.RateScheduleId)
                    .HasConstraintName("FK_saPeriodicFees_saRateSchedules_RateScheduleId_RateScheduleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPeriodicFees_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.TermId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPeriodicFees_arTerm_TermId_TermId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPeriodicFees_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.SaPeriodicFees)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_saPeriodicFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");
            });

            modelBuilder.Entity<SaPmtDisbRel>(entity =>
            {
                entity.HasKey(e => e.PmtDisbRelId);

                entity.ToTable("saPmtDisbRel");

                entity.HasIndex(e => e.Amount);

                entity.HasIndex(e => e.AwardScheduleId);

                entity.HasIndex(e => e.TransactionId);

                entity.Property(e => e.PmtDisbRelId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AwardSchedule)
                    .WithMany(p => p.SaPmtDisbRel)
                    .HasForeignKey(d => d.AwardScheduleId)
                    .HasConstraintName("FK_saPmtDisbRel_faStudentAwardSchedule_AwardScheduleId_AwardScheduleId");

                entity.HasOne(d => d.PayPlanSchedule)
                    .WithMany(p => p.SaPmtDisbRel)
                    .HasForeignKey(d => d.PayPlanScheduleId)
                    .HasConstraintName("FK_saPmtDisbRel_faStuPaymentPlanSchedule_PayPlanScheduleId_PayPlanScheduleId");
            });

            modelBuilder.Entity<SaPmtPeriodBatchHeaders>(entity =>
            {
                entity.HasKey(e => e.PmtPeriodBatchHeaderId);

                entity.ToTable("saPmtPeriodBatchHeaders");

                entity.Property(e => e.BatchCreationDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.BatchName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SaPmtPeriodBatchItems>(entity =>
            {
                entity.HasKey(e => e.PmtPeriodBatchItemId);

                entity.ToTable("saPmtPeriodBatchItems");

                entity.Property(e => e.ChargeAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CreditsHoursValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IncludedInPost)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionDescription)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionReference)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.HasOne(d => d.PmtPeriodBatchHeader)
                    .WithMany(p => p.SaPmtPeriodBatchItems)
                    .HasForeignKey(d => d.PmtPeriodBatchHeaderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPmtPeriodBatchItems_saPmtPeriodBatchHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId");
            });

            modelBuilder.Entity<SaPmtPeriods>(entity =>
            {
                entity.HasKey(e => e.PmtPeriodId);

                entity.ToTable("saPmtPeriods");

                entity.HasIndex(e => new { e.IncrementId, e.PeriodNumber })
                    .HasName("UIX_saPmtPeriods_IncrementId_PeriodNumber")
                    .IsUnique();

                entity.Property(e => e.PmtPeriodId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ChargeAmount).HasColumnType("money");

                entity.Property(e => e.CumulativeValue).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IncrementValue).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser).HasMaxLength(50);

                entity.HasOne(d => d.Increment)
                    .WithMany(p => p.SaPmtPeriods)
                    .HasForeignKey(d => d.IncrementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPmtPeriods_saIncrements_IncrementId_IncrementId");
            });

            modelBuilder.Entity<SaPrgVerDefaultChargePeriods>(entity =>
            {
                entity.HasKey(e => e.ProgramVersionDefaultChargePeriodsId);

                entity.ToTable("saPrgVerDefaultChargePeriods");

                entity.Property(e => e.ProgramVersionDefaultChargePeriodsId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.FeeLevelId).HasColumnName("FeeLevelID");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrgVerId).HasColumnName("PrgVerID");

                entity.Property(e => e.SysTransCodeId).HasColumnName("SysTransCodeID");

                entity.HasOne(d => d.FeeLevel)
                    .WithMany(p => p.SaPrgVerDefaultChargePeriods)
                    .HasForeignKey(d => d.FeeLevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPrgVerDefaultChargePeriods_saFeeLevels_FeeLevelID_FeeLevelId");

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.SaPrgVerDefaultChargePeriods)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPrgVerDefaultChargePeriods_arPrgVersions_PrgVerID_PrgVerId");

                entity.HasOne(d => d.SysTransCode)
                    .WithMany(p => p.SaPrgVerDefaultChargePeriods)
                    .HasForeignKey(d => d.SysTransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saPrgVerDefaultChargePeriods_saSysTransCodes_SysTransCodeID_SysTransCodeId");
            });

            modelBuilder.Entity<SaProgramVersionFees>(entity =>
            {
                entity.HasKey(e => e.PrgVerFeeId);

                entity.ToTable("saProgramVersionFees");

                entity.Property(e => e.PrgVerFeeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.PrgVer)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.PrgVerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saProgramVersionFees_arPrgVersions_PrgVerId_PrgVerId");

                entity.HasOne(d => d.RateSchedule)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.RateScheduleId)
                    .HasConstraintName("FK_saProgramVersionFees_saRateSchedules_RateScheduleId_RateScheduleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saProgramVersionFees_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saProgramVersionFees_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_saProgramVersionFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.SaProgramVersionFees)
                    .HasForeignKey(d => d.UnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saProgramVersionFees_syProgramUnitTypes_UnitId_UnitId");
            });

            modelBuilder.Entity<SaRateScheduleDetails>(entity =>
            {
                entity.HasKey(e => e.RateScheduleDetailId);

                entity.ToTable("saRateScheduleDetails");

                entity.HasIndex(e => new { e.RateScheduleId, e.MaxUnits, e.TuitionCategoryId })
                    .HasName("UIX_saRateScheduleDetails_RateScheduleId_MaxUnits_TuitionCategoryId")
                    .IsUnique();

                entity.HasIndex(e => new { e.RateScheduleId, e.MinUnits, e.TuitionCategoryId })
                    .HasName("UIX_saRateScheduleDetails_RateScheduleId_MinUnits_TuitionCategoryId")
                    .IsUnique();

                entity.Property(e => e.RateScheduleDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FlatAmount).HasColumnType("money");

                entity.Property(e => e.MaxUnits).HasDefaultValueSql("((32767))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rate).HasColumnType("money");

                entity.HasOne(d => d.RateSchedule)
                    .WithMany(p => p.SaRateScheduleDetails)
                    .HasForeignKey(d => d.RateScheduleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saRateScheduleDetails_saRateSchedules_RateScheduleId_RateScheduleId");

                entity.HasOne(d => d.TuitionCategory)
                    .WithMany(p => p.SaRateScheduleDetails)
                    .HasForeignKey(d => d.TuitionCategoryId)
                    .HasConstraintName("FK_saRateScheduleDetails_saTuitionCategories_TuitionCategoryId_TuitionCategoryId");
            });

            modelBuilder.Entity<SaRateSchedules>(entity =>
            {
                entity.HasKey(e => e.RateScheduleId);

                entity.ToTable("saRateSchedules");

                entity.HasIndex(e => new { e.RateScheduleCode, e.RateScheduleDescrip, e.CampGrpId })
                    .HasName("UIX_saRateSchedules_RateScheduleCode_RateScheduleDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.RateScheduleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RateScheduleCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.RateScheduleDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaRateSchedules)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saRateSchedules_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaRateSchedules)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saRateSchedules_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaRefunds>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.ToTable("saRefunds");

                entity.Property(e => e.TransactionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RefundAmount).HasColumnType("money");

                entity.HasOne(d => d.BankAcct)
                    .WithMany(p => p.SaRefunds)
                    .HasForeignKey(d => d.BankAcctId)
                    .HasConstraintName("FK_saRefunds_saBankAccounts_BankAcctId_BankAcctId");

                entity.HasOne(d => d.FundSource)
                    .WithMany(p => p.SaRefunds)
                    .HasForeignKey(d => d.FundSourceId)
                    .HasConstraintName("FK_saRefunds_saFundSources_FundSourceId_FundSourceId");

                entity.HasOne(d => d.Transaction)
                    .WithOne(p => p.SaRefunds)
                    .HasForeignKey<SaRefunds>(d => d.TransactionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saRefunds_saTransactions_TransactionId_TransactionId");
            });

            modelBuilder.Entity<SaReversedTransactions>(entity =>
            {
                entity.HasKey(e => e.ReversedTransactionsId);

                entity.ToTable("saReversedTransactions");

                entity.Property(e => e.ReversedTransactionsId)
                    .HasColumnName("ReversedTransactionsID")
                    .HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReversalReason)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaSysTransCodes>(entity =>
            {
                entity.HasKey(e => e.SysTransCodeId);

                entity.ToTable("saSysTransCodes");

                entity.Property(e => e.SysTransCodeId).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaSysTransCodes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saSysTransCodes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaSysTransCodes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saSysTransCodes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaTransactions>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.ToTable("saTransactions");

                entity.HasIndex(e => e.AcademicYearId);

                entity.HasIndex(e => e.BatchPaymentId);

                entity.HasIndex(e => e.StuEnrollId);

                entity.HasIndex(e => e.TermId);

                entity.HasIndex(e => e.TransCodeId);

                entity.HasIndex(e => e.TransDate);

                entity.HasIndex(e => new { e.TransAmount, e.TransTypeId, e.StuEnrollId, e.TransDate, e.Voided, e.TransactionId, e.TransCodeId })
                    .HasName("IX_saTransactions_TransTypeId_StuEnrollId_TransDate_Voided_TransactionId_TransCodeId_TransAmount");

                entity.HasIndex(e => new { e.TransTypeId, e.StuEnrollId, e.IsPosted, e.Voided, e.TransactionId, e.TransAmount, e.TransDate });

                entity.HasIndex(e => new { e.TransAmount, e.TransTypeId, e.StuEnrollId, e.Voided, e.TransDate, e.TransactionId, e.TermId, e.TransCodeId })
                    .HasName("IX_saTransactions_TransTypeId_StuEnrollId_Voided_TransDate_TransactionId_TermId_TransCodeId_TransAmount");

                entity.Property(e => e.TransactionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.DisplaySequence).HasDefaultValueSql("((99999))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecondDisplaySequence).HasDefaultValueSql("((99999))");

                entity.Property(e => e.TransAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.TransDate).HasColumnType("datetime");

                entity.Property(e => e.TransDescrip)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TransReference)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.AcademicYear)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.AcademicYearId)
                    .HasConstraintName("FK_saTransactions_saAcademicYears_AcademicYearId_AcademicYearId");

                entity.HasOne(d => d.BatchPayment)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.BatchPaymentId)
                    .HasConstraintName("FK_saTransactions_saBatchPayments_BatchPaymentId_BatchPaymentId");

                entity.HasOne(d => d.FundSource)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.FundSourceId)
                    .HasConstraintName("FK_saTransactions_saFundSources_FundSourceId_FundSourceId");

                entity.HasOne(d => d.PaymentCode)
                    .WithMany(p => p.SaTransactionsPaymentCode)
                    .HasForeignKey(d => d.PaymentCodeId)
                    .HasConstraintName("FK_saTransactions_saTransCodes_PaymentCodeId_TransCodeId");

                entity.HasOne(d => d.PmtPeriod)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.PmtPeriodId)
                    .HasConstraintName("FK_saTransactions_saPmtPeriods_PmtPeriodId_PmtPeriodId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransactions_arStuEnrollments_StuEnrollId_StuEnrollId");

                entity.HasOne(d => d.StuEnrollPayPeriod)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.StuEnrollPayPeriodId)
                    .HasConstraintName("FK_saTransactions_arPrgChargePeriodSeq_StuEnrollPayPeriodId_PrgChrPeriodSeqId");

                entity.HasOne(d => d.Term)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.TermId)
                    .HasConstraintName("FK_saTransactions_arTerm_TermId_TermId");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaTransactionsTransCode)
                    .HasForeignKey(d => d.TransCodeId)
                    .HasConstraintName("FK_saTransactions_saTransCodes_TransCodeId_TransCodeId");

                entity.HasOne(d => d.TransType)
                    .WithMany(p => p.SaTransactions)
                    .HasForeignKey(d => d.TransTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransactions_saTransTypes_TransTypeId_TransTypeId");
            });

            modelBuilder.Entity<SaTransCodeGlaccounts>(entity =>
            {
                entity.HasKey(e => e.TransCodeGlaccountId);

                entity.ToTable("saTransCodeGLAccounts");

                entity.Property(e => e.TransCodeGlaccountId)
                    .HasColumnName("TransCodeGLAccountId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Glaccount)
                    .IsRequired()
                    .HasColumnName("GLAccount")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Percentage)
                    .HasColumnType("money")
                    .HasDefaultValueSql("((100.00))");

                entity.HasOne(d => d.TransCode)
                    .WithMany(p => p.SaTransCodeGlaccounts)
                    .HasForeignKey(d => d.TransCodeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransCodeGLAccounts_saTransCodes_TransCodeId_TransCodeId");
            });

            modelBuilder.Entity<SaTransCodes>(entity =>
            {
                entity.HasKey(e => e.TransCodeId);

                entity.ToTable("saTransCodes");

                entity.HasIndex(e => new { e.TransCodeCode, e.TransCodeDescrip, e.CampGrpId })
                    .HasName("UIX_saTransCodes_TransCodeCode_TransCodeDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.TransCodeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransCodeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TransCodeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.BillType)
                    .WithMany(p => p.SaTransCodes)
                    .HasForeignKey(d => d.BillTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransCodes_saBillTypes_BillTypeId_BillTypeId");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaTransCodes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransCodes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaTransCodes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTransCodes_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.SysTransCode)
                    .WithMany(p => p.SaTransCodes)
                    .HasForeignKey(d => d.SysTransCodeId)
                    .HasConstraintName("FK_saTransCodes_saSysTransCodes_SysTransCodeId_SysTransCodeId");
            });

            modelBuilder.Entity<SaTransTypes>(entity =>
            {
                entity.HasKey(e => e.TransTypeId);

                entity.ToTable("saTransTypes");

                entity.Property(e => e.TransTypeId).ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SaTuitionCategories>(entity =>
            {
                entity.HasKey(e => e.TuitionCategoryId);

                entity.ToTable("saTuitionCategories");

                entity.HasIndex(e => new { e.TuitionCategoryCode, e.TuitionCategoryDescrip, e.CampGrpId })
                    .HasName("UIX_saTuitionCategories_TuitionCategoryCode_TuitionCategoryDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.TuitionCategoryId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TuitionCategoryCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TuitionCategoryDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaTuitionCategories)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saTuitionCategories_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaTuitionCategories)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTuitionCategories_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaTuitionEarnings>(entity =>
            {
                entity.HasKey(e => e.TuitionEarningId);

                entity.ToTable("saTuitionEarnings");

                entity.HasIndex(e => new { e.TuitionEarningsCode, e.TuitionEarningsDescrip, e.CampGrpId })
                    .HasName("UIX_saTuitionEarnings_TuitionEarningsCode_TuitionEarningsDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.TuitionEarningId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TuitionEarningsCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.TuitionEarningsDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SaTuitionEarnings)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_saTuitionEarnings_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SaTuitionEarnings)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTuitionEarnings_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SaTuitionEarningsPercentageRanges>(entity =>
            {
                entity.HasKey(e => e.TuitionEarningsPercentageRangeId);

                entity.ToTable("saTuitionEarningsPercentageRanges");

                entity.Property(e => e.TuitionEarningsPercentageRangeId).ValueGeneratedNever();

                entity.Property(e => e.EarnPercent).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpTo).HasColumnType("decimal(5, 2)");

                entity.HasOne(d => d.TuitionEarning)
                    .WithMany(p => p.SaTuitionEarningsPercentageRanges)
                    .HasForeignKey(d => d.TuitionEarningId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_saTuitionEarningsPercentageRanges_saTuitionEarnings_TuitionEarningId_TuitionEarningId");
            });

            modelBuilder.Entity<SyAcademicCalendars>(entity =>
            {
                entity.HasKey(e => e.Acid);

                entity.ToTable("syAcademicCalendars");

                entity.Property(e => e.Acid)
                    .HasColumnName("ACId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Acdescrip)
                    .HasColumnName("ACDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyAddressStatuses>(entity =>
            {
                entity.HasKey(e => e.AddressStatusId);

                entity.ToTable("syAddressStatuses");

                entity.Property(e => e.AddressStatusId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AddressStatusCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.AddressStatusDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CampGrpId).HasColumnName("CampGrpID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyAddressStatuses)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syAddressStatuses_syCampGrps_CampGrpID_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyAddressStatuses)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syAddressStatuses_syStatuses_StatusID_StatusId");
            });

            modelBuilder.Entity<SyAdvantageResourceRelations>(entity =>
            {
                entity.HasKey(e => e.ResRelId);

                entity.ToTable("syAdvantageResourceRelations");

                entity.Property(e => e.ResRelId).ValueGeneratedNever();

                entity.HasOne(d => d.RelatedResource)
                    .WithMany(p => p.SyAdvantageResourceRelationsRelatedResource)
                    .HasForeignKey(d => d.RelatedResourceId)
                    .HasConstraintName("FK_syAdvantageResourceRelations_syResources_RelatedResourceId_ResourceID");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyAdvantageResourceRelationsResource)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_syAdvantageResourceRelations_syResources_ResourceId_ResourceID");
            });

            modelBuilder.Entity<SyAdvBuild>(entity =>
            {
                entity.HasKey(e => e.AdvantageBuildId);

                entity.ToTable("syAdvBuild");

                entity.Property(e => e.AdvantageBuildId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.BuildNumber)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SyAdvFundSources>(entity =>
            {
                entity.HasKey(e => e.AdvFundSourceId);

                entity.ToTable("syAdvFundSources");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyAnnouncements>(entity =>
            {
                entity.HasKey(e => e.AnnouncementId);

                entity.ToTable("syAnnouncements");

                entity.Property(e => e.AnnouncementId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AnnouncementCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AnnouncementDescrip).HasColumnType("text");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyAnnouncements)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syAnnouncements_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyAnnouncements)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syAnnouncements_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyAuditHist>(entity =>
            {
                entity.HasKey(e => e.AuditHistId);

                entity.ToTable("syAuditHist");

                entity.HasIndex(e => e.Event);

                entity.HasIndex(e => e.EventDate);

                entity.HasIndex(e => e.TableName);

                entity.Property(e => e.AuditHistId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.AppName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Event)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.EventDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TableName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyAuditHistDetail>(entity =>
            {
                entity.HasKey(e => e.AuditHistDetailId);

                entity.ToTable("syAuditHistDetail");

                entity.HasIndex(e => e.ColumnName);

                entity.HasIndex(e => e.NewValue);

                entity.HasIndex(e => e.OldValue);

                entity.HasIndex(e => new { e.RowId, e.AuditHistId })
                    .HasName("IX_syAuditHistDetail_AuditHistId_RowId");

                entity.Property(e => e.AuditHistDetailId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ColumnName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewValue)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.OldValue)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.HasOne(d => d.AuditHist)
                    .WithMany(p => p.SyAuditHistDetail)
                    .HasForeignKey(d => d.AuditHistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syAuditHistDetail_syAuditHist_AuditHistId_AuditHistId");
            });

            modelBuilder.Entity<SyAwardTypes9010>(entity =>
            {
                entity.HasKey(e => e.AwardType9010Id);

                entity.ToTable("syAwardTypes9010");

                entity.Property(e => e.AwardType9010Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyAwardTypes9010Mapping>(entity =>
            {
                entity.HasKey(e => e.AwardTypes9010MappingId);

                entity.ToTable("syAwardTypes9010Mapping");

                entity.Property(e => e.AwardTypes9010MappingId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyAwardTypes9010Mapping)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syAwardTypes9010Mapping_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.FundSource)
                    .WithMany(p => p.SyAwardTypes9010Mapping)
                    .HasForeignKey(d => d.FundSourceId)
                    .HasConstraintName("FK_syAwardTypes9010Mapping_saFundSources_FundSourceId_FundSourceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyAwardTypes9010Mapping)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syAwardTypes9010Mapping_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyBridgeResourceIdRptFldId>(entity =>
            {
                entity.HasKey(e => e.BridgeId);

                entity.ToTable("syBridge_ResourceId_RptFldId");
            });

            modelBuilder.Entity<SyBusObjects>(entity =>
            {
                entity.HasKey(e => e.BusObjectId);

                entity.ToTable("syBusObjects");

                entity.Property(e => e.BusObjectId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyCampGrps>(entity =>
            {
                entity.HasKey(e => e.CampGrpId);

                entity.ToTable("syCampGrps");

                entity.HasIndex(e => new { e.CampGrpCode, e.CampGrpDescrip })
                    .HasName("UIX_syCampGrps_CampGrpCode_CampGrpDescrip")
                    .IsUnique();

                entity.Property(e => e.CampGrpId).ValueGeneratedNever();

                entity.Property(e => e.CampGrpCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CampGrpDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsAllCampusGrp).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyCampGrps)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_syCampGrps_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyCampGrps)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syCampGrps_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyCampuses>(entity =>
            {
                entity.HasKey(e => e.CampusId);

                entity.ToTable("syCampuses");

                entity.HasIndex(e => new { e.CampCode, e.CampDescrip, e.StatusId })
                    .HasName("UIX_syCampuses_CampCode_CampDescrip_StatusId")
                    .IsUnique();

                entity.Property(e => e.CampusId).ValueGeneratedNever();

                entity.Property(e => e.AddTypeId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address1)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AdmRepId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CampCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CampDescrip)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CmsId).HasMaxLength(7);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailBody).HasColumnType("text");

                entity.Property(e => e.EmailSubject)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FlexceptionPath)
                    .HasColumnName("FLExceptionPath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FlsourcePath)
                    .HasColumnName("FLSourcePath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FltargetPath)
                    .HasColumnName("FLTargetPath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FseogmatchType)
                    .HasColumnName("FSEOGMatchType")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GenderId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IlarchivePath)
                    .HasColumnName("ILArchivePath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IlexceptionPath)
                    .HasColumnName("ILExceptionPath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IlsourcePath)
                    .HasColumnName("ILSourcePath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvAddress1)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.InvAddress2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.InvCountryId).HasColumnName("InvCountryID");

                entity.Property(e => e.InvFax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvPhone1)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvStateId).HasColumnName("InvStateID");

                entity.Property(e => e.InvZip)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.IsCorporate).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsRemoteServerFl).HasColumnName("IsRemoteServerFL");

                entity.Property(e => e.IsRemoteServerIl).HasColumnName("IsRemoteServerIL");

                entity.Property(e => e.LeadStatusId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Opeid)
                    .HasColumnName("OPEID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Phone3)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneType1Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneType2Id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PortalContactEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PortalCountryId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerPasswordIl)
                    .HasColumnName("RemoteServerPasswordIL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerPwd)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerPwdFl)
                    .HasColumnName("RemoteServerPwdFL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerUserNameIl)
                    .HasColumnName("RemoteServerUserNameIL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerUsrNm)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemoteServerUsrNmFl)
                    .HasColumnName("RemoteServerUsrNmFL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolCodeKissSchoolId)
                    .HasMaxLength(18)
                    .IsUnicode(false);

                entity.Property(e => e.SchoolName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SourceCategoryId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceFolderLoc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SourceFolderLocFl)
                    .HasColumnName("SourceFolderLocFL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SourceTypeId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TargetFolderLoc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TargetFolderLocFl)
                    .HasColumnName("TargetFolderLocFL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TcsourcePath)
                    .HasColumnName("TCSourcePath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TctargetPath)
                    .HasColumnName("TCTargetPath")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Token1098Tservice)
                    .HasColumnName("Token1098TService")
                    .HasMaxLength(38)
                    .IsUnicode(false);

                entity.Property(e => e.TranscriptAuthZnName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TranscriptAuthZnTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Website)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.SyCampuses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_syCampuses_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SyCampuses)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_syCampuses_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyCampuses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syCampuses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyCertifications>(entity =>
            {
                entity.HasKey(e => e.CertificationId);

                entity.ToTable("syCertifications");

                entity.HasIndex(e => new { e.CertificationCode, e.CertificationDescrip, e.CampGrpId })
                    .HasName("UIX_syCertifications_CertificationCode_CertificationDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.CertificationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CertificationCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CertificationDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyCertifications)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syCertifications_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyCertifications)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syCertifications_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyClsSectStatuses>(entity =>
            {
                entity.HasKey(e => e.ClsSectStatusId);

                entity.ToTable("syClsSectStatuses");

                entity.Property(e => e.ClsSectStatusId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyCmpGrpCmps>(entity =>
            {
                entity.HasKey(e => e.CmpGrpCmpId);

                entity.ToTable("syCmpGrpCmps");

                entity.Property(e => e.CmpGrpCmpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyCmpGrpCmps)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syCmpGrpCmps_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyCmpGrpCmps)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syCmpGrpCmps_syCampuses_CampusId_CampusId");
            });

            modelBuilder.Entity<SyCompOps>(entity =>
            {
                entity.HasKey(e => e.CompOpId);

                entity.ToTable("syCompOps");

                entity.Property(e => e.AllowMultiple).HasDefaultValueSql("((0))");

                entity.Property(e => e.CompOptext)
                    .IsRequired()
                    .HasColumnName("CompOPText")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyConfigAppSetLookup>(entity =>
            {
                entity.HasKey(e => e.LookUpId);

                entity.ToTable("syConfigAppSet_Lookup");

                entity.Property(e => e.LookUpId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValueOptions)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyConfigAppSettings>(entity =>
            {
                entity.HasKey(e => e.SettingId);

                entity.ToTable("syConfigAppSettings");

                entity.Property(e => e.CampusSpecific)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Description)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.KeyName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyConfigAppSetValues>(entity =>
            {
                entity.HasKey(e => e.ValueId);

                entity.ToTable("syConfigAppSetValues");

                entity.Property(e => e.ValueId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Value)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyContactTypes>(entity =>
            {
                entity.HasKey(e => e.ContactTypeId);

                entity.ToTable("syContactTypes");

                entity.Property(e => e.ContactTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ContactTypeCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ContactTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyContactTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syContactTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyContactTypes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syContactTypes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyCreditSummary>(entity =>
            {
                entity.ToTable("syCreditSummary");

                entity.HasIndex(e => e.TermStartDate)
                    .HasAnnotation("SqlServer:Clustered", true);

                entity.HasIndex(e => new { e.CountSimpleAverageCredits, e.ProductSimpleAverageCreditsGpa, e.TermId, e.StuEnrollId })
                    .HasName("IX_syCreditSummary_StuEnrollID_INC_C2C17C18");

                entity.HasIndex(e => new { e.CountWeightedAverageCredits, e.ProductWeightedAverageCreditsGpa, e.StuEnrollId, e.TermId })
                    .HasName("IX_syCreditSummary_StuEnrollId_TermId_INC_C15C16");

                entity.HasIndex(e => new { e.CountWeightedAverageCredits, e.ProductWeightedAverageCreditsGpa, e.StuEnrollId, e.TermId, e.ReqId })
                    .HasName("IX_syCreditSummary_StuEnrollId_TermId_ReqId_INC_C15C16");

                entity.HasIndex(e => new { e.Completed, e.Coursecredits, e.CreditsEarned, e.FacreditsEarned, e.FinalGpa, e.FinalScore, e.StuEnrollId, e.TermStartDate, e.TermId })
                    .HasName("IX_syCreditSummary_StuEnrollId_TermStartDate_TermId_Completed_coursecredits_CreditsEarned_FACreditsEarned_FinalGPA_FinalScore");

                entity.HasIndex(e => new { e.StuEnrollId, e.TermId, e.ReqId, e.CreditsAttempted, e.CreditsEarned, e.Completed, e.CurrentScore, e.CurrentGrade, e.FinalScore, e.FinalGrade })
                    .HasName("IX_syCreditSummary_StuEnrollId_TermId_ReqId_C8C7C13C9C10C11C12");

                entity.HasIndex(e => new { e.StuEnrollId, e.TermId, e.ReqId, e.ClsSectionId, e.CreditsAttempted, e.CreditsEarned, e.Completed, e.CurrentScore, e.CurrentGrade, e.FinalScore, e.FinalGrade })
                    .HasName("IX_syCreditSummary_C1_C2_C4_C6_C7_C8_C9_C10_C11_C12_C13");

                entity.HasIndex(e => new { e.Average, e.CountSimpleAverageCredits, e.CountWeightedAverageCredits, e.CreditsAttempted, e.CreditsEarned, e.FacreditsEarned, e.ProductSimpleAverageCreditsGpa, e.ProductWeightedAverageCreditsGpa, e.StuEnrollId, e.TermId, e.TermGpaSimple, e.TermGpaWeighted })
                    .HasName("IX_syCreditSummary_C1_C2_C7_C8_C15_C16_C17_C18_C21_C22_C26_C27");

                entity.HasIndex(e => new { e.CountSimpleAverageCredits, e.CountWeightedAverageCredits, e.ProductSimpleAverageCreditsGpa, e.ProductWeightedAverageCreditsGpa, e.ReqId, e.StuEnrollId, e.TermId, e.CreditsAttempted, e.CreditsEarned, e.Completed, e.CurrentScore, e.CurrentGrade, e.FinalScore, e.FinalGrade })
                    .HasName("IX_syCreditSummary_ReqId_StuEnrollId_TermIdC8C7C13C9C10C11C12__INC_C15C16C17C18");

                entity.HasIndex(e => new { e.CountSimpleAverageCredits, e.CountWeightedAverageCredits, e.ProductSimpleAverageCreditsGpa, e.ProductWeightedAverageCreditsGpa, e.TermId, e.StuEnrollId, e.ReqId, e.CreditsAttempted, e.CreditsEarned, e.Completed, e.CurrentScore, e.CurrentGrade, e.FinalScore, e.FinalGrade })
                    .HasName("IX_syCreditSummary_TermID_StuEnrollId_ReqId_C8C7C13C9C10C11C12_INC_C15C16C17C18");

                entity.Property(e => e.ClsSectionId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountSimpleAverageCredits).HasColumnName("Count_SimpleAverage_Credits");

                entity.Property(e => e.CountWeightedAverageCredits).HasColumnName("Count_WeightedAverage_Credits");

                entity.Property(e => e.Coursecredits).HasColumnName("coursecredits");

                entity.Property(e => e.CumulativeGpa).HasColumnName("CumulativeGPA");

                entity.Property(e => e.CumulativeGpaSimple).HasColumnName("CumulativeGPA_Simple");

                entity.Property(e => e.CurrentGrade)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FacreditsEarned).HasColumnName("FACreditsEarned");

                entity.Property(e => e.FinalGpa).HasColumnName("FinalGPA");

                entity.Property(e => e.FinalGrade)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductSimpleAverageCreditsGpa).HasColumnName("Product_SimpleAverage_Credits_GPA");

                entity.Property(e => e.ProductWeightedAverageCreditsGpa).HasColumnName("Product_WeightedAverage_Credits_GPA");

                entity.Property(e => e.ReqDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermGpaSimple).HasColumnName("TermGPA_Simple");

                entity.Property(e => e.TermGpaWeighted).HasColumnName("TermGPA_Weighted");

                entity.Property(e => e.TermStartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SyDdls>(entity =>
            {
                entity.HasKey(e => e.Ddlid);

                entity.ToTable("syDDLS");

                entity.Property(e => e.Ddlid)
                    .HasColumnName("DDLId")
                    .ValueGeneratedNever();

                entity.Property(e => e.Ddlname)
                    .HasColumnName("DDLName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Tbl)
                    .WithMany(p => p.SyDdls)
                    .HasForeignKey(d => d.TblId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syDDLS_syTables_TblId_TblId");
            });

            modelBuilder.Entity<SyDepartments>(entity =>
            {
                entity.HasKey(e => e.DepartmentId);

                entity.ToTable("syDepartments");

                entity.Property(e => e.DepartmentId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DepartmentCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DepartmentDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyDepartments)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syDepartments_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyDepartments)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syDepartments_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyDocStatuses>(entity =>
            {
                entity.HasKey(e => e.DocStatusId);

                entity.ToTable("syDocStatuses");

                entity.Property(e => e.DocStatusId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DocStatusCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.DocStatusDescrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyDocStatuses)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syDocStatuses_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyDocStatuses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syDocStatuses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyDocumentHistory>(entity =>
            {
                entity.HasKey(e => e.FileId);

                entity.ToTable("syDocumentHistory");

                entity.Property(e => e.FileId).ValueGeneratedNever();

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileExtension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FileNameOnly)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.SyDocumentHistory)
                    .HasForeignKey(d => d.DocumentId)
                    .HasConstraintName("FK_syDocumentHistory_adReqs_DocumentId_adReqId");

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.SyDocumentHistory)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_syDocumentHistory_adLeads_LeadId_LeadId");
            });

            modelBuilder.Entity<SyEdexpNotPostPellAcgSmartTeachDisbursementTable>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.ToTable("syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable");

                entity.Property(e => e.DetailId).ValueGeneratedNever();

                entity.Property(e => e.AccDisbAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.ActionStatusDisb)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DbIn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisbDate).HasColumnType("datetime");

                entity.Property(e => e.DisbNum)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisbRelIndi)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DusbSeqNum)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SimittedDisbAmount).HasColumnType("decimal(19, 4)");
            });

            modelBuilder.Entity<SyEdexpNotPostPellAcgSmartTeachStudentTable>(entity =>
            {
                entity.HasKey(e => e.ParentId);

                entity.ToTable("syEDExpNotPostPell_ACG_SMART_Teach_StudentTable");

                entity.Property(e => e.ParentId).ValueGeneratedNever();

                entity.Property(e => e.AddDate).HasColumnType("datetime");

                entity.Property(e => e.AddTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AwardAmount).HasColumnType("decimal(19, 4)");

                entity.Property(e => e.AwardId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AwardYearEndDate).HasColumnType("datetime");

                entity.Property(e => e.AwardYearStartDate).HasColumnType("datetime");

                entity.Property(e => e.CampusName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DbIn)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DbIndicator)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrantType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OriginationStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEdexpressExceptionReportDirectLoanDisbursementTable>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.ToTable("syEDExpressExceptionReportDirectLoan_DisbursementTable");

                entity.Property(e => e.DetailId).ValueGeneratedNever();

                entity.Property(e => e.DbIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DisbDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbGrossAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbIntRebAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbLoanFeeAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbNetAdjAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbNetAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbNum)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbSeqNum)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbStatusRelInd)
                    .HasColumnName("DisbStatus_RelInd")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorMsg).IsUnicode(false);

                entity.Property(e => e.ExceptionGuid).HasColumnName("ExceptionGUID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEdexpressExceptionReportDirectLoanStudentTable>(entity =>
            {
                entity.HasKey(e => e.ExceptionReportId);

                entity.ToTable("syEDExpressExceptionReportDirectLoan_StudentTable");

                entity.Property(e => e.ExceptionReportId).ValueGeneratedNever();

                entity.Property(e => e.AddDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddTime)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DbIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorMsg).IsUnicode(false);

                entity.Property(e => e.ExceptionGuid).HasColumnName("ExceptionGUID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LoanAmtApproved)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoanFeePer)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoanId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoanPeriodEndDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoanPeriodStartDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoanStatus)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LoanType)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.Mpnstatus)
                    .HasColumnName("MPNStatus")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable>(entity =>
            {
                entity.HasKey(e => e.DetailId);

                entity.ToTable("syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable");

                entity.Property(e => e.DetailId).ValueGeneratedNever();

                entity.Property(e => e.AccDisbAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ActionStatusDisb)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DbIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DisbDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbNum)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DisbRelIndi)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DusbSeqNum)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorMsg).IsUnicode(false);

                entity.Property(e => e.ExceptionGuid).HasColumnName("ExceptionGUID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SimittedDisbAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEdexpressExceptionReportPellAcgSmartTeachStudentTable>(entity =>
            {
                entity.HasKey(e => e.ExceptionReportId);

                entity.ToTable("syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable");

                entity.Property(e => e.ExceptionReportId).ValueGeneratedNever();

                entity.Property(e => e.AddDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddTime)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AwardAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AwardId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AwardYearEndDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AwardYearStartDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DbIn)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorMsg).IsUnicode(false);

                entity.Property(e => e.ExceptionGuid).HasColumnName("ExceptionGUID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Filter)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.GrantType)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalSsn)
                    .HasColumnName("OriginalSSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OriginationStatus)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEmailType>(entity =>
            {
                entity.HasKey(e => e.EmailTypeId);

                entity.ToTable("syEmailType");

                entity.Property(e => e.EmailTypeId)
                    .HasColumnName("EMailTypeId")
                    .ValueGeneratedNever();

                entity.Property(e => e.EmailTypeCode)
                    .IsRequired()
                    .HasColumnName("EMailTypeCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailTypeDescription)
                    .IsRequired()
                    .HasColumnName("EMailTypeDescription")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEntities>(entity =>
            {
                entity.HasKey(e => e.EntityId);

                entity.ToTable("syEntities");

                entity.Property(e => e.EntityId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyEvents>(entity =>
            {
                entity.HasKey(e => e.EventId);

                entity.ToTable("syEvents");

                entity.Property(e => e.EventId)
                    .HasColumnName("EventID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.ShowDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.WhereWhen)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyEvents)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syEvents_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyEvents)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syEvents_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyFameEsparchivedFiles>(entity =>
            {
                entity.HasKey(e => e.ArchiveId);

                entity.ToTable("syFameESPArchivedFiles");

                entity.Property(e => e.ArchiveId).ValueGeneratedNever();

                entity.Property(e => e.FileName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsSuccessfullyProcessed).HasDefaultValueSql("((0))");

                entity.Property(e => e.Moddate)
                    .HasColumnName("moddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Moduser)
                    .HasColumnName("moduser")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyFameEspawardCutOffDate>(entity =>
            {
                entity.HasKey(e => e.AwardCutOffDateGuid);

                entity.ToTable("syFameESPAwardCutOffDate");

                entity.Property(e => e.AwardCutOffDateGuid)
                    .HasColumnName("AwardCutOffDateGUID")
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<SyFameEspexceptionReport>(entity =>
            {
                entity.HasKey(e => e.ExceptionReportId);

                entity.ToTable("syFameESPExceptionReport");

                entity.Property(e => e.ExceptionReportId).ValueGeneratedNever();

                entity.Property(e => e.Awardyear)
                    .HasColumnName("awardyear")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DisbursementDate)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExceptionGuid).HasColumnName("ExceptionGUID");

                entity.Property(e => e.Faid)
                    .HasColumnName("FAID")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Fund)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GrossAmount).HasColumnType("decimal(19, 2)");

                entity.Property(e => e.Hide).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsEligibleForReprocess)
                    .HasColumnName("isEligibleForReprocess")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsInitialImportSuccessful).HasDefaultValueSql("((0))");

                entity.Property(e => e.LoanFees).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Loanenddate)
                    .HasColumnName("loanenddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Loanstartdate)
                    .HasColumnName("loanstartdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Message)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Moddate)
                    .HasColumnName("moddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Moduser)
                    .HasColumnName("moduser")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasColumnName("SSN")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyFamilyIncome>(entity =>
            {
                entity.HasKey(e => e.FamilyIncomeId);

                entity.ToTable("syFamilyIncome");

                entity.HasIndex(e => new { e.FamilyIncomeCode, e.FamilyIncomeDescrip, e.CampGrpId })
                    .HasName("UIX_syFamilyIncome_FamilyIncomeCode_FamilyIncomeDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.FamilyIncomeId)
                    .HasColumnName("FamilyIncomeID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.FamilyIncomeCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FamilyIncomeDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ipedsvalue).HasColumnName("IPEDSValue");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyFamilyIncome)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syFamilyIncome_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyFamilyIncome)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syFamilyIncome_syStatuses_StatusID_StatusId");
            });

            modelBuilder.Entity<SyFieldCalculation>(entity =>
            {
                entity.ToTable("syFieldCalculation");

                entity.Property(e => e.SyFieldCalculationId).HasColumnName("syFieldCalculationId");

                entity.Property(e => e.CalculationSql)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Fld)
                    .WithMany(p => p.SyFieldCalculation)
                    .HasForeignKey(d => d.FldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syFieldCalculation_syFields_FldId_FldId");
            });

            modelBuilder.Entity<SyFields>(entity =>
            {
                entity.HasKey(e => e.FldId);

                entity.ToTable("syFields");

                entity.Property(e => e.FldId).ValueGeneratedNever();

                entity.Property(e => e.Ddlid).HasColumnName("DDLId");

                entity.Property(e => e.DerivedFld).HasDefaultValueSql("((0))");

                entity.Property(e => e.FldName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Mask)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.FldType)
                    .WithMany(p => p.SyFields)
                    .HasForeignKey(d => d.FldTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syFields_syFieldTypes_FldTypeId_FldTypeId");
            });

            modelBuilder.Entity<SyFieldTypes>(entity =>
            {
                entity.HasKey(e => e.FldTypeId);

                entity.ToTable("syFieldTypes");

                entity.Property(e => e.FldTypeId).ValueGeneratedNever();

                entity.Property(e => e.FldType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyFldCaptions>(entity =>
            {
                entity.HasKey(e => e.FldCapId);

                entity.ToTable("syFldCaptions");

                entity.Property(e => e.FldCapId).ValueGeneratedNever();

                entity.Property(e => e.Caption)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FldDescrip)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Fld)
                    .WithMany(p => p.SyFldCaptions)
                    .HasForeignKey(d => d.FldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syFldCaptions_syFields_FldId_FldId");

                entity.HasOne(d => d.Lang)
                    .WithMany(p => p.SyFldCaptions)
                    .HasForeignKey(d => d.LangId)
                    .HasConstraintName("FK_syFldCaptions_syLangs_LangId_LangId");
            });

            modelBuilder.Entity<SyFldCategories>(entity =>
            {
                entity.HasKey(e => new { e.CategoryId, e.EntityId });

                entity.ToTable("syFldCategories");

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyGenerateStudentFormatId>(entity =>
            {
                entity.HasKey(e => e.GenerateStudentFormatId);

                entity.ToTable("syGenerateStudentFormatID");

                entity.Property(e => e.GenerateStudentFormatId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentSeqId).HasColumnName("Student_SeqId");
            });

            modelBuilder.Entity<SyGenerateStudentId>(entity =>
            {
                entity.HasKey(e => e.StudentSeqId);

                entity.ToTable("syGenerateStudentID");

                entity.Property(e => e.StudentSeqId)
                    .HasColumnName("Student_SeqID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyGenerateStudentSeq>(entity =>
            {
                entity.HasKey(e => e.GenerateStudentSeqId);

                entity.ToTable("syGenerateStudentSeq");

                entity.Property(e => e.GenerateStudentSeqId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentSeqId).HasColumnName("Student_SeqId");
            });

            modelBuilder.Entity<SyGradeRounding>(entity =>
            {
                entity.HasKey(e => e.GradeRoundingId);

                entity.ToTable("syGradeRounding");

                entity.Property(e => e.GradeRoundingId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Graderoundingvalue)
                    .HasColumnName("graderoundingvalue")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no')");
            });

            modelBuilder.Entity<SyGrdPolicies>(entity =>
            {
                entity.HasKey(e => e.GrdPolicyId);

                entity.ToTable("syGrdPolicies");

                entity.Property(e => e.GrdPolicyId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyGrdPolicyParams>(entity =>
            {
                entity.HasKey(e => e.GrdPolParamId);

                entity.ToTable("syGrdPolicyParams");

                entity.Property(e => e.GrdPolParamId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.GrdPolicy)
                    .WithMany(p => p.SyGrdPolicyParams)
                    .HasForeignKey(d => d.GrdPolicyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syGrdPolicyParams_syGrdPolicies_GrdPolicyId_GrdPolicyId");
            });

            modelBuilder.Entity<SyHolidays>(entity =>
            {
                entity.HasKey(e => e.HolidayId);

                entity.ToTable("syHolidays");

                entity.HasIndex(e => new { e.StatusId, e.HolidayStartDate, e.CampGrpId })
                    .HasName("UIX_syHolidays_StatusId_HolidayStartDate_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.HolidayId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.HolidayCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.HolidayDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HolidayEndDate).HasColumnType("datetime");

                entity.Property(e => e.HolidayStartDate).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyHolidays)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHolidays_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.EndTime)
                    .WithMany(p => p.SyHolidaysEndTime)
                    .HasForeignKey(d => d.EndTimeId)
                    .HasConstraintName("FK_syHolidays_cmTimeInterval_EndTimeId_TimeIntervalId");

                entity.HasOne(d => d.StartTime)
                    .WithMany(p => p.SyHolidaysStartTime)
                    .HasForeignKey(d => d.StartTimeId)
                    .HasConstraintName("FK_syHolidays_cmTimeInterval_StartTimeId_TimeIntervalId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyHolidays)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHolidays_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyHomePageNotes>(entity =>
            {
                entity.HasKey(e => e.HomePageNoteId);

                entity.ToTable("syHomePageNotes");

                entity.Property(e => e.HomePageNoteId)
                    .HasColumnName("HomePageNoteID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyHomePageNotes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHomePageNotes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyHomePageNotes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHomePageNotes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyHrdepartments>(entity =>
            {
                entity.HasKey(e => e.HrdepartmentId);

                entity.ToTable("syHRDepartments");

                entity.HasIndex(e => new { e.HrdepartmentCode, e.HrdepartmentDescrip, e.CampGrpId })
                    .HasName("UIX_syHRDepartments_HRDepartmentCode_HRDepartmentDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.HrdepartmentId)
                    .HasColumnName("HRDepartmentId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.HrdepartmentCode)
                    .IsRequired()
                    .HasColumnName("HRDepartmentCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.HrdepartmentDescrip)
                    .IsRequired()
                    .HasColumnName("HRDepartmentDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyHrdepartments)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHRDepartments_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyHrdepartments)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syHRDepartments_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyInputMasks>(entity =>
            {
                entity.HasKey(e => e.InputMaskId);

                entity.ToTable("syInputMasks");

                entity.Property(e => e.Item)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Mask)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyInstFldsDdreq>(entity =>
            {
                entity.HasKey(e => e.DdfldReqId);

                entity.ToTable("syInstFldsDDReq");

                entity.Property(e => e.DdfldReqId)
                    .HasColumnName("DDFldReqId")
                    .HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SyInstFldsLog>(entity =>
            {
                entity.HasKey(e => new { e.InstId, e.FldId });

                entity.ToTable("syInstFldsLog");
            });

            modelBuilder.Entity<SyInstFldsMask>(entity =>
            {
                entity.HasKey(e => e.FldId);

                entity.ToTable("syInstFldsMask");

                entity.Property(e => e.FldId).ValueGeneratedNever();

                entity.Property(e => e.Mask)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyInstitutionAddresses>(entity =>
            {
                entity.HasKey(e => e.InstitutionAddressId);

                entity.ToTable("syInstitutionAddresses");

                entity.Property(e => e.InstitutionAddressId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Address1)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.AddressApto)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.City)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignCountry)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ForeignCountyStr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsInternational).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherState)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.AddressType)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.AddressTypeId)
                    .HasConstraintName("FK_syInstitutionAddresses_plAddressTypes_AddressTypeId_AddressTypeId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_syInstitutionAddresses_adCountries_CountryId_CountryId");

                entity.HasOne(d => d.County)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.CountyId)
                    .HasConstraintName("FK_syInstitutionAddresses_adCounties_CountyId_CountyId");

                entity.HasOne(d => d.Institution)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.InstitutionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syInstitutionAddresses_syInstitutions_InstitutionId_HSID");

                entity.HasOne(d => d.StateNavigation)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_syInstitutionAddresses_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyInstitutionAddresses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syInstitutionAddresses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyInstitutionContacts>(entity =>
            {
                entity.HasKey(e => e.InstitutionContactId);

                entity.ToTable("syInstitutionContacts");

                entity.Property(e => e.InstitutionContactId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneExt)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Institution)
                    .WithMany(p => p.SyInstitutionContacts)
                    .HasForeignKey(d => d.InstitutionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syInstitutionContacts_syInstitutions_InstitutionId_HSID");

                entity.HasOne(d => d.Prefix)
                    .WithMany(p => p.SyInstitutionContacts)
                    .HasForeignKey(d => d.PrefixId)
                    .HasConstraintName("FK_syInstitutionContacts_syPrefixes_PrefixId_PrefixId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyInstitutionContacts)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syInstitutionContacts_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Suffix)
                    .WithMany(p => p.SyInstitutionContacts)
                    .HasForeignKey(d => d.SuffixId)
                    .HasConstraintName("FK_syInstitutionContacts_sySuffixes_SuffixId_SuffixId");
            });

            modelBuilder.Entity<SyInstitutionImportTypes>(entity =>
            {
                entity.HasKey(e => e.ImportTypeId);

                entity.ToTable("syInstitutionImportTypes");

                entity.Property(e => e.ImportTypeId)
                    .HasColumnName("ImportTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyInstitutionPhone>(entity =>
            {
                entity.HasKey(e => e.InstitutionPhoneId);

                entity.ToTable("syInstitutionPhone");

                entity.Property(e => e.InstitutionPhoneId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Institution)
                    .WithMany(p => p.SyInstitutionPhone)
                    .HasForeignKey(d => d.InstitutionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syInstitutionPhone_syInstitutions_InstitutionId_HSID");

                entity.HasOne(d => d.PhoneType)
                    .WithMany(p => p.SyInstitutionPhone)
                    .HasForeignKey(d => d.PhoneTypeId)
                    .HasConstraintName("FK_syInstitutionPhone_syPhoneType_PhoneTypeId_PhoneTypeId");
            });

            modelBuilder.Entity<SyInstitutions>(entity =>
            {
                entity.HasKey(e => e.Hsid);

                entity.ToTable("syInstitutions");

                entity.Property(e => e.Hsid)
                    .HasColumnName("HSId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Hscode)
                    .IsRequired()
                    .HasColumnName("HSCode")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Hsname)
                    .HasColumnName("HSName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LevelId).HasColumnName("LevelID");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusId).HasDefaultValueSql("([dbo].[UDF_GetSyStatusesValue]('A'))");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyInstitutions)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syInstitutions_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.ImportType)
                    .WithMany(p => p.SyInstitutions)
                    .HasForeignKey(d => d.ImportTypeId)
                    .HasConstraintName("FK_syInstitutions_syInstitutionImportTypes_ImportTypeId_ImportTypeID");

                entity.HasOne(d => d.Level)
                    .WithMany(p => p.SyInstitutions)
                    .HasForeignKey(d => d.LevelId)
                    .HasConstraintName("FK_syInstitutions_adLevel_LevelId_LevelId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyInstitutions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syInstitutions_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.SyInstitutions)
                    .HasForeignKey(d => d.TypeId)
                    .HasConstraintName("FK_syInstitutions_syInstitutionTypes_TypeId_TypeId");
            });

            modelBuilder.Entity<SyInstitutionTypes>(entity =>
            {
                entity.HasKey(e => e.TypeId);

                entity.ToTable("syInstitutionTypes");

                entity.Property(e => e.TypeId)
                    .HasColumnName("TypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyInstResFldsReq>(entity =>
            {
                entity.HasKey(e => e.ResReqFldId);

                entity.ToTable("syInstResFldsReq");

                entity.HasIndex(e => new { e.ResourceId, e.TblFldsId })
                    .HasName("UIX_syInstResFldsReq_ResourceId_TblFldsId")
                    .IsUnique();

                entity.Property(e => e.ResReqFldId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyIpedsResourcesReportTitle>(entity =>
            {
                entity.ToTable("syIPEDS_Resources_ReportTitle");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ReportTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SurveyTitle)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyKlassAppConfigurationSetting>(entity =>
            {
                entity.ToTable("syKlassAppConfigurationSetting");

                entity.Property(e => e.AdvantageId)
                    .IsRequired()
                    .HasMaxLength(38)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.IsCustomField)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ItemLabel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ItemStatus)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.ItemValue)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ItemValueType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.LastOperationLog)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('OK')");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.KlassEntity)
                    .WithMany(p => p.SyKlassAppConfigurationSetting)
                    .HasForeignKey(d => d.KlassEntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syKlassAppConfigurationSetting_syKlassEntity_KlassEntityId_KlassEntityId");

                entity.HasOne(d => d.KlassOperationType)
                    .WithMany(p => p.SyKlassAppConfigurationSetting)
                    .HasForeignKey(d => d.KlassOperationTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syKlassAppConfigurationSetting_syKlassOperationType_KlassOperationTypeId_KlassOperationTypeId");
            });

            modelBuilder.Entity<SyKlassEntity>(entity =>
            {
                entity.HasKey(e => e.KlassEntityId);

                entity.ToTable("syKlassEntity");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syKlassEntity_Code")
                    .IsUnique();

                entity.Property(e => e.KlassEntityId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyKlassOperationType>(entity =>
            {
                entity.HasKey(e => e.KlassOperationTypeId);

                entity.ToTable("syKlassOperationType");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syKlassOperationType_Code")
                    .IsUnique();

                entity.Property(e => e.KlassOperationTypeId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('')");
            });

            modelBuilder.Entity<SyLangs>(entity =>
            {
                entity.HasKey(e => e.LangId);

                entity.ToTable("syLangs");

                entity.Property(e => e.LangName)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyLeadStatusChangePermissions>(entity =>
            {
                entity.HasKey(e => e.LeadStatusChangePermissionId);

                entity.ToTable("syLeadStatusChangePermissions");

                entity.HasIndex(e => new { e.LeadStatusChangeId, e.RoleId })
                    .HasName("UIX_syLeadStatusChangePermissions_LeadStatusChangeId_RoleId")
                    .IsUnique();

                entity.Property(e => e.LeadStatusChangePermissionId)
                    .HasColumnName("LeadStatusChangePermissionID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.LeadStatusChange)
                    .WithMany(p => p.SyLeadStatusChangePermissions)
                    .HasForeignKey(d => d.LeadStatusChangeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChangePermissions_syLeadStatusChanges_LeadStatusChangeId_LeadStatusChangeId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.SyLeadStatusChangePermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChangePermissions_syRoles_RoleId_RoleId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyLeadStatusChangePermissions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChangePermissions_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyLeadStatusChanges>(entity =>
            {
                entity.HasKey(e => e.LeadStatusChangeId);

                entity.ToTable("syLeadStatusChanges");

                entity.HasIndex(e => new { e.CampGrpId, e.OrigStatusId, e.NewStatusId })
                    .HasName("UIX_syLeadStatusChanges_CampGrpId_OrigStatusId_NewStatusId")
                    .IsUnique();

                entity.Property(e => e.LeadStatusChangeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyLeadStatusChanges)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChanges_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.SyLeadStatusChangesNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChanges_syStatusCodes_NewStatusId_StatusCodeId");

                entity.HasOne(d => d.OrigStatus)
                    .WithMany(p => p.SyLeadStatusChangesOrigStatus)
                    .HasForeignKey(d => d.OrigStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId");
            });

            modelBuilder.Entity<SyLeadStatusesChanges>(entity =>
            {
                entity.HasKey(e => e.StatusChangeId);

                entity.ToTable("syLeadStatusesChanges");

                entity.Property(e => e.StatusChangeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DateOfChange).HasColumnType("datetime");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Lead)
                    .WithMany(p => p.SyLeadStatusesChanges)
                    .HasForeignKey(d => d.LeadId)
                    .HasConstraintName("FK_syLeadStatusesChanges_adLeads_LeadId_LeadId");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.SyLeadStatusesChangesNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syLeadStatusesChanges_syStatusCodes_NewStatusId_StatusCodeId");

                entity.HasOne(d => d.OrigStatus)
                    .WithMany(p => p.SyLeadStatusesChangesOrigStatus)
                    .HasForeignKey(d => d.OrigStatusId)
                    .HasConstraintName("FK_syLeadStatusesChanges_syStatusCodes_OrigStatusId_StatusCodeId");
            });

            modelBuilder.Entity<SyLoggerDualEnrollment>(entity =>
            {
                entity.HasKey(e => e.LoggerId);

                entity.Property(e => e.ClassId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ClassSection)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequirementCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.StudentId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyMenuItems>(entity =>
            {
                entity.HasKey(e => e.MenuItemId);

                entity.ToTable("syMenuItems");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.MenuName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleCode)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Mrutype).HasColumnName("MRUType");

                entity.Property(e => e.Url).HasMaxLength(250);

                entity.HasOne(d => d.MenuItemType)
                    .WithMany(p => p.SyMenuItems)
                    .HasForeignKey(d => d.MenuItemTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMenuItems_syMenuItemType_MenuItemTypeId_MenuItemTypeId");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_syMenuItems_syMenuItems_ParentId_MenuItemId");
            });

            modelBuilder.Entity<SyMenuItemType>(entity =>
            {
                entity.HasKey(e => e.MenuItemTypeId);

                entity.ToTable("syMenuItemType");

                entity.Property(e => e.MenuItemTypeId).ValueGeneratedNever();

                entity.Property(e => e.MenuItemType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyMessageGroups>(entity =>
            {
                entity.HasKey(e => e.MessageGroupId);

                entity.ToTable("syMessageGroups");

                entity.Property(e => e.MessageGroupId).ValueGeneratedNever();

                entity.Property(e => e.GroupDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(((2005)-(1))-(1))");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no user selected')");

                entity.Property(e => e.XmlData).HasColumnType("text");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyMessageGroups)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageGroups_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.MessageSchema)
                    .WithMany(p => p.SyMessageGroups)
                    .HasForeignKey(d => d.MessageSchemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageGroups_syMessageSchemas_MessageSchemaId_MessageSchemaId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyMessageGroups)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageGroups_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyMessageGroupSchemas>(entity =>
            {
                entity.HasKey(e => e.MessageSchemaId);

                entity.ToTable("syMessageGroupSchemas");

                entity.Property(e => e.MessageSchemaId).ValueGeneratedNever();

                entity.Property(e => e.HtmlXslt)
                    .HasColumnName("Html_Xslt")
                    .HasColumnType("text");

                entity.Property(e => e.SpsData)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.SqlStatement).HasColumnType("text");

                entity.Property(e => e.XmlDefaultData).HasColumnType("text");

                entity.HasOne(d => d.MessageSchema)
                    .WithOne(p => p.SyMessageGroupSchemas)
                    .HasForeignKey<SyMessageGroupSchemas>(d => d.MessageSchemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageGroupSchemas_syMessageSchemas_MessageSchemaId_MessageSchemaId");
            });

            modelBuilder.Entity<SyMessages>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.ToTable("syMessages");

                entity.Property(e => e.MessageId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ActuallySentOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.HtmlContent).HasColumnType("text");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2005-01-01')");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.XmlContent)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.MessageTemplate)
                    .WithMany(p => p.SyMessages)
                    .HasForeignKey(d => d.MessageTemplateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessages_syMessageTemplates_MessageTemplateId_MessageTemplateId");
            });

            modelBuilder.Entity<SyMessageSchemas>(entity =>
            {
                entity.HasKey(e => e.MessageSchemaId);

                entity.ToTable("syMessageSchemas");

                entity.Property(e => e.MessageSchemaId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.MessageSchema).HasColumnType("text");

                entity.Property(e => e.MessageSchemaDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MessageSchemaUrl)
                    .HasColumnName("MessageSchemaURL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("('2005-01-01')");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no user')");

                entity.Property(e => e.SqlStatement).HasColumnType("text");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyMessageSchemas)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageSchemas_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyMessageSchemas)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageSchemas_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyMessageTemplates>(entity =>
            {
                entity.HasKey(e => e.MessageTemplateId);

                entity.ToTable("syMessageTemplates");

                entity.Property(e => e.MessageTemplateId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.DelayTimeSpanUnitId).HasDefaultValueSql("((1))");

                entity.Property(e => e.KeepHistory)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.MessageFormatId).HasDefaultValueSql("((1))");

                entity.Property(e => e.MessageTypeId).HasDefaultValueSql("((1))");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(((2005)-(1))-(1))");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('no user selected')");

                entity.Property(e => e.RecipientTypeId).HasDefaultValueSql("((1))");

                entity.Property(e => e.SpsData).HasColumnType("text");

                entity.Property(e => e.SqlStatement).HasColumnType("text");

                entity.Property(e => e.TemplateDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.XslFo)
                    .HasColumnName("Xsl_Fo")
                    .HasColumnType("text");

                entity.Property(e => e.XsltHtml)
                    .HasColumnName("Xslt_Html")
                    .HasColumnType("text");

                entity.Property(e => e.XsltRtf)
                    .HasColumnName("Xslt_Rtf")
                    .HasColumnType("text");

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyMessageTemplates)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageTemplates_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.MessageSchema)
                    .WithMany(p => p.SyMessageTemplates)
                    .HasForeignKey(d => d.MessageSchemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageTemplates_syMessageSchemas_MessageSchemaId_MessageSchemaId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyMessageTemplates)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syMessageTemplates_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyModCaptions>(entity =>
            {
                entity.HasKey(e => e.ModCapId);

                entity.ToTable("syModCaptions");

                entity.Property(e => e.ModCapId).ValueGeneratedNever();

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyModuleDef>(entity =>
            {
                entity.HasKey(e => e.ModDefId);

                entity.ToTable("syModuleDef");

                entity.Property(e => e.ModDefId).ValueGeneratedNever();

                entity.Property(e => e.ChildType)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.SyModuleDef)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_syModuleDef_syModules_ModuleId_ModuleID");
            });

            modelBuilder.Entity<SyModuleEntities>(entity =>
            {
                entity.HasKey(e => e.ModuleEntityId);

                entity.ToTable("syModuleEntities");

                entity.Property(e => e.ModuleEntityId).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.SyModuleEntities)
                    .HasForeignKey(d => d.EntityId)
                    .HasConstraintName("FK_syModuleEntities_syEntities_EntityId_EntityId");
            });

            modelBuilder.Entity<SyModules>(entity =>
            {
                entity.HasKey(e => e.ModuleId);

                entity.ToTable("syModules");

                entity.HasIndex(e => e.ModuleCode)
                    .HasName("UIX_syModules_ModuleCode")
                    .IsUnique();

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.AllowSdf).HasColumnName("AllowSDF");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleName)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyMrus>(entity =>
            {
                entity.HasKey(e => e.Mruid);

                entity.ToTable("syMRUS");

                entity.Property(e => e.Mruid)
                    .HasColumnName("MRUId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Counter).ValueGeneratedOnAdd();

                entity.Property(e => e.IsSticky).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MrutypeId).HasColumnName("MRUTypeId");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyMrus)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_syMRUS_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SyMrus)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_syMRUS_syUsers_UserId_UserId");
            });

            modelBuilder.Entity<SyMrutypes>(entity =>
            {
                entity.HasKey(e => e.MrutypeId);

                entity.ToTable("syMRUTypes");

                entity.Property(e => e.MrutypeId).HasColumnName("MRUTypeId");

                entity.Property(e => e.Mrutype)
                    .IsRequired()
                    .HasColumnName("MRUType")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyNavigationNodes>(entity =>
            {
                entity.HasKey(e => e.HierarchyId);

                entity.ToTable("syNavigationNodes");

                entity.HasIndex(e => e.HierarchyId)
                    .HasName("UIX_syNavigationNodes_HierarchyId")
                    .IsUnique();

                entity.Property(e => e.HierarchyId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.IsPopupWindow).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsShipped).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syNavigationNodes_syNavigationNodes_ParentId_HierarchyId");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyNavigationNodes)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_syNavigationNodes_syResources_ResourceId_ResourceID");
            });

            modelBuilder.Entity<SyNotesPageFields>(entity =>
            {
                entity.HasKey(e => e.PageFieldId);

                entity.ToTable("syNotesPageFields");

                entity.Property(e => e.PageFieldId).ValueGeneratedNever();

                entity.Property(e => e.FieldCaption)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PageName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyPeriods>(entity =>
            {
                entity.HasKey(e => e.PeriodId);

                entity.ToTable("syPeriods");

                entity.HasIndex(e => new { e.PeriodDescrip, e.PeriodId, e.StartTimeId, e.EndTimeId })
                    .HasName("IX_syPeriods_PeriodId_StartTimeId_EndTimeId_PeriodDescrip");

                entity.Property(e => e.PeriodId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyPeriods)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPeriods_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.EndTime)
                    .WithMany(p => p.SyPeriodsEndTime)
                    .HasForeignKey(d => d.EndTimeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPeriods_cmTimeInterval_EndTimeId_TimeIntervalId");

                entity.HasOne(d => d.StartTime)
                    .WithMany(p => p.SyPeriodsStartTime)
                    .HasForeignKey(d => d.StartTimeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPeriods_cmTimeInterval_StartTimeId_TimeIntervalId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyPeriods)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPeriods_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyPeriodsWorkDays>(entity =>
            {
                entity.HasKey(e => e.PeriodIdWorkDayId);

                entity.ToTable("syPeriodsWorkDays");

                entity.Property(e => e.PeriodIdWorkDayId).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SyPhoneStatuses>(entity =>
            {
                entity.HasKey(e => e.PhoneStatusId);

                entity.ToTable("syPhoneStatuses");

                entity.Property(e => e.PhoneStatusId)
                    .HasColumnName("PhoneStatusID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.PhoneStatusCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneStatusDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyPhoneStatuses)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syPhoneStatuses_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyPhoneStatuses)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syPhoneStatuses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyPhoneType>(entity =>
            {
                entity.HasKey(e => e.PhoneTypeId);

                entity.ToTable("syPhoneType");

                entity.HasIndex(e => new { e.PhoneTypeCode, e.PhoneTypeDescrip, e.CampGrpId })
                    .HasName("UIX_syPhoneType_PhoneTypeCode_PhoneTypeDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.PhoneTypeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneTypeCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneTypeDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyPhoneType)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syPhoneType_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyPhoneType)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syPhoneType_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyPositions>(entity =>
            {
                entity.HasKey(e => e.PositionId);

                entity.ToTable("syPositions");

                entity.HasIndex(e => new { e.PositionCode, e.PositionDescrip, e.CampGrpId })
                    .HasName("UIX_syPositions_PositionCode_PositionDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.PositionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PositionCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PositionDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyPositions)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syPositions_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyPositions)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPositions_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyPrefixes>(entity =>
            {
                entity.HasKey(e => e.PrefixId);

                entity.ToTable("syPrefixes");

                entity.Property(e => e.PrefixId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrefixCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PrefixDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyPrefixes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPrefixes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyPrefixes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syPrefixes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyProgramUnitTypes>(entity =>
            {
                entity.HasKey(e => e.UnitId);

                entity.ToTable("syProgramUnitTypes");

                entity.Property(e => e.UnitId).ValueGeneratedNever();

                entity.Property(e => e.UnitDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyQuickLinks>(entity =>
            {
                entity.HasKey(e => e.QuickLinkId);

                entity.ToTable("syQuickLinks");

                entity.Property(e => e.QuickLinkId)
                    .HasColumnName("QuickLinkID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("URL")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyQuickLinks)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syQuickLinks_syStatuses_StatusID_StatusId");
            });

            modelBuilder.Entity<SyR2t4calculationPeriodTypes>(entity =>
            {
                entity.HasKey(e => e.CalculationPeriodTypeId);

                entity.ToTable("syR2T4CalculationPeriodTypes");

                entity.Property(e => e.CalculationPeriodTypeId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.HasOne(d => d.ModifiedByUser)
                    .WithMany(p => p.SyR2t4calculationPeriodTypes)
                    .HasForeignKey(d => d.ModifiedByUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syR2T4CalculationPeriodTypes_syUsers_ModifiedByUserId_UserId");
            });

            modelBuilder.Entity<SyRdfsiteSums>(entity =>
            {
                entity.HasKey(e => e.RdfsiteSumId);

                entity.ToTable("syRDFSiteSums");

                entity.Property(e => e.RdfsiteSumId)
                    .HasColumnName("RDFSiteSumId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RdfsiteSumCode)
                    .IsRequired()
                    .HasColumnName("RDFSiteSumCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RdfsiteSumDescrip)
                    .IsRequired()
                    .HasColumnName("RDFSiteSumDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyRdfsiteSums)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syRDFSiteSums_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyRdfsiteSums)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRDFSiteSums_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyReasonNotEnrolled>(entity =>
            {
                entity.HasKey(e => e.ReasonNotEnrolledId);

                entity.ToTable("syReasonNotEnrolled");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syReasonNotEnrolled_Code")
                    .IsUnique();

                entity.Property(e => e.ReasonNotEnrolledId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyReasonNotEnrolled)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syReasonNotEnrolled_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyReasonNotEnrolled)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syReasonNotEnrolled_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyRelations>(entity =>
            {
                entity.HasKey(e => e.RelationId);

                entity.ToTable("syRelations");

                entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RelationCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.RelationDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyRelationshipStatus>(entity =>
            {
                entity.HasKey(e => e.RelationshipStatusId);

                entity.ToTable("syRelationshipStatus");

                entity.Property(e => e.RelationshipStatusId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyRelationshipStatus)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syRelationshipStatus_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyRelationshipStatus)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syRelationshipStatus_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyReportDescrip>(entity =>
            {
                entity.HasKey(e => e.ReportDescriptionId);

                entity.ToTable("syReportDescrip");

                entity.Property(e => e.ReportDescriptionId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ActualRptName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FriendlyRptName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RptDescrip)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyReports>(entity =>
            {
                entity.HasKey(e => e.ReportId);

                entity.ToTable("syReports");

                entity.Property(e => e.ReportId).ValueGeneratedNever();

                entity.Property(e => e.AssemblyFilePath)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreationMethod)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Rdlname)
                    .HasColumnName("RDLName")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ReportClass)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ReportDescription)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ReportName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShowFilterMode).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShowPerformanceMsg).HasDefaultValueSql("((0))");

                entity.Property(e => e.WebServiceUrl)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyReportTabs>(entity =>
            {
                entity.HasKey(e => new { e.ReportId, e.PageViewId });

                entity.ToTable("syReportTabs");

                entity.Property(e => e.PageViewId).HasMaxLength(200);

                entity.Property(e => e.ControllerClass).HasMaxLength(200);

                entity.Property(e => e.DisplayName).HasMaxLength(200);

                entity.Property(e => e.ParameterSetLookUp).HasMaxLength(200);

                entity.Property(e => e.TabName).HasMaxLength(200);

                entity.Property(e => e.UserControlName)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<SyReportUserPrefs>(entity =>
            {
                entity.HasKey(e => e.PrefId);

                entity.ToTable("syReportUserPrefs");

                entity.Property(e => e.PrefId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.PrefData).IsUnicode(false);

                entity.Property(e => e.PrefDescrip)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PrefName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyReportUserSettings>(entity =>
            {
                entity.HasKey(e => e.UserSettingId);

                entity.ToTable("syReportUserSettings");

                entity.Property(e => e.UserSettingId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.PrefData).IsUnicode(false);

                entity.Property(e => e.PrefDescrip)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.PrefName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyReqTypes>(entity =>
            {
                entity.HasKey(e => e.ReqTypeId);

                entity.ToTable("syReqTypes");

                entity.Property(e => e.ReqTypeId).ValueGeneratedNever();

                entity.Property(e => e.ReqType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyResCaptions>(entity =>
            {
                entity.HasKey(e => e.ResourceCapId);

                entity.ToTable("syResCaptions");

                entity.Property(e => e.ResourceCapId).ValueGeneratedNever();

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyResources>(entity =>
            {
                entity.HasKey(e => e.ResourceId);

                entity.ToTable("syResources");

                entity.HasIndex(e => new { e.ResourceTypeId, e.ResourceId, e.Resource });

                entity.Property(e => e.ResourceId)
                    .HasColumnName("ResourceID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AllowSchlReqFlds).HasDefaultValueSql("((0))");

                entity.Property(e => e.DisplayName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MrutypeId).HasColumnName("MRUTypeId");

                entity.Property(e => e.Resource)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ResourceTypeId).HasColumnName("ResourceTypeID");

                entity.Property(e => e.ResourceUrl)
                    .HasColumnName("ResourceURL")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.ResourceType)
                    .WithMany(p => p.SyResources)
                    .HasForeignKey(d => d.ResourceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syResources_syResourceTypes_ResourceTypeID_ResourceTypeID");
            });

            modelBuilder.Entity<SyResourceSdf>(entity =>
            {
                entity.HasKey(e => e.PkId);

                entity.ToTable("syResourceSdf");

                entity.HasIndex(e => new { e.ResourceId, e.SdfId, e.EntityId })
                    .HasName("UIX_syResourceSdf_ResourceId_sdfID_EntityId")
                    .IsUnique();

                entity.Property(e => e.PkId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SdfId).HasColumnName("sdfID");

                entity.Property(e => e.Sdfvisibilty)
                    .HasColumnName("SDFVisibilty")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SyResourceSdf)
                    .HasForeignKey(d => d.SdfId)
                    .HasConstraintName("FK_syResourceSdf_sySDF_sdfID_SDFId");
            });

            modelBuilder.Entity<SyResourceTypes>(entity =>
            {
                entity.HasKey(e => e.ResourceTypeId);

                entity.ToTable("syResourceTypes");

                entity.Property(e => e.ResourceTypeId).HasColumnName("ResourceTypeID");

                entity.Property(e => e.ResourceType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyResTblFlds>(entity =>
            {
                entity.HasKey(e => e.ResDefId);

                entity.ToTable("syResTblFlds");

                entity.Property(e => e.ResDefId).ValueGeneratedNever();

                entity.Property(e => e.ControlName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsePageSetup)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.TblFlds)
                    .WithMany(p => p.SyResTblFlds)
                    .HasForeignKey(d => d.TblFldsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syResTblFlds_syTblFlds_TblFldsId_TblFldsId");
            });

            modelBuilder.Entity<SyRlsResLvls>(entity =>
            {
                entity.HasKey(e => e.Rrlid);

                entity.ToTable("syRlsResLvls");

                entity.HasIndex(e => new { e.RoleId, e.ResourceId, e.AccessLevel, e.ParentId })
                    .HasName("UIX_syRlsResLvls_RoleId_ResourceID_AccessLevel_ParentId")
                    .IsUnique();

                entity.Property(e => e.Rrlid)
                    .HasColumnName("RRLId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResourceId).HasColumnName("ResourceID");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyRlsResLvls)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRlsResLvls_syResources_ResourceID_ResourceID");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.SyRlsResLvls)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRlsResLvls_syRoles_RoleId_RoleId");
            });

            modelBuilder.Entity<SyRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("syRoles");

                entity.Property(e => e.RoleId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyRoles)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRoles_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.SysRole)
                    .WithMany(p => p.SyRoles)
                    .HasForeignKey(d => d.SysRoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRoles_sySysRoles_SysRoleId_SysRoleId");
            });

            modelBuilder.Entity<SyRolesModules>(entity =>
            {
                entity.HasKey(e => e.RoleModuleId);

                entity.ToTable("syRolesModules");

                entity.Property(e => e.RoleModuleId).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.SyRolesModules)
                    .HasForeignKey(d => d.ModuleId)
                    .HasConstraintName("FK_syRolesModules_syResources_ModuleId_ResourceID");
            });

            modelBuilder.Entity<SyRptAdHocFields>(entity =>
            {
                entity.HasKey(e => e.AdHocFieldId);

                entity.ToTable("syRptAdHocFields");

                entity.Property(e => e.AdHocFieldId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.FormatString)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Header)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdfid).HasColumnName("SDFId");

                entity.HasOne(d => d.AdHocField)
                    .WithOne(p => p.InverseAdHocField)
                    .HasForeignKey<SyRptAdHocFields>(d => d.AdHocFieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRptAdHocFields_syRptAdHocFields_AdHocFieldId_AdHocFieldId");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyRptAdHocFields)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_syRptAdHocFields_syResources_ResourceId_ResourceID");

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SyRptAdHocFields)
                    .HasForeignKey(d => d.Sdfid)
                    .HasConstraintName("FK_syRptAdHocFields_sySDF_SDFId_SDFId");

                entity.HasOne(d => d.TblFlds)
                    .WithMany(p => p.SyRptAdHocFields)
                    .HasForeignKey(d => d.TblFldsId)
                    .HasConstraintName("FK_syRptAdHocFields_syTblFlds_TblFldsId_TblFldsId");
            });

            modelBuilder.Entity<SyRptAdhocRelations>(entity =>
            {
                entity.HasKey(e => e.RelationId);

                entity.ToTable("syRptAdhocRelations");

                entity.Property(e => e.FkColumn)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FkTable)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PkColumn)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.PkTable)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<SyRptAgencies>(entity =>
            {
                entity.HasKey(e => e.RptAgencyId);

                entity.ToTable("syRptAgencies");

                entity.Property(e => e.RptAgencyId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyRptAgencyFields>(entity =>
            {
                entity.HasKey(e => e.RptAgencyFldId);

                entity.ToTable("syRptAgencyFields");

                entity.HasIndex(e => new { e.RptAgencyId, e.RptFldId })
                    .HasName("UIX_syRptAgencyFields_RptAgencyId_RptFldId")
                    .IsUnique();

                entity.Property(e => e.RptAgencyFldId).ValueGeneratedNever();

                entity.HasOne(d => d.RptAgency)
                    .WithMany(p => p.SyRptAgencyFields)
                    .HasForeignKey(d => d.RptAgencyId)
                    .HasConstraintName("FK_syRptAgencyFields_syRptAgencies_RptAgencyId_RptAgencyId");

                entity.HasOne(d => d.RptFld)
                    .WithMany(p => p.SyRptAgencyFields)
                    .HasForeignKey(d => d.RptFldId)
                    .HasConstraintName("FK_syRptAgencyFields_syRptFields_RptFldId_RptFldId");
            });

            modelBuilder.Entity<SyRptAgencyFldValues>(entity =>
            {
                entity.HasKey(e => e.RptAgencyFldValId);

                entity.ToTable("syRptAgencyFldValues");

                entity.Property(e => e.RptAgencyFldValId).ValueGeneratedNever();

                entity.Property(e => e.AgencyDescrip)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AgencyValue).HasMaxLength(50);

                entity.HasOne(d => d.RptAgencyFld)
                    .WithMany(p => p.SyRptAgencyFldValues)
                    .HasForeignKey(d => d.RptAgencyFldId)
                    .HasConstraintName("FK_syRptAgencyFldValues_syRptAgencyFields_RptAgencyFldId_RptAgencyFldId");
            });

            modelBuilder.Entity<SyRptAgencySchoolMapping>(entity =>
            {
                entity.HasKey(e => e.MappingId);

                entity.ToTable("syRptAgencySchoolMapping");

                entity.Property(e => e.MappingId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.RptAgencyFldVal)
                    .WithMany(p => p.SyRptAgencySchoolMapping)
                    .HasForeignKey(d => d.RptAgencyFldValId)
                    .HasConstraintName("FK_syRptAgencySchoolMapping_syRptAgencyFldValues_RptAgencyFldValId_RptAgencyFldValId");
            });

            modelBuilder.Entity<SyRptFields>(entity =>
            {
                entity.HasKey(e => e.RptFldId);

                entity.ToTable("syRptFields");

                entity.HasIndex(e => e.Descrip)
                    .HasName("UIX_syRptFields_Descrip")
                    .IsUnique();

                entity.Property(e => e.RptFldId).ValueGeneratedNever();

                entity.Property(e => e.Ddl)
                    .HasColumnName("DDL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Fld)
                    .WithMany(p => p.SyRptFields)
                    .HasForeignKey(d => d.FldId)
                    .HasConstraintName("FK_syRptFields_syFields_FldId_FldId");
            });

            modelBuilder.Entity<SyRptFilterListPrefs>(entity =>
            {
                entity.HasKey(e => e.FilterListPrefId);

                entity.ToTable("syRptFilterListPrefs");

                entity.Property(e => e.FilterListPrefId).ValueGeneratedNever();

                entity.Property(e => e.FldValue)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Pref)
                    .WithMany(p => p.SyRptFilterListPrefs)
                    .HasForeignKey(d => d.PrefId)
                    .HasConstraintName("FK_syRptFilterListPrefs_syRptUserPrefs_PrefId_PrefId");
            });

            modelBuilder.Entity<SyRptFilterOtherPrefs>(entity =>
            {
                entity.HasKey(e => e.FilterOtherPrefId);

                entity.ToTable("syRptFilterOtherPrefs");

                entity.Property(e => e.FilterOtherPrefId).ValueGeneratedNever();

                entity.Property(e => e.OpValue)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Pref)
                    .WithMany(p => p.SyRptFilterOtherPrefs)
                    .HasForeignKey(d => d.PrefId)
                    .HasConstraintName("FK_syRptFilterOtherPrefs_syRptUserPrefs_PrefId_PrefId");
            });

            modelBuilder.Entity<SyRptParams>(entity =>
            {
                entity.HasKey(e => e.RptParamId);

                entity.ToTable("syRptParams");

                entity.Property(e => e.RptParamId).ValueGeneratedNever();

                entity.Property(e => e.RptCaption)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyRptParams)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRptParams_syResources_ResourceId_ResourceID");

                entity.HasOne(d => d.TblFlds)
                    .WithMany(p => p.SyRptParams)
                    .HasForeignKey(d => d.TblFldsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRptParams_syTblFlds_TblFldsId_TblFldsId");
            });

            modelBuilder.Entity<SyRptPrefsIpedsSfapr>(entity =>
            {
                entity.HasKey(e => e.RptPrefId);

                entity.ToTable("syRptPrefsIPEDS_SFAPR");

                entity.Property(e => e.RptPrefId).HasDefaultValueSql("(newid())");
            });

            modelBuilder.Entity<SyRptProps>(entity =>
            {
                entity.HasKey(e => e.RptPropsId);

                entity.ToTable("syRptProps");

                entity.Property(e => e.RptPropsId).ValueGeneratedNever();

                entity.Property(e => e.AllowFilters)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.AllowSortOrder)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.BaseCaonRegClasses)
                    .HasColumnName("BaseCAOnRegClasses")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.RptSqlid).HasColumnName("RptSQLId");

                entity.Property(e => e.SelectFilters)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.ShowLda)
                    .HasColumnName("ShowLDA")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ShowTransferCampus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShowTransferProgram).HasDefaultValueSql("((0))");

                entity.Property(e => e.ShowUseStuCurrStatus).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyRptProps)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syRptProps_syResources_ResourceId_ResourceID");

                entity.HasOne(d => d.RptObj)
                    .WithMany(p => p.SyRptProps)
                    .HasForeignKey(d => d.RptObjId)
                    .HasConstraintName("FK_syRptProps_syBusObjects_RptObjId_BusObjectId");
            });

            modelBuilder.Entity<SyRptSortPrefs>(entity =>
            {
                entity.HasKey(e => e.SortPrefId);

                entity.ToTable("syRptSortPrefs");

                entity.Property(e => e.SortPrefId).ValueGeneratedNever();

                entity.HasOne(d => d.Pref)
                    .WithMany(p => p.SyRptSortPrefs)
                    .HasForeignKey(d => d.PrefId)
                    .HasConstraintName("FK_syRptSortPrefs_syRptUserPrefs_PrefId_PrefId");
            });

            modelBuilder.Entity<SyRptSql>(entity =>
            {
                entity.HasKey(e => e.RptSqlid);

                entity.ToTable("syRptSQL");

                entity.Property(e => e.RptSqlid)
                    .HasColumnName("RptSQLId")
                    .ValueGeneratedNever();

                entity.Property(e => e.OrderByClause)
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.Sqlstmt)
                    .IsRequired()
                    .HasColumnName("SQLStmt")
                    .HasMaxLength(8000)
                    .IsUnicode(false);

                entity.Property(e => e.WhereClause)
                    .HasMaxLength(8000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyRptSqlcount>(entity =>
            {
                entity.HasKey(e => e.RptSqlcountId);

                entity.ToTable("syRptSQLCount");

                entity.Property(e => e.RptSqlcountId)
                    .HasColumnName("RptSQLCountID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RptCountStmt)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.RptSqlid).HasColumnName("RptSQLID");
            });

            modelBuilder.Entity<SyRptUserPrefs>(entity =>
            {
                entity.HasKey(e => e.PrefId);

                entity.ToTable("syRptUserPrefs");

                entity.Property(e => e.PrefId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.PrefName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySchedulingMethods>(entity =>
            {
                entity.HasKey(e => e.SchedMethodId);

                entity.ToTable("sySchedulingMethods");

                entity.Property(e => e.SchedMethodId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySchoolLogo>(entity =>
            {
                entity.HasKey(e => e.ImgId);

                entity.ToTable("sySchoolLogo");

                entity.Property(e => e.ImgId).ValueGeneratedNever();

                entity.Property(e => e.ContentType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Image).HasColumnType("image");

                entity.Property(e => e.ImageCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ImgFile)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySchoolStateBoardReports>(entity =>
            {
                entity.HasKey(e => e.SchoolStateBoardReportId);

                entity.ToTable("sySchoolStateBoardReports");

                entity.Property(e => e.SchoolStateBoardReportId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfficerName1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfficerName2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OfficerName3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerLicenseNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SySchoolStateBoardReports)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_sySchoolStateBoardReports_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.LicensingAgency)
                    .WithMany(p => p.SySchoolStateBoardReports)
                    .HasForeignKey(d => d.LicensingAgencyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySchoolStateBoardReports_syStateBoardAgencies_LicensingAgencyId_StateBoardAgencyId");

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.SySchoolStateBoardReports)
                    .HasForeignKey(d => d.ReportId)
                    .HasConstraintName("FK_sySchoolStateBoardReports_syReports_ReportId_ReportId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SySchoolStateBoardReports)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("FK_sySchoolStateBoardReports_syStates_StateId_StateId");
            });

            modelBuilder.Entity<SySdf>(entity =>
            {
                entity.HasKey(e => e.Sdfid);

                entity.ToTable("sySDF");

                entity.Property(e => e.Sdfid)
                    .HasColumnName("SDFId")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.DtypeId).HasColumnName("DTypeId");

                entity.Property(e => e.HelpText)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdfdescrip)
                    .IsRequired()
                    .HasColumnName("SDFDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SySdf)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_sySDF_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SySdfDtypes>(entity =>
            {
                entity.HasKey(e => e.DtypeId);

                entity.ToTable("sySdfDTypes");

                entity.Property(e => e.DtypeId).HasColumnName("DTypeId");

                entity.Property(e => e.Dtype)
                    .IsRequired()
                    .HasColumnName("DType")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySdfmoduleValue>(entity =>
            {
                entity.HasKey(e => e.Sdfpkid);

                entity.ToTable("sySDFModuleValue");

                entity.HasIndex(e => new { e.PgPkid, e.Sdfid })
                    .HasName("UIX_sySDFModuleValue_PgPKID_SDFID")
                    .IsUnique();

                entity.Property(e => e.Sdfpkid)
                    .HasColumnName("SDFPKID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModUser)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PgPkid)
                    .HasColumnName("PgPKID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.Sdfid).HasColumnName("SDFID");

                entity.Property(e => e.Sdfvalue)
                    .HasColumnName("SDFValue")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SySdfmoduleValue)
                    .HasForeignKey(d => d.Sdfid)
                    .HasConstraintName("FK_sySDFModuleValue_sySDF_SDFID_SDFId");
            });

            modelBuilder.Entity<SySdfModVis>(entity =>
            {
                entity.HasKey(e => e.SdfModVisId);

                entity.ToTable("sySdfModVis");

                entity.Property(e => e.SdfModVisId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdfid).HasColumnName("SDFId");

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SySdfModVis)
                    .HasForeignKey(d => d.Sdfid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySdfModVis_sySDF_SDFId_SDFId");
            });

            modelBuilder.Entity<SySdfRange>(entity =>
            {
                entity.HasKey(e => e.SdfRangeId);

                entity.ToTable("sySdfRange");

                entity.Property(e => e.SdfRangeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.MaxVal)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MinVal)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.Moduser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdfid).HasColumnName("SDFId");

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SySdfRange)
                    .HasForeignKey(d => d.Sdfid)
                    .HasConstraintName("FK_sySdfRange_sySDF_SDFId_SDFId");
            });

            modelBuilder.Entity<SySdfValList>(entity =>
            {
                entity.HasKey(e => e.SdfListId);

                entity.ToTable("sySdfValList");

                entity.Property(e => e.SdfListId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sdfid).HasColumnName("SDFId");

                entity.Property(e => e.ValList)
                    .IsRequired()
                    .HasMaxLength(5000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Sdf)
                    .WithMany(p => p.SySdfValList)
                    .HasForeignKey(d => d.Sdfid)
                    .HasConstraintName("FK_sySdfValList_sySDF_SDFId_SDFId");
            });

            modelBuilder.Entity<SySdfValTypes>(entity =>
            {
                entity.HasKey(e => e.ValTypeId);

                entity.ToTable("sySdfValTypes");

                entity.Property(e => e.ValType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySdfvisiblity>(entity =>
            {
                entity.HasKey(e => e.VisId);

                entity.ToTable("sySDFVisiblity");

                entity.Property(e => e.Sdfvisibility)
                    .IsRequired()
                    .HasColumnName("SDFVisibility")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyStateBoardAgencies>(entity =>
            {
                entity.HasKey(e => e.StateBoardAgencyId);

                entity.ToTable("syStateBoardAgencies");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LicensingAddress1)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicensingAddress2)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicensingCity)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicensingZipCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.SyStateBoardAgencies)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardAgencies_syUsers_CreatedById_UserId");

                entity.HasOne(d => d.LicensingCountry)
                    .WithMany(p => p.SyStateBoardAgencies)
                    .HasForeignKey(d => d.LicensingCountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySchoolStateBoardReports_adCountries_LicensingCountryId_CountryId");

                entity.HasOne(d => d.LicensingState)
                    .WithMany(p => p.SyStateBoardAgenciesLicensingState)
                    .HasForeignKey(d => d.LicensingStateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySchoolStateBoardReports_syStates_LicensingStateId_StateId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SyStateBoardAgenciesState)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardAgencies_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyStateBoardAgencies)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardAgencies_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyStateBoardCourses>(entity =>
            {
                entity.HasKey(e => e.StateBoardCourseId);

                entity.ToTable("syStateBoardCourses");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.SyStateBoardCourses)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardCourses_syUsers_CreatedById_UserId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SyStateBoardCourses)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardCourses_syStates_StateId_StateId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyStateBoardCourses)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardCourses_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyStateBoardProgramCourseMappings>(entity =>
            {
                entity.HasKey(e => e.StateBoardProgramCourseMappingId);

                entity.ToTable("syStateBoardProgramCourseMappings");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.SyStateBoardProgramCourseMappings)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardProgramCourseMappings_syUsers_CreatedById_UserId");

                entity.HasOne(d => d.Program)
                    .WithMany(p => p.SyStateBoardProgramCourseMappings)
                    .HasForeignKey(d => d.ProgramId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardProgramCourseMappings_arProgram_ProgramId_ProgId");

                entity.HasOne(d => d.SchoolStateBoardReport)
                    .WithMany(p => p.SyStateBoardProgramCourseMappings)
                    .HasForeignKey(d => d.SchoolStateBoardReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardProgramCourseMappings_sySchoolStateBoardReports_SchoolStateBoardReportId_SchoolStateBoardReportId");

                entity.HasOne(d => d.StateBoardCourse)
                    .WithMany(p => p.SyStateBoardProgramCourseMappings)
                    .HasForeignKey(d => d.StateBoardCourseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateBoardProgramCourseMappings_syStateBoardCourses_StateBoardCourseId_StateBoardCourseId");
            });

            modelBuilder.Entity<SyStateReports>(entity =>
            {
                entity.HasKey(e => e.StateReportId);

                entity.ToTable("syStateReports");

                entity.Property(e => e.StateReportId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Report)
                    .WithMany(p => p.SyStateReports)
                    .HasForeignKey(d => d.ReportId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateReports_syReports_ReportId_ReportId");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.SyStateReports)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStateReports_syStates_StateId_StateId");
            });

            modelBuilder.Entity<SyStates>(entity =>
            {
                entity.HasKey(e => e.StateId);

                entity.ToTable("syStates");

                entity.Property(e => e.StateId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Fipscode).HasColumnName("fipscode");

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.StateDescrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyStates)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syStates_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.SyStates)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("FK_syStates_adCountries_countryId_countryId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyStates)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syStates_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyStatusChangeDeleteReasons>(entity =>
            {
                entity.ToTable("syStatusChangeDeleteReasons");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyStatusChangesDeleted>(entity =>
            {
                entity.ToTable("syStatusChangesDeleted");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CaseNumber).HasMaxLength(50);

                entity.Property(e => e.DateOfChange).HasColumnType("datetime");

                entity.Property(e => e.DeleteDate).HasColumnType("datetime");

                entity.Property(e => e.HaveBackup).HasDefaultValueSql("((0))");

                entity.Property(e => e.HaveClientConfirmation).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedBy).HasMaxLength(100);

                entity.Property(e => e.UserDeleted)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyStatusChangesDeleted)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusChangesDeleted_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.DeleteReasons)
                    .WithMany(p => p.SyStatusChangesDeleted)
                    .HasForeignKey(d => d.DeleteReasonsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusChangesDeleted_syStatusChangeDeleteReasons_DeleteReasonsId_Id");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.SyStatusChangesDeletedNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .HasConstraintName("FK_syStatusChangesDeleted_syStatusCodes_NewStatusId_StatusCodeId");

                entity.HasOne(d => d.OrigStatus)
                    .WithMany(p => p.SyStatusChangesDeletedOrigStatus)
                    .HasForeignKey(d => d.OrigStatusId)
                    .HasConstraintName("FK_syStatusChangesDeleted_syStatusCodes_OrigStatusId_StatusCodeId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.SyStatusChangesDeleted)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusChangesDeleted_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<SyStatusCodes>(entity =>
            {
                entity.HasKey(e => e.StatusCodeId);

                entity.ToTable("syStatusCodes");

                entity.HasIndex(e => new { e.StatusCode, e.StatusCodeDescrip, e.CampGrpId })
                    .HasName("UIX_syStatusCodes_StatusCode_StatusCodeDescrip_CampGrpId")
                    .IsUnique();

                entity.Property(e => e.StatusCodeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.IsDefaultLeadStatus).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StatusCodeDescrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyStatusCodes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusCodes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyStatusCodes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusCodes_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.SysStatus)
                    .WithMany(p => p.SyStatusCodes)
                    .HasForeignKey(d => d.SysStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStatusCodes_SySysStatus_SysStatusId_SysStatusId");
            });

            modelBuilder.Entity<SyStatuses>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("syStatuses");

                entity.Property(e => e.StatusId).ValueGeneratedNever();

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.StatusCode)
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyStatusLevels>(entity =>
            {
                entity.HasKey(e => e.StatusLevelId);

                entity.ToTable("syStatusLevels");

                entity.Property(e => e.StatusLevelId).ValueGeneratedNever();

                entity.Property(e => e.StatusLevelDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyStudentAttendanceSummary>(entity =>
            {
                entity.HasKey(e => e.StudentAttendanceSummaryId);

                entity.ToTable("syStudentAttendanceSummary");

                entity.HasIndex(e => new { e.ActualAbsentDaysConvertToHours, e.StuEnrollId, e.TermStartDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualAbsentDays_ConvertTo_Hours");

                entity.HasIndex(e => new { e.ActualAbsentDaysConvertToHoursCalc, e.StuEnrollId, e.StudentAttendedDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ActualAbsentDays_ConvertTo_Hours_Calc");

                entity.HasIndex(e => new { e.ActualPresentDaysConvertToHours, e.StuEnrollId, e.TermStartDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualPresentDays_ConvertTo_Hours");

                entity.HasIndex(e => new { e.ActualPresentDaysConvertToHoursCalc, e.StuEnrollId, e.StudentAttendedDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ActualPresentDays_ConvertTo_Hours_Calc");

                entity.HasIndex(e => new { e.ActualTardyDaysConvertToHours, e.StuEnrollId, e.TermStartDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualTardyDays_ConvertTo_Hours");

                entity.HasIndex(e => new { e.ScheduledHours, e.StuEnrollId, e.StudentAttendedDate, e.TermId })
                    .HasName("IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ScheduledHours");

                entity.HasIndex(e => new { e.ActualRunningMakeupDays, e.ActualRunningScheduledDays, e.AdjustedAbsentDays, e.AdjustedPresentDays, e.StuEnrollId })
                    .HasName("IX_syStudentAttendanceSummary_C1_C6_C9_C11_C12");

                entity.HasIndex(e => new { e.ActualRunningMakeupDays, e.ActualRunningScheduledDays, e.AdjustedAbsentDays, e.AdjustedPresentDays, e.StuEnrollId, e.StudentAttendedDate })
                    .HasName("IX_syStudentAttendanceSummary_C1_C3_C6_C9_C11_C12");

                entity.Property(e => e.ActualAbsentDaysConvertToHours).HasColumnName("ActualAbsentDays_ConvertTo_Hours");

                entity.Property(e => e.ActualAbsentDaysConvertToHoursCalc).HasColumnName("ActualAbsentDays_ConvertTo_Hours_Calc");

                entity.Property(e => e.ActualPresentDaysConvertToHours).HasColumnName("ActualPresentDays_ConvertTo_Hours");

                entity.Property(e => e.ActualPresentDaysConvertToHoursCalc).HasColumnName("ActualPresentDays_ConvertTo_Hours_Calc");

                entity.Property(e => e.ActualRunningExcusedDays).HasDefaultValueSql("((0))");

                entity.Property(e => e.ActualTardyDaysConvertToHours).HasColumnName("ActualTardyDays_ConvertTo_Hours");

                entity.Property(e => e.AttendanceTrackType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsExcused).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTardy).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StudentAttendedDate).HasColumnType("datetime");

                entity.Property(e => e.Tardiesmakingabsence).HasColumnName("tardiesmakingabsence");

                entity.Property(e => e.TermDescrip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TermStartDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SyStudentFormat>(entity =>
            {
                entity.HasKey(e => e.StudentFormatId);

                entity.ToTable("syStudentFormat");

                entity.Property(e => e.FnameNumber).HasColumnName("FNameNumber");

                entity.Property(e => e.FormatType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LnameNumber).HasColumnName("LNameNumber");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyStudentStatusChanges>(entity =>
            {
                entity.HasKey(e => e.StudentStatusChangeId);

                entity.ToTable("syStudentStatusChanges");

                entity.HasIndex(e => e.StuEnrollId);

                entity.Property(e => e.StudentStatusChangeId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CaseNumber).HasMaxLength(50);

                entity.Property(e => e.DateOfChange).HasColumnType("datetime");

                entity.Property(e => e.HaveBackup).HasDefaultValueSql("((0))");

                entity.Property(e => e.HaveClientConfirmation).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedBy).HasMaxLength(100);

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyStudentStatusChanges)
                    .HasForeignKey(d => d.CampusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStudentStatusChanges_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.NewStatus)
                    .WithMany(p => p.SyStudentStatusChangesNewStatus)
                    .HasForeignKey(d => d.NewStatusId)
                    .HasConstraintName("FK_syStudentStatusChanges_syStatusCodes_NewStatusId_StatusCodeId");

                entity.HasOne(d => d.OrigStatus)
                    .WithMany(p => p.SyStudentStatusChangesOrigStatus)
                    .HasForeignKey(d => d.OrigStatusId)
                    .HasConstraintName("FK_syStudentStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId");

                entity.HasOne(d => d.StuEnroll)
                    .WithMany(p => p.SyStudentStatusChanges)
                    .HasForeignKey(d => d.StuEnrollId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStudentStatusChanges_arStuEnrollments_StuEnrollId_StuEnrollId");
            });

            modelBuilder.Entity<SyStuRestrictions>(entity =>
            {
                entity.HasKey(e => e.StuRestrictionId);

                entity.ToTable("syStuRestrictions");

                entity.Property(e => e.StuRestrictionId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SgroupId).HasColumnName("SGroupId");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.SyStuRestrictions)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStuRestrictions_syDepartments_DepartmentId_DepartmentId");

                entity.HasOne(d => d.RestrictionType)
                    .WithMany(p => p.SyStuRestrictions)
                    .HasForeignKey(d => d.RestrictionTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStuRestrictions_syStuRestrictionTypes_RestrictionTypeId_RestrictionTypeId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SyStuRestrictions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStuRestrictions_syUsers_UserId_UserId");
            });

            modelBuilder.Entity<SyStuRestrictionTypes>(entity =>
            {
                entity.HasKey(e => e.RestrictionTypeId);

                entity.ToTable("syStuRestrictionTypes");

                entity.Property(e => e.RestrictionTypeId).ValueGeneratedNever();

                entity.Property(e => e.BlkFa).HasColumnName("BlkFA");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyStuRestrictionTypes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStuRestrictionTypes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyStuRestrictionTypes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syStuRestrictionTypes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SySubMods>(entity =>
            {
                entity.HasKey(e => e.SubModId);

                entity.ToTable("sySubMods");

                entity.Property(e => e.SubModId)
                    .HasColumnName("SubModID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResourceTypeId).HasColumnName("ResourceTypeID");

                entity.Property(e => e.SubModName)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySuffixes>(entity =>
            {
                entity.HasKey(e => e.SuffixId);

                entity.ToTable("sySuffixes");

                entity.Property(e => e.SuffixId).ValueGeneratedNever();

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SuffixCode)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SuffixDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SySuffixes)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySuffixes_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SySuffixes)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySuffixes_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SySummLists>(entity =>
            {
                entity.HasKey(e => e.SummListId);

                entity.ToTable("sySummLists");

                entity.Property(e => e.SummListId).ValueGeneratedNever();

                entity.Property(e => e.SummListName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySysDocStatuses>(entity =>
            {
                entity.HasKey(e => e.SysDocStatusId);

                entity.ToTable("sySysDocStatuses");

                entity.Property(e => e.DocStatusDescrip)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SySysRoles>(entity =>
            {
                entity.HasKey(e => e.SysRoleId);

                entity.ToTable("sySysRoles");

                entity.Property(e => e.SysRoleId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleTypeId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.RoleType)
                    .WithMany(p => p.SySysRoles)
                    .HasForeignKey(d => d.RoleTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySysRoles_syUserType_RoleTypeId_UserTypeId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SySysRoles)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySysRoles_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SySysStatus>(entity =>
            {
                entity.HasKey(e => e.SysStatusId);

                entity.ToTable("sySysStatus");

                entity.Property(e => e.SysStatusId).ValueGeneratedNever();

                entity.Property(e => e.GeprogramStatus)
                    .HasColumnName("GEProgramStatus")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SysStatusDescrip)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SySysStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySysStatus_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.StatusLevel)
                    .WithMany(p => p.SySysStatus)
                    .HasForeignKey(d => d.StatusLevelId)
                    .HasConstraintName("FK_sySysStatus_syStatusLevels_StatusLevelId_StatusLevelId");
            });

            modelBuilder.Entity<SySystemStatusWorkflowRules>(entity =>
            {
                entity.ToTable("sySystemStatusWorkflowRules");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UniqueId).HasDefaultValueSql("(newid())");

                entity.HasOne(d => d.StatusIdFromNavigation)
                    .WithMany(p => p.SySystemStatusWorkflowRulesStatusIdFromNavigation)
                    .HasForeignKey(d => d.StatusIdFrom)
                    .HasConstraintName("FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdFrom_SysStatusId");

                entity.HasOne(d => d.StatusIdToNavigation)
                    .WithMany(p => p.SySystemStatusWorkflowRulesStatusIdToNavigation)
                    .HasForeignKey(d => d.StatusIdTo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdTo_SysStatusId");
            });

            modelBuilder.Entity<SyTables>(entity =>
            {
                entity.HasKey(e => e.TblId);

                entity.ToTable("syTables");

                entity.Property(e => e.TblId).ValueGeneratedNever();

                entity.Property(e => e.TblDescrip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TblName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TblPk).HasColumnName("TblPK");
            });

            modelBuilder.Entity<SyTblCaptions>(entity =>
            {
                entity.HasKey(e => e.TblCapId);

                entity.ToTable("syTblCaptions");

                entity.Property(e => e.TblCapId).ValueGeneratedNever();

                entity.Property(e => e.Caption)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TblDescrip)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.Tbl)
                    .WithMany(p => p.SyTblCaptions)
                    .HasForeignKey(d => d.TblId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTblCaptions_syTables_TblId_TblId");
            });

            modelBuilder.Entity<SyTblDependencies>(entity =>
            {
                entity.HasKey(e => e.TableDependenciesId);

                entity.ToTable("syTblDependencies");

                entity.Property(e => e.TableDependenciesId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.DepTable)
                    .IsRequired()
                    .HasColumnName("depTable")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Table1)
                    .IsRequired()
                    .HasColumnName("table1")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Table2)
                    .IsRequired()
                    .HasColumnName("table2")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyTblFlds>(entity =>
            {
                entity.HasKey(e => e.TblFldsId);

                entity.ToTable("syTblFlds");

                entity.Property(e => e.TblFldsId).ValueGeneratedNever();

                entity.Property(e => e.FkcolDescrip)
                    .HasColumnName("FKColDescrip")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Fld)
                    .WithMany(p => p.SyTblFlds)
                    .HasForeignKey(d => d.FldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTblFlds_syFields_FldId_FldId");

                entity.HasOne(d => d.Tbl)
                    .WithMany(p => p.SyTblFlds)
                    .HasForeignKey(d => d.TblId)
                    .HasConstraintName("FK_syTblFlds_syTables_TblId_TblId");
            });

            modelBuilder.Entity<SyTermTypes>(entity =>
            {
                entity.HasKey(e => e.TermTypeId);

                entity.ToTable("syTermTypes");

                entity.Property(e => e.TermTypeId).ValueGeneratedNever();

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyTitleIvsapCustomVerbiage>(entity =>
            {
                entity.ToTable("syTitleIVSapCustomVerbiage");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.TitleIvsapStatusId).HasColumnName("TitleIVSapStatusId");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.SyTitleIvsapCustomVerbiage)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapCustomVerbiage_syUsers_CreatedById_UserId");

                entity.HasOne(d => d.Sap)
                    .WithMany(p => p.SyTitleIvsapCustomVerbiage)
                    .HasForeignKey(d => d.SapId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapCustomVerbiage_arSap_SapId_SAPId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyTitleIvsapCustomVerbiage)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapCustomVerbiage_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.TitleIvsapStatus)
                    .WithMany(p => p.SyTitleIvsapCustomVerbiage)
                    .HasForeignKey(d => d.TitleIvsapStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapCustomVerbiage_syTitleIVSapStatus_TitleIVSapStatusId_Id");
            });

            modelBuilder.Entity<SyTitleIvsapStatus>(entity =>
            {
                entity.ToTable("syTitleIVSapStatus");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DefaultMessage).HasMaxLength(2000);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedBy)
                    .WithMany(p => p.SyTitleIvsapStatusCreatedBy)
                    .HasForeignKey(d => d.CreatedById)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapStatus_syUsers_CreatedBy_UserId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyTitleIvsapStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syTitleIVSapStatus_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.UpdatedBy)
                    .WithMany(p => p.SyTitleIvsapStatusUpdatedBy)
                    .HasForeignKey(d => d.UpdatedById)
                    .HasConstraintName("FK_syTitleIVSapStatus_syUsers_UpdatedBy_UserId");
            });

            modelBuilder.Entity<SyUniversalSearchModules>(entity =>
            {
                entity.HasKey(e => e.UniversalSearchId);

                entity.ToTable("syUniversalSearchModules");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syUniversalSearchModules_Code")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(70)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyUniversalSearchModules)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUniversalSearchModules_syResources_ResourceId_ResourceID");

                entity.HasOne(d => d.UniversalSearch)
                    .WithOne(p => p.InverseUniversalSearch)
                    .HasForeignKey<SyUniversalSearchModules>(d => d.UniversalSearchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUniversalSearchModules_syUniversalSearchModules_UniversalSearchId_UniversalSearchId");
            });

            modelBuilder.Entity<SyUserImpersonationLog>(entity =>
            {
                entity.HasKey(e => e.UserImpersonationLogId);

                entity.ToTable("syUserImpersonationLog");

                entity.Property(e => e.ImpersonatedUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LogEnd).HasColumnType("datetime");

                entity.Property(e => e.LogStart).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyUserQuestions>(entity =>
            {
                entity.HasKey(e => e.UserQuestionId);

                entity.ToTable("syUserQuestions");

                entity.Property(e => e.UserQuestionId).ValueGeneratedNever();

                entity.Property(e => e.UserQuestionDescrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyUserResources>(entity =>
            {
                entity.HasKey(e => e.ResourceId);

                entity.ToTable("syUserResources");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Resource)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyUserResources)
                    .HasForeignKey(d => d.CampGrpId)
                    .HasConstraintName("FK_syUserResources_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.SyUserResources)
                    .HasForeignKey(d => d.EntityId)
                    .HasConstraintName("FK_syUserResources_syResources_EntityId_ResourceID");

                entity.HasOne(d => d.ResourceType)
                    .WithMany(p => p.SyUserResources)
                    .HasForeignKey(d => d.ResourceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUserResources_syResourceTypes_ResourceTypeId_ResourceTypeID");
            });

            modelBuilder.Entity<SyUserResPermissions>(entity =>
            {
                entity.HasKey(e => e.ResPermissionId);

                entity.ToTable("syUserResPermissions");

                entity.Property(e => e.ResPermissionId).ValueGeneratedNever();
            });

            modelBuilder.Entity<SyUsers>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("syUsers");

                entity.HasIndex(e => new { e.Email, e.CampusId })
                    .HasName("UIX_syUsers_Email_CampusId")
                    .IsUnique();

                entity.HasIndex(e => new { e.UserName, e.Password, e.CampusId })
                    .HasName("UIX_syUsers_UserName_Password_CampusId")
                    .IsUnique();

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.ConfirmPassword)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FullName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsDefaultAdminRep).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsLoggedIn).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Salt)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserTypeId).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.Campus)
                    .WithMany(p => p.SyUsers)
                    .HasForeignKey(d => d.CampusId)
                    .HasConstraintName("FK_syUsers_syCampuses_CampusId_CampusId");

                entity.HasOne(d => d.User)
                    .WithOne(p => p.InverseUser)
                    .HasForeignKey<SyUsers>(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUserTermsOfUse_syUsers_UserId_UserId");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.SyUsers)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUsers_syUserType_UserTypeId_UserTypeId");
            });

            modelBuilder.Entity<SyUserSpecQuestions>(entity =>
            {
                entity.HasKey(e => e.UserSpecQuestionId);

                entity.ToTable("syUserSpecQuestions");

                entity.Property(e => e.UserSpecQuestionId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserAnswer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UserQuestion)
                    .WithMany(p => p.SyUserSpecQuestions)
                    .HasForeignKey(d => d.UserQuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUserSpecQuestions_syUserQuestions_UserQuestionId_UserQuestionId");
            });

            modelBuilder.Entity<SyUsersRolesCampGrps>(entity =>
            {
                entity.HasKey(e => e.UserRoleCampGrpId);

                entity.ToTable("syUsersRolesCampGrps");

                entity.Property(e => e.UserRoleCampGrpId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CampGrp)
                    .WithMany(p => p.SyUsersRolesCampGrps)
                    .HasForeignKey(d => d.CampGrpId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUsersRolesCampGrps_syCampGrps_CampGrpId_CampGrpId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.SyUsersRolesCampGrps)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUsersRolesCampGrps_syRoles_RoleId_RoleId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SyUsersRolesCampGrps)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syUsersRolesCampGrps_syUsers_UserId_UserId");
            });

            modelBuilder.Entity<SyUserTermsOfUse>(entity =>
            {
                entity.HasKey(e => e.UserTermsOfUseId);

                entity.ToTable("syUserTermsOfUse");

                entity.Property(e => e.AcceptedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SyUserType>(entity =>
            {
                entity.HasKey(e => e.UserTypeId);

                entity.ToTable("syUserType");

                entity.Property(e => e.UserTypeId).ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.ModDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModUser)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SyVersionHistory>(entity =>
            {
                entity.ToTable("syVersionHistory");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VersionBegin).HasColumnType("datetime");

                entity.Property(e => e.VersionEnd).HasColumnType("datetime");
            });

            modelBuilder.Entity<SyWapiAllowedServices>(entity =>
            {
                entity.ToTable("syWapiAllowedServices");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syWapiAllowedServices_Code")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(2048)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyWapiBridgeExternalCompanyAllowedServices>(entity =>
            {
                entity.HasKey(e => e.IdWapiBridgeAllowedService);

                entity.ToTable("syWapiBridgeExternalCompanyAllowedServices");

                entity.HasOne(d => d.IdAllowedServicesNavigation)
                    .WithMany(p => p.SyWapiBridgeExternalCompanyAllowedServices)
                    .HasForeignKey(d => d.IdAllowedServices)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWapiBridgeExternalCompanyAllowedServices_syWapiAllowedServices_IdAllowedServices_Id");

                entity.HasOne(d => d.IdExternalCompaniesNavigation)
                    .WithMany(p => p.SyWapiBridgeExternalCompanyAllowedServices)
                    .HasForeignKey(d => d.IdExternalCompanies)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWapiBridgeExternalCompanyAllowedServices_syWapiExternalCompanies_IdExternalCompanies_Id");
            });

            modelBuilder.Entity<SyWapiExternalCompanies>(entity =>
            {
                entity.ToTable("syWapiExternalCompanies");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syWapiExternalCompanies_Code")
                    .IsUnique();

                entity.HasIndex(e => e.Description)
                    .HasName("UIX_syWapiExternalCompanies_Description")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<SyWapiExternalOperationMode>(entity =>
            {
                entity.ToTable("syWapiExternalOperationMode");

                entity.HasIndex(e => e.Code)
                    .HasName("UIX_syWapiExternalOperationMode_Code")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<SyWapiOperationLogger>(entity =>
            {
                entity.ToTable("syWapiOperationLogger");

                entity.Property(e => e.Comment).IsUnicode(false);

                entity.Property(e => e.CompanyCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SyWapiSettings>(entity =>
            {
                entity.ToTable("syWapiSettings");

                entity.HasIndex(e => e.CodeOperation)
                    .HasName("UIX_syWapiSettings_CodeOperation")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodeOperation)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ConsumerKey)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DateLastExecution).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateMod).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExternalUrl)
                    .IsRequired()
                    .HasMaxLength(2048)
                    .IsUnicode(false);

                entity.Property(e => e.FirstAllowedServiceQueryString)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.OperationSecondTimeInterval).HasDefaultValueSql("((86400))");

                entity.Property(e => e.PollSecondForOnDemandOperation).HasDefaultValueSql("((60))");

                entity.Property(e => e.PrivateKey)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SecondAllowedServiceQueryString)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UserMod)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SUPPORT')");

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAllowedServiceNavigation)
                    .WithMany(p => p.SyWapiSettingsIdAllowedServiceNavigation)
                    .HasForeignKey(d => d.IdAllowedService)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWapiSettings_syWapiAllowedServices_IdAllowedService_Id");

                entity.HasOne(d => d.IdExtCompanyNavigation)
                    .WithMany(p => p.SyWapiSettings)
                    .HasForeignKey(d => d.IdExtCompany)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWapiSettings_syWapiExternalCompanies_IdExtCompany_Id");

                entity.HasOne(d => d.IdExtOperationNavigation)
                    .WithMany(p => p.SyWapiSettings)
                    .HasForeignKey(d => d.IdExtOperation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWapiSettings_syWapiExternalOperationMode_IdExtOperation_Id");

                entity.HasOne(d => d.IdSecondAllowedServiceNavigation)
                    .WithMany(p => p.SyWapiSettingsIdSecondAllowedServiceNavigation)
                    .HasForeignKey(d => d.IdSecondAllowedService)
                    .HasConstraintName("FK_syWapiSettings_syWapiAllowedServices_IdSecondAllowedService_Id");
            });

            modelBuilder.Entity<SyWidgetResourceRoles>(entity =>
            {
                entity.HasKey(e => e.WidgetResourceRoleId);

                entity.ToTable("syWidgetResourceRoles");

                entity.Property(e => e.WidgetResourceRoleId).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyWidgetResourceRoles)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_syWidgetResourceRoles_syResources_ResourceId_ResourceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyWidgetResourceRoles)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syWidgetResourceRoles_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.SysRole)
                    .WithMany(p => p.SyWidgetResourceRoles)
                    .HasForeignKey(d => d.SysRoleId)
                    .HasConstraintName("FK_syWidgetResourceRoles_sySysRoles_SysRoleId_SysRoleId");

                entity.HasOne(d => d.Widget)
                    .WithMany(p => p.SyWidgetResourceRoles)
                    .HasForeignKey(d => d.WidgetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWidgetResourceRoles_syWidgets_WidgetId_WidgetId");
            });

            modelBuilder.Entity<SyWidgets>(entity =>
            {
                entity.HasKey(e => e.WidgetId);

                entity.ToTable("syWidgets");

                entity.Property(e => e.WidgetId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyWidgets)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syWidgets_syStatuses_StatusId_StatusId");
            });

            modelBuilder.Entity<SyWidgetUserResourceSettings>(entity =>
            {
                entity.HasKey(e => e.WidgetUserResourceSettingId);

                entity.ToTable("syWidgetUserResourceSettings");

                entity.Property(e => e.WidgetUserResourceSettingId).HasDefaultValueSql("(newsequentialid())");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.SyWidgetUserResourceSettings)
                    .HasForeignKey(d => d.ResourceId)
                    .HasConstraintName("FK_syWidgetUserResourceSettings_syResources_ResourceId_ResourceId");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SyWidgetUserResourceSettings)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_syWidgetUserResourceSettings_syStatuses_StatusId_StatusId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SyWidgetUserResourceSettings)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_syWidgetUserResourceSettings_syUsers_UserId_UserId");

                entity.HasOne(d => d.Widget)
                    .WithMany(p => p.SyWidgetUserResourceSettings)
                    .HasForeignKey(d => d.WidgetId)
                    .HasConstraintName("FK_syWidgetUserResourceSettings_syWidgets_WidgetId_WidgetId");
            });

            modelBuilder.Entity<TblAwardYearMapping>(entity =>
            {
                entity.HasKey(e => e.MappingId);

                entity.ToTable("tblAwardYearMapping");

                entity.Property(e => e.AdvantageAwardYear)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FameEspawardYear).HasColumnName("FameESPAwardYear");
            });

            modelBuilder.Entity<TblPrefReport>(entity =>
            {
                entity.HasKey(e => e.PrefReportId);

                entity.ToTable("tblPrefReport");

                entity.Property(e => e.PrefReportId).ValueGeneratedNever();

                entity.Property(e => e.Attendanceenddate)
                    .HasColumnName("attendanceenddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Attendancestartdate)
                    .HasColumnName("attendancestartdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Moddate)
                    .HasColumnName("moddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Moduser)
                    .HasColumnName("moduser")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TermId).HasColumnName("termId");

                entity.Property(e => e.Termenddate)
                    .HasColumnName("termenddate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Termstartdate)
                    .HasColumnName("termstartdate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<TempStatusChangeHistoryTbl>(entity =>
            {
                entity.Property(e => e.CaseNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChangedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfChange).HasColumnType("datetime");

                entity.Property(e => e.EffectiveDate).HasColumnType("datetime");

                entity.Property(e => e.Reason)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDescription)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.StudentEnrollment)
                    .WithMany(p => p.TempStatusChangeHistoryTbl)
                    .HasForeignKey(d => d.StudentEnrollmentId)
                    .HasConstraintName("FK_TempStatusChangeHistoryTbl_arStuEnrollments_StudentEnrollmentId_StuEnrollId");
            });

            modelBuilder.Entity<TmCategories>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("tmCategories");

                entity.HasIndex(e => new { e.Code, e.Descrip, e.CampGroupId })
                    .HasName("UIX_tmCategories_Code_Descrip_CampGroupId")
                    .IsUnique();

                entity.Property(e => e.CategoryId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<TmPermissions>(entity =>
            {
                entity.HasKey(e => e.Sid);

                entity.ToTable("tmPermissions");

                entity.Property(e => e.Sid)
                    .HasColumnName("SID")
                    .HasDefaultValueSql("(newid())");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmResultActions>(entity =>
            {
                entity.HasKey(e => e.ResultActionId);

                entity.ToTable("tmResultActions");

                entity.Property(e => e.ResultActionId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ColumnName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RowGuid)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tablename)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValuesQuery)
                    .HasColumnName("Values_Query")
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmResults>(entity =>
            {
                entity.HasKey(e => e.ResultId);

                entity.ToTable("tmResults");

                entity.HasIndex(e => new { e.Code, e.Descrip })
                    .HasName("UIX_tmResults_Code_Descrip")
                    .IsUnique();

                entity.Property(e => e.ResultId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ResultActionValue)
                    .HasMaxLength(256)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmResultTasks>(entity =>
            {
                entity.HasKey(e => e.ResultTaskId);

                entity.ToTable("tmResultTasks");

                entity.Property(e => e.ResultTaskId).ValueGeneratedNever();

                entity.Property(e => e.StartGapUnit)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.HasOne(d => d.Result)
                    .WithMany(p => p.TmResultTasks)
                    .HasForeignKey(d => d.ResultId)
                    .HasConstraintName("FK_tmResultTasks_tmResults_ResultId_ResultId");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TmResultTasks)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("FK_tmResultTasks_tmTasks_TaskId_TaskId");
            });

            modelBuilder.Entity<TmTaskResults>(entity =>
            {
                entity.HasKey(e => e.TaskResultId);

                entity.ToTable("tmTaskResults");

                entity.Property(e => e.TaskResultId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.HasOne(d => d.Result)
                    .WithMany(p => p.TmTaskResults)
                    .HasForeignKey(d => d.ResultId)
                    .HasConstraintName("FK_tmTaskResults_tmResults_ResultId_ResultId");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TmTaskResults)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("FK_tmTaskResults_tmTasks_TaskId_TaskId");
            });

            modelBuilder.Entity<TmTasks>(entity =>
            {
                entity.HasKey(e => e.TaskId);

                entity.ToTable("tmTasks");

                entity.HasIndex(e => new { e.Code, e.Descrip, e.CampGroupId })
                    .HasName("UIX_tmTasks_Code_Descrip_CampGroupId")
                    .IsUnique();

                entity.Property(e => e.TaskId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Code)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Descrip)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TmTasks)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_tmTasks_tmCategories_CategoryId_CategoryId");

                entity.HasOne(d => d.StatusCode)
                    .WithMany(p => p.TmTasks)
                    .HasForeignKey(d => d.StatusCodeId)
                    .HasConstraintName("FK_tmTasks_syStatusCodes_StatusCodeId_StatusCodeId");
            });

            modelBuilder.Entity<TmUsersTaskDefaultView>(entity =>
            {
                entity.HasKey(e => e.UserTaskDefViewId);

                entity.ToTable("tmUsersTaskDefaultView");

                entity.Property(e => e.UserTaskDefViewId).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.DefaultView)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.OthersId).HasColumnName("OthersID");

                entity.Property(e => e.UserId).HasColumnName("UserID");
            });

            modelBuilder.Entity<TmUserTasks>(entity =>
            {
                entity.HasKey(e => e.UserTaskId);

                entity.ToTable("tmUserTasks");

                entity.Property(e => e.UserTaskId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.Message)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TmUserTasks)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tmUserTasks_tmTasks_TaskId_TaskId");
            });

            modelBuilder.Entity<TmWorkflow>(entity =>
            {
                entity.HasKey(e => e.WorkflowId);

                entity.ToTable("tmWorkflow");

                entity.Property(e => e.WorkflowId).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Descrip)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModDate).HasColumnType("datetime");

                entity.Property(e => e.ModUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
