﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MissingGroupRequirements.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the MissingGroupRequirements type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Fame.EFCore.Advantage.StoreProcedureEntities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The missing group requirements.
    /// </summary>
    public class MissingGroupRequirements
    {
        /// <summary>
        /// Gets or sets the req grp id.
        /// </summary>
        public Guid? ReqGrpId { get; set; }


        /// <summary>
        /// Gets or sets the descrip.
        /// </summary>
        public string Descrip { get; set; }

        /// <summary>
        /// Gets or sets the num reqs.
        /// </summary>
        public int NumReqs { get; set; }

        /// <summary>
        /// Gets or sets the attempted reqs.
        /// </summary>
        public int AttemptedReqs { get; set; }
    }
}
