﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MissingDocumentRequirements.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the MissingDocumentRequirements type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Fame.EFCore.Advantage.StoreProcedureEntities
{
    using System;

    /// <summary>
    /// The missing document requirements.
    /// </summary>
    public class MissingDocumentRequirements
    {
        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        public Guid? DocumentID { get; set; }

        /// <summary>
        /// Gets or sets the descrip.
        /// </summary>
        public string Descrip { get; set; }
    }
}
