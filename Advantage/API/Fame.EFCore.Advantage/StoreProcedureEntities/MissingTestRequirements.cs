﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MissingTestRequirements.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the MissingTestRequirements type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Fame.EFCore.Advantage.StoreProcedureEntities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The missing test requirements.
    /// </summary>
    public class MissingTestRequirements
    {
        /// <summary>
        /// Gets or sets the adreq id.
        /// </summary>
        public Guid? adreqId { get; set; }

        /// <summary>
        /// Gets or sets the descrip.
        /// </summary>
        public string Descrip { get; set; }
    }
}
