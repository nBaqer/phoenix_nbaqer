﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fame.EFCore.Advantage.Interfaces
{
    using System.Threading.Tasks;

    using Fame.EFCore.Interfaces;

    /// <summary>
    /// The IAdvantageDbContext interface.
    /// </summary>
    public interface IAdvantageContext : IDatabaseContext, IDisposable
    {
        void SaveChanges();
        
    }
}
