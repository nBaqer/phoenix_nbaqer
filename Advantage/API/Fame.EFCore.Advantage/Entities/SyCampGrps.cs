﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCampGrps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCampGrps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCampGrps</summary>*/
    public partial class SyCampGrps
    {
        /*<summary>The constructor for SyCampGrps</summary>*/
        public SyCampGrps()
        {
            AdAdvInterval = new HashSet<AdAdvInterval>();
            AdAgencySponsors = new HashSet<AdAgencySponsors>();
            AdCitizenships = new HashSet<AdCitizenships>();
            AdColleges = new HashSet<AdColleges>();
            AdCounties = new HashSet<AdCounties>();
            AdCountries = new HashSet<AdCountries>();
            AdEdLvls = new HashSet<AdEdLvls>();
            AdEthCodes = new HashSet<AdEthCodes>();
            AdExpertiseLevel = new HashSet<AdExpertiseLevel>();
            AdExtraCurr = new HashSet<AdExtraCurr>();
            AdExtraCurrGrp = new HashSet<AdExtraCurrGrp>();
            AdFullPartTime = new HashSet<AdFullPartTime>();
            AdGenders = new HashSet<AdGenders>();
            AdHighSchools = new HashSet<AdHighSchools>();
            AdImportLeadsMappings = new HashSet<AdImportLeadsMappings>();
            AdLeadGroups = new HashSet<AdLeadGroups>();
            AdMaritalStatus = new HashSet<AdMaritalStatus>();
            AdNationalities = new HashSet<AdNationalities>();
            AdReqGroups = new HashSet<AdReqGroups>();
            AdReqs = new HashSet<AdReqs>();
            AdSourceAdvertisement = new HashSet<AdSourceAdvertisement>();
            AdSourceCatagory = new HashSet<AdSourceCatagory>();
            AdSourceType = new HashSet<AdSourceType>();
            AdTitles = new HashSet<AdTitles>();
            ArBkCategories = new HashSet<ArBkCategories>();
            ArBooks = new HashSet<ArBooks>();
            ArBuildings = new HashSet<ArBuildings>();
            ArCertifications = new HashSet<ArCertifications>();
            ArCourseCategories = new HashSet<ArCourseCategories>();
            ArDegrees = new HashSet<ArDegrees>();
            ArDropReasons = new HashSet<ArDropReasons>();
            ArFerpacategory = new HashSet<ArFerpacategory>();
            ArFerpaentity = new HashSet<ArFerpaentity>();
            ArGradeScales = new HashSet<ArGradeScales>();
            ArGradeSystems = new HashSet<ArGradeSystems>();
            ArGrdBkEvalTyps = new HashSet<ArGrdBkEvalTyps>();
            ArGrdComponentTypes = new HashSet<ArGrdComponentTypes>();
            ArInstructionType = new HashSet<ArInstructionType>();
            ArLoareasons = new HashSet<ArLoareasons>();
            ArPrgGrp = new HashSet<ArPrgGrp>();
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArProgCredential = new HashSet<ArProgCredential>();
            ArProgTypes = new HashSet<ArProgTypes>();
            ArPrograms = new HashSet<ArPrograms>();
            ArReqTypes = new HashSet<ArReqTypes>();
            ArReqs = new HashSet<ArReqs>();
            ArSap = new HashSet<ArSap>();
            ArSdateSetup = new HashSet<ArSdateSetup>();
            ArShifts = new HashSet<ArShifts>();
            ArTerm = new HashSet<ArTerm>();
            CmDocuments = new HashSet<CmDocuments>();
            CmTimeInterval = new HashSet<CmTimeInterval>();
            FaLenders = new HashSet<FaLenders>();
            MsgGroups = new HashSet<MsgGroups>();
            MsgRules = new HashSet<MsgRules>();
            MsgTemplates = new HashSet<MsgTemplates>();
            PlAddressTypes = new HashSet<PlAddressTypes>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlEmployers = new HashSet<PlEmployers>();
            PlFee = new HashSet<PlFee>();
            PlFldStudy = new HashSet<PlFldStudy>();
            PlHowPlaced = new HashSet<PlHowPlaced>();
            PlIndustries = new HashSet<PlIndustries>();
            PlInterview = new HashSet<PlInterview>();
            PlJobBenefit = new HashSet<PlJobBenefit>();
            PlJobCats = new HashSet<PlJobCats>();
            PlJobSchedule = new HashSet<PlJobSchedule>();
            PlJobStatus = new HashSet<PlJobStatus>();
            PlJobType = new HashSet<PlJobType>();
            PlLocations = new HashSet<PlLocations>();
            PlSalaryType = new HashSet<PlSalaryType>();
            PlSkillGroups = new HashSet<PlSkillGroups>();
            PlSkills = new HashSet<PlSkills>();
            PlTransportation = new HashSet<PlTransportation>();
            SaAcademicYears = new HashSet<SaAcademicYears>();
            SaBankAccounts = new HashSet<SaBankAccounts>();
            SaBankCodes = new HashSet<SaBankCodes>();
            SaBillingMethods = new HashSet<SaBillingMethods>();
            SaFundSources = new HashSet<SaFundSources>();
            SaGlaccounts = new HashSet<SaGlaccounts>();
            SaPaymentDescriptions = new HashSet<SaPaymentDescriptions>();
            SaRateSchedules = new HashSet<SaRateSchedules>();
            SaSysTransCodes = new HashSet<SaSysTransCodes>();
            SaTransCodes = new HashSet<SaTransCodes>();
            SaTuitionCategories = new HashSet<SaTuitionCategories>();
            SaTuitionEarnings = new HashSet<SaTuitionEarnings>();
            SyAddressStatuses = new HashSet<SyAddressStatuses>();
            SyAnnouncements = new HashSet<SyAnnouncements>();
            SyCampusFileConfiguration = new HashSet<SyCampusFileConfiguration>();
            SyCertifications = new HashSet<SyCertifications>();
            SyCmpGrpCmps = new HashSet<SyCmpGrpCmps>();
            SyContactTypes = new HashSet<SyContactTypes>();
            SyDepartments = new HashSet<SyDepartments>();
            SyDocStatuses = new HashSet<SyDocStatuses>();
            SyEvents = new HashSet<SyEvents>();
            SyFamilyIncome = new HashSet<SyFamilyIncome>();
            SyHolidays = new HashSet<SyHolidays>();
            SyHomePageNotes = new HashSet<SyHomePageNotes>();
            SyHrdepartments = new HashSet<SyHrdepartments>();
            SyInstitutions = new HashSet<SyInstitutions>();
            SyLeadStatusChanges = new HashSet<SyLeadStatusChanges>();
            SyMessageGroups = new HashSet<SyMessageGroups>();
            SyMessageSchemas = new HashSet<SyMessageSchemas>();
            SyMessageTemplates = new HashSet<SyMessageTemplates>();
            SyPeriods = new HashSet<SyPeriods>();
            SyPhoneStatuses = new HashSet<SyPhoneStatuses>();
            SyPhoneType = new HashSet<SyPhoneType>();
            SyPositions = new HashSet<SyPositions>();
            SyPrefixes = new HashSet<SyPrefixes>();
            SyRdfsiteSums = new HashSet<SyRdfsiteSums>();
            SyReasonNotEnrolled = new HashSet<SyReasonNotEnrolled>();
            SyRelationshipStatus = new HashSet<SyRelationshipStatus>();
            SyStates = new HashSet<SyStates>();
            SyStatusCodes = new HashSet<SyStatusCodes>();
            SyStuRestrictionTypes = new HashSet<SyStuRestrictionTypes>();
            SySuffixes = new HashSet<SySuffixes>();
            SyUserResources = new HashSet<SyUserResources>();
            SyUsersRolesCampGrps = new HashSet<SyUsersRolesCampGrps>();
        }

        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for CampGrpCode</summary>*/
        public string CampGrpCode { get; set; }
        /*<summary>The get and set for CampGrpDescrip</summary>*/
        public string CampGrpDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for IsAllCampusGrp</summary>*/
        public bool? IsAllCampusGrp { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdAdvInterval</summary>*/
        public virtual ICollection<AdAdvInterval>AdAdvInterval { get; set; }
        /*<summary>The navigational property for AdAgencySponsors</summary>*/
        public virtual ICollection<AdAgencySponsors>AdAgencySponsors { get; set; }
        /*<summary>The navigational property for AdCitizenships</summary>*/
        public virtual ICollection<AdCitizenships>AdCitizenships { get; set; }
        /*<summary>The navigational property for AdColleges</summary>*/
        public virtual ICollection<AdColleges>AdColleges { get; set; }
        /*<summary>The navigational property for AdCounties</summary>*/
        public virtual ICollection<AdCounties>AdCounties { get; set; }
        /*<summary>The navigational property for AdCountries</summary>*/
        public virtual ICollection<AdCountries>AdCountries { get; set; }
        /*<summary>The navigational property for AdEdLvls</summary>*/
        public virtual ICollection<AdEdLvls>AdEdLvls { get; set; }
        /*<summary>The navigational property for AdEthCodes</summary>*/
        public virtual ICollection<AdEthCodes>AdEthCodes { get; set; }
        /*<summary>The navigational property for AdExpertiseLevel</summary>*/
        public virtual ICollection<AdExpertiseLevel>AdExpertiseLevel { get; set; }
        /*<summary>The navigational property for AdExtraCurr</summary>*/
        public virtual ICollection<AdExtraCurr>AdExtraCurr { get; set; }
        /*<summary>The navigational property for AdExtraCurrGrp</summary>*/
        public virtual ICollection<AdExtraCurrGrp>AdExtraCurrGrp { get; set; }
        /*<summary>The navigational property for AdFullPartTime</summary>*/
        public virtual ICollection<AdFullPartTime>AdFullPartTime { get; set; }
        /*<summary>The navigational property for AdGenders</summary>*/
        public virtual ICollection<AdGenders>AdGenders { get; set; }
        /*<summary>The navigational property for AdHighSchools</summary>*/
        public virtual ICollection<AdHighSchools>AdHighSchools { get; set; }
        /*<summary>The navigational property for AdImportLeadsMappings</summary>*/
        public virtual ICollection<AdImportLeadsMappings>AdImportLeadsMappings { get; set; }
        /*<summary>The navigational property for AdLeadGroups</summary>*/
        public virtual ICollection<AdLeadGroups>AdLeadGroups { get; set; }
        /*<summary>The navigational property for AdMaritalStatus</summary>*/
        public virtual ICollection<AdMaritalStatus>AdMaritalStatus { get; set; }
        /*<summary>The navigational property for AdNationalities</summary>*/
        public virtual ICollection<AdNationalities>AdNationalities { get; set; }
        /*<summary>The navigational property for AdReqGroups</summary>*/
        public virtual ICollection<AdReqGroups>AdReqGroups { get; set; }
        /*<summary>The navigational property for AdReqs</summary>*/
        public virtual ICollection<AdReqs>AdReqs { get; set; }
        /*<summary>The navigational property for AdSourceAdvertisement</summary>*/
        public virtual ICollection<AdSourceAdvertisement>AdSourceAdvertisement { get; set; }
        /*<summary>The navigational property for AdSourceCatagory</summary>*/
        public virtual ICollection<AdSourceCatagory>AdSourceCatagory { get; set; }
        /*<summary>The navigational property for AdSourceType</summary>*/
        public virtual ICollection<AdSourceType>AdSourceType { get; set; }
        /*<summary>The navigational property for AdTitles</summary>*/
        public virtual ICollection<AdTitles>AdTitles { get; set; }
        /*<summary>The navigational property for ArBkCategories</summary>*/
        public virtual ICollection<ArBkCategories>ArBkCategories { get; set; }
        /*<summary>The navigational property for ArBooks</summary>*/
        public virtual ICollection<ArBooks>ArBooks { get; set; }
        /*<summary>The navigational property for ArBuildings</summary>*/
        public virtual ICollection<ArBuildings>ArBuildings { get; set; }
        /*<summary>The navigational property for ArCertifications</summary>*/
        public virtual ICollection<ArCertifications>ArCertifications { get; set; }
        /*<summary>The navigational property for ArCourseCategories</summary>*/
        public virtual ICollection<ArCourseCategories>ArCourseCategories { get; set; }
        /*<summary>The navigational property for ArDegrees</summary>*/
        public virtual ICollection<ArDegrees>ArDegrees { get; set; }
        /*<summary>The navigational property for ArDropReasons</summary>*/
        public virtual ICollection<ArDropReasons>ArDropReasons { get; set; }
        /*<summary>The navigational property for ArFerpacategory</summary>*/
        public virtual ICollection<ArFerpacategory>ArFerpacategory { get; set; }
        /*<summary>The navigational property for ArFerpaentity</summary>*/
        public virtual ICollection<ArFerpaentity>ArFerpaentity { get; set; }
        /*<summary>The navigational property for ArGradeScales</summary>*/
        public virtual ICollection<ArGradeScales>ArGradeScales { get; set; }
        /*<summary>The navigational property for ArGradeSystems</summary>*/
        public virtual ICollection<ArGradeSystems>ArGradeSystems { get; set; }
        /*<summary>The navigational property for ArGrdBkEvalTyps</summary>*/
        public virtual ICollection<ArGrdBkEvalTyps>ArGrdBkEvalTyps { get; set; }
        /*<summary>The navigational property for ArGrdComponentTypes</summary>*/
        public virtual ICollection<ArGrdComponentTypes>ArGrdComponentTypes { get; set; }
        /*<summary>The navigational property for ArInstructionType</summary>*/
        public virtual ICollection<ArInstructionType>ArInstructionType { get; set; }
        /*<summary>The navigational property for ArLoareasons</summary>*/
        public virtual ICollection<ArLoareasons>ArLoareasons { get; set; }
        /*<summary>The navigational property for ArPrgGrp</summary>*/
        public virtual ICollection<ArPrgGrp>ArPrgGrp { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArProgCredential</summary>*/
        public virtual ICollection<ArProgCredential>ArProgCredential { get; set; }
        /*<summary>The navigational property for ArProgTypes</summary>*/
        public virtual ICollection<ArProgTypes>ArProgTypes { get; set; }
        /*<summary>The navigational property for ArPrograms</summary>*/
        public virtual ICollection<ArPrograms>ArPrograms { get; set; }
        /*<summary>The navigational property for ArReqTypes</summary>*/
        public virtual ICollection<ArReqTypes>ArReqTypes { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
        /*<summary>The navigational property for ArSap</summary>*/
        public virtual ICollection<ArSap>ArSap { get; set; }
        /*<summary>The navigational property for ArSdateSetup</summary>*/
        public virtual ICollection<ArSdateSetup>ArSdateSetup { get; set; }
        /*<summary>The navigational property for ArShifts</summary>*/
        public virtual ICollection<ArShifts>ArShifts { get; set; }
        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
        /*<summary>The navigational property for CmDocuments</summary>*/
        public virtual ICollection<CmDocuments>CmDocuments { get; set; }
        /*<summary>The navigational property for CmTimeInterval</summary>*/
        public virtual ICollection<CmTimeInterval>CmTimeInterval { get; set; }
        /*<summary>The navigational property for FaLenders</summary>*/
        public virtual ICollection<FaLenders>FaLenders { get; set; }
        /*<summary>The navigational property for MsgGroups</summary>*/
        public virtual ICollection<MsgGroups>MsgGroups { get; set; }
        /*<summary>The navigational property for MsgRules</summary>*/
        public virtual ICollection<MsgRules>MsgRules { get; set; }
        /*<summary>The navigational property for MsgTemplates</summary>*/
        public virtual ICollection<MsgTemplates>MsgTemplates { get; set; }
        /*<summary>The navigational property for PlAddressTypes</summary>*/
        public virtual ICollection<PlAddressTypes>PlAddressTypes { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PlFee</summary>*/
        public virtual ICollection<PlFee>PlFee { get; set; }
        /*<summary>The navigational property for PlFldStudy</summary>*/
        public virtual ICollection<PlFldStudy>PlFldStudy { get; set; }
        /*<summary>The navigational property for PlHowPlaced</summary>*/
        public virtual ICollection<PlHowPlaced>PlHowPlaced { get; set; }
        /*<summary>The navigational property for PlIndustries</summary>*/
        public virtual ICollection<PlIndustries>PlIndustries { get; set; }
        /*<summary>The navigational property for PlInterview</summary>*/
        public virtual ICollection<PlInterview>PlInterview { get; set; }
        /*<summary>The navigational property for PlJobBenefit</summary>*/
        public virtual ICollection<PlJobBenefit>PlJobBenefit { get; set; }
        /*<summary>The navigational property for PlJobCats</summary>*/
        public virtual ICollection<PlJobCats>PlJobCats { get; set; }
        /*<summary>The navigational property for PlJobSchedule</summary>*/
        public virtual ICollection<PlJobSchedule>PlJobSchedule { get; set; }
        /*<summary>The navigational property for PlJobStatus</summary>*/
        public virtual ICollection<PlJobStatus>PlJobStatus { get; set; }
        /*<summary>The navigational property for PlJobType</summary>*/
        public virtual ICollection<PlJobType>PlJobType { get; set; }
        /*<summary>The navigational property for PlLocations</summary>*/
        public virtual ICollection<PlLocations>PlLocations { get; set; }
        /*<summary>The navigational property for PlSalaryType</summary>*/
        public virtual ICollection<PlSalaryType>PlSalaryType { get; set; }
        /*<summary>The navigational property for PlSkillGroups</summary>*/
        public virtual ICollection<PlSkillGroups>PlSkillGroups { get; set; }
        /*<summary>The navigational property for PlSkills</summary>*/
        public virtual ICollection<PlSkills>PlSkills { get; set; }
        /*<summary>The navigational property for PlTransportation</summary>*/
        public virtual ICollection<PlTransportation>PlTransportation { get; set; }
        /*<summary>The navigational property for SaAcademicYears</summary>*/
        public virtual ICollection<SaAcademicYears>SaAcademicYears { get; set; }
        /*<summary>The navigational property for SaBankAccounts</summary>*/
        public virtual ICollection<SaBankAccounts>SaBankAccounts { get; set; }
        /*<summary>The navigational property for SaBankCodes</summary>*/
        public virtual ICollection<SaBankCodes>SaBankCodes { get; set; }
        /*<summary>The navigational property for SaBillingMethods</summary>*/
        public virtual ICollection<SaBillingMethods>SaBillingMethods { get; set; }
        /*<summary>The navigational property for SaFundSources</summary>*/
        public virtual ICollection<SaFundSources>SaFundSources { get; set; }
        /*<summary>The navigational property for SaGlaccounts</summary>*/
        public virtual ICollection<SaGlaccounts>SaGlaccounts { get; set; }
        /*<summary>The navigational property for SaPaymentDescriptions</summary>*/
        public virtual ICollection<SaPaymentDescriptions>SaPaymentDescriptions { get; set; }
        /*<summary>The navigational property for SaRateSchedules</summary>*/
        public virtual ICollection<SaRateSchedules>SaRateSchedules { get; set; }
        /*<summary>The navigational property for SaSysTransCodes</summary>*/
        public virtual ICollection<SaSysTransCodes>SaSysTransCodes { get; set; }
        /*<summary>The navigational property for SaTransCodes</summary>*/
        public virtual ICollection<SaTransCodes>SaTransCodes { get; set; }
        /*<summary>The navigational property for SaTuitionCategories</summary>*/
        public virtual ICollection<SaTuitionCategories>SaTuitionCategories { get; set; }
        /*<summary>The navigational property for SaTuitionEarnings</summary>*/
        public virtual ICollection<SaTuitionEarnings>SaTuitionEarnings { get; set; }
        /*<summary>The navigational property for SyAddressStatuses</summary>*/
        public virtual ICollection<SyAddressStatuses>SyAddressStatuses { get; set; }
        /*<summary>The navigational property for SyAnnouncements</summary>*/
        public virtual ICollection<SyAnnouncements>SyAnnouncements { get; set; }
        /*<summary>The navigational property for SyCampusFileConfiguration</summary>*/
        public virtual ICollection<SyCampusFileConfiguration>SyCampusFileConfiguration { get; set; }
        /*<summary>The navigational property for SyCertifications</summary>*/
        public virtual ICollection<SyCertifications>SyCertifications { get; set; }
        /*<summary>The navigational property for SyCmpGrpCmps</summary>*/
        public virtual ICollection<SyCmpGrpCmps>SyCmpGrpCmps { get; set; }
        /*<summary>The navigational property for SyContactTypes</summary>*/
        public virtual ICollection<SyContactTypes>SyContactTypes { get; set; }
        /*<summary>The navigational property for SyDepartments</summary>*/
        public virtual ICollection<SyDepartments>SyDepartments { get; set; }
        /*<summary>The navigational property for SyDocStatuses</summary>*/
        public virtual ICollection<SyDocStatuses>SyDocStatuses { get; set; }
        /*<summary>The navigational property for SyEvents</summary>*/
        public virtual ICollection<SyEvents>SyEvents { get; set; }
        /*<summary>The navigational property for SyFamilyIncome</summary>*/
        public virtual ICollection<SyFamilyIncome>SyFamilyIncome { get; set; }
        /*<summary>The navigational property for SyHolidays</summary>*/
        public virtual ICollection<SyHolidays>SyHolidays { get; set; }
        /*<summary>The navigational property for SyHomePageNotes</summary>*/
        public virtual ICollection<SyHomePageNotes>SyHomePageNotes { get; set; }
        /*<summary>The navigational property for SyHrdepartments</summary>*/
        public virtual ICollection<SyHrdepartments>SyHrdepartments { get; set; }
        /*<summary>The navigational property for SyInstitutions</summary>*/
        public virtual ICollection<SyInstitutions>SyInstitutions { get; set; }
        /*<summary>The navigational property for SyLeadStatusChanges</summary>*/
        public virtual ICollection<SyLeadStatusChanges>SyLeadStatusChanges { get; set; }
        /*<summary>The navigational property for SyMessageGroups</summary>*/
        public virtual ICollection<SyMessageGroups>SyMessageGroups { get; set; }
        /*<summary>The navigational property for SyMessageSchemas</summary>*/
        public virtual ICollection<SyMessageSchemas>SyMessageSchemas { get; set; }
        /*<summary>The navigational property for SyMessageTemplates</summary>*/
        public virtual ICollection<SyMessageTemplates>SyMessageTemplates { get; set; }
        /*<summary>The navigational property for SyPeriods</summary>*/
        public virtual ICollection<SyPeriods>SyPeriods { get; set; }
        /*<summary>The navigational property for SyPhoneStatuses</summary>*/
        public virtual ICollection<SyPhoneStatuses>SyPhoneStatuses { get; set; }
        /*<summary>The navigational property for SyPhoneType</summary>*/
        public virtual ICollection<SyPhoneType>SyPhoneType { get; set; }
        /*<summary>The navigational property for SyPositions</summary>*/
        public virtual ICollection<SyPositions>SyPositions { get; set; }
        /*<summary>The navigational property for SyPrefixes</summary>*/
        public virtual ICollection<SyPrefixes>SyPrefixes { get; set; }
        /*<summary>The navigational property for SyRdfsiteSums</summary>*/
        public virtual ICollection<SyRdfsiteSums>SyRdfsiteSums { get; set; }
        /*<summary>The navigational property for SyReasonNotEnrolled</summary>*/
        public virtual ICollection<SyReasonNotEnrolled>SyReasonNotEnrolled { get; set; }
        /*<summary>The navigational property for SyRelationshipStatus</summary>*/
        public virtual ICollection<SyRelationshipStatus>SyRelationshipStatus { get; set; }
        /*<summary>The navigational property for SyStates</summary>*/
        public virtual ICollection<SyStates>SyStates { get; set; }
        /*<summary>The navigational property for SyStatusCodes</summary>*/
        public virtual ICollection<SyStatusCodes>SyStatusCodes { get; set; }
        /*<summary>The navigational property for SyStuRestrictionTypes</summary>*/
        public virtual ICollection<SyStuRestrictionTypes>SyStuRestrictionTypes { get; set; }
        /*<summary>The navigational property for SySuffixes</summary>*/
        public virtual ICollection<SySuffixes>SySuffixes { get; set; }
        /*<summary>The navigational property for SyUserResources</summary>*/
        public virtual ICollection<SyUserResources>SyUserResources { get; set; }
        /*<summary>The navigational property for SyUsersRolesCampGrps</summary>*/
        public virtual ICollection<SyUsersRolesCampGrps>SyUsersRolesCampGrps { get; set; }
    }
}