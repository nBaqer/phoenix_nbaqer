﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserImpersonationLog.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserImpersonationLog definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserImpersonationLog</summary>*/
    public partial class SyUserImpersonationLog
    {
        /*<summary>The get and set for UserImpersonationLogId</summary>*/
        public int UserImpersonationLogId { get; set; }
        /*<summary>The get and set for ImpersonatedUser</summary>*/
        public string ImpersonatedUser { get; set; }
        /*<summary>The get and set for LogStart</summary>*/
        public DateTime? LogStart { get; set; }
        /*<summary>The get and set for LogEnd</summary>*/
        public DateTime? LogEnd { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}