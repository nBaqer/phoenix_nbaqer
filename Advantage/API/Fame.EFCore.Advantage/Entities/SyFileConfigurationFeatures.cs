﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFileConfigurationFeatures.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFileConfigurationFeatures definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFileConfigurationFeatures</summary>*/
    public partial class SyFileConfigurationFeatures
    {
        /*<summary>The constructor for SyFileConfigurationFeatures</summary>*/
        public SyFileConfigurationFeatures()
        {
            SyCustomFeatureFileConfiguration = new HashSet<SyCustomFeatureFileConfiguration>();
        }

        /*<summary>The get and set for FeatureId</summary>*/
        public int FeatureId { get; set; }
        /*<summary>The get and set for FeatureCode</summary>*/
        public string FeatureCode { get; set; }
        /*<summary>The get and set for FeatureDescription</summary>*/
        public string FeatureDescription { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyCustomFeatureFileConfiguration</summary>*/
        public virtual ICollection<SyCustomFeatureFileConfiguration>SyCustomFeatureFileConfiguration { get; set; }
    }
}