﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdImportLeadsAdvFldMappings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdImportLeadsAdvFldMappings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdImportLeadsAdvFldMappings</summary>*/
    public partial class AdImportLeadsAdvFldMappings
    {
        /*<summary>The get and set for AdvFieldMapping</summary>*/
        public Guid AdvFieldMapping { get; set; }
        /*<summary>The get and set for MappingId</summary>*/
        public Guid MappingId { get; set; }
        /*<summary>The get and set for FieldNumber</summary>*/
        public short FieldNumber { get; set; }
        /*<summary>The get and set for IlfieldId</summary>*/
        public int? IlfieldId { get; set; }

        /*<summary>The navigational property for Ilfield</summary>*/
        public virtual AdImportLeadsFields Ilfield { get; set; }
        /*<summary>The navigational property for Mapping</summary>*/
        public virtual AdImportLeadsMappings Mapping { get; set; }
    }
}