﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGradeSystems.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGradeSystems definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGradeSystems</summary>*/
    public partial class ArGradeSystems
    {
        /*<summary>The constructor for ArGradeSystems</summary>*/
        public ArGradeSystems()
        {
            ArGradeScales = new HashSet<ArGradeScales>();
            ArGradeSystemDetails = new HashSet<ArGradeSystemDetails>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
        }

        /*<summary>The get and set for GrdSystemId</summary>*/
        public Guid GrdSystemId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArGradeScales</summary>*/
        public virtual ICollection<ArGradeScales>ArGradeScales { get; set; }
        /*<summary>The navigational property for ArGradeSystemDetails</summary>*/
        public virtual ICollection<ArGradeSystemDetails>ArGradeSystemDetails { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
    }
}