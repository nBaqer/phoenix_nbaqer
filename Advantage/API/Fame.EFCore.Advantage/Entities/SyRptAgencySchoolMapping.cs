﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAgencySchoolMapping.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAgencySchoolMapping definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAgencySchoolMapping</summary>*/
    public partial class SyRptAgencySchoolMapping
    {
        /*<summary>The get and set for MappingId</summary>*/
        public Guid MappingId { get; set; }
        /*<summary>The get and set for RptAgencyFldValId</summary>*/
        public int? RptAgencyFldValId { get; set; }
        /*<summary>The get and set for SchoolDescripId</summary>*/
        public Guid SchoolDescripId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for RptAgencyFldVal</summary>*/
        public virtual SyRptAgencyFldValues RptAgencyFldVal { get; set; }
    }
}