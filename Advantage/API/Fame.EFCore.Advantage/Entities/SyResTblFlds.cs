﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyResTblFlds.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyResTblFlds definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyResTblFlds</summary>*/
    public partial class SyResTblFlds
    {
        /*<summary>The get and set for ResDefId</summary>*/
        public int ResDefId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }
        /*<summary>The get and set for TblFldsId</summary>*/
        public int TblFldsId { get; set; }
        /*<summary>The get and set for Required</summary>*/
        public bool Required { get; set; }
        /*<summary>The get and set for SchlReq</summary>*/
        public bool SchlReq { get; set; }
        /*<summary>The get and set for ControlName</summary>*/
        public string ControlName { get; set; }
        /*<summary>The get and set for UsePageSetup</summary>*/
        public bool? UsePageSetup { get; set; }

        /*<summary>The navigational property for TblFlds</summary>*/
        public virtual SyTblFlds TblFlds { get; set; }
    }
}