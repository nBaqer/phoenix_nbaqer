﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTermEnrollSummary.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTermEnrollSummary definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTermEnrollSummary</summary>*/
    public partial class ArTermEnrollSummary
    {
        /*<summary>The get and set for TesummId</summary>*/
        public Guid TesummId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid TermId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for DescripXtranscript</summary>*/
        public string DescripXtranscript { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for Term</summary>*/
        public virtual ArTerm Term { get; set; }
    }
}