﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqLeadGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqLeadGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqLeadGroups</summary>*/
    public partial class AdReqLeadGroups
    {
        /*<summary>The get and set for ReqLeadGrpId</summary>*/
        public Guid ReqLeadGrpId { get; set; }
        /*<summary>The get and set for LeadGrpId</summary>*/
        public Guid LeadGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool? IsRequired { get; set; }
        /*<summary>The get and set for AdReqEffectiveDateId</summary>*/
        public Guid? AdReqEffectiveDateId { get; set; }

        /*<summary>The navigational property for LeadGrp</summary>*/
        public virtual AdLeadGroups LeadGrp { get; set; }
    }
}