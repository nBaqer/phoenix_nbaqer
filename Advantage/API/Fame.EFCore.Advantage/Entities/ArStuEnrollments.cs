﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStuEnrollments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStuEnrollments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStuEnrollments</summary>*/
    public partial class ArStuEnrollments
    {
        /*<summary>The constructor for ArStuEnrollments</summary>*/
        public ArStuEnrollments()
        {
            AdLeadByLeadGroups = new HashSet<AdLeadByLeadGroups>();
            AfaPaymentPeriodStaging = new HashSet<AfaPaymentPeriodStaging>();
            ArExternshipAttendance = new HashSet<ArExternshipAttendance>();
            ArFasapchkResults = new HashSet<ArFasapchkResults>();
            ArGrdBkResults = new HashSet<ArGrdBkResults>();
            ArOverridenPreReqs = new HashSet<ArOverridenPreReqs>();
            ArR2t4terminationDetails = new HashSet<ArR2t4terminationDetails>();
            ArResults = new HashSet<ArResults>();
            ArSapchkResults = new HashSet<ArSapchkResults>();
            ArSapquantResults = new HashSet<ArSapquantResults>();
            ArScheduledHoursAdjustments = new HashSet<ArScheduledHoursAdjustments>();
            ArStdSuspensions = new HashSet<ArStdSuspensions>();
            ArStuProbWarnings = new HashSet<ArStuProbWarnings>();
            ArStuReschReason = new HashSet<ArStuReschReason>();
            ArStudentClockAttendance = new HashSet<ArStudentClockAttendance>();
            ArStudentLoas = new HashSet<ArStudentLoas>();
            ArStudentSchedules = new HashSet<ArStudentSchedules>();
            ArStudentTimeClockPunches = new HashSet<ArStudentTimeClockPunches>();
            ArTermEnrollSummary = new HashSet<ArTermEnrollSummary>();
            ArTransferGrades = new HashSet<ArTransferGrades>();
            AtAttendance = new HashSet<AtAttendance>();
            AtClsSectAttendance = new HashSet<AtClsSectAttendance>();
            FaStudentAwards = new HashSet<FaStudentAwards>();
            FaStudentPaymentPlans = new HashSet<FaStudentPaymentPlans>();
            IntegrationEnrollmentTracking = new HashSet<IntegrationEnrollmentTracking>();
            InverseTransferHoursFromThisSchoolEnrollment = new HashSet<ArStuEnrollments>();
            PlExitInterview = new HashSet<PlExitInterview>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
            SaAdmissionDeposits = new HashSet<SaAdmissionDeposits>();
            SaTransactions = new HashSet<SaTransactions>();
            SyStatusChangesDeleted = new HashSet<SyStatusChangesDeleted>();
            SyStudentStatusChanges = new HashSet<SyStudentStatusChanges>();
            TempStatusChangeHistoryTbl = new HashSet<TempStatusChangeHistoryTbl>();
        }

        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for EnrollDate</summary>*/
        public DateTime EnrollDate { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for ExpStartDate</summary>*/
        public DateTime? ExpStartDate { get; set; }
        /*<summary>The get and set for MidPtDate</summary>*/
        public DateTime? MidPtDate { get; set; }
        /*<summary>The get and set for ExpGradDate</summary>*/
        public DateTime? ExpGradDate { get; set; }
        /*<summary>The get and set for TransferDate</summary>*/
        public DateTime? TransferDate { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid? ShiftId { get; set; }
        /*<summary>The get and set for BillingMethodId</summary>*/
        public Guid? BillingMethodId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid? StatusCodeId { get; set; }
        /*<summary>The get and set for Lda</summary>*/
        public DateTime? Lda { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for EnrollmentId</summary>*/
        public string EnrollmentId { get; set; }
        /*<summary>The get and set for AdmissionsRep</summary>*/
        public Guid? AdmissionsRep { get; set; }
        /*<summary>The get and set for AcademicAdvisor</summary>*/
        public Guid? AcademicAdvisor { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for TuitionCategoryId</summary>*/
        public Guid? TuitionCategoryId { get; set; }
        /*<summary>The get and set for DropReasonId</summary>*/
        public Guid? DropReasonId { get; set; }
        /*<summary>The get and set for DateDetermined</summary>*/
        public DateTime? DateDetermined { get; set; }
        /*<summary>The get and set for EdLvlId</summary>*/
        public Guid? EdLvlId { get; set; }
        /*<summary>The get and set for FaadvisorId</summary>*/
        public Guid? FaadvisorId { get; set; }
        /*<summary>The get and set for Attendtypeid</summary>*/
        public Guid? Attendtypeid { get; set; }
        /*<summary>The get and set for Degcertseekingid</summary>*/
        public Guid? Degcertseekingid { get; set; }
        /*<summary>The get and set for ReEnrollmentDate</summary>*/
        public DateTime? ReEnrollmentDate { get; set; }
        /*<summary>The get and set for BadgeNumber</summary>*/
        public string BadgeNumber { get; set; }
        /*<summary>The get and set for CohortStartDate</summary>*/
        public DateTime? CohortStartDate { get; set; }
        /*<summary>The get and set for Sapid</summary>*/
        public Guid? Sapid { get; set; }
        /*<summary>The get and set for ContractedGradDate</summary>*/
        public DateTime? ContractedGradDate { get; set; }
        /*<summary>The get and set for TransferHours</summary>*/
        public decimal? TransferHours { get; set; }
        /*<summary>The get and set for Graduatedorreceiveddate</summary>*/
        public DateTime? Graduatedorreceiveddate { get; set; }
        /*<summary>The get and set for DistanceEdStatus</summary>*/
        public string DistanceEdStatus { get; set; }
        /*<summary>The get and set for PrgVersionTypeId</summary>*/
        public int PrgVersionTypeId { get; set; }
        /*<summary>The get and set for IsDisabled</summary>*/
        public bool? IsDisabled { get; set; }
        /*<summary>The get and set for IsFirstTimeInSchool</summary>*/
        public bool IsFirstTimeInSchool { get; set; }
        /*<summary>The get and set for IsFirstTimePostSecSchool</summary>*/
        public bool IsFirstTimePostSecSchool { get; set; }
        /*<summary>The get and set for EntranceInterviewDate</summary>*/
        public DateTime? EntranceInterviewDate { get; set; }
        /*<summary>The get and set for DisableAutoCharge</summary>*/
        public bool DisableAutoCharge { get; set; }
        /*<summary>The get and set for ThirdPartyContract</summary>*/
        public bool ThirdPartyContract { get; set; }
        /*<summary>The get and set for TransferHoursFromThisSchoolEnrollmentId</summary>*/
        public Guid? TransferHoursFromThisSchoolEnrollmentId { get; set; }
        /*<summary>The get and set for TotalTransferHoursFromThisSchool</summary>*/
        public decimal TotalTransferHoursFromThisSchool { get; set; }
        /*<summary>The get and set for LicensureLastPartWrittenOn</summary>*/
        public DateTime? LicensureLastPartWrittenOn { get; set; }
        /*<summary>The get and set for LicensureWrittenAllParts</summary>*/
        public bool LicensureWrittenAllParts { get; set; }
        /*<summary>The get and set for LicensurePassedAllParts</summary>*/
        public bool LicensurePassedAllParts { get; set; }

        /*<summary>The navigational property for AcademicAdvisorNavigation</summary>*/
        public virtual SyUsers AcademicAdvisorNavigation { get; set; }
        /*<summary>The navigational property for AdmissionsRepNavigation</summary>*/
        public virtual SyUsers AdmissionsRepNavigation { get; set; }
        /*<summary>The navigational property for Attendtype</summary>*/
        public virtual ArAttendTypes Attendtype { get; set; }
        /*<summary>The navigational property for BillingMethod</summary>*/
        public virtual SaBillingMethods BillingMethod { get; set; }
        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for Degcertseeking</summary>*/
        public virtual AdDegCertSeeking Degcertseeking { get; set; }
        /*<summary>The navigational property for DropReason</summary>*/
        public virtual ArDropReasons DropReason { get; set; }
        /*<summary>The navigational property for EdLvl</summary>*/
        public virtual AdEdLvls EdLvl { get; set; }
        /*<summary>The navigational property for Faadvisor</summary>*/
        public virtual SyUsers Faadvisor { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for PrgVersionType</summary>*/
        public virtual ArProgramVersionType PrgVersionType { get; set; }
        /*<summary>The navigational property for Sap</summary>*/
        public virtual ArSap Sap { get; set; }
        /*<summary>The navigational property for Shift</summary>*/
        public virtual ArShifts Shift { get; set; }
        /*<summary>The navigational property for StatusCode</summary>*/
        public virtual SyStatusCodes StatusCode { get; set; }
        /*<summary>The navigational property for TransferHoursFromThisSchoolEnrollment</summary>*/
        public virtual ArStuEnrollments TransferHoursFromThisSchoolEnrollment { get; set; }
        /*<summary>The navigational property for TuitionCategory</summary>*/
        public virtual SaTuitionCategories TuitionCategory { get; set; }
        /*<summary>The navigational property for AdLeadByLeadGroups</summary>*/
        public virtual ICollection<AdLeadByLeadGroups>AdLeadByLeadGroups { get; set; }
        /*<summary>The navigational property for AfaPaymentPeriodStaging</summary>*/
        public virtual ICollection<AfaPaymentPeriodStaging>AfaPaymentPeriodStaging { get; set; }
        /*<summary>The navigational property for ArExternshipAttendance</summary>*/
        public virtual ICollection<ArExternshipAttendance>ArExternshipAttendance { get; set; }
        /*<summary>The navigational property for ArFasapchkResults</summary>*/
        public virtual ICollection<ArFasapchkResults>ArFasapchkResults { get; set; }
        /*<summary>The navigational property for ArGrdBkResults</summary>*/
        public virtual ICollection<ArGrdBkResults>ArGrdBkResults { get; set; }
        /*<summary>The navigational property for ArOverridenPreReqs</summary>*/
        public virtual ICollection<ArOverridenPreReqs>ArOverridenPreReqs { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetails</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetails { get; set; }
        /*<summary>The navigational property for ArResults</summary>*/
        public virtual ICollection<ArResults>ArResults { get; set; }
        /*<summary>The navigational property for ArSapchkResults</summary>*/
        public virtual ICollection<ArSapchkResults>ArSapchkResults { get; set; }
        /*<summary>The navigational property for ArSapquantResults</summary>*/
        public virtual ICollection<ArSapquantResults>ArSapquantResults { get; set; }
        /*<summary>The navigational property for ArScheduledHoursAdjustments</summary>*/
        public virtual ICollection<ArScheduledHoursAdjustments>ArScheduledHoursAdjustments { get; set; }
        /*<summary>The navigational property for ArStdSuspensions</summary>*/
        public virtual ICollection<ArStdSuspensions>ArStdSuspensions { get; set; }
        /*<summary>The navigational property for ArStuProbWarnings</summary>*/
        public virtual ICollection<ArStuProbWarnings>ArStuProbWarnings { get; set; }
        /*<summary>The navigational property for ArStuReschReason</summary>*/
        public virtual ICollection<ArStuReschReason>ArStuReschReason { get; set; }
        /*<summary>The navigational property for ArStudentClockAttendance</summary>*/
        public virtual ICollection<ArStudentClockAttendance>ArStudentClockAttendance { get; set; }
        /*<summary>The navigational property for ArStudentLoas</summary>*/
        public virtual ICollection<ArStudentLoas>ArStudentLoas { get; set; }
        /*<summary>The navigational property for ArStudentSchedules</summary>*/
        public virtual ICollection<ArStudentSchedules>ArStudentSchedules { get; set; }
        /*<summary>The navigational property for ArStudentTimeClockPunches</summary>*/
        public virtual ICollection<ArStudentTimeClockPunches>ArStudentTimeClockPunches { get; set; }
        /*<summary>The navigational property for ArTermEnrollSummary</summary>*/
        public virtual ICollection<ArTermEnrollSummary>ArTermEnrollSummary { get; set; }
        /*<summary>The navigational property for ArTransferGrades</summary>*/
        public virtual ICollection<ArTransferGrades>ArTransferGrades { get; set; }
        /*<summary>The navigational property for AtAttendance</summary>*/
        public virtual ICollection<AtAttendance>AtAttendance { get; set; }
        /*<summary>The navigational property for AtClsSectAttendance</summary>*/
        public virtual ICollection<AtClsSectAttendance>AtClsSectAttendance { get; set; }
        /*<summary>The navigational property for FaStudentAwards</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwards { get; set; }
        /*<summary>The navigational property for FaStudentPaymentPlans</summary>*/
        public virtual ICollection<FaStudentPaymentPlans>FaStudentPaymentPlans { get; set; }
        /*<summary>The navigational property for IntegrationEnrollmentTracking</summary>*/
        public virtual ICollection<IntegrationEnrollmentTracking>IntegrationEnrollmentTracking { get; set; }
        /*<summary>The navigational property for InverseTransferHoursFromThisSchoolEnrollment</summary>*/
        public virtual ICollection<ArStuEnrollments>InverseTransferHoursFromThisSchoolEnrollment { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
        /*<summary>The navigational property for SaAdmissionDeposits</summary>*/
        public virtual ICollection<SaAdmissionDeposits>SaAdmissionDeposits { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
        /*<summary>The navigational property for SyStatusChangesDeleted</summary>*/
        public virtual ICollection<SyStatusChangesDeleted>SyStatusChangesDeleted { get; set; }
        /*<summary>The navigational property for SyStudentStatusChanges</summary>*/
        public virtual ICollection<SyStudentStatusChanges>SyStudentStatusChanges { get; set; }
        /*<summary>The navigational property for TempStatusChangeHistoryTbl</summary>*/
        public virtual ICollection<TempStatusChangeHistoryTbl>TempStatusChangeHistoryTbl { get; set; }
    }
}