﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMessageGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMessageGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMessageGroups</summary>*/
    public partial class SyMessageGroups
    {
        /*<summary>The get and set for MessageGroupId</summary>*/
        public Guid MessageGroupId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for GroupDescrip</summary>*/
        public string GroupDescrip { get; set; }
        /*<summary>The get and set for MessageSchemaId</summary>*/
        public Guid MessageSchemaId { get; set; }
        /*<summary>The get and set for XmlData</summary>*/
        public string XmlData { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for MessageSchema</summary>*/
        public virtual SyMessageSchemas MessageSchema { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}