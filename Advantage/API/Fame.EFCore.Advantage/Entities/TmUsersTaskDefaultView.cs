﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmUsersTaskDefaultView.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmUsersTaskDefaultView definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmUsersTaskDefaultView</summary>*/
    public partial class TmUsersTaskDefaultView
    {
        /*<summary>The get and set for UserTaskDefViewId</summary>*/
        public Guid UserTaskDefViewId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for OthersId</summary>*/
        public Guid? OthersId { get; set; }
        /*<summary>The get and set for DefaultView</summary>*/
        public string DefaultView { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
    }
}