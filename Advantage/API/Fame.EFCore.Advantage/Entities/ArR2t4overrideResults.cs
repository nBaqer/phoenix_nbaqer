﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArR2t4overrideResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArR2t4overrideResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArR2t4overrideResults</summary>*/
    public partial class ArR2t4overrideResults
    {
        /*<summary>The get and set for R2t4overrideResultsId</summary>*/
        public Guid R2t4overrideResultsId { get; set; }
        /*<summary>The get and set for TerminationId</summary>*/
        public Guid TerminationId { get; set; }
        /*<summary>The get and set for SubTotalAmountDisbursedA</summary>*/
        public decimal? SubTotalAmountDisbursedA { get; set; }
        /*<summary>The get and set for SubTotalAmountCouldDisbursedC</summary>*/
        public decimal? SubTotalAmountCouldDisbursedC { get; set; }
        /*<summary>The get and set for SubTotalNetAmountDisbursedB</summary>*/
        public decimal? SubTotalNetAmountDisbursedB { get; set; }
        /*<summary>The get and set for SubTotalNetAmountDisbursedD</summary>*/
        public decimal? SubTotalNetAmountDisbursedD { get; set; }
        /*<summary>The get and set for BoxEresult</summary>*/
        public decimal? BoxEresult { get; set; }
        /*<summary>The get and set for BoxFresult</summary>*/
        public decimal? BoxFresult { get; set; }
        /*<summary>The get and set for BoxGresult</summary>*/
        public decimal? BoxGresult { get; set; }
        /*<summary>The get and set for PercentageOfActualAttendence</summary>*/
        public decimal? PercentageOfActualAttendence { get; set; }
        /*<summary>The get and set for BoxHresult</summary>*/
        public decimal? BoxHresult { get; set; }
        /*<summary>The get and set for BoxIresult</summary>*/
        public decimal? BoxIresult { get; set; }
        /*<summary>The get and set for BoxJresult</summary>*/
        public decimal? BoxJresult { get; set; }
        /*<summary>The get and set for BoxKresult</summary>*/
        public decimal? BoxKresult { get; set; }
        /*<summary>The get and set for BoxLresult</summary>*/
        public decimal? BoxLresult { get; set; }
        /*<summary>The get and set for BoxMresult</summary>*/
        public decimal? BoxMresult { get; set; }
        /*<summary>The get and set for BoxNresult</summary>*/
        public decimal? BoxNresult { get; set; }
        /*<summary>The get and set for BoxOresult</summary>*/
        public decimal? BoxOresult { get; set; }
        /*<summary>The get and set for UnsubDirectLoanSchoolReturn</summary>*/
        public decimal? UnsubDirectLoanSchoolReturn { get; set; }
        /*<summary>The get and set for SubDirectLoanSchoolReturn</summary>*/
        public decimal? SubDirectLoanSchoolReturn { get; set; }
        /*<summary>The get and set for PerkinsLoanSchoolReturn</summary>*/
        public decimal? PerkinsLoanSchoolReturn { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanSchoolReturn</summary>*/
        public decimal? DirectGraduatePlusLoanSchoolReturn { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanSchoolReturn</summary>*/
        public decimal? DirectParentPlusLoanSchoolReturn { get; set; }
        /*<summary>The get and set for BoxPresult</summary>*/
        public decimal? BoxPresult { get; set; }
        /*<summary>The get and set for PellGrantSchoolReturn</summary>*/
        public decimal? PellGrantSchoolReturn { get; set; }
        /*<summary>The get and set for FseogschoolReturn</summary>*/
        public decimal? FseogschoolReturn { get; set; }
        /*<summary>The get and set for TeachGrantSchoolReturn</summary>*/
        public decimal? TeachGrantSchoolReturn { get; set; }
        /*<summary>The get and set for IraqAfgGrantSchoolReturn</summary>*/
        public decimal? IraqAfgGrantSchoolReturn { get; set; }
        /*<summary>The get and set for BoxQresult</summary>*/
        public decimal? BoxQresult { get; set; }
        /*<summary>The get and set for BoxRresult</summary>*/
        public decimal? BoxRresult { get; set; }
        /*<summary>The get and set for BoxSresult</summary>*/
        public decimal? BoxSresult { get; set; }
        /*<summary>The get and set for BoxTresult</summary>*/
        public decimal? BoxTresult { get; set; }
        /*<summary>The get and set for BoxUresult</summary>*/
        public decimal? BoxUresult { get; set; }
        /*<summary>The get and set for PellGrantAmountToReturn</summary>*/
        public decimal? PellGrantAmountToReturn { get; set; }
        /*<summary>The get and set for FseogamountToReturn</summary>*/
        public decimal? FseogamountToReturn { get; set; }
        /*<summary>The get and set for TeachGrantAmountToReturn</summary>*/
        public decimal? TeachGrantAmountToReturn { get; set; }
        /*<summary>The get and set for IraqAfgGrantAmountToReturn</summary>*/
        public decimal? IraqAfgGrantAmountToReturn { get; set; }
        /*<summary>The get and set for TicketNumber</summary>*/
        public long? TicketNumber { get; set; }
        /*<summary>The get and set for PellGrantDisbursed</summary>*/
        public decimal? PellGrantDisbursed { get; set; }
        /*<summary>The get and set for PellGrantCouldDisbursed</summary>*/
        public decimal? PellGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for Fseogdisbursed</summary>*/
        public decimal? Fseogdisbursed { get; set; }
        /*<summary>The get and set for FseogcouldDisbursed</summary>*/
        public decimal? FseogcouldDisbursed { get; set; }
        /*<summary>The get and set for TeachGrantDisbursed</summary>*/
        public decimal? TeachGrantDisbursed { get; set; }
        /*<summary>The get and set for TeachGrantCouldDisbursed</summary>*/
        public decimal? TeachGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for IraqAfgGrantDisbursed</summary>*/
        public decimal? IraqAfgGrantDisbursed { get; set; }
        /*<summary>The get and set for IraqAfgGrantCouldDisbursed</summary>*/
        public decimal? IraqAfgGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for UnsubLoanNetAmountDisbursed</summary>*/
        public decimal? UnsubLoanNetAmountDisbursed { get; set; }
        /*<summary>The get and set for UnsubLoanNetAmountCouldDisbursed</summary>*/
        public decimal? UnsubLoanNetAmountCouldDisbursed { get; set; }
        /*<summary>The get and set for SubLoanNetAmountDisbursed</summary>*/
        public decimal? SubLoanNetAmountDisbursed { get; set; }
        /*<summary>The get and set for SubLoanNetAmountCouldDisbursed</summary>*/
        public decimal? SubLoanNetAmountCouldDisbursed { get; set; }
        /*<summary>The get and set for PerkinsLoanDisbursed</summary>*/
        public decimal? PerkinsLoanDisbursed { get; set; }
        /*<summary>The get and set for PerkinsLoanCouldDisbursed</summary>*/
        public decimal? PerkinsLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanDisbursed</summary>*/
        public decimal? DirectGraduatePlusLoanDisbursed { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanCouldDisbursed</summary>*/
        public decimal? DirectGraduatePlusLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanDisbursed</summary>*/
        public decimal? DirectParentPlusLoanDisbursed { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanCouldDisbursed</summary>*/
        public decimal? DirectParentPlusLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for IsAttendanceNotRequired</summary>*/
        public bool? IsAttendanceNotRequired { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for ScheduledEndDate</summary>*/
        public DateTime? ScheduledEndDate { get; set; }
        /*<summary>The get and set for WithdrawalDate</summary>*/
        public DateTime? WithdrawalDate { get; set; }
        /*<summary>The get and set for CompletedTime</summary>*/
        public decimal? CompletedTime { get; set; }
        /*<summary>The get and set for TotalTime</summary>*/
        public decimal? TotalTime { get; set; }
        /*<summary>The get and set for TuitionFee</summary>*/
        public decimal? TuitionFee { get; set; }
        /*<summary>The get and set for RoomFee</summary>*/
        public decimal? RoomFee { get; set; }
        /*<summary>The get and set for BoardFee</summary>*/
        public decimal? BoardFee { get; set; }
        /*<summary>The get and set for OtherFee</summary>*/
        public decimal? OtherFee { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }
        /*<summary>The get and set for CreditBalanceRefunded</summary>*/
        public decimal? CreditBalanceRefunded { get; set; }
        /*<summary>The get and set for OverriddenData</summary>*/
        public string OverriddenData { get; set; }
        /*<summary>The get and set for PostWithdrawalData</summary>*/
        public string PostWithdrawalData { get; set; }
        /*<summary>The get and set for IsR2t4overrideResultsCompleted</summary>*/
        public bool IsR2t4overrideResultsCompleted { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Termination</summary>*/
        public virtual ArR2t4terminationDetails Termination { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
    }
}