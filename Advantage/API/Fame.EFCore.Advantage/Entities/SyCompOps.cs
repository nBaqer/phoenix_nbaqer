﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCompOps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCompOps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCompOps</summary>*/
    public partial class SyCompOps
    {
        /*<summary>The get and set for CompOpId</summary>*/
        public byte CompOpId { get; set; }
        /*<summary>The get and set for CompOptext</summary>*/
        public string CompOptext { get; set; }
        /*<summary>The get and set for AllowMultiple</summary>*/
        public bool? AllowMultiple { get; set; }
        /*<summary>The get and set for AllowDateTime</summary>*/
        public bool AllowDateTime { get; set; }
        /*<summary>The get and set for AllowString</summary>*/
        public bool AllowString { get; set; }
        /*<summary>The get and set for AllowNumber</summary>*/
        public bool AllowNumber { get; set; }
    }
}