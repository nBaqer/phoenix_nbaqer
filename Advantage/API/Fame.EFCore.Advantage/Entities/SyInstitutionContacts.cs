﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutionContacts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutionContacts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutionContacts</summary>*/
    public partial class SyInstitutionContacts
    {
        /*<summary>The get and set for InstitutionContactId</summary>*/
        public Guid InstitutionContactId { get; set; }
        /*<summary>The get and set for InstitutionId</summary>*/
        public Guid InstitutionId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for PrefixId</summary>*/
        public Guid? PrefixId { get; set; }
        /*<summary>The get and set for SuffixId</summary>*/
        public Guid? SuffixId { get; set; }
        /*<summary>The get and set for Title</summary>*/
        public string Title { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for PhoneExt</summary>*/
        public string PhoneExt { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool? ForeignPhone { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool? IsDefault { get; set; }

        /*<summary>The navigational property for Institution</summary>*/
        public virtual SyInstitutions Institution { get; set; }
        /*<summary>The navigational property for Prefix</summary>*/
        public virtual SyPrefixes Prefix { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Suffix</summary>*/
        public virtual SySuffixes Suffix { get; set; }
    }
}