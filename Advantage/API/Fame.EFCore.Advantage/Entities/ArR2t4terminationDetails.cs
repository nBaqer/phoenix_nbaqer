﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArR2t4terminationDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArR2t4terminationDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArR2t4terminationDetails</summary>*/
    public partial class ArR2t4terminationDetails
    {
        /*<summary>The constructor for ArR2t4terminationDetails</summary>*/
        public ArR2t4terminationDetails()
        {
            ArR2t4additionalInformation = new HashSet<ArR2t4additionalInformation>();
            ArR2t4calculationResults = new HashSet<ArR2t4calculationResults>();
            ArR2t4input = new HashSet<ArR2t4input>();
            ArR2t4overrideInput = new HashSet<ArR2t4overrideInput>();
            ArR2t4overrideResults = new HashSet<ArR2t4overrideResults>();
            ArR2t4results = new HashSet<ArR2t4results>();
        }

        /*<summary>The get and set for TerminationId</summary>*/
        public Guid TerminationId { get; set; }
        /*<summary>The get and set for StuEnrollmentId</summary>*/
        public Guid StuEnrollmentId { get; set; }
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid? StatusCodeId { get; set; }
        /*<summary>The get and set for DropReasonId</summary>*/
        public Guid? DropReasonId { get; set; }
        /*<summary>The get and set for DateWithdrawalDetermined</summary>*/
        public DateTime? DateWithdrawalDetermined { get; set; }
        /*<summary>The get and set for LastDateAttended</summary>*/
        public DateTime? LastDateAttended { get; set; }
        /*<summary>The get and set for IsPerformingR2t4calculator</summary>*/
        public bool IsPerformingR2t4calculator { get; set; }
        /*<summary>The get and set for CalculationPeriodTypeId</summary>*/
        public Guid? CalculationPeriodTypeId { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }
        /*<summary>The get and set for IsR2t4approveTabEnabled</summary>*/
        public bool IsR2t4approveTabEnabled { get; set; }
        /*<summary>The get and set for IsTerminationReversed</summary>*/
        public bool? IsTerminationReversed { get; set; }

        /*<summary>The navigational property for CalculationPeriodType</summary>*/
        public virtual SyR2t4calculationPeriodTypes CalculationPeriodType { get; set; }
        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for DropReason</summary>*/
        public virtual ArDropReasons DropReason { get; set; }
        /*<summary>The navigational property for StatusCode</summary>*/
        public virtual SyStatusCodes StatusCode { get; set; }
        /*<summary>The navigational property for StuEnrollment</summary>*/
        public virtual ArStuEnrollments StuEnrollment { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4additionalInformation</summary>*/
        public virtual ICollection<ArR2t4additionalInformation>ArR2t4additionalInformation { get; set; }
        /*<summary>The navigational property for ArR2t4calculationResults</summary>*/
        public virtual ICollection<ArR2t4calculationResults>ArR2t4calculationResults { get; set; }
        /*<summary>The navigational property for ArR2t4input</summary>*/
        public virtual ICollection<ArR2t4input>ArR2t4input { get; set; }
        /*<summary>The navigational property for ArR2t4overrideInput</summary>*/
        public virtual ICollection<ArR2t4overrideInput>ArR2t4overrideInput { get; set; }
        /*<summary>The navigational property for ArR2t4overrideResults</summary>*/
        public virtual ICollection<ArR2t4overrideResults>ArR2t4overrideResults { get; set; }
        /*<summary>The navigational property for ArR2t4results</summary>*/
        public virtual ICollection<ArR2t4results>ArR2t4results { get; set; }
    }
}