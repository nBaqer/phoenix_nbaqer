﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTriggerOffsetTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTriggerOffsetTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTriggerOffsetTyps</summary>*/
    public partial class ArTriggerOffsetTyps
    {
        /*<summary>The get and set for TrigOffsetTypeId</summary>*/
        public int TrigOffsetTypeId { get; set; }
        /*<summary>The get and set for TrigOffTypDescrip</summary>*/
        public string TrigOffTypDescrip { get; set; }
    }
}