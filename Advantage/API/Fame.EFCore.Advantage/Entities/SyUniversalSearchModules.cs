﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUniversalSearchModules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUniversalSearchModules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUniversalSearchModules</summary>*/
    public partial class SyUniversalSearchModules
    {
        /*<summary>The get and set for UniversalSearchId</summary>*/
        public byte UniversalSearchId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModifiedDate</summary>*/
        public DateTime? ModifiedDate { get; set; }
        /*<summary>The get and set for ModifiedUser</summary>*/
        public string ModifiedUser { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
    }
}