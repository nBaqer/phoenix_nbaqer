﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptParams.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptParams definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptParams</summary>*/
    public partial class SyRptParams
    {
        /*<summary>The get and set for RptParamId</summary>*/
        public short RptParamId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }
        /*<summary>The get and set for TblFldsId</summary>*/
        public int TblFldsId { get; set; }
        /*<summary>The get and set for Required</summary>*/
        public bool Required { get; set; }
        /*<summary>The get and set for SortSec</summary>*/
        public bool SortSec { get; set; }
        /*<summary>The get and set for FilterListSec</summary>*/
        public bool FilterListSec { get; set; }
        /*<summary>The get and set for FilterOtherSec</summary>*/
        public bool FilterOtherSec { get; set; }
        /*<summary>The get and set for RptCaption</summary>*/
        public string RptCaption { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for TblFlds</summary>*/
        public virtual SyTblFlds TblFlds { get; set; }
    }
}