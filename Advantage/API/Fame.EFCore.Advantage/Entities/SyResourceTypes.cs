﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyResourceTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyResourceTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyResourceTypes</summary>*/
    public partial class SyResourceTypes
    {
        /*<summary>The constructor for SyResourceTypes</summary>*/
        public SyResourceTypes()
        {
            SyResources = new HashSet<SyResources>();
            SyUserResources = new HashSet<SyUserResources>();
        }

        /*<summary>The get and set for ResourceTypeId</summary>*/
        public byte ResourceTypeId { get; set; }
        /*<summary>The get and set for ResourceType</summary>*/
        public string ResourceType { get; set; }

        /*<summary>The navigational property for SyResources</summary>*/
        public virtual ICollection<SyResources>SyResources { get; set; }
        /*<summary>The navigational property for SyUserResources</summary>*/
        public virtual ICollection<SyUserResources>SyUserResources { get; set; }
    }
}