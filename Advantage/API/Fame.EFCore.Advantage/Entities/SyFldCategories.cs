﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFldCategories.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFldCategories definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFldCategories</summary>*/
    public partial class SyFldCategories
    {
        /*<summary>The get and set for CategoryId</summary>*/
        public int CategoryId { get; set; }
        /*<summary>The get and set for EntityId</summary>*/
        public short EntityId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
    }
}