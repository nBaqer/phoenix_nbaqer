﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArR2t4additionalInformation.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArR2t4additionalInformation definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArR2t4additionalInformation</summary>*/
    public partial class ArR2t4additionalInformation
    {
        /*<summary>The get and set for AdditionalInfoId</summary>*/
        public Guid AdditionalInfoId { get; set; }
        /*<summary>The get and set for TerminationId</summary>*/
        public Guid TerminationId { get; set; }
        /*<summary>The get and set for StepNo</summary>*/
        public short? StepNo { get; set; }
        /*<summary>The get and set for AdditionalInformtion</summary>*/
        public string AdditionalInformtion { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Termination</summary>*/
        public virtual ArR2t4terminationDetails Termination { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
    }
}