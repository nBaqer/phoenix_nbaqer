﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyQuickLinks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyQuickLinks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyQuickLinks</summary>*/
    public partial class SyQuickLinks
    {
        /*<summary>The get and set for QuickLinkId</summary>*/
        public Guid QuickLinkId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int ModuleId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for Url</summary>*/
        public string Url { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}