﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserResPermissions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserResPermissions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserResPermissions</summary>*/
    public partial class SyUserResPermissions
    {
        /*<summary>The get and set for ResPermissionId</summary>*/
        public Guid ResPermissionId { get; set; }
        /*<summary>The get and set for UserResourceId</summary>*/
        public int? UserResourceId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for Permission</summary>*/
        public int? Permission { get; set; }
    }
}