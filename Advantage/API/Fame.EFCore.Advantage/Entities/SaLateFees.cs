﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaLateFees.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaLateFees definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaLateFees</summary>*/
    public partial class SaLateFees
    {
        /*<summary>The get and set for LateFeesId</summary>*/
        public Guid LateFeesId { get; set; }
        /*<summary>The get and set for LateFeesCode</summary>*/
        public string LateFeesCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for LateFeesDescrip</summary>*/
        public string LateFeesDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid TransCodeId { get; set; }
        /*<summary>The get and set for EffectiveDate</summary>*/
        public DateTime EffectiveDate { get; set; }
        /*<summary>The get and set for FlatAmount</summary>*/
        public decimal FlatAmount { get; set; }
        /*<summary>The get and set for Rate</summary>*/
        public decimal Rate { get; set; }
        /*<summary>The get and set for GracePeriod</summary>*/
        public short GracePeriod { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
    }
}