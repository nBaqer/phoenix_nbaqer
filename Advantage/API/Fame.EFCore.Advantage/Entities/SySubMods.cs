﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySubMods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySubMods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySubMods</summary>*/
    public partial class SySubMods
    {
        /*<summary>The get and set for SubModId</summary>*/
        public short SubModId { get; set; }
        /*<summary>The get and set for SubModName</summary>*/
        public string SubModName { get; set; }
        /*<summary>The get and set for ResourceTypeId</summary>*/
        public byte ResourceTypeId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte ModuleId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}