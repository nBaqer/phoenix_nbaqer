﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkResultsHistory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkResultsHistory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkResultsHistory</summary>*/
    public partial class ArGrdBkResultsHistory
    {
        /*<summary>The get and set for GrdBkResultHistoryId</summary>*/
        public Guid GrdBkResultHistoryId { get; set; }
        /*<summary>The get and set for ModUserHistory</summary>*/
        public string ModUserHistory { get; set; }
        /*<summary>The get and set for ModDateHistory</summary>*/
        public DateTime ModDateHistory { get; set; }
        /*<summary>The get and set for GrdBkResultId</summary>*/
        public Guid GrdBkResultId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ResNum</summary>*/
        public int ResNum { get; set; }
        /*<summary>The get and set for PostDate</summary>*/
        public DateTime? PostDate { get; set; }
    }
}