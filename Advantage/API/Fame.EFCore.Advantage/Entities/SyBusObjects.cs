﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyBusObjects.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyBusObjects definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyBusObjects</summary>*/
    public partial class SyBusObjects
    {
        /*<summary>The constructor for SyBusObjects</summary>*/
        public SyBusObjects()
        {
            SyRptProps = new HashSet<SyRptProps>();
        }

        /*<summary>The get and set for BusObjectId</summary>*/
        public short BusObjectId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for SyRptProps</summary>*/
        public virtual ICollection<SyRptProps>SyRptProps { get; set; }
    }
}