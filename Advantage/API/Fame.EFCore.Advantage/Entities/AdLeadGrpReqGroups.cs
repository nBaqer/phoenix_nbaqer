﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadGrpReqGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadGrpReqGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadGrpReqGroups</summary>*/
    public partial class AdLeadGrpReqGroups
    {
        /*<summary>The get and set for LeadGrpReqGrpId</summary>*/
        public Guid LeadGrpReqGrpId { get; set; }
        /*<summary>The get and set for ReqGrpId</summary>*/
        public Guid? ReqGrpId { get; set; }
        /*<summary>The get and set for LeadGrpId</summary>*/
        public Guid? LeadGrpId { get; set; }
        /*<summary>The get and set for NumReqs</summary>*/
        public int? NumReqs { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }

        /*<summary>The navigational property for LeadGrp</summary>*/
        public virtual AdLeadGroups LeadGrp { get; set; }
        /*<summary>The navigational property for ReqGrp</summary>*/
        public virtual AdReqGroups ReqGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}