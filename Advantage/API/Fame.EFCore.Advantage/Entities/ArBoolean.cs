﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArBoolean.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArBoolean definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArBoolean</summary>*/
    public partial class ArBoolean
    {
        /*<summary>The get and set for BoolValue</summary>*/
        public bool BoolValue { get; set; }
        /*<summary>The get and set for BoolDescrip</summary>*/
        public string BoolDescrip { get; set; }
    }
}