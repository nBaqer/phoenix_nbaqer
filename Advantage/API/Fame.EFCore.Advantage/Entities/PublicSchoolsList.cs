﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PublicSchoolsList.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PublicSchoolsList definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PublicSchoolsList</summary>*/
    public partial class PublicSchoolsList
    {
        /*<summary>The get and set for NcesId</summary>*/
        public string NcesId { get; set; }
        /*<summary>The get and set for SchoolLevel</summary>*/
        public string SchoolLevel { get; set; }
        /*<summary>The get and set for SchoolDistrict</summary>*/
        public string SchoolDistrict { get; set; }
        /*<summary>The get and set for SchoolName</summary>*/
        public string SchoolName { get; set; }
        /*<summary>The get and set for SchoolCounty</summary>*/
        public string SchoolCounty { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for Address</summary>*/
        public string Address { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for State</summary>*/
        public string State { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for SchoolType</summary>*/
        public string SchoolType { get; set; }
        /*<summary>The get and set for StateFips</summary>*/
        public string StateFips { get; set; }
        /*<summary>The get and set for LowGrade</summary>*/
        public string LowGrade { get; set; }
        /*<summary>The get and set for HighGrade</summary>*/
        public string HighGrade { get; set; }
        /*<summary>The get and set for StudentsUg</summary>*/
        public string StudentsUg { get; set; }
        /*<summary>The get and set for StudentsPrek</summary>*/
        public string StudentsPrek { get; set; }
        /*<summary>The get and set for StudentsK</summary>*/
        public string StudentsK { get; set; }
        /*<summary>The get and set for Students1</summary>*/
        public string Students1 { get; set; }
        /*<summary>The get and set for Students2</summary>*/
        public string Students2 { get; set; }
        /*<summary>The get and set for Students3</summary>*/
        public string Students3 { get; set; }
        /*<summary>The get and set for Students4</summary>*/
        public string Students4 { get; set; }
        /*<summary>The get and set for Students5</summary>*/
        public string Students5 { get; set; }
        /*<summary>The get and set for Students6</summary>*/
        public string Students6 { get; set; }
        /*<summary>The get and set for Students7</summary>*/
        public string Students7 { get; set; }
        /*<summary>The get and set for Students8</summary>*/
        public string Students8 { get; set; }
        /*<summary>The get and set for Students9</summary>*/
        public string Students9 { get; set; }
        /*<summary>The get and set for Students10</summary>*/
        public string Students10 { get; set; }
        /*<summary>The get and set for Students11</summary>*/
        public string Students11 { get; set; }
        /*<summary>The get and set for Students12</summary>*/
        public string Students12 { get; set; }
        /*<summary>The get and set for OperationalStatus</summary>*/
        public string OperationalStatus { get; set; }
        /*<summary>The get and set for Latitude</summary>*/
        public string Latitude { get; set; }
        /*<summary>The get and set for Longitude</summary>*/
        public string Longitude { get; set; }
        /*<summary>The get and set for StateSchoolId</summary>*/
        public string StateSchoolId { get; set; }
        /*<summary>The get and set for StateDistrictId</summary>*/
        public string StateDistrictId { get; set; }
        /*<summary>The get and set for TotalMale</summary>*/
        public string TotalMale { get; set; }
        /*<summary>The get and set for TotalFemale</summary>*/
        public string TotalFemale { get; set; }
        /*<summary>The get and set for TotalAmericanIndianAlaskan</summary>*/
        public string TotalAmericanIndianAlaskan { get; set; }
        /*<summary>The get and set for TotalAsian</summary>*/
        public string TotalAsian { get; set; }
        /*<summary>The get and set for TotalBlack</summary>*/
        public string TotalBlack { get; set; }
        /*<summary>The get and set for TotalHispanic</summary>*/
        public string TotalHispanic { get; set; }
        /*<summary>The get and set for TotalWhite</summary>*/
        public string TotalWhite { get; set; }
        /*<summary>The get and set for FreeLunchEligible</summary>*/
        public string FreeLunchEligible { get; set; }
        /*<summary>The get and set for ReducePriceLunchEligible</summary>*/
        public string ReducePriceLunchEligible { get; set; }
        /*<summary>The get and set for FreeReducedLunchTotal</summary>*/
        public string FreeReducedLunchTotal { get; set; }
        /*<summary>The get and set for StudentTeacherRatio</summary>*/
        public string StudentTeacherRatio { get; set; }
        /*<summary>The get and set for FullTimeEquivalentTeachers</summary>*/
        public string FullTimeEquivalentTeachers { get; set; }
        /*<summary>The get and set for LocalEducationAgencyId</summary>*/
        public string LocalEducationAgencyId { get; set; }
        /*<summary>The get and set for LocalSchoolId</summary>*/
        public string LocalSchoolId { get; set; }
        /*<summary>The get and set for MailingAddress</summary>*/
        public string MailingAddress { get; set; }
        /*<summary>The get and set for MailingCity</summary>*/
        public string MailingCity { get; set; }
        /*<summary>The get and set for MailingState</summary>*/
        public string MailingState { get; set; }
        /*<summary>The get and set for MailingZip</summary>*/
        public string MailingZip { get; set; }
        /*<summary>The get and set for MailingZip4</summary>*/
        public string MailingZip4 { get; set; }
        /*<summary>The get and set for Zip4</summary>*/
        public string Zip4 { get; set; }
        /*<summary>The get and set for UnionIdentificationNumber</summary>*/
        public string UnionIdentificationNumber { get; set; }
        /*<summary>The get and set for UrbanLocaleCode</summary>*/
        public string UrbanLocaleCode { get; set; }
        /*<summary>The get and set for AnsiFipsCountyNumber</summary>*/
        public string AnsiFipsCountyNumber { get; set; }
        /*<summary>The get and set for DistrictCode113</summary>*/
        public string DistrictCode113 { get; set; }
        /*<summary>The get and set for DistrictCode112</summary>*/
        public string DistrictCode112 { get; set; }
        /*<summary>The get and set for BureauOfIndian</summary>*/
        public string BureauOfIndian { get; set; }
        /*<summary>The get and set for OfferedUg</summary>*/
        public string OfferedUg { get; set; }
        /*<summary>The get and set for OfferedPrek</summary>*/
        public string OfferedPrek { get; set; }
        /*<summary>The get and set for OfferedK</summary>*/
        public string OfferedK { get; set; }
        /*<summary>The get and set for Offered1</summary>*/
        public string Offered1 { get; set; }
        /*<summary>The get and set for Offered2</summary>*/
        public string Offered2 { get; set; }
        /*<summary>The get and set for Offered3</summary>*/
        public string Offered3 { get; set; }
        /*<summary>The get and set for Offered4</summary>*/
        public string Offered4 { get; set; }
        /*<summary>The get and set for Offered5</summary>*/
        public string Offered5 { get; set; }
        /*<summary>The get and set for Offered6</summary>*/
        public string Offered6 { get; set; }
        /*<summary>The get and set for Offered7</summary>*/
        public string Offered7 { get; set; }
        /*<summary>The get and set for Offered8</summary>*/
        public string Offered8 { get; set; }
        /*<summary>The get and set for Offered9</summary>*/
        public string Offered9 { get; set; }
        /*<summary>The get and set for Offered10</summary>*/
        public string Offered10 { get; set; }
        /*<summary>The get and set for Offered11</summary>*/
        public string Offered11 { get; set; }
        /*<summary>The get and set for Offered12</summary>*/
        public string Offered12 { get; set; }
        /*<summary>The get and set for Title1Status</summary>*/
        public string Title1Status { get; set; }
        /*<summary>The get and set for Title1Eligible</summary>*/
        public string Title1Eligible { get; set; }
        /*<summary>The get and set for Title1SchoolWide</summary>*/
        public string Title1SchoolWide { get; set; }
        /*<summary>The get and set for MagnetSchool</summary>*/
        public string MagnetSchool { get; set; }
        /*<summary>The get and set for CharterSchool</summary>*/
        public string CharterSchool { get; set; }
        /*<summary>The get and set for SharedTimeSchool</summary>*/
        public string SharedTimeSchool { get; set; }
        /*<summary>The get and set for CountFreeLunch</summary>*/
        public string CountFreeLunch { get; set; }
        /*<summary>The get and set for CountReducedLunch</summary>*/
        public string CountReducedLunch { get; set; }
        /*<summary>The get and set for TotalFreeReducedLunch</summary>*/
        public string TotalFreeReducedLunch { get; set; }
        /*<summary>The get and set for TotalPacific</summary>*/
        public string TotalPacific { get; set; }
        /*<summary>The get and set for Total2race</summary>*/
        public string Total2race { get; set; }
        /*<summary>The get and set for TotalStudents</summary>*/
        public string TotalStudents { get; set; }
        /*<summary>The get and set for InstId</summary>*/
        public int InstId { get; set; }
        /*<summary>The get and set for InstitutionId</summary>*/
        public Guid? InstitutionId { get; set; }
    }
}