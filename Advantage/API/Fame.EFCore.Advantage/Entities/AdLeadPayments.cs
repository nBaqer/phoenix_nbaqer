﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadPayments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadPayments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadPayments</summary>*/
    public partial class AdLeadPayments
    {
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for PaymentTypeId</summary>*/
        public int PaymentTypeId { get; set; }
        /*<summary>The get and set for CheckNumber</summary>*/
        public string CheckNumber { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for PaymentReference</summary>*/
        public int PaymentReference { get; set; }
        /*<summary>The get and set for IsDeposited</summary>*/
        public bool IsDeposited { get; set; }

        /*<summary>The navigational property for Transaction</summary>*/
        public virtual AdLeadTransactions Transaction { get; set; }
    }
}