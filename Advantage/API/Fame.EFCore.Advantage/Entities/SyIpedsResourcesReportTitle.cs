﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyIpedsResourcesReportTitle.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyIpedsResourcesReportTitle definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyIpedsResourcesReportTitle</summary>*/
    public partial class SyIpedsResourcesReportTitle
    {
        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for SurveyTitle</summary>*/
        public string SurveyTitle { get; set; }
        /*<summary>The get and set for ReportTitle</summary>*/
        public string ReportTitle { get; set; }
    }
}