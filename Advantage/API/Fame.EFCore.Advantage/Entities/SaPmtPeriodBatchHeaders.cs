﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPmtPeriodBatchHeaders.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPmtPeriodBatchHeaders definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPmtPeriodBatchHeaders</summary>*/
    public partial class SaPmtPeriodBatchHeaders
    {
        /*<summary>The constructor for SaPmtPeriodBatchHeaders</summary>*/
        public SaPmtPeriodBatchHeaders()
        {
            SaPmtPeriodBatchItems = new HashSet<SaPmtPeriodBatchItems>();
        }

        /*<summary>The get and set for PmtPeriodBatchHeaderId</summary>*/
        public int PmtPeriodBatchHeaderId { get; set; }
        /*<summary>The get and set for BatchName</summary>*/
        public string BatchName { get; set; }
        /*<summary>The get and set for BatchCreationDate</summary>*/
        public DateTime BatchCreationDate { get; set; }
        /*<summary>The get and set for IsPosted</summary>*/
        public bool IsPosted { get; set; }
        /*<summary>The get and set for PostedDate</summary>*/
        public DateTime? PostedDate { get; set; }
        /*<summary>The get and set for BatchItemCount</summary>*/
        public int BatchItemCount { get; set; }

        /*<summary>The navigational property for SaPmtPeriodBatchItems</summary>*/
        public virtual ICollection<SaPmtPeriodBatchItems>SaPmtPeriodBatchItems { get; set; }
    }
}