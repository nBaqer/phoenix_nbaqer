﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkConversionResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkConversionResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkConversionResults</summary>*/
    public partial class ArGrdBkConversionResults
    {
        /*<summary>The get and set for ConversionResultId</summary>*/
        public Guid ConversionResultId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ResNum</summary>*/
        public int ResNum { get; set; }
        /*<summary>The get and set for PostDate</summary>*/
        public DateTime? PostDate { get; set; }
        /*<summary>The get and set for PostUser</summary>*/
        public string PostUser { get; set; }
        /*<summary>The get and set for MinResult</summary>*/
        public int? MinResult { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for Required</summary>*/
        public bool? Required { get; set; }
        /*<summary>The get and set for MustPass</summary>*/
        public bool? MustPass { get; set; }
        /*<summary>The get and set for IsCourseCredited</summary>*/
        public bool? IsCourseCredited { get; set; }
    }
}