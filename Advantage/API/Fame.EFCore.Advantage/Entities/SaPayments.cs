﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPayments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPayments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPayments</summary>*/
    public partial class SaPayments
    {
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for PaymentTypeId</summary>*/
        public int PaymentTypeId { get; set; }
        /*<summary>The get and set for CheckNumber</summary>*/
        public string CheckNumber { get; set; }
        /*<summary>The get and set for ScheduledPayment</summary>*/
        public bool ScheduledPayment { get; set; }
        /*<summary>The get and set for BankAcctId</summary>*/
        public Guid? BankAcctId { get; set; }
        /*<summary>The get and set for IsDeposited</summary>*/
        public bool IsDeposited { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for BankAcct</summary>*/
        public virtual SaBankAccounts BankAcct { get; set; }
        /*<summary>The navigational property for PaymentType</summary>*/
        public virtual SaPaymentTypes PaymentType { get; set; }
        /*<summary>The navigational property for Transaction</summary>*/
        public virtual SaTransactions Transaction { get; set; }
    }
}