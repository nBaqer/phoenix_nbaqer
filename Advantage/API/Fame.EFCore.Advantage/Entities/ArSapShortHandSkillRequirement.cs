﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSapShortHandSkillRequirement.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSapShortHandSkillRequirement definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSapShortHandSkillRequirement</summary>*/
    public partial class ArSapShortHandSkillRequirement
    {
        /*<summary>The get and set for ShortHandSkillRequirementId</summary>*/
        public Guid ShortHandSkillRequirementId { get; set; }
        /*<summary>The get and set for SapdetailId</summary>*/
        public Guid? SapdetailId { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid? GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Speed</summary>*/
        public int? Speed { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Operator</summary>*/
        public string Operator { get; set; }
        /*<summary>The get and set for OperatorSequence</summary>*/
        public int? OperatorSequence { get; set; }
    }
}