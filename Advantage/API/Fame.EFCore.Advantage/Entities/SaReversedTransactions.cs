﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaReversedTransactions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaReversedTransactions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaReversedTransactions</summary>*/
    public partial class SaReversedTransactions
    {
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for ReversedTransactionId</summary>*/
        public Guid ReversedTransactionId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ReversalReason</summary>*/
        public string ReversalReason { get; set; }
        /*<summary>The get and set for ReversedTransactionsId</summary>*/
        public Guid ReversedTransactionsId { get; set; }
    }
}