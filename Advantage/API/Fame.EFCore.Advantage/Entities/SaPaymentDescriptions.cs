﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPaymentDescriptions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPaymentDescriptions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPaymentDescriptions</summary>*/
    public partial class SaPaymentDescriptions
    {
        /*<summary>The get and set for PmtDescriptionId</summary>*/
        public Guid PmtDescriptionId { get; set; }
        /*<summary>The get and set for PmtCode</summary>*/
        public string PmtCode { get; set; }
        /*<summary>The get and set for PmtDescription</summary>*/
        public string PmtDescription { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}