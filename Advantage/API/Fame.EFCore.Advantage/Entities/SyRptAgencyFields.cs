﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAgencyFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAgencyFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAgencyFields</summary>*/
    public partial class SyRptAgencyFields
    {
        /*<summary>The constructor for SyRptAgencyFields</summary>*/
        public SyRptAgencyFields()
        {
            SyRptAgencyFldValues = new HashSet<SyRptAgencyFldValues>();
        }

        /*<summary>The get and set for RptAgencyFldId</summary>*/
        public int RptAgencyFldId { get; set; }
        /*<summary>The get and set for RptAgencyId</summary>*/
        public int? RptAgencyId { get; set; }
        /*<summary>The get and set for RptFldId</summary>*/
        public int? RptFldId { get; set; }

        /*<summary>The navigational property for RptAgency</summary>*/
        public virtual SyRptAgencies RptAgency { get; set; }
        /*<summary>The navigational property for RptFld</summary>*/
        public virtual SyRptFields RptFld { get; set; }
        /*<summary>The navigational property for SyRptAgencyFldValues</summary>*/
        public virtual ICollection<SyRptAgencyFldValues>SyRptAgencyFldValues { get; set; }
    }
}