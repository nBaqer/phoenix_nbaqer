﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdStuGrpTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdStuGrpTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdStuGrpTypes</summary>*/
    public partial class AdStuGrpTypes
    {
        /*<summary>The get and set for GrpTypeId</summary>*/
        public Guid GrpTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
    }
}