﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutionAddresses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutionAddresses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutionAddresses</summary>*/
    public partial class SyInstitutionAddresses
    {
        /*<summary>The get and set for InstitutionAddressId</summary>*/
        public Guid InstitutionAddressId { get; set; }
        /*<summary>The get and set for InstitutionId</summary>*/
        public Guid InstitutionId { get; set; }
        /*<summary>The get and set for AddressTypeId</summary>*/
        public Guid? AddressTypeId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for ZipCode</summary>*/
        public string ZipCode { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IsMailingAddress</summary>*/
        public bool IsMailingAddress { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for State</summary>*/
        public string State { get; set; }
        /*<summary>The get and set for IsInternational</summary>*/
        public bool? IsInternational { get; set; }
        /*<summary>The get and set for CountyId</summary>*/
        public Guid? CountyId { get; set; }
        /*<summary>The get and set for ForeignCountyStr</summary>*/
        public string ForeignCountyStr { get; set; }
        /*<summary>The get and set for AddressApto</summary>*/
        public string AddressApto { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool? IsDefault { get; set; }
        /*<summary>The get and set for ForeignCountry</summary>*/
        public string ForeignCountry { get; set; }

        /*<summary>The navigational property for AddressType</summary>*/
        public virtual PlAddressTypes AddressType { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for County</summary>*/
        public virtual AdCounties County { get; set; }
        /*<summary>The navigational property for Institution</summary>*/
        public virtual SyInstitutions Institution { get; set; }
        /*<summary>The navigational property for StateNavigation</summary>*/
        public virtual SyStates StateNavigation { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}