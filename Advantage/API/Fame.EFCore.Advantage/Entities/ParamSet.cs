﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamSet.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamSet definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamSet</summary>*/
    public partial class ParamSet
    {
        /*<summary>The get and set for SetId</summary>*/
        public long SetId { get; set; }
        /*<summary>The get and set for SetName</summary>*/
        public string SetName { get; set; }
        /*<summary>The get and set for SetDisplayName</summary>*/
        public string SetDisplayName { get; set; }
        /*<summary>The get and set for SetDescription</summary>*/
        public string SetDescription { get; set; }
        /*<summary>The get and set for SetType</summary>*/
        public string SetType { get; set; }
    }
}