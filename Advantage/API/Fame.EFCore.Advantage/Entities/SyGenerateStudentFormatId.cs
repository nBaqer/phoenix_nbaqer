﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyGenerateStudentFormatId.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyGenerateStudentFormatId definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyGenerateStudentFormatId</summary>*/
    public partial class SyGenerateStudentFormatId
    {
        /*<summary>The get and set for StudentSeqId</summary>*/
        public int? StudentSeqId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for GenerateStudentFormatId</summary>*/
        public Guid GenerateStudentFormatId { get; set; }
    }
}