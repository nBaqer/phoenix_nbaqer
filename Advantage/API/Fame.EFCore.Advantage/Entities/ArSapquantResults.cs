﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSapquantResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSapquantResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSapquantResults</summary>*/
    public partial class ArSapquantResults
    {
        /*<summary>The get and set for ArSapquantResultId</summary>*/
        public Guid ArSapquantResultId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for SapdetailId</summary>*/
        public Guid SapdetailId { get; set; }
        /*<summary>The get and set for PercentCompleted</summary>*/
        public decimal? PercentCompleted { get; set; }

        /*<summary>The navigational property for InstructionType</summary>*/
        public virtual ArInstructionType InstructionType { get; set; }
        /*<summary>The navigational property for Sapdetail</summary>*/
        public virtual ArSapdetails Sapdetail { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}