﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadDocsReceived.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadDocsReceived definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadDocsReceived</summary>*/
    public partial class AdLeadDocsReceived
    {
        /*<summary>The get and set for LeadDocId</summary>*/
        public Guid LeadDocId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for DocumentId</summary>*/
        public Guid DocumentId { get; set; }
        /*<summary>The get and set for ReceiveDate</summary>*/
        public DateTime? ReceiveDate { get; set; }
        /*<summary>The get and set for DocStatusId</summary>*/
        public Guid? DocStatusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Override</summary>*/
        public bool Override { get; set; }
        /*<summary>The get and set for OverrideReason</summary>*/
        public string OverrideReason { get; set; }
        /*<summary>The get and set for ApprovalDate</summary>*/
        public DateTime? ApprovalDate { get; set; }

        /*<summary>The navigational property for DocStatus</summary>*/
        public virtual SyDocStatuses DocStatus { get; set; }
        /*<summary>The navigational property for Document</summary>*/
        public virtual AdReqs Document { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}