﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaAcademicYears.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaAcademicYears definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaAcademicYears</summary>*/
    public partial class SaAcademicYears
    {
        /*<summary>The constructor for SaAcademicYears</summary>*/
        public SaAcademicYears()
        {
            FaStudentAwards = new HashSet<FaStudentAwards>();
            FaStudentPaymentPlans = new HashSet<FaStudentPaymentPlans>();
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid AcademicYearId { get; set; }
        /*<summary>The get and set for AcademicYearCode</summary>*/
        public string AcademicYearCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for AcademicYearDescrip</summary>*/
        public string AcademicYearDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for FaStudentAwards</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwards { get; set; }
        /*<summary>The navigational property for FaStudentPaymentPlans</summary>*/
        public virtual ICollection<FaStudentPaymentPlans>FaStudentPaymentPlans { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}