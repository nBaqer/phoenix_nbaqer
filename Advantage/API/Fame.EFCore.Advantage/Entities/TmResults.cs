﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmResults</summary>*/
    public partial class TmResults
    {
        /*<summary>The constructor for TmResults</summary>*/
        public TmResults()
        {
            TmResultTasks = new HashSet<TmResultTasks>();
            TmTaskResults = new HashSet<TmTaskResults>();
        }

        /*<summary>The get and set for ResultId</summary>*/
        public Guid ResultId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for ResultActionId</summary>*/
        public Guid? ResultActionId { get; set; }
        /*<summary>The get and set for ResultActionValue</summary>*/
        public string ResultActionValue { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }

        /*<summary>The navigational property for TmResultTasks</summary>*/
        public virtual ICollection<TmResultTasks>TmResultTasks { get; set; }
        /*<summary>The navigational property for TmTaskResults</summary>*/
        public virtual ICollection<TmTaskResults>TmTaskResults { get; set; }
    }
}