﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdCountries.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdCountries definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdCountries</summary>*/
    public partial class AdCountries
    {
        /*<summary>The constructor for AdCountries</summary>*/
        public AdCountries()
        {
            AdColleges = new HashSet<AdColleges>();
            AdHighSchools = new HashSet<AdHighSchools>();
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeads = new HashSet<AdLeads>();
            FaLendersCountry = new HashSet<FaLenders>();
            FaLendersPayCountry = new HashSet<FaLenders>();
            HrEmployees = new HashSet<HrEmployees>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            PlEmployers = new HashSet<PlEmployers>();
            PriorWorkAddress = new HashSet<PriorWorkAddress>();
            SyCampuses = new HashSet<SyCampuses>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
            SyStateBoardAgencies = new HashSet<SyStateBoardAgencies>();
            SyStates = new HashSet<SyStates>();
        }

        /*<summary>The get and set for CountryId</summary>*/
        public Guid CountryId { get; set; }
        /*<summary>The get and set for CountryCode</summary>*/
        public string CountryCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CountryDescrip</summary>*/
        public string CountryDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool? IsDefault { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdColleges</summary>*/
        public virtual ICollection<AdColleges>AdColleges { get; set; }
        /*<summary>The navigational property for AdHighSchools</summary>*/
        public virtual ICollection<AdHighSchools>AdHighSchools { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for FaLendersCountry</summary>*/
        public virtual ICollection<FaLenders>FaLendersCountry { get; set; }
        /*<summary>The navigational property for FaLendersPayCountry</summary>*/
        public virtual ICollection<FaLenders>FaLendersPayCountry { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PriorWorkAddress</summary>*/
        public virtual ICollection<PriorWorkAddress>PriorWorkAddress { get; set; }
        /*<summary>The navigational property for SyCampuses</summary>*/
        public virtual ICollection<SyCampuses>SyCampuses { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
        /*<summary>The navigational property for SyStateBoardAgencies</summary>*/
        public virtual ICollection<SyStateBoardAgencies>SyStateBoardAgencies { get; set; }
        /*<summary>The navigational property for SyStates</summary>*/
        public virtual ICollection<SyStates>SyStates { get; set; }
    }
}