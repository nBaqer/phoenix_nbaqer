﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptFilterListPrefs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptFilterListPrefs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptFilterListPrefs</summary>*/
    public partial class SyRptFilterListPrefs
    {
        /*<summary>The get and set for FilterListPrefId</summary>*/
        public Guid FilterListPrefId { get; set; }
        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for RptParamId</summary>*/
        public short RptParamId { get; set; }
        /*<summary>The get and set for FldValue</summary>*/
        public string FldValue { get; set; }

        /*<summary>The navigational property for Pref</summary>*/
        public virtual SyRptUserPrefs Pref { get; set; }
    }
}