﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTitleIvsapStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTitleIvsapStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTitleIvsapStatus</summary>*/
    public partial class SyTitleIvsapStatus
    {
        /*<summary>The constructor for SyTitleIvsapStatus</summary>*/
        public SyTitleIvsapStatus()
        {
            ArFasapchkResults = new HashSet<ArFasapchkResults>();
            SyTitleIvsapCustomVerbiage = new HashSet<SyTitleIvsapCustomVerbiage>();
        }

        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for DefaultMessage</summary>*/
        public string DefaultMessage { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
        /*<summary>The navigational property for ArFasapchkResults</summary>*/
        public virtual ICollection<ArFasapchkResults>ArFasapchkResults { get; set; }
        /*<summary>The navigational property for SyTitleIvsapCustomVerbiage</summary>*/
        public virtual ICollection<SyTitleIvsapCustomVerbiage>SyTitleIvsapCustomVerbiage { get; set; }
    }
}