﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadEntranceTest.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadEntranceTest definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadEntranceTest</summary>*/
    public partial class AdLeadEntranceTest
    {
        /*<summary>The get and set for LeadEntrTestId</summary>*/
        public Guid LeadEntrTestId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for EntrTestId</summary>*/
        public Guid? EntrTestId { get; set; }
        /*<summary>The get and set for TestTaken</summary>*/
        public string TestTaken { get; set; }
        /*<summary>The get and set for ActualScore</summary>*/
        public decimal? ActualScore { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int? ViewOrder { get; set; }
        /*<summary>The get and set for Required</summary>*/
        public bool? Required { get; set; }
        /*<summary>The get and set for Pass</summary>*/
        public bool? Pass { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for OverRide</summary>*/
        public bool? OverRide { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }
        /*<summary>The get and set for OverrideReason</summary>*/
        public string OverrideReason { get; set; }
        /*<summary>The get and set for CompletedDate</summary>*/
        public DateTime? CompletedDate { get; set; }

        /*<summary>The navigational property for EntrTest</summary>*/
        public virtual AdReqs EntrTest { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}