﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdVendorPayFor.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdVendorPayFor definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdVendorPayFor</summary>*/
    public partial class AdVendorPayFor
    {
        /*<summary>The constructor for AdVendorPayFor</summary>*/
        public AdVendorPayFor()
        {
            AdVendorCampaign = new HashSet<AdVendorCampaign>();
        }

        /*<summary>The get and set for IdPayFor</summary>*/
        public int IdPayFor { get; set; }
        /*<summary>The get and set for PayForCode</summary>*/
        public string PayForCode { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for AdVendorCampaign</summary>*/
        public virtual ICollection<AdVendorCampaign>AdVendorCampaign { get; set; }
    }
}