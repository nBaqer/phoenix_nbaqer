﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPtGrdScales.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPtGrdScales definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPtGrdScales</summary>*/
    public partial class ArPtGrdScales
    {
        /*<summary>The get and set for PtGrdScaleId</summary>*/
        public Guid PtGrdScaleId { get; set; }
        /*<summary>The get and set for PtGrdScaleDescrip</summary>*/
        public string PtGrdScaleDescrip { get; set; }
        /*<summary>The get and set for PtGrdScalePerc</summary>*/
        public decimal PtGrdScalePerc { get; set; }
        /*<summary>The get and set for Letter</summary>*/
        public string Letter { get; set; }
        /*<summary>The get and set for Gpa</summary>*/
        public decimal Gpa { get; set; }
        /*<summary>The get and set for CredtsEarnd</summary>*/
        public byte CredtsEarnd { get; set; }
        /*<summary>The get and set for CredtsAttemptd</summary>*/
        public byte CredtsAttemptd { get; set; }
        /*<summary>The get and set for IncInGpa</summary>*/
        public bool IncInGpa { get; set; }
        /*<summary>The get and set for IncInSap</summary>*/
        public bool IncInSap { get; set; }
        /*<summary>The get and set for TransGrd</summary>*/
        public bool TransGrd { get; set; }
    }
}