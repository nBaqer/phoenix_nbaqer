﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlInterview.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlInterview definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlInterview</summary>*/
    public partial class PlInterview
    {
        /*<summary>The constructor for PlInterview</summary>*/
        public PlInterview()
        {
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for InterviewId</summary>*/
        public Guid InterviewId { get; set; }
        /*<summary>The get and set for InterviewDescrip</summary>*/
        public string InterviewDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for InterViewCode</summary>*/
        public string InterViewCode { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}