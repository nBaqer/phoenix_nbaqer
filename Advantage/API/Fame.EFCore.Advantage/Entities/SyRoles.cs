﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRoles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRoles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRoles</summary>*/
    public partial class SyRoles
    {
        /*<summary>The constructor for SyRoles</summary>*/
        public SyRoles()
        {
            SyLeadStatusChangePermissions = new HashSet<SyLeadStatusChangePermissions>();
            SyRlsResLvls = new HashSet<SyRlsResLvls>();
            SyUsersRolesCampGrps = new HashSet<SyUsersRolesCampGrps>();
        }

        /*<summary>The get and set for RoleId</summary>*/
        public Guid RoleId { get; set; }
        /*<summary>The get and set for Role</summary>*/
        public string Role { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for SysRoleId</summary>*/
        public int SysRoleId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SysRole</summary>*/
        public virtual SySysRoles SysRole { get; set; }
        /*<summary>The navigational property for SyLeadStatusChangePermissions</summary>*/
        public virtual ICollection<SyLeadStatusChangePermissions>SyLeadStatusChangePermissions { get; set; }
        /*<summary>The navigational property for SyRlsResLvls</summary>*/
        public virtual ICollection<SyRlsResLvls>SyRlsResLvls { get; set; }
        /*<summary>The navigational property for SyUsersRolesCampGrps</summary>*/
        public virtual ICollection<SyUsersRolesCampGrps>SyUsersRolesCampGrps { get; set; }
    }
}