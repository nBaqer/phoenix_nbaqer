﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFamilyIncome.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFamilyIncome definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFamilyIncome</summary>*/
    public partial class SyFamilyIncome
    {
        /*<summary>The constructor for SyFamilyIncome</summary>*/
        public SyFamilyIncome()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for FamilyIncomeId</summary>*/
        public Guid FamilyIncomeId { get; set; }
        /*<summary>The get and set for FamilyIncomeDescrip</summary>*/
        public string FamilyIncomeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for FamilyIncomeCode</summary>*/
        public string FamilyIncomeCode { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}