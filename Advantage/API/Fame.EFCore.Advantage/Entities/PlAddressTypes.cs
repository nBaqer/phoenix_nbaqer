﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlAddressTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlAddressTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlAddressTypes</summary>*/
    public partial class PlAddressTypes
    {
        /*<summary>The constructor for PlAddressTypes</summary>*/
        public PlAddressTypes()
        {
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeads = new HashSet<AdLeads>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
        }

        /*<summary>The get and set for AddressTypeId</summary>*/
        public Guid AddressTypeId { get; set; }
        /*<summary>The get and set for AddressDescrip</summary>*/
        public string AddressDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for AddressCode</summary>*/
        public string AddressCode { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
    }
}