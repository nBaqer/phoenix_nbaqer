﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AfaLeadSyncException.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AfaLeadSyncException definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AfaLeadSyncException</summary>*/
    public partial class AfaLeadSyncException
    {
        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for AfaStudentId</summary>*/
        public long AfaStudentId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for ExceptionReason</summary>*/
        public string ExceptionReason { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}