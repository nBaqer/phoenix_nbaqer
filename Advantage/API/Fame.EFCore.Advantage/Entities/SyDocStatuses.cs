﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyDocStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyDocStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyDocStatuses</summary>*/
    public partial class SyDocStatuses
    {
        /*<summary>The constructor for SyDocStatuses</summary>*/
        public SyDocStatuses()
        {
            AdLeadDocsReceived = new HashSet<AdLeadDocsReceived>();
            PlStudentDocs = new HashSet<PlStudentDocs>();
        }

        /*<summary>The get and set for DocStatusId</summary>*/
        public Guid DocStatusId { get; set; }
        /*<summary>The get and set for DocStatusCode</summary>*/
        public string DocStatusCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for DocStatusDescrip</summary>*/
        public string DocStatusDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for SysDocStatusId</summary>*/
        public byte SysDocStatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadDocsReceived</summary>*/
        public virtual ICollection<AdLeadDocsReceived>AdLeadDocsReceived { get; set; }
        /*<summary>The navigational property for PlStudentDocs</summary>*/
        public virtual ICollection<PlStudentDocs>PlStudentDocs { get; set; }
    }
}