﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPrgVerDefaultChargePeriods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPrgVerDefaultChargePeriods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPrgVerDefaultChargePeriods</summary>*/
    public partial class SaPrgVerDefaultChargePeriods
    {
        /*<summary>The get and set for PrgVerPmtId</summary>*/
        public Guid PrgVerPmtId { get; set; }
        /*<summary>The get and set for FeeLevelId</summary>*/
        public byte FeeLevelId { get; set; }
        /*<summary>The get and set for SysTransCodeId</summary>*/
        public int SysTransCodeId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for PeriodCanChange</summary>*/
        public bool PeriodCanChange { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ProgramVersionDefaultChargePeriodsId</summary>*/
        public Guid ProgramVersionDefaultChargePeriodsId { get; set; }

        /*<summary>The navigational property for FeeLevel</summary>*/
        public virtual SaFeeLevels FeeLevel { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for SysTransCode</summary>*/
        public virtual SaSysTransCodes SysTransCode { get; set; }
    }
}