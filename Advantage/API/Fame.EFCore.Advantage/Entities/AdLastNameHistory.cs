﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLastNameHistory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLastNameHistory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLastNameHistory</summary>*/
    public partial class AdLastNameHistory
    {
        /*<summary>The get and set for Id</summary>*/
        public long Id { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}