﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdFullPartTime.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdFullPartTime definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdFullPartTime</summary>*/
    public partial class AdFullPartTime
    {
        /*<summary>The constructor for AdFullPartTime</summary>*/
        public AdFullPartTime()
        {
            PlExitInterview = new HashSet<PlExitInterview>();
        }

        /*<summary>The get and set for FullTimeId</summary>*/
        public Guid FullTimeId { get; set; }
        /*<summary>The get and set for FullPartTimeCode</summary>*/
        public string FullPartTimeCode { get; set; }
        /*<summary>The get and set for FullPartTimeDescrip</summary>*/
        public string FullPartTimeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
    }
}