﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgCredential.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgCredential definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgCredential</summary>*/
    public partial class ArProgCredential
    {
        /*<summary>The constructor for ArProgCredential</summary>*/
        public ArProgCredential()
        {
            ArPrograms = new HashSet<ArPrograms>();
        }

        /*<summary>The get and set for CredentialId</summary>*/
        public Guid CredentialId { get; set; }
        /*<summary>The get and set for CredentialCode</summary>*/
        public string CredentialCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CredentialDescription</summary>*/
        public string CredentialDescription { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Gesequence</summary>*/
        public int? Gesequence { get; set; }
        /*<summary>The get and set for GerptAgencyFldValId</summary>*/
        public int? GerptAgencyFldValId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrograms</summary>*/
        public virtual ICollection<ArPrograms>ArPrograms { get; set; }
    }
}