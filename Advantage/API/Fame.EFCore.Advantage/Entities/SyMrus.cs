﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMrus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMrus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMrus</summary>*/
    public partial class SyMrus
    {
        /*<summary>The get and set for Mruid</summary>*/
        public Guid Mruid { get; set; }
        /*<summary>The get and set for Counter</summary>*/
        public int Counter { get; set; }
        /*<summary>The get and set for ChildId</summary>*/
        public Guid ChildId { get; set; }
        /*<summary>The get and set for MrutypeId</summary>*/
        public byte MrutypeId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for SortOrder</summary>*/
        public int? SortOrder { get; set; }
        /*<summary>The get and set for IsSticky</summary>*/
        public bool? IsSticky { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for User</summary>*/
        public virtual SyUsers User { get; set; }
    }
}