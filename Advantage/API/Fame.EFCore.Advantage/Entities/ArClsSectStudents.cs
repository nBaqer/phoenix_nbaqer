﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArClsSectStudents.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArClsSectStudents definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArClsSectStudents</summary>*/
    public partial class ArClsSectStudents
    {
        /*<summary>The get and set for ClsSectStudentId</summary>*/
        public Guid ClsSectStudentId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for Grade</summary>*/
        public string Grade { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
    }
}