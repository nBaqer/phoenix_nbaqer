﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdSkills.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdSkills definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdSkills</summary>*/
    public partial class AdSkills
    {
        /*<summary>The get and set for SkillId</summary>*/
        public int SkillId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for LevelId</summary>*/
        public int LevelId { get; set; }
        /*<summary>The get and set for SkillGrpId</summary>*/
        public Guid SkillGrpId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for Comment</summary>*/
        public string Comment { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Level</summary>*/
        public virtual AdLevel Level { get; set; }
        /*<summary>The navigational property for SkillGrp</summary>*/
        public virtual PlSkillGroups SkillGrp { get; set; }
    }
}