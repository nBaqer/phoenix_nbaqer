﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdPrgVerTestDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdPrgVerTestDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdPrgVerTestDetails</summary>*/
    public partial class AdPrgVerTestDetails
    {
        /*<summary>The get and set for PrgVerTestDetailId</summary>*/
        public Guid PrgVerTestDetailId { get; set; }
        /*<summary>The get and set for MinScore</summary>*/
        public decimal? MinScore { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int? ViewOrder { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }
        /*<summary>The get and set for ReqGrpId</summary>*/
        public Guid? ReqGrpId { get; set; }
        /*<summary>The get and set for AdReqId</summary>*/
        public Guid? AdReqId { get; set; }

        /*<summary>The navigational property for AdReq</summary>*/
        public virtual AdReqs AdReq { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for ReqGrp</summary>*/
        public virtual AdReqGroups ReqGrp { get; set; }
    }
}