﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="MsgEntitiesFieldGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The MsgEntitiesFieldGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for MsgEntitiesFieldGroups</summary>*/
    public partial class MsgEntitiesFieldGroups
    {
        /*<summary>The get and set for EntityFieldGroupId</summary>*/
        public Guid EntityFieldGroupId { get; set; }
        /*<summary>The get and set for EntityId</summary>*/
        public short? EntityId { get; set; }
        /*<summary>The get and set for FieldGroupName</summary>*/
        public string FieldGroupName { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }
    }
}