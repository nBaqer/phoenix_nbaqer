﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlEmployerJobCats.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlEmployerJobCats definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlEmployerJobCats</summary>*/
    public partial class PlEmployerJobCats
    {
        /*<summary>The get and set for EmployerJobId</summary>*/
        public Guid EmployerJobId { get; set; }
        /*<summary>The get and set for EmployerId</summary>*/
        public Guid? EmployerId { get; set; }
        /*<summary>The get and set for JobCatId</summary>*/
        public Guid? JobCatId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Employer</summary>*/
        public virtual PlEmployers Employer { get; set; }
        /*<summary>The navigational property for JobCat</summary>*/
        public virtual PlJobType JobCat { get; set; }
    }
}