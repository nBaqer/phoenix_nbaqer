﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdSourceCatagory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdSourceCatagory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdSourceCatagory</summary>*/
    public partial class AdSourceCatagory
    {
        /*<summary>The constructor for AdSourceCatagory</summary>*/
        public AdSourceCatagory()
        {
            AdLeads = new HashSet<AdLeads>();
            AdSourceType = new HashSet<AdSourceType>();
        }

        /*<summary>The get and set for SourceCatagoryDescrip</summary>*/
        public string SourceCatagoryDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for SourceCatagoryCode</summary>*/
        public string SourceCatagoryCode { get; set; }
        /*<summary>The get and set for SourceCatagoryId</summary>*/
        public Guid SourceCatagoryId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for AdSourceType</summary>*/
        public virtual ICollection<AdSourceType>AdSourceType { get; set; }
    }
}