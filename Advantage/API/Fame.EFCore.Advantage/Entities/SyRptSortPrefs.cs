﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptSortPrefs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptSortPrefs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptSortPrefs</summary>*/
    public partial class SyRptSortPrefs
    {
        /*<summary>The get and set for SortPrefId</summary>*/
        public Guid SortPrefId { get; set; }
        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for RptParamId</summary>*/
        public short RptParamId { get; set; }
        /*<summary>The get and set for Seq</summary>*/
        public byte Seq { get; set; }

        /*<summary>The navigational property for Pref</summary>*/
        public virtual SyRptUserPrefs Pref { get; set; }
    }
}