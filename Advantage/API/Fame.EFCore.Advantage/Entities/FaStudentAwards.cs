﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaStudentAwards.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaStudentAwards definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaStudentAwards</summary>*/
    public partial class FaStudentAwards
    {
        /*<summary>The constructor for FaStudentAwards</summary>*/
        public FaStudentAwards()
        {
            FaStudentAwardSchedule = new HashSet<FaStudentAwardSchedule>();
        }

        /*<summary>The get and set for StudentAwardId</summary>*/
        public Guid StudentAwardId { get; set; }
        /*<summary>The get and set for AwardId</summary>*/
        public int? AwardId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for AwardTypeId</summary>*/
        public Guid AwardTypeId { get; set; }
        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid? AcademicYearId { get; set; }
        /*<summary>The get and set for LenderId</summary>*/
        public Guid? LenderId { get; set; }
        /*<summary>The get and set for ServicerId</summary>*/
        public Guid? ServicerId { get; set; }
        /*<summary>The get and set for GuarantorId</summary>*/
        public Guid? GuarantorId { get; set; }
        /*<summary>The get and set for GrossAmount</summary>*/
        public decimal GrossAmount { get; set; }
        /*<summary>The get and set for LoanFees</summary>*/
        public decimal? LoanFees { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for AwardStartDate</summary>*/
        public DateTime AwardStartDate { get; set; }
        /*<summary>The get and set for AwardEndDate</summary>*/
        public DateTime AwardEndDate { get; set; }
        /*<summary>The get and set for Disbursements</summary>*/
        public int Disbursements { get; set; }
        /*<summary>The get and set for LoanId</summary>*/
        public string LoanId { get; set; }
        /*<summary>The get and set for EmasfundCode</summary>*/
        public string EmasfundCode { get; set; }
        /*<summary>The get and set for FaId</summary>*/
        public string FaId { get; set; }
        /*<summary>The get and set for AwardCode</summary>*/
        public string AwardCode { get; set; }
        /*<summary>The get and set for AwardSubCode</summary>*/
        public string AwardSubCode { get; set; }
        /*<summary>The get and set for AwardStatus</summary>*/
        public string AwardStatus { get; set; }

        /*<summary>The navigational property for AcademicYear</summary>*/
        public virtual SaAcademicYears AcademicYear { get; set; }
        /*<summary>The navigational property for AwardType</summary>*/
        public virtual SaFundSources AwardType { get; set; }
        /*<summary>The navigational property for Guarantor</summary>*/
        public virtual FaLenders Guarantor { get; set; }
        /*<summary>The navigational property for Lender</summary>*/
        public virtual FaLenders Lender { get; set; }
        /*<summary>The navigational property for Servicer</summary>*/
        public virtual FaLenders Servicer { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for FaStudentAwardSchedule</summary>*/
        public virtual ICollection<FaStudentAwardSchedule>FaStudentAwardSchedule { get; set; }
    }
}