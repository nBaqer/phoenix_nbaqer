﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="References.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The References definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for References</summary>*/
    public partial class References
    {
        /*<summary>The get and set for StudentContactId</summary>*/
        public Guid? StudentContactId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for ProspectId</summary>*/
        public string ProspectId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for Address</summary>*/
        public string Address { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for State</summary>*/
        public string State { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for Country</summary>*/
        public string Country { get; set; }
        /*<summary>The get and set for PhoneNo</summary>*/
        public string PhoneNo { get; set; }
        /*<summary>The get and set for Relationship</summary>*/
        public string Relationship { get; set; }
        /*<summary>The get and set for ForEmergency</summary>*/
        public string ForEmergency { get; set; }
        /*<summary>The get and set for ReferenceId</summary>*/
        public Guid ReferenceId { get; set; }
    }
}