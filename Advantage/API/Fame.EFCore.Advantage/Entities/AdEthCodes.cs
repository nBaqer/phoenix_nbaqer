﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdEthCodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdEthCodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdEthCodes</summary>*/
    public partial class AdEthCodes
    {
        /*<summary>The constructor for AdEthCodes</summary>*/
        public AdEthCodes()
        {
            AdLeads = new HashSet<AdLeads>();
            HrEmployees = new HashSet<HrEmployees>();
        }

        /*<summary>The get and set for EthCodeId</summary>*/
        public Guid EthCodeId { get; set; }
        /*<summary>The get and set for EthCode</summary>*/
        public string EthCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for EthCodeDescrip</summary>*/
        public string EthCodeDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
    }
}