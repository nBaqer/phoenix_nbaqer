﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdExtraCurrGrp.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdExtraCurrGrp definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdExtraCurrGrp</summary>*/
    public partial class AdExtraCurrGrp
    {
        /*<summary>The constructor for AdExtraCurrGrp</summary>*/
        public AdExtraCurrGrp()
        {
            AdExtraCurr = new HashSet<AdExtraCurr>();
            AdExtraCurricular = new HashSet<AdExtraCurricular>();
        }

        /*<summary>The get and set for ExtraCurrGrpId</summary>*/
        public Guid ExtraCurrGrpId { get; set; }
        /*<summary>The get and set for ExtraCurrGrpCode</summary>*/
        public string ExtraCurrGrpCode { get; set; }
        /*<summary>The get and set for ExtraCurrGrpDescrip</summary>*/
        public string ExtraCurrGrpDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdExtraCurr</summary>*/
        public virtual ICollection<AdExtraCurr>AdExtraCurr { get; set; }
        /*<summary>The navigational property for AdExtraCurricular</summary>*/
        public virtual ICollection<AdExtraCurricular>AdExtraCurricular { get; set; }
    }
}