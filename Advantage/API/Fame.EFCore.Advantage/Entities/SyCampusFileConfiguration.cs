﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCampusFileConfiguration.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCampusFileConfiguration definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCampusFileConfiguration</summary>*/
    public partial class SyCampusFileConfiguration
    {
        /*<summary>The constructor for SyCampusFileConfiguration</summary>*/
        public SyCampusFileConfiguration()
        {
            SyCustomFeatureFileConfiguration = new HashSet<SyCustomFeatureFileConfiguration>();
        }

        /*<summary>The get and set for CampusFileConfigurationId</summary>*/
        public Guid CampusFileConfigurationId { get; set; }
        /*<summary>The get and set for CampusGroupId</summary>*/
        public Guid CampusGroupId { get; set; }
        /*<summary>The get and set for FileStorageType</summary>*/
        public int FileStorageType { get; set; }
        /*<summary>The get and set for AppliesToAllFeatures</summary>*/
        public bool AppliesToAllFeatures { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }
        /*<summary>The get and set for Password</summary>*/
        public string Password { get; set; }
        /*<summary>The get and set for Path</summary>*/
        public string Path { get; set; }
        /*<summary>The get and set for CloudKey</summary>*/
        public string CloudKey { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampusGroup</summary>*/
        public virtual SyCampGrps CampusGroup { get; set; }
        /*<summary>The navigational property for SyCustomFeatureFileConfiguration</summary>*/
        public virtual ICollection<SyCustomFeatureFileConfiguration>SyCustomFeatureFileConfiguration { get; set; }
    }
}