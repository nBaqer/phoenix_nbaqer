﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AtAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AtAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AtAttendance</summary>*/
    public partial class AtAttendance
    {
        /*<summary>The get and set for AttendanceId</summary>*/
        public Guid AttendanceId { get; set; }
        /*<summary>The get and set for EnrollId</summary>*/
        public Guid? EnrollId { get; set; }
        /*<summary>The get and set for AttendanceType</summary>*/
        public string AttendanceType { get; set; }
        /*<summary>The get and set for AttendanceTypeId</summary>*/
        public Guid? AttendanceTypeId { get; set; }
        /*<summary>The get and set for AttendanceDate</summary>*/
        public DateTime? AttendanceDate { get; set; }
        /*<summary>The get and set for Actual</summary>*/
        public short? Actual { get; set; }
        /*<summary>The get and set for Tardy</summary>*/
        public bool? Tardy { get; set; }
        /*<summary>The get and set for TardyTime</summary>*/
        public int? TardyTime { get; set; }
        /*<summary>The get and set for Excused</summary>*/
        public bool? Excused { get; set; }
        /*<summary>The get and set for ExcusedTime</summary>*/
        public int? ExcusedTime { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Enroll</summary>*/
        public virtual ArStuEnrollments Enroll { get; set; }
    }
}