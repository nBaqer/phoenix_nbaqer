﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptSqlcount.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptSqlcount definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptSqlcount</summary>*/
    public partial class SyRptSqlcount
    {
        /*<summary>The get and set for RptSqlcountId</summary>*/
        public int RptSqlcountId { get; set; }
        /*<summary>The get and set for RptSqlid</summary>*/
        public int? RptSqlid { get; set; }
        /*<summary>The get and set for RptCountStmt</summary>*/
        public string RptCountStmt { get; set; }
    }
}