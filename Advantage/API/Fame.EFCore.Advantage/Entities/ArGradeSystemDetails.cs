﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGradeSystemDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGradeSystemDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGradeSystemDetails</summary>*/
    public partial class ArGradeSystemDetails
    {
        /*<summary>The constructor for ArGradeSystemDetails</summary>*/
        public ArGradeSystemDetails()
        {
            ArGradeScaleDetails = new HashSet<ArGradeScaleDetails>();
            ArProgVerDef = new HashSet<ArProgVerDef>();
            ArResults = new HashSet<ArResults>();
            ArTransferGrades = new HashSet<ArTransferGrades>();
        }

        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid GrdSysDetailId { get; set; }
        /*<summary>The get and set for GrdSystemId</summary>*/
        public Guid GrdSystemId { get; set; }
        /*<summary>The get and set for Grade</summary>*/
        public string Grade { get; set; }
        /*<summary>The get and set for IsPass</summary>*/
        public bool IsPass { get; set; }
        /*<summary>The get and set for Gpa</summary>*/
        public decimal? Gpa { get; set; }
        /*<summary>The get and set for IsCreditsEarned</summary>*/
        public bool IsCreditsEarned { get; set; }
        /*<summary>The get and set for IsCreditsAttempted</summary>*/
        public bool IsCreditsAttempted { get; set; }
        /*<summary>The get and set for IsInGpa</summary>*/
        public bool IsInGpa { get; set; }
        /*<summary>The get and set for IsInSap</summary>*/
        public bool IsInSap { get; set; }
        /*<summary>The get and set for IsTransferGrade</summary>*/
        public bool IsTransferGrade { get; set; }
        /*<summary>The get and set for IsIncomplete</summary>*/
        public bool IsIncomplete { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool IsDefault { get; set; }
        /*<summary>The get and set for IsDrop</summary>*/
        public bool IsDrop { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for ShowInstructor</summary>*/
        public bool? ShowInstructor { get; set; }
        /*<summary>The get and set for IsSatisfactory</summary>*/
        public bool? IsSatisfactory { get; set; }
        /*<summary>The get and set for IsUnSatisfactory</summary>*/
        public bool? IsUnSatisfactory { get; set; }
        /*<summary>The get and set for IsInPass</summary>*/
        public bool? IsInPass { get; set; }
        /*<summary>The get and set for IsInFail</summary>*/
        public bool? IsInFail { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for GradeDescription</summary>*/
        public string GradeDescription { get; set; }
        /*<summary>The get and set for Quality</summary>*/
        public string Quality { get; set; }
        /*<summary>The get and set for IsCreditsAwarded</summary>*/
        public bool IsCreditsAwarded { get; set; }

        /*<summary>The navigational property for GrdSystem</summary>*/
        public virtual ArGradeSystems GrdSystem { get; set; }
        /*<summary>The navigational property for ArGradeScaleDetails</summary>*/
        public virtual ICollection<ArGradeScaleDetails>ArGradeScaleDetails { get; set; }
        /*<summary>The navigational property for ArProgVerDef</summary>*/
        public virtual ICollection<ArProgVerDef>ArProgVerDef { get; set; }
        /*<summary>The navigational property for ArResults</summary>*/
        public virtual ICollection<ArResults>ArResults { get; set; }
        /*<summary>The navigational property for ArTransferGrades</summary>*/
        public virtual ICollection<ArTransferGrades>ArTransferGrades { get; set; }
    }
}