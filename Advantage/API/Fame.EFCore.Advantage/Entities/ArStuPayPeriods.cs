﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStuPayPeriods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStuPayPeriods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStuPayPeriods</summary>*/
    public partial class ArStuPayPeriods
    {
        /*<summary>The get and set for StuEnrollPayPeriodId</summary>*/
        public Guid StuEnrollPayPeriodId { get; set; }
        /*<summary>The get and set for StudentEnrollmentId</summary>*/
        public Guid StudentEnrollmentId { get; set; }
        /*<summary>The get and set for PaymentPeriod</summary>*/
        public int PaymentPeriod { get; set; }
        /*<summary>The get and set for PaymentPeriodRange</summary>*/
        public string PaymentPeriodRange { get; set; }
        /*<summary>The get and set for AcademicCalendarType</summary>*/
        public string AcademicCalendarType { get; set; }
        /*<summary>The get and set for PeriodStartDate</summary>*/
        public DateTime PeriodStartDate { get; set; }
        /*<summary>The get and set for PeriodEndDate</summary>*/
        public DateTime PeriodEndDate { get; set; }
        /*<summary>The get and set for AcaYrLen</summary>*/
        public int AcaYrLen { get; set; }
        /*<summary>The get and set for ProgLength</summary>*/
        public decimal ProgLength { get; set; }
        /*<summary>The get and set for PeriodLength</summary>*/
        public decimal? PeriodLength { get; set; }
        /*<summary>The get and set for PayPeriodsPerAcYr</summary>*/
        public int PayPeriodsPerAcYr { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for AcademicYear</summary>*/
        public int? AcademicYear { get; set; }
    }
}