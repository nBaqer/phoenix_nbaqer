﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStatusChangeDeleteReasons.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStatusChangeDeleteReasons definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStatusChangeDeleteReasons</summary>*/
    public partial class SyStatusChangeDeleteReasons
    {
        /*<summary>The constructor for SyStatusChangeDeleteReasons</summary>*/
        public SyStatusChangeDeleteReasons()
        {
            SyStatusChangesDeleted = new HashSet<SyStatusChangesDeleted>();
        }

        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for SyStatusChangesDeleted</summary>*/
        public virtual ICollection<SyStatusChangesDeleted>SyStatusChangesDeleted { get; set; }
    }
}