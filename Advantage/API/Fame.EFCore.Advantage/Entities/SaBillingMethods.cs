﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaBillingMethods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaBillingMethods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaBillingMethods</summary>*/
    public partial class SaBillingMethods
    {
        /*<summary>The constructor for SaBillingMethods</summary>*/
        public SaBillingMethods()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
        }

        /*<summary>The get and set for BillingMethodId</summary>*/
        public Guid BillingMethodId { get; set; }
        /*<summary>The get and set for BillingMethodCode</summary>*/
        public string BillingMethodCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for BillingMethodDescrip</summary>*/
        public string BillingMethodDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for BillingMethod</summary>*/
        public short BillingMethod { get; set; }
        /*<summary>The get and set for AutoBill</summary>*/
        public bool AutoBill { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SaIncrements</summary>*/
        public virtual SaIncrements SaIncrements { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
    }
}