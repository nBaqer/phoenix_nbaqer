﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadGroups</summary>*/
    public partial class AdLeadGroups
    {
        /*<summary>The constructor for AdLeadGroups</summary>*/
        public AdLeadGroups()
        {
            AdLeadByLeadGroups = new HashSet<AdLeadByLeadGroups>();
            AdLeadGrpReqGroups = new HashSet<AdLeadGrpReqGroups>();
            AdLeads = new HashSet<AdLeads>();
            AdReqGrpDef = new HashSet<AdReqGrpDef>();
            AdReqLeadGroups = new HashSet<AdReqLeadGroups>();
            ArClassSections = new HashSet<ArClassSections>();
        }

        /*<summary>The get and set for LeadGrpId</summary>*/
        public Guid LeadGrpId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for UseForScheduling</summary>*/
        public bool? UseForScheduling { get; set; }
        /*<summary>The get and set for UseForStudentGroupTracking</summary>*/
        public bool? UseForStudentGroupTracking { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadByLeadGroups</summary>*/
        public virtual ICollection<AdLeadByLeadGroups>AdLeadByLeadGroups { get; set; }
        /*<summary>The navigational property for AdLeadGrpReqGroups</summary>*/
        public virtual ICollection<AdLeadGrpReqGroups>AdLeadGrpReqGroups { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for AdReqGrpDef</summary>*/
        public virtual ICollection<AdReqGrpDef>AdReqGrpDef { get; set; }
        /*<summary>The navigational property for AdReqLeadGroups</summary>*/
        public virtual ICollection<AdReqLeadGroups>AdReqLeadGroups { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
    }
}