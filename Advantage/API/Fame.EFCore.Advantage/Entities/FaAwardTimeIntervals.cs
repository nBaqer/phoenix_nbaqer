﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaAwardTimeIntervals.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaAwardTimeIntervals definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaAwardTimeIntervals</summary>*/
    public partial class FaAwardTimeIntervals
    {
        /*<summary>The get and set for AwdTimeIntervalId</summary>*/
        public int AwdTimeIntervalId { get; set; }
        /*<summary>The get and set for AwdTimeIntervalDescrip</summary>*/
        public string AwdTimeIntervalDescrip { get; set; }
    }
}