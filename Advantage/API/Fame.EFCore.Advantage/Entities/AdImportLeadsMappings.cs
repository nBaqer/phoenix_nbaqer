﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdImportLeadsMappings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdImportLeadsMappings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdImportLeadsMappings</summary>*/
    public partial class AdImportLeadsMappings
    {
        /*<summary>The constructor for AdImportLeadsMappings</summary>*/
        public AdImportLeadsMappings()
        {
            AdImportLeadsAdvFldMappings = new HashSet<AdImportLeadsAdvFldMappings>();
            AdImportLeadsLookupValsMap = new HashSet<AdImportLeadsLookupValsMap>();
        }

        /*<summary>The get and set for MappingId</summary>*/
        public Guid MappingId { get; set; }
        /*<summary>The get and set for MapName</summary>*/
        public string MapName { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdImportLeadsAdvFldMappings</summary>*/
        public virtual ICollection<AdImportLeadsAdvFldMappings>AdImportLeadsAdvFldMappings { get; set; }
        /*<summary>The navigational property for AdImportLeadsLookupValsMap</summary>*/
        public virtual ICollection<AdImportLeadsLookupValsMap>AdImportLeadsLookupValsMap { get; set; }
    }
}