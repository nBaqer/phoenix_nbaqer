﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCreditsPerService.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCreditsPerService definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCreditsPerService</summary>*/
    public partial class ArCreditsPerService
    {
        /*<summary>The get and set for CreditPerServiceId</summary>*/
        public Guid CreditPerServiceId { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid? GrdComponentTypeId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for NumberOfCredits</summary>*/
        public decimal? NumberOfCredits { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
    }
}