﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutionPhone.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutionPhone definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutionPhone</summary>*/
    public partial class SyInstitutionPhone
    {
        /*<summary>The get and set for InstitutionPhoneId</summary>*/
        public Guid InstitutionPhoneId { get; set; }
        /*<summary>The get and set for InstitutionId</summary>*/
        public Guid InstitutionId { get; set; }
        /*<summary>The get and set for PhoneTypeId</summary>*/
        public Guid? PhoneTypeId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsForeignPhone</summary>*/
        public bool IsForeignPhone { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IsDefault</summary>*/
        public bool? IsDefault { get; set; }

        /*<summary>The navigational property for Institution</summary>*/
        public virtual SyInstitutions Institution { get; set; }
        /*<summary>The navigational property for PhoneType</summary>*/
        public virtual SyPhoneType PhoneType { get; set; }
    }
}