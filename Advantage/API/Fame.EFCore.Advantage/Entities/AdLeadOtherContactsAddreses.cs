﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadOtherContactsAddreses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadOtherContactsAddreses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadOtherContactsAddreses</summary>*/
    public partial class AdLeadOtherContactsAddreses
    {
        /*<summary>The get and set for OtherContactsAddresesId</summary>*/
        public Guid OtherContactsAddresesId { get; set; }
        /*<summary>The get and set for OtherContactId</summary>*/
        public Guid OtherContactId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for AddressTypeId</summary>*/
        public Guid AddressTypeId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for ZipCode</summary>*/
        public string ZipCode { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IsMailingAddress</summary>*/
        public bool IsMailingAddress { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for State</summary>*/
        public string State { get; set; }
        /*<summary>The get and set for IsInternational</summary>*/
        public bool? IsInternational { get; set; }
        /*<summary>The get and set for CountyId</summary>*/
        public Guid? CountyId { get; set; }
        /*<summary>The get and set for County</summary>*/
        public string County { get; set; }
        /*<summary>The get and set for Country</summary>*/
        public string Country { get; set; }

        /*<summary>The navigational property for AddressType</summary>*/
        public virtual PlAddressTypes AddressType { get; set; }
        /*<summary>The navigational property for CountryNavigation</summary>*/
        public virtual AdCountries CountryNavigation { get; set; }
        /*<summary>The navigational property for CountyNavigation</summary>*/
        public virtual AdCounties CountyNavigation { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for OtherContact</summary>*/
        public virtual AdLeadOtherContacts OtherContact { get; set; }
        /*<summary>The navigational property for StateNavigation</summary>*/
        public virtual SyStates StateNavigation { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}