﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCustomFeatureFileConfiguration.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCustomFeatureFileConfiguration definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCustomFeatureFileConfiguration</summary>*/
    public partial class SyCustomFeatureFileConfiguration
    {
        /*<summary>The get and set for CustomFeatureConfigurationId</summary>*/
        public Guid CustomFeatureConfigurationId { get; set; }
        /*<summary>The get and set for FeatureId</summary>*/
        public int FeatureId { get; set; }
        /*<summary>The get and set for CampusConfigurationId</summary>*/
        public Guid CampusConfigurationId { get; set; }
        /*<summary>The get and set for FileStorageType</summary>*/
        public int FileStorageType { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }
        /*<summary>The get and set for Password</summary>*/
        public string Password { get; set; }
        /*<summary>The get and set for Path</summary>*/
        public string Path { get; set; }
        /*<summary>The get and set for CloudKey</summary>*/
        public string CloudKey { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampusConfiguration</summary>*/
        public virtual SyCampusFileConfiguration CampusConfiguration { get; set; }
        /*<summary>The navigational property for Feature</summary>*/
        public virtual SyFileConfigurationFeatures Feature { get; set; }
    }
}