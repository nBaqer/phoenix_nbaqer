﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaRefunds.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaRefunds definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaRefunds</summary>*/
    public partial class SaRefunds
    {
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for RefundTypeId</summary>*/
        public int RefundTypeId { get; set; }
        /*<summary>The get and set for BankAcctId</summary>*/
        public Guid? BankAcctId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid? FundSourceId { get; set; }
        /*<summary>The get and set for StudentAwardId</summary>*/
        public Guid? StudentAwardId { get; set; }
        /*<summary>The get and set for IsInstCharge</summary>*/
        public bool IsInstCharge { get; set; }
        /*<summary>The get and set for AwardScheduleId</summary>*/
        public Guid? AwardScheduleId { get; set; }
        /*<summary>The get and set for RefundAmount</summary>*/
        public decimal? RefundAmount { get; set; }

        /*<summary>The navigational property for BankAcct</summary>*/
        public virtual SaBankAccounts BankAcct { get; set; }
        /*<summary>The navigational property for FundSource</summary>*/
        public virtual SaFundSources FundSource { get; set; }
        /*<summary>The navigational property for Transaction</summary>*/
        public virtual SaTransactions Transaction { get; set; }
    }
}