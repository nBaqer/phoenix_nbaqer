﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArR2t4input.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArR2t4input definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArR2t4input</summary>*/
    public partial class ArR2t4input
    {
        /*<summary>The get and set for R2t4inputId</summary>*/
        public Guid R2t4inputId { get; set; }
        /*<summary>The get and set for TerminationId</summary>*/
        public Guid TerminationId { get; set; }
        /*<summary>The get and set for ProgramUnitTypeId</summary>*/
        public int ProgramUnitTypeId { get; set; }
        /*<summary>The get and set for PellGrantDisbursed</summary>*/
        public decimal? PellGrantDisbursed { get; set; }
        /*<summary>The get and set for PellGrantCouldDisbursed</summary>*/
        public decimal? PellGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for Fseogdisbursed</summary>*/
        public decimal? Fseogdisbursed { get; set; }
        /*<summary>The get and set for FseogcouldDisbursed</summary>*/
        public decimal? FseogcouldDisbursed { get; set; }
        /*<summary>The get and set for TeachGrantDisbursed</summary>*/
        public decimal? TeachGrantDisbursed { get; set; }
        /*<summary>The get and set for TeachGrantCouldDisbursed</summary>*/
        public decimal? TeachGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for IraqAfgGrantDisbursed</summary>*/
        public decimal? IraqAfgGrantDisbursed { get; set; }
        /*<summary>The get and set for IraqAfgGrantCouldDisbursed</summary>*/
        public decimal? IraqAfgGrantCouldDisbursed { get; set; }
        /*<summary>The get and set for UnsubLoanNetAmountDisbursed</summary>*/
        public decimal? UnsubLoanNetAmountDisbursed { get; set; }
        /*<summary>The get and set for UnsubLoanNetAmountCouldDisbursed</summary>*/
        public decimal? UnsubLoanNetAmountCouldDisbursed { get; set; }
        /*<summary>The get and set for SubLoanNetAmountDisbursed</summary>*/
        public decimal? SubLoanNetAmountDisbursed { get; set; }
        /*<summary>The get and set for SubLoanNetAmountCouldDisbursed</summary>*/
        public decimal? SubLoanNetAmountCouldDisbursed { get; set; }
        /*<summary>The get and set for PerkinsLoanDisbursed</summary>*/
        public decimal? PerkinsLoanDisbursed { get; set; }
        /*<summary>The get and set for PerkinsLoanCouldDisbursed</summary>*/
        public decimal? PerkinsLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanDisbursed</summary>*/
        public decimal? DirectGraduatePlusLoanDisbursed { get; set; }
        /*<summary>The get and set for DirectGraduatePlusLoanCouldDisbursed</summary>*/
        public decimal? DirectGraduatePlusLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanDisbursed</summary>*/
        public decimal? DirectParentPlusLoanDisbursed { get; set; }
        /*<summary>The get and set for DirectParentPlusLoanCouldDisbursed</summary>*/
        public decimal? DirectParentPlusLoanCouldDisbursed { get; set; }
        /*<summary>The get and set for IsAttendanceNotRequired</summary>*/
        public bool IsAttendanceNotRequired { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for ScheduledEndDate</summary>*/
        public DateTime? ScheduledEndDate { get; set; }
        /*<summary>The get and set for WithdrawalDate</summary>*/
        public DateTime? WithdrawalDate { get; set; }
        /*<summary>The get and set for CompletedTime</summary>*/
        public decimal? CompletedTime { get; set; }
        /*<summary>The get and set for TotalTime</summary>*/
        public decimal? TotalTime { get; set; }
        /*<summary>The get and set for TuitionFee</summary>*/
        public decimal? TuitionFee { get; set; }
        /*<summary>The get and set for RoomFee</summary>*/
        public decimal? RoomFee { get; set; }
        /*<summary>The get and set for BoardFee</summary>*/
        public decimal? BoardFee { get; set; }
        /*<summary>The get and set for OtherFee</summary>*/
        public decimal? OtherFee { get; set; }
        /*<summary>The get and set for IsTuitionChargedByPaymentPeriod</summary>*/
        public bool? IsTuitionChargedByPaymentPeriod { get; set; }
        /*<summary>The get and set for CreditBalanceRefunded</summary>*/
        public decimal? CreditBalanceRefunded { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for UpdatedById</summary>*/
        public Guid? UpdatedById { get; set; }
        /*<summary>The get and set for UpdatedDate</summary>*/
        public DateTime? UpdatedDate { get; set; }
        /*<summary>The get and set for IsR2t4inputCompleted</summary>*/
        public bool IsR2t4inputCompleted { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Termination</summary>*/
        public virtual ArR2t4terminationDetails Termination { get; set; }
        /*<summary>The navigational property for UpdatedBy</summary>*/
        public virtual SyUsers UpdatedBy { get; set; }
    }
}