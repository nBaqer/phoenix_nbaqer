﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArQuantMinUnitTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArQuantMinUnitTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArQuantMinUnitTyps</summary>*/
    public partial class ArQuantMinUnitTyps
    {
        /*<summary>The get and set for QuantMinUnitTypId</summary>*/
        public byte QuantMinUnitTypId { get; set; }
        /*<summary>The get and set for QuantMinTypDesc</summary>*/
        public string QuantMinTypDesc { get; set; }
    }
}