﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArReqGrpDef.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArReqGrpDef definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArReqGrpDef</summary>*/
    public partial class ArReqGrpDef
    {
        /*<summary>The get and set for ReqGrpId</summary>*/
        public Guid ReqGrpId { get; set; }
        /*<summary>The get and set for GrpId</summary>*/
        public Guid GrpId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for ReqSeq</summary>*/
        public byte ReqSeq { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool IsRequired { get; set; }
        /*<summary>The get and set for Cnt</summary>*/
        public short? Cnt { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public decimal? Hours { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for Grp</summary>*/
        public virtual ArReqs Grp { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
    }
}