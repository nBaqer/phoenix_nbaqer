﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArDropReasons.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArDropReasons definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArDropReasons</summary>*/
    public partial class ArDropReasons
    {
        /*<summary>The constructor for ArDropReasons</summary>*/
        public ArDropReasons()
        {
            ArR2t4terminationDetails = new HashSet<ArR2t4terminationDetails>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            SyNaccasdropReasonsMapping = new HashSet<SyNaccasdropReasonsMapping>();
        }

        /*<summary>The get and set for DropReasonId</summary>*/
        public Guid DropReasonId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetails</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetails { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for SyNaccasdropReasonsMapping</summary>*/
        public virtual ICollection<SyNaccasdropReasonsMapping>SyNaccasdropReasonsMapping { get; set; }
    }
}