﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUsersRolesCampGrps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUsersRolesCampGrps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUsersRolesCampGrps</summary>*/
    public partial class SyUsersRolesCampGrps
    {
        /*<summary>The get and set for UserRoleCampGrpId</summary>*/
        public Guid UserRoleCampGrpId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for RoleId</summary>*/
        public Guid RoleId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Role</summary>*/
        public virtual SyRoles Role { get; set; }
        /*<summary>The navigational property for User</summary>*/
        public virtual SyUsers User { get; set; }
    }
}