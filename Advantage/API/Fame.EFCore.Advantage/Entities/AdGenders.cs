﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdGenders.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdGenders definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdGenders</summary>*/
    public partial class AdGenders
    {
        /*<summary>The constructor for AdGenders</summary>*/
        public AdGenders()
        {
            AdLeads = new HashSet<AdLeads>();
            HrEmployees = new HashSet<HrEmployees>();
        }

        /*<summary>The get and set for GenderId</summary>*/
        public Guid GenderId { get; set; }
        /*<summary>The get and set for GenderCode</summary>*/
        public string GenderCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for GenderDescrip</summary>*/
        public string GenderDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }
        /*<summary>The get and set for AfaMappingId</summary>*/
        public Guid? AfaMappingId { get; set; }

        /*<summary>The navigational property for AfaMapping</summary>*/
        public virtual AfaCatalogMapping AfaMapping { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
    }
}