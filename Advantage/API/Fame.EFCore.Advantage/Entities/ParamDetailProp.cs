﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamDetailProp.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamDetailProp definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamDetailProp</summary>*/
    public partial class ParamDetailProp
    {
        /*<summary>The get and set for Detailpropertyid</summary>*/
        public long Detailpropertyid { get; set; }
        /*<summary>The get and set for Detailid</summary>*/
        public long Detailid { get; set; }
        /*<summary>The get and set for Childcontrol</summary>*/
        public string Childcontrol { get; set; }
        /*<summary>The get and set for Propname</summary>*/
        public string Propname { get; set; }
        /*<summary>The get and set for Value</summary>*/
        public string Value { get; set; }
        /*<summary>The get and set for Valuetype</summary>*/
        public string Valuetype { get; set; }
    }
}