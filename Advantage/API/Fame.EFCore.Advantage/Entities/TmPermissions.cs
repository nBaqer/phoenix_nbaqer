﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmPermissions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmPermissions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmPermissions</summary>*/
    public partial class TmPermissions
    {
        /*<summary>The get and set for Sid</summary>*/
        public Guid Sid { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public string UserId { get; set; }
        /*<summary>The get and set for RoleId</summary>*/
        public Guid? RoleId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
    }
}