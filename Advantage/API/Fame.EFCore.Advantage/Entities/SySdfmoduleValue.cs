﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfmoduleValue.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfmoduleValue definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfmoduleValue</summary>*/
    public partial class SySdfmoduleValue
    {
        /*<summary>The get and set for Sdfpkid</summary>*/
        public Guid Sdfpkid { get; set; }
        /*<summary>The get and set for PgPkid</summary>*/
        public Guid? PgPkid { get; set; }
        /*<summary>The get and set for Sdfid</summary>*/
        public Guid? Sdfid { get; set; }
        /*<summary>The get and set for Sdfvalue</summary>*/
        public string Sdfvalue { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public string ModDate { get; set; }

        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
    }
}