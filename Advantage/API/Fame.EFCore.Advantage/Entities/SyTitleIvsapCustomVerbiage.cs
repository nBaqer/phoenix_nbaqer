﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTitleIvsapCustomVerbiage.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTitleIvsapCustomVerbiage definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTitleIvsapCustomVerbiage</summary>*/
    public partial class SyTitleIvsapCustomVerbiage
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for SapId</summary>*/
        public Guid SapId { get; set; }
        /*<summary>The get and set for TitleIvsapStatusId</summary>*/
        public int TitleIvsapStatusId { get; set; }
        /*<summary>The get and set for Message</summary>*/
        public string Message { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Sap</summary>*/
        public virtual ArSap Sap { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TitleIvsapStatus</summary>*/
        public virtual SyTitleIvsapStatus TitleIvsapStatus { get; set; }
    }
}