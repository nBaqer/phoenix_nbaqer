﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqsEffectiveDates.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqsEffectiveDates definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqsEffectiveDates</summary>*/
    public partial class AdReqsEffectiveDates
    {
        /*<summary>The get and set for AdReqEffectiveDateId</summary>*/
        public Guid AdReqEffectiveDateId { get; set; }
        /*<summary>The get and set for AdReqId</summary>*/
        public Guid? AdReqId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for MinScore</summary>*/
        public decimal? MinScore { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for MandatoryRequirement</summary>*/
        public bool? MandatoryRequirement { get; set; }
        /*<summary>The get and set for ValidDays</summary>*/
        public int? ValidDays { get; set; }

        /*<summary>The navigational property for AdReq</summary>*/
        public virtual AdReqs AdReq { get; set; }
    }
}