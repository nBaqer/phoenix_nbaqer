﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgGrp.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgGrp definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgGrp</summary>*/
    public partial class ArPrgGrp
    {
        /*<summary>The constructor for ArPrgGrp</summary>*/
        public ArPrgGrp()
        {
            AdLeads = new HashSet<AdLeads>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
        }

        /*<summary>The get and set for PrgGrpId</summary>*/
        public Guid PrgGrpId { get; set; }
        /*<summary>The get and set for PrgGrpCode</summary>*/
        public string PrgGrpCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for PrgGrpDescrip</summary>*/
        public string PrgGrpDescrip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
    }
}