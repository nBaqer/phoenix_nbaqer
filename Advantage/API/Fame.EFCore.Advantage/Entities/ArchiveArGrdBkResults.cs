﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArchiveArGrdBkResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArchiveArGrdBkResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArchiveArGrdBkResults</summary>*/
    public partial class ArchiveArGrdBkResults
    {
        /*<summary>The get and set for IdArchive</summary>*/
        public long IdArchive { get; set; }
        /*<summary>The get and set for ArchiveDate</summary>*/
        public DateTime? ArchiveDate { get; set; }
        /*<summary>The get and set for ArchiveUser</summary>*/
        public string ArchiveUser { get; set; }
        /*<summary>The get and set for GrdBkResultId</summary>*/
        public Guid? GrdBkResultId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid? ClsSectionId { get; set; }
        /*<summary>The get and set for InstrGrdBkWgtDetailId</summary>*/
        public Guid? InstrGrdBkWgtDetailId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ResNum</summary>*/
        public int? ResNum { get; set; }
        /*<summary>The get and set for PostDate</summary>*/
        public DateTime? PostDate { get; set; }
        /*<summary>The get and set for IsCompGraded</summary>*/
        public bool? IsCompGraded { get; set; }
        /*<summary>The get and set for IsCourseCredited</summary>*/
        public bool? IsCourseCredited { get; set; }
    }
}