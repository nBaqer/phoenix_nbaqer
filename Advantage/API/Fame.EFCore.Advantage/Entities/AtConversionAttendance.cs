﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AtConversionAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AtConversionAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AtConversionAttendance</summary>*/
    public partial class AtConversionAttendance
    {
        /*<summary>The get and set for AttendanceId</summary>*/
        public Guid AttendanceId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for MeetDate</summary>*/
        public DateTime MeetDate { get; set; }
        /*<summary>The get and set for Actual</summary>*/
        public decimal Actual { get; set; }
        /*<summary>The get and set for Tardy</summary>*/
        public bool Tardy { get; set; }
        /*<summary>The get and set for Excused</summary>*/
        public bool Excused { get; set; }
        /*<summary>The get and set for Schedule</summary>*/
        public decimal? Schedule { get; set; }
        /*<summary>The get and set for PostByException</summary>*/
        public string PostByException { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ConversionAttendanceId</summary>*/
        public Guid ConversionAttendanceId { get; set; }
    }
}