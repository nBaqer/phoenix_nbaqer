﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyKlassOperationType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyKlassOperationType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyKlassOperationType</summary>*/
    public partial class SyKlassOperationType
    {
        /*<summary>The constructor for SyKlassOperationType</summary>*/
        public SyKlassOperationType()
        {
            SyKlassAppConfigurationSetting = new HashSet<SyKlassAppConfigurationSetting>();
        }

        /*<summary>The get and set for KlassOperationTypeId</summary>*/
        public int KlassOperationTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for SyKlassAppConfigurationSetting</summary>*/
        public virtual ICollection<SyKlassAppConfigurationSetting>SyKlassAppConfigurationSetting { get; set; }
    }
}