﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlFee.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlFee definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlFee</summary>*/
    public partial class PlFee
    {
        /*<summary>The constructor for PlFee</summary>*/
        public PlFee()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlEmployers = new HashSet<PlEmployers>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for FeeId</summary>*/
        public Guid FeeId { get; set; }
        /*<summary>The get and set for FeeCode</summary>*/
        public string FeeCode { get; set; }
        /*<summary>The get and set for FeeDescrip</summary>*/
        public string FeeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}