﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="HrEmpHrinfo.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The HrEmpHrinfo definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for HrEmpHrinfo</summary>*/
    public partial class HrEmpHrinfo
    {
        /*<summary>The get and set for EmpHrinfoId</summary>*/
        public Guid EmpHrinfoId { get; set; }
        /*<summary>The get and set for EmpId</summary>*/
        public Guid EmpId { get; set; }
        /*<summary>The get and set for HireDate</summary>*/
        public DateTime? HireDate { get; set; }
        /*<summary>The get and set for PositionId</summary>*/
        public Guid? PositionId { get; set; }
        /*<summary>The get and set for DepartmentId</summary>*/
        public Guid? DepartmentId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for Department</summary>*/
        public virtual SyHrdepartments Department { get; set; }
        /*<summary>The navigational property for Emp</summary>*/
        public virtual HrEmployees Emp { get; set; }
        /*<summary>The navigational property for Position</summary>*/
        public virtual SyPositions Position { get; set; }
    }
}