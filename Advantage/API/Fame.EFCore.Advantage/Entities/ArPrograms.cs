﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrograms.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrograms definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrograms</summary>*/
    public partial class ArPrograms
    {
        /*<summary>The constructor for ArPrograms</summary>*/
        public ArPrograms()
        {
            AdLeads = new HashSet<AdLeads>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArSdateSetup = new HashSet<ArSdateSetup>();
            ArTerm = new HashSet<ArTerm>();
            SyStateBoardProgramCourseMappings = new HashSet<SyStateBoardProgramCourseMappings>();
        }

        /*<summary>The get and set for ProgId</summary>*/
        public Guid ProgId { get; set; }
        /*<summary>The get and set for ProgCode</summary>*/
        public string ProgCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ProgDescrip</summary>*/
        public string ProgDescrip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for NumPmtPeriods</summary>*/
        public byte? NumPmtPeriods { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for DegreeId</summary>*/
        public Guid? DegreeId { get; set; }
        /*<summary>The get and set for CalendarTypeId</summary>*/
        public int? CalendarTypeId { get; set; }
        /*<summary>The get and set for Acid</summary>*/
        public int? Acid { get; set; }
        /*<summary>The get and set for Shiftid</summary>*/
        public Guid? Shiftid { get; set; }
        /*<summary>The get and set for Cipcode</summary>*/
        public string Cipcode { get; set; }
        /*<summary>The get and set for CredentialLevel</summary>*/
        public string CredentialLevel { get; set; }
        /*<summary>The get and set for IsGeprogram</summary>*/
        public bool? IsGeprogram { get; set; }
        /*<summary>The get and set for CredentialLvlId</summary>*/
        public Guid? CredentialLvlId { get; set; }
        /*<summary>The get and set for Is1098T</summary>*/
        public bool? Is1098T { get; set; }

        /*<summary>The navigational property for Ac</summary>*/
        public virtual SyAcademicCalendars Ac { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for CredentialLvl</summary>*/
        public virtual ArProgCredential CredentialLvl { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArSdateSetup</summary>*/
        public virtual ICollection<ArSdateSetup>ArSdateSetup { get; set; }
        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
        /*<summary>The navigational property for SyStateBoardProgramCourseMappings</summary>*/
        public virtual ICollection<SyStateBoardProgramCourseMappings>SyStateBoardProgramCourseMappings { get; set; }
    }
}