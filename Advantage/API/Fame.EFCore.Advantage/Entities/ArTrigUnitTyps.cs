﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTrigUnitTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTrigUnitTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTrigUnitTyps</summary>*/
    public partial class ArTrigUnitTyps
    {
        /*<summary>The constructor for ArTrigUnitTyps</summary>*/
        public ArTrigUnitTyps()
        {
            ArSap = new HashSet<ArSap>();
            ArSapdetails = new HashSet<ArSapdetails>();
        }

        /*<summary>The get and set for TrigUnitTypId</summary>*/
        public byte TrigUnitTypId { get; set; }
        /*<summary>The get and set for TrigUnitTypDescrip</summary>*/
        public string TrigUnitTypDescrip { get; set; }

        /*<summary>The navigational property for ArSap</summary>*/
        public virtual ICollection<ArSap>ArSap { get; set; }
        /*<summary>The navigational property for ArSapdetails</summary>*/
        public virtual ICollection<ArSapdetails>ArSapdetails { get; set; }
    }
}