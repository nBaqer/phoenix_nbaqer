﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdQuickLeadMap.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdQuickLeadMap definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdQuickLeadMap</summary>*/
    public partial class AdQuickLeadMap
    {
        /*<summary>The get and set for MapId</summary>*/
        public long MapId { get; set; }
        /*<summary>The get and set for FldName</summary>*/
        public string FldName { get; set; }
        /*<summary>The get and set for CtrlIdName</summary>*/
        public string CtrlIdName { get; set; }
        /*<summary>The get and set for PropName</summary>*/
        public string PropName { get; set; }
        /*<summary>The get and set for ParentCtrlId</summary>*/
        public string ParentCtrlId { get; set; }
        /*<summary>The get and set for Sequence</summary>*/
        public long? Sequence { get; set; }
    }
}