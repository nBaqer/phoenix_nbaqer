﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTuitionCategories.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTuitionCategories definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTuitionCategories</summary>*/
    public partial class SaTuitionCategories
    {
        /*<summary>The constructor for SaTuitionCategories</summary>*/
        public SaTuitionCategories()
        {
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            SaCourseFees = new HashSet<SaCourseFees>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
            SaRateScheduleDetails = new HashSet<SaRateScheduleDetails>();
        }

        /*<summary>The get and set for TuitionCategoryId</summary>*/
        public Guid TuitionCategoryId { get; set; }
        /*<summary>The get and set for TuitionCategoryCode</summary>*/
        public string TuitionCategoryCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TuitionCategoryDescrip</summary>*/
        public string TuitionCategoryDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for SaCourseFees</summary>*/
        public virtual ICollection<SaCourseFees>SaCourseFees { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
        /*<summary>The navigational property for SaRateScheduleDetails</summary>*/
        public virtual ICollection<SaRateScheduleDetails>SaRateScheduleDetails { get; set; }
    }
}