﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadOtherContacts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadOtherContacts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadOtherContacts</summary>*/
    public partial class AdLeadOtherContacts
    {
        /*<summary>The constructor for AdLeadOtherContacts</summary>*/
        public AdLeadOtherContacts()
        {
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeadOtherContactsEmail = new HashSet<AdLeadOtherContactsEmail>();
            AdLeadOtherContactsPhone = new HashSet<AdLeadOtherContactsPhone>();
        }

        /*<summary>The get and set for OtherContactId</summary>*/
        public Guid OtherContactId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for RelationshipId</summary>*/
        public Guid RelationshipId { get; set; }
        /*<summary>The get and set for ContactTypeId</summary>*/
        public Guid ContactTypeId { get; set; }
        /*<summary>The get and set for PrefixId</summary>*/
        public Guid? PrefixId { get; set; }
        /*<summary>The get and set for SufixId</summary>*/
        public Guid? SufixId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for ContactType</summary>*/
        public virtual SyContactTypes ContactType { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Prefix</summary>*/
        public virtual SyPrefixes Prefix { get; set; }
        /*<summary>The navigational property for Relationship</summary>*/
        public virtual SyRelations Relationship { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Sufix</summary>*/
        public virtual SySuffixes Sufix { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsEmail</summary>*/
        public virtual ICollection<AdLeadOtherContactsEmail>AdLeadOtherContactsEmail { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsPhone</summary>*/
        public virtual ICollection<AdLeadOtherContactsPhone>AdLeadOtherContactsPhone { get; set; }
    }
}