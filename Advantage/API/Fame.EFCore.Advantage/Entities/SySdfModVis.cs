﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfModVis.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfModVis definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfModVis</summary>*/
    public partial class SySdfModVis
    {
        /*<summary>The get and set for SdfModVisId</summary>*/
        public Guid SdfModVisId { get; set; }
        /*<summary>The get and set for Sdfid</summary>*/
        public Guid Sdfid { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte ModuleId { get; set; }
        /*<summary>The get and set for VisId</summary>*/
        public byte VisId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
    }
}