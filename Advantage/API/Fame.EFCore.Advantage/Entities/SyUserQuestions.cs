﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserQuestions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserQuestions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserQuestions</summary>*/
    public partial class SyUserQuestions
    {
        /*<summary>The constructor for SyUserQuestions</summary>*/
        public SyUserQuestions()
        {
            SyUserSpecQuestions = new HashSet<SyUserSpecQuestions>();
        }

        /*<summary>The get and set for UserQuestionId</summary>*/
        public Guid UserQuestionId { get; set; }
        /*<summary>The get and set for UserQuestionDescrip</summary>*/
        public string UserQuestionDescrip { get; set; }

        /*<summary>The navigational property for SyUserSpecQuestions</summary>*/
        public virtual ICollection<SyUserSpecQuestions>SyUserSpecQuestions { get; set; }
    }
}