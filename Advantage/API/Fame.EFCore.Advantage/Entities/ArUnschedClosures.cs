﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArUnschedClosures.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArUnschedClosures definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArUnschedClosures</summary>*/
    public partial class ArUnschedClosures
    {
        /*<summary>The get and set for UnschedClosureId</summary>*/
        public Guid UnschedClosureId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime EndDate { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ClsMeetingId</summary>*/
        public Guid? ClsMeetingId { get; set; }
        /*<summary>The get and set for RescheduledMeetingId</summary>*/
        public Guid? RescheduledMeetingId { get; set; }
        /*<summary>The get and set for ClassReSchduledFrom</summary>*/
        public DateTime? ClassReSchduledFrom { get; set; }
        /*<summary>The get and set for ClassReSchduledTo</summary>*/
        public DateTime? ClassReSchduledTo { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
    }
}