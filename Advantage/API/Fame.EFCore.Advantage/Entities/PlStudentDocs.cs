﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlStudentDocs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlStudentDocs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlStudentDocs</summary>*/
    public partial class PlStudentDocs
    {
        /*<summary>The constructor for PlStudentDocs</summary>*/
        public PlStudentDocs()
        {
            DocumentFile = new HashSet<DocumentFile>();
        }

        /*<summary>The get and set for StudentDocId</summary>*/
        public Guid StudentDocId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for DocumentId</summary>*/
        public Guid DocumentId { get; set; }
        /*<summary>The get and set for DocStatusId</summary>*/
        public Guid DocStatusId { get; set; }
        /*<summary>The get and set for RequestDate</summary>*/
        public DateTime? RequestDate { get; set; }
        /*<summary>The get and set for ReceiveDate</summary>*/
        public DateTime? ReceiveDate { get; set; }
        /*<summary>The get and set for ImgData</summary>*/
        public byte[] ImgData { get; set; }
        /*<summary>The get and set for ImgContenttype</summary>*/
        public string ImgContenttype { get; set; }
        /*<summary>The get and set for ScannedId</summary>*/
        public int? ScannedId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte? ModuleId { get; set; }

        /*<summary>The navigational property for DocStatus</summary>*/
        public virtual SyDocStatuses DocStatus { get; set; }
        /*<summary>The navigational property for Document</summary>*/
        public virtual AdReqs Document { get; set; }
        /*<summary>The navigational property for Module</summary>*/
        public virtual SyModules Module { get; set; }
        /*<summary>The navigational property for DocumentFile</summary>*/
        public virtual ICollection<DocumentFile>DocumentFile { get; set; }
    }
}