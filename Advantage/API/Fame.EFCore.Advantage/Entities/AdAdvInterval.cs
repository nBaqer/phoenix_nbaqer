﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdAdvInterval.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdAdvInterval definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdAdvInterval</summary>*/
    public partial class AdAdvInterval
    {
        /*<summary>The constructor for AdAdvInterval</summary>*/
        public AdAdvInterval()
        {
            AdSourceAdvertisement = new HashSet<AdSourceAdvertisement>();
        }

        /*<summary>The get and set for AdvIntervalId</summary>*/
        public Guid AdvIntervalId { get; set; }
        /*<summary>The get and set for AdvIntervalDescrip</summary>*/
        public string AdvIntervalDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for AdvIntervalCode</summary>*/
        public string AdvIntervalCode { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdSourceAdvertisement</summary>*/
        public virtual ICollection<AdSourceAdvertisement>AdSourceAdvertisement { get; set; }
    }
}