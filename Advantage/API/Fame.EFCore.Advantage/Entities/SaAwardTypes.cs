﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaAwardTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaAwardTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaAwardTypes</summary>*/
    public partial class SaAwardTypes
    {
        /*<summary>The constructor for SaAwardTypes</summary>*/
        public SaAwardTypes()
        {
            SaFundSources = new HashSet<SaFundSources>();
        }

        /*<summary>The get and set for AwardTypeId</summary>*/
        public int AwardTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for SaFundSources</summary>*/
        public virtual ICollection<SaFundSources>SaFundSources { get; set; }
    }
}