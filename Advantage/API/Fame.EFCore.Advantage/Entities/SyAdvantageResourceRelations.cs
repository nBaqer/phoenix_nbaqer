﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAdvantageResourceRelations.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAdvantageResourceRelations definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAdvantageResourceRelations</summary>*/
    public partial class SyAdvantageResourceRelations
    {
        /*<summary>The get and set for ResRelId</summary>*/
        public int ResRelId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for RelatedResourceId</summary>*/
        public short? RelatedResourceId { get; set; }

        /*<summary>The navigational property for RelatedResource</summary>*/
        public virtual SyResources RelatedResource { get; set; }
        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
    }
}