﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyModules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyModules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyModules</summary>*/
    public partial class SyModules
    {
        /*<summary>The constructor for SyModules</summary>*/
        public SyModules()
        {
            AllNotes = new HashSet<AllNotes>();
            PlStudentDocs = new HashSet<PlStudentDocs>();
            SyModuleDef = new HashSet<SyModuleDef>();
        }

        /*<summary>The get and set for ModuleId</summary>*/
        public byte ModuleId { get; set; }
        /*<summary>The get and set for ModuleCode</summary>*/
        public string ModuleCode { get; set; }
        /*<summary>The get and set for ModuleName</summary>*/
        public string ModuleName { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for AllowSdf</summary>*/
        public bool AllowSdf { get; set; }

        /*<summary>The navigational property for AllNotes</summary>*/
        public virtual ICollection<AllNotes>AllNotes { get; set; }
        /*<summary>The navigational property for PlStudentDocs</summary>*/
        public virtual ICollection<PlStudentDocs>PlStudentDocs { get; set; }
        /*<summary>The navigational property for SyModuleDef</summary>*/
        public virtual ICollection<SyModuleDef>SyModuleDef { get; set; }
    }
}