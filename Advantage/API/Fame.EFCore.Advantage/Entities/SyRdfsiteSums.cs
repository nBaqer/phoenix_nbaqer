﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRdfsiteSums.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRdfsiteSums definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRdfsiteSums</summary>*/
    public partial class SyRdfsiteSums
    {
        /*<summary>The get and set for RdfsiteSumId</summary>*/
        public Guid RdfsiteSumId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int ModuleId { get; set; }
        /*<summary>The get and set for RdfsiteSumCode</summary>*/
        public string RdfsiteSumCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for RdfsiteSumDescrip</summary>*/
        public string RdfsiteSumDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}