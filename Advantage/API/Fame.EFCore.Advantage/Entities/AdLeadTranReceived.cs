﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadTranReceived.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadTranReceived definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadTranReceived</summary>*/
    public partial class AdLeadTranReceived
    {
        /*<summary>The get and set for AdLeadTranReceivedId</summary>*/
        public Guid AdLeadTranReceivedId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for DocumentId</summary>*/
        public Guid DocumentId { get; set; }
        /*<summary>The get and set for ReceivedDate</summary>*/
        public DateTime ReceivedDate { get; set; }
        /*<summary>The get and set for IsApproved</summary>*/
        public bool IsApproved { get; set; }
        /*<summary>The get and set for ApprovalDate</summary>*/
        public DateTime? ApprovalDate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Override</summary>*/
        public bool Override { get; set; }
        /*<summary>The get and set for OverrideReason</summary>*/
        public string OverrideReason { get; set; }

        /*<summary>The navigational property for Document</summary>*/
        public virtual AdReqs Document { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}