﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaStudentAwardSchedule.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaStudentAwardSchedule definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaStudentAwardSchedule</summary>*/
    public partial class FaStudentAwardSchedule
    {
        /*<summary>The constructor for FaStudentAwardSchedule</summary>*/
        public FaStudentAwardSchedule()
        {
            SaPmtDisbRel = new HashSet<SaPmtDisbRel>();
        }

        /*<summary>The get and set for AwardScheduleId</summary>*/
        public Guid AwardScheduleId { get; set; }
        /*<summary>The get and set for StudentAwardId</summary>*/
        public Guid StudentAwardId { get; set; }
        /*<summary>The get and set for ExpectedDate</summary>*/
        public DateTime? ExpectedDate { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal? Amount { get; set; }
        /*<summary>The get and set for Reference</summary>*/
        public string Reference { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid? TransactionId { get; set; }
        /*<summary>The get and set for TermNumber</summary>*/
        public string TermNumber { get; set; }
        /*<summary>The get and set for Recid</summary>*/
        public string Recid { get; set; }
        /*<summary>The get and set for IsProcessed</summary>*/
        public bool? IsProcessed { get; set; }
        /*<summary>The get and set for DisbursementNumber</summary>*/
        public byte? DisbursementNumber { get; set; }
        /*<summary>The get and set for SequenceNumber</summary>*/
        public byte? SequenceNumber { get; set; }
        /*<summary>The get and set for DisbursementStatus</summary>*/
        public string DisbursementStatus { get; set; }
        /*<summary>The get and set for GrossAmount</summary>*/
        public decimal? GrossAmount { get; set; }
        /*<summary>The get and set for LoanFees</summary>*/
        public decimal? LoanFees { get; set; }

        /*<summary>The navigational property for StudentAward</summary>*/
        public virtual FaStudentAwards StudentAward { get; set; }
        /*<summary>The navigational property for SaPmtDisbRel</summary>*/
        public virtual ICollection<SaPmtDisbRel>SaPmtDisbRel { get; set; }
    }
}