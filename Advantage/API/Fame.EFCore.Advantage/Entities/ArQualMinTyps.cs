﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArQualMinTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArQualMinTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArQualMinTyps</summary>*/
    public partial class ArQualMinTyps
    {
        /*<summary>The get and set for QualMinTypId</summary>*/
        public byte QualMinTypId { get; set; }
        /*<summary>The get and set for QualMinTypDesc</summary>*/
        public string QualMinTypDesc { get; set; }
    }
}