﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqTypes</summary>*/
    public partial class AdReqTypes
    {
        /*<summary>The constructor for AdReqTypes</summary>*/
        public AdReqTypes()
        {
            AdReqs = new HashSet<AdReqs>();
        }

        /*<summary>The get and set for AdReqTypeId</summary>*/
        public int AdReqTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for AdReqs</summary>*/
        public virtual ICollection<AdReqs>AdReqs { get; set; }
    }
}