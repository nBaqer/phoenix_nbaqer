﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AfaCatalogMapping.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AfaCatalogMapping definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AfaCatalogMapping</summary>*/
    public partial class AfaCatalogMapping
    {
        /*<summary>The constructor for AfaCatalogMapping</summary>*/
        public AfaCatalogMapping()
        {
            AdCitizenships = new HashSet<AdCitizenships>();
            AdGenders = new HashSet<AdGenders>();
            AdMaritalStatus = new HashSet<AdMaritalStatus>();
        }

        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for AfaCode</summary>*/
        public string AfaCode { get; set; }
        /*<summary>The get and set for TableId</summary>*/
        public int TableId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for Username</summary>*/
        public string Username { get; set; }

        /*<summary>The navigational property for Table</summary>*/
        public virtual SyTables Table { get; set; }
        /*<summary>The navigational property for AdCitizenships</summary>*/
        public virtual ICollection<AdCitizenships>AdCitizenships { get; set; }
        /*<summary>The navigational property for AdGenders</summary>*/
        public virtual ICollection<AdGenders>AdGenders { get; set; }
        /*<summary>The navigational property for AdMaritalStatus</summary>*/
        public virtual ICollection<AdMaritalStatus>AdMaritalStatus { get; set; }
    }
}