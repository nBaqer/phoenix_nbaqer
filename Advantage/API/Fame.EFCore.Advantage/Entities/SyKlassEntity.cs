﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyKlassEntity.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyKlassEntity definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyKlassEntity</summary>*/
    public partial class SyKlassEntity
    {
        /*<summary>The constructor for SyKlassEntity</summary>*/
        public SyKlassEntity()
        {
            SyKlassAppConfigurationSetting = new HashSet<SyKlassAppConfigurationSetting>();
        }

        /*<summary>The get and set for KlassEntityId</summary>*/
        public int KlassEntityId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for SyKlassAppConfigurationSetting</summary>*/
        public virtual ICollection<SyKlassAppConfigurationSetting>SyKlassAppConfigurationSetting { get; set; }
    }
}