﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStudentSchedules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStudentSchedules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStudentSchedules</summary>*/
    public partial class ArStudentSchedules
    {
        /*<summary>The get and set for StuScheduleId</summary>*/
        public Guid StuScheduleId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid ScheduleId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public bool? Active { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Source</summary>*/
        public string Source { get; set; }

        /*<summary>The navigational property for Schedule</summary>*/
        public virtual ArProgSchedules Schedule { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}