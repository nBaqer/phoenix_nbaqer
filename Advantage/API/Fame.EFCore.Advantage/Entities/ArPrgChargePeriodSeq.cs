﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgChargePeriodSeq.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgChargePeriodSeq definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgChargePeriodSeq</summary>*/
    public partial class ArPrgChargePeriodSeq
    {
        /*<summary>The constructor for ArPrgChargePeriodSeq</summary>*/
        public ArPrgChargePeriodSeq()
        {
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for PrgChrPeriodSeqId</summary>*/
        public Guid PrgChrPeriodSeqId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for FeeLevelId</summary>*/
        public byte FeeLevelId { get; set; }
        /*<summary>The get and set for Sequence</summary>*/
        public short Sequence { get; set; }
        /*<summary>The get and set for StartValue</summary>*/
        public decimal StartValue { get; set; }
        /*<summary>The get and set for Endvalue</summary>*/
        public decimal Endvalue { get; set; }

        /*<summary>The navigational property for FeeLevel</summary>*/
        public virtual SaFeeLevels FeeLevel { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}