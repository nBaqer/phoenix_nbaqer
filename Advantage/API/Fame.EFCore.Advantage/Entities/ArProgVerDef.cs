﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgVerDef.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgVerDef definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgVerDef</summary>*/
    public partial class ArProgVerDef
    {
        /*<summary>The constructor for ArProgVerDef</summary>*/
        public ArProgVerDef()
        {
            ArClassSections = new HashSet<ArClassSections>();
        }

        /*<summary>The get and set for ProgVerDefId</summary>*/
        public Guid ProgVerDefId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for ReqSeq</summary>*/
        public int ReqSeq { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool IsRequired { get; set; }
        /*<summary>The get and set for Cnt</summary>*/
        public short? Cnt { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public decimal? Hours { get; set; }
        /*<summary>The get and set for TrkForCompletion</summary>*/
        public bool? TrkForCompletion { get; set; }
        /*<summary>The get and set for Credits</summary>*/
        public decimal? Credits { get; set; }
        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid? GrdSysDetailId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for TermNo</summary>*/
        public int? TermNo { get; set; }
        /*<summary>The get and set for CourseWeight</summary>*/
        public double CourseWeight { get; set; }

        /*<summary>The navigational property for GrdSysDetail</summary>*/
        public virtual ArGradeSystemDetails GrdSysDetail { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
    }
}