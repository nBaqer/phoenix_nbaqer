﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyNaccasdropReasonsMapping.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyNaccasdropReasonsMapping definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyNaccasdropReasonsMapping</summary>*/
    public partial class SyNaccasdropReasonsMapping
    {
        /*<summary>The get and set for NaccasdropReasonId</summary>*/
        public Guid? NaccasdropReasonId { get; set; }
        /*<summary>The get and set for AdvdropReasonId</summary>*/
        public Guid? AdvdropReasonId { get; set; }
        /*<summary>The get and set for NaccasSettingId</summary>*/
        public int NaccasSettingId { get; set; }
        /*<summary>The get and set for DropReasonMappingId</summary>*/
        public Guid DropReasonMappingId { get; set; }

        /*<summary>The navigational property for AdvdropReason</summary>*/
        public virtual ArDropReasons AdvdropReason { get; set; }
        /*<summary>The navigational property for NaccasSetting</summary>*/
        public virtual SyNaccasSettings NaccasSetting { get; set; }
        /*<summary>The navigational property for NaccasdropReason</summary>*/
        public virtual SyNaccasdropReasons NaccasdropReason { get; set; }
    }
}