﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlEmployerJobs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlEmployerJobs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlEmployerJobs</summary>*/
    public partial class PlEmployerJobs
    {
        /*<summary>The constructor for PlEmployerJobs</summary>*/
        public PlEmployerJobs()
        {
            PlJobWorkDays = new HashSet<PlJobWorkDays>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for EmployerJobId</summary>*/
        public Guid EmployerJobId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for EmployerJobTitle</summary>*/
        public string EmployerJobTitle { get; set; }
        /*<summary>The get and set for JobTitleId</summary>*/
        public Guid JobTitleId { get; set; }
        /*<summary>The get and set for JobDescription</summary>*/
        public string JobDescription { get; set; }
        /*<summary>The get and set for JobGroupId</summary>*/
        public Guid JobGroupId { get; set; }
        /*<summary>The get and set for ContactId</summary>*/
        public Guid? ContactId { get; set; }
        /*<summary>The get and set for TypeId</summary>*/
        public Guid? TypeId { get; set; }
        /*<summary>The get and set for AreaId</summary>*/
        public Guid? AreaId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for WorkDays</summary>*/
        public string WorkDays { get; set; }
        /*<summary>The get and set for FeeId</summary>*/
        public Guid? FeeId { get; set; }
        /*<summary>The get and set for NumberOpen</summary>*/
        public int? NumberOpen { get; set; }
        /*<summary>The get and set for NumberFilled</summary>*/
        public int? NumberFilled { get; set; }
        /*<summary>The get and set for OpenedFrom</summary>*/
        public DateTime? OpenedFrom { get; set; }
        /*<summary>The get and set for OpenedTo</summary>*/
        public DateTime? OpenedTo { get; set; }
        /*<summary>The get and set for Start</summary>*/
        public DateTime? Start { get; set; }
        /*<summary>The get and set for SalaryFrom</summary>*/
        public string SalaryFrom { get; set; }
        /*<summary>The get and set for SalaryTo</summary>*/
        public string SalaryTo { get; set; }
        /*<summary>The get and set for BenefitsId</summary>*/
        public Guid? BenefitsId { get; set; }
        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid? ScheduleId { get; set; }
        /*<summary>The get and set for HoursFrom</summary>*/
        public string HoursFrom { get; set; }
        /*<summary>The get and set for HoursTo</summary>*/
        public string HoursTo { get; set; }
        /*<summary>The get and set for Notes</summary>*/
        public string Notes { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for EmployerId</summary>*/
        public Guid? EmployerId { get; set; }
        /*<summary>The get and set for JobRequirements</summary>*/
        public string JobRequirements { get; set; }
        /*<summary>The get and set for SalaryTypeId</summary>*/
        public Guid? SalaryTypeId { get; set; }
        /*<summary>The get and set for JobPostedDate</summary>*/
        public DateTime? JobPostedDate { get; set; }
        /*<summary>The get and set for ExpertiseId</summary>*/
        public Guid? ExpertiseId { get; set; }

        /*<summary>The navigational property for Area</summary>*/
        public virtual AdCounties Area { get; set; }
        /*<summary>The navigational property for Benefits</summary>*/
        public virtual PlJobBenefit Benefits { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Contact</summary>*/
        public virtual PlEmployerContact Contact { get; set; }
        /*<summary>The navigational property for Employer</summary>*/
        public virtual PlEmployers Employer { get; set; }
        /*<summary>The navigational property for Expertise</summary>*/
        public virtual AdExpertiseLevel Expertise { get; set; }
        /*<summary>The navigational property for Fee</summary>*/
        public virtual PlFee Fee { get; set; }
        /*<summary>The navigational property for JobGroup</summary>*/
        public virtual PlJobCats JobGroup { get; set; }
        /*<summary>The navigational property for JobTitle</summary>*/
        public virtual AdTitles JobTitle { get; set; }
        /*<summary>The navigational property for SalaryType</summary>*/
        public virtual PlSalaryType SalaryType { get; set; }
        /*<summary>The navigational property for Schedule</summary>*/
        public virtual PlJobSchedule Schedule { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Type</summary>*/
        public virtual PlJobType Type { get; set; }
        /*<summary>The navigational property for PlJobWorkDays</summary>*/
        public virtual ICollection<PlJobWorkDays>PlJobWorkDays { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}