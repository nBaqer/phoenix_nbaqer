﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlTransportation.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlTransportation definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlTransportation</summary>*/
    public partial class PlTransportation
    {
        /*<summary>The constructor for PlTransportation</summary>*/
        public PlTransportation()
        {
            PlExitInterview = new HashSet<PlExitInterview>();
        }

        /*<summary>The get and set for TransportationId</summary>*/
        public Guid TransportationId { get; set; }
        /*<summary>The get and set for TransportationCode</summary>*/
        public string TransportationCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for TransportationDescrip</summary>*/
        public string TransportationDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
    }
}