﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAgencies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAgencies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAgencies</summary>*/
    public partial class SyRptAgencies
    {
        /*<summary>The constructor for SyRptAgencies</summary>*/
        public SyRptAgencies()
        {
            SyRptAgencyFields = new HashSet<SyRptAgencyFields>();
        }

        /*<summary>The get and set for RptAgencyId</summary>*/
        public int RptAgencyId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for SyRptAgencyFields</summary>*/
        public virtual ICollection<SyRptAgencyFields>SyRptAgencyFields { get; set; }
    }
}