﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdMaritalStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdMaritalStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdMaritalStatus</summary>*/
    public partial class AdMaritalStatus
    {
        /*<summary>The constructor for AdMaritalStatus</summary>*/
        public AdMaritalStatus()
        {
            AdLeads = new HashSet<AdLeads>();
            HrEmployees = new HashSet<HrEmployees>();
        }

        /*<summary>The get and set for MaritalStatId</summary>*/
        public Guid MaritalStatId { get; set; }
        /*<summary>The get and set for MaritalStatCode</summary>*/
        public string MaritalStatCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for MaritalStatDescrip</summary>*/
        public string MaritalStatDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for AfaMappingId</summary>*/
        public Guid? AfaMappingId { get; set; }

        /*<summary>The navigational property for AfaMapping</summary>*/
        public virtual AfaCatalogMapping AfaMapping { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
    }
}