﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArDegrees.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArDegrees definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArDegrees</summary>*/
    public partial class ArDegrees
    {
        /*<summary>The constructor for ArDegrees</summary>*/
        public ArDegrees()
        {
            AdLeadEducation = new HashSet<AdLeadEducation>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
            HrEmpDegrees = new HashSet<HrEmpDegrees>();
        }

        /*<summary>The get and set for DegreeId</summary>*/
        public Guid DegreeId { get; set; }
        /*<summary>The get and set for DegreeCode</summary>*/
        public string DegreeCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for DegreeDescrip</summary>*/
        public string DegreeDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for DegreeTypeId</summary>*/
        public Guid? DegreeTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadEducation</summary>*/
        public virtual ICollection<AdLeadEducation>AdLeadEducation { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for HrEmpDegrees</summary>*/
        public virtual ICollection<HrEmpDegrees>HrEmpDegrees { get; set; }
    }
}