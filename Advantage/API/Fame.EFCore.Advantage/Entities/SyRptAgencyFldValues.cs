﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAgencyFldValues.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAgencyFldValues definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAgencyFldValues</summary>*/
    public partial class SyRptAgencyFldValues
    {
        /*<summary>The constructor for SyRptAgencyFldValues</summary>*/
        public SyRptAgencyFldValues()
        {
            SyRptAgencySchoolMapping = new HashSet<SyRptAgencySchoolMapping>();
        }

        /*<summary>The get and set for RptAgencyFldValId</summary>*/
        public int RptAgencyFldValId { get; set; }
        /*<summary>The get and set for RptAgencyFldId</summary>*/
        public int? RptAgencyFldId { get; set; }
        /*<summary>The get and set for AgencyDescrip</summary>*/
        public string AgencyDescrip { get; set; }
        /*<summary>The get and set for AgencyValue</summary>*/
        public string AgencyValue { get; set; }

        /*<summary>The navigational property for RptAgencyFld</summary>*/
        public virtual SyRptAgencyFields RptAgencyFld { get; set; }
        /*<summary>The navigational property for SyRptAgencySchoolMapping</summary>*/
        public virtual ICollection<SyRptAgencySchoolMapping>SyRptAgencySchoolMapping { get; set; }
    }
}