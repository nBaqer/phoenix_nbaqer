﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCreditSummary.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCreditSummary definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCreditSummary</summary>*/
    public partial class SyCreditSummary
    {
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for TermDescrip</summary>*/
        public string TermDescrip { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid? ReqId { get; set; }
        /*<summary>The get and set for ReqDescrip</summary>*/
        public string ReqDescrip { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public string ClsSectionId { get; set; }
        /*<summary>The get and set for CreditsEarned</summary>*/
        public decimal? CreditsEarned { get; set; }
        /*<summary>The get and set for CreditsAttempted</summary>*/
        public decimal? CreditsAttempted { get; set; }
        /*<summary>The get and set for CurrentScore</summary>*/
        public decimal? CurrentScore { get; set; }
        /*<summary>The get and set for CurrentGrade</summary>*/
        public string CurrentGrade { get; set; }
        /*<summary>The get and set for FinalScore</summary>*/
        public decimal? FinalScore { get; set; }
        /*<summary>The get and set for FinalGrade</summary>*/
        public string FinalGrade { get; set; }
        /*<summary>The get and set for Completed</summary>*/
        public bool? Completed { get; set; }
        /*<summary>The get and set for FinalGpa</summary>*/
        public decimal? FinalGpa { get; set; }
        /*<summary>The get and set for ProductWeightedAverageCreditsGpa</summary>*/
        public decimal? ProductWeightedAverageCreditsGpa { get; set; }
        /*<summary>The get and set for CountWeightedAverageCredits</summary>*/
        public decimal? CountWeightedAverageCredits { get; set; }
        /*<summary>The get and set for ProductSimpleAverageCreditsGpa</summary>*/
        public decimal? ProductSimpleAverageCreditsGpa { get; set; }
        /*<summary>The get and set for CountSimpleAverageCredits</summary>*/
        public decimal? CountSimpleAverageCredits { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TermGpaSimple</summary>*/
        public decimal? TermGpaSimple { get; set; }
        /*<summary>The get and set for TermGpaWeighted</summary>*/
        public decimal? TermGpaWeighted { get; set; }
        /*<summary>The get and set for Coursecredits</summary>*/
        public decimal? Coursecredits { get; set; }
        /*<summary>The get and set for CumulativeGpa</summary>*/
        public decimal? CumulativeGpa { get; set; }
        /*<summary>The get and set for CumulativeGpaSimple</summary>*/
        public decimal? CumulativeGpaSimple { get; set; }
        /*<summary>The get and set for FacreditsEarned</summary>*/
        public decimal? FacreditsEarned { get; set; }
        /*<summary>The get and set for Average</summary>*/
        public decimal? Average { get; set; }
        /*<summary>The get and set for CumAverage</summary>*/
        public decimal? CumAverage { get; set; }
        /*<summary>The get and set for TermStartDate</summary>*/
        public DateTime? TermStartDate { get; set; }
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
    }
}