﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStatuses</summary>*/
    public partial class SyStatuses
    {
        /*<summary>The constructor for SyStatuses</summary>*/
        public SyStatuses()
        {
            AdAdvInterval = new HashSet<AdAdvInterval>();
            AdAgencySponsors = new HashSet<AdAgencySponsors>();
            AdCitizenships = new HashSet<AdCitizenships>();
            AdColleges = new HashSet<AdColleges>();
            AdCounties = new HashSet<AdCounties>();
            AdCountries = new HashSet<AdCountries>();
            AdEdLvls = new HashSet<AdEdLvls>();
            AdEthCodes = new HashSet<AdEthCodes>();
            AdExpertiseLevel = new HashSet<AdExpertiseLevel>();
            AdExtraCurr = new HashSet<AdExtraCurr>();
            AdExtraCurrGrp = new HashSet<AdExtraCurrGrp>();
            AdFullPartTime = new HashSet<AdFullPartTime>();
            AdGenders = new HashSet<AdGenders>();
            AdHighSchools = new HashSet<AdHighSchools>();
            AdImportLeadsMappings = new HashSet<AdImportLeadsMappings>();
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadEducation = new HashSet<AdLeadEducation>();
            AdLeadEmail = new HashSet<AdLeadEmail>();
            AdLeadGroups = new HashSet<AdLeadGroups>();
            AdLeadGrpReqGroups = new HashSet<AdLeadGrpReqGroups>();
            AdLeadNotes1 = new HashSet<AdLeadNotes1>();
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeadOtherContactsEmail = new HashSet<AdLeadOtherContactsEmail>();
            AdLeadOtherContactsPhone = new HashSet<AdLeadOtherContactsPhone>();
            AdLeadPhone = new HashSet<AdLeadPhone>();
            AdLeadsAddressStatusNavigation = new HashSet<AdLeads>();
            AdLeadsPhoneStatusNavigation = new HashSet<AdLeads>();
            AdLeadsStudentStatus = new HashSet<AdLeads>();
            AdLevel = new HashSet<AdLevel>();
            AdMaritalStatus = new HashSet<AdMaritalStatus>();
            AdNationalities = new HashSet<AdNationalities>();
            AdReqGroups = new HashSet<AdReqGroups>();
            AdReqs = new HashSet<AdReqs>();
            AdSourceAdvertisement = new HashSet<AdSourceAdvertisement>();
            AdSourceCatagory = new HashSet<AdSourceCatagory>();
            AdSourceType = new HashSet<AdSourceType>();
            AdTitles = new HashSet<AdTitles>();
            ArBkCategories = new HashSet<ArBkCategories>();
            ArBooks = new HashSet<ArBooks>();
            ArBuildings = new HashSet<ArBuildings>();
            ArCertifications = new HashSet<ArCertifications>();
            ArCollegeDivisions = new HashSet<ArCollegeDivisions>();
            ArCourseCategories = new HashSet<ArCourseCategories>();
            ArDegrees = new HashSet<ArDegrees>();
            ArDepartments = new HashSet<ArDepartments>();
            ArDropReasons = new HashSet<ArDropReasons>();
            ArFasapchkResults = new HashSet<ArFasapchkResults>();
            ArFerpacategory = new HashSet<ArFerpacategory>();
            ArFerpaentity = new HashSet<ArFerpaentity>();
            ArGradeScales = new HashSet<ArGradeScales>();
            ArGradeSystems = new HashSet<ArGradeSystems>();
            ArGrdBkEvalTyps = new HashSet<ArGrdBkEvalTyps>();
            ArGrdBkWeights = new HashSet<ArGrdBkWeights>();
            ArGrdComponentTypes = new HashSet<ArGrdComponentTypes>();
            ArLoareasons = new HashSet<ArLoareasons>();
            ArPeriod = new HashSet<ArPeriod>();
            ArPrgGrp = new HashSet<ArPrgGrp>();
            ArPrgVersionFees = new HashSet<ArPrgVersionFees>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArProgCredential = new HashSet<ArProgCredential>();
            ArProgTypes = new HashSet<ArProgTypes>();
            ArPrograms = new HashSet<ArPrograms>();
            ArReqTypes = new HashSet<ArReqTypes>();
            ArReqs = new HashSet<ArReqs>();
            ArRooms = new HashSet<ArRooms>();
            ArSap = new HashSet<ArSap>();
            ArSdateSetup = new HashSet<ArSdateSetup>();
            ArShifts = new HashSet<ArShifts>();
            ArTerm = new HashSet<ArTerm>();
            CmTimeInterval = new HashSet<CmTimeInterval>();
            FaLenders = new HashSet<FaLenders>();
            HrEmployeeEmergencyContacts = new HashSet<HrEmployeeEmergencyContacts>();
            HrEmployees = new HashSet<HrEmployees>();
            PlAddressTypes = new HashSet<PlAddressTypes>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlEmployers = new HashSet<PlEmployers>();
            PlExitInterview = new HashSet<PlExitInterview>();
            PlFee = new HashSet<PlFee>();
            PlFldStudy = new HashSet<PlFldStudy>();
            PlHowPlaced = new HashSet<PlHowPlaced>();
            PlIndustries = new HashSet<PlIndustries>();
            PlInterview = new HashSet<PlInterview>();
            PlJobBenefit = new HashSet<PlJobBenefit>();
            PlJobCats = new HashSet<PlJobCats>();
            PlJobSchedule = new HashSet<PlJobSchedule>();
            PlJobStatus = new HashSet<PlJobStatus>();
            PlJobType = new HashSet<PlJobType>();
            PlLocations = new HashSet<PlLocations>();
            PlSalaryType = new HashSet<PlSalaryType>();
            PlSkillGroups = new HashSet<PlSkillGroups>();
            PlSkills = new HashSet<PlSkills>();
            PlTransportation = new HashSet<PlTransportation>();
            RptAdmissionsRep = new HashSet<RptAdmissionsRep>();
            RptLeadStatus = new HashSet<RptLeadStatus>();
            SaAcademicYears = new HashSet<SaAcademicYears>();
            SaBankAccounts = new HashSet<SaBankAccounts>();
            SaBankCodes = new HashSet<SaBankCodes>();
            SaBillingMethods = new HashSet<SaBillingMethods>();
            SaCourseFees = new HashSet<SaCourseFees>();
            SaFundSources = new HashSet<SaFundSources>();
            SaGlaccounts = new HashSet<SaGlaccounts>();
            SaPaymentDescriptions = new HashSet<SaPaymentDescriptions>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaProgramVersionFees = new HashSet<SaProgramVersionFees>();
            SaRateSchedules = new HashSet<SaRateSchedules>();
            SaSysTransCodes = new HashSet<SaSysTransCodes>();
            SaTransCodes = new HashSet<SaTransCodes>();
            SaTuitionCategories = new HashSet<SaTuitionCategories>();
            SaTuitionEarnings = new HashSet<SaTuitionEarnings>();
            SyAccreditingAgencies = new HashSet<SyAccreditingAgencies>();
            SyAddressStatuses = new HashSet<SyAddressStatuses>();
            SyAnnouncements = new HashSet<SyAnnouncements>();
            SyAwardTypes9010Mapping = new HashSet<SyAwardTypes9010Mapping>();
            SyCampGrps = new HashSet<SyCampGrps>();
            SyCampuses = new HashSet<SyCampuses>();
            SyCertifications = new HashSet<SyCertifications>();
            SyContactTypes = new HashSet<SyContactTypes>();
            SyDepartments = new HashSet<SyDepartments>();
            SyDocStatuses = new HashSet<SyDocStatuses>();
            SyEvents = new HashSet<SyEvents>();
            SyFamilyIncome = new HashSet<SyFamilyIncome>();
            SyFileConfigurationFeatures = new HashSet<SyFileConfigurationFeatures>();
            SyHolidays = new HashSet<SyHolidays>();
            SyHomePageNotes = new HashSet<SyHomePageNotes>();
            SyHrdepartments = new HashSet<SyHrdepartments>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
            SyInstitutionContacts = new HashSet<SyInstitutionContacts>();
            SyInstitutions = new HashSet<SyInstitutions>();
            SyLeadStatusChangePermissions = new HashSet<SyLeadStatusChangePermissions>();
            SyMessageGroups = new HashSet<SyMessageGroups>();
            SyMessageSchemas = new HashSet<SyMessageSchemas>();
            SyMessageTemplates = new HashSet<SyMessageTemplates>();
            SyPeriods = new HashSet<SyPeriods>();
            SyPhoneStatuses = new HashSet<SyPhoneStatuses>();
            SyPhoneType = new HashSet<SyPhoneType>();
            SyPositions = new HashSet<SyPositions>();
            SyPrefixes = new HashSet<SyPrefixes>();
            SyQuickLinks = new HashSet<SyQuickLinks>();
            SyRdfsiteSums = new HashSet<SyRdfsiteSums>();
            SyReasonNotEnrolled = new HashSet<SyReasonNotEnrolled>();
            SyRelationshipStatus = new HashSet<SyRelationshipStatus>();
            SyRoles = new HashSet<SyRoles>();
            SySdf = new HashSet<SySdf>();
            SyStateBoardAgencies = new HashSet<SyStateBoardAgencies>();
            SyStateBoardCourses = new HashSet<SyStateBoardCourses>();
            SyStates = new HashSet<SyStates>();
            SyStatusCodes = new HashSet<SyStatusCodes>();
            SyStuRestrictionTypes = new HashSet<SyStuRestrictionTypes>();
            SySuffixes = new HashSet<SySuffixes>();
            SySysRoles = new HashSet<SySysRoles>();
            SySysStatus = new HashSet<SySysStatus>();
            SyTitleIvsapCustomVerbiage = new HashSet<SyTitleIvsapCustomVerbiage>();
            SyTitleIvsapStatus = new HashSet<SyTitleIvsapStatus>();
            SyWidgetResourceRoles = new HashSet<SyWidgetResourceRoles>();
            SyWidgetUserResourceSettings = new HashSet<SyWidgetUserResourceSettings>();
            SyWidgets = new HashSet<SyWidgets>();
        }

        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Status</summary>*/
        public string Status { get; set; }
        /*<summary>The get and set for StatusCode</summary>*/
        public string StatusCode { get; set; }

        /*<summary>The navigational property for AdAdvInterval</summary>*/
        public virtual ICollection<AdAdvInterval>AdAdvInterval { get; set; }
        /*<summary>The navigational property for AdAgencySponsors</summary>*/
        public virtual ICollection<AdAgencySponsors>AdAgencySponsors { get; set; }
        /*<summary>The navigational property for AdCitizenships</summary>*/
        public virtual ICollection<AdCitizenships>AdCitizenships { get; set; }
        /*<summary>The navigational property for AdColleges</summary>*/
        public virtual ICollection<AdColleges>AdColleges { get; set; }
        /*<summary>The navigational property for AdCounties</summary>*/
        public virtual ICollection<AdCounties>AdCounties { get; set; }
        /*<summary>The navigational property for AdCountries</summary>*/
        public virtual ICollection<AdCountries>AdCountries { get; set; }
        /*<summary>The navigational property for AdEdLvls</summary>*/
        public virtual ICollection<AdEdLvls>AdEdLvls { get; set; }
        /*<summary>The navigational property for AdEthCodes</summary>*/
        public virtual ICollection<AdEthCodes>AdEthCodes { get; set; }
        /*<summary>The navigational property for AdExpertiseLevel</summary>*/
        public virtual ICollection<AdExpertiseLevel>AdExpertiseLevel { get; set; }
        /*<summary>The navigational property for AdExtraCurr</summary>*/
        public virtual ICollection<AdExtraCurr>AdExtraCurr { get; set; }
        /*<summary>The navigational property for AdExtraCurrGrp</summary>*/
        public virtual ICollection<AdExtraCurrGrp>AdExtraCurrGrp { get; set; }
        /*<summary>The navigational property for AdFullPartTime</summary>*/
        public virtual ICollection<AdFullPartTime>AdFullPartTime { get; set; }
        /*<summary>The navigational property for AdGenders</summary>*/
        public virtual ICollection<AdGenders>AdGenders { get; set; }
        /*<summary>The navigational property for AdHighSchools</summary>*/
        public virtual ICollection<AdHighSchools>AdHighSchools { get; set; }
        /*<summary>The navigational property for AdImportLeadsMappings</summary>*/
        public virtual ICollection<AdImportLeadsMappings>AdImportLeadsMappings { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadEducation</summary>*/
        public virtual ICollection<AdLeadEducation>AdLeadEducation { get; set; }
        /*<summary>The navigational property for AdLeadEmail</summary>*/
        public virtual ICollection<AdLeadEmail>AdLeadEmail { get; set; }
        /*<summary>The navigational property for AdLeadGroups</summary>*/
        public virtual ICollection<AdLeadGroups>AdLeadGroups { get; set; }
        /*<summary>The navigational property for AdLeadGrpReqGroups</summary>*/
        public virtual ICollection<AdLeadGrpReqGroups>AdLeadGrpReqGroups { get; set; }
        /*<summary>The navigational property for AdLeadNotes1</summary>*/
        public virtual ICollection<AdLeadNotes1>AdLeadNotes1 { get; set; }
        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsEmail</summary>*/
        public virtual ICollection<AdLeadOtherContactsEmail>AdLeadOtherContactsEmail { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsPhone</summary>*/
        public virtual ICollection<AdLeadOtherContactsPhone>AdLeadOtherContactsPhone { get; set; }
        /*<summary>The navigational property for AdLeadPhone</summary>*/
        public virtual ICollection<AdLeadPhone>AdLeadPhone { get; set; }
        /*<summary>The navigational property for AdLeadsAddressStatusNavigation</summary>*/
        public virtual ICollection<AdLeads>AdLeadsAddressStatusNavigation { get; set; }
        /*<summary>The navigational property for AdLeadsPhoneStatusNavigation</summary>*/
        public virtual ICollection<AdLeads>AdLeadsPhoneStatusNavigation { get; set; }
        /*<summary>The navigational property for AdLeadsStudentStatus</summary>*/
        public virtual ICollection<AdLeads>AdLeadsStudentStatus { get; set; }
        /*<summary>The navigational property for AdLevel</summary>*/
        public virtual ICollection<AdLevel>AdLevel { get; set; }
        /*<summary>The navigational property for AdMaritalStatus</summary>*/
        public virtual ICollection<AdMaritalStatus>AdMaritalStatus { get; set; }
        /*<summary>The navigational property for AdNationalities</summary>*/
        public virtual ICollection<AdNationalities>AdNationalities { get; set; }
        /*<summary>The navigational property for AdReqGroups</summary>*/
        public virtual ICollection<AdReqGroups>AdReqGroups { get; set; }
        /*<summary>The navigational property for AdReqs</summary>*/
        public virtual ICollection<AdReqs>AdReqs { get; set; }
        /*<summary>The navigational property for AdSourceAdvertisement</summary>*/
        public virtual ICollection<AdSourceAdvertisement>AdSourceAdvertisement { get; set; }
        /*<summary>The navigational property for AdSourceCatagory</summary>*/
        public virtual ICollection<AdSourceCatagory>AdSourceCatagory { get; set; }
        /*<summary>The navigational property for AdSourceType</summary>*/
        public virtual ICollection<AdSourceType>AdSourceType { get; set; }
        /*<summary>The navigational property for AdTitles</summary>*/
        public virtual ICollection<AdTitles>AdTitles { get; set; }
        /*<summary>The navigational property for ArBkCategories</summary>*/
        public virtual ICollection<ArBkCategories>ArBkCategories { get; set; }
        /*<summary>The navigational property for ArBooks</summary>*/
        public virtual ICollection<ArBooks>ArBooks { get; set; }
        /*<summary>The navigational property for ArBuildings</summary>*/
        public virtual ICollection<ArBuildings>ArBuildings { get; set; }
        /*<summary>The navigational property for ArCertifications</summary>*/
        public virtual ICollection<ArCertifications>ArCertifications { get; set; }
        /*<summary>The navigational property for ArCollegeDivisions</summary>*/
        public virtual ICollection<ArCollegeDivisions>ArCollegeDivisions { get; set; }
        /*<summary>The navigational property for ArCourseCategories</summary>*/
        public virtual ICollection<ArCourseCategories>ArCourseCategories { get; set; }
        /*<summary>The navigational property for ArDegrees</summary>*/
        public virtual ICollection<ArDegrees>ArDegrees { get; set; }
        /*<summary>The navigational property for ArDepartments</summary>*/
        public virtual ICollection<ArDepartments>ArDepartments { get; set; }
        /*<summary>The navigational property for ArDropReasons</summary>*/
        public virtual ICollection<ArDropReasons>ArDropReasons { get; set; }
        /*<summary>The navigational property for ArFasapchkResults</summary>*/
        public virtual ICollection<ArFasapchkResults>ArFasapchkResults { get; set; }
        /*<summary>The navigational property for ArFerpacategory</summary>*/
        public virtual ICollection<ArFerpacategory>ArFerpacategory { get; set; }
        /*<summary>The navigational property for ArFerpaentity</summary>*/
        public virtual ICollection<ArFerpaentity>ArFerpaentity { get; set; }
        /*<summary>The navigational property for ArGradeScales</summary>*/
        public virtual ICollection<ArGradeScales>ArGradeScales { get; set; }
        /*<summary>The navigational property for ArGradeSystems</summary>*/
        public virtual ICollection<ArGradeSystems>ArGradeSystems { get; set; }
        /*<summary>The navigational property for ArGrdBkEvalTyps</summary>*/
        public virtual ICollection<ArGrdBkEvalTyps>ArGrdBkEvalTyps { get; set; }
        /*<summary>The navigational property for ArGrdBkWeights</summary>*/
        public virtual ICollection<ArGrdBkWeights>ArGrdBkWeights { get; set; }
        /*<summary>The navigational property for ArGrdComponentTypes</summary>*/
        public virtual ICollection<ArGrdComponentTypes>ArGrdComponentTypes { get; set; }
        /*<summary>The navigational property for ArLoareasons</summary>*/
        public virtual ICollection<ArLoareasons>ArLoareasons { get; set; }
        /*<summary>The navigational property for ArPeriod</summary>*/
        public virtual ICollection<ArPeriod>ArPeriod { get; set; }
        /*<summary>The navigational property for ArPrgGrp</summary>*/
        public virtual ICollection<ArPrgGrp>ArPrgGrp { get; set; }
        /*<summary>The navigational property for ArPrgVersionFees</summary>*/
        public virtual ICollection<ArPrgVersionFees>ArPrgVersionFees { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArProgCredential</summary>*/
        public virtual ICollection<ArProgCredential>ArProgCredential { get; set; }
        /*<summary>The navigational property for ArProgTypes</summary>*/
        public virtual ICollection<ArProgTypes>ArProgTypes { get; set; }
        /*<summary>The navigational property for ArPrograms</summary>*/
        public virtual ICollection<ArPrograms>ArPrograms { get; set; }
        /*<summary>The navigational property for ArReqTypes</summary>*/
        public virtual ICollection<ArReqTypes>ArReqTypes { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
        /*<summary>The navigational property for ArRooms</summary>*/
        public virtual ICollection<ArRooms>ArRooms { get; set; }
        /*<summary>The navigational property for ArSap</summary>*/
        public virtual ICollection<ArSap>ArSap { get; set; }
        /*<summary>The navigational property for ArSdateSetup</summary>*/
        public virtual ICollection<ArSdateSetup>ArSdateSetup { get; set; }
        /*<summary>The navigational property for ArShifts</summary>*/
        public virtual ICollection<ArShifts>ArShifts { get; set; }
        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
        /*<summary>The navigational property for CmTimeInterval</summary>*/
        public virtual ICollection<CmTimeInterval>CmTimeInterval { get; set; }
        /*<summary>The navigational property for FaLenders</summary>*/
        public virtual ICollection<FaLenders>FaLenders { get; set; }
        /*<summary>The navigational property for HrEmployeeEmergencyContacts</summary>*/
        public virtual ICollection<HrEmployeeEmergencyContacts>HrEmployeeEmergencyContacts { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for PlAddressTypes</summary>*/
        public virtual ICollection<PlAddressTypes>PlAddressTypes { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
        /*<summary>The navigational property for PlFee</summary>*/
        public virtual ICollection<PlFee>PlFee { get; set; }
        /*<summary>The navigational property for PlFldStudy</summary>*/
        public virtual ICollection<PlFldStudy>PlFldStudy { get; set; }
        /*<summary>The navigational property for PlHowPlaced</summary>*/
        public virtual ICollection<PlHowPlaced>PlHowPlaced { get; set; }
        /*<summary>The navigational property for PlIndustries</summary>*/
        public virtual ICollection<PlIndustries>PlIndustries { get; set; }
        /*<summary>The navigational property for PlInterview</summary>*/
        public virtual ICollection<PlInterview>PlInterview { get; set; }
        /*<summary>The navigational property for PlJobBenefit</summary>*/
        public virtual ICollection<PlJobBenefit>PlJobBenefit { get; set; }
        /*<summary>The navigational property for PlJobCats</summary>*/
        public virtual ICollection<PlJobCats>PlJobCats { get; set; }
        /*<summary>The navigational property for PlJobSchedule</summary>*/
        public virtual ICollection<PlJobSchedule>PlJobSchedule { get; set; }
        /*<summary>The navigational property for PlJobStatus</summary>*/
        public virtual ICollection<PlJobStatus>PlJobStatus { get; set; }
        /*<summary>The navigational property for PlJobType</summary>*/
        public virtual ICollection<PlJobType>PlJobType { get; set; }
        /*<summary>The navigational property for PlLocations</summary>*/
        public virtual ICollection<PlLocations>PlLocations { get; set; }
        /*<summary>The navigational property for PlSalaryType</summary>*/
        public virtual ICollection<PlSalaryType>PlSalaryType { get; set; }
        /*<summary>The navigational property for PlSkillGroups</summary>*/
        public virtual ICollection<PlSkillGroups>PlSkillGroups { get; set; }
        /*<summary>The navigational property for PlSkills</summary>*/
        public virtual ICollection<PlSkills>PlSkills { get; set; }
        /*<summary>The navigational property for PlTransportation</summary>*/
        public virtual ICollection<PlTransportation>PlTransportation { get; set; }
        /*<summary>The navigational property for RptAdmissionsRep</summary>*/
        public virtual ICollection<RptAdmissionsRep>RptAdmissionsRep { get; set; }
        /*<summary>The navigational property for RptLeadStatus</summary>*/
        public virtual ICollection<RptLeadStatus>RptLeadStatus { get; set; }
        /*<summary>The navigational property for SaAcademicYears</summary>*/
        public virtual ICollection<SaAcademicYears>SaAcademicYears { get; set; }
        /*<summary>The navigational property for SaBankAccounts</summary>*/
        public virtual ICollection<SaBankAccounts>SaBankAccounts { get; set; }
        /*<summary>The navigational property for SaBankCodes</summary>*/
        public virtual ICollection<SaBankCodes>SaBankCodes { get; set; }
        /*<summary>The navigational property for SaBillingMethods</summary>*/
        public virtual ICollection<SaBillingMethods>SaBillingMethods { get; set; }
        /*<summary>The navigational property for SaCourseFees</summary>*/
        public virtual ICollection<SaCourseFees>SaCourseFees { get; set; }
        /*<summary>The navigational property for SaFundSources</summary>*/
        public virtual ICollection<SaFundSources>SaFundSources { get; set; }
        /*<summary>The navigational property for SaGlaccounts</summary>*/
        public virtual ICollection<SaGlaccounts>SaGlaccounts { get; set; }
        /*<summary>The navigational property for SaPaymentDescriptions</summary>*/
        public virtual ICollection<SaPaymentDescriptions>SaPaymentDescriptions { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaProgramVersionFees</summary>*/
        public virtual ICollection<SaProgramVersionFees>SaProgramVersionFees { get; set; }
        /*<summary>The navigational property for SaRateSchedules</summary>*/
        public virtual ICollection<SaRateSchedules>SaRateSchedules { get; set; }
        /*<summary>The navigational property for SaSysTransCodes</summary>*/
        public virtual ICollection<SaSysTransCodes>SaSysTransCodes { get; set; }
        /*<summary>The navigational property for SaTransCodes</summary>*/
        public virtual ICollection<SaTransCodes>SaTransCodes { get; set; }
        /*<summary>The navigational property for SaTuitionCategories</summary>*/
        public virtual ICollection<SaTuitionCategories>SaTuitionCategories { get; set; }
        /*<summary>The navigational property for SaTuitionEarnings</summary>*/
        public virtual ICollection<SaTuitionEarnings>SaTuitionEarnings { get; set; }
        /*<summary>The navigational property for SyAccreditingAgencies</summary>*/
        public virtual ICollection<SyAccreditingAgencies>SyAccreditingAgencies { get; set; }
        /*<summary>The navigational property for SyAddressStatuses</summary>*/
        public virtual ICollection<SyAddressStatuses>SyAddressStatuses { get; set; }
        /*<summary>The navigational property for SyAnnouncements</summary>*/
        public virtual ICollection<SyAnnouncements>SyAnnouncements { get; set; }
        /*<summary>The navigational property for SyAwardTypes9010Mapping</summary>*/
        public virtual ICollection<SyAwardTypes9010Mapping>SyAwardTypes9010Mapping { get; set; }
        /*<summary>The navigational property for SyCampGrps</summary>*/
        public virtual ICollection<SyCampGrps>SyCampGrps { get; set; }
        /*<summary>The navigational property for SyCampuses</summary>*/
        public virtual ICollection<SyCampuses>SyCampuses { get; set; }
        /*<summary>The navigational property for SyCertifications</summary>*/
        public virtual ICollection<SyCertifications>SyCertifications { get; set; }
        /*<summary>The navigational property for SyContactTypes</summary>*/
        public virtual ICollection<SyContactTypes>SyContactTypes { get; set; }
        /*<summary>The navigational property for SyDepartments</summary>*/
        public virtual ICollection<SyDepartments>SyDepartments { get; set; }
        /*<summary>The navigational property for SyDocStatuses</summary>*/
        public virtual ICollection<SyDocStatuses>SyDocStatuses { get; set; }
        /*<summary>The navigational property for SyEvents</summary>*/
        public virtual ICollection<SyEvents>SyEvents { get; set; }
        /*<summary>The navigational property for SyFamilyIncome</summary>*/
        public virtual ICollection<SyFamilyIncome>SyFamilyIncome { get; set; }
        /*<summary>The navigational property for SyFileConfigurationFeatures</summary>*/
        public virtual ICollection<SyFileConfigurationFeatures>SyFileConfigurationFeatures { get; set; }
        /*<summary>The navigational property for SyHolidays</summary>*/
        public virtual ICollection<SyHolidays>SyHolidays { get; set; }
        /*<summary>The navigational property for SyHomePageNotes</summary>*/
        public virtual ICollection<SyHomePageNotes>SyHomePageNotes { get; set; }
        /*<summary>The navigational property for SyHrdepartments</summary>*/
        public virtual ICollection<SyHrdepartments>SyHrdepartments { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
        /*<summary>The navigational property for SyInstitutionContacts</summary>*/
        public virtual ICollection<SyInstitutionContacts>SyInstitutionContacts { get; set; }
        /*<summary>The navigational property for SyInstitutions</summary>*/
        public virtual ICollection<SyInstitutions>SyInstitutions { get; set; }
        /*<summary>The navigational property for SyLeadStatusChangePermissions</summary>*/
        public virtual ICollection<SyLeadStatusChangePermissions>SyLeadStatusChangePermissions { get; set; }
        /*<summary>The navigational property for SyMessageGroups</summary>*/
        public virtual ICollection<SyMessageGroups>SyMessageGroups { get; set; }
        /*<summary>The navigational property for SyMessageSchemas</summary>*/
        public virtual ICollection<SyMessageSchemas>SyMessageSchemas { get; set; }
        /*<summary>The navigational property for SyMessageTemplates</summary>*/
        public virtual ICollection<SyMessageTemplates>SyMessageTemplates { get; set; }
        /*<summary>The navigational property for SyPeriods</summary>*/
        public virtual ICollection<SyPeriods>SyPeriods { get; set; }
        /*<summary>The navigational property for SyPhoneStatuses</summary>*/
        public virtual ICollection<SyPhoneStatuses>SyPhoneStatuses { get; set; }
        /*<summary>The navigational property for SyPhoneType</summary>*/
        public virtual ICollection<SyPhoneType>SyPhoneType { get; set; }
        /*<summary>The navigational property for SyPositions</summary>*/
        public virtual ICollection<SyPositions>SyPositions { get; set; }
        /*<summary>The navigational property for SyPrefixes</summary>*/
        public virtual ICollection<SyPrefixes>SyPrefixes { get; set; }
        /*<summary>The navigational property for SyQuickLinks</summary>*/
        public virtual ICollection<SyQuickLinks>SyQuickLinks { get; set; }
        /*<summary>The navigational property for SyRdfsiteSums</summary>*/
        public virtual ICollection<SyRdfsiteSums>SyRdfsiteSums { get; set; }
        /*<summary>The navigational property for SyReasonNotEnrolled</summary>*/
        public virtual ICollection<SyReasonNotEnrolled>SyReasonNotEnrolled { get; set; }
        /*<summary>The navigational property for SyRelationshipStatus</summary>*/
        public virtual ICollection<SyRelationshipStatus>SyRelationshipStatus { get; set; }
        /*<summary>The navigational property for SyRoles</summary>*/
        public virtual ICollection<SyRoles>SyRoles { get; set; }
        /*<summary>The navigational property for SySdf</summary>*/
        public virtual ICollection<SySdf>SySdf { get; set; }
        /*<summary>The navigational property for SyStateBoardAgencies</summary>*/
        public virtual ICollection<SyStateBoardAgencies>SyStateBoardAgencies { get; set; }
        /*<summary>The navigational property for SyStateBoardCourses</summary>*/
        public virtual ICollection<SyStateBoardCourses>SyStateBoardCourses { get; set; }
        /*<summary>The navigational property for SyStates</summary>*/
        public virtual ICollection<SyStates>SyStates { get; set; }
        /*<summary>The navigational property for SyStatusCodes</summary>*/
        public virtual ICollection<SyStatusCodes>SyStatusCodes { get; set; }
        /*<summary>The navigational property for SyStuRestrictionTypes</summary>*/
        public virtual ICollection<SyStuRestrictionTypes>SyStuRestrictionTypes { get; set; }
        /*<summary>The navigational property for SySuffixes</summary>*/
        public virtual ICollection<SySuffixes>SySuffixes { get; set; }
        /*<summary>The navigational property for SySysRoles</summary>*/
        public virtual ICollection<SySysRoles>SySysRoles { get; set; }
        /*<summary>The navigational property for SySysStatus</summary>*/
        public virtual ICollection<SySysStatus>SySysStatus { get; set; }
        /*<summary>The navigational property for SyTitleIvsapCustomVerbiage</summary>*/
        public virtual ICollection<SyTitleIvsapCustomVerbiage>SyTitleIvsapCustomVerbiage { get; set; }
        /*<summary>The navigational property for SyTitleIvsapStatus</summary>*/
        public virtual ICollection<SyTitleIvsapStatus>SyTitleIvsapStatus { get; set; }
        /*<summary>The navigational property for SyWidgetResourceRoles</summary>*/
        public virtual ICollection<SyWidgetResourceRoles>SyWidgetResourceRoles { get; set; }
        /*<summary>The navigational property for SyWidgetUserResourceSettings</summary>*/
        public virtual ICollection<SyWidgetUserResourceSettings>SyWidgetUserResourceSettings { get; set; }
        /*<summary>The navigational property for SyWidgets</summary>*/
        public virtual ICollection<SyWidgets>SyWidgets { get; set; }
    }
}