﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkWgtDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkWgtDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkWgtDetails</summary>*/
    public partial class ArGrdBkWgtDetails
    {
        /*<summary>The constructor for ArGrdBkWgtDetails</summary>*/
        public ArGrdBkWgtDetails()
        {
            ArGrdBkResults = new HashSet<ArGrdBkResults>();
        }

        /*<summary>The get and set for InstrGrdBkWgtDetailId</summary>*/
        public Guid InstrGrdBkWgtDetailId { get; set; }
        /*<summary>The get and set for InstrGrdBkWgtId</summary>*/
        public Guid InstrGrdBkWgtId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for Weight</summary>*/
        public decimal? Weight { get; set; }
        /*<summary>The get and set for Seq</summary>*/
        public int? Seq { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Parameter</summary>*/
        public int? Parameter { get; set; }
        /*<summary>The get and set for Number</summary>*/
        public decimal? Number { get; set; }
        /*<summary>The get and set for GrdPolicyId</summary>*/
        public int? GrdPolicyId { get; set; }
        /*<summary>The get and set for Required</summary>*/
        public bool? Required { get; set; }
        /*<summary>The get and set for MustPass</summary>*/
        public bool? MustPass { get; set; }
        /*<summary>The get and set for CreditsPerService</summary>*/
        public decimal? CreditsPerService { get; set; }

        /*<summary>The navigational property for GrdComponentType</summary>*/
        public virtual ArGrdComponentTypes GrdComponentType { get; set; }
        /*<summary>The navigational property for GrdPolicy</summary>*/
        public virtual SyGrdPolicies GrdPolicy { get; set; }
        /*<summary>The navigational property for InstrGrdBkWgt</summary>*/
        public virtual ArGrdBkWeights InstrGrdBkWgt { get; set; }
        /*<summary>The navigational property for ArGrdBkResults</summary>*/
        public virtual ICollection<ArGrdBkResults>ArGrdBkResults { get; set; }
    }
}