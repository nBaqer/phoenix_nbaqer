﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPeriodsWorkDays.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPeriodsWorkDays definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPeriodsWorkDays</summary>*/
    public partial class SyPeriodsWorkDays
    {
        /*<summary>The get and set for PeriodIdWorkDayId</summary>*/
        public Guid PeriodIdWorkDayId { get; set; }
        /*<summary>The get and set for PeriodId</summary>*/
        public Guid PeriodId { get; set; }
        /*<summary>The get and set for WorkDayId</summary>*/
        public Guid WorkDayId { get; set; }

        /*<summary>The navigational property for Period</summary>*/
        public virtual SyPeriods Period { get; set; }
        /*<summary>The navigational property for WorkDay</summary>*/
        public virtual PlWorkDays WorkDay { get; set; }
    }
}