﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArBridgeGradeComponentTypesCourses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArBridgeGradeComponentTypesCourses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArBridgeGradeComponentTypesCourses</summary>*/
    public partial class ArBridgeGradeComponentTypesCourses
    {
        /*<summary>The get and set for GrdComponentTypeIdReqId</summary>*/
        public Guid? GrdComponentTypeIdReqId { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid? GrdComponentTypeId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid? ReqId { get; set; }
        /*<summary>The get and set for BridgeGradeComponentTypesCoursesId</summary>*/
        public Guid BridgeGradeComponentTypesCoursesId { get; set; }

        /*<summary>The navigational property for GrdComponentType</summary>*/
        public virtual ArGrdComponentTypes GrdComponentType { get; set; }
    }
}