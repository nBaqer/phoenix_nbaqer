﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdVendors.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdVendors definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdVendors</summary>*/
    public partial class AdVendors
    {
        /*<summary>The constructor for AdVendors</summary>*/
        public AdVendors()
        {
            AdVendorCampaign = new HashSet<AdVendorCampaign>();
        }

        /*<summary>The get and set for VendorId</summary>*/
        public int VendorId { get; set; }
        /*<summary>The get and set for VendorName</summary>*/
        public string VendorName { get; set; }
        /*<summary>The get and set for VendorCode</summary>*/
        public string VendorCode { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for DateOperationBegin</summary>*/
        public DateTime DateOperationBegin { get; set; }
        /*<summary>The get and set for DateOperationEnd</summary>*/
        public DateTime? DateOperationEnd { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool IsActive { get; set; }
        /*<summary>The get and set for IsDeleted</summary>*/
        public bool IsDeleted { get; set; }

        /*<summary>The navigational property for AdVendorCampaign</summary>*/
        public virtual ICollection<AdVendorCampaign>AdVendorCampaign { get; set; }
    }
}