﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlSalaryType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlSalaryType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlSalaryType</summary>*/
    public partial class PlSalaryType
    {
        /*<summary>The constructor for PlSalaryType</summary>*/
        public PlSalaryType()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for SalaryTypeId</summary>*/
        public Guid SalaryTypeId { get; set; }
        /*<summary>The get and set for SalaryTypeDescrip</summary>*/
        public string SalaryTypeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for SalaryTypeCode</summary>*/
        public string SalaryTypeCode { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}