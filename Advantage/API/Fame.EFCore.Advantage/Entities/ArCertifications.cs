﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCertifications.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCertifications definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCertifications</summary>*/
    public partial class ArCertifications
    {
        /*<summary>The get and set for CertId</summary>*/
        public Guid CertId { get; set; }
        /*<summary>The get and set for CertCode</summary>*/
        public string CertCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CertDescrip</summary>*/
        public string CertDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}