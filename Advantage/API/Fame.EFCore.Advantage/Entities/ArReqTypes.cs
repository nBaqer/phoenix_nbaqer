﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArReqTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArReqTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArReqTypes</summary>*/
    public partial class ArReqTypes
    {
        /*<summary>The constructor for ArReqTypes</summary>*/
        public ArReqTypes()
        {
            ArReqs = new HashSet<ArReqs>();
        }

        /*<summary>The get and set for ReqTypeId</summary>*/
        public short ReqTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IsGroup</summary>*/
        public bool? IsGroup { get; set; }
        /*<summary>The get and set for ChildType</summary>*/
        public byte? ChildType { get; set; }
        /*<summary>The get and set for TrkGrade</summary>*/
        public bool? TrkGrade { get; set; }
        /*<summary>The get and set for TrkHours</summary>*/
        public bool? TrkHours { get; set; }
        /*<summary>The get and set for TrkCount</summary>*/
        public bool? TrkCount { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
    }
}