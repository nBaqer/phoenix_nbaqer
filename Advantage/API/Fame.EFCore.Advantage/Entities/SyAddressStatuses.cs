﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAddressStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAddressStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAddressStatuses</summary>*/
    public partial class SyAddressStatuses
    {
        /*<summary>The get and set for AddressStatusId</summary>*/
        public Guid AddressStatusId { get; set; }
        /*<summary>The get and set for AddressStatusCode</summary>*/
        public string AddressStatusCode { get; set; }
        /*<summary>The get and set for AddressStatusDescrip</summary>*/
        public string AddressStatusDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}