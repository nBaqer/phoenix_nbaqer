﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptUserPrefs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptUserPrefs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptUserPrefs</summary>*/
    public partial class SyRptUserPrefs
    {
        /*<summary>The constructor for SyRptUserPrefs</summary>*/
        public SyRptUserPrefs()
        {
            SyRptFilterListPrefs = new HashSet<SyRptFilterListPrefs>();
            SyRptFilterOtherPrefs = new HashSet<SyRptFilterOtherPrefs>();
            SyRptSortPrefs = new HashSet<SyRptSortPrefs>();
        }

        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public string UserId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for PrefName</summary>*/
        public string PrefName { get; set; }
        /*<summary>The get and set for UserResourceId</summary>*/
        public int? UserResourceId { get; set; }

        /*<summary>The navigational property for SyRptFilterListPrefs</summary>*/
        public virtual ICollection<SyRptFilterListPrefs>SyRptFilterListPrefs { get; set; }
        /*<summary>The navigational property for SyRptFilterOtherPrefs</summary>*/
        public virtual ICollection<SyRptFilterOtherPrefs>SyRptFilterOtherPrefs { get; set; }
        /*<summary>The navigational property for SyRptSortPrefs</summary>*/
        public virtual ICollection<SyRptSortPrefs>SyRptSortPrefs { get; set; }
    }
}