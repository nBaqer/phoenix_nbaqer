﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSapdetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSapdetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSapdetails</summary>*/
    public partial class ArSapdetails
    {
        /*<summary>The constructor for ArSapdetails</summary>*/
        public ArSapdetails()
        {
            ArFasapchkResults = new HashSet<ArFasapchkResults>();
            ArSapchkResults = new HashSet<ArSapchkResults>();
            ArSapquantByInstruction = new HashSet<ArSapquantByInstruction>();
            ArSapquantResults = new HashSet<ArSapquantResults>();
        }

        /*<summary>The get and set for SapdetailId</summary>*/
        public Guid SapdetailId { get; set; }
        /*<summary>The get and set for QualMinValue</summary>*/
        public decimal? QualMinValue { get; set; }
        /*<summary>The get and set for QuantMinValue</summary>*/
        public decimal? QuantMinValue { get; set; }
        /*<summary>The get and set for QualMinTypId</summary>*/
        public byte? QualMinTypId { get; set; }
        /*<summary>The get and set for QuantMinUnitTypId</summary>*/
        public byte QuantMinUnitTypId { get; set; }
        /*<summary>The get and set for TrigValue</summary>*/
        public int TrigValue { get; set; }
        /*<summary>The get and set for TrigUnitTypId</summary>*/
        public byte TrigUnitTypId { get; set; }
        /*<summary>The get and set for TrigOffsetTypId</summary>*/
        public byte TrigOffsetTypId { get; set; }
        /*<summary>The get and set for TrigOffsetSeq</summary>*/
        public byte? TrigOffsetSeq { get; set; }
        /*<summary>The get and set for ConsequenceTypId</summary>*/
        public byte ConsequenceTypId { get; set; }
        /*<summary>The get and set for MinCredsCompltd</summary>*/
        public decimal MinCredsCompltd { get; set; }
        /*<summary>The get and set for Sapid</summary>*/
        public Guid? Sapid { get; set; }
        /*<summary>The get and set for MinAttendanceValue</summary>*/
        public decimal MinAttendanceValue { get; set; }
        /*<summary>The get and set for Accuracy</summary>*/
        public decimal? Accuracy { get; set; }

        /*<summary>The navigational property for Sap</summary>*/
        public virtual ArSap Sap { get; set; }
        /*<summary>The navigational property for TrigOffsetTyp</summary>*/
        public virtual ArTrigOffsetTyps TrigOffsetTyp { get; set; }
        /*<summary>The navigational property for TrigUnitTyp</summary>*/
        public virtual ArTrigUnitTyps TrigUnitTyp { get; set; }
        /*<summary>The navigational property for ArFasapchkResults</summary>*/
        public virtual ICollection<ArFasapchkResults>ArFasapchkResults { get; set; }
        /*<summary>The navigational property for ArSapchkResults</summary>*/
        public virtual ICollection<ArSapchkResults>ArSapchkResults { get; set; }
        /*<summary>The navigational property for ArSapquantByInstruction</summary>*/
        public virtual ICollection<ArSapquantByInstruction>ArSapquantByInstruction { get; set; }
        /*<summary>The navigational property for ArSapquantResults</summary>*/
        public virtual ICollection<ArSapquantResults>ArSapquantResults { get; set; }
    }
}