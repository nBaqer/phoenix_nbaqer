﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaBankAccounts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaBankAccounts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaBankAccounts</summary>*/
    public partial class SaBankAccounts
    {
        /*<summary>The constructor for SaBankAccounts</summary>*/
        public SaBankAccounts()
        {
            SaPayments = new HashSet<SaPayments>();
            SaRefunds = new HashSet<SaRefunds>();
        }

        /*<summary>The get and set for BankAcctId</summary>*/
        public Guid BankAcctId { get; set; }
        /*<summary>The get and set for BankId</summary>*/
        public Guid BankId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for BankAcctDescrip</summary>*/
        public string BankAcctDescrip { get; set; }
        /*<summary>The get and set for BankAcctNumber</summary>*/
        public string BankAcctNumber { get; set; }
        /*<summary>The get and set for BankRoutingNumber</summary>*/
        public string BankRoutingNumber { get; set; }
        /*<summary>The get and set for IsDefaultBankAcct</summary>*/
        public bool? IsDefaultBankAcct { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Bank</summary>*/
        public virtual SaBankCodes Bank { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SaPayments</summary>*/
        public virtual ICollection<SaPayments>SaPayments { get; set; }
        /*<summary>The navigational property for SaRefunds</summary>*/
        public virtual ICollection<SaRefunds>SaRefunds { get; set; }
    }
}