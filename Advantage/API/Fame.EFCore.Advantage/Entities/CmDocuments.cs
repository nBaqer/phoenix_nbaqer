﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="CmDocuments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The CmDocuments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for CmDocuments</summary>*/
    public partial class CmDocuments
    {
        /*<summary>The get and set for DocumentId</summary>*/
        public Guid DocumentId { get; set; }
        /*<summary>The get and set for DocumentCode</summary>*/
        public string DocumentCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for DocumentDescrip</summary>*/
        public string DocumentDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int? ModuleId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
    }
}