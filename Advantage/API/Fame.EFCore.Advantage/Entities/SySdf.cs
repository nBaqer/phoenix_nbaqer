﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdf.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdf definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdf</summary>*/
    public partial class SySdf
    {
        /*<summary>The constructor for SySdf</summary>*/
        public SySdf()
        {
            SyResourceSdf = new HashSet<SyResourceSdf>();
            SyRptAdHocFields = new HashSet<SyRptAdHocFields>();
            SySdfModVis = new HashSet<SySdfModVis>();
            SySdfRange = new HashSet<SySdfRange>();
            SySdfValList = new HashSet<SySdfValList>();
            SySdfmoduleValue = new HashSet<SySdfmoduleValue>();
        }

        /*<summary>The get and set for Sdfid</summary>*/
        public Guid Sdfid { get; set; }
        /*<summary>The get and set for Sdfdescrip</summary>*/
        public string Sdfdescrip { get; set; }
        /*<summary>The get and set for DtypeId</summary>*/
        public byte DtypeId { get; set; }
        /*<summary>The get and set for Len</summary>*/
        public int? Len { get; set; }
        /*<summary>The get and set for Decimals</summary>*/
        public byte? Decimals { get; set; }
        /*<summary>The get and set for HelpText</summary>*/
        public string HelpText { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte? ModuleId { get; set; }
        /*<summary>The get and set for ValTypeId</summary>*/
        public byte? ValTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool IsRequired { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyResourceSdf</summary>*/
        public virtual ICollection<SyResourceSdf>SyResourceSdf { get; set; }
        /*<summary>The navigational property for SyRptAdHocFields</summary>*/
        public virtual ICollection<SyRptAdHocFields>SyRptAdHocFields { get; set; }
        /*<summary>The navigational property for SySdfModVis</summary>*/
        public virtual ICollection<SySdfModVis>SySdfModVis { get; set; }
        /*<summary>The navigational property for SySdfRange</summary>*/
        public virtual ICollection<SySdfRange>SySdfRange { get; set; }
        /*<summary>The navigational property for SySdfValList</summary>*/
        public virtual ICollection<SySdfValList>SySdfValList { get; set; }
        /*<summary>The navigational property for SySdfmoduleValue</summary>*/
        public virtual ICollection<SySdfmoduleValue>SySdfmoduleValue { get; set; }
    }
}