﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyReports.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyReports definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyReports</summary>*/
    public partial class SyReports
    {
        /*<summary>The constructor for SyReports</summary>*/
        public SyReports()
        {
            SySchoolStateBoardReports = new HashSet<SySchoolStateBoardReports>();
            SyStateReports = new HashSet<SyStateReports>();
        }

        /*<summary>The get and set for ReportId</summary>*/
        public Guid ReportId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int ResourceId { get; set; }
        /*<summary>The get and set for ReportName</summary>*/
        public string ReportName { get; set; }
        /*<summary>The get and set for ReportDescription</summary>*/
        public string ReportDescription { get; set; }
        /*<summary>The get and set for Rdlname</summary>*/
        public string Rdlname { get; set; }
        /*<summary>The get and set for AssemblyFilePath</summary>*/
        public string AssemblyFilePath { get; set; }
        /*<summary>The get and set for ReportClass</summary>*/
        public string ReportClass { get; set; }
        /*<summary>The get and set for CreationMethod</summary>*/
        public string CreationMethod { get; set; }
        /*<summary>The get and set for WebServiceUrl</summary>*/
        public string WebServiceUrl { get; set; }
        /*<summary>The get and set for RecordLimit</summary>*/
        public long RecordLimit { get; set; }
        /*<summary>The get and set for AllowedExportTypes</summary>*/
        public int AllowedExportTypes { get; set; }
        /*<summary>The get and set for ReportTabLayout</summary>*/
        public int? ReportTabLayout { get; set; }
        /*<summary>The get and set for ShowPerformanceMsg</summary>*/
        public bool? ShowPerformanceMsg { get; set; }
        /*<summary>The get and set for ShowFilterMode</summary>*/
        public bool? ShowFilterMode { get; set; }

        /*<summary>The navigational property for SySchoolStateBoardReports</summary>*/
        public virtual ICollection<SySchoolStateBoardReports>SySchoolStateBoardReports { get; set; }
        /*<summary>The navigational property for SyStateReports</summary>*/
        public virtual ICollection<SyStateReports>SyStateReports { get; set; }
    }
}