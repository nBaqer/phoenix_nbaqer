﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyGenerateStudentId.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyGenerateStudentId definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyGenerateStudentId</summary>*/
    public partial class SyGenerateStudentId
    {
        /*<summary>The get and set for StudentSeqId</summary>*/
        public int StudentSeqId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}