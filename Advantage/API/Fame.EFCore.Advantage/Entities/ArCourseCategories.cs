﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCourseCategories.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCourseCategories definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCourseCategories</summary>*/
    public partial class ArCourseCategories
    {
        /*<summary>The constructor for ArCourseCategories</summary>*/
        public ArCourseCategories()
        {
            ArReqs = new HashSet<ArReqs>();
        }

        /*<summary>The get and set for CourseCategoryId</summary>*/
        public Guid CourseCategoryId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
    }
}