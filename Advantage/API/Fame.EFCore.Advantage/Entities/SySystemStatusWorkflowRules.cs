﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySystemStatusWorkflowRules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySystemStatusWorkflowRules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySystemStatusWorkflowRules</summary>*/
    public partial class SySystemStatusWorkflowRules
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for UniqueId</summary>*/
        public Guid UniqueId { get; set; }
        /*<summary>The get and set for StatusIdFrom</summary>*/
        public int? StatusIdFrom { get; set; }
        /*<summary>The get and set for StatusIdTo</summary>*/
        public int StatusIdTo { get; set; }
        /*<summary>The get and set for AllowInsert</summary>*/
        public bool AllowInsert { get; set; }
        /*<summary>The get and set for AllowInsertSameStatusType</summary>*/
        public bool AllowInsertSameStatusType { get; set; }
        /*<summary>The get and set for AllowEdit</summary>*/
        public bool AllowEdit { get; set; }
        /*<summary>The get and set for AllowEditSameStatusType</summary>*/
        public bool AllowEditSameStatusType { get; set; }
        /*<summary>The get and set for AllowDelete</summary>*/
        public bool AllowDelete { get; set; }
        /*<summary>The get and set for AllowedInBatch</summary>*/
        public bool AllowedInBatch { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for StatusIdFromNavigation</summary>*/
        public virtual SySysStatus StatusIdFromNavigation { get; set; }
        /*<summary>The navigational property for StatusIdToNavigation</summary>*/
        public virtual SySysStatus StatusIdToNavigation { get; set; }
    }
}