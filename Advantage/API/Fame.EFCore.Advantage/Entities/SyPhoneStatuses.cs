﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPhoneStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPhoneStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPhoneStatuses</summary>*/
    public partial class SyPhoneStatuses
    {
        /*<summary>The get and set for PhoneStatusId</summary>*/
        public Guid PhoneStatusId { get; set; }
        /*<summary>The get and set for PhoneStatusCode</summary>*/
        public string PhoneStatusCode { get; set; }
        /*<summary>The get and set for PhoneStatusDescrip</summary>*/
        public string PhoneStatusDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}