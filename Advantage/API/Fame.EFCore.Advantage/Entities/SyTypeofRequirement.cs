﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTypeofRequirement.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTypeofRequirement definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTypeofRequirement</summary>*/
    public partial class SyTypeofRequirement
    {
        /*<summary>The get and set for RequirementId</summary>*/
        public Guid? RequirementId { get; set; }
        /*<summary>The get and set for TypeOfReq</summary>*/
        public string TypeOfReq { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for TypeOfRequirementId</summary>*/
        public int TypeOfRequirementId { get; set; }
    }
}