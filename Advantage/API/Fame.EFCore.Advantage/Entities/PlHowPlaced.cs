﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlHowPlaced.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlHowPlaced definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlHowPlaced</summary>*/
    public partial class PlHowPlaced
    {
        /*<summary>The constructor for PlHowPlaced</summary>*/
        public PlHowPlaced()
        {
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for HowPlacedId</summary>*/
        public Guid HowPlacedId { get; set; }
        /*<summary>The get and set for HowPlacedCode</summary>*/
        public string HowPlacedCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for HowPlacedDescrip</summary>*/
        public string HowPlacedDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}