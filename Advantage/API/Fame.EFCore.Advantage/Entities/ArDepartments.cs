﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArDepartments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArDepartments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArDepartments</summary>*/
    public partial class ArDepartments
    {
        /*<summary>The constructor for ArDepartments</summary>*/
        public ArDepartments()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArReqs = new HashSet<ArReqs>();
        }

        /*<summary>The get and set for DeptId</summary>*/
        public Guid DeptId { get; set; }
        /*<summary>The get and set for DeptCode</summary>*/
        public string DeptCode { get; set; }
        /*<summary>The get and set for DeptDescrip</summary>*/
        public string DeptDescrip { get; set; }
        /*<summary>The get and set for CollegeDivId</summary>*/
        public Guid CollegeDivId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CollegeDiv</summary>*/
        public virtual ArCollegeDivisions CollegeDiv { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
    }
}