﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdSourceType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdSourceType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdSourceType</summary>*/
    public partial class AdSourceType
    {
        /*<summary>The constructor for AdSourceType</summary>*/
        public AdSourceType()
        {
            AdLeads = new HashSet<AdLeads>();
            AdSourceAdvertisement = new HashSet<AdSourceAdvertisement>();
        }

        /*<summary>The get and set for SourceTypeId</summary>*/
        public Guid SourceTypeId { get; set; }
        /*<summary>The get and set for SourceTypeDescrip</summary>*/
        public string SourceTypeDescrip { get; set; }
        /*<summary>The get and set for SourceCatagoryId</summary>*/
        public Guid SourceCatagoryId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for SourceTypeCode</summary>*/
        public string SourceTypeCode { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for SourceCatagory</summary>*/
        public virtual AdSourceCatagory SourceCatagory { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for AdSourceAdvertisement</summary>*/
        public virtual ICollection<AdSourceAdvertisement>AdSourceAdvertisement { get; set; }
    }
}