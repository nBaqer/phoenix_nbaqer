﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlCorpHqs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlCorpHqs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlCorpHqs</summary>*/
    public partial class PlCorpHqs
    {
        /*<summary>The get and set for CorpHqid</summary>*/
        public Guid CorpHqid { get; set; }
        /*<summary>The get and set for CorpHqcode</summary>*/
        public string CorpHqcode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CorpHqname</summary>*/
        public string CorpHqname { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
    }
}