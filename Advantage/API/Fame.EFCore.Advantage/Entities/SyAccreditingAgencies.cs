﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAccreditingAgencies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAccreditingAgencies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAccreditingAgencies</summary>*/
    public partial class SyAccreditingAgencies
    {
        /*<summary>The get and set for AccreditingAgencyId</summary>*/
        public int AccreditingAgencyId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}