﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSap.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSap definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSap</summary>*/
    public partial class ArSap
    {
        /*<summary>The constructor for ArSap</summary>*/
        public ArSap()
        {
            ArPrgVersionsFasap = new HashSet<ArPrgVersions>();
            ArPrgVersionsSap = new HashSet<ArPrgVersions>();
            ArSapdetails = new HashSet<ArSapdetails>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            SyTitleIvsapCustomVerbiage = new HashSet<SyTitleIvsapCustomVerbiage>();
        }

        /*<summary>The get and set for Sapid</summary>*/
        public Guid Sapid { get; set; }
        /*<summary>The get and set for Sapcode</summary>*/
        public string Sapcode { get; set; }
        /*<summary>The get and set for Sapdescrip</summary>*/
        public string Sapdescrip { get; set; }
        /*<summary>The get and set for SapcriteriaId</summary>*/
        public Guid? SapcriteriaId { get; set; }
        /*<summary>The get and set for MinTermGpa</summary>*/
        public decimal? MinTermGpa { get; set; }
        /*<summary>The get and set for MinTermAv</summary>*/
        public decimal? MinTermAv { get; set; }
        /*<summary>The get and set for TermGpaover</summary>*/
        public decimal? TermGpaover { get; set; }
        /*<summary>The get and set for TermAvOver</summary>*/
        public decimal? TermAvOver { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TrigUnitTypId</summary>*/
        public byte? TrigUnitTypId { get; set; }
        /*<summary>The get and set for TrigOffsetTypId</summary>*/
        public byte? TrigOffsetTypId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for TerminationProbationCnt</summary>*/
        public int? TerminationProbationCnt { get; set; }
        /*<summary>The get and set for TrackExternAttendance</summary>*/
        public bool? TrackExternAttendance { get; set; }
        /*<summary>The get and set for IncludeTransferHours</summary>*/
        public bool? IncludeTransferHours { get; set; }
        /*<summary>The get and set for FaSapPolicy</summary>*/
        public bool FaSapPolicy { get; set; }
        /*<summary>The get and set for PayOnProbation</summary>*/
        public bool? PayOnProbation { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TrigOffsetTyp</summary>*/
        public virtual ArTrigOffsetTyps TrigOffsetTyp { get; set; }
        /*<summary>The navigational property for TrigUnitTyp</summary>*/
        public virtual ArTrigUnitTyps TrigUnitTyp { get; set; }
        /*<summary>The navigational property for ArPrgVersionsFasap</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersionsFasap { get; set; }
        /*<summary>The navigational property for ArPrgVersionsSap</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersionsSap { get; set; }
        /*<summary>The navigational property for ArSapdetails</summary>*/
        public virtual ICollection<ArSapdetails>ArSapdetails { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for SyTitleIvsapCustomVerbiage</summary>*/
        public virtual ICollection<SyTitleIvsapCustomVerbiage>SyTitleIvsapCustomVerbiage { get; set; }
    }
}