﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUsers.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUsers definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUsers</summary>*/
    public partial class SyUsers
    {
        /*<summary>The constructor for SyUsers</summary>*/
        public SyUsers()
        {
            AdLeadsAdmissionsRepNavigation = new HashSet<AdLeads>();
            AdLeadsVendor = new HashSet<AdLeads>();
            ArClassSections = new HashSet<ArClassSections>();
            ArGradeScales = new HashSet<ArGradeScales>();
            ArGrdBkWeights = new HashSet<ArGrdBkWeights>();
            ArInstructorsSupervisorsSupervisor = new HashSet<ArInstructorsSupervisors>();
            ArR2t4additionalInformationCreatedBy = new HashSet<ArR2t4additionalInformation>();
            ArR2t4additionalInformationUpdatedBy = new HashSet<ArR2t4additionalInformation>();
            ArR2t4calculationResultsCreatedBy = new HashSet<ArR2t4calculationResults>();
            ArR2t4calculationResultsUpdatedBy = new HashSet<ArR2t4calculationResults>();
            ArR2t4inputCreatedBy = new HashSet<ArR2t4input>();
            ArR2t4inputUpdatedBy = new HashSet<ArR2t4input>();
            ArR2t4overrideInputCreatedBy = new HashSet<ArR2t4overrideInput>();
            ArR2t4overrideInputUpdatedBy = new HashSet<ArR2t4overrideInput>();
            ArR2t4overrideResultsCreatedBy = new HashSet<ArR2t4overrideResults>();
            ArR2t4overrideResultsUpdatedBy = new HashSet<ArR2t4overrideResults>();
            ArR2t4resultsCreatedBy = new HashSet<ArR2t4results>();
            ArR2t4resultsUpdatedBy = new HashSet<ArR2t4results>();
            ArR2t4terminationDetailsCreatedBy = new HashSet<ArR2t4terminationDetails>();
            ArR2t4terminationDetailsUpdatedBy = new HashSet<ArR2t4terminationDetails>();
            ArStuEnrollmentsAcademicAdvisorNavigation = new HashSet<ArStuEnrollments>();
            ArStuEnrollmentsAdmissionsRepNavigation = new HashSet<ArStuEnrollments>();
            ArStuEnrollmentsFaadvisor = new HashSet<ArStuEnrollments>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
            RptAdmissionsRep = new HashSet<RptAdmissionsRep>();
            SaBatchPayments = new HashSet<SaBatchPayments>();
            SyApprovedNaccasprogramVersion = new HashSet<SyApprovedNaccasprogramVersion>();
            SyMrus = new HashSet<SyMrus>();
            SyNaccasSettings = new HashSet<SyNaccasSettings>();
            SyR2t4calculationPeriodTypes = new HashSet<SyR2t4calculationPeriodTypes>();
            SyStateBoardAgencies = new HashSet<SyStateBoardAgencies>();
            SyStateBoardCourses = new HashSet<SyStateBoardCourses>();
            SyStateBoardProgramCourseMappings = new HashSet<SyStateBoardProgramCourseMappings>();
            SyStuRestrictions = new HashSet<SyStuRestrictions>();
            SyTitleIvsapCustomVerbiage = new HashSet<SyTitleIvsapCustomVerbiage>();
            SyTitleIvsapStatusCreatedBy = new HashSet<SyTitleIvsapStatus>();
            SyTitleIvsapStatusUpdatedBy = new HashSet<SyTitleIvsapStatus>();
            SyUsersRolesCampGrps = new HashSet<SyUsersRolesCampGrps>();
            SyWidgetUserResourceSettings = new HashSet<SyWidgetUserResourceSettings>();
        }

        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for FullName</summary>*/
        public string FullName { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }
        /*<summary>The get and set for Password</summary>*/
        public string Password { get; set; }
        /*<summary>The get and set for ConfirmPassword</summary>*/
        public string ConfirmPassword { get; set; }
        /*<summary>The get and set for Salt</summary>*/
        public string Salt { get; set; }
        /*<summary>The get and set for AccountActive</summary>*/
        public bool? AccountActive { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for ShowDefaultCampus</summary>*/
        public bool ShowDefaultCampus { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public short ModuleId { get; set; }
        /*<summary>The get and set for IsAdvantageSuperUser</summary>*/
        public bool IsAdvantageSuperUser { get; set; }
        /*<summary>The get and set for IsDefaultAdminRep</summary>*/
        public bool? IsDefaultAdminRep { get; set; }
        /*<summary>The get and set for IsLoggedIn</summary>*/
        public bool? IsLoggedIn { get; set; }
        /*<summary>The get and set for UserTypeId</summary>*/
        public int UserTypeId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for UserType</summary>*/
        public virtual SyUserType UserType { get; set; }
        /*<summary>The navigational property for ArInstructorsSupervisorsInstructor</summary>*/
        public virtual ArInstructorsSupervisors ArInstructorsSupervisorsInstructor { get; set; }
        /*<summary>The navigational property for AdLeadsAdmissionsRepNavigation</summary>*/
        public virtual ICollection<AdLeads>AdLeadsAdmissionsRepNavigation { get; set; }
        /*<summary>The navigational property for AdLeadsVendor</summary>*/
        public virtual ICollection<AdLeads>AdLeadsVendor { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArGradeScales</summary>*/
        public virtual ICollection<ArGradeScales>ArGradeScales { get; set; }
        /*<summary>The navigational property for ArGrdBkWeights</summary>*/
        public virtual ICollection<ArGrdBkWeights>ArGrdBkWeights { get; set; }
        /*<summary>The navigational property for ArInstructorsSupervisorsSupervisor</summary>*/
        public virtual ICollection<ArInstructorsSupervisors>ArInstructorsSupervisorsSupervisor { get; set; }
        /*<summary>The navigational property for ArR2t4additionalInformationCreatedBy</summary>*/
        public virtual ICollection<ArR2t4additionalInformation>ArR2t4additionalInformationCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4additionalInformationUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4additionalInformation>ArR2t4additionalInformationUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4calculationResultsCreatedBy</summary>*/
        public virtual ICollection<ArR2t4calculationResults>ArR2t4calculationResultsCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4calculationResultsUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4calculationResults>ArR2t4calculationResultsUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4inputCreatedBy</summary>*/
        public virtual ICollection<ArR2t4input>ArR2t4inputCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4inputUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4input>ArR2t4inputUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4overrideInputCreatedBy</summary>*/
        public virtual ICollection<ArR2t4overrideInput>ArR2t4overrideInputCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4overrideInputUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4overrideInput>ArR2t4overrideInputUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4overrideResultsCreatedBy</summary>*/
        public virtual ICollection<ArR2t4overrideResults>ArR2t4overrideResultsCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4overrideResultsUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4overrideResults>ArR2t4overrideResultsUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4resultsCreatedBy</summary>*/
        public virtual ICollection<ArR2t4results>ArR2t4resultsCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4resultsUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4results>ArR2t4resultsUpdatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetailsCreatedBy</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetailsCreatedBy { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetailsUpdatedBy</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetailsUpdatedBy { get; set; }
        /*<summary>The navigational property for ArStuEnrollmentsAcademicAdvisorNavigation</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollmentsAcademicAdvisorNavigation { get; set; }
        /*<summary>The navigational property for ArStuEnrollmentsAdmissionsRepNavigation</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollmentsAdmissionsRepNavigation { get; set; }
        /*<summary>The navigational property for ArStuEnrollmentsFaadvisor</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollmentsFaadvisor { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
        /*<summary>The navigational property for RptAdmissionsRep</summary>*/
        public virtual ICollection<RptAdmissionsRep>RptAdmissionsRep { get; set; }
        /*<summary>The navigational property for SaBatchPayments</summary>*/
        public virtual ICollection<SaBatchPayments>SaBatchPayments { get; set; }
        /*<summary>The navigational property for SyApprovedNaccasprogramVersion</summary>*/
        public virtual ICollection<SyApprovedNaccasprogramVersion>SyApprovedNaccasprogramVersion { get; set; }
        /*<summary>The navigational property for SyMrus</summary>*/
        public virtual ICollection<SyMrus>SyMrus { get; set; }
        /*<summary>The navigational property for SyNaccasSettings</summary>*/
        public virtual ICollection<SyNaccasSettings>SyNaccasSettings { get; set; }
        /*<summary>The navigational property for SyR2t4calculationPeriodTypes</summary>*/
        public virtual ICollection<SyR2t4calculationPeriodTypes>SyR2t4calculationPeriodTypes { get; set; }
        /*<summary>The navigational property for SyStateBoardAgencies</summary>*/
        public virtual ICollection<SyStateBoardAgencies>SyStateBoardAgencies { get; set; }
        /*<summary>The navigational property for SyStateBoardCourses</summary>*/
        public virtual ICollection<SyStateBoardCourses>SyStateBoardCourses { get; set; }
        /*<summary>The navigational property for SyStateBoardProgramCourseMappings</summary>*/
        public virtual ICollection<SyStateBoardProgramCourseMappings>SyStateBoardProgramCourseMappings { get; set; }
        /*<summary>The navigational property for SyStuRestrictions</summary>*/
        public virtual ICollection<SyStuRestrictions>SyStuRestrictions { get; set; }
        /*<summary>The navigational property for SyTitleIvsapCustomVerbiage</summary>*/
        public virtual ICollection<SyTitleIvsapCustomVerbiage>SyTitleIvsapCustomVerbiage { get; set; }
        /*<summary>The navigational property for SyTitleIvsapStatusCreatedBy</summary>*/
        public virtual ICollection<SyTitleIvsapStatus>SyTitleIvsapStatusCreatedBy { get; set; }
        /*<summary>The navigational property for SyTitleIvsapStatusUpdatedBy</summary>*/
        public virtual ICollection<SyTitleIvsapStatus>SyTitleIvsapStatusUpdatedBy { get; set; }
        /*<summary>The navigational property for SyUsersRolesCampGrps</summary>*/
        public virtual ICollection<SyUsersRolesCampGrps>SyUsersRolesCampGrps { get; set; }
        /*<summary>The navigational property for SyWidgetUserResourceSettings</summary>*/
        public virtual ICollection<SyWidgetUserResourceSettings>SyWidgetUserResourceSettings { get; set; }
    }
}