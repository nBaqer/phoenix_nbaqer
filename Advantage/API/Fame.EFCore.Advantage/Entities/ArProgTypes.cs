﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArProgTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArProgTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArProgTypes</summary>*/
    public partial class ArProgTypes
    {
        /*<summary>The constructor for ArProgTypes</summary>*/
        public ArProgTypes()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
        }

        /*<summary>The get and set for ProgTypId</summary>*/
        public Guid ProgTypId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
    }
}