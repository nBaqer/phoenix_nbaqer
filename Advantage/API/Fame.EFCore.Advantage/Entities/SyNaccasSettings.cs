﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyNaccasSettings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyNaccasSettings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyNaccasSettings</summary>*/
    public partial class SyNaccasSettings
    {
        /*<summary>The constructor for SyNaccasSettings</summary>*/
        public SyNaccasSettings()
        {
            SyApprovedNaccasprogramVersion = new HashSet<SyApprovedNaccasprogramVersion>();
            SyNaccasdropReasonsMapping = new HashSet<SyNaccasdropReasonsMapping>();
        }

        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid? CreatedById { get; set; }
        /*<summary>The get and set for CreateDate</summary>*/
        public DateTime CreateDate { get; set; }
        /*<summary>The get and set for NaccasSettingId</summary>*/
        public int NaccasSettingId { get; set; }
        /*<summary>The get and set for ModUserId</summary>*/
        public Guid? ModUserId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for ModUser</summary>*/
        public virtual SyUsers ModUser { get; set; }
        /*<summary>The navigational property for SyApprovedNaccasprogramVersion</summary>*/
        public virtual ICollection<SyApprovedNaccasprogramVersion>SyApprovedNaccasprogramVersion { get; set; }
        /*<summary>The navigational property for SyNaccasdropReasonsMapping</summary>*/
        public virtual ICollection<SyNaccasdropReasonsMapping>SyNaccasdropReasonsMapping { get; set; }
    }
}