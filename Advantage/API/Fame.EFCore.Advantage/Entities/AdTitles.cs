﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdTitles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdTitles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdTitles</summary>*/
    public partial class AdTitles
    {
        /*<summary>The constructor for AdTitles</summary>*/
        public AdTitles()
        {
            AdLeadEmployment = new HashSet<AdLeadEmployment>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
        }

        /*<summary>The get and set for TitleId</summary>*/
        public Guid TitleId { get; set; }
        /*<summary>The get and set for TitleCode</summary>*/
        public string TitleCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TitleDescrip</summary>*/
        public string TitleDescrip { get; set; }
        /*<summary>The get and set for JobCatId</summary>*/
        public Guid? JobCatId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for JobCat</summary>*/
        public virtual PlJobCats JobCat { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadEmployment</summary>*/
        public virtual ICollection<AdLeadEmployment>AdLeadEmployment { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
    }
}