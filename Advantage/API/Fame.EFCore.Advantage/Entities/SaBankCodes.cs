﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaBankCodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaBankCodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaBankCodes</summary>*/
    public partial class SaBankCodes
    {
        /*<summary>The constructor for SaBankCodes</summary>*/
        public SaBankCodes()
        {
            SaBankAccounts = new HashSet<SaBankAccounts>();
        }

        /*<summary>The get and set for BankId</summary>*/
        public Guid BankId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for BankDescrip</summary>*/
        public string BankDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for Fax</summary>*/
        public string Fax { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for Title</summary>*/
        public string Title { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool? ForeignPhone { get; set; }
        /*<summary>The get and set for ForeignFax</summary>*/
        public bool? ForeignFax { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool? ForeignZip { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SaBankAccounts</summary>*/
        public virtual ICollection<SaBankAccounts>SaBankAccounts { get; set; }
    }
}