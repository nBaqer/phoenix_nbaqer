﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArClsSectMeetings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArClsSectMeetings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArClsSectMeetings</summary>*/
    public partial class ArClsSectMeetings
    {
        /*<summary>The constructor for ArClsSectMeetings</summary>*/
        public ArClsSectMeetings()
        {
            AtClsSectAttendance = new HashSet<AtClsSectAttendance>();
        }

        /*<summary>The get and set for ClsSectMeetingId</summary>*/
        public Guid ClsSectMeetingId { get; set; }
        /*<summary>The get and set for WorkDaysId</summary>*/
        public Guid? WorkDaysId { get; set; }
        /*<summary>The get and set for RoomId</summary>*/
        public Guid? RoomId { get; set; }
        /*<summary>The get and set for TimeIntervalId</summary>*/
        public Guid? TimeIntervalId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for EndIntervalId</summary>*/
        public Guid? EndIntervalId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for PeriodId</summary>*/
        public Guid? PeriodId { get; set; }
        /*<summary>The get and set for AltPeriodId</summary>*/
        public Guid? AltPeriodId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for IsMeetingRescheduled</summary>*/
        public bool? IsMeetingRescheduled { get; set; }
        /*<summary>The get and set for BreakDuration</summary>*/
        public int? BreakDuration { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
        /*<summary>The navigational property for EndInterval</summary>*/
        public virtual CmTimeInterval EndInterval { get; set; }
        /*<summary>The navigational property for InstructionType</summary>*/
        public virtual ArInstructionType InstructionType { get; set; }
        /*<summary>The navigational property for Period</summary>*/
        public virtual SyPeriods Period { get; set; }
        /*<summary>The navigational property for Room</summary>*/
        public virtual ArRooms Room { get; set; }
        /*<summary>The navigational property for TimeInterval</summary>*/
        public virtual CmTimeInterval TimeInterval { get; set; }
        /*<summary>The navigational property for WorkDays</summary>*/
        public virtual PlWorkDays WorkDays { get; set; }
        /*<summary>The navigational property for AtClsSectAttendance</summary>*/
        public virtual ICollection<AtClsSectAttendance>AtClsSectAttendance { get; set; }
    }
}