﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGradeScales.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGradeScales definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGradeScales</summary>*/
    public partial class ArGradeScales
    {
        /*<summary>The constructor for ArGradeScales</summary>*/
        public ArGradeScales()
        {
            ArClassSections = new HashSet<ArClassSections>();
            ArGradeScaleDetails = new HashSet<ArGradeScaleDetails>();
            ArPrgVersions = new HashSet<ArPrgVersions>();
        }

        /*<summary>The get and set for GrdScaleId</summary>*/
        public Guid GrdScaleId { get; set; }
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid? InstructorId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for GrdSystemId</summary>*/
        public Guid GrdSystemId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for GrdSystem</summary>*/
        public virtual ArGradeSystems GrdSystem { get; set; }
        /*<summary>The navigational property for Instructor</summary>*/
        public virtual SyUsers Instructor { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArGradeScaleDetails</summary>*/
        public virtual ICollection<ArGradeScaleDetails>ArGradeScaleDetails { get; set; }
        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
    }
}