﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyKlassAppConfigurationSetting.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyKlassAppConfigurationSetting definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyKlassAppConfigurationSetting</summary>*/
    public partial class SyKlassAppConfigurationSetting
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for AdvantageId</summary>*/
        public string AdvantageId { get; set; }
        /*<summary>The get and set for KlassAppId</summary>*/
        public int KlassAppId { get; set; }
        /*<summary>The get and set for KlassEntityId</summary>*/
        public int KlassEntityId { get; set; }
        /*<summary>The get and set for KlassOperationTypeId</summary>*/
        public int KlassOperationTypeId { get; set; }
        /*<summary>The get and set for IsCustomField</summary>*/
        public bool? IsCustomField { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }
        /*<summary>The get and set for ItemStatus</summary>*/
        public string ItemStatus { get; set; }
        /*<summary>The get and set for ItemValue</summary>*/
        public string ItemValue { get; set; }
        /*<summary>The get and set for ItemLabel</summary>*/
        public string ItemLabel { get; set; }
        /*<summary>The get and set for ItemValueType</summary>*/
        public string ItemValueType { get; set; }
        /*<summary>The get and set for CreationDate</summary>*/
        public DateTime CreationDate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for LastOperationLog</summary>*/
        public string LastOperationLog { get; set; }

        /*<summary>The navigational property for KlassEntity</summary>*/
        public virtual SyKlassEntity KlassEntity { get; set; }
        /*<summary>The navigational property for KlassOperationType</summary>*/
        public virtual SyKlassOperationType KlassOperationType { get; set; }
    }
}