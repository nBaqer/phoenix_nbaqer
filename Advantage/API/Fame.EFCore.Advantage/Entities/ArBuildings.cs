﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArBuildings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArBuildings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArBuildings</summary>*/
    public partial class ArBuildings
    {
        /*<summary>The constructor for ArBuildings</summary>*/
        public ArBuildings()
        {
            ArRooms = new HashSet<ArRooms>();
        }

        /*<summary>The get and set for BldgId</summary>*/
        public Guid BldgId { get; set; }
        /*<summary>The get and set for BldgCode</summary>*/
        public string BldgCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for BldgRooms</summary>*/
        public byte BldgRooms { get; set; }
        /*<summary>The get and set for BldgDescrip</summary>*/
        public string BldgDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for Fax</summary>*/
        public string Fax { get; set; }
        /*<summary>The get and set for BldgOpen</summary>*/
        public string BldgOpen { get; set; }
        /*<summary>The get and set for BldgClose</summary>*/
        public string BldgClose { get; set; }
        /*<summary>The get and set for Sun</summary>*/
        public bool? Sun { get; set; }
        /*<summary>The get and set for Mon</summary>*/
        public bool? Mon { get; set; }
        /*<summary>The get and set for Tue</summary>*/
        public bool? Tue { get; set; }
        /*<summary>The get and set for Wed</summary>*/
        public bool? Wed { get; set; }
        /*<summary>The get and set for Thu</summary>*/
        public bool? Thu { get; set; }
        /*<summary>The get and set for Fri</summary>*/
        public bool? Fri { get; set; }
        /*<summary>The get and set for Sat</summary>*/
        public bool? Sat { get; set; }
        /*<summary>The get and set for BldgName</summary>*/
        public string BldgName { get; set; }
        /*<summary>The get and set for BldgTitle</summary>*/
        public string BldgTitle { get; set; }
        /*<summary>The get and set for BldgEmail</summary>*/
        public string BldgEmail { get; set; }
        /*<summary>The get and set for BldgComments</summary>*/
        public string BldgComments { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool? ForeignPhone { get; set; }
        /*<summary>The get and set for ForeignFax</summary>*/
        public bool? ForeignFax { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool? ForeignZip { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArRooms</summary>*/
        public virtual ICollection<ArRooms>ArRooms { get; set; }
    }
}