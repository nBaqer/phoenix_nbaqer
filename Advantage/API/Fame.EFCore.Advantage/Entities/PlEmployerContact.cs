﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlEmployerContact.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlEmployerContact definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlEmployerContact</summary>*/
    public partial class PlEmployerContact
    {
        /*<summary>The constructor for PlEmployerContact</summary>*/
        public PlEmployerContact()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
        }

        /*<summary>The get and set for EmployerContactId</summary>*/
        public Guid EmployerContactId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for PrefixId</summary>*/
        public Guid? PrefixId { get; set; }
        /*<summary>The get and set for SuffixId</summary>*/
        public Guid? SuffixId { get; set; }
        /*<summary>The get and set for WorkPhone</summary>*/
        public string WorkPhone { get; set; }
        /*<summary>The get and set for HomePhone</summary>*/
        public string HomePhone { get; set; }
        /*<summary>The get and set for CellPhone</summary>*/
        public string CellPhone { get; set; }
        /*<summary>The get and set for Beeper</summary>*/
        public string Beeper { get; set; }
        /*<summary>The get and set for WorkEmail</summary>*/
        public string WorkEmail { get; set; }
        /*<summary>The get and set for HomeEmail</summary>*/
        public string HomeEmail { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for State</summary>*/
        public Guid? State { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for Country</summary>*/
        public Guid? Country { get; set; }
        /*<summary>The get and set for WorkExt</summary>*/
        public string WorkExt { get; set; }
        /*<summary>The get and set for WorkBestTime</summary>*/
        public string WorkBestTime { get; set; }
        /*<summary>The get and set for HomeExt</summary>*/
        public string HomeExt { get; set; }
        /*<summary>The get and set for HomeBestTime</summary>*/
        public string HomeBestTime { get; set; }
        /*<summary>The get and set for CellBestTime</summary>*/
        public string CellBestTime { get; set; }
        /*<summary>The get and set for PinNumber</summary>*/
        public string PinNumber { get; set; }
        /*<summary>The get and set for Notes</summary>*/
        public string Notes { get; set; }
        /*<summary>The get and set for AddressTypeId</summary>*/
        public Guid? AddressTypeId { get; set; }
        /*<summary>The get and set for EmployerId</summary>*/
        public Guid? EmployerId { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool? ForeignZip { get; set; }
        /*<summary>The get and set for ForeignHomePhone</summary>*/
        public bool? ForeignHomePhone { get; set; }
        /*<summary>The get and set for ForeignCellPhone</summary>*/
        public bool? ForeignCellPhone { get; set; }
        /*<summary>The get and set for ForeignWorkPhone</summary>*/
        public bool? ForeignWorkPhone { get; set; }
        /*<summary>The get and set for TitleId</summary>*/
        public string TitleId { get; set; }

        /*<summary>The navigational property for AddressType</summary>*/
        public virtual PlAddressTypes AddressType { get; set; }
        /*<summary>The navigational property for CountryNavigation</summary>*/
        public virtual AdCountries CountryNavigation { get; set; }
        /*<summary>The navigational property for Employer</summary>*/
        public virtual PlEmployers Employer { get; set; }
        /*<summary>The navigational property for Prefix</summary>*/
        public virtual SyPrefixes Prefix { get; set; }
        /*<summary>The navigational property for StateNavigation</summary>*/
        public virtual SyStates StateNavigation { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Suffix</summary>*/
        public virtual SySuffixes Suffix { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
    }
}