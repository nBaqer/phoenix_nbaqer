﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyGrdPolicies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyGrdPolicies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyGrdPolicies</summary>*/
    public partial class SyGrdPolicies
    {
        /*<summary>The constructor for SyGrdPolicies</summary>*/
        public SyGrdPolicies()
        {
            ArGrdBkWgtDetails = new HashSet<ArGrdBkWgtDetails>();
            SyGrdPolicyParams = new HashSet<SyGrdPolicyParams>();
        }

        /*<summary>The get and set for GrdPolicyId</summary>*/
        public int GrdPolicyId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for AllowParam</summary>*/
        public bool AllowParam { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for ArGrdBkWgtDetails</summary>*/
        public virtual ICollection<ArGrdBkWgtDetails>ArGrdBkWgtDetails { get; set; }
        /*<summary>The navigational property for SyGrdPolicyParams</summary>*/
        public virtual ICollection<SyGrdPolicyParams>SyGrdPolicyParams { get; set; }
    }
}