﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobCats.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobCats definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobCats</summary>*/
    public partial class PlJobCats
    {
        /*<summary>The constructor for PlJobCats</summary>*/
        public PlJobCats()
        {
            AdTitles = new HashSet<AdTitles>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
        }

        /*<summary>The get and set for JobCatId</summary>*/
        public Guid JobCatId { get; set; }
        /*<summary>The get and set for JobCatCode</summary>*/
        public string JobCatCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for JobCatDescrip</summary>*/
        public string JobCatDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdTitles</summary>*/
        public virtual ICollection<AdTitles>AdTitles { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
    }
}