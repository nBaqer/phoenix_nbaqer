﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTrigOffsetTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTrigOffsetTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTrigOffsetTyps</summary>*/
    public partial class ArTrigOffsetTyps
    {
        /*<summary>The constructor for ArTrigOffsetTyps</summary>*/
        public ArTrigOffsetTyps()
        {
            ArSap = new HashSet<ArSap>();
            ArSapdetails = new HashSet<ArSapdetails>();
        }

        /*<summary>The get and set for TrigOffsetTypId</summary>*/
        public byte TrigOffsetTypId { get; set; }
        /*<summary>The get and set for TrigOffTypDescrip</summary>*/
        public string TrigOffTypDescrip { get; set; }

        /*<summary>The navigational property for ArSap</summary>*/
        public virtual ICollection<ArSap>ArSap { get; set; }
        /*<summary>The navigational property for ArSapdetails</summary>*/
        public virtual ICollection<ArSapdetails>ArSapdetails { get; set; }
    }
}