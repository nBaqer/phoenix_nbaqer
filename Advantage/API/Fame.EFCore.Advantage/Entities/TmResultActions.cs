﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmResultActions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmResultActions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmResultActions</summary>*/
    public partial class TmResultActions
    {
        /*<summary>The get and set for ResultActionId</summary>*/
        public Guid ResultActionId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for Tablename</summary>*/
        public string Tablename { get; set; }
        /*<summary>The get and set for ColumnName</summary>*/
        public string ColumnName { get; set; }
        /*<summary>The get and set for RowGuid</summary>*/
        public string RowGuid { get; set; }
        /*<summary>The get and set for ValuesQuery</summary>*/
        public string ValuesQuery { get; set; }
    }
}