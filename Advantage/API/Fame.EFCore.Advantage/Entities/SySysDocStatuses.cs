﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySysDocStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySysDocStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySysDocStatuses</summary>*/
    public partial class SySysDocStatuses
    {
        /*<summary>The get and set for SysDocStatusId</summary>*/
        public byte SysDocStatusId { get; set; }
        /*<summary>The get and set for DocStatusDescrip</summary>*/
        public string DocStatusDescrip { get; set; }
    }
}