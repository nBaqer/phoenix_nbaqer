﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPositions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPositions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPositions</summary>*/
    public partial class SyPositions
    {
        /*<summary>The constructor for SyPositions</summary>*/
        public SyPositions()
        {
            HrEmpHrinfo = new HashSet<HrEmpHrinfo>();
        }

        /*<summary>The get and set for PositionId</summary>*/
        public Guid PositionId { get; set; }
        /*<summary>The get and set for PositionCode</summary>*/
        public string PositionCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for PositionDescrip</summary>*/
        public string PositionDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for HrEmpHrinfo</summary>*/
        public virtual ICollection<HrEmpHrinfo>HrEmpHrinfo { get; set; }
    }
}