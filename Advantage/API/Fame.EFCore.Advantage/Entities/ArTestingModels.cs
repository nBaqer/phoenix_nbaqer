﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTestingModels.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTestingModels definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTestingModels</summary>*/
    public partial class ArTestingModels
    {
        /*<summary>The get and set for TestingModelId</summary>*/
        public byte TestingModelId { get; set; }
        /*<summary>The get and set for TestingModelDescrip</summary>*/
        public string TestingModelDescrip { get; set; }
    }
}