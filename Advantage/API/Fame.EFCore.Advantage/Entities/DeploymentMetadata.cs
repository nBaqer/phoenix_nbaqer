﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="DeploymentMetadata.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The DeploymentMetadata definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for DeploymentMetadata</summary>*/
    public partial class DeploymentMetadata
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Name</summary>*/
        public string Name { get; set; }
        /*<summary>The get and set for Type</summary>*/
        public string Type { get; set; }
        /*<summary>The get and set for Action</summary>*/
        public string Action { get; set; }
        /*<summary>The get and set for By</summary>*/
        public string By { get; set; }
        /*<summary>The get and set for As</summary>*/
        public string As { get; set; }
        /*<summary>The get and set for CompletedDate</summary>*/
        public DateTime CompletedDate { get; set; }
        /*<summary>The get and set for With</summary>*/
        public string With { get; set; }
        /*<summary>The get and set for BlockId</summary>*/
        public string BlockId { get; set; }
        /*<summary>The get and set for InsertedSerial</summary>*/
        public byte[] InsertedSerial { get; set; }
        /*<summary>The get and set for UpdatedSerial</summary>*/
        public byte[] UpdatedSerial { get; set; }
        /*<summary>The get and set for MetadataVersion</summary>*/
        public string MetadataVersion { get; set; }
        /*<summary>The get and set for Hash</summary>*/
        public string Hash { get; set; }
    }
}