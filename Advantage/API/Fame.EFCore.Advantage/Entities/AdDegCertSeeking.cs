﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdDegCertSeeking.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdDegCertSeeking definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdDegCertSeeking</summary>*/
    public partial class AdDegCertSeeking
    {
        /*<summary>The constructor for AdDegCertSeeking</summary>*/
        public AdDegCertSeeking()
        {
            AdLeads = new HashSet<AdLeads>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
        }

        /*<summary>The get and set for DegCertSeekingId</summary>*/
        public Guid DegCertSeekingId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
    }
}