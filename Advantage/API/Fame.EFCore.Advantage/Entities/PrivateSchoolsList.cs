﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PrivateSchoolsList.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PrivateSchoolsList definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PrivateSchoolsList</summary>*/
    public partial class PrivateSchoolsList
    {
        /*<summary>The get and set for NcesId</summary>*/
        public string NcesId { get; set; }
        /*<summary>The get and set for SchoolLevel</summary>*/
        public string SchoolLevel { get; set; }
        /*<summary>The get and set for SchoolName</summary>*/
        public string SchoolName { get; set; }
        /*<summary>The get and set for Address</summary>*/
        public string Address { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for State</summary>*/
        public string State { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for AnsiFipsCountyNumber</summary>*/
        public string AnsiFipsCountyNumber { get; set; }
        /*<summary>The get and set for SchoolCounty</summary>*/
        public string SchoolCounty { get; set; }
        /*<summary>The get and set for StateFips</summary>*/
        public string StateFips { get; set; }
        /*<summary>The get and set for LowGrade</summary>*/
        public string LowGrade { get; set; }
        /*<summary>The get and set for HighGrade</summary>*/
        public string HighGrade { get; set; }
        /*<summary>The get and set for StudentsUg</summary>*/
        public string StudentsUg { get; set; }
        /*<summary>The get and set for StudentsPrek</summary>*/
        public string StudentsPrek { get; set; }
        /*<summary>The get and set for StudentsK</summary>*/
        public string StudentsK { get; set; }
        /*<summary>The get and set for StudentsTk</summary>*/
        public string StudentsTk { get; set; }
        /*<summary>The get and set for StudentsT1</summary>*/
        public string StudentsT1 { get; set; }
        /*<summary>The get and set for Students1</summary>*/
        public string Students1 { get; set; }
        /*<summary>The get and set for Students2</summary>*/
        public string Students2 { get; set; }
        /*<summary>The get and set for Students3</summary>*/
        public string Students3 { get; set; }
        /*<summary>The get and set for Students4</summary>*/
        public string Students4 { get; set; }
        /*<summary>The get and set for Students5</summary>*/
        public string Students5 { get; set; }
        /*<summary>The get and set for Students6</summary>*/
        public string Students6 { get; set; }
        /*<summary>The get and set for Students7</summary>*/
        public string Students7 { get; set; }
        /*<summary>The get and set for Students8</summary>*/
        public string Students8 { get; set; }
        /*<summary>The get and set for Students9</summary>*/
        public string Students9 { get; set; }
        /*<summary>The get and set for Students10</summary>*/
        public string Students10 { get; set; }
        /*<summary>The get and set for Students11</summary>*/
        public string Students11 { get; set; }
        /*<summary>The get and set for Students12</summary>*/
        public string Students12 { get; set; }
        /*<summary>The get and set for TotalStudents</summary>*/
        public string TotalStudents { get; set; }
        /*<summary>The get and set for TotalAmericanIndianAlaskan</summary>*/
        public string TotalAmericanIndianAlaskan { get; set; }
        /*<summary>The get and set for TotalAsian</summary>*/
        public string TotalAsian { get; set; }
        /*<summary>The get and set for TotalHispanic</summary>*/
        public string TotalHispanic { get; set; }
        /*<summary>The get and set for TotalBlack</summary>*/
        public string TotalBlack { get; set; }
        /*<summary>The get and set for TotalWhite</summary>*/
        public string TotalWhite { get; set; }
        /*<summary>The get and set for FullTimeEquivalentTeachers</summary>*/
        public string FullTimeEquivalentTeachers { get; set; }
        /*<summary>The get and set for StudentTeacherRatio</summary>*/
        public string StudentTeacherRatio { get; set; }
        /*<summary>The get and set for Coed</summary>*/
        public string Coed { get; set; }
        /*<summary>The get and set for SchoolType</summary>*/
        public string SchoolType { get; set; }
        /*<summary>The get and set for ReligiousAffiliation</summary>*/
        public string ReligiousAffiliation { get; set; }
        /*<summary>The get and set for Library</summary>*/
        public string Library { get; set; }
        /*<summary>The get and set for Latitude</summary>*/
        public string Latitude { get; set; }
        /*<summary>The get and set for Longitude</summary>*/
        public string Longitude { get; set; }
        /*<summary>The get and set for OfferedUg</summary>*/
        public string OfferedUg { get; set; }
        /*<summary>The get and set for OfferedPrek</summary>*/
        public string OfferedPrek { get; set; }
        /*<summary>The get and set for OfferedK</summary>*/
        public string OfferedK { get; set; }
        /*<summary>The get and set for OfferedTk</summary>*/
        public string OfferedTk { get; set; }
        /*<summary>The get and set for OfferedT1</summary>*/
        public string OfferedT1 { get; set; }
        /*<summary>The get and set for Offered1</summary>*/
        public string Offered1 { get; set; }
        /*<summary>The get and set for Offered2</summary>*/
        public string Offered2 { get; set; }
        /*<summary>The get and set for Offered3</summary>*/
        public string Offered3 { get; set; }
        /*<summary>The get and set for Offered4</summary>*/
        public string Offered4 { get; set; }
        /*<summary>The get and set for Offered5</summary>*/
        public string Offered5 { get; set; }
        /*<summary>The get and set for Offered6</summary>*/
        public string Offered6 { get; set; }
        /*<summary>The get and set for Offered7</summary>*/
        public string Offered7 { get; set; }
        /*<summary>The get and set for Offered8</summary>*/
        public string Offered8 { get; set; }
        /*<summary>The get and set for Offered9</summary>*/
        public string Offered9 { get; set; }
        /*<summary>The get and set for Offered10</summary>*/
        public string Offered10 { get; set; }
        /*<summary>The get and set for Offered11</summary>*/
        public string Offered11 { get; set; }
        /*<summary>The get and set for Offered12</summary>*/
        public string Offered12 { get; set; }
        /*<summary>The get and set for TotalPacific</summary>*/
        public string TotalPacific { get; set; }
        /*<summary>The get and set for Total2race</summary>*/
        public string Total2race { get; set; }
        /*<summary>The get and set for TotalMale</summary>*/
        public string TotalMale { get; set; }
        /*<summary>The get and set for TotalFemale</summary>*/
        public string TotalFemale { get; set; }
        /*<summary>The get and set for PercentTo4yearCollege</summary>*/
        public string PercentTo4yearCollege { get; set; }
        /*<summary>The get and set for PrekKT1DayLength</summary>*/
        public string PrekKT1DayLength { get; set; }
        /*<summary>The get and set for PrekKT1DaysPerWeek</summary>*/
        public string PrekKT1DaysPerWeek { get; set; }
        /*<summary>The get and set for MoreThan34TimeTeachers</summary>*/
        public string MoreThan34TimeTeachers { get; set; }
        /*<summary>The get and set for MoreThan12TimeTeachers</summary>*/
        public string MoreThan12TimeTeachers { get; set; }
        /*<summary>The get and set for MoreThan14TimeTeachers</summary>*/
        public string MoreThan14TimeTeachers { get; set; }
        /*<summary>The get and set for LessThan14TimeTeachers</summary>*/
        public string LessThan14TimeTeachers { get; set; }
        /*<summary>The get and set for TotalTeachers</summary>*/
        public string TotalTeachers { get; set; }
        /*<summary>The get and set for HomeSchooling</summary>*/
        public string HomeSchooling { get; set; }
        /*<summary>The get and set for LocatedInHome</summary>*/
        public string LocatedInHome { get; set; }
        /*<summary>The get and set for HasReligiousOrientation</summary>*/
        public string HasReligiousOrientation { get; set; }
        /*<summary>The get and set for AmericanMontessoriSociety</summary>*/
        public string AmericanMontessoriSociety { get; set; }
        /*<summary>The get and set for AssociationMontessoriInternational</summary>*/
        public string AssociationMontessoriInternational { get; set; }
        /*<summary>The get and set for OtherMontessoriAssociations</summary>*/
        public string OtherMontessoriAssociations { get; set; }
        /*<summary>The get and set for AssociationMilitaryCollegesAndSchools</summary>*/
        public string AssociationMilitaryCollegesAndSchools { get; set; }
        /*<summary>The get and set for AssociationWaldorfSchoolsOfNorthAmerica</summary>*/
        public string AssociationWaldorfSchoolsOfNorthAmerica { get; set; }
        /*<summary>The get and set for NationalAssociationOfPrivateSpecialEduCenters</summary>*/
        public string NationalAssociationOfPrivateSpecialEduCenters { get; set; }
        /*<summary>The get and set for OtherAssociationsForExceptionalChildren</summary>*/
        public string OtherAssociationsForExceptionalChildren { get; set; }
        /*<summary>The get and set for EuropeanCouncilForInternationalSchools</summary>*/
        public string EuropeanCouncilForInternationalSchools { get; set; }
        /*<summary>The get and set for NationalAssociationForTheEduOfYoungChildren</summary>*/
        public string NationalAssociationForTheEduOfYoungChildren { get; set; }
        /*<summary>The get and set for NationalAssociationOfLaboratorySchools</summary>*/
        public string NationalAssociationOfLaboratorySchools { get; set; }
        /*<summary>The get and set for NationalCoalitionOfGirlsSchools</summary>*/
        public string NationalCoalitionOfGirlsSchools { get; set; }
        /*<summary>The get and set for OtherSpecialEmphasisAssociations</summary>*/
        public string OtherSpecialEmphasisAssociations { get; set; }
        /*<summary>The get and set for AlternativeSchoolNetwork</summary>*/
        public string AlternativeSchoolNetwork { get; set; }
        /*<summary>The get and set for NationalAssociationOfIndependentSchools</summary>*/
        public string NationalAssociationOfIndependentSchools { get; set; }
        /*<summary>The get and set for StateOrRegionalIndependentSchoolAssociation</summary>*/
        public string StateOrRegionalIndependentSchoolAssociation { get; set; }
        /*<summary>The get and set for NationalCoalitionOfAlternativeCommunitySchools</summary>*/
        public string NationalCoalitionOfAlternativeCommunitySchools { get; set; }
        /*<summary>The get and set for NationalIndependentPrivateSchoolsAssociation</summary>*/
        public string NationalIndependentPrivateSchoolsAssociation { get; set; }
        /*<summary>The get and set for TheAssociationOfBoardingSchools</summary>*/
        public string TheAssociationOfBoardingSchools { get; set; }
        /*<summary>The get and set for OtherSchoolAssociations</summary>*/
        public string OtherSchoolAssociations { get; set; }
        /*<summary>The get and set for DaysInSchoolYear</summary>*/
        public string DaysInSchoolYear { get; set; }
        /*<summary>The get and set for HoursInDay</summary>*/
        public string HoursInDay { get; set; }
        /*<summary>The get and set for MinutesInDay</summary>*/
        public string MinutesInDay { get; set; }
        /*<summary>The get and set for MailingAddress</summary>*/
        public string MailingAddress { get; set; }
        /*<summary>The get and set for MailingCity</summary>*/
        public string MailingCity { get; set; }
        /*<summary>The get and set for MailingState</summary>*/
        public string MailingState { get; set; }
        /*<summary>The get and set for MailingZip</summary>*/
        public string MailingZip { get; set; }
        /*<summary>The get and set for MailingZip4</summary>*/
        public string MailingZip4 { get; set; }
        /*<summary>The get and set for Zip4</summary>*/
        public string Zip4 { get; set; }
        /*<summary>The get and set for InstId</summary>*/
        public int InstId { get; set; }
        /*<summary>The get and set for InstitutionId</summary>*/
        public Guid? InstitutionId { get; set; }
    }
}