﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWidgetResourceRoles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWidgetResourceRoles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWidgetResourceRoles</summary>*/
    public partial class SyWidgetResourceRoles
    {
        /*<summary>The get and set for WidgetResourceRoleId</summary>*/
        public Guid WidgetResourceRoleId { get; set; }
        /*<summary>The get and set for WidgetId</summary>*/
        public Guid WidgetId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for SysRoleId</summary>*/
        public int? SysRoleId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SysRole</summary>*/
        public virtual SySysRoles SysRole { get; set; }
        /*<summary>The navigational property for Widget</summary>*/
        public virtual SyWidgets Widget { get; set; }
    }
}