﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTransTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTransTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTransTypes</summary>*/
    public partial class SaTransTypes
    {
        /*<summary>The constructor for SaTransTypes</summary>*/
        public SaTransTypes()
        {
            AdLeadTransactions = new HashSet<AdLeadTransactions>();
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for TransTypeId</summary>*/
        public int TransTypeId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for AdLeadTransactions</summary>*/
        public virtual ICollection<AdLeadTransactions>AdLeadTransactions { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}