﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlStudentsPlaced.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlStudentsPlaced definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlStudentsPlaced</summary>*/
    public partial class PlStudentsPlaced
    {
        /*<summary>The get and set for PlacementId</summary>*/
        public Guid PlacementId { get; set; }
        /*<summary>The get and set for Supervisor</summary>*/
        public string Supervisor { get; set; }
        /*<summary>The get and set for JobStatusId</summary>*/
        public Guid? JobStatusId { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for Salary</summary>*/
        public decimal? Salary { get; set; }
        /*<summary>The get and set for TerminationReason</summary>*/
        public string TerminationReason { get; set; }
        /*<summary>The get and set for TerminationDate</summary>*/
        public DateTime? TerminationDate { get; set; }
        /*<summary>The get and set for Notes</summary>*/
        public string Notes { get; set; }
        /*<summary>The get and set for WorkDaysId</summary>*/
        public Guid? WorkDaysId { get; set; }
        /*<summary>The get and set for BenefitsId</summary>*/
        public Guid? BenefitsId { get; set; }
        /*<summary>The get and set for JobDescrip</summary>*/
        public string JobDescrip { get; set; }
        /*<summary>The get and set for PlacementRep</summary>*/
        public Guid? PlacementRep { get; set; }
        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid? ScheduleId { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for SalaryTypeId</summary>*/
        public Guid? SalaryTypeId { get; set; }
        /*<summary>The get and set for PlacedDate</summary>*/
        public DateTime? PlacedDate { get; set; }
        /*<summary>The get and set for InterviewId</summary>*/
        public Guid? InterviewId { get; set; }
        /*<summary>The get and set for FldStudyId</summary>*/
        public Guid? FldStudyId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for Fee</summary>*/
        public Guid? Fee { get; set; }
        /*<summary>The get and set for HowPlacedId</summary>*/
        public Guid? HowPlacedId { get; set; }
        /*<summary>The get and set for GraduatedDate</summary>*/
        public DateTime? GraduatedDate { get; set; }
        /*<summary>The get and set for EmployerJobId</summary>*/
        public Guid? EmployerJobId { get; set; }

        /*<summary>The navigational property for Benefits</summary>*/
        public virtual PlJobBenefit Benefits { get; set; }
        /*<summary>The navigational property for EmployerJob</summary>*/
        public virtual PlEmployerJobs EmployerJob { get; set; }
        /*<summary>The navigational property for FeeNavigation</summary>*/
        public virtual PlFee FeeNavigation { get; set; }
        /*<summary>The navigational property for FldStudy</summary>*/
        public virtual PlFldStudy FldStudy { get; set; }
        /*<summary>The navigational property for HowPlaced</summary>*/
        public virtual PlHowPlaced HowPlaced { get; set; }
        /*<summary>The navigational property for Interview</summary>*/
        public virtual PlInterview Interview { get; set; }
        /*<summary>The navigational property for JobStatus</summary>*/
        public virtual PlJobStatus JobStatus { get; set; }
        /*<summary>The navigational property for PlacementRepNavigation</summary>*/
        public virtual SyUsers PlacementRepNavigation { get; set; }
        /*<summary>The navigational property for SalaryType</summary>*/
        public virtual PlSalaryType SalaryType { get; set; }
        /*<summary>The navigational property for Schedule</summary>*/
        public virtual PlJobSchedule Schedule { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for WorkDays</summary>*/
        public virtual PlJobWorkDays WorkDays { get; set; }
    }
}