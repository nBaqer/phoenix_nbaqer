﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFameEspawardCutOffDate.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFameEspawardCutOffDate definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFameEspawardCutOffDate</summary>*/
    public partial class SyFameEspawardCutOffDate
    {
        /*<summary>The get and set for AwardCutOffDateGuid</summary>*/
        public Guid AwardCutOffDateGuid { get; set; }
        /*<summary>The get and set for AwardCutOffDateCount</summary>*/
        public int? AwardCutOffDateCount { get; set; }
    }
}