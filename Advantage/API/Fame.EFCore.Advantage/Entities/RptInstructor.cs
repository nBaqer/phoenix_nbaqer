﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="RptInstructor.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The RptInstructor definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for RptInstructor</summary>*/
    public partial class RptInstructor
    {
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid InstructorId { get; set; }
        /*<summary>The get and set for InstructorDescrip</summary>*/
        public string InstructorDescrip { get; set; }
    }
}