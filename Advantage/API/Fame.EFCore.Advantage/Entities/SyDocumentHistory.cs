﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyDocumentHistory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyDocumentHistory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyDocumentHistory</summary>*/
    public partial class SyDocumentHistory
    {
        /*<summary>The get and set for FileId</summary>*/
        public Guid FileId { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for FileExtension</summary>*/
        public string FileExtension { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for DocumentType</summary>*/
        public string DocumentType { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }
        /*<summary>The get and set for FileNameOnly</summary>*/
        public string FileNameOnly { get; set; }
        /*<summary>The get and set for DocumentId</summary>*/
        public Guid? DocumentId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int? ModuleId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for DisplayName</summary>*/
        public string DisplayName { get; set; }
        /*<summary>The get and set for IsArchived</summary>*/
        public bool? IsArchived { get; set; }

        /*<summary>The navigational property for Document</summary>*/
        public virtual AdReqs Document { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}