﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCertifications.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCertifications definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCertifications</summary>*/
    public partial class SyCertifications
    {
        /*<summary>The constructor for SyCertifications</summary>*/
        public SyCertifications()
        {
            HrEmpCerts = new HashSet<HrEmpCerts>();
        }

        /*<summary>The get and set for CertificationId</summary>*/
        public Guid CertificationId { get; set; }
        /*<summary>The get and set for CertificationCode</summary>*/
        public string CertificationCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CertificationDescrip</summary>*/
        public string CertificationDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for HrEmpCerts</summary>*/
        public virtual ICollection<HrEmpCerts>HrEmpCerts { get; set; }
    }
}