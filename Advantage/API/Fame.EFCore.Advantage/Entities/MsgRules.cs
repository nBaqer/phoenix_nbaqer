﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="MsgRules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The MsgRules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for MsgRules</summary>*/
    public partial class MsgRules
    {
        /*<summary>The get and set for RuleId</summary>*/
        public Guid RuleId { get; set; }
        /*<summary>The get and set for Type</summary>*/
        public short? Type { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGroupId</summary>*/
        public Guid? CampGroupId { get; set; }
        /*<summary>The get and set for RuleSql</summary>*/
        public string RuleSql { get; set; }
        /*<summary>The get and set for TemplateId</summary>*/
        public Guid? TemplateId { get; set; }
        /*<summary>The get and set for RecipientType</summary>*/
        public string RecipientType { get; set; }
        /*<summary>The get and set for ReType</summary>*/
        public string ReType { get; set; }
        /*<summary>The get and set for DeliveryType</summary>*/
        public string DeliveryType { get; set; }
        /*<summary>The get and set for LastRun</summary>*/
        public DateTime? LastRun { get; set; }
        /*<summary>The get and set for LastRunError</summary>*/
        public string LastRunError { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }

        /*<summary>The navigational property for CampGroup</summary>*/
        public virtual SyCampGrps CampGroup { get; set; }
        /*<summary>The navigational property for Template</summary>*/
        public virtual MsgTemplates Template { get; set; }
    }
}