﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgMinorCerts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgMinorCerts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgMinorCerts</summary>*/
    public partial class ArPrgMinorCerts
    {
        /*<summary>The get and set for PrgMinorCertId</summary>*/
        public Guid PrgMinorCertId { get; set; }
        /*<summary>The get and set for ParentId</summary>*/
        public Guid ParentId { get; set; }
        /*<summary>The get and set for ChildId</summary>*/
        public Guid ChildId { get; set; }

        /*<summary>The navigational property for Child</summary>*/
        public virtual ArPrgVersions Child { get; set; }
        /*<summary>The navigational property for Parent</summary>*/
        public virtual ArPrgVersions Parent { get; set; }
    }
}