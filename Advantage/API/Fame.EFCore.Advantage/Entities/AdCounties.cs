﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdCounties.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdCounties definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdCounties</summary>*/
    public partial class AdCounties
    {
        /*<summary>The constructor for AdCounties</summary>*/
        public AdCounties()
        {
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeads = new HashSet<AdLeads>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlEmployers = new HashSet<PlEmployers>();
            PlExitInterview = new HashSet<PlExitInterview>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
        }

        /*<summary>The get and set for CountyId</summary>*/
        public Guid CountyId { get; set; }
        /*<summary>The get and set for CountyCode</summary>*/
        public string CountyCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CountyDescrip</summary>*/
        public string CountyDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Imported</summary>*/
        public bool? Imported { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
    }
}