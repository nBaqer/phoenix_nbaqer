﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStdSuspensions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStdSuspensions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStdSuspensions</summary>*/
    public partial class ArStdSuspensions
    {
        /*<summary>The get and set for StuSuspensionId</summary>*/
        public Guid StuSuspensionId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime EndDate { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for StudentStatusChangeId</summary>*/
        public Guid? StudentStatusChangeId { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}