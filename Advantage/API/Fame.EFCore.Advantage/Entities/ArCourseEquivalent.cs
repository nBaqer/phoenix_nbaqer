﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCourseEquivalent.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCourseEquivalent definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCourseEquivalent</summary>*/
    public partial class ArCourseEquivalent
    {
        /*<summary>The get and set for ReqEquivalentId</summary>*/
        public Guid ReqEquivalentId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid? ReqId { get; set; }
        /*<summary>The get and set for EquivReqId</summary>*/
        public Guid? EquivReqId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for EquivReq</summary>*/
        public virtual ArReqs EquivReq { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
    }
}