﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStudentClockAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStudentClockAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStudentClockAttendance</summary>*/
    public partial class ArStudentClockAttendance
    {
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ScheduleId</summary>*/
        public Guid ScheduleId { get; set; }
        /*<summary>The get and set for RecordDate</summary>*/
        public DateTime RecordDate { get; set; }
        /*<summary>The get and set for SchedHours</summary>*/
        public decimal? SchedHours { get; set; }
        /*<summary>The get and set for ActualHours</summary>*/
        public decimal? ActualHours { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsTardy</summary>*/
        public bool? IsTardy { get; set; }
        /*<summary>The get and set for PostByException</summary>*/
        public string PostByException { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for TardyProcessed</summary>*/
        public bool? TardyProcessed { get; set; }
        /*<summary>The get and set for Converted</summary>*/
        public bool Converted { get; set; }
        /*<summary>The get and set for SchedHoursOnTermination</summary>*/
        public decimal? SchedHoursOnTermination { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}