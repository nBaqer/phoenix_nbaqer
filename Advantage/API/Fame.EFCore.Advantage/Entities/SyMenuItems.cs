﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMenuItems.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMenuItems definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMenuItems</summary>*/
    public partial class SyMenuItems
    {
        /*<summary>The constructor for SyMenuItems</summary>*/
        public SyMenuItems()
        {
            InverseParent = new HashSet<SyMenuItems>();
        }

        /*<summary>The get and set for MenuItemId</summary>*/
        public int MenuItemId { get; set; }
        /*<summary>The get and set for MenuName</summary>*/
        public string MenuName { get; set; }
        /*<summary>The get and set for DisplayName</summary>*/
        public string DisplayName { get; set; }
        /*<summary>The get and set for Url</summary>*/
        public string Url { get; set; }
        /*<summary>The get and set for MenuItemTypeId</summary>*/
        public short MenuItemTypeId { get; set; }
        /*<summary>The get and set for ParentId</summary>*/
        public int? ParentId { get; set; }
        /*<summary>The get and set for DisplayOrder</summary>*/
        public int? DisplayOrder { get; set; }
        /*<summary>The get and set for IsPopup</summary>*/
        public bool IsPopup { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool IsActive { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for HierarchyId</summary>*/
        public Guid? HierarchyId { get; set; }
        /*<summary>The get and set for ModuleCode</summary>*/
        public string ModuleCode { get; set; }
        /*<summary>The get and set for Mrutype</summary>*/
        public int? Mrutype { get; set; }
        /*<summary>The get and set for HideStatusBar</summary>*/
        public bool? HideStatusBar { get; set; }

        /*<summary>The navigational property for MenuItemType</summary>*/
        public virtual SyMenuItemType MenuItemType { get; set; }
        /*<summary>The navigational property for Parent</summary>*/
        public virtual SyMenuItems Parent { get; set; }
        /*<summary>The navigational property for InverseParent</summary>*/
        public virtual ICollection<SyMenuItems>InverseParent { get; set; }
    }
}