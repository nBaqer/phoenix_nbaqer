﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTblFlds.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTblFlds definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTblFlds</summary>*/
    public partial class SyTblFlds
    {
        /*<summary>The constructor for SyTblFlds</summary>*/
        public SyTblFlds()
        {
            SyResTblFlds = new HashSet<SyResTblFlds>();
            SyRptAdHocFields = new HashSet<SyRptAdHocFields>();
            SyRptParams = new HashSet<SyRptParams>();
        }

        /*<summary>The get and set for TblFldsId</summary>*/
        public int TblFldsId { get; set; }
        /*<summary>The get and set for TblId</summary>*/
        public int? TblId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
        /*<summary>The get and set for CategoryId</summary>*/
        public int? CategoryId { get; set; }
        /*<summary>The get and set for FkcolDescrip</summary>*/
        public string FkcolDescrip { get; set; }

        /*<summary>The navigational property for Fld</summary>*/
        public virtual SyFields Fld { get; set; }
        /*<summary>The navigational property for Tbl</summary>*/
        public virtual SyTables Tbl { get; set; }
        /*<summary>The navigational property for SyResTblFlds</summary>*/
        public virtual ICollection<SyResTblFlds>SyResTblFlds { get; set; }
        /*<summary>The navigational property for SyRptAdHocFields</summary>*/
        public virtual ICollection<SyRptAdHocFields>SyRptAdHocFields { get; set; }
        /*<summary>The navigational property for SyRptParams</summary>*/
        public virtual ICollection<SyRptParams>SyRptParams { get; set; }
    }
}