﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptProps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptProps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptProps</summary>*/
    public partial class SyRptProps
    {
        /*<summary>The get and set for RptPropsId</summary>*/
        public short RptPropsId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short ResourceId { get; set; }
        /*<summary>The get and set for AllowParams</summary>*/
        public bool AllowParams { get; set; }
        /*<summary>The get and set for RptSqlid</summary>*/
        public short? RptSqlid { get; set; }
        /*<summary>The get and set for RptObjId</summary>*/
        public short? RptObjId { get; set; }
        /*<summary>The get and set for AllowDescrip</summary>*/
        public bool? AllowDescrip { get; set; }
        /*<summary>The get and set for AllowInstruct</summary>*/
        public bool? AllowInstruct { get; set; }
        /*<summary>The get and set for AllowNotes</summary>*/
        public bool? AllowNotes { get; set; }
        /*<summary>The get and set for AllowFilters</summary>*/
        public bool? AllowFilters { get; set; }
        /*<summary>The get and set for AllowSortOrder</summary>*/
        public bool? AllowSortOrder { get; set; }
        /*<summary>The get and set for SelectFilters</summary>*/
        public bool? SelectFilters { get; set; }
        /*<summary>The get and set for SelectSortOrder</summary>*/
        public bool SelectSortOrder { get; set; }
        /*<summary>The get and set for AllowDateIssue</summary>*/
        public bool AllowDateIssue { get; set; }
        /*<summary>The get and set for AllowStudentGroup</summary>*/
        public bool AllowStudentGroup { get; set; }
        /*<summary>The get and set for AllowProgramGroup</summary>*/
        public bool AllowProgramGroup { get; set; }
        /*<summary>The get and set for ShowClassDates</summary>*/
        public bool ShowClassDates { get; set; }
        /*<summary>The get and set for ShowProjExceedsBal</summary>*/
        public bool ShowProjExceedsBal { get; set; }
        /*<summary>The get and set for ShowCosts</summary>*/
        public bool ShowCosts { get; set; }
        /*<summary>The get and set for ShowExpectedFunding</summary>*/
        public bool ShowExpectedFunding { get; set; }
        /*<summary>The get and set for ShowCategoryBreakdown</summary>*/
        public bool ShowCategoryBreakdown { get; set; }
        /*<summary>The get and set for ShowEnrollmentGroup</summary>*/
        public bool ShowEnrollmentGroup { get; set; }
        /*<summary>The get and set for ShowDisbNotBeenPaid</summary>*/
        public bool ShowDisbNotBeenPaid { get; set; }
        /*<summary>The get and set for ShowLegalDisclaimer</summary>*/
        public bool ShowLegalDisclaimer { get; set; }
        /*<summary>The get and set for BaseCaonRegClasses</summary>*/
        public bool? BaseCaonRegClasses { get; set; }
        /*<summary>The get and set for ShowTransferCampus</summary>*/
        public bool? ShowTransferCampus { get; set; }
        /*<summary>The get and set for ShowTransferProgram</summary>*/
        public bool? ShowTransferProgram { get; set; }
        /*<summary>The get and set for ShowLda</summary>*/
        public bool? ShowLda { get; set; }
        /*<summary>The get and set for ShowUseStuCurrStatus</summary>*/
        public bool? ShowUseStuCurrStatus { get; set; }
        /*<summary>The get and set for ShowUseSignLineForAttnd</summary>*/
        public bool? ShowUseSignLineForAttnd { get; set; }
        /*<summary>The get and set for ShowTermProgressDescription</summary>*/
        public bool? ShowTermProgressDescription { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for RptObj</summary>*/
        public virtual SyBusObjects RptObj { get; set; }
    }
}