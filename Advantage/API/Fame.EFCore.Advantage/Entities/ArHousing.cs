﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArHousing.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArHousing definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArHousing</summary>*/
    public partial class ArHousing
    {
        /*<summary>The constructor for ArHousing</summary>*/
        public ArHousing()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for HousingId</summary>*/
        public Guid HousingId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }

        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}