﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyContactTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyContactTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyContactTypes</summary>*/
    public partial class SyContactTypes
    {
        /*<summary>The constructor for SyContactTypes</summary>*/
        public SyContactTypes()
        {
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
        }

        /*<summary>The get and set for ContactTypeId</summary>*/
        public Guid ContactTypeId { get; set; }
        /*<summary>The get and set for ContactTypeCode</summary>*/
        public string ContactTypeCode { get; set; }
        /*<summary>The get and set for ContactTypeDescrip</summary>*/
        public string ContactTypeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
    }
}