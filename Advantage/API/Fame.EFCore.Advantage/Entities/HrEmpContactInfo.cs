﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="HrEmpContactInfo.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The HrEmpContactInfo definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for HrEmpContactInfo</summary>*/
    public partial class HrEmpContactInfo
    {
        /*<summary>The get and set for EmpContactInfoId</summary>*/
        public Guid EmpContactInfoId { get; set; }
        /*<summary>The get and set for EmpId</summary>*/
        public Guid EmpId { get; set; }
        /*<summary>The get and set for WorkPhone</summary>*/
        public string WorkPhone { get; set; }
        /*<summary>The get and set for HomePhone</summary>*/
        public string HomePhone { get; set; }
        /*<summary>The get and set for CellPhone</summary>*/
        public string CellPhone { get; set; }
        /*<summary>The get and set for Beeper</summary>*/
        public string Beeper { get; set; }
        /*<summary>The get and set for WorkEmail</summary>*/
        public string WorkEmail { get; set; }
        /*<summary>The get and set for HomeEmail</summary>*/
        public string HomeEmail { get; set; }
        /*<summary>The get and set for ForeignHomePhone</summary>*/
        public bool ForeignHomePhone { get; set; }
        /*<summary>The get and set for ForeignWorkPhone</summary>*/
        public bool ForeignWorkPhone { get; set; }
        /*<summary>The get and set for ForeignCellPhone</summary>*/
        public bool ForeignCellPhone { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for Emp</summary>*/
        public virtual HrEmployees Emp { get; set; }
    }
}