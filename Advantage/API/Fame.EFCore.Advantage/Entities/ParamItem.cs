﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamItem.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamItem definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamItem</summary>*/
    public partial class ParamItem
    {
        /*<summary>The get and set for ItemId</summary>*/
        public long ItemId { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
        /*<summary>The get and set for ControllerClass</summary>*/
        public string ControllerClass { get; set; }
        /*<summary>The get and set for Valueprop</summary>*/
        public int Valueprop { get; set; }
        /*<summary>The get and set for ReturnValueName</summary>*/
        public string ReturnValueName { get; set; }
        /*<summary>The get and set for IsItemOverriden</summary>*/
        public bool? IsItemOverriden { get; set; }
    }
}