﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptFilterOtherPrefs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptFilterOtherPrefs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptFilterOtherPrefs</summary>*/
    public partial class SyRptFilterOtherPrefs
    {
        /*<summary>The get and set for FilterOtherPrefId</summary>*/
        public Guid FilterOtherPrefId { get; set; }
        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for RptParamId</summary>*/
        public string RptParamId { get; set; }
        /*<summary>The get and set for OpId</summary>*/
        public byte OpId { get; set; }
        /*<summary>The get and set for OpValue</summary>*/
        public string OpValue { get; set; }
        /*<summary>The get and set for AdHocFieldId</summary>*/
        public Guid? AdHocFieldId { get; set; }

        /*<summary>The navigational property for Pref</summary>*/
        public virtual SyRptUserPrefs Pref { get; set; }
    }
}