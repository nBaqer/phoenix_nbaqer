﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadImports.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadImports definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadImports</summary>*/
    public partial class AdLeadImports
    {
        /*<summary>The get and set for RecNum</summary>*/
        public string RecNum { get; set; }
        /*<summary>The get and set for CurrentEnrollStatus</summary>*/
        public string CurrentEnrollStatus { get; set; }
        /*<summary>The get and set for PrivTotalHrsAttended</summary>*/
        public string PrivTotalHrsAttended { get; set; }
        /*<summary>The get and set for PrivTotalHrsAbsent</summary>*/
        public string PrivTotalHrsAbsent { get; set; }
        /*<summary>The get and set for TotalHrsMakeup</summary>*/
        public string TotalHrsMakeup { get; set; }
        /*<summary>The get and set for LastMonthTotalHrs</summary>*/
        public string LastMonthTotalHrs { get; set; }
        /*<summary>The get and set for LastMonthAbsentHrs</summary>*/
        public string LastMonthAbsentHrs { get; set; }
        /*<summary>The get and set for LastMonthMakeupHrs</summary>*/
        public string LastMonthMakeupHrs { get; set; }
        /*<summary>The get and set for ScheduledHrsLda</summary>*/
        public string ScheduledHrsLda { get; set; }
        /*<summary>The get and set for MonthEndPellPaidHrs</summary>*/
        public string MonthEndPellPaidHrs { get; set; }
        /*<summary>The get and set for AttndSchdHrsMon</summary>*/
        public string AttndSchdHrsMon { get; set; }
        /*<summary>The get and set for SchdDailyHrs1</summary>*/
        public string SchdDailyHrs1 { get; set; }
        /*<summary>The get and set for SchdDailyHrs2</summary>*/
        public string SchdDailyHrs2 { get; set; }
        /*<summary>The get and set for SchdDailyHrs3</summary>*/
        public string SchdDailyHrs3 { get; set; }
        /*<summary>The get and set for SchdDailyHrs4</summary>*/
        public string SchdDailyHrs4 { get; set; }
        /*<summary>The get and set for SchdDailyHrs5</summary>*/
        public string SchdDailyHrs5 { get; set; }
        /*<summary>The get and set for SchdDailyHrs6</summary>*/
        public string SchdDailyHrs6 { get; set; }
        /*<summary>The get and set for SchdDailyHrs7</summary>*/
        public string SchdDailyHrs7 { get; set; }
        /*<summary>The get and set for AttndSchdHrsTue</summary>*/
        public string AttndSchdHrsTue { get; set; }
        /*<summary>The get and set for AttndSchdHrsWed</summary>*/
        public string AttndSchdHrsWed { get; set; }
        /*<summary>The get and set for AttndSchdHrsThr</summary>*/
        public string AttndSchdHrsThr { get; set; }
        /*<summary>The get and set for AttndSchdHrsFri</summary>*/
        public string AttndSchdHrsFri { get; set; }
        /*<summary>The get and set for AttndSchdHrsSat</summary>*/
        public string AttndSchdHrsSat { get; set; }
        /*<summary>The get and set for AttndSchdHrsSun</summary>*/
        public string AttndSchdHrsSun { get; set; }
        /*<summary>The get and set for PrivTransferedInHrs</summary>*/
        public string PrivTransferedInHrs { get; set; }
        /*<summary>The get and set for PrivOvtChargedAmt</summary>*/
        public string PrivOvtChargedAmt { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for AddressLine1</summary>*/
        public string AddressLine1 { get; set; }
        /*<summary>The get and set for AddressCity</summary>*/
        public string AddressCity { get; set; }
        /*<summary>The get and set for AddressState</summary>*/
        public string AddressState { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for PhoneNumAreaCode1</summary>*/
        public string PhoneNumAreaCode1 { get; set; }
        /*<summary>The get and set for PhoneNumPrefix1</summary>*/
        public string PhoneNumPrefix1 { get; set; }
        /*<summary>The get and set for PhoneNumBody1</summary>*/
        public string PhoneNumBody1 { get; set; }
        /*<summary>The get and set for CourseNumEnrolledIn</summary>*/
        public string CourseNumEnrolledIn { get; set; }
        /*<summary>The get and set for AbilityToBenefit</summary>*/
        public string AbilityToBenefit { get; set; }
        /*<summary>The get and set for PrivTclockSchdRecKey</summary>*/
        public string PrivTclockSchdRecKey { get; set; }
        /*<summary>The get and set for ClassIdNum</summary>*/
        public string ClassIdNum { get; set; }
        /*<summary>The get and set for TclockBadgeNum</summary>*/
        public string TclockBadgeNum { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public string StartDate { get; set; }
        /*<summary>The get and set for EnrolledDate</summary>*/
        public string EnrolledDate { get; set; }
        /*<summary>The get and set for ContractedGradDt</summary>*/
        public string ContractedGradDt { get; set; }
        /*<summary>The get and set for RevisedGradDate</summary>*/
        public string RevisedGradDate { get; set; }
        /*<summary>The get and set for LastDateAttended</summary>*/
        public string LastDateAttended { get; set; }
        /*<summary>The get and set for EndLeaveDate</summary>*/
        public string EndLeaveDate { get; set; }
        /*<summary>The get and set for BeginLeaveDate</summary>*/
        public string BeginLeaveDate { get; set; }
        /*<summary>The get and set for BeginProbationDate</summary>*/
        public string BeginProbationDate { get; set; }
        /*<summary>The get and set for PrevEnrollEndDate</summary>*/
        public string PrevEnrollEndDate { get; set; }
        /*<summary>The get and set for ReEnrolledDate</summary>*/
        public string ReEnrolledDate { get; set; }
        /*<summary>The get and set for EntranceinterviewDate</summary>*/
        public string EntranceinterviewDate { get; set; }
        /*<summary>The get and set for FullPartTimeRop</summary>*/
        public string FullPartTimeRop { get; set; }
        /*<summary>The get and set for StateRegId</summary>*/
        public string StateRegId { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for PaymentRecKey</summary>*/
        public string PaymentRecKey { get; set; }
        /*<summary>The get and set for GradRecKey</summary>*/
        public string GradRecKey { get; set; }
        /*<summary>The get and set for FinAidRecKey</summary>*/
        public string FinAidRecKey { get; set; }
        /*<summary>The get and set for PrivCurBalOwed</summary>*/
        public string PrivCurBalOwed { get; set; }
        /*<summary>The get and set for PrivCourseTuitCost</summary>*/
        public string PrivCourseTuitCost { get; set; }
        /*<summary>The get and set for PrivCourseRegCost</summary>*/
        public string PrivCourseRegCost { get; set; }
        /*<summary>The get and set for PrivCourseKitCost</summary>*/
        public string PrivCourseKitCost { get; set; }
        /*<summary>The get and set for PrivCourseOthrCost</summary>*/
        public string PrivCourseOthrCost { get; set; }
        /*<summary>The get and set for AccruedTuit</summary>*/
        public string AccruedTuit { get; set; }
        /*<summary>The get and set for AccruedReg</summary>*/
        public string AccruedReg { get; set; }
        /*<summary>The get and set for AccruedKit</summary>*/
        public string AccruedKit { get; set; }
        /*<summary>The get and set for AccruedOthr</summary>*/
        public string AccruedOthr { get; set; }
        /*<summary>The get and set for BirthDate</summary>*/
        public string BirthDate { get; set; }
        /*<summary>The get and set for TermResKey</summary>*/
        public string TermResKey { get; set; }
        /*<summary>The get and set for SubjectResKey</summary>*/
        public string SubjectResKey { get; set; }
        /*<summary>The get and set for WorkUnitResKey</summary>*/
        public string WorkUnitResKey { get; set; }
        /*<summary>The get and set for ResultTot</summary>*/
        public string ResultTot { get; set; }
        /*<summary>The get and set for ResultCnt</summary>*/
        public string ResultCnt { get; set; }
        /*<summary>The get and set for LabCnt</summary>*/
        public string LabCnt { get; set; }
        /*<summary>The get and set for AttendMonthKey</summary>*/
        public string AttendMonthKey { get; set; }
        /*<summary>The get and set for ProspectKey</summary>*/
        public string ProspectKey { get; set; }
        /*<summary>The get and set for PrivCourseRoomCost</summary>*/
        public string PrivCourseRoomCost { get; set; }
        /*<summary>The get and set for StuTransKey</summary>*/
        public string StuTransKey { get; set; }
        /*<summary>The get and set for StuGroupKey</summary>*/
        public string StuGroupKey { get; set; }
        /*<summary>The get and set for NotesKey</summary>*/
        public string NotesKey { get; set; }
        /*<summary>The get and set for PhoneType1</summary>*/
        public string PhoneType1 { get; set; }
        /*<summary>The get and set for PhoneType2</summary>*/
        public string PhoneType2 { get; set; }
        /*<summary>The get and set for HomeEmail</summary>*/
        public string HomeEmail { get; set; }
        /*<summary>The get and set for XferSubjResKey</summary>*/
        public string XferSubjResKey { get; set; }
        /*<summary>The get and set for StateRegDate</summary>*/
        public string StateRegDate { get; set; }
        /*<summary>The get and set for PrevStudRecKey</summary>*/
        public string PrevStudRecKey { get; set; }
        /*<summary>The get and set for NextStudRecKey</summary>*/
        public string NextStudRecKey { get; set; }
        /*<summary>The get and set for DegreeSeekingType</summary>*/
        public string DegreeSeekingType { get; set; }
        /*<summary>The get and set for PrivStatusLogKey</summary>*/
        public string PrivStatusLogKey { get; set; }
        /*<summary>The get and set for PrivFirstTransDate</summary>*/
        public string PrivFirstTransDate { get; set; }
        /*<summary>The get and set for PrivLastTransDate</summary>*/
        public string PrivLastTransDate { get; set; }
        /*<summary>The get and set for PrivZipPlus4</summary>*/
        public string PrivZipPlus4 { get; set; }
        /*<summary>The get and set for DropReasonCode</summary>*/
        public string DropReasonCode { get; set; }
        /*<summary>The get and set for PhoneNumAreaCode2</summary>*/
        public string PhoneNumAreaCode2 { get; set; }
        /*<summary>The get and set for PhoneNumPrefix2</summary>*/
        public string PhoneNumPrefix2 { get; set; }
        /*<summary>The get and set for PhoneNumBody2</summary>*/
        public string PhoneNumBody2 { get; set; }
        /*<summary>The get and set for TransHrsLnYr1</summary>*/
        public string TransHrsLnYr1 { get; set; }
        /*<summary>The get and set for OhioTheoryHrs</summary>*/
        public string OhioTheoryHrs { get; set; }
        /*<summary>The get and set for OhioPracticalHrs</summary>*/
        public string OhioPracticalHrs { get; set; }
        /*<summary>The get and set for OhioDemoHrs</summary>*/
        public string OhioDemoHrs { get; set; }
        /*<summary>The get and set for OhioClinicHrs</summary>*/
        public string OhioClinicHrs { get; set; }
        /*<summary>The get and set for DefaultLtrRecKey</summary>*/
        public string DefaultLtrRecKey { get; set; }
        /*<summary>The get and set for CreditHrs</summary>*/
        public string CreditHrs { get; set; }
        /*<summary>The get and set for TotHrsLastJune30th</summary>*/
        public string TotHrsLastJune30th { get; set; }
        /*<summary>The get and set for SchdHrsLastJune30th</summary>*/
        public string SchdHrsLastJune30th { get; set; }
        /*<summary>The get and set for TranHrsUpFront</summary>*/
        public string TranHrsUpFront { get; set; }
        /*<summary>The get and set for Atbscore</summary>*/
        public string Atbscore { get; set; }
        /*<summary>The get and set for SapinclPrevEnr</summary>*/
        public string SapinclPrevEnr { get; set; }
        /*<summary>The get and set for Ostudent</summary>*/
        public string Ostudent { get; set; }
        /*<summary>The get and set for BirthDate1</summary>*/
        public string BirthDate1 { get; set; }
        /*<summary>The get and set for Sex</summary>*/
        public string Sex { get; set; }
        /*<summary>The get and set for UrbanType</summary>*/
        public string UrbanType { get; set; }
        /*<summary>The get and set for Race</summary>*/
        public string Race { get; set; }
        /*<summary>The get and set for Dependency</summary>*/
        public string Dependency { get; set; }
        /*<summary>The get and set for EducationLevel</summary>*/
        public string EducationLevel { get; set; }
        /*<summary>The get and set for FullTimePartTime</summary>*/
        public string FullTimePartTime { get; set; }
        /*<summary>The get and set for IncomeLevel</summary>*/
        public string IncomeLevel { get; set; }
        /*<summary>The get and set for Age</summary>*/
        public string Age { get; set; }
        /*<summary>The get and set for MilesFromSchool</summary>*/
        public string MilesFromSchool { get; set; }
        /*<summary>The get and set for Over50Miles</summary>*/
        public string Over50Miles { get; set; }
        /*<summary>The get and set for MaritalStatus</summary>*/
        public string MaritalStatus { get; set; }
        /*<summary>The get and set for AllClinicInfo1</summary>*/
        public string AllClinicInfo1 { get; set; }
        /*<summary>The get and set for AllClinicInfo2</summary>*/
        public string AllClinicInfo2 { get; set; }
        /*<summary>The get and set for AllClinicInfo3</summary>*/
        public string AllClinicInfo3 { get; set; }
        /*<summary>The get and set for AllClinicInfo4</summary>*/
        public string AllClinicInfo4 { get; set; }
        /*<summary>The get and set for AllClinicInfo5</summary>*/
        public string AllClinicInfo5 { get; set; }
        /*<summary>The get and set for AllClinicInfo6</summary>*/
        public string AllClinicInfo6 { get; set; }
        /*<summary>The get and set for AllClinicInfo7</summary>*/
        public string AllClinicInfo7 { get; set; }
        /*<summary>The get and set for AllClinicInfo8</summary>*/
        public string AllClinicInfo8 { get; set; }
        /*<summary>The get and set for AllClinicInfo9</summary>*/
        public string AllClinicInfo9 { get; set; }
        /*<summary>The get and set for AllClinicInfo10</summary>*/
        public string AllClinicInfo10 { get; set; }
        /*<summary>The get and set for AllClinicInfo11</summary>*/
        public string AllClinicInfo11 { get; set; }
        /*<summary>The get and set for AllClinicInfo12</summary>*/
        public string AllClinicInfo12 { get; set; }
        /*<summary>The get and set for AllClinicInfo13</summary>*/
        public string AllClinicInfo13 { get; set; }
        /*<summary>The get and set for AllClinicInfo14</summary>*/
        public string AllClinicInfo14 { get; set; }
        /*<summary>The get and set for AllClinicInfo15</summary>*/
        public string AllClinicInfo15 { get; set; }
        /*<summary>The get and set for AllClinicInfo16</summary>*/
        public string AllClinicInfo16 { get; set; }
        /*<summary>The get and set for AllClinicInfo17</summary>*/
        public string AllClinicInfo17 { get; set; }
        /*<summary>The get and set for AllClinicInfo18</summary>*/
        public string AllClinicInfo18 { get; set; }
        /*<summary>The get and set for AllClinicInfo19</summary>*/
        public string AllClinicInfo19 { get; set; }
        /*<summary>The get and set for AllClinicInfo20</summary>*/
        public string AllClinicInfo20 { get; set; }
        /*<summary>The get and set for AllClinicInfo21</summary>*/
        public string AllClinicInfo21 { get; set; }
        /*<summary>The get and set for AllClinicInfo22</summary>*/
        public string AllClinicInfo22 { get; set; }
        /*<summary>The get and set for AllClinicInfo23</summary>*/
        public string AllClinicInfo23 { get; set; }
        /*<summary>The get and set for AllClinicInfo24</summary>*/
        public string AllClinicInfo24 { get; set; }
        /*<summary>The get and set for AllClinicInfo25</summary>*/
        public string AllClinicInfo25 { get; set; }
        /*<summary>The get and set for AllClinicInfo26</summary>*/
        public string AllClinicInfo26 { get; set; }
        /*<summary>The get and set for AllClinicInfo27</summary>*/
        public string AllClinicInfo27 { get; set; }
        /*<summary>The get and set for AllClinicInfo28</summary>*/
        public string AllClinicInfo28 { get; set; }
        /*<summary>The get and set for AllClinicInfo29</summary>*/
        public string AllClinicInfo29 { get; set; }
        /*<summary>The get and set for AllClinicInfo30</summary>*/
        public string AllClinicInfo30 { get; set; }
        /*<summary>The get and set for AllClinicInfo31</summary>*/
        public string AllClinicInfo31 { get; set; }
        /*<summary>The get and set for AllClinicInfo32</summary>*/
        public string AllClinicInfo32 { get; set; }
        /*<summary>The get and set for AllClinicInfo33</summary>*/
        public string AllClinicInfo33 { get; set; }
        /*<summary>The get and set for AllClinicInfo34</summary>*/
        public string AllClinicInfo34 { get; set; }
        /*<summary>The get and set for AllClinicInfo35</summary>*/
        public string AllClinicInfo35 { get; set; }
        /*<summary>The get and set for AllClinicInfo36</summary>*/
        public string AllClinicInfo36 { get; set; }
        /*<summary>The get and set for AllClinicInfo37</summary>*/
        public string AllClinicInfo37 { get; set; }
        /*<summary>The get and set for AllClinicInfo38</summary>*/
        public string AllClinicInfo38 { get; set; }
        /*<summary>The get and set for AllClinicInfo39</summary>*/
        public string AllClinicInfo39 { get; set; }
        /*<summary>The get and set for AllClinicInfo40</summary>*/
        public string AllClinicInfo40 { get; set; }
        /*<summary>The get and set for TotWrittenScores</summary>*/
        public string TotWrittenScores { get; set; }
        /*<summary>The get and set for TotPracticalScores</summary>*/
        public string TotPracticalScores { get; set; }
        /*<summary>The get and set for NumWrittenTaken</summary>*/
        public string NumWrittenTaken { get; set; }
        /*<summary>The get and set for NumPracticalTaken</summary>*/
        public string NumPracticalTaken { get; set; }
        /*<summary>The get and set for NumOfDeps</summary>*/
        public string NumOfDeps { get; set; }
        /*<summary>The get and set for HeadHousehold</summary>*/
        public string HeadHousehold { get; set; }
        /*<summary>The get and set for YearlyIncome</summary>*/
        public string YearlyIncome { get; set; }
        /*<summary>The get and set for InHighSchool</summary>*/
        public string InHighSchool { get; set; }
        /*<summary>The get and set for CitizenType</summary>*/
        public string CitizenType { get; set; }
        /*<summary>The get and set for HousingType</summary>*/
        public string HousingType { get; set; }
        /*<summary>The get and set for HighSchoolRecKey</summary>*/
        public string HighSchoolRecKey { get; set; }
        /*<summary>The get and set for HighSchGeddate</summary>*/
        public string HighSchGeddate { get; set; }
        /*<summary>The get and set for LeadImportId</summary>*/
        public int LeadImportId { get; set; }
    }
}