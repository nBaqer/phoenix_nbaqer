﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyDepartments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyDepartments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyDepartments</summary>*/
    public partial class SyDepartments
    {
        /*<summary>The constructor for SyDepartments</summary>*/
        public SyDepartments()
        {
            SyStuRestrictions = new HashSet<SyStuRestrictions>();
        }

        /*<summary>The get and set for DepartmentId</summary>*/
        public Guid DepartmentId { get; set; }
        /*<summary>The get and set for DepartmentCode</summary>*/
        public string DepartmentCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for DepartmentDescrip</summary>*/
        public string DepartmentDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyStuRestrictions</summary>*/
        public virtual ICollection<SyStuRestrictions>SyStuRestrictions { get; set; }
    }
}