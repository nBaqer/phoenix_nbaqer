﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamItemProp.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamItemProp definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamItemProp</summary>*/
    public partial class ParamItemProp
    {
        /*<summary>The get and set for Itempropertyid</summary>*/
        public long Itempropertyid { get; set; }
        /*<summary>The get and set for Itemid</summary>*/
        public long Itemid { get; set; }
        /*<summary>The get and set for Childcontrol</summary>*/
        public string Childcontrol { get; set; }
        /*<summary>The get and set for Propname</summary>*/
        public string Propname { get; set; }
        /*<summary>The get and set for Value</summary>*/
        public string Value { get; set; }
        /*<summary>The get and set for Valuetype</summary>*/
        public string Valuetype { get; set; }
    }
}