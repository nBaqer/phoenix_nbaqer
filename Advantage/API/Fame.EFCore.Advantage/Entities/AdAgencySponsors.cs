﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdAgencySponsors.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdAgencySponsors definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdAgencySponsors</summary>*/
    public partial class AdAgencySponsors
    {
        /*<summary>The constructor for AdAgencySponsors</summary>*/
        public AdAgencySponsors()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for AgencySpId</summary>*/
        public Guid AgencySpId { get; set; }
        /*<summary>The get and set for AgencySpCode</summary>*/
        public string AgencySpCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for AgencySpDescrip</summary>*/
        public string AgencySpDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}