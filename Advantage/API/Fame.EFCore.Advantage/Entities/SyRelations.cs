﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRelations.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRelations definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRelations</summary>*/
    public partial class SyRelations
    {
        /*<summary>The constructor for SyRelations</summary>*/
        public SyRelations()
        {
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
            HrEmployeeEmergencyContacts = new HashSet<HrEmployeeEmergencyContacts>();
        }

        /*<summary>The get and set for RelationId</summary>*/
        public Guid RelationId { get; set; }
        /*<summary>The get and set for RelationCode</summary>*/
        public string RelationCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for RelationDescrip</summary>*/
        public string RelationDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
        /*<summary>The navigational property for HrEmployeeEmergencyContacts</summary>*/
        public virtual ICollection<HrEmployeeEmergencyContacts>HrEmployeeEmergencyContacts { get; set; }
    }
}