﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEdexpressExceptionReportPellAcgSmartTeachStudentTable.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEdexpressExceptionReportPellAcgSmartTeachStudentTable definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEdexpressExceptionReportPellAcgSmartTeachStudentTable</summary>*/
    public partial class SyEdexpressExceptionReportPellAcgSmartTeachStudentTable
    {
        /*<summary>The get and set for ExceptionReportId</summary>*/
        public Guid ExceptionReportId { get; set; }
        /*<summary>The get and set for DbIn</summary>*/
        public string DbIn { get; set; }
        /*<summary>The get and set for Filter</summary>*/
        public string Filter { get; set; }
        /*<summary>The get and set for AwardAmount</summary>*/
        public string AwardAmount { get; set; }
        /*<summary>The get and set for AwardId</summary>*/
        public string AwardId { get; set; }
        /*<summary>The get and set for GrantType</summary>*/
        public string GrantType { get; set; }
        /*<summary>The get and set for AddDate</summary>*/
        public string AddDate { get; set; }
        /*<summary>The get and set for AddTime</summary>*/
        public string AddTime { get; set; }
        /*<summary>The get and set for OriginalSsn</summary>*/
        public string OriginalSsn { get; set; }
        /*<summary>The get and set for UpdateDate</summary>*/
        public string UpdateDate { get; set; }
        /*<summary>The get and set for UpdateTime</summary>*/
        public string UpdateTime { get; set; }
        /*<summary>The get and set for OriginationStatus</summary>*/
        public string OriginationStatus { get; set; }
        /*<summary>The get and set for AcademicYearId</summary>*/
        public Guid? AcademicYearId { get; set; }
        /*<summary>The get and set for AwardYearStartDate</summary>*/
        public string AwardYearStartDate { get; set; }
        /*<summary>The get and set for AwardYearEndDate</summary>*/
        public string AwardYearEndDate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ErrorMsg</summary>*/
        public string ErrorMsg { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for ExceptionGuid</summary>*/
        public Guid? ExceptionGuid { get; set; }
    }
}