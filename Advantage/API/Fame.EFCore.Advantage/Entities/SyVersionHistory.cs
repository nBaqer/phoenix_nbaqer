﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyVersionHistory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyVersionHistory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyVersionHistory</summary>*/
    public partial class SyVersionHistory
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Major</summary>*/
        public int? Major { get; set; }
        /*<summary>The get and set for Minor</summary>*/
        public int? Minor { get; set; }
        /*<summary>The get and set for Build</summary>*/
        public int? Build { get; set; }
        /*<summary>The get and set for Revision</summary>*/
        public int? Revision { get; set; }
        /*<summary>The get and set for VersionBegin</summary>*/
        public DateTime? VersionBegin { get; set; }
        /*<summary>The get and set for VersionEnd</summary>*/
        public DateTime? VersionEnd { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}