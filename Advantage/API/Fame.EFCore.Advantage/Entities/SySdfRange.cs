﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfRange.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfRange definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfRange</summary>*/
    public partial class SySdfRange
    {
        /*<summary>The get and set for SdfRangeId</summary>*/
        public Guid SdfRangeId { get; set; }
        /*<summary>The get and set for Sdfid</summary>*/
        public Guid Sdfid { get; set; }
        /*<summary>The get and set for MinVal</summary>*/
        public string MinVal { get; set; }
        /*<summary>The get and set for MaxVal</summary>*/
        public string MaxVal { get; set; }
        /*<summary>The get and set for Moduser</summary>*/
        public string Moduser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
    }
}