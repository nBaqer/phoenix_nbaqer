﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdCitizenships.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdCitizenships definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdCitizenships</summary>*/
    public partial class AdCitizenships
    {
        /*<summary>The constructor for AdCitizenships</summary>*/
        public AdCitizenships()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for CitizenshipId</summary>*/
        public Guid CitizenshipId { get; set; }
        /*<summary>The get and set for CitizenshipCode</summary>*/
        public string CitizenshipCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CitizenshipDescrip</summary>*/
        public string CitizenshipDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Ipedssequence</summary>*/
        public int? Ipedssequence { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }
        /*<summary>The get and set for AfaMappingId</summary>*/
        public Guid? AfaMappingId { get; set; }

        /*<summary>The navigational property for AfaMapping</summary>*/
        public virtual AfaCatalogMapping AfaMapping { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}