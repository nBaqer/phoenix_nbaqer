﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaDeferredRevenues.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaDeferredRevenues definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaDeferredRevenues</summary>*/
    public partial class SaDeferredRevenues
    {
        /*<summary>The get and set for DefRevenueId</summary>*/
        public Guid DefRevenueId { get; set; }
        /*<summary>The get and set for DefRevenueDate</summary>*/
        public DateTime DefRevenueDate { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for Type</summary>*/
        public bool Type { get; set; }
        /*<summary>The get and set for Source</summary>*/
        public byte Source { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal Amount { get; set; }
        /*<summary>The get and set for IsPosted</summary>*/
        public bool IsPosted { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Transaction</summary>*/
        public virtual SaTransactions Transaction { get; set; }
    }
}