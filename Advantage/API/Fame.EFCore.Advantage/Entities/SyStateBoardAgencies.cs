﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStateBoardAgencies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStateBoardAgencies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStateBoardAgencies</summary>*/
    public partial class SyStateBoardAgencies
    {
        /*<summary>The constructor for SyStateBoardAgencies</summary>*/
        public SyStateBoardAgencies()
        {
            SySchoolStateBoardReports = new HashSet<SySchoolStateBoardReports>();
        }

        /*<summary>The get and set for StateBoardAgencyId</summary>*/
        public int StateBoardAgencyId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid StateId { get; set; }
        /*<summary>The get and set for LicensingAddress1</summary>*/
        public string LicensingAddress1 { get; set; }
        /*<summary>The get and set for LicensingAddress2</summary>*/
        public string LicensingAddress2 { get; set; }
        /*<summary>The get and set for LicensingCountryId</summary>*/
        public Guid LicensingCountryId { get; set; }
        /*<summary>The get and set for LicensingStateId</summary>*/
        public Guid LicensingStateId { get; set; }
        /*<summary>The get and set for LicensingCity</summary>*/
        public string LicensingCity { get; set; }
        /*<summary>The get and set for LicensingZipCode</summary>*/
        public string LicensingZipCode { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for LicensingCountry</summary>*/
        public virtual AdCountries LicensingCountry { get; set; }
        /*<summary>The navigational property for LicensingState</summary>*/
        public virtual SyStates LicensingState { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SySchoolStateBoardReports</summary>*/
        public virtual ICollection<SySchoolStateBoardReports>SySchoolStateBoardReports { get; set; }
    }
}