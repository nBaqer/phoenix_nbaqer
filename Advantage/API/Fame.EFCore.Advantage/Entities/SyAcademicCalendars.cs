﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAcademicCalendars.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAcademicCalendars definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAcademicCalendars</summary>*/
    public partial class SyAcademicCalendars
    {
        /*<summary>The constructor for SyAcademicCalendars</summary>*/
        public SyAcademicCalendars()
        {
            ArPrograms = new HashSet<ArPrograms>();
        }

        /*<summary>The get and set for Acid</summary>*/
        public int Acid { get; set; }
        /*<summary>The get and set for Acdescrip</summary>*/
        public string Acdescrip { get; set; }

        /*<summary>The navigational property for ArPrograms</summary>*/
        public virtual ICollection<ArPrograms>ArPrograms { get; set; }
    }
}