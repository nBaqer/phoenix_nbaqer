﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkWeights.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkWeights definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkWeights</summary>*/
    public partial class ArGrdBkWeights
    {
        /*<summary>The constructor for ArGrdBkWeights</summary>*/
        public ArGrdBkWeights()
        {
            ArClassSections = new HashSet<ArClassSections>();
            ArGrdBkWgtDetails = new HashSet<ArGrdBkWgtDetails>();
        }

        /*<summary>The get and set for InstrGrdBkWgtId</summary>*/
        public Guid InstrGrdBkWgtId { get; set; }
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid? InstructorId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid? ReqId { get; set; }
        /*<summary>The get and set for EffectiveDate</summary>*/
        public DateTime? EffectiveDate { get; set; }

        /*<summary>The navigational property for Instructor</summary>*/
        public virtual SyUsers Instructor { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArGrdBkWgtDetails</summary>*/
        public virtual ICollection<ArGrdBkWgtDetails>ArGrdBkWgtDetails { get; set; }
    }
}