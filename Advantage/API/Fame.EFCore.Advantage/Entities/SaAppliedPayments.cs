﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaAppliedPayments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaAppliedPayments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaAppliedPayments</summary>*/
    public partial class SaAppliedPayments
    {
        /*<summary>The get and set for AppliedPmtId</summary>*/
        public Guid AppliedPmtId { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for ApplyToTransId</summary>*/
        public Guid ApplyToTransId { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal Amount { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}