﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaStuPaymentPlanSchedule.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaStuPaymentPlanSchedule definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaStuPaymentPlanSchedule</summary>*/
    public partial class FaStuPaymentPlanSchedule
    {
        /*<summary>The constructor for FaStuPaymentPlanSchedule</summary>*/
        public FaStuPaymentPlanSchedule()
        {
            SaPmtDisbRel = new HashSet<SaPmtDisbRel>();
        }

        /*<summary>The get and set for PayPlanScheduleId</summary>*/
        public Guid PayPlanScheduleId { get; set; }
        /*<summary>The get and set for PaymentPlanId</summary>*/
        public Guid? PaymentPlanId { get; set; }
        /*<summary>The get and set for ExpectedDate</summary>*/
        public DateTime? ExpectedDate { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal? Amount { get; set; }
        /*<summary>The get and set for Reference</summary>*/
        public string Reference { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public string ModDate { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid? TransactionId { get; set; }

        /*<summary>The navigational property for PaymentPlan</summary>*/
        public virtual FaStudentPaymentPlans PaymentPlan { get; set; }
        /*<summary>The navigational property for SaPmtDisbRel</summary>*/
        public virtual ICollection<SaPmtDisbRel>SaPmtDisbRel { get; set; }
    }
}