﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AtOnLineStudents.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AtOnLineStudents definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AtOnLineStudents</summary>*/
    public partial class AtOnLineStudents
    {
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for OnLineStudentId</summary>*/
        public int OnLineStudentId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
    }
}