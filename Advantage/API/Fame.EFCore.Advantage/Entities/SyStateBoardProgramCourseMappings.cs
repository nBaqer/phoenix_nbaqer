﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStateBoardProgramCourseMappings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStateBoardProgramCourseMappings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStateBoardProgramCourseMappings</summary>*/
    public partial class SyStateBoardProgramCourseMappings
    {
        /*<summary>The get and set for StateBoardProgramCourseMappingId</summary>*/
        public int StateBoardProgramCourseMappingId { get; set; }
        /*<summary>The get and set for SchoolStateBoardReportId</summary>*/
        public Guid SchoolStateBoardReportId { get; set; }
        /*<summary>The get and set for ProgramId</summary>*/
        public Guid ProgramId { get; set; }
        /*<summary>The get and set for StateBoardCourseId</summary>*/
        public int StateBoardCourseId { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid CreatedById { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for Program</summary>*/
        public virtual ArPrograms Program { get; set; }
        /*<summary>The navigational property for SchoolStateBoardReport</summary>*/
        public virtual SySchoolStateBoardReports SchoolStateBoardReport { get; set; }
        /*<summary>The navigational property for StateBoardCourse</summary>*/
        public virtual SyStateBoardCourses StateBoardCourse { get; set; }
    }
}