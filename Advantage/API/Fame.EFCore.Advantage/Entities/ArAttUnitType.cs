﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArAttUnitType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArAttUnitType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArAttUnitType</summary>*/
    public partial class ArAttUnitType
    {
        /*<summary>The constructor for ArAttUnitType</summary>*/
        public ArAttUnitType()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
            ArReqs = new HashSet<ArReqs>();
        }

        /*<summary>The get and set for UnitTypeId</summary>*/
        public Guid UnitTypeId { get; set; }
        /*<summary>The get and set for UnitTypeDescrip</summary>*/
        public string UnitTypeDescrip { get; set; }

        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
    }
}