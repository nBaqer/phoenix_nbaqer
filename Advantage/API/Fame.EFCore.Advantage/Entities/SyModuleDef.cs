﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyModuleDef.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyModuleDef definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyModuleDef</summary>*/
    public partial class SyModuleDef
    {
        /*<summary>The get and set for ModDefId</summary>*/
        public short ModDefId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public byte ModuleId { get; set; }
        /*<summary>The get and set for ChildId</summary>*/
        public short ChildId { get; set; }
        /*<summary>The get and set for ChildType</summary>*/
        public string ChildType { get; set; }
        /*<summary>The get and set for Sequence</summary>*/
        public short Sequence { get; set; }

        /*<summary>The navigational property for Module</summary>*/
        public virtual SyModules Module { get; set; }
    }
}