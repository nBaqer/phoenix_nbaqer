﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdExpertiseLevel.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdExpertiseLevel definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdExpertiseLevel</summary>*/
    public partial class AdExpertiseLevel
    {
        /*<summary>The constructor for AdExpertiseLevel</summary>*/
        public AdExpertiseLevel()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlExitInterview = new HashSet<PlExitInterview>();
        }

        /*<summary>The get and set for ExpertiseId</summary>*/
        public Guid ExpertiseId { get; set; }
        /*<summary>The get and set for ExpertiseCode</summary>*/
        public string ExpertiseCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for ExpertiseDescrip</summary>*/
        public string ExpertiseDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlExitInterview</summary>*/
        public virtual ICollection<PlExitInterview>PlExitInterview { get; set; }
    }
}