﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadDuplicates.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadDuplicates definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadDuplicates</summary>*/
    public partial class AdLeadDuplicates
    {
        /*<summary>The get and set for IdDuplicates</summary>*/
        public int IdDuplicates { get; set; }
        /*<summary>The get and set for NewLeadGuid</summary>*/
        public Guid NewLeadGuid { get; set; }
        /*<summary>The get and set for PosibleDuplicateGuid</summary>*/
        public Guid PosibleDuplicateGuid { get; set; }

        /*<summary>The navigational property for NewLeadGu</summary>*/
        public virtual AdLeads NewLeadGu { get; set; }
        /*<summary>The navigational property for PosibleDuplicateGu</summary>*/
        public virtual AdLeads PosibleDuplicateGu { get; set; }
    }
}