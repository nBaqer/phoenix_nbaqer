﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdExtraCurricular.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdExtraCurricular definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdExtraCurricular</summary>*/
    public partial class AdExtraCurricular
    {
        /*<summary>The get and set for IdExtraCurricular</summary>*/
        public int IdExtraCurricular { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for LevelId</summary>*/
        public int LevelId { get; set; }
        /*<summary>The get and set for ExtraCurrGrpId</summary>*/
        public Guid ExtraCurrGrpId { get; set; }
        /*<summary>The get and set for ExtraCurrDescription</summary>*/
        public string ExtraCurrDescription { get; set; }
        /*<summary>The get and set for ExtraCurrComment</summary>*/
        public string ExtraCurrComment { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for ExtraCurrGrp</summary>*/
        public virtual AdExtraCurrGrp ExtraCurrGrp { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Level</summary>*/
        public virtual AdLevel Level { get; set; }
    }
}