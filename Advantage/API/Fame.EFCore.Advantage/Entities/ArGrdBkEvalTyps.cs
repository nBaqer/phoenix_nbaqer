﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkEvalTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkEvalTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkEvalTyps</summary>*/
    public partial class ArGrdBkEvalTyps
    {
        /*<summary>The get and set for GrdBkEvalTypId</summary>*/
        public Guid GrdBkEvalTypId { get; set; }
        /*<summary>The get and set for GrdBkEvalTypCode</summary>*/
        public string GrdBkEvalTypCode { get; set; }
        /*<summary>The get and set for GrdBkEvalTypDescrip</summary>*/
        public string GrdBkEvalTypDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}