﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArClassSections.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArClassSections definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArClassSections</summary>*/
    public partial class ArClassSections
    {
        /*<summary>The constructor for ArClassSections</summary>*/
        public ArClassSections()
        {
            ArClassSectionTerms = new HashSet<ArClassSectionTerms>();
            ArClsSectMeetings = new HashSet<ArClsSectMeetings>();
            ArClsSectStudents = new HashSet<ArClsSectStudents>();
            ArClsSectionTimeClockPolicy = new HashSet<ArClsSectionTimeClockPolicy>();
            ArGrdBkResults = new HashSet<ArGrdBkResults>();
            ArOverridenConflictsLeftClsSection = new HashSet<ArOverridenConflicts>();
            ArOverridenConflictsRightClsSection = new HashSet<ArOverridenConflicts>();
            ArResults = new HashSet<ArResults>();
            ArUnschedClosures = new HashSet<ArUnschedClosures>();
            AtClsSectAttendance = new HashSet<AtClsSectAttendance>();
        }

        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid TermId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for ClsSection</summary>*/
        public string ClsSection { get; set; }
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid? InstructorId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime EndDate { get; set; }
        /*<summary>The get and set for MaxStud</summary>*/
        public int MaxStud { get; set; }
        /*<summary>The get and set for InstrGrdBkWgtId</summary>*/
        public Guid? InstrGrdBkWgtId { get; set; }
        /*<summary>The get and set for GrdScaleId</summary>*/
        public Guid GrdScaleId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for IsGraded</summary>*/
        public bool? IsGraded { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for CourseId</summary>*/
        public string CourseId { get; set; }
        /*<summary>The get and set for TermGuid</summary>*/
        public string TermGuid { get; set; }
        /*<summary>The get and set for Session</summary>*/
        public string Session { get; set; }
        /*<summary>The get and set for ClassRoom</summary>*/
        public string ClassRoom { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid? ShiftId { get; set; }
        /*<summary>The get and set for PeriodId</summary>*/
        public Guid? PeriodId { get; set; }
        /*<summary>The get and set for StudentStartDate</summary>*/
        public DateTime? StudentStartDate { get; set; }
        /*<summary>The get and set for LeadGrpId</summary>*/
        public Guid? LeadGrpId { get; set; }
        /*<summary>The get and set for CohortStartDate</summary>*/
        public DateTime? CohortStartDate { get; set; }
        /*<summary>The get and set for ProgramVersionDefinitionId</summary>*/
        public Guid? ProgramVersionDefinitionId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for GrdScale</summary>*/
        public virtual ArGradeScales GrdScale { get; set; }
        /*<summary>The navigational property for InstrGrdBkWgt</summary>*/
        public virtual ArGrdBkWeights InstrGrdBkWgt { get; set; }
        /*<summary>The navigational property for Instructor</summary>*/
        public virtual SyUsers Instructor { get; set; }
        /*<summary>The navigational property for LeadGrp</summary>*/
        public virtual AdLeadGroups LeadGrp { get; set; }
        /*<summary>The navigational property for ProgramVersionDefinition</summary>*/
        public virtual ArProgVerDef ProgramVersionDefinition { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
        /*<summary>The navigational property for Shift</summary>*/
        public virtual ArShifts Shift { get; set; }
        /*<summary>The navigational property for Term</summary>*/
        public virtual ArTerm Term { get; set; }
        /*<summary>The navigational property for ArClassSectionTerms</summary>*/
        public virtual ICollection<ArClassSectionTerms>ArClassSectionTerms { get; set; }
        /*<summary>The navigational property for ArClsSectMeetings</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetings { get; set; }
        /*<summary>The navigational property for ArClsSectStudents</summary>*/
        public virtual ICollection<ArClsSectStudents>ArClsSectStudents { get; set; }
        /*<summary>The navigational property for ArClsSectionTimeClockPolicy</summary>*/
        public virtual ICollection<ArClsSectionTimeClockPolicy>ArClsSectionTimeClockPolicy { get; set; }
        /*<summary>The navigational property for ArGrdBkResults</summary>*/
        public virtual ICollection<ArGrdBkResults>ArGrdBkResults { get; set; }
        /*<summary>The navigational property for ArOverridenConflictsLeftClsSection</summary>*/
        public virtual ICollection<ArOverridenConflicts>ArOverridenConflictsLeftClsSection { get; set; }
        /*<summary>The navigational property for ArOverridenConflictsRightClsSection</summary>*/
        public virtual ICollection<ArOverridenConflicts>ArOverridenConflictsRightClsSection { get; set; }
        /*<summary>The navigational property for ArResults</summary>*/
        public virtual ICollection<ArResults>ArResults { get; set; }
        /*<summary>The navigational property for ArUnschedClosures</summary>*/
        public virtual ICollection<ArUnschedClosures>ArUnschedClosures { get; set; }
        /*<summary>The navigational property for AtClsSectAttendance</summary>*/
        public virtual ICollection<AtClsSectAttendance>AtClsSectAttendance { get; set; }
    }
}