﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyNavigationNodes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyNavigationNodes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyNavigationNodes</summary>*/
    public partial class SyNavigationNodes
    {
        /*<summary>The constructor for SyNavigationNodes</summary>*/
        public SyNavigationNodes()
        {
            InverseParent = new HashSet<SyNavigationNodes>();
        }

        /*<summary>The get and set for HierarchyId</summary>*/
        public Guid HierarchyId { get; set; }
        /*<summary>The get and set for HierarchyIndex</summary>*/
        public short HierarchyIndex { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for ParentId</summary>*/
        public Guid ParentId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsPopupWindow</summary>*/
        public bool? IsPopupWindow { get; set; }
        /*<summary>The get and set for IsShipped</summary>*/
        public bool? IsShipped { get; set; }

        /*<summary>The navigational property for Parent</summary>*/
        public virtual SyNavigationNodes Parent { get; set; }
        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for InverseParent</summary>*/
        public virtual ICollection<SyNavigationNodes>InverseParent { get; set; }
    }
}