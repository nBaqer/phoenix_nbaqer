﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyLangs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyLangs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyLangs</summary>*/
    public partial class SyLangs
    {
        /*<summary>The constructor for SyLangs</summary>*/
        public SyLangs()
        {
            SyFldCaptions = new HashSet<SyFldCaptions>();
        }

        /*<summary>The get and set for LangId</summary>*/
        public byte LangId { get; set; }
        /*<summary>The get and set for LangName</summary>*/
        public string LangName { get; set; }

        /*<summary>The navigational property for SyFldCaptions</summary>*/
        public virtual ICollection<SyFldCaptions>SyFldCaptions { get; set; }
    }
}