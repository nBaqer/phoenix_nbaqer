﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyResCaptions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyResCaptions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyResCaptions</summary>*/
    public partial class SyResCaptions
    {
        /*<summary>The get and set for ResourceCapId</summary>*/
        public int ResourceCapId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int ResourceId { get; set; }
        /*<summary>The get and set for LangId</summary>*/
        public byte LangId { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
    }
}