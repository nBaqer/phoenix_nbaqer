﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyDdls.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyDdls definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyDdls</summary>*/
    public partial class SyDdls
    {
        /*<summary>The get and set for Ddlid</summary>*/
        public int Ddlid { get; set; }
        /*<summary>The get and set for Ddlname</summary>*/
        public string Ddlname { get; set; }
        /*<summary>The get and set for TblId</summary>*/
        public int TblId { get; set; }
        /*<summary>The get and set for DispFldId</summary>*/
        public int DispFldId { get; set; }
        /*<summary>The get and set for ValFldId</summary>*/
        public int ValFldId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for CulDependent</summary>*/
        public bool? CulDependent { get; set; }

        /*<summary>The navigational property for Tbl</summary>*/
        public virtual SyTables Tbl { get; set; }
    }
}