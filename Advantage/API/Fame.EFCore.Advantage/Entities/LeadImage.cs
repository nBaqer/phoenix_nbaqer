﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="LeadImage.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The LeadImage definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for LeadImage</summary>*/
    public partial class LeadImage
    {
        /*<summary>The get and set for ImageId</summary>*/
        public int ImageId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for Image</summary>*/
        public byte[] Image { get; set; }
        /*<summary>The get and set for ImgFile</summary>*/
        public string ImgFile { get; set; }
        /*<summary>The get and set for ImgSize</summary>*/
        public int ImgSize { get; set; }
        /*<summary>The get and set for MediaType</summary>*/
        public string MediaType { get; set; }
        /*<summary>The get and set for TypeId</summary>*/
        public int TypeId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}