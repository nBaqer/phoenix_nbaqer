﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqGrpDef.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqGrpDef definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqGrpDef</summary>*/
    public partial class AdReqGrpDef
    {
        /*<summary>The get and set for ReqGrpDefId</summary>*/
        public Guid ReqGrpDefId { get; set; }
        /*<summary>The get and set for ReqGrpId</summary>*/
        public Guid? ReqGrpId { get; set; }
        /*<summary>The get and set for AdReqId</summary>*/
        public Guid? AdReqId { get; set; }
        /*<summary>The get and set for Sequence</summary>*/
        public int? Sequence { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool? IsRequired { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for LeadGrpId</summary>*/
        public Guid? LeadGrpId { get; set; }

        /*<summary>The navigational property for AdReq</summary>*/
        public virtual AdReqs AdReq { get; set; }
        /*<summary>The navigational property for LeadGrp</summary>*/
        public virtual AdLeadGroups LeadGrp { get; set; }
        /*<summary>The navigational property for ReqGrp</summary>*/
        public virtual AdReqGroups ReqGrp { get; set; }
    }
}