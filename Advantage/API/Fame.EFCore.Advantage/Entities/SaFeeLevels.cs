﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaFeeLevels.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaFeeLevels definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaFeeLevels</summary>*/
    public partial class SaFeeLevels
    {
        /*<summary>The constructor for SaFeeLevels</summary>*/
        public SaFeeLevels()
        {
            ArPrgChargePeriodSeq = new HashSet<ArPrgChargePeriodSeq>();
            SaPrgVerDefaultChargePeriods = new HashSet<SaPrgVerDefaultChargePeriods>();
        }

        /*<summary>The get and set for FeeLevelId</summary>*/
        public byte FeeLevelId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for ArPrgChargePeriodSeq</summary>*/
        public virtual ICollection<ArPrgChargePeriodSeq>ArPrgChargePeriodSeq { get; set; }
        /*<summary>The navigational property for SaPrgVerDefaultChargePeriods</summary>*/
        public virtual ICollection<SaPrgVerDefaultChargePeriods>SaPrgVerDefaultChargePeriods { get; set; }
    }
}