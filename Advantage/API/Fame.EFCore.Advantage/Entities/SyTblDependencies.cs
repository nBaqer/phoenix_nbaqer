﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTblDependencies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTblDependencies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTblDependencies</summary>*/
    public partial class SyTblDependencies
    {
        /*<summary>The get and set for Table1</summary>*/
        public string Table1 { get; set; }
        /*<summary>The get and set for Table2</summary>*/
        public string Table2 { get; set; }
        /*<summary>The get and set for DepTable</summary>*/
        public string DepTable { get; set; }
        /*<summary>The get and set for TableDependenciesId</summary>*/
        public Guid TableDependenciesId { get; set; }
    }
}