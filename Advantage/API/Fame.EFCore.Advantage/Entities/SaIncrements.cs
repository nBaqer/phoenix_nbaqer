﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaIncrements.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaIncrements definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaIncrements</summary>*/
    public partial class SaIncrements
    {
        /*<summary>The constructor for SaIncrements</summary>*/
        public SaIncrements()
        {
            SaPmtPeriods = new HashSet<SaPmtPeriods>();
        }

        /*<summary>The get and set for IncrementId</summary>*/
        public Guid IncrementId { get; set; }
        /*<summary>The get and set for BillingMethodId</summary>*/
        public Guid BillingMethodId { get; set; }
        /*<summary>The get and set for IncrementType</summary>*/
        public int IncrementType { get; set; }
        /*<summary>The get and set for IncrementName</summary>*/
        public string IncrementName { get; set; }
        /*<summary>The get and set for EffectiveDate</summary>*/
        public DateTime EffectiveDate { get; set; }
        /*<summary>The get and set for ExcAbscenesPercent</summary>*/
        public decimal ExcAbscenesPercent { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for BillingMethod</summary>*/
        public virtual SaBillingMethods BillingMethod { get; set; }
        /*<summary>The navigational property for SaPmtPeriods</summary>*/
        public virtual ICollection<SaPmtPeriods>SaPmtPeriods { get; set; }
    }
}