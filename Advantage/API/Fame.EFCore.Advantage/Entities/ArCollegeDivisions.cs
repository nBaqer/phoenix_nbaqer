﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCollegeDivisions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCollegeDivisions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCollegeDivisions</summary>*/
    public partial class ArCollegeDivisions
    {
        /*<summary>The constructor for ArCollegeDivisions</summary>*/
        public ArCollegeDivisions()
        {
            ArDepartments = new HashSet<ArDepartments>();
        }

        /*<summary>The get and set for CollegeDivId</summary>*/
        public Guid CollegeDivId { get; set; }
        /*<summary>The get and set for CollegeDivCode</summary>*/
        public string CollegeDivCode { get; set; }
        /*<summary>The get and set for CollegeDivDescrip</summary>*/
        public string CollegeDivDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArDepartments</summary>*/
        public virtual ICollection<ArDepartments>ArDepartments { get; set; }
    }
}