﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdColleges.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdColleges definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdColleges</summary>*/
    public partial class AdColleges
    {
        /*<summary>The get and set for CollegeId</summary>*/
        public Guid CollegeId { get; set; }
        /*<summary>The get and set for CollegeCode</summary>*/
        public string CollegeCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CollegeName</summary>*/
        public string CollegeName { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool ForeignZip { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool ForeignPhone { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}