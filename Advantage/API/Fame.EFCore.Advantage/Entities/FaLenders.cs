﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="FaLenders.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The FaLenders definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for FaLenders</summary>*/
    public partial class FaLenders
    {
        /*<summary>The constructor for FaLenders</summary>*/
        public FaLenders()
        {
            FaStudentAwardsGuarantor = new HashSet<FaStudentAwards>();
            FaStudentAwardsLender = new HashSet<FaStudentAwards>();
            FaStudentAwardsServicer = new HashSet<FaStudentAwards>();
        }

        /*<summary>The get and set for LenderId</summary>*/
        public Guid LenderId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for LenderDescrip</summary>*/
        public string LenderDescrip { get; set; }
        /*<summary>The get and set for ForeignAddress</summary>*/
        public bool ForeignAddress { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for PrimaryContact</summary>*/
        public string PrimaryContact { get; set; }
        /*<summary>The get and set for OtherContact</summary>*/
        public string OtherContact { get; set; }
        /*<summary>The get and set for IsLender</summary>*/
        public bool IsLender { get; set; }
        /*<summary>The get and set for IsServicer</summary>*/
        public bool IsServicer { get; set; }
        /*<summary>The get and set for IsGuarantor</summary>*/
        public bool IsGuarantor { get; set; }
        /*<summary>The get and set for ForeignPayAddress</summary>*/
        public bool ForeignPayAddress { get; set; }
        /*<summary>The get and set for PayAddress1</summary>*/
        public string PayAddress1 { get; set; }
        /*<summary>The get and set for PayAddress2</summary>*/
        public string PayAddress2 { get; set; }
        /*<summary>The get and set for PayCity</summary>*/
        public string PayCity { get; set; }
        /*<summary>The get and set for PayStateId</summary>*/
        public Guid? PayStateId { get; set; }
        /*<summary>The get and set for OtherPayState</summary>*/
        public string OtherPayState { get; set; }
        /*<summary>The get and set for PayZip</summary>*/
        public string PayZip { get; set; }
        /*<summary>The get and set for PayCountryId</summary>*/
        public Guid? PayCountryId { get; set; }
        /*<summary>The get and set for CustService</summary>*/
        public string CustService { get; set; }
        /*<summary>The get and set for ForeignCustService</summary>*/
        public bool ForeignCustService { get; set; }
        /*<summary>The get and set for Fax</summary>*/
        public string Fax { get; set; }
        /*<summary>The get and set for ForeignFax</summary>*/
        public bool ForeignFax { get; set; }
        /*<summary>The get and set for PreClaim</summary>*/
        public string PreClaim { get; set; }
        /*<summary>The get and set for ForeignPreClaim</summary>*/
        public bool ForeignPreClaim { get; set; }
        /*<summary>The get and set for PostClaim</summary>*/
        public string PostClaim { get; set; }
        /*<summary>The get and set for ForeignPostClaim</summary>*/
        public bool ForeignPostClaim { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for PayCountry</summary>*/
        public virtual AdCountries PayCountry { get; set; }
        /*<summary>The navigational property for PayState</summary>*/
        public virtual SyStates PayState { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for FaStudentAwardsGuarantor</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwardsGuarantor { get; set; }
        /*<summary>The navigational property for FaStudentAwardsLender</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwardsLender { get; set; }
        /*<summary>The navigational property for FaStudentAwardsServicer</summary>*/
        public virtual ICollection<FaStudentAwards>FaStudentAwardsServicer { get; set; }
    }
}