﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdVehicles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdVehicles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdVehicles</summary>*/
    public partial class AdVehicles
    {
        /*<summary>The get and set for VehicleId</summary>*/
        public int VehicleId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for Position</summary>*/
        public int Position { get; set; }
        /*<summary>The get and set for Permit</summary>*/
        public string Permit { get; set; }
        /*<summary>The get and set for Make</summary>*/
        public string Make { get; set; }
        /*<summary>The get and set for Model</summary>*/
        public string Model { get; set; }
        /*<summary>The get and set for Color</summary>*/
        public string Color { get; set; }
        /*<summary>The get and set for Plate</summary>*/
        public string Plate { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}