﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAwardTypes9010Mapping.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAwardTypes9010Mapping definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAwardTypes9010Mapping</summary>*/
    public partial class SyAwardTypes9010Mapping
    {
        /*<summary>The get and set for AwardTypes9010MappingId</summary>*/
        public Guid AwardTypes9010MappingId { get; set; }
        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid? FundSourceId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for AwardType9010Id</summary>*/
        public Guid AwardType9010Id { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid? TransCodeId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for FundSource</summary>*/
        public virtual SaFundSources FundSource { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
    }
}