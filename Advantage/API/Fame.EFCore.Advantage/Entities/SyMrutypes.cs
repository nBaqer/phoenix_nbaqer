﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMrutypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMrutypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMrutypes</summary>*/
    public partial class SyMrutypes
    {
        /*<summary>The get and set for MrutypeId</summary>*/
        public byte MrutypeId { get; set; }
        /*<summary>The get and set for Mrutype</summary>*/
        public string Mrutype { get; set; }
    }
}