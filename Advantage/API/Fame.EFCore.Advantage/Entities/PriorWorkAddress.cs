﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PriorWorkAddress.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PriorWorkAddress definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PriorWorkAddress</summary>*/
    public partial class PriorWorkAddress
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for StEmploymentId</summary>*/
        public Guid StEmploymentId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Apartment</summary>*/
        public string Apartment { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for IsInternational</summary>*/
        public bool IsInternational { get; set; }
        /*<summary>The get and set for StateInternational</summary>*/
        public string StateInternational { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for ZipCode</summary>*/
        public string ZipCode { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for StEmployment</summary>*/
        public virtual AdLeadEmployment StEmployment { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
    }
}