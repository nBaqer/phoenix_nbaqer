﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="CmTimeInterval.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The CmTimeInterval definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for CmTimeInterval</summary>*/
    public partial class CmTimeInterval
    {
        /*<summary>The constructor for CmTimeInterval</summary>*/
        public CmTimeInterval()
        {
            ArClsSectMeetingsEndInterval = new HashSet<ArClsSectMeetings>();
            ArClsSectMeetingsTimeInterval = new HashSet<ArClsSectMeetings>();
            ArPeriod = new HashSet<ArPeriod>();
            SyHolidaysEndTime = new HashSet<SyHolidays>();
            SyHolidaysStartTime = new HashSet<SyHolidays>();
            SyPeriodsEndTime = new HashSet<SyPeriods>();
            SyPeriodsStartTime = new HashSet<SyPeriods>();
        }

        /*<summary>The get and set for TimeIntervalId</summary>*/
        public Guid TimeIntervalId { get; set; }
        /*<summary>The get and set for TimeIntervalDescrip</summary>*/
        public DateTime? TimeIntervalDescrip { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArClsSectMeetingsEndInterval</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetingsEndInterval { get; set; }
        /*<summary>The navigational property for ArClsSectMeetingsTimeInterval</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetingsTimeInterval { get; set; }
        /*<summary>The navigational property for ArPeriod</summary>*/
        public virtual ICollection<ArPeriod>ArPeriod { get; set; }
        /*<summary>The navigational property for SyHolidaysEndTime</summary>*/
        public virtual ICollection<SyHolidays>SyHolidaysEndTime { get; set; }
        /*<summary>The navigational property for SyHolidaysStartTime</summary>*/
        public virtual ICollection<SyHolidays>SyHolidaysStartTime { get; set; }
        /*<summary>The navigational property for SyPeriodsEndTime</summary>*/
        public virtual ICollection<SyPeriods>SyPeriodsEndTime { get; set; }
        /*<summary>The navigational property for SyPeriodsStartTime</summary>*/
        public virtual ICollection<SyPeriods>SyPeriodsStartTime { get; set; }
    }
}