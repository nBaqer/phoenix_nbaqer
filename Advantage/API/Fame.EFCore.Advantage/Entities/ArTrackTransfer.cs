﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTrackTransfer.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTrackTransfer definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTrackTransfer</summary>*/
    public partial class ArTrackTransfer
    {
        /*<summary>The get and set for TrackTransferId</summary>*/
        public Guid TrackTransferId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for SourcePrgVerId</summary>*/
        public Guid? SourcePrgVerId { get; set; }
        /*<summary>The get and set for TargetPrgVerId</summary>*/
        public Guid? TargetPrgVerId { get; set; }
        /*<summary>The get and set for SourceCampusId</summary>*/
        public Guid? SourceCampusId { get; set; }
        /*<summary>The get and set for TargetCampusId</summary>*/
        public Guid? TargetCampusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}