﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstitutionTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstitutionTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstitutionTypes</summary>*/
    public partial class SyInstitutionTypes
    {
        /*<summary>The constructor for SyInstitutionTypes</summary>*/
        public SyInstitutionTypes()
        {
            SyInstitutions = new HashSet<SyInstitutions>();
        }

        /*<summary>The get and set for TypeId</summary>*/
        public int TypeId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for SyInstitutions</summary>*/
        public virtual ICollection<SyInstitutions>SyInstitutions { get; set; }
    }
}