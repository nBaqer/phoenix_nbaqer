﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArFerpapolicy.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArFerpapolicy definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArFerpapolicy</summary>*/
    public partial class ArFerpapolicy
    {
        /*<summary>The get and set for FerpapolicyId</summary>*/
        public Guid FerpapolicyId { get; set; }
        /*<summary>The get and set for FerpaentityId</summary>*/
        public Guid FerpaentityId { get; set; }
        /*<summary>The get and set for FerpacategoryId</summary>*/
        public Guid FerpacategoryId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Ferpacategory</summary>*/
        public virtual ArFerpacategory Ferpacategory { get; set; }
        /*<summary>The navigational property for Ferpaentity</summary>*/
        public virtual ArFerpaentity Ferpaentity { get; set; }
    }
}