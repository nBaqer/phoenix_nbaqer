﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiBridgeExternalCompanyAllowedServices.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiBridgeExternalCompanyAllowedServices definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiBridgeExternalCompanyAllowedServices</summary>*/
    public partial class SyWapiBridgeExternalCompanyAllowedServices
    {
        /*<summary>The get and set for IdWapiBridgeAllowedService</summary>*/
        public int IdWapiBridgeAllowedService { get; set; }
        /*<summary>The get and set for IdExternalCompanies</summary>*/
        public int IdExternalCompanies { get; set; }
        /*<summary>The get and set for IdAllowedServices</summary>*/
        public int IdAllowedServices { get; set; }

        /*<summary>The navigational property for IdAllowedServicesNavigation</summary>*/
        public virtual SyWapiAllowedServices IdAllowedServicesNavigation { get; set; }
        /*<summary>The navigational property for IdExternalCompaniesNavigation</summary>*/
        public virtual SyWapiExternalCompanies IdExternalCompaniesNavigation { get; set; }
    }
}