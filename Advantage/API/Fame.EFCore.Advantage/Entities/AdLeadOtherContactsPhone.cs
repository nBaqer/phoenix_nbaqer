﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadOtherContactsPhone.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadOtherContactsPhone definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadOtherContactsPhone</summary>*/
    public partial class AdLeadOtherContactsPhone
    {
        /*<summary>The get and set for OtherContactsPhoneId</summary>*/
        public Guid OtherContactsPhoneId { get; set; }
        /*<summary>The get and set for OtherContactId</summary>*/
        public Guid OtherContactId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for PhoneTypeId</summary>*/
        public Guid PhoneTypeId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Extension</summary>*/
        public string Extension { get; set; }
        /*<summary>The get and set for IsForeignPhone</summary>*/
        public bool IsForeignPhone { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for OtherContact</summary>*/
        public virtual AdLeadOtherContacts OtherContact { get; set; }
        /*<summary>The navigational property for PhoneType</summary>*/
        public virtual SyPhoneType PhoneType { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}