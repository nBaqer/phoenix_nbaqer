﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPaymentTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPaymentTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPaymentTypes</summary>*/
    public partial class SaPaymentTypes
    {
        /*<summary>The constructor for SaPaymentTypes</summary>*/
        public SaPaymentTypes()
        {
            SaPayments = new HashSet<SaPayments>();
        }

        /*<summary>The get and set for PaymentTypeId</summary>*/
        public int PaymentTypeId { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }

        /*<summary>The navigational property for SaPayments</summary>*/
        public virtual ICollection<SaPayments>SaPayments { get; set; }
    }
}