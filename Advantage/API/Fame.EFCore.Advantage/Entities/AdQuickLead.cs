﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdQuickLead.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdQuickLead definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdQuickLead</summary>*/
    public partial class AdQuickLead
    {
        /*<summary>The get and set for QkLeadId</summary>*/
        public Guid QkLeadId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public string StudentId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public int? LeadId { get; set; }
        /*<summary>The get and set for LeadValue</summary>*/
        public string LeadValue { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}