﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdExpQuickLeadSections.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdExpQuickLeadSections definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdExpQuickLeadSections</summary>*/
    public partial class AdExpQuickLeadSections
    {
        /*<summary>The get and set for SectionId</summary>*/
        public int? SectionId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int? FldId { get; set; }
        /*<summary>The get and set for ExpId</summary>*/
        public int ExpId { get; set; }
    }
}