﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TblAwardYearMapping.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TblAwardYearMapping definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TblAwardYearMapping</summary>*/
    public partial class TblAwardYearMapping
    {
        /*<summary>The get and set for MappingId</summary>*/
        public int MappingId { get; set; }
        /*<summary>The get and set for FameEspawardYear</summary>*/
        public int? FameEspawardYear { get; set; }
        /*<summary>The get and set for AdvantageAwardYear</summary>*/
        public string AdvantageAwardYear { get; set; }
    }
}