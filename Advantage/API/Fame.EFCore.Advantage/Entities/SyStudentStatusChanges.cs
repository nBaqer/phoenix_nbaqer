﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStudentStatusChanges.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStudentStatusChanges definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStudentStatusChanges</summary>*/
    public partial class SyStudentStatusChanges
    {
        /*<summary>The get and set for StudentStatusChangeId</summary>*/
        public Guid StudentStatusChangeId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for OrigStatusId</summary>*/
        public Guid? OrigStatusId { get; set; }
        /*<summary>The get and set for NewStatusId</summary>*/
        public Guid? NewStatusId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsReversal</summary>*/
        public bool IsReversal { get; set; }
        /*<summary>The get and set for DropReasonId</summary>*/
        public Guid? DropReasonId { get; set; }
        /*<summary>The get and set for DateOfChange</summary>*/
        public DateTime? DateOfChange { get; set; }
        /*<summary>The get and set for Lda</summary>*/
        public DateTime? Lda { get; set; }
        /*<summary>The get and set for CaseNumber</summary>*/
        public string CaseNumber { get; set; }
        /*<summary>The get and set for RequestedBy</summary>*/
        public string RequestedBy { get; set; }
        /*<summary>The get and set for HaveBackup</summary>*/
        public bool? HaveBackup { get; set; }
        /*<summary>The get and set for HaveClientConfirmation</summary>*/
        public bool? HaveClientConfirmation { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for NewStatus</summary>*/
        public virtual SyStatusCodes NewStatus { get; set; }
        /*<summary>The navigational property for OrigStatus</summary>*/
        public virtual SyStatusCodes OrigStatus { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}