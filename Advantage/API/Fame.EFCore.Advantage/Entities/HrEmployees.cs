﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="HrEmployees.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The HrEmployees definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for HrEmployees</summary>*/
    public partial class HrEmployees
    {
        /*<summary>The constructor for HrEmployees</summary>*/
        public HrEmployees()
        {
            HrEmpCerts = new HashSet<HrEmpCerts>();
            HrEmpContactInfo = new HashSet<HrEmpContactInfo>();
            HrEmpDegrees = new HashSet<HrEmpDegrees>();
            HrEmpHrinfo = new HashSet<HrEmpHrinfo>();
            HrEmployeeEmergencyContacts = new HashSet<HrEmployeeEmergencyContacts>();
        }

        /*<summary>The get and set for EmpId</summary>*/
        public Guid EmpId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for Mi</summary>*/
        public string Mi { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for PrefixId</summary>*/
        public Guid? PrefixId { get; set; }
        /*<summary>The get and set for SuffixId</summary>*/
        public Guid? SuffixId { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for Id</summary>*/
        public string Id { get; set; }
        /*<summary>The get and set for BirthDate</summary>*/
        public DateTime? BirthDate { get; set; }
        /*<summary>The get and set for RaceId</summary>*/
        public Guid? RaceId { get; set; }
        /*<summary>The get and set for GenderId</summary>*/
        public Guid? GenderId { get; set; }
        /*<summary>The get and set for MaritalStatId</summary>*/
        public Guid? MaritalStatId { get; set; }
        /*<summary>The get and set for UserName</summary>*/
        public string UserName { get; set; }
        /*<summary>The get and set for Faculty</summary>*/
        public bool Faculty { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool ForeignZip { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for Gender</summary>*/
        public virtual AdGenders Gender { get; set; }
        /*<summary>The navigational property for MaritalStat</summary>*/
        public virtual AdMaritalStatus MaritalStat { get; set; }
        /*<summary>The navigational property for Prefix</summary>*/
        public virtual SyPrefixes Prefix { get; set; }
        /*<summary>The navigational property for Race</summary>*/
        public virtual AdEthCodes Race { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for Suffix</summary>*/
        public virtual SySuffixes Suffix { get; set; }
        /*<summary>The navigational property for HrEmpCerts</summary>*/
        public virtual ICollection<HrEmpCerts>HrEmpCerts { get; set; }
        /*<summary>The navigational property for HrEmpContactInfo</summary>*/
        public virtual ICollection<HrEmpContactInfo>HrEmpContactInfo { get; set; }
        /*<summary>The navigational property for HrEmpDegrees</summary>*/
        public virtual ICollection<HrEmpDegrees>HrEmpDegrees { get; set; }
        /*<summary>The navigational property for HrEmpHrinfo</summary>*/
        public virtual ICollection<HrEmpHrinfo>HrEmpHrinfo { get; set; }
        /*<summary>The navigational property for HrEmployeeEmergencyContacts</summary>*/
        public virtual ICollection<HrEmployeeEmergencyContacts>HrEmployeeEmergencyContacts { get; set; }
    }
}