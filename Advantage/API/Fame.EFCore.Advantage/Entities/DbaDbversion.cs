﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="DbaDbversion.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The DbaDbversion definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for DbaDbversion</summary>*/
    public partial class DbaDbversion
    {
        /*<summary>The get and set for Dbversion</summary>*/
        public string Dbversion { get; set; }
        /*<summary>The get and set for UpgradeDate</summary>*/
        public DateTime UpgradeDate { get; set; }
        /*<summary>The get and set for SubVersion</summary>*/
        public string SubVersion { get; set; }
        /*<summary>The get and set for DbversionId</summary>*/
        public Guid DbversionId { get; set; }
    }
}