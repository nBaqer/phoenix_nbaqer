﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdImportLeadsLookupValsMap.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdImportLeadsLookupValsMap definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdImportLeadsLookupValsMap</summary>*/
    public partial class AdImportLeadsLookupValsMap
    {
        /*<summary>The get and set for LookupValuesMapId</summary>*/
        public Guid LookupValuesMapId { get; set; }
        /*<summary>The get and set for MappingId</summary>*/
        public Guid MappingId { get; set; }
        /*<summary>The get and set for IlfieldId</summary>*/
        public int IlfieldId { get; set; }
        /*<summary>The get and set for FileValue</summary>*/
        public string FileValue { get; set; }
        /*<summary>The get and set for AdvValue</summary>*/
        public Guid AdvValue { get; set; }

        /*<summary>The navigational property for Ilfield</summary>*/
        public virtual AdImportLeadsFields Ilfield { get; set; }
        /*<summary>The navigational property for Mapping</summary>*/
        public virtual AdImportLeadsMappings Mapping { get; set; }
    }
}