﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ParamSection.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ParamSection definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ParamSection</summary>*/
    public partial class ParamSection
    {
        /*<summary>The get and set for SectionId</summary>*/
        public long SectionId { get; set; }
        /*<summary>The get and set for SectionName</summary>*/
        public string SectionName { get; set; }
        /*<summary>The get and set for SectionCaption</summary>*/
        public string SectionCaption { get; set; }
        /*<summary>The get and set for SetId</summary>*/
        public long? SetId { get; set; }
        /*<summary>The get and set for SectionSeq</summary>*/
        public int? SectionSeq { get; set; }
        /*<summary>The get and set for SectionDescription</summary>*/
        public string SectionDescription { get; set; }
        /*<summary>The get and set for SectionType</summary>*/
        public int? SectionType { get; set; }
    }
}