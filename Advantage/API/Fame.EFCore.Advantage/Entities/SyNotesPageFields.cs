﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyNotesPageFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyNotesPageFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyNotesPageFields</summary>*/
    public partial class SyNotesPageFields
    {
        /*<summary>The constructor for SyNotesPageFields</summary>*/
        public SyNotesPageFields()
        {
            AllNotes = new HashSet<AllNotes>();
        }

        /*<summary>The get and set for PageFieldId</summary>*/
        public int PageFieldId { get; set; }
        /*<summary>The get and set for PageName</summary>*/
        public string PageName { get; set; }
        /*<summary>The get and set for FieldCaption</summary>*/
        public string FieldCaption { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for AllNotes</summary>*/
        public virtual ICollection<AllNotes>AllNotes { get; set; }
    }
}