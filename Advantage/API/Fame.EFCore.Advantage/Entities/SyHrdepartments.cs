﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyHrdepartments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyHrdepartments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyHrdepartments</summary>*/
    public partial class SyHrdepartments
    {
        /*<summary>The constructor for SyHrdepartments</summary>*/
        public SyHrdepartments()
        {
            HrEmpHrinfo = new HashSet<HrEmpHrinfo>();
        }

        /*<summary>The get and set for HrdepartmentId</summary>*/
        public Guid HrdepartmentId { get; set; }
        /*<summary>The get and set for HrdepartmentCode</summary>*/
        public string HrdepartmentCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for HrdepartmentDescrip</summary>*/
        public string HrdepartmentDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for HrEmpHrinfo</summary>*/
        public virtual ICollection<HrEmpHrinfo>HrEmpHrinfo { get; set; }
    }
}