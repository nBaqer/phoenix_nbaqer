﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFieldCalculation.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFieldCalculation definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFieldCalculation</summary>*/
    public partial class SyFieldCalculation
    {
        /*<summary>The get and set for SyFieldCalculationId</summary>*/
        public long SyFieldCalculationId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
        /*<summary>The get and set for CalculationSql</summary>*/
        public string CalculationSql { get; set; }

        /*<summary>The navigational property for Fld</summary>*/
        public virtual SyFields Fld { get; set; }
    }
}