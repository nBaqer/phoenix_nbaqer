﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiExternalCompanies.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiExternalCompanies definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiExternalCompanies</summary>*/
    public partial class SyWapiExternalCompanies
    {
        /*<summary>The constructor for SyWapiExternalCompanies</summary>*/
        public SyWapiExternalCompanies()
        {
            SyWapiBridgeExternalCompanyAllowedServices = new HashSet<SyWapiBridgeExternalCompanyAllowedServices>();
            SyWapiSettings = new HashSet<SyWapiSettings>();
        }

        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }

        /*<summary>The navigational property for SyWapiBridgeExternalCompanyAllowedServices</summary>*/
        public virtual ICollection<SyWapiBridgeExternalCompanyAllowedServices>SyWapiBridgeExternalCompanyAllowedServices { get; set; }
        /*<summary>The navigational property for SyWapiSettings</summary>*/
        public virtual ICollection<SyWapiSettings>SyWapiSettings { get; set; }
    }
}