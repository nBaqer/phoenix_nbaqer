﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeads.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeads definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeads</summary>*/
    public partial class AdLeads
    {
        /*<summary>The constructor for AdLeads</summary>*/
        public AdLeads()
        {
            AdEntrTestOverRide = new HashSet<AdEntrTestOverRide>();
            AdExtraCurricular = new HashSet<AdExtraCurricular>();
            AdLastNameHistory = new HashSet<AdLastNameHistory>();
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadByLeadGroups = new HashSet<AdLeadByLeadGroups>();
            AdLeadDocsReceived = new HashSet<AdLeadDocsReceived>();
            AdLeadDuplicatesNewLeadGu = new HashSet<AdLeadDuplicates>();
            AdLeadDuplicatesPosibleDuplicateGu = new HashSet<AdLeadDuplicates>();
            AdLeadEducation = new HashSet<AdLeadEducation>();
            AdLeadEmail = new HashSet<AdLeadEmail>();
            AdLeadEmployment = new HashSet<AdLeadEmployment>();
            AdLeadEntranceTest = new HashSet<AdLeadEntranceTest>();
            AdLeadExtraCurriculars = new HashSet<AdLeadExtraCurriculars>();
            AdLeadNotes = new HashSet<AdLeadNotes>();
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeadOtherContactsEmail = new HashSet<AdLeadOtherContactsEmail>();
            AdLeadOtherContactsPhone = new HashSet<AdLeadOtherContactsPhone>();
            AdLeadPhone = new HashSet<AdLeadPhone>();
            AdLeadReqsReceived = new HashSet<AdLeadReqsReceived>();
            AdLeadSkills = new HashSet<AdLeadSkills>();
            AdLeadTranReceived = new HashSet<AdLeadTranReceived>();
            AdLeadTransactions = new HashSet<AdLeadTransactions>();
            AdSkills = new HashSet<AdSkills>();
            AdVehicles = new HashSet<AdVehicles>();
            AfaLeadSyncException = new HashSet<AfaLeadSyncException>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            LeadImage = new HashSet<LeadImage>();
            SyDocumentHistory = new HashSet<SyDocumentHistory>();
            SyLeadStatusesChanges = new HashSet<SyLeadStatusesChanges>();
        }

        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for ProspectId</summary>*/
        public string ProspectId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for HomeEmail</summary>*/
        public string HomeEmail { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for LeadStatus</summary>*/
        public Guid LeadStatus { get; set; }
        /*<summary>The get and set for WorkEmail</summary>*/
        public string WorkEmail { get; set; }
        /*<summary>The get and set for AddressType</summary>*/
        public Guid? AddressType { get; set; }
        /*<summary>The get and set for Prefix</summary>*/
        public Guid? Prefix { get; set; }
        /*<summary>The get and set for Suffix</summary>*/
        public Guid? Suffix { get; set; }
        /*<summary>The get and set for BirthDate</summary>*/
        public DateTime? BirthDate { get; set; }
        /*<summary>The get and set for Sponsor</summary>*/
        public Guid? Sponsor { get; set; }
        /*<summary>The get and set for AdmissionsRep</summary>*/
        public Guid? AdmissionsRep { get; set; }
        /*<summary>The get and set for AssignedDate</summary>*/
        public DateTime? AssignedDate { get; set; }
        /*<summary>The get and set for Gender</summary>*/
        public Guid? Gender { get; set; }
        /*<summary>The get and set for Race</summary>*/
        public Guid? Race { get; set; }
        /*<summary>The get and set for MaritalStatus</summary>*/
        public Guid? MaritalStatus { get; set; }
        /*<summary>The get and set for FamilyIncome</summary>*/
        public Guid? FamilyIncome { get; set; }
        /*<summary>The get and set for Children</summary>*/
        public string Children { get; set; }
        /*<summary>The get and set for PhoneType</summary>*/
        public Guid? PhoneType { get; set; }
        /*<summary>The get and set for PhoneStatus</summary>*/
        public Guid? PhoneStatus { get; set; }
        /*<summary>The get and set for SourceCategoryId</summary>*/
        public Guid? SourceCategoryId { get; set; }
        /*<summary>The get and set for SourceTypeId</summary>*/
        public Guid? SourceTypeId { get; set; }
        /*<summary>The get and set for SourceDate</summary>*/
        public DateTime? SourceDate { get; set; }
        /*<summary>The get and set for AreaId</summary>*/
        public Guid? AreaId { get; set; }
        /*<summary>The get and set for ProgramId</summary>*/
        public Guid? ProgramId { get; set; }
        /*<summary>The get and set for ExpectedStart</summary>*/
        public DateTime? ExpectedStart { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid? ShiftId { get; set; }
        /*<summary>The get and set for Nationality</summary>*/
        public Guid? Nationality { get; set; }
        /*<summary>The get and set for Citizen</summary>*/
        public Guid? Citizen { get; set; }
        /*<summary>The get and set for DrivLicStateId</summary>*/
        public Guid? DrivLicStateId { get; set; }
        /*<summary>The get and set for DrivLicNumber</summary>*/
        public string DrivLicNumber { get; set; }
        /*<summary>The get and set for AlienNumber</summary>*/
        public string AlienNumber { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for SourceAdvertisement</summary>*/
        public Guid? SourceAdvertisement { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }
        /*<summary>The get and set for Country</summary>*/
        public Guid? Country { get; set; }
        /*<summary>The get and set for County</summary>*/
        public Guid? County { get; set; }
        /*<summary>The get and set for Age</summary>*/
        public string Age { get; set; }
        /*<summary>The get and set for PreviousEducation</summary>*/
        public Guid? PreviousEducation { get; set; }
        /*<summary>The get and set for AddressStatus</summary>*/
        public Guid? AddressStatus { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime? CreatedDate { get; set; }
        /*<summary>The get and set for RecruitmentOffice</summary>*/
        public string RecruitmentOffice { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool ForeignPhone { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool ForeignZip { get; set; }
        /*<summary>The get and set for LeadgrpId</summary>*/
        public Guid? LeadgrpId { get; set; }
        /*<summary>The get and set for DependencyTypeId</summary>*/
        public Guid? DependencyTypeId { get; set; }
        /*<summary>The get and set for DegCertSeekingId</summary>*/
        public Guid? DegCertSeekingId { get; set; }
        /*<summary>The get and set for GeographicTypeId</summary>*/
        public Guid? GeographicTypeId { get; set; }
        /*<summary>The get and set for HousingId</summary>*/
        public Guid? HousingId { get; set; }
        /*<summary>The get and set for Admincriteriaid</summary>*/
        public Guid? Admincriteriaid { get; set; }
        /*<summary>The get and set for DateApplied</summary>*/
        public DateTime? DateApplied { get; set; }
        /*<summary>The get and set for InquiryTime</summary>*/
        public string InquiryTime { get; set; }
        /*<summary>The get and set for AdvertisementNote</summary>*/
        public string AdvertisementNote { get; set; }
        /*<summary>The get and set for Phone2</summary>*/
        public string Phone2 { get; set; }
        /*<summary>The get and set for PhoneType2</summary>*/
        public Guid? PhoneType2 { get; set; }
        /*<summary>The get and set for PhoneStatus2</summary>*/
        public Guid? PhoneStatus2 { get; set; }
        /*<summary>The get and set for ForeignPhone2</summary>*/
        public bool? ForeignPhone2 { get; set; }
        /*<summary>The get and set for DefaultPhone</summary>*/
        public byte? DefaultPhone { get; set; }
        /*<summary>The get and set for IsDisabled</summary>*/
        public bool? IsDisabled { get; set; }
        /*<summary>The get and set for IsFirstTimeInSchool</summary>*/
        public bool IsFirstTimeInSchool { get; set; }
        /*<summary>The get and set for IsFirstTimePostSecSchool</summary>*/
        public bool IsFirstTimePostSecSchool { get; set; }
        /*<summary>The get and set for EntranceInterviewDate</summary>*/
        public DateTime? EntranceInterviewDate { get; set; }
        /*<summary>The get and set for ProgramScheduleId</summary>*/
        public Guid? ProgramScheduleId { get; set; }
        /*<summary>The get and set for BestTime</summary>*/
        public string BestTime { get; set; }
        /*<summary>The get and set for DistanceToSchool</summary>*/
        public int? DistanceToSchool { get; set; }
        /*<summary>The get and set for CampaignId</summary>*/
        public int? CampaignId { get; set; }
        /*<summary>The get and set for ProgramOfInterest</summary>*/
        public string ProgramOfInterest { get; set; }
        /*<summary>The get and set for CampusOfInterest</summary>*/
        public string CampusOfInterest { get; set; }
        /*<summary>The get and set for TransportationId</summary>*/
        public Guid? TransportationId { get; set; }
        /*<summary>The get and set for NickName</summary>*/
        public string NickName { get; set; }
        /*<summary>The get and set for AttendTypeId</summary>*/
        public Guid? AttendTypeId { get; set; }
        /*<summary>The get and set for PreferredContactId</summary>*/
        public int PreferredContactId { get; set; }
        /*<summary>The get and set for AddressApt</summary>*/
        public string AddressApt { get; set; }
        /*<summary>The get and set for NoneEmail</summary>*/
        public bool NoneEmail { get; set; }
        /*<summary>The get and set for HighSchoolId</summary>*/
        public Guid? HighSchoolId { get; set; }
        /*<summary>The get and set for HighSchoolGradDate</summary>*/
        public DateTime? HighSchoolGradDate { get; set; }
        /*<summary>The get and set for AttendingHs</summary>*/
        public bool? AttendingHs { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for StudentNumber</summary>*/
        public string StudentNumber { get; set; }
        /*<summary>The get and set for StudentStatusId</summary>*/
        public Guid StudentStatusId { get; set; }
        /*<summary>The get and set for ReasonNotEnrolledId</summary>*/
        public Guid? ReasonNotEnrolledId { get; set; }
        /*<summary>The get and set for EnrollStateId</summary>*/
        public Guid? EnrollStateId { get; set; }
        /*<summary>The get and set for VendorId</summary>*/
        public Guid? VendorId { get; set; }
        /*<summary>The get and set for AfaStudentId</summary>*/
        public long? AfaStudentId { get; set; }

        /*<summary>The navigational property for AddressStatusNavigation</summary>*/
        public virtual SyStatuses AddressStatusNavigation { get; set; }
        /*<summary>The navigational property for AddressTypeNavigation</summary>*/
        public virtual PlAddressTypes AddressTypeNavigation { get; set; }
        /*<summary>The navigational property for Admincriteria</summary>*/
        public virtual AdAdminCriteria Admincriteria { get; set; }
        /*<summary>The navigational property for AdmissionsRepNavigation</summary>*/
        public virtual SyUsers AdmissionsRepNavigation { get; set; }
        /*<summary>The navigational property for Area</summary>*/
        public virtual ArPrgGrp Area { get; set; }
        /*<summary>The navigational property for AttendType</summary>*/
        public virtual ArAttendTypes AttendType { get; set; }
        /*<summary>The navigational property for Campaign</summary>*/
        public virtual AdVendorCampaign Campaign { get; set; }
        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for CitizenNavigation</summary>*/
        public virtual AdCitizenships CitizenNavigation { get; set; }
        /*<summary>The navigational property for CountryNavigation</summary>*/
        public virtual AdCountries CountryNavigation { get; set; }
        /*<summary>The navigational property for CountyNavigation</summary>*/
        public virtual AdCounties CountyNavigation { get; set; }
        /*<summary>The navigational property for DegCertSeeking</summary>*/
        public virtual AdDegCertSeeking DegCertSeeking { get; set; }
        /*<summary>The navigational property for DependencyType</summary>*/
        public virtual AdDependencyTypes DependencyType { get; set; }
        /*<summary>The navigational property for DrivLicState</summary>*/
        public virtual SyStates DrivLicState { get; set; }
        /*<summary>The navigational property for EnrollState</summary>*/
        public virtual SyStates EnrollState { get; set; }
        /*<summary>The navigational property for FamilyIncomeNavigation</summary>*/
        public virtual SyFamilyIncome FamilyIncomeNavigation { get; set; }
        /*<summary>The navigational property for GenderNavigation</summary>*/
        public virtual AdGenders GenderNavigation { get; set; }
        /*<summary>The navigational property for GeographicType</summary>*/
        public virtual AdGeographicTypes GeographicType { get; set; }
        /*<summary>The navigational property for HighSchool</summary>*/
        public virtual SyInstitutions HighSchool { get; set; }
        /*<summary>The navigational property for Housing</summary>*/
        public virtual ArHousing Housing { get; set; }
        /*<summary>The navigational property for LeadStatusNavigation</summary>*/
        public virtual SyStatusCodes LeadStatusNavigation { get; set; }
        /*<summary>The navigational property for Leadgrp</summary>*/
        public virtual AdLeadGroups Leadgrp { get; set; }
        /*<summary>The navigational property for MaritalStatusNavigation</summary>*/
        public virtual AdMaritalStatus MaritalStatusNavigation { get; set; }
        /*<summary>The navigational property for NationalityNavigation</summary>*/
        public virtual AdNationalities NationalityNavigation { get; set; }
        /*<summary>The navigational property for PhoneStatusNavigation</summary>*/
        public virtual SyStatuses PhoneStatusNavigation { get; set; }
        /*<summary>The navigational property for PrefixNavigation</summary>*/
        public virtual SyPrefixes PrefixNavigation { get; set; }
        /*<summary>The navigational property for PreviousEducationNavigation</summary>*/
        public virtual AdEdLvls PreviousEducationNavigation { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for Program</summary>*/
        public virtual ArPrograms Program { get; set; }
        /*<summary>The navigational property for ProgramSchedule</summary>*/
        public virtual ArProgSchedules ProgramSchedule { get; set; }
        /*<summary>The navigational property for RaceNavigation</summary>*/
        public virtual AdEthCodes RaceNavigation { get; set; }
        /*<summary>The navigational property for Shift</summary>*/
        public virtual ArShifts Shift { get; set; }
        /*<summary>The navigational property for SourceAdvertisementNavigation</summary>*/
        public virtual AdSourceAdvertisement SourceAdvertisementNavigation { get; set; }
        /*<summary>The navigational property for SourceCategory</summary>*/
        public virtual AdSourceCatagory SourceCategory { get; set; }
        /*<summary>The navigational property for SourceType</summary>*/
        public virtual AdSourceType SourceType { get; set; }
        /*<summary>The navigational property for SponsorNavigation</summary>*/
        public virtual AdAgencySponsors SponsorNavigation { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for StudentStatus</summary>*/
        public virtual SyStatuses StudentStatus { get; set; }
        /*<summary>The navigational property for SuffixNavigation</summary>*/
        public virtual SySuffixes SuffixNavigation { get; set; }
        /*<summary>The navigational property for Vendor</summary>*/
        public virtual SyUsers Vendor { get; set; }
        /*<summary>The navigational property for AdEntrTestOverRide</summary>*/
        public virtual ICollection<AdEntrTestOverRide>AdEntrTestOverRide { get; set; }
        /*<summary>The navigational property for AdExtraCurricular</summary>*/
        public virtual ICollection<AdExtraCurricular>AdExtraCurricular { get; set; }
        /*<summary>The navigational property for AdLastNameHistory</summary>*/
        public virtual ICollection<AdLastNameHistory>AdLastNameHistory { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadByLeadGroups</summary>*/
        public virtual ICollection<AdLeadByLeadGroups>AdLeadByLeadGroups { get; set; }
        /*<summary>The navigational property for AdLeadDocsReceived</summary>*/
        public virtual ICollection<AdLeadDocsReceived>AdLeadDocsReceived { get; set; }
        /*<summary>The navigational property for AdLeadDuplicatesNewLeadGu</summary>*/
        public virtual ICollection<AdLeadDuplicates>AdLeadDuplicatesNewLeadGu { get; set; }
        /*<summary>The navigational property for AdLeadDuplicatesPosibleDuplicateGu</summary>*/
        public virtual ICollection<AdLeadDuplicates>AdLeadDuplicatesPosibleDuplicateGu { get; set; }
        /*<summary>The navigational property for AdLeadEducation</summary>*/
        public virtual ICollection<AdLeadEducation>AdLeadEducation { get; set; }
        /*<summary>The navigational property for AdLeadEmail</summary>*/
        public virtual ICollection<AdLeadEmail>AdLeadEmail { get; set; }
        /*<summary>The navigational property for AdLeadEmployment</summary>*/
        public virtual ICollection<AdLeadEmployment>AdLeadEmployment { get; set; }
        /*<summary>The navigational property for AdLeadEntranceTest</summary>*/
        public virtual ICollection<AdLeadEntranceTest>AdLeadEntranceTest { get; set; }
        /*<summary>The navigational property for AdLeadExtraCurriculars</summary>*/
        public virtual ICollection<AdLeadExtraCurriculars>AdLeadExtraCurriculars { get; set; }
        /*<summary>The navigational property for AdLeadNotes</summary>*/
        public virtual ICollection<AdLeadNotes>AdLeadNotes { get; set; }
        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsEmail</summary>*/
        public virtual ICollection<AdLeadOtherContactsEmail>AdLeadOtherContactsEmail { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsPhone</summary>*/
        public virtual ICollection<AdLeadOtherContactsPhone>AdLeadOtherContactsPhone { get; set; }
        /*<summary>The navigational property for AdLeadPhone</summary>*/
        public virtual ICollection<AdLeadPhone>AdLeadPhone { get; set; }
        /*<summary>The navigational property for AdLeadReqsReceived</summary>*/
        public virtual ICollection<AdLeadReqsReceived>AdLeadReqsReceived { get; set; }
        /*<summary>The navigational property for AdLeadSkills</summary>*/
        public virtual ICollection<AdLeadSkills>AdLeadSkills { get; set; }
        /*<summary>The navigational property for AdLeadTranReceived</summary>*/
        public virtual ICollection<AdLeadTranReceived>AdLeadTranReceived { get; set; }
        /*<summary>The navigational property for AdLeadTransactions</summary>*/
        public virtual ICollection<AdLeadTransactions>AdLeadTransactions { get; set; }
        /*<summary>The navigational property for AdSkills</summary>*/
        public virtual ICollection<AdSkills>AdSkills { get; set; }
        /*<summary>The navigational property for AdVehicles</summary>*/
        public virtual ICollection<AdVehicles>AdVehicles { get; set; }
        /*<summary>The navigational property for AfaLeadSyncException</summary>*/
        public virtual ICollection<AfaLeadSyncException>AfaLeadSyncException { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for LeadImage</summary>*/
        public virtual ICollection<LeadImage>LeadImage { get; set; }
        /*<summary>The navigational property for SyDocumentHistory</summary>*/
        public virtual ICollection<SyDocumentHistory>SyDocumentHistory { get; set; }
        /*<summary>The navigational property for SyLeadStatusesChanges</summary>*/
        public virtual ICollection<SyLeadStatusesChanges>SyLeadStatusesChanges { get; set; }
    }
}