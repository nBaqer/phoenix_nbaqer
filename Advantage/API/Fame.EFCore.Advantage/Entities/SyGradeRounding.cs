﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyGradeRounding.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyGradeRounding definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyGradeRounding</summary>*/
    public partial class SyGradeRounding
    {
        /*<summary>The get and set for Graderoundingvalue</summary>*/
        public string Graderoundingvalue { get; set; }
        /*<summary>The get and set for GradeRoundingId</summary>*/
        public Guid GradeRoundingId { get; set; }
    }
}