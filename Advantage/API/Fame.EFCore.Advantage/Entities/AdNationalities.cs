﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdNationalities.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdNationalities definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdNationalities</summary>*/
    public partial class AdNationalities
    {
        /*<summary>The constructor for AdNationalities</summary>*/
        public AdNationalities()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for NationalityId</summary>*/
        public Guid NationalityId { get; set; }
        /*<summary>The get and set for NationalityCode</summary>*/
        public string NationalityCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for NationalityDescrip</summary>*/
        public string NationalityDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}