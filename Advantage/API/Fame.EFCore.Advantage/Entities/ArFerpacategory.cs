﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArFerpacategory.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArFerpacategory definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArFerpacategory</summary>*/
    public partial class ArFerpacategory
    {
        /*<summary>The constructor for ArFerpacategory</summary>*/
        public ArFerpacategory()
        {
            ArFerpapage = new HashSet<ArFerpapage>();
            ArFerpapolicy = new HashSet<ArFerpapolicy>();
        }

        /*<summary>The get and set for FerpacategoryId</summary>*/
        public Guid FerpacategoryId { get; set; }
        /*<summary>The get and set for FerpacategoryCode</summary>*/
        public string FerpacategoryCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for FerpacategoryDescrip</summary>*/
        public string FerpacategoryDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArFerpapage</summary>*/
        public virtual ICollection<ArFerpapage>ArFerpapage { get; set; }
        /*<summary>The navigational property for ArFerpapolicy</summary>*/
        public virtual ICollection<ArFerpapolicy>ArFerpapolicy { get; set; }
    }
}