﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySchoolLogo.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySchoolLogo definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySchoolLogo</summary>*/
    public partial class SySchoolLogo
    {
        /*<summary>The get and set for ImgId</summary>*/
        public int ImgId { get; set; }
        /*<summary>The get and set for Image</summary>*/
        public byte[] Image { get; set; }
        /*<summary>The get and set for ImgLen</summary>*/
        public long? ImgLen { get; set; }
        /*<summary>The get and set for ImgFile</summary>*/
        public string ImgFile { get; set; }
        /*<summary>The get and set for ContentType</summary>*/
        public string ContentType { get; set; }
        /*<summary>The get and set for OfficialUse</summary>*/
        public bool OfficialUse { get; set; }
        /*<summary>The get and set for ImageCode</summary>*/
        public string ImageCode { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
    }
}