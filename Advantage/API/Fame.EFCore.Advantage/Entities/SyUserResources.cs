﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserResources.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserResources definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserResources</summary>*/
    public partial class SyUserResources
    {
        /*<summary>The get and set for ResourceId</summary>*/
        public int ResourceId { get; set; }
        /*<summary>The get and set for Resource</summary>*/
        public string Resource { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ResourceTypeId</summary>*/
        public byte ResourceTypeId { get; set; }
        /*<summary>The get and set for EntityId</summary>*/
        public short? EntityId { get; set; }
        /*<summary>The get and set for Options</summary>*/
        public int? Options { get; set; }
        /*<summary>The get and set for IsPublic</summary>*/
        public bool? IsPublic { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public bool? Active { get; set; }
        /*<summary>The get and set for CreatedBy</summary>*/
        public Guid CreatedBy { get; set; }
        /*<summary>The get and set for UseLeftJoin</summary>*/
        public bool? UseLeftJoin { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Entity</summary>*/
        public virtual SyResources Entity { get; set; }
        /*<summary>The navigational property for ResourceType</summary>*/
        public virtual SyResourceTypes ResourceType { get; set; }
    }
}