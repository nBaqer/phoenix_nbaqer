﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStudentTimeClockPunches.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStudentTimeClockPunches definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStudentTimeClockPunches</summary>*/
    public partial class ArStudentTimeClockPunches
    {
        /*<summary>The get and set for BadgeId</summary>*/
        public string BadgeId { get; set; }
        /*<summary>The get and set for PunchTime</summary>*/
        public DateTime PunchTime { get; set; }
        /*<summary>The get and set for PunchType</summary>*/
        public short PunchType { get; set; }
        /*<summary>The get and set for ClockId</summary>*/
        public string ClockId { get; set; }
        /*<summary>The get and set for Status</summary>*/
        public short? Status { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for FromSystem</summary>*/
        public bool? FromSystem { get; set; }
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for ClsSectMeetingId</summary>*/
        public Guid? ClsSectMeetingId { get; set; }
        /*<summary>The get and set for SpecialCode</summary>*/
        public string SpecialCode { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}