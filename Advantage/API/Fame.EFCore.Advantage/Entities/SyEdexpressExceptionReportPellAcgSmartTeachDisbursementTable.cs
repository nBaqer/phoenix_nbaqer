﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable</summary>*/
    public partial class SyEdexpressExceptionReportPellAcgSmartTeachDisbursementTable
    {
        /*<summary>The get and set for DetailId</summary>*/
        public Guid DetailId { get; set; }
        /*<summary>The get and set for ExceptionReportId</summary>*/
        public Guid ExceptionReportId { get; set; }
        /*<summary>The get and set for DbIn</summary>*/
        public string DbIn { get; set; }
        /*<summary>The get and set for Filter</summary>*/
        public string Filter { get; set; }
        /*<summary>The get and set for AccDisbAmount</summary>*/
        public string AccDisbAmount { get; set; }
        /*<summary>The get and set for DisbDate</summary>*/
        public string DisbDate { get; set; }
        /*<summary>The get and set for DisbNum</summary>*/
        public string DisbNum { get; set; }
        /*<summary>The get and set for DisbRelIndi</summary>*/
        public string DisbRelIndi { get; set; }
        /*<summary>The get and set for DusbSeqNum</summary>*/
        public string DusbSeqNum { get; set; }
        /*<summary>The get and set for SimittedDisbAmount</summary>*/
        public string SimittedDisbAmount { get; set; }
        /*<summary>The get and set for ActionStatusDisb</summary>*/
        public string ActionStatusDisb { get; set; }
        /*<summary>The get and set for OriginalSsn</summary>*/
        public string OriginalSsn { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ErrorMsg</summary>*/
        public string ErrorMsg { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for ExceptionGuid</summary>*/
        public Guid? ExceptionGuid { get; set; }
    }
}