﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTables.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTables definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTables</summary>*/
    public partial class SyTables
    {
        /*<summary>The constructor for SyTables</summary>*/
        public SyTables()
        {
            AfaCatalogMapping = new HashSet<AfaCatalogMapping>();
            SyDdls = new HashSet<SyDdls>();
            SyTblCaptions = new HashSet<SyTblCaptions>();
            SyTblFlds = new HashSet<SyTblFlds>();
        }

        /*<summary>The get and set for TblId</summary>*/
        public int TblId { get; set; }
        /*<summary>The get and set for TblName</summary>*/
        public string TblName { get; set; }
        /*<summary>The get and set for TblDescrip</summary>*/
        public string TblDescrip { get; set; }
        /*<summary>The get and set for TblPk</summary>*/
        public int TblPk { get; set; }

        /*<summary>The navigational property for AfaCatalogMapping</summary>*/
        public virtual ICollection<AfaCatalogMapping>AfaCatalogMapping { get; set; }
        /*<summary>The navigational property for SyDdls</summary>*/
        public virtual ICollection<SyDdls>SyDdls { get; set; }
        /*<summary>The navigational property for SyTblCaptions</summary>*/
        public virtual ICollection<SyTblCaptions>SyTblCaptions { get; set; }
        /*<summary>The navigational property for SyTblFlds</summary>*/
        public virtual ICollection<SyTblFlds>SyTblFlds { get; set; }
    }
}