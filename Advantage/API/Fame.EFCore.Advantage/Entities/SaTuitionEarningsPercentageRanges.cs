﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTuitionEarningsPercentageRanges.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTuitionEarningsPercentageRanges definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTuitionEarningsPercentageRanges</summary>*/
    public partial class SaTuitionEarningsPercentageRanges
    {
        /*<summary>The get and set for TuitionEarningsPercentageRangeId</summary>*/
        public Guid TuitionEarningsPercentageRangeId { get; set; }
        /*<summary>The get and set for TuitionEarningId</summary>*/
        public Guid TuitionEarningId { get; set; }
        /*<summary>The get and set for UpTo</summary>*/
        public decimal UpTo { get; set; }
        /*<summary>The get and set for EarnPercent</summary>*/
        public decimal EarnPercent { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for TuitionEarning</summary>*/
        public virtual SaTuitionEarnings TuitionEarning { get; set; }
    }
}