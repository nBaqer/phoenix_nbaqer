﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqGroups</summary>*/
    public partial class AdReqGroups
    {
        /*<summary>The constructor for AdReqGroups</summary>*/
        public AdReqGroups()
        {
            AdLeadGrpReqGroups = new HashSet<AdLeadGrpReqGroups>();
            AdPrgVerTestDetails = new HashSet<AdPrgVerTestDetails>();
            AdReqGrpDef = new HashSet<AdReqGrpDef>();
        }

        /*<summary>The get and set for ReqGrpId</summary>*/
        public Guid ReqGrpId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsMandatoryReqGrp</summary>*/
        public bool? IsMandatoryReqGrp { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadGrpReqGroups</summary>*/
        public virtual ICollection<AdLeadGrpReqGroups>AdLeadGrpReqGroups { get; set; }
        /*<summary>The navigational property for AdPrgVerTestDetails</summary>*/
        public virtual ICollection<AdPrgVerTestDetails>AdPrgVerTestDetails { get; set; }
        /*<summary>The navigational property for AdReqGrpDef</summary>*/
        public virtual ICollection<AdReqGrpDef>AdReqGrpDef { get; set; }
    }
}