﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRolesModules.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRolesModules definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRolesModules</summary>*/
    public partial class SyRolesModules
    {
        /*<summary>The get and set for RoleModuleId</summary>*/
        public Guid RoleModuleId { get; set; }
        /*<summary>The get and set for RoleId</summary>*/
        public Guid? RoleId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public short? ModuleId { get; set; }

        /*<summary>The navigational property for Module</summary>*/
        public virtual SyResources Module { get; set; }
    }
}