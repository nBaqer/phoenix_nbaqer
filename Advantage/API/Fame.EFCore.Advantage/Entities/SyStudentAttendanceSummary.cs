﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStudentAttendanceSummary.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStudentAttendanceSummary definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStudentAttendanceSummary</summary>*/
    public partial class SyStudentAttendanceSummary
    {
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid? ClsSectionId { get; set; }
        /*<summary>The get and set for StudentAttendedDate</summary>*/
        public DateTime? StudentAttendedDate { get; set; }
        /*<summary>The get and set for ScheduledDays</summary>*/
        public decimal? ScheduledDays { get; set; }
        /*<summary>The get and set for ActualDays</summary>*/
        public decimal? ActualDays { get; set; }
        /*<summary>The get and set for ActualRunningScheduledDays</summary>*/
        public decimal? ActualRunningScheduledDays { get; set; }
        /*<summary>The get and set for ActualRunningPresentDays</summary>*/
        public decimal? ActualRunningPresentDays { get; set; }
        /*<summary>The get and set for ActualRunningAbsentDays</summary>*/
        public decimal? ActualRunningAbsentDays { get; set; }
        /*<summary>The get and set for ActualRunningMakeupDays</summary>*/
        public decimal? ActualRunningMakeupDays { get; set; }
        /*<summary>The get and set for ActualRunningTardyDays</summary>*/
        public decimal? ActualRunningTardyDays { get; set; }
        /*<summary>The get and set for AdjustedPresentDays</summary>*/
        public decimal? AdjustedPresentDays { get; set; }
        /*<summary>The get and set for AdjustedAbsentDays</summary>*/
        public decimal? AdjustedAbsentDays { get; set; }
        /*<summary>The get and set for AttendanceTrackType</summary>*/
        public string AttendanceTrackType { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for ActualPresentDaysConvertToHoursCalc</summary>*/
        public decimal? ActualPresentDaysConvertToHoursCalc { get; set; }
        /*<summary>The get and set for ScheduledHours</summary>*/
        public decimal? ScheduledHours { get; set; }
        /*<summary>The get and set for ActualAbsentDaysConvertToHoursCalc</summary>*/
        public decimal? ActualAbsentDaysConvertToHoursCalc { get; set; }
        /*<summary>The get and set for TermStartDate</summary>*/
        public DateTime? TermStartDate { get; set; }
        /*<summary>The get and set for ActualPresentDaysConvertToHours</summary>*/
        public decimal? ActualPresentDaysConvertToHours { get; set; }
        /*<summary>The get and set for ActualAbsentDaysConvertToHours</summary>*/
        public decimal? ActualAbsentDaysConvertToHours { get; set; }
        /*<summary>The get and set for ActualTardyDaysConvertToHours</summary>*/
        public decimal? ActualTardyDaysConvertToHours { get; set; }
        /*<summary>The get and set for TermDescrip</summary>*/
        public string TermDescrip { get; set; }
        /*<summary>The get and set for Tardiesmakingabsence</summary>*/
        public int? Tardiesmakingabsence { get; set; }
        /*<summary>The get and set for IsExcused</summary>*/
        public bool? IsExcused { get; set; }
        /*<summary>The get and set for ActualRunningExcusedDays</summary>*/
        public int? ActualRunningExcusedDays { get; set; }
        /*<summary>The get and set for IsTardy</summary>*/
        public int? IsTardy { get; set; }
        /*<summary>The get and set for StudentAttendanceSummaryId</summary>*/
        public int StudentAttendanceSummaryId { get; set; }
    }
}