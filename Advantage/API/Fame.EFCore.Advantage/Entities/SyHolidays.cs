﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyHolidays.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyHolidays definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyHolidays</summary>*/
    public partial class SyHolidays
    {
        /*<summary>The get and set for HolidayId</summary>*/
        public Guid HolidayId { get; set; }
        /*<summary>The get and set for HolidayCode</summary>*/
        public string HolidayCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for HolidayDescrip</summary>*/
        public string HolidayDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for HolidayStartDate</summary>*/
        public DateTime HolidayStartDate { get; set; }
        /*<summary>The get and set for AllDay</summary>*/
        public bool AllDay { get; set; }
        /*<summary>The get and set for StartTimeId</summary>*/
        public Guid? StartTimeId { get; set; }
        /*<summary>The get and set for EndTimeId</summary>*/
        public Guid? EndTimeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for HolidayEndDate</summary>*/
        public DateTime? HolidayEndDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for EndTime</summary>*/
        public virtual CmTimeInterval EndTime { get; set; }
        /*<summary>The navigational property for StartTime</summary>*/
        public virtual CmTimeInterval StartTime { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}