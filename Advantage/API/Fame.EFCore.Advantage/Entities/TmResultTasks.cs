﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmResultTasks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmResultTasks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmResultTasks</summary>*/
    public partial class TmResultTasks
    {
        /*<summary>The get and set for ResultTaskId</summary>*/
        public Guid ResultTaskId { get; set; }
        /*<summary>The get and set for ResultId</summary>*/
        public Guid? ResultId { get; set; }
        /*<summary>The get and set for TaskId</summary>*/
        public Guid? TaskId { get; set; }
        /*<summary>The get and set for TaskOrder</summary>*/
        public short? TaskOrder { get; set; }
        /*<summary>The get and set for StartGap</summary>*/
        public short? StartGap { get; set; }
        /*<summary>The get and set for StartGapUnit</summary>*/
        public string StartGapUnit { get; set; }

        /*<summary>The navigational property for Result</summary>*/
        public virtual TmResults Result { get; set; }
        /*<summary>The navigational property for Task</summary>*/
        public virtual TmTasks Task { get; set; }
    }
}