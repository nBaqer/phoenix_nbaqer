﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdGeographicTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdGeographicTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdGeographicTypes</summary>*/
    public partial class AdGeographicTypes
    {
        /*<summary>The constructor for AdGeographicTypes</summary>*/
        public AdGeographicTypes()
        {
            AdLeads = new HashSet<AdLeads>();
        }

        /*<summary>The get and set for GeographicTypeId</summary>*/
        public Guid GeographicTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
    }
}