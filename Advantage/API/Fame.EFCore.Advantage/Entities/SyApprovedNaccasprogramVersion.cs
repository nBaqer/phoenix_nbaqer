﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyApprovedNaccasprogramVersion.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyApprovedNaccasprogramVersion definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyApprovedNaccasprogramVersion</summary>*/
    public partial class SyApprovedNaccasprogramVersion
    {
        /*<summary>The get and set for IsApproved</summary>*/
        public bool IsApproved { get; set; }
        /*<summary>The get and set for ProgramVersionId</summary>*/
        public Guid? ProgramVersionId { get; set; }
        /*<summary>The get and set for CreatedById</summary>*/
        public Guid? CreatedById { get; set; }
        /*<summary>The get and set for CreateDate</summary>*/
        public DateTime? CreateDate { get; set; }
        /*<summary>The get and set for NaccasSettingId</summary>*/
        public int NaccasSettingId { get; set; }
        /*<summary>The get and set for ApprovedNaccasprogramVersionId</summary>*/
        public int ApprovedNaccasprogramVersionId { get; set; }

        /*<summary>The navigational property for CreatedBy</summary>*/
        public virtual SyUsers CreatedBy { get; set; }
        /*<summary>The navigational property for NaccasSetting</summary>*/
        public virtual SyNaccasSettings NaccasSetting { get; set; }
        /*<summary>The navigational property for ProgramVersion</summary>*/
        public virtual ArPrgVersions ProgramVersion { get; set; }
    }
}