﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TblPrefReport.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TblPrefReport definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TblPrefReport</summary>*/
    public partial class TblPrefReport
    {
        /*<summary>The get and set for PrefReportId</summary>*/
        public Guid PrefReportId { get; set; }
        /*<summary>The get and set for PrefId</summary>*/
        public Guid? PrefId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid? TermId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for Termstartdate</summary>*/
        public DateTime? Termstartdate { get; set; }
        /*<summary>The get and set for Termenddate</summary>*/
        public DateTime? Termenddate { get; set; }
        /*<summary>The get and set for Attendancestartdate</summary>*/
        public DateTime? Attendancestartdate { get; set; }
        /*<summary>The get and set for Attendanceenddate</summary>*/
        public DateTime? Attendanceenddate { get; set; }
        /*<summary>The get and set for Moduser</summary>*/
        public string Moduser { get; set; }
        /*<summary>The get and set for Moddate</summary>*/
        public DateTime? Moddate { get; set; }
    }
}