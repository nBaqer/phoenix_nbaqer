﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="RptAdmissionsRep.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The RptAdmissionsRep definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for RptAdmissionsRep</summary>*/
    public partial class RptAdmissionsRep
    {
        /*<summary>The get and set for AdmissionsRepId</summary>*/
        public Guid AdmissionsRepId { get; set; }
        /*<summary>The get and set for AdmissionsRepDescrip</summary>*/
        public string AdmissionsRepDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for RptAdmissionsRepId</summary>*/
        public Guid RptAdmissionsRepId { get; set; }

        /*<summary>The navigational property for AdmissionsRep</summary>*/
        public virtual SyUsers AdmissionsRep { get; set; }
        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}