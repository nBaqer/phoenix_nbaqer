﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmTasks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmTasks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmTasks</summary>*/
    public partial class TmTasks
    {
        /*<summary>The constructor for TmTasks</summary>*/
        public TmTasks()
        {
            TmResultTasks = new HashSet<TmResultTasks>();
            TmTaskResults = new HashSet<TmTaskResults>();
            TmUserTasks = new HashSet<TmUserTasks>();
        }

        /*<summary>The get and set for TaskId</summary>*/
        public Guid TaskId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGroupId</summary>*/
        public Guid? CampGroupId { get; set; }
        /*<summary>The get and set for CategoryId</summary>*/
        public Guid? CategoryId { get; set; }
        /*<summary>The get and set for ModuleEntityId</summary>*/
        public int? ModuleEntityId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid? StatusCodeId { get; set; }

        /*<summary>The navigational property for Category</summary>*/
        public virtual TmCategories Category { get; set; }
        /*<summary>The navigational property for StatusCode</summary>*/
        public virtual SyStatusCodes StatusCode { get; set; }
        /*<summary>The navigational property for TmResultTasks</summary>*/
        public virtual ICollection<TmResultTasks>TmResultTasks { get; set; }
        /*<summary>The navigational property for TmTaskResults</summary>*/
        public virtual ICollection<TmTaskResults>TmTaskResults { get; set; }
        /*<summary>The navigational property for TmUserTasks</summary>*/
        public virtual ICollection<TmUserTasks>TmUserTasks { get; set; }
    }
}