﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStatusLevels.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStatusLevels definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStatusLevels</summary>*/
    public partial class SyStatusLevels
    {
        /*<summary>The constructor for SyStatusLevels</summary>*/
        public SyStatusLevels()
        {
            SySysStatus = new HashSet<SySysStatus>();
        }

        /*<summary>The get and set for StatusLevelId</summary>*/
        public int StatusLevelId { get; set; }
        /*<summary>The get and set for StatusLevelDescrip</summary>*/
        public string StatusLevelDescrip { get; set; }

        /*<summary>The navigational property for SySysStatus</summary>*/
        public virtual ICollection<SySysStatus>SySysStatus { get; set; }
    }
}