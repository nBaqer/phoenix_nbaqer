﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSapquantByInstruction.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSapquantByInstruction definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSapquantByInstruction</summary>*/
    public partial class ArSapquantByInstruction
    {
        /*<summary>The get and set for SapquantInsTypeId</summary>*/
        public Guid SapquantInsTypeId { get; set; }
        /*<summary>The get and set for QuantMinValue</summary>*/
        public decimal QuantMinValue { get; set; }
        /*<summary>The get and set for SapdetailId</summary>*/
        public Guid SapdetailId { get; set; }
        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for Moddate</summary>*/
        public DateTime Moddate { get; set; }

        /*<summary>The navigational property for InstructionType</summary>*/
        public virtual ArInstructionType InstructionType { get; set; }
        /*<summary>The navigational property for Sapdetail</summary>*/
        public virtual ArSapdetails Sapdetail { get; set; }
    }
}