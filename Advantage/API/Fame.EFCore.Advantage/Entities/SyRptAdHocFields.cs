﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptAdHocFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptAdHocFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptAdHocFields</summary>*/
    public partial class SyRptAdHocFields
    {
        /*<summary>The get and set for AdHocFieldId</summary>*/
        public Guid AdHocFieldId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for TblFldsId</summary>*/
        public int? TblFldsId { get; set; }
        /*<summary>The get and set for ColOrder</summary>*/
        public short? ColOrder { get; set; }
        /*<summary>The get and set for Header</summary>*/
        public string Header { get; set; }
        /*<summary>The get and set for Visible</summary>*/
        public bool? Visible { get; set; }
        /*<summary>The get and set for FormatString</summary>*/
        public string FormatString { get; set; }
        /*<summary>The get and set for Width</summary>*/
        public int? Width { get; set; }
        /*<summary>The get and set for ShowInGroupBy</summary>*/
        public bool? ShowInGroupBy { get; set; }
        /*<summary>The get and set for ShowInSummary</summary>*/
        public bool? ShowInSummary { get; set; }
        /*<summary>The get and set for SummaryType</summary>*/
        public int? SummaryType { get; set; }
        /*<summary>The get and set for GroupBy</summary>*/
        public bool? GroupBy { get; set; }
        /*<summary>The get and set for SortBy</summary>*/
        public bool? SortBy { get; set; }
        /*<summary>The get and set for IsParam</summary>*/
        public bool? IsParam { get; set; }
        /*<summary>The get and set for IsRequired</summary>*/
        public bool? IsRequired { get; set; }
        /*<summary>The get and set for Sdfid</summary>*/
        public Guid? Sdfid { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
        /*<summary>The navigational property for TblFlds</summary>*/
        public virtual SyTblFlds TblFlds { get; set; }
    }
}