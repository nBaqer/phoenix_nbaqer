﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadExtraCurriculars.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadExtraCurriculars definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadExtraCurriculars</summary>*/
    public partial class AdLeadExtraCurriculars
    {
        /*<summary>The get and set for LeadExtraCurricularId</summary>*/
        public Guid LeadExtraCurricularId { get; set; }
        /*<summary>The get and set for ExtraCurricularId</summary>*/
        public Guid? ExtraCurricularId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid? LeadId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
    }
}