﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlSkillGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlSkillGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlSkillGroups</summary>*/
    public partial class PlSkillGroups
    {
        /*<summary>The constructor for PlSkillGroups</summary>*/
        public PlSkillGroups()
        {
            AdSkills = new HashSet<AdSkills>();
            PlSkills = new HashSet<PlSkills>();
        }

        /*<summary>The get and set for SkillGrpId</summary>*/
        public Guid SkillGrpId { get; set; }
        /*<summary>The get and set for SkillGrpCode</summary>*/
        public string SkillGrpCode { get; set; }
        /*<summary>The get and set for SkillGrpName</summary>*/
        public string SkillGrpName { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdSkills</summary>*/
        public virtual ICollection<AdSkills>AdSkills { get; set; }
        /*<summary>The navigational property for PlSkills</summary>*/
        public virtual ICollection<PlSkills>PlSkills { get; set; }
    }
}