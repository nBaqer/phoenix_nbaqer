﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStuRestrictionTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStuRestrictionTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStuRestrictionTypes</summary>*/
    public partial class SyStuRestrictionTypes
    {
        /*<summary>The constructor for SyStuRestrictionTypes</summary>*/
        public SyStuRestrictionTypes()
        {
            SyStuRestrictions = new HashSet<SyStuRestrictions>();
        }

        /*<summary>The get and set for RestrictionTypeId</summary>*/
        public Guid RestrictionTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for EnableAlertButton</summary>*/
        public bool EnableAlertButton { get; set; }
        /*<summary>The get and set for BlkGradeBook</summary>*/
        public bool BlkGradeBook { get; set; }
        /*<summary>The get and set for BlkAttendancePosting</summary>*/
        public bool BlkAttendancePosting { get; set; }
        /*<summary>The get and set for BlkFa</summary>*/
        public bool BlkFa { get; set; }
        /*<summary>The get and set for BlkStuPortalClsSched</summary>*/
        public bool BlkStuPortalClsSched { get; set; }
        /*<summary>The get and set for BlkStuPortalGrdBk</summary>*/
        public bool BlkStuPortalGrdBk { get; set; }
        /*<summary>The get and set for BlkStuPortalResume</summary>*/
        public bool BlkStuPortalResume { get; set; }
        /*<summary>The get and set for BlkStuPortalEntire</summary>*/
        public bool BlkStuPortalEntire { get; set; }
        /*<summary>The get and set for BlkStuPortalCareer</summary>*/
        public bool BlkStuPortalCareer { get; set; }
        /*<summary>The get and set for RaiseToRestrictionId</summary>*/
        public Guid? RaiseToRestrictionId { get; set; }
        /*<summary>The get and set for DelayDays</summary>*/
        public short DelayDays { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyStuRestrictions</summary>*/
        public virtual ICollection<SyStuRestrictions>SyStuRestrictions { get; set; }
    }
}