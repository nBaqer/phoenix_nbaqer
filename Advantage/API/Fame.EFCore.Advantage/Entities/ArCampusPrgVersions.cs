﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCampusPrgVersions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCampusPrgVersions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCampusPrgVersions</summary>*/
    public partial class ArCampusPrgVersions
    {
        /*<summary>The get and set for CampusPrgVerId</summary>*/
        public Guid CampusPrgVerId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for IsTitleIv</summary>*/
        public bool IsTitleIv { get; set; }
        /*<summary>The get and set for IsFameapproved</summary>*/
        public bool IsFameapproved { get; set; }
        /*<summary>The get and set for IsSelfPaced</summary>*/
        public bool? IsSelfPaced { get; set; }
        /*<summary>The get and set for CalculationPeriodTypeId</summary>*/
        public Guid? CalculationPeriodTypeId { get; set; }
        /*<summary>The get and set for AllowExcusAbsPerPayPrd</summary>*/
        public decimal? AllowExcusAbsPerPayPrd { get; set; }
        /*<summary>The get and set for TermSubEqualInLen</summary>*/
        public bool? TermSubEqualInLen { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
    }
}