﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArLoareasons.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArLoareasons definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArLoareasons</summary>*/
    public partial class ArLoareasons
    {
        /*<summary>The constructor for ArLoareasons</summary>*/
        public ArLoareasons()
        {
            ArStudentLoas = new HashSet<ArStudentLoas>();
        }

        /*<summary>The get and set for LoareasonId</summary>*/
        public Guid LoareasonId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArStudentLoas</summary>*/
        public virtual ICollection<ArStudentLoas>ArStudentLoas { get; set; }
    }
}