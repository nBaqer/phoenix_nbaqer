﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdStuGrpStudents.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdStuGrpStudents definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdStuGrpStudents</summary>*/
    public partial class AdStuGrpStudents
    {
        /*<summary>The get and set for StuGrpStuId</summary>*/
        public Guid StuGrpStuId { get; set; }
        /*<summary>The get and set for StuGrpId</summary>*/
        public Guid StuGrpId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for DateAdded</summary>*/
        public DateTime DateAdded { get; set; }
        /*<summary>The get and set for DateRemoved</summary>*/
        public DateTime? DateRemoved { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for IsDeleted</summary>*/
        public bool? IsDeleted { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid? StuEnrollId { get; set; }
    }
}