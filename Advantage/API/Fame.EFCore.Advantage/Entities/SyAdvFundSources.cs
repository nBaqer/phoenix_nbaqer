﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyAdvFundSources.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyAdvFundSources definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyAdvFundSources</summary>*/
    public partial class SyAdvFundSources
    {
        /*<summary>The constructor for SyAdvFundSources</summary>*/
        public SyAdvFundSources()
        {
            SaFundSources = new HashSet<SaFundSources>();
        }

        /*<summary>The get and set for AdvFundSourceId</summary>*/
        public byte AdvFundSourceId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for SaFundSources</summary>*/
        public virtual ICollection<SaFundSources>SaFundSources { get; set; }
    }
}