﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStudentOld.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStudentOld definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStudentOld</summary>*/
    public partial class ArStudentOld
    {
        /*<summary>The get and set for StudentId</summary>*/
        public Guid StudentId { get; set; }
        /*<summary>The get and set for FirstName</summary>*/
        public string FirstName { get; set; }
        /*<summary>The get and set for MiddleName</summary>*/
        public string MiddleName { get; set; }
        /*<summary>The get and set for LastName</summary>*/
        public string LastName { get; set; }
        /*<summary>The get and set for Ssn</summary>*/
        public string Ssn { get; set; }
        /*<summary>The get and set for StudentStatus</summary>*/
        public Guid StudentStatus { get; set; }
        /*<summary>The get and set for WorkEmail</summary>*/
        public string WorkEmail { get; set; }
        /*<summary>The get and set for HomeEmail</summary>*/
        public string HomeEmail { get; set; }
        /*<summary>The get and set for Dob</summary>*/
        public DateTime? Dob { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Prefix</summary>*/
        public Guid? Prefix { get; set; }
        /*<summary>The get and set for Suffix</summary>*/
        public Guid? Suffix { get; set; }
        /*<summary>The get and set for Sponsor</summary>*/
        public Guid? Sponsor { get; set; }
        /*<summary>The get and set for AssignedDate</summary>*/
        public DateTime? AssignedDate { get; set; }
        /*<summary>The get and set for Gender</summary>*/
        public Guid? Gender { get; set; }
        /*<summary>The get and set for Race</summary>*/
        public Guid? Race { get; set; }
        /*<summary>The get and set for MaritalStatus</summary>*/
        public Guid? MaritalStatus { get; set; }
        /*<summary>The get and set for FamilyIncome</summary>*/
        public Guid? FamilyIncome { get; set; }
        /*<summary>The get and set for Children</summary>*/
        public int? Children { get; set; }
        /*<summary>The get and set for ExpectedStart</summary>*/
        public DateTime? ExpectedStart { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid? ShiftId { get; set; }
        /*<summary>The get and set for Nationality</summary>*/
        public Guid? Nationality { get; set; }
        /*<summary>The get and set for Citizen</summary>*/
        public Guid? Citizen { get; set; }
        /*<summary>The get and set for DrivLicStateId</summary>*/
        public Guid? DrivLicStateId { get; set; }
        /*<summary>The get and set for DrivLicNumber</summary>*/
        public string DrivLicNumber { get; set; }
        /*<summary>The get and set for AlienNumber</summary>*/
        public string AlienNumber { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for County</summary>*/
        public Guid? County { get; set; }
        /*<summary>The get and set for SourceDate</summary>*/
        public DateTime? SourceDate { get; set; }
        /*<summary>The get and set for EdLvlId</summary>*/
        public Guid? EdLvlId { get; set; }
        /*<summary>The get and set for StudentNumber</summary>*/
        public string StudentNumber { get; set; }
        /*<summary>The get and set for Objective</summary>*/
        public string Objective { get; set; }
        /*<summary>The get and set for DependencyTypeId</summary>*/
        public Guid? DependencyTypeId { get; set; }
        /*<summary>The get and set for DegCertSeekingId</summary>*/
        public Guid? DegCertSeekingId { get; set; }
        /*<summary>The get and set for GeographicTypeId</summary>*/
        public Guid? GeographicTypeId { get; set; }
        /*<summary>The get and set for HousingId</summary>*/
        public Guid? HousingId { get; set; }
        /*<summary>The get and set for Admincriteriaid</summary>*/
        public Guid? Admincriteriaid { get; set; }
        /*<summary>The get and set for AttendTypeId</summary>*/
        public Guid? AttendTypeId { get; set; }
        /*<summary>The get and set for ApportionedCreditBalanceAy1</summary>*/
        public decimal? ApportionedCreditBalanceAy1 { get; set; }
        /*<summary>The get and set for ApportionedCreditBalanceAy2</summary>*/
        public decimal? ApportionedCreditBalanceAy2 { get; set; }
    }
}