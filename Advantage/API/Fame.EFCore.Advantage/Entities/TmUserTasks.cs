﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TmUserTasks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TmUserTasks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TmUserTasks</summary>*/
    public partial class TmUserTasks
    {
        /*<summary>The get and set for UserTaskId</summary>*/
        public Guid UserTaskId { get; set; }
        /*<summary>The get and set for TaskId</summary>*/
        public Guid TaskId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for Priority</summary>*/
        public short? Priority { get; set; }
        /*<summary>The get and set for Message</summary>*/
        public string Message { get; set; }
        /*<summary>The get and set for ReId</summary>*/
        public Guid? ReId { get; set; }
        /*<summary>The get and set for OwnerId</summary>*/
        public Guid? OwnerId { get; set; }
        /*<summary>The get and set for AssignedById</summary>*/
        public Guid? AssignedById { get; set; }
        /*<summary>The get and set for PrevUserTaskId</summary>*/
        public Guid? PrevUserTaskId { get; set; }
        /*<summary>The get and set for ResultId</summary>*/
        public Guid? ResultId { get; set; }
        /*<summary>The get and set for Status</summary>*/
        public short? Status { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }

        /*<summary>The navigational property for Task</summary>*/
        public virtual TmTasks Task { get; set; }
    }
}