﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCmpGrpCmps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCmpGrpCmps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCmpGrpCmps</summary>*/
    public partial class SyCmpGrpCmps
    {
        /*<summary>The get and set for CmpGrpCmpId</summary>*/
        public Guid CmpGrpCmpId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
    }
}