﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWidgetUserResourceSettings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWidgetUserResourceSettings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWidgetUserResourceSettings</summary>*/
    public partial class SyWidgetUserResourceSettings
    {
        /*<summary>The get and set for WidgetUserResourceSettingId</summary>*/
        public Guid WidgetUserResourceSettingId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for WidgetId</summary>*/
        public Guid? WidgetId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public short? ResourceId { get; set; }
        /*<summary>The get and set for PositionX</summary>*/
        public int PositionX { get; set; }
        /*<summary>The get and set for PositionY</summary>*/
        public int PositionY { get; set; }
        /*<summary>The get and set for IsVisible</summary>*/
        public bool IsVisible { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for Resource</summary>*/
        public virtual SyResources Resource { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for User</summary>*/
        public virtual SyUsers User { get; set; }
        /*<summary>The navigational property for Widget</summary>*/
        public virtual SyWidgets Widget { get; set; }
    }
}