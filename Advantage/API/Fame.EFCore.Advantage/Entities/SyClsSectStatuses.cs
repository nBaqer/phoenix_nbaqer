﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyClsSectStatuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyClsSectStatuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyClsSectStatuses</summary>*/
    public partial class SyClsSectStatuses
    {
        /*<summary>The get and set for ClsSectStatusId</summary>*/
        public int ClsSectStatusId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
    }
}