﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFields</summary>*/
    public partial class SyFields
    {
        /*<summary>The constructor for SyFields</summary>*/
        public SyFields()
        {
            SyFieldCalculation = new HashSet<SyFieldCalculation>();
            SyFldCaptions = new HashSet<SyFldCaptions>();
            SyRptFields = new HashSet<SyRptFields>();
            SyTblFlds = new HashSet<SyTblFlds>();
        }

        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
        /*<summary>The get and set for FldName</summary>*/
        public string FldName { get; set; }
        /*<summary>The get and set for FldTypeId</summary>*/
        public int FldTypeId { get; set; }
        /*<summary>The get and set for FldLen</summary>*/
        public int FldLen { get; set; }
        /*<summary>The get and set for Ddlid</summary>*/
        public int? Ddlid { get; set; }
        /*<summary>The get and set for DerivedFld</summary>*/
        public bool? DerivedFld { get; set; }
        /*<summary>The get and set for SchlReq</summary>*/
        public bool? SchlReq { get; set; }
        /*<summary>The get and set for LogChanges</summary>*/
        public bool? LogChanges { get; set; }
        /*<summary>The get and set for Mask</summary>*/
        public string Mask { get; set; }

        /*<summary>The navigational property for FldType</summary>*/
        public virtual SyFieldTypes FldType { get; set; }
        /*<summary>The navigational property for SyFieldCalculation</summary>*/
        public virtual ICollection<SyFieldCalculation>SyFieldCalculation { get; set; }
        /*<summary>The navigational property for SyFldCaptions</summary>*/
        public virtual ICollection<SyFldCaptions>SyFldCaptions { get; set; }
        /*<summary>The navigational property for SyRptFields</summary>*/
        public virtual ICollection<SyRptFields>SyRptFields { get; set; }
        /*<summary>The navigational property for SyTblFlds</summary>*/
        public virtual ICollection<SyTblFlds>SyTblFlds { get; set; }
    }
}