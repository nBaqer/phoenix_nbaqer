﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArReqs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArReqs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArReqs</summary>*/
    public partial class ArReqs
    {
        /*<summary>The constructor for ArReqs</summary>*/
        public ArReqs()
        {
            ArClassSections = new HashSet<ArClassSections>();
            ArCourseBks = new HashSet<ArCourseBks>();
            ArCourseEquivalentEquivReq = new HashSet<ArCourseEquivalent>();
            ArCourseEquivalentReq = new HashSet<ArCourseEquivalent>();
            ArCourseReqsPreCoReq = new HashSet<ArCourseReqs>();
            ArCourseReqsReq = new HashSet<ArCourseReqs>();
            ArOverridenPreReqs = new HashSet<ArOverridenPreReqs>();
            ArProgVerDef = new HashSet<ArProgVerDef>();
            ArReqGrpDefGrp = new HashSet<ArReqGrpDef>();
            ArReqGrpDefReq = new HashSet<ArReqGrpDef>();
            ArTransferGrades = new HashSet<ArTransferGrades>();
            SaCourseFees = new HashSet<SaCourseFees>();
        }

        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for CourseId</summary>*/
        public int? CourseId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ReqTypeId</summary>*/
        public short ReqTypeId { get; set; }
        /*<summary>The get and set for Credits</summary>*/
        public decimal? Credits { get; set; }
        /*<summary>The get and set for FinAidCredits</summary>*/
        public decimal? FinAidCredits { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public decimal? Hours { get; set; }
        /*<summary>The get and set for Cnt</summary>*/
        public short? Cnt { get; set; }
        /*<summary>The get and set for GrdLvlId</summary>*/
        public Guid? GrdLvlId { get; set; }
        /*<summary>The get and set for UnitTypeId</summary>*/
        public Guid? UnitTypeId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for CourseCatalog</summary>*/
        public string CourseCatalog { get; set; }
        /*<summary>The get and set for CourseComments</summary>*/
        public string CourseComments { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Su</summary>*/
        public bool? Su { get; set; }
        /*<summary>The get and set for Pf</summary>*/
        public bool? Pf { get; set; }
        /*<summary>The get and set for CourseCategoryId</summary>*/
        public Guid? CourseCategoryId { get; set; }
        /*<summary>The get and set for TrackTardies</summary>*/
        public bool TrackTardies { get; set; }
        /*<summary>The get and set for TardiesMakingAbsence</summary>*/
        public int? TardiesMakingAbsence { get; set; }
        /*<summary>The get and set for DeptId</summary>*/
        public Guid? DeptId { get; set; }
        /*<summary>The get and set for MinDate</summary>*/
        public DateTime? MinDate { get; set; }
        /*<summary>The get and set for IsComCourse</summary>*/
        public bool? IsComCourse { get; set; }
        /*<summary>The get and set for IsOnLine</summary>*/
        public bool? IsOnLine { get; set; }
        /*<summary>The get and set for CompletedDate</summary>*/
        public DateTime? CompletedDate { get; set; }
        /*<summary>The get and set for IsExternship</summary>*/
        public bool? IsExternship { get; set; }
        /*<summary>The get and set for UseTimeClock</summary>*/
        public bool? UseTimeClock { get; set; }
        /*<summary>The get and set for IsAttendanceOnly</summary>*/
        public bool IsAttendanceOnly { get; set; }
        /*<summary>The get and set for AllowCompletedCourseRetake</summary>*/
        public bool AllowCompletedCourseRetake { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for CourseCategory</summary>*/
        public virtual ArCourseCategories CourseCategory { get; set; }
        /*<summary>The navigational property for Dept</summary>*/
        public virtual ArDepartments Dept { get; set; }
        /*<summary>The navigational property for GrdLvl</summary>*/
        public virtual AdEdLvls GrdLvl { get; set; }
        /*<summary>The navigational property for ReqType</summary>*/
        public virtual ArReqTypes ReqType { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for UnitType</summary>*/
        public virtual ArAttUnitType UnitType { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArCourseBks</summary>*/
        public virtual ICollection<ArCourseBks>ArCourseBks { get; set; }
        /*<summary>The navigational property for ArCourseEquivalentEquivReq</summary>*/
        public virtual ICollection<ArCourseEquivalent>ArCourseEquivalentEquivReq { get; set; }
        /*<summary>The navigational property for ArCourseEquivalentReq</summary>*/
        public virtual ICollection<ArCourseEquivalent>ArCourseEquivalentReq { get; set; }
        /*<summary>The navigational property for ArCourseReqsPreCoReq</summary>*/
        public virtual ICollection<ArCourseReqs>ArCourseReqsPreCoReq { get; set; }
        /*<summary>The navigational property for ArCourseReqsReq</summary>*/
        public virtual ICollection<ArCourseReqs>ArCourseReqsReq { get; set; }
        /*<summary>The navigational property for ArOverridenPreReqs</summary>*/
        public virtual ICollection<ArOverridenPreReqs>ArOverridenPreReqs { get; set; }
        /*<summary>The navigational property for ArProgVerDef</summary>*/
        public virtual ICollection<ArProgVerDef>ArProgVerDef { get; set; }
        /*<summary>The navigational property for ArReqGrpDefGrp</summary>*/
        public virtual ICollection<ArReqGrpDef>ArReqGrpDefGrp { get; set; }
        /*<summary>The navigational property for ArReqGrpDefReq</summary>*/
        public virtual ICollection<ArReqGrpDef>ArReqGrpDefReq { get; set; }
        /*<summary>The navigational property for ArTransferGrades</summary>*/
        public virtual ICollection<ArTransferGrades>ArTransferGrades { get; set; }
        /*<summary>The navigational property for SaCourseFees</summary>*/
        public virtual ICollection<SaCourseFees>SaCourseFees { get; set; }
    }
}