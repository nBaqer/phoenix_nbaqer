﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArCourseBks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArCourseBks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArCourseBks</summary>*/
    public partial class ArCourseBks
    {
        /*<summary>The get and set for CourseBkId</summary>*/
        public Guid CourseBkId { get; set; }
        /*<summary>The get and set for BkId</summary>*/
        public Guid BkId { get; set; }
        /*<summary>The get and set for IsReq</summary>*/
        public bool? IsReq { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for CourseId</summary>*/
        public Guid CourseId { get; set; }

        /*<summary>The navigational property for Bk</summary>*/
        public virtual ArBooks Bk { get; set; }
        /*<summary>The navigational property for Course</summary>*/
        public virtual ArReqs Course { get; set; }
    }
}