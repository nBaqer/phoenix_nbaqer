﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTermTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTermTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTermTypes</summary>*/
    public partial class SyTermTypes
    {
        /*<summary>The constructor for SyTermTypes</summary>*/
        public SyTermTypes()
        {
            ArTerm = new HashSet<ArTerm>();
        }

        /*<summary>The get and set for TermTypeId</summary>*/
        public int TermTypeId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for ArTerm</summary>*/
        public virtual ICollection<ArTerm>ArTerm { get; set; }
    }
}