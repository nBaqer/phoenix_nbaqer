﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPhoneType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPhoneType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPhoneType</summary>*/
    public partial class SyPhoneType
    {
        /*<summary>The constructor for SyPhoneType</summary>*/
        public SyPhoneType()
        {
            AdLeadOtherContactsPhone = new HashSet<AdLeadOtherContactsPhone>();
            AdLeadPhone = new HashSet<AdLeadPhone>();
            SyInstitutionPhone = new HashSet<SyInstitutionPhone>();
        }

        /*<summary>The get and set for PhoneTypeId</summary>*/
        public Guid PhoneTypeId { get; set; }
        /*<summary>The get and set for PhoneTypeCode</summary>*/
        public string PhoneTypeCode { get; set; }
        /*<summary>The get and set for PhoneTypeDescrip</summary>*/
        public string PhoneTypeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for Sequence</summary>*/
        public int? Sequence { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsPhone</summary>*/
        public virtual ICollection<AdLeadOtherContactsPhone>AdLeadOtherContactsPhone { get; set; }
        /*<summary>The navigational property for AdLeadPhone</summary>*/
        public virtual ICollection<AdLeadPhone>AdLeadPhone { get; set; }
        /*<summary>The navigational property for SyInstitutionPhone</summary>*/
        public virtual ICollection<SyInstitutionPhone>SyInstitutionPhone { get; set; }
    }
}