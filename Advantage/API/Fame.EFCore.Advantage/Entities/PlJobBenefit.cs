﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobBenefit.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobBenefit definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobBenefit</summary>*/
    public partial class PlJobBenefit
    {
        /*<summary>The constructor for PlJobBenefit</summary>*/
        public PlJobBenefit()
        {
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for JobBenefitId</summary>*/
        public Guid JobBenefitId { get; set; }
        /*<summary>The get and set for JobBenefitCode</summary>*/
        public string JobBenefitCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for JobBenefitDescrip</summary>*/
        public string JobBenefitDescrip { get; set; }
        /*<summary>The get and set for CamGrpId</summary>*/
        public Guid CamGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CamGrp</summary>*/
        public virtual SyCampGrps CamGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}