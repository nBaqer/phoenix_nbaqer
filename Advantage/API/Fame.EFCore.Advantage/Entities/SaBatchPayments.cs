﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaBatchPayments.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaBatchPayments definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaBatchPayments</summary>*/
    public partial class SaBatchPayments
    {
        /*<summary>The constructor for SaBatchPayments</summary>*/
        public SaBatchPayments()
        {
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for BatchPaymentId</summary>*/
        public Guid BatchPaymentId { get; set; }
        /*<summary>The get and set for BatchPaymentNumber</summary>*/
        public string BatchPaymentNumber { get; set; }
        /*<summary>The get and set for FundSourceId</summary>*/
        public Guid FundSourceId { get; set; }
        /*<summary>The get and set for BatchPaymentDate</summary>*/
        public DateTime BatchPaymentDate { get; set; }
        /*<summary>The get and set for BatchPaymentDescrip</summary>*/
        public string BatchPaymentDescrip { get; set; }
        /*<summary>The get and set for BatchPaymentAmount</summary>*/
        public decimal BatchPaymentAmount { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for IsPosted</summary>*/
        public bool IsPosted { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for FundSource</summary>*/
        public virtual SaFundSources FundSource { get; set; }
        /*<summary>The navigational property for User</summary>*/
        public virtual SyUsers User { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}