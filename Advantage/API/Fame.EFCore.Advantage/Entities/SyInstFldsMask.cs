﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstFldsMask.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstFldsMask definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstFldsMask</summary>*/
    public partial class SyInstFldsMask
    {
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
        /*<summary>The get and set for Mask</summary>*/
        public string Mask { get; set; }
    }
}