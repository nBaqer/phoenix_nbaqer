﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTransferGrades.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTransferGrades definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTransferGrades</summary>*/
    public partial class ArTransferGrades
    {
        /*<summary>The get and set for TransferId</summary>*/
        public Guid TransferId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid ReqId { get; set; }
        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid? GrdSysDetailId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid TermId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsTransferred</summary>*/
        public bool? IsTransferred { get; set; }
        /*<summary>The get and set for CompletedDate</summary>*/
        public DateTime? CompletedDate { get; set; }
        /*<summary>The get and set for IsClinicsSatisfied</summary>*/
        public bool? IsClinicsSatisfied { get; set; }
        /*<summary>The get and set for IsCourseCompleted</summary>*/
        public bool IsCourseCompleted { get; set; }
        /*<summary>The get and set for IsGradeOverridden</summary>*/
        public bool IsGradeOverridden { get; set; }
        /*<summary>The get and set for GradeOverriddenBy</summary>*/
        public string GradeOverriddenBy { get; set; }
        /*<summary>The get and set for GradeOverriddenDate</summary>*/
        public DateTime? GradeOverriddenDate { get; set; }

        /*<summary>The navigational property for GrdSysDetail</summary>*/
        public virtual ArGradeSystemDetails GrdSysDetail { get; set; }
        /*<summary>The navigational property for Req</summary>*/
        public virtual ArReqs Req { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for Term</summary>*/
        public virtual ArTerm Term { get; set; }
    }
}