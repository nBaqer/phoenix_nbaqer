﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyCampuses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyCampuses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyCampuses</summary>*/
    public partial class SyCampuses
    {
        /*<summary>The constructor for SyCampuses</summary>*/
        public SyCampuses()
        {
            AdLeads = new HashSet<AdLeads>();
            ArBuildings = new HashSet<ArBuildings>();
            ArClassSections = new HashSet<ArClassSections>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
            ArStuReschReason = new HashSet<ArStuReschReason>();
            ArTimeClockSpecialCode = new HashSet<ArTimeClockSpecialCode>();
            HrEmployees = new HashSet<HrEmployees>();
            InverseParentCampus = new HashSet<SyCampuses>();
            RptAdmissionsRep = new HashSet<RptAdmissionsRep>();
            RptLeadStatus = new HashSet<RptLeadStatus>();
            SyAwardTypes9010Mapping = new HashSet<SyAwardTypes9010Mapping>();
            SyCampGrps = new HashSet<SyCampGrps>();
            SyCmpGrpCmps = new HashSet<SyCmpGrpCmps>();
            SyMrus = new HashSet<SyMrus>();
            SyNaccasSettings = new HashSet<SyNaccasSettings>();
            SySchoolStateBoardReports = new HashSet<SySchoolStateBoardReports>();
            SyStatusChangesDeleted = new HashSet<SyStatusChangesDeleted>();
            SyStudentStatusChanges = new HashSet<SyStudentStatusChanges>();
            SyTimeClockImportLog = new HashSet<SyTimeClockImportLog>();
            SyUsers = new HashSet<SyUsers>();
        }

        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for CampCode</summary>*/
        public string CampCode { get; set; }
        /*<summary>The get and set for CampDescrip</summary>*/
        public string CampDescrip { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for Phone1</summary>*/
        public string Phone1 { get; set; }
        /*<summary>The get and set for Phone2</summary>*/
        public string Phone2 { get; set; }
        /*<summary>The get and set for Phone3</summary>*/
        public string Phone3 { get; set; }
        /*<summary>The get and set for Fax</summary>*/
        public string Fax { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for Website</summary>*/
        public string Website { get; set; }
        /*<summary>The get and set for IsCorporate</summary>*/
        public bool? IsCorporate { get; set; }
        /*<summary>The get and set for TranscriptAuthZnTitle</summary>*/
        public string TranscriptAuthZnTitle { get; set; }
        /*<summary>The get and set for TranscriptAuthZnName</summary>*/
        public string TranscriptAuthZnName { get; set; }
        /*<summary>The get and set for TcsourcePath</summary>*/
        public string TcsourcePath { get; set; }
        /*<summary>The get and set for TctargetPath</summary>*/
        public string TctargetPath { get; set; }
        /*<summary>The get and set for IsRemoteServer</summary>*/
        public bool? IsRemoteServer { get; set; }
        /*<summary>The get and set for PortalContactEmail</summary>*/
        public string PortalContactEmail { get; set; }
        /*<summary>The get and set for RemoteServerUsrNm</summary>*/
        public string RemoteServerUsrNm { get; set; }
        /*<summary>The get and set for RemoteServerPwd</summary>*/
        public string RemoteServerPwd { get; set; }
        /*<summary>The get and set for SourceFolderLoc</summary>*/
        public string SourceFolderLoc { get; set; }
        /*<summary>The get and set for TargetFolderLoc</summary>*/
        public string TargetFolderLoc { get; set; }
        /*<summary>The get and set for UseCampusAddress</summary>*/
        public bool? UseCampusAddress { get; set; }
        /*<summary>The get and set for InvAddress1</summary>*/
        public string InvAddress1 { get; set; }
        /*<summary>The get and set for InvAddress2</summary>*/
        public string InvAddress2 { get; set; }
        /*<summary>The get and set for InvCity</summary>*/
        public string InvCity { get; set; }
        /*<summary>The get and set for InvStateId</summary>*/
        public Guid? InvStateId { get; set; }
        /*<summary>The get and set for InvZip</summary>*/
        public string InvZip { get; set; }
        /*<summary>The get and set for InvCountryId</summary>*/
        public Guid? InvCountryId { get; set; }
        /*<summary>The get and set for InvPhone1</summary>*/
        public string InvPhone1 { get; set; }
        /*<summary>The get and set for InvFax</summary>*/
        public string InvFax { get; set; }
        /*<summary>The get and set for FlsourcePath</summary>*/
        public string FlsourcePath { get; set; }
        /*<summary>The get and set for FltargetPath</summary>*/
        public string FltargetPath { get; set; }
        /*<summary>The get and set for FlexceptionPath</summary>*/
        public string FlexceptionPath { get; set; }
        /*<summary>The get and set for IsRemoteServerFl</summary>*/
        public bool IsRemoteServerFl { get; set; }
        /*<summary>The get and set for RemoteServerUsrNmFl</summary>*/
        public string RemoteServerUsrNmFl { get; set; }
        /*<summary>The get and set for RemoteServerPwdFl</summary>*/
        public string RemoteServerPwdFl { get; set; }
        /*<summary>The get and set for SourceFolderLocFl</summary>*/
        public string SourceFolderLocFl { get; set; }
        /*<summary>The get and set for TargetFolderLocFl</summary>*/
        public string TargetFolderLocFl { get; set; }
        /*<summary>The get and set for IlsourcePath</summary>*/
        public string IlsourcePath { get; set; }
        /*<summary>The get and set for IlarchivePath</summary>*/
        public string IlarchivePath { get; set; }
        /*<summary>The get and set for IlexceptionPath</summary>*/
        public string IlexceptionPath { get; set; }
        /*<summary>The get and set for IsRemoteServerIl</summary>*/
        public bool IsRemoteServerIl { get; set; }
        /*<summary>The get and set for RemoteServerUserNameIl</summary>*/
        public string RemoteServerUserNameIl { get; set; }
        /*<summary>The get and set for RemoteServerPasswordIl</summary>*/
        public string RemoteServerPasswordIl { get; set; }
        /*<summary>The get and set for SourceCategoryAll</summary>*/
        public bool? SourceCategoryAll { get; set; }
        /*<summary>The get and set for SourceCategoryId</summary>*/
        public string SourceCategoryId { get; set; }
        /*<summary>The get and set for SourceTypeAll</summary>*/
        public bool? SourceTypeAll { get; set; }
        /*<summary>The get and set for SourceTypeId</summary>*/
        public string SourceTypeId { get; set; }
        /*<summary>The get and set for AdmRepAll</summary>*/
        public bool? AdmRepAll { get; set; }
        /*<summary>The get and set for AdmRepId</summary>*/
        public string AdmRepId { get; set; }
        /*<summary>The get and set for LeadStatusAll</summary>*/
        public bool? LeadStatusAll { get; set; }
        /*<summary>The get and set for LeadStatusId</summary>*/
        public string LeadStatusId { get; set; }
        /*<summary>The get and set for PhoneType1All</summary>*/
        public bool? PhoneType1All { get; set; }
        /*<summary>The get and set for PhoneType1Id</summary>*/
        public string PhoneType1Id { get; set; }
        /*<summary>The get and set for PhoneType2All</summary>*/
        public bool? PhoneType2All { get; set; }
        /*<summary>The get and set for PhoneType2Id</summary>*/
        public string PhoneType2Id { get; set; }
        /*<summary>The get and set for AddTypeAll</summary>*/
        public bool? AddTypeAll { get; set; }
        /*<summary>The get and set for AddTypeId</summary>*/
        public string AddTypeId { get; set; }
        /*<summary>The get and set for GenderAll</summary>*/
        public bool? GenderAll { get; set; }
        /*<summary>The get and set for GenderId</summary>*/
        public string GenderId { get; set; }
        /*<summary>The get and set for PortalCountryAll</summary>*/
        public bool? PortalCountryAll { get; set; }
        /*<summary>The get and set for PortalCountryId</summary>*/
        public string PortalCountryId { get; set; }
        /*<summary>The get and set for PortalContactEmailAll</summary>*/
        public bool? PortalContactEmailAll { get; set; }
        /*<summary>The get and set for EmailSubjectAll</summary>*/
        public bool? EmailSubjectAll { get; set; }
        /*<summary>The get and set for EmailSubject</summary>*/
        public string EmailSubject { get; set; }
        /*<summary>The get and set for EmailBodyAll</summary>*/
        public bool? EmailBodyAll { get; set; }
        /*<summary>The get and set for EmailBody</summary>*/
        public string EmailBody { get; set; }
        /*<summary>The get and set for Opeid</summary>*/
        public string Opeid { get; set; }
        /*<summary>The get and set for SchoolName</summary>*/
        public string SchoolName { get; set; }
        /*<summary>The get and set for Token1098Tservice</summary>*/
        public string Token1098Tservice { get; set; }
        /*<summary>The get and set for SchoolCodeKissSchoolId</summary>*/
        public string SchoolCodeKissSchoolId { get; set; }
        /*<summary>The get and set for CmsId</summary>*/
        public string CmsId { get; set; }
        /*<summary>The get and set for FseogmatchType</summary>*/
        public string FseogmatchType { get; set; }
        /*<summary>The get and set for IsBranch</summary>*/
        public bool IsBranch { get; set; }
        /*<summary>The get and set for ParentCampusId</summary>*/
        public Guid? ParentCampusId { get; set; }
        /*<summary>The get and set for AllowGraduateAndStillOweMoney</summary>*/
        public bool AllowGraduateAndStillOweMoney { get; set; }
        /*<summary>The get and set for AllowGraduateWithoutFullCompletion</summary>*/
        public bool AllowGraduateWithoutFullCompletion { get; set; }

        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for ParentCampus</summary>*/
        public virtual SyCampuses ParentCampus { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for ArBuildings</summary>*/
        public virtual ICollection<ArBuildings>ArBuildings { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
        /*<summary>The navigational property for ArStuReschReason</summary>*/
        public virtual ICollection<ArStuReschReason>ArStuReschReason { get; set; }
        /*<summary>The navigational property for ArTimeClockSpecialCode</summary>*/
        public virtual ICollection<ArTimeClockSpecialCode>ArTimeClockSpecialCode { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for InverseParentCampus</summary>*/
        public virtual ICollection<SyCampuses>InverseParentCampus { get; set; }
        /*<summary>The navigational property for RptAdmissionsRep</summary>*/
        public virtual ICollection<RptAdmissionsRep>RptAdmissionsRep { get; set; }
        /*<summary>The navigational property for RptLeadStatus</summary>*/
        public virtual ICollection<RptLeadStatus>RptLeadStatus { get; set; }
        /*<summary>The navigational property for SyAwardTypes9010Mapping</summary>*/
        public virtual ICollection<SyAwardTypes9010Mapping>SyAwardTypes9010Mapping { get; set; }
        /*<summary>The navigational property for SyCampGrps</summary>*/
        public virtual ICollection<SyCampGrps>SyCampGrps { get; set; }
        /*<summary>The navigational property for SyCmpGrpCmps</summary>*/
        public virtual ICollection<SyCmpGrpCmps>SyCmpGrpCmps { get; set; }
        /*<summary>The navigational property for SyMrus</summary>*/
        public virtual ICollection<SyMrus>SyMrus { get; set; }
        /*<summary>The navigational property for SyNaccasSettings</summary>*/
        public virtual ICollection<SyNaccasSettings>SyNaccasSettings { get; set; }
        /*<summary>The navigational property for SySchoolStateBoardReports</summary>*/
        public virtual ICollection<SySchoolStateBoardReports>SySchoolStateBoardReports { get; set; }
        /*<summary>The navigational property for SyStatusChangesDeleted</summary>*/
        public virtual ICollection<SyStatusChangesDeleted>SyStatusChangesDeleted { get; set; }
        /*<summary>The navigational property for SyStudentStatusChanges</summary>*/
        public virtual ICollection<SyStudentStatusChanges>SyStudentStatusChanges { get; set; }
        /*<summary>The navigational property for SyTimeClockImportLog</summary>*/
        public virtual ICollection<SyTimeClockImportLog>SyTimeClockImportLog { get; set; }
        /*<summary>The navigational property for SyUsers</summary>*/
        public virtual ICollection<SyUsers>SyUsers { get; set; }
    }
}