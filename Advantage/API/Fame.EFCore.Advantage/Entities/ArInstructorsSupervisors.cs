﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArInstructorsSupervisors.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArInstructorsSupervisors definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArInstructorsSupervisors</summary>*/
    public partial class ArInstructorsSupervisors
    {
        /*<summary>The get and set for InstructorId</summary>*/
        public Guid InstructorId { get; set; }
        /*<summary>The get and set for SupervisorId</summary>*/
        public Guid SupervisorId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Instructor</summary>*/
        public virtual SyUsers Instructor { get; set; }
        /*<summary>The navigational property for Supervisor</summary>*/
        public virtual SyUsers Supervisor { get; set; }
    }
}