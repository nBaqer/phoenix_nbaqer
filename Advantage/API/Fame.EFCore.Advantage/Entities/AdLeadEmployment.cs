﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadEmployment.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadEmployment definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadEmployment</summary>*/
    public partial class AdLeadEmployment
    {
        /*<summary>The constructor for AdLeadEmployment</summary>*/
        public AdLeadEmployment()
        {
            PriorWorkAddress = new HashSet<PriorWorkAddress>();
            PriorWorkContact = new HashSet<PriorWorkContact>();
        }

        /*<summary>The get and set for StEmploymentId</summary>*/
        public Guid StEmploymentId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for JobTitleId</summary>*/
        public Guid? JobTitleId { get; set; }
        /*<summary>The get and set for JobStatusId</summary>*/
        public Guid? JobStatusId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for JobResponsibilities</summary>*/
        public string JobResponsibilities { get; set; }
        /*<summary>The get and set for EmployerName</summary>*/
        public string EmployerName { get; set; }
        /*<summary>The get and set for EmployerJobTitle</summary>*/
        public string EmployerJobTitle { get; set; }

        /*<summary>The navigational property for JobStatus</summary>*/
        public virtual PlJobStatus JobStatus { get; set; }
        /*<summary>The navigational property for JobTitle</summary>*/
        public virtual AdTitles JobTitle { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for PriorWorkAddress</summary>*/
        public virtual ICollection<PriorWorkAddress>PriorWorkAddress { get; set; }
        /*<summary>The navigational property for PriorWorkContact</summary>*/
        public virtual ICollection<PriorWorkContact>PriorWorkContact { get; set; }
    }
}