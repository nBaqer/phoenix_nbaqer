﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTerm.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTerm definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTerm</summary>*/
    public partial class ArTerm
    {
        /*<summary>The constructor for ArTerm</summary>*/
        public ArTerm()
        {
            ArClassSectionTerms = new HashSet<ArClassSectionTerms>();
            ArClassSections = new HashSet<ArClassSections>();
            ArTermEnrollSummary = new HashSet<ArTermEnrollSummary>();
            ArTransferGrades = new HashSet<ArTransferGrades>();
            SaPeriodicFees = new HashSet<SaPeriodicFees>();
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for TermId</summary>*/
        public Guid TermId { get; set; }
        /*<summary>The get and set for TermCode</summary>*/
        public string TermCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for TermDescrip</summary>*/
        public string TermDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid? ShiftId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for IsModule</summary>*/
        public bool? IsModule { get; set; }
        /*<summary>The get and set for TermTypeId</summary>*/
        public int? TermTypeId { get; set; }
        /*<summary>The get and set for ProgId</summary>*/
        public Guid? ProgId { get; set; }
        /*<summary>The get and set for MidPtDate</summary>*/
        public DateTime? MidPtDate { get; set; }
        /*<summary>The get and set for MaxGradDate</summary>*/
        public DateTime? MaxGradDate { get; set; }
        /*<summary>The get and set for ProgramVersionId</summary>*/
        public Guid? ProgramVersionId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Prog</summary>*/
        public virtual ArPrograms Prog { get; set; }
        /*<summary>The navigational property for ProgramVersion</summary>*/
        public virtual ArPrgVersions ProgramVersion { get; set; }
        /*<summary>The navigational property for Shift</summary>*/
        public virtual ArShifts Shift { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TermType</summary>*/
        public virtual SyTermTypes TermType { get; set; }
        /*<summary>The navigational property for ArClassSectionTerms</summary>*/
        public virtual ICollection<ArClassSectionTerms>ArClassSectionTerms { get; set; }
        /*<summary>The navigational property for ArClassSections</summary>*/
        public virtual ICollection<ArClassSections>ArClassSections { get; set; }
        /*<summary>The navigational property for ArTermEnrollSummary</summary>*/
        public virtual ICollection<ArTermEnrollSummary>ArTermEnrollSummary { get; set; }
        /*<summary>The navigational property for ArTransferGrades</summary>*/
        public virtual ICollection<ArTransferGrades>ArTransferGrades { get; set; }
        /*<summary>The navigational property for SaPeriodicFees</summary>*/
        public virtual ICollection<SaPeriodicFees>SaPeriodicFees { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}