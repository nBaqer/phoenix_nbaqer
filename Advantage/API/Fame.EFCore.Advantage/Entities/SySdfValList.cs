﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfValList.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfValList definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfValList</summary>*/
    public partial class SySdfValList
    {
        /*<summary>The get and set for SdfListId</summary>*/
        public Guid SdfListId { get; set; }
        /*<summary>The get and set for Sdfid</summary>*/
        public Guid Sdfid { get; set; }
        /*<summary>The get and set for ValList</summary>*/
        public string ValList { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Sdf</summary>*/
        public virtual SySdf Sdf { get; set; }
    }
}