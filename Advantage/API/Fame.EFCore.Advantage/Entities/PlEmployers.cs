﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlEmployers.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlEmployers definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlEmployers</summary>*/
    public partial class PlEmployers
    {
        /*<summary>The constructor for PlEmployers</summary>*/
        public PlEmployers()
        {
            InverseParent = new HashSet<PlEmployers>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            PlEmployerJobCats = new HashSet<PlEmployerJobCats>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
        }

        /*<summary>The get and set for EmployerId</summary>*/
        public Guid EmployerId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for EmployerDescrip</summary>*/
        public string EmployerDescrip { get; set; }
        /*<summary>The get and set for ParentId</summary>*/
        public Guid? ParentId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for Address1</summary>*/
        public string Address1 { get; set; }
        /*<summary>The get and set for Address2</summary>*/
        public string Address2 { get; set; }
        /*<summary>The get and set for City</summary>*/
        public string City { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for Zip</summary>*/
        public string Zip { get; set; }
        /*<summary>The get and set for CountyId</summary>*/
        public Guid? CountyId { get; set; }
        /*<summary>The get and set for LocationId</summary>*/
        public Guid? LocationId { get; set; }
        /*<summary>The get and set for Phone</summary>*/
        public string Phone { get; set; }
        /*<summary>The get and set for Fax</summary>*/
        public string Fax { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for FeeId</summary>*/
        public Guid? FeeId { get; set; }
        /*<summary>The get and set for IndustryId</summary>*/
        public Guid? IndustryId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for GroupName</summary>*/
        public bool? GroupName { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }
        /*<summary>The get and set for ForeignPhone</summary>*/
        public bool? ForeignPhone { get; set; }
        /*<summary>The get and set for ForeignFax</summary>*/
        public bool? ForeignFax { get; set; }
        /*<summary>The get and set for ForeignZip</summary>*/
        public bool? ForeignZip { get; set; }
        /*<summary>The get and set for OtherState</summary>*/
        public string OtherState { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for County</summary>*/
        public virtual AdCounties County { get; set; }
        /*<summary>The navigational property for Fee</summary>*/
        public virtual PlFee Fee { get; set; }
        /*<summary>The navigational property for Industry</summary>*/
        public virtual PlIndustries Industry { get; set; }
        /*<summary>The navigational property for Location</summary>*/
        public virtual PlLocations Location { get; set; }
        /*<summary>The navigational property for Parent</summary>*/
        public virtual PlEmployers Parent { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for InverseParent</summary>*/
        public virtual ICollection<PlEmployers>InverseParent { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for PlEmployerJobCats</summary>*/
        public virtual ICollection<PlEmployerJobCats>PlEmployerJobCats { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
    }
}