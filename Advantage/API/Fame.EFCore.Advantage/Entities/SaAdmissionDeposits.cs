﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaAdmissionDeposits.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaAdmissionDeposits definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaAdmissionDeposits</summary>*/
    public partial class SaAdmissionDeposits
    {
        /*<summary>The get and set for AdmDepositId</summary>*/
        public Guid AdmDepositId { get; set; }
        /*<summary>The get and set for ProspectId</summary>*/
        public Guid? ProspectId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for AdmDepositDescrip</summary>*/
        public string AdmDepositDescrip { get; set; }
        /*<summary>The get and set for PaidById</summary>*/
        public Guid PaidById { get; set; }
        /*<summary>The get and set for DepositDate</summary>*/
        public DateTime DepositDate { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal Amount { get; set; }
        /*<summary>The get and set for PaymentType</summary>*/
        public short PaymentType { get; set; }
        /*<summary>The get and set for CheckNumber</summary>*/
        public string CheckNumber { get; set; }
        /*<summary>The get and set for CreditCardTypeId</summary>*/
        public Guid? CreditCardTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}