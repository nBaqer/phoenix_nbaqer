﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgVerInstructionType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgVerInstructionType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgVerInstructionType</summary>*/
    public partial class ArPrgVerInstructionType
    {
        /*<summary>The get and set for PrgVerInstructionTypeId</summary>*/
        public Guid PrgVerInstructionTypeId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid PrgVerId { get; set; }
        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for Hours</summary>*/
        public decimal Hours { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for InstructionType</summary>*/
        public virtual ArInstructionType InstructionType { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
    }
}