﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlLocations.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlLocations definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlLocations</summary>*/
    public partial class PlLocations
    {
        /*<summary>The constructor for PlLocations</summary>*/
        public PlLocations()
        {
            PlEmployers = new HashSet<PlEmployers>();
        }

        /*<summary>The get and set for LocationId</summary>*/
        public Guid LocationId { get; set; }
        /*<summary>The get and set for LocationCode</summary>*/
        public string LocationCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for LocationDescrip</summary>*/
        public string LocationDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
    }
}