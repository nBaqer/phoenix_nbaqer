﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdEdLvls.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdEdLvls definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdEdLvls</summary>*/
    public partial class AdEdLvls
    {
        /*<summary>The constructor for AdEdLvls</summary>*/
        public AdEdLvls()
        {
            AdLeads = new HashSet<AdLeads>();
            AdReqs = new HashSet<AdReqs>();
            ArReqs = new HashSet<ArReqs>();
            ArStuEnrollments = new HashSet<ArStuEnrollments>();
        }

        /*<summary>The get and set for EdLvlId</summary>*/
        public Guid EdLvlId { get; set; }
        /*<summary>The get and set for EdLvlCode</summary>*/
        public string EdLvlCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for EdLvlDescrip</summary>*/
        public string EdLvlDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for AdReqs</summary>*/
        public virtual ICollection<AdReqs>AdReqs { get; set; }
        /*<summary>The navigational property for ArReqs</summary>*/
        public virtual ICollection<ArReqs>ArReqs { get; set; }
        /*<summary>The navigational property for ArStuEnrollments</summary>*/
        public virtual ICollection<ArStuEnrollments>ArStuEnrollments { get; set; }
    }
}