﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyReportUserPrefs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyReportUserPrefs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyReportUserPrefs</summary>*/
    public partial class SyReportUserPrefs
    {
        /*<summary>The get and set for PrefId</summary>*/
        public Guid PrefId { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public string UserId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for PrefName</summary>*/
        public string PrefName { get; set; }
        /*<summary>The get and set for PrefDescrip</summary>*/
        public string PrefDescrip { get; set; }
        /*<summary>The get and set for PrefData</summary>*/
        public string PrefData { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
    }
}