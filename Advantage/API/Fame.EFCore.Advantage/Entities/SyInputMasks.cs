﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInputMasks.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInputMasks definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInputMasks</summary>*/
    public partial class SyInputMasks
    {
        /*<summary>The get and set for InputMaskId</summary>*/
        public byte InputMaskId { get; set; }
        /*<summary>The get and set for Item</summary>*/
        public string Item { get; set; }
        /*<summary>The get and set for Mask</summary>*/
        public string Mask { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
    }
}