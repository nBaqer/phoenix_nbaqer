﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPmtPeriods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPmtPeriods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPmtPeriods</summary>*/
    public partial class SaPmtPeriods
    {
        /*<summary>The constructor for SaPmtPeriods</summary>*/
        public SaPmtPeriods()
        {
            SaTransactions = new HashSet<SaTransactions>();
        }

        /*<summary>The get and set for PmtPeriodId</summary>*/
        public Guid PmtPeriodId { get; set; }
        /*<summary>The get and set for IncrementId</summary>*/
        public Guid IncrementId { get; set; }
        /*<summary>The get and set for PeriodNumber</summary>*/
        public int PeriodNumber { get; set; }
        /*<summary>The get and set for IncrementValue</summary>*/
        public decimal IncrementValue { get; set; }
        /*<summary>The get and set for CumulativeValue</summary>*/
        public decimal CumulativeValue { get; set; }
        /*<summary>The get and set for ChargeAmount</summary>*/
        public decimal ChargeAmount { get; set; }
        /*<summary>The get and set for IsCharged</summary>*/
        public bool IsCharged { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Increment</summary>*/
        public virtual SaIncrements Increment { get; set; }
        /*<summary>The navigational property for SaTransactions</summary>*/
        public virtual ICollection<SaTransactions>SaTransactions { get; set; }
    }
}