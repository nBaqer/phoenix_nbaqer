﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStates.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStates definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStates</summary>*/
    public partial class SyStates
    {
        /*<summary>The constructor for SyStates</summary>*/
        public SyStates()
        {
            AdColleges = new HashSet<AdColleges>();
            AdHighSchools = new HashSet<AdHighSchools>();
            AdLeadAddresses = new HashSet<AdLeadAddresses>();
            AdLeadOtherContactsAddreses = new HashSet<AdLeadOtherContactsAddreses>();
            AdLeadsDrivLicState = new HashSet<AdLeads>();
            AdLeadsEnrollState = new HashSet<AdLeads>();
            AdLeadsState = new HashSet<AdLeads>();
            ArBuildings = new HashSet<ArBuildings>();
            FaLendersPayState = new HashSet<FaLenders>();
            FaLendersState = new HashSet<FaLenders>();
            HrEmployees = new HashSet<HrEmployees>();
            PlCorpHqs = new HashSet<PlCorpHqs>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            PlEmployers = new HashSet<PlEmployers>();
            PriorWorkAddress = new HashSet<PriorWorkAddress>();
            SaBankCodes = new HashSet<SaBankCodes>();
            SyCampuses = new HashSet<SyCampuses>();
            SyInstitutionAddresses = new HashSet<SyInstitutionAddresses>();
            SySchoolStateBoardReports = new HashSet<SySchoolStateBoardReports>();
            SyStateBoardAgenciesLicensingState = new HashSet<SyStateBoardAgencies>();
            SyStateBoardAgenciesState = new HashSet<SyStateBoardAgencies>();
            SyStateBoardCourses = new HashSet<SyStateBoardCourses>();
            SyStateReports = new HashSet<SyStateReports>();
        }

        /*<summary>The get and set for StateId</summary>*/
        public Guid StateId { get; set; }
        /*<summary>The get and set for StateCode</summary>*/
        public string StateCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for StateDescrip</summary>*/
        public string StateDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for Fipscode</summary>*/
        public int? Fipscode { get; set; }
        /*<summary>The get and set for IsStateBoard</summary>*/
        public bool? IsStateBoard { get; set; }
        /*<summary>The get and set for CountryId</summary>*/
        public Guid? CountryId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Country</summary>*/
        public virtual AdCountries Country { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdColleges</summary>*/
        public virtual ICollection<AdColleges>AdColleges { get; set; }
        /*<summary>The navigational property for AdHighSchools</summary>*/
        public virtual ICollection<AdHighSchools>AdHighSchools { get; set; }
        /*<summary>The navigational property for AdLeadAddresses</summary>*/
        public virtual ICollection<AdLeadAddresses>AdLeadAddresses { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsAddreses</summary>*/
        public virtual ICollection<AdLeadOtherContactsAddreses>AdLeadOtherContactsAddreses { get; set; }
        /*<summary>The navigational property for AdLeadsDrivLicState</summary>*/
        public virtual ICollection<AdLeads>AdLeadsDrivLicState { get; set; }
        /*<summary>The navigational property for AdLeadsEnrollState</summary>*/
        public virtual ICollection<AdLeads>AdLeadsEnrollState { get; set; }
        /*<summary>The navigational property for AdLeadsState</summary>*/
        public virtual ICollection<AdLeads>AdLeadsState { get; set; }
        /*<summary>The navigational property for ArBuildings</summary>*/
        public virtual ICollection<ArBuildings>ArBuildings { get; set; }
        /*<summary>The navigational property for FaLendersPayState</summary>*/
        public virtual ICollection<FaLenders>FaLendersPayState { get; set; }
        /*<summary>The navigational property for FaLendersState</summary>*/
        public virtual ICollection<FaLenders>FaLendersState { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for PlCorpHqs</summary>*/
        public virtual ICollection<PlCorpHqs>PlCorpHqs { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
        /*<summary>The navigational property for PriorWorkAddress</summary>*/
        public virtual ICollection<PriorWorkAddress>PriorWorkAddress { get; set; }
        /*<summary>The navigational property for SaBankCodes</summary>*/
        public virtual ICollection<SaBankCodes>SaBankCodes { get; set; }
        /*<summary>The navigational property for SyCampuses</summary>*/
        public virtual ICollection<SyCampuses>SyCampuses { get; set; }
        /*<summary>The navigational property for SyInstitutionAddresses</summary>*/
        public virtual ICollection<SyInstitutionAddresses>SyInstitutionAddresses { get; set; }
        /*<summary>The navigational property for SySchoolStateBoardReports</summary>*/
        public virtual ICollection<SySchoolStateBoardReports>SySchoolStateBoardReports { get; set; }
        /*<summary>The navigational property for SyStateBoardAgenciesLicensingState</summary>*/
        public virtual ICollection<SyStateBoardAgencies>SyStateBoardAgenciesLicensingState { get; set; }
        /*<summary>The navigational property for SyStateBoardAgenciesState</summary>*/
        public virtual ICollection<SyStateBoardAgencies>SyStateBoardAgenciesState { get; set; }
        /*<summary>The navigational property for SyStateBoardCourses</summary>*/
        public virtual ICollection<SyStateBoardCourses>SyStateBoardCourses { get; set; }
        /*<summary>The navigational property for SyStateReports</summary>*/
        public virtual ICollection<SyStateReports>SyStateReports { get; set; }
    }
}