﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArFasapchkResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArFasapchkResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArFasapchkResults</summary>*/
    public partial class ArFasapchkResults
    {
        /*<summary>The get and set for StdRecKey</summary>*/
        public Guid StdRecKey { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for Period</summary>*/
        public int Period { get; set; }
        /*<summary>The get and set for IsMakingSap</summary>*/
        public bool IsMakingSap { get; set; }
        /*<summary>The get and set for HrsAttended</summary>*/
        public decimal? HrsAttended { get; set; }
        /*<summary>The get and set for HrsEarned</summary>*/
        public decimal? HrsEarned { get; set; }
        /*<summary>The get and set for CredsAttempted</summary>*/
        public decimal? CredsAttempted { get; set; }
        /*<summary>The get and set for CredsEarned</summary>*/
        public decimal? CredsEarned { get; set; }
        /*<summary>The get and set for Gpa</summary>*/
        public decimal? Gpa { get; set; }
        /*<summary>The get and set for DatePerformed</summary>*/
        public DateTime DatePerformed { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for SapdetailId</summary>*/
        public Guid? SapdetailId { get; set; }
        /*<summary>The get and set for Average</summary>*/
        public decimal? Average { get; set; }
        /*<summary>The get and set for Attendance</summary>*/
        public decimal? Attendance { get; set; }
        /*<summary>The get and set for CumFinAidCredits</summary>*/
        public decimal? CumFinAidCredits { get; set; }
        /*<summary>The get and set for PercentCompleted</summary>*/
        public decimal? PercentCompleted { get; set; }
        /*<summary>The get and set for CheckPointDate</summary>*/
        public DateTime? CheckPointDate { get; set; }
        /*<summary>The get and set for TermStartDate</summary>*/
        public DateTime? TermStartDate { get; set; }
        /*<summary>The get and set for PreviewSapCheck</summary>*/
        public bool PreviewSapCheck { get; set; }
        /*<summary>The get and set for TitleIvstatusId</summary>*/
        public int TitleIvstatusId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Printed</summary>*/
        public bool Printed { get; set; }

        /*<summary>The navigational property for Sapdetail</summary>*/
        public virtual ArSapdetails Sapdetail { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
        /*<summary>The navigational property for TitleIvstatus</summary>*/
        public virtual SyTitleIvsapStatus TitleIvstatus { get; set; }
    }
}