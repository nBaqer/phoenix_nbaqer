﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaTransCodeGlaccounts.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaTransCodeGlaccounts definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaTransCodeGlaccounts</summary>*/
    public partial class SaTransCodeGlaccounts
    {
        /*<summary>The get and set for TransCodeGlaccountId</summary>*/
        public Guid TransCodeGlaccountId { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid TransCodeId { get; set; }
        /*<summary>The get and set for Glaccount</summary>*/
        public string Glaccount { get; set; }
        /*<summary>The get and set for Percentage</summary>*/
        public decimal Percentage { get; set; }
        /*<summary>The get and set for TypeId</summary>*/
        public byte TypeId { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
    }
}