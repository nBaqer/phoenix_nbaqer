﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTimeClockImportLog.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTimeClockImportLog definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTimeClockImportLog</summary>*/
    public partial class SyTimeClockImportLog
    {
        /*<summary>The get and set for TimeClockImportLogId</summary>*/
        public Guid TimeClockImportLogId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for Message</summary>*/
        public string Message { get; set; }
        /*<summary>The get and set for Status</summary>*/
        public int? Status { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
    }
}