﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyLoggerDualEnrollment.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyLoggerDualEnrollment definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyLoggerDualEnrollment</summary>*/
    public partial class SyLoggerDualEnrollment
    {
        /*<summary>The get and set for LoggerId</summary>*/
        public long LoggerId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public string StudentId { get; set; }
        /*<summary>The get and set for FullName</summary>*/
        public string FullName { get; set; }
        /*<summary>The get and set for ClassId</summary>*/
        public string ClassId { get; set; }
        /*<summary>The get and set for ClassSection</summary>*/
        public string ClassSection { get; set; }
        /*<summary>The get and set for RequirementCode</summary>*/
        public string RequirementCode { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for Classification</summary>*/
        public int Classification { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
    }
}