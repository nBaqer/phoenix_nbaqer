﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="MsgGroups.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The MsgGroups definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for MsgGroups</summary>*/
    public partial class MsgGroups
    {
        /*<summary>The get and set for GroupId</summary>*/
        public Guid GroupId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for CampGroupId</summary>*/
        public Guid? CampGroupId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public Guid? ModUser { get; set; }
        /*<summary>The get and set for Active</summary>*/
        public byte? Active { get; set; }

        /*<summary>The navigational property for CampGroup</summary>*/
        public virtual SyCampGrps CampGroup { get; set; }
    }
}