﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaRateScheduleDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaRateScheduleDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaRateScheduleDetails</summary>*/
    public partial class SaRateScheduleDetails
    {
        /*<summary>The get and set for RateScheduleDetailId</summary>*/
        public Guid RateScheduleDetailId { get; set; }
        /*<summary>The get and set for RateScheduleId</summary>*/
        public Guid RateScheduleId { get; set; }
        /*<summary>The get and set for MinUnits</summary>*/
        public short MinUnits { get; set; }
        /*<summary>The get and set for MaxUnits</summary>*/
        public short MaxUnits { get; set; }
        /*<summary>The get and set for TuitionCategoryId</summary>*/
        public Guid? TuitionCategoryId { get; set; }
        /*<summary>The get and set for FlatAmount</summary>*/
        public decimal? FlatAmount { get; set; }
        /*<summary>The get and set for Rate</summary>*/
        public decimal? Rate { get; set; }
        /*<summary>The get and set for UnitId</summary>*/
        public short? UnitId { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int? ViewOrder { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for RateSchedule</summary>*/
        public virtual SaRateSchedules RateSchedule { get; set; }
        /*<summary>The navigational property for TuitionCategory</summary>*/
        public virtual SaTuitionCategories TuitionCategory { get; set; }
    }
}