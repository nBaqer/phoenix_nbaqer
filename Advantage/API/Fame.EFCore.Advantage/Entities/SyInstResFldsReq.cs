﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyInstResFldsReq.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyInstResFldsReq definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyInstResFldsReq</summary>*/
    public partial class SyInstResFldsReq
    {
        /*<summary>The get and set for ResReqFldId</summary>*/
        public Guid ResReqFldId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int ResourceId { get; set; }
        /*<summary>The get and set for TblFldsId</summary>*/
        public int TblFldsId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
    }
}