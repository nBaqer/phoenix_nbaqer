﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyNaccasdropReasons.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyNaccasdropReasons definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyNaccasdropReasons</summary>*/
    public partial class SyNaccasdropReasons
    {
        /*<summary>The constructor for SyNaccasdropReasons</summary>*/
        public SyNaccasdropReasons()
        {
            SyNaccasdropReasonsMapping = new HashSet<SyNaccasdropReasonsMapping>();
        }

        /*<summary>The get and set for NaccasdropReasonId</summary>*/
        public Guid NaccasdropReasonId { get; set; }
        /*<summary>The get and set for Name</summary>*/
        public string Name { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }

        /*<summary>The navigational property for SyNaccasdropReasonsMapping</summary>*/
        public virtual ICollection<SyNaccasdropReasonsMapping>SyNaccasdropReasonsMapping { get; set; }
    }
}