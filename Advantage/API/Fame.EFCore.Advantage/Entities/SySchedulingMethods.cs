﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySchedulingMethods.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySchedulingMethods definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySchedulingMethods</summary>*/
    public partial class SySchedulingMethods
    {
        /*<summary>The constructor for SySchedulingMethods</summary>*/
        public SySchedulingMethods()
        {
            ArPrgVersions = new HashSet<ArPrgVersions>();
        }

        /*<summary>The get and set for SchedMethodId</summary>*/
        public int SchedMethodId { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }

        /*<summary>The navigational property for ArPrgVersions</summary>*/
        public virtual ICollection<ArPrgVersions>ArPrgVersions { get; set; }
    }
}