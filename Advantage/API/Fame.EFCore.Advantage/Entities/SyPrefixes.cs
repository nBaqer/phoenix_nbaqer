﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyPrefixes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyPrefixes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyPrefixes</summary>*/
    public partial class SyPrefixes
    {
        /*<summary>The constructor for SyPrefixes</summary>*/
        public SyPrefixes()
        {
            AdLeadOtherContacts = new HashSet<AdLeadOtherContacts>();
            AdLeads = new HashSet<AdLeads>();
            HrEmployees = new HashSet<HrEmployees>();
            PlEmployerContact = new HashSet<PlEmployerContact>();
            PriorWorkContact = new HashSet<PriorWorkContact>();
            SyInstitutionContacts = new HashSet<SyInstitutionContacts>();
        }

        /*<summary>The get and set for PrefixId</summary>*/
        public Guid PrefixId { get; set; }
        /*<summary>The get and set for PrefixCode</summary>*/
        public string PrefixCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for PrefixDescrip</summary>*/
        public string PrefixDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdLeadOtherContacts</summary>*/
        public virtual ICollection<AdLeadOtherContacts>AdLeadOtherContacts { get; set; }
        /*<summary>The navigational property for AdLeads</summary>*/
        public virtual ICollection<AdLeads>AdLeads { get; set; }
        /*<summary>The navigational property for HrEmployees</summary>*/
        public virtual ICollection<HrEmployees>HrEmployees { get; set; }
        /*<summary>The navigational property for PlEmployerContact</summary>*/
        public virtual ICollection<PlEmployerContact>PlEmployerContact { get; set; }
        /*<summary>The navigational property for PriorWorkContact</summary>*/
        public virtual ICollection<PriorWorkContact>PriorWorkContact { get; set; }
        /*<summary>The navigational property for SyInstitutionContacts</summary>*/
        public virtual ICollection<SyInstitutionContacts>SyInstitutionContacts { get; set; }
    }
}