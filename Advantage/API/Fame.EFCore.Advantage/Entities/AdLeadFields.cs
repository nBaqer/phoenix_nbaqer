﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadFields.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadFields definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadFields</summary>*/
    public partial class AdLeadFields
    {
        /*<summary>The get and set for LeadId</summary>*/
        public int LeadId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for LeadField</summary>*/
        public int? LeadField { get; set; }
        /*<summary>The get and set for LeadFieldId</summary>*/
        public Guid LeadFieldId { get; set; }
    }
}