﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArPrgVersionFees.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArPrgVersionFees definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArPrgVersionFees</summary>*/
    public partial class ArPrgVersionFees
    {
        /*<summary>The get and set for PrgVersionFeeId</summary>*/
        public Guid PrgVersionFeeId { get; set; }
        /*<summary>The get and set for PrgVersionFeeCode</summary>*/
        public string PrgVersionFeeCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for PrgVersionFeeDescrip</summary>*/
        public string PrgVersionFeeDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for PrgVerId</summary>*/
        public Guid? PrgVerId { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid? TransCodeId { get; set; }
        /*<summary>The get and set for TuitionCategoryId</summary>*/
        public Guid? TuitionCategoryId { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal Amount { get; set; }
        /*<summary>The get and set for RateScheduleId</summary>*/
        public Guid? RateScheduleId { get; set; }
        /*<summary>The get and set for UnitId</summary>*/
        public short UnitId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime? StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for PrgVer</summary>*/
        public virtual ArPrgVersions PrgVer { get; set; }
        /*<summary>The navigational property for RateSchedule</summary>*/
        public virtual SaRateSchedules RateSchedule { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
        /*<summary>The navigational property for TuitionCategory</summary>*/
        public virtual SaTuitionCategories TuitionCategory { get; set; }
    }
}