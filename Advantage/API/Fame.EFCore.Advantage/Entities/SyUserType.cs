﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyUserType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyUserType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyUserType</summary>*/
    public partial class SyUserType
    {
        /*<summary>The constructor for SyUserType</summary>*/
        public SyUserType()
        {
            SySysRoles = new HashSet<SySysRoles>();
            SyUsers = new HashSet<SyUsers>();
        }

        /*<summary>The get and set for UserTypeId</summary>*/
        public int UserTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for SySysRoles</summary>*/
        public virtual ICollection<SySysRoles>SySysRoles { get; set; }
        /*<summary>The navigational property for SyUsers</summary>*/
        public virtual ICollection<SyUsers>SyUsers { get; set; }
    }
}