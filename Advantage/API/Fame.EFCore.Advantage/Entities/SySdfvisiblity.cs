﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySdfvisiblity.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySdfvisiblity definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySdfvisiblity</summary>*/
    public partial class SySdfvisiblity
    {
        /*<summary>The get and set for VisId</summary>*/
        public byte VisId { get; set; }
        /*<summary>The get and set for Sdfvisibility</summary>*/
        public string Sdfvisibility { get; set; }
    }
}