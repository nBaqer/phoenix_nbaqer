﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyBridgeResourceIdRptFldId.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyBridgeResourceIdRptFldId definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyBridgeResourceIdRptFldId</summary>*/
    public partial class SyBridgeResourceIdRptFldId
    {
        /*<summary>The get and set for BridgeId</summary>*/
        public int BridgeId { get; set; }
        /*<summary>The get and set for ResourceId</summary>*/
        public int? ResourceId { get; set; }
        /*<summary>The get and set for RptFldId</summary>*/
        public int? RptFldId { get; set; }
    }
}