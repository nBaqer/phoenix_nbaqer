﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArBkCategories.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArBkCategories definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArBkCategories</summary>*/
    public partial class ArBkCategories
    {
        /*<summary>The constructor for ArBkCategories</summary>*/
        public ArBkCategories()
        {
            ArBooks = new HashSet<ArBooks>();
        }

        /*<summary>The get and set for CategoryId</summary>*/
        public Guid CategoryId { get; set; }
        /*<summary>The get and set for CategoryCode</summary>*/
        public string CategoryCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CategoryDescrip</summary>*/
        public string CategoryDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArBooks</summary>*/
        public virtual ICollection<ArBooks>ArBooks { get; set; }
    }
}