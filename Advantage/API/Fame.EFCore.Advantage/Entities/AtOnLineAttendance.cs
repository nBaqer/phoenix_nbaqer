﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AtOnLineAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AtOnLineAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AtOnLineAttendance</summary>*/
    public partial class AtOnLineAttendance
    {
        /*<summary>The get and set for OnLineStudentId</summary>*/
        public int OnLineStudentId { get; set; }
        /*<summary>The get and set for CourseShortName</summary>*/
        public string CourseShortName { get; set; }
        /*<summary>The get and set for AttendedDate</summary>*/
        public DateTime AttendedDate { get; set; }
        /*<summary>The get and set for Minutes</summary>*/
        public int Minutes { get; set; }
        /*<summary>The get and set for OnLineTermId</summary>*/
        public int OnLineTermId { get; set; }
        /*<summary>The get and set for OnlineAttendanceId</summary>*/
        public Guid OnlineAttendanceId { get; set; }
    }
}