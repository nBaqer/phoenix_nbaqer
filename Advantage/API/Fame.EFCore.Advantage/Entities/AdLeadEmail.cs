﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadEmail.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadEmail definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadEmail</summary>*/
    public partial class AdLeadEmail
    {
        /*<summary>The get and set for LeadEmailId</summary>*/
        public Guid LeadEmailId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for EmailTypeId</summary>*/
        public Guid EmailTypeId { get; set; }
        /*<summary>The get and set for IsPreferred</summary>*/
        public bool IsPreferred { get; set; }
        /*<summary>The get and set for IsPortalUserName</summary>*/
        public bool IsPortalUserName { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IsShowOnLeadPage</summary>*/
        public bool IsShowOnLeadPage { get; set; }

        /*<summary>The navigational property for EmailType</summary>*/
        public virtual SyEmailType EmailType { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}