﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyEmailType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyEmailType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyEmailType</summary>*/
    public partial class SyEmailType
    {
        /*<summary>The constructor for SyEmailType</summary>*/
        public SyEmailType()
        {
            AdLeadEmail = new HashSet<AdLeadEmail>();
            AdLeadOtherContactsEmail = new HashSet<AdLeadOtherContactsEmail>();
        }

        /*<summary>The get and set for EmailTypeId</summary>*/
        public Guid EmailTypeId { get; set; }
        /*<summary>The get and set for EmailTypeCode</summary>*/
        public string EmailTypeCode { get; set; }
        /*<summary>The get and set for EmailTypeDescription</summary>*/
        public string EmailTypeDescription { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }

        /*<summary>The navigational property for AdLeadEmail</summary>*/
        public virtual ICollection<AdLeadEmail>AdLeadEmail { get; set; }
        /*<summary>The navigational property for AdLeadOtherContactsEmail</summary>*/
        public virtual ICollection<AdLeadOtherContactsEmail>AdLeadOtherContactsEmail { get; set; }
    }
}