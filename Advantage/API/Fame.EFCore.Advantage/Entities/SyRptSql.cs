﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyRptSql.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyRptSql definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyRptSql</summary>*/
    public partial class SyRptSql
    {
        /*<summary>The get and set for RptSqlid</summary>*/
        public short RptSqlid { get; set; }
        /*<summary>The get and set for Sqlstmt</summary>*/
        public string Sqlstmt { get; set; }
        /*<summary>The get and set for WhereClause</summary>*/
        public string WhereClause { get; set; }
        /*<summary>The get and set for OrderByClause</summary>*/
        public string OrderByClause { get; set; }
    }
}