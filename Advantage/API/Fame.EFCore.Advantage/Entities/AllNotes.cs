﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AllNotes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AllNotes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AllNotes</summary>*/
    public partial class AllNotes
    {
        /*<summary>The constructor for AllNotes</summary>*/
        public AllNotes()
        {
            AdLeadNotes = new HashSet<AdLeadNotes>();
        }

        /*<summary>The get and set for NotesId</summary>*/
        public int NotesId { get; set; }
        /*<summary>The get and set for ModuleCode</summary>*/
        public string ModuleCode { get; set; }
        /*<summary>The get and set for NoteType</summary>*/
        public string NoteType { get; set; }
        /*<summary>The get and set for UserId</summary>*/
        public Guid UserId { get; set; }
        /*<summary>The get and set for NoteText</summary>*/
        public string NoteText { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for PageFieldId</summary>*/
        public int PageFieldId { get; set; }

        /*<summary>The navigational property for ModuleCodeNavigation</summary>*/
        public virtual SyModules ModuleCodeNavigation { get; set; }
        /*<summary>The navigational property for PageField</summary>*/
        public virtual SyNotesPageFields PageField { get; set; }
        /*<summary>The navigational property for AdLeadNotes</summary>*/
        public virtual ICollection<AdLeadNotes>AdLeadNotes { get; set; }
    }
}