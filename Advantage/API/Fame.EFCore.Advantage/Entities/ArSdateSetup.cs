﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArSdateSetup.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArSdateSetup definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArSdateSetup</summary>*/
    public partial class ArSdateSetup
    {
        /*<summary>The get and set for SdateSetupId</summary>*/
        public Guid SdateSetupId { get; set; }
        /*<summary>The get and set for SdateSetupCode</summary>*/
        public string SdateSetupCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ShiftId</summary>*/
        public Guid ShiftId { get; set; }
        /*<summary>The get and set for Sdate</summary>*/
        public DateTime Sdate { get; set; }
        /*<summary>The get and set for MidPtDate</summary>*/
        public DateTime? MidPtDate { get; set; }
        /*<summary>The get and set for ExpGradDate</summary>*/
        public DateTime ExpGradDate { get; set; }
        /*<summary>The get and set for MaxGradDate</summary>*/
        public DateTime MaxGradDate { get; set; }
        /*<summary>The get and set for SdateSetupDescrip</summary>*/
        public string SdateSetupDescrip { get; set; }
        /*<summary>The get and set for BudStarts</summary>*/
        public int BudStarts { get; set; }
        /*<summary>The get and set for MaxStarts</summary>*/
        public int MaxStarts { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ProgId</summary>*/
        public Guid? ProgId { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Prog</summary>*/
        public virtual ArPrograms Prog { get; set; }
        /*<summary>The navigational property for Shift</summary>*/
        public virtual ArShifts Shift { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}