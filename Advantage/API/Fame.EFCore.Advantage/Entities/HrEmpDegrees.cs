﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="HrEmpDegrees.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The HrEmpDegrees definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for HrEmpDegrees</summary>*/
    public partial class HrEmpDegrees
    {
        /*<summary>The get and set for EmpDegreeId</summary>*/
        public Guid EmpDegreeId { get; set; }
        /*<summary>The get and set for EmpId</summary>*/
        public Guid EmpId { get; set; }
        /*<summary>The get and set for DegreeId</summary>*/
        public Guid DegreeId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for Degree</summary>*/
        public virtual ArDegrees Degree { get; set; }
        /*<summary>The navigational property for Emp</summary>*/
        public virtual HrEmployees Emp { get; set; }
    }
}