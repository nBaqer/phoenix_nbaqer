﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArTimeClockSpecialCode.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArTimeClockSpecialCode definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArTimeClockSpecialCode</summary>*/
    public partial class ArTimeClockSpecialCode
    {
        /*<summary>The get and set for Tcsid</summary>*/
        public Guid Tcsid { get; set; }
        /*<summary>The get and set for TcspecialCode</summary>*/
        public string TcspecialCode { get; set; }
        /*<summary>The get and set for TcspunchType</summary>*/
        public short TcspunchType { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid CampusId { get; set; }
        /*<summary>The get and set for InstructionTypeId</summary>*/
        public Guid InstructionTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for InstructionType</summary>*/
        public virtual ArInstructionType InstructionType { get; set; }
    }
}