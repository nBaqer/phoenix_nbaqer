﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFieldTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFieldTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFieldTypes</summary>*/
    public partial class SyFieldTypes
    {
        /*<summary>The constructor for SyFieldTypes</summary>*/
        public SyFieldTypes()
        {
            SyFields = new HashSet<SyFields>();
        }

        /*<summary>The get and set for FldTypeId</summary>*/
        public int FldTypeId { get; set; }
        /*<summary>The get and set for FldType</summary>*/
        public string FldType { get; set; }

        /*<summary>The navigational property for SyFields</summary>*/
        public virtual ICollection<SyFields>SyFields { get; set; }
    }
}