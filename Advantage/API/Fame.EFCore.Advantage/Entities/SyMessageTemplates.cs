﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMessageTemplates.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMessageTemplates definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMessageTemplates</summary>*/
    public partial class SyMessageTemplates
    {
        /*<summary>The constructor for SyMessageTemplates</summary>*/
        public SyMessageTemplates()
        {
            SyMessages = new HashSet<SyMessages>();
        }

        /*<summary>The get and set for MessageTemplateId</summary>*/
        public Guid MessageTemplateId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for TemplateDescrip</summary>*/
        public string TemplateDescrip { get; set; }
        /*<summary>The get and set for MessageSchemaId</summary>*/
        public Guid MessageSchemaId { get; set; }
        /*<summary>The get and set for AdvantageEventId</summary>*/
        public byte AdvantageEventId { get; set; }
        /*<summary>The get and set for RecipientTypeId</summary>*/
        public byte RecipientTypeId { get; set; }
        /*<summary>The get and set for SqlStatement</summary>*/
        public string SqlStatement { get; set; }
        /*<summary>The get and set for XsltHtml</summary>*/
        public string XsltHtml { get; set; }
        /*<summary>The get and set for XsltRtf</summary>*/
        public string XsltRtf { get; set; }
        /*<summary>The get and set for XslFo</summary>*/
        public string XslFo { get; set; }
        /*<summary>The get and set for SpsData</summary>*/
        public string SpsData { get; set; }
        /*<summary>The get and set for MessageTypeId</summary>*/
        public byte MessageTypeId { get; set; }
        /*<summary>The get and set for MessageFormatId</summary>*/
        public byte MessageFormatId { get; set; }
        /*<summary>The get and set for SendImmediately</summary>*/
        public bool SendImmediately { get; set; }
        /*<summary>The get and set for KeepHistory</summary>*/
        public bool? KeepHistory { get; set; }
        /*<summary>The get and set for DelayTimeSpanUnitId</summary>*/
        public byte DelayTimeSpanUnitId { get; set; }
        /*<summary>The get and set for DelayTimeSpanNumberOfUnits</summary>*/
        public byte DelayTimeSpanNumberOfUnits { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for MessageSchema</summary>*/
        public virtual SyMessageSchemas MessageSchema { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyMessages</summary>*/
        public virtual ICollection<SyMessages>SyMessages { get; set; }
    }
}