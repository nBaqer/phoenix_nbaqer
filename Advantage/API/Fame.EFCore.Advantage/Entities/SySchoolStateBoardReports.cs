﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySchoolStateBoardReports.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySchoolStateBoardReports definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySchoolStateBoardReports</summary>*/
    public partial class SySchoolStateBoardReports
    {
        /*<summary>The constructor for SySchoolStateBoardReports</summary>*/
        public SySchoolStateBoardReports()
        {
            SyStateBoardProgramCourseMappings = new HashSet<SyStateBoardProgramCourseMappings>();
        }

        /*<summary>The get and set for SchoolStateBoardReportId</summary>*/
        public Guid SchoolStateBoardReportId { get; set; }
        /*<summary>The get and set for StateId</summary>*/
        public Guid? StateId { get; set; }
        /*<summary>The get and set for ReportId</summary>*/
        public Guid? ReportId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for ModifiedDate</summary>*/
        public DateTime? ModifiedDate { get; set; }
        /*<summary>The get and set for ModifiedUser</summary>*/
        public string ModifiedUser { get; set; }
        /*<summary>The get and set for OwnerName</summary>*/
        public string OwnerName { get; set; }
        /*<summary>The get and set for OwnerLicenseNumber</summary>*/
        public string OwnerLicenseNumber { get; set; }
        /*<summary>The get and set for OfficerName1</summary>*/
        public string OfficerName1 { get; set; }
        /*<summary>The get and set for OfficerName2</summary>*/
        public string OfficerName2 { get; set; }
        /*<summary>The get and set for OfficerName3</summary>*/
        public string OfficerName3 { get; set; }
        /*<summary>The get and set for LicensingAgencyId</summary>*/
        public int LicensingAgencyId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for LicensingAgency</summary>*/
        public virtual SyStateBoardAgencies LicensingAgency { get; set; }
        /*<summary>The navigational property for Report</summary>*/
        public virtual SyReports Report { get; set; }
        /*<summary>The navigational property for State</summary>*/
        public virtual SyStates State { get; set; }
        /*<summary>The navigational property for SyStateBoardProgramCourseMappings</summary>*/
        public virtual ICollection<SyStateBoardProgramCourseMappings>SyStateBoardProgramCourseMappings { get; set; }
    }
}