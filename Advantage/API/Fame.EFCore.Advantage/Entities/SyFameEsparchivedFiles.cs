﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFameEsparchivedFiles.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFameEsparchivedFiles definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFameEsparchivedFiles</summary>*/
    public partial class SyFameEsparchivedFiles
    {
        /*<summary>The get and set for ArchiveId</summary>*/
        public Guid ArchiveId { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for IsSuccessfullyProcessed</summary>*/
        public bool? IsSuccessfullyProcessed { get; set; }
        /*<summary>The get and set for Moduser</summary>*/
        public string Moduser { get; set; }
        /*<summary>The get and set for Moddate</summary>*/
        public DateTime? Moddate { get; set; }
    }
}