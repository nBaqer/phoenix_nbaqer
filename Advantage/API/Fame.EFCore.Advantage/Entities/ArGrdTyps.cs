﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdTyps.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdTyps definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdTyps</summary>*/
    public partial class ArGrdTyps
    {
        /*<summary>The get and set for GrdTypId</summary>*/
        public byte GrdTypId { get; set; }
        /*<summary>The get and set for GrdTypDescrip</summary>*/
        public string GrdTypDescrip { get; set; }
    }
}