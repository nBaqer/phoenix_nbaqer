﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArExternshipAttendance.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArExternshipAttendance definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArExternshipAttendance</summary>*/
    public partial class ArExternshipAttendance
    {
        /*<summary>The get and set for ExternshipAttendanceId</summary>*/
        public Guid ExternshipAttendanceId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for AttendedDate</summary>*/
        public DateTime AttendedDate { get; set; }
        /*<summary>The get and set for HoursAttended</summary>*/
        public decimal HoursAttended { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }

        /*<summary>The navigational property for GrdComponentType</summary>*/
        public virtual ArGrdComponentTypes GrdComponentType { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}