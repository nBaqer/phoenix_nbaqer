﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdExtraCurr.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdExtraCurr definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdExtraCurr</summary>*/
    public partial class AdExtraCurr
    {
        /*<summary>The get and set for ExtraCurrId</summary>*/
        public Guid ExtraCurrId { get; set; }
        /*<summary>The get and set for ExtraCurrCode</summary>*/
        public string ExtraCurrCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for ExtraCurrDescrip</summary>*/
        public string ExtraCurrDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ExtraCurricularGrpId</summary>*/
        public Guid ExtraCurricularGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for ExtraCurricularGrp</summary>*/
        public virtual AdExtraCurrGrp ExtraCurricularGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}