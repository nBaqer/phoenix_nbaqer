﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyR2t4calculationPeriodTypes.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyR2t4calculationPeriodTypes definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyR2t4calculationPeriodTypes</summary>*/
    public partial class SyR2t4calculationPeriodTypes
    {
        /*<summary>The constructor for SyR2t4calculationPeriodTypes</summary>*/
        public SyR2t4calculationPeriodTypes()
        {
            ArR2t4terminationDetails = new HashSet<ArR2t4terminationDetails>();
        }

        /*<summary>The get and set for CalculationPeriodTypeId</summary>*/
        public Guid CalculationPeriodTypeId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModifiedByUserId</summary>*/
        public Guid ModifiedByUserId { get; set; }

        /*<summary>The navigational property for ModifiedByUser</summary>*/
        public virtual SyUsers ModifiedByUser { get; set; }
        /*<summary>The navigational property for ArR2t4terminationDetails</summary>*/
        public virtual ICollection<ArR2t4terminationDetails>ArR2t4terminationDetails { get; set; }
    }
}