﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWidgets.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWidgets definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWidgets</summary>*/
    public partial class SyWidgets
    {
        /*<summary>The constructor for SyWidgets</summary>*/
        public SyWidgets()
        {
            SyWidgetResourceRoles = new HashSet<SyWidgetResourceRoles>();
            SyWidgetUserResourceSettings = new HashSet<SyWidgetUserResourceSettings>();
        }

        /*<summary>The get and set for WidgetId</summary>*/
        public Guid WidgetId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for Rows</summary>*/
        public int Rows { get; set; }
        /*<summary>The get and set for Columns</summary>*/
        public int Columns { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyWidgetResourceRoles</summary>*/
        public virtual ICollection<SyWidgetResourceRoles>SyWidgetResourceRoles { get; set; }
        /*<summary>The navigational property for SyWidgetUserResourceSettings</summary>*/
        public virtual ICollection<SyWidgetUserResourceSettings>SyWidgetUserResourceSettings { get; set; }
    }
}