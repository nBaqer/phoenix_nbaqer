﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadTransactions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadTransactions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadTransactions</summary>*/
    public partial class AdLeadTransactions
    {
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for TransCodeId</summary>*/
        public Guid TransCodeId { get; set; }
        /*<summary>The get and set for TransReference</summary>*/
        public string TransReference { get; set; }
        /*<summary>The get and set for TransDescrip</summary>*/
        public string TransDescrip { get; set; }
        /*<summary>The get and set for TransAmount</summary>*/
        public decimal TransAmount { get; set; }
        /*<summary>The get and set for TransDate</summary>*/
        public DateTime TransDate { get; set; }
        /*<summary>The get and set for TransTypeId</summary>*/
        public int TransTypeId { get; set; }
        /*<summary>The get and set for IsEnrolled</summary>*/
        public bool? IsEnrolled { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }
        /*<summary>The get and set for Voided</summary>*/
        public bool? Voided { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }
        /*<summary>The get and set for ReversalReason</summary>*/
        public string ReversalReason { get; set; }
        /*<summary>The get and set for DisplaySequence</summary>*/
        public int? DisplaySequence { get; set; }
        /*<summary>The get and set for SecondDisplaySequence</summary>*/
        public int? SecondDisplaySequence { get; set; }
        /*<summary>The get and set for MapTransactionId</summary>*/
        public Guid? MapTransactionId { get; set; }
        /*<summary>The get and set for LeadRequirementId</summary>*/
        public Guid? LeadRequirementId { get; set; }

        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for LeadRequirement</summary>*/
        public virtual AdReqs LeadRequirement { get; set; }
        /*<summary>The navigational property for TransCode</summary>*/
        public virtual SaTransCodes TransCode { get; set; }
        /*<summary>The navigational property for TransType</summary>*/
        public virtual SaTransTypes TransType { get; set; }
        /*<summary>The navigational property for AdLeadPayments</summary>*/
        public virtual AdLeadPayments AdLeadPayments { get; set; }
    }
}