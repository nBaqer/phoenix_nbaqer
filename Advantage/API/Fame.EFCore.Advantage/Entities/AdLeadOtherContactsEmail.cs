﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadOtherContactsEmail.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadOtherContactsEmail definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadOtherContactsEmail</summary>*/
    public partial class AdLeadOtherContactsEmail
    {
        /*<summary>The get and set for OtherContactsEmailId</summary>*/
        public Guid OtherContactsEmailId { get; set; }
        /*<summary>The get and set for OtherContactId</summary>*/
        public Guid OtherContactId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for Email</summary>*/
        public string Email { get; set; }
        /*<summary>The get and set for EmailTypeId</summary>*/
        public Guid EmailTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }

        /*<summary>The navigational property for EmailType</summary>*/
        public virtual SyEmailType EmailType { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for OtherContact</summary>*/
        public virtual AdLeadOtherContacts OtherContact { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}