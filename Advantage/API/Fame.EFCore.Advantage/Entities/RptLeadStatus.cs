﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="RptLeadStatus.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The RptLeadStatus definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for RptLeadStatus</summary>*/
    public partial class RptLeadStatus
    {
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid StatusCodeId { get; set; }
        /*<summary>The get and set for StatusCodeDescrip</summary>*/
        public string StatusCodeDescrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampusId</summary>*/
        public Guid? CampusId { get; set; }

        /*<summary>The navigational property for Campus</summary>*/
        public virtual SyCampuses Campus { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}