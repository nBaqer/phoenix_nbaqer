﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStuProbWarnings.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStuProbWarnings definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStuProbWarnings</summary>*/
    public partial class ArStuProbWarnings
    {
        /*<summary>The get and set for StuProbWarningId</summary>*/
        public Guid StuProbWarningId { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime? EndDate { get; set; }
        /*<summary>The get and set for ProbWarningTypeId</summary>*/
        public int? ProbWarningTypeId { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for StudentStatusChangeId</summary>*/
        public Guid? StudentStatusChangeId { get; set; }

        /*<summary>The navigational property for ProbWarningType</summary>*/
        public virtual ArProbWarningTypes ProbWarningType { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}