﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlExitInterview.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlExitInterview definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlExitInterview</summary>*/
    public partial class PlExitInterview
    {
        /*<summary>The get and set for ExtInterviewId</summary>*/
        public Guid ExtInterviewId { get; set; }
        /*<summary>The get and set for EnrollmentId</summary>*/
        public Guid? EnrollmentId { get; set; }
        /*<summary>The get and set for ScStatusId</summary>*/
        public Guid? ScStatusId { get; set; }
        /*<summary>The get and set for WantAssistance</summary>*/
        public string WantAssistance { get; set; }
        /*<summary>The get and set for WaiverSigned</summary>*/
        public string WaiverSigned { get; set; }
        /*<summary>The get and set for Eligible</summary>*/
        public string Eligible { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for AreaId</summary>*/
        public Guid? AreaId { get; set; }
        /*<summary>The get and set for AvailableDate</summary>*/
        public DateTime? AvailableDate { get; set; }
        /*<summary>The get and set for AvailableDays</summary>*/
        public string AvailableDays { get; set; }
        /*<summary>The get and set for AvailableHours</summary>*/
        public string AvailableHours { get; set; }
        /*<summary>The get and set for LowSalary</summary>*/
        public string LowSalary { get; set; }
        /*<summary>The get and set for HighSalary</summary>*/
        public string HighSalary { get; set; }
        /*<summary>The get and set for TransportationId</summary>*/
        public Guid? TransportationId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ExpertiseLevelId</summary>*/
        public Guid? ExpertiseLevelId { get; set; }
        /*<summary>The get and set for FullTimeId</summary>*/
        public Guid? FullTimeId { get; set; }
        /*<summary>The get and set for InelReasonId</summary>*/
        public Guid? InelReasonId { get; set; }
        /*<summary>The get and set for ExitInterviewDate</summary>*/
        public DateTime? ExitInterviewDate { get; set; }

        /*<summary>The navigational property for Area</summary>*/
        public virtual AdCounties Area { get; set; }
        /*<summary>The navigational property for Enrollment</summary>*/
        public virtual ArStuEnrollments Enrollment { get; set; }
        /*<summary>The navigational property for ExpertiseLevel</summary>*/
        public virtual AdExpertiseLevel ExpertiseLevel { get; set; }
        /*<summary>The navigational property for FullTime</summary>*/
        public virtual AdFullPartTime FullTime { get; set; }
        /*<summary>The navigational property for InelReason</summary>*/
        public virtual SyIneligibilityReasons InelReason { get; set; }
        /*<summary>The navigational property for ScStatus</summary>*/
        public virtual SyStatuses ScStatus { get; set; }
        /*<summary>The navigational property for Transportation</summary>*/
        public virtual PlTransportation Transportation { get; set; }
    }
}