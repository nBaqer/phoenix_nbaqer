﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyConfigAppSetLookup.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyConfigAppSetLookup definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyConfigAppSetLookup</summary>*/
    public partial class SyConfigAppSetLookup
    {
        /*<summary>The get and set for LookUpId</summary>*/
        public Guid LookUpId { get; set; }
        /*<summary>The get and set for SettingId</summary>*/
        public int? SettingId { get; set; }
        /*<summary>The get and set for ValueOptions</summary>*/
        public string ValueOptions { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
    }
}