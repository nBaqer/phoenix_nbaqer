﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMessageSchemas.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMessageSchemas definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMessageSchemas</summary>*/
    public partial class SyMessageSchemas
    {
        /*<summary>The constructor for SyMessageSchemas</summary>*/
        public SyMessageSchemas()
        {
            SyMessageGroups = new HashSet<SyMessageGroups>();
            SyMessageTemplates = new HashSet<SyMessageTemplates>();
        }

        /*<summary>The get and set for MessageSchemaId</summary>*/
        public Guid MessageSchemaId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for MessageSchemaDescrip</summary>*/
        public string MessageSchemaDescrip { get; set; }
        /*<summary>The get and set for SqlStatement</summary>*/
        public string SqlStatement { get; set; }
        /*<summary>The get and set for MessageSchemaUrl</summary>*/
        public string MessageSchemaUrl { get; set; }
        /*<summary>The get and set for MessageSchema</summary>*/
        public string MessageSchema { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for SyMessageGroupSchemas</summary>*/
        public virtual SyMessageGroupSchemas SyMessageGroupSchemas { get; set; }
        /*<summary>The navigational property for SyMessageGroups</summary>*/
        public virtual ICollection<SyMessageGroups>SyMessageGroups { get; set; }
        /*<summary>The navigational property for SyMessageTemplates</summary>*/
        public virtual ICollection<SyMessageTemplates>SyMessageTemplates { get; set; }
    }
}