﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyStudentFormat.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyStudentFormat definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyStudentFormat</summary>*/
    public partial class SyStudentFormat
    {
        /*<summary>The get and set for FormatType</summary>*/
        public string FormatType { get; set; }
        /*<summary>The get and set for YearNumber</summary>*/
        public int? YearNumber { get; set; }
        /*<summary>The get and set for MonthNumber</summary>*/
        public int? MonthNumber { get; set; }
        /*<summary>The get and set for DateNumber</summary>*/
        public int? DateNumber { get; set; }
        /*<summary>The get and set for LnameNumber</summary>*/
        public int? LnameNumber { get; set; }
        /*<summary>The get and set for FnameNumber</summary>*/
        public int? FnameNumber { get; set; }
        /*<summary>The get and set for SeqNumber</summary>*/
        public int? SeqNumber { get; set; }
        /*<summary>The get and set for SeqStartingNumber</summary>*/
        public int? SeqStartingNumber { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StudentFormatId</summary>*/
        public int StudentFormatId { get; set; }
    }
}