﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGradeScaleDetails.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGradeScaleDetails definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGradeScaleDetails</summary>*/
    public partial class ArGradeScaleDetails
    {
        /*<summary>The get and set for GrdScaleDetailId</summary>*/
        public Guid GrdScaleDetailId { get; set; }
        /*<summary>The get and set for GrdScaleId</summary>*/
        public Guid GrdScaleId { get; set; }
        /*<summary>The get and set for MinVal</summary>*/
        public short MinVal { get; set; }
        /*<summary>The get and set for MaxVal</summary>*/
        public short MaxVal { get; set; }
        /*<summary>The get and set for GrdSysDetailId</summary>*/
        public Guid GrdSysDetailId { get; set; }
        /*<summary>The get and set for ViewOrder</summary>*/
        public int ViewOrder { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for GrdScale</summary>*/
        public virtual ArGradeScales GrdScale { get; set; }
        /*<summary>The navigational property for GrdSysDetail</summary>*/
        public virtual ArGradeSystemDetails GrdSysDetail { get; set; }
    }
}