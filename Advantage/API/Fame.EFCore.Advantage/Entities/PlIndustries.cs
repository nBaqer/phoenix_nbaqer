﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlIndustries.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlIndustries definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlIndustries</summary>*/
    public partial class PlIndustries
    {
        /*<summary>The constructor for PlIndustries</summary>*/
        public PlIndustries()
        {
            PlEmployers = new HashSet<PlEmployers>();
        }

        /*<summary>The get and set for IndustryId</summary>*/
        public Guid IndustryId { get; set; }
        /*<summary>The get and set for IndustryCode</summary>*/
        public string IndustryCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for IndustryDescrip</summary>*/
        public string IndustryDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployers</summary>*/
        public virtual ICollection<PlEmployers>PlEmployers { get; set; }
    }
}