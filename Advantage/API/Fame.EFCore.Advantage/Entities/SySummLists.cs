﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SySummLists.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SySummLists definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SySummLists</summary>*/
    public partial class SySummLists
    {
        /*<summary>The get and set for SummListId</summary>*/
        public short SummListId { get; set; }
        /*<summary>The get and set for SummListName</summary>*/
        public string SummListName { get; set; }
        /*<summary>The get and set for TblId</summary>*/
        public short TblId { get; set; }
        /*<summary>The get and set for DispFldId</summary>*/
        public short DispFldId { get; set; }
        /*<summary>The get and set for ValFldId</summary>*/
        public short ValFldId { get; set; }
    }
}