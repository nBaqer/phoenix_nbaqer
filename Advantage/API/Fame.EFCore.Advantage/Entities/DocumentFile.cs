﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="DocumentFile.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The DocumentFile definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for DocumentFile</summary>*/
    public partial class DocumentFile
    {
        /*<summary>The get and set for Id</summary>*/
        public Guid Id { get; set; }
        /*<summary>The get and set for StudentDocId</summary>*/
        public Guid StudentDocId { get; set; }
        /*<summary>The get and set for FileName</summary>*/
        public string FileName { get; set; }
        /*<summary>The get and set for Content</summary>*/
        public byte[] Content { get; set; }
        /*<summary>The get and set for DateCreated</summary>*/
        public DateTime DateCreated { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for StudentDoc</summary>*/
        public virtual PlStudentDocs StudentDoc { get; set; }
    }
}