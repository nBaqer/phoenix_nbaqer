﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArMentorGradeComponentTypesCourses.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArMentorGradeComponentTypesCourses definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArMentorGradeComponentTypesCourses</summary>*/
    public partial class ArMentorGradeComponentTypesCourses
    {
        /*<summary>The get and set for MentorProctoredId</summary>*/
        public Guid MentorProctoredId { get; set; }
        /*<summary>The get and set for ReqId</summary>*/
        public Guid? ReqId { get; set; }
        /*<summary>The get and set for EffectiveDate</summary>*/
        public DateTime? EffectiveDate { get; set; }
        /*<summary>The get and set for MentorRequirement</summary>*/
        public string MentorRequirement { get; set; }
        /*<summary>The get and set for MentorOperator</summary>*/
        public string MentorOperator { get; set; }
        /*<summary>The get and set for GrdComponentTypeId</summary>*/
        public Guid? GrdComponentTypeId { get; set; }
        /*<summary>The get and set for Speed</summary>*/
        public int? Speed { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for OperatorSequence</summary>*/
        public int? OperatorSequence { get; set; }
    }
}