﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyFldCaptions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyFldCaptions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyFldCaptions</summary>*/
    public partial class SyFldCaptions
    {
        /*<summary>The get and set for FldCapId</summary>*/
        public int FldCapId { get; set; }
        /*<summary>The get and set for FldId</summary>*/
        public int FldId { get; set; }
        /*<summary>The get and set for LangId</summary>*/
        public byte LangId { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
        /*<summary>The get and set for FldDescrip</summary>*/
        public string FldDescrip { get; set; }

        /*<summary>The navigational property for Fld</summary>*/
        public virtual SyFields Fld { get; set; }
        /*<summary>The navigational property for Lang</summary>*/
        public virtual SyLangs Lang { get; set; }
    }
}