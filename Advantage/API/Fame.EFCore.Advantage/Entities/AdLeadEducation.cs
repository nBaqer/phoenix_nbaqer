﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdLeadEducation.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdLeadEducation definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdLeadEducation</summary>*/
    public partial class AdLeadEducation
    {
        /*<summary>The get and set for LeadEducationId</summary>*/
        public Guid LeadEducationId { get; set; }
        /*<summary>The get and set for LeadId</summary>*/
        public Guid LeadId { get; set; }
        /*<summary>The get and set for EducationInstId</summary>*/
        public Guid EducationInstId { get; set; }
        /*<summary>The get and set for EducationInstType</summary>*/
        public string EducationInstType { get; set; }
        /*<summary>The get and set for GraduatedDate</summary>*/
        public DateTime? GraduatedDate { get; set; }
        /*<summary>The get and set for FinalGrade</summary>*/
        public string FinalGrade { get; set; }
        /*<summary>The get and set for CertificateId</summary>*/
        public Guid? CertificateId { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Major</summary>*/
        public string Major { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for Graduate</summary>*/
        public bool Graduate { get; set; }
        /*<summary>The get and set for Gpa</summary>*/
        public decimal? Gpa { get; set; }
        /*<summary>The get and set for Rank</summary>*/
        public int? Rank { get; set; }
        /*<summary>The get and set for Percentile</summary>*/
        public int? Percentile { get; set; }
        /*<summary>The get and set for Certificate</summary>*/
        public string Certificate { get; set; }

        /*<summary>The navigational property for CertificateNavigation</summary>*/
        public virtual ArDegrees CertificateNavigation { get; set; }
        /*<summary>The navigational property for Lead</summary>*/
        public virtual AdLeads Lead { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
    }
}