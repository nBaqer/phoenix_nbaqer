﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlFldStudy.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlFldStudy definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlFldStudy</summary>*/
    public partial class PlFldStudy
    {
        /*<summary>The constructor for PlFldStudy</summary>*/
        public PlFldStudy()
        {
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for FldStudyId</summary>*/
        public Guid FldStudyId { get; set; }
        /*<summary>The get and set for FldStudyDescrip</summary>*/
        public string FldStudyDescrip { get; set; }
        /*<summary>The get and set for FldStudyCode</summary>*/
        public string FldStudyCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}