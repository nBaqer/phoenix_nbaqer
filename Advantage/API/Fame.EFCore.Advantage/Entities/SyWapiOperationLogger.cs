﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiOperationLogger.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiOperationLogger definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiOperationLogger</summary>*/
    public partial class SyWapiOperationLogger
    {
        /*<summary>The get and set for Id</summary>*/
        public long Id { get; set; }
        /*<summary>The get and set for ServiceCode</summary>*/
        public string ServiceCode { get; set; }
        /*<summary>The get and set for CompanyCode</summary>*/
        public string CompanyCode { get; set; }
        /*<summary>The get and set for DateExecution</summary>*/
        public DateTime DateExecution { get; set; }
        /*<summary>The get and set for NextPlanningDate</summary>*/
        public DateTime? NextPlanningDate { get; set; }
        /*<summary>The get and set for IsError</summary>*/
        public bool IsError { get; set; }
        /*<summary>The get and set for Comment</summary>*/
        public string Comment { get; set; }
    }
}