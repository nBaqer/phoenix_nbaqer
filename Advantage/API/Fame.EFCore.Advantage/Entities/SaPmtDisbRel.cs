﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SaPmtDisbRel.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SaPmtDisbRel definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SaPmtDisbRel</summary>*/
    public partial class SaPmtDisbRel
    {
        /*<summary>The get and set for PmtDisbRelId</summary>*/
        public Guid PmtDisbRelId { get; set; }
        /*<summary>The get and set for TransactionId</summary>*/
        public Guid TransactionId { get; set; }
        /*<summary>The get and set for Amount</summary>*/
        public decimal Amount { get; set; }
        /*<summary>The get and set for AwardScheduleId</summary>*/
        public Guid? AwardScheduleId { get; set; }
        /*<summary>The get and set for PayPlanScheduleId</summary>*/
        public Guid? PayPlanScheduleId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for StuAwardId</summary>*/
        public Guid? StuAwardId { get; set; }

        /*<summary>The navigational property for AwardSchedule</summary>*/
        public virtual FaStudentAwardSchedule AwardSchedule { get; set; }
        /*<summary>The navigational property for PayPlanSchedule</summary>*/
        public virtual FaStuPaymentPlanSchedule PayPlanSchedule { get; set; }
    }
}