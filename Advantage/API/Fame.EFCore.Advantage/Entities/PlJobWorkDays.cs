﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobWorkDays.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobWorkDays definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobWorkDays</summary>*/
    public partial class PlJobWorkDays
    {
        /*<summary>The constructor for PlJobWorkDays</summary>*/
        public PlJobWorkDays()
        {
            PlStudentsPlaced = new HashSet<PlStudentsPlaced>();
        }

        /*<summary>The get and set for JobWorkDaysId</summary>*/
        public Guid JobWorkDaysId { get; set; }
        /*<summary>The get and set for EmployerJobId</summary>*/
        public Guid? EmployerJobId { get; set; }
        /*<summary>The get and set for WorkDayId</summary>*/
        public Guid? WorkDayId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for EmployerJob</summary>*/
        public virtual PlEmployerJobs EmployerJob { get; set; }
        /*<summary>The navigational property for PlStudentsPlaced</summary>*/
        public virtual ICollection<PlStudentsPlaced>PlStudentsPlaced { get; set; }
    }
}