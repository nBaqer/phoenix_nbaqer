﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArStudentLoas.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArStudentLoas definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArStudentLoas</summary>*/
    public partial class ArStudentLoas
    {
        /*<summary>The get and set for StudentLoaid</summary>*/
        public Guid StudentLoaid { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for StartDate</summary>*/
        public DateTime StartDate { get; set; }
        /*<summary>The get and set for EndDate</summary>*/
        public DateTime EndDate { get; set; }
        /*<summary>The get and set for LoareasonId</summary>*/
        public Guid? LoareasonId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for LoareturnDate</summary>*/
        public DateTime? LoareturnDate { get; set; }
        /*<summary>The get and set for StudentStatusChangeId</summary>*/
        public Guid? StudentStatusChangeId { get; set; }
        /*<summary>The get and set for StatusCodeId</summary>*/
        public Guid? StatusCodeId { get; set; }
        /*<summary>The get and set for LoarequestDate</summary>*/
        public DateTime? LoarequestDate { get; set; }

        /*<summary>The navigational property for Loareason</summary>*/
        public virtual ArLoareasons Loareason { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}