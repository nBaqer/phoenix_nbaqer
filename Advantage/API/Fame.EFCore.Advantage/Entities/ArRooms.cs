﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArRooms.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArRooms definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArRooms</summary>*/
    public partial class ArRooms
    {
        /*<summary>The constructor for ArRooms</summary>*/
        public ArRooms()
        {
            ArClsSectMeetings = new HashSet<ArClsSectMeetings>();
        }

        /*<summary>The get and set for RoomId</summary>*/
        public Guid RoomId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for BldgId</summary>*/
        public Guid BldgId { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for Capacity</summary>*/
        public int? Capacity { get; set; }

        /*<summary>The navigational property for Bldg</summary>*/
        public virtual ArBuildings Bldg { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArClsSectMeetings</summary>*/
        public virtual ICollection<ArClsSectMeetings>ArClsSectMeetings { get; set; }
    }
}