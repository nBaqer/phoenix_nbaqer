﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyWapiAllowedServices.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyWapiAllowedServices definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyWapiAllowedServices</summary>*/
    public partial class SyWapiAllowedServices
    {
        /*<summary>The constructor for SyWapiAllowedServices</summary>*/
        public SyWapiAllowedServices()
        {
            SyWapiBridgeExternalCompanyAllowedServices = new HashSet<SyWapiBridgeExternalCompanyAllowedServices>();
            SyWapiSettingsIdAllowedServiceNavigation = new HashSet<SyWapiSettings>();
            SyWapiSettingsIdSecondAllowedServiceNavigation = new HashSet<SyWapiSettings>();
        }

        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Description</summary>*/
        public string Description { get; set; }
        /*<summary>The get and set for Url</summary>*/
        public string Url { get; set; }
        /*<summary>The get and set for IsActive</summary>*/
        public bool? IsActive { get; set; }

        /*<summary>The navigational property for SyWapiBridgeExternalCompanyAllowedServices</summary>*/
        public virtual ICollection<SyWapiBridgeExternalCompanyAllowedServices>SyWapiBridgeExternalCompanyAllowedServices { get; set; }
        /*<summary>The navigational property for SyWapiSettingsIdAllowedServiceNavigation</summary>*/
        public virtual ICollection<SyWapiSettings>SyWapiSettingsIdAllowedServiceNavigation { get; set; }
        /*<summary>The navigational property for SyWapiSettingsIdSecondAllowedServiceNavigation</summary>*/
        public virtual ICollection<SyWapiSettings>SyWapiSettingsIdSecondAllowedServiceNavigation { get; set; }
    }
}