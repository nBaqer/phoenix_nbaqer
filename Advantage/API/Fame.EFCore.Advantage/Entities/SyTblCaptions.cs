﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyTblCaptions.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyTblCaptions definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyTblCaptions</summary>*/
    public partial class SyTblCaptions
    {
        /*<summary>The get and set for TblCapId</summary>*/
        public int TblCapId { get; set; }
        /*<summary>The get and set for TblId</summary>*/
        public int TblId { get; set; }
        /*<summary>The get and set for LangId</summary>*/
        public byte LangId { get; set; }
        /*<summary>The get and set for Caption</summary>*/
        public string Caption { get; set; }
        /*<summary>The get and set for TblDescrip</summary>*/
        public string TblDescrip { get; set; }

        /*<summary>The navigational property for Tbl</summary>*/
        public virtual SyTables Tbl { get; set; }
    }
}