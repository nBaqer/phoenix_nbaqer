﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArFerpaentity.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArFerpaentity definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArFerpaentity</summary>*/
    public partial class ArFerpaentity
    {
        /*<summary>The constructor for ArFerpaentity</summary>*/
        public ArFerpaentity()
        {
            ArFerpapolicy = new HashSet<ArFerpapolicy>();
        }

        /*<summary>The get and set for FerpaentityId</summary>*/
        public Guid FerpaentityId { get; set; }
        /*<summary>The get and set for FerpaentityCode</summary>*/
        public string FerpaentityCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for FerpaentityDescrip</summary>*/
        public string FerpaentityDescrip { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for ArFerpapolicy</summary>*/
        public virtual ICollection<ArFerpapolicy>ArFerpapolicy { get; set; }
    }
}