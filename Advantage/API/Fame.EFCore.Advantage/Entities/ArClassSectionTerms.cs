﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArClassSectionTerms.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArClassSectionTerms definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArClassSectionTerms</summary>*/
    public partial class ArClassSectionTerms
    {
        /*<summary>The get and set for ClsSectTermId</summary>*/
        public Guid ClsSectTermId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for TermId</summary>*/
        public Guid TermId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
        /*<summary>The navigational property for Term</summary>*/
        public virtual ArTerm Term { get; set; }
    }
}