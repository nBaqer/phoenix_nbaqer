﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyLeadStatusChanges.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyLeadStatusChanges definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyLeadStatusChanges</summary>*/
    public partial class SyLeadStatusChanges
    {
        /*<summary>The constructor for SyLeadStatusChanges</summary>*/
        public SyLeadStatusChanges()
        {
            SyLeadStatusChangePermissions = new HashSet<SyLeadStatusChangePermissions>();
        }

        /*<summary>The get and set for LeadStatusChangeId</summary>*/
        public Guid LeadStatusChangeId { get; set; }
        /*<summary>The get and set for OrigStatusId</summary>*/
        public Guid OrigStatusId { get; set; }
        /*<summary>The get and set for NewStatusId</summary>*/
        public Guid NewStatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid CampGrpId { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for IsReversal</summary>*/
        public bool IsReversal { get; set; }

        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for NewStatus</summary>*/
        public virtual SyStatusCodes NewStatus { get; set; }
        /*<summary>The navigational property for OrigStatus</summary>*/
        public virtual SyStatusCodes OrigStatus { get; set; }
        /*<summary>The navigational property for SyLeadStatusChangePermissions</summary>*/
        public virtual ICollection<SyLeadStatusChangePermissions>SyLeadStatusChangePermissions { get; set; }
    }
}