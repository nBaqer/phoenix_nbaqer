﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="TempStatusChangeHistoryTbl.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The TempStatusChangeHistoryTbl definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for TempStatusChangeHistoryTbl</summary>*/
    public partial class TempStatusChangeHistoryTbl
    {
        /*<summary>The get and set for Id</summary>*/
        public int Id { get; set; }
        /*<summary>The get and set for StudentEnrollmentId</summary>*/
        public Guid? StudentEnrollmentId { get; set; }
        /*<summary>The get and set for EffectiveDate</summary>*/
        public DateTime? EffectiveDate { get; set; }
        /*<summary>The get and set for StatusDescription</summary>*/
        public string StatusDescription { get; set; }
        /*<summary>The get and set for Reason</summary>*/
        public string Reason { get; set; }
        /*<summary>The get and set for RequestedBy</summary>*/
        public string RequestedBy { get; set; }
        /*<summary>The get and set for ChangedBy</summary>*/
        public string ChangedBy { get; set; }
        /*<summary>The get and set for CaseNo</summary>*/
        public string CaseNo { get; set; }
        /*<summary>The get and set for DateOfChange</summary>*/
        public DateTime? DateOfChange { get; set; }

        /*<summary>The navigational property for StudentEnrollment</summary>*/
        public virtual ArStuEnrollments StudentEnrollment { get; set; }
    }
}