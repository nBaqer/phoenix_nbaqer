﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="ArGrdBkResults.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The ArGrdBkResults definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for ArGrdBkResults</summary>*/
    public partial class ArGrdBkResults
    {
        /*<summary>The get and set for GrdBkResultId</summary>*/
        public Guid GrdBkResultId { get; set; }
        /*<summary>The get and set for ClsSectionId</summary>*/
        public Guid ClsSectionId { get; set; }
        /*<summary>The get and set for InstrGrdBkWgtDetailId</summary>*/
        public Guid InstrGrdBkWgtDetailId { get; set; }
        /*<summary>The get and set for Score</summary>*/
        public decimal? Score { get; set; }
        /*<summary>The get and set for Comments</summary>*/
        public string Comments { get; set; }
        /*<summary>The get and set for StuEnrollId</summary>*/
        public Guid StuEnrollId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ResNum</summary>*/
        public int ResNum { get; set; }
        /*<summary>The get and set for PostDate</summary>*/
        public DateTime? PostDate { get; set; }
        /*<summary>The get and set for IsCompGraded</summary>*/
        public bool? IsCompGraded { get; set; }
        /*<summary>The get and set for IsCourseCredited</summary>*/
        public bool? IsCourseCredited { get; set; }
        /*<summary>The get and set for DateCompleted</summary>*/
        public DateTime? DateCompleted { get; set; }

        /*<summary>The navigational property for ClsSection</summary>*/
        public virtual ArClassSections ClsSection { get; set; }
        /*<summary>The navigational property for InstrGrdBkWgtDetail</summary>*/
        public virtual ArGrdBkWgtDetails InstrGrdBkWgtDetail { get; set; }
        /*<summary>The navigational property for StuEnroll</summary>*/
        public virtual ArStuEnrollments StuEnroll { get; set; }
    }
}