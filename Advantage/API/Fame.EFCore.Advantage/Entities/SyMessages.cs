﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="SyMessages.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The SyMessages definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for SyMessages</summary>*/
    public partial class SyMessages
    {
        /*<summary>The get and set for MessageId</summary>*/
        public Guid MessageId { get; set; }
        /*<summary>The get and set for MessageTemplateId</summary>*/
        public Guid MessageTemplateId { get; set; }
        /*<summary>The get and set for RecipientId</summary>*/
        public Guid RecipientId { get; set; }
        /*<summary>The get and set for StudentId</summary>*/
        public Guid? StudentId { get; set; }
        /*<summary>The get and set for XmlContent</summary>*/
        public string XmlContent { get; set; }
        /*<summary>The get and set for CreatedDate</summary>*/
        public DateTime CreatedDate { get; set; }
        /*<summary>The get and set for ActuallySentOn</summary>*/
        public DateTime? ActuallySentOn { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime ModDate { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for HtmlContent</summary>*/
        public string HtmlContent { get; set; }

        /*<summary>The navigational property for MessageTemplate</summary>*/
        public virtual SyMessageTemplates MessageTemplate { get; set; }
    }
}