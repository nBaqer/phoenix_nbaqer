﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="AdReqs.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The AdReqs definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for AdReqs</summary>*/
    public partial class AdReqs
    {
        /*<summary>The constructor for AdReqs</summary>*/
        public AdReqs()
        {
            AdEntrTestOverRide = new HashSet<AdEntrTestOverRide>();
            AdLeadDocsReceived = new HashSet<AdLeadDocsReceived>();
            AdLeadEntranceTest = new HashSet<AdLeadEntranceTest>();
            AdLeadReqsReceived = new HashSet<AdLeadReqsReceived>();
            AdLeadTranReceived = new HashSet<AdLeadTranReceived>();
            AdLeadTransactions = new HashSet<AdLeadTransactions>();
            AdPrgVerTestDetails = new HashSet<AdPrgVerTestDetails>();
            AdReqGrpDef = new HashSet<AdReqGrpDef>();
            AdReqsEffectiveDates = new HashSet<AdReqsEffectiveDates>();
            PlStudentDocs = new HashSet<PlStudentDocs>();
            SyDocumentHistory = new HashSet<SyDocumentHistory>();
        }

        /*<summary>The get and set for AdReqId</summary>*/
        public Guid AdReqId { get; set; }
        /*<summary>The get and set for Code</summary>*/
        public string Code { get; set; }
        /*<summary>The get and set for Descrip</summary>*/
        public string Descrip { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid? StatusId { get; set; }
        /*<summary>The get and set for CampGrpId</summary>*/
        public Guid? CampGrpId { get; set; }
        /*<summary>The get and set for ModuleId</summary>*/
        public int? ModuleId { get; set; }
        /*<summary>The get and set for AdReqTypeId</summary>*/
        public int? AdReqTypeId { get; set; }
        /*<summary>The get and set for ModUser</summary>*/
        public string ModUser { get; set; }
        /*<summary>The get and set for ModDate</summary>*/
        public DateTime? ModDate { get; set; }
        /*<summary>The get and set for EdLvlId</summary>*/
        public Guid? EdLvlId { get; set; }
        /*<summary>The get and set for ReqforEnrollment</summary>*/
        public bool? ReqforEnrollment { get; set; }
        /*<summary>The get and set for ReqforFinancialAid</summary>*/
        public bool? ReqforFinancialAid { get; set; }
        /*<summary>The get and set for ReqforGraduation</summary>*/
        public bool? ReqforGraduation { get; set; }
        /*<summary>The get and set for Ipedsvalue</summary>*/
        public int? Ipedsvalue { get; set; }
        /*<summary>The get and set for IsSystemRequirement</summary>*/
        public bool? IsSystemRequirement { get; set; }
        /*<summary>The get and set for RequiredForTermination</summary>*/
        public bool? RequiredForTermination { get; set; }

        /*<summary>The navigational property for AdReqType</summary>*/
        public virtual AdReqTypes AdReqType { get; set; }
        /*<summary>The navigational property for CampGrp</summary>*/
        public virtual SyCampGrps CampGrp { get; set; }
        /*<summary>The navigational property for EdLvl</summary>*/
        public virtual AdEdLvls EdLvl { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for AdEntrTestOverRide</summary>*/
        public virtual ICollection<AdEntrTestOverRide>AdEntrTestOverRide { get; set; }
        /*<summary>The navigational property for AdLeadDocsReceived</summary>*/
        public virtual ICollection<AdLeadDocsReceived>AdLeadDocsReceived { get; set; }
        /*<summary>The navigational property for AdLeadEntranceTest</summary>*/
        public virtual ICollection<AdLeadEntranceTest>AdLeadEntranceTest { get; set; }
        /*<summary>The navigational property for AdLeadReqsReceived</summary>*/
        public virtual ICollection<AdLeadReqsReceived>AdLeadReqsReceived { get; set; }
        /*<summary>The navigational property for AdLeadTranReceived</summary>*/
        public virtual ICollection<AdLeadTranReceived>AdLeadTranReceived { get; set; }
        /*<summary>The navigational property for AdLeadTransactions</summary>*/
        public virtual ICollection<AdLeadTransactions>AdLeadTransactions { get; set; }
        /*<summary>The navigational property for AdPrgVerTestDetails</summary>*/
        public virtual ICollection<AdPrgVerTestDetails>AdPrgVerTestDetails { get; set; }
        /*<summary>The navigational property for AdReqGrpDef</summary>*/
        public virtual ICollection<AdReqGrpDef>AdReqGrpDef { get; set; }
        /*<summary>The navigational property for AdReqsEffectiveDates</summary>*/
        public virtual ICollection<AdReqsEffectiveDates>AdReqsEffectiveDates { get; set; }
        /*<summary>The navigational property for PlStudentDocs</summary>*/
        public virtual ICollection<PlStudentDocs>PlStudentDocs { get; set; }
        /*<summary>The navigational property for SyDocumentHistory</summary>*/
        public virtual ICollection<SyDocumentHistory>SyDocumentHistory { get; set; }
    }
}