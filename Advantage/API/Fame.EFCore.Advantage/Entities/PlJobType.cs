﻿/*--------------------------------------------------------------------------------------------------------------------
<copyright file="PlJobType.cs" company="FAME Inc.">
    FAME Inc. 2018
</copyright>
<summary>
    The PlJobType definition.
</summary>
--------------------------------------------------------------------------------------------------------------------*/
namespace Fame.EFCore.Advantage.Entities
{
using System;
using System.Collections.Generic;

    /*<summary>The entity definition for PlJobType</summary>*/
    public partial class PlJobType
    {
        /*<summary>The constructor for PlJobType</summary>*/
        public PlJobType()
        {
            PlEmployerJobCats = new HashSet<PlEmployerJobCats>();
            PlEmployerJobs = new HashSet<PlEmployerJobs>();
        }

        /*<summary>The get and set for JobGroupId</summary>*/
        public Guid JobGroupId { get; set; }
        /*<summary>The get and set for JobGroupCode</summary>*/
        public string JobGroupCode { get; set; }
        /*<summary>The get and set for StatusId</summary>*/
        public Guid StatusId { get; set; }
        /*<summary>The get and set for JobGroupDescrip</summary>*/
        public string JobGroupDescrip { get; set; }
        /*<summary>The get and set for CamGrpId</summary>*/
        public Guid CamGrpId { get; set; }

        /*<summary>The navigational property for CamGrp</summary>*/
        public virtual SyCampGrps CamGrp { get; set; }
        /*<summary>The navigational property for Status</summary>*/
        public virtual SyStatuses Status { get; set; }
        /*<summary>The navigational property for PlEmployerJobCats</summary>*/
        public virtual ICollection<PlEmployerJobCats>PlEmployerJobCats { get; set; }
        /*<summary>The navigational property for PlEmployerJobs</summary>*/
        public virtual ICollection<PlEmployerJobs>PlEmployerJobs { get; set; }
    }
}